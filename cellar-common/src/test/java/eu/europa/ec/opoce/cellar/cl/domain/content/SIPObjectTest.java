package eu.europa.ec.opoce.cellar.cl.domain.content;

import eu.europa.ec.opoce.cellar.exception.CellarException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static org.junit.Assert.*;

public class SIPObjectTest {

    private static Path sipPathWithoutRelativePath;
    private static List<File> sipPathWithoutRelativePathFilesList;
    private static Path sipPathWithRelativePath;
    private static List<File> sipPathWithRelativePathFilesList;
    private static Path sipPathWithoutMetsFile;
    private static List<File> sipPathWithoutMetsFileFilesList;

    private static Path relativePath;
    private SIPObject sipObject;

    @BeforeClass
    public static void oneTimeSetup() throws Exception {
        relativePath = Paths.get("src/test/resources/eu/europa/ec/opoce/cellar/cl/domain/content/sipFileWithRelativeHref_v1");

        sipPathWithRelativePath = Paths.get("src/test/resources/eu/europa/ec/opoce/cellar/cl/domain/content/sipFileWithRelativeHref_v1")
                .toAbsolutePath().normalize();
        sipPathWithRelativePathFilesList = getFileListFromPath(sipPathWithRelativePath);

        sipPathWithoutRelativePath = Paths
                .get("src/test/resources/eu/europa/ec/opoce/cellar/cl/domain/content/sipFileWithoutRelativeHref_v1").toAbsolutePath()
                .normalize();
        sipPathWithoutRelativePathFilesList = getFileListFromPath(sipPathWithoutRelativePath);

        sipPathWithoutMetsFile = Paths.get("src/test/resources/eu/europa/ec/opoce/cellar/cl/domain/content/sipFileWithoutMetsFile")
                .toAbsolutePath().normalize();
        sipPathWithoutMetsFileFilesList = getFileListFromPath(sipPathWithoutMetsFile);
    }

    private static List<File> getFileListFromPath(Path sipPath) throws Exception {
        Collection<File> filesCollection = FileUtils.listFiles(new File(sipPath.toString()), null, true);
        File[] filesList = filesCollection.toArray(new File[filesCollection.size()]);
        for (int i = 0; i < filesList.length; i++) {
            Path uncompressedFolderPath = Paths.get(sipPath.toString());
            Path relativePath = uncompressedFolderPath.relativize(filesList[i].toPath());
            filesList[i] = relativePath.normalize().toFile();
        }
        return Arrays.asList(filesList);
    }

    @Before
    public void setup() throws Exception {
        sipObject = new SIPObject();
    }

    @Test
    public void settingEmptyFilesListCreatesEmptyAbsolutePathMap() throws Exception {
        sipObject.setZipExtractionFolder(sipPathWithoutMetsFile.toFile());
        sipObject.setFileList(new ArrayList<>());
        Map<String, Path> absolutePathMap = sipObject.getAbsolutePathMap();
        assertEquals(sipObject.getFileList().isEmpty(), absolutePathMap.isEmpty());
    }

    @Test
    public void settingFilesListCreateMapsWithEqualNumberOfPath() throws Exception {
        sipObject.setZipExtractionFolder(sipPathWithRelativePath.toFile());
        assertTrue(sipObject.getFileList().isEmpty());
        assertTrue(sipObject.getAbsolutePathMap().isEmpty());

        assertEquals(4, sipPathWithRelativePathFilesList.size());

        sipObject.setFileList(sipPathWithRelativePathFilesList);
        List<File> fileList = sipObject.getFileList();
        Map<String, Path> absolutePathMap = sipObject.getAbsolutePathMap();
        assertEquals(fileList.size(), absolutePathMap.size());
    }

    @Test
    public void settingNonRelativePathFilesListCreatesMapWithAbsolutePath() throws Exception {
        sipObject.setZipExtractionFolder(sipPathWithoutRelativePath.toFile());
        sipObject.setFileList(sipPathWithoutRelativePathFilesList);
        checkIfAllFilesHaveAnAbsolutePathInMap(sipPathWithoutRelativePath);
    }

    @Test
    public void settingRelativePathFilesListCreatesMapWithAbsolutePath() throws Exception {
        sipObject.setZipExtractionFolder(sipPathWithRelativePath.toFile());
        sipObject.setFileList(sipPathWithRelativePathFilesList);
        checkIfAllFilesHaveAnAbsolutePathInMap(sipPathWithRelativePath);
    }

    private void checkIfAllFilesHaveAnAbsolutePathInMap(Path filesRootPath) {
        List<File> retrievedFileList = sipObject.getFileList();
        Map<String, Path> absolutePathMap = sipObject.getAbsolutePathMap();
        assertEquals(4, absolutePathMap.size());

        for (File file : retrievedFileList) {
            Path fileAbsolutePath = Paths.get(filesRootPath.toAbsolutePath().toString() + "/" + file.toString());
            Path relativePathKey = filesRootPath.relativize(fileAbsolutePath);
            Path absolutePath = absolutePathMap.get(relativePathKey.toString());
            assertTrue(Files.exists(absolutePath));
        }
    }

    @Test
    public void keysInAbsolutePathMapAreRelativePath() throws Exception {
        sipObject.setZipExtractionFolder(sipPathWithoutRelativePath.toFile());
        sipObject.setFileList(sipPathWithoutRelativePathFilesList);
        Map<String, Path> absolutePathMap = sipObject.getAbsolutePathMap();
        for (String key : absolutePathMap.keySet()) {
            Path absolutePath = absolutePathMap.get(key);
            Path relativePath = sipPathWithoutRelativePath.toAbsolutePath().relativize(absolutePath).normalize();
            assertEquals(relativePath.toString(), key);
        }
    }

    @Test(expected = CellarException.class)
    public void getNonExistingMetsFileThrowsException() throws Exception {
        sipObject.setZipExtractionFolder(sipPathWithoutMetsFile.toFile());
        sipObject.setFileList(sipPathWithoutMetsFileFilesList);
        assertNull(sipObject.getMetsFile());
    }

    @Test
    public void getExistingMetsFileReturnsMetsFile() throws Exception {
        sipObject.setZipExtractionFolder(sipPathWithRelativePath.toFile());
        sipObject.setFileList(sipPathWithRelativePathFilesList);
        assertNotNull(sipObject.getMetsFile());
    }

    @Test
    public void getNonExistingMetsFileAsStreamReturnsNull() throws Exception {
        sipObject.setZipExtractionFolder(sipPathWithoutMetsFile.toFile());
        sipObject.setFileList(sipPathWithoutMetsFileFilesList);
        try(InputStream stream=sipObject.getMetsStream()) {
            assertNull(stream);
        }
    }

    @Test
    public void getExistingMetsFileAsStreamReturnsMetsStream() throws Exception {
        sipObject.setZipExtractionFolder(sipPathWithRelativePath.toFile());
        sipObject.setFileList(sipPathWithRelativePathFilesList);
        try(InputStream stream=sipObject.getMetsStream()) {
            assertNotNull(stream);
        }
    }

    @Test
    public void getExistingFileWithoutRelativePathReturnsStream() throws Exception {
        sipObject.setZipExtractionFolder(sipPathWithoutRelativePath.toFile());
        sipObject.setFileList(sipPathWithoutRelativePathFilesList);
        try(InputStream stream = sipObject.getFileStream("L_2006088DA.01006301.xml");) {
            assertNotNull(stream);
        }
    }

    @Test
    public void getExistingFilesWithRelativePathReturnsStream() throws Exception {
        sipObject.setZipExtractionFolder(sipPathWithRelativePath.toFile());
        sipObject.setFileList(sipPathWithRelativePathFilesList);
        try(InputStream stream = sipObject.getFileStream("pdf/l_08820060325da00630064.pdf")) {
            assertNotNull(stream);
        }
    }

    @Test
    public void getNonExistingFileWithoutRelativePathReturnsNull() throws Exception {
        sipObject.setZipExtractionFolder(sipPathWithoutRelativePath.toFile());
        sipObject.setFileList(sipPathWithoutRelativePathFilesList);
        try(InputStream stream = sipObject.getFileStream("does_not_exist.xml");) {
            assertNull(stream);
        }
    }

    @Test
    public void getNonExistingFileWithRelativePathReturnsNull() throws Exception {
        sipObject.setZipExtractionFolder(sipPathWithRelativePath.toFile());
        sipObject.setFileList(sipPathWithRelativePathFilesList);
        try(InputStream stream = sipObject.getFileStream("does_not_exist.xml");){
            assertNull(stream);
        }
    }

    @Test
    public void getExistingFileWithRelativePathAndNotCorrectFileSeparatorForFileSystemReturnsFile() throws Exception {
        sipObject.setZipExtractionFolder(sipPathWithRelativePath.toFile());
        sipObject.setFileList(sipPathWithRelativePathFilesList);
        try(InputStream streamWithCorrectFileSeaparator = sipObject.getFileStream("xml/L_2006088DA.01006301.xml");
            InputStream streamWithIncorrectFileSeparator = sipObject.getFileStream("xml\\L_2006088DA.01006301.xml");) {

            String contentWithCorrectFileSeaparator = IOUtils.toString(streamWithCorrectFileSeaparator, "UTF-8");
            
            String contentWithIncorrectFileSeparator = IOUtils.toString(streamWithIncorrectFileSeparator, "UTF-8");

            assertEquals(contentWithCorrectFileSeaparator, contentWithIncorrectFileSeparator);
            assertTrue(contentWithCorrectFileSeaparator.equals(contentWithIncorrectFileSeparator));
        }
    }

    @Test
    public void setZipExtractionFolderTransformsThePathToAbsolutePath() throws Exception {
        sipObject.setZipExtractionFolder(sipPathWithRelativePath.toFile());
        File zipExtractionFolder = sipObject.getZipExtractionFolder();
        assertTrue(zipExtractionFolder.isAbsolute());

        sipObject.setZipExtractionFolder(relativePath.toFile());
        zipExtractionFolder = sipObject.getZipExtractionFolder();
        assertTrue(zipExtractionFolder.isAbsolute());
    }

    @Test
    public void windowsFileSepInStringAreReplacedByUnixFileSepOnUnix() throws Exception {
        String stringWithWindowsFileSep = "xml\\test.txt";
        String replacedFileSep = SIPObject.replaceWithUnixFileSeparator(stringWithWindowsFileSep);
        assertEquals("xml/test.txt", replacedFileSep);
    }

    @Test
    public void blankString_replaceWithUnixFileSeparator_emptyStringReturned() throws Exception {
        String returned = SIPObject.replaceWithUnixFileSeparator(null);
        assertEquals("", returned);
        returned = SIPObject.replaceWithUnixFileSeparator("");
        assertEquals("", returned);
    }

    @Test
    public void stringWithUnixPath_replaceWithUnixFileSeparator_stringNotChanged() throws Exception {
        String unixPathString = "/home/user/test/file.txt";
        assertEquals(unixPathString, SIPObject.replaceWithUnixFileSeparator(unixPathString));
    }

    @Test
    public void stringWithBackslash_replaceWithUnixFileSeparator_backslashAreReplacedBySlash() throws Exception {
        String pathWithBackslashString = "test\\with\\backslash\\test";
        assertEquals("test/with/backslash/test", SIPObject.replaceWithUnixFileSeparator(pathWithBackslashString));
    }

    @Test(expected = CellarException.class)
    public void getMetsFileContentThatWasDeletedFromSipThrowsException() throws Exception {
        // cleanup
        Path extractionDirectory = Paths.get(System.getProperty("java.io.tmpdir") + "/test_extraction_folder");
        Path metsFilePath = Paths.get("file.mets.xml");
        Path absolutePathMets = Paths.get(extractionDirectory + "/" + metsFilePath);

        Files.deleteIfExists(absolutePathMets);
        Files.deleteIfExists(extractionDirectory);
        Files.createDirectories(extractionDirectory);
        Files.createFile(absolutePathMets);

        // create a valid scenario, a mets file is found in a uncompress SIP package
        sipObject.setZipExtractionFolder(extractionDirectory.toFile());
        sipObject.setFileList(Collections.singletonList(metsFilePath.toFile()));
        assertEquals(1, sipObject.getAbsolutePathMap().size());
        try(InputStream is=sipObject.getMetsStream()) {
            assertNotNull(is);
        }

        // delete the mets file on the file system
        Files.deleteIfExists(absolutePathMets);
        InputStream is=null;
        try {
            // mets file is not found on the file system so this throws an exception
           is= sipObject.getMetsStream();
        } finally {
            if(is!=null)IOUtils.closeQuietly(is);
            Files.deleteIfExists(absolutePathMets);
            Files.deleteIfExists(extractionDirectory);
        }
    }

    @Test(expected = CellarException.class)
    public void fileDeletedFromUncompressedSip_getFileStream_exceptionThrown() throws Exception {
        // cleanup
        Path extractionDirectory = Paths.get(System.getProperty("java.io.tmpdir") + "/test_extraction_folder");
        Path filePath = Paths.get("file.rdf");
        Path absoluteFilePath = Paths.get(extractionDirectory + "/" + filePath);

        Files.deleteIfExists(absoluteFilePath);
        Files.deleteIfExists(extractionDirectory);
        Files.createDirectories(extractionDirectory);
        Files.createFile(absoluteFilePath);

        // create a valid scenario, a file is found in a uncompress SIP package
        sipObject.setZipExtractionFolder(extractionDirectory.toFile());
        sipObject.setFileList(Collections.singletonList(filePath.toFile()));
        assertEquals(1, sipObject.getAbsolutePathMap().size());
        try(InputStream is=sipObject.getFileStream(filePath.toString())) {
            assertNotNull(is);
        }

        // delete the file on the file system
        Files.deleteIfExists(absoluteFilePath);
        InputStream is= null;
        try {
            // file is not found on the file system so this throws an exception
            is=sipObject.getFileStream(filePath.toString());
        } finally {
            if(is!=null)IOUtils.closeQuietly(is);
            Files.deleteIfExists(absoluteFilePath);
            Files.deleteIfExists(extractionDirectory);
        }
    }
}
