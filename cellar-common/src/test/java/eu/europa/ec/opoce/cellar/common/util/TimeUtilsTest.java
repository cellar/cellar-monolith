/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.util
 *             FILE : BoundedPriorityBlockingQueueTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 16, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.util;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 16, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class TimeUtilsTest {

    @Ignore
    @Test
    public void formatHTTP11DateTest() {
        Date date = null;
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            date = simpleDateFormat.parse("22/06/2006");
        } catch (final ParseException e) {
            e.printStackTrace();
        }
        final String dateStr = TimeUtils.formatHTTP11Date(date);
        final String dateStrExpected = "Wed, 21 Jun 2006 22:00:00 GMT";
        Assert.assertEquals(dateStr, dateStrExpected);
    }

    @Test
    public void formatTimestampForSparqlQueryTest() {
        Date date = null;
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            date = simpleDateFormat.parse("22/06/2006");
        } catch (final ParseException e) {
            e.printStackTrace();
        }
        final String dateStr = TimeUtils.formatTimestampForSparqlQuery(date);
        final String dateStrExpected = "2006-06-22T00:00:00.0Z";
        Assert.assertEquals(dateStr, dateStrExpected);
    }

    @Test
    public void formatTimestampForyyyyMMddTest() {
        Date date = null;
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            date = simpleDateFormat.parse("22/06/2006");
        } catch (final ParseException e) {
            e.printStackTrace();
        }
        final String dateStr = TimeUtils.formatTimestampForyyyyMMdd(date);
        final String dateStrExpected = "20060622";
        Assert.assertEquals(dateStr, dateStrExpected);
    }

    @Test
    public void getDefaultDateTest() {
        final String dateStr = "22/06/2006";
        final boolean strictly = true;
        try {
            final Date defaultDate = TimeUtils.getDefaultDate(dateStr, strictly);

            final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
            final String format = simpleDateFormat.format(defaultDate);
            Assert.assertEquals(dateStr, format);
        } catch (final ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getDefaultDateSilentlyTest() {
        final String dateStr = "22/06/2006";
        final boolean strictly = true;
        final Date defaultDate = TimeUtils.getDefaultDateSilently(dateStr, strictly);

        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        final String format = simpleDateFormat.format(defaultDate);
        Assert.assertEquals(dateStr, format);
    }

    @Test
    public void getDefaultDateTimeTest() {
        final String dateStr = "22/06/2006";
        final String timeStr = "10:10";
        final boolean strictly = true;
        final Date defaultDate;
        try {
            defaultDate = TimeUtils.getDefaultDateTime(dateStr, timeStr, strictly);
            final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
            final String format = simpleDateFormat.format(defaultDate);
            final String dateStrExpected = "22/06/2006 10:10";
            Assert.assertEquals(dateStrExpected, format);
        } catch (final ParseException e) {
            e.printStackTrace();
        }
    }

}
