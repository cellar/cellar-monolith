/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.nal
 *             FILE : NalUtilsTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 01 08, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-01-08 13:23:31 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.nal;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

/**
 * @author ARHS Developments
 */
public class NalOntoUtilsTest {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void testRetrievalTopUri() throws Exception {
        assertEquals("http://publications.europa.eu/resource/authority/multilingual", NalOntoUtils.getBaseUri("http://publications.europa.eu/resource/authority/multilingual/ABC"));
        assertEquals("http://publications.europa.eu/resource/authority/multilingual", NalOntoUtils.getBaseUri("http://publications.europa.eu/resource/authority/multilingual/A_B_C"));
        assertEquals("http://publications.europa.eu/resource/authority/multilingual", NalOntoUtils.getBaseUri("http://publications.europa.eu/resource/authority/multilingual/A-B-C"));
        assertEquals("http://publications.europa.eu/resource/authority/multilingual", NalOntoUtils.getBaseUri("http://publications.europa.eu/resource/authority/multilingual/ABC"));
        assertEquals("http://publications.europa.eu/resource/authority/multilingual", NalOntoUtils.getBaseUri("http://publications.europa.eu/resource/authority/multilingual"));
        assertEquals("http://publications.europa.eu/resource/authority/multilingual", NalOntoUtils.getBaseUri("http://publications.europa.eu/resource/authority/multilingual/"));
        assertEquals("http://eurovoc.europa.eu/100141", NalOntoUtils.getBaseUri("http://eurovoc.europa.eu/100125"));
        assertEquals("http://eurovoc.europa.eu/100141", NalOntoUtils.getBaseUri("http://eurovoc.europa.eu/100141"));
        assertEquals("http://eurovoc.europa.eu/100141", NalOntoUtils.getBaseUri("http://eurovoc.europa.eu/alignment/eurovoc_alignment_gnd"));
    }

    @Test
    public void testFlattenedUri() throws Exception {
        assertEquals("http___publications_europa_eu_resource_authority_multilingual", NalOntoUtils.flattenNalUri("http://publications.europa.eu/resource/authority/multilingual"));
        assertEquals("http___eurovoc_europa_eu_100141", NalOntoUtils.flattenNalUri("http://eurovoc.europa.eu/100141"));
    }
}