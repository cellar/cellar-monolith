/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.util
 *             FILE : BoundedPriorityBlockingQueueTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 16, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.util;

import eu.europa.ec.opoce.cellar.cl.configuration.PrefixConfigurationService;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 16, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest(ServiceLocator.class)
@PowerMockIgnore("javax.management.*")
public class DigitalObjectUtilsTest {




    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(ServiceLocator.class);
        when(ServiceLocator.getService(PrefixConfigurationService.class)).thenReturn(mock(PrefixConfigurationService.class));
    }

    @Test
    public void sortListItemFirst() {

        final List<DigitalObject> digitalObjects = new LinkedList<>();

        DigitalObject digitalObject = new DigitalObject(null);
        digitalObject.setCellarId(new ContentIdentifier("cellar:7b3c57be-1872-11e7-bf59-01aa75ed71a1.0001.02/DOC_1", true));
        digitalObjects.add(digitalObject);

        digitalObject = new DigitalObject(null);
        digitalObject.setCellarId(new ContentIdentifier("cellar:7b3c57be-1872-11e7-bf59-01aa75ed71a1.0003.01", true));
        digitalObjects.add(digitalObject);

        digitalObject = new DigitalObject(null);
        digitalObject.setCellarId(new ContentIdentifier("cellar:7b3c57be-1872-11e7-bf59-01aa75ed71a1.0003.02", true));
        digitalObjects.add(digitalObject);

        digitalObject = new DigitalObject(null);
        digitalObject.setCellarId(new ContentIdentifier("cellar:7b3c57be-1872-11e7-bf59-01aa75ed71a1.0001.02", true));
        digitalObjects.add(digitalObject);

        digitalObject = new DigitalObject(null);
        digitalObject.setCellarId(new ContentIdentifier("cellar:7b3c57be-1872-11e7-bf59-01aa75ed71a1", true));
        digitalObjects.add(digitalObject);

        digitalObject = new DigitalObject(null);
        digitalObject.setCellarId(new ContentIdentifier("cellar:7b3c57be-1872-11e7-bf59-01aa75ed71a1.0002.02", true));
        digitalObjects.add(digitalObject);

        digitalObject = new DigitalObject(null);
        digitalObject.setCellarId(new ContentIdentifier("cellar:7b3c57be-1872-11e7-bf59-01aa75ed71a1.0001", true));
        digitalObjects.add(digitalObject);

        digitalObject = new DigitalObject(null);
        digitalObject.setCellarId(new ContentIdentifier("cellar:7b3c57be-1872-11e7-bf59-01aa75ed71a1.0002", true));
        digitalObjects.add(digitalObject);

        digitalObject = new DigitalObject(null);
        digitalObject.setCellarId(new ContentIdentifier("cellar:7b3c57be-1872-11e7-bf59-01aa75ed71a1.0003", true));
        digitalObjects.add(digitalObject);

        digitalObject = new DigitalObject(null);
        digitalObject.setCellarId(new ContentIdentifier("cellar:7b3c57be-1872-11e7-bf59-01aa75ed71a1.0003.01/DOC_1", true));
        digitalObjects.add(digitalObject);

        final List<DigitalObject> sortedDigitalObjects = DigitalObjectUtils.sortListItemFirst(digitalObjects);

        final String[] sortedCellarIds = {
                "cellar:7b3c57be-1872-11e7-bf59-01aa75ed71a1.0003.02",
                "cellar:7b3c57be-1872-11e7-bf59-01aa75ed71a1.0003.01/DOC_1",
                "cellar:7b3c57be-1872-11e7-bf59-01aa75ed71a1.0003.01",
                "cellar:7b3c57be-1872-11e7-bf59-01aa75ed71a1.0003",
                "cellar:7b3c57be-1872-11e7-bf59-01aa75ed71a1.0002.02",
                "cellar:7b3c57be-1872-11e7-bf59-01aa75ed71a1.0002",
                "cellar:7b3c57be-1872-11e7-bf59-01aa75ed71a1.0001.02/DOC_1",
                "cellar:7b3c57be-1872-11e7-bf59-01aa75ed71a1.0001.02",
                "cellar:7b3c57be-1872-11e7-bf59-01aa75ed71a1.0001",
                "cellar:7b3c57be-1872-11e7-bf59-01aa75ed71a1"
        };


        for(int i = 0; i < sortedCellarIds.length; i++) {
            Assert.assertEquals(sortedCellarIds[i], sortedDigitalObjects.get(i).getCellarId().getIdentifier());
        }
    }

    @Test
    public void addProductionIdentifiersTest() {
        final List<String> productionIdentifers = new ArrayList<String>();
        final DigitalObject digitalObject = mock(DigitalObject.class);
        List<ContentIdentifier> contentidentifiers = new ArrayList<ContentIdentifier>();
        final ContentIdentifier contentIdentifier1 = mock(ContentIdentifier.class);
        final ContentIdentifier contentIdentifier2 = mock(ContentIdentifier.class);
        final ContentIdentifier contentIdentifier3 = mock(ContentIdentifier.class);
        contentidentifiers.add(contentIdentifier1);
        contentidentifiers.add(contentIdentifier2);
        contentidentifiers.add(contentIdentifier3);
        Mockito.when(digitalObject.getContentids()).thenReturn(contentidentifiers);
        DigitalObjectUtils.addProductionIdentifiers(productionIdentifers, digitalObject);
        Assert.assertEquals(productionIdentifers.size(), 3);
    }

    @Test
    public void extractProductionIdentifiersTest() {
        final DigitalObject digitalObject = mock(DigitalObject.class);
        List<ContentIdentifier> contentidentifiers = new ArrayList<ContentIdentifier>();
        final ContentIdentifier contentIdentifier1 = mock(ContentIdentifier.class);
        final ContentIdentifier contentIdentifier2 = mock(ContentIdentifier.class);
        final ContentIdentifier contentIdentifier3 = mock(ContentIdentifier.class);
        contentidentifiers.add(contentIdentifier1);
        contentidentifiers.add(contentIdentifier2);
        contentidentifiers.add(contentIdentifier3);
        Mockito.when(digitalObject.getContentids()).thenReturn(contentidentifiers);
        final List<String> pids = DigitalObjectUtils.extractProductionIdentifiers(digitalObject);
        Assert.assertEquals(pids.size(), 3);
    }

}
