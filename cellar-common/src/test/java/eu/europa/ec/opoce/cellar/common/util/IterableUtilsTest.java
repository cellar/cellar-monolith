package eu.europa.ec.opoce.cellar.common.util;

/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.quad.impl
 *             FILE : IterableUtilsTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 8, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2016-09-01 17:46:05 +0200 (Thu, 01 Sep 2016) $
 *          VERSION : $LastChangedRevision: 11526 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.RoundingMode;
import java.security.SecureRandom;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.Map;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 8, 2017
 *
 * @author ARHS Developments
 * @version $Revision: 11526 $
 */
public class IterableUtilsTest {

    private static final DecimalFormat DF = new DecimalFormat("#.####");
    private static final String COLL_NAME = "coll";

    private final Map<String, Object> map = Maps.newHashMap();

    @BeforeClass
    public static void initTestClass() throws Exception {
        DF.setRoundingMode(RoundingMode.CEILING);
    }

    @Before
    public void initTest() throws Exception {
        map.put(COLL_NAME, initRandomCollection(15));
    }

    @Test
    public void testVoidValues() throws Exception {
        map.put(COLL_NAME, initRandomCollection(0));
        pageUpAndAssert(30);
    }

    @Test
    public void testPaging() throws Exception {
        pageUpAndAssert(4);
    }

    @Test
    public void testOneExactPage() throws Exception {
        pageUpAndAssert(15);
    }

    @Test
    public void testExceededPaging() throws Exception {
        pageUpAndAssert(100);
    }

    @Test
    public void testHugePaging() throws Exception {
        map.put(COLL_NAME, initRandomCollection(1000000));
        pageUpAndAssert(250);
    }

    private void pageUpAndAssert(final int pageSize) {
        final Wrapper<Double> tot = new Wrapper<>(0D);

        IterableUtils.pageUp(map, pageSize, innerMap -> {
            final Collection<Double> innerColl = ((Collection) innerMap.get(COLL_NAME));
            tot.setValue(tot.getValue() + calcCollectionValues(innerColl));
        });

        assertValue(tot.getValue());
    }

    private void assertValue(final Double expectedValue) {
        Assert.assertEquals(DF.format(expectedValue), DF.format(calcCollectionValues((Collection<Double>) map.get(COLL_NAME))));
    }

    private static Double calcCollectionValues(final Collection<Double> values) {
        Double total = 0d;
        for (Double value : values) {
            total += value;
        }
        return total;
    }

    private static Collection<Double> initRandomCollection(final int size) {
        final Collection<Double> values = Sets.newHashSet();
        for (int i = 0; i < size; i++) {
            SecureRandom random = new SecureRandom();
            values.add(random.nextDouble());
        }
        return values;
    }

}
