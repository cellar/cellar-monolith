package eu.europa.ec.opoce.cellar.cl.domain.admin;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CellarIdentifierTest {

    /** The test subject_ a. */
    private CellarIdentifier testSubject_A;

    /** The test subject_ b. */
    private CellarIdentifier testSubject_B;

    /** The test subject_ c. */
    private CellarIdentifier testSubject_C;

    /** The test subject_ d. */
    private CellarIdentifier testSubject_D;

    @Before
    public void before() throws Exception {
        testSubject_A = generateObject(new Long(1), 1, "CELLAR:NEW.CELLAR.IDA");
        testSubject_B = generateObject(new Long(2), 2, "CELLAR:NEW.CELLAR.IDB");
        testSubject_C = generateObject(new Long(1), 1, "CELLAR:NEW.CELLAR.IDA");
        testSubject_D = generateObject(new Long(4), 4, "CELLAR:NEW.CELLAR.IDA");
    }

    private CellarIdentifier generateObject(final Long id, final Integer docIndex, final String uuid) {
        final CellarIdentifier result = new CellarIdentifier();
        result.setId(id);
        result.setDocIndex(docIndex);
        result.setUuid(uuid);
        return result;
    }

    @Test
    public void testEquals() {
        Assert.assertTrue(testSubject_A.equals(testSubject_C));
        Assert.assertTrue(!testSubject_A.equals(testSubject_B));
        Assert.assertTrue(!testSubject_A.equals(testSubject_D));
    }

    @Test
    public void testHashCode() {
        Assert.assertTrue(testSubject_A.hashCode() == testSubject_C.hashCode());
        Assert.assertTrue(testSubject_A.hashCode() != testSubject_B.hashCode());
        Assert.assertTrue(testSubject_A.hashCode() != testSubject_D.hashCode());
    }

    @Test
    public void testToString() {
        Assert.assertTrue(testSubject_A.toString().equals(testSubject_C.toString()));
        Assert.assertTrue(!testSubject_A.toString().equals(testSubject_B.toString()));
        Assert.assertTrue(!testSubject_A.toString().equals(testSubject_D.toString()));
    }
}
