/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.util
 *             FILE : BoundedPriorityBlockingQueueTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 16, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.util;

import org.junit.Assert;
import org.junit.Test;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 16, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class CellarIdUtilsTest {
    private static String ITEM_WITH_CELLAR = "cellar:faf5669c-c382-11e6-8b7e-01aa75ed71a1.0001.01/DOC_1";
    private static String ITEM_WITHOUT_CELLAR = "faf5669c-c382-11e6-8b7e-01aa75ed71a1.0001.01/DOC_1";
    private static String MANIFESTATION_WITH_CELLAR = "cellar:faf5669c-c382-11e6-8b7e-01aa75ed71a1.0001.01";
    private static String MANIFESTATION_WITHOUT_CELLAR = "faf5669c-c382-11e6-8b7e-01aa75ed71a1.0001.01";
    private static String EXPRESSION_WITH_CELLAR = "cellar:faf5669c-c382-11e6-8b7e-01aa75ed71a1.0001";
    private static String EXPRESSION_WITHOUT_CELLAR = "faf5669c-c382-11e6-8b7e-01aa75ed71a1.0001";
    private static String WORK_WITH_CELLAR = "cellar:faf5669c-c382-11e6-8b7e-01aa75ed71a1";
    private static String WORK_WITHOUT_CELLAR = "faf5669c-c382-11e6-8b7e-01aa75ed71a1";

    @Test
    public void getCellarIdAsFilenameTest() {
        final String cellarId = "cellar:fefa50c4-f1aa-11e5-9d87-01aa75ed71a1";
        final String cellarIdAsFilename = CellarIdUtils.getCellarIdAsFilename(cellarId);
        final String cellarIdAsFilenameExpected = "cellar_fefa50c4-f1aa-11e5-9d87-01aa75ed71a1";
        Assert.assertEquals(cellarIdAsFilename, cellarIdAsFilenameExpected);
    }

    @Test
    public void getCellarIdAsFilenameExtensionTest() {
        final String cellarId = "cellar:fefa50c4-f1aa-11e5-9d87-01aa75ed71a1";
        final String extension = ".xml";
        final String cellarIdAsFilename = CellarIdUtils.getCellarIdAsFilename(cellarId, extension);
        final String cellarIdAsFilenameExpected = "cellar_fefa50c4-f1aa-11e5-9d87-01aa75ed71a1.xml";
        Assert.assertEquals(cellarIdAsFilename, cellarIdAsFilenameExpected);
    }

    @Test
    public void getCellarIdBaseTest() {
        String cellarId = "cellar:fefa50c4-f1aa-11e5-9d87-01aa75ed71a1";
        String cellarIdBase = CellarIdUtils.getCellarIdBase(cellarId);
        String cellarIdBaseExpected = "cellar:fefa50c4-f1aa-11e5-9d87-01aa75ed71a1";
        Assert.assertEquals(cellarIdBase, cellarIdBaseExpected);

        cellarId = "cellar:fefa50c4-f1aa-11e5-9d87-01aa75ed71a1.0001";
        cellarIdBase = CellarIdUtils.getCellarIdBase(cellarId);
        Assert.assertEquals(cellarIdBase, cellarIdBaseExpected);

        cellarId = "cellar:fefa50c4-f1aa-11e5-9d87-01aa75ed71a1.0001.01";
        cellarIdBase = CellarIdUtils.getCellarIdBase(cellarId);
        Assert.assertEquals(cellarIdBase, cellarIdBaseExpected);

        cellarId = "cellar:fefa50c4-f1aa-11e5-9d87-01aa75ed71a1.0001.01/DOC_1";
        cellarIdBase = CellarIdUtils.getCellarIdBase(cellarId);
        Assert.assertEquals(cellarIdBase, cellarIdBaseExpected);
    }

    @Test
    public void getCellarIdsTest() {
        final List<String> cellarIds = new LinkedList<String>();
        String cellarId = "cellar:fefa50c4-f1aa-11e5-9d87-01aa75ed71a1";
        String pId = "oj:JOL_2006_088_R_0063_01";
        cellarIds.add(cellarId);
        cellarIds.add(pId);
        final Collection<String> cellarIdList = CellarIdUtils.getCellarIds(cellarIds);
        Assert.assertEquals(cellarIdList.size(), 1);
        Assert.assertTrue(cellarIdList.contains(cellarId));
    }

    @Test
    public void getParentCellarIdTest() {
        String cellarId = "cellar:fefa50c4-f1aa-11e5-9d87-01aa75ed71a1";
        String cellarIdParent = CellarIdUtils.getParentCellarId(cellarId);
        Assert.assertEquals(cellarIdParent, null);

        cellarId = "cellar:fefa50c4-f1aa-11e5-9d87-01aa75ed71a1.0001";
        cellarIdParent = CellarIdUtils.getParentCellarId(cellarId);
        String cellarIdParentExpected = "cellar:fefa50c4-f1aa-11e5-9d87-01aa75ed71a1";
        Assert.assertEquals(cellarIdParent, cellarIdParentExpected);

        cellarId = "cellar:fefa50c4-f1aa-11e5-9d87-01aa75ed71a1.0001.01";
        cellarIdParent = CellarIdUtils.getParentCellarId(cellarId);
        cellarIdParentExpected = "cellar:fefa50c4-f1aa-11e5-9d87-01aa75ed71a1.0001";
        Assert.assertEquals(cellarIdParent, cellarIdParentExpected);

        cellarId = "cellar:fefa50c4-f1aa-11e5-9d87-01aa75ed71a1.0001.01/DOC_1";
        cellarIdParent = CellarIdUtils.getParentCellarId(cellarId);
        cellarIdParentExpected = "cellar:fefa50c4-f1aa-11e5-9d87-01aa75ed71a1.0001.01";
        Assert.assertEquals(cellarIdParent, cellarIdParentExpected);
    }

    @Test(expected = IllegalArgumentException.class)
    public void getCellarIdSuffixExceptionTest() {

        String cellarId = "";
        String cellarIdSuffix = CellarIdUtils.getCellarIdSuffix(cellarId);
        Assert.assertEquals(cellarIdSuffix, null);

        cellarId = "c";
        cellarIdSuffix = CellarIdUtils.getCellarIdSuffix(cellarId);
        Assert.assertEquals(cellarIdSuffix, null);

        cellarId = "cellar";
        cellarIdSuffix = CellarIdUtils.getCellarIdSuffix(cellarId);
        Assert.assertEquals(cellarIdSuffix, null);
    }

    @Test
    public void getCellarIdSuffixTest() {
        String cellarId = "cellar:";
        String cellarIdSuffix = CellarIdUtils.getCellarIdSuffix(cellarId);
        Assert.assertEquals(cellarIdSuffix, null);

        cellarId = "cellar:fefa50c4-f1aa-11e5-9d87-01aa75ed71a1";
        cellarIdSuffix = CellarIdUtils.getCellarIdSuffix(cellarId);
        Assert.assertEquals(cellarIdSuffix, "fefa50c4-f1aa-11e5-9d87-01aa75ed71a1");

        cellarId = "cellar:fefa50c4-f1aa-11e5-9d87-01aa75ed71a1.0001";
        cellarIdSuffix = CellarIdUtils.getCellarIdSuffix(cellarId);
        String cellarIdSuffixExpected = "fefa50c4-f1aa-11e5-9d87-01aa75ed71a1.0001";
        Assert.assertEquals(cellarIdSuffix, cellarIdSuffixExpected);

        cellarId = "cellar:fefa50c4-f1aa-11e5-9d87-01aa75ed71a1.0001.01";
        cellarIdSuffix = CellarIdUtils.getCellarIdSuffix(cellarId);
        cellarIdSuffixExpected = "fefa50c4-f1aa-11e5-9d87-01aa75ed71a1.0001.01";
        Assert.assertEquals(cellarIdSuffix, cellarIdSuffixExpected);

        cellarId = "cellar:fefa50c4-f1aa-11e5-9d87-01aa75ed71a1.0001.01/DOC_1";
        cellarIdSuffix = CellarIdUtils.getCellarIdSuffix(cellarId);
        cellarIdSuffixExpected = "fefa50c4-f1aa-11e5-9d87-01aa75ed71a1.0001.01/DOC_1";
        Assert.assertEquals(cellarIdSuffix, cellarIdSuffixExpected);
    }

    @Test
    public void emptyCellarIdReturnsEmptyPathAsString() throws Exception {
        String actual = CellarIdUtils.createPathFromCellarId("");
        assertEquals("", actual);
    }

    @Test
    public void cellarIdOfWorkReturnsEmptyPathAsString() throws Exception {
        String actual = CellarIdUtils.createPathFromCellarId(WORK_WITH_CELLAR);
        assertEquals("", actual);
    }

    @Test
    public void cellarIdOfWorkWithoutCellarReturnsEmptyPathAsString() throws Exception {
        String actual = CellarIdUtils.createPathFromCellarId(WORK_WITHOUT_CELLAR);
        assertEquals("", actual);
    }

    @Test
    public void cellarIdOfExpressionReturnsExpressionIdentifierPathAsString() throws Exception {
        String actual = CellarIdUtils.createPathFromCellarId(EXPRESSION_WITH_CELLAR);
        assertEquals("0001", actual);
    }

    @Test
    public void cellarIdOfExpressionWithoutCellarReturnsExpressionIdentifierPathAsString() throws Exception {
        String actual = CellarIdUtils.createPathFromCellarId(EXPRESSION_WITHOUT_CELLAR);
        assertEquals("0001", actual);
    }

    @Test
    public void cellarIdOfManifestationReturnsExpressionAndManifestationPathAsString() throws Exception {
        String actual = CellarIdUtils.createPathFromCellarId(MANIFESTATION_WITH_CELLAR);
        assertEquals("0001/01", actual);
    }

    @Test
    public void cellarIdOfManifestationWithoutCellarReturnsExpressionAndManifestationPathAsString() throws Exception {
        String actual = CellarIdUtils.createPathFromCellarId(MANIFESTATION_WITHOUT_CELLAR);
        assertEquals("0001/01", actual);
    }

    @Test
    public void cellarIdOfItemReturnsExpressionAndManifestationAndDocItemPathAsString() throws Exception {
        String actual = CellarIdUtils.createPathFromCellarId(ITEM_WITH_CELLAR);
        assertEquals("0001/01/DOC_1", actual);
    }

    @Test
    public void cellarIdOfItemWithoutCellarReturnsExpressionAndManifestationAndDocItemPathAsString() throws Exception {
        String actual = CellarIdUtils.createPathFromCellarId(ITEM_WITHOUT_CELLAR);
        assertEquals("0001/01/DOC_1", actual);
    }
}
