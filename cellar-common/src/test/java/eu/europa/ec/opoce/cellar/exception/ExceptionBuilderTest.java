/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.exception
 *             FILE : ExceptionBuilderTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 02-08-2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2016-09-01 17:46:05 +0200 (Thu, 01 Sep 2016) $
 *          VERSION : $LastChangedRevision: 11526 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.exception;

import org.junit.Test;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 02-08-2017
 *
 * @author ARHS Developments
 * @version $Revision: 11526 $
 */
public class ExceptionBuilderTest {

    @Test(expected = CellarException.class)
    public void throwWhenNoCondition() throws Exception {
        ExceptionBuilder.throwExc(CellarException.class);
    }

    @Test(expected = CellarException.class)
    public void throwWhenConditionIsNull() throws Exception {
        ExceptionBuilder.throwExc(CellarException.class, null);
    }

    @Test(expected = CellarException.class)
    public void throwWhenConditionIsTrue() throws Exception {
        ExceptionBuilder.throwExc(CellarException.class, Boolean.TRUE);
    }

    @Test(expected = CellarException.class)
    public void throwWhenOneConditionIsNullAndAnotherIsTrue() throws Exception {
        ExceptionBuilder.throwExc(CellarException.class, null, Boolean.TRUE);
    }

    @Test(expected = CellarException.class)
    public void throwWhenAtLeastOneConditionIsTrue() throws Exception {
        ExceptionBuilder.throwExc(CellarException.class, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE);
    }

    @Test
    public void dontThrowWhenConditionIsFalse() throws Exception {
        ExceptionBuilder.throwExc(CellarException.class, Boolean.FALSE);
    }

    @Test
    public void dontThrowWhenAllConditionsAreFalse() throws Exception {
        ExceptionBuilder.throwExc(CellarException.class, Boolean.FALSE, Boolean.FALSE, Boolean.FALSE);
    }

    @Test
    public void dontThrowWhenOneConditionIsNullAndAllOthersAreFalse() throws Exception {
        ExceptionBuilder.throwExc(CellarException.class, null, Boolean.FALSE);
    }

}
