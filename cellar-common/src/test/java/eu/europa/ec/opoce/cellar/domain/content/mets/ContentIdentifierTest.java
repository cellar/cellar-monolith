package eu.europa.ec.opoce.cellar.domain.content.mets;

import eu.europa.ec.opoce.cellar.cl.configuration.PrefixConfigurationService;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.*;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ServiceLocator.class)
public class ContentIdentifierTest {

    private ContentIdentifier contentIdentifierOne;
    private ContentIdentifier contentIdentifierTwo;

    @Before
    public void setUp() throws Exception {
        final PrefixConfigurationService prefixConfigurationService = Mockito.mock(PrefixConfigurationService.class);
        Mockito.when(prefixConfigurationService.getPrefixConfiguration()).thenReturn(null);
        PowerMockito.mockStatic(ServiceLocator.class);
        PowerMockito.when(ServiceLocator.getService(PrefixConfigurationService.class)).thenReturn(prefixConfigurationService);
        contentIdentifierOne = new ContentIdentifier("contentIdentifierOne", true);
        contentIdentifierTwo = new ContentIdentifier("contentIdentifierTwo", true);
    }

    @Test
    public void contentIdentifierNotEqualNull() throws Exception {
        assertFalse(contentIdentifierOne.equals(null));
    }

    @Test
    public void contentIdentifierEqualsItself() throws Exception {
        assertEquals(contentIdentifierOne, contentIdentifierOne);
        assertEquals(contentIdentifierOne.hashCode(), contentIdentifierOne.hashCode());
    }

    @Test
    public void contentIdentifiersWithDifferentContentIdAreDifferent() throws Exception {
        assertNotEquals(contentIdentifierOne, contentIdentifierTwo);
        assertNotEquals(contentIdentifierOne.hashCode(), contentIdentifierTwo.hashCode());
    }

    @Test
    public void contentIdentifiersWithSameContentIdAreEquals() throws Exception {
        ContentIdentifier contentIdentifierSameOne = new ContentIdentifier("contentIdentifierOne", true);
        assertEquals(contentIdentifierOne, contentIdentifierSameOne);
        assertEquals(contentIdentifierOne.hashCode(), contentIdentifierSameOne.hashCode());
    }
}
