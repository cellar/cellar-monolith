package eu.europa.ec.opoce.cellar.cl.domain.admin;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ProductionIdentifierTest {

    /** The test subject_ a. */
    private ProductionIdentifier testSubject_A;

    /** The test subject_ b. */
    private ProductionIdentifier testSubject_B;

    /** The test subject_ c. */
    private ProductionIdentifier testSubject_C;

    /** The test subject_ d. */
    private ProductionIdentifier testSubject_D;

    /**
     * Before.
     *
     * @throws Exception the exception
     */
    @Before
    public void before() throws Exception {
        testSubject_A = generateObject(new Long(1), "OJ:JOL_2006_088_R_0063_01");
        testSubject_B = generateObject(new Long(2), "OJ:JOL_2006_088_R_0063_02");
        testSubject_C = generateObject(new Long(1), "OJ:JOL_2006_088_R_0063_01");
        testSubject_D = generateObject(new Long(4), "OJ:JOL_2006_088_R_0063_01");
    }

    /**
     * Generate object.
     *
     * @param id the id
     * @param docIndex the doc index
     * @param status the status
     * @param uuid the uuid
     * @return the cellar identifier
     */
    private ProductionIdentifier generateObject(final Long id, final String productionId) {
        final ProductionIdentifier result = new ProductionIdentifier();
        result.setId(id);
        result.setCellarIdentifier(null);
        result.setProductionId(productionId);
        return result;
    }

    /**
     * Test equals.
     */
    @Test
    public void testEquals() {
        Assert.assertTrue(testSubject_A.equals(testSubject_C));
        Assert.assertTrue(!testSubject_A.equals(testSubject_B));
        Assert.assertTrue(!testSubject_A.equals(testSubject_D));
    }

    /**
     * Test hash code.
     */
    @Test
    public void testHashCode() {
        Assert.assertTrue(testSubject_A.hashCode() == testSubject_C.hashCode());
        Assert.assertTrue(testSubject_A.hashCode() != testSubject_B.hashCode());
        Assert.assertTrue(testSubject_A.hashCode() != testSubject_D.hashCode());
    }

    /**
     * Test to string.
     */
    @Test
    public void testToString() {
        Assert.assertTrue(testSubject_A.toString().equals(testSubject_C.toString()));
        Assert.assertTrue(!testSubject_A.toString().equals(testSubject_B.toString()));
        Assert.assertTrue(!testSubject_A.toString().equals(testSubject_D.toString()));
    }

}
