/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.monitoring
 *             FILE : RequestMappingAdvice.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 04 04, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-04-04 13:55:55 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.monitoring;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.stereotype.Controller;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.function.Consumer;

import static java.util.Collections.singletonList;

/**
 * @author ARHS Developments
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = RequestMappingAdviceTest.RequestMappingConfiguration.class)
public class RequestMappingAdviceTest {

    @Autowired
    private RequestMappingTopLevel requestMappingTopLevel;

    @Autowired
    private RequestMappingInherited requestMappingInherited;

    @Autowired
    private RequestMappingSuffix requestMappingSuffix;

    @Autowired
    private RequestMappingSuffixWithParam requestMappingSuffixWithParam;

    @Autowired
    private DeepInheritanceSubSubSubClass deepInheritanceSubSubSubClass;

    @Autowired
    private RequestMappingDefault requestMappingDefault;

    @Autowired
    private ApiVerifier apiVerifier;

    @Autowired
    private PoisonController poisonController;

    @Test
    public void annotationAtClassLevelMustBeRead() {
        requestMappingTopLevel.get();
        apiVerifier.check(apiCall -> {
            Assert.assertEquals("[GET]", apiCall.getMethods().toString());
            Assert.assertEquals("/test", apiCall.getPath());
        });
    }

    @Test
    public void annotationAtSuperClassLevelMustBeRead() {
        requestMappingInherited.get();
        apiVerifier.check(apiCall -> {
            Assert.assertEquals("[GET]", apiCall.getMethods().toString());
            Assert.assertEquals("/test", apiCall.getPath());
        });

        deepInheritanceSubSubSubClass.subPathGet("test");
        apiVerifier.check(apiCall -> {
            Assert.assertEquals("[GET]", apiCall.getMethods().toString());
            Assert.assertEquals("/testdeepinheritance/subtest/{param}", apiCall.getPath());
        });
    }

    @Test
    public void classMappingMustPrefixedMethodMapping() {
        requestMappingSuffix.subPath();
        apiVerifier.check(apiCall -> {
            Assert.assertEquals("[GET]", apiCall.getMethods().toString());
            Assert.assertEquals("/test/subtest", apiCall.getPath());
        });
    }

    @Test
    public void paramMustBeReported() {
        requestMappingSuffixWithParam.subPathGet("param_test");
        apiVerifier.check(apiCall -> {
            Assert.assertEquals("[GET]", apiCall.getMethods().toString());
            Assert.assertEquals("/test/subtest/{param}", apiCall.getPath());
            Assert.assertEquals("[param_test]", apiCall.getArgs().toString());
        });
    }

    @Test
    public void methodFallbackToGetIfEmpty() {
        requestMappingDefault.defaultGet();
        apiVerifier.check(apiCall -> Assert.assertEquals(singletonList(RequestMethod.GET), apiCall.getMethods()));
    }

    @Test
    public void internalAspectExceptionMustNotBePropagated() {
        try {
            poisonController.defaultGet();
        } catch (Throwable t) {
            Assert.fail("Exception thrown by the aspect and propagated!");
        }
    }

    @Configuration
    @EnableAspectJAutoProxy(proxyTargetClass = true)
    static class RequestMappingConfiguration {

        @Bean
        public RequestMappingAdvice requestMappingAdvice() {
            return new RequestMappingAdvice(apiAuditService());
        }

        @Bean
        public ApiVerifier apiAuditService() {
            return new ApiVerifier();
        }

        @Bean
        public RequestMappingTopLevel requestMappingTopLevel() {
            return new RequestMappingTopLevel();
        }

        @Bean
        public RequestMappingInherited requestMappingInherited() {
            return new RequestMappingInherited();
        }

        @Bean
        public RequestMappingSuffix requestMappingSuffix() {
            return new RequestMappingSuffix();
        }

        @Bean
        public RequestMappingSuffixWithParam requestMappingSuffixWithParam() {
            return new RequestMappingSuffixWithParam();
        }

        @Bean
        public DeepInheritanceSubSubSubClass deepInheritanceSubSubSubClass() {
            return new DeepInheritanceSubSubSubClass();
        }

        @Bean
        public RequestMappingDefault requestMappingDefault() {
            return new RequestMappingDefault();
        }

        @Bean
        public PoisonController poisonController() {
            return new PoisonController();
        }

    }

    static class ApiVerifier implements ApiAuditService {

        private ApiCall apiCall;

        @Override
        public void audit(ApiCall apiCall) {
            if (apiCall.getPath().equals("/poison")) {
                throw new IllegalStateException();
            }
            this.apiCall = apiCall;
        }

        void check(Consumer<ApiCall> consumer) {
            consumer.accept(apiCall);
        }
    }


}

@Controller
@RequestMapping(value = "/test")
class RequestMappingTopLevel {

    @RequestMapping(method = RequestMethod.GET)
    public void get() {

    }

}

@Controller
class RequestMappingInherited extends RequestMappingTopLevel {

    @RequestMapping(method = RequestMethod.GET)
    public void getExtended() {

    }

}

@Controller
@RequestMapping(value = "/test")
class RequestMappingSuffix {

    @RequestMapping(value = "/subtest", method = RequestMethod.GET)
    public void subPath() {

    }
}

@Controller
@RequestMapping(value = "/test")
class RequestMappingSuffixWithParam {

    @RequestMapping(value = "/subtest/{param}", method = RequestMethod.GET)
    public void subPathGet(String param) {

    }

}

@RequestMapping("/testdeepinheritance")
class DeepInheritance {
}

class DeepInheritanceSubClass extends DeepInheritance {
}

class DeepInheritanceSubSubClass extends DeepInheritanceSubClass {
}

class DeepInheritanceSubSubSubClass extends DeepInheritanceSubSubClass {

    @RequestMapping(value = "/subtest/{param}", method = RequestMethod.GET)
    public void subPathGet(String param) {

    }
}

@Controller
@RequestMapping(value = "/test")
class RequestMappingDefault {

    @RequestMapping(value = "/test")
    public void defaultGet() {

    }

}

@Controller
class PoisonController {

    @RequestMapping(value = "/poison")
    public void defaultGet() {

    }

}