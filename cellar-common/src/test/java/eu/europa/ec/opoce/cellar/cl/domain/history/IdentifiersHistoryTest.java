/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : CellarIdentifier.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 21 mars 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.history;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

/**
 * JUnit test on POJO should test the methods that have business logic
 * equals - compare - hashCode - toString.
 * <br/><br/>
 * <notes>
 * <br/><br/>
 * ON : 21 mars 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class IdentifiersHistoryTest {

    /** The test subject_ a. */
    private IdentifiersHistory testSubject_A;

    /** The test subject_ b. */
    private IdentifiersHistory testSubject_B;

    /** The test subject_ c. */
    private IdentifiersHistory testSubject_C;

    /** The test subject_ d. */
    private IdentifiersHistory testSubject_D;

    /**
     * Creates the test data.
     *
     * @throws Exception the exception
     */
    @Before
    public void before() throws Exception {
        final Date dateNow = new Date();
        this.testSubject_A = generateObject("A.CID", "A.PID", dateNow, false, 1);
        this.testSubject_B = generateObject("B.CID", "B.PID", dateNow, false, 2);
        this.testSubject_C = generateObject("A.CID", "A.PID", dateNow, false, 1);
        this.testSubject_D = generateObject("A.CID", "A.PID", dateNow, false, 2);
    }

    /**
     * Generate object.
     *
     * @param cid the cid
     * @param pid the pid
     * @param creationDate the creation date
     * @param obsolete the obsolete
     * @param deleted the deleted
     * @param id the id
     * @return the identifiers history
     */
    private IdentifiersHistory generateObject(final String cid, final String pid, final Date creationDate, final boolean obsolete,
            final long id) {
        final IdentifiersHistory result = new IdentifiersHistory();
        result.setCellarId(cid);
        result.setProductionId(pid);
        result.setId(id);
        result.setObsolete(obsolete);
        return result;
    }

    /**
     * Test equals.
     */
    @Test
    public void testEquals() {
        Assert.assertTrue(this.testSubject_A.equals(this.testSubject_C));
        Assert.assertTrue(!this.testSubject_A.equals(this.testSubject_B));
        Assert.assertTrue(this.testSubject_A.equals(this.testSubject_D));//only id is different
    }

    /**
     * Test hash code.
     */
    @Test
    public void testHashCode() {
        Assert.assertTrue(this.testSubject_A.hashCode() == this.testSubject_C.hashCode());
        Assert.assertTrue(this.testSubject_A.hashCode() != this.testSubject_B.hashCode());
        Assert.assertTrue(this.testSubject_A.hashCode() == this.testSubject_D.hashCode());//only id is different
    }

    /**
     * Test to string.
     */
    @Test
    public void testToString() {
        Assert.assertTrue(this.testSubject_A.toString().equals(this.testSubject_C.toString()));
        Assert.assertTrue(!this.testSubject_A.toString().equals(this.testSubject_B.toString()));
        Assert.assertTrue(!this.testSubject_A.toString().equals(this.testSubject_D.toString()));
    }

}
