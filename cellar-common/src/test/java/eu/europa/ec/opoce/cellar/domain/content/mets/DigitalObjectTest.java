package eu.europa.ec.opoce.cellar.domain.content.mets;

import eu.europa.ec.opoce.cellar.cl.configuration.PrefixConfigurationService;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.contains;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ServiceLocator.class)
@PowerMockIgnore("javax.management.*")
public class DigitalObjectTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        PowerMockito.mockStatic(ServiceLocator.class);
        when(ServiceLocator.getService(PrefixConfigurationService.class)).thenReturn(mock(PrefixConfigurationService.class));
    }

    @Test
    public void validChildObject_addChildObject_childObjectAdded() throws Exception {
        DigitalObject childObject = new DigitalObject(new StructMap(new MetsDocument()));
        childObject.setCellarId(new ContentIdentifier("cellar:uuid.0001.01", true));
        DigitalObject parentObject = new DigitalObject(new StructMap(new MetsDocument()));
        parentObject.setCellarId(new ContentIdentifier("cellar:uuid.0001", true));
        parentObject.addChildObject(childObject);
        assertEquals(parentObject, childObject.getParentObject());
    }

    @Test
    public void childObjectNotPartOfParentHierarchy_addChildObject_exception() throws Exception {
        expectedException.expect(CellarException.class);
        expectedException.expectMessage(contains("E-039 [details=Child (cellar:uuid.0001.01) cannot be part of parent's hierarchy (cellar:uuid.0002)"));

        DigitalObject childObject = new DigitalObject(new StructMap(new MetsDocument()));
        childObject.setCellarId(new ContentIdentifier("cellar:uuid.0001.01", true));
        DigitalObject parentObject = new DigitalObject(new StructMap(new MetsDocument()));
        parentObject.setCellarId(new ContentIdentifier("cellar:uuid.0002", true));
        parentObject.addChildObject(childObject);
    }
}