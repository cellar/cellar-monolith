/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.util
 *             FILE : BoundedPriorityBlockingQueueTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 16, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.util;

import org.junit.Assert;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 16, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class URLUtilsTest {

    @Test
    public void getQueryParamsTest() {
        final String url = "http://cellar-dev.publications.europa.eu/resource/cellar/43fcdbf8-59cd-11e4-b233-01aa75ed71a1?language=eng&test=fr";
        try {
            final Map<String, List<String>> queryParams = URLUtils.getQueryParams(url);
            Assert.assertEquals(queryParams.size(), 2);
            Assert.assertEquals(queryParams.get("language").get(0), "eng");
            Assert.assertEquals(queryParams.get("test").get(0), "fr");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

}
