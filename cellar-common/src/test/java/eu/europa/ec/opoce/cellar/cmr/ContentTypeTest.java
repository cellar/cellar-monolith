package eu.europa.ec.opoce.cellar.cmr;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

import static com.google.common.collect.ImmutableMap.of;
import static eu.europa.ec.opoce.cellar.cmr.ContentType.DIRECT;
import static eu.europa.ec.opoce.cellar.cmr.ContentType.DIRECT_INFERRED;

/**
 * @author ARHS Developments
 */
public class ContentTypeTest {

    @Test
    public void emptyMapVersionsMustReturnExeContentTypeMapWithNullVersion() {
        Map<ContentType, String> types = ContentType.extract(of(), DIRECT, DIRECT_INFERRED);
        Assert.assertEquals(2, types.size());
        Assert.assertNull(types.get(DIRECT));
        Assert.assertNull(types.get(DIRECT_INFERRED));
    }
}
