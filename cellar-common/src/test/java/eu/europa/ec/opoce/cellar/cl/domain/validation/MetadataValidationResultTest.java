package eu.europa.ec.opoce.cellar.cl.domain.validation;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MetadataValidationResultTest {

    @Test
    public void getTotalTimeTest() {
        final MetadataValidationResult result = new MetadataValidationResult();
        result.setTotalTimeForInferenceInMilliSeconds(1);
        result.setTotalTimeForValidationInMilliSeconds(2);
        assertEquals(3, result.getTotalTimeMilliSeconds());
    }

    @Test
    public void toSummaryStringTest() {
        final MetadataValidationResult result = new MetadataValidationResult();
        result.setRequestId("894f8388-4a2d-45cf-8937-f8e9ae3a6847");
        result.setCellarId("c81c299c-24c4-499c-9b81-f2ccf263fc06");
        result.setConformModelProvided(true);
        result.setTotalTimeForInferenceInMilliSeconds(1);
        result.setTotalTimeForValidationInMilliSeconds(2);
        assertEquals("MetadataValidationResult{requestId=894f8388-4a2d-45cf-8937-f8e9ae3a6847, cellarId=c81c299c-24c4-499c-9b81-f2ccf263fc06, isConform=true, totalTime=3ms}", result.toShortString());
    }
}