package eu.europa.ec.opoce.cellar.logging;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;
import org.junit.Assert;
import org.junit.Test;

import static eu.europa.ec.opoce.cellar.logging.ThresholdMarkerFilter.createFilter;
import static org.apache.logging.log4j.Level.DEBUG;
import static org.apache.logging.log4j.Level.ERROR;
import static org.apache.logging.log4j.core.Filter.Result.*;

/**
 * @author ARHS Developments
 */
public class ThresholdMarkerFilterTest {

    static {
        System.setProperty(ThresholdMarkerFilter.PROPERTY_DISABLE_JMX, "true");
    }

    private static final Marker TEST_MARKER = MarkerManager.getMarker("TEST_MARKER");

    @Test
    public void createMarkerWithNullLevelMustReturnNull() {
        Assert.assertNull(createFilter("TEST_MARKER", null, null, null, null, false));
    }

    @Test
    public void createMarkerWithNullMarkerMustReturnNull() {
        Assert.assertNull(createFilter(null, null, Level.DEBUG, null, null, false));
    }

    @Test
    public void createMarkerWithNullOnAcceptMustFallbackToAccept() {
        ThresholdMarkerFilter filter = createFilter("TEST_MARKER", null, Level.DEBUG, null, DENY, false);
        Assert.assertEquals(ACCEPT, filter.filter(TEST_MARKER, DEBUG));
    }

    @Test
    public void createMarkerWithNullOnDenyMustFallbackToDeny() {
        ThresholdMarkerFilter filter = createFilter("TEST_MARKER", null, Level.ERROR, ACCEPT, null, false);
        Assert.assertEquals(DENY, filter.filter(TEST_MARKER, DEBUG));
    }

    @Test
    public void filterMustAcceptLevelLessSpecific() {
        ThresholdMarkerFilter filter = createFilter(TEST_MARKER.getName(), null, DEBUG, ACCEPT, DENY, false);
        Assert.assertEquals(ACCEPT, filter.filter(TEST_MARKER, DEBUG));
        Assert.assertEquals(ACCEPT, filter.filter(TEST_MARKER, ERROR));
    }

    @Test
    public void filterMustDenyLevelMoreSpecific() {
        ThresholdMarkerFilter filter = createFilter(TEST_MARKER.getName(), null, ERROR, ACCEPT, DENY, false);
        Assert.assertEquals(DENY, filter.filter(TEST_MARKER, DEBUG));
        Assert.assertEquals(ACCEPT, filter.filter(TEST_MARKER, ERROR));
    }

    @Test
    public void filterMustIgnoreNullOrEmptyMarker() {
        ThresholdMarkerFilter filter = createFilter(TEST_MARKER.getName(), null, ERROR, ACCEPT, DENY, false);
        Marker empty = MarkerManager.getMarker("");
        Assert.assertEquals(NEUTRAL, filter.filter(null, DEBUG));
        Assert.assertEquals(NEUTRAL, filter.filter(empty, ERROR));
    }

}
