/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.util
 *             FILE : BoundedPriorityBlockingQueueTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 16, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.util;

import org.junit.Assert;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 16, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class BoundedPriorityBlockingQueueTest {

    @Test
    public void test() throws InterruptedException {
        BoundedPriorityBlockingQueue<String> queue = new BoundedPriorityBlockingQueue<String>(10);

        queue.put("bcd");
        queue.put("abc");

        Assert.assertEquals("abc", queue.take());
        Assert.assertEquals("bcd", queue.take());

        Assert.assertNull(queue.poll());
        Assert.assertNull(queue.poll(1, TimeUnit.NANOSECONDS));
    }
}
