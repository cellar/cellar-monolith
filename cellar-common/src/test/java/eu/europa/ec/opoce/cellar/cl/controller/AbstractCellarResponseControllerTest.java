package eu.europa.ec.opoce.cellar.cl.controller;

import com.sun.jersey.core.header.InBoundHeaders;
import eu.europa.ec.op.cellar.api.service.impl.CellarResponse;
import eu.europa.ec.opoce.cellar.cl.domain.mimeType.MimeTypeMapping;
import eu.europa.ec.opoce.cellar.cl.service.client.MimeTypeCacheService;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.MultivaluedMap;
import java.util.*;

import static org.junit.Assert.assertEquals;

public class AbstractCellarResponseControllerTest {

    private MimeTypeCacheServiceMock mimeTypeCacheService;
    private AbstractCellarResponseController responseController;
    private CellarResponseMock cellarResponse;

    @Before
    public void setUp() throws Exception {
        mimeTypeCacheService = new MimeTypeCacheServiceMock();
        responseController = new AbstractCellarResponseController();
        responseController.setMimeTypeCacheService(mimeTypeCacheService);
        cellarResponse = new CellarResponseMock();
    }

    @Test
    public void nullHeaderShouldReturnPidAsFilename() throws Exception {
        cellarResponse.nullifyHeaders();
        String result = responseController.getAttachmentFileName(cellarResponse, "pid");
        assertEquals("pid", result);
    }

    @Test
    public void emptyHeaderShouldReturnPidAsFilename() throws Exception {
        cellarResponse.clearHeaders();
        String result = responseController.getAttachmentFileName(cellarResponse, "pid");
        assertEquals("pid", result);
    }

    @Test
    public void nullMimeTypeMapShouldReturnPidAsFilename() throws Exception {
        mimeTypeCacheService.nullifyMimeTypeMap();
        String result = responseController.getAttachmentFileName(cellarResponse, "pid");
        assertEquals("pid", result);
    }

    @Test
    public void emptyMimeTypeMapShouldReturnPidAsFilename() throws Exception {
        mimeTypeCacheService.clearMimeTypeMap();
        String result = responseController.getAttachmentFileName(cellarResponse, "pid");
        assertEquals("pid", result);
    }

    @Test
    public void noMatchShouldReturnPidAsFilename() throws Exception {
        String mimeType = "mimeType";
        MimeTypeMapping mimeTypeMappingOne = new MimeTypeMapping();
        mimeTypeMappingOne.setMimeType(mimeType);
        mimeTypeMappingOne.setExtension("extensionOne");
        MimeTypeMapping mimeTypeMappingTwo = new MimeTypeMapping();
        mimeTypeMappingTwo.setMimeType(mimeType);
        mimeTypeMappingTwo.setExtension("extensionTwo");
        List<MimeTypeMapping> mimeTypeMappingList = new ArrayList<>(Arrays.asList(mimeTypeMappingOne, mimeTypeMappingTwo));
        mimeTypeCacheService.feedMimeTypeMap(mimeType, mimeTypeMappingList);

        cellarResponse.putValueListInHeaders("Content-Type", Arrays.asList("valueOne", "valueTwo", "valueThree"));
        String filename = responseController.getAttachmentFileName(cellarResponse, "pid");
        assertEquals("pid", filename);
    }

    @Test
    public void oneMatchingMimeTypeWithOneExtensionReturnsPidAndExtension() throws Exception {
        String mimeType = "mimeType";
        MimeTypeMapping mimeTypeMappingOne = new MimeTypeMapping();
        mimeTypeMappingOne.setMimeType(mimeType);
        mimeTypeMappingOne.setExtension("extensionOne");
        List<MimeTypeMapping> mimeTypeMappingList = new ArrayList<>(Collections.singletonList(mimeTypeMappingOne));
        mimeTypeCacheService.feedMimeTypeMap(mimeType, mimeTypeMappingList);

        cellarResponse.putValueListInHeaders("Content-Type", Arrays.asList("valueOne", "valueTwo", "mimeType"));
        String filename = responseController.getAttachmentFileName(cellarResponse, "pid");
        assertEquals("pid.extensionOne", filename);
    }

    @Test
    public void oneMatchingMimeTypeWithTwoExtensionReturnsPidAndFirstExtensionInSet() throws Exception {
        String mimeType = "mimeType";
        MimeTypeMapping mimeTypeMappingOne = new MimeTypeMapping();
        mimeTypeMappingOne.setMimeType(mimeType);
        mimeTypeMappingOne.setExtension("extensionFirst");
        MimeTypeMapping mimeTypeMappingTwo = new MimeTypeMapping();
        mimeTypeMappingTwo.setMimeType(mimeType);
        mimeTypeMappingTwo.setExtension("extensionSecond");
        List<MimeTypeMapping> mimeTypeMappingList = new ArrayList<>(Arrays.asList(mimeTypeMappingOne, mimeTypeMappingTwo));
        mimeTypeCacheService.feedMimeTypeMap(mimeType, mimeTypeMappingList);

        cellarResponse.putValueListInHeaders("Content-Type", Arrays.asList("valueOne", "valueTwo", "mimeType"));
        String filename = responseController.getAttachmentFileName(cellarResponse, "pid");
        assertEquals("pid.extensionFirst", filename);
    }

    private class MimeTypeCacheServiceMock implements MimeTypeCacheService {

        private Map<String, List<MimeTypeMapping>> map;

        public MimeTypeCacheServiceMock() {
            map = new HashMap<>();
        }

        public void feedMimeTypeMap(String mimeType, List<MimeTypeMapping> mimeTypeMappingList) {
            map.put(mimeType, mimeTypeMappingList);
        }

        @Override
        public Set<String> getMimeTypesSet() {
            return null;
        }

        @Override
        public List<String> getMimeTypesByPriorityDescList() {
            return null;
        }

        @Override
        public Map<String, List<MimeTypeMapping>> getManifestationTypesMap() {
            return null;
        }

        @Override
        public Map<String, List<MimeTypeMapping>> getMimeTypeMap() {
            return map;
        }

        public void nullifyMimeTypeMap() {
            map = null;
        }

        public void clearMimeTypeMap() {
            map.clear();
        }
    }

    public class CellarResponseMock extends CellarResponse {

        private MultivaluedMap<String, String> headerMap;

        public CellarResponseMock() {
            super(null);
            headerMap = new InBoundHeaders();
        }

        public void putValueListInHeaders(String key, List<String> values) {
            headerMap.put(key, values);
        }

        @Override
        public MultivaluedMap<String, String> getHeaders() {
            return headerMap;
        }

        public void nullifyHeaders() {
            headerMap = null;
        }

        public void clearHeaders() {
            headerMap.clear();
        }
    }
}
