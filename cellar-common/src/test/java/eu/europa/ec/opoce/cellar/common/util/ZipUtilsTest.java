/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.util
 *             FILE : ZipUtilsTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 8, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2016-09-01 17:46:05 +0200 (Thu, 01 Sep 2016) $
 *          VERSION : $LastChangedRevision: 11526 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.util;

import eu.europa.ec.opoce.cellar.cl.service.client.AuditTrailEventService;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 8, 2017
 *
 * @author ARHS Developments
 * @version $Revision: 11526 $
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(ServiceLocator.class)
@PowerMockIgnore("javax.management.*")
public class ZipUtilsTest {

    private static Path outputDir;
    private static String inputDir;

    @BeforeClass
    public static void initTestClass() throws Exception {
        outputDir = Paths.get(System.getProperty("java.io.tmpdir") + File.separator + "CELLAR");
        inputDir = "src/test/resources/eu/europa/ec/opoce/cellar/common/util/ZipUtilsTestResources";

        final AuditTrailEventService auditTrailEventService = Mockito.mock(AuditTrailEventService.class);
        PowerMockito.mockStatic(ServiceLocator.class);
        PowerMockito.when(ServiceLocator.getService(AuditTrailEventService.class)).thenReturn(auditTrailEventService);
    }

    @Before
    public void initTest() throws Exception {
        initOutputDirectory();
    }

    @Test(expected = IOException.class)
    public void failWhenZipFileDoesNotExist() throws Exception {
        final Path zipPath = Paths.get(inputDir + "/nonexistent.zip");

        ZipUtils.unzipFilteredEntries(zipPath.toString(), outputDir.toString(), null);
    }

    @Test
    public void checkNoEntryExists() throws Exception {
        final Path zipPath = Paths.get(inputDir + "/test2.zip");
        ZipUtils.unzipFilteredEntries(zipPath.toString(), outputDir.toString(), zipEntry -> zipEntry.getName().endsWith(".nonexistent"));
        Assert.assertTrue("No file with extension '.nonexistent' exists in zip '" + zipPath + "'.",
                new File(outputDir.toString()).list().length == 0);
    }

    @Test
    public void checkTextEntryExistsAtFirstLevel() throws Exception {
        final Path zipPath = Paths.get(inputDir + "/test1.zip");
        ZipUtils.unzipFilteredEntries(zipPath.toString(), outputDir.toString(), zipEntry -> zipEntry.getName().endsWith(".txt"));
        final String filePath = File.separator + "test1.txt";
        assertFileExists("File '" + filePath + "' exists in zip '" + zipPath + "'.", filePath);
    }

    @Test
    public void checkTextEntriesExistAtMultipleLevels() throws Exception {
        final Path zipPath = Paths.get(inputDir + "/test2.zip");
        ZipUtils.unzipFilteredEntries(zipPath.toString(), outputDir.toString(), zipEntry -> zipEntry.getName().endsWith(".txt"));
        String filePath = File.separator + "test1.txt";
        assertFileExists("File '" + filePath + "' exists in zip '" + zipPath + "'.", filePath);

        filePath = File.separator + "1" + File.separator + "test12.txt";
        assertFileExists("File '" + filePath + "' exists in zip '" + zipPath + "'.", filePath);

        filePath = File.separator + "1" + File.separator + "test11.not";
        assertFileNotExists("File '" + filePath + "' does not exist in zip '" + zipPath + "'.", filePath);
    }

    @Test
    public void checkEntriesModifiedBeforeNowExist() throws Exception {
        final Path zipPath = Paths.get(inputDir + "/test2.zip");

        ZipUtils.unzipFilteredEntries(zipPath.toString(), outputDir.toString(), zipEntry -> {
            final Instant lastModified = zipEntry.getLastModifiedTime().toInstant();
            return lastModified.isBefore(Instant.now());
        });
        String filePath = File.separator + "test1.txt";
        assertFileExists("File '" + filePath + "' exists in zip '" + zipPath + "'.", filePath);

        filePath = File.separator + "1" + File.separator + "test11.not";
        assertFileExists("File '" + filePath + "' exists in zip '" + zipPath + "'.", filePath);

        filePath = File.separator + "1" + File.separator + "test12.txt";
        assertFileExists("File '" + filePath + "' exists in zip '" + zipPath + "'.", filePath);
    }

    @Test
    public void checkNoEntriesModifiedAfterNowExist() throws Exception {
        final Path zipPath = Paths.get(inputDir + "/test2.zip");

        final Instant now = Instant.now();
        ZipUtils.unzipFilteredEntries(zipPath.toString(), outputDir.toString(), zipEntry -> {
            final Instant lastModified = zipEntry.getLastModifiedTime().toInstant();
            return lastModified.isAfter(now);
        });
        Assert.assertTrue("No file exists in zip '" + zipPath + "' which has been modified after " + now + ".'",
                new File(outputDir.toString()).list().length == 0);
    }

    @Test
    public void checkEntriesModifiedBeforeDateExist() throws Exception {
        final Path zipPath = Paths.get(inputDir + "/test2.zip");
        final Instant date = DateUtils.parseDate("2017-06-08", new String[]{"yyyy-MM-dd"}).toInstant();

        ZipUtils.unzipFilteredEntries(zipPath.toString(), outputDir.toString(), zipEntry -> {
            final Instant lastModified = zipEntry.getLastModifiedTime().toInstant();
            return lastModified.isBefore(date);
        });
        String filePath = File.separator + "test1.txt";
        assertFileExists("File '" + filePath + "' exists in zip '" + zipPath + "' and has been modified before " + date + ".'",
                filePath);

        filePath = File.separator + "1" + File.separator + "test11.not";
        assertFileExists("File '" + filePath + "' exists in zip '" + zipPath + "' and has been modified before " + date + ".'",
                filePath);

        filePath = File.separator + "1" + File.separator + "test12.txt";
        assertFileNotExists("No file '" + filePath + "' exists in zip '" + zipPath + "' which has been modified before " + date + ".'",
                filePath);
    }

    @Test
    public void checkNoEntriesExistInEmptyZipFile() throws Exception {
        final Path zipPath = Paths.get(inputDir + "/test3.zip");

        ZipUtils.unzipFilteredEntries(zipPath.toString(), outputDir.toString(), null);
        Assert.assertTrue("Zip '" + zipPath + "' is empty.", new File(outputDir.toString()).list().length == 0);
    }

    @Test
    public void checkEntriesSatisfyingOneLevelWildcardExist() throws Exception {
        final Path zipPath = Paths.get(inputDir + "/test2.zip");
        final String wildcard = "*.txt";
        ZipUtils.unzipFilteredEntries(zipPath.toString(), outputDir.toString(), zipEntry -> FilenameUtils.wildcardMatch(zipEntry.getName(), wildcard));
        String filePath = File.separator + "test1.txt";
        assertFileExists("File '" + filePath + "' exists in zip '" + zipPath + "' and matches with wildcard '" + wildcard + "'.", filePath);

        filePath = File.separator + "1" + File.separator + "test12.txt";
        assertFileExists("File '" + filePath + "' exists in zip '" + zipPath + "' and matches with wildcard '" + wildcard + "'.", filePath);

        filePath = File.separator + "1" + File.separator + "test11.not";
        assertFileNotExists("No file '" + filePath + "' exists in zip '" + zipPath + "' which matches with wildcard '" + wildcard + "'.", filePath);
    }

    @Test
    public void checkEntriesSatisfyingMultipleLevelsWildcardExist() throws Exception {
        final Path zipPath = Paths.get(inputDir + "/test2.zip");
        final String wildcard = "1/*12.txt";
        ZipUtils.unzipFilteredEntries(zipPath.toString(), outputDir.toString(), zipEntry -> FilenameUtils.wildcardMatch(zipEntry.getName(), wildcard));
        String filePath = File.separator + "test1.txt";
        assertFileNotExists("No file '" + filePath + "' exists in zip '" + zipPath + "' which matches with wildcard '" + wildcard + "'.", filePath);

        filePath = File.separator + "1" + File.separator + "test12.txt";
        assertFileExists("File '" + filePath + "' exists in zip '" + zipPath + "' and matches with wildcard '" + wildcard + "'.", filePath);

        filePath = File.separator + "1" + File.separator + "test11.not";
        assertFileNotExists("No file '" + filePath + "' exists in zip '" + zipPath + "' which matches with wildcard '" + wildcard + "'.", filePath);
    }

    @Test
    public void checkEntriesSatisfyingMultipleLevelsWildcardDoNotExist() throws Exception {
        final Path zipPath = Paths.get(inputDir + "/test2.zip");
        final String wildcard = "11/*12.txt";
        ZipUtils.unzipFilteredEntries(zipPath.toString(), outputDir.toString(), zipEntry -> FilenameUtils.wildcardMatch(zipEntry.getName(), wildcard));
        String filePath = File.separator + "test1.txt";
        assertFileNotExists("No file '" + filePath + "' exists in zip '" + zipPath + "' which matches with wildcard '" + wildcard + "'.", filePath);

        filePath = File.separator + "1" + File.separator + "test12.txt";
        assertFileNotExists("No file '" + filePath + "' exists in zip '" + zipPath + "' which matches with wildcard '" + wildcard + "'.", filePath);

        filePath = File.separator + "1" + File.separator + "test11.not";
        assertFileNotExists("No file '" + filePath + "' exists in zip '" + zipPath + "' which matches with wildcard '" + wildcard + "'.", filePath);
    }

    @Test
    public void checkEntriesSatisfyingWildcardWithoutSpecialCharsExist() throws Exception {
        final Path zipPath = Paths.get(inputDir + "/test2.zip");
        final String wildcard = "test1.txt";
        ZipUtils.unzipFilteredEntries(zipPath.toString(), outputDir.toString(), zipEntry -> FilenameUtils.wildcardMatch(zipEntry.getName(), wildcard));
        String filePath = File.separator + "test1.txt";
        assertFileExists("File '" + filePath + "' exists in zip '" + zipPath + "' and matches with wildcard '" + wildcard + "'.", filePath);

        filePath = File.separator + "1" + File.separator + "test12.txt";
        assertFileNotExists("No file '" + filePath + "' exists in zip '" + zipPath + "' which matches with wildcard '" + wildcard + "'.", filePath);

        filePath = File.separator + "1" + File.separator + "test11.not";
        assertFileNotExists("No file '" + filePath + "' exists in zip '" + zipPath + "' which matches with wildcard '" + wildcard + "'.", filePath);
    }

    private static void initOutputDirectory() throws IOException {
        if (!Files.exists(outputDir)) {
            Files.createDirectories(outputDir);
        }
        FileUtils.cleanDirectory(outputDir.toFile());
    }

    private void assertFileExists(final String message, final String fileRelativePath) throws IOException {
        Assert.assertTrue(message, new File(outputDir + fileRelativePath).exists());
    }

    private void assertFileNotExists(final String message, final String fileRelativePath) throws IOException {
        Assert.assertFalse(message, new File(outputDir + fileRelativePath).exists());
    }

}
