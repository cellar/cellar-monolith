/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr
 *             FILE : DatastreamWrapperTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 02 23, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-02-23 14:33:42 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr;

import org.junit.Assert;
import org.junit.Test;

/**
 * @author ARHS Developments
 */
public class DatastreamWrapperTest {

    @Test
    public void dataMustMustBeRetrievedIndividuallyByDatastreamName() {
        DataStreamWrapper wrapper = new DataStreamWrapper();
        wrapper.loadFromXml(RAW);

        Assert.assertTrue(wrapper.containsData(ContentType.DECODING));
        Assert.assertTrue(wrapper.containsData(ContentType.DIRECT_INFERRED));
        Assert.assertTrue(wrapper.containsData(ContentType.DIRECT));

        Assert.assertEquals(DECODING, wrapper.getData(ContentType.DECODING));
        Assert.assertEquals(DIRECT_INFERRED, wrapper.getData(ContentType.DIRECT_INFERRED));
        Assert.assertEquals(DIRECT, wrapper.getData(ContentType.DIRECT));
    }

    @Test
    public void createXmlMustMergeTheDataStreams() {
        DataStreamWrapper wrapper = new DataStreamWrapper();
        wrapper.loadFromXml(RAW);
        String xml = wrapper.createXml();
        Assert.assertTrue(xml.contains("<data-stream name=\"DIRECT\">"));
        Assert.assertTrue(xml.contains("<data-stream name=\"DIRECT_INFERRED\">"));
        Assert.assertTrue(xml.contains("<data-stream name=\"DECODING\">"));
    }

    private static final String RAW = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<cmr-stream>\n" +
            "<data-stream name=\"DIRECT\"><![CDATA[<rdf:RDF\n" +
            "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n" +
            "    xmlns:j.0=\"http://publications.europa.eu/ontology/cdm#\"\n" +
            "    xmlns:owl=\"http://www.w3.org/2002/07/owl#\"\n" +
            "    xmlns:rdfs=\"http://www.w3.org/2000/01/rdf-schema#\"\n" +
            "    xmlns:j.1=\"http://publications.europa.eu/ontology/cdm/cmr#\" > \n" +
            "  <rdf:Description rdf:about=\"http://cellar-dev.publications.europa.eu/resource/oj/JOL_2006_088_R_0063_01\">\n" +
            "    <j.0:resource_legal_comment_internal rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">MAN2</j.0:resource_legal_comment_internal>\n" +
            "    <j.0:resource_legal_has_type_act_concept_type_act rdf:resource=\"http://publications.europa.eu/resource/authority/fd_030/DECISION\"/>\n" +
            "    <j.0:work_date_document rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">2006-03-24</j.0:work_date_document>\n" +
            "    <j.0:work_id_document rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">oj:JOL_2006_088_R_0063_01</j.0:work_id_document>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#decision\"/>\n" +
            "    <j.0:work_is_about_concept_eurovoc rdf:resource=\"http://eurovoc.europa.eu/3579\"/>\n" +
            "    <j.0:work_id_document rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">celex:32006D0241</j.0:work_id_document>\n" +
            "    <j.0:resource_legal_date_entry-into-force rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">2006-03-24</j.0:resource_legal_date_entry-into-force>\n" +
            "    <j.0:resource_legal_year rdf:datatype=\"http://www.w3.org/2001/XMLSchema#gYear\">2006</j.0:resource_legal_year>\n" +
            "    <j.0:resource_legal_information_miscellaneous rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">P/EEE</j.0:resource_legal_information_miscellaneous>\n" +
            "    <j.0:work_created_by_agent rdf:resource=\"http://publications.europa.eu/resource/authority/corporate-body/COMM\"/>\n" +
            "    <j.0:resource_legal_id_celex rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">32006D0241</j.0:resource_legal_id_celex>\n" +
            "    <j.0:resource_legal_based_on_treaty rdf:resource=\"http://publications.europa.eu/resource/authority/fd_020/CEU\"/>\n" +
            "    <j.0:resource_legal_repeals_resource_legal rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/31997D0517\"/>\n" +
            "    <j.0:work_is_about_concept_eurovoc rdf:resource=\"http://eurovoc.europa.eu/1729\"/>\n" +
            "    <j.0:work_is_about_concept_eurovoc rdf:resource=\"http://eurovoc.europa.eu/2737\"/>\n" +
            "    <j.0:legislation_secondary_is_about_concept_directory-code rdf:resource=\"http://publications.europa.eu/resource/authority/fd_555/035030\"/>\n" +
            "    <j.0:resource_legal_is_about_subject-matter rdf:resource=\"http://publications.europa.eu/resource/authority/fd_070/VETE\"/>\n" +
            "    <j.0:resource_legal_addresses_institution rdf:resource=\"http://publications.europa.eu/resource/authority/fd_050/15EM\"/>\n" +
            "    <j.0:resource_legal_based_on_resource_legal rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/31997L0078\"/>\n" +
            "    <j.0:work_part_of_collection_document rdf:resource=\"http://publications.europa.eu/resource/authority/document-collection/CELEX\"/>\n" +
            "    <j.0:work_title rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">tbd, title of the work</j.0:work_title>\n" +
            "    <j.0:resource_legal_date_end-of-validity rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">9999-12-31</j.0:resource_legal_date_end-of-validity>\n" +
            "    <j.0:legislation_secondary_modifies_legislation_secondary rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/31997D0517\"/>\n" +
            "    <j.0:work_is_about_concept_eurovoc rdf:resource=\"http://eurovoc.europa.eu/3730\"/>\n" +
            "    <j.0:legislation_secondary_date_notification rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">2006-03-24</j.0:legislation_secondary_date_notification>\n" +
            "    <j.0:work_is_about_concept_eurovoc rdf:resource=\"http://eurovoc.europa.eu/2771\"/>\n" +
            "    <j.0:resource_legal_number_natural_celex rdf:datatype=\"http://www.w3.org/2001/XMLSchema#positiveInteger\">0241</j.0:resource_legal_number_natural_celex>\n" +
            "    <j.0:resource_legal_type rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">D</j.0:resource_legal_type>\n" +
            "    <j.0:resource_legal_id_sector rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">3</j.0:resource_legal_id_sector>\n" +
            "    <j.0:resource_legal_repertoire rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">REP</j.0:resource_legal_repertoire>\n" +
            "    <j.0:resource_legal_consolidated_by_act_consolidated rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/02006D0241\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:nodeID=\"A0\">\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2002/07/owl#Axiom\"/>\n" +
            "    <rdfs:comment>DATNOT</rdfs:comment>\n" +
            "    <j.0:type>EV</j.0:type>\n" +
            "    <owl:annotatedSource rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/32006D0241\"/>\n" +
            "    <owl:annotatedProperty rdf:resource=\"http://publications.europa.eu/ontology/cdm#resource_legal_date_entry-into-force\"/>\n" +
            "    <owl:annotatedTarget rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">2006-03-24</owl:annotatedTarget>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://cellar-dev.publications.europa.eu/resource/cellar/3464aaa5-189b-11e8-9e8e-01aa75ed71a1\">\n" +
            "    <owl:sameAs rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/32006D0241\"/>\n" +
            "    <owl:sameAs rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/oj/JOL_2006_088_R_0063_01\"/>\n" +
            "    <j.1:lastModificationDate rdf:datatype=\"http://www.w3.org/2001/XMLSchema#dateTime\">2018-02-23T14:12:30.843+01:00</j.1:lastModificationDate>\n" +
            "    <j.1:creationDate rdf:datatype=\"http://www.w3.org/2001/XMLSchema#dateTime\">2018-02-23T14:12:32.485+01:00</j.1:creationDate>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:nodeID=\"A1\">\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2002/07/owl#Axiom\"/>\n" +
            "    <rdfs:comment>A22</rdfs:comment>\n" +
            "    <owl:annotatedSource rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/32006D0241\"/>\n" +
            "    <owl:annotatedProperty rdf:resource=\"http://publications.europa.eu/ontology/cdm#resource_legal_based_on_resource_legal\"/>\n" +
            "    <owl:annotatedTarget rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/31997L0078\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:nodeID=\"A2\">\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2002/07/owl#Axiom\"/>\n" +
            "    <j.0:type>MS</j.0:type>\n" +
            "    <owl:annotatedSource rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/32006D0241\"/>\n" +
            "    <owl:annotatedProperty rdf:resource=\"http://publications.europa.eu/ontology/cdm#resource_legal_repeals_resource_legal\"/>\n" +
            "    <owl:annotatedTarget rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/31997D0517\"/>\n" +
            "  </rdf:Description>\n" +
            "</rdf:RDF>\n" +
            "]]></data-stream>\n" +
            "<data-stream name=\"DIRECT_INFERRED\"><![CDATA[<rdf:RDF\n" +
            "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n" +
            "    xmlns:j.0=\"http://publications.europa.eu/ontology/cdm#\"\n" +
            "    xmlns:owl=\"http://www.w3.org/2002/07/owl#\"\n" +
            "    xmlns:j.1=\"http://www.w3.org/2004/02/skos/core#\"\n" +
            "    xmlns:rdfs=\"http://www.w3.org/2000/01/rdf-schema#\"\n" +
            "    xmlns:j.2=\"http://publications.europa.eu/ontology/cdm/cmr#\" > \n" +
            "  <rdf:Description rdf:about=\"http://eurovoc.europa.eu/1729\">\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept_eurovoc\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept\"/>\n" +
            "    <j.1:inScheme rdf:resource=\"http://eurovoc.europa.eu/100141\"/>\n" +
            "    <j.0:concept_eurovoc_subject_of_work rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/oj/JOL_2006_088_R_0063_01\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://eurovoc.europa.eu/2771\">\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept_eurovoc\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept\"/>\n" +
            "    <j.1:inScheme rdf:resource=\"http://eurovoc.europa.eu/100141\"/>\n" +
            "    <j.0:concept_eurovoc_subject_of_work rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/oj/JOL_2006_088_R_0063_01\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:nodeID=\"A0\">\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2002/07/owl#Axiom\"/>\n" +
            "    <j.0:type>MS</j.0:type>\n" +
            "    <owl:annotatedSource rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/32006D0241\"/>\n" +
            "    <owl:annotatedProperty rdf:resource=\"http://publications.europa.eu/ontology/cdm#resource_legal_repeals_resource_legal\"/>\n" +
            "    <owl:annotatedTarget rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/31997D0517\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:nodeID=\"A1\">\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2002/07/owl#Axiom\"/>\n" +
            "    <rdfs:comment>DATNOT</rdfs:comment>\n" +
            "    <j.0:type>EV</j.0:type>\n" +
            "    <owl:annotatedSource rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/32006D0241\"/>\n" +
            "    <owl:annotatedProperty rdf:resource=\"http://publications.europa.eu/ontology/cdm#resource_legal_date_entry-into-force\"/>\n" +
            "    <owl:annotatedTarget rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">2006-03-24</owl:annotatedTarget>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:nodeID=\"A2\">\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2002/07/owl#Axiom\"/>\n" +
            "    <rdfs:comment>A22</rdfs:comment>\n" +
            "    <owl:annotatedSource rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/32006D0241\"/>\n" +
            "    <owl:annotatedProperty rdf:resource=\"http://publications.europa.eu/ontology/cdm#resource_legal_based_on_resource_legal\"/>\n" +
            "    <owl:annotatedTarget rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/31997L0078\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://publications.europa.eu/resource/authority/fd_555/035030\">\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept_directory-code\"/>\n" +
            "    <j.1:inScheme rdf:resource=\"http://publications.europa.eu/resource/authority/fd_555\"/>\n" +
            "    <j.0:concept_directory-code_subject_of_legislation_secondary rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/oj/JOL_2006_088_R_0063_01\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://publications.europa.eu/resource/authority/fd_030/DECISION\">\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept_type_act\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept_decoding\"/>\n" +
            "    <j.1:inScheme rdf:resource=\"http://publications.europa.eu/resource/authority/fd_030\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://cellar-dev.publications.europa.eu/resource/celex/02006D0241\">\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#resource_legal\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#act_consolidated\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#work\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://eurovoc.europa.eu/2737\">\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept_eurovoc\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept\"/>\n" +
            "    <j.1:inScheme rdf:resource=\"http://eurovoc.europa.eu/100141\"/>\n" +
            "    <j.0:concept_eurovoc_subject_of_work rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/oj/JOL_2006_088_R_0063_01\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://publications.europa.eu/resource/authority/fd_020/CEU\">\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#work\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#treaty\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#resource_legal\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://eurovoc.europa.eu/3730\">\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept_eurovoc\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept\"/>\n" +
            "    <j.1:inScheme rdf:resource=\"http://eurovoc.europa.eu/100141\"/>\n" +
            "    <j.0:concept_eurovoc_subject_of_work rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/oj/JOL_2006_088_R_0063_01\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://publications.europa.eu/resource/authority/document-collection/CELEX\">\n" +
            "    <j.1:inScheme rdf:resource=\"http://publications.europa.eu/resource/authority/document-collection\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#work\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#collection_document\"/>\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#collection\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://publications.europa.eu/resource/authority/fd_070/VETE\">\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept\"/>\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#subject-matter\"/>\n" +
            "    <j.1:inScheme rdf:resource=\"http://publications.europa.eu/resource/authority/fd_070\"/>\n" +
            "    <j.0:subject-matter_subject_of_resource_legal rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/oj/JOL_2006_088_R_0063_01\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://cellar-dev.publications.europa.eu/resource/celex/32006D0241\">\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#work\"/>\n" +
            "    <j.0:work_has_expression rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/32006D0241.dan\"/>\n" +
            "    <j.0:work_has_expression rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/32006D0241.ger\"/>\n" +
            "    <j.0:work_has_expression rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/32006D0241.fra\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://eurovoc.europa.eu/3579\">\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept_eurovoc\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept\"/>\n" +
            "    <j.1:inScheme rdf:resource=\"http://eurovoc.europa.eu/100141\"/>\n" +
            "    <j.0:concept_eurovoc_subject_of_work rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/oj/JOL_2006_088_R_0063_01\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://cellar-dev.publications.europa.eu/resource/celex/31997L0078\">\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#resource_legal\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#work\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://cellar-dev.publications.europa.eu/resource/oj/JOL_2006_088_R_0063_01\">\n" +
            "    <j.0:resource_legal_addresses_institution rdf:resource=\"http://publications.europa.eu/resource/authority/fd_050/15EM\"/>\n" +
            "    <j.0:resource_legal_comment_internal rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">MAN2</j.0:resource_legal_comment_internal>\n" +
            "    <j.0:addresses rdf:resource=\"http://publications.europa.eu/resource/authority/fd_050/15EM\"/>\n" +
            "    <j.0:title rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">tbd, title of the work</j.0:title>\n" +
            "    <j.0:modifies rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/31997D0517\"/>\n" +
            "    <j.0:date_notification rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">2006-03-24</j.0:date_notification>\n" +
            "    <j.0:is_about rdf:resource=\"http://eurovoc.europa.eu/1729\"/>\n" +
            "    <j.0:comment_internal rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">MAN2</j.0:comment_internal>\n" +
            "    <j.0:id_document rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">celex:32006D0241</j.0:id_document>\n" +
            "    <j.0:work_is_about_concept_eurovoc rdf:resource=\"http://eurovoc.europa.eu/3730\"/>\n" +
            "    <j.0:based_on rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/31997L0078\"/>\n" +
            "    <j.0:is_about rdf:resource=\"http://eurovoc.europa.eu/2771\"/>\n" +
            "    <j.0:work_id_document rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">celex:32006D0241</j.0:work_id_document>\n" +
            "    <j.0:work_created_by_agent rdf:resource=\"http://publications.europa.eu/resource/authority/corporate-body/COMM\"/>\n" +
            "    <j.0:legislation_secondary_modifies_legislation_secondary rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/31997D0517\"/>\n" +
            "    <j.0:work_is_about_concept_eurovoc rdf:resource=\"http://eurovoc.europa.eu/2771\"/>\n" +
            "    <j.0:work_is_about_concept_eurovoc rdf:resource=\"http://eurovoc.europa.eu/1729\"/>\n" +
            "    <j.0:resource_legal_has_type_act_concept_type_act rdf:resource=\"http://publications.europa.eu/resource/authority/fd_030/DECISION\"/>\n" +
            "    <j.0:has_type_act rdf:resource=\"http://publications.europa.eu/resource/authority/fd_030/DECISION\"/>\n" +
            "    <j.0:resource_legal_information_miscellaneous rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">P/EEE</j.0:resource_legal_information_miscellaneous>\n" +
            "    <j.0:date_document rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">2006-03-24</j.0:date_document>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#legislation_secondary\"/>\n" +
            "    <j.0:resource_legal_number_natural_celex rdf:datatype=\"http://www.w3.org/2001/XMLSchema#positiveInteger\">0241</j.0:resource_legal_number_natural_celex>\n" +
            "    <j.0:is_about rdf:resource=\"http://eurovoc.europa.eu/3730\"/>\n" +
            "    <j.0:resource_legal_based_on_treaty rdf:resource=\"http://publications.europa.eu/resource/authority/fd_020/CEU\"/>\n" +
            "    <j.0:id_sector rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">3</j.0:id_sector>\n" +
            "    <j.0:work_is_about_concept_eurovoc rdf:resource=\"http://eurovoc.europa.eu/2737\"/>\n" +
            "    <j.0:work_date_document rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">2006-03-24</j.0:work_date_document>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#resource_legal\"/>\n" +
            "    <j.0:resource_legal_type rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">D</j.0:resource_legal_type>\n" +
            "    <j.0:is_about rdf:resource=\"http://publications.europa.eu/resource/authority/fd_070/VETE\"/>\n" +
            "    <j.0:is_about rdf:resource=\"http://eurovoc.europa.eu/2737\"/>\n" +
            "    <j.0:consolidated_by rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/02006D0241\"/>\n" +
            "    <j.0:id_document rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">oj:JOL_2006_088_R_0063_01</j.0:id_document>\n" +
            "    <j.0:repertoire rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">REP</j.0:repertoire>\n" +
            "    <j.0:work_title rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">tbd, title of the work</j.0:work_title>\n" +
            "    <j.0:date rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">9999-12-31</j.0:date>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#decision\"/>\n" +
            "    <j.0:id_celex rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">32006D0241</j.0:id_celex>\n" +
            "    <j.0:resource_legal_repeals_resource_legal rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/31997D0517\"/>\n" +
            "    <j.0:resource_legal_date_end-of-validity rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">9999-12-31</j.0:resource_legal_date_end-of-validity>\n" +
            "    <j.0:legislation_secondary_date_notification rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">2006-03-24</j.0:legislation_secondary_date_notification>\n" +
            "    <j.0:type rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">D</j.0:type>\n" +
            "    <j.0:date rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">2006-03-24</j.0:date>\n" +
            "    <j.0:date_end-of-validity rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">9999-12-31</j.0:date_end-of-validity>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#work\"/>\n" +
            "    <j.0:identifier rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">32006D0241</j.0:identifier>\n" +
            "    <j.0:is_about rdf:resource=\"http://eurovoc.europa.eu/3579\"/>\n" +
            "    <j.0:date_entry-into-force rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">2006-03-24</j.0:date_entry-into-force>\n" +
            "    <j.0:resource_legal_repertoire rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">REP</j.0:resource_legal_repertoire>\n" +
            "    <j.0:based_on rdf:resource=\"http://publications.europa.eu/resource/authority/fd_020/CEU\"/>\n" +
            "    <j.0:year rdf:datatype=\"http://www.w3.org/2001/XMLSchema#gYear\">2006</j.0:year>\n" +
            "    <j.0:repeals rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/31997D0517\"/>\n" +
            "    <j.0:work_id_document rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">oj:JOL_2006_088_R_0063_01</j.0:work_id_document>\n" +
            "    <j.0:resource_legal_date_entry-into-force rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">2006-03-24</j.0:resource_legal_date_entry-into-force>\n" +
            "    <j.0:identifier rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">celex:32006D0241</j.0:identifier>\n" +
            "    <j.0:legislation_secondary_is_about_concept_directory-code rdf:resource=\"http://publications.europa.eu/resource/authority/fd_555/035030\"/>\n" +
            "    <j.0:resource_legal_id_celex rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">32006D0241</j.0:resource_legal_id_celex>\n" +
            "    <j.0:work_is_about_concept_eurovoc rdf:resource=\"http://eurovoc.europa.eu/3579\"/>\n" +
            "    <j.0:number_natural_celex rdf:datatype=\"http://www.w3.org/2001/XMLSchema#positiveInteger\">0241</j.0:number_natural_celex>\n" +
            "    <j.0:part_of rdf:resource=\"http://publications.europa.eu/resource/authority/document-collection/CELEX\"/>\n" +
            "    <j.0:resource_legal_based_on_resource_legal rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/31997L0078\"/>\n" +
            "    <j.0:identifier rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">oj:JOL_2006_088_R_0063_01</j.0:identifier>\n" +
            "    <j.0:resource_legal_consolidated_by_act_consolidated rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/02006D0241\"/>\n" +
            "    <j.0:information_miscellaneous rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">P/EEE</j.0:information_miscellaneous>\n" +
            "    <j.0:resource_legal_is_about_subject-matter rdf:resource=\"http://publications.europa.eu/resource/authority/fd_070/VETE\"/>\n" +
            "    <j.0:resource_legal_id_sector rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">3</j.0:resource_legal_id_sector>\n" +
            "    <j.0:created_by rdf:resource=\"http://publications.europa.eu/resource/authority/corporate-body/COMM\"/>\n" +
            "    <j.0:resource_legal_year rdf:datatype=\"http://www.w3.org/2001/XMLSchema#gYear\">2006</j.0:resource_legal_year>\n" +
            "    <j.0:is_about rdf:resource=\"http://publications.europa.eu/resource/authority/fd_555/035030\"/>\n" +
            "    <j.0:work_part_of_collection_document rdf:resource=\"http://publications.europa.eu/resource/authority/document-collection/CELEX\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://publications.europa.eu/resource/authority/fd_050/15EM\">\n" +
            "    <j.1:inScheme rdf:resource=\"http://publications.europa.eu/resource/authority/corporate-body\"/>\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#agent\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#institution\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://cellar-dev.publications.europa.eu/resource/cellar/3464aaa5-189b-11e8-9e8e-01aa75ed71a1\">\n" +
            "    <owl:sameAs rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/32006D0241\"/>\n" +
            "    <owl:sameAs rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/oj/JOL_2006_088_R_0063_01\"/>\n" +
            "    <j.2:lastModificationDate rdf:datatype=\"http://www.w3.org/2001/XMLSchema#dateTime\">2018-02-23T14:12:30.843+01:00</j.2:lastModificationDate>\n" +
            "    <j.2:creationDate rdf:datatype=\"http://www.w3.org/2001/XMLSchema#dateTime\">2018-02-23T14:12:32.485+01:00</j.2:creationDate>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://cellar-dev.publications.europa.eu/resource/celex/31997D0517\">\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#work\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#resource_legal\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#legislation_secondary\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://publications.europa.eu/resource/authority/corporate-body/COMM\">\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#agent\"/>\n" +
            "  </rdf:Description>\n" +
            "</rdf:RDF>\n" +
            "]]></data-stream>\n" +
            "<data-stream name=\"DECODING\"><![CDATA[<rdf:RDF\n" +
            "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n" +
            "    xmlns:cdm=\"http://publications.europa.eu/ontology/cdm#\"\n" +
            "    xmlns:owl=\"http://www.w3.org/2002/07/owl#\"\n" +
            "    xmlns:skos=\"http://www.w3.org/2004/02/skos/core#\"\n" +
            "    xmlns:rdfs=\"http://www.w3.org/2000/01/rdf-schema#\"\n" +
            "    xmlns:cmr=\"http://publications.europa.eu/ontology/cdm/cmr#\" > \n" +
            "  <rdf:Description rdf:about=\"http://publications.europa.eu/resource/authority/document-collection/CELEX\">\n" +
            "    <skos:inScheme rdf:resource=\"http://publications.europa.eu/resource/authority/document-collection\"/>\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://eurovoc.europa.eu/3730\">\n" +
            "    <skos:inScheme rdf:resource=\"http://eurovoc.europa.eu/100141\"/>\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://eurovoc.europa.eu/1729\">\n" +
            "    <skos:inScheme rdf:resource=\"http://eurovoc.europa.eu/100141\"/>\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://eurovoc.europa.eu/2771\">\n" +
            "    <skos:inScheme rdf:resource=\"http://eurovoc.europa.eu/100141\"/>\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://publications.europa.eu/resource/authority/fd_070/VETE\">\n" +
            "    <skos:inScheme rdf:resource=\"http://publications.europa.eu/resource/authority/fd_070\"/>\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://publications.europa.eu/resource/authority/fd_555/035030\">\n" +
            "    <skos:inScheme rdf:resource=\"http://publications.europa.eu/resource/authority/fd_555\"/>\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://eurovoc.europa.eu/3579\">\n" +
            "    <skos:inScheme rdf:resource=\"http://eurovoc.europa.eu/100141\"/>\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://publications.europa.eu/resource/authority/fd_030/DECISION\">\n" +
            "    <skos:inScheme rdf:resource=\"http://publications.europa.eu/resource/authority/fd_030\"/>\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://cellar-dev.publications.europa.eu/resource/oj/JOL_2006_088_R_0063_01\">\n" +
            "    <cdm:is_about rdf:resource=\"http://eurovoc.europa.eu/3579\"/>\n" +
            "    <cdm:resource_legal_has_type_act_concept_type_act rdf:resource=\"http://publications.europa.eu/resource/authority/fd_030/DECISION\"/>\n" +
            "    <cdm:is_about rdf:resource=\"http://eurovoc.europa.eu/2771\"/>\n" +
            "    <cdm:work_is_about_concept_eurovoc rdf:resource=\"http://eurovoc.europa.eu/3579\"/>\n" +
            "    <cdm:is_about rdf:resource=\"http://publications.europa.eu/resource/authority/fd_555/035030\"/>\n" +
            "    <cdm:work_is_about_concept_eurovoc rdf:resource=\"http://eurovoc.europa.eu/1729\"/>\n" +
            "    <cdm:work_is_about_concept_eurovoc rdf:resource=\"http://eurovoc.europa.eu/2737\"/>\n" +
            "    <cdm:has_type_act rdf:resource=\"http://publications.europa.eu/resource/authority/fd_030/DECISION\"/>\n" +
            "    <cdm:legislation_secondary_is_about_concept_directory-code rdf:resource=\"http://publications.europa.eu/resource/authority/fd_555/035030\"/>\n" +
            "    <cdm:resource_legal_is_about_subject-matter rdf:resource=\"http://publications.europa.eu/resource/authority/fd_070/VETE\"/>\n" +
            "    <cdm:addresses rdf:resource=\"http://publications.europa.eu/resource/authority/fd_050/15EM\"/>\n" +
            "    <cdm:is_about rdf:resource=\"http://eurovoc.europa.eu/2737\"/>\n" +
            "    <cdm:resource_legal_addresses_institution rdf:resource=\"http://publications.europa.eu/resource/authority/fd_050/15EM\"/>\n" +
            "    <cdm:is_about rdf:resource=\"http://eurovoc.europa.eu/3730\"/>\n" +
            "    <cdm:is_about rdf:resource=\"http://publications.europa.eu/resource/authority/fd_070/VETE\"/>\n" +
            "    <cdm:work_part_of_collection_document rdf:resource=\"http://publications.europa.eu/resource/authority/document-collection/CELEX\"/>\n" +
            "    <cdm:is_about rdf:resource=\"http://eurovoc.europa.eu/1729\"/>\n" +
            "    <cdm:work_is_about_concept_eurovoc rdf:resource=\"http://eurovoc.europa.eu/2771\"/>\n" +
            "    <cdm:work_is_about_concept_eurovoc rdf:resource=\"http://eurovoc.europa.eu/3730\"/>\n" +
            "    <cdm:part_of rdf:resource=\"http://publications.europa.eu/resource/authority/document-collection/CELEX\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://eurovoc.europa.eu/2737\">\n" +
            "    <skos:inScheme rdf:resource=\"http://eurovoc.europa.eu/100141\"/>\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://publications.europa.eu/resource/authority/fd_050/15EM\">\n" +
            "    <skos:inScheme rdf:resource=\"http://publications.europa.eu/resource/authority/corporate-body\"/>\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "  </rdf:Description>\n" +
            "</rdf:RDF>\n" +
            "]]></data-stream>\n" +
            "</cmr-stream>\n";

    private static final String DECODING = "<rdf:RDF\n" +
            "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n" +
            "    xmlns:cdm=\"http://publications.europa.eu/ontology/cdm#\"\n" +
            "    xmlns:owl=\"http://www.w3.org/2002/07/owl#\"\n" +
            "    xmlns:skos=\"http://www.w3.org/2004/02/skos/core#\"\n" +
            "    xmlns:rdfs=\"http://www.w3.org/2000/01/rdf-schema#\"\n" +
            "    xmlns:cmr=\"http://publications.europa.eu/ontology/cdm/cmr#\" > \n" +
            "  <rdf:Description rdf:about=\"http://publications.europa.eu/resource/authority/document-collection/CELEX\">\n" +
            "    <skos:inScheme rdf:resource=\"http://publications.europa.eu/resource/authority/document-collection\"/>\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://eurovoc.europa.eu/3730\">\n" +
            "    <skos:inScheme rdf:resource=\"http://eurovoc.europa.eu/100141\"/>\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://eurovoc.europa.eu/1729\">\n" +
            "    <skos:inScheme rdf:resource=\"http://eurovoc.europa.eu/100141\"/>\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://eurovoc.europa.eu/2771\">\n" +
            "    <skos:inScheme rdf:resource=\"http://eurovoc.europa.eu/100141\"/>\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://publications.europa.eu/resource/authority/fd_070/VETE\">\n" +
            "    <skos:inScheme rdf:resource=\"http://publications.europa.eu/resource/authority/fd_070\"/>\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://publications.europa.eu/resource/authority/fd_555/035030\">\n" +
            "    <skos:inScheme rdf:resource=\"http://publications.europa.eu/resource/authority/fd_555\"/>\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://eurovoc.europa.eu/3579\">\n" +
            "    <skos:inScheme rdf:resource=\"http://eurovoc.europa.eu/100141\"/>\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://publications.europa.eu/resource/authority/fd_030/DECISION\">\n" +
            "    <skos:inScheme rdf:resource=\"http://publications.europa.eu/resource/authority/fd_030\"/>\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://cellar-dev.publications.europa.eu/resource/oj/JOL_2006_088_R_0063_01\">\n" +
            "    <cdm:is_about rdf:resource=\"http://eurovoc.europa.eu/3579\"/>\n" +
            "    <cdm:resource_legal_has_type_act_concept_type_act rdf:resource=\"http://publications.europa.eu/resource/authority/fd_030/DECISION\"/>\n" +
            "    <cdm:is_about rdf:resource=\"http://eurovoc.europa.eu/2771\"/>\n" +
            "    <cdm:work_is_about_concept_eurovoc rdf:resource=\"http://eurovoc.europa.eu/3579\"/>\n" +
            "    <cdm:is_about rdf:resource=\"http://publications.europa.eu/resource/authority/fd_555/035030\"/>\n" +
            "    <cdm:work_is_about_concept_eurovoc rdf:resource=\"http://eurovoc.europa.eu/1729\"/>\n" +
            "    <cdm:work_is_about_concept_eurovoc rdf:resource=\"http://eurovoc.europa.eu/2737\"/>\n" +
            "    <cdm:has_type_act rdf:resource=\"http://publications.europa.eu/resource/authority/fd_030/DECISION\"/>\n" +
            "    <cdm:legislation_secondary_is_about_concept_directory-code rdf:resource=\"http://publications.europa.eu/resource/authority/fd_555/035030\"/>\n" +
            "    <cdm:resource_legal_is_about_subject-matter rdf:resource=\"http://publications.europa.eu/resource/authority/fd_070/VETE\"/>\n" +
            "    <cdm:addresses rdf:resource=\"http://publications.europa.eu/resource/authority/fd_050/15EM\"/>\n" +
            "    <cdm:is_about rdf:resource=\"http://eurovoc.europa.eu/2737\"/>\n" +
            "    <cdm:resource_legal_addresses_institution rdf:resource=\"http://publications.europa.eu/resource/authority/fd_050/15EM\"/>\n" +
            "    <cdm:is_about rdf:resource=\"http://eurovoc.europa.eu/3730\"/>\n" +
            "    <cdm:is_about rdf:resource=\"http://publications.europa.eu/resource/authority/fd_070/VETE\"/>\n" +
            "    <cdm:work_part_of_collection_document rdf:resource=\"http://publications.europa.eu/resource/authority/document-collection/CELEX\"/>\n" +
            "    <cdm:is_about rdf:resource=\"http://eurovoc.europa.eu/1729\"/>\n" +
            "    <cdm:work_is_about_concept_eurovoc rdf:resource=\"http://eurovoc.europa.eu/2771\"/>\n" +
            "    <cdm:work_is_about_concept_eurovoc rdf:resource=\"http://eurovoc.europa.eu/3730\"/>\n" +
            "    <cdm:part_of rdf:resource=\"http://publications.europa.eu/resource/authority/document-collection/CELEX\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://eurovoc.europa.eu/2737\">\n" +
            "    <skos:inScheme rdf:resource=\"http://eurovoc.europa.eu/100141\"/>\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://publications.europa.eu/resource/authority/fd_050/15EM\">\n" +
            "    <skos:inScheme rdf:resource=\"http://publications.europa.eu/resource/authority/corporate-body\"/>\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "  </rdf:Description>\n" +
            "</rdf:RDF>\n";

    private static final String DIRECT_INFERRED = "<rdf:RDF\n" +
            "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n" +
            "    xmlns:j.0=\"http://publications.europa.eu/ontology/cdm#\"\n" +
            "    xmlns:owl=\"http://www.w3.org/2002/07/owl#\"\n" +
            "    xmlns:j.1=\"http://www.w3.org/2004/02/skos/core#\"\n" +
            "    xmlns:rdfs=\"http://www.w3.org/2000/01/rdf-schema#\"\n" +
            "    xmlns:j.2=\"http://publications.europa.eu/ontology/cdm/cmr#\" > \n" +
            "  <rdf:Description rdf:about=\"http://eurovoc.europa.eu/1729\">\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept_eurovoc\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept\"/>\n" +
            "    <j.1:inScheme rdf:resource=\"http://eurovoc.europa.eu/100141\"/>\n" +
            "    <j.0:concept_eurovoc_subject_of_work rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/oj/JOL_2006_088_R_0063_01\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://eurovoc.europa.eu/2771\">\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept_eurovoc\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept\"/>\n" +
            "    <j.1:inScheme rdf:resource=\"http://eurovoc.europa.eu/100141\"/>\n" +
            "    <j.0:concept_eurovoc_subject_of_work rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/oj/JOL_2006_088_R_0063_01\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:nodeID=\"A0\">\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2002/07/owl#Axiom\"/>\n" +
            "    <j.0:type>MS</j.0:type>\n" +
            "    <owl:annotatedSource rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/32006D0241\"/>\n" +
            "    <owl:annotatedProperty rdf:resource=\"http://publications.europa.eu/ontology/cdm#resource_legal_repeals_resource_legal\"/>\n" +
            "    <owl:annotatedTarget rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/31997D0517\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:nodeID=\"A1\">\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2002/07/owl#Axiom\"/>\n" +
            "    <rdfs:comment>DATNOT</rdfs:comment>\n" +
            "    <j.0:type>EV</j.0:type>\n" +
            "    <owl:annotatedSource rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/32006D0241\"/>\n" +
            "    <owl:annotatedProperty rdf:resource=\"http://publications.europa.eu/ontology/cdm#resource_legal_date_entry-into-force\"/>\n" +
            "    <owl:annotatedTarget rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">2006-03-24</owl:annotatedTarget>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:nodeID=\"A2\">\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2002/07/owl#Axiom\"/>\n" +
            "    <rdfs:comment>A22</rdfs:comment>\n" +
            "    <owl:annotatedSource rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/32006D0241\"/>\n" +
            "    <owl:annotatedProperty rdf:resource=\"http://publications.europa.eu/ontology/cdm#resource_legal_based_on_resource_legal\"/>\n" +
            "    <owl:annotatedTarget rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/31997L0078\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://publications.europa.eu/resource/authority/fd_555/035030\">\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept_directory-code\"/>\n" +
            "    <j.1:inScheme rdf:resource=\"http://publications.europa.eu/resource/authority/fd_555\"/>\n" +
            "    <j.0:concept_directory-code_subject_of_legislation_secondary rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/oj/JOL_2006_088_R_0063_01\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://publications.europa.eu/resource/authority/fd_030/DECISION\">\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept_type_act\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept_decoding\"/>\n" +
            "    <j.1:inScheme rdf:resource=\"http://publications.europa.eu/resource/authority/fd_030\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://cellar-dev.publications.europa.eu/resource/celex/02006D0241\">\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#resource_legal\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#act_consolidated\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#work\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://eurovoc.europa.eu/2737\">\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept_eurovoc\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept\"/>\n" +
            "    <j.1:inScheme rdf:resource=\"http://eurovoc.europa.eu/100141\"/>\n" +
            "    <j.0:concept_eurovoc_subject_of_work rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/oj/JOL_2006_088_R_0063_01\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://publications.europa.eu/resource/authority/fd_020/CEU\">\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#work\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#treaty\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#resource_legal\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://eurovoc.europa.eu/3730\">\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept_eurovoc\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept\"/>\n" +
            "    <j.1:inScheme rdf:resource=\"http://eurovoc.europa.eu/100141\"/>\n" +
            "    <j.0:concept_eurovoc_subject_of_work rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/oj/JOL_2006_088_R_0063_01\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://publications.europa.eu/resource/authority/document-collection/CELEX\">\n" +
            "    <j.1:inScheme rdf:resource=\"http://publications.europa.eu/resource/authority/document-collection\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#work\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#collection_document\"/>\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#collection\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://publications.europa.eu/resource/authority/fd_070/VETE\">\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept\"/>\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#subject-matter\"/>\n" +
            "    <j.1:inScheme rdf:resource=\"http://publications.europa.eu/resource/authority/fd_070\"/>\n" +
            "    <j.0:subject-matter_subject_of_resource_legal rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/oj/JOL_2006_088_R_0063_01\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://cellar-dev.publications.europa.eu/resource/celex/32006D0241\">\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#work\"/>\n" +
            "    <j.0:work_has_expression rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/32006D0241.dan\"/>\n" +
            "    <j.0:work_has_expression rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/32006D0241.ger\"/>\n" +
            "    <j.0:work_has_expression rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/32006D0241.fra\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://eurovoc.europa.eu/3579\">\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept_eurovoc\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#concept\"/>\n" +
            "    <j.1:inScheme rdf:resource=\"http://eurovoc.europa.eu/100141\"/>\n" +
            "    <j.0:concept_eurovoc_subject_of_work rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/oj/JOL_2006_088_R_0063_01\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://cellar-dev.publications.europa.eu/resource/celex/31997L0078\">\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#resource_legal\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#work\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://cellar-dev.publications.europa.eu/resource/oj/JOL_2006_088_R_0063_01\">\n" +
            "    <j.0:resource_legal_addresses_institution rdf:resource=\"http://publications.europa.eu/resource/authority/fd_050/15EM\"/>\n" +
            "    <j.0:resource_legal_comment_internal rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">MAN2</j.0:resource_legal_comment_internal>\n" +
            "    <j.0:addresses rdf:resource=\"http://publications.europa.eu/resource/authority/fd_050/15EM\"/>\n" +
            "    <j.0:title rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">tbd, title of the work</j.0:title>\n" +
            "    <j.0:modifies rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/31997D0517\"/>\n" +
            "    <j.0:date_notification rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">2006-03-24</j.0:date_notification>\n" +
            "    <j.0:is_about rdf:resource=\"http://eurovoc.europa.eu/1729\"/>\n" +
            "    <j.0:comment_internal rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">MAN2</j.0:comment_internal>\n" +
            "    <j.0:id_document rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">celex:32006D0241</j.0:id_document>\n" +
            "    <j.0:work_is_about_concept_eurovoc rdf:resource=\"http://eurovoc.europa.eu/3730\"/>\n" +
            "    <j.0:based_on rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/31997L0078\"/>\n" +
            "    <j.0:is_about rdf:resource=\"http://eurovoc.europa.eu/2771\"/>\n" +
            "    <j.0:work_id_document rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">celex:32006D0241</j.0:work_id_document>\n" +
            "    <j.0:work_created_by_agent rdf:resource=\"http://publications.europa.eu/resource/authority/corporate-body/COMM\"/>\n" +
            "    <j.0:legislation_secondary_modifies_legislation_secondary rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/31997D0517\"/>\n" +
            "    <j.0:work_is_about_concept_eurovoc rdf:resource=\"http://eurovoc.europa.eu/2771\"/>\n" +
            "    <j.0:work_is_about_concept_eurovoc rdf:resource=\"http://eurovoc.europa.eu/1729\"/>\n" +
            "    <j.0:resource_legal_has_type_act_concept_type_act rdf:resource=\"http://publications.europa.eu/resource/authority/fd_030/DECISION\"/>\n" +
            "    <j.0:has_type_act rdf:resource=\"http://publications.europa.eu/resource/authority/fd_030/DECISION\"/>\n" +
            "    <j.0:resource_legal_information_miscellaneous rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">P/EEE</j.0:resource_legal_information_miscellaneous>\n" +
            "    <j.0:date_document rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">2006-03-24</j.0:date_document>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#legislation_secondary\"/>\n" +
            "    <j.0:resource_legal_number_natural_celex rdf:datatype=\"http://www.w3.org/2001/XMLSchema#positiveInteger\">0241</j.0:resource_legal_number_natural_celex>\n" +
            "    <j.0:is_about rdf:resource=\"http://eurovoc.europa.eu/3730\"/>\n" +
            "    <j.0:resource_legal_based_on_treaty rdf:resource=\"http://publications.europa.eu/resource/authority/fd_020/CEU\"/>\n" +
            "    <j.0:id_sector rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">3</j.0:id_sector>\n" +
            "    <j.0:work_is_about_concept_eurovoc rdf:resource=\"http://eurovoc.europa.eu/2737\"/>\n" +
            "    <j.0:work_date_document rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">2006-03-24</j.0:work_date_document>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#resource_legal\"/>\n" +
            "    <j.0:resource_legal_type rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">D</j.0:resource_legal_type>\n" +
            "    <j.0:is_about rdf:resource=\"http://publications.europa.eu/resource/authority/fd_070/VETE\"/>\n" +
            "    <j.0:is_about rdf:resource=\"http://eurovoc.europa.eu/2737\"/>\n" +
            "    <j.0:consolidated_by rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/02006D0241\"/>\n" +
            "    <j.0:id_document rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">oj:JOL_2006_088_R_0063_01</j.0:id_document>\n" +
            "    <j.0:repertoire rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">REP</j.0:repertoire>\n" +
            "    <j.0:work_title rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">tbd, title of the work</j.0:work_title>\n" +
            "    <j.0:date rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">9999-12-31</j.0:date>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#decision\"/>\n" +
            "    <j.0:id_celex rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">32006D0241</j.0:id_celex>\n" +
            "    <j.0:resource_legal_repeals_resource_legal rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/31997D0517\"/>\n" +
            "    <j.0:resource_legal_date_end-of-validity rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">9999-12-31</j.0:resource_legal_date_end-of-validity>\n" +
            "    <j.0:legislation_secondary_date_notification rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">2006-03-24</j.0:legislation_secondary_date_notification>\n" +
            "    <j.0:type rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">D</j.0:type>\n" +
            "    <j.0:date rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">2006-03-24</j.0:date>\n" +
            "    <j.0:date_end-of-validity rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">9999-12-31</j.0:date_end-of-validity>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#work\"/>\n" +
            "    <j.0:identifier rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">32006D0241</j.0:identifier>\n" +
            "    <j.0:is_about rdf:resource=\"http://eurovoc.europa.eu/3579\"/>\n" +
            "    <j.0:date_entry-into-force rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">2006-03-24</j.0:date_entry-into-force>\n" +
            "    <j.0:resource_legal_repertoire rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">REP</j.0:resource_legal_repertoire>\n" +
            "    <j.0:based_on rdf:resource=\"http://publications.europa.eu/resource/authority/fd_020/CEU\"/>\n" +
            "    <j.0:year rdf:datatype=\"http://www.w3.org/2001/XMLSchema#gYear\">2006</j.0:year>\n" +
            "    <j.0:repeals rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/31997D0517\"/>\n" +
            "    <j.0:work_id_document rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">oj:JOL_2006_088_R_0063_01</j.0:work_id_document>\n" +
            "    <j.0:resource_legal_date_entry-into-force rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">2006-03-24</j.0:resource_legal_date_entry-into-force>\n" +
            "    <j.0:identifier rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">celex:32006D0241</j.0:identifier>\n" +
            "    <j.0:legislation_secondary_is_about_concept_directory-code rdf:resource=\"http://publications.europa.eu/resource/authority/fd_555/035030\"/>\n" +
            "    <j.0:resource_legal_id_celex rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">32006D0241</j.0:resource_legal_id_celex>\n" +
            "    <j.0:work_is_about_concept_eurovoc rdf:resource=\"http://eurovoc.europa.eu/3579\"/>\n" +
            "    <j.0:number_natural_celex rdf:datatype=\"http://www.w3.org/2001/XMLSchema#positiveInteger\">0241</j.0:number_natural_celex>\n" +
            "    <j.0:part_of rdf:resource=\"http://publications.europa.eu/resource/authority/document-collection/CELEX\"/>\n" +
            "    <j.0:resource_legal_based_on_resource_legal rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/31997L0078\"/>\n" +
            "    <j.0:identifier rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">oj:JOL_2006_088_R_0063_01</j.0:identifier>\n" +
            "    <j.0:resource_legal_consolidated_by_act_consolidated rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/02006D0241\"/>\n" +
            "    <j.0:information_miscellaneous rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">P/EEE</j.0:information_miscellaneous>\n" +
            "    <j.0:resource_legal_is_about_subject-matter rdf:resource=\"http://publications.europa.eu/resource/authority/fd_070/VETE\"/>\n" +
            "    <j.0:resource_legal_id_sector rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">3</j.0:resource_legal_id_sector>\n" +
            "    <j.0:created_by rdf:resource=\"http://publications.europa.eu/resource/authority/corporate-body/COMM\"/>\n" +
            "    <j.0:resource_legal_year rdf:datatype=\"http://www.w3.org/2001/XMLSchema#gYear\">2006</j.0:resource_legal_year>\n" +
            "    <j.0:is_about rdf:resource=\"http://publications.europa.eu/resource/authority/fd_555/035030\"/>\n" +
            "    <j.0:work_part_of_collection_document rdf:resource=\"http://publications.europa.eu/resource/authority/document-collection/CELEX\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://publications.europa.eu/resource/authority/fd_050/15EM\">\n" +
            "    <j.1:inScheme rdf:resource=\"http://publications.europa.eu/resource/authority/corporate-body\"/>\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2004/02/skos/core#Concept\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#agent\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#institution\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://cellar-dev.publications.europa.eu/resource/cellar/3464aaa5-189b-11e8-9e8e-01aa75ed71a1\">\n" +
            "    <owl:sameAs rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/32006D0241\"/>\n" +
            "    <owl:sameAs rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/oj/JOL_2006_088_R_0063_01\"/>\n" +
            "    <j.2:lastModificationDate rdf:datatype=\"http://www.w3.org/2001/XMLSchema#dateTime\">2018-02-23T14:12:30.843+01:00</j.2:lastModificationDate>\n" +
            "    <j.2:creationDate rdf:datatype=\"http://www.w3.org/2001/XMLSchema#dateTime\">2018-02-23T14:12:32.485+01:00</j.2:creationDate>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://cellar-dev.publications.europa.eu/resource/celex/31997D0517\">\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#work\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#resource_legal\"/>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#legislation_secondary\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://publications.europa.eu/resource/authority/corporate-body/COMM\">\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#agent\"/>\n" +
            "  </rdf:Description>\n" +
            "</rdf:RDF>\n";

    private static final String DIRECT = "<rdf:RDF\n" +
            "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n" +
            "    xmlns:j.0=\"http://publications.europa.eu/ontology/cdm#\"\n" +
            "    xmlns:owl=\"http://www.w3.org/2002/07/owl#\"\n" +
            "    xmlns:rdfs=\"http://www.w3.org/2000/01/rdf-schema#\"\n" +
            "    xmlns:j.1=\"http://publications.europa.eu/ontology/cdm/cmr#\" > \n" +
            "  <rdf:Description rdf:about=\"http://cellar-dev.publications.europa.eu/resource/oj/JOL_2006_088_R_0063_01\">\n" +
            "    <j.0:resource_legal_comment_internal rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">MAN2</j.0:resource_legal_comment_internal>\n" +
            "    <j.0:resource_legal_has_type_act_concept_type_act rdf:resource=\"http://publications.europa.eu/resource/authority/fd_030/DECISION\"/>\n" +
            "    <j.0:work_date_document rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">2006-03-24</j.0:work_date_document>\n" +
            "    <j.0:work_id_document rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">oj:JOL_2006_088_R_0063_01</j.0:work_id_document>\n" +
            "    <rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#decision\"/>\n" +
            "    <j.0:work_is_about_concept_eurovoc rdf:resource=\"http://eurovoc.europa.eu/3579\"/>\n" +
            "    <j.0:work_id_document rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">celex:32006D0241</j.0:work_id_document>\n" +
            "    <j.0:resource_legal_date_entry-into-force rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">2006-03-24</j.0:resource_legal_date_entry-into-force>\n" +
            "    <j.0:resource_legal_year rdf:datatype=\"http://www.w3.org/2001/XMLSchema#gYear\">2006</j.0:resource_legal_year>\n" +
            "    <j.0:resource_legal_information_miscellaneous rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">P/EEE</j.0:resource_legal_information_miscellaneous>\n" +
            "    <j.0:work_created_by_agent rdf:resource=\"http://publications.europa.eu/resource/authority/corporate-body/COMM\"/>\n" +
            "    <j.0:resource_legal_id_celex rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">32006D0241</j.0:resource_legal_id_celex>\n" +
            "    <j.0:resource_legal_based_on_treaty rdf:resource=\"http://publications.europa.eu/resource/authority/fd_020/CEU\"/>\n" +
            "    <j.0:resource_legal_repeals_resource_legal rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/31997D0517\"/>\n" +
            "    <j.0:work_is_about_concept_eurovoc rdf:resource=\"http://eurovoc.europa.eu/1729\"/>\n" +
            "    <j.0:work_is_about_concept_eurovoc rdf:resource=\"http://eurovoc.europa.eu/2737\"/>\n" +
            "    <j.0:legislation_secondary_is_about_concept_directory-code rdf:resource=\"http://publications.europa.eu/resource/authority/fd_555/035030\"/>\n" +
            "    <j.0:resource_legal_is_about_subject-matter rdf:resource=\"http://publications.europa.eu/resource/authority/fd_070/VETE\"/>\n" +
            "    <j.0:resource_legal_addresses_institution rdf:resource=\"http://publications.europa.eu/resource/authority/fd_050/15EM\"/>\n" +
            "    <j.0:resource_legal_based_on_resource_legal rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/31997L0078\"/>\n" +
            "    <j.0:work_part_of_collection_document rdf:resource=\"http://publications.europa.eu/resource/authority/document-collection/CELEX\"/>\n" +
            "    <j.0:work_title rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">tbd, title of the work</j.0:work_title>\n" +
            "    <j.0:resource_legal_date_end-of-validity rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">9999-12-31</j.0:resource_legal_date_end-of-validity>\n" +
            "    <j.0:legislation_secondary_modifies_legislation_secondary rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/31997D0517\"/>\n" +
            "    <j.0:work_is_about_concept_eurovoc rdf:resource=\"http://eurovoc.europa.eu/3730\"/>\n" +
            "    <j.0:legislation_secondary_date_notification rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">2006-03-24</j.0:legislation_secondary_date_notification>\n" +
            "    <j.0:work_is_about_concept_eurovoc rdf:resource=\"http://eurovoc.europa.eu/2771\"/>\n" +
            "    <j.0:resource_legal_number_natural_celex rdf:datatype=\"http://www.w3.org/2001/XMLSchema#positiveInteger\">0241</j.0:resource_legal_number_natural_celex>\n" +
            "    <j.0:resource_legal_type rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">D</j.0:resource_legal_type>\n" +
            "    <j.0:resource_legal_id_sector rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">3</j.0:resource_legal_id_sector>\n" +
            "    <j.0:resource_legal_repertoire rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">REP</j.0:resource_legal_repertoire>\n" +
            "    <j.0:resource_legal_consolidated_by_act_consolidated rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/02006D0241\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:nodeID=\"A0\">\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2002/07/owl#Axiom\"/>\n" +
            "    <rdfs:comment>DATNOT</rdfs:comment>\n" +
            "    <j.0:type>EV</j.0:type>\n" +
            "    <owl:annotatedSource rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/32006D0241\"/>\n" +
            "    <owl:annotatedProperty rdf:resource=\"http://publications.europa.eu/ontology/cdm#resource_legal_date_entry-into-force\"/>\n" +
            "    <owl:annotatedTarget rdf:datatype=\"http://www.w3.org/2001/XMLSchema#date\">2006-03-24</owl:annotatedTarget>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:about=\"http://cellar-dev.publications.europa.eu/resource/cellar/3464aaa5-189b-11e8-9e8e-01aa75ed71a1\">\n" +
            "    <owl:sameAs rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/32006D0241\"/>\n" +
            "    <owl:sameAs rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/oj/JOL_2006_088_R_0063_01\"/>\n" +
            "    <j.1:lastModificationDate rdf:datatype=\"http://www.w3.org/2001/XMLSchema#dateTime\">2018-02-23T14:12:30.843+01:00</j.1:lastModificationDate>\n" +
            "    <j.1:creationDate rdf:datatype=\"http://www.w3.org/2001/XMLSchema#dateTime\">2018-02-23T14:12:32.485+01:00</j.1:creationDate>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:nodeID=\"A1\">\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2002/07/owl#Axiom\"/>\n" +
            "    <rdfs:comment>A22</rdfs:comment>\n" +
            "    <owl:annotatedSource rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/32006D0241\"/>\n" +
            "    <owl:annotatedProperty rdf:resource=\"http://publications.europa.eu/ontology/cdm#resource_legal_based_on_resource_legal\"/>\n" +
            "    <owl:annotatedTarget rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/31997L0078\"/>\n" +
            "  </rdf:Description>\n" +
            "  <rdf:Description rdf:nodeID=\"A2\">\n" +
            "    <rdf:type rdf:resource=\"http://www.w3.org/2002/07/owl#Axiom\"/>\n" +
            "    <j.0:type>MS</j.0:type>\n" +
            "    <owl:annotatedSource rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/32006D0241\"/>\n" +
            "    <owl:annotatedProperty rdf:resource=\"http://publications.europa.eu/ontology/cdm#resource_legal_repeals_resource_legal\"/>\n" +
            "    <owl:annotatedTarget rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/celex/31997D0517\"/>\n" +
            "  </rdf:Description>\n" +
            "</rdf:RDF>\n";

}
