/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.monitoring
 *             FILE : WatchAdviceTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 09 18, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-09-18 11:04:47 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.monitoring;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.monitoring.Watch.WatchArgument;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

/**
 * @author ARHS Developments
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = WatchAdviceTest.HierarchyConfiguration.class)
public class WatchAdviceTest {

    @Autowired
    private AssertWatchAdvice watchAdvice;

    @Autowired
    private A a;

    @Autowired
    private AA aa;

    @Autowired
    private AAA aaa;

    @Autowired
    private C c;

    @After
    public void tearDown() {
        watchAdvice.clear();
    }

    @Test
    public void watchAnnotationMustCreateANewCaller() {
        a.a(0);
        watchAdvice.assertCallerCount(1);
        watchAdvice.clear();
        aa.aa(0);
        watchAdvice.assertCallerCount(2);
    }

    @Test
    public void recallOfTheCallerByACalleeMustNotBreakTheContext() {
        aaa.aaa(3);
        watchAdvice.assertCallerCount(4);
    }

    @Test
    public void primitivesArgumentsAreHandledAsIs() {
        a.a(10);
        watchAdvice.assertArgValue("(The var = 10)");
    }

    @Test
    public void objectsArgumentsAreNavigable() {
        c.c(new Type("test", 12));
        watchAdvice.assertArgValue("(var1 of Type = test)");
    }

    @Test
    public void contextsAreThreadLocal() throws InterruptedException {
        CountDownLatch latch = new CountDownLatch(2);
        ExecutorService exec1 = Executors.newFixedThreadPool(1);
        ExecutorService exec2 = Executors.newFixedThreadPool(1);
        exec1.execute(() -> {
            a.a(0);
            latch.countDown();
        });
        exec2.execute(() -> {
            a.a(0);
            latch.countDown();
        });
        watchAdvice.assertContext(false);
        latch.await(1, TimeUnit.SECONDS);
        CountDownLatch latch2 = new CountDownLatch(2);
        exec1.execute(() -> {
            watchAdvice.assertCallerCount(1);
            latch2.countDown();
        });
        exec2.execute(() -> {
            watchAdvice.assertCallerCount(1);
            latch2.countDown();
        });
        latch2.await(1, TimeUnit.SECONDS);

        exec1.shutdownNow();
        exec2.shutdownNow();
    }

    @Test
    public void timeUnitMustBePropagatedToTheContext() {
        a.aUnit(0);
        watchAdvice.assertTimeUnit(TimeUnit.SECONDS);
    }


    @Configuration
    @EnableAspectJAutoProxy
    static class HierarchyConfiguration {

        @Bean
        public AssertWatchAdvice watchAdvice() {
            ICellarConfiguration cfg = Mockito.mock(ICellarConfiguration.class);
            Mockito.when(cfg.isCellarWatchAdviceEnabled()).thenReturn(true);
            return new AssertWatchAdvice(cfg);
        }

        @Bean
        public A a() {
            return new A();
        }

        @Bean
        public AA aa() {
            return new AA(b());
        }

        @Bean
        public AAA aaa() {
            return new AAA(bb());
        }

        @Bean
        public B b() {
            return new B();
        }

        @Bean
        public BB bb() {
            return new BB();
        }

        @Bean
        public C c() {
            return new C();
        }
    }

    static class AssertWatchAdvice extends WatchAdvice {

        private Consumer<WatchContext> consumer;

        public AssertWatchAdvice(ICellarConfiguration configuration) {
            super(configuration);
        }

        void assertContext(boolean present) {
            Assert.assertEquals(present,CONTEXTS.get() != null);
        }

        void assertCallerCount(int count) {
            Assert.assertEquals(count, CONTEXTS.get().getCallers().size());
        }

        void assertTimeUnit(TimeUnit expected) {
            Assert.assertEquals(expected, CONTEXTS.get().getCallers().getFirst().watch.unit());
        }

        void assertArgValue(String value) {
            Caller caller = CONTEXTS.get().getCallers().getFirst();
            Assert.assertEquals(value, WatchAdvice.resolveArgs(caller.pjp, caller.watch));
        }

        @Override
        protected void clearContext() {
        }

        void clear() {
            super.clearContext();
        }
    }
}

class A {
    @Watch(value = "a", arguments = @WatchArgument(name = "The var", expression = "var"))
    public void a(int var) {
    }

    @Watch(value = "a", arguments = @WatchArgument(name = "The var", expression = "var"), unit = TimeUnit.SECONDS)
    public void aUnit(int var) {
    }
}

class AA {
    private B b;

    AA(B b) {
        this.b = b;
    }

    @Watch("a")
    public void aa(int var) {
        b.b(var);
    }
}

class B {

    @Watch("b")
    public void b(int var) {
    }
}

class AAA {
    private final BB bb;
    private int count = 0;

    AAA(BB bb) {
        this.bb = bb;
    }

    @Watch("aaa")
    public void aaa(int var) {
        if (count < var) {
            count++;
            bb.bb(this, var);
        }
    }
}

class BB {

    @Watch("bb")
    public void bb(AAA aaa, int var) {
        aaa.aaa(var);
    }
}

class C {

    @Watch(value = "c", arguments = @WatchArgument(name = "var1 of Type", expression = "type.var1"))
    public void c(Type type) {

    }
}

class Type {
    private String var1;
    private int var2;

    public Type(String var1, int var2) {
        this.var1 = var1;
        this.var2 = var2;
    }

    public String getVar1() {
        return var1;
    }

    public void setVar1(String var1) {
        this.var1 = var1;
    }

    public int getVar2() {
        return var2;
    }

    public void setVar2(int var2) {
        this.var2 = var2;
    }
}

