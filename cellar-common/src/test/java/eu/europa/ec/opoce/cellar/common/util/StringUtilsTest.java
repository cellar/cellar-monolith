/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.util
 *             FILE : BoundedPriorityBlockingQueueTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 16, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.util;

import eu.europa.ec.opoce.cellar.exception.CellarConfigurationException;
import org.junit.Assert;
import org.junit.Test;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 16, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class StringUtilsTest {

    @Test
    public void abbreviateRightTest() {
        final String string = "String";
        final int maxWidth = 4;
        final String abbreviateRight = StringUtils.abbreviateRight(string, maxWidth);

        Assert.assertEquals(abbreviateRight, "S...");
    }

    @Test
    public void abbreviateLeftTest() {
        final String string = "String";
        final int maxWidth = 4;
        final String abbreviateLeft = StringUtils.abbreviateLeft(string, maxWidth);

        Assert.assertEquals(abbreviateLeft, "...g");
    }

    @Test(expected = CellarConfigurationException.class)
    public void nonCompatibleValuesBooleanTest() {
        StringUtils.isCompatibleValue(Boolean.class, "");
    }

    @Test(expected = CellarConfigurationException.class)
    public void nonCompatibleValuesClassTest() {
        StringUtils.isCompatibleValue(Class.class, "xxx");
    }

    @Test(expected = CellarConfigurationException.class)
    public void nonCompatibleValuesByteTest() {
        StringUtils.isCompatibleValue(Byte.class, Short.MAX_VALUE + "");
    }

    @Test(expected = CellarConfigurationException.class)
    public void nonCompatibleValuesShortTest() {
        StringUtils.isCompatibleValue(Short.class, Integer.MAX_VALUE + "");
    }

    @Test(expected = CellarConfigurationException.class)
    public void nonCompatibleValuesIntegerTest() {
        StringUtils.isCompatibleValue(Integer.class, Long.MAX_VALUE + "");
    }

    @Test(expected = CellarConfigurationException.class)
    public void nonCompatibleValuesLongTest() {
        StringUtils.isCompatibleValue(Long.class, "0.1");
    }

    @Test(expected = CellarConfigurationException.class)
    public void nonCompatibleValuesFloatTest() {
        StringUtils.isCompatibleValue(Float.class, "xxx");
    }

    @Test(expected = CellarConfigurationException.class)
    public void nonCompatibleValuesDoubleTest() {
        StringUtils.isCompatibleValue(Double.class, "xxx");
    }

    @Test
    public void compatibleValuesTest() {
        StringUtils.isCompatibleValue(Boolean.class, "true");
        StringUtils.isCompatibleValue(Boolean.class, "True");
        StringUtils.isCompatibleValue(Class.class, String.class.getName());
        StringUtils.isCompatibleValue(Byte.class, "0");
        StringUtils.isCompatibleValue(Short.class, "0");
        StringUtils.isCompatibleValue(Integer.class, "0");
        StringUtils.isCompatibleValue(Long.class, "-0");
        StringUtils.isCompatibleValue(Float.class, "0");
        StringUtils.isCompatibleValue(Float.class, "-0.1");
        StringUtils.isCompatibleValue(Double.class, "0.11111111111111111111");
        StringUtils.isCompatibleValue(Double.class, "99999.8d");
        StringUtils.isCompatibleValue(null, "xxx");
        StringUtils.isCompatibleValue(String.class, null);
        StringUtils.isCompatibleValue(null, null);
    }
}
