/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.exception
 *             FILE : MetadataValidationResultAwareExceptionBuilder.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 10 05, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-10-05 13:23:01 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.exception;

import eu.europa.ec.opoce.cellar.cl.domain.validation.MetadataValidationResult;

/**
 * @author ARHS Developments
 */
public class MetadataValidationResultAwareExceptionBuilder<EXC extends MetadataValidationResultAwareException> extends ExceptionBuilder<EXC> {

    protected String structMapId;
    protected MetadataValidationResult metadataValidationResult;

    protected MetadataValidationResultAwareExceptionBuilder(final Class<EXC> exceptionClass) {
        super(exceptionClass);
    }

    public static <EXC extends MetadataValidationResultAwareException> MetadataValidationResultAwareExceptionBuilder<EXC> getInstance(final Class<EXC> exceptionClass) {
        return new MetadataValidationResultAwareExceptionBuilder<>(exceptionClass);
    }

    public MetadataValidationResultAwareExceptionBuilder<EXC> withStructMapId(final String structMapId) {
        this.structMapId = structMapId;
        return this;
    }

    public MetadataValidationResultAwareExceptionBuilder<EXC> withMetadataValidationResult(final MetadataValidationResult metadataValidationResult) {
        this.metadataValidationResult = metadataValidationResult;
        return this;
    }
}
