/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.monitoring
 *             FILE : ApiCallHolder.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 04 04, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-04-04 11:40:12 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.monitoring;

import com.google.common.base.MoreObjects;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ARHS Developments
 */
public class ApiCall {

    private String clazz;
    private String method;
    private String path;
    private List<Object> args = new ArrayList<>(0);
    private List<String> produces = new ArrayList<>(0);
    private List<String> consumes = new ArrayList<>(0);
    private List<RequestMethod> methods = new ArrayList<>(0);

    public ApiCall() {
    }

    public String getClazz() {
        return clazz;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public List<Object> getArgs() {
        return args;
    }

    public void setArgs(List<Object> args) {
        this.args = args;
    }

    public List<String> getProduces() {
        return produces;
    }

    public void setProduces(List<String> produces) {
        this.produces = produces;
    }

    public List<String> getConsumes() {
        return consumes;
    }

    public void setConsumes(List<String> consumes) {
        this.consumes = consumes;
    }

    public List<RequestMethod> getMethods() {
        return methods;
    }

    public void setMethods(List<RequestMethod> methods) {
        this.methods = methods;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("clazz", clazz)
                .add("method", method)
                .add("path", path)
                .add("args", args)
                .add("produces", produces)
                .add("consumes", consumes)
                .add("methods", methods)
                .toString();
    }
}
