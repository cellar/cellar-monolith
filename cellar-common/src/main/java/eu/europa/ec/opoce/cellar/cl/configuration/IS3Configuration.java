/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.configuration.impl
 *        FILE : IS3Configuration.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 02-04-2012
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.configuration;

import eu.europa.ec.opoce.cellar.cl.configuration.ConfigurationPropertyService.EXISTINGPROPS_MODE;

/**
 * <class_description> Definition of S3 configuration bean.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 02-04-2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IS3Configuration extends IConfiguration {

    /**
     * Gets the s3 server namespace.
     *
     * @return the s3 server namespace
     */
    String getCellarNamespace();

    void setCellarNamespace(String namespace, EXISTINGPROPS_MODE... syncMode);

    /**
     * Checks if is s3 service ingestion cache enabled.
     *
     * @return true, if is s3 service ingestion cache enabled
     */
    boolean isS3ServiceIngestionCacheEnabled();

    /**
     * Sets the s3 service ingestion cache enabled.
     *
     * @param s3ServiceIngestionCacheEnabled the s3 service ingestion cache enabled
     * @param syncMode                           the sync mode
     */
    void setS3ServiceIngestionCacheEnabled(boolean s3ServiceIngestionCacheEnabled, EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the s3 service ingestion cache coefficient.
     *
     * @return the s3 service ingestion cache coefficient
     */
    int getS3ServiceIngestionCacheCoefficient();

    /**
     * Gets the s3 service ingestion datastream renaming enabled.
     *
     * @return the s3 service ingestion datastream renaming enabled
     */
    boolean getS3ServiceIngestionDatastreamRenamingEnabled();

    void setS3ServiceIngestionDatastreamRenamingEnabled(boolean renamingEnabled, EXISTINGPROPS_MODE... syncMode);

}
