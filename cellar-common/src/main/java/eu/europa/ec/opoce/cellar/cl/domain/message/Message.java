package eu.europa.ec.opoce.cellar.cl.domain.message;

/**
 * The Class Message.
 */
public class Message {

    LEVEL messageLevel;
    String messageCode;
    String defaultMessage;
    Object[] messageArguments;

    public Message(LEVEL messageLevel, String messageCode, String defaultMessage, Object[] messageArguments) {
        this.messageLevel = messageLevel;
        this.messageCode = messageCode;
        this.defaultMessage = defaultMessage;
        this.messageArguments = messageArguments;
    }

    public LEVEL getMessageLevel() {
        return messageLevel;
    }

    public void setMessageLevel(LEVEL messageLevel) {
        this.messageLevel = messageLevel;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getDefaultMessage() {
        return defaultMessage;
    }

    public void setDefaultMessage(String defaultMessage) {
        this.defaultMessage = defaultMessage;
    }

    public Object[] getMessageArguments() {
        return messageArguments;
    }

    public void setMessageArguments(Object[] messageArguments) {
        this.messageArguments = messageArguments;
    }

    public enum LEVEL {
        INFO, SUCCESS, ERROR, WARNING
    }
}
