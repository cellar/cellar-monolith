package eu.europa.ec.opoce.cellar.domain.content.mets;

import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;

import java.io.File;
import java.io.InputStream;

public interface MetsPackage {

    /**
     * @return Pointer to the zip file, never null.
     */
    public File getZipFile();

    /**
     * @return Pointer to the extraction folder, may be null if not extracted, or invalid if in other system. (fallback is getZipFile())
     */
    public File getZipExtractionFolder();

    /**
     * @return the metsfile is returned as an inputStream
     */
    public InputStream getMetsStream();

    /**
     * @param fileRef reference to the file that is needed
     * @return the asked file is returned as an inputStream
     */
    public InputStream getFileStream(String fileRef);

    /**
     * @return the type of sip
     */
    public TYPE getSipType();

}
