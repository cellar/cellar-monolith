/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.database.dao.spring
 *             FILE : SpringBaseDao.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 3 août 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.database.dao.spring;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.dao.BaseDao;
import eu.europa.ec.opoce.cellar.cmr.database.DaoObject;
import eu.europa.ec.opoce.cellar.cmr.database.dao.id.DatabaseIdService;
import eu.europa.ec.opoce.cellar.common.util.Counter;
import eu.europa.ec.opoce.cellar.common.util.IterableUtils;
import eu.europa.ec.opoce.cellar.common.util.Pair;
import eu.europa.ec.opoce.cellar.common.util.StringUtils;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.NotImplementedExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.helper.StringHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.scheduling.annotation.Async;

import javax.sql.DataSource;
import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>Abstract SpringBaseDao class.</p>
 *
 * @author ARHS Developments
 * @version $Revision$
 * @param <ENTITY> the generic type
 * @param <ID> the generic type
 */

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 2 août 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public abstract class SpringBaseDao<ENTITY extends DaoObject, ID extends Serializable> extends NamedParameterJdbcDaoSupport
        implements BaseDao<ENTITY, ID>, RowMapper<ENTITY> {

    private  static final Logger LOG = LoggerFactory.getLogger(SpringBaseDao.class);

    /**
     * The cellar configuration to use.
     * It is static and not persistent, as the latter one uses this Dao class itself, and would not be completely initialized at this point.
     */
    @Autowired
    @Qualifier("cellarStaticConfiguration")
    private ICellarConfiguration cellarConfiguration;

    /**
     * The database id service.
     */
    private DatabaseIdService databaseIdService;

    /**
     * The table name.
     */
    private String tableName;

    /**
     * The column names string.
     */
    private String columnNamesString;

    /**
     * The get by id query.
     */
    private String getByIdQuery;

    /**
     * The id column name.
     */
    private String idColumnName;

    /**
     * The column names.
     */
    protected final Collection<String> columnNames;

    /**
     * The upsert query.
     */
    private final static String UPSERT_QUERY = "" + //
            "MERGE INTO {} a" + "\n" + //
            "USING (" + "\n" + //
            "SELECT :{} {}" + "\n" + //
            "    FROM dual" + "\n" + //
            "    ) incoming" + "\n" + //
            "ON (a.{} = incoming.{})" + "\n" + //
            "WHEN MATCHED THEN" + "\n" + //
            "{}" + "\n" + //
            "WHEN NOT MATCHED THEN" + "\n" + //
            "{}";

    /**
     * <p>Constructor for SpringBaseDao.</p>
     *
     * @param tableName   a {@link java.lang.String} object.
     * @param columnNames a list of all columns to get back
     */
    protected SpringBaseDao(final String tableName, final Collection<String> columnNames) {
        this(tableName, "id", columnNames);
    }

    /**
     * <p>Constructor for SpringBaseDao.</p>
     *
     * @param tableName    a {@link java.lang.String} object.
     * @param idColumnName a {@link java.lang.String} object.
     * @param columnNames  a {@link java.util.Collection} object.
     */
    protected SpringBaseDao(final String tableName, final String idColumnName, final Collection<String> columnNames) {
        this.tableName = tableName;
        this.columnNames = new HashSet<>(columnNames);
        this.columnNamesString = org.apache.commons.lang.StringUtils.join(columnNames, ", ");
        this.idColumnName = idColumnName;
        this.getByIdQuery = StringHelper.format("select {}, {} from {} where {} = ?", idColumnName, this.columnNamesString, tableName,
                idColumnName);
    }

    /**
     * Creates the jdbc template.
     *
     * @param dataSource the data source
     * @return the jdbc template
     * @see org.springframework.jdbc.core.support.JdbcDaoSupport#createJdbcTemplate(javax.sql.DataSource)
     */
    @Override
    protected JdbcTemplate createJdbcTemplate(final DataSource dataSource) {
        return new CellarJdbcTemplate(dataSource, this.cellarConfiguration.isCellarDatabaseReadOnly());
    }

    /**
     * <p>Setter for the field <code>databaseIdService</code>.</p>
     *
     * @param databaseIdService a {@link DatabaseIdService} object.
     */
    public void setDatabaseIdService(final DatabaseIdService databaseIdService) {
        this.databaseIdService = databaseIdService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final ENTITY mapRow(final ResultSet rs, final int rowNum) throws SQLException {
        final ENTITY daoObject = this.create(rs);
        daoObject.setId(rs.getLong(this.idColumnName));
        return daoObject;
    }

    /**
     * <p>create.</p>
     *
     * @param rs a {@link java.sql.ResultSet} object.
     * @return a OBJ object.
     * @throws SQLException the SQL exception
     */
    protected abstract ENTITY create(ResultSet rs) throws SQLException;

    /**
     * <p>find.</p>
     *
     * @param whereQueryPart a {@link java.lang.String} object.
     * @return a {@link java.util.Collection} object.
     */
    protected final Collection<ENTITY> find(final String whereQueryPart) {
        return this.find(whereQueryPart, Collections.<String, Object>emptyMap());
    }

    /**
     * <p>findUnique.</p>
     *
     * @param whereQueryPart a {@link java.lang.String} object.
     * @param map            a {@link java.util.Map} object.
     * @return a OBJ object.
     */
    protected final ENTITY findUnique(final String whereQueryPart, final Map<String, Object> map) {
        final Collection<ENTITY> daoObjects = this.find(whereQueryPart, map);
        if (daoObjects.isEmpty()) {
            return null;
        }
        if (daoObjects.size() > 1) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("Found more than one result for a findUnique call (in {} with where {} )")
                    .withMessageArgs(this.tableName, whereQueryPart).build();
        }
        return daoObjects.iterator().next();
    }

    protected final ENTITY findUnique(final String whereQueryPart, final Map<String, Object> map, Collection<String> columnNames, RowMapper<ENTITY> mapper) {
        final Collection<ENTITY> daoObjects = this.find(whereQueryPart, map, tableName, columnNames, mapper);
        if (daoObjects.isEmpty()) {
            return null;
        }
        if (daoObjects.size() > 1) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("Found more than one result for a findUnique call (in {} with where {} )")
                    .withMessageArgs(this.tableName, whereQueryPart).build();
        }
        return daoObjects.iterator().next();
    }

    /**
     * <p>find.</p>
     *
     * @param whereQueryPart a {@link java.lang.String} object.
     * @param map            a {@link java.util.Map} object.
     * @return a {@link java.util.Collection} object.
     */
    protected final Collection<ENTITY> find(final String whereQueryPart, final Map<String, Object> map) {
        if (StringUtils.isBlank(this.tableName)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Table name '{}' cannot be empty for where clause {}.")
                    .withMessageArgs(this.tableName, whereQueryPart).build();
        }
        return find(whereQueryPart, map, this.tableName);
    }

    /**
     * Find.
     *
     * @param whereQueryPart the where query part
     * @param map            the map
     * @param theTableName   the the table name
     * @return the collection
     */
    protected final Collection<ENTITY> find(final String whereQueryPart, final Map<String, Object> map, final String theTableName) {
        Collection<ENTITY> result;
        if (StringUtils.isNotBlank(theTableName)) {
            final String query = StringHelper.format("select {}, {} from {} where {}", this.idColumnName, this.columnNamesString,
                    theTableName, whereQueryPart);
            result = this.findPagingAware(query, map);

        } else {
            result = find(whereQueryPart, map);
        }

        return result;
    }

    protected final Collection<ENTITY> find(final String whereQueryPart, final Map<String, Object> map, final String table, Collection<String> columns, RowMapper<ENTITY> mapper) {
        if (StringUtils.isNotBlank(table)) {
            final String query = StringHelper.format("select {}, {} from {} where {}", this.idColumnName,
                    StringUtils.join(columns, ", "), table, whereQueryPart);
            LOG.debug("Execute query [{}] with params [{}]", query, map);
            return findPagingAware(query, map, mapper);
        } else {
            return find(whereQueryPart, map);
        }
    }

    /**
     * <p>find.</p>
     *
     * @param whereQueryPart a {@link java.lang.String} object.
     * @param map            a {@link java.util.Map} object.
     * @param limit          the max number of results to retrieve.
     * @return a {@link eu.europa.ec.opoce.cellar.common.util.Pair} object with the result on <code>one</code>,
     * and a boolean which indicates whether the result has been truncated or not on <code>two</code>.
     */
    protected final Pair<Collection<ENTITY>, Boolean> find(final String whereQueryPart, final Map<String, Object> map, final int limit) {
        final String query = StringHelper.format("select {}, {} from ( select {}, {} from {} where {} ) where rownum <= {}",
                this.idColumnName, this.columnNamesString, this.idColumnName, this.columnNamesString, this.tableName, whereQueryPart,
                limit);
        final Collection<ENTITY> result = this.findPagingAware(query, map);

        final Long totalEntries = this.count(whereQueryPart, map);
        return new Pair<Collection<ENTITY>, Boolean>(result, result.size() < totalEntries);
    }

    /**
     * <p>find ordered.</p>
     *
     * @param whereQueryPart a {@link java.lang.String} object.
     * @param map            a {@link java.util.Map} object.
     * @param sortByColumn   the sort by column
     * @return a {@link java.util.Collection} object.
     */
    protected final Collection<ENTITY> findSortBy(final String whereQueryPart, final Map<String, Object> map, final String sortByColumn) {
        final String query = StringHelper.format("select {}, {} from {} where {} order by {}", this.idColumnName, this.columnNamesString,
                this.tableName, whereQueryPart, sortByColumn);
        return this.findPagingAware(query, map);
    }

    /**
     * <p>find ordered and limited.</p>
     *
     * @param whereQueryPart a {@link java.lang.String} object.
     * @param map            a {@link java.util.Map} object.
     * @param sortByColumn   the sort by column
     * @param limit          the max number of results to retrieve.
     * @return a {@link java.util.Collection} object.
     */
    protected final Pair<Collection<ENTITY>, Boolean> findSortBy(final String whereQueryPart, final Map<String, Object> map,
                                                                 final String sortByColumn, final int limit) {
        final String query = StringHelper.format("select {}, {} from ( select {}, {} from {} where {} ) where rownum <= {} order by {}",
                this.idColumnName, this.columnNamesString, this.idColumnName, this.columnNamesString, this.tableName, whereQueryPart, limit,
                sortByColumn);
        final Collection<ENTITY> result = this.findPagingAware(query, map);

        final Long totalEntries = this.count(whereQueryPart, map);
        return new Pair<Collection<ENTITY>, Boolean>(result, result.size() < totalEntries);
    }

    /**
     * Find sort by.
     *
     * @param whereQueryPart the where query part
     * @param map            the map
     * @param sortByColumns  the sort by columns
     * @param limit          the limit
     * @return the collection
     */
    protected final Collection<ENTITY> findSortBy(final String whereQueryPart, final Map<String, Object> map,
                                                  final Map<String, String> sortByColumns, final long limit) {
        final List<String> orderBy = sortByColumns.entrySet().stream().map(entry -> entry.getKey() + " " + entry.getValue())
                .collect(Collectors.toList());
        final String sortString = StringUtils.join(orderBy, ",");
        final String query = StringHelper.format("select {}, {} from ( select {}, {} from {} where {} ) where rownum <= {} order by {}",
                this.idColumnName, this.columnNamesString, this.idColumnName, this.columnNamesString, this.tableName, whereQueryPart, limit,
                sortString);
        final Collection<ENTITY> result = this.findPagingAware(query, map);
        return result;
    }

    /**
     * <p>findAllSortBy.</p>
     *
     * @param column a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    protected final List<ENTITY> findAllSortBy(final String column) {
        final String query = StringHelper.format("select {}, {} from {} order by {}", this.idColumnName, this.columnNamesString,
                this.tableName, column);
        return getNamedParameterJdbcTemplate().query(query, this);
    }

    /**
     * <p>findAllSortBy.</p>
     *
     * @param column a {@link java.lang.String} object.
     * @param limit  the max number of results to retrieve.
     * @return a {@link java.util.List} object.
     */
    protected final List<ENTITY> findAllSortBy(final String column, final int limit) {
        final String query = StringHelper.format("select {}, {} from {} where rownum <= {} order by {}", this.idColumnName,
                this.columnNamesString, this.tableName, limit, column);
        return getNamedParameterJdbcTemplate().query(query, this);
    }

    /**
     * <p>count.</p>
     *
     * @param whereQueryPart a {@link java.lang.String} object.
     * @param map            a {@link java.util.Map} object.
     * @return a long.
     */
    protected final long count(final String whereQueryPart, final Map<String, Object> map) {
        final String query = StringHelper.format("select count(1) from {} where {}", this.tableName, whereQueryPart);

        final Counter counter = new Counter();
        IterableUtils.pageUp(map, this.cellarConfiguration.getCellarDatabaseInClauseParams(), innerMap ->
                counter.increaseBy(getNamedParameterJdbcTemplate().queryForObject(query, innerMap, Long.class))
        );

        return counter.getVal();
    }

    /**
     * <p>count.</p>
     *
     * @param map a {@link java.util.Map} object.
     * @return a long.
     */
    protected final long count(final Map<String, Object> map) {
        final String query = StringHelper.format("select count(1) from {}", this.tableName);

        final Counter counter = new Counter();
        IterableUtils.pageUp(map, this.cellarConfiguration.getCellarDatabaseInClauseParams(), innerMap ->
                counter.increaseBy(getNamedParameterJdbcTemplate().queryForObject(query, innerMap, Long.class))
        );

        return counter.getVal();
    }

    /**
     * <p>fillMap.</p>
     *
     * @param daoObject a OBJ object.
     * @param map       a {@link java.util.Map} object.
     */
    protected abstract void fillMap(ENTITY daoObject, Map<String, Object> map);

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ENTITY> findAll() {
        final String query = StringHelper.format("select {}, {} from {}", this.idColumnName, this.columnNamesString, this.tableName);
        return getNamedParameterJdbcTemplate().query(query, this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final ENTITY getObject(final ID id) {
        final List<ENTITY> daoObjects = getNamedParameterJdbcTemplate().getJdbcOperations().query(this.getByIdQuery, this, id);
        if (daoObjects.isEmpty()) {
            return null;
        }
        if (daoObjects.size() > 1) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("More than one result in table {} for {} {}")
                    .withMessageArgs(this.tableName, this.idColumnName, id).build();
        }
        return daoObjects.get(0);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteObject(final ENTITY daoObject) {
        final String query = StringHelper.format("delete from {} where {} = {}", this.tableName, this.idColumnName, daoObject.getId());
        getNamedParameterJdbcTemplate().getJdbcOperations().update(query);
        this.deleted(daoObject);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void deleteObjects(final Collection<ENTITY> daoObjects) {
        final String daoObjectArgs = StringUtils.join(daoObjects, ",");
        final String query = StringHelper.format("delete from {} where {} in ({})", this.tableName, this.idColumnName, daoObjectArgs);
        getNamedParameterJdbcTemplate().getJdbcOperations().update(query);
        for (final ENTITY daoObject : daoObjects) {
            this.deleted(daoObject);
        }
    }

    @Override
    public int updateObjectsByNamedQueryWithNamedParams(final String queryName, final Map<String, Object> params) {
    	throw NotImplementedExceptionBuilder.get().withMessage("This method is not implemented for this DAO.").build();
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public ENTITY saveOrUpdateObject(final ENTITY daoObject) {
        if (daoObject.getId() == null) {
            return this.saveObject(daoObject);
        } else {
            return this.updateObject(daoObject);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final ENTITY upsertObject(final ENTITY daoObject, final String uniqueColumnName) {
        if (daoObject.getId() != null) {
            return this.updateObject(daoObject);
        }

        final Map<String, Object> map = new HashMap<String, Object>();
        this.fillMap(daoObject, map);

        final String query = this.prepareUpsertQuery(daoObject, uniqueColumnName);
        getNamedParameterJdbcTemplate().update(query, map);
        this.saved(daoObject);

        return daoObject;
    }

    /**
     * Prepare upsert query.
     *
     * @param daoObject        the dao object
     * @param uniqueColumnName the unique column name
     * @return the string
     */
    protected String prepareUpsertQuery(final ENTITY daoObject, final String uniqueColumnName) {
        // build the update part
        boolean firstDone = false;
        StringBuilder queryPartBuilder = new StringBuilder().append("UPDATE SET ");
        for (final String column : this.columnNames) {
            if (column.equals(uniqueColumnName)) {
                continue;
            }
            if (firstDone) {
                queryPartBuilder.append(", ");
            }
            queryPartBuilder.append("a.").append(column).append(" = :").append(column);
            firstDone = true;
        }
        final String updateQueryPart = queryPartBuilder.toString();

        // build the insert part
        firstDone = false;
        queryPartBuilder = new StringBuilder().append("INSERT (");
        insertColumnNames(this.columnNames, queryPartBuilder, "a");
        queryPartBuilder.append(")\n");
        queryPartBuilder.append("VALUES (");
        insertValues(this.columnNames, queryPartBuilder, null);
        queryPartBuilder.append(")");
        final String insertQueryPart = queryPartBuilder.toString();

        final String query = StringHelper.format(UPSERT_QUERY, this.tableName, uniqueColumnName, uniqueColumnName, uniqueColumnName,
                uniqueColumnName, updateQueryPart, insertQueryPart);

        return query;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ENTITY saveObject(final ENTITY daoObject) {
        // the order of iteration should obey the order or insertion
        final Map<String, Object> map = new LinkedHashMap<String, Object>();
        final long id = this.databaseIdService.getId(this.tableName);
        map.put(this.idColumnName, id);//LOB columns should be listed last in the insert or dml command.

        this.fillMap(daoObject, map);

        this.insert(map);
        daoObject.setId(id);

        this.saved(daoObject);
        return daoObject;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ENTITY updateObject(final ENTITY daoObject) {
        final Map<String, Object> map = new HashMap<String, Object>();
        this.fillMap(daoObject, map);

        map.put(this.idColumnName, daoObject.getId());
        this.update(map);

        this.saved(daoObject);
        return daoObject;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int deleteAll() {
        final int deletedEntries = getNamedParameterJdbcTemplate().getJdbcOperations().update("delete from " + this.tableName);
        this.deletedAll();
        return deletedEntries;
    }

    /**
     * <p>saved.</p>
     *
     * @param daoObject a OBJ object.
     */
    @Async
    protected void saved(final ENTITY daoObject) {
    }

    /**
     * <p>deleted.</p>
     *
     * @param daoObject a OBJ object.
     */
    @Async
    protected void deleted(final ENTITY daoObject) {
    }

    /**
     * <p>deletedAll.</p>
     */
    @Async
    protected void deletedAll() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<?> findByNamedQuery(final String queryName) throws DataAccessException {
        throw NotImplementedExceptionBuilder.get().withMessage("This method is not implemented for this DAO.").build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ENTITY findObjectByNamedQuery(final String queryName, final Object... values) throws DataAccessException {
        throw NotImplementedExceptionBuilder.get().withMessage("This method is not implemented for this DAO.").build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ENTITY findObjectByNamedQueryWithNamedParams(final String queryName, final Map<String, Object> params)
            throws DataAccessException {
        throw NotImplementedExceptionBuilder.get().withMessage("This method is not implemented for this DAO.").build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ENTITY> findObjectsByNamedQuery(final String queryName, final Object... values) throws DataAccessException {
        throw NotImplementedExceptionBuilder.get().withMessage("This method is not implemented for this DAO.").build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ENTITY> findObjectsByNamedQueryWithNamedParams(final String queryName, final Map<String, Object> params)
            throws DataAccessException {
        throw NotImplementedExceptionBuilder.get().withMessage("This method is not implemented for this DAO.").build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ENTITY> findObjectsByNamedQueryWithNamedParamsLimit(final String queryName, final Map<String, Object> params,
                                                                    final int startIndex, final int endIndex) throws DataAccessException {
        throw NotImplementedExceptionBuilder.get().withMessage("This method is not implemented for this DAO.").build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ENTITY> findPaginatedObjectsByNamedQuery(final String queryName, final int first, final int last, final Object... values)
            throws DataAccessException {
        throw NotImplementedExceptionBuilder.get().withMessage("This method is not implemented for this DAO.").build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ENTITY> findObjectsByNamedQueryWithNamedParams(final String queryName, final int first, final int last,
                                                               final Map<String, Object> params) throws DataAccessException {
        throw NotImplementedExceptionBuilder.get().withMessage("This method is not implemented for this DAO.").build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ENTITY> findObjectsByNamedQueryWithPagedParams(final String queryName, final String paramsName, final List<String> params,
                                                               final int paramsPageSize) {
        throw NotImplementedExceptionBuilder.get().withMessage("This method is not implemented for this DAO.").build();
    }

    /**
     * Update.
     *
     * @param map the map
     */
    private void update(final Map<String, Object> map) {
        final StringBuilder query = new StringBuilder().append("update ").append(this.tableName).append(" set ");

        boolean firstDone = false;
        for (final String key : map.keySet()) {
            if (firstDone) {
                query.append(", ");
            }
            query.append(key).append(" = :").append(key);
            firstDone = true;
        }
        query.append(" where ").append(this.idColumnName).append(" = :").append(this.idColumnName);
        getNamedParameterJdbcTemplate().update(query.toString(), map);
    }

    /**
     * Insert.
     *
     * @param map the map
     */
    private void insert(final Map<String, Object> map) {
        final StringBuilder query = new StringBuilder().append("insert into ").append(this.tableName).append(" (");

        insertColumnNames(map.keySet(), query, null);
        query.append(") values (");
        insertValues(map.keySet(), query, null);
        query.append(")");
        getNamedParameterJdbcTemplate().update(query.toString(), map);
    }

    /**
     * Insert column names.
     *
     * @param columns      the columns
     * @param query        the query
     * @param tableSynonim the table synonim
     */
    private static void insertColumnNames(final Collection<String> columns, final StringBuilder query, final String tableSynonim) {
        final String myTableSynonim = tableSynonim == null ? "" : tableSynonim + ".";

        boolean firstDone = false;
        for (final String column : columns) {
            if (firstDone) {
                query.append(", ");
            }
            firstDone = true;
            query.append(myTableSynonim);
            query.append(column);
        }
    }

    /**
     * Insert values.
     *
     * @param columns      the columns
     * @param query        the query
     * @param tableSynonim the table synonim
     */
    private static void insertValues(final Collection<String> columns, final StringBuilder query, final String tableSynonim) {
        final String myTableSynonim = tableSynonim == null ? "" : tableSynonim + ".";

        boolean firstDone = false;
        for (final String column : columns) {
            if (firstDone) {
                query.append(", ");
            }
            firstDone = true;
            query.append(":");
            query.append(myTableSynonim);
            query.append(column);
        }
    }

    /**
     * Find paging aware.
     *
     * @param query the query
     * @param map   the map
     * @return the collection
     */
    private final Collection<ENTITY> findPagingAware(final String query, final Map<String, Object> map) {
        final Collection<ENTITY> retCollection = new ArrayList<>();

        IterableUtils.pageUp(map, this.cellarConfiguration.getCellarDatabaseInClauseParams(), innerMap ->
                retCollection.addAll(getNamedParameterJdbcTemplate().query(query, innerMap, SpringBaseDao.this))
        );

        return retCollection;
    }

    /**
     *
     * @param query
     * @param args
     * @param mapper
     * @return
     */
    protected Collection<ENTITY> findPagingAware(final String query, final Map<String, Object> args, RowMapper<ENTITY> mapper) {
        final Collection<ENTITY> results = new ArrayList<>();

        IterableUtils.pageUp(args, this.cellarConfiguration.getCellarDatabaseInClauseParams(), innerMap ->
                results.addAll(getNamedParameterJdbcTemplate().query(query, innerMap, mapper))
        );
        return results;
    }

}
