/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar
 *        FILE : CoreErrorCode.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 13-03-2012
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar;

import eu.europa.ec.opoce.cellar.MessageCode.TYPE;

/**
 * <class_description> Core error codes.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 13-03-2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public final class CoreErrors extends CommonErrors {

    /* 000 Codes */
    public static final MessageCode E_000 = new MessageCode("E-000", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode E_001 = new MessageCode("E-001");
    public static final MessageCode E_002 = new MessageCode("E-002");
    public static final MessageCode E_003 = new MessageCode("E-003");
    public static final MessageCode E_006 = new MessageCode("E-006");
    public static final MessageCode E_007 = new MessageCode("E-007");
    public static final MessageCode E_010 = new MessageCode("E-010");
    public static final MessageCode E_011 = new MessageCode("E-011");
    public static final MessageCode E_012 = new MessageCode("E-012", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode E_013 = new MessageCode("E-013", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode E_014 = new MessageCode("E-014");
    public static final MessageCode E_015 = new MessageCode("E-015");
    public static final MessageCode E_017 = new MessageCode("E-017");
    public static final MessageCode E_018 = new MessageCode("E-018");
    public static final MessageCode E_021 = new MessageCode("E-021", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode E_022 = new MessageCode("E-022", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode E_023 = new MessageCode("E-023", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode E_024 = new MessageCode("E-024", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode E_025 = new MessageCode("E-025", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode E_026 = new MessageCode("E-026", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode E_027 = new MessageCode("E-027", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode E_031 = new MessageCode("E-031", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode E_032 = new MessageCode("E-032", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode E_033 = new MessageCode("E-033");
    public static final MessageCode E_034 = new MessageCode("E-034");
    public static final MessageCode E_035 = new MessageCode("E-035");
    public static final MessageCode E_036 = new MessageCode("E-036");
    public static final MessageCode E_037 = new MessageCode("E-037");
    public static final MessageCode E_040 = new MessageCode("E-040");
    public static final MessageCode E_041 = new MessageCode("E-041");
    public static final MessageCode E_042 = new MessageCode("E-042");
    public static final MessageCode E_050 = new MessageCode("E-050");
    public static final MessageCode E_051 = new MessageCode("E-051");
    public static final MessageCode E_052 = new MessageCode("E-052");
    public static final MessageCode E_053 = new MessageCode("E-053");
    public static final MessageCode E_054 = new MessageCode("E-054");
    public static final MessageCode E_061 = new MessageCode("E-061");
    public static final MessageCode E_070 = new MessageCode("E-070");
    public static final MessageCode E_071 = new MessageCode("E-071");
    public static final MessageCode E_072 = new MessageCode("E-072");
    public static final MessageCode E_073 = new MessageCode("E-073");
    public static final MessageCode E_074 = new MessageCode("E-074");
    /* 100 Codes */
    public static final MessageCode E_100 = new MessageCode("E-100", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode E_101 = new MessageCode("E-101");
    public static final MessageCode E_102 = new MessageCode("E-102");
    public static final MessageCode E_104 = new MessageCode("E-104");
    public static final MessageCode E_105 = new MessageCode("E-105");
    public static final MessageCode E_106 = new MessageCode("E-106");
    public static final MessageCode E_107 = new MessageCode("E-107");
    public static final MessageCode E_108 = new MessageCode("E-108", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode E_109 = new MessageCode("E-109");
    public static final MessageCode E_110 = new MessageCode("E-110");
    public static final MessageCode E_111 = new MessageCode("E-111");
    public static final MessageCode E_112 = new MessageCode("E-112", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode E_113 = new MessageCode("E-113");
    public static final MessageCode E_114 = new MessageCode("E-114");
    public static final MessageCode E_115 = new MessageCode("E-115");
    public static final MessageCode E_116 = new MessageCode("E-116");
    public static final MessageCode E_117 = new MessageCode("E-117");
    public static final MessageCode E_170 = new MessageCode("E-170", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode E_171 = new MessageCode("E-171", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode E_172 = new MessageCode("E-172", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode E_180 = new MessageCode("E-180");
    /* 200 Codes */
    public static final MessageCode E_250 = new MessageCode("E-250");
    /* 300 Codes */
    public static final MessageCode E_391 = new MessageCode("E-391");
    /* 500 Codes */
    public static final MessageCode E_500 = new MessageCode("E-500");

    private CoreErrors() {
    }
}
