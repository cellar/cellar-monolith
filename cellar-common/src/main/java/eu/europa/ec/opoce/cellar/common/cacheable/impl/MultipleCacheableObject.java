/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice
 *             FILE : MultipleCacheableObject.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 29, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.cacheable.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import eu.europa.ec.opoce.cellar.common.cacheable.IMultipleCacheableObject;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 29, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@SuppressWarnings("serial")
public abstract class MultipleCacheableObject<K, V> extends HashMap<K, V> implements IMultipleCacheableObject<K, V> {

    public Collection<V> getMultiple(final Collection<K> keys, final boolean checkCache) {
        final List<K> uncachedKeys = new ArrayList<K>();
        final Collection<V> values = new ArrayList<V>();
        for (K key : keys) {
            if (checkCache && this.containsKey(key)) {
                values.add(super.get(key));
            } else {
                uncachedKeys.add(key);
            }
        }

        final Collection<V> uncachedValues = this.retrieveMultiple(uncachedKeys);
        if (uncachedKeys.size() != uncachedValues.size()) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage(
                            "Cannot retrieve the multiple batched values as the number of keys ({}) is different from the number of retrieved values ({}).")
                    .withMessageArgs(uncachedKeys.size(), uncachedValues.size()).build();
        }

        final Iterator<V> currValuesIterator = uncachedValues.iterator();
        for (K keyNotInCache : uncachedKeys) {
            this.put(keyNotInCache, currValuesIterator.next());
        }
        values.addAll(uncachedValues);

        return values;
    }

    abstract Collection<V> retrieveMultiple(final  Collection<K> key);

}
