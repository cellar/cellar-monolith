/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.audit
 *        FILE : AuditTrailEventProcess.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 01-10-2015
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.audit;

import java.util.ArrayList;
import java.util.List;

/**
 * <class_description> Available processes for Audit Trail.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 01-10-2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public enum AuditTrailEventProcess {

    Ingestion, //
    Indexing, //
    Dissemination, //
    Export,
    Nal, //
    Ontology, //
    Embargo,
    Administration, //
    Aligning,
    Cleaning,
    Cleaning_Scheduling,
    Aligning_Scheduling,
    Embedded_Notice_Synchronization,
    Virtuoso_Backup,
    Undefined;

    static public List<String> getValueNames() {
        final List<String> valueNames = new ArrayList<String>();
        final AuditTrailEventProcess[] processes = AuditTrailEventProcess.values();
        for (final AuditTrailEventProcess process : processes) {
            valueNames.add(process.toString());
        }
        return valueNames;
    }

    public String getName() {
        return toString();
    }
}
