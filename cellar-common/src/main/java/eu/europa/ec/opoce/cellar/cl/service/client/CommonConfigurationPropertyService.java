package eu.europa.ec.opoce.cellar.cl.service.client;

/**
 * Common interface for accessing configuration properties in persistence layer.
 * 
 * @author omeurice
 * 
 */
public interface CommonConfigurationPropertyService {

    /**
     * Return property value identified by the given key, or null if not found.
     * 
     * @param key
     *            the key identifier of the property
     * 
     * @return the property value, or null if not found
     */
    String getPropertyValue(String key);
}
