/**
 * 
 */
package eu.europa.ec.opoce.cellar.domain.content.mets;

import eu.europa.ec.opoce.cellar.common.strategy.IStrategy;

/**
 * This is the interface for any class that will manipulate a DigitalObject.
 * 
 * @author dsavares
 * @deprecated From Arnaud J. What's the point of that ? Weak abstraction.
 * 
 */
@Deprecated
public interface DigitalObjectStrategy extends IStrategy<DigitalObject> {
}
