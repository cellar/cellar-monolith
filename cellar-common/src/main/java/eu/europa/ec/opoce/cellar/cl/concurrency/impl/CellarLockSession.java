/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.concurrency.impl
 *             FILE : CellarLockSession2.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 30, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.concurrency.impl;

import eu.europa.ec.opoce.cellar.cl.concurrency.Concurrent.Locker;
import eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLock;
import eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLock.MODE;
import eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLockSession;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * <class_description> Cellar lock session implementation.
 * <br/><br/>
 * ON : Apr 30, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class CellarLockSession implements ICellarLockSession {

    /**
     * The read locks concerned by the session.
     */
    @JsonIgnore
    private final List<ICellarLock> readCellarLocksPath;

    /**
     * The write locks concerned by the session.
     */
    @JsonIgnore
    private final List<ICellarLock> writeCellarLocksPath;

    /**
     * True if the processing related to this session is started.
     */
    private boolean working;

    /**
     * The process that fired the lock.
     */
    private final Locker locker;

    /**
     * The thread ID of the process that fired the lock.
     */
    private final String lockerId;

    /**
     * The lock acquisition time of the processing related to this session
     */
    private long lockAcquisitionTime;

    public static ICellarLockSession create(final Locker locker) {
        return new CellarLockSession(locker);
    }

    /**
     * The locker IDs of the threads that were identified to already possess resource-locks
     * needed by the thread owning this cellar lock session during the resource acquisition phase.
     */
    private final Set<String> conflictedLockerIds;
    
    /**
     * Constructor.
     */
    @JsonCreator
    private CellarLockSession(@JsonProperty("locker") final Locker locker) {
        this.readCellarLocksPath = new LinkedList<ICellarLock>();
        this.writeCellarLocksPath = new LinkedList<ICellarLock>();
        this.conflictedLockerIds = new HashSet<>();
        this.working = false;
        this.locker = locker;
        this.lockerId = Thread.currentThread().getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isWorking() {
        return this.working;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setWorking(final boolean working) {
        this.working = working;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addCellarLock(final ICellarLock lock, final MODE mode) {
        if (lock.getLockingMode(mode) == MODE.READ) {
            this.readCellarLocksPath.add(lock);
        } else {
            this.writeCellarLocksPath.add(lock);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ICellarLock> getReadCellarLocksPath() {
        return this.readCellarLocksPath;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ICellarLock> getWriteCellarLocksPath() {
        return this.writeCellarLocksPath;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Locker getLocker() {
        return this.locker;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLockerId() {
        return this.lockerId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<String> getConflictedLockerIds() {
		return conflictedLockerIds;
	}

    /**
     * {@inheritDoc}
     */
    public long getLockAcquisitionTime() {
        return this.lockAcquisitionTime;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setLockAcquisitionTime(final long lockAcquisitionTime) {
        this.lockAcquisitionTime = lockAcquisitionTime;
    }

	/**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder("{");
        builder.append("id=" + this.getLockerId());
        builder.append(", locker=" + this.getLocker());
        builder.append("}");
        return builder.toString();
    }

}
