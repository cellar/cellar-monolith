package eu.europa.ec.opoce.cellar.cl.dao;

import eu.europa.ec.opoce.cellar.exception.BuildableException;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

public class DaoException extends CellarException {

    private static final long serialVersionUID = -2194368168102966912L;

    /**
     * Constructs a new exception with its associated builder.
     * 
     * @param builder the builder to use for building the exception
     */
    public DaoException(final ExceptionBuilder<? extends BuildableException> builder) {
        super(builder);
    }

}
