/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.exception
 *        FILE : S3OperationException.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 30-01-2012
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.exception;

/**
 * This is a {@link S3OperationException} buildable by an {@link ExceptionBuilder}.</br>
 * </br>
 * ON : 30-01-2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class S3OperationException extends HttpStatusAwareException {

    private static final long serialVersionUID = -4793233835735331116L;

    /**
     * Constructs a new exception with its associated builder.
     *
     * @param builder the builder to use for building the exception
     */
    public S3OperationException(final HttpStatusAwareExceptionBuilder<? extends BuildableException> builder) {
        super(builder);
    }

    public S3OperationException(final ExceptionBuilder<? extends BuildableException> builder) {
        super(builder);
    }

}
