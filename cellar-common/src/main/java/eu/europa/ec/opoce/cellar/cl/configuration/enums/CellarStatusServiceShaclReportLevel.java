package eu.europa.ec.opoce.cellar.cl.configuration.enums;

import eu.europa.ec.opoce.cellar.common.util.EnumUtils;

/**
 * Contains the acceptable values for the 'cellar.status.service.shacl.report.level'
 * configuration parameter. In case of an invalid value, silently defaults to NO.
 * @author EUROPEAN DYNAMICS S.A.
 */
public enum CellarStatusServiceShaclReportLevel {
    
    /**
     *
     */
    WARNING,
    /**
     *
     */
    ERROR,
    /**
     *
     */
    NO;
    
    public static CellarStatusServiceShaclReportLevel resolve(final String key) {
        return EnumUtils.resolve(CellarStatusServiceShaclReportLevel.class, key, NO);
    }
}
