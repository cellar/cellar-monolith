package eu.europa.ec.opoce.cellar.cl.domain.validator;

/**
 * 
 * @author dcraeye
 *
 */
public interface ValidationStrategy {

    /**
     * Validates the given object.
     * 
     * @param validationDetails
     *            the {@link ValidationDetails} to validate.
    
     * @return a {@link ValidationResult}.
     */
    ValidationResult validate(final ValidationDetails validationDetails);
}
