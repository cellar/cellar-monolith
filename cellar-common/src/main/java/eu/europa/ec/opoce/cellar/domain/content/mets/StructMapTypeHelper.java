/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.domain.content.mets
 *             FILE : StructMapTypeHelper.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 25-06-2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.domain.content.mets;

/**
 * <class_description> Helper for evaluating the type of a {@link StructMap}.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 25-06-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public final class StructMapTypeHelper {

    public static boolean isTreeType(final StructMapType operationType) {
        return StructMapType.Tree.equals(operationType);
    }

    public static boolean isTreeAllType(final StructMap structMap) {
        return isTreeType(structMap.getMetadataOperationType()) && isTreeType(structMap.getContentOperationType());
    }

    public static boolean isTreeContentType(final StructMap structMap) {
        return structMap.getMetadataOperationType() == null && isTreeType(structMap.getContentOperationType());
    }

    public static boolean isTreeMetadataType(final StructMap structMap) {
        return structMap.getContentOperationType() == null && isTreeType(structMap.getMetadataOperationType());
    }

    public static boolean isNodeType(final StructMapType operationType) {
        return StructMapType.Node.equals(operationType);
    }

    public static boolean isNodeAllType(final StructMap structMap) {
        return isNodeType(structMap.getMetadataOperationType()) && isNodeType(structMap.getContentOperationType());
    }

    public static boolean isNodeContentType(final StructMap structMap) {
        return structMap.getMetadataOperationType() == null && isNodeType(structMap.getContentOperationType());
    }

    public static boolean isNodeMetadataType(final StructMap structMap) {
        return structMap.getContentOperationType() == null && isNodeType(structMap.getMetadataOperationType());
    }

}
