/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.configuration.enums
 *        FILE : CellarServiceSipQueueManagerFileAttributeDateType.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 02-06-2022
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2022 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.configuration.enums;

import eu.europa.ec.opoce.cellar.common.util.EnumUtils;

/**
 * Contains the acceptable values for the 'cellar.service.sipQueueManager.fileAttribute.dateType'
 * configuration parameter. In case of an invalid value, silently defaults to CHANGE.
 * @author EUROPEAN DYNAMICS S.A.
 */
public enum CellarServiceSipQueueManagerFileAttributeDateType {

	/**
	 * 
	 */
	ACCESS("lastAccessTime"),
	/**
	 * 
	 */
	MODIFY("lastModifiedTime"),
	/**
	 * 
	 */
	CHANGE("unix:ctime");
	
	
	private String fileAttribute;

	
	private CellarServiceSipQueueManagerFileAttributeDateType(String fileAttribute) {
		this.fileAttribute = fileAttribute;
	}
	
	public static String resolve(final String key) {
		CellarServiceSipQueueManagerFileAttributeDateType value = 
				EnumUtils.resolve(CellarServiceSipQueueManagerFileAttributeDateType.class, key,
                CHANGE);
		return value.fileAttribute;
    }
	
}
