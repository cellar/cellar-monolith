package eu.europa.ec.opoce.cellar.common.util;

import com.google.common.base.Splitter;
import com.google.common.io.ByteStreams;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.function.Predicate;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * IO Zip Utilities.
 *
 * @author dsavares
 */
public final class ZipUtils {

    /**
     * Zip the files contained in the <i>filesToZipFolder</i> int the archive <i>zipFile</i>.
     *
     * @param zipFile          the absrtact path name of the zip file.
     * @param filesToZipFolder the absrtact name of the folder cointaining the file to zip.
     * @throws IOException if an I/O error occurs.
     */
    public static void zipFiles(File zipFile, File filesToZipFolder) throws IOException {
        try (FileOutputStream dest = new FileOutputStream(zipFile);
             ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(dest))) {

            File[] fileList = filesToZipFolder.listFiles();
            if (fileList != null) {
                for (File fileToZip : fileList) {
                    ZipEntry entry = new ZipEntry(fileToZip.getName());
                    out.putNextEntry(entry);
                    try (FileInputStream in = new FileInputStream(fileToZip)) {
                        IOUtils.copy(in, out);
                    }
                }
            }
        }
    }

    /**
     * Unzip a compressed archive file.
     *
     * @param destinationFolder the directory where to unzip the files stored into the archive
     * @param file              the full path of the compressed archive file
     * @throws IOException if an I/O error occurs.
     */
    public static void unzipFiles(final File destinationFolder, final File file) throws IOException {
        try (final ZipFile zipFile = new ZipFile(file)) {
            final Enumeration<? extends ZipEntry> entries = zipFile.entries();
            while (entries.hasMoreElements()) {
                final ZipEntry entry = entries.nextElement();
                final File entryDestination = new File(destinationFolder, validateFilenameInDir(entry.getName()));
                if (entry.isDirectory()) {
                    entryDestination.mkdirs();
                } else {
                    entryDestination.getParentFile().mkdirs();
                    try (
                            final InputStream in = zipFile.getInputStream(entry);
                            final OutputStream out = new FileOutputStream(entryDestination)
                    ) {
                        IOUtils.copy(in, out);
                    }
                }
            }
        }
    }

    /**
     * Extract a specific entry from a compressed stream.
     *
     * @param zipInputStream    the input stream hosting the zip bytes
     * @param entryName         the name of the entry to extract
     * @param entryOutputStream the output stream meant to host the bytes of the found entry
     * @return true if an entry has been found, false otherwise
     * @throws IOException in case something goes wrong during the extraction
     */
    public static boolean extractEntry(final InputStream zipInputStream, final String entryName,
                                       final OutputStream entryOutputStream) throws IOException {
        if (zipInputStream == null || StringUtils.isBlank(entryName) || entryOutputStream == null) {
            return false;
        }

        ZipEntry currEntry;
        try (final ZipInputStream zis = new ZipInputStream(zipInputStream)) {
            while ((currEntry = zis.getNextEntry()) != null) {
                if (entryName.equals(currEntry.getName())) {
                    ByteStreams.copy(zis, entryOutputStream);
                    break;
                }
            }
        }

        return currEntry != null;
    }

    /**
     * Gets from {@code zipPath} the entries satisfying the provided {@code filter} and uncompress them to
     * {@code outputDir} without uncompressing the whole zip.<br>
     * <br>
     * Examples:<br>
     * <pre> {@code
     * // Extracts files ending in .txt from the zip archive
     * uncompressFilteredEntries("myZip.zip", "/outputDir", zipEntry -> zipEntry.getName().endsWith(".txt"));
     *
     * // Extracts files last written in or after year 2015
     * uncompressFilteredEntries("myZip.zip", "/outputDir", zipEntry -> {
     *      Instant lastModified = zipEntry.getLastModifiedTime().toInstant();
     *      int yearModified = lastModified.get(ChronoField.YEAR);
     *      return yearModified >= 2015;
     * });
     * } </pre>
     */
    public static void unzipFilteredEntries(final String zipPath, final String outputDir, final Predicate<ZipEntry> filter)
            throws IOException {
        new ZipFilteredReader(zipPath, outputDir).filteredExpandZipFile(filter);
    }

    /**
     * Validate {@code filename} in a directory. Also validates the {@code filename} itself with {@link ZipUtils#validateStringFilename(String)}
     * <p>
     * This makes sure that the {@code filename} exists in the intended directory,
     * avoiding any path manipulation that may lead to arbitrarily overwrite files on the system.
     * </p><br>
     *
     * <b>Positive Examples:</b>
     * <pre>
     * <b>Path to Verify:</b>        <b>Path of filename to check:</b>
     * /home/cellar/    --&gt;   /home/cellar/create_package.mets.xml
     * /home/cellar/    --&gt;   /home/cellar/sample_ProCat_Madagaskar_32006D0241.rdf
     * /home/cellar/    --&gt;   /home/cellar/sample_ProCat_Madagaskar_32006D0241.techMD_dan_formex.rdf
     * </pre>
     *
     * <b>Negative Examples:</b>
     * <pre>
     * <b>Path to Verify:</b>        <b>Path of filename to check:</b>
     * /home/cellar/    --&gt;   /home/testtool/create_package.mets.xml
     * /home/cellar/    --&gt;   /etc/create_package.mets.xml
     * /home/cellar/    --&gt;   /sys/kernel/vmcoreinfo
     * </pre>
     *
     * @param filename the file name to validate
     * @return the {@code filename} if it is successfully validated, otherwise throw {@link IllegalStateException}
     */
    public static String validateFilenameInDir(String filename) {
        String absolutePathToCheck = new File(filename).getAbsolutePath();
        String absolutePathToVerify = new File("").getAbsolutePath();

        if(validateStringFilename(filename) && absolutePathToCheck.startsWith(absolutePathToVerify)) {
            return filename;
        } else {
            throw new IllegalStateException("This file is outside the intended extraction directory, " +
                    "or its name does not meet the requirements.");
        }
    }

    /**
     * Validate the String as a file name.
     *
     * @param filename the file name to validate
     * @return true if {@code filename} is valid
     */
    public static boolean validateStringFilename(String filename) {
        Paths.get(filename);
        return true;
    }

    /**
     * ZipFilteredReader allows filtering one or more matching
     * files from a ZipInputStream. Instead of expanding the whole archive
     * this uses the Function interface to only expand matching files.
     * <p>
     * Files are output to the OUTPUT_DIR directory.
     */
    private static class ZipFilteredReader {

        private final Path zipLocation;
        private final Path outputDirectory;

        /**
         * Constructs the filtered zip reader passing in the zip file to
         * be expanded by filter and the output directory
         *
         * @param zipLocation the zip file
         * @param outputDir   the output directory
         */
        ZipFilteredReader(String zipLocation, String outputDir) {
            this.zipLocation = Paths.get(zipLocation);
            this.outputDirectory = Paths.get(outputDir);
        }

        /**
         * This method iterates through all entries in the zip archive. Each
         * entry is checked against the predicate (filter) that is passed to
         * the method. If the filter returns true, the entry is expanded,
         * otherwise it is ignored.
         *
         * @param filter the predicate used to compare each entry against
         */
        private void filteredExpandZipFile(Predicate<ZipEntry> filter) throws IOException {
            try (ZipInputStream stream = new ZipInputStream(new FileInputStream(zipLocation.toFile()))) {

                // we now iterate through all files in the archive testing them
                // again the predicate filter that we passed in. Only items that
                // match the filter are expanded.
                ZipEntry entry;
                while ((entry = stream.getNextEntry()) != null) {
                    if (!entry.isDirectory() && filter.test(entry)) {
                        extractFileFromArchive(stream, entry.getName());
                    }
                }
            }
        }

        /**
         * We only get here when we the stream is located on a zip entry.
         * Now we can read the file data from the stream for this current
         * ZipEntry. Just like a normal input stream we continue reading
         * until read() returns 0 or less.
         */
        private void extractFileFromArchive(ZipInputStream stream, String outputName) throws IOException {
            final Iterator<String> outputNameTokens = Splitter.on(File.separator).split(outputName).iterator();
            Path outpath = outputDirectory;
            while (outputNameTokens.hasNext()) {
                outpath = outpath.resolve(outputNameTokens.next());
                // if it's a folder, just create it
                if (outputNameTokens.hasNext()) {
                    outpath.toFile().mkdir();
                }
                // if it's a file, extract it
                else {
                    Files.copy(stream, outpath);
                }
            }
        }
    }
}
