/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.batchJob
 *             FILE : BatchJob.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 11, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.sparql;

import javax.persistence.*;

/**
 * <class_description> Uri Templates
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 11, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Entity
@Table(name = "URI_TEMPLATES")
@NamedQueries({
        @NamedQuery(name = "findAllOrderedBySequence", query = "select ut from UriTemplates ut order by ut.sequence asc"),
        @NamedQuery(name = "findUriTemplatesBySequence", query = "select ut from UriTemplates ut where ut.sequence >= ?0 order by ut.sequence asc")})
public class UriTemplates {

    /**
     * Internal id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "uri_templates_gen")
    @SequenceGenerator(name = "uri_templates_gen", sequenceName = "URI_TEMPLATES_SEQ", allocationSize = 1)
    @Column(name = "ID")
    private Long id;

    /**
     * The uri pattern.
     */
    @Column(name = "URI_PATTERN", nullable = false)
    private String uriPattern;

    /**
     * The sparql.
     */
    @Column(name = "SPARQL", nullable = false)
    private String sparql;

    /**
     * The xslt.
     */
    @Column(name = "XSLT")
    private String xslt;

    /**
     * The sequence.
     */
    @Column(name = "SEQUENCE")
    private Long sequence;

    /**
     * Default constructor.
     */
    public UriTemplates() {

    }

    /**
     * Default constructor.
     */
    public UriTemplates(final String uriPattern, final String sparql, final String xslt, final Long sequence) {
        super();
        this.uriPattern = uriPattern;
        this.sparql = sparql;
        this.xslt = xslt;
        this.sequence = sequence;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * @param id the id to set
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * @return the uriPattern
     */
    public String getUriPattern() {
        return this.uriPattern;
    }

    /**
     * @param uriPattern the uriPattern to set
     */
    public void setUriPattern(final String uriPattern) {
        this.uriPattern = uriPattern;
    }

    /**
     * @return the sparql
     */
    public String getSparql() {
        return this.sparql;
    }

    /**
     * @param sparql the sparql to set
     */
    public void setSparql(final String sparql) {
        this.sparql = sparql;
    }

    /**
     * @return the xslt
     */
    public String getXslt() {
        return this.xslt;
    }

    /**
     * @param xslt the xslt to set
     */
    public void setXslt(final String xslt) {
        this.xslt = xslt;
    }

    /**
     * @return the sequence
     */
    public Long getSequence() {
        return this.sequence;
    }

    /**
     * @param sequence the sequence to set
     */
    public void setSequence(final Long sequence) {
        this.sequence = sequence;
    }

}
