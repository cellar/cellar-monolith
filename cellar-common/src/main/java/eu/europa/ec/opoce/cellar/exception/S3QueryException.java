/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.exception
 *        FILE : S3QueryException.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 30-01-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.exception;

/**
 * This is a {@link S3QueryException} buildable by an {@link ExceptionBuilder}.</br>
 * </br> 
 * ON : 30-01-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public class S3QueryException extends CellarException {

    private static final long serialVersionUID = 4146832603629786531L;

    /**
     * Constructs a new exception with its associated builder.
     * 
     * @param builder the builder to use for building the exception
     */
    public S3QueryException(final ExceptionBuilder<? extends BuildableException> builder) {
        super(builder);
    }
}
