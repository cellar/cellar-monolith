/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.content
 *        FILE : SIPResourceDependencies
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 21-11-2021
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2021 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.content;

import java.util.Date;
import java.util.Set;

import eu.europa.ec.opoce.cellar.domain.content.mets.OperationType;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;

/**
 * Represents the results of the SIP dependency checking process
 * for a given SIP package.
 * @author EUROPEAN DYNAMICS S.A.
 */
public class SIPResourceDependencies {
	
	private String sipName;
	
	private TYPE sipType;
	
	private Date sipDetectionDate;
	
	private OperationType operationType;
	
	private Long packageHistoryId;
	
	private Set<String> rootAndChildren;
	
	private Set<String> directRelations;
	
	
	public SIPResourceDependencies() {
	}
	
	public SIPResourceDependencies(String sipName, TYPE sipType, Date sipDetectionDate, OperationType operationType,
			Long packageHistoryId, Set<String> rootAndChildren, Set<String> directRelations) {
		this.sipName = sipName;
		this.sipType = sipType;
		this.sipDetectionDate = sipDetectionDate;
		this.operationType = operationType;
		this.packageHistoryId = packageHistoryId;
		this.rootAndChildren = rootAndChildren;
		this.directRelations = directRelations;
	}
	
	
	public String getSipName() {
		return sipName;
	}
	
	public void setSipName(String sipName) {
		this.sipName = sipName;
	}
	
	public TYPE getSipType() {
		return sipType;
	}
	
	public void setSipType(TYPE sipType) {
		this.sipType = sipType;
	}
	
	public Date getSipDetectionDate() {
		return sipDetectionDate;
	}
	
	public void setSipDetectionDate(Date sipDetectionDate) {
		this.sipDetectionDate = sipDetectionDate;
	}
	
	public OperationType getOperationType() {
		return operationType;
	}
	
	public void setOperationType(OperationType operationType) {
		this.operationType = operationType;
	}
	
	public Set<String> getRootAndChildren() {
		return rootAndChildren;
	}
	
	public void setRootAndChildren(Set<String> rootAndChildren) {
		this.rootAndChildren = rootAndChildren;
	}
	
	public Set<String> getDirectRelations() {
		return directRelations;
	}
	
	public void setDirectRelations(Set<String> directRelations) {
		this.directRelations = directRelations;
	}
	
	public Long getPackageHistoryId() {
		return packageHistoryId;
	}
	
	public void setPackageHistoryId(Long packageHistoryId) {
		this.packageHistoryId = packageHistoryId;
	}
}
