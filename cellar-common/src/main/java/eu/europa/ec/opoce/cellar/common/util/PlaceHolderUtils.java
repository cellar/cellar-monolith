/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.util
 *             FILE : SparqlUtils.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Aug 8, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.util;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.common.Placeholder;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Aug 8, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class PlaceHolderUtils {

    public static String replacePlaceholders(final String str, final List<Placeholder> placeholders, final String replaceStr) {
        String constructedStr = str;

        for (Placeholder placeholder : placeholders) {
            constructedStr = constructedStr.replace(placeholder.getValue(), replaceStr);
        }

        return constructedStr;
    }

    /**
     * Replaces the placeholders with the formatted dates.
     * @param str string concerned by the placeholders
     * @param placeholders placeholders to replace
     * @param date date to format
     * @return the string with the placeholders replaced
     */
    public static String replacePlaceholdersByDate(final String str, final List<Placeholder> placeholders, final Date date) {
        String constructedStr = str;
        String content = null;
        String dateFormat = null;
        String formattedDate = null;
        int startDateFormat, endDateFormat;
        SimpleDateFormat dateFormatter = null;

        for (Placeholder placeholder : placeholders) {
            content = placeholder.getValue();
            // extract the date format to use
            startDateFormat = content.indexOf("\"");
            endDateFormat = content.lastIndexOf("\"");
            if (startDateFormat != -1 && endDateFormat != -1 && startDateFormat < endDateFormat) {
                dateFormat = content.substring(startDateFormat + 1, endDateFormat);
            } else {
                dateFormat = null; // default date format
            }

            if (StringUtils.isBlank(dateFormat)) { // use the default date format
                formattedDate = TimeUtils.formatTimestampForSparqlQuery(date);
            } else { // use the specified date format
                try {
                    dateFormatter = new SimpleDateFormat(dateFormat);
                    dateFormatter.setLenient(false);
                    formattedDate = dateFormatter.format(date);
                } catch (IllegalArgumentException e) {
                    throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.CELLAR_INTERNAL_ERROR)
                            .withMessage("The placehoder '{}' in '{}' is invalid").withMessageArgs(content, str).withCause(e).build();
                }
            }
            constructedStr = constructedStr.replace(placeholder.getValue(), formattedDate);
        }

        return constructedStr;
    }

    /**
     * Extracts the placeholders from the source.
     * @param str source
     * @param pattern the pattern used to find the placeholders
     * @return the placeholders
     */
    public static List<Placeholder> getPlaceholders(final String str, final Pattern pattern) {
        final Matcher matcher = pattern.matcher(str);
        final LinkedList<Placeholder> placeholders = new LinkedList<Placeholder>();

        // extract the placeholders
        while (matcher.find()) {
            placeholders.add(new Placeholder(matcher.start(), matcher.end(), matcher.group()));
        }

        return placeholders;
    }
}
