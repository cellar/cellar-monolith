/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.util
 *             FILE : TimeUtils.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Nov 12, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

/**
 * <class_description> Services class that helps the time parse/format.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Nov 9, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public final class TimeUtils {

    private final static String EMPTY_STRING = "";
    private final static String SEPARATOR = "#";

    public final static String DEFAULT_DATE_FORMAT = "dd/MM/yyyy";
    public final static String DEFAULT_DATE_REGEX = "(0[1-9]|[1-9]|[12][0-9]|3[01])/(0[1-9]|1[012]|[1-9])/(19|20)\\d{2}";
    public final static String DEFAULT_TIME_FORMAT = "HH:mm";
    public final static String DEFAULT_TIME_REGEX = "([01]?[0-9]|2[0-3]):[0-5][0-9]";

    public static final String SPARQL_QUERY_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.S'Z'";
    public final static String yyyyMMdd_DATE_FORMAT = "yyyyMMdd";

    public static final String ISO_8601_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.S'Z'";
    public static final String ISO_8601_DATE_ALTERNATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";

    public static final String HTTP_1_1_DATE_FORMAT = "EEE, dd MMM yyyy HH:mm:ss z";

    /**
     * Constructor.
     */
    private TimeUtils() {

    }

    private static final ThreadLocal<DateFormat> DEFAULT_DATE_FORMAT_THREAD_LOCAL = new ThreadLocal<DateFormat>() {

        @Override
        protected DateFormat initialValue() {
            final SimpleDateFormat dateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
            dateFormat.setLenient(false);
            return dateFormat;
        };
    };

    private static final ThreadLocal<DateFormat> DEFAULT_DATE_TIME_FORMAT_THREAD_LOCAL = new ThreadLocal<DateFormat>() {

        @Override
        protected DateFormat initialValue() {
            final SimpleDateFormat dateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT + SEPARATOR + DEFAULT_TIME_FORMAT);
            dateFormat.setLenient(false);
            return dateFormat;
        };
    };

    private static final ThreadLocal<DateFormat> SPARQL_QUERY_DATE_FORMAT_THREAD_LOCAL = new ThreadLocal<DateFormat>() {

        @Override
        protected DateFormat initialValue() {
            final SimpleDateFormat dateFormat = new SimpleDateFormat(SPARQL_QUERY_DATE_FORMAT);
            dateFormat.setLenient(false);
            return dateFormat;
        };
    };

    private static final ThreadLocal<DateFormat> yyyyMMdd_DATE_FORMAT_THREAD_LOCAL = new ThreadLocal<DateFormat>() {

        @Override
        protected DateFormat initialValue() {
            final SimpleDateFormat dateFormat = new SimpleDateFormat(yyyyMMdd_DATE_FORMAT);
            dateFormat.setLenient(false);
            return dateFormat;
        };
    };

    private static final ThreadLocal<DateFormat> HTTP_1_1_DATE_FORMAT_THREAD_LOCAL = new ThreadLocal<DateFormat>() {

        @Override
        protected DateFormat initialValue() {
            final SimpleDateFormat dateFormat = new SimpleDateFormat(HTTP_1_1_DATE_FORMAT);
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            return dateFormat;
        };
    };

    private static final ThreadLocal<DateFormat> ISO_8601_DATE_ALTERNATE_FORMAT_THREAD_LOCAL = new ThreadLocal<DateFormat>() {

        @Override
        protected DateFormat initialValue() {
            final SimpleDateFormat dateFormat = new SimpleDateFormat(ISO_8601_DATE_ALTERNATE_FORMAT);
            dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
            return dateFormat;
        };
    };

    /**
     * Formats the provided timestamp for display in the format yyyy-MM-dd'T'HH:mm:ss.S'Z'.
     * 
     * @param date
     *            the date to format
     * @return the formatted date
     */
    public static String formatTimestampForSparqlQuery(final Date date) {
        if (date == null) {
            return EMPTY_STRING;
        }
        return SPARQL_QUERY_DATE_FORMAT_THREAD_LOCAL.get().format(date);
    }

    /**
     * Formats the provided timestamp for display in the format yyyyMMdd.
     * 
     * @param date
     *            the date to format
     * @return the formatted date
     */
    public static String formatTimestampForyyyyMMdd(final Date date) {
        if (date == null) {
            return EMPTY_STRING;
        }
        return yyyyMMdd_DATE_FORMAT_THREAD_LOCAL.get().format(date);
    }

    /**
     * Parse 2 strings (date and time) to a date. Required strings formats: dd/MM/yyyy HH:mm.
     * @param dateStr the string date
     * @param timeStr the string time
     * @param strictly true if you want to check strictly the date format, otherwise false
     * @return the date
     * @throws ParseException 
     */
    public static Date getDefaultDateTime(final String dateStr, final String timeStr, final boolean strictly) throws ParseException {
        if (dateStr == null) {
            return null;
        }

        final String dateTimeStr = dateStr + SEPARATOR + timeStr;
        if (strictly && !Pattern.matches(DEFAULT_DATE_REGEX + SEPARATOR + DEFAULT_TIME_REGEX, dateTimeStr)) {
            throw new ParseException("Unparseable date and time: " + dateTimeStr, 0);
        }

        return DEFAULT_DATE_TIME_FORMAT_THREAD_LOCAL.get().parse(dateTimeStr);
    }

    /**
     * Parse a string to a date. Required strings formats: dd/MM/yyyy.
     * @param dateStr the string date
     * @param strictly true if you want to check strictly the date format, otherwise false
     * @return the date
     * @throws ParseException 
     */
    public static Date getDefaultDate(final String dateStr, final boolean strictly) throws ParseException {
        if (dateStr == null) {
            return null;
        }

        if (strictly && !Pattern.matches(DEFAULT_DATE_REGEX, dateStr)) {
            throw new ParseException("Unparseable date and time: " + dateStr, 0);
        }

        return DEFAULT_DATE_FORMAT_THREAD_LOCAL.get().parse(dateStr);
    }

    /**
     * Parse a string to a date silently. Required strings formats: dd/MM/yyyy.
     * @param dateStr the string date
     * @param strictly true if you want to check strictly the date format, otherwise false
     * @return the date
     * @throws ParseException 
     */
    public static Date getDefaultDateSilently(final String dateStr, final boolean strictly) {
        if ((dateStr == null) || (strictly && !Pattern.matches(DEFAULT_DATE_REGEX, dateStr))) {
            return null;
        }

        try {
            return DEFAULT_DATE_FORMAT_THREAD_LOCAL.get().parse(dateStr);
        } catch (final ParseException e) {
            return null;
        }
    }

    public static Date getHTTP11DateSilently(final String dateStr) {

        if (StringUtils.isBlank(dateStr)) {
            return null;
        }

        try {
            return HTTP_1_1_DATE_FORMAT_THREAD_LOCAL.get().parse(dateStr);
        } catch (final ParseException e) {
            return null;
        }
    }

    public static String formatHTTP11Date(final Date date) {
        if (date == null) {
            return EMPTY_STRING;
        }
        return HTTP_1_1_DATE_FORMAT_THREAD_LOCAL.get().format(date);
    }

    public static Date toISO8601Date(final String dateStr) throws ParseException {
        if (StringUtils.isBlank(dateStr)) {
            return null;
        }

        return ISO_8601_DATE_ALTERNATE_FORMAT_THREAD_LOCAL.get().parse(dateStr);
    }

    public String toISO8601String(final Date date) {
        if (date != null) {
            return null;
        }

        return ISO_8601_DATE_ALTERNATE_FORMAT_THREAD_LOCAL.get().format(date);
    }

    public static boolean isFutureDate(final Date date) {
        return (date != null) && date.after(new Date());
    }

    public static boolean isPastDate(final Date date) {
        if(date == null) { return false; } //not accepting null date
        return !isFutureDate(date);
    }
}
