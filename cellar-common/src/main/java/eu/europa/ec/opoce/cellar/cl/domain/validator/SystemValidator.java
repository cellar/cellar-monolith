package eu.europa.ec.opoce.cellar.cl.domain.validator;

/**
 * All cellar's system validator must implements this interface.
 * 
 * @author dcraeye
 */
public interface SystemValidator extends Validator {

}
