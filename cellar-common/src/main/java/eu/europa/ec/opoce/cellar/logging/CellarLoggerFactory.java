package eu.europa.ec.opoce.cellar.logging;

import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;

import java.util.HashMap;
import java.util.Map;

/**
 * The <code>CellarLoggerFactory</code> is a utility class producing
 * <code>org.slf4j.Logger</code> instances to be used by CMR module. The
 * produced <code>org.slf4j.Logger</code> instances provide the required CMR
 * logging functionality.
 * <p/>
 * The produced <code>org.slf4j.Logger</code> instances are essentially wrappers
 * around <code>org.slf4j.Logger</code> instances produced by another logging
 * framework like log4j,... The actual <code>org.slf4j.Logger</code> instance to
 * which the logging is delegated to is either set in a
 * {@link eu.europa.ec.opoce.cellar.logging.LoggerContext}-instance on the
 * current thread's local or - if no
 * {@link eu.europa.ec.opoce.cellar.logging.LoggerContext}-instance set - a
 * <code>org.slf4j.Logger</code> instance named according to the name of this
 * Logger.
 * Deprecation: Scheduled for deletion in 8.0
 */
@Deprecated
public class CellarLoggerFactory {

    /**
     * <code>org.slf4j.Logger</code> implementing class used for CMR logging
     * functionality. This class is essentially a wrapper around an
     * <code>org.slf4j.Logger</code> instance.
     * <p/>
     * The <code>org.slf4j.Logger</code> instance to which instances of this
     * class delegate to is either set in a
     * {@link eu.europa.ec.opoce.cellar.logging.LoggerContext}-instance on the
     * current thread's local or - if no
     * {@link eu.europa.ec.opoce.cellar.logging.LoggerContext}-instance set - a
     * <code>org.slf4j.Logger</code> instance named according to the name of
     * this Logger
     */
    public static class CellarLogger implements Logger {

        /**
         * The name.
         */
        private final String name;

        /**
         * The default logger.
         */
        private Logger defaultLogger;

        /**
         * Instantiates a new cellar logger.
         *
         * @param name the name
         */
        private CellarLogger(final String name) {
            this.name = name;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void debug(final Marker marker, final String msg) {
            if (!this.isDebugEnabled(marker)) {
                return;
            }

            this.getLogger().debug(marker, msg);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void debug(final Marker marker, final String format, final Object arg) {
            if (!this.isDebugEnabled(marker)) {
                return;
            }

            this.getLogger().debug(marker, format, arg);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void debug(final Marker marker, final String format, final Object arg1, final Object arg2) {
            if (!this.isDebugEnabled(marker)) {
                return;
            }

            this.getLogger().debug(marker, format, arg1, arg2);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void debug(final Marker marker, final String format, final Object... argArray) {
            if (!this.isDebugEnabled(marker)) {
                return;
            }

            this.getLogger().debug(marker, format, argArray);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void debug(final Marker marker, final String msg, final Throwable t) {
            if (!this.isDebugEnabled(marker)) {
                return;
            }

            this.getLogger().debug(marker, msg, t);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void debug(final String msg) {
            if (!this.isDebugEnabled()) {
                return;
            }

            this.getLogger().debug(msg);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void debug(final String format, final Object arg) {
            if (!this.isDebugEnabled()) {
                return;
            }

            this.getLogger().debug(format, arg);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void debug(final String format, final Object arg1, final Object arg2) {
            if (!this.isDebugEnabled()) {
                return;
            }

            this.getLogger().debug(format, arg1, arg2);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void debug(final String format, final Object... argArray) {
            if (!this.isDebugEnabled()) {
                return;
            }

            this.getLogger().debug(format, argArray);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void debug(final String msg, final Throwable t) {
            if (!this.isDebugEnabled()) {
                return;
            }

            this.getLogger().debug(msg, t);
        }

        /**
         * Default logger.
         *
         * @return the logger
         */
        private Logger defaultLogger() {
            if (this.defaultLogger == null) {
                this.defaultLogger = LoggerFactory.getLogger(this.name);
            }
            return this.defaultLogger;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void error(final Marker marker, final String msg) {
            if (!this.isErrorEnabled(marker)) {
                return;
            }

            this.getLogger().error(marker, msg);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void error(final Marker marker, final String format, final Object arg) {
            if (!this.isErrorEnabled(marker)) {
                return;
            }

            this.getLogger().error(marker, format, arg);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void error(final Marker marker, final String format, final Object arg1, final Object arg2) {
            if (!this.isErrorEnabled(marker)) {
                return;
            }

            this.getLogger().error(marker, format, arg1, arg2);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void error(final Marker marker, final String format, final Object... argArray) {
            if (!this.isErrorEnabled(marker)) {
                return;
            }

            this.getLogger().error(marker, format, argArray);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void error(final Marker marker, final String msg, final Throwable t) {
            if (!this.isErrorEnabled(marker)) {
                return;
            }

            this.getLogger().error(marker, msg, t);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void error(final String msg) {
            if (!this.isErrorEnabled()) {
                return;
            }

            this.getLogger().error(msg);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void error(final String format, final Object arg) {
            if (!this.isErrorEnabled()) {
                return;
            }

            this.getLogger().error(format, arg);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void error(final String format, final Object arg1, final Object arg2) {
            if (!this.isErrorEnabled()) {
                return;
            }

            this.getLogger().error(format, arg1, arg2);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void error(final String format, final Object... argArray) {
            if (!this.isErrorEnabled()) {
                return;
            }

            this.getLogger().error(format, argArray);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void error(final String msg, final Throwable t) {
            if (!this.isErrorEnabled()) {
                return;
            }

            this.getLogger().error(msg, t);
        }

        /**
         * Gets the logger.
         *
         * @return the logger
         */
        private Logger getLogger() {
            final LoggerContext loggerContext = getCurrentLoggerContext();
            if (loggerContext != null) {
                return loggerContext.getLogger();
            }
            return this.defaultLogger();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public String getName() {
            return this.name;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void info(final Marker marker, final String msg) {
            if (!this.isInfoEnabled(marker)) {
                return;
            }

            this.getLogger().info(marker, msg);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void info(final Marker marker, final String format, final Object arg) {
            if (!this.isInfoEnabled(marker)) {
                return;
            }

            this.getLogger().info(marker, format, arg);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void info(final Marker marker, final String format, final Object arg1, final Object arg2) {
            if (!this.isInfoEnabled(marker)) {
                return;
            }

            this.getLogger().info(marker, format, arg1, arg2);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void info(final Marker marker, final String format, final Object... argArray) {
            if (!this.isInfoEnabled(marker)) {
                return;
            }

            this.getLogger().info(marker, format, argArray);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void info(final Marker marker, final String msg, final Throwable t) {
            if (!this.isInfoEnabled(marker)) {
                return;
            }

            this.getLogger().info(marker, msg, t);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void info(final String msg) {
            if (!this.isInfoEnabled()) {
                return;
            }

            this.getLogger().info(msg);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void info(final String format, final Object arg) {
            if (!this.isInfoEnabled()) {
                return;
            }

            this.getLogger().info(format, arg);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void info(final String format, final Object arg1, final Object arg2) {
            if (!this.isInfoEnabled()) {
                return;
            }

            this.getLogger().info(format, arg1, arg2);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void info(final String format, final Object... argArray) {
            if (!this.isInfoEnabled()) {
                return;
            }

            this.getLogger().info(format, argArray);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void info(final String msg, final Throwable t) {
            if (!this.isInfoEnabled()) {
                return;
            }

            this.getLogger().info(msg, t);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean isDebugEnabled() {
            final LoggerContext loggerContext = getCurrentLoggerContext();
            return loggerContext != null ? loggerContext.isDebugEnabled() : this.getLogger().isDebugEnabled();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean isDebugEnabled(final Marker marker) {
            final LoggerContext loggerContext = getCurrentLoggerContext();
            return loggerContext != null ? loggerContext.isDebugEnabled() : this.getLogger().isDebugEnabled(marker);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean isErrorEnabled() {
            final LoggerContext loggerContext = getCurrentLoggerContext();
            return loggerContext != null ? loggerContext.isErrorEnabled() : this.getLogger().isErrorEnabled();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean isErrorEnabled(final Marker marker) {
            final LoggerContext loggerContext = getCurrentLoggerContext();
            return loggerContext != null ? loggerContext.isErrorEnabled() : this.getLogger().isErrorEnabled(marker);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean isInfoEnabled() {
            final LoggerContext loggerContext = getCurrentLoggerContext();
            return loggerContext != null ? loggerContext.isInfoEnabled() : this.getLogger().isInfoEnabled();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean isInfoEnabled(final Marker marker) {
            final LoggerContext loggerContext = getCurrentLoggerContext();
            return loggerContext != null ? loggerContext.isInfoEnabled() : this.getLogger().isInfoEnabled(marker);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean isTraceEnabled() {
            final LoggerContext loggerContext = getCurrentLoggerContext();
            return loggerContext != null ? loggerContext.isTraceEnabled() : this.getLogger().isTraceEnabled();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean isTraceEnabled(final Marker marker) {
            final LoggerContext loggerContext = getCurrentLoggerContext();
            return loggerContext != null ? loggerContext.isTraceEnabled() : this.getLogger().isTraceEnabled(marker);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean isWarnEnabled() {
            final LoggerContext loggerContext = getCurrentLoggerContext();
            return loggerContext != null ? loggerContext.isWarnEnabled() : this.getLogger().isWarnEnabled();
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean isWarnEnabled(final Marker marker) {
            final LoggerContext loggerContext = getCurrentLoggerContext();
            return loggerContext != null ? loggerContext.isWarnEnabled() : this.getLogger().isWarnEnabled(marker);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void trace(final Marker marker, final String msg) {
            if (!this.isTraceEnabled(marker)) {
                return;
            }

            this.getLogger().trace(marker, msg);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void trace(final Marker marker, final String format, final Object arg) {
            if (!this.isTraceEnabled(marker)) {
                return;
            }

            this.getLogger().trace(marker, format, arg);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void trace(final Marker marker, final String format, final Object arg1, final Object arg2) {
            if (!this.isTraceEnabled(marker)) {
                return;
            }

            this.getLogger().trace(marker, format, arg1, arg2);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void trace(final Marker marker, final String format, final Object... argArray) {
            if (!this.isTraceEnabled(marker)) {
                return;
            }

            this.getLogger().trace(marker, format, argArray);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void trace(final Marker marker, final String msg, final Throwable t) {
            if (!this.isTraceEnabled(marker)) {
                return;
            }

            this.getLogger().trace(marker, msg, t);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void trace(final String msg) {
            if (!this.isTraceEnabled()) {
                return;
            }

            this.getLogger().trace(msg);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void trace(final String format, final Object arg) {
            if (!this.isTraceEnabled()) {
                return;
            }

            this.getLogger().trace(format, arg);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void trace(final String format, final Object arg1, final Object arg2) {
            if (!this.isTraceEnabled()) {
                return;
            }

            this.getLogger().trace(format, arg1, arg2);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void trace(final String format, final Object... argArray) {
            if (!this.isTraceEnabled()) {
                return;
            }

            this.getLogger().trace(format, argArray);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void trace(final String msg, final Throwable t) {
            if (!this.isTraceEnabled()) {
                return;
            }

            this.getLogger().trace(msg, t);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void warn(final Marker marker, final String msg) {
            if (!this.isWarnEnabled(marker)) {
                return;
            }

            this.getLogger().warn(marker, msg);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void warn(final Marker marker, final String format, final Object arg) {
            if (!this.isWarnEnabled(marker)) {
                return;
            }

            this.getLogger().warn(marker, format, arg);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void warn(final Marker marker, final String format, final Object arg1, final Object arg2) {
            if (!this.isWarnEnabled(marker)) {
                return;
            }

            this.getLogger().warn(marker, format, arg1, arg2);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void warn(final Marker marker, final String format, final Object... argArray) {
            if (!this.isWarnEnabled(marker)) {
                return;
            }

            this.getLogger().warn(marker, format, argArray);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void warn(final Marker marker, final String msg, final Throwable t) {
            if (!this.isWarnEnabled(marker)) {
                return;
            }

            this.getLogger().warn(marker, msg, t);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void warn(final String msg) {
            if (!this.isWarnEnabled()) {
                return;
            }

            this.getLogger().warn(msg);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void warn(final String format, final Object arg) {
            if (!this.isWarnEnabled()) {
                return;
            }

            this.getLogger().warn(format, arg);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void warn(final String format, final Object arg1, final Object arg2) {
            if (!this.isWarnEnabled()) {
                return;
            }

            this.getLogger().warn(format, arg1, arg2);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void warn(final String format, final Object... argArray) {
            if (!this.isWarnEnabled()) {
                return;
            }

            this.getLogger().warn(format, argArray);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void warn(final String msg, final Throwable t) {
            if (!this.isWarnEnabled()) {
                return;
            }

            this.getLogger().warn(msg, t);
        }
    }

    /**
     * Constant <code>currentLoggerContext</code>.
     */
    private static ThreadLocal<LoggerContext> currentLoggerContext = new ThreadLocal<>();

    /**
     * Constant <code>INSTANCE</code>.
     */
    private static CellarLoggerFactory INSTANCE = new CellarLoggerFactory();

    /**
     * <p>
     * bindCurrentLoggerContext.
     * </p>
     *
     * @param loggerContext a {@link eu.europa.ec.opoce.cellar.logging.LoggerContext} object.
     */
    private static void bindCurrentLoggerContext(final LoggerContext loggerContext) {
        currentLoggerContext.set(loggerContext);
    }

    /**
     * <p>
     * Getter for the field <code>currentLoggerContext</code>.
     * </p>
     *
     * @return a {@link eu.europa.ec.opoce.cellar.logging.LoggerContext} object.
     */
    private static LoggerContext getCurrentLoggerContext() {
        return currentLoggerContext.get();
    }

    /**
     * Return a Logger named corresponding to the class passed as parameter,
     * using the statically bound
     * {@link eu.europa.ec.opoce.cellar.logging.CellarLoggerFactory} instance.
     * <p/>
     * If a {@link eu.europa.ec.opoce.cellar.logging.LoggerContext} set on the
     * current thread local then this Logger will delegate all calls to the
     * Logger set in the current
     * {@link eu.europa.ec.opoce.cellar.logging.LoggerContext} Else a default
     * Logger named after the given clazz will be used.
     *
     * @param clazz the returned logger will be named after clazz
     * @return logger
     */
    public static Logger getLogger(final Class<?> clazz) {
        if (clazz == null) {
            throw ExceptionBuilder.get(CellarException.class).build();
        }
        return getLogger(clazz.getName());
    }

    /**
     * Return a logger named according to the name parameter, using the
     * statically bound
     * {@link eu.europa.ec.opoce.cellar.logging.CellarLoggerFactory} instance.
     * <p/>
     * If a {@link eu.europa.ec.opoce.cellar.logging.LoggerContext} set on the
     * current thread local then this Logger will delegate all calls to the
     * Logger set in the current
     * {@link eu.europa.ec.opoce.cellar.logging.LoggerContext} Else a default
     * Logger named after the given name.
     *
     * @param name The name of the logger.
     * @return logger
     */
    public static Logger getLogger(final String name) {
        if (name == null) {
            throw ExceptionBuilder.get(CellarException.class).build();
        }
        return INSTANCE.getCellarLogger(name);
    }

    /**
     * <p>
     * getLoggerContext.
     * </p>
     *
     * @return a {@link eu.europa.ec.opoce.cellar.logging.LoggerContext} object.
     */
    public static LoggerContext getLoggerContext() {
        return getCurrentLoggerContext();
    }

    /**
     * <p>
     * setLoggerContext.
     * </p>
     *
     * @param loggerContext a {@link eu.europa.ec.opoce.cellar.logging.LoggerContext} object.
     */
    public static void setLoggerContext(final LoggerContext loggerContext) {
        bindCurrentLoggerContext(loggerContext);
    }

    /**
     * <p>
     * setLoggerContext.
     * </p>
     *
     * @param name                  a {@link java.lang.String} object.
     * @param cellarLogLevelService a {@link eu.europa.ec.opoce.cellar.logging.CellarLogLevelService} object.
     */
    public static void setLoggerContext(final String name, final CellarLogLevelService cellarLogLevelService) {
        if (name == null) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument name must not be null!")
                    .build();
        }
        if (cellarLogLevelService == null) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("Required argument cellarLogLevelService must not be null!").build();
        }

        setLoggerContext(new LoggerContext(LoggerFactory.getLogger(name), cellarLogLevelService));
    }

    /**
     * <p>
     * setLoggerContext.
     * </p>
     *
     * @param name                  a {@link java.lang.String} object.
     * @param loggingContextValues  a {@link java.util.Map} object.
     * @param cellarLogLevelService a {@link eu.europa.ec.opoce.cellar.logging.CellarLogLevelService} object.
     */
    public static void setLoggerContext(final String name, final Map<String, String> loggingContextValues,
                                        final CellarLogLevelService cellarLogLevelService) {
        if (name == null) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument name must not be null!")
                    .build();
        }
        if (cellarLogLevelService == null) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("Required argument cellarLogLevelService must not be null!").build();
        }

        setLoggerContext(new LoggerContext(LoggerFactory.getLogger(name), loggingContextValues, cellarLogLevelService));
    }

    /**
     * The logger map.
     */
    private final Map<String, CellarLogger> loggerMap = new HashMap<>();

    /**
     * <p>
     * Constructor for CellarLoggerFactory.
     * </p>
     */
    private CellarLoggerFactory() {
    }

    /**
     * <p>
     * getCellarLogger.
     * </p>
     *
     * @param name a {@link java.lang.String} object.
     * @return a {@link org.slf4j.Logger} object.
     */
    private Logger getCellarLogger(final String name) {
        CellarLogger logger;
        synchronized (this) {
            logger = this.loggerMap.computeIfAbsent(name, CellarLogger::new);
        }
        return logger;
    }
}
