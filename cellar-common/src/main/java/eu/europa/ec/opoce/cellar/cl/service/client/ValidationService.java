package eu.europa.ec.opoce.cellar.cl.service.client;

import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.cl.domain.validator.SystemValidator;
import eu.europa.ec.opoce.cellar.cl.domain.validator.ValidationDetails;
import eu.europa.ec.opoce.cellar.cl.domain.validator.ValidationResult;
import eu.europa.ec.opoce.cellar.cl.domain.validator.ValidationStrategy;

import java.util.List;

/**
 * Service to manage system and custome validator.
 * 
 * @author dcraeye
 * 
 */
public interface ValidationService {

    /**
     * Return the list of founded Validation Strategy.
     * This method searches in the classpath all class that implements the interface ValidationStrategy. 
     * @return list of {@link ValidationStrategy}
     */
    List<ValidationStrategy> getValidationStrategyList();

    /**
     * Searches in the classpath all classes which implements {@link ValidationStrategy}.
     * Then execute the validate method on each classes found.
     * 
     * @return a {@link ValidationResult} containing the result of these validations. 
     */
    ValidationResult validate(ValidationDetails validationParameters);

    /**
     * Apply the system validation process on objectToValidate.
     * Test before if the validator is active. 
     * @param objectToValidate the object to validate
     * @param sipResource this parameters is needed by auditable
     * @param validator the validator class.
     * @return a {@link ValidationResult} containing the result of the validation.
     */
    ValidationResult executeSystemValidator(Object objectToValidate, SIPResource sipResource, SystemValidator validator);

    /**
     * Apply the system validation process on validationParameters.
     * Test before if the validator is active. 
     * @param validationParameters the {@link ValidationDetails} containing elements to validate
     * @param sipResource this parameters is needed by auditable
     * @param validator the validator class.
     * @return a {@link ValidationResult} containing the result of the validation.
     */
    ValidationResult executeSystemValidator(ValidationDetails validationParameters, SystemValidator validator);

    /**
     * Apply the system validation process on objectToValidate.
     * Test before if the validator is active. 
     * @param objectToValidate the object to validate
     * @param sipResource this parameters is needed by auditable
     * @param validator the validator class.
     * @param validatorQualifierName the qualifier name used by spring to instantiate this validator.
     * @return a {@link ValidationResult} containing the result of the validation.
     */
    ValidationResult executeSystemValidator(Object objectToValidate, SIPResource sipResource, SystemValidator validator,
            String validatorQualifierName);

    /**
     * Apply the system validation process on validationParameters.
     * Test before if the validator is active. 
     * @param validationParameters the {@link ValidationDetails} containing elements to validate
     * @param sipResource this parameters is needed by auditable
     * @param validator the validator class.
     * @param validatorQualifierName the qualifier name used by spring to instantiate this validator.
     * @return a {@link ValidationResult} containing the result of the validation.
     */
    ValidationResult executeSystemValidator(ValidationDetails validationParameters, SystemValidator validator,
            String validatorQualifierName);

}
