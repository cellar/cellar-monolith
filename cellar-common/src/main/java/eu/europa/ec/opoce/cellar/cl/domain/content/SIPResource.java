package eu.europa.ec.opoce.cellar.cl.domain.content;

import eu.europa.ec.opoce.cellar.cl.domain.package_history.PackageHistory;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;

import java.io.Serializable;

/**
 * The sip resource.
 * 
 * @author dcraeye
 * 
 */
public class SIPResource implements Serializable {

    /**
     * The <code>serialVersionUID</code>.
     */
    private static final long serialVersionUID = 8813252821057112981L;

    /**
     * The METS document identifier.
     */
    private String metsID;

    /**
     * The folder name in which the sip file is exploded.
     */
    private String folderName;

    /**
     * The sip's source archive file name (aka: sip-file).
     */
    private String archiveName;

    /**
     * The sip's type archive (AUTHENTICOJ, DAILY, BULK, BULK_LOWPRIORITY).
     */
    private TYPE sipType;

    /**
     * Priority of this sip?
     */
    private int priority;

    /**
     * Number of structMap inside his mets?
     */
    private int structMapCount;

    /**
     * Number of digital object inside his mets?
     */
    private int digitalObjectCount;

    /**
     * Element <metsHdr> and its full content.
     */
    private String metsHdr;
    
    /**
     * Corresponding PACKAGE_HISTORY entry.
     */
    private PackageHistory packageHistory;

    /**
     * Constructor.
     */
    public SIPResource() {

    }

    /**
     * Constructor with parameters
     * 
     * @param metsID
     *            the metsID to set
     * @param folderName
     *            the folder name to set.
     * @param archiveName
     *            the archive name to set.
     */
    public SIPResource(String metsID, String folderName, String archiveName, TYPE sipType) {
        super();
        this.metsID = metsID;
        this.folderName = folderName;
        this.archiveName = archiveName;
        this.sipType = sipType;
    }

    /**
     * Set the metsID.
     * 
     * @param metsID
     *            the METS document identifier to set
     */
    public void setMetsID(String metsID) {
        this.metsID = metsID;
    }

    /**
     * Gets the value of the METS document identifier.
     * 
     * @return the metsID
     */
    public String getMetsID() {
        return metsID;
    }

    /**
     * Set the folder name.
     * 
     * @param folderName
     *            the folder name to set
     */
    public void setFolderName(final String folderName) {
        this.folderName = folderName;
    }

    /**
     * Gets the value of the folderName. (The folder name is the directory name in which all the
     * files are uncompressed)
     * 
     * @return the folderName
     */
    public String getFolderName() {
        return folderName;
    }

    /**
     * Set the sip's original archiveName.
     * 
     * @param archiveName
     *            the sip's original archiveName name to set
     */
    public void setArchiveName(final String archiveName) {
        this.archiveName = archiveName;
    }

    /**
     * Gets the value of the sip's original archiveName. (The file name is the source archive name
     * from which the resources are uncompressed)
     * 
     * @return the fileName
     */
    public String getArchiveName() {
        return archiveName;
    }

    /**
     * @return the sipType
     */
    public TYPE getSipType() {
        return sipType;
    }

    /**
     * @param sipType the sipType to set
     */
    public void setSipType(TYPE sipType) {
        this.sipType = sipType;
    }

    /**
     * The priority of this SIP.
     * 
     * @return the priority of this sip.
     */
    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getStructMapCount() {
        return structMapCount;
    }

    public void setStructMapCount(int structMapCount) {
        this.structMapCount = structMapCount;
    }

    public int getDigitalObjectCount() {
        return digitalObjectCount;
    }

    public void setDigitalObjectCount(int digitalObjectCount) {
        this.digitalObjectCount = digitalObjectCount;
    }

    /**
     * Get the mets header value.
     * 
     * @return METS header.
     */
    public String getMetsHdr() {
        return metsHdr;
    }

    /**
     * Set the metsHdr value
     * 
     * @param metsHdr
     *            METS header.
     */
    public void setMetsHdr(final String metsHdr) {
        this.metsHdr = metsHdr;
    }
    
    /**
     * Gets the {@link PackageHistory} object.
     *
     * @return the {@link PackageHistory} object
     */
    public PackageHistory getPackageHistory() {
        return packageHistory;
    }
    
    /**
     * Sets the {@link PackageHistory} object.
     *
     * @param packageHistory the {@link PackageHistory} object
     */
    public void setPackageHistory(PackageHistory packageHistory) {
        this.packageHistory = packageHistory;
    }
    
    @Override
    public String toString() {
        return "SIPResource [sipType(" + sipType + ") : metsID=" + metsID + ", folderName=" + folderName + ", archiveName=" + archiveName
                + "]";
    }

}
