package eu.europa.ec.opoce.cellar.domain.content.mets;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

public enum ContentStreamLocType {
    URL("URL"), OTHER("OTHER");

    private String xmlValue;

    ContentStreamLocType(String xmlValue) {
        this.xmlValue = xmlValue;
    }

    public static ContentStreamLocType getByXmlValue(String xmlValue) {
        for (ContentStreamLocType type : ContentStreamLocType.values()) {
            if (type.xmlValue.equals(xmlValue)) {
                return type;
            }
        }
        throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.UNKNOWN_CONTENT_LOCTYPE)
                .withMessage("Cannot find LocType with value '{}'.").withMessageArgs(xmlValue).build();
    }

}
