package eu.europa.ec.opoce.cellar.common.http.headers;

import eu.europa.ec.opoce.cellar.common.MessageFormatter;
import eu.europa.ec.opoce.cellar.common.util.TimeUtils;

import java.util.Date;

public class LinkImpl implements ILink {

    private static final String URI_REL_FORMAT = "<{}>;rel=\"{}\"";
    private static final String URI_REL_DATETIME_FORMAT = "<{}>;rel=\"{}\";datetime=\"{}\"";

    /**
     * The URI associated to the link
     */
    private String uri;

    /**
     * The relation type associated to the link
     */
    private RelType rel;

    /**
     * The datetime associated to the link
     */
    private Date dateTime;

    /**
     * Constructor, instantiates a new Link with dateTime set to null
     * @param uri the Link's URI 
     * @param rel the Link's relationship type
     */
    public LinkImpl(final String uri, final RelType rel) {
        this(uri, rel, null);
    }

    /**
     * Constructor, instantiates a new Link
     * @param uri the Link's URI
     * @param rel the Link's relationship type
     * @param dateTime the Link's dateTime
     */
    public LinkImpl(final String uri, final RelType rel, final Date dateTime) {
        this.uri = uri;
        this.rel = rel;
        this.dateTime = dateTime;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUri() {
        return uri;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public RelType getRel() {
        return rel;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Date getDateTime() {
        return dateTime;
    }

    @Override
    public String toString() {

        final StringBuilder builder = new StringBuilder();

        if (null == dateTime) {
            builder.append(MessageFormatter.format(URI_REL_FORMAT, uri, rel.getValue()));
        } else {
            builder.append(
                    MessageFormatter.format(URI_REL_DATETIME_FORMAT, uri, rel.getValue(), TimeUtils.formatHTTP11Date(this.dateTime)));
        }
        return builder.toString();

    }

}
