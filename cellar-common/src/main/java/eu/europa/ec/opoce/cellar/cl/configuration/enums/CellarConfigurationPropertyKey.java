/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.configuration.enums
 *        FILE : CellarConfigurationProperty.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 02-04-2012
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.configuration.enums;

import eu.europa.ec.opoce.cellar.cl.configuration.impl.CellarPersistentConfiguration;
import eu.europa.ec.opoce.cellar.cl.service.client.IIndexRequestPoolService;
import eu.europa.ec.opoce.cellar.cl.service.client.SparqlExecutingService;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import eu.europa.ec.opoce.cellar.common.metadata.IExistingMetadataLoader;
import eu.europa.ec.opoce.cellar.ingestion.service.QueueWatcher;
import eu.europa.ec.opoce.cellar.ingestion.service.SIPWatcher;
import net.sf.ehcache.CacheManager;

/**
 * <class_description> Cellar configuration property's keys.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 02-04-2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public enum CellarConfigurationPropertyKey {

    /*
     * CELLAR BASIC INFORMATION These are the properties to be loaded as first, as the others depend on these
     */
    cellarConfigurationDatabaseHasPriority_Key("cellar.configuration.database.hasPriority"), //
    cellarInstanceId_Key("cellar.instanceId"), //
    cellarConfigurationVersion_Key("cellar.configuration.version"), //

    /*
     * CELLAR FOLDERS
     */
    cellarFolderRoot_Key("cellar.folder.root"), //
    cellarFolderAuthenticOJReception_Key("cellar.folder.authenticOJReception"), //
    cellarFolderDailyReception_Key("cellar.folder.dailyReception"), //
    cellarFolderBulkReception_Key("cellar.folder.bulkReception"), //
    cellarFolderBulkLowPriorityReception_Key("cellar.folder.bulkLowPriorityReception"), //
    cellarFolderAuthenticOJResponse_Key("cellar.folder.authenticOJResponse"), //
    cellarFolderDailyResponse_Key("cellar.folder.dailyResponse"), //
    cellarFolderBulkResponse_Key("cellar.folder.bulkResponse"), //
    cellarFolderBulkLowPriorityResponse_Key("cellar.folder.bulkLowPriorityResponse"), //
    cellarFolderFoxml_Key("cellar.folder.foxml"), //
    cellarFolderError_Key("cellar.folder.error"), //
    cellarFolderTemporaryDissemination_Key("cellar.folder.temporaryDissemination"), //
    cellarFolderTemporaryWork_Key("cellar.folder.temporaryWork"), //
    cellarFolderTemporaryStore_Key("cellar.folder.temporaryStore"), //
    cellarFolderLock_Key("cellar.folder.lock"), //
    cellarFolderBackup_Key("cellar.folder.backup"), //
    cellarFolderExportRest_Key("cellar.folder.export.rest"), //
    cellarFolderExportBatchJob_Key("cellar.folder.export.batchJob"), //
    cellarFolderExportBatchJobTemporary_Key("cellar.folder.export.batchJob.temporary"), //
    cellarFolderExportUpdateResponse_Key("cellar.folder.export.updateResponse"), //
    cellarFolderCmrLog_Key("cellar.folder.cmrlog"), //
    cellarFolderLicenseHolderArchiveExtraction_Key("cellar.folder.licenseHolder.archive.extraction"), //
    cellarFolderLicenseHolderArchiveAdhocExtraction_Key("cellar.folder.licenseHolder.archive.adhocExtraction"), //
    cellarFolderLicenseHolderTemporaryArchive_Key("cellar.folder.licenseHolder.temporary.archive"), //
    cellarFolderLicenseHolderTemporarySparql_Key("cellar.folder.licenseHolder.temporary.sparql"), //
    cellarFolderBatchJobSparqlQueries_Key("cellar.folder.batchJob.sparql.queries"), //
    cellarFolderSparqlResponseTemplates_Key("cellar.folder.sparql.responseTemplates"), //

    /*
     * CELLAR SERVER
     */
    cellarServerName_Key("cellar.server.name"), //
    cellarServerPort_Key("cellar.server.port"), //
    cellarServerContext_Key("cellar.server.context"), //
    cellarServerBaseUrl_Key("cellar.server.baseUrl"), //

    /*
     * CELLAR URIS
     */
    cellarUriDisseminationBase_Key("cellar.uri.disseminationBase"), //
    cellarUriResourceBase_Key("cellar.uri.resourceBase"), //
    cellarUriTranformationResourceBase_Key("cellar.uri.tranformationResourceBase"), //

    /*
     * CELLAR APIS
     */
    cellarApiConnectionTimeout_Key("cellar.api.connectionTimeout"), //
    cellarApiReadTimeout_Key("cellar.api.readTimeout"), //
    cellarApiBaseUrl_Key("cellar.api.baseUrl"), //
    cellarApiHostHeader_Key("cellar.api.hostHeader"), //

    /*
     * CELLAR ENCRYPTOR
     */
    cellarEncryptorAlgorithm_Key("cellar.encryptor.algorithm"), //
    cellarEncryptorPassword_Key("cellar.encryptor.password"), //

    /*
     * CELLAR SERVICES
     */
    cellarServiceIngestionEnabled_Key("cellar.service.ingestion.enabled"), //
    cellarServiceIngestionPoolThreads_Key("cellar.service.ingestion.pool.threads") {

        @Override
        public void executeSatelliteOperations() {
            final QueueWatcher queueWatcher = ServiceLocator.getService(QueueWatcher.class);
            queueWatcher.reconfigureThreadPoolExecutor();
            final IExistingMetadataLoader<?, ?, ?> existingMetadataLoader = ServiceLocator.getService("existingMetadataLoader",
                    IExistingMetadataLoader.class);
            existingMetadataLoader.restart();
        }
    }, //
    cellarServiceIngestionExecutionRate_Key("cellar.service.ingestion.execution.rate"), //
    cellarServiceIngestionValidationMetadataEnabled_Key("cellar.service.ingestion.validation.metadata.enabled"),
    cellarServiceIngestionValidationItemExclusionEnabled_Key("cellar.service.ingestion.validation.item.exclusion.enabled"),//
    cellarServiceIngestionSipCopyRetryPauseSeconds_Key("cellar.service.ingestion.sipCopy.retryPause.seconds"), //
    cellarServiceIngestionSipCopyTimeoutSeconds_Key("cellar.service.ingestion.sipCopy.timeout.seconds"), //
    cellarServiceIngestionCmrLockMode_Key("cellar.service.ingestion.cmr.lock.mode"), //
    cellarServiceIngestionCmrLockMaxRetries_Key("cellar.service.ingestion.cmr.lock.maxRetries"), //
    cellarServiceIngestionCmrLockCeilingDelay_Key("cellar.service.ingestion.cmr.lock.ceilingDelay"), //
    cellarServiceIngestionCmrLockBase_Key("cellar.service.ingestion.cmr.lock.base"), //
    cellarServiceIngestionConcurrencyControllerAutologgingEnabled_Key("cellar.service.ingestion.concurrencyController.autologging.enabled"), //
    cellarServiceIngestionConcurrencyControllerAutologgingCronSettings_Key(
            "cellar.service.ingestion.concurrencyController.autologging.cron.settings"), //
    cellarServiceIngestionConcurrencyControllerThreadDelayMilliseconds_Key("cellar.service.ingestion.concurrencyController.thread.delay.milliseconds"), //
    cellarServiceIngestionCheckWriteOncePropertiesEnabled_Key("cellar.service.ingestion.checkWriteOnceProperties.enabled"), //
    cellarServiceIngestionPickupMode_Key("cellar.service.ingestion.pickup.mode"), //
    cellarStatusServiceShaclReportLevel_Key("cellar.status.service.shacl.report.level"), //
    cellarServiceNalLoadDecodingValidationEnabled_Key("cellar.service.nalLoad.decoding.validation.enabled"), //
    cellarServiceNalLoadEnabled_Key("cellar.service.nalLoad.enabled"), //
    cellarServiceNalLoadInferenceMaxIteration_Key("cellar.service.nalLoad.inference.maxIteration"),
    cellarServiceNalLoadCronSettings_Key("cellar.service.nalLoad.cron.settings"), //
    cellarServiceOntoLoadEnabled_Key("cellar.service.ontoLoad.enabled"), //
    cellarServiceSparqlLoadCronSettings_Key("cellar.service.sparqlLoad.cron.settings"),
    cellarServiceSparqlLoadEnabled_Key("cellar.service.sparqlLoad.enabled"),
    cellarServiceBackupProcessedFileEnabled_Key("cellar.service.backupProcessedFile.enabled"), //
    cellarServiceDeleteProcessedDataEnabled_Key("cellar.service.deleteProcessedData.enabled"), //
    cellarServiceIndexingEnabled_Key("cellar.service.indexing.enabled"), //
    cellarServiceIndexingCronSettings_Key("cellar.service.indexing.cron.settings"), //
    cellarServiceSynchronizingCronSettings_Key("cellar.service.synchronizing.cron.settings"), //
    cellarServiceIndexingMinimumAgeMinutes_Key("cellar.service.indexing.minimumAge.minutes"), //
    cellarServiceIndexingMinimumPriority_Key("cellar.service.indexing.minimumPriority"), //

    cellarServiceIndexingConcurrencyControllerEnabled_Key("cellar.service.indexing.concurrencyController.enabled"), //

    cellarServiceIndexingPoolThreads_Key("cellar.service.indexing.pool.threads") {

        @Override
        public void executeSatelliteOperations() {
            final IIndexRequestPoolService indexRequestProcessingService = ServiceLocator.getService("indexRequestProcessingService",
                    IIndexRequestPoolService.class);
            indexRequestProcessingService.refreshPoolSize();
        }
    }, //
    cellarServiceIndexingCleanerEnabled_Key("cellar.service.indexing.cleaner.enabled"), //
    cellarServiceIndexingCleanerCronSettings_Key("cellar.service.indexing.cleaner.cron.settings"), //
    cellarServiceIndexingCleanerDoneMinimumAgeDays_Key("cellar.service.indexing.cleaner.done.minimumAge.days"), //
    cellarServiceIndexingCleanerRedundantMinimumAgeDays_Key("cellar.service.indexing.cleaner.redundant.minimumAge.days"), //
    cellarServiceIndexingCleanerErrorMinimumAgeDays_Key("cellar.service.indexing.cleaner.error.minimumAge.days"), //
    cellarServiceIndexingQueryLimitResults_Key("cellar.service.indexing.query.limit.results"), //
    cellarServiceIndexingScheduledPriority_Key("cellar.service.indexing.scheduled.priority"), //
    cellarServiceIndexingScheduledCalcInverse_Key("cellar.service.indexing.scheduled.calcInverse"), //
    cellarServiceIndexingScheduledCalcEmbedded_Key("cellar.service.indexing.scheduled.calcEmbedded"), //
    cellarServiceIndexingScheduledCalcNotice_Key("cellar.service.indexing.scheduled.calcNotice"), //
    cellarServiceIndexingScheduledCalcExpanded_Key("cellar.service.indexing.scheduled.calcExpanded"), //
    cellarServiceIndexingScheduledSelectionPageSize_Key("cellar.service.indexing.scheduled.selection.pageSize"), //
    cellarServiceIndexingScheduledBatchSleepTime_Key("cellar.service.indexing.scheduled.batch.sleepTime"), //
    cellarServiceIndexingScheduledExampleSparqlQueries_Key("cellar.service.indexing.scheduled.example.sparql.queries"), //

    cellarServiceAutomaticDisembargoEnabled_Key("cellar.service.automaticDisembargo.enabled"), //

    cellarServiceDisembargoInitialDelay_Key("cellar.service.disembargo.initialDelay"),
    cellarServiceBatchJobEnabled_Key("cellar.service.batchJob.enabled"),
    cellarServiceBatchJobSparqlCronSettings_Key("cellar.service.batchJob.sparqlExecuting.cron.settings"), //
    cellarServiceBatchJobProcessingCronSettings_Key("cellar.service.batchJob.batchProcessing.cron.settings"), //
    cellarServiceBatchJobSparqlUpdateMaxCron_Key("cellar.service.batchJob.sparqlUpdate.max.cron"), //
    cellarServiceBatchJobPoolCorePoolSize_Key("cellar.service.batchJob.pool.corePoolSize"), //
    cellarServiceBatchJobPoolMaxPoolSize_Key("cellar.service.batchJob.pool.maxPoolSize"), //
    cellarServiceBatchJobPoolQueueSize_Key("cellar.service.batchJob.pool.queueSize"), //
    cellarServiceBatchJobPoolSelectionSize_Key("cellar.service.batchJob.pool.selectionSize"), //
    cellarServiceSparqlTimeout_Key("cellar.service.sparql.timeout") {
        @Override
        public void executeSatelliteOperations() {
            SparqlExecutingService sparqlExecutingService = ServiceLocator.getService(SparqlExecutingService.class);
            sparqlExecutingService.updateTimeout();
        }
    }, //
    cellarServiceSparqlMaxConnections_Key("cellar.service.sparql.max-connections"),
    cellarServiceSparqlTimeoutConnections_Key("cellar.service.sparql.timeout-connections"),
    cellarServiceSparqlUri_Key("cellar.service.sparql.uri"), //
    cellarServiceLicenseHolderEnabled_Key("cellar.service.licenseHolder.enabled"), //
    cellarServiceLicenseHolderWorkerCronSettings_Key("cellar.service.licenseHolder.worker.cron.settings"), //
    cellarServiceLicenseHolderWorkerTaskExecutorCorePoolSize_Key("cellar.service.licenseHolder.worker.taskExecutor.corePoolSize"), //
    cellarServiceLicenseHolderWorkerTaskExecutorMaxPoolSize_Key("cellar.service.licenseHolder.worker.taskExecutor.maxPoolSize"), //
    cellarServiceLicenseHolderWorkerTaskExecutorQueueCapacity_Key("cellar.service.licenseHolder.worker.taskExecutor.queueCapacity"), //
    cellarServiceLicenseHolderWorkerArchivingExecutorCorePoolSize_Key(
            "cellar.service.licenseHolder.worker.archivingBatchExecutor.corePoolSize"), //
    cellarServiceLicenseHolderWorkerArchivingExecutorMaxPoolSize_Key(
            "cellar.service.licenseHolder.worker.archivingBatchExecutor.maxPoolSize"), //
    cellarServiceLicenseHolderWorkerArchivingExecutorQueueCapacity_Key(
            "cellar.service.licenseHolder.worker.archivingBatchExecutor.queueCapacity"), //
    cellarServiceLicenseHolderSchedulerCronSettings_Key("cellar.service.licenseHolder.scheduler.cron.settings"), //
    cellarServiceLicenseHolderCleanerCronSettings_Key("cellar.service.licenseHolder.cleaner.cron.settings"), //
    cellarServiceLicenseHolderKeptDaysDaily_Key("cellar.service.licenseHolder.keptDays.daily"), //
    cellarServiceLicenseHolderKeptDaysWeekly_Key("cellar.service.licenseHolder.keptDays.weekly"), //
    cellarServiceLicenseHolderKeptDaysMonthly_Key("cellar.service.licenseHolder.keptDays.monthly"), //
    cellarServiceLicenseHolderBatchSizePerThread_Key("cellar.service.licenseHolder.batchSize.perThread"), //
    cellarServiceLicenseHolderBatchSizeFolder_Key("cellar.service.licenseHolder.batchSize.folder"), //
    cellarServiceInverseNoticesWorkEnabled_Key("cellar.service.inverseNotices.work.enabled"), //
    cellarServiceInverseNoticesExpressionEnabled_Key("cellar.service.inverseNotices.expression.enabled"), //
    cellarServiceInverseNoticesManifestationEnabled_Key("cellar.service.inverseNotices.manifestation.enabled"), //
    cellarServiceInverseNoticesDossierEnabled_Key("cellar.service.inverseNotices.dossier.enabled"), //
    cellarServiceInverseNoticesEventEnabled_Key("cellar.service.inverseNotices.event.enabled"), //
    cellarServiceInverseNoticesAgentEnabled_Key("cellar.service.inverseNotices.agent.enabled"), //
    cellarServiceInverseNoticesTopLevelEventEnabled_Key("cellar.service.inverseNotices.toplevelevent.enabled"), //
    cellarServiceInverseNoticesLimit_Key("cellar.service.inverseNotices.limit"), //
    cellarServiceInverseNoticesRemoveNodeEnabled_Key("cellar.service.inverseNotices.removeNode.enabled"), //
    cellarServiceExternalStartupTimeoutSeconds_Key("cellar.service.external.startup.timeout.seconds"), //
    cellarServiceDisseminationInNoticePropertiesOnlyEnabled_Key("cellar.service.dissemination.inNoticePropertiesOnly.enabled"), //
    cellarServiceDisseminationDatabaseOptimizationEnabled_Key("cellar.service.dissemination.databaseOptimization.enabled"), //
    cellarServiceDisseminationContentStreamMaxSize_Key("cellar.service.dissemination.contentStream.maxSize"), //
    cellarServiceDisseminationNoticesSortEnabled_Key("cellar.service.dissemination.notices.sort.enabled"), //
    cellarServiceDisseminationDecodingDefault_Key("cellar.service.dissemination.decoding.default"), //
    cellarServiceDisseminationMementoEnabled_Key("cellar.service.dissemination.memento.enabled"), //
    cellarServiceDisseminationRetrieveEmbargoedResourceEnabled_Key("cellar.service.dissemination.retrieve.embargoed.resource.enabled"), //
    cellarServiceRDFStoreCleanerEnabled_Key("cellar.service.rdfStoreCleaner.enabled"), //
    cellarServiceRDFStoreCleanerMode_Key("cellar.service.rdfStoreCleaner.mode"), //
    cellarServiceRDFStoreCleanerHierarchiesBatchSize_Key("cellar.service.rdfStoreCleaner.hierarchies.batchSize"), //
    cellarServiceRDFStoreCleanerResourcesBatchSize_Key("cellar.service.rdfStoreCleaner.resources.batchSize"), //
    cellarServiceRDFStoreCleanerExecutorCorePoolSize_Key("cellar.service.rdfStoreCleaner.executor.corePoolSize"), //
    cellarServiceRDFStoreCleanerExecutorMaxPoolSize_Key("cellar.service.rdfStoreCleaner.executor.maxPoolSize"), //
    cellarServiceRDFStoreCleanerExecutorQueueCapacity_Key("cellar.service.rdfStoreCleaner.executor.queueCapacity"), //
    cellarServiceNotificationItemsPerPage_Key("cellar.service.notification.itemsPerPage"), //
    cellarServiceDisseminationDailyOjCacheRefreshMinutes_Key("cellar.service.dissemination.daily.oj.cache.refresh.minutes"), //

    cellarServiceAlignerExecutorEnabled_Key("cellar.service.aligner.executor.enabled"), //
    cellarServiceAlignerExecutorCorePoolSize_Key("cellar.service.aligner.executor.corePoolSize"), //
    cellarServiceAlignerExecutorMaxPoolSize_Key("cellar.service.aligner.executor.maxPoolSize"), //
    cellarServiceAlignerExecutorQueueCapacity_Key("cellar.service.aligner.executor.queueCapacity"), //
    cellarServiceAlignerMode_Key("cellar.service.aligner.mode"), //
    cellarServiceAlignerHierarchiesBatchSize_Key("cellar.service.aligner.hierarchies.batchSize"), //
    cellarServiceLanguagesSkosCachePeriod_Key("cellar.service.languages.skos.cachePeriod"), //
    cellarServiceFileTypesSkosCachePeriod_Key("cellar.service.fileTypes.skos.cachePeriod"), //

    cellarServiceIntegrationArchivistEnabled_Key("cellar.service.integration.archivist.enabled"), //
    cellarServiceIntegrationArchivistBaseUrl_Key("cellar.service.integration.archivist.baseUrl"), //
    cellarServiceIntegrationFixityEnabled_Key("cellar.service.integration.fixity.enabled"), //
    cellarServiceIntegrationFixityBaseUrl_Key("cellar.service.integration.fixity.baseUrl"), //
    cellarServiceIntegrationValidationBaseUrl_Key("cellar.service.integration.validation.baseUrl"),
    cellarServiceCellarResourceMaxElementsInMemory_Key("cellar.service.cellarResource.maxElementsInMemory"), //
    cellarServiceCellarResourceTimeToLiveSeconds_Key("cellar.service.cellarResource.timeToLiveSeconds"), //
    cellarServiceEmbeddedNoticeCacheMaxBytesLocalHeap_Key("cellar.service.embeddedNoticeCache.maxBytesLocalHeap") {
    	
        @Override
        public void executeSatelliteOperations() {
            final CacheManager cacheManager = ServiceLocator.getService("realCacheManager", CacheManager.class);
            final CellarPersistentConfiguration configuration = ServiceLocator.getService(CellarPersistentConfiguration.class);
            cacheManager.getCache("embeddedNoticeCache").getCacheConfiguration().setMaxBytesLocalHeap(configuration.getCellarServiceEmbeddedNoticeCacheMaxBytesLocalHeap());
        }
        
    }, //
    cellarServiceEmbeddedNoticeCacheMaxBytesLocalDisk_Key("cellar.service.embeddedNoticeCache.maxBytesLocalDisk") {
    	
        @Override
        public void executeSatelliteOperations() {
            final CacheManager cacheManager = ServiceLocator.getService("realCacheManager", CacheManager.class);
            final CellarPersistentConfiguration configuration = ServiceLocator.getService(CellarPersistentConfiguration.class);
            cacheManager.getCache("embeddedNoticeCache").getCacheConfiguration().setMaxBytesLocalDisk(configuration.getCellarServiceEmbeddedNoticeCacheMaxBytesLocalDisk());
        }
        
    }, //
    cellarServiceEmbeddedNoticeCacheTimeToLiveSeconds_Key("cellar.service.embeddedNoticeCache.timeToLiveSeconds") {

        @Override
        public void executeSatelliteOperations() {
            final CacheManager cacheManager = ServiceLocator.getService("realCacheManager", CacheManager.class);
            final CellarPersistentConfiguration configuration = ServiceLocator.getService(CellarPersistentConfiguration.class);
            cacheManager.getCache("embeddedNoticeCache").getCacheConfiguration().setTimeToLiveSeconds(configuration.getCellarServiceEmbeddedNoticeCacheTimeToLiveSeconds());
        }
    }, //
    cellarServiceEmbeddedNoticeCacheEvaluationStatisticsEnabled_Key("cellar.service.embeddedNoticeCache.evaluation.statistics.enabled"), //

    cellarServiceSipDependencyCheckerEnabled_Key("cellar.service.sipDependencyChecker.enabled"),
    cellarServiceSipDependencyCheckerCronSettings_Key("cellar.service.sipDependencyChecker.cron.settings"),
    cellarServiceSipDependencyCheckerPoolThreads_Key("cellar.service.sipDependencyChecker.pool.threads") {

        @Override
        public void executeSatelliteOperations() {
            final SIPWatcher sipWatcher = ServiceLocator.getService(SIPWatcher.class);
            sipWatcher.reconfigureThreadPoolExecutor();
        }
    },
    cellarServiceSipQueueManagerCronSettings_Key("cellar.service.sipQueueManager.cron.settings"),
    cellarServiceSipQueueManagerFileAttributeDateType_Key("cellar.service.sipQueueManager.fileAttribute.dateType"),
    
    /**
     * DORIE
     */
    cellarServiceDorieDedicatedEnabled_Key("cellar.service.dorie.dedicated.enabled"),

    /**
     * CELLAR METS UPLOAD
     */
    cellarMetsUploadCallbackTimeout_Key("cellar.mets.upload.callback.timeout"),

    /**
     * CELLAR STATUS SERVICE
     */
    cellarStatusServiceBaseUrl_Key("cellar.status.service.baseUrl"),
    cellarStatusServicePackageLevelEndpoint_Key("cellar.status.service.package.level.endpoint"),
    cellarStatusServiceObjectLevelEndpoint_Key("cellar.status.service.object.level.endpoint"),
    cellarStatusServiceLookupEndpoint_Key("cellar.status.service.lookup.endpoint"),

    /**
     * Cellar graceful shutdown
     */
    cellarGracefulShutdownEnabled_key("cellar.shutdown.graceful.enabled"),
    cellarGracefulShutdownTimeout_key("cellar.shutdown.graceful.timeout"),


    cellarInferenceOntologyPath("cellar.service.ontology.inference.path"),

    /*
     * CELLAR DATABASE
     */
    cellarDatabaseReadOnly_Key("cellar.database.readonly"), //
    cellarDatabaseRdfPassword_Key("cellar.database.rdf.password"), //
    cellarDatabaseRdfIngestionOptimizationEnabled_Key("cellar.database.rdf.ingestion.optimization.enabled"), //
    cellarDatabaseInClauseParams_Key("cellar.database.inClause.params"), //
    cellarDatabaseAuditEnabled_Key("cellar.database.audit.enabled"), //

    /*
     * CELLAR DATASOURCES
     */
    cellarDatasourceCellar_Key("cellar.datasource.cellar"), //
    cellarDatasourceCmr_Key("cellar.datasource.cmr"), //
    cellarDatasourceIdol_Key("cellar.datasource.idol"), //
    cellarDatasourceVirtuoso_Key("cellar.datasource.virtuoso"), //

    cellarWatchAdviceEnabled("cellar.monitoring.watch.enabled"),

    /*
    * CELLAR FEED
     */
    cellarFeedIngestionDescription_Key("cellar.feed.ingestion.description"),
    cellarFeedEmbargoDescription_Key("cellar.feed.embargo.description"),
    cellarFeedNalDescription_Key("cellar.feed.nal.description"),
    cellarFeedSparqlLoadDescription_Key("cellar.feed.sparqlload.description"),
    cellarFeedOntologyDescription_Key("cellar.feed.ontology.description"),
    cellarFeedIngestionTitle_Key("cellar.feed.ingestion.title"),
    cellarFeedEmbargoTitle_Key("cellar.feed.embargo.title"),
    cellarFeedNalTitle_Key("cellar.feed.nal.title"),
    cellarFeedSparqlLoadTitle_Key("cellar.feed.sparqlload.title"),
    cellarFeedOntologyTitle_Key("cellar.feed.ontology.title"),
    cellarFeedIngestionItemTitle_Key("cellar.feed.ingestion.item.title"),
    cellarFeedEmbargoItemTitle_Key("cellar.feed.embargo.item.title"),
    cellarFeedNalItemTitle_Key("cellar.feed.nal.item.title"),
    cellarFeedSparqlLoadItemTitle_Key("cellar.feed.sparqlload.item.title"),
    cellarFeedOntologyItemTitle_Key("cellar.feed.ontology.item.title"),
    cellarFeedIngestionEntrySummary_Key("cellar.feed.ingestion.entry.summary"),
    cellarFeedEmbargoEntrySummary_Key("cellar.feed.embargo.entry.summary"),
    cellarFeedNalEntrySummary_Key("cellar.feed.nal.entry.summary"),
    cellarFeedOntologyEntrySummary_Key("cellar.feed.ontology.entry.summary"),
    cellarFeedSparqlLoadEntrySummary_Key("cellar.feed.sparqlload.entry.summary"),

    /*
     * S3
     */
    s3Url("s3.url"),
    s3BucketName("s3.bucket"),
    s3RegionName("s3.region"),
    s3CredentialsAccessKey("s3.credentials.access-key"),
    s3CredentialsSecretKey("s3.credentials.secret-key"),
    s3ClientConnectionTimeout("s3.client.connection-timeout"),
    s3ClientSocketTimeout("s3.client.socket-timeout"),
    s3ProxyEnabled("s3.proxy.enabled"),
    s3ProxyHost("s3.proxy.host"),
    s3ProxyPort("s3.proxy.port"),
    s3ProxyUser("s3.proxy.username"),
    s3ProxyPassword("s3.proxy.password"),
    s3Unsafe("s3.unsafe"),
    s3RollbackTag("s3.rollback.tag"),

    s3RetryEnabled_Key("s3.retry.enabled"),

    s3RetryMaxAttempts_Key("s3.retry.max-attempts"),

    s3RetryDelay_Key("s3.retry.delay"),

    s3RetryRandom_Key("s3.retry.random"),

    s3RetryMultiplier_Key("s3.retry.multiplier"),

    s3RetryMaxDelay_Key("s3.retry.max-delay"),

    cellarNamespace_Key("cellar.prefix"),
    s3ServiceIngestionDatastreamRenamingEnabled_Key("s3.service.ingestion.datastream.renaming.enabled") {
        @Override
        public void executeSatelliteOperations() {
            ServiceLocator.getService("existingMetadataLoader", IExistingMetadataLoader.class).restart();
        }
    },

    /*
     * VIRTUOSO
     */
    virtuosoIngestionEnabled_Key("virtuoso.ingestion.enabled"), //
    virtuosoIngestionRetryStrategy_Key("virtuoso.ingestion.retry.strategy"), //
    virtuosoIngestionRetryMaxRetries_Key("virtuoso.ingestion.retry.maxRetries"), //
    virtuosoIngestionRetryCeilingDelay_Key("virtuoso.ingestion.retry.ceilingDelay"), //
    virtuosoIngestionRetryBase_Key("virtuoso.ingestion.retry.base"), //
    virtuosoIngestionTechnicalEnabled_Key("virtuoso.ingestion.technical.enabled"), //
    virtuosoIngestionBackupEnabled_Key("virtuoso.ingestion.backup.enabled"), //
    virtuosoIngestionBackupSchedulerCronSettings_Key("virtuoso.ingestion.backup.scheduler.cron.settings"), //
    virtuosoIngestionBackupConcurrencyControllerEnabled_Key("virtuoso.ingestion.backup.concurrencyController.enabled"), //
    virtuosoIngestionBackupMaxResults_Key("virtuoso.ingestion.backup.max.results"), //
    virtuosoIngestionBackupLogVerbose_Key("virtuoso.ingestion.backup.log.verbose"), //
    virtuosoIngestionNormalizationEnabled_Key("virtuoso.ingestion.normalization.enabled"), //
    virtuosoIngestionSkolemizationEnabled_Key("virtuoso.ingestion.skolemization.enabled"), //
    virtuosoIngestionBackupExceptionMessagesCachePeriod_Key("virtuoso.ingestion.backup.exceptionMessages.cachePeriod"), //
    virtuosoDisseminationSparqlMapperEnabled_Key("virtuoso.dissemination.sparql.mapper.enabled"), //
    virtuosoIngestionLinkrotRealignmentEnabled_Key("virtuoso.ingestion.linkrot.realignment.enabled"), //

    /*
     * TEST
     */
    testEnabled_Key("test.enabled"), //
    testDatabaseEnabled_Key("test.database.enabled"), //
    testDatabaseCronSettings_Key("test.database.cron.settings"), //
    testRollbackEnabled_Key("test.rollback.enabled"), //
    testVirtuosoRollbackEnabled_Key("test.virtuoso.rollback.enabled"),
    testIndexingDelayExecutionEnabled_Key("test.indexing.delayExecution.enabled"),
    testVirtuosoRetryEnabled_Key("test.virtuoso.retry.enabled"),
    testS3RetryEnabled_Key("test.s3.retry.enabled"),
    testProcessMonitorPhase_Key("test.process.monitor.phase"), //
    testProcessMonitorPhaseReached_Key("test.process.monitor.phase.reached"), //

    debugVerboseLoggingEnabled_Key("debug.verbose.logging.enabled"),

    /**
     * EMAIL & EULOGIN
     */
    cellarServiceUserAccessNotificationEmailEnabled_Key("cellar.service.userAccess.notificationEmail.enabled"),
    cellarEmailCron_Key("cellar.service.email.cron"),
	cellarServiceEmailCleanupMaxAgeDays_Key("cellar.service.email.cleanup.maxAge.days"),
    cellarServiceEmailEnabled_Key("cellar.service.email.enabled"),
    cellarServiceEuloginUserMigrationEnabled_Key("cellar.service.eulogin.userMigration.enabled"),
    cellarMailHost_Key("cellar.mail.host"),
    cellarMailPort_Key("cellar.mail.port"),
    cellarMailSmtpUser_Key("cellar.mail.smtp.user"),
    cellarMailSmtpPassword_Key("cellar.mail.smtp.password"),
    cellarMailSmtpAuth_Key("cellar.mail.smtp.auth"),
    cellarMailSmtpStarttlsEnable_Key("cellar.mail.smtp.starttls.enable"),
    cellarMailSmtpStarttlsRequired_Key("cellar.mail.smtp.starttls.required"),
    cellarMailSmtpFrom_Key("cellar.mail.smtp.from"),
    cellarMetsUploadBufferSize_Key("cellar.mets.upload.buffer.size");
    

    /**
     * Gets the property whose key is {@code key}.
     */
    public static CellarConfigurationPropertyKey get(final String key) {
        return CellarConfigurationPropertyKeyLooker.lookup.get(key);
    }

    private final String key;

    CellarConfigurationPropertyKey(final String key) {
        this.key = key;
    }

    /**
     * Executes the satellite operations after the current property has been updated.
     */
    public void executeSatelliteOperations() {
    }

    @Override
    public String toString() {
        return this.key;
    }
}
