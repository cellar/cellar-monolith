package eu.europa.ec.opoce.cellar.cl.controller.util;

import org.springframework.ui.ModelMap;

/**
 * Helper class to convert {@link Throwable} objects into xml error messages.
 *
 * @author phvdveld
 *
 */
public class ThrowableToXmlErrorConvertor {

    /** the template string for xml errors. */
    private static final String XML_ERROR_TEMPLATE = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + "\n" + //
            "<error>" + "\n" + //
            "<id>{id}</id>" + "\n" + //
            "<message>{message}</message>" + "\n" + //
            "<cause>{cause}</cause>" + "\n" + //
            "<description><![CDATA[" + "\n" + //
            "The CELLAR could not satisfy the request as an error with HTTP status of {id} has been generated." + "\n" + //
            "\n" + //
            "What you can do now:" + "\n" + //
            "1) check your request against the error's details shown above" + "\n" + //
            "2) if solution 1) did not fix the problem, please gather further details from the CELLAR's logs" + "\n" + //
            "3) if none of the solutions above fixed the problem, please contact the CELLAR administrator at your premises." + "\n" + //
            "]]></description>" + "\n" + //
            "</error>";

    /**
     * Converts the {@link Throwable} with a given errorCode.
     *
     * @param errorCode
     *            the code to give to the error
     * @param cause
     *            the {@link Throwable} to convert
     * @return a xml formatted String
     */
    public String convert(int errorCode, Throwable cause) {
        final SimpleTemplate simpleTemplate = new SimpleTemplate(XML_ERROR_TEMPLATE);
        simpleTemplate.assign("id", errorCode);
        final String message = cause != null ? cause.getMessage() : "";
        simpleTemplate.assign("message", message);
        final String causeString = cause != null ? cause.toString() : "";
        simpleTemplate.assign("cause", causeString);
        final String result = simpleTemplate.parse();
        return result;
    }

    /**
     * Helper class for simple assignable templates. Assignable values are represented by values
     * between brackets (<code>{aValue}</code>)
     *
     * Example: <blockquote>
     *
     * <pre>
     * Template tpl = new Template("Hello world! This is my message: {myMessage}");
     * tpl.assign("myMessage", "My taylor is rich.");
     * String message = tpl.parse();
     * System.out.println(message);
     * </pre>
     *
     * </blockquote> Will produce: <blockquote>
     *
     * <pre>
     * Hello world! This is my message: My taylor is rich.
     * </pre>
     *
     * </blockquote>
     *
     * @author phvdveld
     *
     */
    private static final class SimpleTemplate {

        /** the source template. */
        private String template;

        public SimpleTemplate(String templateStr) {
            this.template = templateStr;
        }

        /**
         * Assigns the given name to the given value in the template.
         *
         * @param name
         *            the name of the value to be assigned
         * @param value
         *            the value to be assigned
         * @return the {@link Template} to support chaining
         */
        public SimpleTemplate assign(String name, int value) {
            return this.assign(name, Integer.toString(value));
        }

        /**
         * Assigns the given name to the given value in the template.
         *
         * @param name
         *            the name of the value to be assigned
         * @param value
         *            the value to be assigned
         * @return the {@link Template} to support chaining
         */
        public SimpleTemplate assign(String name, Object value) {
            final String replacingString = value == null ? "Unknown" : value.toString();
            this.template = this.template.replace("{" + name + "}", replacingString);
            return this;
        }

        /**
         * Gets the parsed template representation.
         *
         * @return a String
         */
        public String parse() {
            return template;
        }

        /**
         * Assigns all the names in the model map to their associated value.
         *
         * @param model
         *            the {@link ModelMap} to assign
         * @return the {@link Template} to support chaining
         */
        @SuppressWarnings("unused")
        public SimpleTemplate assign(ModelMap model) {
            model.entrySet().forEach(entry->this.assign(entry.getKey(), entry.getValue())); 
            return this;
        }
    }
}
