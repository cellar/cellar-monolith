/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.service.client
 *        FILE : IIndexingService.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 13-03-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.client;

import eu.europa.ec.opoce.cellar.common.util.EnumUtils;

import java.util.Comparator;
import java.util.stream.Stream;

/**
 * <class_description> Interface for the services meant to manage indexation.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 02-04-2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IIndexingService {

    Priority getMinimalLevelOfPriorityHandled();

    void setMinimalLevelOfPriorityHandled(final Priority minimalLevelOfPriorityToBeHandled);

    boolean isIndexingEnabled();

    void setIndexingEnabled(final boolean indexing);

    INDEXING_SERVICE_STATUS getIndexingStatus();

    boolean isIndexingRunning();

    void restartFailedCmrIndexRequests();

    /**
     * Gets the count index requests to handle.
     *
     * @return the count index requests to handle
     */
    long getCountIndexRequestsToHandle();

    /**
     * Gets the count failed index requests.
     *
     * @return the count failed index requests
     */
    long getCountFailedIndexRequests();

    enum INDEXING_SERVICE_STATUS {

        /**
         * The indexing.
         */
        INDEXING, /**
         * The not indexing.
         */
        NOT_INDEXING, /**
         * The stopping.
         */
        STOPPING;
    }

    /**
     * The Enum Priority.
     */
    enum Priority {
        //remember priority value is used to determine order
        /**
         * The high.
         */
        HIGH(3),

        /**
         * The normal.
         */
        NORMAL(2),

        /**
         * The low.
         */
        LOW(1),
        
        /**
         * The lowest.
         */
        LOWEST(0);

        /**
         * The priority.
         */
        private final int priority;

        /**
         * Instantiates a new priority.
         *
         * @param priority the priority
         */
        Priority(final int priority) {
            this.priority = priority;
        }

        /**
         * Find by priority value.
         *
         * @param priority the priority
         * @return the priority
         */
        public static Priority findByPriorityValue(final int priority) {
            for (final Priority minimalLevel : values()) {
                if (priority == minimalLevel.priority) {
                    return minimalLevel;
                }
            }

            return null;
        }

        public static Priority resolve(final String key) {
            return EnumUtils.resolve(Priority.class, key, LOWEST);
        }

        /**
         * Gets the priority value.
         *
         * @return the priority value
         */
        public int getPriorityValue() {
            return priority;
        }

        /**
         * Gets the name.
         *
         * @return the name
         */
        public String getName() {
            return name();
        }
    }

    /**
     * The Enum LinkedIndexingBehavior.
     */
    enum LinkedIndexingBehavior {
        //remember priorityvalue is used to determine order
        NONE(0) {// no indexation at all: do not add any linked documents to the indexation

            @Override
            public Priority transformToPriority(final int originalPriority) {
                return null;
            }
        },
        LOWEST(1) { // [default] index at lowest priority

            @Override
            public Priority transformToPriority(final int originalPriority) {
                return Stream.of(Priority.values())
                        .sorted(Comparator.comparingInt(Priority::getPriorityValue))
                        .findFirst()
                        .orElse(null);
            }
        },
        SAME(2) { // index at same priority (standard in the past)

            @Override
            public Priority transformToPriority(final int originalPriority) {
                return Priority.findByPriorityValue(originalPriority);
            }
        };

        private final int priority;

        LinkedIndexingBehavior(int priority) {
            this.priority = priority;
        }

        public static LinkedIndexingBehavior findByPriorityValue(final int priority) {
            for (final LinkedIndexingBehavior linkedIndexingBehavior : values()) {
                if (priority == linkedIndexingBehavior.priority) {
                    return linkedIndexingBehavior;
                }
            }
            return null;
        }

        public int getPriorityValue() {
            return priority;
        }

        /**
         * Transform to priority.
         *
         * @param originalPriority the original priority
         * @return the priority
         */
        public abstract Priority transformToPriority(final int originalPriority);
    }
}
