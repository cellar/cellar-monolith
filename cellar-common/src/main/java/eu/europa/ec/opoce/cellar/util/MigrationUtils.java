package eu.europa.ec.opoce.cellar.util;

import org.apache.commons.lang.StringUtils;

import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentStream;

public class MigrationUtils {

    /** The Constant CONTENT_PREFIX. */
    private static final String CONTENT_PREFIX = "DOC_";

    public static String getContentStreamSourceId(final ContentStream contentStream, final int docIndex) {
        String result = null;

        final ContentIdentifier cellarId = contentStream.getCellarId();
        if (cellarId != null) {//ingestion of exported SIPs
            final String identifier = cellarId.getIdentifier();
            if (StringUtils.isNotBlank(identifier)) {
                if (identifier.contains(CONTENT_PREFIX)) {
                    final int indexOf = StringUtils.indexOf(identifier, CONTENT_PREFIX);
                    result = identifier.substring(indexOf);
                }
            }
        }
        if (result == null) {
            // TODO: what is this service ?
/*
            final boolean enabled = this.s3Configuration.getS3ServiceIngestionDatastreamRenamingEnabled();
            if (!enabled) {
                result = contentStream.getLabel();
            } else {
*/
                result = CONTENT_PREFIX + docIndex;
            //}
        }
        return result;
    }

}
