package eu.europa.ec.opoce.cellar.common;

import org.apache.xerces.util.XMLCatalogResolver;

public interface CatalogService {

    XMLCatalogResolver getCatalog();
}
