/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.util
 *             FILE : DigitalObjectUtils.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 10, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.util;

import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentStream;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 10, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class DigitalObjectUtils {

    /**
     * Adds the production identifiers.
     *
     * @param productionIdentifers the production identifers
     * @param digitalObject the digital object
     */
    public static void addProductionIdentifiers(final Collection<String> productionIdentifers, final DigitalObject digitalObject) {
        for (final ContentIdentifier ci : digitalObject.getContentids()) {
            productionIdentifers.add(ci.getIdentifier());
        }
    }

    /**
     * Extract production identifiers.
     *
     * @param digitalObject the digital object
     * @return the list
     */
    public static List<String> extractProductionIdentifiers(final DigitalObject digitalObject) {
        final List<String> productionIdentifers = new LinkedList<String>();

        for (final ContentIdentifier ci : digitalObject.getContentids()) {
            productionIdentifers.add(ci.getIdentifier());
        }

        return productionIdentifers;
    }

    /**
     * Checks if is under embargo.
     *
     * @param digitalObject the digital object
     * @return true, if is under embargo
     */
    public static final boolean isUnderEmbargo(final DigitalObject digitalObject) {
        final Date embargoDate = getInheritedEmbargoDate(digitalObject);
        return (embargoDate != null) && embargoDate.after(new Date());
    }

    /**
     * Gets the embargo date.
     *
     * @param digitalObject the digital object
     * @return the embargo date
     */
    public static Date getInheritedEmbargoDate(final DigitalObject digitalObject) {
        Date embargoDate = null;
        if (digitalObject != null) {
            embargoDate = digitalObject.getEmbargoDate();
        }
        if (embargoDate == null) {
            final DigitalObject parent = digitalObject.getParentObject();
            if (parent != null) {
                embargoDate = getInheritedEmbargoDate(parent);
            }
        }
        return embargoDate;
    }

    /**
     * Gets the parents.
     *
     * @param digitalObject the digital object
     * @return the parents
     */
    public static List<DigitalObject> getParents(final DigitalObject digitalObject) {
        final List<DigitalObject> result = new ArrayList<DigitalObject>();
        final DigitalObject parentObject = digitalObject.getParentObject();
        if (parentObject != null) {
            result.add(parentObject);
            result.addAll(getParents(parentObject));
        }
        return result;
    }

    /**
     * Sort list - item first (ITEM -> WORK)
     * In some operations the order of the digital object list must be fixed
     * When updating the embargo date in cellar resource md objects for example
     * If the embargo date of the parent node should be set on the child  
     * @param digitalObjectList the digital object list
     * @return the list
     */
    public static List<DigitalObject> sortListItemFirst(final Collection<DigitalObject> digitalObjectList) {
        final Comparator<DigitalObject> comparator = (do1, do2) -> {
            final String identifier1 = do1.getCellarId().getIdentifier();
            final String identifier2 = do2.getCellarId().getIdentifier();

            return identifier2.compareTo(identifier1);
        };

        return digitalObjectList.stream().sorted(comparator).collect(Collectors.toList());
    }

    /**
     * Gets the content streams.
     *
     * @param digitalObject the digital object
     * @return the content streams
     */
    public static Set<ContentStream> getContentStreams(final DigitalObject digitalObject) {
        final Set<ContentStream> result = new HashSet<ContentStream>();
        digitalObject.getAllChilds(true).stream().filter(element -> element.getContentStreams() != null).forEach(element -> {
            result.addAll(element.getContentStreams());
        });
        return result;
    }

    /**
     * Gets the all manifestations.
     *
     * @param digitalObject the digital object
     * @return the all manifestations
     */
    public static Set<DigitalObject> getAllManifestations(final DigitalObject digitalObject) {
        final Set<DigitalObject> result = digitalObject.getAllChilds(true).stream()
                .filter(element -> DigitalObjectType.MANIFESTATION.equals(element.getType())).collect(Collectors.toSet());
        return result;
    }
}
