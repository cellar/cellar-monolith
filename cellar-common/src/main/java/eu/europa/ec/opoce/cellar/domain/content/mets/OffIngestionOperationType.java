/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.domain.content.mets
 *             FILE : NotOperationType.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 19-12-2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.domain.content.mets;

import eu.europa.ec.opoce.cellar.cl.concurrency.Concurrent.Locker;
import eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLockSession;
import eu.europa.ec.opoce.cellar.exception.NotImplementedExceptionBuilder;

/**
 * <class_description> Enumeration defining processes that are not operation of ingestion.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 19-12-2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public enum OffIngestionOperationType implements Locker {

    EMBARGO() {

        @Override
        public void doLock(final ICellarLockSession cellarLockSession, final Object... args) throws InterruptedException {
            this.getConcurrencyController().lockForEmbargo(cellarLockSession, (String) args[0]);
        }
    },
    CLEANING() {

        @Override
        public void doLock(final ICellarLockSession cellarLockSession, final Object... args) throws InterruptedException {
            this.getConcurrencyController().lockForCleaning(cellarLockSession, (String) args[0]);
        }
    },
    CLEANING_SCHEDULING() {

        @Override
        public void doLock(final ICellarLockSession cellarLockSession, final Object... args) throws InterruptedException {
            this.getConcurrencyController().lockForCleaningScheduling(cellarLockSession, (String) args[0]);
        }
    },
    ALIGNING_SCHEDULING() {

        @Override
        public void doLock(final ICellarLockSession cellarLockSession, final Object... args) throws InterruptedException {
            this.getConcurrencyController().lockForAligningScheduling(cellarLockSession, (String) args[0]);
        }
    },
    ALIGNING() {

        @Override
        public void doLock(final ICellarLockSession cellarLockSession, final Object... args) throws InterruptedException {
            this.getConcurrencyController().lockForAligning(cellarLockSession, (String) args[0]);
        }
    },
    INDEXING() {

        @Override
        public void doLock(final ICellarLockSession cellarLockSession, final Object... args) throws InterruptedException {
            this.getConcurrencyController().lockForIndexing(cellarLockSession, (String) args[0]);
        }
    },
    VIRTUOSO_BACKUP() {

        @Override
        public void doLock(final ICellarLockSession cellarLockSession, final Object... args) throws InterruptedException {
            this.getConcurrencyController().lockForVirtuosoBackup(cellarLockSession, (String) args[0]);
        }
    },
    EXPORT() {

        @Override
        public void doLock(final ICellarLockSession cellarLockSession, final Object... args) throws InterruptedException {
            this.getConcurrencyController().lockForExport(cellarLockSession, (String) args[0]);
        }
    },
    UNKNOWN() {

        @Override
        public void doLock(final ICellarLockSession cellarLockSession, final Object... args) throws InterruptedException {
            throw NotImplementedExceptionBuilder.get().withMessage("Lock strategy non implemented for operation {}.")
                    .withMessageArgs(UNKNOWN).build();
        }
    },
    EMBEDDED_NOTICE_SYNCHRONIZATION() {

        @Override
        public void doLock(final ICellarLockSession cellarLockSession, final Object... args) throws InterruptedException {
            this.getConcurrencyController().lockForEmbeddedNoticeSynchronization(cellarLockSession, (String) args[0]);
													  
        }
    },
	 VIRTUOSO_CLEANUP() {

        @Override
        public void doLock(final ICellarLockSession cellarLockSession, final Object... args) throws InterruptedException {
            this.getConcurrencyController().lockForVirtuosoBackup(cellarLockSession, (String) args[0]);
        }
    };
}