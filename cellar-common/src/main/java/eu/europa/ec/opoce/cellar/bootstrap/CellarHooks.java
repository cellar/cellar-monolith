package eu.europa.ec.opoce.cellar.bootstrap;

/**
 * @author ARHS Developments
 */
public final class CellarHooks {

    public static final String CELLAR_DEBUG_KEY = "cellarDebug";

    private static String debugContext = "";

    private CellarHooks() {
    }

    public static void setDebugContext(String context) {
        debugContext = context;
    }

    public static String getDebugContext() {
        return debugContext;
    }
}
