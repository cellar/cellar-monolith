/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.dao.datatable
 *             FILE : AbstractJdbcSupportDatatableDao.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 1 juil. 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao.datatable;

import eu.europa.ec.opoce.cellar.cl.domain.datatable.DatatableColumn;
import eu.europa.ec.opoce.cellar.cmr.database.DaoObject;
import eu.europa.ec.opoce.cellar.common.util.Pair;

import java.io.Serializable;
import java.util.HashMap;

/**
 * The Class AbstractJdbcSupportDatatableDao.
 * <class_description> .
 * <br/><br/>
 * <notes> .
 * <br/><br/>
 * ON : 1 juil. 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
//@Repository
public abstract class AbstractJdbcSupportDatatableDao<OBJ extends DaoObject, ID extends Serializable> { //extends SpringBaseDao<OBJ, ID> implements DatatableDao {

    /**
     * Builds the where clause.
     *
     * @param columns the columns
     * @param buffer the buffer
     * @param startPage the start page
     */
    protected void buildWhereClause(final DatatableColumn[] columns, final StringBuffer buffer, final Integer startPage) {
        if (this.isAtLeastOneValueNotNull(columns)) {
            //create where clause
            int count = 0;//number of parameters different than 0 to add the AND clause
            buffer.append(" WHERE ");

            for (final DatatableColumn column : columns) {
                if (column.isSearchable() && column.getSearchValue() != null) {
                    if (count > 0) {
                        buffer.append(" AND ");
                    }
                    if (column.getSearchValue() instanceof Pair) {//dates
                        buffer.append(column.getName() + " BETWEEN :" + column.getName() + "_start AND :" + column.getName() + "_end");
                    } else {
                        buffer.append(column.getName() + " LIKE :" + column.getName());
                    }

                    count++;
                }
            }
            if (startPage != null) {
                if (count > 0) {
                    buffer.append(" AND ");
                }
                buffer.append(" ROWNUM = " + startPage);
            }
        }
    }

    /**
     * get the parameters.
     *
     * @param columns the columns
     * @param queryObject the query object
     */
    protected HashMap<String, Object> getParameters(final DatatableColumn[] columns) {
        final HashMap<String, Object> result = new HashMap<String, Object>();
        if (this.isAtLeastOneValueNotNull(columns)) {
            for (final DatatableColumn column : columns) {
                if (column.isSearchable() && column.getSearchValue() != null) {
                    if (column.getSearchValue() instanceof Pair) {//dates
                        final Pair<?,?> pair = (Pair<?,?>) column.getSearchValue();
                        result.put(column.getName() + "_start", pair.getOne());
                        result.put(column.getName() + "_end", pair.getTwo());
                    } else {
                        result.put(column.getName(), column.getSearchValue());
                    }
                }
            }
        }
        return result;
    }

    /**
     * Checks if is at least one value not null.
     *
     * @param columns the columns
     * @return true, if is at least one value not null
     */
    protected boolean isAtLeastOneValueNotNull(final DatatableColumn[] columns) {
        boolean result = false;
        for (final DatatableColumn column : columns) {
            if (column.getSearchValue() != null) {
                result = true;
                break;
            }
        }
        return result;
    }
    /**
     * implement the following on the "jdbc suppport based" entity dao
     * */
    //  @Override
    //    public List<String[]> findAll(final DatatableColumnsWrapper columnsWrapper, final int startPage, final int pageSize,
    //            final ResultTransformer resultTransformer) {
    //
    //        final DatatableColumn[] columns = columnsWrapper.getColumns();
    //        final StringBuffer buffer = new StringBuffer("select xpto from ");
    //        buffer.append(columnsWrapper.getEntityName());
    //        buffer.append(" xpto");
    //        this.buildWhereClause(columns, buffer, startPage);
    //        buffer.append(" ORDER BY ");
    //        buffer.append(columns[columnsWrapper.getSortByColumnIndex()].getName());
    //        buffer.append(" ");
    //        buffer.append(columnsWrapper.getSortOrder().toUpperCase());
    //        final HashMap<String, Object> parameters = getParameters(columns);
    //
    //        final JdbcTemplate jdbcTemplate = getJdbcTemplate();
    //        jdbcTemplate.setMaxRows(pageSize);
    //        jdbcTemplate.setFetchSize(pageSize);
    //        final String query = buffer.toString();
    //        final List<Archive> records = getNamedParameterJdbcTemplate().query(query, parameters, this);
    //        final List<String[]> result = new ArrayList<String[]>();
    //        for (final Archive archive : records) {
    //            final String[] row = new String[] {
    //                    archive.getSipName(), archive.getProductionIdentifier(), simpleDateFormat.format(archive.getArchivingTime()),
    //                    simpleDateFormat.format(archive.getIngestionTime()).toString(), archive.getPriority(),
    //                    Boolean.toString(archive.isFailed())};
    //            result.add(row);
    //        }
    //        return result;
    //    }

    //    @Override
    //    public Long count(final DatatableColumnsWrapper columnsWrapper) {
    //        final DatatableColumn[] columns = columnsWrapper.getColumns();
    //        final StringBuffer buffer = new StringBuffer("select count(*) from ");
    //        buffer.append(columnsWrapper.getEntityName());
    //        this.buildWhereClause(columns, buffer, null);
    //        buffer.append(" ORDER BY ");
    //        buffer.append(columns[columnsWrapper.getSortByColumnIndex()].getName());
    //        buffer.append(" ");
    //        buffer.append(columnsWrapper.getSortOrder().toUpperCase());
    //        final HashMap<String, Object> parameters = getParameters(columns);
    //        final String query = buffer.toString();
    //        final SqlRowSet queryForRowSet = getNamedParameterJdbcTemplate().queryForRowSet(query, parameters);
    //        return queryForRowSet.getLong(0);
    //    }

    //    @Override
    //    public Long countAll(final DatatableColumnsWrapper columnsWrapper) {
    //        final StringBuffer buffer = new StringBuffer("select count(*) from ");
    //        buffer.append(columnsWrapper.getEntityName());
    //        final JdbcTemplate jdbcTemplate = getJdbcTemplate();
    //        final String query = buffer.toString();
    //        final SqlRowSet queryForRowSet = jdbcTemplate.queryForRowSet(query);
    //        return queryForRowSet.getLong(0);
    //    }
}
