package eu.europa.ec.opoce.cellar.cl.dao;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.springframework.dao.DataAccessException;

/**
 * A basic dao.
 *
 * @author dsavares
 *
 * @param <ENTITY> the entity's type to be handled by this DAO
 * @param <ID> the id type for the serial ID of the entity
 */
public interface BaseDao<ENTITY, ID extends Serializable> {

    /**
     * Return all persistent instances of this entity's type.
     *
     * @return a {@link List} containing 0 or more persistent instances.
     */
    List<ENTITY> findAll();

    /**
     * Return the persistent instance of of this entity's type with the given identifier,
     * or null if not found.
     *
     * @param id the identifier of the persistent instance
     * @return the persistent instance, or null if not found
     */
    ENTITY getObject(final ID id);

    /**
     * Persist the given transient instance.
     *
     * @param object the transient instance to be persisted
     * @return the persisted object
     */
    ENTITY saveOrUpdateObject(final ENTITY object);

    /**
     * Persist the given transient instance.
     *
     * @param object the transient instance to be persisted
     * @return the saved object
     */
    ENTITY saveObject(final ENTITY object);

    /**
     * Update the given persistent instance, associating it with the current Hibernate
     * {@link Session}.
     *
     * @param object the persistent instance to update
     * @return the updated object
     */
    ENTITY updateObject(final ENTITY object);

    /**
     * Update or insert the given persistent instance according to the existence of the given value for unique column {@link uniqueColumnName},
     * and associating it with the current Hibernate {@link Session}.
     *
     * @param object the persistent instance to update
     * @return the updated object
     */
    ENTITY upsertObject(final ENTITY daoObject, final String uniqueColumnName);

    /**
     * Delete the given persistent instance.
     *
     * @param object the persistent instance to delete
     */
    void deleteObject(final ENTITY object);

    /**
     * Delete the given persistent instances.
     *
     * @param objects the persistent instances to delete
     */
    void deleteObjects(Collection<ENTITY> objects);

    /**
     * Delete all of the given persistent instances.
     *
     * @return the number of entities deleted.
     */
    int deleteAll();

    /**
     * Method for executing named queries that issue update or delete statements.
     * @param queryName The query name.
     * @param params The named parameters.
     * @return the number of rows that were updated or deleted.
     */
    int updateObjectsByNamedQueryWithNamedParams(final String queryName, final Map<String, Object> params);
    
    /**
     * Convenience finder methods for named queries that should return a single object.
     *
     * @param queryName The query name.
     * @param values The 0-based positional parameters.
     * @return The Entity corresponding to the query result if there is a single result, null if
     *         there is no result.
     * @throws DataAccessException If there is more than one result.
     */
    ENTITY findObjectByNamedQuery(final String queryName, final Object... values) throws DataAccessException;

    /**
     * Convenience finder methods for named queries that should return a single object.<br />
     * The parameters are passed via a map containing the parameter's name as key, and the parameter's value as value.
     *
     * @param queryName The query name.
     * @param params The named parameters.
     * @return The Entity corresponding to the query result if there is a single result, null if
     *         there is no result.
     * @throws DataAccessException If there is more than one result.
     */
    ENTITY findObjectByNamedQueryWithNamedParams(final String queryName, final Map<String, Object> params) throws DataAccessException;

    /**
     * Convenience finder methods for named queries that should return a list of objects.
     *
     * @param queryName The query name.
     * @param values The 0-based positional parameters.
     * @return The list of objects corresponding to the query result if there is a single result,
     *         null if there is no result.
     * @throws DataAccessException If there is more than one result.
     */
    List<?> findObjectsByNamedQuery(final String queryName, final Object... values) throws DataAccessException;

    /**
     * Convenience finder methods for named queries that should return a list of objects.<br />
     * The parameters are passed via a map containing the parameter's name as key, and the parameter's value as value.
     *
     * @param queryName The query name.
     * @param params The named parameters.
     * @return The list of objects corresponding to the query result if there is a single result,
     *         null if there is no result.
     * @throws DataAccessException If there is more than one result.
     */
    List<?> findObjectsByNamedQueryWithNamedParams(final String queryName, final Map<String, Object> params) throws DataAccessException;

    /**
     * Convenience finder methods for named queries that should return a list of objects.<br />
     * The parameters are passed via a map containing the parameter's name as key, and the parameter's value as value.
     *
     * @param queryName The query name.
     * @param params The named parameters.
     * @param startIndex The startIndex.
     * @param endIndex TheendIndex.
     * @return The list of objects corresponding to the query result if there is a single result,
     *         null if there is no result.
     * @throws DataAccessException If there is more than one result.
     */
    List<?> findObjectsByNamedQueryWithNamedParamsLimit(final String queryName, final Map<String, Object> params, final int startIndex,
            final int endIndex) throws DataAccessException;

    /**
     * Convenience finder methods for named queries that should return a list of objects.<br />
     * The underlying query is supposed to contain a param of type list with name {@link paramsName}:
     * this list is the object that contains the actual params, which are meant to be paged by factor {@link paramsPageSize}.
     *
     * @param queryName The query name.
     * @param params The list of parameters.
     * @param paramsName The name of the list.
     * @param paramsPageSize The paging factor.
     * @return The list of objects corresponding to the query result.
     * @throws DataAccessException If there is more than one result.
     */
    List<?> findObjectsByNamedQueryWithPagedParams(final String queryName, final String paramsName, final List<String> params,
            final int paramsPageSize);

    /**
     * Convenience finder methods for named queries that should return a list of objects.
     *
     * @param queryName The query name.
     * @param first First row index.
     * @param last Last row index.
     * @param values The 0-based positional parameters.
     * @return The list of objects corresponding to the query result if there is a single result,
     *         null if there is no result.
     * @throws DataAccessException If there is more than one result.
     */
    List<?> findPaginatedObjectsByNamedQuery(final String queryName, final int first, final int last, final Object... values)
            throws DataAccessException;

    /**
     * Convenience finder methods for named queries that should return a list of objects.<br />
     * The parameters are passed via a map containing the parameter's name as key, and the parameter's value as value.
     *
     * @param queryName The query name.
     * @param first First row index.
     * @param last Last row index.
     * @param params The named parameters.
     * @return The list of objects corresponding to the query result if there is a single result,
     *         null if there is no result.
     * @throws DataAccessException If there is more than one result.
     */
    List<?> findObjectsByNamedQueryWithNamedParams(final String queryName, final int first, final int last,
            final Map<String, Object> params) throws DataAccessException;

    /**
     * Convenience finder methods for named queries that should return a list of objects.
     *
     * @param queryName The query name.
     * @return The list of objects corresponding to the query result if there is a single result,
     *         null if there is no result.
     */
    List<?> findByNamedQuery(String queryName);

}
