/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service
 *             FILE : ScheduledReindexationService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 13 sept. 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service;

import eu.europa.ec.opoce.cellar.cl.domain.index.ScheduledReindexation;

/**
 * The Interface ScheduledReindexationService.
 * <class_description> The Service class interface to access the ScheduledReindexation DAO
 * <br/><br/>
 * ON : 13 sept. 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface ScheduledReindexationService {

    /**
     * Gets the scheduled reindexation configuration.
     *
     * @return the scheduled reindexation configuration
     */
    ScheduledReindexation getScheduledReindexationConfiguration();

    /**
     * Save or update.
     *
     * @param scheduledReindexation the scheduled reindexation
     * @return the scheduled reindexation
     */
    ScheduledReindexation saveOrUpdate(ScheduledReindexation scheduledReindexation);

    /**
     * Checks if is valid.
     *
     * @param scheduledReindexation the scheduled reindexation
     * @return true, if is valid
     */
    boolean isValid(ScheduledReindexation scheduledReindexation);

}
