package eu.europa.ec.opoce.cellar.domain.content.mets;

import java.util.Map;

public interface PrefixConfiguration {

    String getPrefixUri(String prefix);

    Map<String, String> getPrefixUris();
}
