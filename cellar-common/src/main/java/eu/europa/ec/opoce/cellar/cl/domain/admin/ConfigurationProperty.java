package eu.europa.ec.opoce.cellar.cl.domain.admin;

import javax.persistence.*;

/**
 * This object represents a Configuration Property.
 * 
 * @author dbacquel
 * 
 */
@Entity
@Table(name = "CONFIGURATION_PROPERTY")
@NamedQueries({
        @NamedQuery(name = "getByKey", query = "from ConfigurationProperty where PROPERTY_KEY = ?0"),
        @NamedQuery(name = "findAllOrderedByKey", query = "select cp from ConfigurationProperty cp order by propertyKey asc")})
public class ConfigurationProperty {

    /**
     * Internal id.
     */
    private Long id;

    /**
     * The name of the property.
     */
    public String propertyKey;

    /**
     * The value of the property 
     */
    public String propertyValue;

    public ConfigurationProperty() {
        super();
    }

    /**
     * @param propertyKey
     * @param propertyValue
     */
    public ConfigurationProperty(String propertyKey, String propertyValue) {
        super();
        this.propertyKey = propertyKey;
        this.propertyValue = propertyValue;
    }

    /**
     * Get the id.
     * 
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    /**
     * Sets a new value for the id.
     * 
     * @param id
     *            the id to set
     * @return a reference to the current instance for chaining
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the propertyKey
     */
    @Column(name = "PROPERTY_KEY")
    public String getPropertyKey() {
        return propertyKey;
    }

    /**
     * @param propertyKey 
     * the propertyKey to set
     */
    public void setPropertyKey(String propertyKey) {
        this.propertyKey = propertyKey;
    }

    /**
     * @return the propertyValue
     */
    @Column(name = "PROPERTY_VALUE")
    public String getPropertyValue() {
        return propertyValue;
    }

    /**
     * @param propertyValue the propertyValue to set
     */
    public void setPropertyValue(String propertyValue) {
        this.propertyValue = propertyValue;
    }
}
