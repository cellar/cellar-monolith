package eu.europa.ec.opoce.cellar.common;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.context.support.XmlWebApplicationContext;

/**
 * ServiceLocator is a class that can be used to find services that are defined within the springcontext it is loaded in
 */
public class ServiceLocator implements ApplicationContextAware {

    /**
     * Constant <code>applicationContext</code>
     */
    private static ApplicationContext applicationContext;

    /**
     * {@inheritDoc}
     */
    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        ServiceLocator.applicationContext = applicationContext;
    }

    /**
     * find a service
     *
     * @param name  name under wich the service is known, this to avoid confusion between 2 possible candidates
     * @param clazz the class of which the return service needs to be an instance of
     * @param <T>   a T object.
     * @return the service
     */
    public static <T> T getService(String name, Class<T> clazz) {
        refreshContext();
        final T bean = applicationContext.getBean(name, clazz);
        return bean;
    }

    /**
     * find a service
     *
     * @param clazz the class of which the return service needs to be an instance of
     * @param <T>   a T object.
     * @return the service
     */
    public static <T> T getService(Class<T> clazz) {
        refreshContext();
        final T bean = applicationContext.getBean(clazz);
        return bean;
    }

    /**
     * Refresh context.
     * java.lang.IllegalStateException: BeanFactory not initialized or already closed - call 'refresh' before accessing beans via the ApplicationContext
     */
    private static void refreshContext() {
        final XmlWebApplicationContext xmlWebApplicationContext = (XmlWebApplicationContext) applicationContext;
        final boolean active = xmlWebApplicationContext.isActive();
        if (!active && xmlWebApplicationContext.isRunning()) {
            xmlWebApplicationContext.refresh();
        }
    }
}
