/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.concurrency
 *             FILE : PausableThreadPoolExecutor.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 22 juin 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.concurrency;

import java.io.Serializable;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * The Class PausableThreadPoolExecutor.
 * <class_description> A pausable ThreadPoolExecutor object.
 * <br/><br/>
 * <notes> https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ThreadPoolExecutor.html.
 * <br/><br/>
 * ON : 22 juin 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class PausableThreadPoolExecutor extends ThreadPoolExecutor implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 6275904157147774817L;

    /** The is paused. */
    private final AtomicBoolean isPaused = new AtomicBoolean(false);

    /** The pause lock. */
    private final ReentrantLock pauseLock = new ReentrantLock();

    /** The unpaused. */
    private final Condition unpaused = pauseLock.newCondition();

    /**
     * Instantiates a new pausable thread pool executor.
     *
     * @param corePoolSize the core pool size
     * @param maximumPoolSize the maximum pool size
     * @param keepAliveTime the keep alive time
     * @param unit the unit
     * @param workQueue the work queue
     * @param handler the handler
     */
    public PausableThreadPoolExecutor(final int corePoolSize, final int maximumPoolSize, final long keepAliveTime, final TimeUnit unit,
            final BlockingQueue<Runnable> workQueue, final RejectedExecutionHandler handler) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, handler);
    }

    /**
     * Instantiates a new pausable thread pool executor.
     *
     * @param corePoolSize the core pool size
     * @param maximumPoolSize the maximum pool size
     * @param keepAliveTime the keep alive time
     * @param unit the unit
     * @param workQueue the work queue
     * @param threadFactory the thread factory
     * @param handler the handler
     */
    public PausableThreadPoolExecutor(final int corePoolSize, final int maximumPoolSize, final long keepAliveTime, final TimeUnit unit,
            final BlockingQueue<Runnable> workQueue, final ThreadFactory threadFactory, final RejectedExecutionHandler handler) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, handler);
    }

    /**
     * Instantiates a new pausable thread pool executor.
     *
     * @param corePoolSize the core pool size
     * @param maximumPoolSize the maximum pool size
     * @param keepAliveTime the keep alive time
     * @param unit the unit
     * @param workQueue the work queue
     * @param threadFactory the thread factory
     */
    public PausableThreadPoolExecutor(final int corePoolSize, final int maximumPoolSize, final long keepAliveTime, final TimeUnit unit,
            final BlockingQueue<Runnable> workQueue, final ThreadFactory threadFactory) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory);
    }

    /**
     * Instantiates a new pausable thread pool executor.
     *
     * @param corePoolSize the core pool size
     * @param maximumPoolSize the maximum pool size
     * @param keepAliveTime the keep alive time
     * @param unit the unit
     * @param workQueue the work queue
     */
    public PausableThreadPoolExecutor(final int corePoolSize, final int maximumPoolSize, final long keepAliveTime, final TimeUnit unit,
            final BlockingQueue<Runnable> workQueue) {
        super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
    }

    /**
     * Before execute.
     *
     * @param t the t
     * @param r the r
     */
    @Override
    protected void beforeExecute(final Thread t, final Runnable r) {
        super.beforeExecute(t, r);
        pauseLock.lock();
        try {
            while (isPaused.get()) {
                unpaused.await();
            }
        } catch (final InterruptedException ie) {
            t.interrupt();
        } finally {
            pauseLock.unlock();
        }
    }

    /**
     * Pause.
     */
    public void pause() {
        pauseLock.lock();
        try {
            isPaused.set(true);
        } finally {
            pauseLock.unlock();
        }
    }

    /**
     * Resume.
     */
    public void resume() {
        pauseLock.lock();
        try {
            isPaused.set(false);
            unpaused.signalAll();
        } finally {
            pauseLock.unlock();
        }
    }

    /**
     * Checks if is paused.
     *
     * @return true, if is paused
     */
    public boolean isPaused() {
        return isPaused.get();
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return super.toString() + "[is paused:" + isPaused.get() + "]";
    }
}
