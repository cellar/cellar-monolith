/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.httpHeaders
 *             FILE : CacheControl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 22, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.http.headers.directives;

/**
 * <class_description> This enum holds all used values for the Cache-Control header 
 * <br/><br/>
 * <notes> All 
 * <br/><br/>headers must be compliant with Http 1.1
 * ON : Jan 22, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public enum CacheControl {

    /** The no store header. */
    NO_STORE("no-store"),
    /** The must re-validate header. */
    //MUST_REVALIDATE("must-revalidate"),
    /** The no cache header. */
    NO_CACHE("no-cache");

    /** The directive. */
    private String directive;

    /**
     * Instantiates a new cache control.
     *
     * @param directive the directive
     */
    private CacheControl(final String directive) {
        this.directive = directive;
    }

    /**
     * Gets the directive.
     *
     * @return the directive
     */
    public String getDirective() {
        return this.directive;
    }
}
