/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.datatable
 *             FILE : DatatableColumn.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 07-Mar-2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.datatable;

/**
 * The Class DatatableColumn.
 */
public class DatatableColumn {

    /** The searchable. */
    private boolean searchable;

    /** The index. */
    private int index;

    /** The name. */
    private String name;

    private String dbName;

    /** The search value. */
    private Object searchValue;

    private String comparisonType;

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * @return the dbName
     */
    public String getDbName() {
        return dbName;
    }

    /**
     * @param dbName the dbName to set
     */
    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    /**
     * Checks if is searchable.
     *
     * @return true, if is searchable
     */
    public boolean isSearchable() {
        return this.searchable;
    }

    /**
     * Sets the searchable.
     *
     * @param searchable the new searchable
     */
    public void setSearchable(final boolean searchable) {
        this.searchable = searchable;
    }

    /**
     * Gets the index.
     *
     * @return the index
     */
    public int getIndex() {
        return this.index;
    }

    /**
     * Sets the index.
     *
     * @param index the new index
     */
    public void setIndex(final int index) {
        this.index = index;
    }

    /**
     * Gets the search value.
     *
     * @return the search value
     */
    public Object getSearchValue() {
        return this.searchValue;
    }

    /**
     * Sets the search value.
     *
     * @param searchValue the new search value
     */
    public void setSearchValue(final Object searchValue) {
        this.searchValue = searchValue;
    }

    /**
     * Gets the comparison type.
     *
     * @return the comparisonType
     */
    public String getComparisonType() {
        return comparisonType;
    }

    /**
     * @param comparisonType the comparisonType to set
     */
    public void setComparisonType(String comparisonType) {
        this.comparisonType = comparisonType;
    }

}
