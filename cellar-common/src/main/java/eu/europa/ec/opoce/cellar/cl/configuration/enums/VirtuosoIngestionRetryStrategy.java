/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.configuration.enums
 *             FILE : VirtuosoIngestionRetryStrategy.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 15-07-2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.configuration.enums;

import eu.europa.ec.opoce.cellar.common.util.EnumUtils;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 15-07-2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public enum VirtuosoIngestionRetryStrategy {

    virtuosoNoRetry, virtuosoExponentialRetry, virtuosoExponentialBackoffRetry;

    public static VirtuosoIngestionRetryStrategy resolve(final String key) {
        return EnumUtils.resolve(VirtuosoIngestionRetryStrategy.class, key,
                virtuosoExponentialBackoffRetry);
    }

}
