/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.admin.log
 *        FILE : LogService.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 30-01-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.admin.log;

import java.io.InputStream;
import java.util.List;

/**
 * <class_description> Interface to interact with the cmr log files.
 *
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * ON : 02-08-2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface LogService {

    /**
     * This method return a list of strings. Every string is a representation of a log file.
     * The list is ordered by date of the logs.
     *
     * @return list of string's containing log files
     */
    List<String> getLogs();

    /**
     * This method returns a list of all the log files from today
     *
     * @return a list of string's containing log files
     */
    List<String> getLogsToday();

    /**
     * This method returns a list of all the log files from yesterday
     *
     * @return a list of string's containing log files
     */
    List<String> getLogsYesterday();

    /**
     * This method returns a list of all the log files from this week, ordered by date
     *
     * @return a list of string's containing log files
     */
    List<String> getLogsThisWeek();

    /**
     * This method returns a log input stream
     *
     * @param log: string filename of the logfile
     * @return a string containing the log, or an empty string if the log is not found
     */
    InputStream getLog(String log);

    /**
     * This method removes all log files.
     */
    void clearLogs();

    /**
     * This method removes all the log files prior to the specified date
     *
     * @param date: a string containing the date (yyyy-MM-dd)
     */
    void clearLogs(String date);

    /**
     * This method set's the default log level of the application
     *
     * @param logLevel: a string containing the log level, this string can only be 'debug' or 'info'
     */
    void setLogLevel(String logLevel);

    /**
     * This method returns the string value of the application's log level
     *
     * @return a string containing the log level
     */
    String getLogLevel();
}
