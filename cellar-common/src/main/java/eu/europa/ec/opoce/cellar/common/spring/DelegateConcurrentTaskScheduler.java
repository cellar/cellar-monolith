/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.spring
 *             FILE : DelegateConcurrentTaskScheduler.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 06 13, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-06-13 10:50:45 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.spring;

import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;

import java.util.List;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author ARHS Developments
 */
class DelegateConcurrentTaskScheduler extends ConcurrentTaskScheduler implements CloseableConcurrentTaskScheduler {

    private final ScheduledThreadPoolExecutor executor;

    DelegateConcurrentTaskScheduler(String name) {
        this.executor = new ScheduledThreadPoolExecutor(1, new TaskThreadFactory(name));
        setScheduledExecutor(executor);
    }

    @Override
    public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
        return executor.awaitTermination(timeout, unit);
    }

    @Override
    public void shutdown() {
        executor.shutdown();
    }

    @Override
    public void shutdown(boolean awaitTermination, long timeout, TimeUnit unit) throws InterruptedException {
        shutdown();
        executor.getQueue().clear(); // Not started work is dropped
        if (awaitTermination) {
            while (!executor.isTerminated()) {
                awaitTermination(timeout, unit);
            }
        } else {
            if (!awaitTermination(timeout, unit)) {
                shutdownNow();
            }
        }
    }

    @Override
    public List<Runnable> shutdownNow() {
        return executor.shutdownNow();
    }

    @Override
    public boolean isTerminated() {
        return executor.isTerminated();
    }

    static class TaskThreadFactory implements ThreadFactory {

        private final AtomicInteger idx = new AtomicInteger();
        private final String name;

        TaskThreadFactory(String name) {
            this.name = name;
        }

        @Override
        public Thread newThread(Runnable r) {
            final Thread t = new Thread(r);
            t.setDaemon(false);
            t.setPriority(Thread.NORM_PRIORITY);
            t.setName(name + "-cellar-task-" + idx.incrementAndGet());
            return t;
        }
    }
}
