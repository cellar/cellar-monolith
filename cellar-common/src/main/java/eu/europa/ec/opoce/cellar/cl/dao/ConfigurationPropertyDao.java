/**
 * 
 */
package eu.europa.ec.opoce.cellar.cl.dao;

import eu.europa.ec.opoce.cellar.cl.domain.admin.ConfigurationProperty;

import java.util.Collection;

/**
 * Dao for the the Entity {@link ConfigurationProperty}.
 * @author dbacquel
 *
 */
public interface ConfigurationPropertyDao {

    /**
     * Persist the given transient instance.
     * 
     * @param property
     *            the transient instance to be persisted
     */
    void createProperty(ConfigurationProperty property);

    /**
     * Delete the given persistent instance.
     * 
     * @param property
     *            the persistent instance to delete
     */
    void deleteProperty(ConfigurationProperty property);

    /**
     * Delete the given persistent instances.
     * 
     * @param properties
     *            the persistent instances to delete
     */
    void deleteProperties(Collection<ConfigurationProperty> properties);

    /**
     * Return all persistent instances of {@link ConfigurationProperty}.
     * 
     * @return a {@link Collection} containing 0 or more persistent instances.
     */
    Collection<ConfigurationProperty> getProperties();

    /**
     * Return the persistent instance with the given internal identifier, or null
     * if not found.
     * 
     * @param id
     *            the internal identifier of the persistent instance
     * 
     * @return the persistent instance, or null if not found
     */
    @Deprecated
    ConfigurationProperty getProperty(Long id);

    /**
     * Return the persistent instance identified by the given key, or null
     * if not found.
     * 
     * @param key
     *            the key identifier of the persistent instance
     * 
     * @return the persistent instance, or null if not found
     */
    ConfigurationProperty getPropertyByKey(String key);

    /**
     * Return true if exist
     * @param configurationProperty
     * @return true if exist
     */
    boolean isPropertyExists(ConfigurationProperty configurationProperty);

    /**
     * Update the given persistent instance.
     * 
     * @param configurationProperty
     *            the persistent instance to update
     */
    void updateProperty(ConfigurationProperty configurationProperty);

}
