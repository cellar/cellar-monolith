/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.domain.content.mets.impl
 *             FILE : NonImplementedOperationTypeVisitor.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jul 3, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.domain.content.mets.impl;

import eu.europa.ec.opoce.cellar.domain.content.mets.OperationType;
import eu.europa.ec.opoce.cellar.domain.content.mets.OperationTypeVisitor;
import eu.europa.ec.opoce.cellar.exception.NotImplementedException;
import eu.europa.ec.opoce.cellar.exception.NotImplementedExceptionBuilder;

/**
 * <class_description> Implementation of {@link OperationTypeVisitor} that does nothing but throwing a {@link NotImplementedException}.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jul 3, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class NonImplementedOperationTypeVisitor implements OperationTypeVisitor<Void, Void> {

    /**
     * @see eu.europa.ec.opoce.cellar.domain.content.mets.OperationTypeVisitor#visitRead(java.lang.Object)
     */
    @Override
    public Void visitRead(final Void in) {
        throwNotImplementedException(OperationType.Read);
        return null;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.domain.content.mets.OperationTypeVisitor#visitCreate(java.lang.Object)
     */
    @Override
    public Void visitCreate(final Void in) {
        throwNotImplementedException(OperationType.Create);
        return null;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.domain.content.mets.OperationTypeVisitor#visitCreateOrIgnore(java.lang.Object)
     */
    @Override
    public Void visitCreateOrIgnore(final Void in) {
        throwNotImplementedException(OperationType.CreateOrIgnore);
        return null;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.domain.content.mets.OperationTypeVisitor#visitUpdate(java.lang.Object)
     */
    @Override
    public Void visitUpdate(final Void in) {
        throwNotImplementedException(OperationType.Update);
        return null;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.domain.content.mets.OperationTypeVisitor#visitDelete(java.lang.Object)
     */
    @Override
    public Void visitDelete(final Void in) {
        throwNotImplementedException(OperationType.Delete);
        return null;
    }

    private static void throwNotImplementedException(final OperationType type) throws NotImplementedException {
        throw NotImplementedExceptionBuilder.get().withMessage("Operation [{}] is not supported.").withMessageArgs(type).build();
    }

}
