/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.exception
 *        FILE : VirtuosoException.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 23-07-2015
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.exception;

/**
 * This is a {@link VirtuosoException} buildable by an {@link ExceptionBuilder}.</br>
 * </br> 
 * ON : 23-07-2015
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public class VirtuosoException extends CellarException {

    private static final long serialVersionUID = -4793233835735331116L;

    /**
     * Constructs a new exception with its associated builder.
     * 
     * @param builder the builder to use for building the exception
     */
    public VirtuosoException(final ExceptionBuilder<? extends BuildableException> builder) {
        super(builder);
    }

}
