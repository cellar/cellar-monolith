package eu.europa.ec.opoce.cellar.ingestion;

import eu.europa.ec.opoce.cellar.cl.domain.package_history.PackageHistory;

import java.io.File;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * The Class SIPWork.
 */
public class SIPWork implements Comparable<SIPWork>, Serializable {

    /**
     * The Enum TYPE.
     * <p>The order of the enumeration has to be kept</p>
     * <p><strong>The order of the enumeration must reflect the priority between each TYPE element</strong></p>
     */
    public enum TYPE {

        /** The authenticOJ . */
        AUTHENTICOJ,
        /** The daily. */
        DAILY,
        /** The bulk. */
        BULK,
        /** The low priority bulk. */
        BULK_LOWPRIORITY;
    }

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -2655052846148158729L;

    /** The type. */
    private TYPE type;

    /** The sip file. */
    private File sipFile;

    /** The creation date. */
    private Date creationDate;

    /** The start date. */
    private Date startDate;

    /** The end date. */
    private Date endDate;
    
    /** The corresponding PACKAGE_HISTORY entry. */
    private PackageHistory packageHistory;

    private SIPState state;

    /**
     * Instantiates a new SIP work.
     */
    public SIPWork() {
    }

    /**
     * Gets the creation date.
     *
     * @return the creation date
     */
    public Date getCreationDate() {
        return this.creationDate;
    }

    /**
     * Gets the end date.
     *
     * @return the end date
     */
    public Date getEndDate() {
        return this.endDate;
    }

    /**
     * Gets the priority.
     *
     * @return the priority
     */
    public int getPriority() {
    	if (TYPE.AUTHENTICOJ.equals(this.type)) {
    		return 3;
    	}
    	else if (TYPE.DAILY.equals(this.type)) {
    		return 2;
    	}
    	else if (TYPE.BULK.equals(this.type)) {
    		return 1;
    	}
    	else {
    		return 0;
    	}
    }

    /**
     * Gets the sip file.
     *
     * @return the sip file
     */
    public File getSipFile() {
        return this.sipFile;
    }

    /**
     * Gets the start date.
     *
     * @return the start date
     */
    public Date getStartDate() {
        return this.startDate;
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public TYPE getType() {
        return this.type;
    }
    
    /**
     * Gets the {@link PackageHistory} object.
     *
     * @return the {@link PackageHistory} object
     */
    public PackageHistory getPackageHistory() {
        return packageHistory;
    }

    /**
     * Sets the creation date.
     *
     * @param creationDate the new creation date
     */
    public void setCreationDate(final Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * Sets the end date.
     *
     * @param endDate the new end date
     */
    public void setEndDate(final Date endDate) {
        this.endDate = endDate;
    }

    /**
     * Sets the sip file.
     *
     * @param sipFile the new sip file
     */
    public void setSipFile(final File sipFile) {
        this.sipFile = sipFile;
    }

    /**
     * Sets the start date.
     *
     * @param startDate the new start date
     */
    public void setStartDate(final Date startDate) {
        this.startDate = startDate;
    }

    /**
     * Sets the type.
     *
     * @param type the new type
     */
    public void setType(final TYPE type) {
        this.type = type;
    }
    
    /**
     * Sets the {@link PackageHistory} object.
     *
     * @param packageHistory the {@link PackageHistory} object
     */
    public void setPackageHistory(PackageHistory packageHistory) {
        this.packageHistory = packageHistory;
    }
    
    /** {@inheritDoc} */
    @Override
    public String toString() {
        return "SIPWork [type=" + this.type + ", sipFile=" + this.sipFile + ", creationDate=" + this.creationDate
                + ", packageHistory=" + this.packageHistory + "]";
    }

    public SIPState getState() {
        return state;
    }

    public void setState(SIPState state) {
        this.state = state;
    }

    /** {@inheritDoc} */
    @Override
    public int compareTo(final SIPWork o) {
        final TYPE thisSipWorkType = this.getType();
        final TYPE otherSipWorkType = o.getType();
        int result = thisSipWorkType.compareTo(otherSipWorkType);
        //if they have the same type then order by date
        if (result == 0) {
            final File thisSipFile = this.getSipFile();
            final File otherSipFile = o.getSipFile();
            result = thisSipFile.compareTo(otherSipFile);
            if (result != 0) {
                //if Files are different compare the dates
                final Date thisStartDate = this.getStartDate();
                final Date otherStartDate = o.getStartDate();
                if ((thisStartDate != null) && (otherStartDate != null)) {
                    result = thisStartDate.compareTo(otherStartDate);
                } else if ((thisStartDate != null) && (otherStartDate == null)) {
                    result = 1;
                } else if ((otherStartDate != null) && (thisStartDate == null)) {
                    result = -1;
                } else {
                    //creation dates should never be null
                    final Date thisCreationDate = this.getCreationDate();
                    final Date otherCreationDate = o.getCreationDate();
                    result = thisCreationDate.compareTo(otherCreationDate);
                }
            }
        }
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof SIPWork) {
            final SIPWork other = (SIPWork) obj;
            return Objects.equals(this.sipFile, other.getSipFile());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.sipFile);
    }
    
    /**
     * Creates a new SIPWork object based on the provided SIP file
     * to be processed and its reception folder type.
     * @param sipFileToProcess the absolute path of the SIP file to process.
     * @param type the reception folder type that contains the SIP file.
     * @return a new SIPWork object.
     */
    public static SIPWork createSIPProcessObject(final File sipFileToProcess, final TYPE type, final Date detectionDate, final PackageHistory packageHistory) {
		final SIPWork sipProcessObject = new SIPWork();
		sipProcessObject.setSipFile(sipFileToProcess);
		sipProcessObject.setType(type);
		sipProcessObject.setCreationDate(detectionDate);
        sipProcessObject.setPackageHistory(packageHistory);
		return sipProcessObject;
	}

}
