/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.configuration
 *        FILE : PropertyManager.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 02-04-2012
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.configuration;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.enums.DataSource;
import eu.europa.ec.opoce.cellar.common.util.StringUtils;
import eu.europa.ec.opoce.cellar.exception.CellarConfigurationException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;

/**
 * <class_description> Class for managing application level properties.<br/>
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 02-04-2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class PropertyManager {

    private static final Logger LOG = LogManager.getLogger(PropertyManager.class);
    private final Properties properties;
    private final Map<String, Class<?>> keysTypes;

    public PropertyManager() {
        this(new Properties());
    }

    public PropertyManager(final Properties properties) {
        this.properties = properties;
        this.keysTypes = new HashMap<>();
    }

    /**
     * Converts the <code>String</code> value into a <code>boolean</code> type. Returns default if the
     * property does not exist.
     */
    public boolean getBooleanProperty(final String key) {
        return this.getBooleanProperty(key, false);
    }

    public boolean getBooleanProperty(final String key, final boolean defaultValue) {
        boolean retValue = defaultValue;

        final String value = this.getProperty(key);
        if (this.isBooleanProperty(key) && StringUtils.isBooleanValue(value)) {
            retValue = Boolean.valueOf(value).booleanValue();
        }

        return retValue;
    }

    public boolean isBooleanProperty(final String key) {
        return this.isPropertyOfType(key, Boolean.class);
    }

    /**
     * Converts the <code>String</code> value into a <code>Class</code> using <code>Class.forName()</code>.
     * Returns the default value if the property can't be found or the value can't be converted into a
     * <code>Class</code>.
     */
    public Class<?> getClassProperty(final String key) {
        return this.getClassProperty(key, null);
    }

    public Class<?> getClassProperty(final String key, final Class<?> defaultValue) {
        Class<?> retValue = defaultValue;

        final String value = this.getProperty(key);
        if (this.isClassProperty(key) && StringUtils.isClassValue(value)) {
            try {
                retValue = Class.forName(value);
            } catch (final ClassNotFoundException e) {
            }
        }

        return retValue;
    }

    public boolean isClassProperty(final String key) {
        return this.isPropertyOfType(key, Class.class);
    }

    /**
     * Convert the string property value into a <code>int</code>. Returns the default value if the property
     * is not found or if there is a problem converting the value to a <code>int</code>.
     */
    public int getIntProperty(final String key) {
        return this.getIntProperty(key, 0);
    }

    public int getIntProperty(final String key, final int defaultValue) {
        int retValue = defaultValue;

        final String value = this.getProperty(key);
        if (this.isIntProperty(key) && StringUtils.isIntValue(value)) {
            retValue = Integer.parseInt(value);
        }

        return retValue;
    }

    public boolean isIntProperty(final String key) {
        return this.isPropertyOfType(key, Integer.class);
    }

    /**
     * Convert the string property value into a <code>long</code>. Returns the default value if the property
     * is not found or if there is a problem converting the value to a <code>long</code>.
     */
    public long getLongProperty(final String key) {
        return this.getLongProperty(key, 0);
    }

    public long getLongProperty(final String key, final long defaultValue) {
        long retValue = defaultValue;

        final String value = this.getProperty(key);
        if (this.isLongProperty(key) && StringUtils.isLongValue(value)) {
            retValue = Long.parseLong(value);
        }

        return retValue;
    }

    public boolean isLongProperty(final String key) {
        return this.isPropertyOfType(key, Long.class) || this.isIntProperty(key);
    }

    /**
     * Convert the string property value into a <code>Float</code>. Returns the default value if the property
     * is not found or if there is a problem converting the value to a <code>Float</code>.
     */
    public float getFloatProperty(final String key) {
        return this.getFloatProperty(key, 0);
    }

    public float getFloatProperty(final String key, final float defaultValue) {
        float retValue = defaultValue;

        final String value = this.getProperty(key);
        if (this.isFloatProperty(key) && StringUtils.isFloatValue(value)) {
            retValue = Float.valueOf(value).floatValue();
        }

        return retValue;
    }

    public boolean isFloatProperty(final String key) {
        return this.isPropertyOfType(key, Float.class) || this.isLongProperty(key) || this.isIntProperty(key);
    }

    /**
     * Get a string from DataSource property.
     */
    public String getDataSourceProperty(final String key) {
        return this.getProperty(key, "", true);
    }

    /**
     * Get a string property.
     */
    public String getProperty(final String key) {
        return this.getProperty(key, "", false);
    }

    public String getProperty(final String key, final String defaultValue, boolean isDataSourceProperty) {
        String envKey = toEnvKey(key);
        String envVar = getEnvVar(key, envKey, isDataSourceProperty);
        if (envVar != null) { // Environment variable will override the values in the files
            LOG.info(IConfiguration.CONFIG, "Property {} overridden by {}={}", key, envKey, envVar);
            return envVar;
        }
        if (!this.existsProperty(key)) {
            return defaultValue;
        }
        return this.properties.get(key).toString();
    }

    private static String toEnvKey(String key) {
        if (key != null && !key.isEmpty()) {
            return key.replaceAll("[\\.-]", "_").toUpperCase();
        }
        return key;
    }

    private String getEnvVar(String key, String envKey, boolean isDataSourceProperty) {
        String envVar = FilenameUtils.normalize(System.getenv(envKey)); // normalize this value to avoid path manipulations

        //special treatment for DataSource properties, because they are used with a lookup method in JdbcUtils
        if((envVar != null) && isDataSourceProperty) {
            if(!isDataSourceValid(envVar)) {
                LOG.warn("{} is not an acceptable DataSource value for property: {}. {} from the configuration file will be used instead",
                        envVar, key, this.properties.get(key));
                return null;
            } else {
                return resolveDataSourceValue(envVar); //do not directly return a user input value
            }
        }
        return envVar;
    }

    /**
     * Check if the DataSource is valid against a whitelist of legitimate DataSources
     *
     * @param propertyValue the value of the property that is to be checked
     * @return true if {@code propertyValue} is valid, false otherwise
     */
    private boolean isDataSourceValid(final String propertyValue) {
        return Objects.equals(propertyValue, DataSource.CELLAR_DS.getDatasource()) ||
                Objects.equals(propertyValue, DataSource.CMR_DS.getDatasource()) ||
                Objects.equals(propertyValue, DataSource.STATS_DS.getDatasource()) ||
                Objects.equals(propertyValue, DataSource.IDOL_DS.getDatasource()) ||
                Objects.equals(propertyValue, DataSource.VIRTUOSO_DS.getDatasource());
    }

    /**
     * Resolve and return a legitimate value from the {@link DataSource} enum constants.
     * The purpose is to introduce a level of indirection, thus avoiding to return any direct user input.
     *
     * @param propertyValue the value of the property that is to be resolved
     * @return the resolved value
     */
    private String resolveDataSourceValue(final String propertyValue) {
        if(Objects.equals(propertyValue, DataSource.CELLAR_DS.getDatasource())) {
            return DataSource.CELLAR_DS.getDatasource();
        }
        if(Objects.equals(propertyValue, DataSource.CMR_DS.getDatasource())) {
            return DataSource.CMR_DS.getDatasource();
        }
        if(Objects.equals(propertyValue, DataSource.STATS_DS.getDatasource())) {
            return DataSource.STATS_DS.getDatasource();
        }
        if(Objects.equals(propertyValue, DataSource.IDOL_DS.getDatasource())) {
            return DataSource.IDOL_DS.getDatasource();
        }
        if(Objects.equals(propertyValue, DataSource.VIRTUOSO_DS.getDatasource())) {
            return DataSource.VIRTUOSO_DS.getDatasource();
        }
        return null;
    }

    /**
     * Set a generic object property.
     */
    public void setProperty(final String key, final String value) throws CellarConfigurationException {
        if (value == null) {
            throw ExceptionBuilder.get(CellarConfigurationException.class).withCode(CommonErrors.FAILED_PROCESSING_CONFIGURATION)
                    .withMessage("Cannot set property '{}' with a null value: the value may come from the database configuration table, "
                            + "or from the configuration file. Please remove the null value before proceeding.")
                    .withMessageArgs(key).build();
        }
        this.isCompatibleValueForProperty(key, value);

        Object valueObj = value;
        if (StringUtils.isBooleanValue(value)) {
            valueObj = Boolean.valueOf(value);
        } else if (StringUtils.isClassValue(value)) {
            try {
                valueObj = Class.forName(value);
            } catch (final ClassNotFoundException e) {
            }
        } else if (StringUtils.isIntValue(value)) {
            valueObj = Integer.valueOf(value);
        } else if (StringUtils.isLongValue(value)) {
            valueObj = Long.valueOf(value);
        }
        else if (StringUtils.isFloatValue(value)) {
        	valueObj = Float.valueOf(value);
        }
        this.properties.put(key, valueObj);
        this.keysTypes.put(key, valueObj.getClass());
    }

    /**
     * Returns true if the property {@code key} exists, false otherwise.
     */
    public boolean existsProperty(final String key) {
        return this.properties.containsKey(key);
    }

    /**
     * Check if the property identified by {@code key} is compatible with {@code value}, otherwise throws an {@link CellarConfigurationException}.
     *
     * @throws CellarConfigurationException
     */
    public void isCompatibleValueForProperty(final String key, final String value) throws CellarConfigurationException {
        final Class<?> expectedType = this.keysTypes.get(key);
        StringUtils.isCompatibleValue(expectedType, value);
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder str = new StringBuilder(this.getClass().getSimpleName() + " [");
        for (final Object propertyKey : this.properties.keySet()) {
            str.append(propertyKey).append('=').append(this.properties.get(propertyKey)).append(',');
        }
        str.setCharAt(str.length() - 1, ']');
        return str.toString();
    }

    /**
     * Returns all properties and their values.
     *
     * @return the properties
     */
    public Properties getProperties() {
        return this.properties;
    }

    private boolean isPropertyOfType(final String key, final Class<?> clazz) {
        final Class<?> expectedType = this.keysTypes.get(key);
        return (expectedType == null) || expectedType.equals(clazz);
    }

}
