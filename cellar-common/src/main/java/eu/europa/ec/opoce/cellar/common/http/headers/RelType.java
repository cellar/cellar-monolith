/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.http.memento.link
 *             FILE : RelType.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 29, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.http.headers;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 29, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public enum RelType implements IURLTokenable {
    MEMENTO("memento"),
    ORIGINAL("original"),
    ORIGINAL_TIMEGATE("original timegate"),
    TIMEGATE("timegate"),
    TIMEMAP("timemap"),
    SELF("self"),
    MEMENTO_FIRST("memento first"),
    MEMENTO_LAST("memento last");

    private String value;

    private RelType(final String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    @Override
    public String getURLToken() {
        return this.getValue();
    }

    @Override
    public String getErrorLabel() {
        return this.toString();
    }
}
