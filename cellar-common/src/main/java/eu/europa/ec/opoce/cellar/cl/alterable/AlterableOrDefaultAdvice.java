/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.alterable
 *             FILE : AlterableOrDefaultAdvice.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jul 15, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.alterable;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jul 15, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Aspect
public class AlterableOrDefaultAdvice {

    private static final AlterableOrDefaultAdvice instance = new AlterableOrDefaultAdvice();

    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    @Around("@annotation(alterableOrDefault)")
    public Object process(final ProceedingJoinPoint pjp, final AlterableOrDefault alterableOrDefault) throws Throwable {

        if (this.cellarConfiguration.isCellarDatabaseReadOnly()) {
            return null;
        }

        return pjp.proceed(pjp.getArgs());
    }

    /**
     * Needed by AspectJ
     */
    public static AlterableOrDefaultAdvice aspectOf() {
        return instance;
    }
}
