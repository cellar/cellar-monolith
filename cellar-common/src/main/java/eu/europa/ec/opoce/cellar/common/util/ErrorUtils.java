/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.util
 *             FILE : ErrorUtils.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Nov 21, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.util;

/**
 * <class_description> Services class that helps the error extraction.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Nov 21, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class ErrorUtils {

    private final static String ERROR_SEPARATOR = "#";
    private final static String HTML_LINE_SEPARATOR = "<br />";

    /**
     * Gets the error separator.
     * @return the error separator
     */
    public static String getErrorSeparator() {
        return ERROR_SEPARATOR;
    }

    /**
     * Returns the stacktrace of the error in HTML.
     * @param error the error with the stacktrace to transform
     * @return the stacktrace in HTML
     */
    public static String getHtmlStacktrace(final Throwable error) {
        return getStackTrace(error, HTML_LINE_SEPARATOR);
    }

    /**
     * Returns the stacktrace of the error in a string.
     * @param error the error with the stacktrace to transform
     * @return the stacktrace in string
     */
    private static String getStackTrace(final Throwable error, final String lineSeparator) {
        final StringBuilder result = new StringBuilder(error.getMessage());
        result.append(error.toString());

        result.append(lineSeparator);

        for (StackTraceElement element : error.getStackTrace()) {
            result.append(element);
            result.append(lineSeparator);
        }
        return result.toString();
    }
}
