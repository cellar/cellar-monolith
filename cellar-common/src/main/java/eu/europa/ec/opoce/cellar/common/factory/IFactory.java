/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.factory
 *             FILE : IFactory.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 07-04-2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.factory;

/**
 * <class_description> Factory interface for classes that create new instances of {@link R}
 * on the basis of param {@link P}.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 07-04-2014
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IFactory<R, P> {

    R create(final P param);
}
