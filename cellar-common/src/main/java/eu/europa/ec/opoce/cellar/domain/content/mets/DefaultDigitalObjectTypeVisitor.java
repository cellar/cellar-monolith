/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.domain.content.mets
 *             FILE : DefaultDigitalObjectTypeVisitor.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 10-11-2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.domain.content.mets;

import eu.europa.ec.opoce.cellar.exception.NotImplementedExceptionBuilder;

/**
 * <class_description> Default implementation of the visitor for a {@link DigitalObjectType}.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 10-11-2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class DefaultDigitalObjectTypeVisitor<IN, OUT> implements DigitalObjectTypeVisitor<IN, OUT> {

    @Override
    public OUT visitWork(final IN in) {
        this.throwUp("visitWork");
        return null;
    }

    @Override
    public OUT visitExpression(final IN in) {
        this.throwUp("visitExpression");
        return null;
    }

    @Override
    public OUT visitManifestation(final IN in) {
        this.throwUp("visitManifestation");
        return null;
    }

    @Override
    public OUT visitItem(final IN in) {
        this.throwUp("visitItem");
        return null;
    }

    @Override
    public OUT visitDossier(final IN in) {
        this.throwUp("visitWork");
        return null;
    }

    @Override
    public OUT visitEvent(final IN in) {
        this.throwUp("visitEvent");
        return null;
    }

    @Override
    public OUT visitAgent(final IN in) {
        this.throwUp("visitAgent");
        return null;
    }

    @Override
    public OUT visitTopLevelEvent(final IN in) {
        this.throwUp("visitTopLevelEvent");
        return null;
    }

    @Override
    public OUT visitEuronal(final IN in) {
        this.throwUp("visitEuronal");
        return null;
    }

    private OUT throwUp(final String methodName) {
        throw NotImplementedExceptionBuilder.get().withMessage("Method '{}' is not implemented for class '{}'.")
                .withMessageArgs(methodName, this.getClass().getName()).build();
    }
}
