/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.dao
 *             FILE : MimeTypeCache.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 5, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.client;

import eu.europa.ec.opoce.cellar.cl.domain.mimeType.MimeTypeMapping;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <class_description> Mime type cache manager interface.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 5, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface MimeTypeCacheService {

    /**
     * Return all persistent manifestation types.
     *
     * @return a {@link HashSet} containing 0 or more persistent manifestation types
     */
    Set<String> getMimeTypesSet();

    /**
     * Return all persistent manifestation types ordered by priority desc.
     *
     * @return a {@link List) containing 0 or more persistent manifestation types
     */
    List<String> getMimeTypesByPriorityDescList();

    /**
     * Gets the manifestation types map.
     *
     * @return the manifestation types map
     */
    Map<String, List<MimeTypeMapping>> getManifestationTypesMap();

    /**
     * Gets the mime type map.
     *
     * @return the mime type map
     */
    Map<String, List<MimeTypeMapping>> getMimeTypeMap();
}
