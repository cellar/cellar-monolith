package eu.europa.ec.opoce.cellar.domain.content.mets;

import java.io.Serializable;

public class MetadataStream implements Serializable {

    private static final long serialVersionUID = 3873062183073386716L;

    private String manifestationType;
    private String fileRef;
    private boolean embedded;

    public MetadataStream(String manifestationType, String fileRef) {
        this.manifestationType = manifestationType;
        this.fileRef = fileRef;
    }

    public String getManifestationType() {
        return manifestationType;
    }

    public String getFileRef() {
        return fileRef;
    }

    public boolean isEmbedded() {
        return embedded;
    }

}
