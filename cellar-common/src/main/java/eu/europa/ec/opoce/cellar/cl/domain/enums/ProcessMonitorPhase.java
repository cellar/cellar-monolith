package eu.europa.ec.opoce.cellar.cl.domain.enums;

import eu.europa.ec.opoce.cellar.common.util.EnumUtils;

/**
 * <class_description> Available phases for monitoring regarding testing purposes
 * (see also {@link eu.europa.ec.opoce.cellar.common.service.TestingThreadService}).
 * <br/><br/>
 * <notes> Enumeration of process phases.
 * <br/><br/>
 * ON : 10-10-2023
 *
 * @author EUROPEAN DYNAMICS S.A.
 */
public enum ProcessMonitorPhase {
    
    Ingestion_SipDependencyChecker,
    Ingestion_Validate,
    Ingestion_Convert,
    Ingestion_StructMapProcessing,
    Ingestion_Complete,
    Indexation_Pending,
    Indexation_Execution,
    Indexation_Redundant,
    Indexation_Complete,
    None;
    
    public static ProcessMonitorPhase resolve(final String key) {
        return EnumUtils.resolve(ProcessMonitorPhase.class, key, None);
    }
}
