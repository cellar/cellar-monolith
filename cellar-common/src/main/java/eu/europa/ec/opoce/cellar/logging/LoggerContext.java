package eu.europa.ec.opoce.cellar.logging;

import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import eu.europa.ec.opoce.cellar.common.closure.Closure;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import org.slf4j.Logger;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Central class to provide thread based logging.
 * <p/>
 * <p>A LoggerContext provides:
 * <ul>
 * <li>method for accessing the <code>org.slf4j.Logger</code> instance to be used by the current thread.
 * <li>methods to check the thread based logging is enabled for a given {@link eu.europa.ec.opoce.cellar.logging.LoggerContext.CellarLevel}.
 * </ul>
 * <p>
 * Deprecation: See {@link LogContext} for replacement of {@link #around(String, Closure)},
 * {@link #around(String, Map, Closure)}. Scheduled for deletion in 8.0.
 */
@Deprecated
public class LoggerContext {

    /**
     * Puts a LoggerContext for the given Closure on the current thread.
     * After execution of the given Closure the LoggerContext is removed.
     *
     * @param name    a {@link java.lang.String} object.
     * @param closure a {@link Closure} object.
     */
    public static void around(final String name, final Closure closure) {
        if (name == null) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument name cannot be null.").build();
        }
        if (closure == null) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument closure cannot be null.").build();
        }

        LoggerContext currentContext = CellarLoggerFactory.getLoggerContext();
        try {
            CellarLoggerFactory.setLoggerContext(name, ServiceLocator.getService(CellarLogLevelService.class));
            closure.call();
        } finally {
            CellarLoggerFactory.setLoggerContext(currentContext);
        }
    }

    /**
     * Puts a LoggerContext for the given closure on the current thread.
     * Also give a list of properties that are useful for logging to identify problems easier
     * After execution of the given closure the LoggerContext is removed.
     *
     * @param name                 a {@link java.lang.String} object.
     * @param loggingContextValues a {@link java.util.Map} object.
     * @param closure              a {@link Closure} object.
     */
    public static void around(final String name, final Map<String, String> loggingContextValues, final Closure closure) {
        if (name == null) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument name cannot be null.").build();
        }
        if (closure == null) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument closure cannot be null.").build();
        }

        LoggerContext currentContext = CellarLoggerFactory.getLoggerContext();
        try {
            CellarLoggerFactory.setLoggerContext(name, loggingContextValues, ServiceLocator.getService(CellarLogLevelService.class));
            closure.call();
        } finally {
            CellarLoggerFactory.setLoggerContext(currentContext);
        }
    }

    private Logger logger;
    private Map<String, String> loggingContextValues;
    private CellarLogLevelService cellarLogLevelService;

    /**
     * <p>Constructor for LoggerContext.</p>
     *
     * @param logger                a {@link org.slf4j.Logger} object.
     * @param cellarLogLevelService a {@link eu.europa.ec.opoce.cellar.logging.CellarLogLevelService} object.
     */
    LoggerContext(Logger logger, CellarLogLevelService cellarLogLevelService) {
        this(logger, null, cellarLogLevelService);
    }

    /**
     * <p>Constructor for LoggerContext.</p>
     *
     * @param logger                a {@link org.slf4j.Logger} object.
     * @param loggingContextValues  a {@link java.util.Map} object.
     * @param cellarLogLevelService a {@link eu.europa.ec.opoce.cellar.logging.CellarLogLevelService} object.
     */
    LoggerContext(Logger logger, Map<String, String> loggingContextValues, CellarLogLevelService cellarLogLevelService) {
        if (logger == null) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument logger cannot be null").build();
        }
        if (cellarLogLevelService == null) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument cellarLogLevelService cannot be null").build();
        }
        // to avoid infinite loops
        if (logger instanceof CellarLoggerFactory.CellarLogger) {
            throw ExceptionBuilder.get(CellarException.class).build();
        }

        this.logger = logger;
        this.loggingContextValues = loggingContextValues == null ? Collections.emptyMap()
                : Collections.unmodifiableMap(new HashMap<>(loggingContextValues));
        this.cellarLogLevelService = cellarLogLevelService;
    }

    /**
     * <p>Getter for the field <code>logger</code>.</p>
     *
     * @return a {@link org.slf4j.Logger} object.
     */
    public Logger getLogger() {
        return logger;
    }

    /**
     * <p>getContextValues.</p>
     *
     * @return a {@link java.util.Map} object.
     */
    public Map<String, String> getContextValues() {
        return loggingContextValues;
    }

    /**
     * <p>isTraceEnabled.</p>
     *
     * @return a boolean.
     */
    public boolean isTraceEnabled() {
        return cellarLogLevelService.getLevel().ordinal() <= CellarLevel.TRACE.ordinal();
    }

    /**
     * <p>isDebugEnabled.</p>
     *
     * @return a boolean.
     */
    public boolean isDebugEnabled() {
        return cellarLogLevelService.getLevel().ordinal() <= CellarLevel.DEBUG.ordinal();
    }

    /**
     * <p>isInfoEnabled.</p>
     *
     * @return a boolean.
     */
    public boolean isInfoEnabled() {
        return cellarLogLevelService.getLevel().ordinal() <= CellarLevel.INFO.ordinal();
    }

    /**
     * <p>isWarnEnabled.</p>
     *
     * @return a boolean.
     */
    public boolean isWarnEnabled() {
        return cellarLogLevelService.getLevel().ordinal() <= CellarLevel.WARNING.ordinal();
    }

    /**
     * <p>isErrorEnabled.</p>
     *
     * @return a boolean.
     */
    public boolean isErrorEnabled() {
        return cellarLogLevelService.getLevel().ordinal() <= CellarLevel.ERROR.ordinal();
    }

    public enum CellarLevel {
        TRACE, DEBUG, INFO, WARNING, ERROR
    }
}
