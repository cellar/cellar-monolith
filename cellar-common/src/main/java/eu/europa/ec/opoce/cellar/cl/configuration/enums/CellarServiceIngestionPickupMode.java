/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.configuration.enums
 *             FILE : CellarServiceIngestionPickupMode.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Nov 18, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.configuration.enums;

import eu.europa.ec.opoce.cellar.common.util.EnumUtils;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Nov 18, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public enum CellarServiceIngestionPickupMode {

    none, random;

    public static CellarServiceIngestionPickupMode resolve(final String key) {
        return EnumUtils.resolve(CellarServiceIngestionPickupMode.class, key, none);
    }
}
