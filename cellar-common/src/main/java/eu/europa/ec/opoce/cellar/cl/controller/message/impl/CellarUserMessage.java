/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.util.impl
 *             FILE : CellarUserMessage.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 22, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.message.impl;

import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage;

/**
 * <class_description> Cellar user message.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 22, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class CellarUserMessage implements ICellarUserMessage {

    /**
     * 
     */
    private static final long serialVersionUID = 2413119384286169029L;

    /**
     * The level of the message.
     */
    private final LEVEL messageLevel;

    /**
     * The message code to use in the message source.
     */
    private final String messageCode;

    /**
     * The default message if the message code doesn't exist in the message source.
     */
    private final String defaultMessage;

    /**
     * The message arguments to insert in the message.
     */
    private final Object[] messageArguments;

    /**
     * Constructor.
     * @param messageLevel the level of the message
     * @param messageCode the message code to use in the message source
     */
    public CellarUserMessage(final LEVEL messageLevel, final String messageCode) {
        this.messageLevel = messageLevel;
        this.messageCode = messageCode;
        this.defaultMessage = null;
        this.messageArguments = null;
    }

    /**
     * Constructor.
     * @param messageLevel the level of the message
     * @param messageCode the message code to use in the message source
     * @param defaultMessage the default message if the message code doesn't exist in the message source
     */
    public CellarUserMessage(final LEVEL messageLevel, final String messageCode, final String defaultMessage) {
        this.messageLevel = messageLevel;
        this.messageCode = messageCode;
        this.defaultMessage = defaultMessage;
        this.messageArguments = null;
    }

    /**
     * Constructor.
     * @param messageLevel the level of the message
     * @param messageCode the message code to use in the message source
     * @param defaultMessage the default message if the message code doesn't exist in the message source
     * @param messageArguments the message arguments to insert in the message
     */
    public CellarUserMessage(final LEVEL messageLevel, final String messageCode, final String defaultMessage,
            final Object[] messageArguments) {
        this.messageLevel = messageLevel;
        this.messageCode = messageCode;
        this.defaultMessage = defaultMessage;
        this.messageArguments = messageArguments;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LEVEL getMessageLevel() {
        return this.messageLevel;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getMessageCode() {
        return this.messageCode;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDefaultMessage() {
        return this.defaultMessage;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object[] getMessageArguments() {
        return this.messageArguments;
    }

}
