package eu.europa.ec.opoce.cellar.cmr;

import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import org.apache.commons.lang.StringUtils;

import java.util.EnumMap;
import java.util.Map;

public enum ContentType {

    DIRECT("direct.nt"),
    DIRECT_INFERRED("direct-inferred.nt"),
    TECHNICAL("technical.nt"),
    DECODING("decoding.nt"),
    FILE("content"),

    SAME_AS("sameas.nt"),
    INVERSE("inverse.nt"),
    EMBEDDED_NOTICE("embedded-notice.xml");

    private final String file;

    ContentType(String file) {
        this.file = file;
    }

    public String file() {
        return file;
    }

    public static Map<ContentType, String> extract(Map<ContentType, String> src, ContentType... types) {
        Map<ContentType, String> values = new EnumMap<>(ContentType.class);
        for (ContentType type : types) {
            values.put(type, src.get(type));
        }
        return values;
    }

    public static boolean isCellarContentType(final String contentTypeName) {
        boolean result = true;
        if (StringUtils.isNotBlank(contentTypeName)) {
            for (final ContentType contentType : ContentType.values()) {
                final String type = contentType.file();
                if (StringUtils.startsWith(contentTypeName, type)) {
                    result = false;
                }
                if (!result) {
                    break;
                }
            }
        } else {
            result = false;
        }
        return result;
    }

    public static boolean isCellarContentStreamUri(final String uri) {
        boolean result = false;
        final String cellarPath = StringUtils.substringAfter(uri, ContentIdentifier.CELLAR_PREFIX);//cellar/b3365968-1e8e-4ce1-a251-221f7c106066.0011.01/DOC_1
        final String cellarId = cellarPath.replaceFirst("/", ":");//cellar:b3365968-1e8e-4ce1-a251-221f7c106066.0011.01/DOC_1
        if (DigitalObjectType.isItem(cellarId)) {
            final String[] split = uri.split("/");
            final int length = split.length;
            if (length > 1) {
                final String dataStreamName = split[length - 1];
                result = isCellarContentType(dataStreamName);
            }
        }
        return result;
    }

    /**
     * Checks if is cellar content stream uuid.
     * cellar:12e14a0c-6dc4-11e5-975a-01aa75ed71a1.0005.01/DOC_1
     *
     * @param uuid the uuid
     * @return true, if is cellar content stream uuid
     */
    public static boolean isCellarContentStreamUuid(final String uuid) {
        boolean result = false;
        if (DigitalObjectType.isItem(uuid)) {
            final String[] split = uuid.split("/");
            final int length = split.length;
            if (length > 1) {
                final String dataStreamName = split[length - 1];
                result = isCellarContentType(dataStreamName);
            }
        }
        return result;
    }
}
