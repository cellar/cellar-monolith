/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.exception
 *        FILE : ExceptionUtils.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 25-03-2013
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.exception;

import eu.europa.ec.opoce.cellar.common.util.StringUtils;

import static eu.europa.ec.opoce.cellar.AppConstants.NEWLINE;

/**
 * Utility class for exception handling.</br>
 * </br>
 * ON : 25-03-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public abstract class ExceptionUtils extends org.apache.commons.lang.exception.ExceptionUtils {

    public static ExceptionHandler raise(RuntimeException ex) {
        return new ExceptionHandler(ex);
    }

    /**
     * Handle to throw exceptions on rule validation.
     */
    public static class ExceptionHandler {

        private final RuntimeException ex;

        ExceptionHandler(RuntimeException ex) {
            this.ex = ex;
        }

        public void whenFalse(boolean test) {
            if (!test) {
                throw ex;
            }
        }

        public void whenTrue(boolean test) {
            if (test) {
                throw ex;
            }
        }

        public void whenEquals(Object obj1, Object obj2) {
            if (obj1.equals(obj2)) {
                throw ex;
            }
        }

        public void whenNotEquals(Object obj1, Object obj2) {
            if (!obj1.equals(obj2)) {
                throw ex;
            }
        }
    }

    /**
     * This method extracts the auditable exceptions from the given {@link exception}'s hierarchy.
     * Then concatenates them in a single message and returns it.
     *
     * @param exception the exception from which to extract the auditable exceptions
     * @param limit abbreviates the message to {@link limit} characters if {@link limit} > 0
     */
    public static String extractAuditableMessage(final Throwable exception, final int limit) {
        final StringBuilder messageBuilder = new StringBuilder();

        // navigate the hierarchy of the exception
        Throwable myException = exception;
        while (myException != null) {
            // do not audit if it is a non-auditable CellarException
            boolean auditable = true;
            if (myException instanceof CellarException && !((CellarException) myException).isVisibleOnStackTrace()) {
                auditable = false;
            }

            // audit
            if (auditable) {
                if (messageBuilder.length() > 0) {
                    messageBuilder.append(NEWLINE + "Caused by: ");
                }
                messageBuilder.append(myException.toString());
            }

            // get deeper
            myException = myException.getCause();
        }

        // if no auditable exceptions were found, default to the root exception
        String message = messageBuilder.toString();
        if (message.length() == 0) {
            message = exception.toString();
        }

        // return
        if (limit > 0) {
            message = StringUtils.abbreviate(message, limit);
        }
        return message;
    }

    /**
     * Evaluates if {@link exception} contains one of the given {@link messages} in itself, or in the children exceptions down the stack-trace.
     *
     * @param exception the exception to evaluate
     * @param messages the messages to check
     * @return true if {@link exception} contains one of the given {@link messages}
     */
    public static boolean exceptionContainsMessages(final Throwable exception, final String... messages) {
        Throwable innerException = exception;
        boolean textContainsStrings = false;
        while (!textContainsStrings) {
            textContainsStrings = textContainsStrings(innerException.getMessage(), messages);
            if (textContainsStrings) {
                break;
            }
            innerException = innerException.getCause();
            if (innerException == null) {
                break;
            }
        }
        return textContainsStrings;
    }

    /**
     * Get the full stack trace of the given {@link exception}.
     *
     * @param exception the exception to check
     * @return the stack trace
     */
    public static String getStackTrace(final Throwable exception) {
        return org.apache.commons.lang.exception.ExceptionUtils.getFullStackTrace(exception);
    }

    /**
     * Get the throwables' headers (that is, the name of the throwable only without the related stacktrace) in a "\n"-separated list.
     *
     * @param exception the exception to check
     * @return the string representing the list
     */
    public static String getThrowablesHeaders(final Throwable exception) {
        final StringBuilder listBuilder = new StringBuilder();

        final Throwable[] throwables = ExceptionUtils.getThrowables(exception);
        for (final Throwable throwable : throwables) {
            if (listBuilder.length() > 0) {
                listBuilder.append("\n");
            }
            listBuilder.append(throwable);
        }

        return listBuilder.toString();
    }

    private static boolean textContainsStrings(final String text, final String... stringsToCheck) {
        boolean contains = false;
        if (StringUtils.isEmpty(text)) {
            return contains;
        }

        for (final String stringToCheck : stringsToCheck) {
            if (text.contains(stringToCheck) || stringToCheck.contains(text)) {
                contains = true;
                break;
            }
        }

        return contains;
    }

}
