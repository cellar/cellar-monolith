package eu.europa.ec.opoce.cellar.cl.dao;

import eu.europa.ec.opoce.cellar.cl.domain.package_history.PackageHistory;
import eu.europa.ec.opoce.cellar.cl.domain.structmap_status_history.StructMapStatusHistory;

import java.util.List;

/**
 * Dao for the Entity {@link StructMapStatusHistory}.
 *
 * @author EUROPEAN DYNAMICS S.A.
 *
 */
public interface StructMapStatusHistoryDao extends BaseDao<StructMapStatusHistory, Long>{
    
    /**
     * Return a persistent instance of {@link StructMapStatusHistory} based on its STRUCTMAP_NAME and PACKAGE_HISTORY entry.
     *
     * @param structMapName the name of the structmap to look for.
     * @param packageHistory the {@link PackageHistory} the structmap belongs to.
     *
     * @return the StructMap Status History reference matching the provided arguments.
     */
    StructMapStatusHistory getByNameAndPackageHistory(String structMapName, PackageHistory packageHistory);
    
    /**
     * Return all persistent instances of {@link StructMapStatusHistory} belonging to the same {@link PackageHistory}.
     *
     * @param packageHistory the {@link PackageHistory} the structmaps belong to.
     *
     * @return the list of {@link StructMapStatusHistory} references matching the provided argument.
     */
    List<StructMapStatusHistory> getByPackageHistory(PackageHistory packageHistory);
}
