package eu.europa.ec.opoce.cellar.common.transformer;

import java.util.Map;

/**
 * Converts the typed object into a Map<String, Object> used by velocity.
 * @author phvdveld
 *
 * @param <T>
 */
public interface MapTransformer<T> {

    /**
     * Executes the transformation of the given type to a Map<String, Object>.
     * @param t the type to transform
     * @return a Map<String, Object>
     */
    Map<String, Object> transform(T t);
}
