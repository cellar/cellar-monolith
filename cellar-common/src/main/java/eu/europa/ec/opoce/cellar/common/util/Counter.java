/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.util
 *             FILE : Counter.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 15, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.util;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 15, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class Counter {

    private long val;

    public Counter() {
        this(0);
    }

    public Counter(final long intialVal) {
        this.val = intialVal;
    }

    public void increaseBy(final long increment) {
        this.val += increment;
    }

    public void increase() {
        this.increaseBy(1);
    }

    public void decreaseBy(final long decrement) {
        this.val -= decrement;
    }

    public void decrease() {
        this.decreaseBy(1);
    }

    public long getVal() {
        return this.val;
    }

}
