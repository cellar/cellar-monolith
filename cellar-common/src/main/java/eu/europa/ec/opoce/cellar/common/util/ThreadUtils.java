/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.support
 *             FILE : ThreadUtils.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Aug 30, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Aug 30, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class ThreadUtils {

    private static final Logger LOG = LoggerFactory.getLogger(ThreadUtils.class);

    /**
     * Waits for the {@link condition} to be satisfied.<br>
     * The condition is checked each {@link pause} milliseconds: if {@link pause} <= 0, then it checks continuously with no pause.<br>
     * If {@link timeout} milliseconds are reached, then it stops to wait, regardless the {@link condition}. If {@link timeout} <= 0, there is no timeout.
     * 
     * @param condition the condition to wait for
     * @param pause the pause in milliseconds
     * @param timeout the timeout in milliseconds
     * @param printInterruptedException if true, it prints the possible InterruptedException
     */
    public static void waitFor(final boolean condition, final long pause, final long timeout, final boolean printInterruptedException) {
        final long myPause = (pause < 0 ? 0 : pause);
        final long myTimeout = (timeout < 0 ? Long.MAX_VALUE : timeout);
        final long startMillis = System.currentTimeMillis();
        while (!condition && System.currentTimeMillis() - startMillis < myTimeout) {
            waitFor(myPause, printInterruptedException);
        }
    }

    /**
     * Waits for {@link millis} milliseconds.<br>
     * If {@link millis} <= 0, then it does not wait.
     * 
     * @param millis the milliseconds to wait
     * @param printInterruptedException if true, it prints the possible InterruptedException
     */
    public static void waitFor(final long millis, final boolean printInterruptedException) {
        final long myMillis = (millis < 0 ? 0 : millis);
        try {
            Thread.sleep(myMillis);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            if (printInterruptedException) {
                LOG.info("An unexpected error occurred while waiting.", e);
            }
        }
    }

}
