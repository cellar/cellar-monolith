/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.audit
 *             FILE : AuditTrailEvent.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 02-10-2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.audit;

import eu.europa.ec.opoce.cellar.cl.domain.package_history.PackageHistory;
import eu.europa.ec.opoce.cellar.cl.domain.structmap_status_history.StructMapStatusHistory;
import eu.europa.ec.opoce.cellar.common.util.StringUtils;
import org.hibernate.validator.constraints.Length;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * <class_description> Audit Trail Event object.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 02-10-2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Entity
@Table(name = "AUDIT_TRAIL_EVENT")
@NamedQueries({
        @NamedQuery(name = "getLogsByIdentifier", query = "select ate from AuditTrailEvent ate where ate.objectIdentifier = ?0 order by ate.date asc"),
        @NamedQuery(name = "getLogsByIdentifierWildcard", query = "select ate from AuditTrailEvent ate where ate.objectIdentifier like ?0 order by ate.date asc"),
        @NamedQuery(name = "getLogsByProcess", query = "select ate from AuditTrailEvent ate where ate.process = ?0 order by ate.date asc"),
        @NamedQuery(name = "getLogsByProcessAndIdentifier", query = "select ate from AuditTrailEvent ate where ate.process = ?0 and ate.objectIdentifier = ?1 order by ate.date asc"),
        @NamedQuery(name = "getLogsByProcessAndIdentifierWildcard", query = "select ate from AuditTrailEvent ate where ate.process = ?0 and ate.objectIdentifier like ?1 order by ate.date asc"),
        @NamedQuery(name = "getLogsByDate", query = "select ate from AuditTrailEvent ate where ate.date >= ?0 and ate.date <= ?1 order by ate.date asc"),
        @NamedQuery(name = "getLogsByThreadName", query = "select ate from AuditTrailEvent ate where ate.threadName = ?0 order by ate.date asc"),
        @NamedQuery(name = "countLogsBetweenDates", query = "select count(*) from AuditTrailEvent ate where ate.date >= :dateFrom and ate.date <= :dateTo"),
        @NamedQuery(name = "countLogsBetweenDatesByIdentifier", query = "select count(*) from AuditTrailEvent ate where ate.date >= :dateFrom and ate.date <= :dateTo and ate.objectIdentifier = :objectIdentifier"),
        @NamedQuery(name = "countLogsBetweenDatesByProcess", query = "select count(*) from AuditTrailEvent ate where ate.date >= :dateFrom and ate.date <= :dateTo and ate.process = :process"),
        @NamedQuery(name = "countLogsBetweenDatesByIdentifierAndProcess", query = "select count(*) from AuditTrailEvent ate where ate.date >= :dateFrom and ate.date <= :dateTo and ate.objectIdentifier = :objectIdentifier and ate.process = :process")})
public class AuditTrailEvent {

    /**
     * Logger for this class.
     */
    @SuppressWarnings("unused")
    private transient static final Logger LOGGER = LoggerFactory.getLogger(AuditTrailEvent.class);

    /**
     * Identifier of the event.
     */
    @Id
    @GeneratedValue
    private Long id;

    /**
     * Process of the event.
     */
    @NotNull
    @Column(name = "PROCESS")
    @Enumerated(EnumType.STRING)
    private AuditTrailEventProcess process;

    /**
     * Action of the event.
     */
    @NotNull
    @Column(name = "ACTION")
    @Enumerated(EnumType.STRING)
    private AuditTrailEventAction action;

    /**
     * Identifier of the object topic of the event.
     */
    @NotNull
    @Column(name = "OBJECT_IDENTIFIER")
    private String objectIdentifier;

    /**
     * Date of the event.
     */
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "EVENT_DATE")
    private Date date;

    /**
     * Type of the event.
     */
    @NotNull
    @Column(name = "TYPE")
    @Enumerated(EnumType.STRING)
    private AuditTrailEventType type;

    /**
     * Message value.
     */
    @Length(min = 1, max = 4000)
    @Column(name = "MESSAGE", length = 4000)
    private String message;

    /**
     * Name of the underlying thread.
     */
    @Column(name = "THREAD_NAME")
    private String threadName;
    
    /**
     * Structmap the logging event may be referring to.
     * e.g. during ingestion process
     */
    @ManyToOne
    @JoinColumn(name = "STRUCTMAP_ID")
    private StructMapStatusHistory structMapStatusHistory;
    
    /**
     * Package the logging event may be referring to.
     * e.g. during ingestion process
     */
    @ManyToOne
    @JoinColumn(name = "PACKAGE_ID")
    private PackageHistory packageHistory;

    /**
     * Default constructor.
     */
    public AuditTrailEvent() {
    }

    /**
     * Create a new <code>AuditTrailEvent</code> instance.
     * 
     * @param process the process of this audit
     * @param action action performed
     * @param objectIdentifier related object identifier
     * @param type type of the event
     */
    public AuditTrailEvent(final AuditTrailEventProcess process, final AuditTrailEventAction action, final String objectIdentifier,
            final AuditTrailEventType type) {
        this.process = process;
        this.setAction(action);
        this.setObjectIdentifier(objectIdentifier);
        this.setType(type);
        this.setDate(new Date());
    }

    public Long getId() {
        return id;
    }

    public AuditTrailEventProcess getProcess() {
        return process;
    }

    public AuditTrailEventAction getAction() {
        return action;
    }

    public void setAction(AuditTrailEventAction action) {
        this.action = action;
    }

    public String getObjectIdentifier() {
        return objectIdentifier;
    }

    public void setObjectIdentifier(String objectIdentifier) {
        this.objectIdentifier = objectIdentifier;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public AuditTrailEventType getType() {
        return type;
    }

    public void setType(AuditTrailEventType type) {
        this.type = type;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        if (message != null) {
            this.message = StringUtils.abbreviateRight(message, 4000);
        }
    }

    public String getThreadName() {
        return threadName;
    }

    public void setThreadName(String threadName) {
        this.threadName = threadName;
    }
    
    public void setStructMapStatusHistory(StructMapStatusHistory structMapStatusHistory) {
        this.structMapStatusHistory = structMapStatusHistory;
    }
    
    public void setPackageHistory(PackageHistory packageHistory) {
        this.packageHistory = packageHistory;
    }
    
    public StructMapStatusHistory getStructMapStatusHistory() {
        return structMapStatusHistory;
    }
    
    public PackageHistory getPackageHistory() {
        return packageHistory;
    }
    
    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder("AuditTrailEvent {");
        builder.append(this.getId() != null ? "id='" + this.getId() + "', " : "");
        builder.append(this.getProcess() != null ? "process='" + this.getProcess() + "', " : "");
        builder.append(this.getAction() != null ? "action='" + this.getAction() + "', " : "");
        builder.append(this.getObjectIdentifier() != null ? "objectIdentifier='" + this.getObjectIdentifier() + "', " : "");
        builder.append(this.getDate() != null ? "date='" + this.getDate() + "', " : "");
        builder.append(this.getType() != null ? "type='" + this.getType() + "', " : "");
        builder.append(this.getMessage() != null ? "message='" + this.getMessage() + "', " : "");
        builder.append(this.getThreadName() != null ? "threadName='" + this.getThreadName() + "' " : "");
        builder.append(this.getStructMapStatusHistory() != null ? "structmapId='" + this.getStructMapStatusHistory().getId()  + "', " : "");
        builder.append(this.getPackageHistory() != null ? "packageId='" + this.getPackageHistory().getId() + "' " : "");
        builder.append("}");
        return builder.toString();
    }
}
