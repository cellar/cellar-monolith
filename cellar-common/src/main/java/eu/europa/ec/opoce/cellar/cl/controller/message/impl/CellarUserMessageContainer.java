/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.util.impl
 *             FILE : CellarUserMessageContainer.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 22, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.message.impl;

import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage;
import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessageContainer;
import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

/**
 * <class_description> Cellar user message container.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 22, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component(value = "cellarUserMessageContainer")
@Scope(value = "session", proxyMode = ScopedProxyMode.INTERFACES)
public class CellarUserMessageContainer implements ICellarUserMessageContainer {

    /**
     * 
     */
    private static final long serialVersionUID = 7136641362428717220L;

    /**
     * Cellar user messages by level.
     */
    private Map<LEVEL, List<ICellarUserMessage>> cellarUserMessages;

    /**
     * Constructor.
     */
    public CellarUserMessageContainer() {
        this.cellarUserMessages = new HashMap<ICellarUserMessage.LEVEL, List<ICellarUserMessage>>();

        for (LEVEL level : LEVEL.values()) { // init the different levels
            this.cellarUserMessages.put(level, new ArrayList<ICellarUserMessage>());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ICellarUserMessage> getInfoMessages() {
        return this.getAndClearMessages(LEVEL.INFO);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ICellarUserMessage> getSuccessMessages() {
        return this.getAndClearMessages(LEVEL.SUCCESS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ICellarUserMessage> getErrorMessages() {
        return this.getAndClearMessages(LEVEL.ERROR);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ICellarUserMessage> getWarningMessages() {
        return this.getAndClearMessages(LEVEL.WARNING);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addMessage(ICellarUserMessage message) {
        this.cellarUserMessages.get(message.getMessageLevel()).add(message);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addMessage(LEVEL messageLevel, String messageCode) {
        this.cellarUserMessages.get(messageLevel).add(new CellarUserMessage(messageLevel, messageCode));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addMessage(LEVEL messageLevel, String messageCode, String defaultMessage) {
        this.cellarUserMessages.get(messageLevel).add(new CellarUserMessage(messageLevel, messageCode, defaultMessage));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addMessage(LEVEL messageLevel, String messageCode, String defaultMessage, Object[] messageArguments) {
        this.cellarUserMessages.get(messageLevel).add(new CellarUserMessage(messageLevel, messageCode, defaultMessage, messageArguments));
    }

    /**
     * Gets and clear the messages at the specified level.
     * @param level the level concerned by the get and clear
     * @return the list of messages at the specified level
     */
    private List<ICellarUserMessage> getAndClearMessages(final LEVEL level) {
        final List<ICellarUserMessage> messages = this.cellarUserMessages.get(level);

        final List<ICellarUserMessage> tmp = new ArrayList<ICellarUserMessage>(messages.size());
        tmp.addAll(messages); // copy the messages
        messages.clear(); // clear the message in the session

        return tmp;
    }
}
