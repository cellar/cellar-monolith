package eu.europa.ec.opoce.cellar.cl.domain.ontology;

import java.io.Serializable;

/**
 * The Class FileMetadata.
 * used for the response of the jquery file upload plugin
 */
public class FileMetadata implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The name. */
    private String name;

    /** The size. */
    private long size;

    /** The error.
     * To display the error messages we will use the ICellarUserMessageContainer
     * The messages are not asynchronous the page has to be refreshed to display the errors
     * Once the page is refreshed we loose the list of previously chosen files
     * Giving this property in the response allows asynchronous messages display
     * This is kept as a reference for possible future use if needed
     * */
    private String error;

    public FileMetadata() {
        this("", 0L);
    }

    public FileMetadata(String name, long size) {
        this.name = name;
        this.size = size;
    }

    /**
     * Gets the error.
     *
     * @return the error
     */
    public String getError() {
        return this.error;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    public String getName() {
        return this.name;
    }

    /**
     * Gets the size.
     *
     * @return the size
     */
    public long getSize() {
        return this.size;
    }

    /**
     * Sets the error.
     *
     * @param error the new error
     */
    public void setError(final String error) {
        this.error = error;
    }

    /**
     * Sets the name.
     *
     * @param name the new name
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Sets the size.
     *
     * @param size the new size
     */
    public void setSize(final long size) {
        this.size = size;
    }

}
