package eu.europa.ec.opoce.cellar.common.http.headers;

import java.util.Date;

public interface ILink {

    /**
     * Returns the URI associated to the link.
     * @return a stringified URI.
     */
    String getUri();

    /**
     * Returns the link rel parameter, or null if this parameter is not specified.
     * @return the relation type as a String, or null.
     */
    RelType getRel();

    /**
     * Returns the datetime parameter, or null if this parameter is not specified.
     * @return the datetime asa String, or null.
     */
    Date getDateTime();

    /**
     * Returns the link as a formatted String, e.g.:
     * <http://localhost:5000/memento/01992L0043>; rel="original timegate"
     * @return
     */
    String toString();

}
