/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.monitoring
 *             FILE : Watch.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 07 12, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-07-12 09:25:57 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.monitoring;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

/**
 * @author ARHS Developments
 * @see WatchAdvice
 * @since 7.5
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Watch {

    /**
     * The name of the {@link Watch}. This name is essentially
     * used to understand the business context of the execution.
     *
     * @return the name
     */
    String value();

    /**
     * The array of arguments to add to the context for a better
     * understanding of the current execution.<br/>
     *
     * @return the arguments
     * @see WatchArgument
     * <pre>
     * {@code
     * @Watch(value = "Update SIP Mets", arguments = {
     *      @WatchArgument(name = "Sip name", expression = "sipMetsProcessing.sipName"),
     *      @WatchArgument(name = "Sip priority", expression = "sipMetsProcessing.priority"),
     *      @WatchArgument(name = "Sip type", expression = "sipMetsProcessing.type")
     * })
     * public void updateSipMetsProcessing(SipMetsProcessing sipMetsProcessing) {
     *  ...
     * }
     * </pre>
     */
    WatchArgument[] arguments() default {};

    /**
     * By default the value of the time spent in a method is
     * converted into MILLISECONDS, however it can be more readable
     * by converted it into another {@link TimeUnit} (like SECONDS
     * or MINUTES for very long process).<br/>
     *
     * @return the unit
     */
    TimeUnit unit() default TimeUnit.MILLISECONDS;

    @Documented
    @Retention(RetentionPolicy.RUNTIME)
    @Target(ElementType.METHOD)
    @interface WatchArgument {

        /**
         * @return the name
         */
        String name();

        /**
         * The SpEL expression used to navigated into the Java bean.
         *
         * @return the expression
         */
        String expression();

    }

}
