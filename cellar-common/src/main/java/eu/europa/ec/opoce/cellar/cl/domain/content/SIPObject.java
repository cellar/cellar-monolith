/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.content
 *             FILE : SIPObject.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 25-Oct-2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.content;

import eu.europa.ec.opoce.cellar.common.util.StringUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsPackage;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import static eu.europa.ec.opoce.cellar.CommonErrors.ERROR_WHILE_PARSING_THE_METS_DOCUMENT;
import static eu.europa.ec.opoce.cellar.CommonErrors.FILE_INPUT_COULD_NOT_BE_OPENED;
import static eu.europa.ec.opoce.cellar.CommonErrors.NO_METS_ENTRY_IN_ZIPFILE;

/**
 * The class SIPObject.
 * <class_description>  This class contains information about the SIP package like the SIP content, the zip file location,
 *                      the zip extraction folder and provides methods to easily retrieve the METS and the other files.
 * <br/><br/>
 * ON : 25-Oct-2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class SIPObject implements Iterable<File>, Serializable, MetsPackage {

    /** Logger instance. */
    private static final Logger LOG = LoggerFactory.getLogger(SIPObject.class);

    /**
     * The <code>serialVersionUID</code>.
     */
    private static final long serialVersionUID = -8575441368957691585L;

    private static final String METS_FILTER_STRING = ".mets.xml";

    /**
     * Folder where the sip file has been uncompressed. 
     */
    private File zipExtractionFolder;

    /**
     * Zip file. 
     */
    private File zipFile;

    /**
     * The list of file related to the sip Object.
     */
    private List<File> fileList;

    /**
     * The sip's type archive (AUTHENTICOJ, DAILY, BULK).
     */
    private TYPE sipType;

    /**
     * The map containing relative path of a file from the uncompressed folder as key and the absolute path of this file as value
     */
    private Map<String, Path> absolutePathMap;

    /**
     * {@inheritDoc}
     */
    @Override
    public InputStream getMetsStream() {
        try {
            for (String relativePathKey : absolutePathMap.keySet()) {
                if (relativePathKey.endsWith(METS_FILTER_STRING))
                    return new FileInputStream(absolutePathMap.get(relativePathKey).toFile());
            }
        } catch (FileNotFoundException fnfe) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("[Filter: {}] A METS is present in the SIP file list [{}] but not on the file system")
                    .withMessageArgs(METS_FILTER_STRING, fileList)
                    .withCause(fnfe)
                    .withCode(ERROR_WHILE_PARSING_THE_METS_DOCUMENT)
                    .build();
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    public File getMetsFile() {
        for (String relativePathKey : absolutePathMap.keySet()) {
            if (relativePathKey.endsWith(METS_FILTER_STRING))
                return absolutePathMap.get(relativePathKey).toFile();
        }
        throw ExceptionBuilder.get(CellarException.class)
                .withMessage("[Filter: {}] There is no METS file present the file list: {}")
                .withMessageArgs(METS_FILTER_STRING, fileList)
                .withCode(NO_METS_ENTRY_IN_ZIPFILE)
                .build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InputStream getFileStream(final String fileRef) {
        try {
            String fileRefWithReplacedFileSeparator = replaceWithUnixFileSeparator(fileRef);
            Path normalizedPath = Paths.get(fileRefWithReplacedFileSeparator).normalize();
            Path absolutePath = absolutePathMap.get(normalizedPath.toString());
            if (absolutePath != null)
                return new FileInputStream(absolutePath.toFile());
        } catch (FileNotFoundException fnfe) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("The file '{}' is present in the file list: [{}] but was not found on the filesystem.")
                    .withMessageArgs(fileRef, fileList)
                    .withCause(fnfe)
                    .withCode(FILE_INPUT_COULD_NOT_BE_OPENED)
                    .build();
        }
        return null;
    }

    public static String replaceWithUnixFileSeparator(String fileRef) {
        if (StringUtils.isNotBlank(fileRef)) {
            return StringUtils.replaceBackslashesWithSlashes(fileRef);
        }
        return "";
    }

    /**
     * Returns an iterator over a set of Files.
     *
     * @return an iterator.
     */
    @Override
    public Iterator<File> iterator() {
        return fileList.iterator();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "SIPObject (type? " + getSipType() + ") - [fileList=" + StringUtils.join(fileList, ", ") + "]";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public File getZipExtractionFolder() {
        return zipExtractionFolder;
    }

    /**
     * {@inheritDoc}
     */
    public void setZipExtractionFolder(File zipExtractionFolder) {
        if (!zipExtractionFolder.isAbsolute())
            zipExtractionFolder = zipExtractionFolder.getAbsoluteFile();
        this.zipExtractionFolder = zipExtractionFolder;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public File getZipFile() {
        return zipFile;
    }

    /**
     * {@inheritDoc}
     */
    public void setZipFile(File zipFile) {
        this.zipFile = zipFile;
    }

    /**
     * @return the sipType
     */
    public TYPE getSipType() {
        return sipType;
    }

    /**
     * @param sipType the sipType to set
     */
    public void setSipType(TYPE sipType) {
        this.sipType = sipType;
    }

    /**
     * @return a map containing relative path to a file as key for which the values are the absolute path on the system
     */
    public Map<String, Path> getAbsolutePathMap() {
        if (absolutePathMap == null)
            absolutePathMap = new HashMap<>();
        return absolutePathMap;
    }

    /**
     * Gets the value of the fileList.
     *
     * @return the fileList
     */
    public List<File> getFileList() {
        if (fileList == null)
            fileList = new ArrayList<>();
        return fileList;
    }

    /**
     * Sets a new value for the fileList.
     * This will trigger the (re)creation of an internal map containing relative path - absolute path for each file in the list
     *
     * @param fileList
     *            the fileList to set
     */
    public void setFileList(final List<File> fileList) {
        this.fileList = fileList;
        createMapWithPath();
    }

    private void createMapWithPath() {
        absolutePathMap = new HashMap<>();

        for (File file : fileList) {
            Path zipExtractionFolderAbsolutePath = zipExtractionFolder.toPath().toAbsolutePath();
            Path fileAbsolutePath = Paths.get(zipExtractionFolderAbsolutePath + "/" + file.toPath()).normalize();
            Path relativePathKey = zipExtractionFolderAbsolutePath.relativize(fileAbsolutePath).normalize();
            absolutePathMap.put(relativePathKey.toString(), fileAbsolutePath);
        }
    }
}
