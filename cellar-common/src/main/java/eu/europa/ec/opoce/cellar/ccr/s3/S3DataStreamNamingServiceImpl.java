/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ccr.s3
 *             FILE : S3DataStreamNamingServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 7, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$$
 *          VERSION : $LastChangedRevision$$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.s3;

import eu.europa.ec.opoce.cellar.cl.configuration.IS3Configuration;
import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 7, 2015
 *
 * @author ARHS Developments
 * @version $Revision$$
 */
@Service
public class S3DataStreamNamingServiceImpl implements S3DataStreamNamingService {

    @Autowired
    @Qualifier("cellarConfiguration")
    private IS3Configuration s3Configuration;

    private static final String CONTENT_PREFIX = "DOC_";

    @Override
    public void renameCellarIdentifier(final CellarIdentifier cellarIdentifier, final String label,
                                       final String baseUuid, final int docIndex) {
        final boolean enabled = this.s3Configuration.getS3ServiceIngestionDatastreamRenamingEnabled();
        if (!enabled) {
            cellarIdentifier.setUuid(baseUuid + "/" + label);
        } else {
            cellarIdentifier.setUuid(baseUuid + "/" + CONTENT_PREFIX + docIndex);
        }
        cellarIdentifier.setDocIndex(docIndex);
    }

}
