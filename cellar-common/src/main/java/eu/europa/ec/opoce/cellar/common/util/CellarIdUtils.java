/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.util
 *             FILE : CellarIdUtils.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 30, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.util;

import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * <class_description> The cellar id utils.
 * <br/><br/>
 * ON : Apr 30, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class CellarIdUtils {

    public final static String PREFIX_SEPARATOR = ":";
    public final static String I_SEPARATOR = "/";
    public final static String WEM_SEPARATOR = ".";
    public final static String TRANSFORMATION_PREFIX = "transformed" + PREFIX_SEPARATOR;

    private final static String FILENAME_REGEX = PREFIX_SEPARATOR + "|" + "\\" + WEM_SEPARATOR + "|" + I_SEPARATOR;

    public static Collection<String> getCellarIds(final Collection<String> pids) {
        final List<String> cellarIds = new LinkedList<String>();

        for (final String pid : pids) {
            if (pid.startsWith(ContentIdentifier.CELLAR_PREFIX)) {
                cellarIds.add(pid);
            }
        }

        return cellarIds;
    }

    public static String getCellarIdAsFilename(final String cellarId) {
        return cellarId.replaceAll(FILENAME_REGEX, "_");
    }

    public static String getCellarIdAsFilename(final String cellarId, final String extension) {
        return getCellarIdAsFilename(cellarId) + extension;
    }

    public static String getCellarIdBase(final String cellarId) {
        String result = null;
        final int index = cellarId.indexOf(WEM_SEPARATOR);
        if (index > 0) {
            result = cellarId.substring(0, index);
        } else {
            result = cellarId;
        }

        if (result.contains(TRANSFORMATION_PREFIX)) {
            result = StringUtils.substringAfter(result, TRANSFORMATION_PREFIX);
        }

        return result;
    }

    public static String getParentCellarId(final String cellarId) {
        if (StringUtils.isBlank(cellarId)) {
            return null;
        }

        if (DigitalObjectType.isItem(cellarId)) {
            final int index = cellarId.lastIndexOf(I_SEPARATOR);
            return cellarId.substring(0, index);
        }

        final int index = cellarId.lastIndexOf(WEM_SEPARATOR);
        if (index > 0) {
            return cellarId.substring(0, index);
        }

        return null;
    }

    public static String getCellarIdSuffix(final String cellarId) {
        if (!cellarId.startsWith(ContentIdentifier.CELLAR_PREFIX + PREFIX_SEPARATOR)) {
            throw new IllegalArgumentException(cellarId + " is not a prefixed cellar identifier.");
        }

        final int index = cellarId.indexOf(PREFIX_SEPARATOR);

        if ((index + 1) >= cellarId.length()) {
            return null;
        }

        return cellarId.substring(index + 1);
    }

    public static String getRootCellarId(final String cellarId) {
        return StringUtils.substringBefore(cellarId, WEM_SEPARATOR);
    }

    /**
     * This method returns a String representing a relative path on the file system based on the CELLAR ID.
     * Only the expressions, manifestations and items are returned.
     * For example:
     * - cellar:uuid.0001.01 => 0001/01
     * - cellar:uuid.0001.01/DOC_1 => 0001/01/DOC_1
     * - cellar:uuid => empty string
     *
     * @param cellarId the complete and valid cellar id which contains cellar:
     * @return the digital object hierarchy represented as a file path with '/'
     */
    public static String createPathFromCellarId(String cellarId) {
        int slash = cellarId.indexOf("/");
        String id = slash > 0 ? cellarId.substring(0, slash) : cellarId;
        String doc = slash > 0 ? cellarId.substring(slash) : "";
        StringBuilder builder = new StringBuilder();
        String[] elements = id.split("\\" + WEM_SEPARATOR);
        for (String element : elements) {
            if (!element.startsWith(ContentIdentifier.CELLAR_PREFIX) && !element.contains("-")) {
                builder.append(element);
                builder.append("/");
            }
        }
        builder.append(doc);
        int indexOfLastFileSeparator = builder.toString().lastIndexOf("/");
        if (indexOfLastFileSeparator != -1)
            builder.deleteCharAt(indexOfLastFileSeparator);
        return builder.toString();
    }
}
