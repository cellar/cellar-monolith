package eu.europa.ec.opoce.cellar.cl.dao.datatable;

import eu.europa.ec.opoce.cellar.cl.domain.datatable.DatatableColumnsWrapper;

import java.util.List;

import org.springframework.jdbc.core.RowMapper;

/**
 * The Interface DatatableDao.
 */
public interface DatatableDao {

    /**
     * Find all.
     *
     * @param columnsWrapper the columns wrapper
     * @param startPage the start page
     * @param pageSize the page size
     * @param resultTransformer the result transformer
     * @return the list
     */
    List<String[]> findAll(final DatatableColumnsWrapper columnsWrapper, final int startPage, final int pageSize,
            final RowMapper<String[]> rowMapper);

    /**
     * Count.
     *
     * @param columnsWrapper the columns wrapper
     * @return the count
     */
    Integer count(final DatatableColumnsWrapper columnsWrapper);

    /**
     * Count all.
     *
     * @param columnsWrapper the columns wrapper
     * @return the count
     */
    Integer countAll(final DatatableColumnsWrapper columnsWrapper);

}
