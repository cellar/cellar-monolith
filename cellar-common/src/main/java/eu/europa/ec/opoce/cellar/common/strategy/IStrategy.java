/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.common.strategy
 *        FILE : IStrategy.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 20-03-2013
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.strategy;

/**
 * <class_description> General purpose strategy.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 20-03-2013
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IStrategy<T> {

    /**
     * Applies the strategy.<br />
     * Override it with the implementation of the desired strategy.
     * 
     * @param param the parameter of the strategy
     */
    void applyStrategy(T param);

}
