/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.concurrency
 *             FILE : ConcurrentAdvice.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 19-12-2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.concurrency;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.cl.concurrency.Concurrent.Locker;
import eu.europa.ec.opoce.cellar.domain.content.mets.OffIngestionOperationType;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;

/**
 * <class_description> This is an advice that, at each method annotated with {@link Concurrent}, drives the access towards the database in order to avoid deadlocks.<br/>
 * If necessary, it locks the resources to process following the strategy defined either by attribute {@link lockFor}, either by the operation type itself.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 19-12-2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Aspect
@Order(1)
public class ConcurrentAdvice {

    private static final ConcurrentAdvice instance = new ConcurrentAdvice();

    @Around("@annotation(concurrent)")
    public Object process(final ProceedingJoinPoint pjp, final Concurrent concurrent) throws Throwable {
        Object retObject = null;

        // collect arguments
        final Object[] pjpArgs = pjp.getArgs();
        Object[] lockArgs = null;
        final Object callingObject = pjp.getThis();
        if (callingObject instanceof IConcurrentArgsResolver) {
            lockArgs = ((IConcurrentArgsResolver) callingObject).resolveArgsForConcurrency(pjpArgs);
        } else {
            lockArgs = pjpArgs;
        }

        // try to retrieve the non-ingestion lock type
        Locker locker = concurrent.locker();
        // if unknown, then it means it is an ingestion, and try to retrieve the lock type from the operation itself
        if (locker == OffIngestionOperationType.UNKNOWN) {
            final StructMap structMap = find(lockArgs, StructMap.class);
            locker = structMap.getMetsDocument().getType();
        }

        final ICellarLockSession cellarLockSession = locker.createCellarLockSession();
        try {
            // lock accordingly to the lock type
            locker.lock(cellarLockSession, lockArgs);

            // proceed
            retObject = pjp.proceed(pjpArgs);
        } finally {
            // release the lock
            locker.unlock(cellarLockSession);
        }

        return retObject;
    }

    /**
     * Needed by AspectJ
     */
    public static ConcurrentAdvice aspectOf() {
        return instance;
    }

    @SuppressWarnings("unchecked")
    private static <T> T find(final Object[] args, final Class<T> argClassToFind) {
        T toReturn = null;

        for (final Object o : args) {
            if (o.getClass().isAssignableFrom(argClassToFind)) {
                toReturn = (T) o;
                break;
            }
        }

        if (toReturn == null) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.NOT_IMPLEMENTED)
                    .withMessage(
                            "Cannot drive the concurrency controller properly, as the required argument of type {} is not present in the method annotated with {}.")
                    .withMessageArgs(argClassToFind, Concurrent.class).build();
        }
        return toReturn;
    }

}
