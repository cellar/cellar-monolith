package eu.europa.ec.opoce.cellar.common.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author ARHS Developments
 */
public final class MapUtils {

    private MapUtils() {
    }

    /**
     * <p>addValueToMappedSet</p>
     *
     * @param map a {@link java.util.Map} object.
     * @param key a T object.
     * @param value a U object.
     * @param <U> a U object.
     * @param <T> a T object.
     */
    public static <U, T> void addValueToMappedSet(Map<T, Set<U>> map, T key, U value) {
        Set<U> set = addMappedSet(map, key);
        set.add(value);
    }

    /**
     * <p>addMappedSet</p>
     *
     * @param map a {@link java.util.Map} object.
     * @param key a T object.
     * @param <U> a U object.
     * @param <T> a T object.
     * @return a {@link java.util.Set} object.
     */
    public static <U, T> Set<U> addMappedSet(Map<T, Set<U>> map, T key) {
        return map.computeIfAbsent(key, k -> new HashSet<U>());
    }

    /**
     * <p>addMappedMap</p>
     *
     * @param map a {@link java.util.Map} object.
     * @param key a T object.
     * @param <U> a U object.
     * @param <Y> a Y object.
     * @param <T> a T object.
     * @return a {@link java.util.Map} object.
     */
    public static <U, Y, T> Map<U, Y> addMappedMap(Map<T, Map<U, Y>> map, T key) {
        return map.computeIfAbsent(key, k -> new HashMap<U, Y>());
    }

    /**
     * <p>addValueToMappedList</p>
     *
     * @param map a {@link java.util.Map} object.
     * @param key a T object.
     * @param value a U object.
     * @param <U> a U object.
     * @param <T> a T object.
     */
    public static <U, T> void addValueToMappedList(Map<T, List<U>> map, T key, U value) {
        List<U> list = addMappedList(map, key);
        list.add(value);
    }

    /**
     * <p>addMappedList</p>
     *
     * @param map a {@link java.util.Map} object.
     * @param key a T object.
     * @param <U> a U object.
     * @param <T> a T object.
     * @return a {@link java.util.List} object.
     */
    public static <U, T> List<U> addMappedList(Map<T, List<U>> map, T key) {
        return map.computeIfAbsent(key, k -> new ArrayList<>());
    }
}
