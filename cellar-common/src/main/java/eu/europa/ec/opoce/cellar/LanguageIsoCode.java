/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar
 *             FILE : LanguageIsoCode.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 19-Apr-2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar;

import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * The Class LanguageIsoCode.
 *
 * @author ARHS Developments
 * @version $$Revision$$$
 */
public class LanguageIsoCode implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1273814012146823020L;

    /** The Constant ISO_TWO_CHAR_CODE_LENGTH. */
    public static final int ISO_TWO_CHAR_CODE_LENGTH = 2;

    /** The Constant ISO_THREE_CHAR_CODE_LENGTH. */
    public static final int ISO_THREE_CHAR_CODE_LENGTH = 3;

    /** The iso code two char. */
    private String isoCodeTwoChar;

    /** The iso code three char. */
    private String isoCodeThreeChar;

    /**
     * Gets the iso code two char.
     *
     * @return the iso code two char
     */
    public String getIsoCodeTwoChar() {
        return this.isoCodeTwoChar;
    }

    /**
     * Gets the iso code three char.
     *
     * @return the iso code three char
     */
    public String getIsoCodeThreeChar() {
        return this.isoCodeThreeChar;
    }

    /**
     * Sets the iso code two char.
     *
     * @param isoCodeTwoChar the new iso code two char
     */
    public void setIsoCodeTwoChar(final String isoCodeTwoChar) {
        if (StringUtils.isBlank(isoCodeTwoChar) || ((isoCodeTwoChar.length() != ISO_TWO_CHAR_CODE_LENGTH)
                && (this.isoCodeThreeChar.length() != ISO_THREE_CHAR_CODE_LENGTH))) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.LANGUAGE_CODE_IS_INVALID)
                    .withMessage("{} is not a valid iso639_1").withMessageArgs(isoCodeTwoChar).build();
        }
        this.isoCodeTwoChar = isoCodeTwoChar;
    }

    /**
     * Sets the iso code three char.
     *
     * @param isoCodeThreeChar the new iso code three char
     */
    public void setIsoCodeThreeChar(final String isoCodeThreeChar) {
        if (StringUtils.isBlank(isoCodeThreeChar) || (isoCodeThreeChar.length() != ISO_THREE_CHAR_CODE_LENGTH)) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.LANGUAGE_CODE_IS_INVALID)
                    .withMessage("{} is not a valid iso639_3").withMessageArgs(isoCodeThreeChar).build();
        }
        this.isoCodeThreeChar = isoCodeThreeChar;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof LanguageIsoCode)) {
            return false;
        }
        final LanguageIsoCode other = (LanguageIsoCode) obj;
        final EqualsBuilder equalsBuilder = new EqualsBuilder();
        equalsBuilder.append(this.isoCodeTwoChar, other.isoCodeTwoChar);
        equalsBuilder.append(this.isoCodeThreeChar, other.isoCodeThreeChar);
        return equalsBuilder.isEquals();
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

}
