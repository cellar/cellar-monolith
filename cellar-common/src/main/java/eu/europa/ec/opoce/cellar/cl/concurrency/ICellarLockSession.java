/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.concurrency
 *             FILE : ICellarLockSession2.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 30, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.concurrency;

import eu.europa.ec.opoce.cellar.cl.concurrency.Concurrent.Locker;
import eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLock.MODE;

import java.util.List;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

/**
 * <class_description> Cellar lock session interface.
 * <br/><br/>
 * ON : Apr 30, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */

@JsonTypeInfo(use = Id.CLASS)
public interface ICellarLockSession {

    /**
     * Add the lock to the lock session.
     * @param lock the lock to add
     */
    void addCellarLock(final ICellarLock lock, final MODE mode);

    /**
     * Return the read locks concerned by the session.
     * @return the locks path
     */
    List<ICellarLock> getReadCellarLocksPath();

    /**
     * Return the write locks concerned by the session.
     * @return the locks path
     */
    List<ICellarLock> getWriteCellarLocksPath();

    /**
     * Return true if the processing related to this session is started.
     * @return true if the processing related to this session is started
     */
    boolean isWorking();

    /**
     * Set the working status of the processing related to this session.
     * @param working the working status to set
     */
    void setWorking(final boolean working);

    /**
     * Get the process that fired the lock.
     * @return the process that fired the lock
     */
    Locker getLocker();

    /**
     * Get the thread ID of the process that fired the lock.
     * @return the thread of the process that fired the lock
     */
    String getLockerId();
    
    /**
     * Get the locker IDs of the threads that were identified to already possess resource-locks
     * needed by the thread owning this cellar lock session during the resource acquisition phase.
     * @return the related locker IDs.
     */
    Set<String> getConflictedLockerIds();

    /**
     * Return lock acquisition time of the processing related to this session.
     * @return lock acquisition time of the processing related to this session
     */
    long getLockAcquisitionTime();

    /**
     * Set lock acquisition time of the processing related to this session.
     * @param lockAcquisitionTime lock acquisition time to set
     */
    void setLockAcquisitionTime(final long lockAcquisitionTime);
}
