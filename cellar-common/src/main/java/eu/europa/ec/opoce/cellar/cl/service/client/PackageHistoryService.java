package eu.europa.ec.opoce.cellar.cl.service.client;

import eu.europa.ec.opoce.cellar.cl.domain.enums.ProcessStatus;
import eu.europa.ec.opoce.cellar.cl.domain.package_history.PackageHistory;

import java.util.Date;

/**
 * <class_description> Service interface for logging entries in the 'PACKAGE_HISTORY' table.
 *
 * ON : 14-06-2023
 *
 * @author EUROPEAN DYNAMICS S.A.
 */
public interface PackageHistoryService {
    
    /**
     * Save an instance of {@link PackageHistory}.
     *
     * @return corresponding {@link PackageHistory} that was saved (or the null in case of exception).
     */
    PackageHistory saveEntry(PackageHistory packageHistory);
    
    /**
     * Return a persistent instance of {@link PackageHistory} based on its primary key.
     * @param id identifier of {@link PackageHistory}
     * @return corresponding {@link PackageHistory} (or the null in case of exception).
     */
    PackageHistory getEntry(Long id);
    
    /**
     * Return a persistent instance of {@link PackageHistory} based on its METS_PACKAGE_NAME and RECEIVED_DATE.
     *
     * @param metsPackageName the name of the METS package to look for.
     * @param receivedDate the timestamp this package was received.
     *
     * @return the {@link PackageHistory} reference matching the provided arguments.
     */
    PackageHistory findByPackageNameAndDate(String metsPackageName, Date receivedDate);
    
    /**
     * Set the PREPROCESS_INGESTION_STATUS of {@link PackageHistory} entry to the one provided.
     *
     * @param packageHistory the current 'PACKAGE_HISTORY' object.
     * @param newStatus the new status.
     *
     * @return the {@link PackageHistory} that was updated (or the original in case of exception)
     */
    PackageHistory updatePackageHistoryStatus(PackageHistory packageHistory, ProcessStatus newStatus);
}
