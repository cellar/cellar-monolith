/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.util
 *             FILE : ICellarUserMessage.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 22, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.message;

import java.io.Serializable;

/**
 * <class_description> Interface of a Cellar user message.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 22, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface ICellarUserMessage extends Serializable {

    /**
     * <class_description> Type of message.
     * 
     * <br/><br/>
     * <notes> Includes guaranteed invariants, usage instructions and/or examples,
     *         reminders about desired improvements, etc.
     * <br/><br/>
     * ON : Jan 15, 2013
     *
     * @author ARHS Developments
     * @version $Revision$
     */
    public enum LEVEL {
        INFO, SUCCESS, ERROR, WARNING
    }

    /**
     * @return the messageLevel
     */
    LEVEL getMessageLevel();

    /**
     * @return the messageCode
     */
    String getMessageCode();

    /**
     * @return the defaultMessage
     */
    String getDefaultMessage();

    /**
     * @return the messageArguments
     */
    Object[] getMessageArguments();
}
