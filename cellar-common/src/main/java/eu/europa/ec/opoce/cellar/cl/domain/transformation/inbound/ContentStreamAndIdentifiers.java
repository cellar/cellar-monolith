package eu.europa.ec.opoce.cellar.cl.domain.transformation.inbound;

import java.io.InputStream;
import java.util.List;

/**
 * Link a content input stream with 1 or more production identifiers.
 * 
 * @author dcraeye
 */
public interface ContentStreamAndIdentifiers {

    /**
     * Returns the production identifiers of this content stream.
     * 
     * @return a list of production identifiers.
     */
    public List<String> getContentIdentifierList();

    /**
     * Returns the {@link InputStream} of this content.
     * The inputstream is opened and must be close by the caller method.
     *
     * @return the inputstream.
     */
    public InputStream getContentStream();

    /**
     * Returns the manifestation type of this content.
     *
     * @return the manifestation type.
     */
    public String getManifestationType();

    /**
     * Returns the checksum of this content.
     * @return the checksum.
     */
    public String getChecksum();

    /**
     * Returns the checksumType of this content.
     * @return the checksumType.
     */
    public String getChecksumType();

}
