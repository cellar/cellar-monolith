/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.exception
 *             FILE : MetadataValidationResultAwareException.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 10 05, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-10-05 13:23:14 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.exception;

import eu.europa.ec.opoce.cellar.cl.domain.validation.MetadataValidationResult;

/**
 * @author ARHS Developments
 */
public class MetadataValidationResultAwareException extends CellarException {
    public MetadataValidationResultAwareException(final MetadataValidationResultAwareExceptionBuilder<? extends BuildableException> builder) {
        super(builder);
    }

    @SuppressWarnings("unchecked")
    public MetadataValidationResult getMetadataValidationResult() {
        return this.builder().metadataValidationResult;
    }

    public String getStructMapId() {
        return this.builder().structMapId;
    }

    private MetadataValidationResultAwareExceptionBuilder<? extends MetadataValidationResultAwareException> builder() {
        return ((MetadataValidationResultAwareExceptionBuilder<? extends MetadataValidationResultAwareException>) this.builder);
    }
}
