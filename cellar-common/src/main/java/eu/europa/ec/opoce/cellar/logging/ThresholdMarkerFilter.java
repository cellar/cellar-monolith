package eu.europa.ec.opoce.cellar.logging;

import com.google.common.base.MoreObjects;
import com.google.common.base.Strings;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.ThreadContext;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.core.config.Node;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.filter.AbstractFilter;
import org.apache.logging.log4j.core.jmx.Server;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.util.PropertiesUtil;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;

/**
 * This filter use the {@link Marker}s and the {@link LogContext}
 * to filter the log events.
 * <p>
 * {@link Marker} will always override the {@link LogContext}, which
 * means that if a method is annotated with @{@link LogContext} and
 * contain a {@link org.apache.logging.log4j.Logger} which use a
 * {@link Marker} to log something, then the filtering will be done
 * by using the {@link Marker}'s value and not the {@link LogContext}.
 * This capabilities can be useful if a method is used by multiple
 * services with different {@link LogContext} and the message needs
 * to be different with some cases.
 * <p>
 * Only the marker and the level re required, by default, the context
 * is evaluated by using the {@link ThreadContext}.
 * <p>
 * JMX: The filter is configurable at runtime through JMX. The name of
 * the configuration is the name of the {@link Marker}, the
 * {@link LogContext} is only used as a fallback.
 *
 * @author ARHS Developments
 * @see LogContext
 * @since 7.7
 */
@Plugin(name = "ThresholdMarkerFilter", category = Node.CATEGORY, elementType = Filter.ELEMENT_TYPE, printObject = true)
public class ThresholdMarkerFilter extends AbstractFilter implements ThresholdMarkerFilterMBean {

    static final String PROPERTY_DISABLE_JMX = "log4j2.disable.jmx";
    private static final String PATTERN = Server.DOMAIN + ":type=%s,component=ThresholdMarkerFilter,name=%s";

    private final String marker;
    private final String context;
    private volatile Level level;
    private volatile boolean ignoreContext;

    private ThresholdMarkerFilter(final String marker, final String context, final Level level, final Result onMatch,
                                  final Result onMismatch, final boolean ignoreContext) {
        super(onMatch == null ? Result.ACCEPT : onMatch, onMismatch == null ? Result.DENY : onMismatch);
        this.marker = marker;
        this.context = context;
        this.level = level;
        this.ignoreContext = ignoreContext;
    }

    /**
     * Create the ThresholdMarkerFilter.
     *
     * @param marker   The Marker name to match.
     * @param level    The minimum level to match
     * @param match    The action to take if a match occurs.
     * @param mismatch The action to take if no match occurs.
     * @return A MarkerFilter.
     */
    @PluginFactory
    public static ThresholdMarkerFilter createFilter(@PluginAttribute("marker") final String marker,
                                                     @PluginAttribute("context") final String context,
                                                     @PluginAttribute("level") final Level level,
                                                     @PluginAttribute("onMatch") final Result match,
                                                     @PluginAttribute("onMismatch") final Result mismatch,
                                                     @PluginAttribute("ignoreContext") final boolean ignoreContext) {

        if (marker == null || level == null) {
            LOGGER.error("A marker and a level must be provided for ThresholdMarkerFilter");
            return null;
        }
        final ThresholdMarkerFilter filter = new ThresholdMarkerFilter(marker, context, level, match, mismatch, ignoreContext);
        if (!isJmxDisabled()) {
            final MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
            final String ctxName = Server.escape(org.apache.logging.log4j.core.LoggerContext.getContext().getName());
            final String configName = Server.escape(marker);
            final String name = String.format(PATTERN, ctxName, configName);
            try {
                mbs.registerMBean(filter, new ObjectName(name));
            } catch (Exception e) {
                LOGGER.error("JMX configuration failed for ThresholdMarkerFilter [" + filter + "]: ", e);
            }
        }
        return filter;
    }

    public Result filter(final Marker marker, final Level level) {
        if (marker == null || !marker.isInstanceOf(this.marker)) {
            if (checkContext() && this.level.isLessSpecificThan(level)) {
                return onMatch;
            }
            return Result.NEUTRAL;
        } else if (marker.isInstanceOf(this.marker) && this.level.isLessSpecificThan(level)) {
            return onMatch;
        } else if (marker.isInstanceOf(this.marker) && this.level.isMoreSpecificThan(level)) {
            return onMismatch;
        }
        return Result.NEUTRAL;
    }

    public boolean checkContext() {
        String ctx = ThreadContext.get(LoggerAdvice.CELLAR_CONTEXT);
        return !Strings.isNullOrEmpty(ctx) && !ignoreContext && ctx.equals(context);
    }

    @Override
    public Result filter(final Logger logger, final Level level, final Marker marker, final String msg,
                         final Object... params) {
        return filter(marker, level);
    }

    @Override
    public Result filter(final Logger logger, final Level level, final Marker marker, final Object msg,
                         final Throwable t) {
        return filter(marker, level);
    }

    @Override
    public Result filter(final Logger logger, final Level level, final Marker marker, final Message msg,
                         final Throwable t) {
        return filter(marker, level);
    }

    @Override
    public Result filter(final LogEvent event) {
        return filter(event.getMarker(), event.getLevel());
    }

    @Override
    public Result filter(final Logger logger, final Level level, final Marker marker, final String msg,
                         final Object p0) {
        return filter(marker, level);
    }

    @Override
    public Result filter(final Logger logger, final Level level, final Marker marker, final String msg,
                         final Object p0, final Object p1) {
        return filter(marker, level);
    }

    @Override
    public Result filter(final Logger logger, final Level level, final Marker marker, final String msg,
                         final Object p0, final Object p1, final Object p2) {
        return filter(marker, level);
    }

    @Override
    public Result filter(final Logger logger, final Level level, final Marker marker, final String msg,
                         final Object p0, final Object p1, final Object p2, final Object p3) {
        return filter(marker, level);
    }

    @Override
    public Result filter(final Logger logger, final Level level, final Marker marker, final String msg,
                         final Object p0, final Object p1, final Object p2, final Object p3,
                         final Object p4) {
        return filter(marker, level);
    }

    @Override
    public Result filter(final Logger logger, final Level level, final Marker marker, final String msg,
                         final Object p0, final Object p1, final Object p2, final Object p3,
                         final Object p4, final Object p5) {
        return filter(marker, level);
    }

    @Override
    public Result filter(final Logger logger, final Level level, final Marker marker, final String msg,
                         final Object p0, final Object p1, final Object p2, final Object p3,
                         final Object p4, final Object p5, final Object p6) {
        return filter(marker, level);
    }

    @Override
    public Result filter(final Logger logger, final Level level, final Marker marker, final String msg,
                         final Object p0, final Object p1, final Object p2, final Object p3,
                         final Object p4, final Object p5, final Object p6,
                         final Object p7) {
        return filter(marker, level);
    }

    @Override
    public Result filter(final Logger logger, final Level level, final Marker marker, final String msg,
                         final Object p0, final Object p1, final Object p2, final Object p3,
                         final Object p4, final Object p5, final Object p6,
                         final Object p7, final Object p8) {
        return filter(marker, level);
    }

    @Override
    public Result filter(final Logger logger, final Level level, final Marker marker, final String msg,
                         final Object p0, final Object p1, final Object p2, final Object p3,
                         final Object p4, final Object p5, final Object p6,
                         final Object p7, final Object p8, final Object p9) {
        return filter(marker, level);
    }

    private static boolean isJmxDisabled() {
        return PropertiesUtil.getProperties().getBooleanProperty(PROPERTY_DISABLE_JMX);
    }

    @Override
    public String getLevel() {
        return level.name();
    }

    @Override
    public void setLevel(String level) {
        this.level = Level.getLevel(level);
    }

    @Override
    public boolean getIgnoreContext() {
        return ignoreContext;
    }

    @Override
    public void setIgnoreContext(boolean ignoreContext) {
        this.ignoreContext = ignoreContext;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("marker", marker)
                .add("context", context)
                .add("level", level)
                .toString();
    }
}
