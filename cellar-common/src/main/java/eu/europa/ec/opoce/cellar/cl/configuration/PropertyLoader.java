/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.configuration
 *        FILE : PropertyLoader.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 02-04-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.configuration;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.exception.CellarConfigurationException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

/**
 * <class_description> Class for loading and accessing application level properties.<br/>
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 02-04-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public class PropertyLoader {

    public PropertyManager propertyManager;

    /**
     * Builds a new ${link PropertyLoader} by loading the properties from property files input streams paths are listed in ${code propertiesStreams}.
     * 
     * @param propertiesFilePaths the list of paths to property files
     * @throws CellarConfigurationException raised if one of the file property cannot be resolved
     */
    public PropertyLoader(final List<InputStream> propertiesStreams) throws CellarConfigurationException {
        final Properties properties = new Properties();
        for (final InputStream propertiesStream : propertiesStreams) {
            try {
                properties.load(propertiesStream);
            } catch (IOException e) {
                throw ExceptionBuilder.get(CellarConfigurationException.class).withCode(CommonErrors.FAILED_PROCESSING_CONFIGURATION)
                        .withMessage("Cannot load file located at: {}").withMessageArgs(propertiesStream).withCause(e).build();
            }
        }
        this.propertyManager = new PropertyManager(properties);
    }

    public PropertyManager getPropertyManager() {
        return propertyManager;
    }

    public void setPropertyManager(PropertyManager propertyManager) {
        this.propertyManager = propertyManager;
    }

}
