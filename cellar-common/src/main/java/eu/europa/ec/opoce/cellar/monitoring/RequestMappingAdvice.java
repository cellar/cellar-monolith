/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.audit
 *             FILE : RequestMappingAspect.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 04 04, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-04-04 08:33:19 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.monitoring;

import com.google.common.base.MoreObjects;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.RemovalListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author ARHS Developments
 * @since 7.7
 */
@Aspect
@Order(Ordered.HIGHEST_PRECEDENCE + 1)
@Component
public class RequestMappingAdvice {

    private static final RequestMappingAdvice INSTANCE = new RequestMappingAdvice(null);
    private static final Logger LOG = LogManager.getLogger(RequestMappingAdvice.class);
    private final Cache<ClassRequestMappingKey, String> mappings = CacheBuilder.newBuilder()
            .maximumSize(1000)
            .expireAfterWrite(10, TimeUnit.MINUTES)
            .removalListener((RemovalListener<ClassRequestMappingKey, String>) notification ->
                    LOG.debug("Remove value {} based on key {} from the cache (eviction={}, cause={})",
                            notification.getValue(), notification.getKey(), notification.wasEvicted(),
                            notification.getCause()))
            .build();

    private final ApiAuditService apiAuditService;

    @Autowired
    public RequestMappingAdvice(ApiAuditService apiAuditService) {
        this.apiAuditService = apiAuditService;
    }

    @Before(value = "@annotation(requestMapping)")
    public void intercept(final JoinPoint pjp, final RequestMapping requestMapping) throws Throwable {
        try {
            Class<?> target = pjp.getTarget().getClass();
            // Avoid intensive reflection if the endpoint is called often by caching the path
            String path = mappings.get(new ClassRequestMappingKey(target, requestMapping), () -> resolvePath(target, requestMapping));
            LOG.debug("Path resolved -> {}", path);
            apiAuditService.audit(newApiCall(pjp, path, requestMapping));
        } catch (Exception e) { // Prevent the aspect to throw an unexpected exception
            LOG.debug(e);
        }
    }

    private static String resolvePath(final Class<?> target, final RequestMapping requestMapping) {
        if (target.isAnnotationPresent(RequestMapping.class)) { // RequestMapping can be inherited from the class
            RequestMapping mapping = target.getAnnotation(RequestMapping.class);
            LOG.debug("Found annotation @RequestMapping on {}", target);
            return revolvePath(mapping, requestMapping);
        } else if (target.getSuperclass() != null) { // RequestMapping can be inherited from any class of the hierarchy
            return resolvePath(target.getSuperclass(), requestMapping);
        }
        return revolvePath(requestMapping);
    }

    private static String revolvePath(RequestMapping... mappings) {
        StringBuilder path = new StringBuilder();
        for (RequestMapping m : mappings) {
            String[] p = m.value();
            if (p.length == 1) {
                path.append(p[0]);
            } else if (p.length > 0) {
                path.append(Arrays.toString(p));
            }
        }
        return path.toString();
    }

    private static ApiCall newApiCall(final JoinPoint pjp, final String path, final RequestMapping mapping) {
        ApiCall apiCall = new ApiCall();
        apiCall.setClazz(pjp.getTarget().getClass().getSimpleName());
        apiCall.setMethod(pjp.getSignature().getName());
        apiCall.setConsumes(Arrays.asList(mapping.consumes()));
        apiCall.setProduces(Arrays.asList(mapping.produces()));
        apiCall.setPath(path);
        List<RequestMethod> methods = Arrays.asList(mapping.method());
        apiCall.setMethods(methods.isEmpty() ? Collections.singletonList(RequestMethod.GET) : methods);
        apiCall.setArgs(Arrays.stream(pjp.getArgs())
                .filter(a -> !(a instanceof HttpServletRequest || a instanceof HttpServletResponse))
                .collect(Collectors.toList()));
        return apiCall;
    }

    static class ClassRequestMappingKey {
        private Class<?> clazz;
        private RequestMapping mapping;

        ClassRequestMappingKey(Class<?> clazz, RequestMapping mapping) {
            this.clazz = clazz;
            this.mapping = mapping;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            ClassRequestMappingKey that = (ClassRequestMappingKey) o;
            return (clazz != null ? clazz.equals(that.clazz) : that.clazz == null)
                    && (mapping != null ? mapping.equals(that.mapping) : that.mapping == null);
        }

        @Override
        public int hashCode() {
            int result = clazz != null ? clazz.hashCode() : 0;
            result = 31 * result + (mapping != null ? mapping.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return MoreObjects.toStringHelper(this)
                    .add("clazz", clazz)
                    .add("mapping", mapping)
                    .toString();
        }
    }

    /**
     * Needed by AspectJ
     */
    @SuppressWarnings("unused")
    public static RequestMappingAdvice aspectOf() {
        return INSTANCE;
    }
}