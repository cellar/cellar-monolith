package eu.europa.ec.opoce.cellar.domain.content.mets;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.exception.CellarValidationException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

public enum MetsProfile {
    METS_1("SIP_PRENOTICE", true, MetsVersion.V1), //
    METS_2("SIP_NOTICE", false, MetsVersion.V1), //
    METS_3("http://publications.europa.eu/resource/mets/op-sip-profile_002", false, MetsVersion.V2);

    public enum MetsVersion {
        V1, V2;
    }

    private String xmlValue;
    private boolean weakValidation;
    private MetsVersion version;

    MetsProfile(final String xmlValue, final boolean weakValidation, final MetsVersion version) {
        this.xmlValue = xmlValue;
        this.weakValidation = weakValidation;
        this.version = version;
    }

    public boolean hasWeakValidation() {
        return this.weakValidation;
    }

    public MetsVersion getVersion() {
        return this.version;
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "MetsProfile { " + //
                "value=" + this.xmlValue + //
                ", validation=" + (this.hasWeakValidation() ? "weak" : "strong") + //
                " }";
    }

    public static MetsProfile getByXmlValue(final String xmlValue) {
        for (MetsProfile profile : MetsProfile.values()) {
            if (profile.xmlValue.equals(xmlValue)) {
                return profile;
            }
        }

        throw ExceptionBuilder.get(CellarValidationException.class).withCode(CommonErrors.UNKNOWN_METS_PROFILE)
                .withMessage("Unknown METS profile '{}'.").withMessageArgs(xmlValue).build();
    }

}
