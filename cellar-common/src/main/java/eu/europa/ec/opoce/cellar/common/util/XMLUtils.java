package eu.europa.ec.opoce.cellar.common.util;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPathFactoryConfigurationException;
import java.beans.XMLEncoder;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class XMLUtils {

    public static void saveObject(Object o, File file) throws IOException {

        try(FileOutputStream fos = new FileOutputStream(file);) {
            XMLEncoder encoder = new XMLEncoder(fos);
            encoder.writeObject(o);
            encoder.flush();
            encoder.close();
        }
    }

    /**
     * Creates a pretty printed copy of the xml provided file.  
     * @param fileToFormat the file to pretty print
     * @return a copy of the File containing pretty printed xml
     */
    public static final File format(File fileToFormat) {

        try {
            Document document = parseXmlFile(fileToFormat);

            OutputFormat format = OutputFormat.createPrettyPrint();
            File prettyPrintFile = getPrettyPrintFile(fileToFormat);
            try(FileWriter fw=new FileWriter(prettyPrintFile);)
            {
                XMLWriter out = new XMLWriter(fw, format);
                out.write(document);
                out.close();
            }
            return prettyPrintFile;
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (DocumentException e) {
            throw new RuntimeException(e);
        }
    }

    /**
    * Sorts the children of the given node upto the specified depth if available.
    * 
    * @param node - node whose children will be sorted
    * @param descending - true for sorting in descending order
    * @param depth - depth upto which to sort in DOM
    * @param comparator - comparator used to sort, if null a default NodeName comparator is used.
    */
    public static void sortChildNodes(final Node node, final boolean descending, final int depth, final Comparator<Node> comparator) {
        final int myDepth = (depth < 0 ? Integer.MAX_VALUE : depth);

        final List<Node> nodes = new ArrayList<Node>();
        final NodeList childNodeList = node.getChildNodes();
        if (myDepth > 0 && childNodeList.getLength() > 0) {
            for (int i = 0; i < childNodeList.getLength(); i++) {
                final Node tNode = childNodeList.item(i);
                sortChildNodes(tNode, descending, myDepth - 1, comparator);
                // Remove empty text nodes
                if ((!(tNode instanceof Text)) || (tNode instanceof Text && ((Text) tNode).getTextContent().trim().length() > 1)) {
                    nodes.add(tNode);
                }
            }

            final Comparator<Node> comp = (comparator != null) ? comparator : new Comparator<Node>() {

                @Override
                public int compare(final Node node1, final Node node2) {
                    return node1.getNodeName().compareTo(node2.getNodeName());
                }
            };
            if (descending) {
                //if descending is true, get the reverse ordered comparator
                Collections.sort(nodes, Collections.reverseOrder(comp));
            } else {
                Collections.sort(nodes, comp);
            }

            for (final Node currNode : nodes) {
                node.removeChild(currNode);
                node.appendChild(currNode);
            }
        }
    }

    /**
     * Instantiate the default Java XPath factory. We avoid the JAXP search mechanism because of its unreliability and slowness.<br>
     * See also http://stackoverflow.com/questions/7914915/syntax-error-in-javax-xml-xpath-xpathfactory-provider-configuration-file-of-saxo
     * @throws XPathFactoryConfigurationException 
     */
    public static XPathFactory newXPathFactory() throws XPathFactoryConfigurationException {
        return XPathFactory.newInstance(XPathFactory.DEFAULT_OBJECT_MODEL_URI, "com.sun.org.apache.xpath.internal.jaxp.XPathFactoryImpl",
                ClassLoader.getSystemClassLoader());
    }

    private static final File getPrettyPrintFile(File fileToFormat) {
        return new File(fileToFormat.getParentFile(), "formatted-" + fileToFormat.getName());
    }

    private static final Document parseXmlFile(File in) throws DocumentException {
        SAXReader reader = new SAXReader();
        Document document = reader.read(in);
        return document;
    }

}
