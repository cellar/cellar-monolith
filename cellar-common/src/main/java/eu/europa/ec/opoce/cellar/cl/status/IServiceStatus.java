/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.status
 *        FILE : IServiceStatus.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 14-05-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.status;

import eu.europa.ec.opoce.cellar.exception.CellarConfigurationException;

/**
 * <class_description> This interface defines the utilities for checking the status of a service.</br>
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 14-05-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IServiceStatus {

    /**
     * Service status checker. It throws a {@link CellarConfigurationException} in case the service is not available.
     */
    void check() throws CellarConfigurationException;
}
