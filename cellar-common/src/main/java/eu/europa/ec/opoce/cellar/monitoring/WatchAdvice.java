/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.monitoring
 *             FILE : WatchAdvice.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 07 12, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-07-12 09:58:58 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.monitoring;

import com.google.common.annotations.VisibleForTesting;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.monitoring.Watch.WatchArgument;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.expression.CachedExpressionEvaluator;
import org.springframework.context.expression.MethodBasedEvaluationContext;
import org.springframework.core.DefaultParameterNameDiscoverer;
import org.springframework.core.Ordered;
import org.springframework.core.ParameterNameDiscoverer;
import org.springframework.core.annotation.Order;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.Expression;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import static eu.europa.ec.opoce.cellar.AppConstants.NEWLINE;

/**
 * @author ARHS Developments
 * @see Watch
 * @since 7.5
 */
@Aspect
@Order(Ordered.HIGHEST_PRECEDENCE + 1)
@Component
public class WatchAdvice {

    private static final Logger LOG = LoggerFactory.getLogger(WatchAdvice.class);
    private static final WatchAdvice INSTANCE = new WatchAdvice(null);
    static final InheritableThreadLocal<WatchContext> CONTEXTS = new InheritableThreadLocal<>();

    private static final ArgumentsExpressionEvaluator EXPRESSION_EVALUATOR = new ArgumentsExpressionEvaluator();
    private final ICellarConfiguration configuration;

    @Autowired
    public WatchAdvice(@Qualifier("cellarStaticConfiguration") ICellarConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * Measure the time of the methods execution and group the other
     * {@link Watch}ed methods called subsequently in order to understand
     * the time decomposition.
     *
     * @param pjp   the {@link ProceedingJoinPoint}
     * @param watch the {@link Watch} annotation on the method
     * @return the object of the underlying method
     * @throws Throwable if something goes wrong in the underlying method
     */
    @Around("@annotation(watch)")
    public Object intercept(ProceedingJoinPoint pjp, Watch watch) throws Throwable {
        if (!configuration.isCellarWatchAdviceEnabled()) {
            return pjp.proceed();
        }
        setContext();
        final Caller caller = new Caller(pjp, watch);
        CONTEXTS.get().addCaller(caller);

        final long start = System.nanoTime();
        try {
            return pjp.proceed();
        } finally {
            caller.time = System.nanoTime() - start;
            final WatchContext ctx = CONTEXTS.get();
            try {
                if (ctx != null && ctx.isFirstCaller(caller)) {
                    report(ctx);
                    clearContext();
                }
            } catch (Exception e) {
                // Avoid internal exception propagation outside the advice
                LOG.warn("Error in WatchAdvice ", e);
            }
        }
    }

    private static void setContext() {
        if (CONTEXTS.get() == null) {
            CONTEXTS.set(new WatchContext());
        }
    }

    protected void clearContext() {
        final WatchContext ctx = CONTEXTS.get();
        if (ctx != null) {
            CONTEXTS.get().callers.clear();
            CONTEXTS.remove();
        }
    }

    /**
     * Print the {@link WatchContext}.
     *
     * @param ctx the current {@link WatchContext}
     */
    protected void report(WatchContext ctx) {
        final StringBuilder sb = new StringBuilder(NEWLINE + "[").append(ctx.callers.getFirst().watch.value()).append(']');
        for (Caller caller : ctx.callers) {
            sb.append('[').append(caller.watch.value()).append(resolveArgs(caller.pjp, caller.watch)).append("] ");
            sb.append(caller.getSignature().getName()).append(" -> ")
                    .append(caller.watch.unit().convert(caller.time, TimeUnit.NANOSECONDS))
                    .append(" (").append(caller.watch.unit()).append(')');
            sb.append(NEWLINE + "\t");
        }
        LOG.info(sb.delete(sb.length() - 2, sb.length()).toString());
    }

    static String resolveArgs(ProceedingJoinPoint pjp, Watch watch) {
        if (watch.arguments().length == 0) {
            return "";
        }
        StringBuilder sb = new StringBuilder("(");
        for (WatchArgument argument : watch.arguments()) {
            sb.append(argument.name()).append(" = ")
                    .append(getValue(pjp, argument))
                    .append(',');
        }
        return sb.deleteCharAt(sb.length() - 1).append(")").toString();
    }

    private static Object getValue(ProceedingJoinPoint pjp, WatchArgument argument) {
        try {
            return EXPRESSION_EVALUATOR.getParameterValue(pjp, argument.expression());
        } catch (Exception e) {
            LOG.debug("Value not found for {} in {}", argument.expression(), pjp.getSignature());
            return "(not found)";
        }
    }

    static class WatchContext {
        private final ConcurrentLinkedDeque<Caller> callers = new ConcurrentLinkedDeque<>();

        boolean isFirstCaller(Caller caller) {
            return callers.getFirst().getSignature().getName().equals(caller.getSignature().getName());
        }

        void addCaller(Caller caller) {
            callers.add(caller);
        }

        /**
         * For testing only, do not use directly
         *
         * @return the callers deque.
         * @see #isFirstCaller(Caller)
         * @see #addCaller(Caller)
         */
        @VisibleForTesting
        ConcurrentLinkedDeque<Caller> getCallers() {
            return callers;
        }
    }

    static class Caller {
        private long time;
        final Watch watch;
        final ProceedingJoinPoint pjp;

        Caller(ProceedingJoinPoint pjp, Watch watch) {
            this.pjp = pjp;
            this.watch = watch;
        }

        Signature getSignature() {
            return pjp.getSignature();
        }
    }

    private static class ArgumentsExpressionEvaluator extends CachedExpressionEvaluator {

        private final ParameterNameDiscoverer parameterNameDiscoverer = new DefaultParameterNameDiscoverer();
        private final ConcurrentMap<String, Expression> cache = new ConcurrentHashMap<>();

        /**
         * Retrieve the value of the method argument based on the SpEL expression.
         *
         * @param pjp              the {@link ProceedingJoinPoint}
         * @param expressionString the SpEL expression
         * @return the value of the argument
         */
        private Object getParameterValue(ProceedingJoinPoint pjp, String expressionString) {
            final MethodSignature ms = ((MethodSignature) pjp.getSignature());
            final int index = getTargetArgumentIndex(ms, expressionString);

            if (expressionString.contains(".")) {
                final Expression expression = cache.computeIfAbsent(expressionString, exp -> {
                    final String e = exp.substring(exp.indexOf('.') + 1, exp.length());
                    return getParser().parseExpression(e);
                });
                return Stream.of(newEvaluationContext(pjp, ms, index))
                        .filter(Optional::isPresent)
                        .map(o -> expression.getValue(o.get()))
                        .filter(Objects::nonNull)
                        .findFirst()
                        .orElse("(not found)");
            }
            return index >= 0 ? pjp.getArgs()[index] : "(not found)";
        }

        /**
         * Create an {@link EvaluationContext} based on the {@link ProceedingJoinPoint}
         * and the optional SpEL expression.
         *
         * @param pjp   the {@link ProceedingJoinPoint}
         * @param ms    the method signature
         * @param param the index of the argument to explore
         * @return the optional {@link EvaluationContext}
         */
        private Optional<EvaluationContext> newEvaluationContext(ProceedingJoinPoint pjp, MethodSignature ms, int param) {
            if (param >= 0) {
                return Optional.of(new MethodBasedEvaluationContext(pjp.getArgs()[param], ms.getMethod(), pjp.getArgs(),
                        parameterNameDiscoverer));
            }
            return Optional.empty();
        }

        private int getTargetArgumentIndex(MethodSignature methodSignature, String expression) {
            final String[] exp = expression.split("\\.");
            String[] params = methodSignature.getParameterNames();
            for (int i = 0; i < params.length; i++) {
                if (params[i].equals(exp[0])) {
                    return i;
                }
            }
            return -1;
        }
    }

    // Required aspectj
    @SuppressWarnings("unused")
    public static WatchAdvice aspectOf() {
        return INSTANCE;
    }
}
