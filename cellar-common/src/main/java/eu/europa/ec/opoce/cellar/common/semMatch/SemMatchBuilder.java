/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.semMatch
 *             FILE : SemMatchBuilder.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jul 9, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.semMatch;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jul 9, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class SemMatchBuilder {

    private static final String WHERE_CLAUSE = "WHERE %s";

    private static final String SEM_ALIASES = "SEM_ALIASES(%s)";

    private static final String SEM_ALIAS = "SEM_ALIAS('%s','%s')";

    private static final String SEM_MODEL = "'%s'";

    private static final String SEM_MODELS = "SEM_MODELS(%s)";

    private static final String SELECT_QUERY = "SELECT %s " //
            + "FROM TABLE(SEM_MATCH(" //
            + "'{ %s }', " //
            + "%s, " // sem_models
            + "NULL, " //
            + "%s, " // sem_aliases
            + "NULL, NULL)) ";

    private final Map<String, String> aliases;

    private String selectVariables;

    private final List<String> semMatchQueryParts;
    private final Map<String, String> semMatchQueryParams;

    private final Set<String> models;

    private final List<String> whereExpressionParts;
    private final Map<String, String> whereExpressionParams;

    private SemMatchBuilder() {
        this.aliases = new HashMap<String, String>();
        this.semMatchQueryParts = new LinkedList<String>();
        this.semMatchQueryParams = new HashMap<String, String>();
        this.models = new HashSet<String>();
        this.whereExpressionParts = new LinkedList<String>();
        this.whereExpressionParams = new HashMap<String, String>();
    }

    public static SemMatchBuilder get() {
        return new SemMatchBuilder();
    }

    public String build() {
        final String query = String.format(SELECT_QUERY, this.selectVariables, this.buildSemMatchQuery(), this.buildSemModels(),
                this.buildSemAliases());

        if (!this.whereExpressionParts.isEmpty()) {
            return query + String.format(WHERE_CLAUSE, this.buildWhereExpression());
        }

        return query;
    }

    private String buildSemAliases() {
        final List<String> semAliases = new LinkedList<String>();

        for (Map.Entry<String, String> alias : this.aliases.entrySet()) {
            semAliases.add(String.format(SEM_ALIAS, alias.getKey(), alias.getValue()));
        }

        return String.format(SEM_ALIASES, StringUtils.join(semAliases, ","));
    }

    private String buildSemModels() {
        final List<String> semModels = new LinkedList<String>();

        for (final String model : this.models) {
            semModels.add(String.format(SEM_MODEL, model));
        }

        return String.format(SEM_MODELS, StringUtils.join(semModels, ","));
    }

    private String buildSemMatchQuery() {
        return StringUtils.replaceEach(StringUtils.join(this.semMatchQueryParts, null),
                this.semMatchQueryParams.keySet().toArray(new String[0]), this.semMatchQueryParams.values().toArray(new String[0]));
    }

    private String buildWhereExpression() {
        return StringUtils.replaceEach(StringUtils.join(this.whereExpressionParts, null),
                this.whereExpressionParams.keySet().toArray(new String[0]), this.whereExpressionParams.values().toArray(new String[0]));
    }

    public SemMatchBuilder withAlias(final String id, final String value) {
        this.aliases.put(id, value);
        return this;
    }

    public SemMatchBuilder withSelectVariables(final String selectVariables) {
        this.selectVariables = selectVariables;
        return this;
    }

    public SemMatchBuilder withSemMatchQueryPart(final String semMatchQueryPart) {
        this.semMatchQueryParts.add(semMatchQueryPart);
        return this;
    }

    public SemMatchBuilder withSemMatchQueryParam(final String id, final Object value) {
        this.semMatchQueryParams.put(id, value.toString());
        return this;
    }

    public SemMatchBuilder withModel(final String model) {
        this.models.add(model);
        return this;
    }

    public SemMatchBuilder withWhereExpressionPart(final String whereExpressionPart) {
        this.whereExpressionParts.add(whereExpressionPart);
        return this;
    }

    public SemMatchBuilder withWhereExpressionParam(final String id, final Object value) {
        this.whereExpressionParams.put(id, value.toString());
        return this;
    }
}
