package eu.europa.ec.opoce.cellar.logging;

import eu.europa.ec.opoce.cellar.bootstrap.CellarHooks;
import eu.europa.ec.opoce.cellar.common.concurrent.ConcurrencyStrategy;
import eu.europa.ec.opoce.cellar.common.concurrent.PriorityCallable;
import org.apache.logging.log4j.ThreadContext;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ARHS Developments
 */
public class ThreadContextConcurrencyStrategy implements ConcurrencyStrategy {

    @Override
    public <T> PriorityCallable<T> wrapCallable(PriorityCallable<T> callable) {
        try {
            Map<String, String> values = new HashMap<>(ThreadContext.getContext());
            values.put(CellarHooks.CELLAR_DEBUG_KEY, CellarHooks.getDebugContext());
            return new PriorityCallable<T>() {
                @Override
                public int compareTo(PriorityCallable<T> c) {
                    // When callable.compare(c) is used, a ClassCastException is thrown
                    // The comparison has been reversed in order to bypass the exception
                    return -c.compareTo(callable);
                }

                @Override
                public T call() throws Exception {
                    ThreadContext.putAll(values);
                    return callable.call();
                }
            };
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
