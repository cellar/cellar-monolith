/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.domain.content.mets
 *             FILE : CellarIdentifiedObject.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 25-Jan-2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.domain.content.mets;

import eu.europa.ec.opoce.cellar.cmr.ContentType;

import java.io.Serializable;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * data-structure to group all productionIdentifiers along with a cellarIdentifier and a digital object type.
 *
 * @author ARHS Developments
 * @version $$Revision$$$
 */
public class CellarIdentifiedObject implements Serializable {

    private static final long serialVersionUID = 2624153902748067174L;

    private ContentIdentifier cellarId;

    private List<ContentIdentifier> contentIdentifiers;

    private DigitalObjectType type;

    private Boolean readOnly;

    private Map<ContentType, String> versions = new EnumMap<>(ContentType.class);

    public CellarIdentifiedObject() {
        this(Collections.emptyList());
    }

    public CellarIdentifiedObject(Map<ContentType, String> versions) {
        this(Collections.emptyList(), versions);
    }

    public CellarIdentifiedObject(List<ContentIdentifier> contentIdentifiers) {
        this(contentIdentifiers, Collections.emptyMap());
    }

    public CellarIdentifiedObject(List<ContentIdentifier> contentIdentifiers, Map<ContentType, String> versions) {
        this.contentIdentifiers = contentIdentifiers;
        setVersions(versions);
    }

    public ContentIdentifier getCellarId() {
        return cellarId;
    }

    public void setCellarId(ContentIdentifier cellarId) {
        this.cellarId = cellarId;
    }

    public List<ContentIdentifier> getContentids() {
        return contentIdentifiers;
    }

    public void setContentids(List<ContentIdentifier> contentidentifiers) {
        this.contentIdentifiers = contentidentifiers;
    }

    public Set<String> getAllUris(final boolean withCellarUri) {
        final Set<String> uris = new HashSet<String>();
        if (withCellarUri) {
            uris.add(getCellarId().getUri());
        }
        for (final ContentIdentifier contentIdentifier : getContentids()) {
            uris.add(contentIdentifier.getUri());
        }
        return uris;
    }

    public Set<String> getAllUris() {
        return this.getAllUris(true);
    }

    public Set<String> getAllIds(final boolean withCellarId) {
        final Set<String> ids = new HashSet<String>();
        if (withCellarId) {
            ids.add(getCellarId().getIdentifier());
        }
        for (final ContentIdentifier contentIdentifier : getContentids()) {
            ids.add(contentIdentifier.getIdentifier());
        }
        return ids;
    }

    public DigitalObjectType getType() {
        return type;
    }

    public void setType(DigitalObjectType type) {
        this.type = type;
    }

    public Boolean getReadOnly() {
        return readOnly;
    }

    public void setReadOnly(Boolean readOnly) {
        this.readOnly = readOnly;
    }

    public Map<ContentType, String> getVersions() {
        return versions;
    }

    public void setVersions(Map<ContentType, String> versions) {
        this.versions = versions != null && !versions.isEmpty() ? new EnumMap<>(versions) : new EnumMap<>(ContentType.class);
    }

    @Override
    public boolean equals(final Object object) {
        if (!(object instanceof CellarIdentifiedObject)) {
            return false;
        }
        final CellarIdentifiedObject myObject = (CellarIdentifiedObject) object;

        final ContentIdentifier objectCellarId = myObject.getCellarId();
        return objectCellarId != null
                && cellarId != null
                && Objects.equals(cellarId.getIdentifier(), objectCellarId.getIdentifier());
    }

    @Override
    public int hashCode() {
        return cellarId != null ? Objects.hash(cellarId.getIdentifier()) : 0;
    }
}
