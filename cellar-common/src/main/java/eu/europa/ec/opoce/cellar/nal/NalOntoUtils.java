/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.nal
 *             FILE : NalUtils.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 01 08, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-01-08 13:22:53 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.nal;

import org.apache.commons.lang.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author ARHS Developments
 */
public final class NalOntoUtils {

    public static final String EUROVOC_ONTOLOGY_URI = "http://eurovoc.europa.eu/schema";
    public static final String AUTHORITY_ONTOLOGY_URI = "http://publications.europa.eu/resource/authority";
    private static final String ONTO_BASE_URI = "http://publications.europa.eu";

    public static final String EUROVOC_CONCEPTSCHEME_URI = "http://eurovoc.europa.eu/100141";
    public static final String NAL_LANGUAGE_URI = "http://publications.europa.eu/resource/authority/language";
    public static final String NAL_FILE_TYPE_URI = "http://publications.europa.eu/resource/authority/file-type";

    public static final String NAL_PREFIX = "nal/";
    public static final String NAL_PREFIX_LEGACY = "nal:";
    public static final String ONTO_PREFIX = "onto/";
    public static final String ONTO_PREFIX_LEGACY = "onto:";
    public static final String SPARQL_LOAD_PREFIX = "sparql-load:";

    private static final String REPLACE_URI_PATTERN_STR = "[:|\\/|\\.]";
    private static final String AUTHORITY_ONTOLOGY_URI_PATTERN_STR = "https?:\\/\\/.*\\/authority\\/.*\\/";
    private static final Pattern AUTHORITY_ONTOLOGY_URI_PATTERN = Pattern.compile(AUTHORITY_ONTOLOGY_URI_PATTERN_STR);


    private NalOntoUtils() {
    }

    public static String getBaseUri(String nalUri) {
        if (nalUri.contains("eurovoc")) {
            return EUROVOC_CONCEPTSCHEME_URI;
        } else {
            Matcher matcher = AUTHORITY_ONTOLOGY_URI_PATTERN.matcher(nalUri);
            if (matcher.find()) {
                String baseUri = matcher.group(0);
                if (StringUtils.isNotBlank(baseUri)) {
                    return baseUri.endsWith("/") ? baseUri.substring(0, baseUri.lastIndexOf("/")) : baseUri;
                }
            }
        }
        return nalUri;
    }

    // used to create a unique name from the NAL URI (to name folders, ...)
    // http://publications.europa.eu/resource/authority/multilingual -> http_publications_europa_eu_resource_authority_multilingual
    public static String flattenNalUri(String nalUri) {
        return nalUri.replaceAll(REPLACE_URI_PATTERN_STR, "_");
    }
}
