package eu.europa.ec.opoce.cellar.domain.content.mets;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.common.CellarCatalogResolver;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import org.apache.xerces.util.XMLCatalogResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.ext.EntityResolver2;
import org.xml.sax.helpers.XMLReaderFactory;

import javax.xml.XMLConstants;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;

/**
 *  this is a class that helps resolving entities in files and returns inputStreams back
 */
public class ResolvedInputStreamHelper implements EntityResolver2 {

    private final XMLCatalogResolver catalogResolver = CellarCatalogResolver.getInstance();
    private File metsDirectory;

    public ResolvedInputStreamHelper(File metsDirectory) {
        this.metsDirectory = metsDirectory;
    }

    @Override
    public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
        InputSource resolvedEntity = catalogResolver.resolveEntity(publicId, systemId);
        if (null != resolvedEntity)
            return resolvedEntity;

        // need to do some fixes here, this is the old method and the systemId receives an already expanded path/url
        if (null == systemId)
            return new InputSource();
        int index = systemId.replace('\\', '/').lastIndexOf('/');
        systemId = index == -1 ? systemId : systemId.substring(index + 1);
        return resolveEntityWithMetsPackage(systemId);
    }

    private InputSource resolveEntityWithMetsPackage(String systemId) {
        try(InputStream fileStream=new FileInputStream(new File(metsDirectory,systemId))) {
            return new InputSource(fileStream);
        } catch (FileNotFoundException e) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.FILE_INPUT_COULD_NOT_BE_OPENED)
                    .withMessage("Did not find system entity resolver in mets {}").withMessageArgs(systemId).withCause(e).build();
        }
        catch(IOException e){
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.FILE_INPUT_COULD_NOT_BE_OPENED)
                    .withMessage("An error occured while trying to open file of mets {}").withMessageArgs(systemId).withCause(e).build();
        }
    }

    @Override
    public InputSource getExternalSubset(String name, String baseURI) throws SAXException, IOException {
        return catalogResolver.getExternalSubset(name, baseURI);
    }

    @Override
    public InputSource resolveEntity(String name, String publicId, String baseURI, String systemId) throws SAXException, IOException {
        InputSource resolvedEntity = catalogResolver.resolveEntity(name, publicId, baseURI, systemId);
        if (null != resolvedEntity)
            return resolvedEntity;

        return null == systemId ? null : resolveEntityWithMetsPackage(systemId);
    }

    public ByteArrayInputStream getResolvedStream(InputStream inputStream, String checkedData) {
        return new ByteArrayInputStream(getResolvedBytes(inputStream, checkedData));
    }

    private byte[] getResolvedBytes(InputStream inputStream, String checkedData) {
        try {
            XMLReader xmlReader = XMLReaderFactory.createXMLReader();
            // no need to set this, if no support -> exception, if support -> set to true by default
            // xmlReader.setFeature("http://xml.org/sax/features/use-entity-resolver2", true);
            xmlReader.setEntityResolver(this);

            SAXSource inputSource = new SAXSource(xmlReader, new InputSource(inputStream));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            TransformerFactory trfactory = TransformerFactory.newInstance();
            trfactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            Transformer transformer = trfactory.newTransformer();

            transformer.transform(inputSource, new StreamResult(baos));
            return baos.toByteArray();
        } catch (Exception exception) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.FAILED_PARSING_CONTENT_STREAM)
                    .withMessage("Failed to resolve stream: {}").withMessageArgs(checkedData).withCause(exception).build();
        }
    }
}
