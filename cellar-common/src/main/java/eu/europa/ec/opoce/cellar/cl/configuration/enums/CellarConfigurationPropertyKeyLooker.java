/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.configuration.enums
 *        FILE : CellarConfigurationPropertyKeyLooker.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 02-04-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.configuration.enums;

import java.util.HashMap;
import java.util.Map;

/**
 * <class_description> This is a class only meant to get a {@link CellarConfigurationPropertyKey} by its key.<br/>
 * <br/>
 * This class is external to the enum {@link CellarConfigurationPropertyKey} in order to avoid classloader issues
 * (see also http://stackoverflow.com/questions/1080904/how-can-i-lookup-a-java-enum-from-its-string-value)
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 02-04-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 * @see http://stackoverflow.com/questions/1080904/how-can-i-lookup-a-java-enum-from-its-string-value
 */
final class CellarConfigurationPropertyKeyLooker {

    static final Map<String, CellarConfigurationPropertyKey> lookup = new HashMap<String, CellarConfigurationPropertyKey>();

    static {
        for (CellarConfigurationPropertyKey propertyKey : CellarConfigurationPropertyKey.values()) {
            lookup.put(propertyKey.toString(), propertyKey);
        }
    }
}
