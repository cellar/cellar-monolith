package eu.europa.ec.opoce.cellar.domain.content.mets;

public interface OperationTypeVisitor<IN, OUT> {

    OUT visitRead(IN in);

    OUT visitCreate(IN in);

    OUT visitCreateOrIgnore(IN in);

    OUT visitUpdate(IN in);

    OUT visitDelete(IN in);

}
