/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.service.client
 *        FILE : QueueWatcher.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 13-03-2012
 * 
 * MODIFIED BY : EUROPEAN DYNAMICS S.A.
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2021 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ingestion.service;

import eu.europa.ec.opoce.cellar.ingestion.SIPWork;

import java.util.concurrent.ConcurrentMap;

/**
 * <class_description> Interface for the services meant to periodically poll the PACKAGE_QUEUE
 * database view for retrieving the references of the highest priority packages, in order
 * to subsequently load them from the ingestion folders and submit them to the ingestion
 * executor.
 * <br/><br/>
 * <notes> 
 * <br/><br/>
 * ON : 02-04-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public interface QueueWatcher {

    /**
     * Gets the SIP packages being ingested.
     * @return the treatments being ingested.
     */
    ConcurrentMap<SIPWork, Boolean> getIngestionTreatments();

    /**
     * Checks if there are SIP packages currently being ingested.
     * @return true if there are no SIPs currently being ingested,
     * false otherwise.
     */
    boolean isIngestionTreatmentSetEmpty();

    /**
     * Reconfigure the thread pool executor.
     * The properties have been changed so the watcher needs to reset the thread pool executor properties
     */
    void reconfigureThreadPoolExecutor();

    /**
     * Polls the head of the DB queue in order to retrieve a SIP package reference
     * to ingest, fetches the SIP file from the appropriate reception folder
     * and submits it to the ingestion executor.
     */
    void watchQueue();

}
