/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.retry.impl
 *             FILE : ExponentialRetryStrategy.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 15-07-2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.retry.impl;

/**
 * <class_description> An implementation of {@link eu.europa.ec.opoce.cellar.common.retry.impl.BaseRetryStrategy} which drives the fire-and-retry mechanism
 * on the basis of the formula <code>{@link base}^X</code>, where:
 * <ul>
 * <li>{@link base} is a value expressed in milliseconds representing the base of the formula above (default is <code>3000</code>)</li>
 * <li><code>X</code> is the number of the current retry being executed</li>
 * </ul>
 * For instance, if {@link base} is <code>1700</code>, the system will wait for <code>1700, 2889, 4912, 8352 ..</code> milliseconds between retries.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 15-07-2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class ExponentialRetryStrategy<P> extends BaseRetryStrategy<P> {

    /**
     * @see eu.europa.ec.opoce.cellar.common.retry.impl.BaseRetryStrategy#calculateDelay(int)
     */
    @Override
    protected long calculateDelay(final int currentRetry) {
        return Double.valueOf(Math.pow(this.base / 1000D, currentRetry + 1) * 1000).longValue();
    }

}
