package eu.europa.ec.opoce.cellar.domain.content.mets;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

import java.io.Serializable;

/**
 * data-structure to group information about metadata
 */
public class Metadata implements Serializable {

    private static final long serialVersionUID = 8905674516460368599L;

    private final String id;
    private final MetadataType type;
    private final DigitalObject digitalObject;

    public Metadata(final DigitalObject digitalObject, final MetadataType type, final String id) {
        if (digitalObject == null) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.INPUT_MUST_BE_DEFINED)
                    .withMessage("Digital Object cannot be null.").build();
        }
        if (type == null) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.INPUT_MUST_BE_DEFINED)
                    .withMessage("Metadata Type cannot be null.").build();
        }

        this.id = id;
        this.type = type;
        this.digitalObject = digitalObject;

        // execute a check of existence of the file, respectivly the proper type if no file is present
        if (isMetaDataFileInvalid()) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.METADATAFILE_NOT_FOUND)
                    .withMessage("No metadata file found for id " + id).build();
        }
    }

    private boolean isMetaDataFileInvalid() {
        return ((!this.digitalObject.getStructMap().getMetsDocument().hasMetadataStream(id)) && //
                (this.type != MetadataType.ABSENT));
    }

    public String getId() {
        return id;
    }

    public MetadataType getType() {
        return type;
    }

    public DigitalObject getDigitalObject() {
        return digitalObject;
    }

    public MetsDocument getMetsDocument() {
        return this.getDigitalObject().getStructMap().getMetsDocument();
    }

    public MetadataStream getMetadataStream() {
        return getMetsDocument().getMetadataStream(id);
    }

    public String getFileRef() {
        if (type.equals(MetadataType.TECHNICAL)) {
            try {
                return getMetadataStream().getFileRef();
            } catch (final Exception e) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.PROBLEM_TO_GET_FILEREF_FROM_METADATA).withCause(e)
                        .build();
            }
        } else if (type.equals(MetadataType.DMD)) {
            try {
                return getMetadataStream().getFileRef();
            } catch (final Exception e) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.PROBLEM_TO_GET_FILEREF_FROM_METADATA).withCause(e)
                        .build();
            }

        } else if (type.equals(MetadataType.ABSENT)) {
            return null;
        }
        return null;
    }
}
