package eu.europa.ec.opoce.cellar.cl.configuration.enums;

/**
 * Contains the acceptable values for DataSources.
 * @author EUROPEAN DYNAMICS S.A.
 */
public enum DataSource {

    CELLAR_DS("CellarDS"),
    CMR_DS("CmrDS"),
    STATS_DS("StatsDS"),
    IDOL_DS("IdolDS"),
    VIRTUOSO_DS("VirtuosoDS");

    /**
     * The dataSource
     */
    private final String dataSource;

    /**
     * Enum Constructor
     */
    DataSource(String dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Get the DataSource
     */
    public String getDatasource() {
        return this.dataSource;
    }
}
