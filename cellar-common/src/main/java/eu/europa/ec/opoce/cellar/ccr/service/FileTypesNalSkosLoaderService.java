/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ccr.service
 *             FILE : FileTypesNalSkosLoaderService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 26 août 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.service;

import eu.europa.ec.opoce.cellar.cl.domain.mimeType.MimeTypeMapping;

import java.util.List;
import java.util.Set;

/**
 * The Interface FileTypesNalSkosLoaderService.
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 26 août 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface FileTypesNalSkosLoaderService {

    /**
     * Check if the file-types were loaded correctly.
     * Used by cellar's splahs screen
     * @return true, if the in memory maps contain elements
     */
    boolean areAllItemsLoaded();

    /**
     * Number of file-type descriptions loaded.
     *
     * @return the int
     */
    int numberOfItemDescriptionsLoaded();

    /**
     * Gets the mime type mappings by extension.
     *
     * @param contentExtension the content extension
     * @return the mime type mappings by extension
     */
    List<MimeTypeMapping> getMimeTypeMappingsByExtension(String contentExtension);

    /**
     * Gets the mime type mappings by priority desc.
     *
     * @return the mime type mappings by priority desc
     */
    Set<MimeTypeMapping> getMimeTypeMappingsByPriorityDesc();

    /**
     * Gets the mime type mapping by manifestation type.
     *
     * @param manifestationType the manifestation type
     * @return the mime type mapping by manifestation type
     */
    List<MimeTypeMapping> getMimeTypeMappingByManifestationType(String manifestationType);

    /**
     * Gets the file extension matching mimetype.
     *
     * @param mimeType the mime type
     * @return the file extension matching mimetype
     */
    Set<String> getFileExtensionMatchingMimetype(String mimeType);

    /**
     * Gets the all possible extensions.
     *
     * @return the all possible extensions
     */
    Set<String> getAllPossibleExtensions();

    List<MimeTypeMapping> getMimeTypeMappingByMimeType(String mimeType);
}
