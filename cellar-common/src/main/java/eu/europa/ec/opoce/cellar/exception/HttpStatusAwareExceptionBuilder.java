/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.exception
 *        FILE : HttpStatusAwareExceptionBuilder.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 30-01-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.exception;

import eu.europa.ec.opoce.cellar.MessageCode;
import eu.europa.ec.opoce.cellar.MessageCode.TYPE;

import org.springframework.http.HttpStatus;

/**
 * This is the exception builder meant to build an instance of {@link HttpStatusAwareException}.</br>
 * </br> 
 * ON : 30-01-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public class HttpStatusAwareExceptionBuilder<EXC extends HttpStatusAwareException> extends ExceptionBuilder<EXC> {

    /**
     * The exception's HTTP status.
     */
    protected HttpStatus httpStatus;

    public static <EXC extends HttpStatusAwareException> HttpStatusAwareExceptionBuilder<EXC> getInstance(final Class<EXC> exceptionClass) {
        return new HttpStatusAwareExceptionBuilder<EXC>(exceptionClass);
    }

    protected HttpStatusAwareExceptionBuilder(final Class<EXC> exceptionClass) {
        super(exceptionClass);
    }

    public HttpStatusAwareExceptionBuilder<EXC> withHttpStatus(final HttpStatus httpStatus) {
        this.httpStatus = httpStatus;

        return this;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.exception.ExceptionBuilder#fallback()
     */
    @Override
    protected void fallback() {
        if (this.httpStatus == null) {
            this.httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        if (this.code == null) {
            final TYPE messageType = this.resolveMessageTypeFromHttpStatus();
            this.code = new MessageCode("HTTP-E-" + this.httpStatus.value(), messageType);
        }

        super.fallback();
    }

    private TYPE resolveMessageTypeFromHttpStatus() {
        TYPE messageType = null;

        final int httpStatusValue = this.httpStatus.value();
        if (httpStatusValue < 399) {
            messageType = TYPE.SUCCESS;
        } else if (httpStatusValue < 499) {
            messageType = TYPE.FUNCTIONAL_ERROR;
        } else {
            messageType = TYPE.TECHNICAL_ERROR;
        }

        return messageType;
    }

}
