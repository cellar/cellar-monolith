package eu.europa.ec.opoce.cellar.cl.domain.transformation.inbound.impl;

import eu.europa.ec.opoce.cellar.cl.domain.transformation.inbound.ContentStreamAndIdentifiers;
import org.apache.commons.lang.StringUtils;

import java.io.InputStream;
import java.util.List;

/**
 * Links a content input stream with 1 or more identifiers.
 * @author dcraeye
 *
 */
public class ContentStreamAndIdentifiersImpl implements ContentStreamAndIdentifiers {

    /** List of identifiers related to this content. */
    private List<String> contentIdList;

    /** Inputstream of this content. */
    private InputStream inputStream;

    /** manifestation type of this content. */
    private String manifestationType;

    /** Checksum of this content. */
    private String checksum;
    /** ChecksumType of this content. */
    private String checksumType;

    /**
     * Link several content Identifiers with this content stream. 
     * @param contentIdList list of content identifier.
     * @param inputStream the inputstream.
     */
    public ContentStreamAndIdentifiersImpl(List<String> contentIdList, InputStream inputStream, String manifestationType) {
        super();
        this.contentIdList = contentIdList;
        this.inputStream = inputStream;
        this.manifestationType = manifestationType;
    }

    /**
     * Returns the content identifiers of this content stream.
     * @return a list of identifiers.
     */
    public List<String> getContentIdentifierList() {
        return contentIdList;
    }

    public void setContentIdList(List<String> contentIdList) {
        this.contentIdList = contentIdList;
    }

    /**
     * Returns the {@link InputStream} of this content.
     * The inputstream is open and shoud be close by the caller method.
     * @return the inputstream.
     */
    public InputStream getContentStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public void setManifestationType(String manifestationType) {
        this.manifestationType = manifestationType;
    }

    /**
     * Returns the manifestation type of this content.
     * @return the manifestation type.
     */
    public String getManifestationType() {
        return manifestationType;
    }

    /**
     * Returns the checksum of this content.
     * @return the checksum.
     */
    public String getChecksum() {
        if (StringUtils.isBlank(checksum))
            checksum = "N.A.";
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    /**
     * Returns the checksumType of this content.
     * @return the checksumType.
     */
    public String getChecksumType() {
        if (StringUtils.isBlank(checksumType))
            checksum = "N.A.";
        return checksumType;
    }

    public void setChecksumType(String checksumType) {
        this.checksumType = checksumType;
    }

}
