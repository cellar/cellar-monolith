package eu.europa.ec.opoce.cellar.domain.content.mets;

public enum MetadataType {
    DMD, // case dmdSec
    TECHNICAL, // case amdSec/techMd 
    ABSENT // case DMD is missing -> considered as empty 
}
