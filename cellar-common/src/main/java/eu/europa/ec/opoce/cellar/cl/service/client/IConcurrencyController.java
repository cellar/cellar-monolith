/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.client
 *             FILE : IConcurrencyController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 30, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.client;

import eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLockSession;
import eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLockStatistic;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsPackage;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;

import java.util.List;
import java.util.Map;

/**
 * <class_description> Concurrency controller interface.
 * <br/><br/>
 * ON : Apr 30, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IConcurrencyController {

    /**
     * Return the concurrency controller statistics.
     *
     * @return the concurrency controller statistics
     */
	Map<String, List<ICellarLockStatistic>> getConcurrencyControllerStatistics();

    /**
     * Lock for ingesting the struct map and the related data.
     * 
     * @param cellarLockSession the lock session
     * @param metsPackage the metsPackage containing the struct map data
     * @param structMap the struct map to lock
     * @throws InterruptedException the interrupted exception
     */
    void lockForIngestion(final ICellarLockSession cellarLockSession, final MetsPackage metsPackage, final StructMap structMap)
            throws InterruptedException;

    /**
     * Lock for embargo/disembargo.
     * 
     * @param cellarLockSession the lock session
     * @param cellarId the cellar identifier concerned by the embargo/disembargo
     * @throws InterruptedException the interrupted exception
     */
    void lockForEmbargo(final ICellarLockSession cellarLockSession, final String cellarId) throws InterruptedException;

    /**
     * Lock for export.
     * 
     * @param cellarLockSession the lock session
     * @param pid the production identifier concerned by the export
     * @throws InterruptedException the interrupted exception
     */
    void lockForExport(final ICellarLockSession cellarLockSession, final String pid) throws InterruptedException;

    /**
     * Lock for execute the cleaning.
     * 
     * @param cellarLockSession the lock session
     * @param cellarId the cellar identifier concerned by the cleaning
     * @throws InterruptedException the interrupted exception
     */
    void lockForCleaning(final ICellarLockSession cellarLockSession, final String cellarId) throws InterruptedException;

    /**
     * Lock for execute the cleaning/scheduling.
     * 
     * @param cellarLockSession the lock session
     * @param cellarId the cellar identifier concerned by the cleaning/scheduling
     * @throws InterruptedException the interrupted exception
     */
    void lockForCleaningScheduling(final ICellarLockSession cellarLockSession, final String cellarId) throws InterruptedException;

    /**
     * Lock for execute the aligning of misaligned resources.
     * 
     * @param cellarLockSession the lock session
     * @param cellarId the cellar identifier concerned by the aligning
     * @throws InterruptedException the interrupted exception
     */
    void lockForAligning(final ICellarLockSession cellarLockSession, final String cellarId) throws InterruptedException;

    /**
     * Lock for aligning scheduling.
     *
     * @param cellarLockSession the lock session
     * @param cellarId the cellar id
     * @throws InterruptedException the interrupted exception
     */
    void lockForAligningScheduling(final ICellarLockSession cellarLockSession, final String cellarId) throws InterruptedException;

    /**
     * Lock for indexing.
     * 
     * @param cellarLockSession the lock session
     * @param cellarIdUri the URI concerned by the generating
     * @throws InterruptedException the interrupted exception
     */
    void lockForIndexing(final ICellarLockSession cellarLockSession, final String cellarIdUri) throws InterruptedException;

    /**
     * Lock for virtuoso backup.
     *
     * @param cellarLockSession the lock session
     * @param cellarId the cellar id
     * @throws InterruptedException the interrupted exception
     */
    void lockForVirtuosoBackup(final ICellarLockSession cellarLockSession, final String cellarId) throws InterruptedException;

    /**
     * Unlock the lock session.
     * 
     * @param cellarLockSession the lock session to unlock
     */
    void unlock(final ICellarLockSession cellarLockSessione);

    /**
     * Lock for synchronizing tables CMR_CELLAR_RESOURCE_MD and CMR_INDEXATION_CALC_EMBEDDED_NOTICE.
     *
     * @param cellarLockSession the lock session
     * @param cellarId the cellar id
     * @throws InterruptedException the interrupted exception
     */
    void lockForEmbeddedNoticeSynchronization(ICellarLockSession cellarLockSession, final String cellarId) throws InterruptedException;
}
