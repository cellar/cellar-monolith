package eu.europa.ec.opoce.cellar.atomikos;

import com.atomikos.datasource.pool.event.ConnectionPoolExhaustedEvent;
import com.atomikos.icatch.event.Event;
import com.atomikos.icatch.event.EventListener;
import com.atomikos.publish.EventPublisher;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import static eu.europa.ec.opoce.cellar.cl.domain.audit.Auditable.LogLevel.WARN;

/**
 * @author ARHS Developments
 */
@Component
public class AtomikosEventListener implements EventListener {

    private static final Logger LOG = LogManager.getLogger(AtomikosEventListener.class);

    @PostConstruct
    public void register() {
        EventPublisher.registerEventListener(this);
    }

    @Override
    public void eventOccurred(Event event) {
        if (event instanceof ConnectionPoolExhaustedEvent) {
            ConnectionPoolExhaustedEvent exhausted = (ConnectionPoolExhaustedEvent) event;
            AuditBuilder.get()
                    .withMessage("Connection pool exhausted for resource {}. Consider increase the max pool size.")
                    .withMessageArgs(exhausted.uniqueResourceName)
                    .withLogger(LOG)
                    .withLogLevel(WARN)
                    .logEvent();
        }
    }
}
