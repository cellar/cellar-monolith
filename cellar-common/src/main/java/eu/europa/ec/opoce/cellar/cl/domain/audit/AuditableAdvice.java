/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.audit
 *             FILE : AuditableAdvice.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 02-10-2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.audit;

import eu.europa.ec.opoce.cellar.cl.domain.package_history.PackageHistory;
import eu.europa.ec.opoce.cellar.cl.domain.structmap_status_history.StructMapStatusHistory;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;

import static org.apache.commons.lang.StringUtils.defaultString;

/**
 * <class_description> This is an advice that, at each method annotated with {@link Auditable}, logs the console and/or the database.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 02-10-2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Aspect
@Order(0)
public class AuditableAdvice {

    /** class's LOGGER. */
    private transient static final Logger ADVICE_LOGGER = LogManager.getLogger(AuditableAdvice.class);

    public static final String AUDIT_KEY = "audit_key";

    private static final AuditableAdvice instance = new AuditableAdvice();

    @Around("@annotation(auditable)")
    public Object audit(final ProceedingJoinPoint pjp, final Auditable auditable) throws Throwable {
        final Object[] pjpArgs = pjp.getArgs();
        final Object callingObject = pjp.getThis();

        // collect info from auditable object
        final AuditTrailEventProcess process = auditable.process();
        final boolean start = auditable.start();
        final boolean end = auditable.end();
        final boolean fail = auditable.fail();
        final Auditable.LogLevel logLevel = auditable.logLevel();
        final Logger logger = LogManager.getLogger(pjp.getTarget().getClass());

        AuditTrailEventAction action = auditable.action();
        if (AuditTrailEventAction.Undefined == action && callingObject instanceof IAuditableActionResolver) {
            action = ((IAuditableActionResolver) callingObject).resolveAction(pjpArgs);
        }

        String objectIdentifier = null;
        if (callingObject instanceof IAuditableObjectIdentifierResolver) {
            objectIdentifier = ((IAuditableObjectIdentifierResolver) callingObject).resolveObjectIdentifier(pjpArgs);
        }
        
        StructMapStatusHistory structMapStatusHistory = null;
        if (callingObject instanceof IAuditableStructMapStatusHistoryResolver) {
            structMapStatusHistory = ((IAuditableStructMapStatusHistoryResolver) callingObject).resolveStructMapStatusHistory(pjpArgs);
        }
        
        PackageHistory packageHistory = null;
        if (callingObject instanceof IAuditableSIPResourceResolver) {
            packageHistory = (((IAuditableSIPResourceResolver) callingObject).resolveSIPResource(pjpArgs)).getPackageHistory();
        }

        // process start
        if (start) {
            this.saveAndPrintMessageAudit(process, action, AuditTrailEventType.Start, objectIdentifier, structMapStatusHistory, packageHistory, logLevel, logger);
        }

        // proceed
        Object retObject = null;
        try {
            retObject = pjp.proceed(pjpArgs);
        } catch (Throwable exception) {
            try {
                if (fail) {
                    this.saveAndPrintErrorAudit(process, action, objectIdentifier,  structMapStatusHistory, packageHistory, exception, logger);
                }
            } catch (Exception e) {
                ADVICE_LOGGER.error(e.getMessage());
            }
            throw exception;
        }

        // process end
        if (end) {
            this.saveAndPrintMessageAudit(process, action, AuditTrailEventType.End, objectIdentifier, structMapStatusHistory, packageHistory,logLevel, logger);
        }

        return retObject;
    }

    private void saveAndPrintMessageAudit(final AuditTrailEventProcess process, final AuditTrailEventAction action,
            final AuditTrailEventType type, final String objectIdentifier, final StructMapStatusHistory structMapStatusHistory,
            final PackageHistory packageHistory, final Auditable.LogLevel logLevel, final Logger logger) {
        // save the message in audit log table
        try {
            AuditBuilder.get(process) //
                    .withAction(action) //
                    .withType(type) //
                    .withObject(defaultString(objectIdentifier)) //
                    .withStructMapStatusHistory(structMapStatusHistory) //
                    .withPackageHistory(packageHistory) //
                    .withProcessId(ThreadContext.get(AUDIT_KEY)) //
                    .withLogger(logger) //
                    .withLogLevel(logLevel) //
                    .withLogDatabase(true) //
                    .logEvent();
        } catch (Exception e) {
            ADVICE_LOGGER.error("Failed to save event in audit log table", e.getMessage());
        }
    }

    private void saveAndPrintErrorAudit(final AuditTrailEventProcess process, final AuditTrailEventAction action,
            final String objectIdentifier, final StructMapStatusHistory structMapStatusHistory,
            final PackageHistory packageHistory, final Throwable exception, final Logger logger) {
        // if throwable is a non-cellar exception, embed it into a cellar exception
        Throwable myException = exception;
        if (!(myException instanceof CellarException)) {
            myException = ExceptionBuilder.get(CellarException.class).withCause(myException).build();
        }

        // save the message in audit log table (but only if the exception is not silent)
        final CellarException cellarException = (CellarException) myException;
        if (!cellarException.isSilent()) {
            try {
                AuditBuilder.get(process) //
                        .withAction(action) //
                        .withType(AuditTrailEventType.Fail) //
                        .withObject(defaultString(objectIdentifier)) //
                        .withStructMapStatusHistory(structMapStatusHistory) //
                        .withPackageHistory(packageHistory) //
                        .withProcessId(ThreadContext.get(AUDIT_KEY)) //
                        .withException(cellarException) //
                        .withLogger(logger) //
                        .withLogDatabase(true) //
                        .logEvent();
            } catch (Exception e) {
                ADVICE_LOGGER.error("Failed to save error in audit log table", e.getMessage());
            }
        }
    }

    /**
     * Needed by AspectJ
     */
    public static AuditableAdvice aspectOf() {
        return instance;
    }

}
