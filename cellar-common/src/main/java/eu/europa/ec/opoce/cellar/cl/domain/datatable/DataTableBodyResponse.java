package eu.europa.ec.opoce.cellar.cl.domain.datatable;

import java.util.List;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * The Class DataTableBodyResponse.
 *
 * @param <E> the element type
 */
public class DataTableBodyResponse<E> {

    /** The records total. */
    private String recordsTotal;

    /** The records filtered. */
    private String recordsFiltered;

    /** The draw. */
    private String draw;

    /** The data. */
    private List<E> data;

    /**
     * Gets the records total.
     *
     * @return the records total
     */
    public String getRecordsTotal() {
        return this.recordsTotal;
    }

    /**
     * Sets the records total.
     *
     * @param recordsTotal the new records total
     */
    public void setRecordsTotal(final String recordsTotal) {
        this.recordsTotal = recordsTotal;
    }

    /**
     * Gets the records filtered.
     *
     * @return the records filtered
     */
    public String getRecordsFiltered() {
        return this.recordsFiltered;
    }

    /**
     * Sets the records filtered.
     *
     * @param recordsFiltered the new records filtered
     */
    public void setRecordsFiltered(final String recordsFiltered) {
        this.recordsFiltered = recordsFiltered;
    }

    /**
     * Gets the draw.
     *
     * @return the draw
     */
    public String getDraw() {
        return this.draw;
    }

    /**
     * Sets the draw.
     *
     * @param draw the new draw
     */
    public void setDraw(final String draw) {
        this.draw = draw;
    }

    /**
     * Gets the data.
     *
     * @return the data
     */
    public List<E> getData() {
        return this.data;
    }

    /**
     * Sets the data.
     *
     * @param data the new data
     */
    public void setData(final List<E> data) {
        this.data = data;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
