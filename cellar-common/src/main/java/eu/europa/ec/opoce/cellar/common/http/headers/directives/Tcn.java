/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.httpHeaders.directives
 *             FILE : Tcn.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 3, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.http.headers.directives;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 3, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public enum Tcn {
    CHOICE("choice");

    private String directive;

    private Tcn(final String directive) {
        this.directive = directive;
    }

    public String getDirective() {
        return this.directive;
    }
}
