/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.dao
 *             FILE : CellarIdentifierDao.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Sep 30, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$$
 *          VERSION : $LastChangedRevision$$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao;

import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;

import java.util.List;

/**
 * Dao for the the Entity {@link CellarIdentifier}.
 *
 * @author dsavares
 *
 */
public interface CellarIdentifierDao {

    /**
     * Persist the given transient instance.
     *
     * @param cellarIdentifier            the transient instance to be persisted
     * @return the cellar identifier
     */
    CellarIdentifier saveCellarIdentifier(CellarIdentifier cellarIdentifier);

    /**
     * Update the given instance.
     *
     * @param cellarIdentifier
     *            Cellar identifier to update.
     */
    void updateCellarIdentifier(CellarIdentifier cellarIdentifier);

    /**
     * Return the persistent instance with the given internal identifier, or null if not found.
     *
     * @param id
     *            the internal identifier of the persistent instance
     *
     * @return the persistent instance, or null if not found
     */
    CellarIdentifier getCellarIdentifier(Long id);

    /**
     * Return all persistent instances of {@link CellarIdentifier}.
     *
     * @return a {@link List} containing 0 or more persistent instances.
     */
    //    List<CellarIdentifier> getCellarIdentifiers();

    /**
     * Delete the given persistent instance.
     *
     * @param cellarIdentifier
     *            the persistent instance to delete
     */
    void deleteCellarIdentifier(CellarIdentifier cellarIdentifier);

    /**
     * Return the persistent instance with the given business identifier, or null if not found.
     *
     * @param identifier
     *            the business identifier of the persistent instance
     *
     * @return the persistent instance, or null if not found
     */
    CellarIdentifier getCellarIdentifier(String identifier);

    /**
     * Get the instance with the specified UUID and file name. Must be a content stream identifier.
     *
     * @param identifier
     *            Cellar UUID
     * @param fileRef
     *            File name of the content stream.
     * @return The <code>CellarIdentifier</code> instance that matches the UUID and the file name.
     */
    CellarIdentifier getCellarIdentifier(String identifier, String fileRef);

    /**
     * Indicates whether the given cellar identifier exists in database.
     *
     * @param cellarIdentifier
     *            the cellar identifier to test
     * @return true if the cellar identifier exists in database, false otherwise
     */
    boolean isCellarIdentifierExists(CellarIdentifier cellarIdentifier);

    /**
     * Get all the cellar identifier of an object tree with all associated content ids.
     *
     * @param cellarId
     *            The cellar identifier of the top node of the tree of ids to be retrieved.
     * @return A <code>List</code> of <code>Identifier</code> instances.
     */
    List<Identifier> getTreePid(String cellarId);

    /**
     * Get all the cellar identifier of an object tree with all associated content ids.
     *
     * @param cellarId
     *            The cellar identifier of the top node of the tree of ids to be retrieved.
     * @param withContentStream
     *            Flag to return also content stream id or not.
     * @return A <code>List</code> of <code>Identifier</code> instances.
     */
    List<Identifier> getTreePid(String cellarId, boolean withContentStream);

    /**
     * Get all the cellar identifier of an object tree with all associated content ids.
     *
     * @param cellarId
     *            The cellar identifier of the top node of the tree of ids to be retrieved.
     * @return A <code>List</code> of <code>Identifier</code> instances.
     */
    List<Identifier> getTreeContentPid(String cellarId);

    /**
     * get all the children CellarIdentifiers of the given expression.
     *
     * @param expressionCellarId the expression cellar id
     * @return the list of children CellarIdentifier
     */
    List<CellarIdentifier> getExpressionChildrenPids(String expressionCellarId);

    /**
     * get all (except content streams) the children CellarIdentifiers of the given id.
     *
     * @param cellarId the cellarId of the parent
     * @return the list of children CellarIdentifier
     */
    List<CellarIdentifier> getChildrenPids(String cellarId);

    /**
     * Gets all the children CellarIdentifiers of the given id.
     *
     * @param cellarId the cellar id
     * @return the content streams pids
     */
    List<CellarIdentifier> getAllChildrenPids(String cellarId);

    /**
     * Gets the max doc index.
     *
     * @param uuid the uuid
     * @return the max doc index
     */
    Integer getMaxDocIndex(final String uuid);
}
