/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.monitoring.cache
 *        FILE : CacheManagerMBeanAgent.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 03-01-2023
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2023 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.monitoring.cache;

import java.lang.management.ManagementFactory;

import javax.annotation.PostConstruct;
import javax.management.MBeanServer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import net.sf.ehcache.CacheManager;
import net.sf.ehcache.management.ManagementService;

/**
 * Helper bean for registering the CacheManager MBean to the MBeanServer.
 * @author EUROPEAN DYNAMICS S.A
 */
@Component
public class CacheManagerMBeanAgent {

	/**
	 * The cache manager in use.
	 */
	private final CacheManager cacheManager;
	
	
	@Autowired
	public CacheManagerMBeanAgent(@Qualifier("realCacheManager") CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}


	/**
	 * Registers CacheManager MBean to the MBeanServer.
	 */
	@PostConstruct
	public void registerCacheManager() {
		final MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
		ManagementService.registerMBeans(cacheManager, mbs, false, true, true, true);
	}
	
}
