/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.map
 *             FILE : LRUSynchronizedMap.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Mar 24, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.map;

import org.apache.commons.collections15.map.LRUMap;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page. <br/>
 * <br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc. <br/>
 * <br/>
 * ON : Mar 24, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class LRUSynchronizedMap<K, V> implements Map<K, V> {

	private final ReentrantReadWriteLock lock;
	private LRUMap<K, V> map;

	public LRUSynchronizedMap() {
		this.lock = new ReentrantReadWriteLock();
		this.map = new LRUMap<K, V>();
	}

	public LRUSynchronizedMap(final int maxSize) {
		this.lock = new ReentrantReadWriteLock();
		this.initialize(maxSize);
	}

	private void initialize(final int maxSize) {
		this.lock.writeLock().lock();
		try {
			this.map = new LRUMap<K, V>(maxSize);
		} finally {
			unlock(this.lock.writeLock());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Map#size()
	 */
	@Override
	public int size() {
		this.lock.readLock().lock();
		try {
			return this.map.size();
		} finally {
			unlock(this.lock.readLock());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Map#isEmpty()
	 */
	@Override
	public boolean isEmpty() {
		this.lock.readLock().lock();
		try {
			return this.map.isEmpty();
		} finally {
			unlock(this.lock.readLock());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Map#containsKey(java.lang.Object)
	 */
	@Override
	public boolean containsKey(Object key) {
		this.lock.readLock().lock();
		try {
			return this.map.containsKey(key);
		} finally {
			unlock(this.lock.readLock());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Map#containsValue(java.lang.Object)
	 */
	@Override
	public boolean containsValue(Object value) {
		this.lock.readLock().lock();
		try {
			return this.map.containsValue(value);
		} finally {
			unlock(this.lock.readLock());
		}
	}

	private void unlock(Lock lockParam) {
		if (lockParam != null) {
			lockParam.unlock();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Map#get(java.lang.Object)
	 */
	@Override
	public V get(Object key) {
		this.lock.readLock().lock();
		try {
			return this.map.get(key);
		} finally {
			unlock(this.lock.readLock());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Map#put(java.lang.Object, java.lang.Object)
	 */
	@Override
	public V put(K key, V value) {
		this.lock.writeLock().lock();
		try {
			return this.map.put(key, value);
		} finally {
			unlock(this.lock.writeLock());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Map#remove(java.lang.Object)
	 */
	@Override
	public V remove(Object key) {

		this.lock.writeLock().lock();
		try {
			return this.map.remove(key);
		} finally {
			unlock(this.lock.writeLock());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Map#putAll(java.util.Map)
	 */
	@Override
	public void putAll(Map<? extends K, ? extends V> m) {

		this.lock.writeLock().lock();
		try {
			this.map.putAll(m);
		} finally {
			unlock(this.lock.writeLock());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Map#clear()
	 */
	@Override
	public void clear() {

		this.lock.writeLock().lock();
		try {
			this.map.clear();
		} finally {
			unlock(this.lock.writeLock());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Map#keySet()
	 */
	@Override
	public Set<K> keySet() {

		this.lock.readLock().lock();
		try {
			return this.map.keySet();
		} finally {
			unlock(this.lock.readLock());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Map#values()
	 */
	@Override
	public Collection<V> values() {

		this.lock.readLock().lock();
		try {
			return this.map.values();
		} finally {
			unlock(this.lock.readLock());
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Map#entrySet()
	 */
	@Override
	public Set<Map.Entry<K, V>> entrySet() {

		this.lock.readLock().lock();
		try {
			return this.map.entrySet();
		} finally {
			unlock(this.lock.readLock());
		}
	}

}
