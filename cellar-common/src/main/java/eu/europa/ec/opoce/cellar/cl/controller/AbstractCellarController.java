/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller
 *             FILE : AbstractCellarController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Nov 6, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL;
import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessageContainer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 * <class_description> Cellar super controller that injects the Cellar configuration in {@ ModelAndView} instances.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Nov 6, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public abstract class AbstractCellarController {

    private final static String URL_PREFIX = "/admin";

    protected final static String CONTENT_TYPE_TEXT = "text/plain";

    @Autowired
    private ICellarUserMessageContainer cellarUserMessageContainer;

    /**
     * The Cellar configuration storing runtime parameters.
     */
    @Autowired
    @Qualifier("cellarConfiguration")
    protected ICellarConfiguration cellarConfiguration;

    @ModelAttribute("cellarConfiguration")
    public ICellarConfiguration exposeConfiguration() {
        return this.cellarConfiguration;
    }

    /**
     * Adds a new user message.
     * @param messageLevel the level of the message
     * @param messageCode the message code to use to retrieve the message from the message source
     */
    public void addUserMessage(final LEVEL messageLevel, final String messageCode) {
        this.cellarUserMessageContainer.addMessage(messageLevel, messageCode);
    }

    /**
     * Adds a new user message.
     * @param messageLevel the level of the message
     * @param messageCode the message code to use to retrieve the message from the message source
     * @param defaultMessage the default message used if there is no message in the message source
     */
    public void addUserMessage(final LEVEL messageLevel, final String messageCode, final String defaultMessage) {
        this.cellarUserMessageContainer.addMessage(messageLevel, messageCode, defaultMessage);
    }

    /**
     * Adds a new user message.
     * @param messageLevel the level of the message
     * @param messageCode the message code to use to retrieve the message from the message source
     * @param defaultMessage the default message used if there is no message in the message source
     * @param messageArguments arguments to inject in the message
     */
    public void addUserMessage(final LEVEL messageLevel, final String messageCode, final String defaultMessage,
            final Object[] messageArguments) {
        this.cellarUserMessageContainer.addMessage(messageLevel, messageCode, defaultMessage, messageArguments);
    }

    /**
     * Returns the redirect pattern with the pageUrl.
     * @param pageUrl the destination
     * @return the redirect pattern with destination
     */
    public static String redirect(final String pageUrl) {
        return "redirect:" + URL_PREFIX + pageUrl;
    }

    /**
     * Returns the prefixed url.
     * @param pageUrl the url to prefix
     * @return the prefixed url
     */
    public static String prefixedUrl(final String pageUrl) {
        return URL_PREFIX + pageUrl;
    }

    /**
     * Returns the redirect pattern with the pageUrl and the first parameter.
     * @param pageUrl the destination
     * @param paramId the parameter identifier
     * @param paramValue the parameter value
     * @return the redirect pattern with the destination and the parameter
     */
    public static String redirect(final String pageUrl, final String paramId, final Object paramValue) {
        return redirect(pageUrl) + "?" + paramId + "=" + paramValue.toString();
    }
}
