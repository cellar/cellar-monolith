package eu.europa.ec.opoce.cellar.cl.dao.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.dao.BaseDao;
import eu.europa.ec.opoce.cellar.cl.dao.hibernate.CellarHibernateTemplate;
import eu.europa.ec.opoce.cellar.exception.NotImplementedExceptionBuilder;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.*;
import java.util.Map.Entry;

/**
 * A basic dao extending the {@link HibernateDaoSupport}.
 *
 * @author dsavares
 *
 * @param <ENTITY> the entity's type to be handled by this DAO
 * @param <ID> the id type for the serial ID of the entity
 */
public abstract class BaseDaoImpl<ENTITY, ID extends Serializable> extends HibernateDaoSupport implements BaseDao<ENTITY, ID> {

    private Class<ENTITY> entityClass;

    @SuppressWarnings("unchecked")
    protected Class<ENTITY> getEntityClass() {
        if (this.entityClass == null) {
            this.entityClass = (Class<ENTITY>) ((ParameterizedType) this.getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        }
        return this.entityClass;
    }

    /**
     * The SessionFactory to be used by this DAO.
     */
    @Autowired
    @Qualifier("cellarSessionFactory")
    private SessionFactory sessionFactory;

    /**
     * The cellar configuration to use.
     * It is static and not persistent, as the latter one uses this Dao class itself, and would not be completely initialized at this point.
     */
    @Autowired
    @Qualifier("cellarStaticConfiguration")
    protected ICellarConfiguration cellarConfiguration;

    /**
     * Set the Hibernate SessionFactory to be used by this DAO.
     */
    @PostConstruct
    void setFactoryBean() {
        super.setSessionFactory(this.sessionFactory);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected HibernateTemplate createHibernateTemplate(final SessionFactory sessionFactory) {
        return new CellarHibernateTemplate(sessionFactory, this.cellarConfiguration.isCellarDatabaseReadOnly());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ENTITY saveOrUpdateObject(final ENTITY objectToSave) {
        this.getHibernateTemplate().saveOrUpdate(objectToSave);
        return objectToSave;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ENTITY saveObject(final ENTITY objectToSave) {
        this.getHibernateTemplate().save(objectToSave);
        return objectToSave;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ENTITY updateObject(final ENTITY objectToUpdate) {
        this.getHibernateTemplate().update(objectToUpdate);
        return objectToUpdate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ENTITY> findAll() {
        return this.getHibernateTemplate().loadAll(this.getEntityClass());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<?> findByNamedQuery(final String queryName) {
        return this.getHibernateTemplate().findByNamedQuery(queryName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ENTITY getObject(final ID id) {
        return this.getHibernateTemplate().get(this.getEntityClass(), id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ENTITY upsertObject(final ENTITY daoObject, final String uniqueColumnName) {
        throw NotImplementedExceptionBuilder.get().withMessage("This method is not implemented for this DAO.").build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteObject(final ENTITY objectToDelete) {
        this.getHibernateTemplate().delete(objectToDelete);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteObjects(final Collection<ENTITY> objectsToDelete) {
        this.getHibernateTemplate().deleteAll(objectsToDelete);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int deleteAll() {
        return this.getHibernateTemplate().execute(session -> {
            final Query queryObject = session.createQuery("delete from " + getEntityClass().getSimpleName());
            return queryObject.executeUpdate();
        });
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public int updateObjectsByNamedQueryWithNamedParams(final String queryName, final Map<String, Object> params) {
        return this.getHibernateTemplate().execute(session -> {
            final Query queryObject = buildNamedQuery(session, queryName, params);
            return queryObject.executeUpdate();
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ENTITY findObjectByNamedQuery(final String queryName, final Object... values) throws DataAccessException {
        return this.getHibernateTemplate().execute(session -> {
            final Query queryObject = buildNamedQuery(session, queryName, values);
            return (ENTITY) queryObject.uniqueResult();
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ENTITY findObjectByNamedQueryWithNamedParams(final String queryName, final Map<String, Object> params)
            throws DataAccessException {
        return this.getHibernateTemplate().execute(session -> {
            final Query queryObject = buildNamedQuery(session, queryName, params);
            return (ENTITY) queryObject.uniqueResult();
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ENTITY> findObjectsByNamedQuery(final String queryName, final Object... values) throws DataAccessException {
        return this.getHibernateTemplate().execute(session -> {
            final Query queryObject = buildNamedQuery(session, queryName, values);
            return queryObject.list();
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ENTITY> findObjectsByNamedQueryWithNamedParams(final String queryName, final Map<String, Object> params)
            throws DataAccessException {
        return this.getHibernateTemplate().execute(session -> {
            final Query queryObject = buildNamedQuery(session, queryName, params);
            return queryObject.list();
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ENTITY> findObjectsByNamedQueryWithNamedParamsLimit(final String queryName, final Map<String, Object> params,
            final int startIndex, final int endIndex) throws DataAccessException {
        return this.getHibernateTemplate().execute(session -> {
            final Query queryObject = buildNamedQuery(session, queryName, params);
            queryObject.setFirstResult(startIndex);
            queryObject.setMaxResults(endIndex);
            return queryObject.list();
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ENTITY> findObjectsByNamedQueryWithPagedParams(final String queryName, final String paramsName, final List<String> params,
            final int paramsPageSize) {
        final List<ENTITY> results = new ArrayList<>();

        int fromIndex = 0;
        int toIndex = 0;
        while (toIndex != params.size()) {
            toIndex = fromIndex + paramsPageSize;
            if (toIndex <= 0 || toIndex > params.size()) {
                toIndex = params.size();
            }
            final List<String> pagedParams = params.subList(fromIndex, toIndex);
            if (!pagedParams.isEmpty()) {
                final Map<String, Object> pagedParamsParam = new HashMap<>();
                pagedParamsParam.put(paramsName, pagedParams);
                final List<ENTITY> pagedResults = this.findObjectsByNamedQueryWithNamedParams(queryName, pagedParamsParam);
                results.addAll(pagedResults);
            }
            fromIndex = toIndex;
        }

        return results;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ENTITY> findPaginatedObjectsByNamedQuery(final String queryName, final int first, final int last, final Object... values)
            throws DataAccessException {
        return this.getHibernateTemplate().execute(session -> {
            final Query queryObject = buildNamedQuery(session, queryName, values);
            queryObject.setFirstResult(first);
            if (last > 0) {
                queryObject.setMaxResults(last - first);
            }
            return queryObject.list();
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ENTITY> findObjectsByNamedQueryWithNamedParams(final String queryName, final int first, final int last,
            final Map<String, Object> params) throws DataAccessException {
        return this.getHibernateTemplate().execute(session -> {
            final Query queryObject = buildNamedQuery(session, queryName, params);
            queryObject.setFirstResult(first);
            queryObject.setMaxResults(last - first);
            return queryObject.list();
        });
    }

    public static Query buildNamedQuery(final Session session, final String queryName, final Map<String, Object> params) {
        final Query query = session.getNamedQuery(queryName);

        if (params != null) {
            for (final Entry<String, Object> param : params.entrySet()) {
                if (param.getValue() instanceof Collection<?>) {
                    query.setParameterList(param.getKey(), (Collection<?>) param.getValue());
                } else {
                    query.setParameter(param.getKey(), param.getValue());
                }
            }
        }

        return query;
    }

    public static Query buildNamedQuery(final Session session, final String queryName, final Object... values) {
        final Query query = session.getNamedQuery(queryName);

        if (values != null) {
            for (int i = 0; i < values.length; i++) {
                query.setParameter(i, values[i]);
            }
        }

        return query;
    }
}
