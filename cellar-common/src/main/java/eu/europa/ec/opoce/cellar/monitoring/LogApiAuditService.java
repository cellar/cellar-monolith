/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.monitoring
 *             FILE : LogApiAuditService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 04 04, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-04-04 11:05:15 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.monitoring;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * TODO replace this class by micrometer + influxdb (or similar)
 *
 * @author ARHS Developments
 */
@Service
public class LogApiAuditService implements ApiAuditService {

    private static final Logger LOG = LogManager.getLogger(LogApiAuditService.class);

    private final ConcurrentMap<String, Integer> counters = new ConcurrentHashMap<>();

    @Override
    public void audit(ApiCall apiCall) {
        String key = apiCall.getClazz() + "#" + apiCall.getMethod();
        updateCount(key);
        LOG.info("{} {} ({}) (count = {})", apiCall.getMethods(), apiCall.getPath(),
                apiCall.getArgs(), counters.get(key));
        LOG.debug("Call to [{} {} {}] handled by {} ", apiCall.getMethod(), apiCall.getPath(), apiCall.getArgs(), key);
    }

    private void updateCount(String key) {
        if (!counters.containsKey(key)) {
            counters.put(key, 1);
        } else {
            Integer oldVal, newVal;
            do {
                oldVal = counters.get(key);
                newVal = oldVal + 1;
            } while (!counters.replace(key, oldVal, newVal));
        }
    }

}
