/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.support
 *             FILE : IterableUtils.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 04-01-2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.util;

import com.google.common.collect.Lists;
import eu.europa.ec.opoce.cellar.common.closure.ClosureException;
import eu.europa.ec.opoce.cellar.common.closure.PClosure;
import eu.europa.ec.opoce.cellar.common.closure.RPClosure;
import eu.europa.ec.opoce.cellar.common.transformer.MapTransformer;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.MultivaluedMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 * <class_description> Utility class for manipulating iterable objects.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 04-01-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class IterableUtils {

    private static Logger LOGGER = LoggerFactory.getLogger(IterableUtils.class);

    public static <T> Iterable<Object> filter(Iterable<T> iter, RPClosure<Object, T> accessor, RPClosure<Boolean, T> filter) {
        List<Object> filterdList = new LinkedList<>();
        for (T t : iter) {
            if (filter.call(t)) {
                final Object data = accessor.call(t);
                if (data != null) {
                    filterdList.add(data);
                }
            }
        }
        return filterdList;
    }

    /**
     * Constructs a new Iterable by applying the accessor functor on each element of the Iterable.
     *
     * @param iter     the iterable to loop over
     * @param accessor the closure to access data of the object
     * @return a list
     */
    public static <T> Iterable<T> getEach(Iterable<T> iter, RPClosure<Object, T> accessor) {
        List<T> list = new LinkedList<>();
        for (T t : iter) {
            Object data;
            try {
                data = accessor.call(t);
            } catch (Exception e) {
                throw ExceptionBuilder.get(ClosureException.class).withCause(e).visibleOnStackTrace(false).build();
            }
            if (data != null) {
                list.add(t);
            }
        }
        return list;
    }

    /**
     * Constructs a new Iterable by applying the accessor functor on each element of the Iterable.
     *
     * @param iter     the iterable to loop over
     * @param accessor the closure to access data of the object
     * @return a list
     */
    public static <T> Iterable<T> getFirst(Iterable<T> iter, RPClosure<Object, T> accessor) {
        List<T> list = new LinkedList<>();
        for (T t : iter) {
            Object data;
            try {
                data = accessor.call(t);
            } catch (Exception e) {
                throw ExceptionBuilder.get(ClosureException.class).withCause(e).visibleOnStackTrace(false).build();
            }
            if (data != null) {
                list.add(t);
                break;
            }
        }
        return list;
    }

    /**
     * Converts each element of the provided list with the provided {@code MapTransformer}.
     */
    public static <T> Iterable<Map<String, Object>> convertEach(Collection<T> toConvert, MapTransformer<T> transformer) {
        List<Map<String, Object>> transformedList = new ArrayList<>(toConvert.size());
        for (T t : toConvert) {
            transformedList.add(transformer.transform(t));
        }
        return transformedList;
    }

    /**
     * Converts a {@link javax.ws.rs.core.MultivaluedMap} to a {@link java.util.Map}.</br>
     * Only the first value of each key-value pairs is retrieved from the multivalued map.
     *
     * @param sourceMap the multivalued map from where getting the key-value pairs
     * @return the map populated with the key-value pairs retrieved from the multivalued map
     */
    public static <K, V> Map<K, V> fromMultivaluedMapToMap(final MultivaluedMap<K, V> sourceMap) {
        final Map<K, V> targetMap = new HashMap<>();

        for (K key : sourceMap.keySet()) {
            targetMap.put(key, getSingleValueFromMultivaluedMap(sourceMap, key));
        }

        return targetMap;
    }

    /**
     * Gets the first value with key {@code key} from the multivalued {@code map} passed.
     *
     * @param map the multivalued map from where getting the value
     * @return the first value with key {@code key}
     */
    public static <K, V> V getSingleValueFromMultivaluedMap(final MultivaluedMap<K, V> map, final K key) {
        final List<V> values = map.get(key);
        if (values != null && values.size() == 1) {
            return values.get(0);
        } else {
            return null;
        }
    }

    /**
     * Add to the multivalued {@code map} passed the pair {@code key}-{@code value}.</br>
     * The value of the pair is added as first and only element.
     *
     * @param map the multivalued map to which adding the pair
     */
    public static <K, V> void addSingleValueToMultivaluedMap(final MultivaluedMap<K, V> map, final K key, final V value) {
        if (value != null) {
            map.remove(key);
            map.add(key, value);
        }
    }

    /**
     * Automatically pages up the input collection {@code params} per blocks of {@code pageSize} size,
     * and per each block executes the code specified by the delegator {@code pageDelegator}.
     *
     * @param params   the input collection to page
     * @param pageSize the size of the page
     * @param closure  the closure that specifies the code to execute per each page
     */
    public static <P> void pageUp(final Collection<P> params, final int pageSize, final PClosure<Collection<P>> closure) {
        int fromIndex = 0;
        int toIndex = 0;
        final List<P> myParams = Lists.newArrayList(params);
        while (toIndex != myParams.size()) {
            toIndex = fromIndex + pageSize;
            if (toIndex <= 0 || toIndex > myParams.size()) {
                toIndex = myParams.size();
            }

            final List<P> pagedParams = myParams.subList(fromIndex, toIndex);
            if (!pagedParams.isEmpty()) {
                closure.call(pagedParams);
            }
            fromIndex = toIndex;
        }
    }

    /**
     * Automatically pages up one of the parameters {@code params} per blocks of {@code pageSize} size,
     * and per each block executes the code specified by the closure {@code closure}.<br>
     * Only 1 parameter of type {@link Collection} should be present.
     *
     * @param map      the input collection to page
     * @param pageSize the size of the page
     * @param closure  the closure that specifies the code to execute per each page
     */
    public static void pageUp(final Map<String, Object> map, final int pageSize, final PClosure<Map<String, Object>> closure) {
        final Entry<String, Object> paramToBePaged = paramToBePaged(map);
        if (paramToBePaged == null) {
            closure.call(map);
        } else {
            final Map<String, Object> pagedMap = new HashMap<>(map);
            final String paramToBePagedName = paramToBePaged.getKey();
            @SuppressWarnings("unchecked") final Collection<Object> paramToBePagedValues = (Collection<Object>) paramToBePaged.getValue();
            IterableUtils.pageUp(paramToBePagedValues, pageSize, pagedParamValues -> {
                try {
                    pagedMap.put(paramToBePagedName, pagedParamValues);
                    closure.call(pagedMap);
                } catch (final Exception dae) {
                    throw ExceptionBuilder.get(CellarException.class)
                            .withMessage("A problem occurred while retrieving paged param values {}.").withMessageArgs(pagedParamValues)
                            .withCause(dae).build();
                }
            });
        }
    }

    public static <T> String toInClauseParams(final Collection<T> params) {
        final StringBuilder retStr = new StringBuilder();

        for (Iterator<T> iterator = params.iterator(); iterator.hasNext(); ) {
            T param = iterator.next();
            if (param instanceof String) {
                retStr.append("'").append(param).append("'");
            } else {
                retStr.append(param);
            }
            if (iterator.hasNext()) {
                retStr.append(",");
            }
        }

        return retStr.toString();
    }

    public static String toInClauseParams(final int size) {
        final StringBuilder retStr = new StringBuilder();

        for (int i = 0; i < size; i++) {
            retStr.append("?");
            if (i < size - 1) {
                retStr.append(",");
            }
        }

        return retStr.toString();
    }

    private static Entry<String, Object> paramToBePaged(final Map<String, Object> map) {
        Entry<String, Object> retParam = null;

        for (Entry<String, Object> param : map.entrySet()) {
            if (param.getValue() instanceof Collection<?>) {
                if (retParam == null) {
                    retParam = param;
                } else {
                        LOGGER.warn("Cannot page the execution of a query that has more than 1 paramater of type collection." +
                                        " Paging only on the base of the first param list named '{}.'", retParam.getKey());
                    break;
                }
            }
        }

        return retParam;
    }

}
