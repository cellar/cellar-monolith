package eu.europa.ec.opoce.cellar.cl.controller.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.http.HttpStatus;

/**
 * Helper class to convert {@link Throwable} objects into JSON error message.
 *
 * @author EUROPEAN DYNAMICS S.A.
 */
public class ThrowableToJsonErrorConvertor {

    public String convert(HttpStatus status, Throwable cause) {
        final ObjectMapper objectMapper = new ObjectMapper();
        final ObjectNode errorNode = objectMapper.createObjectNode();
        errorNode.put("status", status.value());
        errorNode.put("error", status.getReasonPhrase());
        errorNode.put("message", cause != null ? cause.getMessage() : "");

        try {
            return objectMapper.writeValueAsString(errorNode);
        } catch (Exception e) {
            return "";
        }
    }
}
