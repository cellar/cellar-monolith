/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.common.filter
 *        FILE : IClosure.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 27-03-2014
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.filter;

/**
 * <class_description> General purpose filter.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 27-03-2014
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IFilter<T> {

    /**
     * Accept the {@link in} object if it satisfies to filter's conditions.
     * 
     * @param in the object to evaluate
     * @return true if the object satisfies the filter's conditions, false otherwise
     */
    boolean accept(final T in);

}
