/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.bootstrap
 *             FILE : CellarContextLoadereListener.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 11 13, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-11-13 08:20:08 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.bootstrap;

import com.google.common.annotations.VisibleForTesting;
import eu.europa.ec.opoce.cellar.logging.LoggerAdvice;
import org.apache.jena.JenaRuntime;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.ServletContext;

/**
 * @author ARHS Developments
 */
public class CellarContextLoaderListener extends ContextLoaderListener {

    @Override
    public WebApplicationContext initWebApplicationContext(ServletContext servletContext) {
        LoggerAdvice.init();
        setJenaRuntime();
        return super.initWebApplicationContext(servletContext);
    }

    @VisibleForTesting
    public static void setJenaRuntime() {
        // CELLAR clients are not ready to received RDF 1.1 model so
        // we need to stick to RDF 1.0
        JenaRuntime.isRDF11 = false;
    }
}
