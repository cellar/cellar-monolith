/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.common.closure
 *        FILE : RClosure.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 20-03-2013
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate: 2016-03-09 13:57:52 +0100 (Wed, 09 Mar 2016) $
 *     VERSION : $LastChangedRevision: 10409 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.closure;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

/**
 * <class_description> General purpose closure that accepts no parameter and returns an object.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 20-03-2013
 *
 * @author ARHS Developments
 * @version $Revision: 10409 $
 */
public interface RClosure<R> {

    /**
     * Calls the closure.
     *
     * @return the result of the closure
     * @throws ClosureException should the call to the closure fail
     */
    default R call() throws ClosureException {
        try {
            return this.doCall();
        } catch (Exception e) {
            throw ExceptionBuilder.get(ClosureException.class).withCode(CommonErrors.CLOSURE_FAILED)
                    .withCause(e).visibleOnStackTrace(false).build();
        }
    }

    /**
     * Performs the actual call to the closure.
     *
     * @return the result of the closure
     * @throws Exception should the call to the closure fail
     */
    R doCall() throws Exception;

}
