/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ccr.service
 *             FILE : LanguagesSkosService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 2, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$$
 *          VERSION : $LastChangedRevision$$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.service;

import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 2, 2015
 *
 * @author ARHS Developments
 * @version $Revision$$
 */
public interface LanguagesNalSkosLoaderService {

    /**
     * Gets the language bean by uri.
     *
     * @param languageUri the language uri
     * @return the language bean
     */
    LanguageBean getLanguageBeanByUri(String languageUri);

    /**
     * Gets the language bean by three char code.
     *
     * @param threeCharCode the three char code
     * @return the language bean by three char code
     */
    LanguageBean getLanguageBeanByThreeCharCode(String threeCharCode);

    /**
     * Gets the language bean by two char code.
     *
     * @param twoCharCode the two char code
     * @return the language bean by two char code
     */
    LanguageBean getLanguageBeanByTwoCharCode(String twoCharCode);

    /**
     * Check if the languages were loaded correctly.
     * Used by cellar's splahs screen
     * @return true, if the in memory maps contain elements
     */
    boolean areAllItemsLoaded();

    /**
     * Number of language descriptions loaded.
     *
     * @return the int
     */
    int numberOfItemDescriptionsLoaded();
}
