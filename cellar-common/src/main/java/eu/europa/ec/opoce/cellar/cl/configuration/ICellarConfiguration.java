/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.configuration.impl
 *        FILE : ICellarConfiguration.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 02-04-2012
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.configuration;

import com.google.common.collect.ImmutableMap;
import eu.europa.ec.opoce.cellar.cl.configuration.ConfigurationPropertyService.EXISTINGPROPS_MODE;
import eu.europa.ec.opoce.cellar.cl.configuration.enums.*;
import eu.europa.ec.opoce.cellar.cl.domain.enums.ProcessMonitorPhase;
import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService.Priority;

/**
 * <class_description> Definition of Cellar configuration bean.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 02-04-2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface ICellarConfiguration extends IConfiguration {

    /**
     * Gets the cellar api base url.
     *
     * @return the cellar api base url
     */
    String getCellarApiBaseUrl();

    /**
     * Gets the cellar api connection timeout.
     *
     * @return the cellar api connection timeout
     */
    int getCellarApiConnectionTimeout();

    /**
     * Gets the cellar api host header.
     *
     * @return the cellar api host header
     */
    String getCellarApiHostHeader();

    /**
     * Gets the cellar api read timeout.
     *
     * @return the cellar api read timeout
     */
    int getCellarApiReadTimeout();

    /**
     * Gets the cellar configuration version.
     *
     * @return the cellar configuration version
     */
    String getCellarConfigurationVersion();

    /**
     * Gets the cellar database in clause params.
     *
     * @return the cellar database in clause params
     */
    int getCellarDatabaseInClauseParams();

    /**
     * Gets the cellar database rdf password.
     *
     * @return the cellar database rdf password
     */
    String getCellarDatabaseRdfPassword();

    /**
     * Check if the cellar database rdf ingestion optimization is enabled.
     *
     * @return true if the cellar database rdf ingestion optimization is enabled.
     */
    boolean isCellarDatabaseRdfIngestionOptimizationEnabled();

    /**
     * Gets the cellar datasource cellar.
     *
     * @return the cellar datasource cellar
     */
    String getCellarDatasourceCellar();

    /**
     * Gets the cellar datasource cmr.
     *
     * @return the cellar datasource cmr
     */
    String getCellarDatasourceCmr();

    /**
     * Gets the cellar datasource virtuoso.
     *
     * @return the cellar datasource virtuoso
     */
    String getCellarDatasourceVirtuoso();

    /**
     * Gets the cellar datasource idol.
     *
     * @return the cellar datasource idol
     */
    String getCellarDatasourceIdol();

    /**
     * Gets the cellar encryptor algorithm.
     *
     * @return the cellar encryptor algorithm
     */
    String getCellarEncryptorAlgorithm();

    /**
     * Gets the cellar encryptor password.
     *
     * @return the cellar encryptor password
     */
    String getCellarEncryptorPassword();

    /**
     * Gets the cellar folder batch export.
     *
     * @return the cellar folder batch export
     */
    String getCellarFolderExportBatchJob();

    /**
     * Gets the cellar folder temporary batch export.
     *
     * @return the cellar folder temporary batch export
     */
    String getCellarFolderExportBatchJobTemporary();

    /**
     * Gets the cellar folder export update.
     *
     * @return the cellar folder export update
     */
    String getCellarFolderExportUpdateResponse();

    /**
     * Gets the cellar folder used by REST export.
     *
     * @return the cellar REST export folder
     */
    String getCellarFolderExportRest();

    /**
     * Gets the cellar folder backup.
     *
     * @return the cellar folder backup
     */
    String getCellarFolderBackup();

    /**
     * Gets the cellar folder batch job sparql queries.
     *
     * @return the cellar folder batch job sparql queries
     */
    String getCellarFolderBatchJobSparqlQueries();

    /**
     * Gets the cellar folder sparql response templates.
     *
     * @return the cellar foldersparql response templates
     */
    String getCellarFolderSparqlResponseTemplates();

    /**
     * Gets the cellar folder authenticOJ reception.
     *
     * @return the cellar folder authenticOJ reception
     */
    String getCellarFolderAuthenticOJReception();

    /**
     * Gets the cellar folder authenticOJ response.
     *
     * @return the cellar folder authenticOJ response
     */
    String getCellarFolderAuthenticOJResponse();

    /**
     * Gets the cellar folder bulk reception.
     *
     * @return the cellar folder bulk reception
     */
    String getCellarFolderBulkReception();

    /**
     * Gets the cellar folder bulk response.
     *
     * @return the cellar folder bulk response
     */
    String getCellarFolderBulkResponse();
    
    /**
     * Gets the cellar folder low priority bulk reception.
     * 
     * @return the cellar folder low priority bulk reception
     */
    String getCellarFolderBulkLowPriorityReception();
    
    /**
     * Gets the cellar folder low priority bulk response.
     * 
     * @return the cellar folder low priority bulk response
     */
    String getCellarFolderBulkLowPriorityResponse();
    
    /**
     * Gets the cellar folder cmr log.
     *
     * @return the cellar folder cmr log
     */
    String getCellarFolderCmrLog();

    /**
     * Gets the cellar folder daily reception.
     *
     * @return the cellar folder daily reception
     */
    String getCellarFolderDailyReception();

    /**
     * Gets the cellar folder daily response.
     *
     * @return the cellar folder daily response
     */
    String getCellarFolderDailyResponse();

    /**
     * Gets the cellar folder error.
     *
     * @return the cellar folder error
     */
    String getCellarFolderError();

    /**
     * Gets the cellar folder foxml.
     *
     * @return the cellar folder foxml
     */
    String getCellarFolderFoxml();

    /**
     * Gets the cellar folder license holder archive adhoc extraction.
     *
     * @return the cellar folder license holder archive adhoc extraction
     */
    String getCellarFolderLicenseHolderArchiveAdhocExtraction();

    /**
     * Gets the cellar folder license holder archive extraction.
     *
     * @return the cellar folder license holder archive extraction
     */
    String getCellarFolderLicenseHolderArchiveExtraction();

    /**
     * Gets the cellar folder license holder temporary archive.
     *
     * @return the cellar folder license holder temporary archive
     */
    String getCellarFolderLicenseHolderTemporaryArchive();

    /**
     * Gets the cellar folder license holder temporary sparql.
     *
     * @return the cellar folder license holder temporary sparql
     */
    String getCellarFolderLicenseHolderTemporarySparql();

    /**
     * Gets the cellar folder lock.
     *
     * @return the cellar folder lock
     */
    String getCellarFolderLock();

    /**
     * Gets the cellar folder root.
     *
     * @return the cellar folder root
     */
    String getCellarFolderRoot();

    /**
     * Gets the cellar folder temporary dissemination.
     *
     * @return the cellar folder temporary dissemination
     */
    String getCellarFolderTemporaryDissemination();

    /**
     * Gets the cellar folder temporary store.
     *
     * @return the cellar folder temporary store
     */
    String getCellarFolderTemporaryStore();

    /**
     * Gets the cellar folder temporary work.
     *
     * @return the cellar folder temporary work
     */
    String getCellarFolderTemporaryWork();

    /**
     * Gets the cellar instance id.
     *
     * @return the cellar instance id
     */
    String getCellarInstanceId();

    /**
     * Gets the cellar server base url.
     *
     * @return the cellar server base url
     */
    String getCellarServerBaseUrl();

    /**
     * Gets the cellar server context.
     *
     * @return the cellar server context
     */
    String getCellarServerContext();

    /**
     * Gets the cellar server name.
     *
     * @return the cellar server name
     */
    String getCellarServerName();

    /**
     * Gets the cellar server port.
     *
     * @return the cellar server port
     */
    String getCellarServerPort();

    /**
     * Gets the cellar service batch job processing cron settings.
     *
     * @return the cellar service batch job processing cron settings
     */
    String getCellarServiceBatchJobProcessingCronSettings();

    /**
     * Gets the cellar service batch job sparql update max cron.
     *
     * @return the cellar service batch job sparql update max cron
     */
    String getCellarServiceBatchJobSparqlUpdateMaxCron();

    /**
     * Gets the cellar service batch job sparql cron settings.
     *
     * @return the cellar service batch job sparql cron settings
     */
    String getCellarServiceBatchJobSparqlCronSettings();

    /**
     * Gets the cellar service dissemination content stream max size.
     *
     * @return the cellar service dissemination content stream max size
     */
    long getCellarServiceDisseminationContentStreamMaxSize();

    /**
     * Gets the cellar service dissemination daily oj cache refresh minutes.
     *
     * @return the cellar service dissemination daily oj cache refresh minutes
     */
    Integer getCellarServiceDisseminationDailyOjCacheRefreshMinutes();

    /**
     * Gets the cellar service dissemination decoding default.
     *
     * @return the cellar service dissemination decoding default
     */
    String getCellarServiceDisseminationDecodingDefault();

    /**
     * Gets the cellar service dissemination memento enabled status.
     *
     * @return a boolean indicating whether the service dissemination memento is enabled (true) or not (false)
     */
    boolean isCellarServiceDisseminationMementoEnabled();

    /**
     * Sets the cellar service dissemination memento enabled status.
     *
     * @param cellarServiceDisseminationMementoEnabled the cellar service dissemination memento enabled
     * @param syncMode                                 the sync mode
     */
    void setCellarServiceDisseminationMementoEnabled(final boolean cellarServiceDisseminationMementoEnabled,
                                                     final EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the cellar service external startup timeout seconds.
     *
     * @return the cellar service external startup timeout seconds
     */
    int getCellarServiceExternalStartupTimeoutSeconds();

    /**
     * Gets the cellar service indexing cron settings.
     *
     * @return the cellar service indexing cron settings
     */
    String getCellarServiceIndexingCronSettings();

    /**
     * Gets the cellar service Synchronizing cron settings.
     *
     * @return the cellar service Synchronizing cron settings
     */
    String getCellarServiceSynchronizingCronSettings();

    /**
     * Gets the cellar service indexing minimum age minutes.
     *
     * @return the cellar service indexing minimum age minutes
     */
    int getCellarServiceIndexingMinimumAgeMinutes();

    /**
     * Gets the cellar service indexing minimum priority handled.
     *
     * @return the cellar service indexing minimum priority
     */
    Priority getCellarServiceIndexingMinimumPriority();

    /**
     * Gets the cellar service indexing cleaner enabled.
     *
     * @return the cellar service indexing cleaner enabled
     */
    boolean isCellarServiceIndexingCleanerEnabled();

    /**
     * Gets the cellar service indexing cleaner cron settings.
     *
     * @return the cellar service indexing cleaner cron settings
     */
    String getCellarServiceIndexingCleanerCronSettings();

    /**
     * Gets the cellar service indexing cleaner minimum age (in days) for requests marked as Done.
     *
     * @return the cellar service indexing cleaner minimum age (in days) for requests marked as Done
     */
    int getCellarServiceIndexingCleanerDoneMinimumAgeDays();

    /**
     * Gets the cellar service indexing cleaner minimum age (in days) for requests marked as Redundant.
     *
     * @return the cellar service indexing cleaner minimum age (in days) for requests marked as Redundant
     */
    int getCellarServiceIndexingCleanerRedundantMinimumAgeDays();

    /**
     * Gets the cellar service indexing cleaner minimum age (in days) for requests marked as Error.
     *
     * @return the cellar service indexing cleaner minimum age (in days) for requests marked as Error
     */
    int getCellarServiceIndexingCleanerErrorMinimumAgeDays();

    /**
     * Gets the cellar service indexing pool threads.
     *
     * @return the cellar service indexing pool threads
     */
    int getCellarServiceIndexingPoolThreads();

    /**
     * Gets the cellar service indexing query limit results.
     *
     * @return the cellar service indexing query limit results
     */
    int getCellarServiceIndexingQueryLimitResults();

    /**
     * Gets the cellar service ingestion cmr lock mode.
     *
     * @return the cellar service ingestion cmr lock mode
     */
    CellarServiceIngestionCmrLockMode getCellarServiceIngestionCmrLockMode();

    /**
     * Gets the cellar service ingestion cmr lock ceiling delay.
     *
     * @return the cellar service ingestion cmr lock ceiling delay
     */
    long getCellarServiceIngestionCmrLockCeilingDelay();

    /**
     * Gets the cellar service ingestion cmr lock max retries.
     *
     * @return the cellar service ingestion cmr lock max retries
     */
    int getCellarServiceIngestionCmrLockMaxRetries();

    /**
     * Gets the cellar service ingestion cmr lock base.
     *
     * @return the cellar service ingestion cmr lock base
     */
    int getCellarServiceIngestionCmrLockBase();

    /**
     * Gets the cellar service ingestion concurrency controller autologging cron settings.
     *
     * @return the cellar service ingestion concurrency controller autologging cron settings
     */
    String getCellarServiceIngestionConcurrencyControllerAutologgingCronSettings();
    
    /**
     * Gets the cellar service ingestion concurrency controller thread delay in milliseconds.
     *
     * @return the cellar service ingestion concurrency controller thread delay in milliseconds
     */
    long getCellarServiceIngestionConcurrencyControllerThreadDelayMilliseconds();

    /**
     * Gets the cellar service ingestion execution rate.
     *
     * @return the cellar service ingestion execution rate
     */
    String getCellarServiceIngestionExecutionRate();

    /**
     * Checks if cellar service ingestion validation metadata is enabled.
     *
     * @return true, if cellar service ingestion validation metadata is enabled
     */
    boolean isCellarServiceIngestionValidationMetadataEnabled();

    boolean getCellarServiceIngestionValidationItemExclusionEnabled();
    
    /**
     * Gets the cellar status service shacl report level
     *
     * @return the cellar status service shacl report level
     */
    CellarStatusServiceShaclReportLevel getCellarStatusServiceShaclReportLevel();

    /**
     * Gets the cellar service ingestion pickup mode.
     *
     * @return the cellar service ingestion pickup mode
     */
    CellarServiceIngestionPickupMode getCellarServiceIngestionPickupMode();

    /**
     * Gets the cellar service ingestion pool core pool size.
     *
     * @return the cellar service ingestion pool core pool size
     */
    int getCellarServiceIngestionPoolThreads();

    /**
     * Gets the cellar service ingestion sip copy retry pause seconds.
     *
     * @return the cellar service ingestion sip copy retry pause seconds
     */
    int getCellarServiceIngestionSipCopyRetryPauseSeconds();

    /**
     * Gets the cellar service ingestion sip copy timeout seconds.
     *
     * @return the cellar service ingestion sip copy timeout seconds
     */
    int getCellarServiceIngestionSipCopyTimeoutSeconds();

    /**
     * Gets the cellar service inverse notices limit.
     *
     * @return the cellar service inverse notices limit
     */
    int getCellarServiceInverseNoticesLimit();

    /**
     * Gets the cellar service license holder batch size folder.
     *
     * @return the cellar service license holder batch size folder
     */
    int getCellarServiceLicenseHolderBatchSizeFolder();

    /**
     * Gets the cellar service license holder batch size per thread.
     *
     * @return the cellar service license holder batch size per thread
     */
    int getCellarServiceLicenseHolderBatchSizePerThread();

    /**
     * Gets the cellar service license holder cleaner cron settings.
     *
     * @return the cellar service license holder cleaner cron settings
     */
    String getCellarServiceLicenseHolderCleanerCronSettings();

    /**
     * Gets the cellar service license holder kept days daily.
     *
     * @return the cellar service license holder kept days daily
     */
    int getCellarServiceLicenseHolderKeptDaysDaily();

    /**
     * Gets the cellar service license holder kept days monthly.
     *
     * @return the cellar service license holder kept days monthly
     */
    int getCellarServiceLicenseHolderKeptDaysMonthly();

    /**
     * Gets the cellar service license holder kept days weekly.
     *
     * @return the cellar service license holder kept days weekly
     */
    int getCellarServiceLicenseHolderKeptDaysWeekly();

    /**
     * Gets the cellar service license holder scheduler cron settings.
     *
     * @return the cellar service license holder scheduler cron settings
     */
    String getCellarServiceLicenseHolderSchedulerCronSettings();

    /**
     * Gets the cellar service license holder worker archiving executor core pool size.
     *
     * @return the cellar service license holder worker archiving executor core pool size
     */
    int getCellarServiceLicenseHolderWorkerArchivingExecutorCorePoolSize();

    /**
     * Gets the cellar service license holder worker archiving executor max pool size.
     *
     * @return the cellar service license holder worker archiving executor max pool size
     */
    int getCellarServiceLicenseHolderWorkerArchivingExecutorMaxPoolSize();

    /**
     * Gets the cellar service license holder worker archiving executor queue capacity.
     *
     * @return the cellar service license holder worker archiving executor queue capacity
     */
    int getCellarServiceLicenseHolderWorkerArchivingExecutorQueueCapacity();

    /**
     * Gets the cellar service license holder worker cron settings.
     *
     * @return the cellar service license holder worker cron settings
     */
    String getCellarServiceLicenseHolderWorkerCronSettings();

    /**
     * Gets the cellar service license holder worker task executor core pool size.
     *
     * @return the cellar service license holder worker task executor core pool size
     */
    int getCellarServiceLicenseHolderWorkerTaskExecutorCorePoolSize();

    /**
     * Gets the cellar service license holder worker task executor max pool size.
     *
     * @return the cellar service license holder worker task executor max pool size
     */
    int getCellarServiceLicenseHolderWorkerTaskExecutorMaxPoolSize();

    /**
     * Gets the cellar service license holder worker task executor queue capacity.
     *
     * @return the cellar service license holder worker task executor queue capacity
     */
    int getCellarServiceLicenseHolderWorkerTaskExecutorQueueCapacity();

    /**
     * Gets the cellar service nal load cron settings.
     *
     * @return the cellar service nal load cron settings
     */
    String getCellarServiceNalLoadCronSettings();

    /**
     * Gets the cellar service notification items per page.
     *
     * @return the cellar service notification items per page
     */
    int getCellarServiceNotificationItemsPerPage();

    /**
     * Gets the cellar service rdf store cleaner executor core pool size.
     *
     * @return the cellar service rdf store cleaner executor core pool size
     */
    int getCellarServiceRDFStoreCleanerExecutorCorePoolSize();

    /**
     * Gets the cellar service rdf store cleaner executor max pool size.
     *
     * @return the cellar service rdf store cleaner executor max pool size
     */
    int getCellarServiceRDFStoreCleanerExecutorMaxPoolSize();

    /**
     * Gets the cellar service rdf store cleaner executor queue capacity.
     *
     * @return the cellar service rdf store cleaner executor queue capacity
     */
    int getCellarServiceRDFStoreCleanerExecutorQueueCapacity();

    /**
     * Gets the cellar service rdf store cleaner hierarchies batch size.
     *
     * @return the cellar service rdf store cleaner hierarchies batch size
     */
    int getCellarServiceRDFStoreCleanerHierarchiesBatchSize();

    /**
     * Gets the cellar service rdf store cleaner mode.
     *
     * @return the cellar service rdf store cleaner mode
     */
    CellarServiceRDFStoreCleanerMode getCellarServiceRDFStoreCleanerMode();

    /**
     * Gets the cellar service rdf store cleaner resources batch size.
     *
     * @return the cellar service rdf store cleaner resources batch size
     */
    int getCellarServiceRDFStoreCleanerResourcesBatchSize();

    /**
    * Gets the cellar service SIP dependency checker cron settings.
    * 
    * @return the cellar service SIP dependency checker cron settings
    */
    String getCellarServiceSipDependencyCheckerCronSettings();
    
    /**
     * Gets the cellar service SIP dependency checker thread pool settings.
     * 
     * @return the cellar service SIP dependency checker thread pool settings
     */
    int getCellarServiceSipDependencyCheckerPoolThreads();
    
    /**
     * Gets the cellar service SIP queue manager cron settings.
     * 
     * @return the cellar service SIP queue manager cron settings
     */
     String getCellarServiceSipQueueManagerCronSettings();
    
    /**
     * Gets the cellar uri dissemination base.
     *
     * @return the cellar uri dissemination base
     */
    String getCellarUriDisseminationBase();

    /**
     * Gets the cellar uri resource base.
     *
     * @return the cellar uri resource base
     */
    String getCellarUriResourceBase();

    int getCellarServiceSparqlMaxConnections();

    void setCellarServiceSparqlMaxConnections(int cellarServiceSparqlMaxConnections, EXISTINGPROPS_MODE... syncMode);

    int getCellarServiceSparqlTimeoutConnections();

    /**
     * Gets the cellar uri sparql service.
     *
     * @return the cellar uri sparql service
     */
    String getCellarServiceSparqlUri();

    /**
     * Gets the cellar uri tranformation resource base.
     *
     * @return the cellar uri tranformation resource base
     */
    String getCellarUriTranformationResourceBase();

    /**
     * Gets the configuration path.
     *
     * @return the configuration path
     */
    String getConfigurationPath();

    /**
     * Gets the property.
     *
     * @param propertyKey the property key
     * @return the property
     */
    String getProperty(final CellarConfigurationPropertyKey propertyKey);

    /**
     * Gets the test database cron settings.
     *
     * @return the test database cron settings
     */
    String getTestDatabaseCronSettings();

    /**
     * Checks if cellar configuration database has the priority.
     *
     * @return true, if cellar configuration database has the priority
     */
    boolean isCellarConfigurationDatabaseHasPriority();

    /**
     * Checks if cellar database audit is enabled.
     *
     * @return true, if cellar database audit is enabled
     */
    boolean isCellarDatabaseAuditEnabled();

    /**
     * Checks if cellar database is read only.
     *
     * @return true, if cellar database is read only
     */
    boolean isCellarDatabaseReadOnly();

    /**
     * Checks if cellar service automatic disembargo is enabled.
     *
     * @return true, if cellar service automatic disembargo is enabled
     */
    boolean isCellarServiceAutomaticDisembargoEnabled();

    int getCellarServiceDisembargoInitialDelay();

    /**
     * Checks if cellar service backup processed file is enabled.
     *
     * @return true, if cellar service backup processed file is enabled
     */
    boolean isCellarServiceBackupProcessedFileEnabled();

    /**
     * Checks if cellar service batch job is enabled.
     *
     * @return true, if cellar service batch job is enabled
     */
    boolean isCellarServiceBatchJobEnabled();

    /**
     * Checks if cellar service delete processed data is enabled.
     *
     * @return true, if cellar service delete processed data is enabled
     */
    boolean isCellarServiceDeleteProcessedDataEnabled();

    /**
     * Checks if cellar service dissemination database optimization is enabled.
     *
     * @return true, if cellar service dissemination database optimization is enabled
     */
    boolean isCellarServiceDisseminationDatabaseOptimizationEnabled();

    /**
     * Checks if cellar service dissemination in notice properties only is enabled.
     *
     * @return true, if cellar service dissemination in notice properties only is enabled
     */
    boolean isCellarServiceDisseminationInNoticePropertiesOnlyEnabled();

    /**
     * Checks if cellar service dissemination notices sort is enabled.
     *
     * @return true, if cellar service dissemination notices sort is enabled
     */
    boolean isCellarServiceDisseminationNoticesSortEnabled();

    /**
     * Checks if is cellar service dissemination retrieve embargoed resource enabled.
     *
     * @return true, if is cellar service dissemination retrieve embargoed resource enabled
     */
    boolean isCellarServiceDisseminationRetrieveEmbargoedResourceEnabled();

    /**
     * Sets the cellar service dissemination embargo content visible.
     *
     * @param cellarServiceDisseminationRetrieveEmbargoedResourceEnabled the cellar service dissemination retrieve embargoed resource enabled
     * @param syncMode                                                   the sync mode
     */
    void setCellarServiceDisseminationRetrieveEmbargoedResourceEnabled(boolean cellarServiceDisseminationRetrieveEmbargoedResourceEnabled,
                                                                       EXISTINGPROPS_MODE... syncMode);

    /**
     * Checks if cellar service indexing concurrency controller is enabled.
     *
     * @return true, if cellar service indexing concurrency controller is enabled
     */
    boolean isCellarServiceIndexingConcurrencyControllerEnabled();

    /**
     * Checks if cellar service indexing is enabled.
     *
     * @return true, if cellar service indexing is enabled
     */
    boolean isCellarServiceIndexingEnabled();

    /**
     * Checks if cellar service ingestion check write once properties is enabled.
     *
     * @return true, if cellar service ingestion check write once properties is enabled
     */
    boolean isCellarServiceIngestionCheckWriteOncePropertiesEnabled();

    /**
     * Checks if cellar service ingestion concurrency controller autologging is enabled.
     *
     * @return true, if cellar service ingestion concurrency controller autologging is enabled
     */
    boolean isCellarServiceIngestionConcurrencyControllerAutologgingEnabled();

    /**
     * Checks if cellar service ingestion is enabled.
     *
     * @return true, if cellar service ingestion is enabled
     */
    boolean isCellarServiceIngestionEnabled();

    /**
     * Checks if cellar service inverse notices agent is enabled.
     *
     * @return true, if cellar service inverse notices agent is enabled
     */
    boolean isCellarServiceInverseNoticesAgentEnabled();

    /**
     * Checks if cellar service inverse notices top level event is enabled.
     *
     * @return true, if cellar service inverse notices top level event is enabled
     */
    boolean isCellarServiceInverseNoticesTopLevelEventEnabled();

    /**
     * Checks if cellar service inverse notices dossier is enabled.
     *
     * @return true, if cellar service inverse notices dossier is enabled
     */
    boolean isCellarServiceInverseNoticesDossierEnabled();

    /**
     * Checks if cellar service inverse notices event is enabled.
     *
     * @return true, if cellar service inverse notices event is enabled
     */
    boolean isCellarServiceInverseNoticesEventEnabled();

    /**
     * Checks if cellar service inverse notices expression is enabled.
     *
     * @return true, if cellar service inverse notices expression is enabled
     */
    boolean isCellarServiceInverseNoticesExpressionEnabled();

    /**
     * Checks if cellar service inverse notices manifestation is enabled.
     *
     * @return true, if cellar service inverse notices manifestation is enabled
     */
    boolean isCellarServiceInverseNoticesManifestationEnabled();

    /**
     * Checks if cellar service inverse notices work is enabled.
     *
     * @return true, if cellar service inverse notices work is enabled
     */
    boolean isCellarServiceInverseNoticesWorkEnabled();

    /**
     * Checks if cellar service license holder is enabled.
     *
     * @return true, if cellar service license holder is enabled
     */
    boolean isCellarServiceLicenseHolderEnabled();

    /**
     * Sets the cellar service nal load decoding validation enabled.
     *
     * @param cellarServiceNalLoadDecodingValidationEnabled the cellar service nal load decoding validation enabled
     * @param syncMode                                      the sync mode
     */
    void setCellarServiceNalLoadDecodingValidationEnabled(final boolean cellarServiceNalLoadDecodingValidationEnabled,
                                                          final EXISTINGPROPS_MODE... syncMode);

    /**
     * Checks if is cellar service nal load decoding validation enabled.
     *
     * @return true, if is cellar service nal load decoding validation enabled
     */
    boolean isCellarServiceNalLoadDecodingValidationEnabled();

    /**
     * Checks if cellar service nal load is enabled.
     *
     * @return true, if cellar service nal load is enabled
     */
    boolean isCellarServiceNalLoadEnabled();

    /**
     * Checks if cellar service onto load is enabled.
     *
     * @return true, if cellar service onto load is enabled
     */
    boolean isCellarServiceOntoLoadEnabled();

    boolean isCellarServiceSparqlLoadEnabled();

    String getCellarServiceSparqlLoadCronSettings();

    /**
     * Checks if cellar service rdf store cleaner is enabled.
     *
     * @return true, if cellar service rdf store cleaner is enabled
     */
    boolean isCellarServiceRDFStoreCleanerEnabled();

    /**
     * Checks if test is enabled.
     *
     * @return true, if test is enabled
     */
    boolean isTestEnabled();

    /**
     * Checks if test rollback is enabled.
     *
     * @return true, if test rollback is enabled
     */
    boolean isTestRollbackEnabled();

    /**
     * Checks if is virtuoso test rollback enabled.
     *
     * @return true, if is virtuoso test rollback enabled
     */
    boolean isTestVirtuosoRollbackEnabled();

    /**
     * Checks if the indexing execution delay is enabled.
     *
     * @return true, if the indexing execution delay is enabled.
     */
    boolean isTestIndexingDelayExecutionEnabled();
    
    /**
     * Checks if the Virtuoso Retry is enabled for testing.
     *
     * @return true, if the Virtuoso Retry is enabled for testing.
     */
    boolean isTestVirtuosoRetryEnabled();

    /**
     * Checks if the S3 Retry is enabled for testing.
     *
     * @return true, if the S3 Retry is enabled for testing.
     */
    boolean isTestS3RetryEnabled();
    
    /**
     * Gets the process phase to monitor during testing.
     *
     * @return {@link ProcessMonitorPhase} to monitor during testing
     */
    ProcessMonitorPhase getTestProcessMonitorPhase();
    
    /**
     * Gets if the process phase to monitor is reached during testing.
     *
     * @return true, if the process phase to monitor is reached during testing
     */
    boolean isTestProcessMonitorPhaseReached();
    
    /**
     * Sets the cellar api base url.
     *
     * @param cellarApiBaseUrl the cellar api base url
     * @param syncMode         the sync mode
     */
    void setCellarApiBaseUrl(String cellarApiBaseUrl, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar api connection timeout.
     *
     * @param cellarApiConnectionTimeout the cellar api connection timeout
     * @param syncMode                   the sync mode
     */
    void setCellarApiConnectionTimeout(int cellarApiConnectionTimeout, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar api host header.
     *
     * @param cellarApiHostHeader the cellar api host header
     * @param syncMode            the sync mode
     */
    void setCellarApiHostHeader(String cellarApiHostHeader, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar api read timeout.
     *
     * @param cellarApiReadTimeout the cellar api read timeout
     * @param syncMode             the sync mode
     */
    void setCellarApiReadTimeout(int cellarApiReadTimeout, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar configuration database has priority.
     *
     * @param cellarConfigurationDatabaseHasPriority the new cellar configuration database has priority
     */
    void setCellarConfigurationDatabaseHasPriority(boolean cellarConfigurationDatabaseHasPriority);

    /**
     * Sets the cellar configuration version.
     *
     * @param cellarConfigurationVersion the cellar configuration version
     * @param syncMode                   the sync mode
     */
    void setCellarConfigurationVersion(String cellarConfigurationVersion, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar database audit enabled.
     *
     * @param cellarDatabaseAuditEnabled the cellar database audit enabled
     * @param syncMode                   the sync mode
     */
    void setCellarDatabaseAuditEnabled(boolean cellarDatabaseAuditEnabled, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar database in clause params.
     *
     * @param cellarDatabaseInClauseParams the cellar database in clause params
     * @param syncMode                     the sync mode
     */
    void setCellarDatabaseInClauseParams(int cellarDatabaseInClauseParams, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar database rdf password.
     *
     * @param cellarDatabaseRdfPassword the cellar database rdf password
     * @param syncMode                  the sync mode
     */
    void setCellarDatabaseRdfPassword(String cellarDatabaseRdfPassword, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar database rdf ingestion optimization enabled.
     *
     * @param cellarDatabaseRdfIngestionOptimizationEnabled if true, the cellar database rdf ingestion optimization is set to enabled
     * @param syncMode                                      the sync mode
     */
    void setCellarDatabaseRdfIngestionOptimizationEnabled(final boolean cellarDatabaseRdfIngestionOptimizationEnabled,
                                                          final EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar database read only.
     *
     * @param cellarDatabaseReadOnly the cellar database read only
     * @param syncMode               the sync mode
     */
    void setCellarDatabaseReadOnly(boolean cellarDatabaseReadOnly, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar datasource cellar.
     *
     * @param cellarDatasourceCellar the cellar datasource cellar
     * @param syncMode               the sync mode
     */
    void setCellarDatasourceCellar(String cellarDatasourceCellar, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar datasource cmr.
     *
     * @param cellarDatasourceCmr the cellar datasource cmr
     * @param syncMode            the sync mode
     */
    void setCellarDatasourceCmr(String cellarDatasourceCmr, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar datasource virtuoso.
     *
     * @param cellarDatasourceVirtuoso the cellar datasource virtuoso
     * @param syncMode                 the sync mode
     */
    void setCellarDatasourceVirtuoso(String cellarDatasourceVirtuoso, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar datasource idol.
     *
     * @param cellarDatasourceIdol the cellar datasource idol
     * @param syncMode             the sync mode
     */
    void setCellarDatasourceIdol(String cellarDatasourceIdol, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar encryptor algorithm.
     *
     * @param cellarEncryptorAlgorithm the cellar encryptor algorithm
     * @param syncMode                 the sync mode
     */
    void setCellarEncryptorAlgorithm(String cellarEncryptorAlgorithm, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar encryptor password.
     *
     * @param cellarEncryptorPassword the cellar encryptor password
     * @param syncMode                the sync mode
     */
    void setCellarEncryptorPassword(String cellarEncryptorPassword, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar folder batch export.
     *
     * @param cellarFolderExportBatchJob the cellar folder batch export
     * @param syncMode                   the sync mode
     */
    void setCellarFolderExportBatchJob(final String cellarFolderExportBatchJob, final EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar temporary folder batch export.
     *
     * @param cellarFolderExportBatchJobTemporary the cellar temporary folder batch export
     * @param syncMode                            the sync mode
     */
    void setCellarFolderExportBatchJobTemporary(final String cellarFolderExportBatchJobTemporary, final EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar REST export folder.
     *
     * @param cellarFolderExportRest the cellar REST export folder
     * @param syncMode               the sync mode
     */
    void setCellarFolderExportRest(final String cellarFolderExportRest, final EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar folder export update.
     *
     * @param cellarFolderExportUpdateResponse the cellar folder export update
     * @param syncMode                         the sync mode
     */
    void setCellarFolderExportUpdateResponse(final String cellarFolderExportUpdateResponse, final EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar folder backup.
     *
     * @param cellarFolderBackup the cellar folder backup
     * @param syncMode           the sync mode
     */
    void setCellarFolderBackup(String cellarFolderBackup, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar folder batch job sparql queries.
     *
     * @param cellarFolderBatchJobSparqlQueries the cellar folder batch job sparql queries
     * @param syncMode                          the sync mode
     */
    void setCellarFolderBatchJobSparqlQueries(String cellarFolderBatchJobSparqlQueries, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar folder sparql response templates.
     *
     * @param cellarFolderSparqlResponseTemplates the cellar folder sparql response templates
     * @param syncMode                            the sync mode
     */
    void setCellarFolderSparqlResponseTemplates(String cellarFolderSparqlResponseTemplates, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar folder authenticOJ reception.
     *
     * @param cellarFolderAuthenticOJReception the cellar folder authenticOJ reception
     * @param syncMode                         the sync mode
     */
    void setCellarFolderAuthenticOJReception(String cellarFolderAuthenticOJReception, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar folder authenticOJ response.
     *
     * @param cellarFolderAuthenticOJResponse the cellar folder authenticOJ response
     * @param syncMode                        the sync mode
     */
    void setCellarFolderAuthenticOJResponse(String cellarFolderAuthenticOJResponse, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar folder bulk reception.
     *
     * @param cellarFolderBulkReception the cellar folder bulk reception
     * @param syncMode                  the sync mode
     */
    void setCellarFolderBulkReception(String cellarFolderBulkReception, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar folder bulk response.
     *
     * @param cellarFolderBulkResponse the cellar folder bulk response
     * @param syncMode                 the sync mode
     */
    void setCellarFolderBulkResponse(String cellarFolderBulkResponse, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar folder low priority bulk reception.
     *
     * @param cellarFolderBulkLowPriorityReception the cellar folder low priority bulk reception
     * @param syncMode                  the sync mode
     */
    void setCellarFolderBulkLowPriorityReception(String cellarFolderBulkLowPriorityReception, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar folder low priority bulk response.
     *
     * @param cellarFolderBulkLowPriorityResponse the cellar folder low priority bulk response
     * @param syncMode                 the sync mode
     */
    void setCellarFolderBulkLowPriorityResponse(String cellarFolderBulkLowPriorityResponse, EXISTINGPROPS_MODE... syncMode);
    
    /**
     * Sets the cellar folder cmr log.
     *
     * @param cellarFolderCmrLog the cellar folder cmr log
     * @param syncMode           the sync mode
     */
    void setCellarFolderCmrLog(String cellarFolderCmrLog, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar folder daily reception.
     *
     * @param cellarFolderDailyReception the cellar folder daily reception
     * @param syncMode                   the sync mode
     */
    void setCellarFolderDailyReception(String cellarFolderDailyReception, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar folder daily response.
     *
     * @param cellarFolderDailyResponse the cellar folder daily response
     * @param syncMode                  the sync mode
     */
    void setCellarFolderDailyResponse(String cellarFolderDailyResponse, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar folder error.
     *
     * @param cellarFolderError the cellar folder error
     * @param syncMode          the sync mode
     */
    void setCellarFolderError(String cellarFolderError, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar folder foxml.
     *
     * @param cellarFolderFoxml the cellar folder foxml
     * @param syncMode          the sync mode
     */
    void setCellarFolderFoxml(String cellarFolderFoxml, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar folder license holder archive adhoc extraction.
     *
     * @param cellarFolderLicenseHolderArchiveAdhocExtraction the cellar folder license holder archive adhoc extraction
     * @param syncMode                                        the sync mode
     */
    void setCellarFolderLicenseHolderArchiveAdhocExtraction(String cellarFolderLicenseHolderArchiveAdhocExtraction,
                                                            EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar folder license holder archive extraction.
     *
     * @param cellarFolderLicenseHolderArchiveExtraction the cellar folder license holder archive extraction
     * @param syncMode                                   the sync mode
     */
    void setCellarFolderLicenseHolderArchiveExtraction(String cellarFolderLicenseHolderArchiveExtraction, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar folder license holder temporary archive.
     *
     * @param cellarFolderLicenseHolderTemporaryArchive the cellar folder license holder temporary archive
     * @param syncMode                                  the sync mode
     */
    void setCellarFolderLicenseHolderTemporaryArchive(String cellarFolderLicenseHolderTemporaryArchive, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar folder license holder temporary sparql.
     *
     * @param cellarFolderLicenseHolderTemporarySparql the cellar folder license holder temporary sparql
     * @param syncMode                                 the sync mode
     */
    void setCellarFolderLicenseHolderTemporarySparql(String cellarFolderLicenseHolderTemporarySparql, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar folder lock.
     *
     * @param cellarFolderLock the cellar folder lock
     * @param syncMode         the sync mode
     */
    void setCellarFolderLock(String cellarFolderLock, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar folder root.
     *
     * @param cellarFolderRoot the cellar folder root
     * @param syncMode         the sync mode
     */
    void setCellarFolderRoot(String cellarFolderRoot, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar folder temporary dissemination.
     *
     * @param cellarFolderTemporaryDissemination the cellar folder temporary dissemination
     * @param syncMode                           the sync mode
     */
    void setCellarFolderTemporaryDissemination(String cellarFolderTemporaryDissemination, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar folder temporary store.
     *
     * @param cellarFolderTemporaryStore the cellar folder temporary store
     * @param syncMode                   the sync mode
     */
    void setCellarFolderTemporaryStore(String cellarFolderTemporaryStore, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar folder temporary work.
     *
     * @param cellarFolderTemporaryWork the cellar folder temporary work
     * @param syncMode                  the sync mode
     */
    void setCellarFolderTemporaryWork(String cellarFolderTemporaryWork, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar instance id.
     *
     * @param cellarInstanceId the cellar instance id
     * @param syncMode         the sync mode
     */
    void setCellarInstanceId(String cellarInstanceId, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar server base url.
     *
     * @param cellarServerBaseUrl the cellar server base url
     * @param syncMode            the sync mode
     */
    void setCellarServerBaseUrl(String cellarServerBaseUrl, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar server context.
     *
     * @param cellarServerContext the cellar server context
     * @param syncMode            the sync mode
     */
    void setCellarServerContext(String cellarServerContext, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar server name.
     *
     * @param cellarServerName the cellar server name
     * @param syncMode         the sync mode
     */
    void setCellarServerName(String cellarServerName, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar server port.
     *
     * @param cellarServerPort the cellar server port
     * @param syncMode         the sync mode
     */
    void setCellarServerPort(String cellarServerPort, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service automatic disembargo enabled.
     *
     * @param cellarServiceAutomaticDisembargoEnabled the cellar service automatic disembargo enabled
     * @param syncMode                                the sync mode
     */
    void setCellarServiceAutomaticDisembargoEnabled(boolean cellarServiceAutomaticDisembargoEnabled, EXISTINGPROPS_MODE... syncMode);

    void setCellarServiceDisembargoInitialDelay(int cellarServiceDisembargoInitialDelay, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service backup processed file enabled.
     *
     * @param cellarServiceBackupProcessedFileEnabled the cellar service backup processed file enabled
     * @param syncMode                                the sync mode
     */
    void setCellarServiceBackupProcessedFileEnabled(boolean cellarServiceBackupProcessedFileEnabled, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service batch job enabled.
     *
     * @param cellarServiceBatchJobEnabled the cellar service batch job enabled
     * @param syncMode                     the sync mode
     */
    void setCellarServiceBatchJobEnabled(boolean cellarServiceBatchJobEnabled, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service batch job processing cron settings.
     *
     * @param cellarServiceBatchJobProcessingCronSettings the cellar service batch job processing cron settings
     * @param syncMode                                    the sync mode
     */
    void setCellarServiceBatchJobProcessingCronSettings(String cellarServiceBatchJobProcessingCronSettings, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service batch job sparql update max cron.
     *
     * @param cellarServiceBatchJobSparqlUpdateMaxCron the cellar service batch job sparql update max cron
     * @param syncMode                                 the sync mode
     */
    void setCellarServiceBatchJobSparqlUpdateMaxCron(String cellarServiceBatchJobSparqlUpdateMaxCron, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service batch job sparql cron settings.
     *
     * @param cellarBatchJobSparqlCronSettings the cellar batch job sparql cron settings
     * @param syncMode                         the sync mode
     */
    void setCellarServiceBatchJobSparqlCronSettings(String cellarBatchJobSparqlCronSettings, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service batch job thread pool core pool size.
     *
     * @param cellarServiceBatchJobThreadPoolCorePoolSize the cellar service batch job thread pool core pool size
     * @param syncMode                                    the sync mode
     */
    void setCellarServiceBatchJobPoolCorePoolSize(String cellarServiceBatchJobThreadPoolCorePoolSize, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service batch job thread pool max pool size.
     *
     * @param cellarServiceBatchJobThreadPoolMaxPoolSize the cellar service batch job thread pool max pool size
     * @param syncMode                                   the sync mode
     */
    void setCellarServiceBatchJobPoolMaxPoolSize(String cellarServiceBatchJobThreadPoolMaxPoolSize, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service batch job thread pool queue size.
     *
     * @param cellarServiceBatchJobThreadPoolQueueSize the cellar service batch job thread pool queue size
     * @param syncMode                                 the sync mode
     */
    void setCellarServiceBatchJobPoolQueueSize(String cellarServiceBatchJobThreadPoolQueueSize, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service batch job thread pool selection size .
     *
     * @param cellarServiceBatchJobThreadPoolSelectionSize the cellar service batch job thread pool selection size
     * @param syncMode                                     the sync mode
     */
    void setCellarServiceBatchJobPoolSelectionSize(String cellarServiceBatchJobThreadPoolSelectionSize, EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the cellar service batch job thread pool core pool size.
     *
     * @return the cellar service batch job thread pool core pool size
     */
    String getCellarServiceBatchJobPoolCorePoolSize();

    /**
     * Gets the cellar service batch job thread pool max pool size.
     *
     * @return the cellar service batch job thread pool max pool size
     */
    String getCellarServiceBatchJobPoolMaxPoolSize();

    /**
     * Gets the cellar service batch job thread pool queue size.
     *
     * @return the cellar service batch job thread pool queue size
     */
    String getCellarServiceBatchJobPoolQueueSize();

    /**
     * Gets the cellar service batch job thread pool selection size.
     *
     * @return the cellar service batch job thread pool selection size
     */
    String getCellarServiceBatchJobPoolSelectionSize();

    /**
     * Sets the cellar service delete processed data enabled.
     *
     * @param cellarServiceDeleteProcessedDataEnabled the cellar service delete processed data enabled
     * @param syncMode                                the sync mode
     */
    void setCellarServiceDeleteProcessedDataEnabled(boolean cellarServiceDeleteProcessedDataEnabled, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service dissemination content stream max size.
     *
     * @param cellarServiceDisseminationContentStreamMaxSize the cellar service dissemination content stream max size
     * @param syncMode                                       the sync mode
     */
    void setCellarServiceDisseminationContentStreamMaxSize(long cellarServiceDisseminationContentStreamMaxSize,
                                                           EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service dissemination daily oj cache refresh minutes.
     *
     * @param numberOfMinutes the number of minutes
     * @param syncMode        the sync mode
     */
    void setCellarServiceDisseminationDailyOjCacheRefreshMinutes(Integer numberOfMinutes, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service dissemination database optimization enabled.
     *
     * @param cellarServiceDisseminationDatabaseOptimizationEnabled the cellar service dissemination database optimization enabled
     * @param syncMode                                              the sync mode
     */
    void setCellarServiceDisseminationDatabaseOptimizationEnabled(boolean cellarServiceDisseminationDatabaseOptimizationEnabled,
                                                                  EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service dissemination decoding default.
     *
     * @param cellarServiceDisseminationDecodingDefault the cellar service dissemination decoding default
     * @param syncMode                                  the sync mode
     */
    void setCellarServiceDisseminationDecodingDefault(final String cellarServiceDisseminationDecodingDefault,
                                                      final EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service dissemination in notice properties only enabled.
     *
     * @param cellarServiceDisseminationInNoticePropertiesOnlyEnabled the cellar service dissemination in notice properties only enabled
     * @param syncMode                                                the sync mode
     */
    void setCellarServiceDisseminationInNoticePropertiesOnlyEnabled(boolean cellarServiceDisseminationInNoticePropertiesOnlyEnabled,
                                                                    EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service dissemination notices sort enabled.
     *
     * @param cellarServiceDisseminationNoticesSortEnabled the cellar service dissemination notices sort enabled
     * @param syncMode                                     the sync mode
     */
    void setCellarServiceDisseminationNoticesSortEnabled(boolean cellarServiceDisseminationNoticesSortEnabled,
                                                         EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service external startup timeout seconds.
     *
     * @param cellarServiceExternalStartupTimeoutSeconds the cellar service external startup timeout seconds
     * @param syncMode                                   the sync mode
     */
    void setCellarServiceExternalStartupTimeoutSeconds(int cellarServiceExternalStartupTimeoutSeconds, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service indexing concurrency controller enabled.
     *
     * @param cellarServiceIndexingConcurrencyControllerEnabled the cellar service indexing concurrency controller enabled
     * @param syncMode                                          the sync mode
     */
    void setCellarServiceIndexingConcurrencyControllerEnabled(boolean cellarServiceIndexingConcurrencyControllerEnabled,
                                                              EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service indexing cron settings.
     *
     * @param cellarServiceIndexingCronSettings the cellar service indexing cron settings
     * @param syncMode                          the sync mode
     */
    void setCellarServiceIndexingCronSettings(final String cellarServiceIndexingCronSettings, final EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service Synchronizing cron settings.
     *
     * @param cellarServiceSynchronizingCronSettings  the cellar service Synchronizing cron settings
     * @param syncMode                          the sync mode
     */
    void setCellarServiceSynchronizingCronSettings(final String cellarServiceSynchronizingCronSettings, final EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service indexing enabled.
     *
     * @param cellarServiceIndexingEnabled the cellar service indexing enabled
     * @param syncMode                     the sync mode
     */
    void setCellarServiceIndexingEnabled(boolean cellarServiceIndexingEnabled, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service indexing minimum age minutes.
     *
     * @param cellarServiceIndexingMinimumAgeMinutes the cellar service indexing minimum age minutes
     * @param syncMode                               the sync mode
     */
    void setCellarServiceIndexingMinimumAgeMinutes(final int cellarServiceIndexingMinimumAgeMinutes, final EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service indexing minimum priority handled.
     *
     * @param cellarServiceIndexingMinimumPriority the cellar service indexing minimum priority
     * @param syncMode                             the sync mode
     */
    void setCellarServiceIndexingMinimumPriority(final Priority cellarServiceIndexingMinimumPriority, final EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service indexing cleaner enabled.
     *
     * @param cellarServiceIndexingCleanerEnabled indicate if the cleaning service is enabled
     * @param syncMode                            the sync mode
     */
    void setCellarServiceIndexingCleanerEnabled(final boolean cellarServiceIndexingCleanerEnabled, final EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service indexing cleaner cron settings.
     *
     * @param cellarServiceIndexingCleanerCronSettings the cron setting
     * @param syncMode                                 the sync mode
     */
    void setCellarServiceIndexingCleanerCronSettings(final String cellarServiceIndexingCleanerCronSettings,
                                                     final EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service indexing cleaner minimum age (in days) for requests marked as Done.
     *
     * @param cellarServiceIndexingCleanerDoneMinimumAgeDays the minimum age
     * @param syncMode                                       the sync mode
     */
    void setCellarServiceIndexingCleanerDoneMinimumAgeDays(final int cellarServiceIndexingCleanerDoneMinimumAgeDays,
                                                           final EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service indexing cleaner minimum age (in days) for requests marked as Redundant.
     *
     * @param cellarServiceIndexingCleanerRedundantMinimumAgeDays the minimum age
     * @param syncMode                                       the sync mode
     */
    void setCellarServiceIndexingCleanerRedundantMinimumAgeDays(int cellarServiceIndexingCleanerRedundantMinimumAgeDays,
                                                                    EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service indexing cleaner minimum age (in days) for requests marked as Error.
     *
     * @param cellarServiceIndexingCleanerErrorMinimumAgeDays the minimum age
     * @param syncMode                                        the sync mode
     */
    void setCellarServiceIndexingCleanerErrorMinimumAgeDays(final int cellarServiceIndexingCleanerErrorMinimumAgeDays,
                                                            final EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service indexing pool threads.
     *
     * @param cellarServiceIndexingPoolThreads the cellar service indexing pool threads
     * @param syncMode                         the sync mode
     */
    void setCellarServiceIndexingPoolThreads(int cellarServiceIndexingPoolThreads, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service indexing query limit results.
     *
     * @param cellarServiceIndexingQueryLimitResults the cellar service indexing query limit results
     * @param syncMode                               the sync mode
     */
    void setCellarServiceIndexingQueryLimitResults(int cellarServiceIndexingQueryLimitResults, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service indexing scheduled priority.
     *
     * @param cellarServiceIndexingScheduledPriority the cellar service indexing scheduled priority
     * @param syncMode                               the sync mode
     */
    void setCellarServiceIndexingScheduledPriority(final String cellarServiceIndexingScheduledPriority, EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the cellar service indexing scheduled priority.
     *
     * @return the cellar service indexing scheduled priority
     */
    String getCellarServiceIndexingScheduledPriority();

    /**
     * Gets the cellar service indexing scheduled calc inverse.
     *
     * @return the cellar service indexing scheduled calc inverse
     */
    boolean getCellarServiceIndexingScheduledCalcInverse();

    /**
     * Gets the cellar service indexing scheduled calc embedded.
     *
     * @return the cellar service indexing scheduled calc embedded
     */
    boolean getCellarServiceIndexingScheduledCalcEmbedded();

    /**
     * Gets the cellar service indexing scheduled calc notice.
     *
     * @return the cellar service indexing scheduled calc notice
     */
    boolean getCellarServiceIndexingScheduledCalcNotice();

    /**
     * Gets the cellar service indexing scheduled calc expanded.
     *
     * @return the cellar service indexing scheduled calc expanded
     */
    boolean getCellarServiceIndexingScheduledCalcExpanded();

    /**
     * Sets the cellar service indexing scheduled calc inverse.
     *
     * @param cellarServiceIndexingScheduledCalcInverse the cellar service indexing scheduled calc inverse
     * @param syncMode                                  the sync mode
     */
    void setCellarServiceIndexingScheduledCalcInverse(final boolean cellarServiceIndexingScheduledCalcInverse,
                                                      EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service indexing scheduled calc embedded.
     *
     * @param cellarServiceIndexingScheduledCalcEmbedded the cellar service indexing scheduled calc embedded
     * @param syncMode                                   the sync mode
     */
    void setCellarServiceIndexingScheduledCalcEmbedded(final boolean cellarServiceIndexingScheduledCalcEmbedded,
                                                       EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service indexing scheduled calc notice.
     *
     * @param cellarServiceIndexingScheduledCalcNotice the cellar service indexing scheduled calc notice
     * @param syncMode                                 the sync mode
     */
    void setCellarServiceIndexingScheduledCalcNotice(final boolean cellarServiceIndexingScheduledCalcNotice,
                                                     EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service indexing scheduled calc expanded.
     *
     * @param cellarServiceIndexingScheduledCalcExpanded the cellar service indexing scheduled calc expanded
     * @param syncMode                                   the sync mode
     */
    void setCellarServiceIndexingScheduledCalcExpanded(final boolean cellarServiceIndexingScheduledCalcExpanded,
                                                       EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the cellar service indexing scheduled selection page size.
     *
     * @return the cellar service indexing scheduled selection page size
     */
    long getCellarServiceIndexingScheduledSelectionPageSize();

    /**
     * Sets the cellar service indexing scheduled selection page size.
     *
     * @param cellarServiceIndexingScheduledSelectionPageSize the cellar service indexing scheduled selection page size
     * @param syncMode                                        the sync mode
     */
    void setCellarServiceIndexingScheduledSelectionPageSize(final long cellarServiceIndexingScheduledSelectionPageSize,
                                                            EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the cellar service indexing scheduled batch sleep time.
     *
     * @return the cellar service indexing scheduled batch sleep time
     */
    long getCellarServiceIndexingScheduledBatchSleepTime();

    /**
     * Sets the cellar service indexing scheduled batch sleep time.
     *
     * @param cellarServiceIndexingScheduledBatchSleepTime the cellar service indexing scheduled batch sleep time
     * @param syncMode                                     the sync mode
     */
    void setCellarServiceIndexingScheduledBatchSleepTime(final long cellarServiceIndexingScheduledBatchSleepTime,
                                                         EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the cellar service indexing scheduled example sparql queries.
     *
     * @return the cellar service indexing scheduled example sparql queries
     */
    String getCellarServiceIndexingScheduledExampleSparqlQueries();

    /**
     * Sets the cellar service indexing scheduled example sparql queries.
     *
     * @param cellarServiceIndexingScheduledExampleSparqlQueries the cellar service indexing scheduled example sparql queries
     * @param syncMode                                           the sync mode
     */
    void setCellarServiceIndexingScheduledExampleSparqlQueries(final String cellarServiceIndexingScheduledExampleSparqlQueries,
                                                               EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service ingestion check write once properties enabled.
     *
     * @param cellarServiceIngestionCheckWriteOncePropertiesEnabled the cellar service ingestion check write once properties enabled
     * @param syncMode                                              the sync mode
     */
    void setCellarServiceIngestionCheckWriteOncePropertiesEnabled(boolean cellarServiceIngestionCheckWriteOncePropertiesEnabled,
                                                                  EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service ingestion cmr lock mode.
     *
     * @param cellarServiceIngestionCmrLockMode the cellar service ingestion cmr lock mode
     * @param syncMode                          the sync mode
     */
    void setCellarServiceIngestionCmrLockMode(CellarServiceIngestionCmrLockMode cellarServiceIngestionCmrLockMode,
                                              EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service ingestion cmr lock ceiling delay.
     *
     * @param cellarServiceIngestionCmrLockCeilingDelay the cellar service ingestion cmr lock ceiling delay
     * @param syncMode                                  the sync mode
     */
    void setCellarServiceIngestionCmrLockCeilingDelay(long cellarServiceIngestionCmrLockCeilingDelay, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service ingestion cmr lock max retries.
     *
     * @param cellarServiceIngestionCmrLockMaxRetries the cellar service ingestion cmr lock max retries
     * @param syncMode                                the sync mode
     */
    void setCellarServiceIngestionCmrLockMaxRetries(int cellarServiceIngestionCmrLockMaxRetries, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service ingestion cmr lock base.
     *
     * @param cellarServiceIngestionCmrLockBase the cellar service ingestion cmr lock base
     * @param syncMode                          the sync mode
     */
    void setCellarServiceIngestionCmrLockBase(int cellarServiceIngestionCmrLockBase, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service ingestion concurrency controller autologging cron settings.
     *
     * @param cellarServiceIngestionConcurrencyControllerAutologgingCronSettings the cellar service ingestion concurrency controller autologging cron settings
     * @param syncMode                                                           the sync mode
     */
    void setCellarServiceIngestionConcurrencyControllerAutologgingCronSettings(
            String cellarServiceIngestionConcurrencyControllerAutologgingCronSettings, EXISTINGPROPS_MODE... syncMode);
    
    /**
     * Sets the cellar service ingestion concurrency controller thread delay in milliseconds.
     *
     * @param cellarServiceIngestionConcurrencyControllerThreadDelayMilliseconds the cellar service ingestion concurrency controller thread delay in milliseconds
     * @param syncMode                                the sync mode
     */
    void setCellarServiceIngestionConcurrencyControllerThreadDelayMilliseconds(
    		long cellarServiceIngestionConcurrencyControllerThreadDelayMilliseconds, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service ingestion concurrency controller autologging enabled.
     *
     * @param cellarServiceIngestionConcurrencyControllerAutologgingEnabled the cellar service ingestion concurrency controller autologging enabled
     * @param syncMode                                                      the sync mode
     */
    void setCellarServiceIngestionConcurrencyControllerAutologgingEnabled(
            boolean cellarServiceIngestionConcurrencyControllerAutologgingEnabled, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service ingestion execution rate.
     *
     * @param cellarServiceIngestionExecutionRate the cellar service ingestion execution rate
     * @param syncMode                           the sync mode
     */
    void setCellarServiceIngestionExecutionRate(final String cellarServiceIngestionExecutionRate, final EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service ingestion validation metadata enabled.
     *
     * @param cellarServiceIngestionValidationMetadataEnabled the cellar service ingestion validation metadata enabled
     * @param syncMode                                        the sync mode
     */
    void setCellarServiceIngestionValidationMetadataEnabled(boolean cellarServiceIngestionValidationMetadataEnabled, EXISTINGPROPS_MODE... syncMode);

    void setCellarServiceIngestionValidationItemExclusionEnabled(final boolean cellarServiceIngestionCronSettings,
                                                                 final EXISTINGPROPS_MODE... syncMode);
    
    /**
     * Sets the cellar status service shacl report level
     *
     * @param cellarStatusServiceShaclReportLevel cellar status service shacl report level
     * @param syncMode                                the sync mode
     */
    void setCellarStatusServiceShaclReportLevel(
            CellarStatusServiceShaclReportLevel cellarStatusServiceShaclReportLevel, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service ingestion enabled.
     *
     * @param cellarServiceIngestionEnabled the cellar service ingestion enabled
     * @param syncMode                      the sync mode
     */
    void setCellarServiceIngestionEnabled(boolean cellarServiceIngestionEnabled, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service ingestion pickup mode.
     *
     * @param cellarServiceIngestionPickupMode the cellar service ingestion pickup mode
     * @param syncMode                         the sync mode
     */
    void setCellarServiceIngestionPickupMode(CellarServiceIngestionPickupMode cellarServiceIngestionPickupMode,
                                             EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service ingestion pool core pool size.
     *
     * @param corePoolSize the core pool size
     * @param syncMode     the sync mode
     */
    void setCellarServiceIngestionPoolThreads(int corePoolSize, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service ingestion sip copy retry pause seconds.
     *
     * @param cellarServiceIngestionSipCopyRetryPauseSeconds the cellar service ingestion sip copy retry pause seconds
     * @param syncMode                                       the sync mode
     */
    void setCellarServiceIngestionSipCopyRetryPauseSeconds(int cellarServiceIngestionSipCopyRetryPauseSeconds,
                                                           EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service ingestion sip copy timeout seconds.
     *
     * @param cellarServiceIngestionSipCopyTimeoutSeconds the cellar service ingestion sip copy timeout seconds
     * @param syncMode                                    the sync mode
     */
    void setCellarServiceIngestionSipCopyTimeoutSeconds(int cellarServiceIngestionSipCopyTimeoutSeconds, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service inverse notices agent enabled.
     *
     * @param cellarServiceInverseNoticesAgentEnabled the cellar service inverse notices agent enabled
     * @param syncMode                                the sync mode
     */
    void setCellarServiceInverseNoticesAgentEnabled(boolean cellarServiceInverseNoticesAgentEnabled, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service inverse notices top level event enabled.
     *
     * @param cellarServiceInverseNoticesTopLevelEventEnabled the cellar service inverse notices top level event enabled
     * @param syncMode                                        the sync mode
     */
    void setCellarServiceInverseNoticesTopLevelEventEnabled(boolean cellarServiceInverseNoticesTopLevelEventEnabled,
                                                            EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service inverse notices dossier enabled.
     *
     * @param cellarServiceInverseNoticesDossierEnabled the cellar service inverse notices dossier enabled
     * @param syncMode                                  the sync mode
     */
    void setCellarServiceInverseNoticesDossierEnabled(boolean cellarServiceInverseNoticesDossierEnabled, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service inverse notices event enabled.
     *
     * @param cellarServiceInverseNoticesEventEnabled the cellar service inverse notices event enabled
     * @param syncMode                                the sync mode
     */
    void setCellarServiceInverseNoticesEventEnabled(boolean cellarServiceInverseNoticesEventEnabled, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service inverse notices expression enabled.
     *
     * @param cellarServiceInverseNoticesExpressionEnabled the cellar service inverse notices expression enabled
     * @param syncMode                                     the sync mode
     */
    void setCellarServiceInverseNoticesExpressionEnabled(boolean cellarServiceInverseNoticesExpressionEnabled,
                                                         EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service inverse notices limit.
     *
     * @param cellarServiceInverseNoticesLimit the cellar service inverse notices limit
     * @param syncMode                         the sync mode
     */
    void setCellarServiceInverseNoticesLimit(int cellarServiceInverseNoticesLimit, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service inverse notices manifestation enabled.
     *
     * @param cellarServiceInverseNoticesManifestationEnabled the cellar service inverse notices manifestation enabled
     * @param syncMode                                        the sync mode
     */
    void setCellarServiceInverseNoticesManifestationEnabled(boolean cellarServiceInverseNoticesManifestationEnabled,
                                                            EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service inverse notices work enabled.
     *
     * @param cellarServiceInverseNoticesWorkEnabled the cellar service inverse notices work enabled
     * @param syncMode                               the sync mode
     */
    void setCellarServiceInverseNoticesWorkEnabled(boolean cellarServiceInverseNoticesWorkEnabled, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service inverse notices node enabled.
     *
     * @param cellarServiceInverseNoticesNodeEnabled the cellar service inverse notices node enabled
     * @param syncMode                               the sync mode
     */
    void setCellarServiceInverseNoticesRemoveNodeEnabled(final boolean cellarServiceInverseNoticesNodeEnabled,
                                                         final EXISTINGPROPS_MODE... syncMode);

    /**
     * Checks if is cellar service inverse notices node enabled.
     *
     * @return true, if is cellar service inverse notices node enabled
     */
    boolean isCellarServiceInverseNoticesRemoveNodeEnabled();

    /**
     * Sets the cellar service license holder batch size folder.
     *
     * @param cellarServiceLicenseHolderBatchSizeFolder the cellar service license holder batch size folder
     * @param syncMode                                  the sync mode
     */
    void setCellarServiceLicenseHolderBatchSizeFolder(int cellarServiceLicenseHolderBatchSizeFolder, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service license holder batch size per thread.
     *
     * @param cellarServiceLicenseHolderBatchSizePerThread the cellar service license holder batch size per thread
     * @param syncMode                                     the sync mode
     */
    void setCellarServiceLicenseHolderBatchSizePerThread(int cellarServiceLicenseHolderBatchSizePerThread, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service license holder cleaner cron settings.
     *
     * @param cellarServiceLicenseHolderCleanerCronSettings the cellar service license holder cleaner cron settings
     * @param syncMode                                      the sync mode
     */
    void setCellarServiceLicenseHolderCleanerCronSettings(String cellarServiceLicenseHolderCleanerCronSettings,
                                                          EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service license holder enabled.
     *
     * @param cellarServiceLicenseHolderEnabled the cellar service license holder enabled
     * @param syncMode                          the sync mode
     */
    void setCellarServiceLicenseHolderEnabled(boolean cellarServiceLicenseHolderEnabled, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service license holder kept days daily.
     *
     * @param cellarServiceLicenseHolderKeptDaysDaily the cellar service license holder kept days daily
     * @param syncMode                                the sync mode
     */
    void setCellarServiceLicenseHolderKeptDaysDaily(int cellarServiceLicenseHolderKeptDaysDaily, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service license holder kept days monthly.
     *
     * @param cellarServiceLicenseHolderKeptDaysMonthly the cellar service license holder kept days monthly
     * @param syncMode                                  the sync mode
     */
    void setCellarServiceLicenseHolderKeptDaysMonthly(int cellarServiceLicenseHolderKeptDaysMonthly, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service license holder kept days weekly.
     *
     * @param cellarServiceLicenseHolderKeptDaysWeekly the cellar service license holder kept days weekly
     * @param syncMode                                 the sync mode
     */
    void setCellarServiceLicenseHolderKeptDaysWeekly(int cellarServiceLicenseHolderKeptDaysWeekly, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service license holder scheduler cron settings.
     *
     * @param cellarServiceLicenseHolderSchedulerCronSettings the cellar service license holder scheduler cron settings
     * @param syncMode                                        the sync mode
     */
    void setCellarServiceLicenseHolderSchedulerCronSettings(String cellarServiceLicenseHolderSchedulerCronSettings,
                                                            EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service license holder worker archiving executor core pool size.
     *
     * @param cellarServiceLicenseHolderWorkerArchivingExecutorCorePoolSize the cellar service license holder worker archiving executor core pool size
     * @param syncMode                                                      the sync mode
     */
    void setCellarServiceLicenseHolderWorkerArchivingExecutorCorePoolSize(int cellarServiceLicenseHolderWorkerArchivingExecutorCorePoolSize,
                                                                          EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service license holder worker archiving executor max pool size.
     *
     * @param cellarServiceLicenseHolderWorkerArchivingExecutorMaxPoolSize the cellar service license holder worker archiving executor max pool size
     * @param syncMode                                                     the sync mode
     */
    void setCellarServiceLicenseHolderWorkerArchivingExecutorMaxPoolSize(int cellarServiceLicenseHolderWorkerArchivingExecutorMaxPoolSize,
                                                                         EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service license holder worker archiving executor queue capacity.
     *
     * @param cellarServiceLicenseHolderWorkerArchivingExecutorQueueCapacity the cellar service license holder worker archiving executor queue capacity
     * @param syncMode                                                       the sync mode
     */
    void setCellarServiceLicenseHolderWorkerArchivingExecutorQueueCapacity(
            int cellarServiceLicenseHolderWorkerArchivingExecutorQueueCapacity, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service license holder worker cron settings.
     *
     * @param cellarServiceLicenseHolderWorkerCronSettings the cellar service license holder worker cron settings
     * @param syncMode                                     the sync mode
     */
    void setCellarServiceLicenseHolderWorkerCronSettings(String cellarServiceLicenseHolderWorkerCronSettings,
                                                         EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service license holder worker task executor core pool size.
     *
     * @param cellarServiceLicenseHolderWorkerTaskExecutorCorePoolSize the cellar service license holder worker task executor core pool size
     * @param syncMode                                                 the sync mode
     */
    void setCellarServiceLicenseHolderWorkerTaskExecutorCorePoolSize(int cellarServiceLicenseHolderWorkerTaskExecutorCorePoolSize,
                                                                     EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service license holder worker task executor max pool size.
     *
     * @param cellarServiceLicenseHolderWorkerTaskExecutorMaxPoolSize the cellar service license holder worker task executor max pool size
     * @param syncMode                                                the sync mode
     */
    void setCellarServiceLicenseHolderWorkerTaskExecutorMaxPoolSize(int cellarServiceLicenseHolderWorkerTaskExecutorMaxPoolSize,
                                                                    EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service license holder worker task executor queue capacity.
     *
     * @param cellarServiceLicenseHolderWorkerTaskExecutorQueueCapacity the cellar service license holder worker task executor queue capacity
     * @param syncMode                                                  the sync mode
     */
    void setCellarServiceLicenseHolderWorkerTaskExecutorQueueCapacity(int cellarServiceLicenseHolderWorkerTaskExecutorQueueCapacity,
                                                                      EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service nal load cron settings.
     *
     * @param cellarServiceNalLoadCronSettings the cellar service nal load cron settings
     * @param syncMode                         the sync mode
     */
    void setCellarServiceNalLoadCronSettings(String cellarServiceNalLoadCronSettings, EXISTINGPROPS_MODE... syncMode);

    void setCellarServiceSparqlLoadCronSettings(String cellarServiceSparqlLoadCronSettings, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service nal load enabled.
     *
     * @param cellarServiceNalLoadEnabled the cellar service nal load enabled
     * @param syncMode                    the sync mode
     */
    void setCellarServiceNalLoadEnabled(boolean cellarServiceNalLoadEnabled, EXISTINGPROPS_MODE... syncMode);

    /**
     * @param cellarServiceSparqlLoadEnabled
     * @param syncMode
     */
    void setCellarServiceSparqlLoadEnabled(boolean cellarServiceSparqlLoadEnabled, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service notification items per page.
     *
     * @param cellarServiceNotificationItemsPerPage the cellar service notification items per page
     * @param syncMode                              the sync mode
     */
    void setCellarServiceNotificationItemsPerPage(int cellarServiceNotificationItemsPerPage, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service onto load enabled.
     *
     * @param cellarServiceOntoLoadEnabled the cellar service onto load enabled
     * @param syncMode                     the sync mode
     */
    void setCellarServiceOntoLoadEnabled(boolean cellarServiceOntoLoadEnabled, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service rdf store cleaner enabled.
     *
     * @param cellarServiceRDFStoreCleanerEnabled the cellar service rdf store cleaner enabled
     * @param syncMode                            the sync mode
     */
    void setCellarServiceRDFStoreCleanerEnabled(boolean cellarServiceRDFStoreCleanerEnabled, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service rdf store cleaner executor core pool size.
     *
     * @param cellarServiceRDFStoreCleanerExecutorCorePoolSize the cellar service rdf store cleaner executor core pool size
     * @param syncMode                                         the sync mode
     */
    void setCellarServiceRDFStoreCleanerExecutorCorePoolSize(int cellarServiceRDFStoreCleanerExecutorCorePoolSize,
                                                             EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service rdf store cleaner executor max pool size.
     *
     * @param cellarServiceRDFStoreCleanerExecutorMaxPoolSize the cellar service rdf store cleaner executor max pool size
     * @param syncMode                                        the sync mode
     */
    void setCellarServiceRDFStoreCleanerExecutorMaxPoolSize(int cellarServiceRDFStoreCleanerExecutorMaxPoolSize,
                                                            EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service rdf store cleaner executor queue capacity.
     *
     * @param cellarServiceRDFStoreCleanerExecutorQueueCapacity the cellar service rdf store cleaner executor queue capacity
     * @param syncMode                                          the sync mode
     */
    void setCellarServiceRDFStoreCleanerExecutorQueueCapacity(int cellarServiceRDFStoreCleanerExecutorQueueCapacity,
                                                              EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service rdf store cleaner hierarchies batch size.
     *
     * @param cellarServiceRDFStoreCleanerHierarchiesBatchSize the cellar service rdf store cleaner hierarchies batch size
     * @param syncMode                                         the sync mode
     */
    void setCellarServiceRDFStoreCleanerHierarchiesBatchSize(int cellarServiceRDFStoreCleanerHierarchiesBatchSize,
                                                             EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service rdf store cleaner mode.
     *
     * @param cellarServiceRDFStoreCleanerMode the cellar service rdf store cleaner mode
     * @param syncMode                         the sync mode
     */
    void setCellarServiceRDFStoreCleanerMode(CellarServiceRDFStoreCleanerMode cellarServiceRDFStoreCleanerMode,
                                             EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service rdf store cleaner resources batch size.
     *
     * @param cellarServiceRDFStoreCleanerResourcesBatchSize the cellar service rdf store cleaner resources batch size
     * @param syncMode                                       the sync mode
     */
    void setCellarServiceRDFStoreCleanerResourcesBatchSize(int cellarServiceRDFStoreCleanerResourcesBatchSize,
                                                           EXISTINGPROPS_MODE... syncMode);

   /**
    * Sets the cellar service SIP dependency checker cron settings.
    * @param cellarServiceSipDependencyCheckerCronSettings the cellar service SIP dependency checker cron settings
    * @param syncMode the sync mode
    */
    void setCellarServiceSipDependencyCheckerCronSettings(final String cellarServiceSipDependencyCheckerCronSettings, final EXISTINGPROPS_MODE... syncMode);
    
    /**
     * Sets the cellar service SIP dependency checker thread pool settings.
     * @param corePoolSize the cellar service SIP dependency checker thread pool settings
     * @param syncMode the sync mode
     */
    void setCellarServiceSipDependencyCheckerPoolThreads(final int corePoolSize, final EXISTINGPROPS_MODE... syncMode);
    
    /**
     * Sets the cellar service SIP queue manager cron settings.
     * @param cellarServiceSipQueueManagerCronSettings the cellar service SIP queue manager cron settings
     * @param syncMode the sync mode
     */
     void setCellarServiceSipQueueManagerCronSettings(final String cellarServiceSipQueueManagerCronSettings, final EXISTINGPROPS_MODE... syncMode);
    
    /**
     * Sets the cellar uri dissemination base.
     *
     * @param cellarUriDisseminationBase the cellar uri dissemination base
     * @param syncMode                   the sync mode
     */
    void setCellarUriDisseminationBase(String cellarUriDisseminationBase, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar uri resource base.
     *
     * @param cellarUriResourceBase the cellar uri resource base
     * @param syncMode              the sync mode
     */
    void setCellarUriResourceBase(String cellarUriResourceBase, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar uri sparql service.
     *
     * @param cellarUriSparqlService the cellar uri sparql service
     * @param syncMode               the sync mode
     */
    void setCellarServiceSparqlUri(String cellarUriSparqlService, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar uri tranformation resource base.
     *
     * @param cellarUriTranformationResourceBase the cellar uri tranformation resource base
     * @param syncMode                           the sync mode
     */
    void setCellarUriTranformationResourceBase(String cellarUriTranformationResourceBase, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the test database cron settings.
     *
     * @param testDatabaseCronSettings the test database cron settings
     * @param syncMode                 the sync mode
     */
    void setTestDatabaseCronSettings(String testDatabaseCronSettings, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the test database enabled.
     *
     * @param testDatabaseEnabled the test database enabled
     * @param syncMode            the sync mode
     */
    void setTestDatabaseEnabled(boolean testDatabaseEnabled, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the test enabled.
     *
     * @param testEnabled the test enabled
     * @param syncMode    the sync mode
     */
    void setTestEnabled(boolean testEnabled, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the test rollback enabled.
     *
     * @param testRollbackEnabled the test rollback enabled
     * @param syncMode            the sync mode
     */
    void setTestRollbackEnabled(boolean testRollbackEnabled, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the virtuoso test rollback enabled.
     *
     * @param testRollbackEnabled the test rollback enabled
     * @param syncMode            the sync mode
     */
    void setTestVirtuosoRollbackEnabled(boolean testRollbackEnabled, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the indexing delay execution enabled for testing.
     *
     * @param testIndexingDelayExecutionEnabled the indexing delay execution enabled for testing
     * @param syncMode            the sync mode
     */
    void setTestIndexingDelayExecutionEnabled(boolean testIndexingDelayExecutionEnabled, final EXISTINGPROPS_MODE... syncMode);
    
    /**
     * Sets the Virtuoso Retry enabled for testing.
     *
     * @param testVirtuosoRetryEnabled the test virtuoso retry enabled
     * @param syncMode                 the sync mode
     */
    void setTestVirtuosoRetryEnabled(final boolean testVirtuosoRetryEnabled, final EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the S3 Retry enabled for testing.
     *
     * @param testS3RetryEnabled the test s3 retry enabled
     * @param syncMode           the sync mode
     */
    void setTestS3RetryEnabled(final boolean testS3RetryEnabled, final EXISTINGPROPS_MODE... syncMode);
    
    /**
     * Sets the process phase to monitor during testing.
     *
     * @param testProcessMonitorPhase the {@link ProcessMonitorPhase} to monitor during testing
     * @param syncMode                the sync mode
     */
    void setTestProcessMonitorPhase(final ProcessMonitorPhase testProcessMonitorPhase, final EXISTINGPROPS_MODE... syncMode);
    
    /**
     * Sets that the process phase to monitor is reached during testing.
     *
     * @param testProcessMonitorPhaseReached the process phase to monitor during testing
     * @param syncMode                       the sync mode
     */
    void setTestProcessMonitorPhaseReached(final boolean testProcessMonitorPhaseReached, final EXISTINGPROPS_MODE... syncMode);
    
    /**
     * Sets the cellar service aligner executor core pool size.
     *
     * @param corePoolSize the core pool size
     * @param syncMode     the sync mode
     */
    void setCellarServiceAlignerExecutorCorePoolSize(int corePoolSize, EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the cellar service aligner executor core pool size.
     *
     * @return the cellar service aligner executor core pool size
     */
    int getCellarServiceAlignerExecutorCorePoolSize();

    /**
     * Sets the cellar service aligner executor max pool size.
     *
     * @param maxPoolSize the max pool size
     * @param syncMode    the sync mode
     */
    void setCellarServiceAlignerExecutorMaxPoolSize(int maxPoolSize, EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the cellar service aligner executor max pool size.
     *
     * @return the cellar service aligner executor max pool size
     */
    int getCellarServiceAlignerExecutorMaxPoolSize();

    /**
     * Sets the cellar service aligner executor queue capacity.
     *
     * @param queueCapacity the queue capacity
     * @param syncMode      the sync mode
     */
    void setCellarServiceAlignerExecutorQueueCapacity(int queueCapacity, EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the cellar service aligner executor queue capacity.
     *
     * @return the cellar service aligner executor queue capacity
     */
    int getCellarServiceAlignerExecutorQueueCapacity();

    /**
     * Sets the cellar service aligner mode.
     *
     * @param mode     the mode
     * @param syncMode the sync mode
     */
    void setCellarServiceAlignerMode(String mode, EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the cellar service aligner mode.
     *
     * @return the cellar service aligner mode
     */
    String getCellarServiceAlignerMode();

    /**
     * Sets the cellar service aligner hierarchies batch size.
     *
     * @param batchSize the batch size
     * @param syncMode  the sync mode
     */
    void setCellarServiceAlignerHierarchiesBatchSize(int batchSize, EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the cellar service languages skos cache period.
     *
     * @return the cellar service languages skos cache period
     */
    String getCellarServiceLanguagesSkosCachePeriod();

    /**
     * Sets the cellar service languages skos cache period.
     *
     * @param languagesSkosCachePeriod the languages skos cache period
     * @param syncMode                 the sync mode
     */
    void setCellarServiceLanguagesSkosCachePeriod(String languagesSkosCachePeriod, EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the cellar service file types skos cache period.
     *
     * @return the cellar service file types skos cache period
     */
    String getCellarServiceFileTypesSkosCachePeriod();

    /**
     * Sets the cellar service file types skos cache period.
     *
     * @param cellarServiceLanguagesSkosCachePeriod the cellar service languages skos cache period
     * @param syncMode                              the sync mode
     */
    void setCellarServiceFileTypesSkosCachePeriod(final String cellarServiceLanguagesSkosCachePeriod, final EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the cellar service aligner hierarchies batch size.
     *
     * @return the cellar service aligner hierarchies batch size
     */
    int getCellarServiceAlignerHierarchiesBatchSize();

    /**
     * Checks if cellar service aligner executor enabled.
     *
     * @return true, if cellar service aligner executor enabled
     */
    boolean isCellarServiceAlignerExecutorEnabled();

    /**
     * Sets the cellar service aligner executor enabled.
     *
     * @param enabled  the new cellar service aligner executor enabled
     * @param syncMode the sync mode
     */
    void setCellarServiceAlignerExecutorEnabled(boolean enabled, final EXISTINGPROPS_MODE... syncMode);

    /**
     * Checks if is the ingestion towards Virtuoso is enabled.
     *
     * @return true, if is cellar virtuoso ingestion enabled
     */
    boolean isVirtuosoIngestionEnabled();

    /**
     * Sets the ingestion towards Virtuoso enabled.
     *
     * @param enabled  if true, the ingestion towards Virtuoso is enabled
     * @param syncMode the sync mode
     */
    void setVirtuosoIngestionEnabled(boolean enabled, final EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the ingestion retry strategy for Virtuoso.
     *
     * @return the ingestion retry strategy for Virtuoso
     */
    VirtuosoIngestionRetryStrategy getVirtuosoIngestionRetryStrategy();

    /**
     * Sets the ingestion retry strategy for Virtuoso.
     *
     * @param virtuosoIngestionRetryStrategy the ingestion retry strategy for Virtuoso
     * @param syncMode                       the sync mode
     */
    void setVirtuosoIngestionRetryStrategy(VirtuosoIngestionRetryStrategy virtuosoIngestionRetryStrategy, EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the ceiling delay for the retry strategy for Virtuoso.
     *
     * @return the ceiling delay for the retry strategy for Virtuoso
     */
    long getVirtuosoIngestionRetryCeilingDelay();

    /**
     * Sets the ceiling delay for the retry strategy for Virtuoso.
     *
     * @param virtuosoIngestionRetryCeilingDelay the ceiling delay for the retry strategy for Virtuoso
     * @param syncMode                           the sync mode
     */
    void setVirtuosoIngestionRetryCeilingDelay(long virtuosoIngestionRetryCeilingDelay, EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the max retries for the retry strategy for Virtuoso.
     *
     * @return the max retries for the retry strategy for Virtuoso
     */
    int getVirtuosoIngestionRetryMaxRetries();

    /**
     * Sets the max retries for the retry strategy for Virtuoso.
     *
     * @param virtuosoIngestionRetryMaxRetries the max retries for the retry strategy for Virtuoso
     * @param syncMode                         the sync mode
     */
    void setVirtuosoIngestionRetryMaxRetries(int virtuosoIngestionRetryMaxRetries, EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the retry base for the retry strategy for Virtuoso.
     *
     * @return the retry base for the retry strategy for Virtuoso.
     */
    int getVirtuosoIngestionRetryBase();

    /**
     * Sets the retry base for the retry strategy for Virtuoso.
     *
     * @param virtuosoIngestionRetryBase the retry base for the retry strategy for Virtuoso
     * @param syncMode                   the sync mode
     */
    void setVirtuosoIngestionRetryBase(int virtuosoIngestionRetryBase, EXISTINGPROPS_MODE... syncMode);

    /**
     * Enable/disable the technical metadata ingestion in virtuoso
     *
     * @param virtuosoIngestionTechnicalEnabled the boolean value if virtuoso technical metadata ingestion is enabled
     * @param syncMode                   the sync mode
     */
    void setVirtuosoIngestionTechnicalEnabled(boolean virtuosoIngestionTechnicalEnabled, EXISTINGPROPS_MODE... syncMode);

    /**
     * Check if the technical metadata ingestion in Virtuoso is enabled.
     *
     * @return true, if the technical metadata ingestion in Virtuoso is enabled.
     */
    boolean isVirtuosoIngestionTechnicalEnabled();
    /**
     * Check if the backup of Virtuoso during ingestions is enabled.
     *
     * @return true, if the backup of Virtuoso during ingestions is enabled
     */
    boolean isVirtuosoIngestionBackupEnabled();

    /**
     * Sets the backup of Virtuoso during ingestions enabled/disabled.
     *
     * @param virtuosoIngestionBackupEnabled if true, the backup of Virtuoso during ingestions is enabled
     * @param syncMode                       the sync mode
     */
    void setVirtuosoIngestionBackupEnabled(boolean virtuosoIngestionBackupEnabled, EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the cron settings for the scheduler that drives the backup of Virtuoso during ingestions.
     *
     * @return the cron settings for the scheduler that drives the backup of Virtuoso during ingestions
     */
    String getVirtuosoIngestionBackupSchedulerCronSettings();

    /**
     * Sets the cron settings for the scheduler that drives the backup of Virtuoso during ingestions.
     *
     * @param virtuosoIngestionBackupSchedulerCronSettings the cron settings for the scheduler that drives the backup of Virtuoso during ingestions
     * @param syncMode                                     the sync mode
     */
    void setVirtuosoIngestionBackupSchedulerCronSettings(String virtuosoIngestionBackupSchedulerCronSettings,
                                                         EXISTINGPROPS_MODE... syncMode);

    /**
     * Check if the Concurrency Controller during the Virtuoso ingestions is enabled.
     *
     * @return true, if the Concurrency Controller during the Virtuoso ingestions is enabled
     */
    boolean isVirtuosoIngestionBackupConcurrencyControllerEnabled();

    /**
     * Sets the Concurrency Controller during the Virtuoso ingestions enabled/disabled.
     *
     * @param virtuosoIngestionBackupConcurrencyControllerEnabled if true, the Concurrency Controller during the Virtuoso ingestions is enabled
     * @param syncMode                                            the sync mode
     */
    void setVirtuosoIngestionBackupConcurrencyControllerEnabled(boolean virtuosoIngestionBackupConcurrencyControllerEnabled,
                                                                EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the max results for the backup during Virtuoso ingestions.
     *
     * @return the max results for the backup during Virtuoso ingestions
     */
    int getVirtuosoIngestionBackupMaxResults();

    /**
     * Sets the max results for the backup during Virtuoso ingestions.
     *
     * @param virtuosoIngestionBackupMaxResults the max results for the backup during Virtuoso ingestions
     * @param syncMode                          the sync mode
     */
    void setVirtuosoIngestionBackupMaxResults(int virtuosoIngestionBackupMaxResults, EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the log verbose for the backup during Virtuoso ingestions.
     *
     * @return the log verbose for the backup during Virtuoso ingestions
     */
    boolean isVirtuosoIngestionBackupLogVerbose();

    /**
     * Checks if the normalization of the metadata in Virtuoso is enabled.
     *
     * @return true, if the normalization is enabled. Otherwise, false.
     */
    boolean isVirtuosoIngestionNormalizationEnabled();

    /**
     * Checks if the skolemization of the blank nodes in Virtuoso is enabled.
     *
     * @return true, if the skolemization is enabled. Otherwise, false.
     */
    boolean isVirtuosoIngestionSkolemizationEnabled();

    /**
     * Checks if the SPARQL mapper is enabled.
     *
     * @return true, if the SPARQL mapper is enabled. Otherwise, false.
     */
    boolean isVirtuosoDisseminationSparqlMapperEnabled();

    /**
     * Gets the SPARQL timeout in seconds.
     *
     * @return The SPARQL timeout in seconds..
     */
    int getCellarServiceSparqlTimeout();

    /**
     * Sets the cellar service integration archivist enabled.
     *
     * @param cellarServiceIntegrationArchivistEnabled the cellar service integration archivist enabled
     * @param syncMode                                 the sync mode
     */
    void setCellarServiceIntegrationArchivistEnabled(final boolean cellarServiceIntegrationArchivistEnabled,
                                                     final EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the cellar service integration archivist enabled.
     *
     * @return the cellar service integration archivist enabled
     */
    Boolean getCellarServiceIntegrationArchivistEnabled();

    /**
     * Sets the cellar service integration archivist base url.
     *
     * @param cellarServiceIntegrationArchivistBaseUrl the cellar service integration archivist base url
     * @param syncMode                                 the sync mode
     */
    void setCellarServiceIntegrationArchivistBaseUrl(final String cellarServiceIntegrationArchivistBaseUrl,
                                                     final EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the cellar service integration archivist base url.
     *
     * @return the cellar service integration archivist base url
     */
    String getCellarServiceIntegrationArchivistBaseUrl();

    void setCellarServiceIntegrationFixityEnabled(boolean cellarServiceIntegrationFixityEnabled,
                                                  EXISTINGPROPS_MODE... syncMode);

    Boolean getCellarServiceIntegrationFixityEnabled();

    void setCellarServiceIntegrationFixityBaseUrl(String cellarServiceIntegrationFixityBaseUrl,
                                                  EXISTINGPROPS_MODE... syncMode);

    String getCellarServiceIntegrationFixityBaseUrl();

    String getCellarServiceIntegrationValidationBaseUrl();

    void setCellarServiceIntegrationValidationBaseUrl(String cellarServiceIntegrationValidationBaseUrl,
                                                      EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the cellar service cellar resource max elements in memory.
     *
     * @param cellarServiceCellarResourceMaxElementsInMemory the cellar service cellar resource max elements in memory
     * @param syncMode                                       the sync mode
     */
    void setCellarServiceCellarResourceMaxElementsInMemory(final int cellarServiceCellarResourceMaxElementsInMemory,
                                                           final EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the cellar service cellar resource max elements in memory.
     *
     * @return the cellar service cellar resource max elements in memory
     */
    int getCellarServiceCellarResourceMaxElementsInMemory();

    /**
     * Sets the cellar service cellar resource time To live seconds.
     *
     * @param cellarServiceCellarResourceTimeToLiveSeconds the cellar service cellar resource time To live seconds
     * @param syncMode                                     the sync mode
     */
    void setCellarServiceCellarResourceTimeToLiveSeconds(final int cellarServiceCellarResourceTimeToLiveSeconds,
                                                         final EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the cellar service cellar resource time To live seconds.
     *
     * @return the cellar service cellar resource time To live seconds
     */
    int getCellarServiceCellarResourceTimeToLiveSeconds();    
    
    /**
     * Sets the cellar service embedded notice cache max bytes local heap.
     *
     * @param cellarServiceEmbeddedNoticeCacheMaxBytesLocalHeap the cellar service embedded notice cache max bytes local heap
     * @param syncMode                                       the sync mode
     */
    void setCellarServiceEmbeddedNoticeCacheMaxBytesLocalHeap(final String cellarServiceEmbeddedNoticeCacheMaxBytesLocalHeap,
                                                           final EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the cellar service embedded notice cache max bytes local heap
     *
     * @return the cellar service embedded notice cache max bytes local heap
     */
    String getCellarServiceEmbeddedNoticeCacheMaxBytesLocalHeap();
    
    /**
     * Sets the cellar service embedded notice cache max bytes local disk.
     *
     * @param cellarServiceEmbeddedNoticeCacheMaxBytesLocalDisk the cellar service embedded notice cache max bytes local disk
     * @param syncMode                                       the sync mode
     */
    void setCellarServiceEmbeddedNoticeCacheMaxBytesLocalDisk(final String cellarServiceEmbeddedNoticeCacheMaxBytesLocalDisk,
                                                           final EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the cellar service embedded notice cache max bytes local disk
     *
     * @return the cellar service embedded notice cache max bytes local disk
     */
    String getCellarServiceEmbeddedNoticeCacheMaxBytesLocalDisk();
    
    /**
     * Sets the cellar service embedded notice cache time To live seconds.
     *
     * @param cellarServiceEmbeddedNoticeCacheTimeToLiveSeconds the cellar service embedded notice cache time To live seconds
     * @param syncMode                                     the sync mode
     */
    void setCellarServiceEmbeddedNoticeCacheTimeToLiveSeconds(final long cellarServiceEmbeddedNoticeCacheTimeToLiveSeconds,
                                                         final EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the cellar service embedded notice cache time To live seconds.
     *
     * @return the cellar service embedded notice cache time To live seconds
     */
    long getCellarServiceEmbeddedNoticeCacheTimeToLiveSeconds();
    
    /**
     * Sets the cellar service embedded notice cache evaluation statistics enabled.
     *
     * @param cellarServiceEmbeddedNoticeCacheEvaluationStatisticsEnabled the cellar service embedded notice cache evaluation statistics enabled
     * @param syncMode                                     the sync mode
     */
    void setCellarServiceEmbeddedNoticeCacheEvaluationStatisticsEnabled(final boolean cellarServiceEmbeddedNoticeCacheEvaluationStatisticsEnabled,
                                                         final EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the cellar service embedded notice cache evaluation statistics enabled.
     *
     * @return the cellar service embedded notice cache evaluation statistics enabled
     */
    boolean isCellarServiceEmbeddedNoticeCacheEvaluationStatisticsEnabled();
    
    /**
     * Sets the file attribute date type to use when performing
     * package ingestion prioritization.
     * 
     * @param cellarServiceSipQueueManagerFileAttributeDateType the file
     * attribute date type to use.
     * @param syncMode the sync mode
     */
    void setCellarServiceSipQueueManagerFileAttributeDateType(final String cellarServiceSipQueueManagerFileAttributeDateType,
        													  final EXISTINGPROPS_MODE... syncMode);
    
    /**
     * Gets the file attribute date type to use when performing
     * package ingestion prioritization.
     * 
     * @return the file attribute date type to use.
     */
    String getCellarServiceSipQueueManagerFileAttributeDateType();

    /**
     * Sets whether the CELLAR BO instance is DORIE dedicated.
     * @param cellarServiceDorieDedicatedEnabled {@code true} to indicated DORIE dedicated BO instance.
     * @param syncMode the sync mode
     */
    void setCellarServiceDorieDedicatedEnabled(final boolean cellarServiceDorieDedicatedEnabled, final EXISTINGPROPS_MODE... syncMode);
    
    /**
     * Gets the cellar service DORIE dedicated enabled.
     * @return the cellar service DORIE dedicated enabled
     */
    boolean isCellarServiceDorieDedicatedEnabled();
    
    /**
     * Sets if the cellar service SipDependencyChecker is enabled.
     *
     * @param cellarServiceSipDependencyCheckerEnabled indicates if the cellar service SipDependencyChecker is enabled
     * @param syncMode the sync mode
     */
    void setCellarServiceSipDependencyCheckerEnabled(final String cellarServiceSipDependencyCheckerEnabled,
                                                       final EXISTINGPROPS_MODE... syncMode);

    /**
     * Checks if the cellar service SipDependencyChecker is enabled.
     *
     * @return true, if the cellar service SipDependencyChecker is enabled.
     */
    boolean isCellarServiceSipDependencyCheckerEnabled();
    
    /**
     * Sets if the normalization of the metadata in Virtuoso is enabled.
     *
     * @param virtuosoIngestionNormalizationEnabled indicates if the normalization of the metadata in Virtuoso is enabled
     * @param syncMode                              the sync mode
     */
    void setVirtuosoIngestionNormalizationEnabled(final boolean virtuosoIngestionNormalizationEnabled,
                                                  final EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets if the skolemization of the blank nodes in Virtuoso is enabled.
     *
     * @param virtuosoIngestionSkolemizationEnabled indicates if the skolemization of the the blank nodes in Virtuoso is enabled
     * @param syncMode                              the sync mode
     */
    void setVirtuosoIngestionSkolemizationEnabled(final boolean virtuosoIngestionSkolemizationEnabled,
                                                  final EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets if the SPARQL mapper is enabled.
     *
     * @param virtuosoDisseminationSparqlMapperEnabled indicates if the SPARQL mapper is enabled
     * @param syncMode                                 the sync mode
     */
    void setVirtuosoDisseminationSparqlMapperEnabled(final boolean virtuosoDisseminationSparqlMapperEnabled,
                                                     final EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the SPARQL timeout in seconds.
     *
     * @param cellarServiceSparqlTimeoutSeconds the SPARQL timeout in seconds
     * @param syncMode                          the sync mode
     */
    void setCellarServiceSparqlTimeout(final int cellarServiceSparqlTimeoutSeconds, final EXISTINGPROPS_MODE... syncMode);

    void setCellarServiceSparqlTimeoutConnections(int cellarServiceSparqlTimeoutConnections, EXISTINGPROPS_MODE... syncMode);

    /**
     * Checks if is virtuoso ingestion linkrot realignment enabled.
     *
     * @return true, if is virtuoso ingestion linkrot realignment enabled
     */
    boolean isVirtuosoIngestionLinkrotRealignmentEnabled();

    /**
     * Sets the virtuoso ingestion linkrot realignment enabled.
     *
     * @param virtuosoIngestionLinkrotRealignmentEnabled the virtuoso ingestion linkrot realignment enabled
     * @param syncMode                                   the sync mode
     */
    void setVirtuosoIngestionLinkrotRealignmentEnabled(final boolean virtuosoIngestionLinkrotRealignmentEnabled,
                                                       final EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the log verbose for the backup during Virtuoso ingestions.
     *
     * @param virtuosoIngestionBackupLogVerbose the log verbose for the backup during Virtuoso ingestions
     * @param syncMode                          the sync mode
     */
    void setVirtuosoIngestionBackupLogVerbose(boolean virtuosoIngestionBackupLogVerbose, EXISTINGPROPS_MODE... syncMode);

    void setCellarServiceEmailEnabled(boolean enabled,
                                      EXISTINGPROPS_MODE... syncMode);

    boolean getCellarServiceEmailEnabled();

    /**
     * Sets the maximum age of email entries stored in the database
     * after which they are considered eligible for removal/cleanup.
     * @param maxAgeDays the maximum age in days for email entries to remain in the database.
     * @param syncMode the sync mode.
     */
    void setCellarServiceEmailCleanupMaxAgeDays(int maxAgeDays, EXISTINGPROPS_MODE... syncMode);
    
    /**
     * Retrieves the maximum age in days for email entries to remain in the database.
     * @return the maximum age in days for email entries to remain in the database.
     */
    int getCellarServiceEmailCleanupMaxAgeDays();
    
    void setCellarServiceEuloginUserMigrationEnabled(boolean enabled,
                                                     EXISTINGPROPS_MODE... syncMode);

    boolean getCellarServiceEuloginUserMigrationEnabled();

    /**
     * Checks if S3 retry mechanism is enabled.
     *
     * @return true, if S3 retry mechanism is enabled
     */
    boolean isS3RetryEnabled();

    /**
     * Sets the S3 retry mechanism enabled.
     *
     * @param s3RetryEnabled the cellar service ingestion enabled
     * @param syncMode                      the sync mode
     */
    void setS3RetryEnabled(boolean s3RetryEnabled, EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the max attempts to retry (including the first failure) for the retry strategy of S3.
     *
     * @return the max attempts to retry (including the first failure) for the retry strategy of S3
     */
    int getS3RetryMaxAttempts();

    /**
     * Sets the max attempts to retry (including the first failure) for the retry strategy of S3.
     *
     * @param s3RetryMaxAttempts the max attempts to retry (including the first failure) for the retry strategy of S3
     * @param syncMode                         the sync mode
     */
    void setS3RetryMaxAttempts(int s3RetryMaxAttempts, EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the delay for the retry strategy of S3.
     *
     * @return the delay for the retry strategy of S3
     */
    long getS3RetryDelay();

    /**
     * Sets the delay for the retry strategy of S3.
     *
     * @param s3RetryDelay the delay for the retry strategy of S3.
     * @param syncMode                         the sync mode
     */
    void setS3RetryDelay(long s3RetryDelay, EXISTINGPROPS_MODE... syncMode);

    /**
     * Checks if S3 random is enabled for S3 retry mechanism.
     *
     * @return true, if S3 random is enabled for S3 retry mechanism
     */
    boolean isS3RetryRandom();

    /**
     * Sets the random for the retry strategy of S3.
     *
     * @param s3RetryRandom the delay for the retry strategy of S3.
     * @param syncMode                         the sync mode
     */
    void setS3RetryRandom(boolean s3RetryRandom, EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the multiplier for the retry strategy of S3.
     *
     * @return the multiplier for the retry strategy of S3
     */
    int getS3RetryMultiplier();

    /**
     * Sets the multiplier for the retry strategy of S3.
     *
     * @param s3RetryMultiplier the multiplier for the retry strategy of S3.
     * @param syncMode                         the sync mode
     */
    void setS3RetryMultiplier(int s3RetryMultiplier, EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the max delay for the retry strategy of S3.
     *
     * @return the max delay for the retry strategy of S3
     */
    long getS3RetryMaxDelay();

    /**
     * Sets the max delay for the retry strategy of S3.
     *
     * @param s3RetryDelay the max delay for the retry strategy of S3.
     * @param syncMode                         the sync mode
     */
    void setS3RetryMaxDelay(long s3RetryDelay, EXISTINGPROPS_MODE... syncMode);

    /**
     * Sets the virtuoso ingestion backup exception messages cache period.
     *
     * @param virtuosoIngestionBackupExceptionMessagesCachePeriod the virtuoso ingestion backup exception messages cache period
     * @param syncMode                                            the sync mode
     */
    void setVirtuosoIngestionBackupExceptionMessagesCachePeriod(final String virtuosoIngestionBackupExceptionMessagesCachePeriod,
                                                                final EXISTINGPROPS_MODE... syncMode);

    /**
     * Gets the virtuoso ingestion backup exception messages cache period.
     *
     * @return the virtuoso ingestion backup exception messages cache period
     */
    String getVirtuosoIngestionBackupExceptionMessagesCachePeriod();


    void setGracefulShutdownEnabled(boolean enabled, EXISTINGPROPS_MODE... syncMode);

    boolean isGracefulShutdownEnabled();

    void setGracefulShutdownTimeout(long timeout, EXISTINGPROPS_MODE... syncMode);

    long getGracefulShutdownTimeout();

    void setCellarWatchAdviceEnabled(boolean enabled, EXISTINGPROPS_MODE... syncMode);

    boolean isCellarWatchAdviceEnabled();

    void setCellarServiceNalLoadInferenceMaxIteration(int cellarServiceNalLoadInferenceMaxIteration, final EXISTINGPROPS_MODE... syncMode);

    int getCellarServiceNalLoadInferenceMaxIteration();


    void setInferenceOntologyPath(String path, EXISTINGPROPS_MODE... syncMode);

    String getInferenceOntologyPath();

    String getS3Url();

    void setS3Url(String url, EXISTINGPROPS_MODE... syncMode);

    String getS3Bucket();

    void setS3Bucket(String bucketName, EXISTINGPROPS_MODE... syncMode);

    String getS3Region();

    void setS3Region(String regionName, EXISTINGPROPS_MODE... syncMode);

    void setS3CredentialsAccessKey(String accessKey, EXISTINGPROPS_MODE... syncMode);

    String getS3CredentialsAccessKey();

    void setS3CredentialsSecretKey(String secretKey, EXISTINGPROPS_MODE... syncMode);

    String getS3CredentialsSecretKey();

    void setS3ClientConnectionTimeout(int timeout, EXISTINGPROPS_MODE... syncMode);

    int getS3ClientConnectionTimeout();

    void setS3ClientSocketTimeout(int timeout, EXISTINGPROPS_MODE... syncMode);

    int getS3ClientSocketTimeout();

    void setProxyEnabled(boolean enabled, EXISTINGPROPS_MODE... syncMode);

    boolean isProxyEnabled();

    void setProxyHost(String host, EXISTINGPROPS_MODE... syncMode);

    String getProxyHost();

    void setProxyPort(int port, EXISTINGPROPS_MODE... syncMode);

    int getProxyPort();

    void setProxyUser(String username, EXISTINGPROPS_MODE... syncMode);

    String getProxyUser();

    void setProxyPassword(String password, EXISTINGPROPS_MODE... syncMode);

    String getProxyPassword();

    boolean isS3Unsafe();

    void setS3Unsafe(boolean unsafe, EXISTINGPROPS_MODE... syncMode);

    ImmutableMap<String, String> getS3RollbackTag();

    void setS3RollbackTag(String enabled, EXISTINGPROPS_MODE... syncMode);

    int getCellarMetsUploadCallbackTimeout();
    void setCellarMetsUploadCallbackTimeout(int cellarMetsUploadCallbackTimeout, EXISTINGPROPS_MODE... syncMode);
    String getCellarStatusServiceBaseUrl();
    void setCellarStatusServiceBaseUrl(String path, EXISTINGPROPS_MODE... syncMode);
    String getCellarStatusServicePackageLevelEndpoint();
    void setCellarStatusServicePackageLevelEndpoint(String path, EXISTINGPROPS_MODE... syncMode);
    String getCellarStatusServiceObjectLevelEndpoint();
    void setCellarStatusServiceObjectLevelEndpoint(String path, EXISTINGPROPS_MODE... syncMode);
    String getCellarStatusServiceLookupEndpoint();
    void setCellarStatusServiceLookupEndpoint(String path, EXISTINGPROPS_MODE... syncMode);

    void setCellarServiceUserAccessNotificationEmailEnabled(boolean enabled, EXISTINGPROPS_MODE... syncMode);

    boolean getCellarServiceUserAccessNotificationEmailEnabled();

    void setCellarEmailJobCron(String cron, EXISTINGPROPS_MODE... syncMode);
   
    String getCellarEmailJobCron();

    String getCellarFeedIngestionDescription();

    String getCellarFeedEmbargoDescription();

    String getCellarFeedNalDescription();

    String getCellarFeedSparqlLoadDescription();

    String getCellarFeedOntologyDescription();

    void setCellarFeedIngestionDescription(String cellarFeedIngestionDescription,EXISTINGPROPS_MODE... syncMode);

    void setCellarFeedEmbargoDescription(String cellarFeedEmbargoDescription,EXISTINGPROPS_MODE... syncMode);

    void setCellarFeedNalDescription(String cellarFeedNalDescription,EXISTINGPROPS_MODE... syncMode);

    void setCellarFeedSparqlLoadDescription(String cellarFeedSparqlLoadDescription,EXISTINGPROPS_MODE... syncMode);

    void setCellarFeedOntologyDescription(String cellarFeedOntologyDescription,EXISTINGPROPS_MODE... syncMode);

    String getCellarFeedIngestionTitle();

    String getCellarFeedEmbargoTitle();

    String getCellarFeedNalTitle();

    String getCellarFeedSparqlLoadTitle();

    String getCellarFeedOntologyTitle();

    void setCellarFeedIngestionTitle(String cellarFeedIngestionTitle,EXISTINGPROPS_MODE... syncMode);

    void setCellarFeedEmbargoTitle(String cellarFeedEmbargoTitle,EXISTINGPROPS_MODE... syncMode);

    void setCellarFeedNalTitle(String cellarFeedNalTitle,EXISTINGPROPS_MODE... syncMode);

    void setCellarFeedSparqlLoadTitle(String cellarFeedSparqlLoadTitle,EXISTINGPROPS_MODE... syncMode);

    void setCellarFeedOntologyTitle(String cellarFeedOntologyTitle,EXISTINGPROPS_MODE... syncMode);

    String getCellarFeedIngestionItemTitle();

    String getCellarFeedEmbargoItemTitle();

    String getCellarFeedNalItemTitle();

    String getCellarFeedSparqlLoadItemTitle();

    String getCellarFeedOntologyItemTitle();

    String getCellarFeedIngestionEntrySummary();

    String getCellarFeedEmbargoEntrySummary();

    String getCellarFeedOntologyEntrySummary();

    String getCellarFeedNalEntrySummary();

    String getCellarFeedSparqlLoadEntrySummary();

    void setCellarFeedIngestionEntrySummary(String cellarFeedIngestionEntrySummary,EXISTINGPROPS_MODE... syncMode);

    void setCellarFeedEmbargoEntrySummary(String cellarFeedEmbargoEntrySummary,EXISTINGPROPS_MODE... syncMode);

    void setCellarFeedOntologyEntrySummary(String cellarFeedOntologyEntrySummary,EXISTINGPROPS_MODE... syncMode);

    void setCellarFeedNalEntrySummary(String cellarFeedNalEntrySummary,EXISTINGPROPS_MODE... syncMode);

    void setCellarFeedSparqlLoadEntrySummary(String cellarFeedSparqlLoadEntrySummary,EXISTINGPROPS_MODE... syncMode);

    void setCellarFeedIngestionItemTitle(String cellarFeedIngestionItemTitle,EXISTINGPROPS_MODE... syncMode);

    void setCellarFeedEmbargoItemTitle(String cellarFeedEmbargoItemTitle,EXISTINGPROPS_MODE... syncMode);

    void setCellarFeedNalItemTitle(String cellarFeedNalItemTitle,EXISTINGPROPS_MODE... syncMode);

    void setCellarFeedSparqlLoadItemTitle(String cellarFeedSparqlLoadItemTitle,EXISTINGPROPS_MODE... syncMode);

    void setCellarFeedOntologyItemTitle(String cellarFeedOntologyItemTitle,EXISTINGPROPS_MODE... syncMode);

    void setDebugVerboseLoggingEnabled(boolean enabled,EXISTINGPROPS_MODE... syncMode);

    boolean isDebugVerboseLoggingEnabled();
    
    public void setCellarMailHost(String host, EXISTINGPROPS_MODE... syncMode);
    
    public String getCellarMailHost();
    
    public void setCellarMailPort(int port, EXISTINGPROPS_MODE... syncMode);
    
    public int getCellarMailPort();
    
    public void setCellarMailSmtpUser(String user, EXISTINGPROPS_MODE... syncMode);
    
    public String getCellarMailSmtpUser();
    
    public void setCellarMailSmtpPassword(String password, EXISTINGPROPS_MODE... syncMode);
    
    public String getCellarMailSmtpPassword();
    
    public void setCellarMailSmtpAuth(boolean enabled, EXISTINGPROPS_MODE... syncMode);
    
    public boolean getCellarMailSmtpAuth();
    
    public void setCellarMailSmtpStarttlsEnable(boolean enabled, EXISTINGPROPS_MODE... syncMode);
    
    public boolean getCellarMailSmtpStarttlsEnable();
   
    public void setCellarMailSmtpStarttlsRequired(boolean required, EXISTINGPROPS_MODE... syncMode);
    
    public boolean getCellarMailSmtpStarttlsRequired();
    
    public void setCellarMailSmtpFrom(String mailFrom,EXISTINGPROPS_MODE... syncMode);
    
    public String getCellarMailSmtpFrom();

    public void setCellarMetsUploadBufferSize(Integer bufferSize,EXISTINGPROPS_MODE... syncMode);

    public Integer getCellarMetsUploadBufferSize();
}
