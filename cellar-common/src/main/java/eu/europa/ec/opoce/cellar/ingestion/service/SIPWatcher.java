/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.ingestion.service
 *        FILE : SIPWatcher.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 21-11-2021
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2021 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ingestion.service;


/**
 * Service for identifying new packages to be processed by the SIP
 * dependency checker service, and packages that need to be
 * removed from the database.
 * @author EUROPEAN DYNAMICS S.A.
 */
public interface SIPWatcher {
	
	/**
	 * Wrapper method for asynchronously invoking {@code watchFolders()}.
	 */
	void watchFoldersAsync();
	
	/**
	 * Periodically checks the contents of the ingestion folders in order
	 * to identify packages that need to be processed by the dependency checker executor,
	 * or packages that are no longer needed in the DB and should be removed.
	 */
	void watchFolders();
	
	/**
	 * Reconfigures the parameters of the dependency checker executor
	 * when they are updated from the admin page.
	 */
	void reconfigureThreadPoolExecutor();
	
}
