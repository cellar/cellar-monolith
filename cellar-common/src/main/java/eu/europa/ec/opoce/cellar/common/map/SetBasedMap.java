/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.map
 *             FILE : MutualSetMap.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 19-08-2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.map;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * <class_description> This is an implementation of {@link Map} based on the entries of a {@link Set} that works as described below.<br/>
 * Consider a set populated with <code>{a, b, c}</code>:
 * <ul>
 *   <li>if the parameter {@link exclusive} is false, then the resulting map is populated with <code>{a=[a, b, c], b=[a, b, c], c=[a, b, c]}</code></li>
 *   <li>if the parameter {@link exclusive} is true, then the resulting map is populated with <code>{a=[b, c], b=[a, c], c=[a, b]}</code></li>
 * </ul>
 * <br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 29, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@SuppressWarnings("serial")
public class SetBasedMap<K> extends HashMap<K, Set<K>> {

    public SetBasedMap() {
        super();
    }

    public SetBasedMap(final Set<K> keys) {
        this(keys, false);
    }

    public SetBasedMap(final Set<K> keys, final boolean exclusive) {
        this.add(keys, exclusive);
    }

    public void add(final Set<K> keys) {
        this.add(keys, false);
    }

    public void add(final Set<K> keys, final boolean exclusive) {
        for (final K key : keys) {
            Set<K> currKeys = this.get(key);
            if (currKeys == null) {
                currKeys = new HashSet<K>();
                this.put(key, currKeys);
            }
            for (final K internKey : keys) {
                if (!exclusive || internKey != key) {
                    currKeys.add(internKey);
                }
            }
        }
    }

}
