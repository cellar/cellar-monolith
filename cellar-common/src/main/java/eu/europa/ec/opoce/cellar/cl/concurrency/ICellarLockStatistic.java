/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.concurrency
 *             FILE : ILockManagerStatistic.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : May 3, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.concurrency;

import java.util.Set;

/**
 * <class_description> Cellar lock statistics.
 * <br/><br/>
 * ON : May 3, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface ICellarLockStatistic {

    /**
     * Return the resource identifier.
     * @return the resource identifier
     */
    String getResourceIdentifier();

    /**
     * Return the number of locks.
     * @return the number of locks
     */
    int getLockCount();

    /**
     * Return the lock object unique reference.
     * @return the lock object unique reference
     */
    String getLockObjectRef();

    /**
    * Return the CELLAR lock sessions.
    */
    Set<ICellarLockSession> getCellarLockSessions();
}
