/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.retry.impl
 *             FILE : ExponentialBackoffRetryStrategy.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 15-07-2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.retry.impl;

import java.security.SecureRandom;

/**
 * <class_description> An implementation of {@link eu.europa.ec.opoce.cellar.common.retry.impl.BaseRetryStrategy} which drives the fire-and-retry mechanism
 * on the basis of the <b>Exponential Backoff</b> pattern, which is the best way for randomizing the retry intervals.<br/>
 * Between each retry, an amount of time based on the formula <code>{@link base}+{@link base}*R*X</code> is waited, where:
 * <ul>
 *   <li>{@link base} is a value expressed in milliseconds: it should be tuned to the particular hardware and network setup in order to attain maximum efficiency,
 *   that is, the minimum number of retries with the minimum interval between retries (default is 3000)</li>
 *   <li>R is a random floating number between 0 (included) and 1 (excluded)</li>
 *   <li>X is the current retry number</li>
 * </ul>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 15-07-2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class ExponentialBackoffRetryStrategy<P> extends BaseRetryStrategy<P> {

    /**
     * @see eu.europa.ec.opoce.cellar.common.retry.impl.BaseRetryStrategy#calculateDelay(int)
     */
    @Override
    protected long calculateDelay(int currentRetry) {
        SecureRandom random = new SecureRandom();
        return this.base + (long) (this.base * random.nextDouble() * (currentRetry + 1));
    }

}
