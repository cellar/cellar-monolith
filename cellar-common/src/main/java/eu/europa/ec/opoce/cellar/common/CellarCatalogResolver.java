package eu.europa.ec.opoce.cellar.common;

import org.apache.xerces.util.XMLCatalogResolver;

import java.io.IOException;
import java.io.UncheckedIOException;

public class CellarCatalogResolver {

    public static XMLCatalogResolver getInstance() {
        return ServiceLocator.getService(CatalogService.class).getCatalog();
    }

    public static CatalogResolver adapt(XMLCatalogResolver xmlCatalogResolver) {
        return key -> {
            try {
                return xmlCatalogResolver.resolveURI(key);
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        };
    }

}
