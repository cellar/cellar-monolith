package eu.europa.ec.opoce.cellar.cl.domain.structmap_pid;

import eu.europa.ec.opoce.cellar.cl.domain.structmap_status_history.StructMapStatusHistory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * <class_description> StructMap Pid object.
 * <br/><br/>
 * <notes> Information about the production identifiers of the root level entity of a structmap
 * <br/><br/>
 * ON : 15-06-2023
 *
 * @author EUROPEAN DYNAMICS S.A.
 */
@Entity
@Table(name = "STRUCTMAP_PID")
@NamedQuery(name = "StructMapPid.findByStructMapStatusHistory",
        query = "SELECT spid FROM StructMapPid spid WHERE spid.structMapStatusHistory = :spidStructMapStatusHistory")
public class StructMapPid {
    
    /**
     * Primary key.
     */
    @Id
    @GeneratedValue
    private Long id;
    
    /**
     * Structmap the production identifier is mentioned in.
     */
    @NotNull
    @ManyToOne
    @JoinColumn(name = "STRUCTMAP_ID")
    private StructMapStatusHistory structMapStatusHistory;
    
    /**
     * Production identifier of the root level entity.
     */
    @NotNull
    @Column(name = "PRODUCTION_ID")
    private String productionId;
    
    /**
     * Default constructor.
     */
    public StructMapPid() {
    }
    
    /**
     * Create a new <code>StructMapPid</code> instance.
     * @param structMapStatusHistory the structmap in question
     * @param productionId the production identifier of the specific root entity
     */
    public StructMapPid(StructMapStatusHistory structMapStatusHistory, String productionId) {
        this.structMapStatusHistory = structMapStatusHistory;
        this.productionId = productionId;
    }
    
    public Long getId() {
        return id;
    }
    
    public StructMapStatusHistory getStructMapStatusHistory() {
        return structMapStatusHistory;
    }
    
    public String getProductionId() {
        return productionId;
    }
    
    public void setStructMapStatusHistory(StructMapStatusHistory structMapStatusHistory) {
        this.structMapStatusHistory = structMapStatusHistory;
    }
    
    public void setProductionId(String productionId) {
        this.productionId = productionId;
    }
    
    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "StructMapPid { "
                + "id = '" + this.getId() + "', "
                + "structmap id = '" + this.getStructMapStatusHistory().getId() + "', "
                + "production id = '" + this.getProductionId()
                + " }";
    }
}
