package eu.europa.ec.opoce.cellar.cl.domain.structmap_status_history;

import eu.europa.ec.opoce.cellar.cl.domain.enums.IndexExecutionStatus;
import eu.europa.ec.opoce.cellar.cl.domain.enums.ProcessStatus;
import eu.europa.ec.opoce.cellar.cl.domain.package_history.PackageHistory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * <class_description> StructMap Status History object.
 * <br/><br/>
 * <notes> Information about the status of a structmap, such as 'name', ingestion-related
 *         and indexation-related info
 * <br/><br/>
 * ON : 14-06-2023
 *
 * @author EUROPEAN DYNAMICS S.A.
 */

@Entity
@Table(name = "STRUCTMAP_STATUS_HISTORY")
@NamedQuery(name = "StructMapStatusHistory.findByNameAndPackageHistory",
        query = "SELECT ssh FROM StructMapStatusHistory ssh WHERE ssh.structMapName = :sshStructMapName AND ssh.packageHistory = :sshPackageHistory")
@NamedQuery(name = "StructMapStatusHistory.findByPackageHistory",
        query = "SELECT ssh FROM StructMapStatusHistory ssh WHERE ssh.packageHistory = :sshPackageHistory")
public class StructMapStatusHistory {
    
    /**
     * Primary key.
     */
    @Id
    @GeneratedValue
    private Long id;
    
    /**
     * Structmap name.
     */
    @Column(name = "STRUCTMAP_NAME")
    private String structMapName;
    
    /**
     * Package the structmap belongs to.
     */
    @NotNull
    @ManyToOne
    @JoinColumn(name = "PACKAGE_ID")
    private PackageHistory packageHistory;
    
    /**
     * Ingestion status of the structmap.
     * Refers to status of sequence 'Create/Update/Delete/Append', 'ValidateDirectMetadata'
     * and 'ValidateTechnicalMetadata'.
     */
    @NotNull
    @Column(name = "INGESTION_STATUS")
    @Enumerated(EnumType.STRING)
    private ProcessStatus ingestionStatus;
    
    /**
     * Cellar Identifier of the root entity (if it already exists)
     */
    @Column(name = "CELLAR_ID")
    private String cellarId;
    
    /**
     * Indexation status of the 'notice' index request regarding the structmap.
     */
    @Column(name = "INDX_EXECUTION_STATUS")
    @Enumerated(EnumType.STRING)
    private IndexExecutionStatus indxExecutionStatus;
    
    /**
     * Datetime that the execution of index request ended at.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "INDX_EXECUTION_DATE")
    private Date indxExecutionDate;
    
    /**
     * Datetime that the execution of index request started at.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "INDX_EXECUTION_START_DATE")
    private Date indxExecutionStartDate;
    
    /**
     * Datetime that the index request was created at.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "INDX_CREATED_ON")
    private Date indxCreatedOnDate;
    
    /**
     * String containing the languages of the indexed notices in a comma-separated fashion.
     */
    @Column(name = "LANGUAGES")
    private String languages;
    
    /**
     * The SHACL report, in case there is an error in validation.
     */
    @Column(name = "SHACL_REPORT")
    private String shaclReport;
    
    
    /**
     * Default constructor.
     */
    public StructMapStatusHistory() {
    }
    
    /**
     * Create a new <code>StructMapStatusHistory</code> instance.
     * @param packageHistory the package containing this structmap
     * @param ingestionStatus the status of ingestion
     * @param indxExecutionStatus the status of indexation
     */
    public StructMapStatusHistory(PackageHistory packageHistory, ProcessStatus ingestionStatus, IndexExecutionStatus indxExecutionStatus) {
        this.packageHistory = packageHistory;
        this.ingestionStatus = ingestionStatus;
        this.indxExecutionStatus = indxExecutionStatus;
    }
    
    /**
     * Clone constructor
     * @param structMapStatusHistory the original object
     */
    public StructMapStatusHistory(StructMapStatusHistory structMapStatusHistory){
        this.id = structMapStatusHistory.getId();
        this.structMapName = structMapStatusHistory.getStructMapName();
        this.packageHistory = new PackageHistory(structMapStatusHistory.getPackageHistory());
        this.ingestionStatus = structMapStatusHistory.getIngestionStatus();
        this.cellarId = structMapStatusHistory.getCellarId();
        this.indxExecutionStatus = structMapStatusHistory.getIndxExecutionStatus();
        this.indxExecutionDate = (structMapStatusHistory.getIndxExecutionDate() != null
                ? new Date(structMapStatusHistory.getIndxExecutionDate().getTime()) : null);
        this.indxExecutionStartDate = (structMapStatusHistory.getIndxExecutionStartDate() != null
                ? new Date(structMapStatusHistory.getIndxExecutionStartDate().getTime()) : null);
        this.indxCreatedOnDate = (structMapStatusHistory.getIndxCreatedOnDate() != null
                ? new Date(structMapStatusHistory.getIndxCreatedOnDate().getTime()) : null);
        this.languages = structMapStatusHistory.getLanguages();
        this.shaclReport = structMapStatusHistory.getShaclReport();
    }
    
    public Long getId() {
        return id;
    }
    
    public String getStructMapName() { return structMapName; }
    
    public PackageHistory getPackageHistory() {
        return packageHistory;
    }
    
    public ProcessStatus getIngestionStatus() {
        return ingestionStatus;
    }
    
    public String getCellarId() {
        return cellarId;
    }
    
    public IndexExecutionStatus getIndxExecutionStatus() {
        return indxExecutionStatus;
    }
    
    public Date getIndxExecutionDate() {
        return indxExecutionDate;
    }
    
    public Date getIndxExecutionStartDate() {
        return indxExecutionStartDate;
    }
    
    public Date getIndxCreatedOnDate() {
        return indxCreatedOnDate;
    }
    
    public String getLanguages() {
        return languages;
    }
    
    public String getShaclReport() {
        return shaclReport;
    }
    
    public void setStructMapName(String structMapName) {
        this.structMapName = structMapName;
    }
    
    public void setPackageHistory(PackageHistory packageHistory) {
        this.packageHistory = packageHistory;
    }
    
    public void setIngestionStatus(ProcessStatus ingestionStatus) {
        this.ingestionStatus = ingestionStatus;
    }
    
    public void setCellarId(String cellarId) {
        this.cellarId = cellarId;
    }
    
    public void setIndxExecutionStatus(IndexExecutionStatus indxExecutionStatus) {
        this.indxExecutionStatus = indxExecutionStatus;
    }
    
    public void setIndxExecutionDate(Date indxExecutionDate) {
        this.indxExecutionDate = indxExecutionDate;
    }
    
    public void setIndxExecutionStartDate(Date indxExecutionStartDate) {
        this.indxExecutionStartDate = indxExecutionStartDate;
    }
    
    public void setIndxCreatedOnDate(Date indxCreatedOnDate) {
        this.indxCreatedOnDate = indxCreatedOnDate;
    }
    
    public void setLanguages(String languages) {
        this.languages = languages;
    }
    
    public void setShaclReport(String shaclReport) {
        this.shaclReport = shaclReport;
    }
    
    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "StructMapStatusHistory { "
                + "id = '" + this.getId() + "', "
                + (this.getStructMapName() != null ? "structmap name = '" + this.getStructMapName() + "', " : "")
                + "package id = '" + this.getPackageHistory().getId() + "', "
                + "ingestion status = '" + this.getIngestionStatus() + "', "
                + (this.getCellarId() != null ? "cellar id = '" + this.getCellarId() + "', " : "")
                + (this.getIndxExecutionStatus() != null ? "indx execution status = '" + this.getIndxExecutionStatus() + "', " : "")
                + (this.getIndxExecutionDate() != null ? "indx execution date = '"
                + this.getIndxExecutionDate() + "', " : "")
                + (this.getIndxExecutionStartDate() != null ? "indx execution start date = '"
                + this.getIndxExecutionStartDate() + "', " : "")
                + (this.getIndxCreatedOnDate() != null ? "indx created on date = '"
                + this.getIndxCreatedOnDate() + "', " : "")
                + (this.getLanguages() != null ? "languages = '" + this.getLanguages() + "', " : "")
                + (this.getShaclReport() != null ? "shacl report = '" + this.getShaclReport() : "")
                + " }";
    }
}
