package eu.europa.ec.opoce.cellar.cl.domain.enums;

/**
 * <class_description> Available statuses for INDX_EXECUTION_STATUS field of STRUCTMAP_STATUS_HISTORY table
 * of CELLAROWNER schema.
 * <br/><br/>
 * <notes> Describes the status of the respective process.
 * <br/><br/>
 * ON : 30-08-2023
 *
 * @author EUROPEAN DYNAMICS S.A.
 */
public enum IndexExecutionStatus {
    
    /**
     * Available statuses
     */
    N("New"),
    P("Pending"),
    X("Execution"),
    D("Done"),
    E("Error"),
    R("Redundant");
    
    /**
     * Explanation of status
     */
    private final String explanation;
    
    /**
     * Enum constructor
     */
    IndexExecutionStatus(String explanation) {
        this.explanation = explanation;
    }
    
    /**
     * Getter - explanation
     */
    public String getExplanation() {
        return this.explanation;
    }
}
