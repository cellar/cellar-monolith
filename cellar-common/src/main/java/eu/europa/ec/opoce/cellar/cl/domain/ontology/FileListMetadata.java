package eu.europa.ec.opoce.cellar.cl.domain.ontology;

import eu.europa.ec.opoce.cellar.cl.domain.message.Message;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * The Class FileListMetadata.
 * used for the response of the jquery file upload plugin
 */
public class FileListMetadata implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The message. */
    private List<Message> messages;

    /** The inError. */
    private boolean inError;

    /** The files. */
    private List<FileMetadata> files;

    public FileListMetadata() {

    }

    public FileListMetadata(List<FileMetadata> metadata) {
        this.files = metadata;
    }

    /**
     * Gets the files.
     *
     * @return the files
     */
    public List<FileMetadata> getFiles() {
        return this.files;
    }

    /**
     * Sets the files.
     *
     * @param files the new files
     */
    public void setFiles(final LinkedList<FileMetadata> files) {
        this.files = files;
    }

    /**
     * Gets the messages.
     *
     * @return the messages
     */
    public List<Message> getMessages() {
        return messages;
    }

    /**
     * Sets the messages.
     *
     * @param messages the messages
     */
    public void setMessages(List<Message> messages) {
        this.messages = messages;
    }

    /**
     * Gets the inError.
     *
     * @return the inError
     */
    public boolean isInError() {
        return inError;
    }

    /**
     * Sets the inError.
     *
     * @param inError the inError
     */
    public void setInError(boolean inError) {
        this.inError = inError;
    }
}
