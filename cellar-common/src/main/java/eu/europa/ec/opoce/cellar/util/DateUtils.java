/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.feed.controller
 *             FILE : BaseNotificationController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 1, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-01-02 11:32:44 +0100 (Mon, 02 Jan 2017) $
 *          VERSION : $LastChangedRevision: 12298 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.util;

import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.FastDateFormat;

import java.text.ParseException;
import java.util.Date;

/**
 * The base notification controller: all controllers aimed at notifying feeds must be based upon this class.
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 20 juin 2016
 *
 * @author ARHS Developments
 * @version $Revision: 12298 $
 */
public final class DateUtils {
    private DateUtils() {

    }

    private static final FastDateFormat DATABASE_DATE_FORMAT = FastDateFormat.getInstance("yyyy-MM-dd'T'HH:mm:ss.SSSZZ");

    private static final FastDateFormat ADMIN_INTERFACE_DATE_FORMAT = FastDateFormat.getInstance("yyyy-MM-dd HH:mm:ss");

    private static final String[] VALID_DATE_PATTERNS = new String[] {
            DateFormatUtils.ISO_DATE_FORMAT.getPattern(), // yyyy-MM-dd --> 2013-12-02
            DateFormatUtils.ISO_DATETIME_FORMAT.getPattern(), // yyyy-MM-dd'T'HH:mm:ss --> 2013-12-02T09:24:22
            DateFormatUtils.ISO_DATETIME_TIME_ZONE_FORMAT.getPattern(), // yyyy-MM-dd'T'HH:mm:ssZZ --> 2013-12-02T09:24:22-01:00
            ADMIN_INTERFACE_DATE_FORMAT.getPattern(), // yyyy-MM-dd HH:mm:ss --> 2013-12-02 09:24:22
            DATABASE_DATE_FORMAT.getPattern() //yyyy-MM-dd'T'HH:mm:ss.SSSZZ --> 2013-12-02T09:24:22.123-01:00
    };

    public static final String ACCEPT_RSS = "application/rss+xml";
    public static final String ACCEPT_ATOM = "application/atom+xml";
    public static final String ACCEPT_ALL = "*/*";

    public static String formatDate(final Date date) {
        if (date == null) {
            return null;
        }

        return DATABASE_DATE_FORMAT.format(date);
    }

    public static Date parseDate(final String dateStr) throws CellarException {
        if (dateStr == null) {
            return null;
        }

        Date retDate;
        try {
            retDate = org.apache.commons.lang.time.DateUtils.parseDateStrictly(dateStr, DateUtils.VALID_DATE_PATTERNS);
        } catch (ParseException e) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Format of date {} is not compatible with any of patterns {}.")
                    .withMessageArgs(dateStr, VALID_DATE_PATTERNS).build();
        }
        return retDate;
    }

}
