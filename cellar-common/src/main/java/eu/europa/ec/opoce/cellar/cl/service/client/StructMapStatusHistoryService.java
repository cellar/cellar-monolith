package eu.europa.ec.opoce.cellar.cl.service.client;

import eu.europa.ec.opoce.cellar.cl.domain.enums.IndexExecutionStatus;
import eu.europa.ec.opoce.cellar.cl.domain.enums.ProcessStatus;
import eu.europa.ec.opoce.cellar.cl.domain.package_history.PackageHistory;
import eu.europa.ec.opoce.cellar.cl.domain.structmap_status_history.StructMapStatusHistory;

import java.util.Date;
import java.util.List;

/**
 * <class_description> Service interface for logging entries in the 'STRUCTMAP_STATUS_HISTORY' table.
 *
 * ON : 15-06-2023
 *
 * @author EUROPEAN DYNAMICS S.A.
 */
public interface StructMapStatusHistoryService {
    
    /**
     * Save an instance of {@link StructMapStatusHistory}.
     *
     * @return corresponding {@link StructMapStatusHistory} that was saved (or the null in case of exception).
     */
    StructMapStatusHistory saveEntry(StructMapStatusHistory structMapStatusHistory);
    
    /**
     * Return a persistent instance of {@link StructMapStatusHistory} based on its primary key.
     *
     * @param id identifier of {@link StructMapStatusHistory}
     *
     * @return corresponding {@link StructMapStatusHistory} (or the null in case of exception).
     */
    StructMapStatusHistory getEntry(Long id);
    
    /**
     * Return all persistent instances of {@link StructMapStatusHistory} belonging to the same {@link PackageHistory}.
     *
     * @param packageHistory the {@link PackageHistory} the structmaps belong to.
     *
     * @return the list of {@link StructMapStatusHistory} references matching the provided argument.
     */
    List<StructMapStatusHistory> getByPackageHistory(PackageHistory packageHistory);
    
    /**
     * Set the INGESTION_STATUS field of all {@link StructMapStatusHistory} entries belonging to the same {@link PackageHistory} provided.
     *
     * @param packageHistory the {@link PackageHistory} the structmaps belong to.
     * @param newStatus the status to set.
     */
    void updateAllStructMapIngestionStatus(PackageHistory packageHistory, ProcessStatus newStatus);
    
    /**
     * Set the INGESTION_STATUS field of multiple {@link StructMapStatusHistory} entries provided.
     *
     * @param structMapStatusHistories the corresponding {@link StructMapStatusHistory} entries.
     * @param newStatus the status to set.
     */
    void updateMultipleStructMapIngestionStatus(List<StructMapStatusHistory> structMapStatusHistories, ProcessStatus newStatus);
    
    /**
     * Set the INGESTION_STATUS field of the defined {@link StructMapStatusHistory} entry to the status provided.
     *
     * @param structMapStatusHistory the current 'STRUCTMAP_STATUS_HISTORY' object.
     * @param newStatus the status to set.
     *
     * @return the {@link StructMapStatusHistory} that was updated (or the original in case of exception)
     */
    StructMapStatusHistory updateStructMapIngestionStatus(StructMapStatusHistory structMapStatusHistory, ProcessStatus newStatus);
    
    /**
     * Set the CELLAR_ID field of the defined {@link StructMapStatusHistory} entry to the one provided.
     *
     * @param structMapStatusHistory the current 'STRUCTMAP_STATUS_HISTORY' object.
     * @param cellarId the CELLAR_ID to set.
     *
     * @return the {@link StructMapStatusHistory} that was updated (or the original in case of exception)
     */
    StructMapStatusHistory updateStructMapCellarId(StructMapStatusHistory structMapStatusHistory, String cellarId);
    
    /**
     * Set the SHACL_REPORT field of the defined {@link StructMapStatusHistory} entry.
     *
     * @param structMapStatusHistory the current 'STRUCTMAP_STATUS_HISTORY' object.
     * @param shaclReport the provided String report.
     *
     * @return the {@link StructMapStatusHistory} that was updated (or the original in case of exception)
     */
    StructMapStatusHistory updateStructMapShaclReport(StructMapStatusHistory structMapStatusHistory, String shaclReport);
    
    /**
     * Set the INDX_EXECUTION_STATUS ('NEW') and the INDX_CREATED_ON fields of the defined {@link StructMapStatusHistory} entry.
     *
     * @param structMapStatusHistory the current 'STRUCTMAP_STATUS_HISTORY' object.
     * @param createdOnDate the 'createdOn' date.
     *
     * @return the {@link StructMapStatusHistory} that was updated (or the original in case of exception)
     */
    StructMapStatusHistory updateStructMapIndxStatusAndCreatedOn(StructMapStatusHistory structMapStatusHistory, Date createdOnDate);
    
    /**
     * Set the INDX_EXECUTION_STATUS  ('PENDING') of the defined {@link StructMapStatusHistory} entry.
     *
     * @param structMapStatusHistory the current 'STRUCTMAP_STATUS_HISTORY' object.
     * @return the {@link StructMapStatusHistory} that was updated (or the original in case of exception)
     */
    StructMapStatusHistory updateStructMapIndxStatusToPending(StructMapStatusHistory structMapStatusHistory);
    
    /**
     * Set the INDX_EXECUTION_STATUS ('RUNNING') and the INDX_EXECUTION_START_DATE fields of the defined {@link StructMapStatusHistory} entry.
     *
     * @param structMapStatusHistory the current 'STRUCTMAP_STATUS_HISTORY' object.
     * @param executionStartDate the 'execution start' date.
     *
     * @return the {@link StructMapStatusHistory} that was updated (or the original in case of exception)
     */
    StructMapStatusHistory updateStructMapIndxStatusAndExecutionStartDate(StructMapStatusHistory structMapStatusHistory, Date executionStartDate);
    
    /**
     * Set the INDX_EXECUTION_DATE and INDX_EXECUTION_STATUS fields of the defined {@link StructMapStatusHistory} entry.
     *
     * @param structMapStatusHistory the current 'STRUCTMAP_STATUS_HISTORY' object.
     * @param executionDate the 'execution' date.
     * @param newExecutionStatus new indexation status to set
     *
     * @return the {@link StructMapStatusHistory} that was updated (or the original in case of exception)
     */
    StructMapStatusHistory updateStructMapIndxStatusAndExecutionDate(StructMapStatusHistory structMapStatusHistory, Date executionDate, IndexExecutionStatus newExecutionStatus);
    
    /**
     * Set the LANGUAGES field of the defined {@link StructMapStatusHistory} entry.
     *
     * @param structMapStatusHistory the current 'STRUCTMAP_STATUS_HISTORY' object.
     * @param languages the comma-separated string of the languages of the expanded notices
     *
     * @return the {@link StructMapStatusHistory} that was updated (or the original in case of exception)
     */
    StructMapStatusHistory updateStructMapLanguages(StructMapStatusHistory structMapStatusHistory, String languages);
    
    /**
     * Reset the defined {@link StructMapStatusHistory} entry after FAILED ingestion.
     * (i.e. set INGESTION_STATUS to 'F',INDX_EXECUTION_STATUS to 'NA', INDX_CREATED_ON to 'null')
     *
     * @param structMapStatusHistory the current 'STRUCTMAP_STATUS_HISTORY' object.
     *
     * @return the {@link StructMapStatusHistory} that was updated (or the original in case of exception)
     */
    StructMapStatusHistory resetStructMapAfterFailedIngestion(StructMapStatusHistory structMapStatusHistory);
    
}
