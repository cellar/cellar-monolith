package eu.europa.ec.opoce.cellar.cl.domain.validator;

/**
 * Validator interface.
 * Each validator must implements this interface.
 * 
 * @author dcraeye
 */
public interface Validator {

    /**
     * Validates the given object.
     * 
     * @param validationDetails
     *            the {@link ValidationDetails} to validate.
    
     * @return a {@link ValidationResult}.
     */
    ValidationResult validate(final ValidationDetails validationDetails);
}
