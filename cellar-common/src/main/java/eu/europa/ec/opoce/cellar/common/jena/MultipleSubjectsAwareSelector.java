package eu.europa.ec.opoce.cellar.common.jena;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.SimpleSelector;
import org.apache.jena.rdf.model.Statement;

import java.util.HashSet;
import java.util.Set;

/**
 * <class_description> A simple selector to filter out all statements with the specified subjects.
 * <br/><br/>
 * <br/><br/>
 * ON : 12-Jan-2017
 * The Class MultipleSubjectsAwareSelector.
 *
 * @author ARHS Developments
 * @version $$Revision: 12424 $$$
 */
public class MultipleSubjectsAwareSelector extends SimpleSelector {

    /** The subjects. */
    private Set<Resource> subjects;

    /**
     * Instantiates a new multiple subjects aware selector.
     *
     * @param subjects the subjects
     * @param predicate the predicate
     * @param object the object
     */
    public MultipleSubjectsAwareSelector(final Set<Resource> subjects, final Property predicate, final String object) {
        super(null, predicate, object);
        this.subjects = subjects == null ? new HashSet<>() : subjects;
    }

    /** {@inheritDoc} */
    @Override
    public boolean test(final Statement s) {
        boolean test = false;

        for (final Resource subject : this.subjects) {
            this.subject = subject;
            test = super.test(s);
            if (test) {
                break;
            }
        }

        return test;
    }
}
