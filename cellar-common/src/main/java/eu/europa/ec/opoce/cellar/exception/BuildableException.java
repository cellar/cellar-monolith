/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.exception
 *        FILE : BuildableException.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 30-01-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.exception;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.MessageCode;
import org.apache.commons.lang.StringUtils;

import java.util.LinkedHashMap;
import java.util.Locale;

/**
 * This is the base class for all exceptions that can be built by an {@link ExceptionBuilder}.</br>
 * </br> 
 * ON : 30-01-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public abstract class BuildableException extends RuntimeException {

    protected ExceptionBuilder<? extends BuildableException> builder;
    protected LinkedHashMap<String, String> subHeaders;

    /**
     * Set private to avoid direct instantiation: any instantiation should be done by due builder
     */
    @SuppressWarnings("unused")
    private BuildableException() {
    }

    /**
     * Constructs a new exception with its associated builder
     * 
     * @param builder the builder to use for building the exception
     */
    protected BuildableException(final ExceptionBuilder<? extends BuildableException> builder) {
        super(builder.message, builder.cause);
        this.builder = builder;
        this.subHeaders = new LinkedHashMap<>();
        this.subHeaders.put("message", this.getMessage());
        this.subHeaders.put("metsDocumentId", this.getMetsDocumentId());
    }

    /**
     * It gets the exception's error code.
     * 
     * @return the exception's error code
     * @see {@link CommonErrors}
     */
    public MessageCode getCode() {
        return this.builder.code;
    }

    /**
     * It gets the exception's internationalized header.<br>
     * This header is internationalized out of the properties messages configured by property {@code ems}.
     * 
     * @return the exception's internationalized header
     */
    public String getHeader() {
        return this.builder.header;
    }

    /**
     * It gets the exception's METS document id.
     * 
     * @return the METS document id
     */
    public String getMetsDocumentId() {
        return this.builder.metsDocumentId;
    }

    /**
     * It returns true if the exception is set as silent, false otherwise.
     * 
     * @return true if the exception is set as silent, false otherwise
     * deprecation: There is no such thing that a silent exception. If the process needs
     * to finish, then use a logger not an exception that will be ignored.
     */
    @Deprecated
    public boolean isSilent() {
        return this.builder.silent;
    }

    /**
     * It returns true if the exception is set as visible in the stack trace, false otherwise.
     * 
     * @return true if the exception is set as visible in the stack trace, false otherwise
     */
    public boolean isVisibleOnStackTrace() {
        return this.builder.visibleOnStackTrace;
    }

    /**
     * It returns true if the exception is set as root: in this case, the exception is considered the main cause of the stack.<br>
     * If it has a cause, then that cause is treated as informative only, which means that it does not participate to the stack's severity.
     * 
     * @return true if the exception is set as as root, false otherwise
     */
    public boolean isRoot() {
        return this.builder.root;
    }

    /**
     * It builds and return a formatted message describing the exception.
     * 
     * @return the formatted message
     */
    @Override
    public String toString() {
        final StringBuilder retStr = new StringBuilder();

        retStr.append(this.getCode());
        if (StringUtils.isNotBlank(this.getHeader())) {
            retStr.append(": ").append(this.getHeader());
        }
        retStr.append(this.buildSubHeaders());

        return retStr.toString();
    }

    private String buildSubHeaders() {
        final StringBuilder retStr = new StringBuilder();

        boolean bracketOpen = false;
        for (String subHeaderKey : this.subHeaders.keySet()) {
            final String subHeaderValue = this.subHeaders.get(subHeaderKey);
            String subHeaderLabel = subHeaderKey;
            if (this.builder.getResourceBundle() != null) {
                subHeaderLabel = this.builder.getResourceBundle().getMessage("exception." + subHeaderKey, new Object[] {}, subHeaderKey,
                        Locale.getDefault());
            }
            if (StringUtils.isNotBlank(subHeaderValue)) {
                if (bracketOpen) {
                    retStr.append(", ");
                } else {
                    retStr.append(" [");
                    bracketOpen = true;
                }
                retStr.append(subHeaderLabel).append("=").append(subHeaderValue);
            }
        }
        if (bracketOpen) {
            retStr.append("]");
        }

        return retStr.toString();
    }

}
