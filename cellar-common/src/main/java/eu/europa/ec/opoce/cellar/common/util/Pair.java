package eu.europa.ec.opoce.cellar.common.util;

/**
 * <p>Pair class.</p>
 *
 */
public class Pair<T1, T2> {

    private final T1 one;
    private final T2 two;

    /**
     * <p>Constructor for Pair.</p>
     *
     * @param one a T1 object.
     * @param two a T2 object.
     * @param <T1> a T1 object.
     * @param <T2> a T2 object.
     */
    public Pair(T1 one, T2 two) {
        this.one = one;
        this.two = two;
    }

    /**
     * <p>Getter for the field <code>one</code>.</p>
     *
     * @return a T1 object.
     */
    public T1 getOne() {
        return one;
    }

    /**
     * <p>Getter for the field <code>two</code>.</p>
     *
     * @return a T2 object.
     */
    public T2 getTwo() {
        return two;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Pair<?, ?> pair = (Pair<?, ?>) o;

        if (one != null ? !one.equals(pair.one) : pair.one != null)
            return false;
        if (two != null ? !two.equals(pair.two) : pair.two != null)
            return false;

        return true;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        int result = one != null ? one.hashCode() : 0;
        return 31 * result + (two != null ? two.hashCode() : 0);
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return new StringBuilder().append("Pair (").append(one).append(", ").append(two).append(")").toString();
    }
}
