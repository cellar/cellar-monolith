/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.domain.content.mets
 *             FILE : DigitalObjectType.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 10-11-2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.domain.content.mets;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.common.Namespace;
import eu.europa.ec.opoce.cellar.common.util.TimeBasedUUIDGenerator;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.CellarValidationException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.NotImplementedExceptionBuilder;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 * <class_description> Enumeration for a {@link DigitalObject}.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 10-11-2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public enum DigitalObjectType {

    /**
     * The work.
     */
    WORK(null, 0) {
        @Override
        public <IN, OUT> OUT accept(final DigitalObjectTypeVisitor<IN, OUT> visitor, final IN in) {
            return visitor.visitWork(in);
        }
    },

    /**
     * The expression.
     */
    EXPRESSION(WORK, 1) {
        @Override
        public <IN, OUT> OUT accept(final DigitalObjectTypeVisitor<IN, OUT> visitor, final IN in) {
            return visitor.visitExpression(in);
        }
    },

    /**
     * The manifestation.
     */
    MANIFESTATION(EXPRESSION, 2) {
        @Override
        public <IN, OUT> OUT accept(final DigitalObjectTypeVisitor<IN, OUT> visitor, final IN in) {
            return visitor.visitManifestation(in);
        }
    },

    /**
     * The item.
     */
    ITEM(MANIFESTATION, 3, Integer.MAX_VALUE) {
        @Override
        public <IN, OUT> OUT accept(final DigitalObjectTypeVisitor<IN, OUT> visitor, final IN in) {
            return visitor.visitItem(in);
        }
    },

    /**
     * The dossier.
     */
    DOSSIER(null, 0) {
        @Override
        public <IN, OUT> OUT accept(final DigitalObjectTypeVisitor<IN, OUT> visitor, final IN in) {
            return visitor.visitDossier(in);
        }
    },

    /**
     * The event.
     */
    EVENT(DOSSIER, 1, Integer.MAX_VALUE) {
        @Override
        public <IN, OUT> OUT accept(final DigitalObjectTypeVisitor<IN, OUT> visitor, final IN in) {
            return visitor.visitEvent(in);
        }
    },

    /**
     * The agent.
     */
    AGENT(null, 0, Integer.MAX_VALUE) {
        @Override
        public <IN, OUT> OUT accept(final DigitalObjectTypeVisitor<IN, OUT> visitor, final IN in) {
            return visitor.visitAgent(in);
        }
    },

    /**
     * The toplevelevent.
     */
    TOPLEVELEVENT(null, 0, Integer.MAX_VALUE) {
        @Override
        public <IN, OUT> OUT accept(final DigitalObjectTypeVisitor<IN, OUT> visitor, final IN in) {
            return visitor.visitTopLevelEvent(in);
        }

        @Override
        public String getLabel() {
            return EVENT.getLabel();
        }
    },

    /**
     * The euronal.
     */
    EURONAL(null) {
        @Override
        public <IN, OUT> OUT accept(final DigitalObjectTypeVisitor<IN, OUT> visitor, final IN in) {
            return visitor.visitEuronal(in);
        }
    },

    /**
     * The undefined.
     */
    UNDEFINED(null);

    /**
     * Patterns to match WEMI objects.
     */
    private static final Pattern W_PATTERN = Pattern.compile("^" + ContentIdentifier.CELLAR_PREFIX + "\\:[a-zA-Z0-9\\-]*$");

    /**
     * The Constant E_PATTERN.
     */
    private static final Pattern E_PATTERN = Pattern.compile("^" + ContentIdentifier.CELLAR_PREFIX + "\\:[a-zA-Z0-9\\-]*\\.\\d{4}$");

    /**
     * The Constant M_PATTERN.
     */
    private static final Pattern M_PATTERN = Pattern.compile("^" + ContentIdentifier.CELLAR_PREFIX + "\\:[a-zA-Z0-9\\-]*\\.\\d{4}.\\d{2}$");

    /**
     * The Constant I_PATTERN.
     */
    private static final Pattern I_PATTERN = Pattern
            .compile("^" + ContentIdentifier.CELLAR_PREFIX + "\\:[a-zA-Z0-9\\-]*\\.\\d{4}.\\d{2}\\/[a-zA-Z0-9\\._]+$");

    /**
     * Cellar identifier formatting for WEMI objects.
     */
    private static final String E_FORMAT = "%s.%04d";

    /**
     * The Constant M_FORMAT.
     */
    private static final String M_FORMAT = "%s.%02d";

    /**
     * The Constant TYPES_THAT_TAKE_EMBARGO_DATE.
     */
    private static final List<DigitalObjectType> TYPES_THAT_TAKE_EMBARGO_DATE = Arrays.asList(WORK, EXPRESSION, MANIFESTATION, ITEM,
            DOSSIER, EVENT, AGENT, TOPLEVELEVENT);

    /**
     * The parent type.
     */
    private DigitalObjectType parentType;

    /**
     * The model resource.
     */
    private Resource modelResource;

    /**
     * The hierarchy levels.
     */
    private List<Integer> hierarchyLevels;

    /**
     * Instantiates a new digital object type.
     *
     * @param parentType      the parent type
     * @param hierarchyLevels the hierarchy levels
     */
    DigitalObjectType(final DigitalObjectType parentType, final Integer... hierarchyLevels) {
        this.parentType = parentType;
        this.hierarchyLevels = new ArrayList<Integer>();
        this.hierarchyLevels.addAll(Arrays.asList(hierarchyLevels));
        if (this.hierarchyLevels.isEmpty()) {
            this.hierarchyLevels.add(Integer.MAX_VALUE);
        }
        this.modelResource = ResourceFactory.createResource(Namespace.cdm + this.toString().toLowerCase());
    }

    /**
     * Gets the by value.
     *
     * @param value the value
     * @return the by value
     */
    public static DigitalObjectType getByValue(final String value) {
        for (final DigitalObjectType type : DigitalObjectType.values()) {
            if (type.toString().equalsIgnoreCase(value)) {
                return type;
            }
        }

        throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.UNKNOWN_DIGITAL_OBJECT_TYPE)
                .withMessage("Cannot find DigitalObjectType '{}'.").withMessageArgs(value).build();
    }

    public static DigitalObjectType findType(String cellarID) {
        if (W_PATTERN.matcher(cellarID).find()) {
            return WORK;
        } else if (E_PATTERN.matcher(cellarID).find()) {
            return EXPRESSION;
        } else if (M_PATTERN.matcher(cellarID).find()) {
            return MANIFESTATION;
        } else if (isItem(cellarID)) {
            return ITEM;
        } else {
            return UNDEFINED;
        }
    }

    /**
     * Accept.
     *
     * @param <IN>    the generic type
     * @param <OUT>   the generic type
     * @param visitor the visitor
     * @param in      the in
     * @return the out
     */
    public <IN, OUT> OUT accept(final DigitalObjectTypeVisitor<IN, OUT> visitor, final IN in) {
        throw NotImplementedExceptionBuilder.get().withMessage("Method 'accept' is not implemented for Digital Object '{}'.")
                .withMessageArgs(this).build();
    }

    /**
     * Gets the pattern.
     *
     * @return the pattern
     */
    public Pattern getPattern() {
        Pattern pattern = null;

        if (this.isTopLevel()) {
            pattern = W_PATTERN;
        } else if (this.hasHierarchyLevel(1)) {
            pattern = E_PATTERN;
        } else if (this.hasHierarchyLevel(2)) {
            pattern = M_PATTERN;
        } else if (this.hasHierarchyLevel(3)) {
            pattern = I_PATTERN;
        } else {
            throw NotImplementedExceptionBuilder.get().withMessage("Method 'getPattern' is not implemented for Digital Object '{}'.")
                    .withMessageArgs(this).build();
        }

        return pattern;
    }

    /**
     * Gets the parent type.
     *
     * @return the parent type
     */
    public DigitalObjectType getParentType() {
        return this.parentType;
    }

    /**
     * Gets the hierarchy levels.
     *
     * @return the hierarchy levels
     */
    public List<Integer> getHierarchyLevels() {
        return this.hierarchyLevels;
    }

    /**
     * Checks for hierarchy level.
     *
     * @param hierarchyLevel the hierarchy level
     * @return true, if successful
     */
    public boolean hasHierarchyLevel(final Integer hierarchyLevel) {
        return this.getHierarchyLevels().contains(hierarchyLevel);
    }

    /**
     * Checks if is top level.
     *
     * @return true, if is top level
     */
    public boolean isTopLevel() {
        return this.hasHierarchyLevel(0);
    }

    /**
     * Checks if is bottom level.
     *
     * @return true, if is bottom level
     */
    public boolean isBottomLevel() {
        return this.hasHierarchyLevel(Integer.MAX_VALUE);
    }

    /**
     * Gets the model resource.
     *
     * @return the model resource
     */
    public Resource getModelResource() {
        return this.modelResource;
    }

    /**
     * Generate time based cellar id.
     *
     * @param parentId the parent id
     * @param cardinal the cardinal
     * @param sipType  the sip type
     * @return the string
     */
    public String generateTimeBasedCellarID(final String parentId, final Integer cardinal, final TYPE sipType) {
        String cellarID = null;

        if (this.isTopLevel()) {
            cellarID = TimeBasedUUIDGenerator.INSTANCE.generate(sipType);
        } else if (this.hasHierarchyLevel(1)) {
            if (cardinal > 9999) {
                throw ExceptionBuilder.get(CellarValidationException.class).withCode(CoreErrors.TOO_MANY_CELLAR_ID).withMessage(
                        "Unable to create a CELLAR identifier with the cardinal[{}].The CELLAR identifier used for the EXPRESSION is limited to an index lower than 9999.")//
                        .withMessageArgs(cardinal).withMessageArgs().build();
            }
            cellarID = String.format(E_FORMAT, parentId, cardinal);
        } else if (this.hasHierarchyLevel(2)) {
            if (cardinal > 99) {
                throw ExceptionBuilder.get(CellarValidationException.class).withCode(CoreErrors.TOO_MANY_CELLAR_ID).withMessage(
                        "Unable to create a CELLAR identifier with the cardinal[{}].The CELLAR identifier used for the MANIFESTATION is limited to an index lower than 99.")//
                        .withMessageArgs(cardinal).withMessageArgs().build();
            }
            cellarID = String.format(M_FORMAT, parentId, cardinal);
        } else {
            throw NotImplementedExceptionBuilder.get().withMessage("Method 'generateCellarID' is not implemented for Digital Object '{}'.")
                    .withMessageArgs(this).build();
        }

        return cellarID;
    }

    /**
     * Gets the label.
     *
     * @return the label
     */
    public String getLabel() {
        return this.toString();
    }

    /**
     * Checks if is work dossier top level event or agent.
     *
     * @param cellarId the cellar id
     * @return true, if is work dossier top level event or agent
     */
    public static boolean isWorkDossierTopLevelEventOrAgent(final String cellarId) {
        return W_PATTERN.matcher(cellarId).find();
    }

    /**
     * Checks if is expression or event.
     *
     * @param cellarId the cellar id
     * @return true, if is expression or event
     */
    public static boolean isExpressionOrEvent(final String cellarId) {
        return E_PATTERN.matcher(cellarId).find();
    }

    /**
     * Checks if is manifestation.
     *
     * @param cellarId the cellar id
     * @return true, if is manifestation
     */
    public static boolean isManifestation(final String cellarId) {
        return M_PATTERN.matcher(cellarId).find();
    }

    /**
     * Checks if is item.
     *
     * @param cellarId the cellar id
     * @return true, if is item
     */
    public static boolean isItem(final String cellarId) {
        if (cellarId == null) {
            return false;
        } else if (I_PATTERN.matcher(cellarId).find()) {
            return true;
        } else {
            int idx = cellarId.indexOf("/");
            if (idx < 0) {
                return false;
            }
            String sub = cellarId.substring(0, idx);
            return StringUtils.countOccurrencesOf(sub, ".") == 2;
        }
    }

    /**
     * Gets the digital object types that have embargo date.
     *
     * @return the digital object types that have embargo date
     */
    public static List<DigitalObjectType> getEmbargoTypes() {
        return TYPES_THAT_TAKE_EMBARGO_DATE;
    }

    public static boolean takeEmbargoDate(final DigitalObjectType type) {
        return TYPES_THAT_TAKE_EMBARGO_DATE.contains(type);
    }

    public static DigitalObjectType getChildrenType(final DigitalObjectType type) {
        switch (type) {
            case WORK:
                return EXPRESSION;
            case EXPRESSION:
                return MANIFESTATION;
            case MANIFESTATION:
                return ITEM;
            case DOSSIER:
                return EVENT;
            default:
                return null;
        }
    }
}
