/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.catalog
 *             FILE : CatalogResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 01 10, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-01-10 10:48:11 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common;

/**
 * @author ARHS Developments
 */
public interface CatalogResolver {

    String resolve(String key);
}
