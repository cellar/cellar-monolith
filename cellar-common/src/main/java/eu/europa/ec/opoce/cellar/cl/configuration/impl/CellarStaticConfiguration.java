/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.configuration.impl
 *        FILE : CellarStaticConfiguration.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 02-04-2012
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.configuration.impl;

import com.google.common.base.Splitter;
import com.google.common.collect.ImmutableMap;
import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.*;
import eu.europa.ec.opoce.cellar.cl.configuration.ConfigurationPropertyService.EXISTINGPROPS_MODE;
import eu.europa.ec.opoce.cellar.cl.configuration.enums.*;
import eu.europa.ec.opoce.cellar.cl.domain.admin.ConfigurationProperty;
import eu.europa.ec.opoce.cellar.cl.domain.enums.ProcessMonitorPhase;
import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService.Priority;
import eu.europa.ec.opoce.cellar.common.util.FileSystemUtils;
import eu.europa.ec.opoce.cellar.exception.CellarConfigurationException;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import static eu.europa.ec.opoce.cellar.cl.configuration.enums.CellarConfigurationPropertyKey.*;


/**
 * <class_description> Static (=non persistent) Cellar configuration bean: used at start-up time only.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 02-04-2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component("cellarStaticConfiguration")
public class CellarStaticConfiguration implements ICellarConfiguration, IS3Configuration {

    /**
     * The Constant DEFAULT_CONF_PATH.
     */
    private static final String DEFAULT_CONF_PATH = "cellar.properties";

    // the classpath to the default (internal to the war) configuration file
    /**
     * The Constant PERSISTENT_CONF_PATH.
     */
    public static final String PERSISTENT_CONF_PATH = "cellar-persistent.properties";

    // the classpath to the persistent configuration file
    /**
     * The Constant WAR_INTERNAL_CP_PREFIX.
     */
    public static final String WAR_INTERNAL_CP_PREFIX = "classpath:/";

    // the key identifying a WAR-internal classpath
    /**
     * class's logger.
     */
    private transient static final Logger LOG = LogManager.getLogger(CellarStaticConfiguration.class);

    // the system property name that possibly specifies the path to the configuration file
    /**
     * The Constant CONF_SYS_KEY.
     */
    private static final String CONF_SYS_KEY = "configuration.path";

    // the system property name that specifies whether or not an internal configuration file may be used
    // instead of an external one
    // If this property is not set (or set to false), a valid external configuration file must be set using the
    // configuration.path key.
    // Whenever the configuration.path key is set and the referenced path is not valid, the application will check
    // if the key allow.internal.configuration is set and has value true. In such case, it will try to load the
    // specified internal file.
    private static final String ALLOW_INTERNAL_CONFIGURATION_KEY = "allow.internal.configuration";

    // mock configuration service: used only by instances of CellarStaticConfiguration, in order not to write onto the database
    /**
     * The configuration path.
     */
    private String configurationPath;

    // main cellar configuration file's path
    /**
     * The configuration property service.
     */
    private ConfigurationPropertyService configurationPropertyService;
    // property manager for in-memory storing of properties
    /**
     * The property manager.
     */
    private PropertyManager propertyManager;
    // property loader for loading properties from file system at initialization
    /**
     * The property loader.
     */
    private PropertyLoader propertyLoader;

    /**
     * The initialized.
     */
    private boolean initialized;
    private int cellarServiceNalLoadInferenceMaxIteration;

    /**
     * Gets the configuration property service.
     *
     * @return the configuration property service
     * @see eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration#getConfigurationPropertyService()
     */
    @Override
    public ConfigurationPropertyService getConfigurationPropertyService() {
        return this.configurationPropertyService;
    }

    /**
     * Gets the property loader.
     *
     * @return the property loader
     */
    public PropertyLoader getPropertyLoader() {
        return this.propertyLoader;
    }

    /**
     * Gets the property manager.
     *
     * @return the property manager
     */
    public PropertyManager getPropertyManager() {
        return this.propertyManager;
    }

    /**
     * Initializes the service.
     */
    @PostConstruct
    public void init() {
        // initializing class fields
        this.propertyManager = new PropertyManager();
        this.configurationPropertyService = new NullConfigurationPropertyServiceImpl();

        final List<InputStream> propertyFiles = new ArrayList<>();
        boolean errorOccured = false;

        // configuration paths
        try {
            //Load the configuration files as Stream
            propertyFiles.add(getMainConfigurationStream());
            propertyFiles.add(getPersistentConfigurationStream());
            this.propertyLoader = new PropertyLoader(propertyFiles);
        } catch (final CellarException e) {
            LOG.error(IConfiguration.CONFIG, "A fatal error occurred during loading of configuration.", e);
            errorOccured = true;
        } finally {
            closeStreams(propertyFiles);
            if (errorOccured) {
                System.exit(1);
            }
        }

        // removing obsolete properties from db, if any
        removeObsoletePropertiesFromDB();

        final PropertyManager propertyManagerFromPropertyLoader = this.propertyLoader.getPropertyManager();

        // CELLAR DATABASE PRIORITY
        setCellarConfigurationDatabaseHasPriority(
                propertyManagerFromPropertyLoader.getBooleanProperty(cellarConfigurationDatabaseHasPriority_Key.toString()));

        // CELLAR BASIC CONFIGURATION
        setCellarInstanceId(propertyManagerFromPropertyLoader.getProperty(cellarInstanceId_Key.toString()));
        setCellarConfigurationVersion(propertyManagerFromPropertyLoader.getProperty(cellarConfigurationVersion_Key.toString()));

        // CELLAR FOLDERS
        setCellarFolderRoot(propertyManagerFromPropertyLoader.getProperty(cellarFolderRoot_Key.toString()));
        setCellarFolderAuthenticOJReception(propertyManagerFromPropertyLoader.getProperty(cellarFolderAuthenticOJReception_Key.toString()));
        setCellarFolderDailyReception(propertyManagerFromPropertyLoader.getProperty(cellarFolderDailyReception_Key.toString()));
        setCellarFolderBulkReception(propertyManagerFromPropertyLoader.getProperty(cellarFolderBulkReception_Key.toString()));
        setCellarFolderBulkLowPriorityReception(propertyManagerFromPropertyLoader.getProperty(cellarFolderBulkLowPriorityReception_Key.toString()));
        setCellarFolderAuthenticOJResponse(propertyManagerFromPropertyLoader.getProperty(cellarFolderAuthenticOJResponse_Key.toString()));
        setCellarFolderDailyResponse(propertyManagerFromPropertyLoader.getProperty(cellarFolderDailyResponse_Key.toString()));
        setCellarFolderBulkResponse(propertyManagerFromPropertyLoader.getProperty(cellarFolderBulkResponse_Key.toString()));
        setCellarFolderBulkLowPriorityResponse(propertyManagerFromPropertyLoader.getProperty(cellarFolderBulkLowPriorityResponse_Key.toString()));
        setCellarFolderFoxml(propertyManagerFromPropertyLoader.getProperty(cellarFolderFoxml_Key.toString()));
        setCellarFolderError(propertyManagerFromPropertyLoader.getProperty(cellarFolderError_Key.toString()));
        setCellarFolderTemporaryDissemination(
                propertyManagerFromPropertyLoader.getProperty(cellarFolderTemporaryDissemination_Key.toString()));
        setCellarFolderTemporaryWork(propertyManagerFromPropertyLoader.getProperty(cellarFolderTemporaryWork_Key.toString()));
        setCellarFolderTemporaryStore(propertyManagerFromPropertyLoader.getProperty(cellarFolderTemporaryStore_Key.toString()));
        setCellarFolderLock(propertyManagerFromPropertyLoader.getProperty(cellarFolderLock_Key.toString()));
        setCellarFolderBackup(propertyManagerFromPropertyLoader.getProperty(cellarFolderBackup_Key.toString()));
        setCellarFolderExportBatchJob(propertyManagerFromPropertyLoader.getProperty(cellarFolderExportBatchJob_Key.toString()));
        setCellarFolderExportBatchJobTemporary(
                propertyManagerFromPropertyLoader.getProperty(cellarFolderExportBatchJobTemporary_Key.toString()));
        setCellarFolderExportRest(propertyManagerFromPropertyLoader.getProperty(cellarFolderExportRest_Key.toString()));
        setCellarFolderExportUpdateResponse(propertyManagerFromPropertyLoader.getProperty(cellarFolderExportUpdateResponse_Key.toString()));
        setCellarFolderCmrLog(propertyManagerFromPropertyLoader.getProperty(cellarFolderCmrLog_Key.toString()));
        setCellarFolderLicenseHolderArchiveExtraction(
                propertyManagerFromPropertyLoader.getProperty(cellarFolderLicenseHolderArchiveExtraction_Key.toString()));
        setCellarFolderLicenseHolderArchiveAdhocExtraction(
                propertyManagerFromPropertyLoader.getProperty(cellarFolderLicenseHolderArchiveAdhocExtraction_Key.toString()));
        setCellarFolderLicenseHolderTemporaryArchive(
                propertyManagerFromPropertyLoader.getProperty(cellarFolderLicenseHolderTemporaryArchive_Key.toString()));
        setCellarFolderLicenseHolderTemporarySparql(
                propertyManagerFromPropertyLoader.getProperty(cellarFolderLicenseHolderTemporarySparql_Key.toString()));
        setCellarFolderBatchJobSparqlQueries(
                propertyManagerFromPropertyLoader.getProperty(cellarFolderBatchJobSparqlQueries_Key.toString()));
        setCellarFolderSparqlResponseTemplates(
                propertyManagerFromPropertyLoader.getProperty(cellarFolderSparqlResponseTemplates_Key.toString()));

        // CELLAR SERVER
        setCellarServerName(propertyManagerFromPropertyLoader.getProperty(cellarServerName_Key.toString()));
        setCellarServerPort(propertyManagerFromPropertyLoader.getProperty(cellarServerPort_Key.toString()));
        setCellarServerContext(propertyManagerFromPropertyLoader.getProperty(cellarServerContext_Key.toString()));
        setCellarServerBaseUrl(propertyManagerFromPropertyLoader.getProperty(cellarServerBaseUrl_Key.toString()));

        // CELLAR URIS
        setCellarUriDisseminationBase(propertyManagerFromPropertyLoader.getProperty(cellarUriDisseminationBase_Key.toString()));
        setCellarUriResourceBase(propertyManagerFromPropertyLoader.getProperty(cellarUriResourceBase_Key.toString()));
        setCellarUriTranformationResourceBase(
                propertyManagerFromPropertyLoader.getProperty(cellarUriTranformationResourceBase_Key.toString()));

        // CELLAR APIS
        setCellarApiConnectionTimeout(propertyManagerFromPropertyLoader.getIntProperty(cellarApiConnectionTimeout_Key.toString()));
        setCellarApiReadTimeout(propertyManagerFromPropertyLoader.getIntProperty(cellarApiReadTimeout_Key.toString()));
        setCellarApiBaseUrl(propertyManagerFromPropertyLoader.getProperty(cellarApiBaseUrl_Key.toString()));
        setCellarApiHostHeader(propertyManagerFromPropertyLoader.getProperty(cellarApiHostHeader_Key.toString()));

        // CELLAR ENCRYPTOR
        setCellarEncryptorAlgorithm(propertyManagerFromPropertyLoader.getProperty(cellarEncryptorAlgorithm_Key.toString()));
        setCellarEncryptorPassword(propertyManagerFromPropertyLoader.getProperty(cellarEncryptorPassword_Key.toString()));

        // CELLAR SERVICES
        setCellarServiceIngestionEnabled(
                propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceIngestionEnabled_Key.toString()));
        setCellarServiceIngestionValidationMetadataEnabled(
                propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceIngestionValidationMetadataEnabled_Key.toString()));
        setCellarServiceIngestionPoolThreads(
                propertyManagerFromPropertyLoader.getIntProperty(cellarServiceIngestionPoolThreads_Key.toString()));
        setCellarServiceIngestionExecutionRate(
                propertyManagerFromPropertyLoader.getProperty(cellarServiceIngestionExecutionRate_Key.toString()));
        setCellarServiceIngestionSipCopyRetryPauseSeconds(
                propertyManagerFromPropertyLoader.getIntProperty(cellarServiceIngestionSipCopyRetryPauseSeconds_Key.toString()));
        setCellarServiceIngestionSipCopyTimeoutSeconds(
                propertyManagerFromPropertyLoader.getIntProperty(cellarServiceIngestionSipCopyTimeoutSeconds_Key.toString()));
        setCellarServiceIngestionCmrLockMode(CellarServiceIngestionCmrLockMode
                .resolve(propertyManagerFromPropertyLoader.getProperty(cellarServiceIngestionCmrLockMode_Key.toString())));
        setCellarServiceIngestionCmrLockMaxRetries(
                propertyManagerFromPropertyLoader.getIntProperty(cellarServiceIngestionCmrLockMaxRetries_Key.toString()));
        setCellarServiceIngestionCmrLockCeilingDelay(
                propertyManagerFromPropertyLoader.getLongProperty(cellarServiceIngestionCmrLockCeilingDelay_Key.toString()));
        setCellarServiceIngestionCmrLockBase(
                propertyManagerFromPropertyLoader.getIntProperty(cellarServiceIngestionCmrLockBase_Key.toString()));
        setCellarServiceIngestionConcurrencyControllerAutologgingEnabled(propertyManagerFromPropertyLoader
                .getBooleanProperty(cellarServiceIngestionConcurrencyControllerAutologgingEnabled_Key.toString()));
        setCellarServiceIngestionValidationItemExclusionEnabled(propertyManagerFromPropertyLoader
                .getBooleanProperty(cellarServiceIngestionValidationItemExclusionEnabled_Key.toString()));
        setCellarServiceIngestionConcurrencyControllerAutologgingCronSettings(propertyManagerFromPropertyLoader
                .getProperty(cellarServiceIngestionConcurrencyControllerAutologgingCronSettings_Key.toString()));
        setCellarServiceIngestionConcurrencyControllerThreadDelayMilliseconds(
                propertyManagerFromPropertyLoader.getLongProperty(cellarServiceIngestionConcurrencyControllerThreadDelayMilliseconds_Key.toString()));
        setCellarServiceIngestionCheckWriteOncePropertiesEnabled(
                propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceIngestionCheckWriteOncePropertiesEnabled_Key.toString()));
        setCellarStatusServiceShaclReportLevel(
                CellarStatusServiceShaclReportLevel.resolve(
                        propertyManagerFromPropertyLoader.getProperty(cellarStatusServiceShaclReportLevel_Key.toString())));
        setCellarServiceNalLoadDecodingValidationEnabled(
                propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceNalLoadDecodingValidationEnabled_Key.toString()));
        setCellarServiceNalLoadEnabled(propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceNalLoadEnabled_Key.toString()));
        setCellarServiceNalLoadInferenceMaxIteration(propertyManagerFromPropertyLoader.getIntProperty(cellarServiceNalLoadInferenceMaxIteration_Key.toString()));
        setCellarServiceNalLoadCronSettings(propertyManagerFromPropertyLoader.getProperty(cellarServiceNalLoadCronSettings_Key.toString()));
        setCellarServiceSparqlLoadEnabled(propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceSparqlLoadEnabled_Key.toString()));
        setCellarServiceSparqlLoadCronSettings(propertyManagerFromPropertyLoader.getProperty(cellarServiceSparqlLoadCronSettings_Key.toString()));
        setCellarServiceOntoLoadEnabled(propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceOntoLoadEnabled_Key.toString()));
        setCellarServiceBackupProcessedFileEnabled(
                propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceBackupProcessedFileEnabled_Key.toString()));
        setCellarServiceDeleteProcessedDataEnabled(
                propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceDeleteProcessedDataEnabled_Key.toString()));
        setCellarServiceIndexingEnabled(propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceIndexingEnabled_Key.toString()));
        setCellarServiceIndexingCronSettings(
                propertyManagerFromPropertyLoader.getProperty(cellarServiceIndexingCronSettings_Key.toString()));
        setCellarServiceSynchronizingCronSettings(
                propertyManagerFromPropertyLoader.getProperty(cellarServiceSynchronizingCronSettings_Key.toString()));
        setCellarServiceIndexingMinimumAgeMinutes(
                propertyManagerFromPropertyLoader.getIntProperty(cellarServiceIndexingMinimumAgeMinutes_Key.toString()));
        setCellarServiceIndexingMinimumPriority(
                Priority.resolve(propertyManagerFromPropertyLoader.getProperty(cellarServiceIndexingMinimumPriority_Key.toString())));
        setCellarServiceIndexingConcurrencyControllerEnabled(
                propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceIndexingConcurrencyControllerEnabled_Key.toString()));
        setCellarServiceIndexingPoolThreads(
                propertyManagerFromPropertyLoader.getIntProperty(cellarServiceIndexingPoolThreads_Key.toString()));
        setCellarServiceIndexingQueryLimitResults(
                propertyManagerFromPropertyLoader.getIntProperty(cellarServiceIndexingQueryLimitResults_Key.toString()));
        setCellarServiceIndexingCleanerEnabled(
                propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceIndexingCleanerEnabled_Key.toString()));
        setCellarServiceIndexingCleanerCronSettings(
                propertyManagerFromPropertyLoader.getProperty(cellarServiceIndexingCleanerCronSettings_Key.toString()));
        setCellarServiceIndexingCleanerDoneMinimumAgeDays(
                propertyManagerFromPropertyLoader.getIntProperty(cellarServiceIndexingCleanerDoneMinimumAgeDays_Key.toString()));
        setCellarServiceIndexingCleanerRedundantMinimumAgeDays(
                propertyManagerFromPropertyLoader.getIntProperty(cellarServiceIndexingCleanerRedundantMinimumAgeDays_Key.toString()));
        setCellarServiceIndexingCleanerErrorMinimumAgeDays(
                propertyManagerFromPropertyLoader.getIntProperty(cellarServiceIndexingCleanerErrorMinimumAgeDays_Key.toString()));

        setCellarServiceIndexingScheduledPriority(
                propertyManagerFromPropertyLoader.getProperty(cellarServiceIndexingScheduledPriority_Key.toString()));
        setCellarServiceIndexingScheduledCalcEmbedded(
                propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceIndexingScheduledCalcEmbedded_Key.toString()));
        setCellarServiceIndexingScheduledCalcExpanded(
                propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceIndexingScheduledCalcExpanded_Key.toString()));
        setCellarServiceIndexingScheduledCalcInverse(
                propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceIndexingScheduledCalcInverse_Key.toString()));
        setCellarServiceIndexingScheduledCalcNotice(
                propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceIndexingScheduledCalcNotice_Key.toString()));
        setCellarServiceIndexingScheduledSelectionPageSize(
                propertyManagerFromPropertyLoader.getLongProperty(cellarServiceIndexingScheduledSelectionPageSize_Key.toString()));
        setCellarServiceIndexingScheduledBatchSleepTime(
                propertyManagerFromPropertyLoader.getLongProperty(cellarServiceIndexingScheduledBatchSleepTime_Key.toString()));
        setCellarServiceIndexingScheduledExampleSparqlQueries(
                propertyManagerFromPropertyLoader.getProperty(cellarServiceIndexingScheduledExampleSparqlQueries_Key.toString()));

        setCellarServiceAutomaticDisembargoEnabled(
                propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceAutomaticDisembargoEnabled_Key.toString()));

        setCellarServiceBatchJobEnabled(propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceBatchJobEnabled_Key.toString()));
        setCellarServiceBatchJobSparqlCronSettings(
                propertyManagerFromPropertyLoader.getProperty(cellarServiceBatchJobSparqlCronSettings_Key.toString()));
        setCellarServiceBatchJobProcessingCronSettings(
                propertyManagerFromPropertyLoader.getProperty(cellarServiceBatchJobProcessingCronSettings_Key.toString()));
        setCellarServiceBatchJobSparqlUpdateMaxCron(
                propertyManagerFromPropertyLoader.getProperty(cellarServiceBatchJobSparqlUpdateMaxCron_Key.toString()));
        setCellarServiceBatchJobPoolCorePoolSize(
                propertyManagerFromPropertyLoader.getProperty(cellarServiceBatchJobPoolCorePoolSize_Key.toString()));
        setCellarServiceBatchJobPoolMaxPoolSize(
                propertyManagerFromPropertyLoader.getProperty(cellarServiceBatchJobPoolMaxPoolSize_Key.toString()));
        setCellarServiceBatchJobPoolQueueSize(
                propertyManagerFromPropertyLoader.getProperty(cellarServiceBatchJobPoolQueueSize_Key.toString()));
        setCellarServiceBatchJobPoolSelectionSize(
                propertyManagerFromPropertyLoader.getProperty(cellarServiceBatchJobPoolSelectionSize_Key.toString()));
        setCellarServiceSparqlTimeout(propertyManagerFromPropertyLoader.getIntProperty(cellarServiceSparqlTimeout_Key.toString()));
        setCellarServiceSparqlMaxConnections(propertyManagerFromPropertyLoader.getIntProperty(cellarServiceSparqlMaxConnections_Key.toString()));
        setCellarServiceSparqlTimeoutConnections(propertyManagerFromPropertyLoader.getIntProperty(cellarServiceSparqlTimeoutConnections_Key.toString()));
        setCellarServiceSparqlUri(propertyManagerFromPropertyLoader.getProperty(cellarServiceSparqlUri_Key.toString()));
        setCellarServiceLicenseHolderEnabled(
                propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceLicenseHolderEnabled_Key.toString()));
        setCellarServiceLicenseHolderWorkerCronSettings(
                propertyManagerFromPropertyLoader.getProperty(cellarServiceLicenseHolderWorkerCronSettings_Key.toString()));
        setCellarServiceLicenseHolderWorkerTaskExecutorCorePoolSize(
                propertyManagerFromPropertyLoader.getIntProperty(cellarServiceLicenseHolderWorkerTaskExecutorCorePoolSize_Key.toString()));
        setCellarServiceLicenseHolderWorkerTaskExecutorMaxPoolSize(
                propertyManagerFromPropertyLoader.getIntProperty(cellarServiceLicenseHolderWorkerTaskExecutorMaxPoolSize_Key.toString()));
        setCellarServiceLicenseHolderWorkerTaskExecutorQueueCapacity(
                propertyManagerFromPropertyLoader.getIntProperty(cellarServiceLicenseHolderWorkerTaskExecutorQueueCapacity_Key.toString()));
        setCellarServiceLicenseHolderWorkerArchivingExecutorCorePoolSize(propertyManagerFromPropertyLoader
                .getIntProperty(cellarServiceLicenseHolderWorkerArchivingExecutorCorePoolSize_Key.toString()));
        setCellarServiceLicenseHolderWorkerArchivingExecutorMaxPoolSize(propertyManagerFromPropertyLoader
                .getIntProperty(cellarServiceLicenseHolderWorkerArchivingExecutorMaxPoolSize_Key.toString()));
        setCellarServiceLicenseHolderWorkerArchivingExecutorQueueCapacity(propertyManagerFromPropertyLoader
                .getIntProperty(cellarServiceLicenseHolderWorkerArchivingExecutorQueueCapacity_Key.toString()));
        setCellarServiceLicenseHolderSchedulerCronSettings(
                propertyManagerFromPropertyLoader.getProperty(cellarServiceLicenseHolderSchedulerCronSettings_Key.toString()));
        setCellarServiceLicenseHolderCleanerCronSettings(
                propertyManagerFromPropertyLoader.getProperty(cellarServiceLicenseHolderCleanerCronSettings_Key.toString()));
        setCellarServiceLicenseHolderKeptDaysDaily(
                propertyManagerFromPropertyLoader.getIntProperty(cellarServiceLicenseHolderKeptDaysDaily_Key.toString()));
        setCellarServiceLicenseHolderKeptDaysWeekly(
                propertyManagerFromPropertyLoader.getIntProperty(cellarServiceLicenseHolderKeptDaysWeekly_Key.toString()));
        setCellarServiceLicenseHolderKeptDaysMonthly(
                propertyManagerFromPropertyLoader.getIntProperty(cellarServiceLicenseHolderKeptDaysMonthly_Key.toString()));
        setCellarServiceLicenseHolderBatchSizePerThread(
                propertyManagerFromPropertyLoader.getIntProperty(cellarServiceLicenseHolderBatchSizePerThread_Key.toString()));
        setCellarServiceLicenseHolderBatchSizeFolder(
                propertyManagerFromPropertyLoader.getIntProperty(cellarServiceLicenseHolderBatchSizeFolder_Key.toString()));
        setCellarServiceInverseNoticesWorkEnabled(
                propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceInverseNoticesWorkEnabled_Key.toString()));
        setCellarServiceInverseNoticesExpressionEnabled(
                propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceInverseNoticesExpressionEnabled_Key.toString()));
        setCellarServiceInverseNoticesManifestationEnabled(
                propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceInverseNoticesManifestationEnabled_Key.toString()));
        setCellarServiceInverseNoticesDossierEnabled(
                propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceInverseNoticesDossierEnabled_Key.toString()));
        setCellarServiceInverseNoticesEventEnabled(
                propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceInverseNoticesEventEnabled_Key.toString()));
        setCellarServiceInverseNoticesAgentEnabled(
                propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceInverseNoticesAgentEnabled_Key.toString()));
        setCellarServiceInverseNoticesTopLevelEventEnabled(
                propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceInverseNoticesTopLevelEventEnabled_Key.toString()));
        setCellarServiceInverseNoticesLimit(
                propertyManagerFromPropertyLoader.getIntProperty(cellarServiceInverseNoticesLimit_Key.toString()));
        setCellarServiceInverseNoticesRemoveNodeEnabled(
                propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceInverseNoticesRemoveNodeEnabled_Key.toString()));
        setCellarServiceExternalStartupTimeoutSeconds(
                propertyManagerFromPropertyLoader.getIntProperty(cellarServiceExternalStartupTimeoutSeconds_Key.toString()));
        setCellarServiceDisseminationInNoticePropertiesOnlyEnabled(propertyManagerFromPropertyLoader
                .getBooleanProperty(cellarServiceDisseminationInNoticePropertiesOnlyEnabled_Key.toString()));
        setCellarServiceDisseminationDatabaseOptimizationEnabled(
                propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceDisseminationDatabaseOptimizationEnabled_Key.toString()));
        setCellarServiceDisseminationContentStreamMaxSize(
                propertyManagerFromPropertyLoader.getLongProperty(cellarServiceDisseminationContentStreamMaxSize_Key.toString()));
        setCellarServiceDisseminationNoticesSortEnabled(
                propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceDisseminationNoticesSortEnabled_Key.toString()));
        setCellarServiceDisseminationDecodingDefault(
                propertyManagerFromPropertyLoader.getProperty(cellarServiceDisseminationDecodingDefault_Key.toString()));
        setCellarServiceDisseminationMementoEnabled(
                propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceDisseminationMementoEnabled_Key.toString()));
        setCellarServiceDisseminationRetrieveEmbargoedResourceEnabled(propertyManagerFromPropertyLoader
                .getBooleanProperty(cellarServiceDisseminationRetrieveEmbargoedResourceEnabled_Key.toString()));
        setCellarServiceRDFStoreCleanerEnabled(
                propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceRDFStoreCleanerEnabled_Key.toString()));
        setCellarServiceRDFStoreCleanerMode(CellarServiceRDFStoreCleanerMode
                .resolve(propertyManagerFromPropertyLoader.getProperty(cellarServiceRDFStoreCleanerMode_Key.toString())));
        setCellarServiceRDFStoreCleanerHierarchiesBatchSize(
                propertyManagerFromPropertyLoader.getIntProperty(cellarServiceRDFStoreCleanerHierarchiesBatchSize_Key.toString()));
        setCellarServiceRDFStoreCleanerResourcesBatchSize(
                propertyManagerFromPropertyLoader.getIntProperty(cellarServiceRDFStoreCleanerResourcesBatchSize_Key.toString()));
        setCellarServiceRDFStoreCleanerExecutorCorePoolSize(
                propertyManagerFromPropertyLoader.getIntProperty(cellarServiceRDFStoreCleanerExecutorCorePoolSize_Key.toString()));
        setCellarServiceRDFStoreCleanerExecutorMaxPoolSize(
                propertyManagerFromPropertyLoader.getIntProperty(cellarServiceRDFStoreCleanerExecutorMaxPoolSize_Key.toString()));
        setCellarServiceRDFStoreCleanerExecutorQueueCapacity(
                propertyManagerFromPropertyLoader.getIntProperty(cellarServiceRDFStoreCleanerExecutorQueueCapacity_Key.toString()));
        setCellarServiceNotificationItemsPerPage(
                propertyManagerFromPropertyLoader.getIntProperty(cellarServiceNotificationItemsPerPage_Key.toString()));
        // daily oj request cache refresh rate in minutes
        final Integer cellarServiceDisseminationDailyOjCacheRefreshMinutes = propertyManagerFromPropertyLoader
                .getIntProperty(cellarServiceDisseminationDailyOjCacheRefreshMinutes_Key.toString());
        setCellarServiceDisseminationDailyOjCacheRefreshMinutes(cellarServiceDisseminationDailyOjCacheRefreshMinutes);

        final Boolean cellarServiceAlignerExecutorEnabledValue = propertyManagerFromPropertyLoader
                .getBooleanProperty(cellarServiceAlignerExecutorEnabled_Key.toString());
        setCellarServiceAlignerExecutorEnabled(cellarServiceAlignerExecutorEnabledValue);
        final Integer cellarServiceAlignerExecutorCorePoolSizeValue = propertyManagerFromPropertyLoader
                .getIntProperty(cellarServiceAlignerExecutorCorePoolSize_Key.toString());
        setCellarServiceAlignerExecutorCorePoolSize(cellarServiceAlignerExecutorCorePoolSizeValue);
        final Integer cellarServiceAlignerExecutorMaxPoolSizeValue = propertyManagerFromPropertyLoader
                .getIntProperty(cellarServiceAlignerExecutorMaxPoolSize_Key.toString());
        setCellarServiceAlignerExecutorMaxPoolSize(cellarServiceAlignerExecutorMaxPoolSizeValue);
        final Integer cellarServiceAlignerExecutorQueueCapacityValue = propertyManagerFromPropertyLoader
                .getIntProperty(cellarServiceAlignerExecutorQueueCapacity_Key.toString());
        setCellarServiceAlignerExecutorQueueCapacity(cellarServiceAlignerExecutorQueueCapacityValue);
        final Integer cellarServiceAlignerHierarchiesBatchSizeValue = propertyManagerFromPropertyLoader
                .getIntProperty(cellarServiceAlignerHierarchiesBatchSize_Key.toString());
        setCellarServiceAlignerHierarchiesBatchSize(cellarServiceAlignerHierarchiesBatchSizeValue);
        final String cellarServiceAlignerModeValue = propertyManagerFromPropertyLoader.getProperty(cellarServiceAlignerMode_Key.toString());
        setCellarServiceAlignerMode(cellarServiceAlignerModeValue);

        final String cellarServiceLanguagesSkosCachePeriodValue = propertyManagerFromPropertyLoader
                .getProperty(cellarServiceLanguagesSkosCachePeriod_Key.toString());
        setCellarServiceLanguagesSkosCachePeriod(cellarServiceLanguagesSkosCachePeriodValue);

        final String cellarServiceFileTypesSkosCachePeriodValue = propertyManagerFromPropertyLoader
                .getProperty(cellarServiceFileTypesSkosCachePeriod_Key.toString());
        setCellarServiceFileTypesSkosCachePeriod(cellarServiceFileTypesSkosCachePeriodValue);

        final String cellarServiceIntegrationArchivistBaseUrlValue = propertyManagerFromPropertyLoader
                .getProperty(cellarServiceIntegrationArchivistBaseUrl_Key.toString());
        setCellarServiceIntegrationArchivistBaseUrl(cellarServiceIntegrationArchivistBaseUrlValue);
        final Boolean cellarServiceIntegrationArchivistEnabledValue = propertyManagerFromPropertyLoader
                .getBooleanProperty(cellarServiceIntegrationArchivistEnabled_Key.toString());
        setCellarServiceIntegrationArchivistEnabled(cellarServiceIntegrationArchivistEnabledValue);

        final String cellarServiceIntegrationFixityBaseUrlValue = propertyManagerFromPropertyLoader
                .getProperty(cellarServiceIntegrationFixityBaseUrl_Key.toString());
        setCellarServiceIntegrationFixityBaseUrl(cellarServiceIntegrationFixityBaseUrlValue);
        final Boolean cellarServiceIntegrationFixityEnabledValue = propertyManagerFromPropertyLoader
                .getBooleanProperty(cellarServiceIntegrationFixityEnabled_Key.toString());
        setCellarServiceIntegrationFixityEnabled(cellarServiceIntegrationFixityEnabledValue);

        final String cellarServiceIntegrationValidationBaseUrlValue = propertyManagerFromPropertyLoader
                .getProperty(cellarServiceIntegrationValidationBaseUrl_Key.toString());
        setCellarServiceIntegrationValidationBaseUrl(cellarServiceIntegrationValidationBaseUrlValue);

        final Integer cellarServiceCellarResourceMaxElementsInMemoryValue = propertyManagerFromPropertyLoader
                .getIntProperty(cellarServiceCellarResourceMaxElementsInMemory_Key.toString());
        setCellarServiceCellarResourceMaxElementsInMemory(cellarServiceCellarResourceMaxElementsInMemoryValue);
        final Integer cellarServiceCellarResourceTimeToLiveSecondsValue = propertyManagerFromPropertyLoader
                .getIntProperty(cellarServiceCellarResourceTimeToLiveSeconds_Key.toString());
        setCellarServiceCellarResourceTimeToLiveSeconds(cellarServiceCellarResourceTimeToLiveSecondsValue);
        
        final String cellarServiceEmbeddedNoticeCacheMaxBytesLocalHeapValue = propertyManagerFromPropertyLoader
                .getProperty(cellarServiceEmbeddedNoticeCacheMaxBytesLocalHeap_Key.toString());
        setCellarServiceEmbeddedNoticeCacheMaxBytesLocalHeap(cellarServiceEmbeddedNoticeCacheMaxBytesLocalHeapValue);
        final String cellarServiceEmbeddedNoticeCacheMaxBytesLocalDiskValue = propertyManagerFromPropertyLoader
                .getProperty(cellarServiceEmbeddedNoticeCacheMaxBytesLocalDisk_Key.toString());
        setCellarServiceEmbeddedNoticeCacheMaxBytesLocalDisk(cellarServiceEmbeddedNoticeCacheMaxBytesLocalDiskValue);
        final Long cellarServiceEmbeddedNoticeCacheTimeToLiveSecondsValue = propertyManagerFromPropertyLoader
                .getLongProperty(cellarServiceEmbeddedNoticeCacheTimeToLiveSeconds_Key.toString());
        setCellarServiceEmbeddedNoticeCacheTimeToLiveSeconds(cellarServiceEmbeddedNoticeCacheTimeToLiveSecondsValue);
        final Boolean cellarServiceEmbeddedNoticeCacheEvaluationStatisticsEnabled = propertyManagerFromPropertyLoader
                .getBooleanProperty(cellarServiceEmbeddedNoticeCacheEvaluationStatisticsEnabled_Key.toString());
        setCellarServiceEmbeddedNoticeCacheEvaluationStatisticsEnabled(cellarServiceEmbeddedNoticeCacheEvaluationStatisticsEnabled);
        
        setCellarServiceSipDependencyCheckerEnabled(
                propertyManagerFromPropertyLoader.getProperty(cellarServiceSipDependencyCheckerEnabled_Key.toString()));
        setCellarServiceSipDependencyCheckerCronSettings(
        		propertyManagerFromPropertyLoader.getProperty(cellarServiceSipDependencyCheckerCronSettings_Key.toString()));
        setCellarServiceSipDependencyCheckerPoolThreads(
        		propertyManagerFromPropertyLoader.getIntProperty(cellarServiceSipDependencyCheckerPoolThreads_Key.toString()));
        setCellarServiceSipQueueManagerCronSettings(
        		propertyManagerFromPropertyLoader.getProperty(cellarServiceSipQueueManagerCronSettings_Key.toString()));
        setCellarServiceSipQueueManagerFileAttributeDateType(
        		propertyManagerFromPropertyLoader.getProperty(cellarServiceSipQueueManagerFileAttributeDateType_Key.toString()));
        
        // DORIE
        setCellarServiceDorieDedicatedEnabled(
                propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceDorieDedicatedEnabled_Key.toString()));

        // CELLAR METS UPLOAD
        setCellarMetsUploadCallbackTimeout(propertyManagerFromPropertyLoader.getIntProperty(cellarMetsUploadCallbackTimeout_Key.toString()));
        setCellarMetsUploadBufferSize(propertyManagerFromPropertyLoader.getIntProperty(cellarMetsUploadBufferSize_Key.toString()));

        // CELLAR STATUS SERVICE
        setCellarStatusServiceBaseUrl(propertyManagerFromPropertyLoader.getProperty(cellarStatusServiceBaseUrl_Key.toString()));
        setCellarStatusServicePackageLevelEndpoint(propertyManagerFromPropertyLoader.getProperty(cellarStatusServicePackageLevelEndpoint_Key.toString()));
        setCellarStatusServiceObjectLevelEndpoint(propertyManagerFromPropertyLoader.getProperty(cellarStatusServiceObjectLevelEndpoint_Key.toString()));
        setCellarStatusServiceLookupEndpoint(propertyManagerFromPropertyLoader.getProperty(cellarStatusServiceLookupEndpoint_Key.toString()));

        // CELLAR DATABASE
        setCellarDatabaseReadOnly(propertyManagerFromPropertyLoader.getBooleanProperty(cellarDatabaseReadOnly_Key.toString()));
        setCellarDatabaseRdfPassword(propertyManagerFromPropertyLoader.getProperty(cellarDatabaseRdfPassword_Key.toString()));
        setCellarDatabaseRdfIngestionOptimizationEnabled(
                propertyManagerFromPropertyLoader.getBooleanProperty(cellarDatabaseRdfIngestionOptimizationEnabled_Key.toString()));
        setCellarDatabaseInClauseParams(propertyManagerFromPropertyLoader.getIntProperty(cellarDatabaseInClauseParams_Key.toString()));
        setCellarDatabaseAuditEnabled(propertyManagerFromPropertyLoader.getBooleanProperty(cellarDatabaseAuditEnabled_Key.toString()));

        // CELLAR DATASOURCES
        setCellarDatasourceCellar(propertyManagerFromPropertyLoader.getDataSourceProperty(cellarDatasourceCellar_Key.toString()));
        setCellarDatasourceCmr(propertyManagerFromPropertyLoader.getDataSourceProperty(cellarDatasourceCmr_Key.toString()));
        setCellarDatasourceIdol(propertyManagerFromPropertyLoader.getDataSourceProperty(cellarDatasourceIdol_Key.toString()));
        setCellarDatasourceVirtuoso(propertyManagerFromPropertyLoader.getDataSourceProperty(cellarDatasourceVirtuoso_Key.toString()));

        setCellarWatchAdviceEnabled(propertyManagerFromPropertyLoader.getBooleanProperty(cellarWatchAdviceEnabled.toString()));



        //CELLAR FEED
        setCellarFeedIngestionDescription(propertyManagerFromPropertyLoader.getProperty(cellarFeedIngestionDescription_Key.toString()));
        setCellarFeedIngestionTitle(propertyManagerFromPropertyLoader.getProperty(cellarFeedIngestionTitle_Key.toString()));
        setCellarFeedIngestionItemTitle(propertyManagerFromPropertyLoader.getProperty(cellarFeedIngestionItemTitle_Key.toString()));
        setCellarFeedIngestionEntrySummary(propertyManagerFromPropertyLoader.getProperty(cellarFeedIngestionEntrySummary_Key.toString()));

        setCellarFeedEmbargoDescription(propertyManagerFromPropertyLoader.getProperty(cellarFeedEmbargoDescription_Key.toString()));
        setCellarFeedEmbargoTitle(propertyManagerFromPropertyLoader.getProperty(cellarFeedEmbargoTitle_Key.toString()));
        setCellarFeedEmbargoItemTitle(propertyManagerFromPropertyLoader.getProperty(cellarFeedEmbargoItemTitle_Key.toString()));
        setCellarFeedEmbargoEntrySummary(propertyManagerFromPropertyLoader.getProperty(cellarFeedEmbargoEntrySummary_Key.toString()));

        setCellarFeedNalDescription(propertyManagerFromPropertyLoader.getProperty(cellarFeedNalDescription_Key.toString()));
        setCellarFeedNalTitle(propertyManagerFromPropertyLoader.getProperty(cellarFeedNalTitle_Key.toString()));
        setCellarFeedNalItemTitle(propertyManagerFromPropertyLoader.getProperty(cellarFeedNalItemTitle_Key.toString()));
        setCellarFeedNalEntrySummary(propertyManagerFromPropertyLoader.getProperty(cellarFeedNalEntrySummary_Key.toString()));

        setCellarFeedSparqlLoadDescription(propertyManagerFromPropertyLoader.getProperty(cellarFeedSparqlLoadDescription_Key.toString()));
        setCellarFeedSparqlLoadTitle(propertyManagerFromPropertyLoader.getProperty(cellarFeedSparqlLoadTitle_Key.toString()));
        setCellarFeedSparqlLoadItemTitle(propertyManagerFromPropertyLoader.getProperty(cellarFeedSparqlLoadItemTitle_Key.toString()));
        setCellarFeedSparqlLoadEntrySummary(propertyManagerFromPropertyLoader.getProperty(cellarFeedSparqlLoadEntrySummary_Key.toString()));

        setCellarFeedOntologyDescription(propertyManagerFromPropertyLoader.getProperty(cellarFeedOntologyDescription_Key.toString()));
        setCellarFeedOntologyTitle(propertyManagerFromPropertyLoader.getProperty(cellarFeedOntologyTitle_Key.toString()));
        setCellarFeedOntologyItemTitle(propertyManagerFromPropertyLoader.getProperty(cellarFeedOntologyItemTitle_Key.toString()));
        setCellarFeedOntologyEntrySummary(propertyManagerFromPropertyLoader.getProperty(cellarFeedOntologyEntrySummary_Key.toString()));



        // S3
        setS3Url(propertyManagerFromPropertyLoader.getProperty(s3Url.toString()));
        setS3Bucket(propertyManagerFromPropertyLoader.getProperty(s3BucketName.toString()));
        setS3Region(propertyManagerFromPropertyLoader.getProperty(s3RegionName.toString()));
        setS3CredentialsAccessKey(propertyManagerFromPropertyLoader.getProperty(s3CredentialsAccessKey.toString()));
        setS3CredentialsSecretKey(propertyManagerFromPropertyLoader.getProperty(s3CredentialsSecretKey.toString()));
        setS3ClientConnectionTimeout(propertyManagerFromPropertyLoader.getIntProperty(s3ClientConnectionTimeout.toString()));
        setS3ClientSocketTimeout(propertyManagerFromPropertyLoader.getIntProperty(s3ClientSocketTimeout.toString()));
        setS3Unsafe(propertyManagerFromPropertyLoader.getBooleanProperty(s3Unsafe.toString()));
        setS3RollbackTag(propertyManagerFromPropertyLoader.getProperty(s3RollbackTag.toString()));
        setProxyEnabled(propertyManagerFromPropertyLoader.getBooleanProperty(s3ProxyEnabled.toString()));
        setProxyHost(propertyManagerFromPropertyLoader.getProperty(s3ProxyHost.toString()));
        setProxyPort(propertyManagerFromPropertyLoader.getIntProperty(s3ProxyPort.toString()));
        setProxyUser(propertyManagerFromPropertyLoader.getProperty(s3ProxyUser.toString()));
        setProxyPassword(propertyManagerFromPropertyLoader.getProperty(s3ProxyPassword.toString()));
        setS3RetryEnabled(propertyManagerFromPropertyLoader.getBooleanProperty(s3RetryEnabled_Key.toString()));
        setS3RetryMaxAttempts(propertyManagerFromPropertyLoader.getIntProperty(s3RetryMaxAttempts_Key.toString()));
        setS3RetryDelay(propertyManagerFromPropertyLoader.getLongProperty(s3RetryDelay_Key.toString()));
        setS3RetryRandom(propertyManagerFromPropertyLoader.getBooleanProperty(s3RetryRandom_Key.toString()));
        setS3RetryMultiplier(propertyManagerFromPropertyLoader.getIntProperty(s3RetryMultiplier_Key.toString()));
        setS3RetryMaxDelay(propertyManagerFromPropertyLoader.getLongProperty(s3RetryMaxDelay_Key.toString()));
        setS3ServiceIngestionDatastreamRenamingEnabled(propertyManagerFromPropertyLoader.getBooleanProperty(s3ServiceIngestionDatastreamRenamingEnabled_Key.toString()));
        
        // EMAIL & EULOGIN
        setCellarServiceUserAccessNotificationEmailEnabled(propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceUserAccessNotificationEmailEnabled_Key.toString()));
        setCellarEmailJobCron(propertyManagerFromPropertyLoader.getProperty(cellarEmailCron_Key.toString()));
        setCellarServiceEmailCleanupMaxAgeDays(propertyManagerFromPropertyLoader.getIntProperty(cellarServiceEmailCleanupMaxAgeDays_Key.toString()));
        setCellarServiceEmailEnabled(propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceEmailEnabled_Key.toString()));
        setCellarServiceEuloginUserMigrationEnabled(propertyManagerFromPropertyLoader.getBooleanProperty(cellarServiceEuloginUserMigrationEnabled_Key.toString()));
        setCellarMailHost(propertyManagerFromPropertyLoader.getProperty(cellarMailHost_Key.toString()));
        setCellarMailPort(propertyManagerFromPropertyLoader.getIntProperty(cellarMailPort_Key.toString()));
        setCellarMailSmtpUser(propertyManagerFromPropertyLoader.getProperty(cellarMailSmtpUser_Key.toString()));
        setCellarMailSmtpPassword(propertyManagerFromPropertyLoader.getProperty(cellarMailSmtpPassword_Key.toString()));
        setCellarMailSmtpAuth(propertyManagerFromPropertyLoader.getBooleanProperty(cellarMailSmtpAuth_Key.toString()));
        setCellarMailSmtpStarttlsEnable(propertyManagerFromPropertyLoader.getBooleanProperty(cellarMailSmtpStarttlsEnable_Key.toString()));
        setCellarMailSmtpStarttlsRequired(propertyManagerFromPropertyLoader.getBooleanProperty(cellarMailSmtpStarttlsRequired_Key.toString()));
        setCellarMailSmtpFrom(propertyManagerFromPropertyLoader.getProperty(cellarMailSmtpFrom_Key.toString()));

        // VIRTUOSO
        setVirtuosoIngestionEnabled(propertyManagerFromPropertyLoader.getBooleanProperty(virtuosoIngestionEnabled_Key.toString()));
        setVirtuosoIngestionRetryStrategy(VirtuosoIngestionRetryStrategy
                .resolve(propertyManagerFromPropertyLoader.getProperty(virtuosoIngestionRetryStrategy_Key.toString())));
        setVirtuosoIngestionRetryMaxRetries(
                propertyManagerFromPropertyLoader.getIntProperty(virtuosoIngestionRetryMaxRetries_Key.toString()));
        setVirtuosoIngestionRetryCeilingDelay(
                propertyManagerFromPropertyLoader.getLongProperty(virtuosoIngestionRetryCeilingDelay_Key.toString()));
        setVirtuosoIngestionRetryBase(propertyManagerFromPropertyLoader.getIntProperty(virtuosoIngestionRetryBase_Key.toString()));
        setVirtuosoIngestionTechnicalEnabled(propertyManagerFromPropertyLoader.getBooleanProperty(virtuosoIngestionTechnicalEnabled_Key.toString()));
        setVirtuosoIngestionBackupEnabled(
                propertyManagerFromPropertyLoader.getBooleanProperty(virtuosoIngestionBackupEnabled_Key.toString()));
        setVirtuosoIngestionBackupSchedulerCronSettings(
                propertyManagerFromPropertyLoader.getProperty(virtuosoIngestionBackupSchedulerCronSettings_Key.toString()));
        setVirtuosoIngestionBackupConcurrencyControllerEnabled(
                propertyManagerFromPropertyLoader.getBooleanProperty(virtuosoIngestionBackupConcurrencyControllerEnabled_Key.toString()));
        setVirtuosoIngestionBackupMaxResults(
                propertyManagerFromPropertyLoader.getIntProperty(virtuosoIngestionBackupMaxResults_Key.toString()));
        setVirtuosoIngestionBackupLogVerbose(
                propertyManagerFromPropertyLoader.getBooleanProperty(virtuosoIngestionBackupLogVerbose_Key.toString()));
        setVirtuosoIngestionNormalizationEnabled(
                propertyManagerFromPropertyLoader.getBooleanProperty(virtuosoIngestionNormalizationEnabled_Key.toString()));
        setVirtuosoIngestionSkolemizationEnabled(
                propertyManagerFromPropertyLoader.getBooleanProperty(virtuosoIngestionSkolemizationEnabled_Key.toString()));
        setVirtuosoDisseminationSparqlMapperEnabled(
                propertyManagerFromPropertyLoader.getBooleanProperty(virtuosoDisseminationSparqlMapperEnabled_Key.toString()));
        setVirtuosoIngestionLinkrotRealignmentEnabled(
                propertyManagerFromPropertyLoader.getBooleanProperty(virtuosoIngestionLinkrotRealignmentEnabled_Key.toString()));

        setVirtuosoIngestionBackupExceptionMessagesCachePeriod(
                propertyManagerFromPropertyLoader.getProperty(virtuosoIngestionBackupExceptionMessagesCachePeriod_Key.toString()));

        setGracefulShutdownEnabled(propertyManagerFromPropertyLoader.getBooleanProperty(cellarGracefulShutdownEnabled_key.toString()));
        setGracefulShutdownTimeout(propertyManagerFromPropertyLoader.getLongProperty(cellarGracefulShutdownTimeout_key.toString()));
        setInferenceOntologyPath(propertyManagerFromPropertyLoader.getProperty(cellarInferenceOntologyPath.toString()));

        // TEST
        setTestEnabled(propertyManagerFromPropertyLoader.getBooleanProperty(testEnabled_Key.toString()));
        setTestDatabaseEnabled(propertyManagerFromPropertyLoader.getBooleanProperty(testDatabaseEnabled_Key.toString()));
        setTestDatabaseCronSettings(propertyManagerFromPropertyLoader.getProperty(testDatabaseCronSettings_Key.toString()));
        setTestRollbackEnabled(propertyManagerFromPropertyLoader.getBooleanProperty(testRollbackEnabled_Key.toString()));
        setTestVirtuosoRollbackEnabled(propertyManagerFromPropertyLoader.getBooleanProperty(testVirtuosoRollbackEnabled_Key.toString()));
        setTestIndexingDelayExecutionEnabled(propertyManagerFromPropertyLoader.getBooleanProperty(testIndexingDelayExecutionEnabled_Key.toString()));
        setTestS3RetryEnabled(propertyManagerFromPropertyLoader.getBooleanProperty(testS3RetryEnabled_Key.toString()));
        setTestProcessMonitorPhase(ProcessMonitorPhase.resolve(testProcessMonitorPhase_Key.toString()));
        setTestProcessMonitorPhaseReached(propertyManagerFromPropertyLoader.getBooleanProperty(testProcessMonitorPhaseReached_Key.toString()));
        setTestVirtuosoRetryEnabled(propertyManagerFromPropertyLoader.getBooleanProperty(testVirtuosoRetryEnabled_Key.toString()));

        //DEBUG
        setDebugVerboseLoggingEnabled(propertyManagerFromPropertyLoader.getBooleanProperty(debugVerboseLoggingEnabled_Key.toString()));

        // CELLAR SPECIAL *PRIORITY*/*PLACE* INGESTION PROPERTIES -- JRebel reloading issue
        setCellarServiceIngestionPickupMode(CellarServiceIngestionPickupMode
                .resolve(propertyManagerFromPropertyLoader.getProperty(cellarServiceIngestionPickupMode_Key.toString())));

        // CELLAR SPECIAL *PRIORITY*/*PLACE* S3 SERVICES PROPERTIES -- JRebel reloading issue
        setS3ServiceIngestionCacheEnabled(false);

        setCellarNamespace(propertyManagerFromPropertyLoader.getProperty(cellarNamespace_Key.toString()));

        LOG.info(IConfiguration.CONFIG, "{} properties registered.", propertyManagerFromPropertyLoader.getProperties().size());

        this.initialized = true;
    }

    private InputStream getPersistentConfigurationStream() {
        return FileSystemUtils.resolveGenericResource(PERSISTENT_CONF_PATH, true);
    }

    private void closeStreams(List<InputStream> propertyFiles) {
        propertyFiles.forEach(stream -> {
            try {
                stream.close();
            } catch (IOException e) {
                LOG.error("Error when trying to close stream {}", stream, e);
            }
        });
    }

    private InputStream getMainConfigurationStream() {
        // resolves the main configuration file
        InputStream mainConfiguration = null;
        final String mainConfigurationSystemPath = System.getProperty(CONF_SYS_KEY);
        final String allowInternalConfigurationString = System.getProperty(ALLOW_INTERNAL_CONFIGURATION_KEY);
        // internal configuration files are not allowed by default
        Boolean allowInternalConfiguration = Boolean.FALSE;

        if (null != allowInternalConfigurationString) {
            allowInternalConfiguration = Boolean.parseBoolean(allowInternalConfigurationString);
        }

        if (mainConfigurationSystemPath != null) {
            try {
                mainConfiguration = FileSystemUtils.resolveGenericResource(mainConfigurationSystemPath, false);
                this.configurationPath = mainConfigurationSystemPath;
            } catch (final CellarException e) {
                if (!allowInternalConfiguration) {
                    final ExceptionBuilder<CellarConfigurationException> exceptionBuilder = ExceptionBuilder
                            .get(CellarConfigurationException.class);
                    exceptionBuilder.withCode(CommonErrors.FAILED_PROCESSING_CONFIGURATION);
                    final String message = "A valid external configuration file must be specified. CELLAR cannot load the file [{}].";
                    exceptionBuilder.withMessage(message);
                    exceptionBuilder.withMessageArgs(mainConfigurationSystemPath);
                    throw exceptionBuilder.build();
                }
            }
        }

        if (mainConfiguration == null) {
            if (!allowInternalConfiguration) {
                final ExceptionBuilder<CellarConfigurationException> exceptionBuilder = ExceptionBuilder
                        .get(CellarConfigurationException.class);
                exceptionBuilder.withCode(CommonErrors.FAILED_PROCESSING_CONFIGURATION);
                final String message = "A valid external configuration file must be specified. Start CELLAR with -D" + CONF_SYS_KEY
                        + " JVM argument";
                exceptionBuilder.withMessage(message).withMessageArgs(mainConfigurationSystemPath);
                throw exceptionBuilder.build();

            }
            mainConfiguration = FileSystemUtils.resolveGenericResource(DEFAULT_CONF_PATH, true);
            this.configurationPath = WAR_INTERNAL_CP_PREFIX + DEFAULT_CONF_PATH;
        }
        return mainConfiguration;
    }

    /**
     * Removes obsolete properties from DB.
     */
    private void removeObsoletePropertiesFromDB() {
        final Collection<ConfigurationProperty> dbProps = getConfigurationPropertyService().getProperties();
        final Collection<ConfigurationProperty> dbPropsToRemove = new ArrayList<>();
        for (final ConfigurationProperty dbProp : dbProps) {
            if (CellarConfigurationPropertyKey.get(dbProp.getPropertyKey()) == null) {
                dbPropsToRemove.add(dbProp);
            }
        }

        try {
            getConfigurationPropertyService().deleteProperties(dbPropsToRemove);
        } catch (final CellarConfigurationException e) {
            // just ignore the exception if it is due to a write attempt to a read-only database
            if (!CommonErrors.READONLY_DATABASE.equals(e.getCode())) {
                throw e;
            }
        }
    }

    /**
     * Internal synchronize property.
     *
     * @param key      the key
     * @param value    the value
     * @param syncMode the sync mode
     * @throws CellarConfigurationException the cellar configuration exception
     */
    private void internalSynchronizeProperty(final CellarConfigurationPropertyKey key, final Object value,
                                             final EXISTINGPROPS_MODE... syncMode) throws CellarConfigurationException {
    	if (LOG.isInfoEnabled()) {
        	LOG.info("{}: {}", key, value);
    	}
        try {
            EXISTINGPROPS_MODE mySyncMode = null;
            if ((syncMode != null) && (syncMode.length == 1)) {
                mySyncMode = syncMode[0];
            }
            if (mySyncMode == null) {
                mySyncMode = isCellarConfigurationDatabaseHasPriority() ? EXISTINGPROPS_MODE.FROM_DB : EXISTINGPROPS_MODE.TO_DB;
            }
            synchronizeProperty(key.toString(), value, mySyncMode);
        } catch (final CellarConfigurationException e) {
            // just ignore the exception if it is due to a write attempt to a read-only database
            if (!CommonErrors.READONLY_DATABASE.equals(e.getCode())) {
                throw e;
            }
        }
    }

    /**
     * Internal synchronize property secure.
     *
     * @param key      the key
     * @param value    the value
     * @param syncMode the sync mode
     * @throws CellarConfigurationException the cellar configuration exception
     */
    private void internalSynchronizePropertySecure(final CellarConfigurationPropertyKey key, final Object value,
                                                   final EXISTINGPROPS_MODE... syncMode) throws CellarConfigurationException {
        try {
            EXISTINGPROPS_MODE mySyncMode = null;
            if ((syncMode != null) && (syncMode.length == 1)) {
                mySyncMode = syncMode[0];
            }
            if (mySyncMode == null) {
                mySyncMode = isCellarConfigurationDatabaseHasPriority() ? EXISTINGPROPS_MODE.FROM_DB : EXISTINGPROPS_MODE.TO_DB;
            }
            synchronizeProperty(key.toString(), value, mySyncMode);
        } catch (final CellarConfigurationException e) {
            // just ignore the exception if it is due to a write attempt to a read-only database
            if (!CommonErrors.READONLY_DATABASE.equals(e.getCode())) {
                throw e;
            }
        }
    }

    /**
     * Synchronize properties.
     *
     * @param syncMode the sync mode
     * @throws CellarConfigurationException the cellar configuration exception
     * @see eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration#synchronizeProperties(eu.europa.ec.opoce.cellar.cl.configuration.ConfigurationPropertyService.EXISTINGPROPS_MODE)
     */
    @Override
    public void synchronizeProperties(final EXISTINGPROPS_MODE syncMode) throws CellarConfigurationException {
        // for each in-memory persistent property..
        for (final Object keyObj : this.getPropertyManager().getProperties().keySet()) {
            final String key = keyObj.toString();
            // ..gets the current in-memory value
            final String value = getPropertyManager().getProperty(key);
            // ..and synchronizes it
            synchronizeProperty(key, value, syncMode);
        }
    }

    /**
     * Synchronize property.
     *
     * @param key      the key
     * @param value    the value
     * @param syncMode the sync mode
     * @throws CellarConfigurationException the cellar configuration exception
     * @see eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration#synchronizeProperty(java.lang.String, java.lang.Object, eu.europa.ec.opoce.cellar.cl.configuration.ConfigurationPropertyService.EXISTINGPROPS_MODE)
     */
    @Override
    public void synchronizeProperty(final String key, final Object value, final EXISTINGPROPS_MODE syncMode)
            throws CellarConfigurationException {
        // check if it is a known property
        final CellarConfigurationPropertyKey propertyKey = CellarConfigurationPropertyKey.get(key);
        if (propertyKey == null) {
            LOG.warn("Cannot synchronize the unknown property '{}'.", key);
            return;
        }

        String syncedPropertyValue = value.toString();
        try {
            // checks if the value of property is compatible with its type
            getPropertyManager().isCompatibleValueForProperty(key, syncedPropertyValue);

            // if property is persistent, synchronizes the property from/to database
            if (isPersistent(key)) {
                syncedPropertyValue = getConfigurationPropertyService().synchronizeProperty(syncedPropertyValue, key, syncMode);
            }

            // set the synchronized value in-memory
            getPropertyManager().setProperty(key, syncedPropertyValue);

            // run the satellite operations specific to the property just changed
            if (this.initialized) {
                propertyKey.executeSatelliteOperations();
            }
        } catch (final CellarConfigurationException e) {
            throw e;
        } catch (final Exception cause) {
            throw ExceptionBuilder.get(CellarConfigurationException.class).withCode(CommonErrors.FAILED_PROCESSING_CONFIGURATION)
                    .withMessage(cause.getMessage()).withCause(cause).build();
        } finally {
            // if the property does not exist in-memory, set it
            if (!getPropertyManager().existsProperty(key)) {
                getPropertyManager().setProperty(key, syncedPropertyValue);
            }
        }
    }

    /**
     * To string.
     *
     * @return the string
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return this.propertyManager.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarApiBaseUrl() {
        return getPropertyManager().getProperty(cellarApiBaseUrl_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarApiConnectionTimeout() {
        return getPropertyManager().getIntProperty(cellarApiConnectionTimeout_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarApiHostHeader() {
        return getPropertyManager().getProperty(cellarApiHostHeader_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarApiReadTimeout() {
        return getPropertyManager().getIntProperty(cellarApiReadTimeout_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarConfigurationVersion() {
        return getPropertyManager().getProperty(cellarConfigurationVersion_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarDatabaseInClauseParams() {
        return getPropertyManager().getIntProperty(cellarDatabaseInClauseParams_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarDatabaseRdfPassword() {
        return getPropertyManager().getProperty(cellarDatabaseRdfPassword_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarDatabaseRdfIngestionOptimizationEnabled() {
        return getPropertyManager().getBooleanProperty(cellarDatabaseRdfIngestionOptimizationEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarDatasourceCellar() {
        return getPropertyManager().getDataSourceProperty(cellarDatasourceCellar_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarDatasourceVirtuoso() {
        return getPropertyManager().getDataSourceProperty(cellarDatasourceVirtuoso_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarDatasourceCmr() {
        return getPropertyManager().getDataSourceProperty(cellarDatasourceCmr_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarDatasourceIdol() {
        return getPropertyManager().getDataSourceProperty(cellarDatasourceIdol_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarEncryptorAlgorithm() {
        return getPropertyManager().getProperty(cellarEncryptorAlgorithm_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarEncryptorPassword() {
        return getPropertyManager().getProperty(cellarEncryptorPassword_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarFolderExportBatchJob() {
        return getPropertyManager().getProperty(cellarFolderExportBatchJob_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarFolderExportBatchJobTemporary() {
        return getPropertyManager().getProperty(cellarFolderExportBatchJobTemporary_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarFolderExportUpdateResponse() {
        return getPropertyManager().getProperty(cellarFolderExportUpdateResponse_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarFolderExportRest() {
        return getPropertyManager().getProperty(cellarFolderExportRest_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarFolderBackup() {
        return getPropertyManager().getProperty(cellarFolderBackup_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarFolderBatchJobSparqlQueries() {
        return getPropertyManager().getProperty(cellarFolderBatchJobSparqlQueries_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarFolderSparqlResponseTemplates() {
        return getPropertyManager().getProperty(cellarFolderSparqlResponseTemplates_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarFolderBulkReception() {
        return getPropertyManager().getProperty(cellarFolderBulkReception_Key.toString());
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarFolderBulkResponse() {
        return getPropertyManager().getProperty(cellarFolderBulkResponse_Key.toString());
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarFolderBulkLowPriorityReception() {
    	return getPropertyManager().getProperty(cellarFolderBulkLowPriorityReception_Key.toString());
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarFolderBulkLowPriorityResponse() {
    	return getPropertyManager().getProperty(cellarFolderBulkLowPriorityResponse_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarFolderCmrLog() {
        return getPropertyManager().getProperty(cellarFolderCmrLog_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarFolderAuthenticOJReception() {
        return getPropertyManager().getProperty(cellarFolderAuthenticOJReception_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarFolderAuthenticOJResponse() {
        return getPropertyManager().getProperty(cellarFolderAuthenticOJResponse_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarFolderDailyReception() {
        return getPropertyManager().getProperty(cellarFolderDailyReception_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarFolderDailyResponse() {
        return getPropertyManager().getProperty(cellarFolderDailyResponse_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarFolderError() {
        return getPropertyManager().getProperty(cellarFolderError_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarFolderFoxml() {
        return getPropertyManager().getProperty(cellarFolderFoxml_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarFolderLicenseHolderArchiveAdhocExtraction() {
        return getPropertyManager().getProperty(cellarFolderLicenseHolderArchiveAdhocExtraction_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarFolderLicenseHolderArchiveExtraction() {
        return getPropertyManager().getProperty(cellarFolderLicenseHolderArchiveExtraction_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarFolderLicenseHolderTemporaryArchive() {
        return getPropertyManager().getProperty(cellarFolderLicenseHolderTemporaryArchive_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarFolderLicenseHolderTemporarySparql() {
        return getPropertyManager().getProperty(cellarFolderLicenseHolderTemporarySparql_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarFolderLock() {
        return getPropertyManager().getProperty(cellarFolderLock_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarFolderRoot() {
        return getPropertyManager().getProperty(cellarFolderRoot_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarFolderTemporaryDissemination() {
        return getPropertyManager().getProperty(cellarFolderTemporaryDissemination_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarFolderTemporaryStore() {
        return getPropertyManager().getProperty(cellarFolderTemporaryStore_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarFolderTemporaryWork() {
        return getPropertyManager().getProperty(cellarFolderTemporaryWork_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarInstanceId() {
        return getPropertyManager().getProperty(cellarInstanceId_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServerBaseUrl() {
        return getPropertyManager().getProperty(cellarServerBaseUrl_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServerContext() {
        return getPropertyManager().getProperty(cellarServerContext_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServerName() {
        return getPropertyManager().getProperty(cellarServerName_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServerPort() {
        return getPropertyManager().getProperty(cellarServerPort_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServiceBatchJobProcessingCronSettings() {
        return getPropertyManager().getProperty(cellarServiceBatchJobProcessingCronSettings_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServiceBatchJobSparqlUpdateMaxCron() {
        return getPropertyManager().getProperty(cellarServiceBatchJobSparqlUpdateMaxCron_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServiceBatchJobSparqlCronSettings() {
        return getPropertyManager().getProperty(cellarServiceBatchJobSparqlCronSettings_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getCellarServiceDisseminationContentStreamMaxSize() {
        return getPropertyManager().getLongProperty(cellarServiceDisseminationContentStreamMaxSize_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer getCellarServiceDisseminationDailyOjCacheRefreshMinutes() {
        return getPropertyManager().getIntProperty(cellarServiceDisseminationDailyOjCacheRefreshMinutes_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServiceDisseminationDecodingDefault() {
        return getPropertyManager().getProperty(cellarServiceDisseminationDecodingDefault_Key.toString());
    }

    /**
     * {@inheritDoc}
     */


    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceExternalStartupTimeoutSeconds() {
        return getPropertyManager().getIntProperty(cellarServiceExternalStartupTimeoutSeconds_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServiceIndexingCronSettings() {
        return getPropertyManager().getProperty(cellarServiceIndexingCronSettings_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServiceSynchronizingCronSettings() {
        return getPropertyManager().getProperty(cellarServiceSynchronizingCronSettings_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceIndexingMinimumAgeMinutes() {
        return getPropertyManager().getIntProperty(cellarServiceIndexingMinimumAgeMinutes_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Priority getCellarServiceIndexingMinimumPriority() {
        return Priority.resolve(getPropertyManager().getProperty(cellarServiceIndexingMinimumPriority_Key.toString()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceIndexingCleanerEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceIndexingCleanerEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServiceIndexingCleanerCronSettings() {
        return getPropertyManager().getProperty(cellarServiceIndexingCleanerCronSettings_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceIndexingCleanerDoneMinimumAgeDays() {
        return getPropertyManager().getIntProperty(cellarServiceIndexingCleanerDoneMinimumAgeDays_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceIndexingCleanerRedundantMinimumAgeDays() {
        return getPropertyManager().getIntProperty(cellarServiceIndexingCleanerRedundantMinimumAgeDays_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceIndexingCleanerErrorMinimumAgeDays() {
        return getPropertyManager().getIntProperty(cellarServiceIndexingCleanerErrorMinimumAgeDays_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceIndexingPoolThreads() {
        return getPropertyManager().getIntProperty(cellarServiceIndexingPoolThreads_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceIndexingQueryLimitResults() {
        return getPropertyManager().getIntProperty(cellarServiceIndexingQueryLimitResults_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CellarServiceIngestionCmrLockMode getCellarServiceIngestionCmrLockMode() {
        return CellarServiceIngestionCmrLockMode
                .resolve(getPropertyManager().getProperty(cellarServiceIngestionCmrLockMode_Key.toString()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getCellarServiceIngestionCmrLockCeilingDelay() {
        return getPropertyManager().getLongProperty(cellarServiceIngestionCmrLockCeilingDelay_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceIngestionCmrLockMaxRetries() {
        return getPropertyManager().getIntProperty(cellarServiceIngestionCmrLockMaxRetries_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceIngestionCmrLockBase() {
        return getPropertyManager().getIntProperty(cellarServiceIngestionCmrLockBase_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServiceIngestionConcurrencyControllerAutologgingCronSettings() {
        return getPropertyManager().getProperty(cellarServiceIngestionConcurrencyControllerAutologgingCronSettings_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServiceIngestionExecutionRate() {
        return getPropertyManager().getProperty(cellarServiceIngestionExecutionRate_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceIngestionValidationMetadataEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceIngestionValidationMetadataEnabled_Key.toString());
    }

    @Override
    public boolean getCellarServiceIngestionValidationItemExclusionEnabled(){
        return getPropertyManager().getBooleanProperty(cellarServiceIngestionValidationItemExclusionEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CellarServiceIngestionPickupMode getCellarServiceIngestionPickupMode() {
        return CellarServiceIngestionPickupMode.resolve(getPropertyManager().getProperty(cellarServiceIngestionPickupMode_Key.toString()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceIngestionPoolThreads() {
        return getPropertyManager().getIntProperty(cellarServiceIngestionPoolThreads_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceIngestionSipCopyRetryPauseSeconds() {
        return getPropertyManager().getIntProperty(cellarServiceIngestionSipCopyRetryPauseSeconds_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceIngestionSipCopyTimeoutSeconds() {
        return getPropertyManager().getIntProperty(cellarServiceIngestionSipCopyTimeoutSeconds_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceInverseNoticesLimit() {
        return getPropertyManager().getIntProperty(cellarServiceInverseNoticesLimit_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceLicenseHolderBatchSizeFolder() {
        return getPropertyManager().getIntProperty(cellarServiceLicenseHolderBatchSizeFolder_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceLicenseHolderBatchSizePerThread() {
        return getPropertyManager().getIntProperty(cellarServiceLicenseHolderBatchSizePerThread_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServiceLicenseHolderCleanerCronSettings() {
        return getPropertyManager().getProperty(cellarServiceLicenseHolderCleanerCronSettings_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceLicenseHolderKeptDaysDaily() {
        return getPropertyManager().getIntProperty(cellarServiceLicenseHolderKeptDaysDaily_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceLicenseHolderKeptDaysMonthly() {
        return getPropertyManager().getIntProperty(cellarServiceLicenseHolderKeptDaysMonthly_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceLicenseHolderKeptDaysWeekly() {
        return getPropertyManager().getIntProperty(cellarServiceLicenseHolderKeptDaysWeekly_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServiceLicenseHolderSchedulerCronSettings() {
        return getPropertyManager().getProperty(cellarServiceLicenseHolderSchedulerCronSettings_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceLicenseHolderWorkerArchivingExecutorCorePoolSize() {
        return getPropertyManager().getIntProperty(cellarServiceLicenseHolderWorkerArchivingExecutorCorePoolSize_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceLicenseHolderWorkerArchivingExecutorMaxPoolSize() {
        return getPropertyManager().getIntProperty(cellarServiceLicenseHolderWorkerArchivingExecutorMaxPoolSize_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceLicenseHolderWorkerArchivingExecutorQueueCapacity() {
        return getPropertyManager().getIntProperty(cellarServiceLicenseHolderWorkerArchivingExecutorQueueCapacity_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServiceLicenseHolderWorkerCronSettings() {
        return getPropertyManager().getProperty(cellarServiceLicenseHolderWorkerCronSettings_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceLicenseHolderWorkerTaskExecutorCorePoolSize() {
        return getPropertyManager().getIntProperty(cellarServiceLicenseHolderWorkerTaskExecutorCorePoolSize_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceLicenseHolderWorkerTaskExecutorMaxPoolSize() {
        return getPropertyManager().getIntProperty(cellarServiceLicenseHolderWorkerTaskExecutorMaxPoolSize_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceLicenseHolderWorkerTaskExecutorQueueCapacity() {
        return getPropertyManager().getIntProperty(cellarServiceLicenseHolderWorkerTaskExecutorQueueCapacity_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServiceNalLoadCronSettings() {
        return getPropertyManager().getProperty(cellarServiceNalLoadCronSettings_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceNotificationItemsPerPage() {
        return getPropertyManager().getIntProperty(cellarServiceNotificationItemsPerPage_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceRDFStoreCleanerExecutorCorePoolSize() {
        return getPropertyManager().getIntProperty(cellarServiceRDFStoreCleanerExecutorCorePoolSize_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceRDFStoreCleanerExecutorMaxPoolSize() {
        return getPropertyManager().getIntProperty(cellarServiceRDFStoreCleanerExecutorMaxPoolSize_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceRDFStoreCleanerExecutorQueueCapacity() {
        return getPropertyManager().getIntProperty(cellarServiceRDFStoreCleanerExecutorQueueCapacity_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceRDFStoreCleanerHierarchiesBatchSize() {
        return getPropertyManager().getIntProperty(cellarServiceRDFStoreCleanerHierarchiesBatchSize_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CellarServiceRDFStoreCleanerMode getCellarServiceRDFStoreCleanerMode() {
        return CellarServiceRDFStoreCleanerMode.resolve(getPropertyManager().getProperty(cellarServiceRDFStoreCleanerMode_Key.toString()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceRDFStoreCleanerResourcesBatchSize() {
        return getPropertyManager().getIntProperty(cellarServiceRDFStoreCleanerResourcesBatchSize_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarUriDisseminationBase() {
        return getPropertyManager().getProperty(cellarUriDisseminationBase_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarUriResourceBase() {
        return getPropertyManager().getProperty(cellarUriResourceBase_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarUriTranformationResourceBase() {
        return getPropertyManager().getProperty(cellarUriTranformationResourceBase_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getConfigurationPath() {
        return this.configurationPath;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarNamespace() {
        return getPropertyManager().getProperty(cellarNamespace_Key.toString());
    }

    @Override
    public void setCellarNamespace(String namespace, EXISTINGPROPS_MODE... syncMode) {
        getPropertyManager().setProperty(cellarNamespace_Key.toString(), namespace);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getS3ServiceIngestionCacheCoefficient() {
        return 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getProperty(final CellarConfigurationPropertyKey propertyKey) {
        return getPropertyManager().getProperty(propertyKey.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getTestDatabaseCronSettings() {
        return getPropertyManager().getProperty(testDatabaseCronSettings_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarConfigurationDatabaseHasPriority() {
        return getPropertyManager().getBooleanProperty(cellarConfigurationDatabaseHasPriority_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarConfigurationDatabaseHasPriority(final boolean cellarConfigurationDatabaseHasPriority) {
        // this is the only property for which we just call the set on property manager instead of synchronizeProperty method,
        // given that the synchronizeProperty method depends on the value of this property, that is not set yet at this time
        // important: as a consequence, this property cannot be configured as persistent
        getPropertyManager().setProperty(cellarConfigurationDatabaseHasPriority_Key.toString(),
                Boolean.toString(cellarConfigurationDatabaseHasPriority));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarDatabaseAuditEnabled() {
        return getPropertyManager().getBooleanProperty(cellarDatabaseAuditEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarDatabaseReadOnly() {
        return getPropertyManager().getBooleanProperty(cellarDatabaseReadOnly_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceAutomaticDisembargoEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceAutomaticDisembargoEnabled_Key.toString());
    }

    @Override
    public int getCellarServiceDisembargoInitialDelay(){
        return getPropertyManager().getIntProperty(cellarServiceDisembargoInitialDelay_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceBackupProcessedFileEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceBackupProcessedFileEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceBatchJobEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceBatchJobEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceDeleteProcessedDataEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceDeleteProcessedDataEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceDisseminationDatabaseOptimizationEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceDisseminationDatabaseOptimizationEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceDisseminationInNoticePropertiesOnlyEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceDisseminationInNoticePropertiesOnlyEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceDisseminationNoticesSortEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceDisseminationNoticesSortEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceIndexingConcurrencyControllerEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceIndexingConcurrencyControllerEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceIndexingEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceIndexingEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceIngestionCheckWriteOncePropertiesEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceIngestionCheckWriteOncePropertiesEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceIngestionConcurrencyControllerAutologgingEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceIngestionConcurrencyControllerAutologgingEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceIngestionEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceIngestionEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceInverseNoticesAgentEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceInverseNoticesAgentEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceInverseNoticesTopLevelEventEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceInverseNoticesTopLevelEventEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceInverseNoticesDossierEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceInverseNoticesDossierEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceInverseNoticesEventEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceInverseNoticesEventEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceInverseNoticesExpressionEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceInverseNoticesExpressionEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceInverseNoticesManifestationEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceInverseNoticesManifestationEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceInverseNoticesWorkEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceInverseNoticesWorkEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceLicenseHolderEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceLicenseHolderEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceNalLoadEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceNalLoadEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceNalLoadDecodingValidationEnabled(final boolean cellarServiceNalLoadDecodingValidationEnabled,
                                                                 final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceNalLoadDecodingValidationEnabled_Key, cellarServiceNalLoadDecodingValidationEnabled,
                syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceNalLoadDecodingValidationEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceNalLoadDecodingValidationEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceOntoLoadEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceOntoLoadEnabled_Key.toString());
    }

    @Override
    public boolean isCellarServiceSparqlLoadEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceSparqlLoadEnabled_Key.toString());
    }

    @Override
    public String getCellarServiceSparqlLoadCronSettings() {
        return getPropertyManager().getProperty(cellarServiceSparqlLoadCronSettings_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceRDFStoreCleanerEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceRDFStoreCleanerEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isS3ServiceIngestionCacheEnabled() {
        return false;
    }

    /**
     * Checks if is persistent.
     *
     * @param key the key
     * @return true, if is persistent
     * @see eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration#isPersistent(java.lang.String)
     */
    @Override
    public boolean isPersistent(final String key) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isTestEnabled() {
        return getPropertyManager().getBooleanProperty(testEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isTestRollbackEnabled() {
        return getPropertyManager().getBooleanProperty(testRollbackEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isTestVirtuosoRollbackEnabled() {
        return getPropertyManager().getBooleanProperty(testVirtuosoRollbackEnabled_Key.toString());
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isTestIndexingDelayExecutionEnabled() {
        return getPropertyManager().getBooleanProperty(testIndexingDelayExecutionEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isTestVirtuosoRetryEnabled() {
        return getPropertyManager().getBooleanProperty(testVirtuosoRetryEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isTestS3RetryEnabled() {
        return getPropertyManager().getBooleanProperty(testS3RetryEnabled_Key.toString());
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public ProcessMonitorPhase getTestProcessMonitorPhase() {
        return ProcessMonitorPhase.resolve(getPropertyManager().getProperty(testProcessMonitorPhase_Key.toString()));
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isTestProcessMonitorPhaseReached() {
        return getPropertyManager().getBooleanProperty(testProcessMonitorPhaseReached_Key.toString());
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarApiBaseUrl(final String cellarApiBaseUrl, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarApiBaseUrl_Key, cellarApiBaseUrl, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarApiConnectionTimeout(final int cellarApiConnectionTimeout, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarApiConnectionTimeout_Key, cellarApiConnectionTimeout, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarApiHostHeader(final String cellarApiHostHeader, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarApiHostHeader_Key, cellarApiHostHeader, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarApiReadTimeout(final int cellarApiReadTimeout, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarApiReadTimeout_Key, cellarApiReadTimeout, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarConfigurationVersion(final String cellarConfigurationVersion, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarConfigurationVersion_Key, cellarConfigurationVersion, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarDatabaseAuditEnabled(final boolean cellarDatabaseAuditEnabled, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarDatabaseAuditEnabled_Key, cellarDatabaseAuditEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarDatabaseInClauseParams(final int cellarDatabaseInClauseParams, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarDatabaseInClauseParams_Key, cellarDatabaseInClauseParams, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarDatabaseRdfPassword(final String cellarDatabaseRdfPassword, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizePropertySecure(cellarDatabaseRdfPassword_Key, cellarDatabaseRdfPassword, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarDatabaseRdfIngestionOptimizationEnabled(final boolean cellarDatabaseRdfIngestionOptimizationEnabled,
                                                                 final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarDatabaseRdfIngestionOptimizationEnabled_Key, cellarDatabaseRdfIngestionOptimizationEnabled,
                syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarDatabaseReadOnly(final boolean cellarDatabaseReadOnly, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarDatabaseReadOnly_Key, cellarDatabaseReadOnly, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarDatasourceCellar(final String cellarDatasourceCellar, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarDatasourceCellar_Key, cellarDatasourceCellar, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarDatasourceVirtuoso(final String cellarDatasourceVirtuoso, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarDatasourceVirtuoso_Key, cellarDatasourceVirtuoso, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarDatasourceCmr(final String cellarDatasourceCmr, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarDatasourceCmr_Key, cellarDatasourceCmr, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarDatasourceIdol(final String cellarDatasourceIdol, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarDatasourceIdol_Key, cellarDatasourceIdol, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarEncryptorAlgorithm(final String cellarEncryptorAlgorithm, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarEncryptorAlgorithm_Key, cellarEncryptorAlgorithm, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarEncryptorPassword(final String cellarEncryptorPassword, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizePropertySecure(cellarEncryptorPassword_Key, cellarEncryptorPassword, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarFolderExportBatchJob(final String cellarFolderExportBatchJob, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFolderExportBatchJob_Key, cellarFolderExportBatchJob, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarFolderExportBatchJobTemporary(final String cellarFolderExportBatchJobTemporary,
                                                       final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFolderExportBatchJobTemporary_Key, cellarFolderExportBatchJobTemporary, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarFolderExportRest(final String cellarFolderExportRest, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFolderExportRest_Key, cellarFolderExportRest, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarFolderExportUpdateResponse(final String cellarFolderExportUpdateResponse, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFolderExportUpdateResponse_Key, cellarFolderExportUpdateResponse, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarFolderBackup(final String cellarFolderBackup, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFolderBackup_Key, cellarFolderBackup, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarFolderBatchJobSparqlQueries(final String cellarFolderBatchJobSparqlQueries, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFolderBatchJobSparqlQueries_Key, cellarFolderBatchJobSparqlQueries, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarFolderSparqlResponseTemplates(final String cellarFolderSparqlResponseTemplates,
                                                       final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFolderSparqlResponseTemplates_Key, cellarFolderSparqlResponseTemplates, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarFolderBulkReception(final String cellarFolderBulkReception, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFolderBulkReception_Key, cellarFolderBulkReception, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarFolderBulkResponse(final String cellarFolderBulkResponse, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFolderBulkResponse_Key, cellarFolderBulkResponse, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarFolderBulkLowPriorityReception(final String cellarFolderBulkLowPriorityReception, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFolderBulkLowPriorityReception_Key, cellarFolderBulkLowPriorityReception, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarFolderBulkLowPriorityResponse(final String cellarFolderBulkLowPriorityResponse, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFolderBulkLowPriorityResponse_Key, cellarFolderBulkLowPriorityResponse, syncMode);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarFolderCmrLog(final String cellarFolderCmrLog, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFolderCmrLog_Key, cellarFolderCmrLog, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarFolderAuthenticOJReception(final String cellarFolderAuthenticOJReception, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFolderAuthenticOJReception_Key, cellarFolderAuthenticOJReception, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarFolderAuthenticOJResponse(final String cellarFolderAuthenticOJResponse, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFolderAuthenticOJResponse_Key, cellarFolderAuthenticOJResponse, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarFolderDailyReception(final String cellarFolderDailyReception, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFolderDailyReception_Key, cellarFolderDailyReception, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarFolderDailyResponse(final String cellarFolderDailyResponse, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFolderDailyResponse_Key, cellarFolderDailyResponse, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarFolderError(final String cellarFolderError, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFolderError_Key, cellarFolderError, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarFolderFoxml(final String cellarFolderFoxml, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFolderFoxml_Key, cellarFolderFoxml, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarFolderLicenseHolderArchiveAdhocExtraction(final String cellarFolderLicenseHolderArchiveAdhocExtraction,
                                                                   final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFolderLicenseHolderArchiveAdhocExtraction_Key, cellarFolderLicenseHolderArchiveAdhocExtraction,
                syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarFolderLicenseHolderArchiveExtraction(final String cellarFolderLicenseHolderArchiveExtraction,
                                                              final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFolderLicenseHolderArchiveExtraction_Key, cellarFolderLicenseHolderArchiveExtraction, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarFolderLicenseHolderTemporaryArchive(final String cellarFolderLicenseHolderTemporaryArchive,
                                                             final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFolderLicenseHolderTemporaryArchive_Key, cellarFolderLicenseHolderTemporaryArchive, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarFolderLicenseHolderTemporarySparql(final String cellarFolderLicenseHolderTemporarySparql,
                                                            final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFolderLicenseHolderTemporarySparql_Key, cellarFolderLicenseHolderTemporarySparql, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarFolderLock(final String cellarFolderLock, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFolderLock_Key, cellarFolderLock, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarFolderRoot(final String cellarFolderRoot, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFolderRoot_Key, cellarFolderRoot, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarFolderTemporaryDissemination(final String cellarFolderTemporaryDissemination,
                                                      final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFolderTemporaryDissemination_Key, cellarFolderTemporaryDissemination, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarFolderTemporaryStore(final String cellarFolderTemporaryStore, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFolderTemporaryStore_Key, cellarFolderTemporaryStore, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarFolderTemporaryWork(final String cellarFolderTemporaryWork, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFolderTemporaryWork_Key, cellarFolderTemporaryWork, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarInstanceId(final String cellarInstanceId, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarInstanceId_Key, cellarInstanceId, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServerBaseUrl(final String cellarServerBaseUrl, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServerBaseUrl_Key, cellarServerBaseUrl, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServerContext(final String cellarServerContext, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServerContext_Key, cellarServerContext, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServerName(final String cellarServerName, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServerName_Key, cellarServerName, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServerPort(final String cellarServerPort, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServerPort_Key, cellarServerPort, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceAutomaticDisembargoEnabled(final boolean cellarServiceAutomaticDisembargoEnabled,
                                                           final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceAutomaticDisembargoEnabled_Key, cellarServiceAutomaticDisembargoEnabled, syncMode);
    }

    @Override
    public void setCellarServiceDisembargoInitialDelay(final int cellarServiceDisembargoInitialDelay,
                                                       final EXISTINGPROPS_MODE... syncMode){
        internalSynchronizeProperty(cellarServiceDisembargoInitialDelay_Key,cellarServiceDisembargoInitialDelay);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceBackupProcessedFileEnabled(final boolean cellarServiceBackupProcessedFileEnabled,
                                                           final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceBackupProcessedFileEnabled_Key, cellarServiceBackupProcessedFileEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceBatchJobEnabled(final boolean cellarServiceBatchJobEnabled, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceBatchJobEnabled_Key, cellarServiceBatchJobEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceBatchJobProcessingCronSettings(final String cellarServiceBatchJobProcessingCronSettings,
                                                               final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceBatchJobProcessingCronSettings_Key, cellarServiceBatchJobProcessingCronSettings, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceBatchJobSparqlUpdateMaxCron(final String cellarServiceBatchJobSparqlUpdateMaxCron,
                                                            final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceBatchJobSparqlUpdateMaxCron_Key, cellarServiceBatchJobSparqlUpdateMaxCron, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceBatchJobSparqlCronSettings(final String cellarServiceBatchJobSparqlCronSettings,
                                                           final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceBatchJobSparqlCronSettings_Key, cellarServiceBatchJobSparqlCronSettings, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceDeleteProcessedDataEnabled(final boolean cellarServiceDeleteProcessedDataEnabled,
                                                           final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceDeleteProcessedDataEnabled_Key, cellarServiceDeleteProcessedDataEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceDisseminationContentStreamMaxSize(final long cellarServiceDisseminationContentStreamMaxSize,
                                                                  final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceDisseminationContentStreamMaxSize_Key, cellarServiceDisseminationContentStreamMaxSize,
                syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceDisseminationDailyOjCacheRefreshMinutes(final Integer numberOfMinutes,
                                                                        final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceDisseminationDailyOjCacheRefreshMinutes_Key, numberOfMinutes, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceDisseminationDatabaseOptimizationEnabled(
            final boolean cellarServiceDisseminationDatabaseOptimizationEnabled, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceDisseminationDatabaseOptimizationEnabled_Key,
                cellarServiceDisseminationDatabaseOptimizationEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceDisseminationDecodingDefault(final String cellarServiceDisseminationDecodingDefault,
                                                             final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceDisseminationDecodingDefault_Key, cellarServiceDisseminationDecodingDefault, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceDisseminationInNoticePropertiesOnlyEnabled(
            final boolean cellarServiceDisseminationInNoticePropertiesOnlyEnabled, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceDisseminationInNoticePropertiesOnlyEnabled_Key,
                cellarServiceDisseminationInNoticePropertiesOnlyEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceDisseminationNoticesSortEnabled(final boolean cellarServiceDisseminationNoticesSortEnabled,
                                                                final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceDisseminationNoticesSortEnabled_Key, cellarServiceDisseminationNoticesSortEnabled,
                syncMode);
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceExternalStartupTimeoutSeconds(final int cellarServiceExternalStartupTimeoutSeconds,
                                                              final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceExternalStartupTimeoutSeconds_Key, cellarServiceExternalStartupTimeoutSeconds, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIndexingConcurrencyControllerEnabled(final boolean cellarServiceIndexingConcurrencyControllerEnabled,
                                                                     final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIndexingConcurrencyControllerEnabled_Key,
                cellarServiceIndexingConcurrencyControllerEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIndexingCronSettings(final String cellarServiceIndexingCronSettings, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIndexingCronSettings_Key, cellarServiceIndexingCronSettings, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceSynchronizingCronSettings(final String cellarServiceSynchronizingCronSettings, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceSynchronizingCronSettings_Key, cellarServiceSynchronizingCronSettings, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIndexingEnabled(final boolean cellarServiceIndexingEnabled, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIndexingEnabled_Key, cellarServiceIndexingEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIndexingMinimumAgeMinutes(final int cellarServiceIndexingMinimumAgeMinutes,
                                                          final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIndexingMinimumAgeMinutes_Key, cellarServiceIndexingMinimumAgeMinutes, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIndexingMinimumPriority(final Priority cellarServiceIndexingMinimumPriority,
                                                        final EXISTINGPROPS_MODE... syncMode) {

        internalSynchronizeProperty(cellarServiceIndexingMinimumPriority_Key, cellarServiceIndexingMinimumPriority.toString(), syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIndexingCleanerEnabled(final boolean cellarServiceIndexingCleanerEnabled,
                                                       final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIndexingCleanerEnabled_Key, cellarServiceIndexingCleanerEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIndexingCleanerCronSettings(final String cellarServiceIndexingCleanerCronSettings,
                                                            final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIndexingCleanerCronSettings_Key, cellarServiceIndexingCleanerCronSettings, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIndexingCleanerDoneMinimumAgeDays(final int cellarServiceIndexingCleanerDoneMinimumAgeDays,
                                                                  final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIndexingCleanerDoneMinimumAgeDays_Key, cellarServiceIndexingCleanerDoneMinimumAgeDays,
                syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIndexingCleanerRedundantMinimumAgeDays(final int cellarServiceIndexingCleanerRedundantMinimumAgeDays,
                                                                           final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIndexingCleanerRedundantMinimumAgeDays_Key, cellarServiceIndexingCleanerRedundantMinimumAgeDays,
                syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIndexingCleanerErrorMinimumAgeDays(final int cellarServiceIndexingCleanerErrorMinimumAgeDays,
                                                                   final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIndexingCleanerErrorMinimumAgeDays_Key, cellarServiceIndexingCleanerErrorMinimumAgeDays,
                syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIndexingScheduledPriority(final String cellarServiceIndexingScheduledPriority,
                                                          final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIndexingScheduledPriority_Key, cellarServiceIndexingScheduledPriority, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServiceIndexingScheduledPriority() {
        return getPropertyManager().getProperty(cellarServiceIndexingScheduledPriority_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIndexingPoolThreads(final int cellarServiceIndexingPoolThreads, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIndexingPoolThreads_Key, cellarServiceIndexingPoolThreads, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIndexingQueryLimitResults(final int cellarServiceIndexingQueryLimitResults,
                                                          final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIndexingQueryLimitResults_Key, cellarServiceIndexingQueryLimitResults, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean getCellarServiceIndexingScheduledCalcInverse() {
        return getPropertyManager().getBooleanProperty(cellarServiceIndexingScheduledCalcInverse_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean getCellarServiceIndexingScheduledCalcEmbedded() {
        return getPropertyManager().getBooleanProperty(cellarServiceIndexingScheduledCalcEmbedded_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean getCellarServiceIndexingScheduledCalcNotice() {
        return getPropertyManager().getBooleanProperty(cellarServiceIndexingScheduledCalcNotice_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean getCellarServiceIndexingScheduledCalcExpanded() {
        return getPropertyManager().getBooleanProperty(cellarServiceIndexingScheduledCalcExpanded_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIndexingScheduledCalcInverse(final boolean cellarServiceIndexingScheduledCalcInverse,
                                                             final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIndexingScheduledCalcInverse_Key, cellarServiceIndexingScheduledCalcInverse, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIndexingScheduledCalcEmbedded(final boolean cellarServiceIndexingScheduledCalcEmbedded,
                                                              final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIndexingScheduledCalcEmbedded_Key, cellarServiceIndexingScheduledCalcEmbedded, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIndexingScheduledCalcNotice(final boolean cellarServiceIndexingScheduledCalcNotice,
                                                            final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIndexingScheduledCalcNotice_Key, cellarServiceIndexingScheduledCalcNotice, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIndexingScheduledCalcExpanded(final boolean cellarServiceIndexingScheduledCalcExpanded,
                                                              final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIndexingScheduledCalcExpanded_Key, cellarServiceIndexingScheduledCalcExpanded, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getCellarServiceIndexingScheduledSelectionPageSize() {
        return getPropertyManager().getLongProperty(cellarServiceIndexingScheduledSelectionPageSize_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIndexingScheduledSelectionPageSize(final long cellarServiceIndexingScheduledSelectionPageSize,
                                                                   final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIndexingScheduledSelectionPageSize_Key, cellarServiceIndexingScheduledSelectionPageSize,
                syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getCellarServiceIndexingScheduledBatchSleepTime() {
        return getPropertyManager().getLongProperty(cellarServiceIndexingScheduledBatchSleepTime_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIndexingScheduledBatchSleepTime(final long cellarServiceIndexingScheduledBatchSleepTime,
                                                                final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIndexingScheduledBatchSleepTime_Key, cellarServiceIndexingScheduledBatchSleepTime,
                syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServiceIndexingScheduledExampleSparqlQueries() {
        return getPropertyManager().getProperty(cellarServiceIndexingScheduledExampleSparqlQueries_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIndexingScheduledExampleSparqlQueries(final String cellarServiceIndexingScheduledExampleSparqlQueries,
                                                                      final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIndexingScheduledExampleSparqlQueries_Key,
                cellarServiceIndexingScheduledExampleSparqlQueries, syncMode);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIngestionCheckWriteOncePropertiesEnabled(
            final boolean cellarServiceIngestionCheckWriteOncePropertiesEnabled, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIngestionCheckWriteOncePropertiesEnabled_Key,
                cellarServiceIngestionCheckWriteOncePropertiesEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIngestionCmrLockMode(final CellarServiceIngestionCmrLockMode cellarServiceIngestionCmrLockMode,
                                                     final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIngestionCmrLockMode_Key, cellarServiceIngestionCmrLockMode.toString(), syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIngestionCmrLockCeilingDelay(final long cellarServiceIngestionCmrLockCeilingDelay,
                                                             final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIngestionCmrLockCeilingDelay_Key, cellarServiceIngestionCmrLockCeilingDelay, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIngestionCmrLockMaxRetries(final int cellarServiceIngestionCmrLockMaxRetries,
                                                           final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIngestionCmrLockMaxRetries_Key, cellarServiceIngestionCmrLockMaxRetries, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIngestionCmrLockBase(final int cellarServiceIngestionCmrLockBase, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIngestionCmrLockBase_Key, cellarServiceIngestionCmrLockBase, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIngestionConcurrencyControllerAutologgingCronSettings(
            final String cellarServiceIngestionConcurrencyControllerAutologgingCronSettings, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIngestionConcurrencyControllerAutologgingCronSettings_Key,
                cellarServiceIngestionConcurrencyControllerAutologgingCronSettings, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIngestionConcurrencyControllerAutologgingEnabled(
            final boolean cellarServiceIngestionConcurrencyControllerAutologgingEnabled, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIngestionConcurrencyControllerAutologgingEnabled_Key,
                cellarServiceIngestionConcurrencyControllerAutologgingEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIngestionExecutionRate(final String cellarServiceIngestionExecutionRate,
                                                      final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIngestionExecutionRate_Key, cellarServiceIngestionExecutionRate, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIngestionValidationMetadataEnabled(final boolean cellarServiceIngestionMetadataValidationEnabled, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIngestionValidationMetadataEnabled_Key, cellarServiceIngestionMetadataValidationEnabled, syncMode);
    }

    @Override
    public void setCellarServiceIngestionValidationItemExclusionEnabled(final boolean cellarServiceIngestionValidationItemExclusionEnabled,
                                                      final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIngestionValidationItemExclusionEnabled_Key, cellarServiceIngestionValidationItemExclusionEnabled, syncMode);
    }




    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIngestionEnabled(final boolean cellarServiceIngestionEnabled, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIngestionEnabled_Key, cellarServiceIngestionEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIngestionPickupMode(final CellarServiceIngestionPickupMode cellarServiceIngestionPickupMode,
                                                    final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIngestionPickupMode_Key, cellarServiceIngestionPickupMode.toString(), syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIngestionPoolThreads(final int corePoolSize, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIngestionPoolThreads_Key, corePoolSize, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIngestionSipCopyRetryPauseSeconds(final int cellarServiceIngestionSipCopyRetryPauseSeconds,
                                                                  final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIngestionSipCopyRetryPauseSeconds_Key, cellarServiceIngestionSipCopyRetryPauseSeconds,
                syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIngestionSipCopyTimeoutSeconds(final int cellarServiceIngestionSipCopyTimeoutSeconds,
                                                               final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIngestionSipCopyTimeoutSeconds_Key, cellarServiceIngestionSipCopyTimeoutSeconds, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceInverseNoticesAgentEnabled(final boolean cellarServiceInverseNoticesAgentEnabled,
                                                           final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceInverseNoticesAgentEnabled_Key, cellarServiceInverseNoticesAgentEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceInverseNoticesTopLevelEventEnabled(final boolean cellarServiceInverseNoticesTopLevelEventEnabled,
                                                                   final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceInverseNoticesTopLevelEventEnabled_Key, cellarServiceInverseNoticesTopLevelEventEnabled,
                syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceInverseNoticesDossierEnabled(final boolean cellarServiceInverseNoticesDossierEnabled,
                                                             final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceInverseNoticesDossierEnabled_Key, cellarServiceInverseNoticesDossierEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceInverseNoticesEventEnabled(final boolean cellarServiceInverseNoticesEventEnabled,
                                                           final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceInverseNoticesEventEnabled_Key, cellarServiceInverseNoticesEventEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceInverseNoticesExpressionEnabled(final boolean cellarServiceInverseNoticesExpressionEnabled,
                                                                final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceInverseNoticesExpressionEnabled_Key, cellarServiceInverseNoticesExpressionEnabled,
                syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceInverseNoticesLimit(final int cellarServiceInverseNoticesLimit, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceInverseNoticesLimit_Key, cellarServiceInverseNoticesLimit, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceInverseNoticesManifestationEnabled(final boolean cellarServiceInverseNoticesManifestationEnabled,
                                                                   final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceInverseNoticesManifestationEnabled_Key, cellarServiceInverseNoticesManifestationEnabled,
                syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceInverseNoticesWorkEnabled(final boolean cellarServiceInverseNoticesWorkEnabled,
                                                          final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceInverseNoticesWorkEnabled_Key, cellarServiceInverseNoticesWorkEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceInverseNoticesRemoveNodeEnabled(final boolean cellarServiceInverseNoticesNodeEnabled,
                                                                final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceInverseNoticesRemoveNodeEnabled_Key, cellarServiceInverseNoticesNodeEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceInverseNoticesRemoveNodeEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceInverseNoticesRemoveNodeEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceLicenseHolderBatchSizeFolder(final int cellarServiceLicenseHolderBatchSizeFolder,
                                                             final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceLicenseHolderBatchSizeFolder_Key, cellarServiceLicenseHolderBatchSizeFolder, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceLicenseHolderBatchSizePerThread(final int cellarServiceLicenseHolderBatchSizePerThread,
                                                                final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceLicenseHolderBatchSizePerThread_Key, cellarServiceLicenseHolderBatchSizePerThread,
                syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceLicenseHolderCleanerCronSettings(final String cellarServiceLicenseHolderCleanerCronSettings,
                                                                 final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceLicenseHolderCleanerCronSettings_Key, cellarServiceLicenseHolderCleanerCronSettings,
                syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceLicenseHolderEnabled(final boolean cellarServiceLicenseHolderEnabled,
                                                     final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceLicenseHolderEnabled_Key, cellarServiceLicenseHolderEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceLicenseHolderKeptDaysDaily(final int cellarServiceLicenseHolderKeptDaysDaily,
                                                           final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceLicenseHolderKeptDaysDaily_Key, cellarServiceLicenseHolderKeptDaysDaily, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceLicenseHolderKeptDaysMonthly(final int cellarServiceLicenseHolderKeptDaysMonthly,
                                                             final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceLicenseHolderKeptDaysMonthly_Key, cellarServiceLicenseHolderKeptDaysMonthly, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceLicenseHolderKeptDaysWeekly(final int cellarServiceLicenseHolderKeptDaysWeekly,
                                                            final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceLicenseHolderKeptDaysWeekly_Key, cellarServiceLicenseHolderKeptDaysWeekly, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceLicenseHolderSchedulerCronSettings(final String cellarServiceLicenseHolderSchedulerCronSettings,
                                                                   final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceLicenseHolderSchedulerCronSettings_Key, cellarServiceLicenseHolderSchedulerCronSettings,
                syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceLicenseHolderWorkerArchivingExecutorCorePoolSize(
            final int cellarServiceLicenseHolderWorkerArchivingExecutorCorePoolSize, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceLicenseHolderWorkerArchivingExecutorCorePoolSize_Key,
                cellarServiceLicenseHolderWorkerArchivingExecutorCorePoolSize, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceLicenseHolderWorkerArchivingExecutorMaxPoolSize(
            final int cellarServiceLicenseHolderWorkerArchivingExecutorMaxPoolSize, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceLicenseHolderWorkerArchivingExecutorMaxPoolSize_Key,
                cellarServiceLicenseHolderWorkerArchivingExecutorMaxPoolSize, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceLicenseHolderWorkerArchivingExecutorQueueCapacity(
            final int cellarServiceLicenseHolderWorkerArchivingExecutorQueueCapacity, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceLicenseHolderWorkerArchivingExecutorQueueCapacity_Key,
                cellarServiceLicenseHolderWorkerArchivingExecutorQueueCapacity, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceLicenseHolderWorkerCronSettings(final String cellarServiceLicenseHolderWorkerCronSettings,
                                                                final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceLicenseHolderWorkerCronSettings_Key, cellarServiceLicenseHolderWorkerCronSettings,
                syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceLicenseHolderWorkerTaskExecutorCorePoolSize(
            final int cellarServiceLicenseHolderWorkerTaskExecutorCorePoolSize, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceLicenseHolderWorkerTaskExecutorCorePoolSize_Key,
                cellarServiceLicenseHolderWorkerTaskExecutorCorePoolSize, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceLicenseHolderWorkerTaskExecutorMaxPoolSize(
            final int cellarServiceLicenseHolderWorkerTaskExecutorMaxPoolSize, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceLicenseHolderWorkerTaskExecutorMaxPoolSize_Key,
                cellarServiceLicenseHolderWorkerTaskExecutorMaxPoolSize, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceLicenseHolderWorkerTaskExecutorQueueCapacity(
            final int cellarServiceLicenseHolderWorkerTaskExecutorQueueCapacity, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceLicenseHolderWorkerTaskExecutorQueueCapacity_Key,
                cellarServiceLicenseHolderWorkerTaskExecutorQueueCapacity, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceNalLoadCronSettings(final String cellarServiceNalLoadCronSettings, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceNalLoadCronSettings_Key, cellarServiceNalLoadCronSettings, syncMode);
    }

    @Override
    public void setCellarServiceSparqlLoadCronSettings(final String cellarServiceSparqlLoadCronSettings, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceSparqlLoadCronSettings_Key, cellarServiceSparqlLoadCronSettings, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceNalLoadEnabled(final boolean cellarServiceNalLoadEnabled, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceNalLoadEnabled_Key, cellarServiceNalLoadEnabled, syncMode);
    }

    @Override
    public void setCellarServiceSparqlLoadEnabled(final boolean cellarServiceSparqlLoadEnabled, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceSparqlLoadEnabled_Key, cellarServiceSparqlLoadEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceNotificationItemsPerPage(final int cellarServiceNotificationItemsPerPage,
                                                         final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceNotificationItemsPerPage_Key, cellarServiceNotificationItemsPerPage, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceOntoLoadEnabled(final boolean cellarServiceOntoLoadEnabled, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceOntoLoadEnabled_Key, cellarServiceOntoLoadEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceRDFStoreCleanerEnabled(final boolean cellarServiceRDFStoreCleanerEnabled,
                                                       final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceRDFStoreCleanerEnabled_Key, cellarServiceRDFStoreCleanerEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceRDFStoreCleanerExecutorCorePoolSize(final int cellarServiceRDFStoreCleanerExecutorCorePoolSize,
                                                                    final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceRDFStoreCleanerExecutorCorePoolSize_Key, cellarServiceRDFStoreCleanerExecutorCorePoolSize,
                syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceRDFStoreCleanerExecutorMaxPoolSize(final int cellarServiceRDFStoreCleanerExecutorMaxPoolSize,
                                                                   final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceRDFStoreCleanerExecutorMaxPoolSize_Key, cellarServiceRDFStoreCleanerExecutorMaxPoolSize,
                syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceRDFStoreCleanerExecutorQueueCapacity(final int cellarServiceRDFStoreCleanerExecutorQueueCapacity,
                                                                     final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceRDFStoreCleanerExecutorQueueCapacity_Key,
                cellarServiceRDFStoreCleanerExecutorQueueCapacity, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceRDFStoreCleanerHierarchiesBatchSize(final int cellarServiceRDFStoreCleanerHierarchiesBatchSize,
                                                                    final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceRDFStoreCleanerHierarchiesBatchSize_Key, cellarServiceRDFStoreCleanerHierarchiesBatchSize,
                syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceRDFStoreCleanerMode(final CellarServiceRDFStoreCleanerMode cellarServiceRDFStoreCleanerMode,
                                                    final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceRDFStoreCleanerMode_Key, cellarServiceRDFStoreCleanerMode.toString(), syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceRDFStoreCleanerResourcesBatchSize(final int cellarServiceRDFStoreCleanerResourcesBatchSize,
                                                                  final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceRDFStoreCleanerResourcesBatchSize_Key, cellarServiceRDFStoreCleanerResourcesBatchSize,
                syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarUriDisseminationBase(final String cellarUriDisseminationBase, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarUriDisseminationBase_Key, cellarUriDisseminationBase, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarUriResourceBase(final String cellarUriResourceBase, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarUriResourceBase_Key, cellarUriResourceBase, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceSparqlUri(final String cellarUriSparqlService, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceSparqlUri_Key, cellarUriSparqlService, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarUriTranformationResourceBase(final String cellarUriTranformationResourceBase,
                                                      final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarUriTranformationResourceBase_Key, cellarUriTranformationResourceBase, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setS3ServiceIngestionCacheEnabled(final boolean s3ServiceIngestionCacheEnabled,
                                                  final EXISTINGPROPS_MODE... syncMode) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setTestDatabaseCronSettings(final String testDatabaseCronSettings, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(testDatabaseCronSettings_Key, testDatabaseCronSettings, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setTestDatabaseEnabled(final boolean testDatabaseEnabled, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(testDatabaseEnabled_Key, testDatabaseEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setTestEnabled(final boolean testEnabled, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(testEnabled_Key, testEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setTestRollbackEnabled(final boolean testRollbackEnabled, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(testRollbackEnabled_Key, testRollbackEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setTestVirtuosoRollbackEnabled(final boolean testRollbackEnabled, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(testVirtuosoRollbackEnabled_Key, testRollbackEnabled, syncMode);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void setTestIndexingDelayExecutionEnabled(final boolean testIndexingDelayExecutionEnabled, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(testIndexingDelayExecutionEnabled_Key, testIndexingDelayExecutionEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setTestVirtuosoRetryEnabled(final boolean testVirtuosoRetryEnabled, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(testVirtuosoRetryEnabled_Key, testVirtuosoRetryEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setTestS3RetryEnabled(final boolean testS3RetryEnabled, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(testS3RetryEnabled_Key, testS3RetryEnabled, syncMode);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void setTestProcessMonitorPhase(ProcessMonitorPhase testProcessMonitorPhase, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(testProcessMonitorPhase_Key, testProcessMonitorPhase.toString(), syncMode);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void setTestProcessMonitorPhaseReached(final boolean testProcessMonitorPhaseReached, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(testProcessMonitorPhaseReached_Key, testProcessMonitorPhaseReached, syncMode);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceAlignerExecutorMaxPoolSize(final int maxPoolSize, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceAlignerExecutorMaxPoolSize_Key, maxPoolSize, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceAlignerExecutorMaxPoolSize() {
        return getPropertyManager().getIntProperty(cellarServiceAlignerExecutorMaxPoolSize_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceAlignerExecutorQueueCapacity(final int queueCapacity, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceAlignerExecutorQueueCapacity_Key, queueCapacity, syncMode);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceAlignerExecutorQueueCapacity() {
        return getPropertyManager().getIntProperty(cellarServiceAlignerExecutorQueueCapacity_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceAlignerMode(final String mode, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceAlignerMode_Key, mode, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServiceAlignerMode() {
        return getPropertyManager().getProperty(cellarServiceAlignerMode_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceAlignerHierarchiesBatchSize(final int batchSize, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceAlignerHierarchiesBatchSize_Key, batchSize, syncMode);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceAlignerHierarchiesBatchSize() {
        return getPropertyManager().getIntProperty(cellarServiceAlignerHierarchiesBatchSize_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceAlignerExecutorCorePoolSize(final int corePoolSize, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceAlignerExecutorCorePoolSize_Key, corePoolSize, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceAlignerExecutorCorePoolSize() {
        return getPropertyManager().getIntProperty(cellarServiceAlignerExecutorCorePoolSize_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceAlignerExecutorEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceAlignerExecutorEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceAlignerExecutorEnabled(final boolean enabled, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceAlignerExecutorEnabled_Key, enabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isVirtuosoIngestionEnabled() {
        return getPropertyManager().getBooleanProperty(virtuosoIngestionEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setVirtuosoIngestionEnabled(final boolean enabled, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(virtuosoIngestionEnabled_Key, enabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public VirtuosoIngestionRetryStrategy getVirtuosoIngestionRetryStrategy() {
        return VirtuosoIngestionRetryStrategy.resolve(getPropertyManager().getProperty(virtuosoIngestionRetryStrategy_Key.toString()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setVirtuosoIngestionRetryStrategy(final VirtuosoIngestionRetryStrategy virtuosoIngestionRetryStrategy,
                                                  final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(virtuosoIngestionRetryStrategy_Key, virtuosoIngestionRetryStrategy.toString(), syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getVirtuosoIngestionRetryCeilingDelay() {
        return getPropertyManager().getLongProperty(virtuosoIngestionRetryCeilingDelay_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setVirtuosoIngestionRetryCeilingDelay(final long virtuosoIngestionRetryCeilingDelay, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(virtuosoIngestionRetryCeilingDelay_Key, virtuosoIngestionRetryCeilingDelay, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getVirtuosoIngestionRetryMaxRetries() {
        return getPropertyManager().getIntProperty(virtuosoIngestionRetryMaxRetries_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setVirtuosoIngestionRetryMaxRetries(final int virtuosoIngestionRetryMaxRetries, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(virtuosoIngestionRetryMaxRetries_Key, virtuosoIngestionRetryMaxRetries, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getVirtuosoIngestionRetryBase() {
        return getPropertyManager().getIntProperty(virtuosoIngestionRetryBase_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setVirtuosoIngestionRetryBase(final int virtuosoIngestionRetryBase, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(virtuosoIngestionRetryBase_Key, virtuosoIngestionRetryBase, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setVirtuosoIngestionTechnicalEnabled(boolean virtuosoIngestionTechnicalEnabled, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(virtuosoIngestionTechnicalEnabled_Key, virtuosoIngestionTechnicalEnabled, syncMode);
    }

    /**
     *
     */
    @Override
    public boolean isVirtuosoIngestionTechnicalEnabled(){
       return getPropertyManager().getBooleanProperty(virtuosoIngestionTechnicalEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isVirtuosoIngestionBackupEnabled() {
        return getPropertyManager().getBooleanProperty(virtuosoIngestionBackupEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setVirtuosoIngestionBackupEnabled(final boolean virtuosoIngestionBackupEnabled, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(virtuosoIngestionBackupEnabled_Key, virtuosoIngestionBackupEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getVirtuosoIngestionBackupSchedulerCronSettings() {
        return getPropertyManager().getProperty(virtuosoIngestionBackupSchedulerCronSettings_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setVirtuosoIngestionBackupSchedulerCronSettings(final String virtuosoIngestionBackupSchedulerCronSettings,
                                                                final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(virtuosoIngestionBackupSchedulerCronSettings_Key, virtuosoIngestionBackupSchedulerCronSettings,
                syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isVirtuosoIngestionBackupConcurrencyControllerEnabled() {
        return getPropertyManager().getBooleanProperty(virtuosoIngestionBackupConcurrencyControllerEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setVirtuosoIngestionBackupConcurrencyControllerEnabled(final boolean virtuosoIngestionBackupConcurrencyControllerEnabled,
                                                                       final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(virtuosoIngestionBackupConcurrencyControllerEnabled_Key,
                virtuosoIngestionBackupConcurrencyControllerEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getVirtuosoIngestionBackupMaxResults() {
        return getPropertyManager().getIntProperty(virtuosoIngestionBackupMaxResults_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setVirtuosoIngestionBackupMaxResults(final int virtuosoIngestionBackupMaxResults, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(virtuosoIngestionBackupMaxResults_Key, virtuosoIngestionBackupMaxResults, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isVirtuosoIngestionBackupLogVerbose() {
        return getPropertyManager().getBooleanProperty(virtuosoIngestionBackupLogVerbose_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setVirtuosoIngestionBackupLogVerbose(final boolean virtuosoIngestionBackupLogVerbose,
                                                     final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(virtuosoIngestionBackupLogVerbose_Key, virtuosoIngestionBackupLogVerbose, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isVirtuosoIngestionNormalizationEnabled() {
        return getPropertyManager().getBooleanProperty(virtuosoIngestionNormalizationEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setVirtuosoIngestionNormalizationEnabled(final boolean virtuosoIngestionNormalizationEnabled,
                                                         final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(virtuosoIngestionNormalizationEnabled_Key, virtuosoIngestionNormalizationEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isVirtuosoIngestionSkolemizationEnabled() {
        return getPropertyManager().getBooleanProperty(virtuosoIngestionSkolemizationEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setVirtuosoIngestionSkolemizationEnabled(final boolean virtuosoIngestionSkolemizationEnabled,
                                                         final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(virtuosoIngestionSkolemizationEnabled_Key, virtuosoIngestionSkolemizationEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isVirtuosoDisseminationSparqlMapperEnabled() {
        return getPropertyManager().getBooleanProperty(virtuosoDisseminationSparqlMapperEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setVirtuosoDisseminationSparqlMapperEnabled(final boolean virtuosoDisseminationSparqlMapperEnabled,
                                                            final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(virtuosoDisseminationSparqlMapperEnabled_Key, virtuosoDisseminationSparqlMapperEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceSparqlTimeout() {
        return getPropertyManager().getIntProperty(cellarServiceSparqlTimeout_Key.toString());
    }

    @Override
    public int getCellarServiceSparqlMaxConnections() {
        return getPropertyManager().getIntProperty(cellarServiceSparqlMaxConnections_Key.toString());
    }

    @Override
    public int getCellarServiceSparqlTimeoutConnections() {
        return getPropertyManager().getIntProperty(cellarServiceSparqlTimeoutConnections_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServiceSparqlUri() {
        return getPropertyManager().getProperty(cellarServiceSparqlUri_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceSparqlTimeout(final int cellarServiceSparqlTimeout, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceSparqlTimeout_Key, cellarServiceSparqlTimeout, syncMode);
    }

    @Override
    public void setCellarServiceSparqlMaxConnections(int cellarServiceSparqlMaxConnections, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceSparqlMaxConnections_Key, cellarServiceSparqlMaxConnections, syncMode);
    }

    @Override
    public void setCellarServiceSparqlTimeoutConnections(int cellarServiceSparqlTimeoutConnections, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceSparqlTimeoutConnections_Key, cellarServiceSparqlTimeoutConnections, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isVirtuosoIngestionLinkrotRealignmentEnabled() {
        return getPropertyManager().getBooleanProperty(virtuosoIngestionLinkrotRealignmentEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setVirtuosoIngestionLinkrotRealignmentEnabled(final boolean virtuosoIngestionLinkrotRealignmentEnabled,
                                                              final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(virtuosoIngestionLinkrotRealignmentEnabled_Key, virtuosoIngestionLinkrotRealignmentEnabled, syncMode);
    }

    @Override
    public void setCellarWatchAdviceEnabled(boolean enabled, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarWatchAdviceEnabled, enabled, syncMode);
    }

    @Override
    public boolean isCellarWatchAdviceEnabled() {
        return getPropertyManager().getBooleanProperty(cellarWatchAdviceEnabled.toString());
    }

    @Override
    public String getVirtuosoIngestionBackupExceptionMessagesCachePeriod() {
        return getPropertyManager().getProperty(virtuosoIngestionBackupExceptionMessagesCachePeriod_Key.toString());
    }

    @Override
    public void setGracefulShutdownEnabled(boolean enabled, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarGracefulShutdownEnabled_key, enabled, syncMode);
    }

    @Override
    public boolean isGracefulShutdownEnabled() {
        return getPropertyManager().getBooleanProperty(cellarGracefulShutdownEnabled_key.toString());
    }

    @Override
    public void setGracefulShutdownTimeout(long timeout, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarGracefulShutdownTimeout_key, timeout, syncMode);
    }

    @Override
    public long getGracefulShutdownTimeout() {
        return getPropertyManager().getLongProperty(cellarGracefulShutdownTimeout_key.toString());
    }

    @Override
    public void setInferenceOntologyPath(String path, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarInferenceOntologyPath, path, syncMode);
    }

    @Override
    public String getInferenceOntologyPath() {
        return getPropertyManager().getProperty(cellarInferenceOntologyPath.toString());
    }

    @Override
    public String getS3Url() {
        return getPropertyManager().getProperty(s3Url.toString());
    }

    @Override
    public void setS3Url(String url, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(s3Url, url, syncMode);
    }

    @Override
    public void setS3Bucket(String bucketName, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(s3BucketName, bucketName, syncMode);
    }

    @Override
    public String getS3Region() {
        return getPropertyManager().getProperty(s3RegionName.toString());
    }

    @Override
    public void setS3Region(String regionName, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(s3RegionName, regionName, syncMode);
    }

    @Override
    public String getS3Bucket() {
        return getPropertyManager().getProperty(s3BucketName.toString());
    }

    @Override
    public void setS3CredentialsAccessKey(String accessKey, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizePropertySecure(s3CredentialsAccessKey, accessKey, syncMode);
    }

    @Override
    public String getS3CredentialsAccessKey() {
        return getPropertyManager().getProperty(s3CredentialsAccessKey.toString());
    }

    @Override
    public void setS3CredentialsSecretKey(String secretKey, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizePropertySecure(s3CredentialsSecretKey, secretKey, syncMode);
    }

    @Override
    public String getS3CredentialsSecretKey() {
        return getPropertyManager().getProperty(s3CredentialsSecretKey.toString());
    }

    @Override
    public void setS3ClientConnectionTimeout(int timeout, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(s3ClientConnectionTimeout, timeout, syncMode);
    }

    @Override
    public int getS3ClientConnectionTimeout() {
        return getPropertyManager().getIntProperty(s3ClientConnectionTimeout.toString());
    }

    @Override
    public void setS3ClientSocketTimeout(int timeout, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(s3ClientSocketTimeout, timeout, syncMode);
    }

    @Override
    public int getS3ClientSocketTimeout() {
        return getPropertyManager().getIntProperty(s3ClientSocketTimeout.toString());
    }

    @Override
    public void setProxyEnabled(boolean enabled, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(s3ProxyEnabled, enabled, syncMode);
    }

    @Override
    public boolean isProxyEnabled() {
        return getPropertyManager().getBooleanProperty(s3ProxyEnabled.toString());
    }

    @Override
    public void setProxyHost(String host, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(s3ProxyHost, host, syncMode);
    }

    @Override
    public String getProxyHost() {
        return getPropertyManager().getProperty(s3ProxyHost.toString());
    }

    @Override
    public void setProxyPort(int port, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(s3ProxyPort, port, syncMode);
    }

    @Override
    public int getProxyPort() {
        return getPropertyManager().getIntProperty(s3ProxyPort.toString());
    }

    @Override
    public void setProxyUser(String username, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizePropertySecure(s3ProxyUser, username, syncMode);
    }

    @Override
    public String getProxyUser() {
        return getPropertyManager().getProperty(s3ProxyUser.toString());
    }

    @Override
    public void setProxyPassword(String password, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizePropertySecure(s3ProxyPassword, password, syncMode);
    }

    @Override
    public String getProxyPassword() {
        return getPropertyManager().getProperty(s3ProxyPassword.toString());
    }

    @Override
    public boolean isS3Unsafe() {
        return getPropertyManager().getBooleanProperty(s3Unsafe.toString());
    }

    @Override
    public void setS3Unsafe(boolean unsafe, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(s3Unsafe, unsafe, syncMode);
    }

    @Override
    public ImmutableMap<String, String> getS3RollbackTag() {
        final String tag = getPropertyManager().getProperty(s3RollbackTag.toString());
        return ImmutableMap.copyOf(Splitter.on(',')
                .withKeyValueSeparator(':')
                .split(tag));
    }

    @Override
    public void setS3RollbackTag(String tag, EXISTINGPROPS_MODE... syncMode) {
        if (!tag.contains(":")) {
            throw new IllegalStateException(" Separator ':' not found. " +
                    "Expected format: <KEY:VALUE> or <KEY1:VALUE1,KEY2:VALUE2>");
        }
        internalSynchronizeProperty(s3RollbackTag, tag, syncMode);
    }

    /**
     * @return
     */
    @Override
    public String getCellarFeedIngestionDescription() {
        return getPropertyManager().getProperty(cellarFeedIngestionDescription_Key.toString());
    }

    /**
     * @return
     */
    @Override
    public String getCellarFeedEmbargoDescription() {
        return getPropertyManager().getProperty(cellarFeedEmbargoDescription_Key.toString());
    }

    /**
     * @return
     */
    @Override
    public String getCellarFeedNalDescription() {
        return getPropertyManager().getProperty(cellarFeedNalDescription_Key.toString());
    }

    /**
     * @return
     */
    @Override
    public String getCellarFeedSparqlLoadDescription() {
        return getPropertyManager().getProperty(cellarFeedSparqlLoadDescription_Key.toString());
    }

    /**
     * @return
     */
    @Override
    public String getCellarFeedOntologyDescription() {
        return getPropertyManager().getProperty(cellarFeedOntologyDescription_Key.toString());
    }

    /**
     * @param cellarFeedIngestionDescription
     */
    @Override
    public void setCellarFeedIngestionDescription(String cellarFeedIngestionDescription,EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFeedIngestionDescription_Key, cellarFeedIngestionDescription, syncMode);
    }

    /**
     * @param cellarFeedEmbargoDescription
     */
    @Override
    public void setCellarFeedEmbargoDescription(String cellarFeedEmbargoDescription, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFeedEmbargoDescription_Key, cellarFeedEmbargoDescription, syncMode);
    }

    /**
     * @param cellarFeedNalDescription
     */
    @Override
    public void setCellarFeedNalDescription(String cellarFeedNalDescription, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFeedNalDescription_Key, cellarFeedNalDescription, syncMode);
    }

    /**
     * @param cellarFeedSparqlLoadDescription
     */
    @Override
    public void setCellarFeedSparqlLoadDescription(String cellarFeedSparqlLoadDescription, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFeedSparqlLoadDescription_Key, cellarFeedSparqlLoadDescription, syncMode);
    }

    /**
     * @param cellarFeedOntologyDescription
     */
    @Override
    public void setCellarFeedOntologyDescription(String cellarFeedOntologyDescription, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFeedOntologyDescription_Key, cellarFeedOntologyDescription, syncMode);
    }

    /**
     * @return
     */
    @Override
    public String getCellarFeedIngestionTitle() {
        return getPropertyManager().getProperty(cellarFeedIngestionTitle_Key.toString());
    }

    /**
     * @return
     */
    @Override
    public String getCellarFeedEmbargoTitle() {
        return getPropertyManager().getProperty(cellarFeedEmbargoTitle_Key.toString());
    }

    /**
     * @return
     */
    @Override
    public String getCellarFeedNalTitle() {
        return getPropertyManager().getProperty(cellarFeedNalTitle_Key.toString());
    }

    /**
     * @return
     */
    @Override
    public String getCellarFeedSparqlLoadTitle() {
        return getPropertyManager().getProperty(cellarFeedSparqlLoadTitle_Key.toString());
    }

    /**
     * @return
     */
    @Override
    public String getCellarFeedOntologyTitle() {
        return getPropertyManager().getProperty(cellarFeedOntologyTitle_Key.toString());
    }

    /**
     * @param cellarFeedIngestionTitle
     */
    @Override
    public void setCellarFeedIngestionTitle(String cellarFeedIngestionTitle, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFeedIngestionTitle_Key, cellarFeedIngestionTitle, syncMode);
    }

    /**
     * @param cellarFeedEmbargoTitle
     */
    @Override
    public void setCellarFeedEmbargoTitle(String cellarFeedEmbargoTitle, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFeedEmbargoTitle_Key, cellarFeedEmbargoTitle, syncMode);
    }

    /**
     * @param cellarFeedNalTitle
     */
    @Override
    public void setCellarFeedNalTitle(String cellarFeedNalTitle, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFeedNalTitle_Key, cellarFeedNalTitle, syncMode);
    }

    /**
     * @param cellarFeedSparqlLoadTitle
     */
    @Override
    public void setCellarFeedSparqlLoadTitle(String cellarFeedSparqlLoadTitle, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFeedSparqlLoadTitle_Key, cellarFeedSparqlLoadTitle, syncMode);
    }

    /**
     * @param cellarFeedOntologyTitle
     */
    @Override
    public void setCellarFeedOntologyTitle(String cellarFeedOntologyTitle, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFeedOntologyTitle_Key, cellarFeedOntologyTitle, syncMode);
    }

    /**
     * @return
     */
    @Override
    public String getCellarFeedIngestionItemTitle() {
        return getPropertyManager().getProperty(cellarFeedIngestionItemTitle_Key.toString());
    }

    /**
     * @return
     */
    @Override
    public String getCellarFeedEmbargoItemTitle() {
        return getPropertyManager().getProperty(cellarFeedEmbargoItemTitle_Key.toString());
    }

    /**
     * @return
     */
    @Override
    public String getCellarFeedNalItemTitle() {
        return getPropertyManager().getProperty(cellarFeedNalItemTitle_Key.toString());
    }

    /**
     * @return
     */
    @Override
    public String getCellarFeedSparqlLoadItemTitle() {
        return getPropertyManager().getProperty(cellarFeedSparqlLoadItemTitle_Key.toString());
    }

    /**
     * @return
     */
    @Override
    public String getCellarFeedOntologyItemTitle() {
        return getPropertyManager().getProperty(cellarFeedOntologyItemTitle_Key.toString());
    }

    /**
     * @return
     */
    @Override
    public String getCellarFeedIngestionEntrySummary() {
        return getPropertyManager().getProperty(cellarFeedIngestionEntrySummary_Key.toString());
    }

    /**
     * @return
     */
    @Override
    public String getCellarFeedEmbargoEntrySummary() {
        return getPropertyManager().getProperty(cellarFeedEmbargoEntrySummary_Key.toString());
    }

    /**
     * @return
     */
    @Override
    public String getCellarFeedOntologyEntrySummary() {
        return getPropertyManager().getProperty(cellarFeedOntologyEntrySummary_Key.toString());
    }

    /**
     * @return
     */
    @Override
    public String getCellarFeedNalEntrySummary() {
        return getPropertyManager().getProperty(cellarFeedNalEntrySummary_Key.toString());
    }

    /**
     * @return
     */
    @Override
    public String getCellarFeedSparqlLoadEntrySummary() {
        return getPropertyManager().getProperty(cellarFeedSparqlLoadEntrySummary_Key.toString());
    }

    /**
     * @param cellarFeedIngestionEntrySummary
     * @param syncMode
     */
    @Override
    public void setCellarFeedIngestionEntrySummary(String cellarFeedIngestionEntrySummary, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFeedIngestionEntrySummary_Key, cellarFeedIngestionEntrySummary, syncMode);
    }

    /**
     * @param cellarFeedEmbargoEntrySummary
     * @param syncMode
     */
    @Override
    public void setCellarFeedEmbargoEntrySummary(String cellarFeedEmbargoEntrySummary, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFeedEmbargoEntrySummary_Key, cellarFeedEmbargoEntrySummary, syncMode);
    }

    /**
     * @param cellarFeedOntologyEntrySummary
     * @param syncMode
     */
    @Override
    public void setCellarFeedOntologyEntrySummary(String cellarFeedOntologyEntrySummary, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFeedOntologyEntrySummary_Key, cellarFeedOntologyEntrySummary, syncMode);
    }

    /**
     * @param cellarFeedNalEntrySummary
     * @param syncMode
     */
    @Override
    public void setCellarFeedNalEntrySummary(String cellarFeedNalEntrySummary, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFeedNalEntrySummary_Key, cellarFeedNalEntrySummary, syncMode);
    }

    /**
     * @param cellarFeedSparqlLoadEntrySummary
     * @param syncMode
     */
    @Override
    public void setCellarFeedSparqlLoadEntrySummary(String cellarFeedSparqlLoadEntrySummary, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFeedSparqlLoadEntrySummary_Key, cellarFeedSparqlLoadEntrySummary, syncMode);
    }

    /**
     * @param cellarFeedIngestionItemTitle
     */
    @Override
    public void setCellarFeedIngestionItemTitle(String cellarFeedIngestionItemTitle, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFeedIngestionItemTitle_Key, cellarFeedIngestionItemTitle, syncMode);
    }

    /**
     * @param cellarFeedEmbargoItemTitle
     */
    @Override
    public void setCellarFeedEmbargoItemTitle(String cellarFeedEmbargoItemTitle, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFeedEmbargoItemTitle_Key, cellarFeedEmbargoItemTitle, syncMode);
    }

    /**
     * @param cellarFeedNalItemTitle
     */
    @Override
    public void setCellarFeedNalItemTitle(String cellarFeedNalItemTitle, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFeedNalItemTitle_Key, cellarFeedNalItemTitle, syncMode);
    }

    /**
     * @param cellarFeedSparqlLoadItemTitle
     */
    @Override
    public void setCellarFeedSparqlLoadItemTitle(String cellarFeedSparqlLoadItemTitle, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFeedSparqlLoadItemTitle_Key, cellarFeedSparqlLoadItemTitle, syncMode);
    }

    /**
     * @param cellarFeedOntologyItemTitle
     */
    @Override
    public void setCellarFeedOntologyItemTitle(String cellarFeedOntologyItemTitle, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarFeedOntologyItemTitle_Key, cellarFeedOntologyItemTitle, syncMode);
    }

    /**
     * @param enabled
     * @param syncMode
     */
    @Override
    public void setDebugVerboseLoggingEnabled(boolean enabled, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(debugVerboseLoggingEnabled_Key,enabled,syncMode);
    }

    /**
     * @return
     */
    @Override
    public boolean isDebugVerboseLoggingEnabled() {
        return getPropertyManager().getBooleanProperty(debugVerboseLoggingEnabled_Key.toString());
    }

    @Override
    public int getCellarMetsUploadCallbackTimeout() {
        return getPropertyManager().getIntProperty(cellarMetsUploadCallbackTimeout_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarMetsUploadCallbackTimeout(final int cellarMetsUploadCallbackTimeout, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarMetsUploadCallbackTimeout_Key, cellarMetsUploadCallbackTimeout, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarStatusServiceBaseUrl() {
        return getPropertyManager().getProperty(cellarStatusServiceBaseUrl_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarStatusServiceBaseUrl(String path, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarStatusServiceBaseUrl_Key, path, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarStatusServicePackageLevelEndpoint() {
        return getPropertyManager().getProperty(cellarStatusServicePackageLevelEndpoint_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarStatusServicePackageLevelEndpoint(String path, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarStatusServicePackageLevelEndpoint_Key, path, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarStatusServiceObjectLevelEndpoint() {
        return getPropertyManager().getProperty(cellarStatusServiceObjectLevelEndpoint_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarStatusServiceObjectLevelEndpoint(String path, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarStatusServiceObjectLevelEndpoint_Key, path, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarStatusServiceLookupEndpoint() {
        return getPropertyManager().getProperty(cellarStatusServiceLookupEndpoint_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarStatusServiceLookupEndpoint(String path, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarStatusServiceLookupEndpoint_Key, path, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isS3RetryEnabled() {
        return getPropertyManager().getBooleanProperty(s3RetryEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setS3RetryEnabled(final boolean s3RetryEnabled, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(s3RetryEnabled_Key, s3RetryEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getS3RetryMaxAttempts() {
        return getPropertyManager().getIntProperty(s3RetryMaxAttempts_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setS3RetryMaxAttempts(final int s3RetryMaxAttempts, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(s3RetryMaxAttempts_Key, s3RetryMaxAttempts, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getS3RetryDelay() {
        return getPropertyManager().getLongProperty(s3RetryDelay_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setS3RetryDelay(final long s3RetryDelay, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(s3RetryDelay_Key, s3RetryDelay, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isS3RetryRandom() {
        return getPropertyManager().getBooleanProperty(s3RetryRandom_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setS3RetryRandom(final boolean s3RetryRandom, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(s3RetryRandom_Key, s3RetryRandom, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getS3RetryMultiplier() {
        return getPropertyManager().getIntProperty(s3RetryMultiplier_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setS3RetryMultiplier(final int s3RetryMultiplier, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(s3RetryMultiplier_Key, s3RetryMultiplier, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getS3RetryMaxDelay() {
        return getPropertyManager().getLongProperty(s3RetryMaxDelay_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setS3RetryMaxDelay(final long s3RetryMaxDelay, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(s3RetryMaxDelay_Key, s3RetryMaxDelay, syncMode);
    }
    
    @Override
    public void setCellarServiceUserAccessNotificationEmailEnabled(boolean enabled,
                                                                   EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceUserAccessNotificationEmailEnabled_Key, enabled, syncMode);
    }

    @Override
    public boolean getCellarServiceUserAccessNotificationEmailEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceUserAccessNotificationEmailEnabled_Key.toString());
    }

    @Override
    public void setCellarEmailJobCron(String cron, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarEmailCron_Key, cron, syncMode);
    }

    @Override
    public String getCellarEmailJobCron() {
        return getPropertyManager().getProperty(cellarEmailCron_Key.toString());
    }

    @Override
    public void setCellarServiceEmailCleanupMaxAgeDays(int maxAgeDays, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceEmailCleanupMaxAgeDays_Key, maxAgeDays, syncMode);
    }

    @Override
    public int getCellarServiceEmailCleanupMaxAgeDays() {
        return getPropertyManager().getIntProperty(cellarServiceEmailCleanupMaxAgeDays_Key.toString());
    }
    
    @Override
    public void setCellarServiceEmailEnabled(boolean enabled,
                                             EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceEmailEnabled_Key, enabled, syncMode);
    }

    @Override
    public boolean getCellarServiceEmailEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceEmailEnabled_Key.toString());
    }

    @Override
    public void setCellarServiceEuloginUserMigrationEnabled(boolean enabled,
                                                            EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceEuloginUserMigrationEnabled_Key, enabled, syncMode);
    }

    @Override
    public boolean getCellarServiceEuloginUserMigrationEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceEuloginUserMigrationEnabled_Key.toString());
    }

    @Override
    public void setCellarMailHost(String host, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarMailHost_Key, host, syncMode);
    }
    
    @Override
    public String getCellarMailHost() {
        return getPropertyManager().getProperty(cellarMailHost_Key.toString());
    }
    
    @Override
    public void setCellarMailPort(int port, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarMailPort_Key, port, syncMode);
    }
    
    @Override
    public int getCellarMailPort() {
        return getPropertyManager().getIntProperty(cellarMailPort_Key.toString());
    }

    @Override
    public void setCellarMailSmtpUser(String user, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarMailSmtpUser_Key, user, syncMode);
    }
    
    @Override
    public String getCellarMailSmtpUser() {
        return getPropertyManager().getProperty(cellarMailSmtpUser_Key.toString());
    }
    
    @Override
    public void setCellarMailSmtpPassword(String password, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarMailSmtpPassword_Key, password, syncMode);
    }
    
    @Override
    public String getCellarMailSmtpPassword() {
        return getPropertyManager().getProperty(cellarMailSmtpPassword_Key.toString());
    }

    @Override
    public void setCellarMailSmtpAuth(boolean enabled, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarMailSmtpAuth_Key, enabled, syncMode);
    }
    
    @Override
    public boolean getCellarMailSmtpAuth() {
        return getPropertyManager().getBooleanProperty(cellarMailSmtpAuth_Key.toString());
    }

    @Override
    public void setCellarMailSmtpStarttlsEnable(boolean enabled, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarMailSmtpStarttlsEnable_Key, enabled, syncMode);
    }
    
    @Override
    public boolean getCellarMailSmtpStarttlsEnable() {
        return getPropertyManager().getBooleanProperty(cellarMailSmtpStarttlsEnable_Key.toString());
    }

    @Override
    public void setCellarMailSmtpStarttlsRequired(boolean required, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarMailSmtpStarttlsRequired_Key, required, syncMode);
    }
    
    @Override
    public boolean getCellarMailSmtpStarttlsRequired() {
        return getPropertyManager().getBooleanProperty(cellarMailSmtpStarttlsRequired_Key.toString());
    }
    
    @Override
    public void setCellarMailSmtpFrom(String mailFrom, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarMailSmtpFrom_Key, mailFrom, syncMode);
    }
    
    @Override
    public String getCellarMailSmtpFrom() {
        return getPropertyManager().getProperty(cellarMailSmtpFrom_Key.toString());
    }

    @Override
    public void setCellarMetsUploadBufferSize(Integer bufferSize,
                                              EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarMetsUploadBufferSize_Key, bufferSize, syncMode);
    }

    @Override
    public Integer getCellarMetsUploadBufferSize() {
        return getPropertyManager().getIntProperty(cellarMetsUploadBufferSize_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setVirtuosoIngestionBackupExceptionMessagesCachePeriod(final String virtuosoIngestionBackupExceptionMessagesCachePeriod,
                                                                       final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(virtuosoIngestionBackupExceptionMessagesCachePeriod_Key,
                virtuosoIngestionBackupExceptionMessagesCachePeriod, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServiceLanguagesSkosCachePeriod() {
        return getPropertyManager().getProperty(cellarServiceLanguagesSkosCachePeriod_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceLanguagesSkosCachePeriod(final String languagesSkosCachePeriod, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceLanguagesSkosCachePeriod_Key, languagesSkosCachePeriod, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServiceFileTypesSkosCachePeriod() {
        return getPropertyManager().getProperty(cellarServiceFileTypesSkosCachePeriod_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceFileTypesSkosCachePeriod(final String cellarServiceFileTypesSkosCachePeriod,
                                                         final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceFileTypesSkosCachePeriod_Key, cellarServiceFileTypesSkosCachePeriod, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean getS3ServiceIngestionDatastreamRenamingEnabled() {
        return getPropertyManager().getBooleanProperty(s3ServiceIngestionDatastreamRenamingEnabled_Key.toString());
    }

    @Override
    public void setS3ServiceIngestionDatastreamRenamingEnabled(boolean renamingEnabled, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(s3ServiceIngestionDatastreamRenamingEnabled_Key, renamingEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceDisseminationMementoEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceDisseminationMementoEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceDisseminationMementoEnabled(final boolean cellarServiceDisseminationMementoEnabled,
                                                            final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceDisseminationMementoEnabled_Key, cellarServiceDisseminationMementoEnabled, syncMode);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceBatchJobPoolCorePoolSize(final String cellarServiceBatchJobThreadPoolCorePoolSize,
                                                         final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceBatchJobPoolCorePoolSize_Key, cellarServiceBatchJobThreadPoolCorePoolSize, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceBatchJobPoolMaxPoolSize(final String cellarServiceBatchJobThreadPoolMaxPoolSize,
                                                        final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceBatchJobPoolMaxPoolSize_Key, cellarServiceBatchJobThreadPoolMaxPoolSize, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceBatchJobPoolQueueSize(final String cellarServiceBatchJobThreadPoolQueueSize,
                                                      final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceBatchJobPoolQueueSize_Key, cellarServiceBatchJobThreadPoolQueueSize, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceBatchJobPoolSelectionSize(final String cellarServiceBatchJobThreadPoolSelectionSize,
                                                          final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceBatchJobPoolSelectionSize_Key, cellarServiceBatchJobThreadPoolSelectionSize, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServiceBatchJobPoolCorePoolSize() {
        return getPropertyManager().getProperty(cellarServiceBatchJobPoolCorePoolSize_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServiceBatchJobPoolMaxPoolSize() {
        return getPropertyManager().getProperty(cellarServiceBatchJobPoolMaxPoolSize_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServiceBatchJobPoolQueueSize() {
        return getPropertyManager().getProperty(cellarServiceBatchJobPoolQueueSize_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServiceBatchJobPoolSelectionSize() {
        return getPropertyManager().getProperty(cellarServiceBatchJobPoolSelectionSize_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIntegrationArchivistEnabled(final boolean cellarServiceIntegrationArchivistEnabled,
                                                            final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIntegrationArchivistEnabled_Key, cellarServiceIntegrationArchivistEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean getCellarServiceIntegrationArchivistEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceIntegrationArchivistEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIntegrationArchivistBaseUrl(final String cellarServiceIntegrationArchivistBaseUrl,
                                                            final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIntegrationArchivistBaseUrl_Key, cellarServiceIntegrationArchivistBaseUrl, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServiceIntegrationArchivistBaseUrl() {
        return getPropertyManager().getProperty(cellarServiceIntegrationArchivistBaseUrl_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIntegrationFixityEnabled(final boolean cellarServiceIntegrationFixityEnabled,
                                                         final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIntegrationFixityEnabled_Key, cellarServiceIntegrationFixityEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Boolean getCellarServiceIntegrationFixityEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceIntegrationFixityEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceIntegrationFixityBaseUrl(final String cellarServiceIntegrationFixityBaseUrl,
                                                         final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIntegrationFixityBaseUrl_Key, cellarServiceIntegrationFixityBaseUrl, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServiceIntegrationFixityBaseUrl() {
        return getPropertyManager().getProperty(cellarServiceIntegrationFixityBaseUrl_Key.toString());
    }

    @Override
    public String getCellarServiceIntegrationValidationBaseUrl() {
        return getPropertyManager().getProperty(cellarServiceIntegrationValidationBaseUrl_Key.toString());
    }

    @Override
    public void setCellarServiceIntegrationValidationBaseUrl(final String cellarServiceIntegrationValidationBaseUrl,
                                                             final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceIntegrationValidationBaseUrl_Key, cellarServiceIntegrationValidationBaseUrl, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceCellarResourceMaxElementsInMemory(final int cellarServiceCellarResourceMaxElementsInMemory,
                                                                  final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceCellarResourceMaxElementsInMemory_Key, cellarServiceCellarResourceMaxElementsInMemory,
                syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceCellarResourceMaxElementsInMemory() {
        return getPropertyManager().getIntProperty(cellarServiceCellarResourceMaxElementsInMemory_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceCellarResourceTimeToLiveSeconds(final int cellarServiceCellarResourceTimeToLiveSeconds,
                                                                final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceCellarResourceTimeToLiveSeconds_Key, cellarServiceCellarResourceTimeToLiveSeconds,
                syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceCellarResourceTimeToLiveSeconds() {
        return getPropertyManager().getIntProperty(cellarServiceCellarResourceTimeToLiveSeconds_Key.toString());
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceEmbeddedNoticeCacheMaxBytesLocalHeap(final String cellarServiceEmbeddedNoticeCacheMaxBytesLocalHeap,
                                                                  final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceEmbeddedNoticeCacheMaxBytesLocalHeap_Key, cellarServiceEmbeddedNoticeCacheMaxBytesLocalHeap,
                syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServiceEmbeddedNoticeCacheMaxBytesLocalHeap() {
        return getPropertyManager().getProperty(cellarServiceEmbeddedNoticeCacheMaxBytesLocalHeap_Key.toString());
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceEmbeddedNoticeCacheMaxBytesLocalDisk(final String cellarServiceEmbeddedNoticeCacheMaxBytesLocalDisk,
                                                                  final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceEmbeddedNoticeCacheMaxBytesLocalDisk_Key, cellarServiceEmbeddedNoticeCacheMaxBytesLocalDisk,
                syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServiceEmbeddedNoticeCacheMaxBytesLocalDisk() {
        return getPropertyManager().getProperty(cellarServiceEmbeddedNoticeCacheMaxBytesLocalDisk_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceEmbeddedNoticeCacheTimeToLiveSeconds(final long cellarServiceEmbeddedNoticeCacheTimeToLiveSeconds,
                                                                final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceEmbeddedNoticeCacheTimeToLiveSeconds_Key, cellarServiceEmbeddedNoticeCacheTimeToLiveSeconds,
                syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long getCellarServiceEmbeddedNoticeCacheTimeToLiveSeconds() {
        return getPropertyManager().getIntProperty(cellarServiceEmbeddedNoticeCacheTimeToLiveSeconds_Key.toString());
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceEmbeddedNoticeCacheEvaluationStatisticsEnabled(final boolean cellarServiceEmbeddedNoticeCacheEvaluationStatisticsEnabled,
                                                                final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceEmbeddedNoticeCacheEvaluationStatisticsEnabled_Key, cellarServiceEmbeddedNoticeCacheEvaluationStatisticsEnabled,
                syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceEmbeddedNoticeCacheEvaluationStatisticsEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceEmbeddedNoticeCacheEvaluationStatisticsEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceDisseminationRetrieveEmbargoedResourceEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceDisseminationRetrieveEmbargoedResourceEnabled_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceDisseminationRetrieveEmbargoedResourceEnabled(
            final boolean cellarServiceDisseminationRetrieveEmbargoedResourceEnabled, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceDisseminationRetrieveEmbargoedResourceEnabled_Key,
                cellarServiceDisseminationRetrieveEmbargoedResourceEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceSipDependencyCheckerEnabled(final String cellarServiceSipDependencyCheckerEnabled, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceSipDependencyCheckerEnabled_Key, cellarServiceSipDependencyCheckerEnabled, syncMode);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceSipDependencyCheckerEnabled() {
        return getPropertyManager().getBooleanProperty(cellarServiceSipDependencyCheckerEnabled_Key.toString());
    }

    /**
    * {@inheritDoc}
    */
    @Override
    public void setCellarServiceSipDependencyCheckerCronSettings(final String cellarServiceSipDependencyCheckerCronSettings, final EXISTINGPROPS_MODE... syncMode) {
    	internalSynchronizeProperty(cellarServiceSipDependencyCheckerCronSettings_Key, cellarServiceSipDependencyCheckerCronSettings, syncMode);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServiceSipDependencyCheckerCronSettings() {
    	return getPropertyManager().getProperty(cellarServiceSipDependencyCheckerCronSettings_Key.toString());
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceSipDependencyCheckerPoolThreads(final int corePoolSize, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceSipDependencyCheckerPoolThreads_Key, corePoolSize, syncMode);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public int getCellarServiceSipDependencyCheckerPoolThreads() {
    	return getPropertyManager().getIntProperty(cellarServiceSipDependencyCheckerPoolThreads_Key.toString());
    }

    /**
    * {@inheritDoc}
    */
    @Override
    public void setCellarServiceSipQueueManagerCronSettings(final String cellarServiceSipQueueManagerCronSettings, final EXISTINGPROPS_MODE... syncMode) {
    	internalSynchronizeProperty(cellarServiceSipQueueManagerCronSettings_Key, cellarServiceSipQueueManagerCronSettings, syncMode);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServiceSipQueueManagerCronSettings() {
    	return getPropertyManager().getProperty(cellarServiceSipQueueManagerCronSettings_Key.toString());
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceSipQueueManagerFileAttributeDateType(final String cellarServiceSipQueueManagerFileAttributeDateType, final EXISTINGPROPS_MODE... syncMode) {
    	internalSynchronizeProperty(cellarServiceSipQueueManagerFileAttributeDateType_Key, cellarServiceSipQueueManagerFileAttributeDateType, syncMode);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellarServiceDorieDedicatedEnabled() {
    	return getPropertyManager().getBooleanProperty(cellarServiceDorieDedicatedEnabled_Key.toString());
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarServiceDorieDedicatedEnabled(final boolean cellarServiceDorieDedicatedEnabled, final EXISTINGPROPS_MODE... syncMode) {
    	internalSynchronizeProperty(cellarServiceDorieDedicatedEnabled_Key, cellarServiceDorieDedicatedEnabled, syncMode);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarServiceSipQueueManagerFileAttributeDateType() {
    	return CellarServiceSipQueueManagerFileAttributeDateType.resolve(
    			getPropertyManager().getProperty(cellarServiceSipQueueManagerFileAttributeDateType_Key.toString()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, Map<String, Object>> getAllConfigurationProperties() {
        final Map<String, Map<String, Object>> allProps = new TreeMap<>();

        final Properties properties = this.getPropertyManager().getProperties();
        for (final Object keyObj : properties.keySet()) {
            final String key = (String) keyObj;
            final Object value = properties.get(key);
            final Map<String, Object> propProps = new HashMap<>();
            propProps.put("value", value);
            propProps.put("persistent", this.isPersistent(key));
            allProps.put(key, propProps);
        }

        return allProps;
    }

    @Override
    public void setCellarServiceNalLoadInferenceMaxIteration(int cellarServiceNalLoadInferenceMaxIteration, final EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarServiceNalLoadInferenceMaxIteration_Key,
                cellarServiceNalLoadInferenceMaxIteration, syncMode);
    }

    @Override
    public int getCellarServiceNalLoadInferenceMaxIteration() {
        return getPropertyManager().getIntProperty(cellarServiceNalLoadInferenceMaxIteration_Key.toString());
    }

    /**
     * {@inheritDoc}
     */
	@Override
	public long getCellarServiceIngestionConcurrencyControllerThreadDelayMilliseconds() {
		return getPropertyManager().getLongProperty(cellarServiceIngestionConcurrencyControllerThreadDelayMilliseconds_Key.toString());
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public void setCellarServiceIngestionConcurrencyControllerThreadDelayMilliseconds(
			long cellarServiceIngestionConcurrencyControllerThreadDelayMilliseconds, EXISTINGPROPS_MODE... syncMode) {
		internalSynchronizeProperty(cellarServiceIngestionConcurrencyControllerThreadDelayMilliseconds_Key, cellarServiceIngestionConcurrencyControllerThreadDelayMilliseconds, syncMode);
	}
    
    /**
     * {@inheritDoc}
     */
    @Override
    public CellarStatusServiceShaclReportLevel getCellarStatusServiceShaclReportLevel() {
        return CellarStatusServiceShaclReportLevel.resolve(
                getPropertyManager().getProperty(cellarStatusServiceShaclReportLevel_Key.toString()));
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void setCellarStatusServiceShaclReportLevel(
            CellarStatusServiceShaclReportLevel cellarStatusServiceShaclReportLevel, EXISTINGPROPS_MODE... syncMode) {
        internalSynchronizeProperty(cellarStatusServiceShaclReportLevel_Key, cellarStatusServiceShaclReportLevel.toString(), syncMode);
    }

}
