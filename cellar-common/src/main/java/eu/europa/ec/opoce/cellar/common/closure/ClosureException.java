/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.common.closure
 *        FILE : ClosureException.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 22-03-2013
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.closure;

import eu.europa.ec.opoce.cellar.exception.BuildableException;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

/**
 * This is a {@link ClosureException} buildable by an {@link ExceptionBuilder}.</br>
 * All methods that calls closures must embed the original {@link Exception} in this {@link ClosureException} before re-throwing.
 * </br> 
 * ON : 22-03-2013
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public class ClosureException extends CellarException {

    private static final long serialVersionUID = -9189528130107447198L;

    /**
     * Constructs a new exception with its associated builder.
     * 
     * @param builder the builder to use for building the exception
     */
    public ClosureException(final ExceptionBuilder<? extends BuildableException> builder) {
        super(builder);
    }

}
