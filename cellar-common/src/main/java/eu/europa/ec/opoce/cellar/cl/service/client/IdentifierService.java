/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.client
 *             FILE : IdentifierService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Sep 30, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$$
 *          VERSION : $LastChangedRevision$$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.client;

import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Service layer interface for common access to PID persistence layer.
 *
 * @author omeurice
 *
 */
public interface IdentifierService {

    /** The Constant PREFIX_CELLAR. */
    String PREFIX_CELLAR = "cellar:";

    /**
     * Gets the all pid if cellar id exists.
     *
     * @param identifier the identifier
     * @return the all pid if cellar id exists
     */
    List<String> getAllPIDIfCellarIdExists(final Identifier identifier);

    /**
     * Get the {@link Identifier} corresponding to the given {@code id}:
     * it will contain all the <code>ProductionIdentifier</code>s and the associated Cellar's UUID.
     *
     * @param id May be a Cellar id or a production id.
     * @return A <code>Identifier</code> instance with the list of <code>ProductionIdentifier</code>s and the associated Cellar's UUID.
     */
    Identifier getIdentifier(String id);

    /**
     * Get the collection of {@link Identifier}s corresponding to the given list {@code ids}:
     * each identifier will contain all the <code>ProductionIdentifier</code>s and the Cellar's UUID associated to the n-th id.
     *
     * @param ids It is a list that may contain Cellar ids or a production ids.
     * @return A collection of <code>Identifier</code>s instance, each containing the list of <code>ProductionIdentifier</code>s and the Cellar's UUID associated to the n-th id.
     */
    Collection<Identifier> getIdentifiers(final Collection<String> ids);

    /**
     * Checks if is cellar uuid.
     *
     * @param pid the pid
     * @return true, if is cellar uuid
     */
    boolean isCellarUUID(final String pid);

    /**
     * Get the CELLAR UUID correponding to the given production identifier.
     *
     * @param pid
     *            Production identifier.
     * @return The CELLAR UUID corresponding to the given production identifier.
     */
    String getCellarUUID(String pid);

    /**
     * Get the CELLAR UUID correponding to the given production identifier or null if not found.
     * No exception are thrown.
     * @param pid
     *            Production identifier.
     * @return The CELLAR UUID corresponding to the given production identifier or ull if not found.
     */
    String getCellarUUIDIfExists(String pid);

    /**
     * Gets the unknown production identifers.
     *
     * @param pids the pids
     * @return the unknown production identifers
     */
    Collection<String> getUnknownProductionIdentifers(final List<String> pids);

    /**
     * Gets the cellar uuid if exists in.
     *
     * @param pids the pids
     * @return the cellar uuid if exists in
     */
    String getCellarUUIDIfExistsIn(final List<String> pids);

    /**
     * Gets the cellar uui ds.
     *
     * @param pids the pids
     * @return the cellar uui ds
     */
    Set<String> getCellarUUIDs(final List<String> pids);

    /**
     * Get all the cellar identifier of an object tree with all associated content ids.
     *
     * @param cellarId
     *            The cellar identifier from where we want the tree of ids to be retrieved.
     * @return A <code>List</code> of <code>Identifier</code> instances.
     */
    List<Identifier> getTreeIdentifiers(String cellarId);

    /**
     * Get all the cellar identifier of an object tree with all associated content ids.
     *
     * @param cellarId
     *            The cellar identifier from where we want the tree of ids to be retrieved.
     * @param withContentStream
     *              Flag to return also content stream id or not.
     * @return A <code>List</code> of <code>Identifier</code> instances.
     */
    List<Identifier> getTreeIdentifiers(String cellarId, boolean withContentStream);

    /**
     * Gets the prefixed from uri.
     *
     * @param uri the uri
     * @return the prefixed from uri
     */
    String getPrefixedFromUri(String uri);

    /**
     * Checks if is uri.
     *
     * @param uriOrPrefix the uri or prefix
     * @return true, if is uri
     */
    boolean isUri(String uriOrPrefix);

    /**
     * Gets the uri.
     *
     * @param uriOrPrefix the uri or prefix
     * @return the uri
     */
    String getUri(String uriOrPrefix);

    /**
     * Gets the prefixed.
     *
     * @param uriOrPrefix the uri or prefix
     * @return the prefixed
     */
    String getPrefixed(String uriOrPrefix);

    /**
     * Gets the prefixed or null.
     *
     * @param uriOrPrefix the uri or prefix
     * @return the prefixed or null
     */
    String getPrefixedOrNull(String uriOrPrefix);

    /**
     * Gets the cellar prefixed.
     *
     * @param uriOrPrefix the uri or prefix
     * @return the cellar prefixed
     */
    String getCellarPrefixed(String uriOrPrefix);

    /**
     * Gets the cellar prefixed or null.
     *
     * @param uriOrPrefix the uri or prefix
     * @return the cellar prefixed or null
     */
    String getCellarPrefixedOrNull(String uriOrPrefix);

    /**
     * Gets the cellar uri or null.
     *
     * @param uriOrPrefix the uri or prefix
     * @return the cellar uri or null
     */
    String getCellarUriOrNull(final String uriOrPrefix);

}
