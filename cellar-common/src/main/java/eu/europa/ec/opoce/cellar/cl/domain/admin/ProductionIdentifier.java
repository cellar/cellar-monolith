/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : CellarIdentifier.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 24 mai 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.admin;

import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import java.util.Date;

/**
 * This object represents a production identifiers.
 *
 * @author dsavares
 *
 */
@Entity
@Table(name = "PRODUCTION_IDENTIFIER")
@NamedQueries({
        @NamedQuery(name = "getByIdentifier", query = "from ProductionIdentifier where PRODUCTION_ID = ?0"),
        @NamedQuery(name = "getByIdentifiers", query = "from ProductionIdentifier where PRODUCTION_ID in (:pids)"),
        @NamedQuery(name = "getUnboundResources", query = "from ProductionIdentifier where CELLAR_IDENTIFIER_ID is null"),
        @NamedQuery(name = "getProductionIdentifiersByCellarIdentifier", query = "from ProductionIdentifier where CELLAR_IDENTIFIER_ID = ?0"),
        @NamedQuery(name = "getProductionIdentifiersByCellarId", query = "from ProductionIdentifier where cellarIdentifier.uuid = ?0"),
        @NamedQuery(name = "getProductionIdentifiersByProductionId", query = "from ProductionIdentifier as pi " + //
                "where pi.cellarIdentifier.uuid = some (select spi.cellarIdentifier.uuid from ProductionIdentifier as spi where spi.productionId = ?0)" //
        ),
        @NamedQuery(name = "getProductionIdentifiersByCellarIds", query = "from ProductionIdentifier where cellarIdentifier.uuid in (:ids)" //
        ), @NamedQuery(name = "getProductionIdentifiersByProductionIds", query = "from ProductionIdentifier as pi " + //
                "where pi.cellarIdentifier.uuid = some (select spi.cellarIdentifier.uuid from ProductionIdentifier as spi where spi.productionId in (:ids))" //
        )})
public class ProductionIdentifier {

    /**
     * Internal id.
     */
    private Long id;

    /**
     * The production id.
     */
    private String productionId;

    /**
     * The creation date of the cellar identifier.
     */
    private Date creationDate;

    //    /** The read only. */
    //    private Boolean readOnly;

    /**
     * The cellar identifier.
     */
    private CellarIdentifier cellarIdentifier;

    /**
     * Instantiates a new production identifier.
     */
    public ProductionIdentifier() {
    }

    /**
     * Instantiates a new production identifier.
     *
     * @param cellarId the cellar id
     * @param productionId the production id
     */
    public ProductionIdentifier(final ContentIdentifier cellarId, final ContentIdentifier productionId) {
        this.cellarIdentifier = new CellarIdentifier(cellarId.getIdentifier());
        this.productionId = productionId.getIdentifier();
        this.creationDate = new Date();
    }

    /**
     * Instantiates a new production identifier.
     *
     * @param cellarId the cellar id
     * @param productionId the production id
     */
    public ProductionIdentifier(final String cellarId, final ContentIdentifier productionId) {
        this.cellarIdentifier = new CellarIdentifier(cellarId);
        this.productionId = productionId.getIdentifier();
        this.creationDate = new Date();
    }

    /**
     * Gets the value of the cellarIdentifier.
     *
     * @return the cellarIdentifier
     */
    @ManyToOne
    @JoinColumn(name = "CELLAR_IDENTIFIER_ID")
    public CellarIdentifier getCellarIdentifier() {
        return cellarIdentifier;
    }

    /**
     * Gets the value of the id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    /**
     * Gets the value of the productionId.
     *
     * @return the productionId
     */
    @NaturalId
    @Column(name = "PRODUCTION_ID")
    public String getProductionId() {
        return productionId;
    }

    /**
     * Sets a new value for the cellarIdentifier.
     *
     * @param cellarIdentifier
     *            the cellarIdentifier to set
     */
    public void setCellarIdentifier(final CellarIdentifier cellarIdentifier) {
        this.cellarIdentifier = cellarIdentifier;
    }

    /**
     * Set the value of the id.
     *
     * @param id
     *            the id to set
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Sets a new value for the productionId.
     *
     * @param productionId
     *            the productionId to set
     */
    public void setProductionId(final String productionId) {
        this.productionId = productionId;
    }

    /**
     * Gets the value of the creationDate.
     *
     * @return the creationDate
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATION_DATE")
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * Sets a new value for the creationDate.
     *
     * @param creationDate
     *            the creationDate to set
     */
    public void setCreationDate(final Date creationDate) {
        this.creationDate = creationDate;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (id == null ? 0 : id.hashCode());
        result = prime * result + (productionId == null ? 0 : productionId.hashCode());
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ProductionIdentifier other = (ProductionIdentifier) obj;
        if (id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!id.equals(other.id)) {
            return false;
        }
        if (productionId == null) {
            if (other.productionId != null) {
                return false;
            }
        } else if (!productionId.equals(other.productionId)) {
            return false;
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "ProductionIdentifier [id=" + id + ", pid=" + productionId + ", cellarId=" + cellarIdentifier + "]";
    }
}
