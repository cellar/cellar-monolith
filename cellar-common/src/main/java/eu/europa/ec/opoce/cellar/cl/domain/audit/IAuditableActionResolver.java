/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.audit
 *             FILE : IAuditableNameResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 19-12-2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.audit;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 19-12-2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IAuditableActionResolver {

    /**
     * Return the auditable action on the basis of the parameters of the audited method.
     * 
     * @param the parameters of the audited method
     * @return the auditable action on the basis of the parameters of the audited method
     */
    AuditTrailEventAction resolveAction(final Object[] args);
}
