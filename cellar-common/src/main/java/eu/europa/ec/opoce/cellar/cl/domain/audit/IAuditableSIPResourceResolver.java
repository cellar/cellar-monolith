package eu.europa.ec.opoce.cellar.cl.domain.audit;

import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 03-07-2023
 *
 * @author EUROPEAN DYNAMICS S.A.
 */
public interface IAuditableSIPResourceResolver {
    
    /**
     * Return the SIPResource argument of the auditable object on the basis of the parameters of the audited method.
     *
     * @param args parameters of the audited method
     * @return the SIPResource argument of the auditable object on the basis of the parameters of the audited method
     */
    SIPResource resolveSIPResource(final Object[] args);
}
