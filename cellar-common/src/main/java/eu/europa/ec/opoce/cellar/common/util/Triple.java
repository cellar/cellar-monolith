package eu.europa.ec.opoce.cellar.common.util;

/**
 * <p>Triple class.</p>
 *
 */
public class Triple<T1, T2, T3> {

    private final T1 one;
    private final T2 two;
    private final T3 three;

    /**
     * <p>Constructor for Triple.</p>
     *
     * @param one a T1 object.
     * @param two a T2 object.
     * @param three a T3 object.
     * @param <T1> a T1 object.
     * @param <T2> a T2 object.
     * @param <T3> a T3 object.
     */
    public Triple(T1 one, T2 two, T3 three) {
        this.one = one;
        this.two = two;
        this.three = three;
    }

    /**
     * <p>Getter for the field <code>one</code>.</p>
     *
     * @return a T1 object.
     */
    public T1 getOne() {
        return one;
    }

    /**
     * <p>Getter for the field <code>two</code>.</p>
     *
     * @return a T2 object.
     */
    public T2 getTwo() {
        return two;
    }

    /**
     * <p>Getter for the field <code>three</code>.</p>
     *
     * @return a T3 object.
     */
    public T3 getThree() {
        return three;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Triple<?, ?, ?> triple = (Triple<?, ?, ?>) o;

        if (one != null ? !one.equals(triple.one) : triple.one != null)
            return false;
        if (three != null ? !three.equals(triple.three) : triple.three != null)
            return false;
        if (two != null ? !two.equals(triple.two) : triple.two != null)
            return false;

        return true;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        int result = one != null ? one.hashCode() : 0;
        result = 31 * result + (two != null ? two.hashCode() : 0);
        result = 31 * result + (three != null ? three.hashCode() : 0);
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return new StringBuilder().append("Triple (").append(one).append(", ").append(two).append(")").toString();
    }
}
