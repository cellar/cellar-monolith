package eu.europa.ec.opoce.cellar.domain.content.mets;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.cl.concurrency.Concurrent.Locker;
import eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLockSession;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

public enum OperationType implements Locker {

    Read("read", "read.req") {

        @Override
        public <IN, OUT> OUT accept(final OperationTypeVisitor<IN, OUT> visitor, final IN in) {
            return visitor.visitRead(in);
        }
    },
    Create("create", "create.req") {

        @Override
        public <IN, OUT> OUT accept(final OperationTypeVisitor<IN, OUT> visitor, final IN in) {
            return visitor.visitCreate(in);
        }
    },
    CreateOrIgnore("create_or_ignore", "create_or_ignore.req") {

        @Override
        public <IN, OUT> OUT accept(final OperationTypeVisitor<IN, OUT> visitor, final IN in) {
            return visitor.visitCreateOrIgnore(in);
        }
    },
    Update("update", "update.req") {

        @Override
        public <IN, OUT> OUT accept(final OperationTypeVisitor<IN, OUT> visitor, final IN in) {
            return visitor.visitUpdate(in);
        }
    },
    Delete("delete", "delete.req") {

        @Override
        public <IN, OUT> OUT accept(final OperationTypeVisitor<IN, OUT> visitor, final IN in) {
            return visitor.visitDelete(in);
        }

    };

    private String[] values;

    OperationType(final String... values) {
        this.values = values;
    }

    public String[] getValues() {
        return this.values;
    }

    public String getRepresentativeValue() {
        return this.values != null && this.values.length > 0 ? this.values[0] : null;
    }

    public abstract <IN, OUT> OUT accept(OperationTypeVisitor<IN, OUT> visitor, IN in);

    public boolean accept(final StructMap structMap) {
        return structMap.getMetsDocument().getType() == this;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.concurrency.Concurrent.Locker#doLock(eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLockSession, java.lang.Object[])
     */
    @Override
    public void doLock(final ICellarLockSession cellarLockSession, final Object... args) throws InterruptedException {
        this.getConcurrencyController().lockForIngestion(cellarLockSession, (MetsPackage) args[0], (StructMap) args[1]);
    }

    public static OperationType getByXmlValue(final String value) {
        for (final OperationType currOpType : OperationType.values()) {
            for (final String currOpTypeValue : currOpType.values) {
                if (currOpTypeValue.equalsIgnoreCase(value)) {
                    return currOpType;
                }
            }
        }
        throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.UNKNOWN_METS_OPERATION)
                .withMessage("Cannot find OperationType with value '{}'.").withMessageArgs(value).build();
    }

}
