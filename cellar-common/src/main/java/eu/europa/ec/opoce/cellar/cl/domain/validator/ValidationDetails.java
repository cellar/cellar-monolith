package eu.europa.ec.opoce.cellar.cl.domain.validator;

import java.io.Serializable;

import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;

/**
 * This class is given as parameters to validator.
 * It contains the reference to the object to validate.
 * The SIPResource object contains information on the SIP.
 * 
 * @author dcraeye
 *
 */
public class ValidationDetails implements Serializable {

    /** the class's serial. */
    private static final long serialVersionUID = -1313486637437056807L;

    Object toValidate;
    SIPResource sipResource;

    public ValidationDetails() {
        super();
    }

    public ValidationDetails(Object toValidate, SIPResource sipResource) {
        super();
        this.toValidate = toValidate;
        this.sipResource = sipResource;
    }

    public Object getToValidate() {
        return toValidate;
    }

    public void setToValidate(Object toValidate) {
        this.toValidate = toValidate;
    }

    public SIPResource getSipResource() {
        return sipResource;
    }

    public void setSipResource(SIPResource sipResource) {
        this.sipResource = sipResource;
    }

}
