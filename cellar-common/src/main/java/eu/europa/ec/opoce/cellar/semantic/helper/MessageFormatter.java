package eu.europa.ec.opoce.cellar.semantic.helper;

import java.util.HashMap;
import java.util.Map;

// contributors: lizongbo: proposed special treatment of array parameter values
// Jörn Huxhorn: pointed out double[] omission, suggested deep array copy

/**
 * Formats messages according to very simple substitution rules. Substitutions
 * can be made 1, 2 or more arguments.
 * <p/>
 * For example,
 * <p/>
 * <pre>MessageFormatter.format(&quot;Hi {}.&quot;, &quot;there&quot;)</pre>
 * <p/>
 * will return the string "Hi there.".
 * <p/>
 * The {} pair is called the <em>formatting anchor</em>. It serves to
 * designate the location where arguments need to be substituted within the
 * message pattern.
 * <p/>
 * In case your message contains the '{' or the '}' character, you do not have
 * to do anything special unless the '}' character immediately follows '{'. For
 * example,
 * <p/>
 * <pre>
 * MessageFormatter.format(&quot;Set {1,2,3} is not equal to {}.&quot;, &quot;1,2&quot;);
 * </pre>
 * <p/>
 * will return the string "Set {1,2,3} is not equal to 1,2.".
 * <p/>
 * <p>If for whatever reason you need to place the string "{}" in the message
 * without its <em>formatting anchor</em> meaning, then you need to escape the
 * '{' character with '\', that is the backslash character. Only the '{'
 * character should be escaped. There is no need to escape the '}' character.
 * For example,
 * <p/>
 * <pre>
 * MessageFormatter.format(&quot;Set \\{} is not equal to {}.&quot;, &quot;1,2&quot;);
 * </pre>
 * <p/>
 * will return the string "Set {} is not equal to 1,2.".
 * <p/>
 * <p/>
 * The escaping behavior just described can be overridden by escaping the escape
 * character '\'. Calling
 * <p/>
 * <pre>
 * MessageFormatter.format(&quot;File name is C:\\\\{}.&quot;, &quot;file.zip&quot;);
 * </pre>
 * <p/>
 * will return the string "File name is C:\file.zip".
 * <p/>
 * <p/>
 * See {@link #format(String, Object)}, {@link #format(String, Object, Object)}
 * and {@link #arrayFormat(String, Object[])} methods for more details.
 */
final public class MessageFormatter {

    /** Constant <code>DELIM_START='{'</code> */
    static final char DELIM_START = '{';
    /** Constant <code>DELIM_STOP='}'</code> */
    static final char DELIM_STOP = '}';
    /** Constant <code>DELIM_STR="{}"</code> */
    static final String DELIM_STR = "{}";
    /** Constant <code>ESCAPE_CHAR='\\'</code> */
    private static final char ESCAPE_CHAR = '\\';

    /**
     * Performs single argument substitution for the 'messagePattern' passed as
     * parameter.
     * <p/>
     * For example,
     * <p/>
     * <pre>
     * MessageFormatter.format(&quot;Hi {}.&quot;, &quot;there&quot;);
     * </pre>
     * <p/>
     * will return the string "Hi there.".
     * <p/>
     *
     * @param messagePattern The message pattern which will be parsed and formatted
     * @param arg            The argument to be substituted in place of the formatting
     *                       anchor
     * @return The formatted message
     */
    final public static String format(String messagePattern, Object arg) {
        return arrayFormat(messagePattern, new Object[] {
                arg});
    }

    /**
     * Performs a two argument substitution for the 'messagePattern' passed as
     * parameter.
     * <p/>
     * For example,
     * <p/>
     * <pre>
     * MessageFormatter.format(&quot;Hi {}. My name is {}.&quot;, &quot;Alice&quot;, &quot;Bob&quot;);
     * </pre>
     * <p/>
     * will return the string "Hi Alice. My name is Bob.".
     *
     * @param messagePattern The message pattern which will be parsed and formatted
     * @param arg1           The argument to be substituted in place of the first
     *                       formatting anchor
     * @param arg2           The argument to be substituted in place of the second
     *                       formatting anchor
     * @return The formatted message
     */
    final public static String format(final String messagePattern, Object arg1, Object arg2) {
        return arrayFormat(messagePattern, new Object[] {
                arg1, arg2});
    }

    /**
     * Same principle as the {@link #format(String, Object)} and
     * {@link #format(String, Object, Object)} methods except that any number of
     * arguments can be passed in an array.
     *
     * @param messagePattern The message pattern which will be parsed and formatted
     * @param argArray       An array of arguments to be substituted in place of
     *                       formatting anchors
     * @return The formatted message
     */
    final public static String arrayFormat(final String messagePattern, final Object[] argArray) {
        if (messagePattern == null) {
            return null;
        }
        if (argArray == null) {
            return messagePattern;
        }
        int i = 0;
        int j;
        StringBuffer sbuf = new StringBuffer(messagePattern.length() + 50);

        for (int L = 0; L < argArray.length; L++) {

            j = messagePattern.indexOf(DELIM_STR, i);

            if (j == -1) {
                // no more variables
                if (i == 0) { // this is a simple string
                    return messagePattern;
                } else { // add the tail string which contains no variables and return
                    // the result.
                    sbuf.append(messagePattern.substring(i, messagePattern.length()));
                    return sbuf.toString();
                }
            } else {
                if (isEscapedDelimeter(messagePattern, j)) {
                    if (!isDoubleEscaped(messagePattern, j)) {
                        L--; // DELIM_START was escaped, thus should not be incremented
                        sbuf.append(messagePattern.substring(i, j - 1));
                        sbuf.append(DELIM_START);
                        i = j + 1;
                    } else {
                        // The escape character preceding the delemiter start is
                        // itself escaped: "abc x:\\{}"
                        // we have to consume one backward slash
                        sbuf.append(messagePattern.substring(i, j - 1));
                        deeplyAppendParameter(sbuf, argArray[L], new HashMap<Object[], Object>());
                        i = j + 2;
                    }
                } else {
                    // normal case
                    sbuf.append(messagePattern.substring(i, j));
                    deeplyAppendParameter(sbuf, argArray[L], new HashMap<Object[], Object>());
                    i = j + 2;
                }
            }
        }
        // append the characters following the last {} pair.
        sbuf.append(messagePattern.substring(i, messagePattern.length()));
        return sbuf.toString();
    }

    /**
     * <p>isEscapedDelimeter</p>
     *
     * @param messagePattern a {@link java.lang.String} object.
     * @param delimeterStartIndex a int.
     * @return a boolean.
     */
    final static boolean isEscapedDelimeter(String messagePattern, int delimeterStartIndex) {

        if (delimeterStartIndex == 0) {
            return false;
        }
        char potentialEscape = messagePattern.charAt(delimeterStartIndex - 1);
        if (potentialEscape == ESCAPE_CHAR) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * <p>isDoubleEscaped</p>
     *
     * @param messagePattern a {@link java.lang.String} object.
     * @param delimeterStartIndex a int.
     * @return a boolean.
     */
    final static boolean isDoubleEscaped(String messagePattern, int delimeterStartIndex) {
        if (delimeterStartIndex >= 2 && messagePattern.charAt(delimeterStartIndex - 2) == ESCAPE_CHAR) {
            return true;
        } else {
            return false;
        }
    }

    // special treatment of array values was suggested by 'lizongbo'

    /**
     * <p>deeplyAppendParameter</p>
     *
     * @param sbuf a {@link java.lang.StringBuffer} object.
     * @param o a {@link java.lang.Object} object.
     * @param seenMap a {@link java.util.Map} object.
     */
    private static <V> void deeplyAppendParameter(StringBuffer sbuf, Object o, Map<Object[], V> seenMap) {
        if (o == null) {
            sbuf.append("null");
            return;
        }
        if (!o.getClass().isArray()) {
            sbuf.append(o);
        } else {
            // check for primitive array types because they
            // unfortunately cannot be cast to Object[]
            if (o instanceof Boolean[]) {
                booleanArrayAppend(sbuf, (Boolean[]) o);
            } else if (o instanceof Byte[]) {
                byteArrayAppend(sbuf, (Byte[]) o);
            } else if (o instanceof Character[]) {
                charArrayAppend(sbuf, (Character[]) o);
            } else if (o instanceof Short[]) {
                shortArrayAppend(sbuf, (Short[]) o);
            } else if (o instanceof Integer[]) {
                intArrayAppend(sbuf, (Integer[]) o);
            } else if (o instanceof Long[]) {
                longArrayAppend(sbuf, (Long[]) o);
            } else if (o instanceof Float[]) {
                floatArrayAppend(sbuf, (Float[]) o);
            } else if (o instanceof Double[]) {
                doubleArrayAppend(sbuf, (Double[]) o);
            } else {
                objectArrayAppend(sbuf, (Object[]) o, seenMap);
            }
        }
    }

    /**
     * <p>objectArrayAppend</p>
     *
     * @param sbuf a {@link java.lang.StringBuffer} object.
     * @param a an array of {@link java.lang.Object} objects.
     * @param seenMap a {@link java.util.Map} object.
     */
    private static <V> void objectArrayAppend(StringBuffer sbuf, Object[] a, Map<Object[], V> seenMap) {
        sbuf.append('[');
        if (!seenMap.containsKey(a)) {
            seenMap.put(a, null);
            final int len = a.length;
            for (int i = 0; i < len; i++) {
                deeplyAppendParameter(sbuf, a[i], seenMap);
                if (i != len - 1)
                    sbuf.append(", ");
            }
            // allow repeats in siblings
            seenMap.remove(a);
        } else {
            sbuf.append("...");
        }
        sbuf.append(']');
    }

    /**
     * <p>booleanArrayAppend</p>
     *
     * @param sbuf a {@link java.lang.StringBuffer} object.
     * @param a an array of boolean.
     */
    private static void booleanArrayAppend(StringBuffer sbuf, Boolean[] a) {
        sbuf.append('[');
        final int len = a.length;
        for (int i = 0; i < len; i++) {
            sbuf.append(a[i]);
            if (i != len - 1)
                sbuf.append(", ");
        }
        sbuf.append(']');
    }

    /**
     * <p>byteArrayAppend</p>
     *
     * @param sbuf a {@link java.lang.StringBuffer} object.
     * @param a an array of byte.
     */
    private static void byteArrayAppend(StringBuffer sbuf, Byte[] a) {
        sbuf.append('[');
        final int len = a.length;
        for (int i = 0; i < len; i++) {
            sbuf.append(a[i]);
            if (i != len - 1)
                sbuf.append(", ");
        }
        sbuf.append(']');
    }

    /**
     * <p>charArrayAppend</p>
     *
     * @param sbuf a {@link java.lang.StringBuffer} object.
     * @param a an array of char.
     */
    private static void charArrayAppend(StringBuffer sbuf, Character[] a) {
        sbuf.append('[');
        final int len = a.length;
        for (int i = 0; i < len; i++) {
            sbuf.append(a[i]);
            if (i != len - 1)
                sbuf.append(", ");
        }
        sbuf.append(']');
    }

    /**
     * <p>shortArrayAppend</p>
     *
     * @param sbuf a {@link java.lang.StringBuffer} object.
     * @param a an array of short.
     */
    private static void shortArrayAppend(StringBuffer sbuf, Short[] a) {
        sbuf.append('[');
        final int len = a.length;
        for (int i = 0; i < len; i++) {
            sbuf.append(a[i]);
            if (i != len - 1)
                sbuf.append(", ");
        }
        sbuf.append(']');
    }

    /**
     * <p>intArrayAppend</p>
     *
     * @param sbuf a {@link java.lang.StringBuffer} object.
     * @param a an array of int.
     */
    private static void intArrayAppend(StringBuffer sbuf, Integer[] a) {
        sbuf.append('[');
        final int len = a.length;
        for (int i = 0; i < len; i++) {
            sbuf.append(a[i]);
            if (i != len - 1)
                sbuf.append(", ");
        }
        sbuf.append(']');
    }

    /**
     * <p>longArrayAppend</p>
     *
     * @param sbuf a {@link java.lang.StringBuffer} object.
     * @param a an array of long.
     */
    private static void longArrayAppend(StringBuffer sbuf, Long[] a) {
        sbuf.append('[');
        final int len = a.length;
        for (int i = 0; i < len; i++) {
            sbuf.append(a[i]);
            if (i != len - 1)
                sbuf.append(", ");
        }
        sbuf.append(']');
    }

    /**
     * <p>floatArrayAppend</p>
     *
     * @param sbuf a {@link java.lang.StringBuffer} object.
     * @param a an array of float.
     */
    private static void floatArrayAppend(StringBuffer sbuf, Float[] a) {
        sbuf.append('[');
        final int len = a.length;
        for (int i = 0; i < len; i++) {
            sbuf.append(a[i]);
            if (i != len - 1)
                sbuf.append(", ");
        }
        sbuf.append(']');
    }

    /**
     * <p>doubleArrayAppend</p>
     *
     * @param sbuf a {@link java.lang.StringBuffer} object.
     * @param a an array of double.
     */
    private static void doubleArrayAppend(StringBuffer sbuf, Double[] a) {
        sbuf.append('[');
        final int len = a.length;
        for (int i = 0; i < len; i++) {
            sbuf.append(a[i]);
            if (i != len - 1)
                sbuf.append(", ");
        }
        sbuf.append(']');
    }
}
