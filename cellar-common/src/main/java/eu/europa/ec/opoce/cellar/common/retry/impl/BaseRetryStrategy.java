/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.retry.impl
 *             FILE : BaseRetryStrategy.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 06-03-2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.retry.impl;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.common.closure.PClosure;
import eu.europa.ec.opoce.cellar.common.retry.IRetryAwareConnection;
import eu.europa.ec.opoce.cellar.common.retry.IRetryStrategy;
import eu.europa.ec.opoce.cellar.common.util.ThreadUtils;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.ExceptionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <class_description> An base abstract implementation of {@link eu.europa.ec.opoce.cellar.common.retry.IRetryStrategy}:
 * the retry algorithm is to be provided by subclasses.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 06-03-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public abstract class BaseRetryStrategy<P> implements IRetryStrategy<P> {

    private static final Logger LOG = LoggerFactory.getLogger(BaseRetryStrategy.class);

    private static final int DEFAULT_MAX_ATTEMPTS = 20;
    private static final int DEFAULT_BASE = 3000;

    // the maximum number of attempts to perform. -1 means to try indefinitely until the process succeeds
    private int maxAttempts;

    // after this waiting time, the exponentiation stops, and this value is used as the next waiting time. -1 means no ceiling defined 
    private long ceilingDelay;

    // the base to consider when calculating the delay
    protected int base;

    // the retry-aware connection to execute
    private IRetryAwareConnection<P> retryAwareConnection;

    // the name of the recipient towards which the retry strategy is meant to operate: useful for logging purposes
    private String recipientName;

    /**
     * @see eu.europa.ec.opoce.cellar.common.retry.IRetryStrategy#maxAttempts(int)
     */
    @Override
    public IRetryStrategy<P> maxAttempts(final int maxAttempts) {
        this.maxAttempts = (maxAttempts == 0 ? DEFAULT_MAX_ATTEMPTS : maxAttempts);
        this.maxAttempts = (this.maxAttempts == -1 ? Integer.MAX_VALUE : this.maxAttempts);
        return this;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.common.retry.IRetryStrategy#ceilingDelay(long)
     */
    @Override
    public IRetryStrategy<P> ceilingDelay(final long ceilingDelay) {
        this.ceilingDelay = (ceilingDelay == -1 ? Long.MAX_VALUE : ceilingDelay);
        return this;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.common.retry.IRetryStrategy#base(int)
     */
    @Override
    public IRetryStrategy<P> base(final int base) {
        this.base = (base == 0 ? DEFAULT_BASE : base);
        return this;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.common.retry.IRetryStrategy#retryAwareConnection(eu.europa.ec.opoce.cellar.common.retry.IRetryAwareConnection)
     */
    @Override
    public IRetryStrategy<P> retryAwareConnection(final IRetryAwareConnection<P> retryAwareConnection) {
        this.retryAwareConnection = retryAwareConnection;
        return this;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.common.retry.IRetryStrategy#recipientName(java.lang.String)
     */
    @Override
    public IRetryStrategy<P> recipientName(final String recipientName) {
        this.recipientName = recipientName;
        return this;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.common.retry.IRetryStrategy#retryAwareConnection(IRetryAwareConnection)
     */
    @Override
    public <C extends PClosure<P>> void retry(final C connectionCode) throws CellarException {
        final String recipientMessage = StringUtils.isBlank(this.recipientName) ? "" : " towards " + this.recipientName;

        if (this.retryAwareConnection == null) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.RETRY_AWARE_CONN_FAILED)
                    .withMessage("Cannot execute the retry-aware call{}, as the associated connection is not instantiated.")
                    .withMessageArgs(recipientMessage).build();
        }

        Exception exception = null;

        // cycle on attempts
        for (int i = 0; i < this.maxAttempts; i++) {
            // try to execute
            try {
                LOG.debug("Attempt {}/{} to execute the retry-aware call {}..", (i + 1), maxAttempts, recipientMessage);
                this.retryAwareConnection.connect(connectionCode);
                LOG.debug("The retry-aware call {} has been successfully executed at attempt #{}.", recipientMessage, (i + 1));
                return;
            } catch (CellarException e) {
                if (!ExceptionUtils.exceptionContainsMessages(e, this.retryAwareConnection.resolveRetriableMessages())) {
                    throw e;
                }
                exception = e;
            }

            // in case it is not the last retry..
            if (i < this.maxAttempts - 1) {
                // rollback the transaction
                try {
                    this.retryAwareConnection.rollback();
                } catch (Exception e) {
                    throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.RETRY_AWARE_CONN_FAILED)
                            .withMessage(
                                    "The execution of the retry-aware call{} has failed because a problem occurred while rolling back the transaction after attempt #{}.")
                            .withMessageArgs(recipientMessage, i + 1).withCause(e).build();
                }

                // ..and wait the calculate delay before the next retry
                long delay = this.calculateDelay(i);
                delay = (delay > this.ceilingDelay ? this.ceilingDelay : delay);
                LOG.warn("Attempt {}/{} to execute the retry-aware call {} has failed. Waiting now for {} milliseconds before next retry..",
                        (i + 1), maxAttempts, recipientMessage, delay);
                ThreadUtils.waitFor(delay, false);
            }
        }

        // if the last try failed, throw the exception
        throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.RETRY_AWARE_CONN_FAILED)
                .withMessage(
                        "The execution of the retry-aware call{} has failed because the limit of attempts ({}) has been reached. The last thrown exception is in stack trace.")
                .withMessageArgs(recipientMessage, this.maxAttempts).withCause(exception).build();
    }

    /**
     * Calculates the delay in milliseconds between each attempt.
     * 
     * @param currentAttempt the number of the current attempt being executed (the first being 0)
     * @return the calculated delay
     */
    protected abstract long calculateDelay(final int currentAttempt);

}
