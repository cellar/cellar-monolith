/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.util
 *             FILE : BoundedPriorityBlockingQueue.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 16, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.util;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 16, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class BoundedPriorityBlockingQueue<E> implements BlockingQueue<E> {

    private final PriorityBlockingQueue<E> priorityBlockingQueue;

    private final int capacity;
    private final Semaphore in;

    public BoundedPriorityBlockingQueue(final int capacity) {
        this.capacity = capacity;
        this.priorityBlockingQueue = new PriorityBlockingQueue<E>(capacity);
        this.in = new Semaphore(capacity);
    }

    public BoundedPriorityBlockingQueue(final int capacity, final Comparator<E> comparator) {
        this.capacity = capacity;
        this.priorityBlockingQueue = new PriorityBlockingQueue<E>(capacity, comparator);
        this.in = new Semaphore(capacity);
    }

    @Override
    public E element() {
        return this.priorityBlockingQueue.element();
    }

    @Override
    public E peek() {
        return this.priorityBlockingQueue.peek();
    }

    @Override
    public E poll() {
        final E e = this.priorityBlockingQueue.poll();

        if (e != null) {
            this.in.release();
        }

        return e;
    }

    @Override
    public E remove() {
        final E e = this.priorityBlockingQueue.remove();

        if (e != null) {
            this.in.release();
        }

        return e;
    }

    @Override
    public boolean addAll(final Collection<? extends E> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(final Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean retainAll(final Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean add(final E e) {
        this.checkElementNull(e);

        if (this.in.tryAcquire()) {
            return this.priorityBlockingQueue.add(e);
        }

        throw new IllegalStateException();
    }

    @Override
    public boolean offer(final E e) {
        this.checkElementNull(e);

        if (this.in.tryAcquire()) {
            return this.priorityBlockingQueue.offer(e);
        }

        return false;
    }

    private void checkElementNull(final E e) {
        if (e == null) {
            throw new NullPointerException();
        }
    }

    @Override
    public boolean offer(final E e, final long timeout, final TimeUnit unit) throws InterruptedException {

        if (this.in.tryAcquire(timeout, unit)) {
            return this.priorityBlockingQueue.offer(e);
        }

        return false;
    }

    @Override
    public E poll(final long timeout, final TimeUnit unit) throws InterruptedException {
        final E e = this.priorityBlockingQueue.poll(timeout, unit);

        if (e != null) {
            this.in.release();
        }

        return e;
    }

    @Override
    public void put(final E e) throws InterruptedException {
        this.checkElementNull(e);

        this.in.acquire();

        this.priorityBlockingQueue.put(e);
    }

    @Override
    public int remainingCapacity() {
        return this.capacity - this.priorityBlockingQueue.size();
    }

    @Override
    public boolean remove(final Object o) {
        final boolean isRemoved = this.priorityBlockingQueue.remove(o);

        if (isRemoved) {
            this.in.release();
        }

        return isRemoved;
    }

    @Override
    public E take() throws InterruptedException {
        final E e = this.priorityBlockingQueue.take();

        if (e != null) {
            this.in.release();
        }

        return e;
    }

    @Override
    public void clear() {
        this.priorityBlockingQueue.clear();
    }

    @Override
    public boolean containsAll(final Collection<?> arg0) {
        return this.priorityBlockingQueue.containsAll(arg0);
    }

    @Override
    public boolean isEmpty() {
        return this.priorityBlockingQueue.isEmpty();
    }

    @Override
    public Iterator<E> iterator() {
        return this.priorityBlockingQueue.iterator();
    }

    @Override
    public int size() {
        return this.priorityBlockingQueue.size();
    }

    @Override
    public Object[] toArray() {
        return this.priorityBlockingQueue.toArray();
    }

    @Override
    public <T> T[] toArray(final T[] arg0) {
        return this.priorityBlockingQueue.toArray(arg0);
    }

    @Override
    public boolean contains(final Object o) {
        return this.priorityBlockingQueue.contains(o);
    }

    @Override
    public int drainTo(final Collection<? super E> c) {
        final int drained = this.priorityBlockingQueue.drainTo(c);

        this.in.release(drained);

        return drained;
    }

    @Override
    public int drainTo(final Collection<? super E> c, final int maxElements) {
        final int drained = this.priorityBlockingQueue.drainTo(c, maxElements);

        this.in.release(drained);

        return drained;
    }

}
