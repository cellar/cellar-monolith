/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.metadata
 *             FILE : IModelLoader.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : May 23, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.metadata;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : May 23, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IModelLoader<T> {

    /**
     * Loads the models.
     */
    void load(final T data);

    /**
     * Closes the loader.
     */
    void closeQuietly();

}
