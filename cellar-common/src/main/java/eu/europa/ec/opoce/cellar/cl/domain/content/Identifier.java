package eu.europa.ec.opoce.cellar.cl.domain.content;

import eu.europa.ec.opoce.cellar.common.MessageFormatter;
import org.apache.commons.lang.StringUtils;

import java.util.List;

/**
 * Non persisted entity used to give common access to CELLAR identifier and related production
 * identifiers.
 * 
 * @author omeurice
 * 
 */
public class Identifier {

    /**
     * CELLAR UUID.
     */
    private String cellarId;

    /**
     * List of production id.
     */
    private List<String> pids;

    /**
     * Default constructor.
     */
    public Identifier() {

    }

    public Identifier(final String cellarId) {
        this();
        this.cellarId = cellarId;
    }

    /**
     * Create a new instance.
     * 
     * @param cellarId
     *            CELLAR UUID.
     * @param pids
     *            List of production ids.
     */
    public Identifier(final String cellarId, final List<String> pids) {
        this();
        this.cellarId = cellarId;
        this.pids = pids;
    }

    public Identifier(final List<String> pids) {
        this();
        this.pids = pids;
    }

    /**
     * Get the CELLAR UUID.
     * 
     * @return The CELLAR UUID value.
     */
    public String getCellarId() {
        return cellarId;
    }

    /**
     * Set the CELLAR UUID.
     * 
     * @param cellarId
     *            The CELLAR UUID to set.
     */
    public void setCellarId(final String cellarId) {
        this.cellarId = cellarId;
    }

    /**
     * Get the list of production ids.
     * 
     * @return <code>List</code> of production ids values.
     */
    public List<String> getPids() {
        return pids;
    }

    /**
     * Set the <code>List</code> of production ids.
     * 
     * @param pids
     *            The <code>List</code> of production ids values to set.
     */
    public void setPids(final List<String> pids) {
        this.pids = pids;
    }

    public boolean cellarIdExists() {
        return this.cellarId != null;
    }

    @Override
    public String toString() {
        return MessageFormatter.format("[{}: {}]", this.cellarId, StringUtils.join(this.pids, ","));
    }
}
