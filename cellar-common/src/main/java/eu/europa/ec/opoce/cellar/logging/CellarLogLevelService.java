package eu.europa.ec.opoce.cellar.logging;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

/**
 * Service class to retrieve and set the current Cellar Logger level
 * Deprecation: scheduled for deletion in 8.0
 */
@Deprecated
@Service
public class CellarLogLevelService {

    private LoggerContext.CellarLevel level;

    /**
     * <p>init.</p>
     */
    @PostConstruct
    private void init() {
        // TODO retrieve the configured value from the appropriate configuration table
        level = LoggerContext.CellarLevel.INFO; // use INFO as default
    }

    /**
     * Get the current <code>LoggerContext.CellarLevel</code>.
     *
     * @return the current <code>LoggerContext.CellarLevel</code>
     */
    public LoggerContext.CellarLevel getLevel() {
        return level;
    }

    /**
     * Set the current <code>LoggerContext.CellarLevel</code>.
     *
     * @param level a {@link eu.europa.ec.opoce.cellar.logging.LoggerContext.CellarLevel} object.
     */
    public void setLevel(LoggerContext.CellarLevel level) {
        this.level = level;
    }
}
