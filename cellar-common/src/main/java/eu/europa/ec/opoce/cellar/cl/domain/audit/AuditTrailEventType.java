/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.audit
 *        FILE : AuditTrailEventType.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 01-10-2015
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.audit;

/**
 * <class_description> Available types for Audit Trail.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 01-10-2015
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public enum AuditTrailEventType {

    Found, Start, End, Fail;

    public String getName() {
        return this.toString();
    }

}
