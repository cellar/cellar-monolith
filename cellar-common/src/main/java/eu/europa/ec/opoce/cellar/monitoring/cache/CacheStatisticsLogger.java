/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.monitoring.cache
 *        FILE : CacheStatisticsLogger.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 09-01-2023
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2023 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.monitoring.cache;

import java.lang.management.ManagementFactory;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.logging.LogContext;
import eu.europa.ec.opoce.cellar.logging.LogContext.Context;

/**
 * Contains logic for retrieving cache-related statistics from caches
 * registered as MBeans and logging them.
 * @author EUROPEAN DYNAMICS S.A
 */
@Component
@Order(Ordered.LOWEST_PRECEDENCE)
public class CacheStatisticsLogger {

	/**
	 * 
	 */
	private static final Logger LOG = LogManager.getLogger(CacheStatisticsLogger.class);
	/**
	 * The MBean Server.
	 */
	private static final MBeanServer MBEAN_SERVER = ManagementFactory.getPlatformMBeanServer();
	/**
	 * Name of the cache to retrieve for which statistics shall be retrieved.
	 */
	private static final String EMBEDDED_NOTICE_CACHE_NAME = "embeddedNoticeCache";
	/**
	 * The fully qualified name of the cache to retrieve for which statistics shall be retrieved.
	 */
	private static final String EMBEDDED_NOTICE_CACHE_OBJECT_NAME = "net.sf.ehcache:type=CacheStatistics,CacheManager=defaultCacheManager,name=" + EMBEDDED_NOTICE_CACHE_NAME;
	/**
	 * The cache statistics attributes to retrieve.
	 */
	private static final List<String> EMBEDDED_NOTICE_CACHE_STATS_ATTRIBUTES = Arrays.asList(
			"CacheHitPercentage", "CacheHits", "CacheMisses", "InMemoryHitPercentage", "MemoryStoreObjectCount", "OnDiskHitPercentage", "DiskStoreObjectCount");
	
	
	private final ICellarConfiguration cellarConfiguration;
	
	private ObjectName disseminationEmbeddedNoticeCacheObjectName = null;
	
	
	@Autowired
	public CacheStatisticsLogger(@Qualifier("cellarStaticConfiguration") ICellarConfiguration cellarConfiguration) {
		this.cellarConfiguration = cellarConfiguration;
	}


	@PostConstruct
	private void init() {
		try {
			this.disseminationEmbeddedNoticeCacheObjectName = new ObjectName(EMBEDDED_NOTICE_CACHE_OBJECT_NAME);
		} catch (MalformedObjectNameException e) {
			LOG.error("Could not create ObjectName for {}.", EMBEDDED_NOTICE_CACHE_OBJECT_NAME, e);
		}
	}

	/**
	 * Logs the cache-related statistics.
	 */
	@LogContext(Context.CACHE_STATISTICS)
	public void logCacheStatistics() {
		if (cellarConfiguration.isCellarServiceEmbeddedNoticeCacheEvaluationStatisticsEnabled()) {
			StringBuilder sb = new StringBuilder();
			sb.append("Cache: ").append(EMBEDDED_NOTICE_CACHE_NAME).append(", ");
			for (String statsAttribute : EMBEDDED_NOTICE_CACHE_STATS_ATTRIBUTES) {
				try {
					Number value = (Number) MBEAN_SERVER.getAttribute(disseminationEmbeddedNoticeCacheObjectName, statsAttribute);
					sb.append(statsAttribute).append(": ");
					if (value instanceof Double) {
						double percentage = (double) value * 100;
						sb.append((int) percentage).append("%");
					}
					else {
						sb.append(value);
					}
					sb.append(", ");
				} catch (InstanceNotFoundException | AttributeNotFoundException | ReflectionException | MBeanException e) {
					LOG.error("An error occurred while retrieving attribute {} from MBean {}.", statsAttribute, EMBEDDED_NOTICE_CACHE_OBJECT_NAME, e);
				}
			}
			LOG.info(sb.substring(0, sb.length() - 2));
		}
	}
	
}
