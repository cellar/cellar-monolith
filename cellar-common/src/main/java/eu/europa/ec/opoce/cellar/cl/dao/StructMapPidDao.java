package eu.europa.ec.opoce.cellar.cl.dao;

import eu.europa.ec.opoce.cellar.cl.domain.structmap_pid.StructMapPid;
import eu.europa.ec.opoce.cellar.cl.domain.structmap_status_history.StructMapStatusHistory;

import java.util.List;

/**
 * Dao for the Entity {@link StructMapPid}.
 *
 * @author EUROPEAN DYNAMICS S.A.
 *
 */
public interface StructMapPidDao extends BaseDao<StructMapPid, Long>{
    
    /**
     * Return all persistent instances of {@link StructMapPid} belonging to the same {@link StructMapStatusHistory}.
     *
     * @param structMapStatusHistory the {@link StructMapStatusHistory} the pids belong to.
     *
     * @return the list of {@link StructMapPid} references matching the provided argument.
     */
    List<StructMapPid> getByStructMapStatusHistory(StructMapStatusHistory structMapStatusHistory);
}
