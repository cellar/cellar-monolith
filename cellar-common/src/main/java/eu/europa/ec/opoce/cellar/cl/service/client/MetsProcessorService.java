package eu.europa.ec.opoce.cellar.cl.service.client;

import eu.europa.ec.opoce.cellar.domain.content.mets.MetsDocument;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsPackage;

public interface MetsProcessorService {

    /**
     * parse the metsDocument of a metsPackage
     *
     * @param metsPackage the package that needs to be validated and parsed
     * @return in-memory representation of a metsDocument
     */
    MetsDocument convert(MetsPackage metsPackage);

}
