/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : CellarIdentifier.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 8 mars 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.history;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * The Class IdentifiersHistory.
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 8 mars 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Entity
@Table(name = "HISTORY_PID_CID")
@NamedQueries({
        @NamedQuery(name = "getDeleted", query = "FROM IdentifiersHistory WHERE obsolete = false AND productionId IN (:productionIds) ORDER BY creationDate DESC"), //
        @NamedQuery(name = "getIdentifierHistoryTree", query = "FROM IdentifiersHistory WHERE obsolete = false AND productionId LIKE :productionId AND cellarId LIKE :cellarId ORDER BY creationDate DESC"), //
        @NamedQuery(name = "getIdentifierHistoryStartingWithCellarId", query = "FROM IdentifiersHistory WHERE cellarId LIKE :cellarId ORDER BY creationDate DESC")})
public class IdentifiersHistory implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -6420867006703984478L;

    /** The id. */
    @Id
    @Column(name = "ID")
    private long id;

    /** The cellar id. */
    @Column(name = "CELLAR_IDENTIFIER")
    private String cellarId;

    /** The production id. */
    @Column(name = "PRODUCTION_IDENTIFIER")
    private String productionId;

    /** The creation date. */
    @Column(name = "CREATION_DATE")
    private Date creationDate;

    /** The obsolete. */
    @Column(name = "OBSOLETE")
    private Boolean obsolete;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final long id) {
        this.id = id;
    }

    /**
     * Gets the cellar id.
     *
     * @return the cellar id
     */
    public String getCellarId() {
        return this.cellarId;
    }

    /**
     * Sets the cellar id.
     *
     * @param cellarId the new cellar id
     */
    public void setCellarId(final String cellarId) {
        this.cellarId = cellarId;
    }

    /**
     * Gets the production id.
     *
     * @return the production id
     */
    public String getProductionId() {
        return this.productionId;
    }

    /**
     * Sets the production id.
     *
     * @param productionId the new production id
     */
    public void setProductionId(final String productionId) {
        this.productionId = productionId;
    }

    /**
     * Gets the creation date.
     *
     * @return the creation date
     */
    public Date getCreationDate() {
        return this.creationDate;
    }

    /**
     * Sets the creation date.
     *
     * @param creationDate the new creation date
     */
    public void setCreationDate(final Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
    * Checks if is obsolete.
    *
    * @return true, if is obsolete
    */
    public Boolean isObsolete() {
        return this.obsolete;
    }

    /**
     * Sets the obsolete.
     *
     * @param obsolete the new obsolete
     */
    public void setObsolete(final Boolean obsolete) {
        this.obsolete = obsolete;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj, new String[] {
                "id"});
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this, new String[] {
                "id"});
    }

    /** {@inheritDoc} */
    @Override
    public final String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
