package eu.europa.ec.opoce.cellar.common.util;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Collection;
import java.util.UUID;
import java.util.zip.Adler32;
import java.util.zip.CheckedInputStream;
import java.util.zip.Checksum;

/**
 * <p>
 * FileSystemUtils class.
 * </p>
 */
public class FileSystemUtils {

    /**
     * Constant <code>log</code>
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(FileSystemUtils.class);

    /**
     * <p>
     * getResource.
     * </p>
     *
     * @param resourcePath a {@link java.lang.String} object.
     * @return a {@link org.springframework.core.io.Resource} object.
     */
    public static Resource getResource(final String resourcePath) {
        return new ClassPathResource(normalizeResourcePath(resourcePath));
    }

    /**
     * <p>
     * getInputStream.
     * </p>
     *
     * @param resourcePath a {@link java.lang.String} object.
     * @return a {@link java.io.InputStream} object.
     */
    public static InputStream getInputStream(final String resourcePath) {
        return FileSystemUtils.class.getClassLoader().getResourceAsStream(resourcePath);
    }

    /**
     * <p>
     * resolveGenericResource.
     * </p>
     * This method tries to resolve a resource's path this way:<br>
     * - if {@code classpathFirst} is true, first tries to resolve it as a
     * classpath (with higher priority), then as a file system path (with lower
     * priority)<br>
     * - if {@code classpathFirst} is false, first tries to resolve it as a file
     * system path (with higher priority), then as a classpath (with lower
     * priority).
     *
     * @param resourcePath a {@link java.lang.String} object to resolve
     * @return the resolved {@link java.io.InputStream} object.
     * @throws CellarException if the resource cannot be resolved neither as a file system path, neither as a classpath
     */
    public static InputStream resolveGenericResource(final String resourcePath, final boolean classpathFirst)
            throws CellarException {

        InputStream inputStream;
        if (classpathFirst) {
            inputStream = resolveResourceFromClasspath(resourcePath);
            if (inputStream == null) {
                inputStream = resolveResourceFromFileSystem(resourcePath);
            }
        } else {
            inputStream = resolveResourceFromFileSystem(resourcePath);
            if (inputStream == null) {
                inputStream = resolveResourceFromClasspath(resourcePath);
            }
        }

        if (inputStream == null) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("Cannot locate resource at path: {}")
                    .withMessageArgs(resourcePath).build();
        }
        return inputStream;
    }

    private static InputStream resolveResourceFromFileSystem(final String resourcePath) {

        try {
            return new FileInputStream(resourcePath);
        } catch (FileNotFoundException e) {
            LOGGER.error("No file could be found for {}", resourcePath, e);
        }
        return null;
    }

    private static InputStream resolveResourceFromClasspath(final String resourcePath) {
        final PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        final Resource resource = resolver.getResource(resourcePath);

        try {
            return resource.getInputStream();
        } catch (IOException e) {
            LOGGER.error("No file could be found for {}", resourcePath, e);
        }
        return null;
    }

    private static String normalizeResourcePath(final String resourcePath) {
        if (resourcePath == null) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.INPUT_MUST_BE_DEFINED)
                    .withMessage("The resource path is not specified").build();
        }

        String myResourcePath = resourcePath;

        if (!myResourcePath.startsWith("/")) {
            myResourcePath = "/" + myResourcePath;
        }

        return myResourcePath;
    }

    /**
     * If a file is being written, we cannot read it yet: this method waits
     * until a file is completely written, by checking if there is a lock on it.
     * <br/>
     * If the timeout of {@code timeout} is reached, an {@link IOException} is
     * thrown.
     *
     * @param file       the file to check
     * @param retryDelay the delay to wait for before next retry
     * @param timeout    the timeout to wait before giving up with an
     *                   {@link IOException}
     * @return true if the system waited for the file to be completed, false
     * otherwise
     * @throws IOException thrown if the timeout is reached
     */
    public static boolean waitFileCompletionByLock(final File file, final long retryDelay, final long timeout)
            throws IOException {
        boolean waited = false;

        final long startMillis = System.currentTimeMillis();
        while (System.currentTimeMillis() - startMillis < timeout) {
            if (FileSystemUtils.isFileLocked(file)) {
                LOGGER.debug("File is locked because still being copied: waiting {} seconds before next retry..", retryDelay);
                ThreadUtils.waitFor(retryDelay, false);
                waited = true;
            } else {
                return waited;
            }
        }

        throw new IOException("The file '" + file.getAbsolutePath() + "' has not been completely written in "
                + (timeout / 60) + " seconds.");
    }

    /**
     * If a file is being written, we cannot read it yet: this method waits
     * until a file is completely written by checking its checksum at 2
     * different moments: if they are different, it means that the file is still
     * being written.<br/>
     * If the timeout of {@code timeout} ms is reached, an {@link IOException}
     * is thrown.
     *
     * @param file       the file to check
     * @param retryDelay the delay to wait for before next retry
     * @param timeout    the timeout to wait before giving up with an
     *                   {@link IOException}
     * @return true if the system waited for the file to be completed, false
     * otherwise
     * @throws IOException thrown if the timeout is reached
     */
    public static boolean waitFileCompletionByChecksum(final File file, final long retryDelay, final long timeout)
            throws IOException {
        boolean waited = false;

        final long startMillis = System.currentTimeMillis();
        String checksum1 = null;
        while (System.currentTimeMillis() - startMillis < timeout) {
            final String checksum2 = extractChecksum(file, 1024);
            if (checksum1 != null && checksum1.equals(checksum2)) {
                return true;
            }
            checksum1 = checksum2;
            LOGGER.debug("File is increasing its size because still being copied: waiting {} seconds before next retry..", retryDelay);
            ThreadUtils.waitFor(retryDelay, false);
        }

        throw new IOException("The file '" + file.getAbsolutePath() + "' has not been completely written in "
                + (timeout / 60) + " seconds.");
    }

    /**
     * Calculates the checksums of the file {@code file} by bits of
     * {@code slice} bytes, and returns them in a line-separated string, where
     * the nth line is the checksum of the file from byte
     * <code>(n-1)*slice</code> to byte <code>(n-1)*slice+(slice-1)</code>.
     *
     * @param file  the file
     * @param slice the amount of bytes on which calculate the checkusm
     * @return true if the checksum of the first and the second file are the
     * same
     * @throws IOException if something bad happen during the evaluation
     */
    private static String extractChecksum(final File file, final int slice) throws IOException {
        final Checksum adler = new Adler32();
        final FileInputStream fis = new FileInputStream(file);
        final CheckedInputStream cis = new CheckedInputStream(fis, adler);

        final StringBuilder checksums = new StringBuilder();
        final byte[] buffer = new byte[slice];
        try {
            while (cis.read(buffer) >= 0) {
                final long checksum = cis.getChecksum().getValue();
                checksums.append(checksum + "\n");
            }
        } finally {
            IOUtils.closeQuietly(cis);
            IOUtils.closeQuietly(fis);
        }

        return checksums.toString();
    }

    /**
     * Calculates the checksums of the file {@code file} by bits of
     * {@code slice} bytes, and put them in the line-separated text file located
     * at {@code checksumFile}, where the nth line is the checksum of the file
     * from byte <code>(n-1)*slice</code> to byte
     * <code>(n-1)*slice+(slice-1)</code>.
     *
     * @param file         the file
     * @param slice        the amount of bytes on which calculate the checkusm
     * @param checksumFile the checksum
     * @throws IOException if something bad happen during the evaluation
     */
    public static void extractChecksum(final File file, final int slice, final File checksumFile) throws IOException {
        FileUtils.writeStringToFile(checksumFile, extractChecksum(file, slice), (Charset) null);
    }

    /**
     * Compares two files.
     *
     * @param firstFile  first file
     * @param secondFile second file
     * @return true if the checksum of the first and the second file are the
     * same
     * @throws IOException if something bad happen during the evaluation
     */
    public static boolean compare(final File firstFile, final File secondFile) throws IOException {
        if (firstFile.length() != secondFile.length()) {
            return false;
        }

        final Checksum adler = new Adler32();

        final FileInputStream firstFis = new FileInputStream(firstFile);
        final CheckedInputStream firstCis = new CheckedInputStream(firstFis, adler);

        final FileInputStream secondFis = new FileInputStream(secondFile);
        final CheckedInputStream secondCis = new CheckedInputStream(secondFis, adler);

        final byte[] firstBuffer = new byte[1024];
        final byte[] secondBuffer = new byte[1024];

        try {
            while (firstCis.read(firstBuffer) >= 0 && secondCis.read(secondBuffer) >= 0) {
                final long checksum = firstCis.getChecksum().getValue();
                final long checksum2 = secondCis.getChecksum().getValue();

                if (checksum != checksum2) {
                    return false;
                }
            }
        } finally {
            IOUtils.closeQuietly(firstCis);
            IOUtils.closeQuietly(firstFis);
            IOUtils.closeQuietly(secondCis);
            IOUtils.closeQuietly(secondFis);
        }

        return true;
    }

    /**
     * Check if the file {@code file} is being used by another process.
     *
     * @param file the file to check
     * @return true if the file is being used by another process, false
     * otherwise
     */
    public static boolean isFileLocked(final File file) {
        boolean locked = false;

        InputStream is = null;
        try {
            is = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            locked = e.getMessage()
                    .endsWith("(The process cannot access the file because it is being used by another process)");
        } finally {
            IOUtils.closeQuietly(is);
        }

        return locked;
    }

    /**
     * Write the given inputStream to a String preserving the encoding.
     *
     * @param inputStream the input stream to write
     * @return a correct encoded string.
     */
    public static String inputStreamToString(final InputStream inputStream) {
        String value = null;

        final StringWriter sw = new StringWriter();
        if (inputStream != null) {
            try {
                IOUtils.copy(inputStream, sw, (Charset) null);
                value = sw.getBuffer().toString();
            } catch (IOException e) {
                LOGGER.error(e.getMessage());
            } finally {
                try {
                    sw.close();
                } catch (IOException e) {
                    LOGGER.error(e.getMessage());
                }
            }
        }

        return value;
    }

    /**
     * Write the given file to a String preserving the encoding.
     *
     * @param file the file to read from
     * @return a correctly encoded string.
     */
    public static String fileToString(final File file) throws IOException {
        return new String(Files.readAllBytes(file.toPath()));
    }

    public static void closeQuietly(final Collection<InputStream> inputs) {
        for (InputStream input : inputs) {
            IOUtils.closeQuietly(input);
        }
    }

    public static File newTempFileSystemObject(final String containingFolder) {
        final String fsObjectName = UUID.randomUUID().toString();
        return new File(containingFolder, fsObjectName);
    }
    
    /**
     * Retrieves the specified attribute associated with the provided file.
     * @param file the file whose attribute shall be retrieved.
     * @param attribute the attribute to retrieve.
     * @return the attribute value.
     */
    public static Object readAttribute(File file, String attribute) {
    	try {
    		if (file.exists()) {
    			return Files.getAttribute(file.toPath(), attribute);
    		}
			return null;
		} catch (IOException e) {
			LOGGER.error("An error occurred while attempting to read attribute [{}] of file [{}]", attribute, file, e);
			return null;
		}
    }
    
}
