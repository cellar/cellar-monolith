package eu.europa.ec.opoce.cellar.cmr;

import eu.europa.ec.opoce.cellar.cl.domain.response.IOperation;

public interface MetaDataIngestionService<T> {

    /**
     * Ingest all the data into the cmr database, and log the operations performed into {@link operation}
     *
     * @param data data to ingest
     * @param operation the operation object in which to store the ingest log information
     */
    void ingest(final T data, final IOperation<?> operation);

}
