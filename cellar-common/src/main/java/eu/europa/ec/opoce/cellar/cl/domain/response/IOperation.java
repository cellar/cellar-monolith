package eu.europa.ec.opoce.cellar.cl.domain.response;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

public interface IOperation<TYPE> extends Serializable {

    public boolean isCreate();

    public boolean isUpdate();

    public boolean isRead();

    public boolean isDelete();

    public void addAllOperations(final Collection<TYPE> operations);

    public void addOperation(final TYPE operation);

    public TYPE getOperation(String cellarId);

    public Iterable<TYPE> listOperations();

    public Map<String, TYPE> getOperations();

    public void setOperations(final Map<String, TYPE> operations);

    public ResponseOperationType getOperationType();

    public void setOperationType(final ResponseOperationType type);

    public String getId();

    public void setId(String id);

}
