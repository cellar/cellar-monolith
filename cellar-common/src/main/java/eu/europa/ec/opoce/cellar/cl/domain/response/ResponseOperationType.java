package eu.europa.ec.opoce.cellar.cl.domain.response;

public enum ResponseOperationType {
    CREATE, //
    APPEND, //
    UPDATE, //
    KEEP, //
    READ, //
    DELETE, //
    ERROR, //
    UNKNOWN;

}
