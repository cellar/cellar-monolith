/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.domain.content.mets.impl
 *             FILE : GetAllPidSynonymsStrategy.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 30, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.domain.content.mets.impl;

import java.util.LinkedList;
import java.util.List;

import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectStrategy;

/**
 * <class_description> Strategy to extract the identifiers (with synonyms) from the METS.
 * <br/><br/>
 * ON : Apr 30, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class GetAllPidSynonymsStrategy implements DigitalObjectStrategy {

    private final List<List<String>> pids;

    /**
     * Constructor.
     */
    public GetAllPidSynonymsStrategy() {
        this.pids = new LinkedList<List<String>>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void applyStrategy(final DigitalObject digitalObject) {
        List<String> pids = new LinkedList<String>();
        for (ContentIdentifier ci : digitalObject.getContentids()) {
            pids.add(ci.getIdentifier());
        }

        this.pids.add(pids);
    }

    /**
     * Return the synonyms.
     * @return the synonyms
     */
    public List<List<String>> getPids() {
        return this.pids;
    }

}
