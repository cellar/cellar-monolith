/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.common
 *        FILE : IResolver.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 22-05-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common;

/**
 * <class_description> Generic resolver interface.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 22-05-2012
 *
 * @author ARHS Developments
 * @version $Revision$
 * @param <SOURCE> the generic type
 * @param <TARGET> the generic type
 */
public interface IResolver<SOURCE, TARGET> {

    /**
     * Resolve.
     *
     * @param source the source
     * @return the target
     */
    TARGET resolve(final SOURCE source);

}
