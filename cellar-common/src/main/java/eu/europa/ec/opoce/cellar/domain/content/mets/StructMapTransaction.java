/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.domain.content.mets
 *             FILE : StructMapTransaction.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 23-Jan-2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.domain.content.mets;

import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.cl.domain.response.IOperation;
import eu.europa.ec.opoce.cellar.cl.domain.structmap_status_history.StructMapStatusHistory;

import java.io.File;

/**
 * Classes that provides methods to process a structMap(create/update/read) according to a given mets type,
 * should implements this interface.
 */
public interface StructMapTransaction<R extends IOperation<?>> {

    /**
      * StructMap processing.
      * 
      * @param metsPackage to find metadata associated for every digital object under the structMap.
      * @param structMap to find all digital objects in depth first traversal
      * @param metsDirectory a reference to the directory where the SIP has been exploded.
      * @param sipResource the sipResource to audit the method.
      * @param structMapStatusHistory current STRUCTMAP_STATUS_HISTORY entry (to audit method + update)
      * @return R
      */
    R executeTreatment(final MetsPackage metsPackage, final StructMap structMap, final File metsDirectory,
                       final SIPResource sipResource, final StructMapStatusHistory structMapStatusHistory);

}
