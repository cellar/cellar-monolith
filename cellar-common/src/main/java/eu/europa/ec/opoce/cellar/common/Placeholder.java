/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common
 *             FILE : Placeholder.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Mar 25, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common;

/**
 * <class_description> Class that represents a placeholder in a string.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Mar 25, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class Placeholder {

    /**
     * The start index of the placeholder.
     */
    private final int start;

    /**
     * The end index of the placeholder.
     */
    private final int end;

    /**
     * The value of the placeholder.
     */
    private final String value;

    /**
     * Constructor.
     * @param start the start index of the placeholder
     * @param end the end index of the placeholder
     * @param value the value of the placeholder
     */
    public Placeholder(final int start, final int end, final String value) {
        this.start = start;
        this.end = end;
        this.value = value;
    }

    /**
     * Gets the start index of the placeholder.
     * @return the start index of the placeholder
     */
    public int getStart() {
        return this.start;
    }

    /**
     * Gets the end index of the placeholder.
     * @return the end index of the placeholder
     */
    public int getEnd() {
        return this.end;
    }

    /**
     * Gets the value of the placeholder.
     * @return the value of the placeholder
     */
    public String getValue() {
        return this.value;
    }
}
