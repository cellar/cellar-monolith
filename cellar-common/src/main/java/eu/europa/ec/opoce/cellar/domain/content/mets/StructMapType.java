package eu.europa.ec.opoce.cellar.domain.content.mets;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

public enum StructMapType {
    Tree("tree"), Node("node");

    private String xmlValue;

    private StructMapType(String xmlValue) {
        this.xmlValue = xmlValue;
    }

    public static StructMapType getByXmlValue(String xmlValue) {
        for (StructMapType type : StructMapType.values()) {
            if (type.xmlValue.equals(xmlValue)) {
                return type;
            }
        }

        throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.UNKNOWN_STRUCTMAP_TYPE)
                .withMessage("Cannot find StructMapType with value '{}'.").withMessageArgs(xmlValue).build();
    }

}
