/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.domain.content.mets
 *             FILE : DigitalObjectTypeVisitor.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 10-11-2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.domain.content.mets;

/**
 * <class_description> Visitor interface for a {@link DigitalObjectType}.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 10-11-2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface DigitalObjectTypeVisitor<IN, OUT> {

    OUT visitWork(final IN in);

    OUT visitExpression(final IN in);

    OUT visitManifestation(final IN in);

    OUT visitItem(final IN in);

    OUT visitDossier(final IN in);

    OUT visitEvent(final IN in);

    OUT visitAgent(final IN in);

    OUT visitTopLevelEvent(final IN in);

    OUT visitEuronal(final IN in);
}
