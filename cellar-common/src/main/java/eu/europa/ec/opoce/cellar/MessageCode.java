/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar
 *        FILE : MessageCode.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 07-10-2015
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar;

import eu.europa.ec.opoce.cellar.cl.domain.audit.Auditable.LogLevel;

/**
 * <class_description> Generic message code.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 07-10-2015
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public class MessageCode {

    private String code;

    private TYPE type;

    public MessageCode(final String code) {
        this(code, null);
    }

    public MessageCode(final String code, final TYPE type) {
        this.code = code;
        this.type = (type == null ? TYPE.TECHNICAL_ERROR : type);
    }

    public String getCode() {
        return code;
    }

    public TYPE getType() {
        return type;
    }

    @Override
    public String toString() {
        return this.getCode();
    }

    public enum TYPE {
        TECHNICAL_ERROR { // default

            @Override
            public LogLevel getLogLevel() {
                return LogLevel.ERROR;
            }
        },
        FUNCTIONAL_ERROR {

            @Override
            public LogLevel getLogLevel() {
                return LogLevel.WARN;
            }
        },
        SUCCESS {

            @Override
            public LogLevel getLogLevel() {
                return LogLevel.INFO;
            }
        };

        public abstract LogLevel getLogLevel();
    }
}
