/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.monitoring
 *             FILE : ApiAuditService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 04 04, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-04-04 11:05:03 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.monitoring;

import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author ARHS Developments
 * @since 7.7
 */
public interface ApiAuditService {

    /**
     * Audit the call of a method annotated with {@link RequestMapping}
     *
     * @param apiCall the description of the HTTP endpoint
     * @see RequestMappingAdvice
     */
    void audit(ApiCall apiCall);
}
