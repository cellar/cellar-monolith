package eu.europa.ec.opoce.cellar.cl.domain.ontology;

import org.apache.jena.rdf.model.Model;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class OntologyModel. used in the loading process
 */
public class OntologyModel implements Serializable {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = -2801584665305637321L;

	/** The base uri. */
	private String baseURI;

	/** The import ur is. */
	private List<String> importURIs = new ArrayList<String>();

	/** The model file. */
	private File modelFile;

	private Model model;

	/** The file metadata. */
	private FileMetadata fileMetadata;

	/** The imported by ur is. */
	private List<String> importedByURIs = new ArrayList<String>();

	/**
	 * Adds the imported by ur is.
	 *
	 * @param uri
	 *            the uri
	 */
	public void addImportedByURIs(final String uri) {
		this.importedByURIs.add(uri);
	}

	/**
	 * Adds the import ur is.
	 *
	 * @param uri
	 *            the uri
	 */
	public void addImportURIs(final String uri) {
		this.importURIs.add(uri);
	}

	/**
	 * Gets the base uri.
	 *
	 * @return the base uri
	 */
	public String getBaseURI() {
		return this.baseURI;
	}

	/**
	 * Gets the file metadata.
	 *
	 * @return the file metadata
	 */
	public FileMetadata getFileMetadata() {
		return this.fileMetadata;
	}

	/**
	 * Gets the imported by ur is.
	 *
	 * @return the imported by ur is
	 */
	public List<String> getImportedByURIs() {
		return this.importedByURIs;
	}

	/**
	 * Gets the import ur is.
	 *
	 * @return the import ur is
	 */
	public List<String> getImportURIs() {
		return this.importURIs;
	}

	/**
	 * Gets the model file.
	 *
	 * @return the model file
	 */
	public File getModelFile() {
		return this.modelFile;
	}

	/**
	 * Sets the base uri.
	 *
	 * @param baseURI
	 *            the new base uri
	 */
	public void setBaseURI(final String baseURI) {
		this.baseURI = baseURI;
	}

	/**
	 * Sets the file metadata.
	 *
	 * @param fileMetadata
	 *            the new file metadata
	 */
	public void setFileMetadata(final FileMetadata fileMetadata) {
		this.fileMetadata = fileMetadata;
	}

	/**
	 * Sets the imported by ur is.
	 *
	 * @param importedByURIs
	 *            the new imported by ur is
	 */
	public void setImportedByURIs(final List<String> importedByURIs) {
		this.importedByURIs = importedByURIs;
	}

	/**
	 * Sets the import ur is.
	 *
	 * @param importURIs
	 *            the new import ur is
	 */
	public void setImportURIs(final List<String> importURIs) {
		this.importURIs = importURIs;
	}

	/**
	 * Sets the model file.
	 *
	 * @param modelFile
	 *            the new model file
	 */
	public void setModelFile(final File modelFile) {
		this.modelFile = modelFile;
	}

	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}
}
