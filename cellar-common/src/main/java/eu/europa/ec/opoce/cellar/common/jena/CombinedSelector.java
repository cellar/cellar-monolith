/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.common.jena
 *        FILE : CombinedSelector.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 10-04-2014
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.jena;

import org.apache.jena.rdf.model.Selector;
import org.apache.jena.rdf.model.SimpleSelector;
import org.apache.jena.rdf.model.Statement;

import java.util.ArrayList;
import java.util.List;

/**
 * <class_description> Jena Selector that combines different selectors to accept a given statement.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 10-04-2014
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public final class CombinedSelector extends SimpleSelector {

    private List<Selector> selectors;

    public static CombinedSelector get() {
        return new CombinedSelector();
    }

    private CombinedSelector() {
        super();
        this.selectors = new ArrayList<Selector>();
    }

    public CombinedSelector chain(final Selector selector) {
        this.selectors.add(selector);
        return this;
    }

    public CombinedSelector renew() {
        this.selectors.clear();
        return this;
    }

    /**
     * @see org.apache.jena.rdf.model.SimpleSelector#selects(org.apache.jena.rdf.model.Statement)
     */
    @Override
    public boolean selects(final Statement s) {
        for (Selector selector : this.selectors) {
            if (!selector.test(s)) {
                return false;
            }
        }
        return true;
    }

}
