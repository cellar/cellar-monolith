/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : CellarIdentifier.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 8 mars 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.history.dao;

import eu.europa.ec.opoce.cellar.cl.domain.history.IdentifiersHistory;

import java.util.Collection;
import java.util.List;

/**
 * The Interface IdentifiersHistoryDao.
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 8 mars 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IdentifiersHistoryDao {

    /**
     * Gets the latest.
     *
     * @param productionIds the production ids
     * @return the latest
     */
    IdentifiersHistory getLatest(final Collection<String> productionIds);

    /**
     * Gets the latest.
     *
     * @param productionId the production id
     * @return the latest
     */
    IdentifiersHistory getLatest(final String productionId);

    /**
     * Gets the deleted.
     *
     * @param productionIds the production ids
     * @return the deleted
     */
    List<IdentifiersHistory> getDeleted(final Collection<String> productionIds);

    /**
     * Gets the deleted.
     *
     * @param productionId the production id
     * @return the deleted
     */
    List<IdentifiersHistory> getDeleted(final String productionId);

    /**
     * Delete.
     *
     * @param id the id
     */
    void delete(final Long id);

    /**
     * Update.
     *
     * @param identifiersHistory the identifiers history
     * @return the identifiers history
     */
    IdentifiersHistory update(final IdentifiersHistory identifiersHistory);

    /**
     * Gets the identifier history tree.
     *
     * @param productionId the production id
     * @return the identifier history tree
     */
    List<IdentifiersHistory> getIdentifierHistoryTree(final String productionId, final String cellarId);

    /**
     * Gets all records with similar starting cellar id.
     *
     * @param cellarId the cellar id
     * @return the like cellar id
     */
    List<IdentifiersHistory> getAllRecordsStartingWithCellarId(final String cellarId);

}
