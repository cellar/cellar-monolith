package eu.europa.ec.opoce.cellar.domain.content.mets;


import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.common.CellarCatalogResolver;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;

import org.apache.xerces.util.XMLCatalogResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.ext.EntityResolver2;
import org.xml.sax.helpers.XMLReaderFactory;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;

/**
 * this is a metsPackage that if a stream is an entity resolved stream is returned.
 */
public class ResolvedMetsPackage implements MetsPackage, EntityResolver2 {

    private final MetsPackage toResolve;
    private final XMLCatalogResolver catalogResolver = CellarCatalogResolver.getInstance();

    /**
     * create a resolved mets package from a mets package
     * nesting resolved packages will have no effect,
     * as it is always the original non resolved package that will be used to make the resolved package
     *
     * @param toResolve the packages that needs entity-resolving
     */
    public ResolvedMetsPackage(MetsPackage toResolve) {
        this.toResolve = (toResolve instanceof ResolvedMetsPackage) ? ((ResolvedMetsPackage) toResolve).toResolve : toResolve;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public File getZipFile() {
        return toResolve.getZipFile();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public File getZipExtractionFolder() {
        return toResolve.getZipExtractionFolder();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InputStream getMetsStream() {
        try(InputStream is=toResolve.getMetsStream()) {
            return getResolvedStream(is, "mets.xml");
        }
        catch(IOException e){
            throw new UncheckedIOException("Error while processing mets file for :"+ toResolve.getZipFile().getName(), e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InputStream getFileStream(String fileRef) {
        try(InputStream is=toResolve.getFileStream(fileRef)) {
            return getResolvedStream(is, fileRef);
        }catch(IOException e){
            throw new UncheckedIOException("Error while processing file :" + fileRef, e);
        }
    }

    @Override
    public TYPE getSipType() {
        return toResolve.getSipType();
    }

    /**
       * {@inheritDoc}
       */
    @Override
    public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
        InputSource resolvedEntity = catalogResolver.resolveEntity(publicId, systemId);
        if (null != resolvedEntity)
            return resolvedEntity;

        // need to do some fixes here, this is the old method and the systemId receives an already expanded path/url
        if (null == systemId)
            return new InputSource();
        int index = systemId.replace('\\', '/').lastIndexOf('/');
        systemId = index == -1 ? systemId : systemId.substring(index + 1);
        return resolveEntityWithMetsPackage(systemId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InputSource getExternalSubset(String name, String baseURI) throws SAXException, IOException {
        return catalogResolver.getExternalSubset(name, baseURI);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InputSource resolveEntity(String name, String publicId, String baseURI, String systemId) throws SAXException, IOException {
        InputSource resolvedEntity = catalogResolver.resolveEntity(name, publicId, baseURI, systemId);
        if (null != resolvedEntity)
            return resolvedEntity;

        return null == systemId ? null : resolveEntityWithMetsPackage(systemId);
    }

    private InputSource resolveEntityWithMetsPackage(String systemId) {
        try(InputStream fileStream = toResolve.getFileStream(systemId);) {
            return null == fileStream ? null : new InputSource(fileStream);
        }catch(IOException e){
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.FILE_INPUT_COULD_NOT_BE_OPENED)
                    .withMessage("An error occured while trying to open file of mets {}").withMessageArgs(systemId).withCause(e).build();
        }
    }

    public ByteArrayInputStream getResolvedStream(InputStream inputStream, String checkedData) {
        return new ByteArrayInputStream(getResolvedBytes(inputStream, checkedData));
    }

    private static ThreadLocal<TransformerFactory> transformerFactory = new ThreadLocal<TransformerFactory>() {

        @Override
        protected TransformerFactory initialValue() {
            return TransformerFactory.newInstance();
        }
    };

    private byte[] getResolvedBytes(InputStream inputStream, String checkedData) {
        try {
            XMLReader xmlReader = XMLReaderFactory.createXMLReader();
            // no need to set this, if no support -> exception, if support -> set to true by default
            // xmlReader.setFeature("http://xml.org/sax/features/use-entity-resolver2", true);
            xmlReader.setEntityResolver(this);

            SAXSource inputSource = new SAXSource(xmlReader, new InputSource(inputStream));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            Transformer transformer = transformerFactory.get().newTransformer();
            transformer.transform(inputSource, new StreamResult(baos));
            return baos.toByteArray();
        } catch (Exception exception) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.FAILED_PARSING_CONTENT_STREAM)
                    .withMessage("Failed to resolve stream: {}").withMessageArgs(checkedData).withCause(exception).root(true).build();
        }
    }

}
