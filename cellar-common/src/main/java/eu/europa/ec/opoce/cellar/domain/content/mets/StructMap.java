/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : CellarIdentifier.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 24 mai 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.domain.content.mets;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.exception.CellarValidationException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

import java.io.Serializable;

/**
 * data-structure that groups all information about a structmap.
 */
public class StructMap implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 4439084206031929871L;

    /** The content operation type. */
    private StructMapType contentOperationType;

    /** The metadata operation type. */
    private StructMapType metadataOperationType;

    /** The document. */
    private final MetsDocument document;

    /** The digital object. */
    private DigitalObject digitalObject;

    /** The id. */
    private String id;

    /** The operation sub type. */
    private OperationSubType operationSubType;

    /**
     * Instantiates a new struct map.
     *
     * @param document the document
     */
    public StructMap(final MetsDocument document) {
        this.document = document;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public String getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final String id) {
        this.id = id;
    }

    /**
     * Gets the mets document.
     *
     * @return the mets document
     */
    public MetsDocument getMetsDocument() {
        return this.document;
    }

    /**
     * Gets the digital object.
     *
     * @return the digital object
     */
    public DigitalObject getDigitalObject() {
        return this.digitalObject;
    }

    /**
     * Sets the digital object.
     *
     * @param digitalObject the new digital object
     */
    public void setDigitalObject(final DigitalObject digitalObject) {
        this.digitalObject = digitalObject;
    }

    /**
     * Gets the metadata operation type.
     *
     * @return the metadata operation type
     */
    public StructMapType getMetadataOperationType() {
        return this.metadataOperationType;
    }

    /**
     * Sets the metadata operation type.
     *
     * @param metadataOperationType the new metadata operation type
     */
    public void setMetadataOperationType(final StructMapType metadataOperationType) {
        this.metadataOperationType = metadataOperationType;
    }

    /**
     * Gets the content operation type.
     *
     * @return the content operation type
     */
    public StructMapType getContentOperationType() {
        return this.contentOperationType;
    }

    /**
     * Sets the content operation type.
     *
     * @param contentOperationType the new content operation type
     */
    public void setContentOperationType(final StructMapType contentOperationType) {
        this.contentOperationType = contentOperationType;
    }

    /**
     * Resolve operation sub type.
     *
     * @return the operation sub type
     */
    public OperationSubType resolveOperationSubType() {
        if (this.operationSubType != null) {
            return this.operationSubType;
        }

        OperationSubType operationSubType = null;

        for (final OperationSubType currOperationSubType : OperationSubType.values()) {
            if (currOperationSubType.accept(this)) {
                operationSubType = currOperationSubType;
                break;
            }
        }
        if (operationSubType == null) {
            throw ExceptionBuilder.get(CellarValidationException.class).withCode(CommonErrors.UNKNOWN_METS_OPERATION)
                    .withMessage("Invalid METS structure: operation's type is [{}], but no sub-types are compatible with this structMap.")
                    .withMessageArgs(this.getMetsDocument().getType()).build();
        }

        this.operationSubType = operationSubType;
        return this.operationSubType;
    }

}
