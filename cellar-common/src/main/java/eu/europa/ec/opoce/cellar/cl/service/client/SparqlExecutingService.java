/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.client
 *             FILE : SparqlExecutingService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Sep 24, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.client;

import java.io.File;

/**
 * <class_description> Service for finding {@link ReindexationWorkIdentifier} instances from a {@link ReindexationJob}'s SPARQL query.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Sep 24, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface SparqlExecutingService {

    /**
     * Executes the SPARQL query and returns the temp file with the result.
     * @param query the SPARQL query to execute
     * @param format the return format
     * @return the temp file with the result
     */
    File executeSparqlQueryFormat(final String query, String format) throws Exception;

    /**
     * Executes the SPARQL query and store the result in the specified file.
     * @param query the SPARQL query to execute
     * @param filePath the destination file path
     * @param format the return format
     */
    void executeSparqlQuery(final String query, final String filePath, final String format) throws Exception;

    /**
     * Parses the SPARQL result.
     * @param reponseFilePath the file with the SPARQL result to parse
     * @param uriFoundDelegator the delegator to apply on the uri
     */
    void parseSparqlReponse(final String reponseFilePath, final UriFoundDelegator uriFoundDelegator) throws Exception;

    void updateTimeout();
}
