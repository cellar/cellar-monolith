/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.util
 *             FILE : StringUtils.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 01-08-2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.util;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.exception.CellarConfigurationException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

/**
 * <class_description> Utilities for manipulating strings.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 01-08-2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public final class StringUtils extends org.apache.commons.lang.StringUtils {

    /**
     * Return the given {@link string} abbreviated on the right to {@link maxWidth} max characters.
     *
     * @param string the string to abbreviate
     * @param maxWidth the max numbers of characters
     * @return the abbreviated string
     */
    public static String abbreviateRight(final String string, final int maxWidth) {
        String retString = string;
        retString = abbreviate(retString, maxWidth);
        return retString;
    }

    /**
     * Return the given {@link string} abbreviated on the right to {@link maxWidth} max characters.
     *
     * @param string the string to abbreviate
     * @param maxWidth the max numbers of characters
     * @return the abbreviated string
     */
    public static String abbreviateLeft(final String string, final int maxWidth) {
        String retString = string;
        retString = reverse(retString);
        retString = abbreviateRight(retString, maxWidth);
        retString = reverse(retString);
        return retString;
    }

    /**
     * Throws a {@link CellarConfigurationException} if the value {@link value} cannot be set on property {@link key}.
     * 
     * @throws CellarConfigurationException
     */
    public static void isCompatibleValue(final Class<?> expectedType, final String value) throws CellarConfigurationException {
        if (expectedType == null || value == null) {
            return;
        }

        final ExceptionBuilder<?> configExcBuilder = ExceptionBuilder.get(CellarConfigurationException.class)
                .withCode(CommonErrors.FAILED_PROCESSING_CONFIGURATION).withMessageArgs(expectedType, value);
        if ((expectedType.equals(Boolean.class) && !isBooleanValue(value)) || //
                (expectedType.equals(Class.class) && !isClassValue(value)) || //
                (expectedType.equals(Byte.class) && !isByteValue(value)) || //
                (expectedType.equals(Short.class) && !isShortValue(value)) || //
                (expectedType.equals(Integer.class) && !isIntValue(value)) || //
                (expectedType.equals(Long.class) && !isLongValue(value)) || //
                (expectedType.equals(Float.class) && !isFloatValue(value)) || //
                (expectedType.equals(Double.class) && !isDoubleValue(value))) {
            throw configExcBuilder.withMessage("Cannot set '" + value + "' as '" + expectedType + "'.").build();
        }
    }

    public static boolean isBooleanValue(final String value) {
        return "true".equalsIgnoreCase(value) || "false".equalsIgnoreCase(value);
    }

    public static boolean isClassValue(final String value) {
        try {
            Class.forName(value);
            return true;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    public static boolean isByteValue(final String value) {
        try {
            Byte.parseByte(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isShortValue(final String value) {
        try {
            Short.parseShort(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isIntValue(final String value) {
        try {
            Integer.parseInt(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isLongValue(final String value) {
        try {
            Long.parseLong(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isFloatValue(final String value) {
        try {
            Float.valueOf(value).floatValue();
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isDoubleValue(final String value) {
        try {
            Double.valueOf(value).doubleValue();
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    /**
     * This method returns a String where the backslashes are replaced by slashes.
     * If the input parameter doesn't contain backslashes, it is returned as is.
     *
     * @param value the string containing backslaches or not
     * @return      the input value with backslashes replaced by slashes
     */
    public static String replaceBackslashesWithSlashes(String value) {
        return value.replaceAll("\\\\", "/");
    }
}
