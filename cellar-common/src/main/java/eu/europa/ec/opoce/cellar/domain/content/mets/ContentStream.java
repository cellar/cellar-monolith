/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.domain.content.mets
 *             FILE : ContentStream.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 1, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$$
 *          VERSION : $LastChangedRevision$$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.domain.content.mets;

import java.io.Serializable;

import com.google.common.base.MoreObjects;
import org.apache.commons.io.FilenameUtils;

import eu.europa.ec.opoce.cellar.common.util.StringUtils;

/**
 * data-structure to group all information about a contentstream.
 *
 * TODO: ?
 * Content stream maps to an ITEM
 *
 * @author ARHS Developments
 * @version $Revision$$
 */
/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 1, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class ContentStream extends CellarIdentifiedObject implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -8407410756770261801L;

    /** The mime type. */
    private String mimeType;

    /** The file ref. */
    private String fileRef;

    /** The is embedded. */
    private final boolean isEmbedded = false;

    /** The ccr_datastream. */
    private String ccr_datastream;

    /** The other loc type. */
    private String otherLocType;

    /** The use. */
    private String use;

    /** The location type. */
    private ContentStreamLocType locationType;

    /** The checksum. */
    private String checksum;

    /** The checksum type. */
    private String checksumType;

    /** The label. */
    private String label;

    /** The doc index. */
    private Integer docIndex;

    /** The parent object. */
    private DigitalObject parentObject;


    // Only one version
    private String version;

    /**
     * Instantiates a new content stream.
     */
    public ContentStream() {
    }

    /**
     * Instantiates a new content stream.
     *
     * @param mimetype the mimetype
     * @param fileRef the file ref
     */
    public ContentStream(final String mimetype, final String fileRef) {
        this.mimeType = mimetype;
        this.fileRef = fileRef != null ? StringUtils.replaceBackslashesWithSlashes(fileRef) : "";
        this.label = FilenameUtils.getName(fileRef);
    }

    /**
     * Gets the location type.
     *
     * @return the location type
     */
    public ContentStreamLocType getLocationType() {
        return this.locationType;
    }

    /**
     * Sets the location type.
     *
     * @param locationType the new location type
     */
    public void setLocationType(final ContentStreamLocType locationType) {
        this.locationType = locationType;
    }

    /**
     * Sets the mime type.
     *
     * @param mimeType the new mime type
     */
    public void setMimeType(final String mimeType) {
        this.mimeType = mimeType;
    }

    /**
     * Gets the other loc type.
     *
     * @return the other loc type
     */
    public String getOtherLocType() {
        return this.otherLocType;
    }

    /**
     * Sets the other loc type.
     *
     * @param otherLocType the new other loc type
     */
    public void setOtherLocType(final String otherLocType) {
        this.otherLocType = otherLocType;
    }

    /**
     * Gets the use.
     *
     * @return the use
     */
    public String getUse() {
        return this.use;
    }

    /**
     * Sets the use.
     *
     * @param use the new use
     */
    public void setUse(final String use) {
        this.use = use;
    }

    /**
     * Gets the mime type.
     *
     * @return the mime type
     */
    public String getMimeType() {
        return this.mimeType;
    }

    /**
     * Gets the file ref.
     *
     * @return the file ref
     */
    public String getFileRef() {
        return this.fileRef;
    }

    /**
     * Checks if is embedded.
     *
     * @return true, if is embedded
     */
    public boolean isEmbedded() {
        return this.isEmbedded;
    }// @xlink:href

    /**
     * Gets the CC r_datastream.
     *
     * @return the CC r_datastream
     */
    public String getCCR_datastream() {
        return this.ccr_datastream;
    }

    /**
     * Sets the CC r_datastream.
     *
     * @param ccr_datastream the new CC r_datastream
     */
    public void setCCR_datastream(final String ccr_datastream) {
        this.ccr_datastream = ccr_datastream;
    }

    /**
     * Gets the checksum.
     *
     * @return the checksum
     */
    public String getChecksum() {
        return this.checksum;
    }

    /**
     * Sets the checksum.
     *
     * @param checksum the new checksum
     */
    public void setChecksum(final String checksum) {
        this.checksum = checksum;
    }

    /**
     * Gets the checksum type.
     *
     * @return the checksum type
     */
    public String getChecksumType() {
        return this.checksumType;
    }

    /**
     * Sets the checksum type.
     *
     * @param checksumType the new checksum type
     */
    public void setChecksumType(final String checksumType) {
        this.checksumType = checksumType;
    }

    /**
     * Gets the label.
     *
     * @return the label
     */
    public String getLabel() {
        if (StringUtils.isBlank(this.label)) {
            this.label = FilenameUtils.getName(this.fileRef);
        }

        return this.label;
    }

    /**
     * Sets the label.
     *
     * @param label the new label
     */
    public void setLabel(final String label) {
        this.label = label;
    }

    /**
     * Gets the doc index.
     *
     * @return the doc index
     */
    public Integer getDocIndex() {
        return this.docIndex;
    }

    /**
     * Sets the doc index.
     *
     * @param docIndex the new doc index
     */
    public void setDocIndex(final Integer docIndex) {
        this.docIndex = docIndex;
    }

    /**
     * Gets the parent object.
     *
     * @return the parent object
     */
    public DigitalObject getParentObject() {
        return parentObject;
    }

    /**
     * Sets the parent object.
     *
     * @param parentObject the new parent object
     */
    public void setParentObject(final DigitalObject parentObject) {
        this.parentObject = parentObject;
    }


    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("mimeType", mimeType)
                .add("fileRef", fileRef)
                .add("isEmbedded", isEmbedded)
                .add("ccr_datastream", ccr_datastream)
                .add("otherLocType", otherLocType)
                .add("use", use)
                .add("locationType", locationType)
                .add("checksum", checksum)
                .add("checksumType", checksumType)
                .add("label", label)
                .add("docIndex", docIndex)
                .add("version", version)
                .toString();
    }
}
