package eu.europa.ec.opoce.cellar.cl.domain.validator;

import com.google.common.collect.Lists;
import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration;
import eu.europa.ec.opoce.cellar.cl.exception.ValidationException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import javax.annotation.PostConstruct;
import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Implementation of the {@link Validator}.
 * Validate the CONTENTIDS tag of mets.xml. Throws a CellarException if the CONTENTIDS tag is not valid.
 * (ex: oj:JOL_2006_088_R_0063_01.mets.xml)
 *
 * @throws Exception if the CONTENTIDS tag is not valid.
 */
@Component("contentIdsValidator")
public class ContentIdsValidator implements SystemValidator {

    private static final Logger LOG = LogManager.getLogger(ContentIdsValidator.class);

    public static final String PSI_XML = "psi/psi.xml";
    public static final String PSI_XSD = "/xsd/psi/psi.xsd";

    private List<Pattern> patterns;

    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;


    @PostConstruct
    private void init() {
        this.patterns = Lists.newArrayList();
        InputStream xmlFileStream=null;
        InputStream xsdFileStream=null;
        try {
            final File cellarFolderRoot = new File(cellarConfiguration.getCellarFolderRoot());
            final File xmlFile = new File(cellarFolderRoot, PSI_XML);
            xmlFileStream = FileUtils.openInputStream(xmlFile);
            xsdFileStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(PSI_XSD);

            //validation of psi.xml with psi.xsd
            Source xmlFileSource = new StreamSource(xmlFileStream);
            Source xsdFileSource = new StreamSource(xsdFileStream);
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            try {
                Schema schema = schemaFactory.newSchema(xsdFileSource);
                javax.xml.validation.Validator validator = schema.newValidator();
                validator.validate(xmlFileSource);
            } catch (SAXException e) {
                throw ExceptionBuilder.get(ValidationException.class).withCode(CommonErrors.FAILED_PROCESSING_CONFIGURATION)
                        .withMessage("The file 'psi.xml' is not valid.").withCause(e).build();
            } catch (IOException e) {
                throw ExceptionBuilder.get(ValidationException.class).withCode(CommonErrors.FAILED_PROCESSING_CONFIGURATION)
                        .withMessage("The file 'psi.xml' is not loaded correctly.").withCause(e).build();
            }
            //load PSI patterns of psi.xml
                SAXReader reader = new SAXReader();
                xmlFileStream = FileUtils.openInputStream(xmlFile);
                Document doc = reader.read(xmlFileStream);
                final Element rootElement = doc.getRootElement();
                @SuppressWarnings("unchecked") final List<Element> regexs = rootElement.elements("regex");
                for (Element regex : regexs) {
                    final Element pattern = regex.element("pattern");
                    final String patternValue = pattern.getText();
                    Pattern regexPattern = Pattern.compile(patternValue);
                    patterns.add(regexPattern);
                }
            LOG.info(IConfiguration.CONFIG, "Content IDs loaded from {}, {} patterns registered.", xmlFile, patterns.size());
        } catch (DocumentException | IOException e) {
            throw ExceptionBuilder.get(ValidationException.class).withCode(CommonErrors.FAILED_PROCESSING_CONFIGURATION)
                    .withMessage("The file 'psi.xml' is not loaded correctly.").withCause(e).build();
        }
        finally {
            IOUtils.closeQuietly(xsdFileStream);
            IOUtils.closeQuietly(xmlFileStream);
        }
    }

    @Override
    public ValidationResult validate(ValidationDetails validationDetails) {
        ValidationResult result = new ValidationResult();

        if (validationDetails.getToValidate() == null) {
            throw ExceptionBuilder.get(ValidationException.class).withCode(CoreErrors.E_070).withMessage("object cannot be null")
                    .withCause(new NullPointerException()).build();
        }
        if (!(validationDetails.getToValidate() instanceof String)) {
            throw ExceptionBuilder.get(ValidationException.class).withCode(CoreErrors.E_073)
                    .withMessage(validationDetails.getToValidate().getClass().toString()).build();
        }

        result.setValid(validateContentIds((String) validationDetails.getToValidate()));

        return result;
    }

    private boolean validateContentIds(final String contentId) {
        boolean match = false;
        for (Pattern pattern : patterns) {
            if (Pattern.matches(pattern.pattern(), contentId)) {
                match = true;
                break;
            }
        }
        return match;
    }

    public List<Pattern> getPatterns() {
        return patterns;
    }
}
