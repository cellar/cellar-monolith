/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.support
 *             FILE : ThreadUtils.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Aug 30, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.util;

import java.util.Arrays;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Aug 30, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class EnumUtils {

    private static final Logger LOG = LoggerFactory.getLogger(EnumUtils.class);

    /**
     * Gets the enum with key {@link key} out of the enum class {@link clazz}.<br>
     * If not enum key {@link key}, defaults to {@link _default}.
     * 
     * @param clazz the enum clazz
     * @param key the enum key
     * @return the enum
     */
    @SuppressWarnings({
            "unchecked", "rawtypes"})
    public static <T extends Enum> T resolve(final Class<T> clazz, final String key, final T _default) {
        final String myKey = (key == null ? "" : key);
        T resolved = _default;
        try {
            resolved = (T) T.valueOf(clazz, myKey);
        } catch (IllegalArgumentException e) {
            LOG.info("Cannot resolve enum value '" + key + "' out of enum class '" + clazz + "'. Defaulting to '" + _default + "'.");
        }
        return resolved;
    }

    /**
     * Returns true if the given <code>enumElement</code> is equal to one element in the <code>enumElementsToMatch</code> array. 
     * @param element The element to check.
     * @param elements The array of potential matchers
     * @return <code>true</code> if on element in the array is equal to the given element, <code>false</code> otherwise.
     */
    @SafeVarargs
    @SuppressWarnings({
            "rawtypes"})
    public static <T extends Enum> boolean equalsAny(final T enumElement, final T... enumElementsToMatch) {
        for (T element : enumElementsToMatch) {
            if (enumElement == element) {
                return true;
            }
        }
        return false;
    }

    /**
     * Converts the enumeration's values to a collection of strings. 
     * @param enumClass The enumeration's class
     * @return The enumeration's values as a collection of strings.
     */
    public static Collection<String> enumValuesToStringCollection(Class<? extends Enum<?>> enumClass) {
        return Arrays.asList(Arrays.stream(enumClass.getEnumConstants()).map(Enum::name).toArray(String[]::new));
    }

}
