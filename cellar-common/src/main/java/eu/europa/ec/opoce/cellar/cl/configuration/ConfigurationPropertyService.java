package eu.europa.ec.opoce.cellar.cl.configuration;

import eu.europa.ec.opoce.cellar.cl.domain.admin.ConfigurationProperty;

import java.util.Collection;
import java.util.List;

public interface ConfigurationPropertyService {

    enum EXISTINGPROPS_MODE {
        TO_DB, FROM_DB, NO_SYNC
    }

    /**
     * Persist the given transient instance.
     * 
     * @param property
     *            the transient instance to be persisted
     */
    void createProperty(ConfigurationProperty property);

    /**
     * Delete the given persistent instance.
     * 
     * @param property
     *            the persistent instance to delete
     */
    void deleteProperty(ConfigurationProperty property);

    /**
     * Delete the given persistent instances.
     * 
     * @param properties
     *            the persistent instances to delete
     */
    void deleteProperties(Collection<ConfigurationProperty> properties);

    /**
     * Return all persistent instances of {@link ConfigurationProperty}.
     * 
     * @return a {@link List} containing 0 or more persistent instances.
     */
    Collection<ConfigurationProperty> getProperties();

    /**
     * Return the persistent instance with the given internal identifier, or null if not found.
     * 
     * @param id
     *            the internal identifier of the persistent instance
     * 
     * @return the persistent instance, or null if not found
     */
    @Deprecated
    ConfigurationProperty getPropertyById(Long id);

    /**
     * Return the persistent instance identified by the given key, or null if not found.
     * 
     * @param key
     *            the key identifier of the persistent instance
     * 
     * @return the persistent instance, or null if not found
     */
    ConfigurationProperty getPropertyByKey(String key);

    /**
     * Return true if exist
     * 
     * @param configurationProperty
     * @return true if exist
     */
    boolean isPropertyExists(ConfigurationProperty configurationProperty);

    /**
     * Update a ConfigurationProperty.
     * 
     * @param rule
     *            the {@link ConfigurationProperty} to create
     */
    void updateProperty(ConfigurationProperty configurationProperty);

    /**
     * Synchronize the property {@link propertyKey} this way:<br>
     * - if property exists on database and {@link syncMode} is {@link EXISTINGPROPS_MODE.FROM_DB}, it loads the value from database and returns this value;<br>
     * - if property exists on database and {@link ifExistsKeepDatabaseValue} is {@link EXISTINGPROPS_MODE.TO_DB}, it sets {@link propertyValue} into database and returns {@link propertyValue};<br>
     * - if property exists on database and {@link ifExistsKeepDatabaseValue} is {@link EXISTINGPROPS_MODE.NO_SYNC}, it just returns {@link propertyValue} with no synchronization from/to db;<br>
     * - otherwise, it creates the property into database with value {@link propertyValue}, and returns {@link propertyValue}.
     * 
     * @param propertyValue
     *            Property value.
     * @param propertyKey
     *            Property key.
     * @param properties
     *            List of all properties.
     * @return The property value.
     */
    String synchronizeProperty(String propertyValue, String propertyKey, EXISTINGPROPS_MODE syncMode);

}
