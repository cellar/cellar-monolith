/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.common.closure
 *        FILE : RPClosure.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 20-03-2013
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.closure;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

/**
 * <class_description> General purpose closure that accepts a parameter and returns an object.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 20-03-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface RPClosure<R, P> {

    /**
     * Calls the closure.
     *
     * @param param the parameter of the closure
     * @return the result of the closure
     * @throws ClosureException should the call to the closure fail
     */
    default R call(final P param) throws ClosureException {
        try {
            return this.doCall(param);
        } catch (Exception e) {
            throw ExceptionBuilder.get(ClosureException.class).withCode(CommonErrors.CLOSURE_FAILED)
                    .withCause(e).visibleOnStackTrace(false).build();
        }
    }

    /**
     * Performs the actual call to the closure.
     *
     * @param param the parameter of the closure
     * @return the result of the closure
     * @throws Exception should the call to the closure fail
     */
    R doCall(final P param) throws Exception;

}
