/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.audit
 *        FILE : AuditBuilder.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 09-01-2013
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.audit;

import eu.europa.ec.opoce.cellar.CommonSuccessMessages;
import eu.europa.ec.opoce.cellar.MessageCode;
import eu.europa.ec.opoce.cellar.cl.concurrency.Concurrent;
import eu.europa.ec.opoce.cellar.cl.domain.audit.Auditable.LogLevel;
import eu.europa.ec.opoce.cellar.cl.domain.package_history.PackageHistory;
import eu.europa.ec.opoce.cellar.cl.domain.structmap_status_history.StructMapStatusHistory;
import eu.europa.ec.opoce.cellar.cl.service.client.AuditTrailEventService;
import eu.europa.ec.opoce.cellar.common.MessageFormatter;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import eu.europa.ec.opoce.cellar.domain.content.mets.OffIngestionOperationType;
import eu.europa.ec.opoce.cellar.exception.BuildableException;
import eu.europa.ec.opoce.cellar.exception.ExceptionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.support.ResourceBundleMessageSource;

import java.util.Collection;
import java.util.Locale;

import static eu.europa.ec.opoce.cellar.AppConstants.NEWLINE;
import static eu.europa.ec.opoce.cellar.cl.domain.audit.AuditableAdvice.AUDIT_KEY;
import static org.apache.commons.lang.StringUtils.isEmpty;

/**
 * <class_description> Builder class for conveniently logging events on the audit trail event table.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 09-01-2013
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public class AuditBuilder {

    /** The Constant LOG. */
    @SuppressWarnings("unused")
    private static final Logger LOG = LogManager.getLogger(AuditBuilder.class);

    /**
     * The builder's resource bundle.
     */
    private static ResourceBundleMessageSource ems = new ResourceBundleMessageSource() {
        {
            setBasename("messages.exceptions");
        }
    };

    /**
     * The builder's audit event service.
     */
    private final AuditTrailEventService auditService;

    /**
     * The builder's audit event type.
     */
    private AuditTrailEventType type;

    /**
     * The builder's action.
     */
    private AuditTrailEventAction action;

    /**
     * The builder's duration.
     */
    private long duration;

    /** The builder's message code. */
    private MessageCode code;

    /**
     * The builder's exception.
     */
    protected Throwable exception;

    /** The builder's audit process. */
    private final AuditTrailEventProcess process;

    /**
     * The builder's log4j logger.
     */
    private Logger logger;

    /** The builder's message. */
    private String message;

    /** The builder's message arguments. */
    private Object[] messageArgs;

    /**
     * The builder's object.
     */
    private String object;

    /**
     * The log level to print the console with.
     */
    private Auditable.LogLevel logLevel;

    /**
     * If true, logs the entry onto the database.
     */
    private boolean logDatabase;

    private String processId;
    
    /**
     * Structmap the logging event may be referring to.
     * e.g. during ingestion process
     */
    private StructMapStatusHistory structMapStatusHistory;
    
    /**
     * Package the logging event may be referring to.
     * e.g. during ingestion process
     */
    private PackageHistory packageHistory;

    /**
     * Gets a new builder of type {@code process} for conveniently building an event to be audited in the audit trail table.
     * {@code process} is the process of the event, which may be one of {@link eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventProcess}.<br/>
     * Once the new builder is created, the user can populate its properties with the {@code with*} methods, and finally log the event by calling the method {@code eu.europa.ec.opoce.cellar.cl.domain.audit.AuditBuilder.logEvent}.<br/>
     * <br/>Some examples:<br/>
     * <br/><code>AuditBuilder.get(AuditTrailEventProcess.INGESTION).withAction(AuditTrailEventAction.CREATE).withMessage("The indexation has been performed correctly.").logEvent();</code><br/>
     * <br/><code>AuditBuilder.get(AuditTrailEventProcess.EMBARGO).withAction(AuditTrailEventAction.PUSH).withObject(uri).withMessage("Embargo date of {} has been correctly changed to {}.").withMessageArgs(uri, date).withLogger(LOG).logEvent();</code><br/>
     * <br/><code>AuditBuilder.get(AuditTrailEventProcess.NAL).withAction(AuditTrailEventAction.LOAD).withObject(uri).withCode(ErrorCode.E_041).withException(e).withLogger(LOG).logEvent();</code>
     *
     * @param process the process
     * @return the builder newly created
     */
    public static AuditBuilder get(final AuditTrailEventProcess process) {
        return new AuditBuilder(process);
    }

    /**
     * Gets a new builder of type {@code process} based on {@code Concurrent.Locker} instance for conveniently building an event to be audited in the audit trail table.
     *
     * @return the builder newly created
     * @see AuditBuilder#get(AuditTrailEventProcess)
     * @see AuditBuilder#get()
     */
    public static AuditBuilder get(final Concurrent.Locker locker) {
        return new AuditBuilder(locker);
    }

    /**
     * Gets a new builder of type {@code AuditTrailEventProcess.Undefined} for conveniently building an event to be audited in the audit trail table.
     *
     * @return the builder newly created
     * @see AuditBuilder#get(AuditTrailEventProcess)
     * @see AuditBuilder#get(Concurrent.Locker)
     */
    public static AuditBuilder get() {
        return get(AuditTrailEventProcess.Undefined);
    }

    /**
     * Set private to avoid direct instantiation: any instantiation should come from static method {@code eu.europa.ec.opoce.cellar.cl.domain.audit.AuditBuilder.get(AuditTrailEventProcess)}
     *
     * @param process the process
     */
    private AuditBuilder(final AuditTrailEventProcess process) {
        this.auditService = ServiceLocator.getService(AuditTrailEventService.class);
        this.process = process;
    }

    /**
     * Set private to avoid direct instantiation: any instantiation should come from static method {@code eu.europa.ec.opoce.cellar.cl.domain.audit.AuditBuilder.get(Concurrent.Locker)}
     *
     * @param locker the locker
     */
    private AuditBuilder(final Concurrent.Locker locker) {
        this.auditService = ServiceLocator.getService(AuditTrailEventService.class);
        this.process = getProcessByOperationType(locker);
    }

    /**
     * Adds an action to the event.
     * 
     * @param action the action
     * @return the builder itself
     */
    public AuditBuilder withAction(final AuditTrailEventAction action) {
        this.action = action;
        return this;
    }

    /**
     * Adds a message code to the event.<br/>
     * If set, and if {@code message} is not set, the i18n message resolved by this {@code code} will be used as the event's message.
     * 
     * @param code the code
     * @return the builder itself
     */
    public AuditBuilder withCode(final MessageCode code) {
        this.code = code;
        return this;
    }

    /**
     * Adds an exception to the event.<br/>
     * If set, the event's audit type is automatically set to {@code eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventType.FAIL},
     * and the exception' message automatically added to the message.
     * 
     * @param exception the exception
     * @return the builder itself
     */
    public AuditBuilder withException(final Throwable exception) {
        this.exception = exception;
        return this;
    }

    /**
     * Adds a audit type to the event.<br/>
     * If not set, the event's audit level is automatically set to {@code eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventType.START}
     * if there is no exception, or the {@code eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventType.FAIL} otherwise.
     * 
     * @param type the audit type
     * @return the builder itself
     */
    public AuditBuilder withType(final AuditTrailEventType type) {
        this.type = type;
        return this;
    }

    /**
     * Adds a audit type to the event.<br/>
     *
     * @param duration the audit duration
     * @return the builder itself
     */
    public AuditBuilder withDuration(final long duration) {
        this.duration = duration;
        return this;
    }

    public AuditBuilder withLogger(Logger logger) {
        this.logger = logger;
        return this;
    }

    /**
     * Adds a message to the event.<br/>
     * If set, it has the priority over {@code code}: this means that, even if code is set, this {@code message} will be logged instead.
     * 
     * @param message the message
     * @return the builder itself
     */
    public AuditBuilder withMessage(final String message) {
        this.message = message;
        return this;
    }

    /**
     * Adds a list of arguments to the message of the event.<br/>
     * If set, it may work in combination with the {@code message}, or even with the message resolved by the {@code code}.
     * 
     * @param messageArgs the message's arguments
     * @return the builder itself
     */
    public AuditBuilder withMessageArgs(final Object... messageArgs) {
        this.messageArgs = messageArgs;
        return this;
    }

    /**
     * Adds an object of reference to the event.<br/>
     * If set, it also acts as a message's argument.
     * 
     * @param object the object of reference
     * @return the builder itself
     */
    public AuditBuilder withObject(final String object) {
        this.object = object;
        return this;
    }

    /**
     * Adds a list of comma-separated objects of reference to the event.<br/>
     * If set, it also acts as a message's argument.
     * 
     * @param objects the list of objects of reference
     * @return the builder itself
     */
    public AuditBuilder withObject(final Collection<String> objects) {
        this.object = StringUtils.join(objects, ",");
        return this;
    }

    /**
     * Sets the log level to print the console with.
     * 
     * @param logLevel the log level to print the console with
     * @return the builder itself
     */
    public AuditBuilder withLogLevel(final Auditable.LogLevel logLevel) {
        this.logLevel = logLevel;
        return this;
    }

    /**
     * If true, the event is logged onto the database.
     * 
     * @param logDatabase If true, the event is logged onto the database
     * @return the builder itself
     */
    public AuditBuilder withLogDatabase(final boolean logDatabase) {
        this.logDatabase = logDatabase;
        return this;
    }

    public  AuditBuilder withProcessId(String processId) {
        this.processId = processId;
        return this;
    }
    
    /**
     * Set the structmap this logging event may be referring to e.g. during ingestion process
     * @param structMapStatusHistory the structmap in question
     * @return the builder itself
     */
    public AuditBuilder withStructMapStatusHistory(StructMapStatusHistory structMapStatusHistory) {
        this.structMapStatusHistory = structMapStatusHistory;
        return this;
    }
    
    /**
     * Set the package this logging event may be referring to e.g. during ingestion process
     * @param packageHistory the package in question
     * @return the builder itself
     */
    public AuditBuilder withPackageHistory(PackageHistory packageHistory){
        this.packageHistory = packageHistory;
        return this;
    }

    /**
     * Logs the event to the audit trail table. If {@code logger} is set, the event is logged to log4j as well.
     */
    public void logEvent() {
        // fallbacks properties
        this.fallback();

        // logs event on console
        this.logEventOnConsole();

        // logs event on database
        this.logEventOnDatabase();
    }

    /**
     * Gets the type.
     *
     * @return the type
     */
    public AuditTrailEventType getType() {
        return type;
    }

    /**
     * Gets the action.
     *
     * @return the action
     */
    public AuditTrailEventAction getAction() {
        return action;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public MessageCode getCode() {
        return code;
    }

    /**
     * Gets the exception.
     *
     * @return the exception
     */
    public Throwable getException() {
        return exception;
    }

    /**
     * Gets the process.
     *
     * @return the process
     */
    public AuditTrailEventProcess getProcess() {
        return process;
    }

    /**
     * Gets the logger.
     *
     * @return the logger
     */
    public Logger getLogger() {
        return logger;
    }

    /**
     * Gets the message.
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Gets the message args.
     *
     * @return the message args
     */
    public Object[] getMessageArgs() {
        return messageArgs;
    }

    /**
     * Gets the object.
     *
     * @return the object
     */
    public String getObject() {
        return object;
    }
    
    /**
     * Gets the structmap info
     * @return the structmap
     */
    public StructMapStatusHistory getStructMapStatusHistory(){ return structMapStatusHistory; }
    
    /**
     * Gets the package info
     * @return the package
     */
    public PackageHistory getPackageHistory() { return packageHistory; }
    
    /**
     * Gets the log level.
     *
     * @return the log level
     */
    public Auditable.LogLevel getLogLevel() {
        return logLevel;
    }

    /**
     * Checks if is log database.
     *
     * @return true, if is log database
     */
    public boolean isLogDatabase() {
        return logDatabase;
    }

    /* Private methods */

    /**
     * It fallbacks mandatory properties that are {@code null}.
     */
    private void fallback() {
        // if the object of reference is null, sets it to the empty string
        if (this.object == null) {
            this.object = "";
        }

        // if there are no message args, it uses the object as such
        if (ArrayUtils.isEmpty(this.messageArgs) && !isEmpty(this.object)) {
            this.messageArgs = new String[] {
                    this.object};
        }

        // if type is null
        if (this.type == null) {
            // if there is no exception, it uses START, otherwise FAIL
            this.type = (this.exception == null ? AuditTrailEventType.Start : AuditTrailEventType.Fail);
        }

        // if message is null tries to resolve the message out of the resource bundle
        if (this.message == null) {
            resolveMessageWithResourceBundle();
        }
        // otherwise
        else {
            // use the message itself
            this.message = MessageFormatter.arrayFormat(this.message, this.messageArgs);
            // if code is null but exception is a non-null BuildableException, set the code accordingly
            if ((this.code == null) && (this.exception != null) && (this.exception instanceof BuildableException)) {
                this.code = ((BuildableException) this.exception).getCode();
            }
        }

        // if logLevel is null
        if (this.logLevel == null) {
            // if code is not null, set the log level accordingly
            if (this.code != null) {
                this.logLevel = this.code.getType().getLogLevel();
            }
            // set by default to INFO
            else {
                this.logLevel = Auditable.LogLevel.INFO;
            }
        }
    }

    /**
     * Resolve message with resource bundle.
     */
    private void resolveMessageWithResourceBundle() {
        // if code is null, defaults to auditable codes
        if (this.code == null) {
            switch (this.type) {
            case Start:
                initMessage(CommonSuccessMessages.AUDITABLE_START_MESSAGE, new Object[] {
                        this.process, this.action, this.object});
                break;
            case End:
                initMessage(CommonSuccessMessages.AUDITABLE_END_MESSAGE, new Object[] {
                        this.process, this.action, this.object});
                break;
            case Fail:
                initMessage(CommonSuccessMessages.AUDITABLE_FAIL_MESSAGE, new Object[] {
                        this.process, this.action, this.object, ExceptionUtils.extractAuditableMessage(this.exception, 0)});
            }
        } else {
            initMessage(code, this.messageArgs);
        }
    }

    /**
     * Inits the message.
     *
     * @param messageCode the message code
     * @param messageArgs the message args
     */
    private void initMessage(MessageCode messageCode, Object[] messageArgs) {
        if (messageCode != null) {
            try {
                this.message = ems.getMessage(messageCode.getCode(), messageArgs, Locale.getDefault());
            } catch (final NoSuchMessageException e) {
            }
        }
    }

    /**
     * Logs the event on the console.
     */
    private void logEventOnConsole() {
        // if there is a logger, logs the message on the console
        if (this.logger != null) {
            final LogLevel myLogLevel = extractLogLevel(this.exception);
            switch (myLogLevel) {
                case TRACE:
                    this.logger.trace(buildLogMessage());
                    break;
                case DEBUG:
                    this.logger.debug(buildLogMessage());
                    break;
                case INFO:
                    this.logger.info(buildLogMessage());
                    break;
                case WARN:
                    this.logger.warn(buildLogMessage(), this.exception);
                    break;
                case ERROR:
                    this.logger.error(buildLogMessage(), this.exception);
            }
        }
    }

    /**
     * Logs the event on the database.
     */
    private void logEventOnDatabase() {
        if (this.logDatabase) {
            // if there is an exception with a non-null message, attaches the exception to the message
            if ((this.exception != null) && (this.exception.getMessage() != null)) {
                this.message = ((message == null ? "" : message + NEWLINE) + this.exception.getMessage());
            }

            // logs the event
            this.auditService.saveEvent(process, action, type, object, message,
                    processId != null ? processId : ThreadContext.get(AUDIT_KEY),
                    structMapStatusHistory,
                    packageHistory);
        }
    }

    /**
     * Append log token.
     *
     * @param token the token
     * @param messageBuilder the message builder
     */
    private static void appendLogToken(final Enum<?> token, final StringBuilder messageBuilder) {
        if (token != null) {
            appendLogToken(token.toString(), messageBuilder);
        }
    }

    /**
     * Append log token.
     *
     * @param token the token
     * @param messageBuilder the message builder
     */
    private static void appendLogToken(final String token, final StringBuilder messageBuilder) {
        if (!isEmpty(token)) {
            messageBuilder.append("[").append(token).append("]");
        }
    }

    /**
     * Builds the log message.
     *
     * @return the string
     */
    private StringBuilder buildLogMessage() {
        final StringBuilder messageBuilder = new StringBuilder();
        appendLogToken(this.process, messageBuilder);
        appendLogToken(this.action, messageBuilder);
        appendLogToken(this.type, messageBuilder);
        if (this.type == AuditTrailEventType.End && this.duration != 0L) {
            appendLogToken(this.duration + " ms", messageBuilder);
        }
        if (!isEmpty(this.message)) {
            messageBuilder.append(messageBuilder.length() == 0 ? "" : " " + this.message);
        }
        return messageBuilder;
    }

    /**
     * Extract log level.
     *
     * @param exception the exception
     * @return the log level
     */
    private LogLevel extractLogLevel(final Throwable exception) {
        return this.extractLogLevel(exception, this.logLevel);
    }

    /**
     * Extract log level.
     *
     * @param exception the exception
     * @param logLevel the log level
     * @return the log level
     */
    private LogLevel extractLogLevel(final Throwable exception, final LogLevel logLevel) {
        LogLevel retLogLevel = logLevel;
        if (exception == null) {
            return retLogLevel;
        }

        if (exception instanceof BuildableException) {
            final BuildableException buildableException = (BuildableException) exception;
            retLogLevel = buildableException.getCode().getType().getLogLevel();
            if (!buildableException.isRoot()) {
                retLogLevel = extractLogLevel(buildableException.getCause(), retLogLevel);
            }
        } else {
            retLogLevel = LogLevel.ERROR;
        }

        return retLogLevel;
    }

    /**
     * Identifies the AuditTrailEventProcess based on OffIngestionOperationType.
     * @param locker the locker instance
     * @return the process
     */
    private AuditTrailEventProcess getProcessByOperationType(Concurrent.Locker locker) {
        if (locker == OffIngestionOperationType.INDEXING){
            return AuditTrailEventProcess.Indexing;
        }
        if (locker == OffIngestionOperationType.EMBARGO) {
            return AuditTrailEventProcess.Embargo;
        }
        if (locker == OffIngestionOperationType.EXPORT) {
            return AuditTrailEventProcess.Export;
        }
        if (locker == OffIngestionOperationType.ALIGNING) {
            return AuditTrailEventProcess.Aligning;
        }
        if (locker == OffIngestionOperationType.ALIGNING_SCHEDULING) {
            return AuditTrailEventProcess.Aligning_Scheduling;
        }
        if (locker == OffIngestionOperationType.CLEANING) {
            return AuditTrailEventProcess.Cleaning;
        }
        if (locker == OffIngestionOperationType.CLEANING_SCHEDULING) {
            return AuditTrailEventProcess.Cleaning_Scheduling;
        }
        if (locker == OffIngestionOperationType.EMBEDDED_NOTICE_SYNCHRONIZATION) {
            return AuditTrailEventProcess.Embedded_Notice_Synchronization;
        }
        if (locker == OffIngestionOperationType.VIRTUOSO_BACKUP) {
            return AuditTrailEventProcess.Virtuoso_Backup;
        }
        if (locker == OffIngestionOperationType.UNKNOWN) {
            return AuditTrailEventProcess.Undefined;
        }

        return AuditTrailEventProcess.Ingestion;
    }

}
