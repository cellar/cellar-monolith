/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.database.dao.spring
 *             FILE : CellarJdbcTemplate.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 9 May 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.database.dao.spring;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.support.KeyHolder;

/**
 * <class_description> This JDBC template checks if the database is in read-only
 * mode before performing the operation: <br>
 * if so, and if the operation to be executed is a write operation, it does
 * nothing but logging the attempt. <br/>
 * <br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc. <br/>
 * <br/>
 * ON : dd-MM-YYYY
 *
 *
 * @author ARHS Developments
 * @version $Revision$: 3760 $
 */
public class CellarJdbcTemplate extends JdbcTemplate {

	private static final Logger LOG = LoggerFactory.getLogger(CellarJdbcTemplate.class);

	private final boolean readOnly;

	/**
	 * Construct a new JdbcTemplate, given a DataSource to obtain connections
	 * from.
	 * <p>
	 * Note: This will not trigger initialization of the exception translator.
	 * 
	 * @param dataSource
	 *            the JDBC DataSource to obtain connections from
	 * @param readOnly
	 *            assumes that the database is in read-only mode, hence blocking
	 *            any CUD operation
	 */
	public CellarJdbcTemplate(DataSource dataSource, final boolean readOnly) {
		super(dataSource);
		this.readOnly = readOnly;
	}

	/**
	 * @see org.springframework.jdbc.core.JdbcTemplate#update(java.lang.String)
	 */
	@Override
	public int update(final String sql) throws DataAccessException {
		if (this.isDatabaseReadOnly()) {
			return 0;
		} else {
			return super.update(sql);
		}
	}

	/**
	 * @see org.springframework.jdbc.core.JdbcTemplate#update(org.springframework.jdbc.core.PreparedStatementCreator,
	 *      org.springframework.jdbc.core.PreparedStatementSetter)
	 */
	@Override
	protected int update(final PreparedStatementCreator psc, final PreparedStatementSetter pss)
			throws DataAccessException {
		if (this.isDatabaseReadOnly()) {
			return 0;
		} else {
			return super.update(psc, pss);
		}
	}

	/**
	 * @see org.springframework.jdbc.core.JdbcTemplate#update(org.springframework.jdbc.core.PreparedStatementCreator,
	 *      org.springframework.jdbc.support.KeyHolder)
	 */
	@Override
	public int update(final PreparedStatementCreator psc, final KeyHolder generatedKeyHolder)
			throws DataAccessException {
		if (this.isDatabaseReadOnly()) {
			return 0;
		} else {
			return super.update(psc, generatedKeyHolder);
		}
	}

	/**
	 * @see org.springframework.jdbc.core.JdbcTemplate#batchUpdate(java.lang.String[])
	 */
	@Override
	public int[] batchUpdate(final String... sql) throws DataAccessException {
		if (this.isDatabaseReadOnly()) {
			return new int[0];
		} else {
			return super.batchUpdate(sql);
		}
	}

	/**
	 * @see org.springframework.jdbc.core.JdbcTemplate#batchUpdate(java.lang.String,
	 *      org.springframework.jdbc.core.BatchPreparedStatementSetter)
	 */
	@Override
	public int[] batchUpdate(String sql, final BatchPreparedStatementSetter pss) throws DataAccessException {
		if (this.isDatabaseReadOnly()) {
			return new int[0];
		} else {
			return super.batchUpdate(sql, pss);
		}
	}

	private boolean isDatabaseReadOnly() {
		if (this.readOnly) {
			LOG.debug("Cannot execute write operation as the database is in read-only mode.");
			return true;
		}
		return false;
	}

}
