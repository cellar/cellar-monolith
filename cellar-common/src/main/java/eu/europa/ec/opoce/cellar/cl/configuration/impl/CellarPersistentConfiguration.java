/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.configuration.impl
 *        FILE : CellarPersistentConfiguration.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 02-04-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.configuration.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.ConfigurationPropertyService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * <class_description> Cellar configuration bean that synchronizes with properties stored on the persistent layer.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 02-04-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
@Component("cellarConfiguration")
public class CellarPersistentConfiguration extends CellarStaticConfiguration {

    @Autowired
    private ConfigurationPropertyService configurationPropertyService;

    /**
     * @see eu.europa.ec.opoce.cellar.cl.configuration.impl.CellarStaticConfiguration#isPersistent(java.lang.String)
     */
    @Override
    public boolean isPersistent(final String key) {
        return this.getPropertyLoader().getPropertyManager().getBooleanProperty(key + ".persistent");
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.configuration.impl.CellarStaticConfiguration#getConfigurationPropertyService()
     */
    @Override
    public ConfigurationPropertyService getConfigurationPropertyService() {
        return this.configurationPropertyService;
    }

}
