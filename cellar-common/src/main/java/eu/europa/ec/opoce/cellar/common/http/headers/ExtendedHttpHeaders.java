package eu.europa.ec.opoce.cellar.common.http.headers;

import java.util.Map;

public interface ExtendedHttpHeaders {

    public Map<String, String> getLinks();

    /**
     * See {@link <a href="http://tools.ietf.org/html/rfc5988#page-6">Web Linking (IETF RFC-5988) documentation.</a>}
     * Note: this has been implemented in Java 7 - In case the CELLAR project would migrate from Java 6 to a further version
     * of Java, it is recommended to use the official implementation of this specific header, cfr.
     * {@link <a href="http://docs.oracle.com/javaee/7/api/javax/ws/rs/core/Link.html">Link (Java(TM) EE Specification APIs).</a>}
     */
    static final String LINK = "Link";
}
