/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.exception
 *             FILE : DigitalObjectNotFoundException.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 10 31, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-10-31 16:28:19 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.exception;

/**
 * @author ARHS Developments
 */
public class DigitalObjectNotFoundException extends CellarException {

    public DigitalObjectNotFoundException(ExceptionBuilder<? extends BuildableException> builder) {
        super(builder);
    }
}
