package eu.europa.ec.opoce.cellar.logging;

/**
 * @author ARHS Developments
 * @since 7.7
 */
public interface ThresholdMarkerFilterMBean {

    /**
     * Returns the {@code LoggerConfig} level as a String.
     *
     * @return the {@code LoggerConfig} level.
     */
    String getLevel();

    /**
     * Sets the {@code LoggerConfig} level to the specified value.
     *
     * @param level the new {@code LoggerConfig} level.
     * @throws IllegalArgumentException if the specified level is not one of
     *             "OFF", "FATAL", "ERROR", "WARN", "INFO", "DEBUG", "TRACE",
     *             "ALL"
     */
    void setLevel(String level);

    /**
     * Returns true if the check of the {@link org.apache.logging.log4j.ThreadContext}
     * is disabled.
     * @return true if the check is disabled, otherwise false
     */
    boolean getIgnoreContext();

    /**
     *
     * @param ignoreContext the ignore context value
     */
    void setIgnoreContext(boolean ignoreContext);

}
