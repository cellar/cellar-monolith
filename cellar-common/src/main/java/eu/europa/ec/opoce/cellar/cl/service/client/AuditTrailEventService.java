/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.service.client
 *        FILE : AuditTrailEventService.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 09-01-2013
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.client;

import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEvent;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventAction;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventProcess;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventType;
import eu.europa.ec.opoce.cellar.cl.domain.package_history.PackageHistory;
import eu.europa.ec.opoce.cellar.cl.domain.structmap_status_history.StructMapStatusHistory;

import java.util.Date;
import java.util.List;

/**
 * <class_description> Service interface for logging events on the audit trail event table.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 09-01-2013
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public interface AuditTrailEventService {

    /**
     * Save an audit event.
     */
    void saveEvent(AuditTrailEvent event);

    /**
     * Save an audit event.
     *
     * @param process that fired the event
     * @param action action performed
     * @param type the event type
     * @param referenceObject the reference of the object
     * @param message the message to save
     */
    void saveEvent(AuditTrailEventProcess process, AuditTrailEventAction action, AuditTrailEventType type, String referenceObject,
                   String message, String processId, StructMapStatusHistory structMapStatusHistory, PackageHistory packageHistory);

    /**
     * Delete an audit event.
     */
    void deleteEvent(AuditTrailEvent event);

    /**
     * Delete all instances created before the specified date.
     * 
     * @param endDate Date limit for deletion.
     */
    void deleteEvent(Date endDate);

    /**
     * Get all log entries of the specified <code>AuditTrailEventProcess</code>.
     * 
     * @param process The <code>AuditTrailEventProcess</code>.
     * @return All entries of the type specified.
     */
    List<AuditTrailEvent> getLogsByProcess(AuditTrailEventProcess process);

    /**
     * Get all log entries for the specified object identifier.
     * 
     * @param id The object identifier.
     * @return All entries for the specified object identifier.
     */
    List<AuditTrailEvent> getLogsByIdentifier(String id);

    /**
     * Get all log entries for the specified object identifier.
     * 
     * @param id The object identifier with ending wildcard (*).
     * @return All entries for the specified object identifier.
     */
    List<AuditTrailEvent> getLogsByIdentifierWildcard(String id);

    /**
     * Get all log entries of the specified <code>AuditTrailEventProcess</code> and for the specified
     * object identifier.
     * 
     * @param process The <code>AuditTrailEventProcess</code>.
     * @param id The object identifier with ending wildcard (*).
     * @return All entries of the type specified and for the specified object identifier.
     */
    List<AuditTrailEvent> getLogsByProcessAndIdentifierWildcard(AuditTrailEventProcess process, String id);

    /**
     * Get all log entries of the specified <code>AuditTrailEventProcess</code> for the specified
     * object identifier.
     * 
     * @param process The <code>AuditTrailEventProcess</code>.
     * @param id The object identifier.
     * @return All entries of the type specified and for the specified object identifier.
     */
    List<AuditTrailEvent> getLogsByProcessAndIdentifier(AuditTrailEventProcess process, String id);

    /**
     * Get log entries for the specified <code>java.util.Date</code> range.
     * 
     * @param from Starting date.
     * @param to Ending date.
     * @return All logs entries in the specified date range.
     */
    List<AuditTrailEvent> getLogs(Date from, Date to);

    /**
     * Get log entries for the specified <code>java.util.Date</code> range.
     * 
     * @param from Starting date.
     * @param to Ending date.
     * @param start First row index.
     * @param end Last row index.
     * @return All logs entries in the specified date range.
     */
    List<AuditTrailEvent> getLogs(Date from, Date to, int start, int end);

    /**
     * Get log entries created by the current thread.
     * 
     * @return the log entries created by the current thread.
     */
    List<AuditTrailEvent> getLogs();

    /**
     * Format in a XML the log entries created by the current thread.
     * 
     * @return the XML
     */
    StringBuffer getXMLLogs();

    /**
     * Format in a XML the log entries by object identifier.
     * 
     * @param objectId the object identifier
     * @return the XML
     */
    StringBuffer getXMLLogsFromIdentifier(String objectId);

    /**
     * Get the number of log entries in the specified date range.
     * 
     * @param from Starting date.
     * @param to Ending date.
     * @param process log entry process of type <code>AuditTrailEventProcess</code>.
     * @param objectId Object identifier.
     * @return The number of log entries in the specified date range.
     */
    Long countLogsBetweenDates(Date from, Date to, AuditTrailEventProcess process, String objectId);

}
