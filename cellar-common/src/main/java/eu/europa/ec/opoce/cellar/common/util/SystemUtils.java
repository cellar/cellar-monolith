/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.util
 *             FILE : SystemUtils.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 03 30, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-03-30 13:56:36 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.management.RuntimeMXBean;

import static java.lang.management.ManagementFactory.getRuntimeMXBean;

/**
 * @author ARHS Developments
 */
public final class SystemUtils {

    private static final Logger LOG = LogManager.getLogger(SystemUtils.class);

    private static final String PID_SEPARATOR = "@";
    
    private static final int PID;
    

    static {
        // TODO Java 9+ -> replace by ProcessHandle.current().getPid();
        int pid = -1;
    	RuntimeMXBean runtime = getRuntimeMXBean();
        String jvmName = runtime.getName();
        if (jvmName.contains(PID_SEPARATOR)) {
        	String pidStr = StringUtils.substringBefore(jvmName, PID_SEPARATOR);
        	try {
        		pid = Integer.parseInt(pidStr);
        	}
        	catch (NumberFormatException e) {
        		LOG.error("Could not parse PID to integer.", e);
        	}
        }
        else {
        	LOG.error("Could not detect PID within the JVM name: {}.", jvmName);
        }
        PID = pid;
    }

    private SystemUtils() {
    }

    public static int getPid() {
        return PID;
    }
}
