/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller
 *             FILE : AbstractCellarResponseController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Mar 2, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller;

import eu.europa.ec.op.cellar.api.service.ICellarResponse;
import eu.europa.ec.opoce.cellar.cl.controller.util.ControllerUtil;
import eu.europa.ec.opoce.cellar.cl.domain.mimeType.MimeTypeMapping;
import eu.europa.ec.opoce.cellar.cl.exception.CellarWebException;
import eu.europa.ec.opoce.cellar.cl.service.client.MimeTypeCacheService;
import eu.europa.ec.opoce.cellar.common.IResolver;
import eu.europa.ec.opoce.cellar.common.util.StringUtils;
import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

/**
 * <class_description> Abstract controller to manipulate Cellar response from the Cellar-api.
 *
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Mar 2, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
public class AbstractCellarResponseController extends AbstractCellarController {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractCellarResponseController.class);

    private static final String ENCODING = "UTF-8";

    /**
     * Mime type resolver.
     */
    @Autowired
    private IResolver<ICellarResponse, String> responseMimetypesResolver;

    /**
     * Mime types cache service.
     */
    @Autowired
    private MimeTypeCacheService mimeTypeCacheService;

    /**
     * Return the String ResponseEntity corresponding to the Cellar response.
     * @param cellarResponse the response to transform
     * @return the String ResponseEntity corresponding to the Cellar response
     */
    protected ResponseEntity<String> retrieveStringResponse(final ICellarResponse cellarResponse) {
        final Writer writer = new StringWriter();
        String responseStr = null;
        try {
            IOUtils.copy(cellarResponse.getResponse(), writer, ENCODING); // copy to the writer
            responseStr = writer.toString();
        } catch (final Exception e) {
            final CellarWebException exc = HttpStatusAwareExceptionBuilder.getInstance(CellarWebException.class).withMessage(e.getMessage())
                    .withCause(e).build();
            return ControllerUtil.makeErrorResponse(exc, exc.getHttpStatus());
        } finally {
            try {
                writer.close();
            } catch (final IOException e) {
                LOG.warn("Unable to close response writer", e);
            }
        }

        // builds and returns the response entity
        final ResponseEntity<String> responseEntity = ControllerUtil.makeXMLResponse(responseStr);
        return responseEntity;
    }

    /**
     * Return the byte ResponseEntity corresponding to the Cellar response.
     * @param cellarResponse the response to transform
     * @param pid the production identifier corresponding to the response
     * @return the byte ResponseEntity corresponding to the Cellar response
     */
    protected ResponseEntity<byte[]> retrieveBytesResponse(final ICellarResponse cellarResponse, final String pid) {
        return this.retrieveBytesResponse(cellarResponse, pid, null);
    }

    /**
     * Return the byte ResponseEntity corresponding to the Cellar response.
     * @param cellarResponse the response to transform
     * @param pid the production identifier corresponding to the response
     * @param attachmentFilename the filename if the response must be returned as attachment
     * @return the byte ResponseEntity corresponding to the Cellar response
     */
    protected ResponseEntity<byte[]> retrieveBytesResponse(final ICellarResponse cellarResponse, final String pid,
            final String attachmentFilename) {
        byte[] bytes = null;
        try {
            bytes = IOUtils.toByteArray(cellarResponse.getResponse());
        } catch (final Exception e) {
            final CellarWebException exc = HttpStatusAwareExceptionBuilder.getInstance(CellarWebException.class).withMessage(e.getMessage())
                    .withCause(e).build();
            final ResponseEntity<String> responseErrorStr = ControllerUtil.makeErrorResponse(exc, exc.getHttpStatus());
            return ControllerUtil.toBytesResponseEntity(responseErrorStr);
        }

        // resolves the response's mimetype against the in-browser viewable mimetypes:
        // if the mimetype is such, then the content is viewed in-browser, otherwise it is presented as an attachment to be downloaded
        String myAttachmentFilename;
        final String resolvedResponseMimetype = this.responseMimetypesResolver.resolve(cellarResponse);
        if (resolvedResponseMimetype != null) {
            myAttachmentFilename = null;
        } else if (attachmentFilename == null) {
            myAttachmentFilename = this.getAttachmentFileName(cellarResponse, pid);
        } else {
            myAttachmentFilename = attachmentFilename;
        }

        // builds and returns the response entity
        final ResponseEntity<byte[]> responseEntity = ControllerUtil.makeBytesResponse(bytes, resolvedResponseMimetype,
                myAttachmentFilename == null, myAttachmentFilename);
        return responseEntity;
    }

    /**
     * Return the attachment filename corresponding to the response and the pid.
     * @param cellarResponse the Cellar response
     * @param pid the production identifier
     * @return the attachment filename
     */
    protected String getAttachmentFileName(final ICellarResponse cellarResponse, final String pid) {
        String result = null;
        final Map<String, List<MimeTypeMapping>> mimeTypeMappingByMimeTypeMap = mimeTypeCacheService.getMimeTypeMap();
        if ((mimeTypeMappingByMimeTypeMap != null) && !mimeTypeMappingByMimeTypeMap.isEmpty()) {
            final List<String> contentTypeList = cellarResponse.getHeaders().get("Content-Type"); // get the content-type of the response
            if ((contentTypeList != null) && !contentTypeList.isEmpty()) {
                result = extractFileName(pid, mimeTypeMappingByMimeTypeMap, contentTypeList);
            }
        }
        return result != null ? result : pid;
    }

    /**
     * @param pid
     * @param result
     * @param mimeTypeMappingByMimeTypeMap
     * @param contentTypeList
     * @return
     */
    private String extractFileName(final String pid, final Map<String, List<MimeTypeMapping>> mimeTypeMappingByMimeTypeMap,
            final List<String> contentTypeList) {
        final Stream<String> map = mimeTypeMappingByMimeTypeMap.entrySet().stream()
                .map(mimeTypeMappingByMimeTypeMapEntry -> extractFileName(pid, contentTypeList, mimeTypeMappingByMimeTypeMapEntry.getKey(),
                        mimeTypeMappingByMimeTypeMapEntry.getValue()))
                .filter(Objects::nonNull);
        final String result = map.findFirst().orElse(null);
        return result;
    }

    /**
     * @param pid
     * @param result
     * @param contentTypeList
     * @param mimeType
     * @param mimeTypeMappings
     * @return
     */
    private String extractFileName(final String pid, final List<String> contentTypeList, final String mimeType,
            final List<MimeTypeMapping> mimeTypeMappings) {
        final Stream<String> map = contentTypeList.stream().filter(contentType -> contentType.contains(mimeType))
                .map(contentType -> extractFileName(pid, mimeTypeMappings)).filter(Objects::nonNull);
        final String result = map.findFirst().orElse(null);
        return result;
    }

    /**
     * @param pid
     * @param result
     * @param mimeTypeMappings
     * @return
     */
    private String extractFileName(final String pid, final List<MimeTypeMapping> mimeTypeMappings) {
        final Stream<MimeTypeMapping> filter = mimeTypeMappings.stream()
                .filter(mimeTypeMapping -> StringUtils.isNotBlank(mimeTypeMapping.getExtension())).filter(Objects::nonNull);
        final MimeTypeMapping match = filter.findFirst().orElse(null);
        final String result = match != null ? pid + "." + match.getExtension() : null;
        return result;
    }

    void setMimeTypeCacheService(MimeTypeCacheService mimeTypeCacheService) {
        this.mimeTypeCacheService = mimeTypeCacheService;
    }
}
