/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.retry.impl
 *             FILE : NoRetryStrategy.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 15-07-2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.retry.impl;

/**
 * <class_description> An implementation of {@link eu.europa.ec.opoce.cellar.common.retry.impl.BaseRetryStrategy}
 * which attempts execution just once - that is, making no retry.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 15-07-2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class NoRetryStrategy<P> extends BaseRetryStrategy<P> {

    public NoRetryStrategy() {
        this.maxAttempts(1);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.common.retry.impl.BaseRetryStrategy#calculateDelay(int)
     */
    @Override
    protected long calculateDelay(int currentAttempt) {
        return 0;
    }

}
