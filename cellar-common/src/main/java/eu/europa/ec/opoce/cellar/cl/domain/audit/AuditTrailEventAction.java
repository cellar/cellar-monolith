/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.audit
 *        FILE : AuditTrailEventAction.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 01-10-2015
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.audit;

/**
 * <class_description> Available actions for Audit Trail.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 01-10-2015
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public enum AuditTrailEventAction {

    Validate,
    AbstractValidation,
    ValidationShacl,
    ValidateDirectMetadata,
    ValidateTechnicalMetadata,
    Convert,
    Read,
    Create,
    Append,
    Update,
    Merge,
    Delete,
    Load,
    Decode,
    Archive,
    Push,
    Pop,
    Set,
    SipDependencyChecker,
    Package,
    UnzippingPackage,
    AcquireMainLock,
    ConcurrencyControllerLocking,
    Insert,
    IngestionStructMap,
    LoadingMetadaSnippets,
    LoadingMetadaSnippetsIntoS3,
    LoadingMetadaSnippetsIntoOracle,
    UpdateInverseTables,
    UpdateCellarResourceTables,
    VirtuosoWrite,
    VirtuosoDelete,
    Undefined;

    public String getName() {
        return this.toString();
    }
}
