/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : CellarIdentifier.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 25 mai 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.client;

import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;

/**
 * The Interface CellarIdentifierService.
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 25 mai 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface CellarIdentifierService {

    /**
     * Checks if any parent of the given digital object is read only or itself.
     *
     * @param digitalObject the digital object
     * @return the boolean
     */
    Boolean isAnyParentReadOnly(final DigitalObject digitalObject);

    /**
     * Checks if any child of the given digital object is read only or itself.
     *
     * @param digitalObject the digital object
     * @return the boolean
     */
    Boolean isAnyChildReadOnly(final DigitalObject digitalObject);

    /**
     * Gets the cellar identifier.
     *
     * @param cellarId the cellar id
     * @return the cellar identifier
     */
    CellarIdentifier getCellarIdentifier(final String cellarId);

    /**
     * Checks if is read only.
     *
     * @param cellarId the cellar id
     * @return the boolean
     */
    Boolean isReadOnly(final String cellarId);

    /**
     * Update cellar identifier.
     *
     * @param cellarIdentifier the cellar identifier
     */
    void updateCellarIdentifier(final CellarIdentifier cellarIdentifier);
}
