/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.exception
 *        FILE : NotImplementedExceptionBuilder.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 19-03-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.exception;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.MessageCode;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the exception builder meant to build an instance of {@link NotImplementedException}.</br>
 * </br> 
 * ON : 19-03-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public class NotImplementedExceptionBuilder extends ExceptionBuilder<NotImplementedException> {

    private transient static final Logger LOGGER = LoggerFactory.getLogger(NotImplementedExceptionBuilder.class);

    public static NotImplementedExceptionBuilder get() {
        return new NotImplementedExceptionBuilder(NotImplementedException.class);
    }

    protected NotImplementedExceptionBuilder(final Class<NotImplementedException> exceptionClass) {
        super(exceptionClass);
    }

    @Override
    public NotImplementedExceptionBuilder withCode(final MessageCode code) {
        LOGGER.warn("Cannot set code of a {}, as it is locked to {}.", NotImplementedException.class, CommonErrors.NOT_IMPLEMENTED);
        return this;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.exception.ExceptionBuilder#fallback()
     */
    @Override
    protected void fallback() {
        if (this.code == null) {
            this.code = CommonErrors.NOT_IMPLEMENTED;
        }
    }

}
