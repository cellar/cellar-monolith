/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.builder
 *             FILE : IBuilder.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 12, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.builder;

/**
 * <class_description> A common interface for builder pattern.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 12, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IBuilder<R> {

    /**
     * Build the object.
     * @return the object
     */
    R build();
}
