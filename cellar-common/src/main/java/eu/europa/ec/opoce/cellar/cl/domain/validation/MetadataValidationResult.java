package eu.europa.ec.opoce.cellar.cl.domain.validation;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

public class MetadataValidationResult {

    public enum ModelType {
        DIRECT,
        TECHNICAL
    }

    @JsonProperty(value = "requestId")
    private String requestId;

    @JsonProperty(value = "cellarId")
    private String cellarId;

    @JsonProperty(value = "modelType")
    private ModelType modelType;

    @JsonProperty(value = "errorMessage")
    private String errorMessage;

    @JsonRawValue
    private String validationResult;

    @JsonProperty(value = "validJenaModelProvided")
    private boolean validJenaModelProvided;

    @JsonProperty(value = "conformModelProvided")
    private boolean conformModelProvided;

    @JsonProperty(value = "totalTimeForInferenceInMilliSeconds")
    private long totalTimeForInferenceInMilliSeconds;

    @JsonProperty(value = "totalTimeForValidationInMilliSeconds")
    private long totalTimeForValidationInMilliSeconds;

    /**
     * Set that property to true if the model contains no violation
     * and no warning.
     */
    @JsonIgnore
    private boolean ignored;

    public MetadataValidationResult() {
        requestId = null;
        errorMessage = "";
        validationResult = "";
        validJenaModelProvided = false;
        totalTimeForInferenceInMilliSeconds = -1;
        totalTimeForValidationInMilliSeconds = -1;
        conformModelProvided = false;
        ignored = false;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getCellarId() {
        return cellarId;
    }

    public void setCellarId(String cellarId) {
        this.cellarId = cellarId;
    }

    public ModelType getModelType() {
        return modelType;
    }

    public void setModelType(ModelType modelType) {
        this.modelType = modelType;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    // the validation result is a JSON coming from a Jena Model (RDF/JSON) and is not formatted properly
    // we don't want this property to be added to the main result so the result is manually split in 2 files
    // one for the info (all fields here - validation result) and one with only the validation result
//    @JsonIgnore
    public String getValidationResult() {
        return validationResult;
    }

    @JsonProperty(value = "validationResult")
    public void setValidationResult(String validationResult) {
        this.validationResult = validationResult;
    }

    public boolean isValidJenaModelProvided() {
        return validJenaModelProvided;
    }

    public void setValidJenaModelProvided(boolean validJenaModelProvided) {
        this.validJenaModelProvided = validJenaModelProvided;
    }

    public boolean conformModelProvided() {
        return conformModelProvided;
    }

    public void setConformModelProvided(boolean conformModelProvided) {
        this.conformModelProvided = conformModelProvided;
    }

    public long getTotalTimeForInferenceInMilliSeconds() {
        return totalTimeForInferenceInMilliSeconds;
    }

    public void setTotalTimeForInferenceInMilliSeconds(long totalTimeForInferenceInMilliSeconds) {
        this.totalTimeForInferenceInMilliSeconds = totalTimeForInferenceInMilliSeconds;
    }

    public long getTotalTimeForValidationInMilliSeconds() {
        return totalTimeForValidationInMilliSeconds;
    }

    public void setTotalTimeForValidationInMilliSeconds(long totalTimeForValidationInMilliSeconds) {
        this.totalTimeForValidationInMilliSeconds = totalTimeForValidationInMilliSeconds;
    }

    @JsonIgnore
    public long getTotalTimeMilliSeconds() {
        return this.totalTimeForValidationInMilliSeconds + this.totalTimeForInferenceInMilliSeconds;
    }

    public boolean isConformModelProvided() {
        return conformModelProvided;
    }

    public boolean isIgnored() {
        return ignored;
    }

    public void setIgnored(boolean ignored) {
        this.ignored = ignored;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MetadataValidationResult result = (MetadataValidationResult) o;
        return validJenaModelProvided == result.validJenaModelProvided &&
                conformModelProvided == result.conformModelProvided &&
                totalTimeForInferenceInMilliSeconds == result.totalTimeForInferenceInMilliSeconds &&
                totalTimeForValidationInMilliSeconds == result.totalTimeForValidationInMilliSeconds &&
                Objects.equal(requestId, result.requestId) &&
                Objects.equal(cellarId, result.cellarId) &&
                modelType == result.modelType &&
                Objects.equal(errorMessage, result.errorMessage) &&
                Objects.equal(validationResult, result.validationResult);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(requestId, cellarId, modelType, errorMessage, validationResult, validJenaModelProvided,
                conformModelProvided, totalTimeForInferenceInMilliSeconds, totalTimeForValidationInMilliSeconds);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("requestId", requestId)
                .add("cellarId", cellarId)
                .add("errorMessage", errorMessage)
                .add("validationResult", validationResult)
                .add("validJenaModelProvided", validJenaModelProvided)
                .add("conformModelProvided", conformModelProvided)
                .add("totalTimeForInferenceInMilliSeconds", totalTimeForInferenceInMilliSeconds)
                .add("totalTimeForValidationInMilliSeconds", totalTimeForValidationInMilliSeconds)
                .toString();
    }

    @JsonIgnore
    public String toShortString() {
        return MoreObjects.toStringHelper(this)
                .add("requestId", requestId)
                .add("cellarId", cellarId)
                .add("isConform", conformModelProvided)
                .add("totalTime", this.getTotalTimeMilliSeconds() + "ms")
                .toString();
    }
}
