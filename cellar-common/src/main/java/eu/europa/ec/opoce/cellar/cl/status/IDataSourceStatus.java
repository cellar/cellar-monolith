/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.status
 *        FILE : IDataSourceStatus.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 24-12-2015
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.status;

import eu.europa.ec.opoce.cellar.exception.CellarConfigurationException;

/**
 * <class_description> This interface defines the utilities for checking the status of a datasource.</br>
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 14-05-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IDataSourceStatus extends IServiceStatus {

    /**
     * Gets the URL to the database connection.
     * 
     * @return the URL of the connection to the database
     * @throws CellarConfigurationException the cellar configuration exception
     */
    String getDatabaseUrl() throws CellarConfigurationException;
}
