package eu.europa.ec.opoce.cellar.cl.dao;

import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEvent;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventProcess;

import java.util.Date;
import java.util.List;

/**
 * Dao for the Entity {@link AuditTrailEvent}.
 * 
 * @author dcraeye
 * 
 */
public interface AuditTrailEventDao {

    /**
     * Delete the given persistent instance.
     * 
     * @param auditTrailEvent
     *            the persistent instance to delete
     */
    void deleteAuditTrailEvent(AuditTrailEvent auditTrailEvent);

    /**
     * Delete all instances created before the specified date.
     * 
     * @param endDate
     *            Date limit for deletion.
     */
    void deleteAuditTrailEvent(Date endDate);

    /**
     * Return the persistent instance with the given internal identifier, or null if not found.
     * 
     * @param id
     *            the internal identifier of the persistent instance
     * 
     * @return the persistent instance, or null if not found
     */
    AuditTrailEvent getAuditTrailEvent(Long id);

    /**
     * Return all persistent instances of {@link AuditTrailEvent}.
     * 
     * @return a {@link List} containing 0 or more persistent instances.
     */
    List<AuditTrailEvent> getAuditTrailEvents();

    /**
     * Persist the given transient instance.
     * 
     * @param auditTrailEvent
     *            the transient instance to be persisted
     */
    void saveAuditTrailEvent(AuditTrailEvent auditTrailEvent);

    /**
     * Get all log entries of the specified <code>AuditTrailEventProcess</code>.
     * 
     * @param process
     *            The <code>AuditTrailEventProcess</code>.
     * @return All entries of the type specified.
     */
    List<AuditTrailEvent> getLogsByProcess(AuditTrailEventProcess process);

    /**
     * Get all log entries for the specified object identifier.
     * 
     * @param id
     *            The object identifier.
     * @return All entries for the specified object identifier.
     */
    List<AuditTrailEvent> getLogsByIdentifier(String id);

    /**
     * Get all log entries for the specified object identifier.
     * 
     * @param id
     *            The object identifier with ending wildcard (*).
     * @return All entries for the specified object identifier.
     */
    List<AuditTrailEvent> getLogsByIdentifierWildcard(String id);

    /**
     * Get all log entries of the specified <code>AuditTrailEventProcess</code> and for the specified
     * object identifier.
     * 
     * @param process
     *            The <code>AuditTrailEventprocess</code>.
     * @param id
     *            The object identifier with ending wildcard (*).
     * @return All entries of the type specified and for the specified object identifier.
     */
    List<AuditTrailEvent> getLogsByProcessAndIdentifierWildcard(AuditTrailEventProcess process, String id);

    /**
     * Get all log entries of the specified <code>AuditTrailEventProcess</code> for the specified
     * object identifier.
     * 
     * @param process
     *            The <code>AuditTrailEventProcess</code>.
     * @param id
     *            The object identifier.
     * @return All entries of the type specified and for the specified object identifier.
     */
    List<AuditTrailEvent> getLogsByProcessAndIdentifier(AuditTrailEventProcess process, String id);

    /**
     * Get log entries for the specified <code>java.util.Date</code> range.
     * 
     * @param from
     *            Starting date.
     * @param to
     *            Ending date.
     * @return All logs entries in the specified date range.
     */
    List<AuditTrailEvent> getLogs(Date from, Date to);

    /**
     * Get log entries for the specified <code>java.util.Date</code> range.
     * 
     * @param from
     *            Starting date.
     * @param to
     *            Ending date.
     * @param start
     *            First row index.
     * @param end
     *            Last row index.
     * @return All logs entries in the specified date range.
     */
    List<AuditTrailEvent> getLogs(Date from, Date to, int start, int end);

    /**
     * Get log entries created by the thread with the given threadName.
     * 
     * @param threadName the thread's name whose entries to search for
     * @return the log entries created by the thread with the given threadName
     */
    List<AuditTrailEvent> getLogsByThreadName(String threadName);

    /**
     * Get the number of log entries in the specified date range.
     * 
     * @param from
     *            Starting date.
     * @param to
     *            Ending date.
     * @param process
     *            Log entry type of type <code>AuditTrailEventProcess</code>.
     * @param objectId
     *            Object identifier.
     * @return The number of log entries in the specified date range.
     */
    Long countLogsBetweenDates(Date from, Date to, AuditTrailEventProcess process, String objectId);
}
