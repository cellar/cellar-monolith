package eu.europa.ec.opoce.cellar.common.util;

/**
 * <p>Wrapper class.</p>
 *
 */
public class Wrapper<T> {

    private T value;

    /**
     * <p>Constructor for Wrapper.</p>
     *
     * @param <T> a T object.
     */
    public Wrapper() {
    }

    /**
     * <p>Constructor for Wrapper.</p>
     *
     * @param value a T object.
     */
    public Wrapper(T value) {
        this.value = value;
    }

    /**
     * <p>Getter for the field <code>value</code>.</p>
     *
     * @return a T object.
     */
    public T getValue() {
        return value;
    }

    /**
     * <p>Setter for the field <code>value</code>.</p>
     *
     * @param value a T object.
     */
    public void setValue(T value) {
        this.value = value;
    }
}
