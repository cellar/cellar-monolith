package eu.europa.ec.opoce.cellar.domain.content.mets;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * data-structure to group all data about a mets-document
 */
public class MetsDocument implements Serializable {

    private static final long serialVersionUID = -7313348184494425666L;

    private String documentId;
    private Date createDate;
    private OperationType type;
    private Map<String, StructMap> structMaps = new LinkedHashMap<String, StructMap>();
    private Map<String, MetadataStream> metadataStreams = new HashMap<String, MetadataStream>();
    private Map<String, ContentStream> contentStreams = new HashMap<String, ContentStream>();
    private MetsProfile profile;
    private String header;

    public MetsProfile getProfile() {
        return profile;
    }

    public void setProfile(MetsProfile profile) {
        this.profile = profile;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public OperationType getType() {
        return type;
    }

    public void setType(OperationType type) {
        this.type = type;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public MetadataStream getMetadataStream(String id) {
        return metadataStreams.get(id);
    }

    public boolean hasMetadataStream(String id) {
        return metadataStreams.containsKey(id);
    }

    // Load result of dmdSec & amdSec/techMD

    public void addMetadataStream(String id, MetadataStream metadataStream) {
        metadataStreams.put(id, metadataStream);
    }

    public ContentStream getContentStream(String id) {
        return contentStreams.get(id);
    }

    public boolean hasContentStream(String id) {
        return contentStreams.containsKey(id);
    }

    // Load result of file element

    public void addContentStream(String id, ContentStream contentStream) {
        contentStreams.put(id, contentStream);
    }

    public Map<String, ContentStream> getContentStreams() {
        return contentStreams;
    }

    public Map<String, StructMap> getStructMaps() {
        return structMaps;
    }

    public StructMap getStructMap(String id) {
        return structMaps.get(id);
    }

    public void addStructMap(StructMap structMap) {
        structMaps.put(structMap.getId(), structMap);
    }

    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "MetsDocument { " + //
                "id=" + this.getDocumentId() + //
                ", createDate='" + this.getCreateDate() + //
                ", type=" + this.getType() + //
                ", header='" + this.getHeader() + //
                ", profile=" + this.getProfile() + //
                " }";
    }
}
