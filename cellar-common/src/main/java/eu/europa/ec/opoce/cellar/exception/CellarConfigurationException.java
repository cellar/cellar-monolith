/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.exception
 *        FILE : CellarConfigurationException.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 13-06-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.exception;

/**
 * This is a {@link CellarConfigurationException} buildable by an {@link ExceptionBuilder}.</br>
 * </br> 
 * ON : 13-06-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public class CellarConfigurationException extends CellarException {

    private static final long serialVersionUID = 8719822445602335100L;

    /**
     * Constructs a new exception with its associated builder.
     * 
     * @param builder the builder to use for building the exception
     */
    public CellarConfigurationException(final ExceptionBuilder<? extends BuildableException> builder) {
        super(builder);
    }

}
