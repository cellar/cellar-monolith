/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.virtuoso.service
 *             FILE : VirtuosoExceptionMessagesService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 18 août 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.virtuoso.service;

import eu.europa.ec.opoce.cellar.cl.domain.virtuoso.VirtuosoExceptionMessages;

import java.util.List;

/**
 * The Interface VirtuosoExceptionMessagesService.
 * <class_description> The service class to access the DAO of the virtuoso messages objects.
 * <br/><br/>
 * ON : 18 août 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface VirtuosoExceptionMessagesService {

    /**
     * Find all.
     *
     * @return the list
     */
    List<VirtuosoExceptionMessages> findAll();

    /**
     * Find by message.
     *
     * @param message the message
     * @return the list
     */
    List<VirtuosoExceptionMessages> findByMessage(String message);

}
