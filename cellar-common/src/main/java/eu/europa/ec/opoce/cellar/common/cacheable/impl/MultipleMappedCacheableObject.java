/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice
 *             FILE : MultipleCacheableObject.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 29, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.cacheable.impl;

import eu.europa.ec.opoce.cellar.common.cacheable.IMultipleCacheableObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 29, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@SuppressWarnings("serial")
public abstract class MultipleMappedCacheableObject<K, V> extends HashMap<K, V> implements IMultipleCacheableObject<K, V> {

    @Override
    public Collection<V> getMultiple(final Collection<K> keys) {
        final List<K> uncachedKeys = new ArrayList<K>();
        final Collection<V> values = new ArrayList<V>();
        for (final K key : keys) {
            if (containsKey(key)) {
                values.add(get(key));
            } else {
                uncachedKeys.add(key);
            }
        }

        final Collection<V> uncachedValues = this.retrieveMultiple(uncachedKeys).values();
        values.addAll(uncachedValues);

        return values;
    }

    protected abstract Map<K, V> retrieveMultiple(final Collection<K> key);

}
