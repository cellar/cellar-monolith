package eu.europa.ec.opoce.cellar;

/**
 * This class provides constants to be widely used throughout the application.
 *
 * @author EUROPEAN DYNAMICS S.A.
 */
public final class AppConstants {

    /**
     * A custom new line character. <br><br>
     * <p>
     * Cellar will by-default escape all '\n' characters in all log4j.xml files.
     * Regex '&lt;newline&gt;' will be replaced in all log4j.xml files with a newline.
     * </p>
     * <p>
     * Use this custom newline character instead of '\n' to create a newline in Cellar.
     * </p>
     */
    public static final String NEWLINE = "<newline>";

    private AppConstants() {
    }
}
