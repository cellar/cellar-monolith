/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.virtuoso.service
 *             FILE : VirtuosoExceptionMessagesLoaderService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 18 août 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.virtuoso.service;

import java.util.Set;

/**
 * The Interface VirtuosoExceptionMessagesLoaderService.
 * <class_description> This service should be used to check if a given virtuoso exception message should trigger the retry strategy
 * <br/><br/>
 * ON : 18 août 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface VirtuosoExceptionMessagesLoaderService {

    /**
     * Contains similar message.
     *
     * @param exceptionMessage the exception message
     * @return true, if successful
     */
    boolean containsSimilarMessage(String exceptionMessage);

    /**
     * Gets the all messages.
     *
     * @return the all messages
     */
    Set<String> getAllMessages();

}
