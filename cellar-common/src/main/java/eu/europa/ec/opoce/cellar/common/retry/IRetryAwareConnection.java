/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.jena.oracle.impl
 *             FILE : OracleExponentialRetryConnection.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 06-03-2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.retry;

import eu.europa.ec.opoce.cellar.common.closure.PClosure;
import eu.europa.ec.opoce.cellar.exception.CellarException;

/**
 * <class_description> Manages the concurrent accesses to the CMR tables through a fire-and-retry mechanism based
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 06-03-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IRetryAwareConnection<P> {

    /**
     * <p>connect.</p>
     *
     * @param connectionCode a C object.
     * @throws CellarException in case a problem occurs during the attempt to connect.
     */
    <C extends PClosure<P>> void connect(final C connectionCode) throws CellarException;

    void rollback() throws CellarException;

    String[] resolveRetriableMessages();

}
