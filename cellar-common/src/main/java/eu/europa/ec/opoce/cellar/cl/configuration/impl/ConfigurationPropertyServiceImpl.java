/**
 * 
 */
package eu.europa.ec.opoce.cellar.cl.configuration.impl;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.ccr.service.database.DefaultTransactionService;
import eu.europa.ec.opoce.cellar.cl.configuration.ConfigurationPropertyService;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.dao.ConfigurationPropertyDao;
import eu.europa.ec.opoce.cellar.cl.domain.admin.ConfigurationProperty;
import eu.europa.ec.opoce.cellar.cl.service.client.CommonConfigurationPropertyService;
import eu.europa.ec.opoce.cellar.exception.CellarConfigurationException;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * DAO layer implementation for accessing <code>ConfigurationProperty</code> instances in the
 * persistence layer.
 * 
 * @author dbacquel
 * 
 */
@Service
public class ConfigurationPropertyServiceImpl extends DefaultTransactionService
        implements ConfigurationPropertyService, CommonConfigurationPropertyService {

    @Autowired
    @Qualifier("configurationPropertyDbDao")
    private ConfigurationPropertyDao configurationPropertyDao;

    /**
     * The cellar configuration to use.
     * It is static and not persistent, as the latter one uses this Dao class itself, and would not be completely initialized at this point.
     */
    @Autowired
    @Qualifier("cellarStaticConfiguration")
    private ICellarConfiguration cellarConfiguration;

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void createProperty(final ConfigurationProperty property) {
        this.checkDatabaseReadOnly();
        configurationPropertyDao.createProperty(property);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void deleteProperty(final ConfigurationProperty property) {
        this.checkDatabaseReadOnly();
        configurationPropertyDao.deleteProperty(property);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void deleteProperties(final Collection<ConfigurationProperty> properties) {
        this.checkDatabaseReadOnly();
        configurationPropertyDao.deleteProperties(properties);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<ConfigurationProperty> getProperties() {
        return configurationPropertyDao.getProperties();
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("deprecation")
    @Override
    public ConfigurationProperty getPropertyById(final Long id) {
        return configurationPropertyDao.getProperty(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ConfigurationProperty getPropertyByKey(final String key) {
        return configurationPropertyDao.getPropertyByKey(key);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPropertyValue(final String key) {
        return configurationPropertyDao.getPropertyByKey(key).getPropertyValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isPropertyExists(final ConfigurationProperty configurationProperty) {
        return configurationPropertyDao.isPropertyExists(configurationProperty);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void updateProperty(final ConfigurationProperty configurationProperty) {
        this.checkDatabaseReadOnly();
        configurationPropertyDao.updateProperty(configurationProperty);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.configuration.ConfigurationPropertyService#synchronizeProperty(java.lang.String, java.lang.String, eu.europa.ec.opoce.cellar.cl.configuration.ConfigurationPropertyService.EXISTINGPROPS_MODE)
     */
    @Override
    @Transactional
    public String synchronizeProperty(final String propertyValue, final String propertyKey, final EXISTINGPROPS_MODE syncMode) {
        final ConfigurationProperty currentDatabaseProperty = this.getPropertyByKey(propertyKey);
        if (currentDatabaseProperty != null) {
            if (EXISTINGPROPS_MODE.FROM_DB.equals(syncMode)) {
                return currentDatabaseProperty.getPropertyValue();
            } else if (EXISTINGPROPS_MODE.TO_DB.equals(syncMode)) {
                this.checkDatabaseReadOnly();
                currentDatabaseProperty.setPropertyValue(propertyValue);
                this.updateProperty(currentDatabaseProperty);
                return propertyValue;
            } else {
                return propertyValue;
            }
        } else {
            this.checkDatabaseReadOnly();
            final ConfigurationProperty newDatabaseProperty = new ConfigurationProperty(propertyKey, propertyValue);
            this.createProperty(newDatabaseProperty);
            return propertyValue;
        }
    }

    /**
     * All methods annotated with @Transcational need to call this check first, otherwise they will
     * try to autocommit the transaction overcoming the CellarHibernateTemplate, hence generating a database access error. 
     */
    private void checkDatabaseReadOnly() throws CellarException {
        if (this.cellarConfiguration.isCellarDatabaseReadOnly()) {
            throw ExceptionBuilder.get(CellarConfigurationException.class).withCode(CommonErrors.READONLY_DATABASE).build();
        }
    }

}
