package eu.europa.ec.opoce.cellar.domain.content.mets;

import java.io.Serializable;
import java.util.List;

/**
 * This class is used for mapping the inbound transformation on a manifestation node. 
 * 
 * @author dcraeye
 *
 */
public class ManifestationTransformator implements Serializable {

    private static final long serialVersionUID = -2709191466877842700L;

    private String href;

    private List<ContentStream> contentStreamList;
    private List<Metadata> technicalMetadataList;

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public List<ContentStream> getContentStreamList() {
        return contentStreamList;
    }

    public void setContentStreamList(List<ContentStream> contentStreamList) {
        this.contentStreamList = contentStreamList;
    }

    public List<Metadata> getTechnicalMetadataList() {
        return technicalMetadataList;
    }

    public void setTechnicalMetadataList(List<Metadata> technicalMetadataList) {
        this.technicalMetadataList = technicalMetadataList;
    }

}
