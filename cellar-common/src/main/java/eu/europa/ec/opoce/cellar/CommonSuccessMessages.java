/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar
 *        FILE : SuccessCode.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 07-10-2015
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar;

import eu.europa.ec.opoce.cellar.MessageCode.TYPE;

/**
 * <class_description> Common success messages.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 07-10-2015
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public final class CommonSuccessMessages {

    private CommonSuccessMessages() {

    }

    public static final MessageCode AUDITABLE_START_MESSAGE = new MessageCode("auditable.start.message", TYPE.SUCCESS);
    public static final MessageCode AUDITABLE_END_MESSAGE = new MessageCode("auditable.end.message", TYPE.SUCCESS);
    public static final MessageCode AUDITABLE_FAIL_MESSAGE = new MessageCode("auditable.fail.message", TYPE.SUCCESS);
    public static final MessageCode USER_SUCCESSFULLY_AUTHENTICATED = new MessageCode("I-001", TYPE.SUCCESS);
}
