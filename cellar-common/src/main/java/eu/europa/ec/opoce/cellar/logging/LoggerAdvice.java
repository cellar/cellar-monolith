/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.logging
 *             FILE : LoggerAdvice.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 11 08, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-11-08 14:51:00 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.logging;

import com.google.common.base.Throwables;
import eu.europa.ec.opoce.cellar.bootstrap.CellarHooks;
import eu.europa.ec.opoce.cellar.logging.LogContext.Context;
import net.bytebuddy.agent.ByteBuddyAgent;
import net.bytebuddy.agent.builder.AgentBuilder;
import net.bytebuddy.asm.Advice;
import net.bytebuddy.description.annotation.AnnotationDescription;
import net.bytebuddy.description.annotation.AnnotationList;
import net.bytebuddy.description.method.ParameterDescription;
import net.bytebuddy.description.type.TypeDescription;
import net.bytebuddy.dynamic.DynamicType;
import net.bytebuddy.utility.JavaModule;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicBoolean;

import static net.bytebuddy.matcher.ElementMatchers.*;

/**
 * @author ARHS Developments
 * @since 7.6
 */
@Aspect
@Order(Ordered.HIGHEST_PRECEDENCE + 1)
@Component
public class LoggerAdvice {

    private static final Logger LOG = LogManager.getLogger(LoggerAdvice.class);
    private static final LoggerAdvice INSTANCE = new LoggerAdvice();
    public static final String CELLAR_CONTEXT = "CELLAR_CONTEXT";
    private static final String PACKAGE_PREFIX = "eu.europa.ec.opoce.cellar";
    private static final AtomicBoolean INIT = new AtomicBoolean(false);

    static {
        init();
    }

    /**
     * Intercepts the methods annotated with {@link LogContext} in order
     * to add configuration relative to the logging strategy.
     *
     * @param pjp        the {@link ProceedingJoinPoint}
     * @param logContext the {@link LogContext} annotation on the method
     * @return the object of the underlying method
     * @throws Throwable if something goes wrong in the underlying method
     */
    @Around("@annotation(logContext)")
    public Object intercept(ProceedingJoinPoint pjp, LogContext logContext) throws Throwable {
        Context ctx = logContext.value();
        String previousCellarContext = ThreadContext.get(CELLAR_CONTEXT);
        enter(ctx);
        try {
            return pjp.proceed();
        } finally {
            leave(previousCellarContext);
        }
    }

    /**
     * Before executing the method, that method is called to
     * set the configuration of the logging system.
     *
     * @param context the current logging context
     * @see Context
     */
    public static void enter(Context context) {
        if (context != Context.UNDEFINED) {
            ThreadContext.put(CELLAR_CONTEXT, context.name());
        }
        ThreadContext.put(CellarHooks.CELLAR_DEBUG_KEY, CellarHooks.getDebugContext());
    }

    /**
     *
     */
    public static void leave() {
        leave(null);
    }
    
    public static void leave(String previousCellarContext) {
        if (previousCellarContext == null) {
            ThreadContext.remove(CELLAR_CONTEXT);
        } else {
            // restore previous context
            ThreadContext.put(CELLAR_CONTEXT, previousCellarContext);
        }
    }

    /**
     * Non-Spring support. Activating load-time weaving in
     * spring to support more use cases produce unexpected
     * side effects to the current version (7.5).
     * ByteBuddy (http://bytebuddy.net) is used to implement
     * a hybrid solution to avoid these issues.
     */
    public static void init() {
        if (INIT.compareAndSet(false, true)) {
            ByteBuddyAgent.install();
            new AgentBuilder.Default()
                    // only current application, not the whole classpath
                    .with(new LoggerAdviceListener())
                    .with(AgentBuilder.RedefinitionStrategy.REDEFINITION)
                    .with(AgentBuilder.TypeStrategy.Default.REDEFINE)
                    .type(nameStartsWith(PACKAGE_PREFIX)
                            .and(isAnnotatedWith(LogContext.class))
                            .and(not(named(LoggerAdvice.class.getName())))
                            .and(not(named(LogContext.class.getName())))
                            .and(not(isAnnotatedWith(Service.class)))
                            .and(not(isAnnotatedWith(Component.class)))
                            .and(not(isAnnotatedWith(Controller.class)))
                            .and(not(isAnnotatedWith(Repository.class))))
                    .transform((builder, type, cl, mod) -> builder
                            .visit(Advice.withCustomMapping()
                                    .bind(new LogContextOffsetMappingFactory())
                                    .to(LoggerAdvice.class)
                                    .on(isAnnotatedWith(LogContext.class))))
                    .installOnByteBuddyAgent();
        }
    }

    @Advice.OnMethodEnter
    public static String before(@LogContext String ctx) {
    	String previousCellarContext = ThreadContext.get(CELLAR_CONTEXT);
        enter(LogContext.Context.valueOf(ctx));
        return previousCellarContext;
    }

    @Advice.OnMethodExit
    public static void after(@Advice.Enter String previousContext) {
        leave(previousContext);
    }

    static class LogContextOffsetMappingFactory implements Advice.OffsetMapping.Factory<LogContext> {

        @Override
        public Class<LogContext> getAnnotationType() {
            return LogContext.class;
        }

        @Override
        public Advice.OffsetMapping make(ParameterDescription.InDefinedShape target, AnnotationDescription.Loadable<LogContext> annotation,
                                         AdviceType adviceType) {
            return (instrumentedType, instrumentedMethod, assigner, context, sort) -> {
                AnnotationList annotations = instrumentedMethod.getDeclaredAnnotations();
                AnnotationDescription.Loadable<LogContext> ctx = annotations.ofType(LogContext.class);
                String value = ctx.load().value().name();
                return Advice.OffsetMapping.Target.ForStackManipulation.of(value);
            };
        }
    }

    static class LoggerAdviceListener implements AgentBuilder.Listener {

        @Override
        public void onDiscovery(String typeName, ClassLoader classLoader, JavaModule module, boolean loaded) {

        }

        @Override
        public void onTransformation(TypeDescription typeDescription, ClassLoader classLoader, JavaModule module, boolean loaded,
                                     DynamicType dynamicType) {
            LOG.debug("Transform {}", typeDescription);
        }

        @Override
        public void onIgnored(TypeDescription typeDescription, ClassLoader classLoader, JavaModule module, boolean loaded) {

        }

        @Override
        public void onError(String typeName, ClassLoader classLoader, JavaModule module, boolean loaded, Throwable throwable) {
            LOG.warn("Error when transforming {}: {}", typeName, Throwables.getStackTraceAsString(throwable));
        }

        @Override
        public void onComplete(String typeName, ClassLoader classLoader, JavaModule module, boolean loaded) {

        }
    }

    // Required aspectj
    @SuppressWarnings("unused")
    public static LoggerAdvice aspectOf() {
        return INSTANCE;
    }
}
