/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.virtuoso.dao
 *             FILE : VirtuosoExceptionMessagesDao.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 18 août 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.virtuoso.dao;

import eu.europa.ec.opoce.cellar.cl.dao.BaseDao;
import eu.europa.ec.opoce.cellar.cl.domain.virtuoso.VirtuosoExceptionMessages;

import java.util.List;

/**
 * The Interface VirtuosoExceptionMessagesDao.
 * <class_description> The dao class the accesses the repository to retrieve the virtuoso message objects
 * <br/><br/>
 * ON : 18 août 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface VirtuosoExceptionMessagesDao extends BaseDao<VirtuosoExceptionMessages, Long> {

    /**
     * Find by message.
     *
     * @param message the message
     * @return the list
     */
    List<VirtuosoExceptionMessages> findByMessage(String message);

}
