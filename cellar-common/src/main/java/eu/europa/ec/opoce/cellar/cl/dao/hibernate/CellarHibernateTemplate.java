package eu.europa.ec.opoce.cellar.cl.dao.hibernate;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.exception.CellarConfigurationException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.orm.hibernate5.HibernateCallback;
import org.springframework.orm.hibernate5.HibernateTemplate;

/**
 * <class_description> This Hibernate template checks if the database is in read-only mode before performing the operation:<br>
 * if so, and if the operation to be executed is a write operation, it does nothing but logging the attempt.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : dd-MM-YYYY
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class CellarHibernateTemplate extends HibernateTemplate {

    private static final Logger LOG = LoggerFactory.getLogger(CellarHibernateTemplate.class);

    private final boolean readOnly;

    /**
     * @param sessionFactory
     */
    public CellarHibernateTemplate(final SessionFactory sessionFactory, final boolean readOnly) {
        super(sessionFactory);
        this.readOnly = readOnly;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void checkWriteOperationAllowed(final Session session) throws InvalidDataAccessApiUsageException, CellarConfigurationException {
        if (this.readOnly) {
            throw ExceptionBuilder.get(CellarConfigurationException.class).withCode(CommonErrors.READONLY_DATABASE).build();
        }
        //when a new session is created the flush mode is set to MANUAL
        // when the flush mode is MANUAL the checkWriteOperationAllowed method from the parent throws an exception
        // super.checkWriteOperationAllowed(session);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected <T> T doExecute(HibernateCallback<T> action, boolean enforceNativeSession) throws DataAccessException {
        T result = null;

        try {
            result = super.doExecute(action, enforceNativeSession);
        } catch (final CellarConfigurationException e) {
            // this can only be a CellarConfigurationException informing that the Cellar is trying to execute a write operation in a read-only database
            LOG.debug(e.getMessage());
        }

        return result;
    }

}
