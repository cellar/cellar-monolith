/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.metadata
 *             FILE : IExistingMetadataLoader.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 19-03-2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.metadata;

import eu.europa.ec.opoce.cellar.exception.NotImplementedException;
import eu.europa.ec.opoce.cellar.exception.NotImplementedExceptionBuilder;

/**
 * <class_description> Definition of the service for loading existing data.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 19-03-2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IExistingMetadataLoader<IN, OUT, CACHE> {

    /**
     * It restarts the component.
     */
    default void restart() {
    }

    /**
     * It loads the existing direct model corresponding to the given {@code in} input data.
     *
     * @param in the input data.
     * @return the existing direct model.
     */
    default OUT loadDirect(final IN in) {
        throwNotImplementedException("loadDirect");
        return null;
    }

    /**
     * It loads the existing inferred model corresponding to the given {@code in} input data.
     *
     * @param in the input data.
     * @return the existing inferred model.
     */
    default OUT loadInferred(final IN in) {
        throwNotImplementedException("loadInferred");
        return null;
    }

    /**
     * It loads the existing direct and inferred model corresponding to the given {@code in} input data.
     *
     * @param in      the input data.
     * @param options possible options.
     * @return the existing direct and inferred model.
     */
    OUT loadDirectAndInferred(final IN in, final Object... options);

    /**
     * It loads the existing inverse model corresponding to the given {@code in} input data.
     *
     * @param in the input data.
     * @return @return the existing inverse model.
     */
    default OUT loadInverse(final IN in) {
        throwNotImplementedException("loadInverse");
        return null;
    }

    /**
     * It updates with data {@code cacheData} the loader's cache of object corresponding to the input data {@code in}.
     *
     * @param in        the input data.
     * @param cacheData the data to cache.
     */
    default void updateCache(final IN in, final CACHE cacheData) {
    }

    /**
     * It gets from the loader's cache the object corresponding to the input data {@code in}.
     *
     * @param in the input data.
     * @return the cached data to cache.
     */
    default CACHE getCache(final IN in) {
        return null;
    }

    /**
     * It evicts from the loader's cache the object corresponding to the input data {@code in}.
     *
     * @param in the input data of the object to evict.
     */
    default void evictCache(final IN in) {
    }

    static void throwNotImplementedException(final String methodName) throws NotImplementedException {
        throw NotImplementedExceptionBuilder.get().withMessage("Method [{}] is not implemented for this class.").withMessageArgs(methodName)
                .build();
    }

}
