/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.exception
 *        FILE : CellarConversionException.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 25-01-2016
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2016 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.exception;

/**
 * This is a {@link CellarConversionException} buildable by an {@link ExceptionBuilder}.</br>
 * </br> 
 * ON : 25-01-2016
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public class CellarConversionException extends CellarException {

    private static final long serialVersionUID = 620902338842664568L;

    /**
     * Constructs a new exception with its associated builder.
     * 
     * @param builder the builder to use for building the exception
     */
    public CellarConversionException(final ExceptionBuilder<? extends BuildableException> builder) {
        super(builder);
    }

}
