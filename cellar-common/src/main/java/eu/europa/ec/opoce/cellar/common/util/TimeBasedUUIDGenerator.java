/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.common.util
 *        FILE : UUIDTimeBasedGenerator.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 06-11-2014
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.util;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.IS3Configuration;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;

import java.io.File;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.Random;
import java.util.UUID;

import com.fasterxml.uuid.EthernetAddress;
import com.fasterxml.uuid.UUIDTimer;
import com.fasterxml.uuid.ext.FileBasedTimestampSynchronizer;
import com.fasterxml.uuid.impl.TimeBasedGenerator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * <class_description> Time-based UUID generator for CELLAR ids.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 06-11-2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public enum TimeBasedUUIDGenerator {

    INSTANCE;

    private static final Logger LOG = LogManager.getLogger(TimeBasedUUIDGenerator.class);

    private static final ICellarConfiguration CELLAR_CONFIGURATION = ServiceLocator.getService("cellarConfiguration",
            ICellarConfiguration.class);

    private static final IS3Configuration S3_CONFIGURATION = ServiceLocator.getService("cellarConfiguration",
            IS3Configuration.class);

    private FileBasedTimestampSynchronizer fileBasedTimestampSynchronizer;

    /** default multicast address for time based UUID generation */
    private final EthernetAddress ethernetAddress = new EthernetAddress("01:aa:75:ed:71:a1");

    private TimeBasedGenerator timeBasedGenerator;

    public String generate(final TYPE sipType) {
        String uuid = null;

        if (TYPE.BULK.equals(sipType) || TYPE.BULK_LOWPRIORITY.equals(sipType)) {
            uuid = S3_CONFIGURATION.getCellarNamespace() + ":" + UUID.randomUUID().toString();
        } else {
            uuid = TimeBasedUUIDGenerator.INSTANCE.generate();
        }
        if(LOG.isDebugEnabled()) {
        	LOG.debug(IConfiguration.INGESTION, "UUID {} generated for type={}, prefix={}", uuid, sipType,
                    S3_CONFIGURATION.getCellarNamespace());
        }
        return uuid;
    }

    private synchronized String generate() {
        final UUID id = this.getTimeBasedGenerator().generate();
        return S3_CONFIGURATION.getCellarNamespace() + ":" + id.toString();
    }

    /**
     * Lazy init TimeBasedGenerator.
     * The com.fasterxml.uuid.impl.TimeBasedGenerator classes encure that the instances are thread safe.
     * @return the TimeBasedGenerator
     */
    private TimeBasedGenerator getTimeBasedGenerator() {
        if (this.timeBasedGenerator == null) {
            try {
                final Random random = new SecureRandom();
                this.timeBasedGenerator = new TimeBasedGenerator(this.ethernetAddress,
                        new UUIDTimer(random, this.getFileBasedTimestampSynchronizer()));
            } catch (final IOException e) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.UUID_GENERATION_FAILED)
                        .withMessage("An unexpected error occurred during the instantiation of the time-based UUID generator").withCause(e)
                        .build();
            }
        }
        return this.timeBasedGenerator;
    }

    private FileBasedTimestampSynchronizer getFileBasedTimestampSynchronizer() {
        if (this.fileBasedTimestampSynchronizer == null) {
            final String lockFolder = CELLAR_CONFIGURATION.getCellarFolderLock();
            try {
                final File lock1 = new File(lockFolder, "timeBasedUUIDGeneration_lock1");
                final File lock2 = new File(lockFolder, "timeBasedUUIDGeneration_lock2");
                this.fileBasedTimestampSynchronizer = new FileBasedTimestampSynchronizer(lock1, lock2);
            } catch (final IOException e) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.UUID_GENERATION_FAILED)
                        .withMessage("An unexpected error occurred during the instantiation of the file-based timestamp synchronizer")
                        .withCause(e).build();
            } finally {

            }
        }
        return this.fileBasedTimestampSynchronizer;
    }

}
