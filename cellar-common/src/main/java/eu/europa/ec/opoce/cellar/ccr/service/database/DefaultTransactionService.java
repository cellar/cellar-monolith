/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *        FILE : DefaultTransactionService.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 15-03-2013
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.service.database;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * <class_description> This class holds the default configuration for transactional classes.<br/>
 * Each class that aims to be transactional, MUST extends this class in order to inherit the default transaction configuration.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 15-03-2013
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public abstract class DefaultTransactionService {

}
