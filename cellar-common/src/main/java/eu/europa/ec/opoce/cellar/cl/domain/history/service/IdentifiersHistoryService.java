/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : CellarIdentifier.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 8 mars 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.history.service;

import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;

/**
 * The Interface IdentifiersHistoryService.
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 8 mars 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IdentifiersHistoryService {

    /**
     * Delete.
     *
     * @param id the id
     */
    void delete(final Long id);

    /**
     * Generate cellar id.
     *
     * @param digitalObject the digital object
     * @param parentId the parent id
     * @param cardinal the cardinal
     * @param sipType the sip type
     * @return the string
     */
    String generateCellarID(final DigitalObject digitalObject, final String parentId, final Integer cardinal, final TYPE sipType);

}
