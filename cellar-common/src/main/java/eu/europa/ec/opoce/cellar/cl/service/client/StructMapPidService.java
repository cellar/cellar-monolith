package eu.europa.ec.opoce.cellar.cl.service.client;

import eu.europa.ec.opoce.cellar.cl.domain.structmap_pid.StructMapPid;

/**
 * <class_description> Service interface for logging entries in the 'STRUCTMAP_PID' table.
 *
 * ON : 16-06-2023
 *
 * @author EUROPEAN DYNAMICS S.A.
 */
public interface StructMapPidService {
    
    /**
     * Save an instance of {@link StructMapPid}.
     *
     * @return corresponding {@link StructMapPid} that was saved (or the null in case of exception).
     */
    StructMapPid saveEntry(StructMapPid structMapPid);
}
