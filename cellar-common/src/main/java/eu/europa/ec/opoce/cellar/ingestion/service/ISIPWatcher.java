/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.service.client
 *        FILE : ISIPWatcher.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 13-03-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ingestion.service;

import eu.europa.ec.opoce.cellar.ingestion.SIPWork;

import java.util.concurrent.ConcurrentMap;

/**
 * <class_description> Interface for the services meant to periodically poll the authenticOJ, daily and bulk reception folder for ingesting new SIPs.
 * <br/><br/>
 * <notes> 
 * <br/><br/>
 * ON : 02-04-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public interface ISIPWatcher {

    /**
     * Gets the treatment queue.
     *
     * @return the treatment queue
     */
    ConcurrentMap<SIPWork, Boolean> getTreatments();

    /**
     * Checks if is treatment queue empty.
     *
     * @return true, if is treatment queue empty
     */
    boolean isTreatmentSetEmpty();

    /**
     * Reconfigure thread pool executor.
     * The properties have been changed so the watcher needs to reset the thread pool executor properties
     */
    void reconfigureThreadPoolExecutor();

    /**
     * Watch folders.
     */
    void watchFolders();

}
