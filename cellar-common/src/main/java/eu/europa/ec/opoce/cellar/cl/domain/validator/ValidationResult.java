package eu.europa.ec.opoce.cellar.cl.domain.validator;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * This class contains the validation result.
 * 
 * We retrieve the validation status : valid or not.
 * When validation has failed, the status code can be used to identify the validation error type.
 * The list of messages allow to return human readable validation errors. 
 * 
 * @author dcraeye
 *
 */
public class ValidationResult implements Serializable {

    /** the class's serial. */
    private static final long serialVersionUID = -1313486637437056807L;

    private boolean valid = true;
    private int statusCode = 0;
    private List<String> messages = new LinkedList<String>();

    public ValidationResult() {
        super();
    }

    /**
     * Check that the validation has processed a valid object.
     * @return true if the object to validate is valid, false otherwise. 
     */
    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void setMessages(List<String> messages) {
        this.messages = messages;
    }

    public void addMessage(String message) {
        messages.add(message);
    }

}
