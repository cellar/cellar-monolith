package eu.europa.ec.opoce.cellar.cl.domain.enums;

/**
 * <class_description> Available statuses for PACKAGE_HISTORY and STRUCTMAP_STATUS_HISTORY tables
 *                     of CELLAROWNER schema.
 * <br/><br/>
 * <notes> Describes the status of the respective process.
 * <br/><br/>
 * ON : 13-06-2023
 *
 * @author EUROPEAN DYNAMICS S.A.
 */
public enum ProcessStatus {
    
    /**
     * Available statuses
     */
    NA("Not available"),
    W("Waiting"),
    R("Running"),
    S("Success"),
    F("Failed");
    
    /**
     * Explanation of status
     */
    private final String explanation;
    
    /**
     * Enum constructor
     */
    ProcessStatus(String explanation){
        this.explanation = explanation;
    }
    
    /**
     * Getter - explanation
     */
    public String getExplanation() {
        return this.explanation;
    }
}
