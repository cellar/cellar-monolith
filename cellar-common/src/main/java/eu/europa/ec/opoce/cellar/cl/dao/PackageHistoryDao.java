package eu.europa.ec.opoce.cellar.cl.dao;

import eu.europa.ec.opoce.cellar.cl.domain.package_history.PackageHistory;

import java.util.Date;

/**
 * Dao for the Entity {@link PackageHistory}.
 *
 * @author EUROPEAN DYNAMICS S.A.
 *
 */
public interface PackageHistoryDao extends BaseDao<PackageHistory,Long>{
    
    /**
     * Return a persistent instance of {@link PackageHistory} based on its METS_PACKAGE_NAME and RECEIVED_DATE.
     *
     * @param metsPackageName the name of the METS package to look for.
     * @param receivedDate the timestamp this package was received
     *
     * @return the Package History reference matching the provided arguments.
     */
    PackageHistory getByPackageNameAndDate(String metsPackageName, Date receivedDate);
}
