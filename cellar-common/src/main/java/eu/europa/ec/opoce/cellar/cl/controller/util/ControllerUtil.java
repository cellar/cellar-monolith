package eu.europa.ec.opoce.cellar.cl.controller.util;

import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

import static javax.ws.rs.core.HttpHeaders.CONTENT_TYPE;

public final class ControllerUtil {

    private static final Logger LOG = LogManager.getLogger(ControllerUtil.class);

    public static final String RESPONSE_HEADER_DATE_FORMAT = "EEE, dd MMM yyyy HH:mm:ss zzz";
    private static final String ENCODING = "UTF-8";

    public static void setHttpHeaders(final HttpServletResponse httpServletResponse, final HttpHeaders httpHeaders) {
        if (httpHeaders != null) {
            for (Entry<String, List<String>> entry : httpHeaders.entrySet()) {
                httpServletResponse.setHeader(entry.getKey(), StringUtils.join(entry.getValue(), ','));
            }
        }
    }

    /**
     * Makes an xml error response from the given cause
     *
     * @param cause
     *            The {@link Throwable} to convert into a REST response
     * @return a {@link ResponseEntity}
     */
    public static ResponseEntity<String> makeErrorResponse(Throwable cause) {
        return makeErrorResponse(cause, HttpStatus.NOT_IMPLEMENTED);
    }

    /**
     * Makes an xml error response from the given cause
     *
     * @param cause
     *            The {@link Throwable} to convert into a REST response
     * @param status
     *            The {@link HttpStatus} status for this response.
     * @return a {@link ResponseEntity}
     */
    public static ResponseEntity<String> makeErrorResponse(Throwable cause, HttpStatus status) {
        ThrowableToXmlErrorConvertor exceptionToXml = new ThrowableToXmlErrorConvertor();
        String body = exceptionToXml.convert(status.value(), cause);
        HttpHeaders headers = new HttpHeaders();
        headers.add(CONTENT_TYPE, MediaType.APPLICATION_XML + ";charset=" + ENCODING);
        return new ResponseEntity<String>(body, headers, status);
    }

    /**
     * Makes an xml error response from the given cause
     *
     * @param cause
     *            The {@link Throwable} to convert into a REST response
     * @param status
     *            The {@link HttpStatus} status for this response.
     * @return a {@link ResponseEntity}
     */
    public static ResponseEntity<Object> makeErrorResponseObject(Throwable cause, HttpStatus status) {
        ThrowableToXmlErrorConvertor exceptionToXml = new ThrowableToXmlErrorConvertor();
        String body = exceptionToXml.convert(status.value(), cause);
        HttpHeaders headers = new HttpHeaders();
        headers.add(CONTENT_TYPE, MediaType.APPLICATION_XML + ";charset=" + ENCODING);
        return new ResponseEntity<Object>(body, headers, status);
    }

    /**
     * Makes an JSON error response from the given cause
     *
     * @param cause The {@link Throwable} to convert into a REST response
     * @param status The {@link HttpStatus} status for this response.
     * @return a {@link ResponseEntity}
     */
    public static ResponseEntity<String> makeJsonErrorResponse(Throwable cause, HttpStatus status) {
        ThrowableToJsonErrorConvertor exceptionToJson = new ThrowableToJsonErrorConvertor();
        String body = exceptionToJson.convert(status, cause);
        HttpHeaders headers = new HttpHeaders();
        headers.add(CONTENT_TYPE, MediaType.APPLICATION_JSON + ";charset=" + ENCODING);
        return new ResponseEntity<>(body, headers, status);
    }

    /**
     * Makes an xml response from the given message.
     *
     * @param message
     *            the message to put in the response
     * @return a {@link ResponseEntity}
     */
    public static ResponseEntity<String> makeXMLResponse(String xml) {
        HttpStatus status = HttpStatus.OK;
        HttpHeaders headers = new HttpHeaders();
        headers.add(CONTENT_TYPE, MediaType.APPLICATION_XML + ";charset=" + ENCODING);
        return new ResponseEntity<String>(xml, headers, status);
    }

    /**
     * Makes an InputStream response from the given message.
     *
     * @param message
     *            the message to put in the response
     * @return a {@link ResponseEntity}
     */
    public static ResponseEntity<InputStream> makeInputStreamResponse(InputStream stream) {
        HttpStatus status = HttpStatus.OK;
        HttpHeaders headers = new HttpHeaders();
        headers.add(CONTENT_TYPE, MediaType.APPLICATION_XHTML_XML + ";charset=" + ENCODING);
        return new ResponseEntity<InputStream>(stream, headers, status);
    }

    /**
     * Makes an xml response from the given message.
     *
     * @param message
     *            the message to put in the response
     * @return a {@link ResponseEntity}
     */
    public static ResponseEntity<String> makeResponse(String message) {
        HttpStatus status = HttpStatus.OK;
        HttpHeaders headers = new HttpHeaders();
        headers.add(CONTENT_TYPE, MediaType.APPLICATION_XML + ";charset=" + ENCODING);
        String body = String.format("<?xml version=\"1.0\" encoding=\"UTF-8\"?><success>%s</success>", message);
        return new ResponseEntity<String>(body, headers, status);
    }

    /**
     * Builds and returns a response entity containing <code>byte[]</code>. Useful for displaying/downloading streams.
     *
     * @param bytes the body content
     * @param contentType the content type to set as <code>Content-Type</code> header; if <code>null</code> or empty, it defaults to {@link MediaType.APPLICATION_OCTET_STREAM}
     * @param inline if true, the <code>Content-Disposition</code> header is set as <code>inline</code>, otherwise as <code>attachment</code>
     * @param attachmentFilename if not <code>null</code>, <code>; filename=&lt;attachmentFilename&gt;</code> is added to the <code>Content-Disposition</code> header
     * @return the built response entity
     */
    public static ResponseEntity<byte[]> makeBytesResponse(final byte[] bytes, final String contentType, final boolean inline,
            final String attachmentFilename) {
        final HttpHeaders headers = new HttpHeaders();

        // sets the content type
        String myContentType = contentType;
        if (myContentType == null || myContentType.length() == 0) {
            myContentType = MediaType.APPLICATION_OCTET_STREAM.toString();
        }
        headers.add(CONTENT_TYPE, myContentType);

        // sets content disposition
        String contentDispositionValue = inline ? "inline" : "attachment";
        if (attachmentFilename != null) {
            contentDispositionValue += "; filename=" + attachmentFilename;
        }
        headers.add("Content-Disposition", contentDispositionValue);

        // adds other headers
        headers.add("Content-Length", "" + bytes.length);

        // returns
        return new ResponseEntity<byte[]>(bytes, headers, HttpStatus.OK);
    }

    /**
     * Create the export Rest API response.
     *
     * @param file, the file where the exported resource exists temporarily.
     * @param contentType, the content type to be set in the response's header
     * @param inline, flag indicating if the Content-Disposition header is inline or attachment
     * @param attachmentFilename, the name of the file attached in the response
     * @return ResponseEntity, a response streaming the file
     */
    public static ResponseEntity<InputStreamResource> makeResponse(final File file, final String contentType, final boolean inline,
            final String attachmentFilename) {
        final HttpHeaders headers = new HttpHeaders();

        // sets the content type
        String myContentType = contentType;
        if (myContentType == null || myContentType.length() == 0) {
            myContentType = MediaType.APPLICATION_OCTET_STREAM.toString();
        }
        headers.add(CONTENT_TYPE, myContentType);

        // sets content disposition
        String contentDispositionValue = inline ? "inline" : "attachment";
        if (attachmentFilename != null) {
            contentDispositionValue += "; filename=" + attachmentFilename;
        }
        headers.add("Content-Disposition", contentDispositionValue);

        // adds other headers
        headers.add("Content-Length", "" + file.length());

        try {
            FileInputStream fis=new FileInputStream(file);
        InputStreamResource resource = new InputStreamResource(fis);

        // returns
        return new ResponseEntity<InputStreamResource>(resource, headers, HttpStatus.OK);
        } catch (Exception e)
        {
            String messagge = "Error in response creation; "+e.getMessage();
            LOG.error(messagge, e);
            return new ResponseEntity<InputStreamResource>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Converts a response entity containing a <code>String</code> to a response entity containing <code>byte[]</code>
     *
     * @param stringResponseEntity the source response entity containing a <code>String</code>
     * @return the destination response entity containing <code>byte[]</code>
     */
    public static ResponseEntity<byte[]> toBytesResponseEntity(final ResponseEntity<String> stringResponseEntity) {
        return new ResponseEntity<byte[]>(stringResponseEntity.getBody().getBytes(), stringResponseEntity.getHeaders(),
                stringResponseEntity.getStatusCode());
    }

    /**
     * Converts a response entity containing <code>byte[]</code> to a response entity containing a <code>String</code>
     *
     * @param bytesResponseEntity the source response entity containing <code>byte[]</code>
     * @return the destination response entity containing a <code>String</code>
     */
    public static ResponseEntity<String> toStringResponseEntity(final ResponseEntity<byte[]> bytesResponseEntity) {
        return new ResponseEntity<String>(new String(bytesResponseEntity.getBody()), bytesResponseEntity.getHeaders(),
                bytesResponseEntity.getStatusCode());
    }

    /**
     * Set the last modification date to the response header. If the given lastModified parameter is
     * null, set the begin date (01/01/1970)
     *
     * @param response
     *            the response object.
     * @param lastModified
     *            the date to set.
     */
    public static void setLastModifiedDateResponse(HttpServletResponse response, Date lastModified) {
        SimpleDateFormat sdf = new SimpleDateFormat(RESPONSE_HEADER_DATE_FORMAT);
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        if (lastModified == null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(0L);
            lastModified = calendar.getTime();
        }
        response.setHeader("Last-Modified", sdf.format(lastModified));
    }

    /**
     * Set the expired date to the response header. If the given expiredDate parameter is null, set
     * the current date.
     *
     * @param response
     *            the response object.
     * @param lastModified
     *            the date to set.
     */
    public static void setExpiredDateResponse(HttpServletResponse response, Date expiredDate) {
        SimpleDateFormat sdf = new SimpleDateFormat(ControllerUtil.RESPONSE_HEADER_DATE_FORMAT);
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        if (expiredDate == null) {
            expiredDate = Calendar.getInstance(TimeZone.getTimeZone("GMT-0")).getTime();
        }
        response.setHeader("Expires", sdf.format(expiredDate));
    }

    public static void setResponseError(HttpServletResponse response, Throwable throwable, HttpStatus status) {
        setXMLResponseError(response, new ThrowableToXmlErrorConvertor().convert(status.value(), throwable), status);
    }

    public static void setXMLResponseError(HttpServletResponse response, String message, HttpStatus status) {
        response.setHeader("Content-Type", "application/xml;charset=" + ENCODING);
        response.setStatus(status.value());
        try {
            response.getOutputStream().write(message.getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
    }

    /**
     * Get the Jar files and their release number.
     *
     * @return The list of Jar files and their version.
     * @throws IOException
     *             Raised if Jar file manifest cannot be read.
     */
    public static List<Map<String, String>> getJarVersions() throws IOException {
        List<Map<String, String>> packages = new ArrayList<Map<String, String>>();

        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        URL[] urls = ((URLClassLoader) cl).getURLs();
        List<String> packageAlreadTreated = new LinkedList<String>();
        for (URL url : urls) {
            String urlname = url.getFile();
            if (StringUtils.startsWith(StringUtils.substringAfterLast(urlname, "/"), ContentIdentifier.CELLAR_PREFIX)) {
                String jarFileName = StringUtils.substringBefore(StringUtils.substringAfterLast(urlname, "/"), ".jar") + ".jar";
                if (!packageAlreadTreated.contains(jarFileName)) {
                    packageAlreadTreated.add(jarFileName);

                    Manifest manifest;
                    try (JarFile jarFile = new JarFile(urlname)) {
                        manifest = jarFile.getManifest();
                    }

                    Map<String, String> manifestationMap = new HashMap<String, String>();
                    manifestationMap.put("name", jarFileName);
                    String revision = null;
                    if (manifest != null) {
                        revision = manifest.getMainAttributes().getValue("Implementation-Version");
                    }
                    if (revision == null || revision.trim().length() == 0) {
                        revision = "unknown revision";
                    }
                    manifestationMap.put("revision", revision);
                    packages.add(manifestationMap);
                }
            }
        }
        return packages;
    }

    /**
     * Convert http code (integer) in http status.
     * @param httpCode http code integer
     * @return http status object
     */
    public static HttpStatus resolveHttpStatus(final int httpCode) {
        HttpStatus retStatus = null;

        try {
            retStatus = HttpStatus.valueOf(httpCode);
        } catch (IllegalArgumentException e) {
            LOG.warn("Unknown HTTP status '" + httpCode + "' returned from call to Cellar APIs. Defaulting to '500 Internal Server Error'.");
            retStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }

        return retStatus;
    }
}
