/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.configuration.impl
 *        FILE : NullConfigurationPropertyServiceImpl.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 14-06-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.configuration.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.ConfigurationPropertyService;
import eu.europa.ec.opoce.cellar.cl.domain.admin.ConfigurationProperty;
import eu.europa.ec.opoce.cellar.cl.service.client.CommonConfigurationPropertyService;

import java.util.ArrayList;
import java.util.Collection;

/**
 * <class_description> Null implementation of {@link ConfigurationPropertyService}.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 14-06-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public class NullConfigurationPropertyServiceImpl implements ConfigurationPropertyService, CommonConfigurationPropertyService {

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.client.CommonConfigurationPropertyService#getPropertyValue(java.lang.String)
     */
    @Override
    public String getPropertyValue(String key) {
        return null;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.configuration.ConfigurationPropertyService#createProperty(eu.europa.ec.opoce.cellar.cl.domain.admin.ConfigurationProperty)
     */
    @Override
    public void createProperty(ConfigurationProperty property) {
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.configuration.ConfigurationPropertyService#deleteProperty(eu.europa.ec.opoce.cellar.cl.domain.admin.ConfigurationProperty)
     */
    @Override
    public void deleteProperty(ConfigurationProperty property) {
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.configuration.ConfigurationPropertyService#deleteProperties(java.util.Collection)
     */
    @Override
    public void deleteProperties(Collection<ConfigurationProperty> properties) {
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.configuration.ConfigurationPropertyService#getProperties()
     */
    @Override
    public Collection<ConfigurationProperty> getProperties() {
        return new ArrayList<ConfigurationProperty>();
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.configuration.ConfigurationPropertyService#getPropertyById(java.lang.Long)
     */
    @Override
    public ConfigurationProperty getPropertyById(Long id) {
        return null;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.configuration.ConfigurationPropertyService#getPropertyByKey(java.lang.String)
     */
    @Override
    public ConfigurationProperty getPropertyByKey(String key) {
        return null;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.configuration.ConfigurationPropertyService#isPropertyExists(eu.europa.ec.opoce.cellar.cl.domain.admin.ConfigurationProperty)
     */
    @Override
    public boolean isPropertyExists(ConfigurationProperty configurationProperty) {
        return false;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.configuration.ConfigurationPropertyService#updateProperty(eu.europa.ec.opoce.cellar.cl.domain.admin.ConfigurationProperty)
     */
    @Override
    public void updateProperty(ConfigurationProperty configurationProperty) {
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.configuration.ConfigurationPropertyService#synchronizeProperty(java.lang.String, java.lang.String, eu.europa.ec.opoce.cellar.cl.configuration.ConfigurationPropertyService.EXISTINGPROPS_MODE)
     */
    @Override
    public String synchronizeProperty(String propertyValue, String propertyKey, EXISTINGPROPS_MODE syncMode) {
        return null;
    }

}
