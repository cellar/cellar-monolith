/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.logging
 *             FILE : LogContext.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 11 08, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-11-08 11:09:24 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.logging;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation is designed to set a logging context to the current
 * method. The logging context will associate to the current thread the
 * value of the annotation. The logging context is thread local which
 * means that this annotation is required each time you switch to another
 * thread.
 * <p>
 * Even if {@link ElementType#PARAMETER} and {@link ElementType#TYPE} is
 * a valid target, that target is only use internally by {@link LoggerAdvice}
 * and has no effect elsewhere.
 * <p>
 * Non-spring beans must be annotated with @{@link LogContext} on class
 * and the methods to make it work.
 * Technically speaking it is perfectly feasible to put only the annotation
 * on the methods but the startup of the application will be much slower.
 *
 * @author ARHS Developments
 * @since 7.6
 */
@Documented
@Target({ElementType.METHOD, ElementType.PARAMETER, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface LogContext {

    Context value() default Context.UNDEFINED;

    enum Context {
        BATCH_PROCESSING,
        CMR_ADMIN,
        CMR_CONCURRENCY,
        CMR_DISSEMINATION,
        CMR_EMBARGO,
        CMR_EUROVOC_API,
        CMR_INDEXING,
        CMR_INGESTION,
        CMR_LICENSE_HOLDER,
        CMR_METS_EXPORT,
        CMR_MODEL_LOAD,
        CMR_NAL_API,
        CMR_NAL_LOAD,
        CMR_SPARQL_LOAD,
        CMR_RE_INDEXING,
        CMR_TMD_VALIDATION,
        CMR_SHACL,
        CMR_VIRTUOSO,
        HIERARCHY_ALIGNER,
        HIERARCHY_ALIGNER_CLEANER,
        RDF_STORE_CLEANER,
        SPARQL_EXECUTING,
        SIP_DEPENDENCY_CHECKER,
        SIP_QUEUE_MANAGER,
        CACHE_STATISTICS,
        UNDEFINED
    }
}
