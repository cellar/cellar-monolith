package eu.europa.ec.opoce.cellar.ingestion;

public enum SIPState {

    RUNNING,
    VALIDATION_FAILED,
    SIP_FILE_REMOVE_FAILED,
    SUCCESS,
    FAILURE

}
