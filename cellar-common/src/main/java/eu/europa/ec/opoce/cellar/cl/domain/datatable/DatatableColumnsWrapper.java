/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.datatable
 *             FILE : DatatableColumnsWrapper.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 07-Mar-2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.datatable;

/**
 * The Class DatatableColumnsWrapper.
 *
 * @author ARHS Developments
 * @version $$Revision$$$
 */
public class DatatableColumnsWrapper {

    /** The columns. */
    private DatatableColumn[] columns;

    /** The sort by column index. */
    private int sortByColumnIndex;

    /** The sort order. */
    private String sortOrder;

    /** The table name. */
    private String tableName;

    /**
     * Gets the columns.
     *
     * @return the columns
     */
    public DatatableColumn[] getColumns() {
        return this.columns;
    }

    /**
     * Sets the columns.
     *
     * @param columns the new columns
     */
    public void setColumns(final DatatableColumn[] columns) {
        this.columns = columns;
    }

    /**
     * Gets the table name.
     *
     * @return the tableName
     */
    public String getTableName() {
        return tableName;
    }

    /**
     * Sets the table name.
     *
     * @param tableName the tableName to set
     */
    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    /**
     * Gets the sort by column index.
     *
     * @return the sort by column index
     */
    public int getSortByColumnIndex() {
        return this.sortByColumnIndex;
    }

    /**
     * Sets the sort by column index.
     *
     * @param sortByColumnIndex the new sort by column index
     */
    public void setSortByColumnIndex(final int sortByColumnIndex) {
        this.sortByColumnIndex = sortByColumnIndex;
    }

    /**
     * Gets the sort order.
     *
     * @return the sort order
     */
    public String getSortOrder() {
        return this.sortOrder;
    }

    /**
     * Sets the sort order.
     *
     * @param sortOrder the new sort order
     */
    public void setSortOrder(final String sortOrder) {
        this.sortOrder = sortOrder;
    }

}
