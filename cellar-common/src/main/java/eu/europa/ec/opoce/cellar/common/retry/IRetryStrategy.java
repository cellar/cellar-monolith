/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.jena.oracle
 *             FILE : RetryAwareConnection.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Sep 6, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.retry;

import eu.europa.ec.opoce.cellar.common.closure.PClosure;
import eu.europa.ec.opoce.cellar.common.closure.RPClosure;
import eu.europa.ec.opoce.cellar.exception.CellarException;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Sep 6, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IRetryStrategy<P> {

    /**
     * <p>Retry.</p>
     *
     * @param connectionCode a {@link RPClosure} object.
     */
    <C extends PClosure<P>> void retry(final C connectionCode) throws CellarException;

    /**
     * Sets the maximum number of attempts.
     * 
     * @param maxAttempts the maximum number of attempts
     * @return the instance of this retry strategy
     */
    IRetryStrategy<P> maxAttempts(final int maxAttempts);

    /**
     * Sets the ceiling delay, that is, the time that - once elapsed - the retries will stop.
     * 
     * @param ceilingDelay the ceiling delay
     * @return the instance of this retry strategy
     */
    IRetryStrategy<P> ceilingDelay(final long ceilingDelay);

    /**
     * Sets the base of the retry formula.
     * 
     * @param base the base of the retry formula
     * @return the instance of this retry strategy
     */
    IRetryStrategy<P> base(final int base);

    /**
     * Sets the retry-aware connection to execute.
     * 
     * @param retryAwareConnection the retry-aware connection to execute
     * @return the instance of this retry strategy
     */
    IRetryStrategy<P> retryAwareConnection(final IRetryAwareConnection<P> retryAwareConnection);

    /**
     * Sets the name of the recipient towards which the retry strategy is meant to operate: useful for logging purposes.
     * 
     * @param recipientName the name of the recipient
     * @return the instance of this retry strategy
     */
    IRetryStrategy<P> recipientName(final String recipientName);

}
