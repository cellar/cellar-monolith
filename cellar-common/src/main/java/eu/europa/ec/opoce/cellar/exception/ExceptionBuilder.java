/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.exception
 *        FILE : ExceptionBuilder.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 30-01-2012
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.exception;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.MessageCode;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditBuilder;
import eu.europa.ec.opoce.cellar.common.MessageFormatter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.support.ResourceBundleMessageSource;

import java.util.Arrays;
import java.util.Locale;

/**
 * This is the exception builder meant to build an instance of
 * {@link BuildableException}.</br>
 * </br>
 * ON : 30-01-2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class ExceptionBuilder<EXC extends BuildableException> {

    private transient static final Logger LOGGER = LoggerFactory.getLogger(ExceptionBuilder.class);

    private static final MessageCode DEFAULT_ERROR_CODE = CommonErrors.CELLAR_INTERNAL_ERROR;

    private static ResourceBundleMessageSource ems = new ResourceBundleMessageSource() {

        {
            setBasename("messages.exceptions");
        }
    };

    private Class<EXC> exceptionClass;

    /**
     * The exception's error code.
     *
     * @see {@link MessageCode}
     */
    protected MessageCode code;

    /**
     * The exception's internationalized header.<br>
     * This header is internationalized out of the properties messages
     * configured by property {@code ems}.
     */
    protected String header;

    /**
     * The exception's message.
     */
    protected String message;

    /**
     * The message's arguments.
     */
    protected Object[] messageArgs;

    /**
     * The exception's METS document id.
     */
    protected String metsDocumentId;

    /**
     * The exception's cause.
     */
    protected Throwable cause;

    /**
     * If true, the exception is set as silent.
     */
    protected boolean silent;

    /**
     * If true, the exception is set as visible on the stack trace.
     */
    protected boolean visibleOnStackTrace;

    /**
     * If true, the exception is considered the main cause of the stack.<br>
     * If it has a cause, then that cause is treated as informative only, which
     * means that it does not participate to the stack's severity.
     */
    protected boolean root;

    /**
     * The audit builder.
     */
    protected AuditBuilder auditBuilder;

    /**
     * Gets an instance of the exception builder for the exception of class {@code exceptionClass}.
     *
     * @param exceptionClass the exception to get
     * @param <EXC>          the class of the exception to get
     * @return an instance of the exception builder
     */
    public static <EXC extends BuildableException> ExceptionBuilder<EXC> get(final Class<EXC> exceptionClass) {
        return new ExceptionBuilder<>(exceptionClass);
    }

    /**
     * Convenience method for building an exception of class {@code exceptionClass} and throwing it right away.
     *
     * @param exceptionClass the exception to throw
     * @param <EXC>          the class of the exception to throw
     * @param conditions     throws the exception if at least one of these conditions is true
     * @throws EXC the built exception
     */
    public static <EXC extends BuildableException> void throwExc(final Class<EXC> exceptionClass, final Boolean... conditions) {
        if (conditions == null || conditions.length == 0) {
            throw get(exceptionClass).build();
        }
        for (Boolean condition : conditions) {
            if (condition != null && condition) {
                throw get(exceptionClass).build();
            }
        }
    }

    /**
     * Set private to avoid direct instantiation: any instantiation should come
     * from static method
     * {@code eu.europa.ec.opoce.cellar.exception.ExceptionBuilder.get}
     */
    private ExceptionBuilder() {
        this.silent = false;
        this.visibleOnStackTrace = true;
        this.root = false;
    }

    protected ExceptionBuilder(final Class<EXC> exceptionClass) {
        this();
        this.exceptionClass = exceptionClass;
    }

    public ExceptionBuilder<EXC> withCode(final MessageCode code) {
        this.code = code;
        return this;
    }

    public ExceptionBuilder<EXC> withMessage(final String message) {
        this.message = message;
        return this;
    }

    public ExceptionBuilder<EXC> withMessageArgs(final Object... messageArgs) {
        this.messageArgs = messageArgs;
        return this;
    }

    public ExceptionBuilder<EXC> withMetsDocumentId(final String metsDocumentId) {
        this.metsDocumentId = metsDocumentId;
        return this;
    }

    public ExceptionBuilder<EXC> withCause(final Throwable cause) {
        this.cause = cause;
        return this;
    }

    public ExceptionBuilder<EXC> withAuditBuilder(final AuditBuilder auditBuilder) {
        this.auditBuilder = auditBuilder;
        return this;
    }

    public ExceptionBuilder<EXC> silent(final boolean silent) {
        this.silent = silent;
        return this;
    }

    public ExceptionBuilder<EXC> visibleOnStackTrace(final boolean visibleOnStackTrace) {
        this.visibleOnStackTrace = visibleOnStackTrace;
        return this;
    }

    public ExceptionBuilder<EXC> root(final boolean root) {
        this.root = root;
        return this;
    }

    public EXC build() {
        // fallbacks null mandatory properties
        this.fallback();

        // resolves the message against its possible arguments
        this.message = MessageFormatter.arrayFormat(this.message, this.messageArgs);

        // builds a new instance of exception
        EXC exc;
        try {
            exc = this.exceptionClass.getConstructor(new Class<?>[]{this.getClass()}).newInstance(this);
        } catch (final Exception e) {
            LOGGER.error("Cannot instantiate exception: returning null.", e);
            return null;
        }

        // rearranges the stack trace in order to get rid of reflection-related
        // elements (first 5 on JDK 1.8)
        // this is needed to avoid confusion in reading exceptions
        final StackTraceElement[] stackTrace = Arrays.copyOfRange(exc.getStackTrace(), 5, exc.getStackTrace().length);
        exc.setStackTrace(stackTrace);

        // returns the exception
        return exc;
    }

    /**
     * It fallbacks mandatory properties that are {@code null}.
     */
    protected void fallback() {
        // if auditBuilder is not null, set several other fields of the
        // exceptionBuilder if they are null
        if (this.auditBuilder != null) {
            if (this.code == null && this.auditBuilder.getCode() != null
                    && this.auditBuilder.getCode() instanceof MessageCode) {
                this.code = this.auditBuilder.getCode();
            }
            if (this.cause == null && this.auditBuilder.getException() != null) {
                this.cause = this.auditBuilder.getException();
            }
            if (this.message == null && this.auditBuilder.getMessage() != null) {
                this.message = this.auditBuilder.getMessage();
            }
            if (this.messageArgs == null && this.auditBuilder.getMessageArgs() != null) {
                this.messageArgs = this.auditBuilder.getMessageArgs();
            }
        }

        // if cause is not null and it is a BuildableException, set several
        // other fields of the exceptionBuilder if they are null
        if (cause != null && cause instanceof BuildableException) {
            final BuildableException buildableCause = (BuildableException) cause;
            if (this.code == null) {
                this.code = buildableCause.getCode();
            }
            this.silent = buildableCause.isSilent();
        }

        // if code is null, set the default error code
        if (code == null) {
            this.code = DEFAULT_ERROR_CODE;
        }

        // if a resource adapter is configured, tries to resolve the header out
        // of the exception's code
        if (ems != null) {
            try {
                this.header = ems.getMessage(this.code.toString(), new Object[]{}, Locale.getDefault());
            } catch (final NoSuchMessageException e) {
            }
        }
    }

    protected ResourceBundleMessageSource getResourceBundle() {
        return ems;
    }

}
