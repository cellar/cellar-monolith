package eu.europa.ec.opoce.cellar.cmr;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

/**
 * this is a data-structure to group and ungroup different datastreams in 1 single datastream
 */
public class DataStreamWrapper {

    private static final String CDATA_REPLACEMENT = "__CDATA__END__REPLACE_STRING__";

    private static final ThreadLocal<SAXParserFactory> SAX_PARSER_FACTORY = ThreadLocal.withInitial(() -> {
        final SAXParserFactory parserFactory = SAXParserFactory.newInstance();
        parserFactory.setNamespaceAware(true);
        return parserFactory;
    });

    private final Map<ContentType, String> map = new HashMap<>();

    public DataStreamWrapper() {
    }

    /**
     * Check if the mentioned data stream is available.
     *
     * @param dataStreamName the data stream.
     */
    public boolean containsData(ContentType dataStreamName) {
        return map.containsKey(dataStreamName);
    }

    public String getData(ContentType dataStreamName) {
        return map.get(dataStreamName);
    }

    public void setData(final ContentType contentType, final String data) {
        if (data == null) {
            map.remove(contentType);
        } else {
            map.put(contentType, data);
        }
    }

    public String createXml() {
        StringBuilder xml = new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<cmr-stream>\n");
        for (Map.Entry<ContentType, String> entry : map.entrySet()) {
            xml.append("<data-stream name=\"")
                    .append(entry.getKey().name())
                    .append("\"><![CDATA[")
                    .append(entry.getValue().replace("]]>", CDATA_REPLACEMENT))
                    .append("]]></data-stream>\n");
        }
        xml.append("</cmr-stream>\n");
        return xml.toString();
    }

    public DataStreamWrapper loadFromXml(final String xml) {
        map.clear();
        try (InputStream stream = new ByteArrayInputStream(xml.getBytes(Charset.forName("UTF-8")))) {
            final SAXParser parser = SAX_PARSER_FACTORY.get().newSAXParser();
            parser.parse(stream, new WrappedXmlHandler(map));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return this;
    }

    private static class WrappedXmlHandler extends DefaultHandler {

        private final Map<ContentType, String> map;
        private ContentType currentStream;
        private StringBuilder builder;

        WrappedXmlHandler(Map<ContentType, String> map) {
            this.map = map;
        }

        @Override
        public void startElement(final String uri, final String localName, final String qName, final Attributes attributes) {
            if (localName.equals("data-stream")) {
                currentStream = ContentType.valueOf(attributes.getValue("name"));
                builder = new StringBuilder();
            }
        }

        @Override
        public void endElement(final String uri, final String localName, final String qName) {
            if (currentStream != null) {
                map.put(currentStream, builder.toString().replace(CDATA_REPLACEMENT, "]]>"));
                currentStream = null;
                builder = null;
            }
        }

        @Override
        public void characters(final char[] ch, final int start, final int length) {
            if (currentStream != null) {
                builder.append(ch, start, length);
            }
        }
    }
}
