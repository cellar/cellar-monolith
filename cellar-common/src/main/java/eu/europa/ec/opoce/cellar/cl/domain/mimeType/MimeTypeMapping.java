/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.mimeType
 *             FILE : MimeTypeMapping.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 29 août 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.mimeType;

import java.util.Objects;

/**
 * This object represents a MimeTypeMapping.
 *
 * @author dbacquel
 *
 */
public class MimeTypeMapping implements Comparable<MimeTypeMapping> {

    /** The manifestation type. */
    private String manifestationType;

    /** The mime type. */
    private String mimeType;

    /** The extension. */
    private String extension;

    /** The eudor type. */
    private String eudorType;

    /** The priority weight. */
    private Long priorityWeight;

    /** The is textual. */
    private boolean isTextual;

    /**
     * Instantiates a new mime type mapping.
     */
    public MimeTypeMapping() {
        super();
    }

    /**
     * Instantiates a new mime type mapping.
     *
     * @param manifestationType the manifestation type
     * @param mimeType the mime type
     * @param extension the extension
     * @param eudorType the eudor type
     * @param priorityWeight the priority weight
     * @param isTextual the is textual
     */
    public MimeTypeMapping(final String manifestationType, final String mimeType, final String extension, final String eudorType,
            final Long priorityWeight, final boolean isTextual) {
        super();
        this.manifestationType = manifestationType;
        this.mimeType = mimeType;
        this.extension = extension;
        this.eudorType = eudorType;
        this.priorityWeight = priorityWeight;
        this.isTextual = isTextual;
    }

    /**
     * Gets the manifestation type.
     *
     * @return the manifestationType
     */
    public String getManifestationType() {
        return manifestationType;
    }

    /**
     * Sets the manifestation type.
     *
     * @param manifestationType the manifestationType to set
     */
    public void setManifestationType(final String manifestationType) {
        this.manifestationType = manifestationType;
    }

    /**
     * Gets the mime type.
     *
     * @return the mimeType
     */
    public String getMimeType() {
        return mimeType;
    }

    /**
     * Sets the mime type.
     *
     * @param mimeType the mimeType to set
     */
    public void setMimeType(final String mimeType) {
        this.mimeType = mimeType;
    }

    /**
     * Gets the eudor type.
     *
     * @return the eudorType
     */
    public String getEudorType() {
        return eudorType;
    }

    /**
     * Sets the eudor type.
     *
     * @param eudorType the eudorType to set
     */
    public void setEudorType(final String eudorType) {
        this.eudorType = eudorType;
    }

    /**
     * Gets the extension.
     *
     * @return the extension
     */
    public String getExtension() {
        return extension;
    }

    /**
     * Sets the extension.
     *
     * @param extension the extension to set
     */
    public void setExtension(final String extension) {
        this.extension = extension;
    }

    /**
     * Gets the priority.
     * @return the priority
     */
    public Long getPriority() {
        return this.priorityWeight;
    }

    /**
     * Sets the priority.
     *
     * @param priorityWeight the new priority
     */
    public void setPriority(final Long priorityWeight) {
        this.priorityWeight = priorityWeight;
    }

    /**
     * Checks if is textual.
     *
     * @return the isTextual
     */
    public boolean isTextual() {
        return this.isTextual;
    }

    /**
     * Sets the textual.
     *
     * @param isTextual the isTextual to set
     */
    public void setTextual(final boolean isTextual) {
        this.isTextual = isTextual;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof MimeTypeMapping))
            return false;
        MimeTypeMapping other = (MimeTypeMapping) obj;
        return Objects.equals(this.manifestationType, other.manifestationType) &&
               Objects.equals(this.mimeType, other.mimeType) &&
               Objects.equals(this.extension, other.extension) &&
               Objects.equals(this.priorityWeight, other.priorityWeight);
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return Objects.hash(manifestationType, mimeType, extension, priorityWeight);
    }

    /** {@inheritDoc} */
    @Override
    public int compareTo(final MimeTypeMapping other) {
        int result = 0;
        if (!this.equals(other)) {
            if (this.getPriority().equals(other.getPriority())) {
                if (this.manifestationType.equals(other.manifestationType)) {
                    if (this.mimeType.equals(other.mimeType)) {
                        return this.extension.compareTo(other.extension);
                    } else {
                        return this.mimeType.compareTo(other.mimeType);
                    }
                } else {
                    return this.manifestationType.compareTo(other.manifestationType);
                }
            } else {
                return this.priorityWeight.compareTo(other.priorityWeight);
            }
        }
        return result;
    }
}
