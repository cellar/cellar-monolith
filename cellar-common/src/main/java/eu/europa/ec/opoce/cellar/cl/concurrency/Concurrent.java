/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.concurrency
 *             FILE : Concurrent.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 19-12-2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.concurrency;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;
import eu.europa.ec.opoce.cellar.cl.concurrency.impl.CellarLockSession;
import eu.europa.ec.opoce.cellar.cl.service.client.IConcurrencyController;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import eu.europa.ec.opoce.cellar.domain.content.mets.OffIngestionOperationType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <class_description> Each method annotated with {@link Concurrent} drives the access towards the persistence layers in order to avoid deadlocks.<br>
 * If necessary, it locks the resources to process by following the strategy defined either by attribute {@link Locker}, either by the operation type itself.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 19-12-2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface Concurrent {

    @JsonTypeInfo(use = Id.CLASS)
    interface Locker {

        void doLock(final ICellarLockSession cellarLockSession, final Object... lockArgs) throws InterruptedException;

        default ICellarLockSession createCellarLockSession() {
        	return CellarLockSession.create(this);
        }
        
        default void lock(final ICellarLockSession cellarLockSession, final Object... lockArgs) throws InterruptedException {
            if (lockArgs != null && lockArgs.length > 0) {
                this.doLock(cellarLockSession, lockArgs);
                cellarLockSession.setLockAcquisitionTime(System.currentTimeMillis());
            }
        }

        default void unlock(final ICellarLockSession cellarLockSession) {
            this.getConcurrencyController().unlock(cellarLockSession);
        }

        default IConcurrencyController getConcurrencyController() {
            return ServiceLocator.getService(IConcurrencyController.class);
        }
    }

    /**
     * Defines the type of off-ingestion operation (that is, an operation which is not an ingestion) for which to lock.
     */
    OffIngestionOperationType locker() default OffIngestionOperationType.UNKNOWN;
}
