/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.concurrency
 *             FILE : ICellarLock.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : May 2, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.concurrency;

import java.util.List;
import java.util.Set;

/**
 * <class_description> Cellar lock interface.
 * <br/><br/>
 * ON : May 2, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface ICellarLock {

    /**
     * <class_description> Cellar locking mode enum.
     * <br/><br/>
     * ON : May 15, 2013
     *
     * @author ARHS Developments
     * @version $Revision$
     */
    public enum MODE {
        DEFAULT, READ, WRITE;
    };

    /**
     * Add the CELLAR lock session.
     */
    void addLockSession(final ICellarLockSession cellarLockSession);

    /**
     * Remove the CELLAR lock session.
     */
    boolean removeLockSession(final ICellarLockSession cellarLockSession);

    /**
     * Return the number of locks.
     * @return the number of locks
     */
    int getLockCount();

    /**
     * Return true if the lock is not used.
     * @return true if the lock is not used
     */
    boolean isNotUsed();

    /**
     * Return the CELLAR lock sessions.
     * @return the CELLAR lock sessions
     */
    Set<ICellarLockSession> getCellarLockSessions();

    /**
     * Acquire the lock.
     * @param mode the locking mode to use
     * @throws InterruptedException
     */
    void lock(final MODE mode) throws InterruptedException;

    /**
     * Try to acquire the lock.
     * @param mode the locking mode to use
     * @return true if the lock is acquired
     */
    boolean tryLock(final MODE mode);

    /**
     * Release the lock.
     * @param mode the locking mode to use
     */
    void unlock(final MODE mode);

    /**
     * Return the production identifiers concerned by the lock.
     * @return the production identifiers concerned by the lock
     */
    List<String> getPids();

    /**
     * Return the default locking mode (READ/WRITE).
     * @return the default locking mode
     */
    MODE getDefaultMode();

    /**
     * Return the locking mode (READ/WRITE). If the locking mode is DEFAULT, the default mode of the current lock is returned.
     * @param mode the initial mode (READ/WRITE/DEFAULT)
     * @return the locking mode (READ/WRITE)
     */
    MODE getLockingMode(final MODE mode);
}
