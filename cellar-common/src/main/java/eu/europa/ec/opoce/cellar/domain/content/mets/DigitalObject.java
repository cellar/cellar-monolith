/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : CellarIdentifier.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 24 mai 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.domain.content.mets;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.LanguageIsoCode;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static com.google.common.collect.Maps.immutableEnumMap;

/**
 * The Class DigitalObject.
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 24 mai 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class DigitalObject extends CellarIdentifiedObject implements Serializable {

    private static final long serialVersionUID = 8889950179318070823L;

    private transient static final Logger LOGGER = LoggerFactory.getLogger(DigitalObject.class);

    private Metadata businessMetadata;

    private Metadata technicalMetadata;

    private final StructMap structMap;

    private Date lastModificationDate;

    private Date embargoDate;

    private Date label;

    private List<LanguageIsoCode> languages;

    private String manifestationMimeType;

    private final List<DigitalObject> childObjects = new LinkedList<DigitalObject>();

    private final List<ContentStream> contentStreams = new ArrayList<ContentStream>();

    private List<ManifestationTransformator> transformatorList;

    private DigitalObject parentObject;

    private String deleteModelString;

    private String deleteSparqlQuery;

    private String modelLoadURL;

    private Date modelLoadActivationDate;

    private String sparqlLoadURL;

    private Date sparqlLoadActivationDate;

    private boolean isOntologyLoad = false;

    private Map<ContentType, String> versions = Collections.emptyMap();

    private List<ContentType> updatedTypes = new ArrayList<>();

    /**
     * Instantiates a new digital object.
     *
     * @param map the map
     */
    public DigitalObject(final StructMap map) {
        this.structMap = map;
    }

    /**
     * Gets the all childs.
     *
     * @param includeCurrent the include current
     * @return the all childs
     */
    public List<DigitalObject> getAllChilds(final boolean includeCurrent) {
        final List<DigitalObject> all = new ArrayList<DigitalObject>();
        if (includeCurrent) {
            all.add(this);
        }
        addChilds(all);
        return all;
    }

    /**
     * Adds the childs.
     *
     * @param allChilds the all childs
     */
    private void addChilds(final List<DigitalObject> allChilds) {
        allChilds.addAll(childObjects);
        for (final DigitalObject child : childObjects) {
            child.addChilds(allChilds);
        }
    }

    /**
     * Gets the business metadata.
     *
     * @return the business metadata
     */
    public Metadata getBusinessMetadata() {
        return businessMetadata;
    }

    /**
     * Sets the business metadata.
     *
     * @param businessMetadata the new business metadata
     */
    public void setBusinessMetadata(final Metadata businessMetadata) {
        this.businessMetadata = businessMetadata;
    }

    /**
     * Gets the technical metadata.
     *
     * @return the technical metadata
     */
    public Metadata getTechnicalMetadata() {
        return technicalMetadata;
    }

    /**
     * Sets the technical metadata.
     *
     * @param technicalMetadata the new technical metadata
     */
    public void setTechnicalMetadata(final Metadata technicalMetadata) {
        this.technicalMetadata = technicalMetadata;
    }

    /**
     * Gets the struct map.
     *
     * @return the struct map
     */
    public StructMap getStructMap() {
        return structMap;
    }

    /**
     * Sets the languages.
     *
     * @param languages the new languages
     */
    public void setLanguages(final List<LanguageIsoCode> languages) {
        this.languages = languages;
    }

    /**
     * Gets the languages.
     *
     * @return the languages
     */
    public List<LanguageIsoCode> getLanguages() {
        return languages != null ? languages : Collections.<LanguageIsoCode>emptyList();
    }

    /**
     * Gets the manifestation mime type.
     *
     * @return the manifestation mime type
     */
    public String getManifestationMimeType() {
        return manifestationMimeType;
    }

    /**
     * Sets the manifestation mime type.
     *
     * @param manifestationMimeType the new manifestation mime type
     */
    public void setManifestationMimeType(final String manifestationMimeType) {
        this.manifestationMimeType = manifestationMimeType;
    }

    /**
     * Gets the last modification date.
     *
     * @return the last modification date
     */
    public Date getLastModificationDate() {
        return lastModificationDate;
    }

    /**
     * Sets the last modification date.
     *
     * @param lastModificationDate the new last modification date
     */
    public void setLastModificationDate(final Date lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    /**
     * Gets the embargo date.
     *
     * @return the embargo date
     */
    public Date getEmbargoDate() {
        return embargoDate;
    }

    /**
     * Sets the embargo date.
     *
     * @param embargoDate the new embargo date
     */
    public void setEmbargoDate(final Date embargoDate) {
        this.embargoDate = embargoDate;
    }

    /**
     * Gets the label.
     *
     * @return the label
     */
    public Date getLabel() {
        return label;
    }

    /**
     * Sets the label.
     *
     * @param label the new label
     */
    public void setLabel(final Date label) {
        this.label = label;
    }

    /**
     * Gets the content streams.
     *
     * @return the content streams
     */
    public List<ContentStream> getContentStreams() {
        return contentStreams;
    }

    /**
     * Adds the content stream.
     *
     * @param contentStream the content stream
     */
    public void addContentStream(final ContentStream contentStream) {
        if (contentStream != null) {
            contentStream.setParentObject(this);
            contentStreams.add(contentStream);
        }
    }

    /**
     * Gets the parent object.
     *
     * @return the parent object
     */
    public DigitalObject getParentObject() {
        return parentObject;
    }

    /**
     * Sets the parent object.
     *
     * @param object the new parent object
     */
    public void setParentObject(final DigitalObject object) {
        this.parentObject = object;
    }

    /**
     * Gets the child objects.
     *
     * @return the child objects
     */
    public List<DigitalObject> getChildObjects() {
        return childObjects;
    }

    /**
     * Adds the child object.
     *
     * @param childObject the child object
     */
    public void addChildObject(final DigitalObject childObject) {
        ContentIdentifier childContentIdentifier = childObject.getCellarId();
        String childCellarId = "";
        if (childContentIdentifier != null) {
            childCellarId = childContentIdentifier.getIdentifier();
        }

        ContentIdentifier parentContentIdentifier = this.getCellarId();
        String parentCellarId = "";
        if (parentContentIdentifier != null) {
            parentCellarId = parentContentIdentifier.getIdentifier();
        }

        if (!childCellarId.contains(parentCellarId)) {
            throw ExceptionBuilder
                    .get(CellarException.class)
                    .withCode(CommonErrors.INVALID_CELLARID_HIERARCHY)
                    .withMessage("Child ({}) cannot be part of parent's hierarchy ({})")
                    .withMessageArgs(childCellarId, parentCellarId)
                    .build();
        }
        childObject.setParentObject(this);
        this.childObjects.add(childObject);
    }

    /**
     * Perform on the current instance the operations contained in the DigitalObjectStrategy passed to the method.
     *
     * @param digitalObjectStrategy an object that implements the desired strategy.
     */
    public void applyStrategyOnSelfAndChildren(final DigitalObjectStrategy digitalObjectStrategy) {
        digitalObjectStrategy.applyStrategy(this);
        for (final DigitalObject digitalObject : this.childObjects) {
            digitalObject.applyStrategyOnSelfAndChildren(digitalObjectStrategy);
        }
    }

    /**
     * Gets the transformator list.
     *
     * @return the transformator list
     */
    public List<ManifestationTransformator> getTransformatorList() {
        return transformatorList;
    }

    /**
     * Adds the transformator.
     *
     * @param manifestationTransformator the manifestation transformator
     */
    public void addTransformator(final ManifestationTransformator manifestationTransformator) {
        if (transformatorList == null) {
            transformatorList = new ArrayList<ManifestationTransformator>(1);
        }

        transformatorList.add(manifestationTransformator);
    }

    /**
     * Check languages.
     *
     * @throws CellarException the cellar exception
     */
    public void checkLanguages() throws CellarException {
        this.checkLanguages(this.getLanguages());
    }

    /**
     * Check languages.
     *
     * @param <L>       the generic type
     * @param languages the languages
     * @throws CellarException the cellar exception
     */
    public <L> void checkLanguages(final Collection<L> languages) throws CellarException {
        if (languages.isEmpty()) {
            final String msg = "Expression ['{}' - '{}'] has no language associated.";
            if (this.getStructMap().getMetsDocument().getProfile().hasWeakValidation()) {
                LOGGER.warn(msg, this.getCellarId().getIdentifier(), this.getCellarId().getUri());
            } else {
                throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.MODEL_LOADING_INCOMPLETE_DATA).withMessage(msg)
                        .withMessageArgs(this.getCellarId().getIdentifier(), this.getCellarId().getUri()).build();
            }
        }
    }

    /**
     * Gets the delete model string.
     *
     * @return the delete model string
     */
    public String getDeleteModelString() {
        return this.deleteModelString;
    }

    /**
     * Sets the delete model string.
     *
     * @param deleteModelString the new delete model string
     */
    public void setDeleteModelString(final String deleteModelString) {
        this.deleteModelString = deleteModelString;
    }

    /**
     * Gets the delete sparql query.
     *
     * @return the delete sparql query
     */
    public String getDeleteSparqlQuery() {
        return this.deleteSparqlQuery;
    }

    /**
     * Sets the delete sparql query.
     *
     * @param deleteSparqlQuery the new delete sparql query
     */
    public void setDeleteSparqlQuery(final String deleteSparqlQuery) {
        this.deleteSparqlQuery = deleteSparqlQuery;
    }

    /**
     * Gets the model load url.
     *
     * @return the modelLoadURL
     */
    public String getModelLoadURL() {
        return modelLoadURL;
    }

    /**
     * Sets the model load url.
     *
     * @param modelLoadURL the modelLoadURL to set
     */
    public void setModelLoadURL(final String modelLoadURL) {
        this.modelLoadURL = modelLoadURL;
    }

    /**
     * Gets the model load activation date.
     *
     * @return the modelLoadActivationDate
     */
    public Date getModelLoadActivationDate() {
        return modelLoadActivationDate;
    }

    /**
     * Sets the model load activation date.
     *
     * @param modelLoadActivationDate the modelLoadActivationDate to set
     */
    public void setModelLoadActivationDate(final Date modelLoadActivationDate) {
        this.modelLoadActivationDate = modelLoadActivationDate;
    }

    public String getSparqlLoadURL() {
        return sparqlLoadURL;
    }

    public void setSparqlLoadURL(String sparqlLoadURL) {
        this.sparqlLoadURL = sparqlLoadURL;
    }

    public Date getSparqlLoadActivationDate() {
        return sparqlLoadActivationDate;
    }

    public void setSparqlLoadActivationDate(Date sparqlLoadActivationDate) {
        this.sparqlLoadActivationDate = sparqlLoadActivationDate;
    }

    /**
     * Checks if is ontology load.
     *
     * @return the ontologyLoad
     */
    public boolean isOntologyLoad() {
        return isOntologyLoad;
    }

    /**
     * Sets the ontology load.
     *
     * @param ontologyLoad the ontologyLoad to set
     */
    public void setOntologyLoad(final boolean ontologyLoad) {
        isOntologyLoad = ontologyLoad;
    }

    public Map<ContentType, String> getVersions() {
        return versions;
    }

    public void setVersions(Map<ContentType, String> versions) {
        this.versions = immutableEnumMap(versions);
    }

    public List<ContentType> getUpdatedTypes() {
        return updatedTypes;
    }

    public void registerUpdatedType(ContentType type) {
        this.updatedTypes.add(type);
    }

    public void setUpdatedTypes(List<ContentType> updatedTypes) {
        this.updatedTypes = updatedTypes;
    }

    /**
     * To string.
     *
     * @return the string
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        if (this.getCellarId() != null) {
            return this.getCellarId().toString();
        } else {
            return super.toString();
        }
    }
}
