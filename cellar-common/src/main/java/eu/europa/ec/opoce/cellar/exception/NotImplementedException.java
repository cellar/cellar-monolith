/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.exception
 *        FILE : NotImplementedException.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 19-03-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.exception;

/**
 * This is a {@link NotImplementedException} buildable by an {@link ExceptionBuilder}.</br>
 * </br> 
 * ON : 19-03-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public class NotImplementedException extends CellarException {

    private static final long serialVersionUID = -634383432252014093L;

    /**
     * Constructs a new exception with its associated builder.
     * 
     * @param builder the builder to use for building the exception
     */
    public NotImplementedException(final NotImplementedExceptionBuilder builder) {
        super(builder);
    }

}
