package eu.europa.ec.opoce.cellar.domain.content.mets;

import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsProfile.MetsVersion;

import java.util.ArrayList;
import java.util.Collection;

public enum OperationSubType {

    Read(OperationType.Read) {

        @Override
        protected boolean doAccept(final StructMap structMap) {
            return true;
        }
    }, //
    Create(OperationType.Create, OperationType.CreateOrIgnore) {

        @Override
        protected boolean doAccept(final StructMap structMap) {
            return !OperationSubType.Append.accept(structMap);
        }
    }, //
    Append(OperationType.Create, OperationType.CreateOrIgnore) {

        @Override
        protected boolean doAccept(final StructMap structMap) {
            boolean accepted = true;

            final DigitalObject rootDigitalObject = structMap.getDigitalObject();
            if (rootDigitalObject.getType().isTopLevel()) {
                if (rootDigitalObject.getBusinessMetadata() != null) {
                    accepted = false;
                }
            }

            return accepted;
        }
    }, //
    Update(OperationType.Update) {

        @Override
        protected boolean doAccept(final StructMap structMap) {
            return structMap.getMetsDocument().getProfile().getVersion() == MetsVersion.V1;
        }
    }, //
    Merge(OperationType.Update) {

        @Override
        protected boolean doAccept(final StructMap structMap) {
            return structMap.getMetsDocument().getProfile().getVersion() == MetsVersion.V2;
        }
    }, //
    Delete(OperationType.Delete) {

        @Override
        protected boolean doAccept(final StructMap structMap) {
            return true;
        }
    };

    private Collection<OperationType> supertypes;

    private OperationSubType(final OperationType... supertype) {
        this.supertypes = new ArrayList<OperationType>();
        for (final OperationType currSupertype : supertype) {
            this.supertypes.add(currSupertype);
        }
    }

    public final boolean accept(final StructMap structMap) {
        boolean accepted = false;

        for (final OperationType supertype : this.supertypes) {
            if (supertype.accept(structMap)) {
                accepted = true;
                break;
            }
        }
        if (accepted) {
            accepted = this.doAccept(structMap);
        }

        return accepted;
    }

    public StructMapTransaction<?> getStructMapTransaction() {
        return ServiceLocator.getService("structMap" + this, StructMapTransaction.class);
    }

    protected abstract boolean doAccept(final StructMap structMap);

    public boolean isChildOf(final OperationType supertype) {
        return this.supertypes.contains(supertype);
    }

}
