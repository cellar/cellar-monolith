/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.util
 *             FILE : CronUtils.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 24, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.util;

import eu.europa.ec.opoce.cellar.common.MessageFormatter;

/**
 * <class_description> Cron expression helper.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 24, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public final class CronUtils {

    private final static String EVERY_X_MINUTES = "0 */{} * * * *";

    /**
     * Returns the cron expression "0 *\/<code>minutes</code> * * * *".
     * @param minutes the interval in minutes
     * @return the cron expression
     */
    public static String formatEveryXMinutes(final int minutes) {
        return MessageFormatter.format(EVERY_X_MINUTES, minutes);
    }
}
