/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.support
 *             FILE : IterableUtils.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 17-09-2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */

package eu.europa.ec.opoce.cellar.common.util;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * <class_description> Utility class extending commons IOUtils for computing message digest during the copy of a stream.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 17-09-2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class IOUtils extends org.apache.commons.io.IOUtils {

    /**
     * SHA-256 message digest algorithm name.
     */
    public static final String SHA_256 = "SHA-256";

    /**
     * Logger instance.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(IOUtils.class);

    /**
     * Copy the String to the output stream and compute the message digest during a single iteration.
     * @param inputString the input string
     * @param outputStream the output stream
     * @param messageDigestAlgorithm the message algorithm name to use
     * @return the message digest object
     * @throws IOException
     */
    public static MessageDigest copyAndDigest(final String inputString, final OutputStream outputStream,
            final String messageDigestAlgorithm) throws IOException {
        InputStream inputStream = null;

        try {
            inputStream = new ByteArrayInputStream(inputString.getBytes(StandardCharsets.UTF_8));
            return copyAndDigest(inputStream, outputStream, messageDigestAlgorithm);
        } finally {
            closeQuietly(inputStream);
        }
    }

    /**
     * Copy the input stream to the output stream and compute the message digest during a single iteration.
     * @param inputStream the input stream
     * @param outputStream the output stream
     * @param messageDigestAlgorithm the message algorithm name to use
     * @return the message digest object
     * @throws IOException
     */
    public static MessageDigest copyAndDigest(final InputStream inputStream, final OutputStream outputStream,
            final String messageDigestAlgorithm) throws IOException {

        MessageDigest messageDigest = null;
        try {
            messageDigest = MessageDigest.getInstance(messageDigestAlgorithm);
        } catch (final NoSuchAlgorithmException e) {
            LOGGER.error("{} message digest algorithm does not exist.", messageDigestAlgorithm);
        }

        final byte[] data = new byte[1024];
        int read = 0;
        while ((read = inputStream.read(data)) != -1) {
            if (messageDigest != null) {
                messageDigest.update(data, 0, read);
            }
            outputStream.write(data, 0, read);
        }

        return messageDigest;
    }

    /**
     * Copy the String to the output stream and compute the message digest during a single iteration.
     * @param inputString the input string
     * @param outputStream the output stream
     * @param messageDigestAlgorithm the message algorithm name to use
     * @return the message digest as hex string
     * @throws IOException
     */
    public static String copyAndDigestHex(final String inputString, final OutputStream outputStream, final String messageDigestAlgorithm)
            throws IOException {
        return encodeHex(copyAndDigest(inputString, outputStream, messageDigestAlgorithm));
    }

    /**
     * Copy the input stream to the output stream and compute the message digest during a single iteration.
     * @param inputStream the input stream
     * @param outputStream the output stream
     * @param messageDigestAlgorithm the message algorithm name to use
     * @return the message digest as hex string
     * @throws IOException
     */
    public static String copyAndDigestHex(final InputStream inputStream, final OutputStream outputStream,
            final String messageDigestAlgorithm) throws IOException {
        return encodeHex(copyAndDigest(inputStream, outputStream, messageDigestAlgorithm));
    }

    /**
     * Encode the message digest in hex.
     * @param messageDigest the message digest object
     * @return the message digest in hex
     */
    public static String encodeHex(final MessageDigest messageDigest) {

        if (messageDigest == null) {
            LOGGER.info("Message digest is null.");
            return StringUtils.EMPTY;
        }

        return String.valueOf(Hex.encodeHex(messageDigest.digest()));
    }

    /**
     * Digest.
     *
     * @param input the input
     * @param messageDigestAlgorithm the message digest algorithm
     * @return the string
     */
    public static String digest(final File input, final String messageDigestAlgorithm) {
        String result = null;
        MessageDigest messageDigest = null;
        FileInputStream inputStream = null;
        try {
            messageDigest = MessageDigest.getInstance(messageDigestAlgorithm);
            inputStream = new FileInputStream(input);
            final byte[] data = new byte[1024];
            int read = 0;
            while ((read = inputStream.read(data)) != -1) {
                if (messageDigest != null) {
                    messageDigest.update(data, 0, read);
                }
            }
            result = encodeHex(messageDigest);
        } catch (final Exception e) {
            final RuntimeException excpt = new RuntimeException(
                    "An error occurred while calculating the checksum for {" + input.getAbsolutePath() + "}");
            throw excpt;
        } finally {
            closeQuietly(inputStream);
        }

        return result;
    }

}
