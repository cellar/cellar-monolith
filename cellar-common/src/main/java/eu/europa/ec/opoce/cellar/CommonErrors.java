/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar
 *        FILE : ErrorCode.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 13-03-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar;

import eu.europa.ec.opoce.cellar.MessageCode.TYPE;

/**
 * <class_description> Generic error codes.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 13-03-2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class CommonErrors {

    /* 000 Codes */
    public static final MessageCode UNSUPPORTED_DIGITAL_OBJECT_TYPE = new MessageCode("E-036");
    public static final MessageCode INVALID_CELLARID_HIERARCHY = new MessageCode("E-039", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode INVALID_CELLARID_FORMAT = new MessageCode("E-045", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode AT_LEAST_ONE_BUSINESS_ID = new MessageCode("E-046", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode TOO_MANY_CELLAR_ID = new MessageCode("E-047", TYPE.FUNCTIONAL_ERROR);
    /* 200 Codes */
    public static final MessageCode CELLAR_INTERNAL_ERROR = new MessageCode("E-200");
    public static final MessageCode INPUT_MUST_BE_DEFINED = new MessageCode("E-201");
    public static final MessageCode SERVICE_NOT_AVAILABLE = new MessageCode("E-202", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode MODEL_LOADING_INCOMPLETE_DATA = new MessageCode("E-231");
    /* 300 Codes */
    public static final MessageCode CLOSURE_FAILED = new MessageCode("E-300");
    public static final MessageCode LANGUAGE_CODE_IS_INVALID = new MessageCode("E-350");
    public static final MessageCode CREATION_OF_ZIPFILE_FAILED = new MessageCode("E-351");
    public static final MessageCode NO_METS_ENTRY_IN_ZIPFILE = new MessageCode("E-352", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode METS_INPUT_COULD_NOT_BE_OPENED = new MessageCode("E-353");
    public static final MessageCode FILE_INPUT_COULD_NOT_BE_OPENED = new MessageCode("E-354");
    public static final MessageCode UNKNOWN_METS_OPERATION = new MessageCode("E-355", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode UNKNOWN_METS_PROFILE = new MessageCode("E-356", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode ERROR_WHILE_PARSING_THE_METS_DOCUMENT = new MessageCode("E-357", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode URI_NAMESPACE_SHOULD_BE_OF_METS = new MessageCode("E-358");
    public static final MessageCode ONLY_SELLECT_SIP_NOTICES_SUPPORTED = new MessageCode("E-359");
    public static final MessageCode ONLY_URLS_SUPPORTED_AS_CONTENT = new MessageCode("E-360");
    public static final MessageCode CONTENTSTREAMS_MUST_HAVE_MANIFESTATION_AS_PARENT = new MessageCode("E-361");
    public static final MessageCode INVALID_FILEPOINTER_FOR_CONTENTSTREAM = new MessageCode("E-362", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode WHEN_FILEPOINTER_DEFINED_CONTENTSTREAM_IS_NEEDED = new MessageCode("E-363");
    public static final MessageCode NOT_ALLOWED_CELLARID_DEFINED_IN_CREATE = new MessageCode("E-364");
    public static final MessageCode INVALID_STRUCTURE_OF_DIGITAL_OBJECTS = new MessageCode("E-365", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode ONLY_URLS_SUPPORTED_AS_METADATA = new MessageCode("E-366");
    public static final MessageCode METADATAFILE_NOT_FOUND = new MessageCode("E-367");
    public static final MessageCode PROBLEM_TO_GET_FILEREF_FROM_METADATA = new MessageCode("E-368");
    public static final MessageCode UNKNOWN_DIGITAL_OBJECT_TYPE = new MessageCode("E-369");
    public static final MessageCode USED_PREFIX_IS_NOT_ALLOWED = new MessageCode("E-370");
    public static final MessageCode UNKNOWN_STRUCTMAP_TYPE = new MessageCode("E-371");
    public static final MessageCode MISSING_ID_ON_STRUCTMAP = new MessageCode("E-372");
    public static final MessageCode MISSING_TYPE_ON_STRUCTMAP = new MessageCode("E-373");
    public static final MessageCode INVALID_TYPE_ON_STRUCTMAP = new MessageCode("E-374");
    public static final MessageCode LABEL_ATTRIBUTE_NOT_EXCEPTED = new MessageCode("E-375");
    public static final MessageCode LABEL_ATTRIBUTE_NOT_CORRECT_DATEFORMAT = new MessageCode("E-376");
    public static final MessageCode UNKNOWN_CONTENT_LOCTYPE = new MessageCode("E-377");
    public static final MessageCode ENTITY_RESOLVING_ERROR = new MessageCode("E-378");
    public static final MessageCode FAILED_PARSING_CONTENT_STREAM = new MessageCode("E-379", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode FAILED_PROCESSING_CONFIGURATION = new MessageCode("E-380");
    public static final MessageCode DIRECT_MODEL_VALIDATION_FAILED = new MessageCode("E-381", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode TECHNICAL_MODEL_VALIDATION_FAILED = new MessageCode("E-382", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode NAL_MODEL_VALIDATION_FAILED = new MessageCode("E-383");
    /* 400 Codes */
    public static final MessageCode READONLY_DATABASE = new MessageCode("E-400");
    public static final MessageCode UUID_GENERATION_FAILED = new MessageCode("E-401");
    public static final MessageCode RETRY_AWARE_CONN_FAILED = new MessageCode("E-402");
    public static final MessageCode TARGET_RESOURCE_DOES_NOT_EXIST = new MessageCode("E-403", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode NAL_MISSING_URI = new MessageCode("E-450", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode NAL_MISSING_ONTOLOGY_URI = new MessageCode("E-451", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode NAL_MISSING_NAME = new MessageCode("E-452", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode NAL_COULD_NOT_BE_ADDED = new MessageCode("E-453");
    public static final MessageCode INVALID_EMBARGO_DATE_IN_OBJECT_HIERARCHY = new MessageCode("E-460", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode MISSING_EMBARGO_DATE_IN_OBJECT_HIERARCHY = new MessageCode("E-461", TYPE.FUNCTIONAL_ERROR);
    /* 900 Codes */
    public static final MessageCode TEST_EXCEPTION = new MessageCode("E-998", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode NOT_IMPLEMENTED = new MessageCode("E-999");
    protected CommonErrors() {
    }
}
