/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.httpHeaders
 *             FILE : CellarHttpHeaders.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 22, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.http.headers;

import javax.ws.rs.core.HttpHeaders;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Apr 22, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface CellarHttpHeaders extends HttpHeaders {

    /**
     * http://www.ietf.org/rfc/rfc2295.txt
     */
    public final static String NEGOTIATE = "Negotiate";

    /**
     * http://www.ietf.org/rfc/rfc2295.txt
     */
    public final static String ALTERNATES = "Alternates";

    /**
     * http://www.ietf.org/rfc/rfc2295.txt
     */
    public final static String TCN = "TCN";

    /**
     * Accept max content stream size in bytes.
     */
    public final static String ACCEPT_MAX_CS_SIZE = "Accept-Max-Cs-Size";

    /**
     * See: http://mementoweb.org/guide/rfc/
     */
    public final static String MEMENTO_DATETIME = "Memento-Datetime";

    /**
     * See {@link <a https://tools.ietf.org/html/rfc5988}
     */
    public final static String LINK = "Link";

    /**
     * See: http://mementoweb.org/guide/rfc/
     */
    public final static String ACCEPT_DATETIME = "Accept-Datetime";
}
