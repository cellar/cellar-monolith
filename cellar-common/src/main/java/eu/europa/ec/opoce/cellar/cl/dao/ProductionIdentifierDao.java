package eu.europa.ec.opoce.cellar.cl.dao;

import java.util.List;

import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.admin.ProductionIdentifier;

/**
 * Dao for the the Entity {@link ProductionIdentifier}.
 * 
 * @author dsavares
 * 
 */
public interface ProductionIdentifierDao {

    /**
     * Persist the given transient instance.
     * 
     * @param productionIdentifier
     *            the transient instance to be persisted
     */
    void saveProductionIdentifier(ProductionIdentifier productionIdentifier);

    /**
     * Return the persistent instance with the given internal identifier, or null if not found.
     * 
     * @param id
     *            the internal identifier of the persistent instance
     * 
     * @return the persistent instance, or null if not found
     */
    ProductionIdentifier getProductionIdentifier(Long id);

    /**
     * Return the persistent instance with the given business identifier, or null if not found.
     * 
     * @param identifier
     *            the business identifier of the persistent instance
     * 
     * @return the persistent instance, or null if not found
     */
    ProductionIdentifier getProductionIdentifier(String identifier);

    /**
     * Gets the list of all ProductionIdentifier that have no CellarIdentifier.
     * 
     * @return a {@link List} of {@link ProductionIdentifier}.
     */
    List<ProductionIdentifier> getUnboundResources();

    /**
     * Return all persistent instances of {@link ProductionIdentifier}.
     * 
     * @return a {@link List} containing 0 or more persistent instances.
     */
    List<ProductionIdentifier> getProductionIdentifiers();

    /**
     * Delete the given persistent instance.
     * 
     * @param productionIdentifier
     *            the persistent instance to delete
     */
    void deleteProductionIdentifier(ProductionIdentifier productionIdentifier);

    /**
     * Indicates whether the given production identifier exists in database.
     * @param productionIdentifier the production identifier to test
     * @return true if the production identifier exists in database, false otherwise
     */
    boolean isProductionIdentifierExists(ProductionIdentifier productionIdentifier);

    /**
     * Gets the list of all ProductionIdentifier linked to this cellarIdentifier.
     * 
     * @param cellarIdentifier
     *            the cellarIdentifier.
     * @return a {@link List} of {@link ProductionIdentifier}.
     */
    List<ProductionIdentifier> getProductionIdentifiers(CellarIdentifier cellarIdentifier);

    /**
     * Gets the list of all ProductionIdentifier linked to this cellarId.
     * 
     * @param cellarId the cellarId.
     * @return a {@link List} of {@link ProductionIdentifier}.
     */
    List<ProductionIdentifier> getProductionIdentifiersByCellarId(final String cellarId);

    /**
     * Gets the list of all ProductionIdentifier linked to this list of cellarIds.
     * 
     * @param cellarIds the list of cellarIds.
     * @param pageSize the number of cellarIds processed per single request. If it outgoes the cellarIds's size,
     * the result is paged and multiple requests must be fired by the client in order to retrieve the whole result.
     * @return a {@link List} of {@link ProductionIdentifier}.
     */
    List<ProductionIdentifier> getProductionIdentifiersByCellarIds(final List<String> cellarIds, final int pageSize);

    /**
     * Gets the list of all ProductionIdentifier linked to this list of productionIds.
     * 
     * @param productionIds the list of productionIds.
     * @param pageSize the number of cellarIds processed per single request. If it outgoes the cellarIds's size,
     * the result is paged and multiple requests must be fired by the client in order to retrieve the whole result.
     * @return a {@link List} of {@link ProductionIdentifier}.
     */
    List<ProductionIdentifier> getProductionIdentifiersByProductionIds(final List<String> productionIds, final int pageSize);

    /**
     * Gets the list of ProductionIdentifier linked to this list of identifiers.
     * 
     * @param identifiers the list of identifiers.
     * @return a {@link List} of {@link ProductionIdentifier}.
     */
    List<ProductionIdentifier> getProductionIdentifiers(final List<String> identifiers);
}
