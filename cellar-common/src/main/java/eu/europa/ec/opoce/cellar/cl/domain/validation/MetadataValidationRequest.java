package eu.europa.ec.opoce.cellar.cl.domain.validation;

import org.apache.jena.rdf.model.Model;

import static java.util.Objects.requireNonNull;

public class MetadataValidationRequest {

    private final Model model;
    private String cellarId;

    public MetadataValidationRequest(Model model) {
        this.model = requireNonNull(model);
    }

    public MetadataValidationRequest cellarId(String cellarId) {
        this.cellarId = cellarId;
        return this;
    }

    public Model getModel() {
        return model;
    }

    public String getCellarId() {
        return cellarId;
    }
}
