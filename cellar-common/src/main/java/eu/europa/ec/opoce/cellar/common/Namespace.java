/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common
 *             FILE : Namespace.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 26 août 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common;

import org.apache.jena.vocabulary.DC;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.apache.jena.vocabulary.XSD;

/**
 * <p>Namespace interface.</p>
 */
public interface Namespace {

    /**
     * Constant <code>language_ontology="http://publications.europa.eu/resource/"{trunked}</code>
     */
    String language_ontology = "http://publications.europa.eu/resource/authority/language/";

    /** The file type_ontology. */
    String fileType_ontology = "http://publications.europa.eu/resource/authority/file-type/";
    /**
     * Constant <code>publications_ontology="http://publications.europa.eu/ontology/"</code>
     */
    String publications_ontology = "http://publications.europa.eu/ontology/";
    /**
     * Constant <code>annotation="publications_ontology + annotation#"</code>
     */
    String annotation = publications_ontology + "annotation#";
    /**
     * Constant <code>cdm="publications_ontology + cdm#"</code>
     */
    String cdm = publications_ontology + "cdm#";
    /**
     * Constant <code>datatype="publications_ontology + datatype#"</code>
     */
    String datatype = publications_ontology + "datatype#";
    /**
     * Constant <code>tdm="publications_ontology + tdm#"</code>
     */
    String tdm = publications_ontology + "tdm#";
    /**
     * Constant <code>deriv="publications_ontology + cdm/derived_cla"{trunked}</code>
     */
    String deriv = publications_ontology + "cdm/derived_class#";
    /**
     * Constant <code>import_onto="publications_ontology + cdm/import#"</code>
     */
    String import_onto = publications_ontology + "cdm/import#";
    /**
     * Constant <code>indexation="publications_ontology + cdm/indexation#"</code>
     */
    String indexation = publications_ontology + "cdm/indexation#";
    /**
     * Constant <code>cmr="publications_ontology + cdm/cmr#"</code>
     */
    String cmr = publications_ontology + "cdm/cmr#";
    /**
     * Constant <code>cmr="publications_ontology + dorie#"</code>
     */
    String dorie = publications_ontology + "dorie#";
    /**
     * Constant <code>ev="http://eurovoc.europa.eu/schema#"</code>
     */
    String ev = "http://eurovoc.europa.eu/schema#";
    /**
     * Constant <code>at="http://publications.europa.eu/resource/"{trunked}</code>
     */
    String at = "http://publications.europa.eu/resource/authority/";
    /**
     * Constant <code>skos="http://www.w3.org/2004/02/skos/core#"</code>
     */
    String skos = "http://www.w3.org/2004/02/skos/core#";
    /**
     * Constant <code>skosUri="http://www.w3.org/2004/02/skos/core"</code>
     */
    String skosUri = "http://www.w3.org/2004/02/skos/core";
    /**
     * Constant <code>skosXl="http://www.w3.org/2008/05/skos-xl#"</code>
     */
    String skosXl = "http://www.w3.org/2008/05/skos-xl#";
    /**
     * Constant <code>skosXlUri="http://www.w3.org/2008/05/skos-xl"</code>
     */
    String skosXlUri = "http://www.w3.org/2008/05/skos-xl";
    /**
     * Constant <code>dc="DC.NS"</code>
     */
    String dc = DC.NS;
    /**
     * Constant <code>owl="OWL.NS"</code>
     */
    String owl = OWL.NS;
    /**
     * Constant <code>rdf="RDF.getURI()"</code>
     */
    String rdf = RDF.getURI();
    /**
     * Constant <code>rdfs="RDFS.getURI()"</code>
     */
    String rdfs = RDFS.getURI();
    /**
     * Constant <code>xsd="XSD.getURI()"</code>
     */
    String xsd = XSD.getURI();
}
