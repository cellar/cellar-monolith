/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : CellarIdentifier.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Sep 29, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$$
 *          VERSION : $LastChangedRevision$$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.admin;

import org.hibernate.annotations.NaturalId;

import javax.persistence.*;

/**
 *
 * This object store the Cellar UUID.
 *
 * @author dsavares
 *
 */
@Entity
@Table(name = "CELLAR_IDENTIFIER")
@NamedQueries({
        @NamedQuery(name = "getByCellarIdentifier", query = "from CellarIdentifier where UUID = ?0"),
        @NamedQuery(name = "getByCellarIdentifierLike", query = "from CellarIdentifier where UUID like ?0"),
        @NamedQuery(name = "getByCellarIdentifierOrLike", query = "from CellarIdentifier where UUID = ?0 OR UUID like ?1"),
        @NamedQuery(name = "getByFileRef", query = "select id from CellarIdentifier id where UUID like ?0 and FILE_NAME = ?1"),
        @NamedQuery(name = "getMaxDocIndex", query = "select max(docIndex) from CellarIdentifier where UUID like ?0")})
public class CellarIdentifier {

    private Long id;
    private String uuid;
    private String fileName;
    private Integer docIndex;
    private Boolean readOnly;

    public CellarIdentifier() {
        super();
    }

    /**
     * Constructor.
     *
     * @param uuid
     *            the UUID to set.
     */
    public CellarIdentifier(final String uuid) {
        this.uuid = uuid;
    }

    /**
     * Create a new instance.
     *
     * @param uuid
     *            Cellar UUID
     * @param fileRef
     *            File ref in case of the cellar id is a content stream id.
     */
    public CellarIdentifier(final String uuid, final String fileRef) {
        this.uuid = uuid;
        this.fileName = fileRef;
    }

    /**
     * Gets the value of the id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    public Long getId() {
        return this.id;
    }

    /**
     * Set the value of the id.
     *
     * @param id
     *            the id to set
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Set the UUID.
     *
     * @param uuid
     *            the new uuid
     */
    public void setUuid(final String uuid) {
        this.uuid = uuid;
    }

    /**
     * Get the UUID.
     *
     * @return the uuid
     */
    @Column(name = "UUID")
    @NaturalId
    public String getUuid() {
        return this.uuid;
    }

    /**
     * Get the file name of the content stream.
     *
     * @return The file name of the content stream if it exists else null.
     */
    @Column(name = "FILE_NAME")
    public String getFileName() {
        return this.fileName;
    }

    /**
     * Set the file name of the content stream.
     *
     * @param fileName
     *            The file name to set.
     */
    public void setFileName(final String fileName) {
        this.fileName = fileName;
    }

    /**
     * Gets the doc index.
     *
     * @return the doc index
     */
    @Column(name = "DOC_INDEX")
    public Integer getDocIndex() {
        return this.docIndex;
    }

    /**
     * Sets the doc index.
     *
     * @param docIndex the new doc index
     */
    public void setDocIndex(final Integer docIndex) {
        this.docIndex = docIndex;
    }

    /**
     * Gets the read only.
     *
     * @return the read only
     */
    @Column(name = "READ_ONLY")
    public Boolean getReadOnly() {
        return readOnly;
    }

    /**
     * Sets the read only.
     *
     * @param readOnly the new read only
     */
    public void setReadOnly(final Boolean readOnly) {
        this.readOnly = readOnly;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = (prime * result) + (this.id == null ? 0 : this.id.hashCode());
        result = (prime * result) + (this.uuid == null ? 0 : this.uuid.hashCode());
        return result;
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        final CellarIdentifier other = (CellarIdentifier) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        if (this.uuid == null) {
            if (other.uuid != null) {
                return false;
            }
        } else if (!this.uuid.equals(other.uuid)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "CellarIdentifier [id=" + this.id + ", uuid=" + this.uuid + "]";
    }
}
