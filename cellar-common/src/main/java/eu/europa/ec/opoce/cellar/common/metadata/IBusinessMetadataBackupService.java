/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.metadata
 *             FILE : IBusinessMetadataBackupService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Sep 6, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.metadata;

import eu.europa.ec.opoce.cellar.cl.domain.response.IOperation;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Sep 6, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IBusinessMetadataBackupService<SOURCE extends StructMap, TARGET extends IOperation<?>> {

    /**
     * Backup into {@link target} the business information taken from the given {@link source}.
     */
    void backupBusinessMetadata(final SOURCE source, final TARGET target);

}
