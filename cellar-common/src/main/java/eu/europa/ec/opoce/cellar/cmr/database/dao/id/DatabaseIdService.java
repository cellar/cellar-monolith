package eu.europa.ec.opoce.cellar.cmr.database.dao.id;

/**
 * <p>DatabaseIdService interface.</p>
 */
public interface DatabaseIdService {

    /**
     * <p>getId.</p>
     *
     * @param tableName a {@link java.lang.String} object.
     * @return a long.
     */
    long getId(String tableName);
}
