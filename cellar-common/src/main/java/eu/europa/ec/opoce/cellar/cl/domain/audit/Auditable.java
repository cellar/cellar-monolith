package eu.europa.ec.opoce.cellar.cl.domain.audit;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({
        ElementType.METHOD})
public @interface Auditable {

    public enum LogLevel {

        TRACE, DEBUG, INFO, WARN, ERROR;
    }

    AuditTrailEventProcess process() default AuditTrailEventProcess.Undefined;

    AuditTrailEventAction action() default AuditTrailEventAction.Undefined;

    boolean start() default true;

    boolean end() default true;

    boolean fail() default true;

    LogLevel logLevel() default LogLevel.INFO;

}
