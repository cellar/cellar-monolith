/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ccr.s3
 *             FILE : S3DataStreamNamingService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 7, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$$
 *          VERSION : $LastChangedRevision$$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.s3;

import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;

/**
 * <class_description> A temporary service class.
 * <br/><br/>
 * <notes>  Once the FEDORA patch is in place this service can be deleted
 * <br/><br/>
 * ON : Oct 7, 2015
 *
 * @author ARHS Developments
 * @version $Revision$$
 */
public interface S3DataStreamNamingService {

    /**
     * Rename cellar identifier.
     *
     * @param cellarIdentifier the cellar identifier
     * @param label the label
     * @param baseUuid the base uuid
     * @param docIndex the doc index
     */
    void renameCellarIdentifier(final CellarIdentifier cellarIdentifier, final String label, final String baseUuid, final int docIndex);
}
