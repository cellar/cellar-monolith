/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice
 *             FILE : CachableObject.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 29, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.cacheable.impl;

import java.util.HashMap;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 29, 2013
 *
 * @deprecated Don't use this class for new developments. Use spring-cache or any
 * decent cache implementation.
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@SuppressWarnings("serial")
@Deprecated
public abstract class CacheableObject<K, V> extends HashMap<K, V> {

    public V get(final Object key, final boolean checkCache) {
        @SuppressWarnings("unchecked")
        final K myKey = (K) key;

        V value;
        if (checkCache && containsKey(myKey)) {
            value = super.get(myKey);
        } else {
            value = retrieve(myKey);
            if (value != null) { // avoid cache pollution with null values
                put(myKey, value);
            }
        }
        return value;
    }

    /**
     * @see java.util.HashMap#get(java.lang.Object)
     */
    @Override
    public V get(final Object key) {
        return this.get(key, true);
    }

    protected abstract V retrieve(final K key);

}
