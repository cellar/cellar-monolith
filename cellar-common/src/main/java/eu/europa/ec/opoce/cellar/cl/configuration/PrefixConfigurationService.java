package eu.europa.ec.opoce.cellar.cl.configuration;

import eu.europa.ec.opoce.cellar.domain.content.mets.PrefixConfiguration;

import java.util.Map;

/**
 * <p>PrefixConfigurationService interface.</p>
 */
public interface PrefixConfigurationService {

    /**
     * <p>getPrefixConfiguration.</p>
     *
     * @return a {@link eu.europa.ec.opoce.cellar.domain.content.mets.PrefixConfiguration} object.
     */
    PrefixConfiguration getPrefixConfiguration();

    /**
     * <p>getPrefixUri.</p>
     *
     * @param prefix a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    String getPrefixUri(String prefix);

    /**
     * <p>getPrefixUris.</p>
     *
     * @return a {@link java.util.Map} object.
     */
    Map<String, String> getPrefixUris();
}
