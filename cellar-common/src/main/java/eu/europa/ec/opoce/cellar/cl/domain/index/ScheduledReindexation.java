/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.index
 *             FILE : ScheduledReindexation.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 12 sept. 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.index;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * The Class ScheduledReindexation.
 * <class_description> This bean maps the one and only configuration for the automatic reindexation batch job
 * This job is triggered at a configurable rate - cron
 * It should execute during a specific amount of time - duration - given in minutes or hours
 *
 * It should run over all content that can be :
 * selected through a sparql query - sparqlQuery
 * all existing content - allEligibleContent
 * all content that past a given period - allEligibleContentWithinPeriod & period - given in days/weeks/months
 *
 * A trigger should be added to prevent adding more than one record
 * <br/><br/>
 * ON : 12 sept. 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Entity
@Table(name = "SCHEDULED_REINDEX")
public class ScheduledReindexation implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 6668160829044680629L;

    /**
     * Internal id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    /** The cron expression that defines when the job should be triggered. */
    @Column(name = "CRON_EXPRESSION")
    private String cron;

    /** The duration the amount of time for the scheduled execution : expressed in a string X:amount m (minutes)/h (hours). */
    @Column(name = "DURATION")
    private String duration;

    /** The use sparql query as method of selection. */
    @Column(name = "USE_SPARQL")
    private boolean useSparql;

    /** The sparql query used to select targets. */
    @Column(name = "SPARQL_QUERY")
    private String sparqlQuery;

    /** The all eligible content flag to set the service to track down all content. */
    @Column(name = "ALL_CONTENT")
    private boolean allEligibleContent;

    /** The all eligible content within period flag to set the service to track down all content on the past specified period. */
    @Column(name = "ALL_CONTENT_PERIOD")
    private boolean allEligibleContentWithinPeriod;

    /** The period the configurable amount of time to use in the content selection: expressed in a string X:amount d (days)/w (weeks)/m (months). */
    @Column(name = "PERIOD")
    private String period;

    /** The enabled flag the flag used to enable or disable the automatic execution. */
    @Column(name = "ENABLED")
    private boolean enabled;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the cron.
     *
     * @return the cron
     */
    public String getCron() {
        return this.cron;
    }

    /**
     * Sets the cron.
     *
     * @param cron the new cron
     */
    public void setCron(final String cron) {
        this.cron = cron;
    }

    /**
     * Gets the duration.
     *
     * @return the duration
     */
    public String getDuration() {
        return this.duration;
    }

    /**
     * Sets the duration.
     *
     * @param duration the new duration
     */
    public void setDuration(final String duration) {
        this.duration = duration;
    }

    /**
     * Gets the sparql query.
     *
     * @return the sparql query
     */
    public String getSparqlQuery() {
        return this.sparqlQuery;
    }

    /**
     * Sets the sparql query.
     *
     * @param sparqlQuery the new sparql query
     */
    public void setSparqlQuery(final String sparqlQuery) {
        this.sparqlQuery = sparqlQuery;
    }

    /**
     * Gets the period.
     *
     * @return the period
     */
    public String getPeriod() {
        return this.period;
    }

    /**
     * Sets the period.
     *
     * @param period the new period
     */
    public void setPeriod(final String period) {
        this.period = period;
    }

    /**
     * Checks if is all eligible content.
     *
     * @return true, if is all eligible content
     */
    public boolean isAllEligibleContent() {
        return this.allEligibleContent;
    }

    /**
     * Sets the all eligible content.
     *
     * @param allEligibleContent the new all eligible content
     */
    public void setAllEligibleContent(final boolean allEligibleContent) {
        this.allEligibleContent = allEligibleContent;
    }

    /**
     * Checks if is all eligible content within period.
     *
     * @return true, if is all eligible content within period
     */
    public boolean isAllEligibleContentWithinPeriod() {
        return this.allEligibleContentWithinPeriod;
    }

    /**
     * Sets the all eligible content within period.
     *
     * @param allEligibleContentWithinPeriod the new all eligible content within period
     */
    public void setAllEligibleContentWithinPeriod(final boolean allEligibleContentWithinPeriod) {
        this.allEligibleContentWithinPeriod = allEligibleContentWithinPeriod;
    }

    /**
     * Checks if is enabled.
     *
     * @return true, if is enabled
     */
    public boolean isEnabled() {
        return this.enabled;
    }

    /**
     * Sets the enabled.
     *
     * @param enabled the new enabled
     */
    public void setEnabled(final boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * Checks if is use sparql.
     *
     * @return true, if is use sparql
     */
    public boolean isUseSparql() {
        return this.useSparql;
    }

    /**
     * Sets the use sparql.
     *
     * @param useSparql the new use sparql
     */
    public void setUseSparql(final boolean useSparql) {
        this.useSparql = useSparql;
    }

    /** {@inheritDoc} */
    @Override
    public final String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
