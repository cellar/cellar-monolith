package eu.europa.ec.opoce.cellar.common.service;

import eu.europa.ec.opoce.cellar.cl.domain.enums.ProcessMonitorPhase;

/**
 * <class_description> TestingThreadService interface.
 * <br/><br/>
 * <notes> Provides services around thread execution for testing purposes.
 * <br/><br/>
 * ON : 10-10-2023
 *
 * @author EUROPEAN DYNAMICS S.A.
 */
public interface TestingThreadService {
    
    /**
     * Pauses current thread execution in a looped fashion for testing purposes.
     * Utilize configuration parameter 'test.process.monitor.phase' whether to keep pausing or not.
     *
     * @param phase current phase to pause thread execution in
     */
    void checkpointFreezingControl(ProcessMonitorPhase phase);
}
