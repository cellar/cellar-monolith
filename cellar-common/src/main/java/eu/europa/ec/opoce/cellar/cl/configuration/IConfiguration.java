/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.configuration
 *        FILE : IConfiguration.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 02-04-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.configuration;

import eu.europa.ec.opoce.cellar.cl.configuration.ConfigurationPropertyService.EXISTINGPROPS_MODE;
import eu.europa.ec.opoce.cellar.exception.CellarConfigurationException;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;

import java.util.Map;

/**
 * <class_description> Super-interface for all interfaces meant to configure the Cellar.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 02-04-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IConfiguration {

    Marker CONFIG = MarkerManager.getMarker("CONFIG");
    Marker NAL = MarkerManager.getMarker("NAL");
    Marker ONTOLOGY = MarkerManager.getMarker("ONTOLOGY");
    Marker EMBARGO = MarkerManager.getMarker("EMBARGO");
    Marker INDEXATION = MarkerManager.getMarker("INDEXATION");
    Marker VIRTUOSO = MarkerManager.getMarker("VIRTUOSO");
    Marker INGESTION = MarkerManager.getMarker("INGESTION");
    Marker SPARQL_LOAD = MarkerManager.getMarker("SPARQL_LOAD");

    Marker INGESTION_MD_UPDATE = MarkerManager.getMarker("INGESTION_MD_UPDATE");

    /**
     * Synchronize properties this way:<br>
     * - if {@link ifExistsKeepDatabaseValue} is {@link EXISTINGPROPS_MODE.FROM_DB}, the existing properties are synchronized from the database to the memory;<br>
     * - if {@link ifExistsKeepDatabaseValue} is {@link EXISTINGPROPS_MODE.TO_DB}, the existing properties are synchronized from the memory to the database;<br>
     * - if {@link ifExistsKeepDatabaseValue} is {@link EXISTINGPROPS_MODE.NO_SYNC}, the existing properties are not synchronized from/to the database.<br>
     * - the non-existing properties are created into the database and set in memory in any case.
     */
    void synchronizeProperties(final EXISTINGPROPS_MODE syncMode) throws CellarConfigurationException;

    /**
     * Synchronize property {@link key} with value {@link value} this way:<br>
     * - if the property exists and if {@link ifExistsKeepDatabaseValue} is {@link EXISTINGPROPS_MODE.FROM_DB}, the property is synchronized from the database to the memory;<br>
     * - if the property exists and {@link ifExistsKeepDatabaseValue} is {@link EXISTINGPROPS_MODE.TO_DB}, the property is synchronized from the memory to the database;<br>
     * - if the property exists and {@link ifExistsKeepDatabaseValue} is {@link EXISTINGPROPS_MODE.NO_SYNC}, the property is not synchronized from/to the database.<br>
     * - if the property does not exist, it is created into the database and set in memory in any case.
     */
    void synchronizeProperty(final String key, final Object value, final EXISTINGPROPS_MODE syncMode) throws CellarConfigurationException;

    /**
     * Returns true if the property {@key} is persistent, false otherwise.
     */
    boolean isPersistent(final String key);

    /**
     * Provides the configured implementation of {@link ConfigurationPropertyService}.
     */
    ConfigurationPropertyService getConfigurationPropertyService();

    /**
     * Returns all properties in a map where the key is the property's key, and the value a map containing
     * the property's details, here included value and persistency.
     *
     * @return the properties in a map
     */
    Map<String, Map<String, Object>> getAllConfigurationProperties();

}
