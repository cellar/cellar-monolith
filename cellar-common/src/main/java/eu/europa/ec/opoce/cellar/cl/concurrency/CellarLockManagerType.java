/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.concurrency
 *        FILE : CellarLockManagerType.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 03-02-2022
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2022 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.concurrency;

/**
 * Contains the available Cellar Lock Manager types.
 * @author EUROPEAN DYNAMICS S.A.
 */
public enum CellarLockManagerType {
	
	/**
	 * Cellar lock manager for all operation types except for Indexing.
	 */
	NON_INDEXING,
	/**
	 * Indexing-only Cellar lock manager.
	 */
	INDEXING_ONLY;
	
}
