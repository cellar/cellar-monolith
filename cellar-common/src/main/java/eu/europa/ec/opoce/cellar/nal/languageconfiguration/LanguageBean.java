package eu.europa.ec.opoce.cellar.nal.languageconfiguration;

import eu.europa.ec.opoce.cellar.LanguageIsoCode;

import java.util.List;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * <p>LanguageBean class.</p>
 */
public class LanguageBean {

    private String uri;
    private final LanguageIsoCode isoCode = new LanguageIsoCode();
    private String code;

    /**
     * <p>Getter for the field <code>uri</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getUri() {
        return this.uri;
    }

    /**
     * <p>Setter for the field <code>uri</code>.</p>
     *
     * @param uri a {@link java.lang.String} object.
     */
    public void setUri(final String uri) {
        this.uri = uri;
    }

    /**
     * <p>Getter for the field <code>code</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCode() {
        return this.code;
    }

    /**
     * <p>Setter for the field <code>code</code>.</p>
     *
     * @param code a {@link java.lang.String} object.
     */
    public void setCode(final String code) {
        this.code = code;
    }

    /**
     * <p>Getter for the field <code>isoCode</code>.</p>
     *
     * @return a {@link eu.europa.ec.opoce.cellar.LanguageIsoCode} object.
     */
    public LanguageIsoCode getIsoCode() {
        return this.isoCode;
    }

    /**
     * <p>setIsoCodeThreeChar.</p>
     *
     * @param isoCodeThreeChar a {@link java.lang.String} object.
     */
    public void setIsoCodeThreeChar(final String isoCodeThreeChar) {
        this.isoCode.setIsoCodeThreeChar(isoCodeThreeChar);
    }

    /**
     * <p>getIsoCodeThreeChar.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getIsoCodeThreeChar() {
        return this.isoCode.getIsoCodeThreeChar();
    }

    /**
     * <p>setIsoCodeTwoChar.</p>
     *
     * @param isoCodeTwoChar a {@link java.lang.String} object.
     */
    public void setIsoCodeTwoChar(final String isoCodeTwoChar) {
        this.isoCode.setIsoCodeTwoChar(isoCodeTwoChar);
    }

    /**
     * <p>getIsoCodeTwoChar.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getIsoCodeTwoChar() {
        return this.isoCode.getIsoCodeTwoChar();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return this.getIsoCodeThreeChar();
    }

    /**
     * <p>toString.</p>
     *
     * @param languages a {@link java.util.List} object.
     * @return a {@link java.lang.String} object.
     */
    public static String toString(final List<LanguageBean> languages) {
        return StringUtils.join(CollectionUtils.collect(languages, new Transformer<LanguageBean, String>() {

            @Override
            public String transform(final LanguageBean input) {
                return input != null ? input.getIsoCodeThreeChar() : null;
            }
        }), ", ");
    }

    @Override
    public boolean equals(final Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj, new String[] {});
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this, new String[] {});
    }

}
