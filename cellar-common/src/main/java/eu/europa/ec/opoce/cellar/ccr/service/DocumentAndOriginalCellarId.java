/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ccr.service
 *             FILE : DocumentAndOriginalCellarId.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Sep 30, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$$
 *          VERSION : $LastChangedRevision$$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.service;

import eu.europa.ec.opoce.cellar.cmr.ContentType;

import java.util.EnumMap;
import java.util.Map;

/**
 * This class is used during dissemination to retrieve information about the original content if it's a transformed content.
 * So, if the content is not transformed, documentId and originalCellarId have the same value.
 * If the content is a 'transformed content', the originalCellarId point to the original document and the
 * documentId contains the temporary cellar id for this transformed content.
 *
 * @author dcraeye
 */
public class DocumentAndOriginalCellarId {

    public enum Type {
        MANIFESTATION,
        ORIGINAL_CONTENT,
        TRANSFORMED_CONTENT
    }

    private final String documentId;

    /**
     * The original idfentifier point to the 'original' content if it's a 'transformed' content.
     * Otherwise, the originalCellarId has the same value as the documentId.
     */
    private final String originalCellarId;

    private final Type type;

    private Integer docIndex;

    private EnumMap<ContentType, String> versions;

    /**
     * Constructor.
     *
     * @param documentId       the document id
     * @param originalCellarId the original cellar id
     * @param type             the type
     */
    public DocumentAndOriginalCellarId(final String documentId, final String originalCellarId, Map<ContentType, String> versions, final Type type) {
        this.documentId = documentId;
        this.originalCellarId = originalCellarId;
        this.type = type;
        this.versions = versions != null && !versions.isEmpty() ? new EnumMap<>(versions) : new EnumMap<>(ContentType.class);
    }

    /**
     * The identifier of this content.
     *
     * @return the document id
     */
    public String getDocumentId() {
        return this.documentId;
    }

    /**
     * The identifier of the original content for a 'transformed' content.
     * Or the identifier of this document himself if it's not a 'transformed' content.
     *
     * @return the original cellar id
     */
    public String getOriginalCellarId() {
        return this.originalCellarId;
    }

    /**
     * The type kof this content.
     *
     * @return the type
     */
    public Type getType() {
        return this.type;
    }

    /**
     * Gets the doc index.
     *
     * @return the doc index
     */
    public Integer getDocIndex() {
        return this.docIndex;
    }

    /**
     * Sets the doc index.
     *
     * @param docIndex the new doc index
     */
    public void setDocIndex(final Integer docIndex) {
        this.docIndex = docIndex;
    }

    public EnumMap<ContentType, String> getVersions() {
        return versions;
    }

}
