package eu.europa.ec.opoce.cellar.cmr.database;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * <p>Abstract DaoObject class.</p>
 */
public abstract class DaoObject {

    /**
     * <p>sortById.</p>
     *
     * @param daoObjects a {@link java.util.List} object.
     */
    public static void sortById(List<? extends DaoObject> daoObjects) {
        if (daoObjects.size() < 2) {
            return;
        }

        Collections.sort(daoObjects, new Comparator<DaoObject>() {

            @Override
            public int compare(DaoObject o1, DaoObject o2) {
                Long idOne = o1.getId();
                Long idTwo = o2.getId();
                if (idOne == null && idTwo == null) {
                    return 0;
                }
                if (idOne == null) {
                    return 1;
                }
                if (idTwo == null) {
                    return -1;
                }

                return idOne.compareTo(idTwo);
            }
        });
    }

    private Long id;

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.Long} object.
     */
    public final Long getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a long.
     */
    public final void setId(long id) {
        this.id = id;
    }

}
