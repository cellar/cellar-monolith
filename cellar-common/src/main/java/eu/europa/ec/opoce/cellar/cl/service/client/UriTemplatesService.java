package eu.europa.ec.opoce.cellar.cl.service.client;

import eu.europa.ec.opoce.cellar.cl.domain.sparql.UriTemplates;

import java.util.List;

/**
 * <p>ResponseTemplateService interface.</p>
 */
public interface UriTemplatesService {

    /**
     * Get a uriTemplates by id
     * 
     * @param uriTemplatesId the uriTemplatesId
     * @return
     */
    UriTemplates getUriTemplatesById(final Long uriTemplatesId);

    /**
     * Create a uriTemplates
     * 
     * @param uriTemplates the uriTemplates
     * @return a new uriTemplates
     */
    UriTemplates savelUriTemplates(final UriTemplates uriTemplates);

    /**
     * Update a uriTemplates
     * 
     * @param uriTemplates the uriTemplates
     * @return a uriTemplates updated
     */
    UriTemplates updatelUriTemplates(final UriTemplates uriTemplates);

    /**
     * Delete a uriTemplates
     * 
     * @param uriTemplates the uriTemplates
     */
    void deleteUriTemplates(final UriTemplates uriTemplates);

    /**
     * find all uriTemplates
     * 
     * @return all uriTemplates
     */
    List<UriTemplates> findAllUriTemplates();

    /**
     * Find all ordered by sequence
     * 
     * @return all ordered by sequence
     */
    List<UriTemplates> findAllOrderedBySequence();

    /**
     * Find Uri Templates Where sequence is superior to a given sequence
     * 
     * @param sequence the sequence
     * @return  Uri Templates Where sequence is superior to a given sequence
     */
    List<UriTemplates> findUriTemplatesBySequence(Long sequence);

    /**
     * Get xslt files contained in the response templates folder
     * 
     * @return xslt files contained in the response templates folder
     */
    String[] getXslt();
}
