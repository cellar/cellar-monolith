/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.util
 *             FILE : ICellarUserMessageContainer.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 22, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.message;

import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL;

import java.io.Serializable;
import java.util.List;

/**
 * <class_description> Interface of the Cellar user messages container.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 22, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface ICellarUserMessageContainer extends Serializable {

    /**
     * @return the list of info messages.
     */
    List<ICellarUserMessage> getInfoMessages();

    /**
     * @return the list of success messages.
     */
    List<ICellarUserMessage> getSuccessMessages();

    /**
     * @return the list of error messages.
     */
    List<ICellarUserMessage> getErrorMessages();

    /**
     * @return the list of warning messages.
     */
    List<ICellarUserMessage> getWarningMessages();

    /**
     * Add a new message.
     * @param message the message to add
     */
    void addMessage(final ICellarUserMessage message);

    /**
     * Add a new message.
     * @param messageLevel the message level
     * @param messageCode the message code
     */
    void addMessage(final LEVEL messageLevel, final String messageCode);

    /**
     * Add a new message.
     * @param messageLevel the message level
     * @param messageCode the message code
     * @param defaultMessage the default message to use if the message code doesn't exist
     */
    void addMessage(final LEVEL messageLevel, final String messageCode, final String defaultMessage);

    /**
     * Add a new message.
     * @param messageLevel the message level
     * @param messageCode the message code
     * @param defaultMessage the default message to use if the message code doesn't exist
     * @param messageArguments the arguments to insert in the message
     */
    void addMessage(final LEVEL messageLevel, final String messageCode, final String defaultMessage, final Object[] messageArguments);
}
