package eu.europa.ec.opoce.cellar.cl.service.client;

import java.io.File;
import java.util.List;

/**
 * Service for uploading ontology model.
 *
 * @author dbacquel
 *
 */
public interface OntologyLoadConfigService {

    /**
     * Upload ontology model
     *
     * @param itemsFiles the list of item files
     * @return
     */
    boolean uploadOntologyModel(List<File> itemsFiles);
}
