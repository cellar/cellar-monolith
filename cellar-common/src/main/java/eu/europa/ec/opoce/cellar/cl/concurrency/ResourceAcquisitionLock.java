/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.concurrency
 *        FILE : ResourceAcquisitionLock.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 15-12-2022
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2022 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.concurrency;

import java.util.concurrent.locks.ReentrantLock;

/**
 * ReentrantLock implementation for usage during the ConcurrencyController
 * resource acquisition phase.
 * @author EUROPEAN DYNAMICS S.A.
 */
public class ResourceAcquisitionLock extends ReentrantLock {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6480767994882655037L;
	/**
	 * 
	 */
	private CellarLockManagerType type;
	

	public ResourceAcquisitionLock(boolean fair, CellarLockManagerType type) {
		super(fair);
		this.type = type;
	}


	@Override
	public String toString() {
		return type.name().toLowerCase();
	}
	
}
