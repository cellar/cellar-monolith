package eu.europa.ec.opoce.cellar.cl.service.client;

import java.io.File;
import java.io.InputStream;

import eu.europa.ec.opoce.cellar.cl.exception.XslTransformationException;

/**
 * Service to apply xsl transformation.
 * 
 * @author phvdveld
 *
 */
public interface XslTransformerService {

    /**
     * Transforms the given xml file according to the xsl files and writes the result in the
     * foxmlFile.
     * 
     * @param xmlFile
     *            the source xml file to be transformed
     * @param xslFile
     *            the stylesheet to apply transformation
     * @param resultFile
     *            the file to write the resulting transformation
     * @throws XslTransformationException
     *             if a transformation error occured
     */
    void transform(final File xmlFile, final File xslFile, final File resultFile) throws XslTransformationException;

    /**
     * Transforms the given xml file according to the xsl files and writes the result in the
     * foxmlFile.
     * 
     * @param xmlStream
     *            the source xml stream to be transformed
     * @param xslStream
     *            the stylesheet to apply transformation
     * @param resultFile
     *            the file to write the resulting transformation
     * @throws XslTransformationException
     *             if a transformation error occured
     */
    void transform(final InputStream xmlStream, final InputStream xslStream, final File resultFile) throws XslTransformationException;
}
