package eu.europa.ec.opoce.cellar.common.service.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.enums.ProcessMonitorPhase;
import eu.europa.ec.opoce.cellar.common.service.TestingThreadService;
import eu.europa.ec.opoce.cellar.common.util.ThreadUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * <class_description> An implementation of {@link eu.europa.ec.opoce.cellar.common.service.TestingThreadService}.
 * <br/><br/>
 * <notes> Provides services around thread execution for testing purposes.
 * <br/><br/>
 * ON : 10-10-2023
 *
 * @author EUROPEAN DYNAMICS S.A.
 */
@Service
public class TestingThreadServiceImpl implements TestingThreadService {
    
    private static final long THREAD_SLEEP_IN_MILLIS = 2000L;
    
    private final ICellarConfiguration cellarConfiguration;
    
    @Autowired
    public TestingThreadServiceImpl(@Qualifier("cellarConfiguration") ICellarConfiguration cellarConfiguration) {
        this.cellarConfiguration = cellarConfiguration;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void checkpointFreezingControl(ProcessMonitorPhase phase) {
        // Mark the current checkpoint as "reached"
        this.cellarConfiguration.setTestProcessMonitorPhaseReached(true);
        // Pause thread execution
        try {
            while (this.cellarConfiguration.getTestProcessMonitorPhase().equals(phase)) {
                ThreadUtils.waitFor(THREAD_SLEEP_IN_MILLIS, true);
            }
        } finally {
            // Reset checkpoint mark
            this.cellarConfiguration.setTestProcessMonitorPhaseReached(false);
        }
        //One-time custom delay (so that polling from Test-tool yields the correct values)
        ThreadUtils.waitFor(THREAD_SLEEP_IN_MILLIS, true);
    }
}
