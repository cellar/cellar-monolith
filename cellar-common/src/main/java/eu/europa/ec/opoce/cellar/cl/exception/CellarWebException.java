/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.exception
 *        FILE : CellarWebException.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 30-01-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.exception;

import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareException;
import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;

/**
 * This is a {@link CellarWebException} buildable by an {@link HttpStatusAwareExceptionBuilder}.</br>
 * </br> 
 * ON : 30-01-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public class CellarWebException extends HttpStatusAwareException {

    private static final long serialVersionUID = 7001781190985337060L;

    /**
     * Constructs a new exception with its associated builder
     * 
     * @param builder the builder to use for building the exception
     */
    public CellarWebException(final HttpStatusAwareExceptionBuilder<? extends HttpStatusAwareException> builder) {
        super(builder);
    }
}
