/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common
 *             FILE : PriorityThreadPoolExecutor.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 30, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.executor;

import eu.europa.ec.opoce.cellar.common.util.BoundedPriorityBlockingQueue;

import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 30, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class PriorityThreadPoolExecutor extends ThreadPoolExecutor {

    /**
     * Logger instance.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(PriorityThreadPoolExecutor.class);

    /**
     * Instantiates a new priority thread pool executor.
     *
     * @param corePoolSize the core pool size
     * @param maximumPoolSizeCoefficient the maximum pool size coefficient
     * @param queueSize the queue size
     * @param handler the handler
     */
    public PriorityThreadPoolExecutor(int corePoolSize, float maximumPoolSizeCoefficient, int queueSize, RejectedExecutionHandler handler) {
        super(corePoolSize, computeMaximumPoolSize(corePoolSize, maximumPoolSizeCoefficient), 120, TimeUnit.MINUTES,
                new BoundedPriorityBlockingQueue<Runnable>(queueSize), handler);
    }

    /**
     * Sets the pool size.
     *
     * @param corePoolSize the core pool size
     * @param maximumPoolSizeCoefficient the maximum pool size coefficient
     */
    public void setPoolSize(int corePoolSize, float maximumPoolSizeCoefficient) {
        final int maximumPoolSize = computeMaximumPoolSize(corePoolSize, maximumPoolSizeCoefficient);

        this.setCorePoolSize(corePoolSize);
        this.setMaximumPoolSize(maximumPoolSize);

        LOGGER.info("New core-pool-size: '{}' - New maximum-pool-size: '{}'", corePoolSize, maximumPoolSize);
    }

    /**
     * Compute maximum pool size.
     *
     * @param corePoolSize the core pool size
     * @param maximumPoolSizeCoefficient the maximum pool size coefficient
     * @return the int
     */
    private static int computeMaximumPoolSize(int corePoolSize, float maximumPoolSizeCoefficient) {
        if (maximumPoolSizeCoefficient < 1F) {
            maximumPoolSizeCoefficient = 1F;
        }

        return (int) (corePoolSize * maximumPoolSizeCoefficient);
    }
}
