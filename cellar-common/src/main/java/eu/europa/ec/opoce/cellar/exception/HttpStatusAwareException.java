/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.exception
 *        FILE : HttpStatusAwareException.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 30-01-2012
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.exception;

import org.springframework.http.HttpStatus;

/**
 * This is the base class for all exceptions that can be built by an {@link HttpStatusAwareExceptionBuilder}.</br>
 * </br>
 * ON : 30-01-2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public abstract class HttpStatusAwareException extends CellarException {

    private static final long serialVersionUID = 6215626226109257235L;

    /**
     * Constructs a new exception with its associated builder
     *
     * @param builder the builder to use for building the exception
     */
    protected HttpStatusAwareException(final HttpStatusAwareExceptionBuilder<? extends HttpStatusAwareException> builder) {
        super(builder);
        this.subHeaders.put("httpStatus", this.getHttpStatus().toString());
    }

    public HttpStatusAwareException(final ExceptionBuilder<? extends BuildableException> builder) {
        super(builder);
    }

    @SuppressWarnings("unchecked")
    public HttpStatus getHttpStatus() {
        return ((HttpStatusAwareExceptionBuilder<? extends HttpStatusAwareException>) this.builder).httpStatus;
    }

}
