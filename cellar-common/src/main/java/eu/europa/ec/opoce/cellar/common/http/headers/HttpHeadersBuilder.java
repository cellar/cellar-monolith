/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.httpHeaders
 *             FILE : HttpHeadersBuilder.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 22, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.common.http.headers;

import eu.europa.ec.opoce.cellar.common.http.headers.directives.CacheControl;
import eu.europa.ec.opoce.cellar.common.http.headers.directives.Tcn;
import eu.europa.ec.opoce.cellar.common.util.TimeUtils;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 22, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class HttpHeadersBuilder {

    private static final String LINK_SEPARATOR = ", ";

    private transient static final Logger LOGGER = LoggerFactory.getLogger(HttpHeadersBuilder.class);

    private HttpHeaders httpHeaders;
    private final List<String> links;

    private HttpHeadersBuilder() {
        this.httpHeaders = new HttpHeaders();
        this.links = new LinkedList<String>();
    }

    /**
     * Copy constructor
     * @param httpHeaders the HttpHeaders to copy
     */
    private HttpHeadersBuilder(final HttpHeaders httpHeaders) {
        this.httpHeaders = new HttpHeaders();
        //Standard HttpHeaders are unmodifiable, so we copy the values in a new,
        //modifiable HttpHeaders object.
        for (final String key : httpHeaders.keySet()) {
            this.httpHeaders.put(key, httpHeaders.get(key));
        }
        this.links = new LinkedList<String>();
    }

    public static HttpHeadersBuilder get() {
        return new HttpHeadersBuilder();
    }

    public static HttpHeadersBuilder fromHeaders(final HttpHeaders httpHeaders) {
        return new HttpHeadersBuilder(httpHeaders);
    }

    public HttpHeaders getHttpHeaders() {
        if (!this.links.isEmpty()) {
            this.httpHeaders.set(CellarHttpHeaders.LINK, StringUtils.join(this.links, LINK_SEPARATOR));
        }
        return this.httpHeaders;
    }

    public HttpHeadersBuilder withCacheControl(final CacheControl directive) {
        this.httpHeaders.setCacheControl(directive.getDirective());
        return this;
    }

    public HttpHeadersBuilder withDate(final Date date) {
        this.httpHeaders.setDate(date.getTime());
        return this;
    }

    public HttpHeadersBuilder withDateNow() {
        return this.withDate(new Date());
    }

    public HttpHeadersBuilder withLocation(final String uri) {
        try {
            this.httpHeaders.setLocation(new URI(uri));
        } catch (final URISyntaxException e) {
            LOGGER.error("Invalid URI", e);
        }
        return this;
    }

    public HttpHeadersBuilder withETag(final String eTag) {
        this.httpHeaders.setETag(eTag);
        return this;
    }

    public HttpHeadersBuilder withContentLanguage(final String contentLanguage) {
        this.httpHeaders.set(javax.ws.rs.core.HttpHeaders.CONTENT_LANGUAGE, contentLanguage);
        return this;
    }

    public HttpHeadersBuilder withContentType(final MediaType mediaType) {
        if (mediaType != null) {
            return this.withContentType(mediaType.toString());
        }

        return this;
    }

    public HttpHeadersBuilder withLastModified(final Date date) {
        this.httpHeaders.setLastModified(date.getTime());
        return this;
    }

    public HttpHeadersBuilder withContentType(final String contentType) {
        this.httpHeaders.set(javax.ws.rs.core.HttpHeaders.CONTENT_TYPE, contentType);
        return this;
    }

    public HttpHeadersBuilder withDefaultNoticeHeaders(final String eTag, final Date lastModified, final MediaType mediaType) {
        return this.withETag(eTag).withLastModified(lastModified).withCacheControl(CacheControl.NO_CACHE).withContentType(mediaType)
                .withDateNow();
    }

    public HttpHeadersBuilder withDefaultNoticeHeaders(final String eTag, final Date lastModified, final String mediaType) {
        return this.withETag(eTag).withLastModified(lastModified).withCacheControl(CacheControl.NO_CACHE).withContentType(mediaType)
                .withDateNow();
    }

    public HttpHeadersBuilder withAlternates(final Set<String> alternates) {
        this.httpHeaders.add(CellarHttpHeaders.ALTERNATES, StringUtils.join(alternates, ", "));
        return this;
    }

    public HttpHeadersBuilder withTcn(final Tcn directive) {
        this.httpHeaders.set(CellarHttpHeaders.TCN, directive.getDirective());
        return this;
    }

    public HttpHeadersBuilder withMementoDatetime(final Date mementoDatetime) {
        this.httpHeaders.set(CellarHttpHeaders.MEMENTO_DATETIME, TimeUtils.formatHTTP11Date(mementoDatetime));
        return this;
    }

    public HttpHeadersBuilder withLink(final String link) {
        this.links.add(link);
        return this;
    }

    public HttpHeadersBuilder withVary(final String vary) {
        this.httpHeaders.set(CellarHttpHeaders.VARY, vary);
        return this;
    }

}
