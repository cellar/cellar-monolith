/**
 * 
 */
package eu.europa.ec.opoce.cellar.cl.dao.impl;

import eu.europa.ec.opoce.cellar.cl.dao.ConfigurationPropertyDao;
import eu.europa.ec.opoce.cellar.cl.domain.admin.ConfigurationProperty;

import java.util.Collection;
import java.util.List;

import org.springframework.stereotype.Repository;

/**
 * Implementation of the {@link ConfigurationPropertyDao}.
 * @author dbacquel
 *
 */
@Repository(value = "configurationPropertyDbDao")
public class ConfigurationPropertyDaoImpl extends BaseDaoImpl<ConfigurationProperty, Long> implements ConfigurationPropertyDao {

    /**
     * {@inheritDoc}
     */
    @Override
    public void createProperty(final ConfigurationProperty configurationProperty) {
        super.saveObject(configurationProperty);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteProperty(final ConfigurationProperty configurationProperty) {
        super.deleteObject(configurationProperty);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteProperties(final Collection<ConfigurationProperty> configurationProperties) {
        super.deleteObjects(configurationProperties);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ConfigurationProperty> getProperties() {
        return super.findObjectsByNamedQuery("findAllOrderedByKey");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ConfigurationProperty getProperty(final Long id) {
        return super.getObject(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ConfigurationProperty getPropertyByKey(final String identifier) {
        return super.findObjectByNamedQuery("getByKey", identifier);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isPropertyExists(ConfigurationProperty configurationProperty) {
        return this.getProperty(configurationProperty.getId()) != null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateProperty(ConfigurationProperty configurationProperty) {
        super.updateObject(configurationProperty);
    }

}
