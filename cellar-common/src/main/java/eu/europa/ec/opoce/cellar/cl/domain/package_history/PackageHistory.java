package eu.europa.ec.opoce.cellar.cl.domain.package_history;

import eu.europa.ec.opoce.cellar.cl.domain.enums.ProcessStatus;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * <class_description> Package History object.
 * <br/><br/>
 * <notes> Information about the status of a METS package that was just received, such as 'name', 'received date'
 *         and 'preprocess ingestion status'
 * <br/><br/>
 * ON : 13-06-2023
 *
 * @author EUROPEAN DYNAMICS S.A.
 */

@Entity
@Table(name = "PACKAGE_HISTORY")
@NamedQuery(name = "PackageHistory.findByPackageNameAndDate",
                query = "SELECT ph FROM PackageHistory ph WHERE ph.packageName = :phMetsPackageName AND ph.receivedDate = :phReceivedDate")
public class PackageHistory {
    
    /**
     * Primary key.
     */
    @Id
    @GeneratedValue
    private Long id;
    
    /**
     * Identifier of the package.
     */
    @NotNull
    @Column(name = "PACKAGE_UUID")
    private String uuid;
    
    /**
     * Datetime that the package was received at.
     */
    @NotNull
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "RECEIVED_DATE")
    private Date receivedDate;
    
    /**
     * Callback URL to send HTTP status request to, once ingestion is complete.
     */
    @Column(name = "CALLBACK_URL")
    private String callbackUrl;
    
    /**
     * Name of the package.
     */
    @NotNull
    @Column(name = "METS_PACKAGE_NAME")
    private String packageName;
    
    /**
     * Preprocess ingestion status of the package.
     * Refers to status of sequence 'SIP Dependency Check', 'Validate' and 'Convert'.
     */
    @NotNull
    @Column(name = "PREPROCESS_INGESTION_STATUS")
    @Enumerated(EnumType.STRING)
    private ProcessStatus preprocessIngestionStatus;
    
    /**
     * Default constructor.
     */
    public PackageHistory() {
    }

    /**
     * Create a new <code>PackageHistory</code> instance for Mets Upload.
     * @param uuid unique identifier of the package
     * @param receivedDate datetime that the package was received at
     * @param packageName name of the package
     * @param callbackUrl callback URL to send HTTP status request to, once ingestion is complete
     * @param preprocessingIngestionStatus preprocess ingestion status of the package
     */
    public PackageHistory(String uuid, Date receivedDate, String packageName, String callbackUrl, ProcessStatus preprocessingIngestionStatus) {
        this.uuid = uuid;
        this.receivedDate = receivedDate;
        this.callbackUrl = callbackUrl;
        this.packageName = packageName;
        this.preprocessIngestionStatus = preprocessingIngestionStatus;
    }
    
    /**
     * Clone constructor
     * @param packageHistory the original object
     */
    public PackageHistory(PackageHistory packageHistory) {
        this.id = packageHistory.getId();
        this.uuid = packageHistory.getUuid();
        this.receivedDate = new Date(packageHistory.getReceivedDate().getTime());
        this.callbackUrl = packageHistory.getCallbackUrl();
        this.packageName = packageHistory.getPackageName();
        this.preprocessIngestionStatus = packageHistory.getPreprocessIngestionStatus();
    }
    
    public Long getId() {
        return id;
    }
    
    public String getUuid() {
        return uuid;
    }
    
    public Date getReceivedDate() {
        return receivedDate;
    }
    
    public String getCallbackUrl() {
        return callbackUrl;
    }
    
    public String getPackageName() {
        return packageName;
    }
    
    public ProcessStatus getPreprocessIngestionStatus() {
        return preprocessIngestionStatus;
    }
    
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
    
    public void setReceivedDate(Date receivedDate) {
        this.receivedDate = receivedDate;
    }
    
    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }
    
    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
    
    public void setPreprocessIngestionStatus(ProcessStatus preprocessIngestionStatus) {
        this.preprocessIngestionStatus = preprocessIngestionStatus;
    }
    
    
    /**
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "PackageHistory { "
                + "id = '" + this.getId() + "', "
                + "uuid = '" + this.getUuid() + "', "
                + "received date = '" + this.getReceivedDate() + "', "
                + (this.getCallbackUrl() != null ? "callback url = '" + this.getCallbackUrl() + "', " : "")
                + "package name = '" + this.getPackageName() + "', "
                + "preprocess ingestion status = '" + this.getPreprocessIngestionStatus() + "'"
                + " }";
    }
}
