package eu.europa.ec.opoce.cellar.semantic.helper;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;

/**
 * <p>StringHelper class.</p>
 *
 */
public class StringHelper {

    /** Constant <code>newline="\n"</code> */
    public static final String newline = "\n";
    /** Constant <code>tab="\t"</code> */
    public static final String tab = "\t";

    /**
     * <p>Constructor for StringHelper.</p>
     */
    private StringHelper() {
    }

    /**
     * <p>getBytes</p>
     *
     * @param value a {@link java.lang.String} object.
     * @return an array of byte.
     */
    public static byte[] getBytes(String value) {
        return value.getBytes(StandardCharsets.UTF_8);
    }

    /**
     * <p>newlineTab</p>
     *
     * @param numberOfTabs a int.
     * @return a {@link java.lang.String} object.
     */
    public static String newlineTab(int numberOfTabs) {
        Assert.isTrue(numberOfTabs >= 0, "Number of tabs cannot be negative.");
        if (numberOfTabs == 0)
            return newline;

        StringBuilder builder = new StringBuilder(numberOfTabs + 2);
        builder.append(newline);
        for (int i = 0; i < numberOfTabs; i++) {
            builder.append(tab);
        }

        return builder.toString();
    }

    /**
     * <p>fixNewlines</p>
     *
     * @param toFix a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String fixNewlines(String toFix) {
        return toFix.replace("\r\n", "\n").replace("\n\r", "\n").replace("\r", "\n");
    }

    /**
     * <p>format</p>
     *
     * @param message a {@link java.lang.String} object.
     * @param params a {@link java.lang.Object} object.
     * @return a {@link java.lang.String} object.
     */
    public static String format(String message, Object... params) {
        //nasty trick to support older version of slf4j which returns String for this method and not FormattingTuple
        return MessageFormatter.arrayFormat(message, params);
        //    Object object = MessageFormatter.arrayFormat(message, params);
        //    return object instanceof FormattingTuple ? ((FormattingTuple) object).getMessage() : object.toString();
    }

    /**
     * <p>output</p>
     *
     * @param message a {@link java.lang.String} object.
     * @param params a {@link java.lang.Object} object.
     */
    public static void output(String message, Object... params) {
        output(format(message, params));
    }

    /**
     * <p>output</p>
     *
     * @param message a {@link java.lang.String} object.
     */
    public static void output(String message) {
        System.out.println(message);
    }

    /**
     * <p>output</p>
     *
     * @param o a {@link java.lang.Object} object.
     */
    public static void output(Object o) {
        if (o != null) {
            output(o.toString());
        } else {
            output("null");
        }
    }

    /**
     * <p>stringize</p>
     *
     * @param throwable a {@link java.lang.Throwable} object.
     * @return a {@link java.lang.String} object.
     */
    public static String stringize(Throwable throwable) {
        StringWriter stringWriter = new StringWriter();
        PrintWriter printWriter = new PrintWriter(stringWriter);
        throwable.printStackTrace(printWriter);
        return stringWriter.toString();
    }

    /**
     * <p>removeAll</p>
     *
     * @param string a {@link java.lang.String} object.
     * @param toRemoveChars a char.
     * @return a {@link java.lang.String} object.
     */
    public static String removeAll(String string, char... toRemoveChars) {
        for (char toRemoveChar : toRemoveChars) {
            string = StringUtils.remove(string, toRemoveChar);
        }
        return string;
    }

    /**
     * <p>javafy</p>
     *
     * @param string a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String javafy(String string) {
        StringBuilder builder = new StringBuilder(string.length());
        for (int i = 0; i < string.length(); i++) {
            char c = string.charAt(i);
            if ('a' <= c && c <= 'z' || 'A' <= c && c <= 'Z' || '0' <= c && c <= '9') {
                builder.append(c);
            } else {
                builder.append('_');
            }
        }
        return builder.toString();
    }

    /**
     * <p>htmlEncode</p>
     *
     * @param value a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String htmlEncode(String value) {
        return value == null ? "" : value.replace("&", "&amp;").replace(">", "&gt;").replace("<", "&lt;").replace("\"", "&quot;");
    }

    /**
     * <p>multiRemoveEnd</p>
     *
     * @param input a {@link java.lang.String} object.
     * @param removes a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String multiRemoveEnd(String input, String... removes) {
        String result = input;
        for (String remove : removes) {
            result = StringUtils.removeEnd(result, remove);
        }
        return result;
    }
}
