/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.domain.content.mets
 *             FILE : ContentIdentifier.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Sep 29, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$$
 *          VERSION : $LastChangedRevision$$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.domain.content.mets;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.PrefixConfigurationService;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Objects;
import java.util.StringTokenizer;

/**
 * data-structure to group the prefixed and uri version of an identifier.
 *
 * @author ARHS Developments
 * @version $Revision$$
 */
public class ContentIdentifier implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 4918779648355067963L;

    /** The Constant CELLAR_PREFIX. */
    public final static String CELLAR_PREFIX = "cellar";

    /** The prefix configuration. */
    private final transient PrefixConfiguration prefixConfiguration = ServiceLocator.getService(PrefixConfigurationService.class)
            .getPrefixConfiguration();

    /** The identifier. */
    private final String identifier;

    /** The uri. */
    private final String uri;

    /** The doc index. */
    private int docIndex;

    /**
     * Instantiates a new content identifier.
     *
     * @param contentid the contentid
     * @param ignoreUri the ignore uri
     */
    public ContentIdentifier(final String contentid, final boolean ignoreUri) {
        if (ignoreUri) {
            this.identifier = contentid;
            this.uri = null;
        } else {
            final StringTokenizer stk = new StringTokenizer(contentid, ":");
            final String prefixProduction = stk.nextToken();
            final String alias = prefixConfiguration.getPrefixUri(prefixProduction);

            if (null == alias) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.USED_PREFIX_IS_NOT_ALLOWED)
                        .withMessage("Alias not found for production prefix: {} in configuration").withMessageArgs(prefixProduction)
                        .build();
            }

            this.identifier = contentid;
            this.uri = this.identifier.replaceAll(":", "/").replaceFirst(prefixProduction + "/", alias);
        }
    }

    /**
     * Instantiates a new content identifier.
     *
     * @param contentid the contentid
     */
    public ContentIdentifier(final String contentid) {
        this(contentid, false);
    }

    /**
     * Gets the identifier.
     *
     * @return the identifier
     */
    public String getIdentifier() {
        return this.identifier;
    }

    /**
     * Get the encoded identifier.
     *
     * @return the encoded identifier
     * @throws UnsupportedEncodingException the unsupported encoding exception
     */
    public String getEncodedIdentifier() throws UnsupportedEncodingException {
        final String encoded = URLEncoder.encode(this.identifier, "UTF-8");
        return encoded;
    }

    /**
     * Gets the uri.
     *
     * @return the uri
     */
    public String getUri() {
        return this.uri;
    }

    /**
     * Gets the doc index.
     *
     * @return the doc index
     */
    public int getDocIndex() {
        return this.docIndex;
    }

    /**
     * Sets the doc index.
     *
     * @param docIndex the new doc index
     */
    public void setDocIndex(final int docIndex) {
        this.docIndex = docIndex;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return this.identifier;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof ContentIdentifier)) {
            return false;
        }

        final ContentIdentifier other = (ContentIdentifier) obj;
        return Objects.equals(identifier, other.identifier);
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return Objects.hash(identifier);
    }

}
