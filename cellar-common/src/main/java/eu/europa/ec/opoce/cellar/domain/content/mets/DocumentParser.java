/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.domain.content.mets
 *             FILE : DocumentParser.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 5, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.domain.content.mets;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.cl.service.client.MimeTypeCacheService;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.Set;

/**
 * <class_description> Abstract class that validates the mime types of the document.
 *
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 5, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Configurable
public abstract class DocumentParser extends DefaultHandler {

    public static final String METS = "http://www.loc.gov/METS/";

    public static final String XLINK = "http://www.w3.org/1999/xlink";

    /**
     * The manager of mime types supported by Cellar.
     */
    @Autowired(required = true)
    private MimeTypeCacheService mimeTypeCacheService;

    /**
     * Validates the mime type specified in the list of attributes.
     * @param attributes the list of attributes containing the mime type to validate
     */
    protected void validateMimeType(final Attributes attributes) {
        this.validateMimeType(attributes.getValue("MIMETYPE"));
    }

    /**
     * Validates a mime type with the mime types supported by Cellar.
     * @param mimeType the mime type to validate
     */
    protected void validateMimeType(final String mimeType) {
        final Set<String> mimeTypesSet = this.mimeTypeCacheService.getMimeTypesSet(); // gets the set of mime types

        if (mimeType == null || !mimeTypesSet.contains(mimeType))
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("An unexpected error occurred while parsing the mets package: the mime type '{}' is not authorized")
                    .withMessageArgs(mimeType)
                    .withCode(CommonErrors.ERROR_WHILE_PARSING_THE_METS_DOCUMENT)
                    .build();
    }
}
