/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.domain.content.mets.impl
 *             FILE : GetAllPidSynonymsStrategy.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 30, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.domain.content.mets.impl;

import java.util.ArrayList;
import java.util.List;

import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectStrategy;

/**
 * <class_description> Strategy to extract a flat list of digital objects belonging to a given struct map.
 * <br/><br/>
 * ON : Apr 30, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class GetAllDigitalObjectsStrategy implements DigitalObjectStrategy {

    private final List<DigitalObject> digitalObjects;

    /**
     * Constructor.
     */
    public GetAllDigitalObjectsStrategy() {
        this.digitalObjects = new ArrayList<DigitalObject>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void applyStrategy(final DigitalObject digitalObject) {
        this.digitalObjects.add(digitalObject);
    }

    public List<DigitalObject> getDigitalObjects() {
        return digitalObjects;
    }

}
