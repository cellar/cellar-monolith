/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.service.client
 *        FILE : PidManagerService.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 16-03-2012
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.client;

import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.admin.ProductionIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;

import java.util.List;

/**
 * <class_description> Service for providing and finding identifiers.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 16-03-2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface PidManagerService {

    /**
     * Gets the ProductionIdentifier instance with the specified id as a String object.
     *
     * @param id
     *            the identifier of the {@link ProductionIdentifier} to get.
     * @return a {@link ProductionIdentifier}.
     */
    ProductionIdentifier getProductionIdentifierByIdentifier(String id);

    /**
     * Gets the ProductionIdentifier instance with the specified URI .
     *
     * @param uri
     *            the URI of the {@link ProductionIdentifier} to get.
     * @return a {@link ProductionIdentifier}.
     */
    ProductionIdentifier getProductionIdentifierByURI(String uri);

    /**
     * Delete the ProductionIdentifier instance in the persistence layer.
     *
     * @param identifier the identifier
     */
    void deleteProductionIdentifier(final String identifier);

    /**
     * Delete the ProductionIdentifier instance in the persistence layer.
     *
     * @param productionIdentifier
     *            the {@link ProductionIdentifier} to delete.
     */
    void deleteProductionIdentifier(final ProductionIdentifier productionIdentifier);

    /**
     * Saves the ProductionIdentifier instance in the persistence layer.
     *
     * @param productionIdentifier
     *            the {@link ProductionIdentifier} to save.
     * @param ignoreDuplicatedPids
     *            if true, it silently ignores (that is, no exception is thrown) duplicated PIDs.
     */
    void save(ProductionIdentifier productionIdentifier, boolean ignoreDuplicatedPids);

    /**
     * Update the status of a CellarIdentifier instance in the persistence layer.
     *
     * @param cellarIdentifier
     *            the {@link CellarIdentifier} to update.
     */
    void update(CellarIdentifier cellarIdentifier);

    /**
     * Delete the CellarIdentifier instance in the persistence layer.
     *
     * @param cellarIdentifier
     *            the cellar identifier to delete.
     */
    void deleteCellarIdentifier(final String cellarIdentifier);

    /**
     * Check if the given CellarIdentifier exists in the persistence layer.
     *
     * @param cellarIdentifier
     *          the cellar identifier to check
     * @return true, if the cellar identifier exists in the persistence layer, false otherwise.
     */
    boolean isCellarIdentifierExists(final String cellarIdentifier);

    /**
     * Delete the CellarIdentifier instance in the persistence layer.
     *
     * @param cellarIdentifier
     *            the {@link CellarIdentifier} to delete.
     */
    void deleteCellarIdentifier(final CellarIdentifier cellarIdentifier);

    /**
     * Saves all new ProductionIdentifier mapped to the cellarId instance in the persistence layer.
     *
     * @param digitalObject the digital object
     * @param cellarId            the {@link CellarIdentifier} to map with these new contentIds.
     * @param ignoreDuplicatedPids            if true, it silently ignores (that is, no exception is thrown) duplicated PIDs.
     */
    void save(DigitalObject digitalObject, String cellarId, boolean ignoreDuplicatedPids);

    /**
     * Save.
     *
     * @param contentids the contentids
     * @param cellarId the cellar id
     * @param ignoreDuplicatedPids the ignore duplicated pids
     * @param readOnly the read only
     */
    void save(final List<ContentIdentifier> contentids, final String cellarId, final boolean ignoreDuplicatedPids, final Boolean readOnly);

    /**
     * Gets the list of all ProductionIdentifier that have no CellarIdentifier.
     *
     * @return a {@link List} of {@link ProductionIdentifier}.
     */
    List<ProductionIdentifier> getUnboundResources();

    /**
     * Builds and returns the CMR URI for the specified cellarId.
     *
     * @param cellarId
     *            a cellar identifier (ex: '110E8400-E29B-11D4-A716-446655440000.00001.01')
     *
     * @return the CMR URI
     */
    public String getURI(final String cellarId);

    /**
     * Builds and returns the CMR URI for the specified ProductionIdentifier.
     *
     * @param productionIdentifier
     *            the {@link ProductionIdentifier}
     *
     * @return the CMR URI
     */
    String getURI(ProductionIdentifier productionIdentifier);

    /**
     * Creates a ProductionIdentifier with the specified CMR URI.
     *
     * @param uri
     *            the CMR {@link String }
     * @return a {@link ProductionIdentifier}
     */
    ProductionIdentifier createProductionIdentifier(String uri);

    /**
     * Check that all production identifiers have the same cellar identifier.
     *
     * @param productionIdentifiers            the list of production identifiers.
     * @return the unique {@link CellarIdentifier}
     */
    CellarIdentifier checkSameCellarIdentifier(List<ProductionIdentifier> productionIdentifiers);

    /**
     * Goes through the structMap, generates and assigns the ID to each DigitalObject and saves
     * them along with the production identifiers (<i>{@link ProductionIdentifier} instances must be
     * created and saved</i>).
     *
     * @param structMap            the {@link StructMap} to process.
     * @param sipResource            needed by the auditservice.
     */
    boolean assignAndSaveCellarIdentifiers(StructMap structMap, SIPResource sipResource);

    /**
     * Goes through the structMap and saves the cellar IDs for each content Datastream along with their production
     * identifiers.
     *
     * @param structMap
     *            the {@link StructMap} to process.
     * @param sipResource
     *            needed by the auditservice.
     */
    void saveDatastreamIdentifiers(StructMap structMap, SIPResource sipResource);

    /**
     * Goes through the digitalObject and saves the cellar IDs for each content Datastream along with their production
     * identifiers.
     *
     * @param digitalObject
     *            the {@link DigitalObject} to process.
     * @param sipResource
     *            needed by the auditservice.
     */
    void saveDatastreamIdentifiers(DigitalObject digitalObject, SIPResource sipResource);

    /**
     * Map all the given content identifiers to the same cellar identifier.
     * This method also create the missing production identifiers.
     *
     * @param contentIds the content ids
     * @param cellarIdentifier the cellar identifier
     * @return the list
     */
    List<String> mapProductionIdentifiersToCellarIdentifier(List<ContentIdentifier> contentIds, CellarIdentifier cellarIdentifier);

    /**
     * Test if this cellarId identifies a work.
     *
     * @param cellarId the cellar id
     * @return true, if is work
     * @true if this cellarId identifies a work.
     */
    boolean isWork(String cellarId);

    /**
     * Test if this cellarId identifies an expression or an event.
     *
     * @param cellarId the cellar id
     * @return true, if is expression or event
     * @true if this cellarId identifies an expression or an event.
     */
    boolean isExpressionOrEvent(String cellarId);

    /**
     * Test if this cellarId identifies to a manifestation.
     *
     * @param cellarId the cellar id
     * @return true, if is manifestation
     * @true if this cellarId identifies an manifestation.
     */
    boolean isManifestation(String cellarId);

    /**
     * Test if this cellarId identifies to a manifestation content.
     *
     * @param cellarId the cellar id
     * @return true, if is manifestation content
     * @true if this cellarId identifies an manifestation content.
     */
    boolean isManifestationContent(String cellarId);

    /**
     * Get all the cellar identifier of an object tree with all associated content ids.
     *
     * @param cellarId
     *            The cellar identifier from where we want the tree of content'sids to be retrieved.
     * @return A <code>List</code> of content <code>Identifier</code> instances.
     */
    List<Identifier> getTreeContentPid(String cellarId);

    /**
     * Gets the cellar identifier by uuid.
     *
     * @param uuid the uuid
     * @return the cellar identifier by uuid
     */
    CellarIdentifier getCellarIdentifierByUuid(final String uuid);

}
