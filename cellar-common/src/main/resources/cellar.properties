######################################################################################
############################# CELLAR'S MAIN PROPERTY FILE ############################
######################################################################################
# This is the Cellar's main properties file, filled by the Maven build.              #
# It is placed at the root of the classpath, internally to the WAR built.            #
#                                                                                    #
# If needed, it may be modified by the customer by:                                  #
#   1) copy and paste it somewhere in the file system, let's say at path X           #
#   2) fill up EACH property value (no ${*} placeholder is allowed)                  #
#   3) restarting the Cellar with the system option -Dconfiguration.path=X           #
# In such case, the file at X will be loaded instead of the internal one:            #
# this is useful should the customer need to modify the configuration without        #
# rebuilding the Cellar from scratch.                                                #
#                                                                                    #
# All string properties default to the empty string.                                 #
# All numeric properties default to 0.                                               #
# All boolean properties default to false.                                           #
#                                                                                    #
######################################################################################

############################
### CELLAR CONFIGURATION ###
############################

# CELLAR BASIC CONFIGURATION
# if Cellar is running on different instances, each cellar.instanceId must be unique
cellar.instanceId=${cellar.instanceId}
cellar.configuration.version=${cellar.configuration.version}
cellar.configuration.database.hasPriority=${cellar.configuration.database.hasPriority}

# CELLAR FOLDERS
cellar.folder.root=${cellar.folder.root}
cellar.folder.authenticOJReception=${cellar.folder.authenticOJReception}
cellar.folder.dailyReception=${cellar.folder.dailyReception}
cellar.folder.bulkReception=${cellar.folder.bulkReception}
cellar.folder.bulkLowPriorityReception=${cellar.folder.bulkLowPriorityReception}
cellar.folder.authenticOJResponse=${cellar.folder.authenticOJResponse}
cellar.folder.dailyResponse=${cellar.folder.dailyResponse}
cellar.folder.bulkResponse=${cellar.folder.bulkResponse}
cellar.folder.bulkLowPriorityResponse=${cellar.folder.bulkLowPriorityResponse}
cellar.folder.foxml=${cellar.folder.foxml}
cellar.folder.error=${cellar.folder.error}
cellar.folder.temporaryDissemination=${cellar.folder.temporaryDissemination}
cellar.folder.temporaryWork=${cellar.folder.temporaryWork}
cellar.folder.temporaryStore=${cellar.folder.temporaryStore}
cellar.folder.lock=${cellar.folder.lock}
cellar.folder.backup=${cellar.folder.backup}
cellar.folder.export.rest=${cellar.folder.export.rest}
cellar.folder.export.batchJob=${cellar.folder.export.batchJob}
cellar.folder.export.batchJob.temporary=${cellar.folder.export.batchJob.temporary}
cellar.folder.export.updateResponse=${cellar.folder.export.updateResponse}
cellar.folder.cmrlog=${cellar.folder.cmrlog}
cellar.folder.batchJob.sparql.queries=${cellar.folder.batchJob.sparql.queries}
cellar.folder.sparql.responseTemplates=${cellar.folder.sparql.responseTemplates}
cellar.folder.licenseHolder.archive.extraction=${cellar.folder.licenseHolder.archive.extraction}
cellar.folder.licenseHolder.archive.adhocExtraction=${cellar.folder.licenseHolder.archive.adhocExtraction}
cellar.folder.licenseHolder.temporary.archive=${cellar.folder.licenseHolder.temporary.archive}
cellar.folder.licenseHolder.temporary.sparql=${cellar.folder.licenseHolder.temporary.sparql}

# CELLAR SERVER
cellar.server.name=${cellar.server.name}
cellar.server.port=${cellar.server.port}
cellar.server.context=${cellar.server.context}
cellar.server.baseUrl=${cellar.server.baseUrl}

# CELLAR URIS
cellar.uri.disseminationBase=${cellar.uri.disseminationBase}
cellar.uri.resourceBase=${cellar.uri.resourceBase}
cellar.uri.tranformationResourceBase=${cellar.uri.tranformationResourceBase}

# CELLAR APIS
cellar.api.connectionTimeout=${cellar.api.connectionTimeout}
cellar.api.readTimeout=${cellar.api.readTimeout}
cellar.api.baseUrl=${cellar.api.baseUrl}
cellar.api.hostHeader=${cellar.api.hostHeader}
    
# CELLAR ENCRYPTOR
cellar.encryptor.algorithm=${cellar.encryptor.algorithm}
cellar.encryptor.password=${cellar.encryptor.password}

# CELLAR SERVICES
cellar.service.ingestion.enabled=${cellar.service.ingestion.enabled}
cellar.service.ingestion.sipCopy.retryPause.seconds=${cellar.service.ingestion.sipCopy.retryPause.seconds}
cellar.service.ingestion.sipCopy.timeout.seconds=${cellar.service.ingestion.sipCopy.timeout.seconds}
# possible values for cellar.service.ingestion.cmr.lock.mode: oracleExponentialRetry, oracleExponentialBackoffRetry, oracleNoRetry
cellar.service.ingestion.cmr.lock.mode=${cellar.service.ingestion.cmr.lock.mode}
cellar.service.ingestion.cmr.lock.maxRetries=${cellar.service.ingestion.cmr.lock.maxRetries}
cellar.service.ingestion.cmr.lock.ceilingDelay=${cellar.service.ingestion.cmr.lock.ceilingDelay}
cellar.service.ingestion.cmr.lock.base=${cellar.service.ingestion.cmr.lock.base}
cellar.service.ingestion.concurrencyController.autologging.enabled=${cellar.service.ingestion.concurrencyController.autologging.enabled}
cellar.service.ingestion.concurrencyController.autologging.cron.settings=${cellar.service.ingestion.concurrencyController.autologging.cron.settings}
cellar.service.ingestion.concurrencyController.thread.delay.milliseconds=${cellar.service.ingestion.concurrencyController.thread.delay.milliseconds}
cellar.service.ingestion.checkWriteOnceProperties.enabled=${cellar.service.ingestion.checkWriteOnceProperties.enabled}
cellar.service.ingestion.pickup.mode=${cellar.service.ingestion.pickup.mode}
cellar.service.ingestion.pool.threads=${cellar.service.ingestion.pool.threads}
# checks if available threads and then queries the db queue to obtain the head eligible
cellar.service.ingestion.execution.rate=${cellar.service.ingestion.execution.rate}
cellar.service.ingestion.validation.metadata.enabled=${cellar.service.ingestion.validation.metadata.enabled}
cellar.service.ingestion.validation.item.exclusion.enabled=${cellar.service.ingestion.validation.item.exclusion.enabled}
# determines the level of detail of SHACL_REPORT field in an HTTP Status response ("WARNING", "ERROR", "NO")
cellar.status.service.shacl.report.level=${cellar.status.service.shacl.report.level}
cellar.service.nalLoad.decoding.validation.enabled=${cellar.service.nalLoad.decoding.validation.enabled}
cellar.service.nalLoad.enabled=${cellar.service.nalLoad.enabled}
cellar.service.nalLoad.cron.settings=${cellar.service.nalLoad.cron.settings}
cellar.service.sparqlLoad.enabled=${cellar.service.sparqlLoad.enabled}
cellar.service.sparqlLoad.cron.settings=${cellar.service.sparqlLoad.cron.settings}
cellar.service.nalLoad.inference.maxIteration=${cellar.service.nalLoad.inference.maxIteration}
cellar.service.ontoLoad.enabled=${cellar.service.ontoLoad.enabled}
cellar.service.backupProcessedFile.enabled=${cellar.service.backupProcessedFile.enabled}
cellar.service.deleteProcessedData.enabled=${cellar.service.deleteProcessedData.enabled}
cellar.service.indexing.enabled=${cellar.service.indexing.enabled}
cellar.service.indexing.cron.settings=${cellar.service.indexing.cron.settings}
cellar.service.synchronizing.cron.settings=${cellar.service.synchronizing.cron.settings}
cellar.service.indexing.minimumAge.minutes=${cellar.service.indexing.minimumAge.minutes}
# possible values for cellar.service.indexing.minimumPriority: HIGH, NORMAL, LOW
cellar.service.indexing.minimumPriority=${cellar.service.indexing.minimumPriority}
cellar.service.indexing.pool.threads=${cellar.service.indexing.pool.threads}
cellar.service.indexing.concurrencyController.enabled=${cellar.service.indexing.concurrencyController.enabled}
cellar.service.indexing.query.limit.results=${cellar.service.indexing.query.limit.results}
cellar.service.indexing.cleaner.enabled=${cellar.service.indexing.cleaner.enabled}
cellar.service.indexing.cleaner.cron.settings=${cellar.service.indexing.cleaner.cron.settings}
cellar.service.indexing.cleaner.done.minimumAge.days=${cellar.service.indexing.cleaner.done.minimumAge.days}
cellar.service.indexing.cleaner.redundant.minimumAge.days=${cellar.service.indexing.cleaner.redundant.minimumAge.days}
cellar.service.indexing.cleaner.error.minimumAge.days=${cellar.service.indexing.cleaner.error.minimumAge.days}
cellar.service.indexing.scheduled.priority=${cellar.service.indexing.scheduled.priority}
cellar.service.indexing.scheduled.calcInverse=${cellar.service.indexing.scheduled.calcInverse}
cellar.service.indexing.scheduled.calcEmbedded=${cellar.service.indexing.scheduled.calcEmbedded}
cellar.service.indexing.scheduled.calcNotice=${cellar.service.indexing.scheduled.calcNotice}
cellar.service.indexing.scheduled.calcExpanded=${cellar.service.indexing.scheduled.calcExpanded}
cellar.service.indexing.scheduled.selection.pageSize=${cellar.service.indexing.scheduled.selection.pageSize}
cellar.service.indexing.scheduled.batch.sleepTime=${cellar.service.indexing.scheduled.batch.sleepTime}
cellar.service.indexing.scheduled.example.sparql.queries=${cellar.service.indexing.scheduled.example.sparql.queries}
cellar.service.automaticDisembargo.enabled=${cellar.service.automaticDisembargo.enabled}
cellar.service.disembargo.initialDelay=${cellar.service.disembargo.initialDelay}
cellar.service.batchJob.enabled=${cellar.service.batchJob.enabled}
cellar.service.batchJob.sparqlExecuting.cron.settings=${cellar.service.batchJob.sparqlExecuting.cron.settings}
cellar.service.batchJob.batchProcessing.cron.settings=${cellar.service.batchJob.batchProcessing.cron.settings}
cellar.service.batchJob.sparqlUpdate.max.cron=${cellar.service.batchJob.sparqlUpdate.max.cron}
cellar.service.batchJob.pool.corePoolSize=${cellar.service.batchJob.pool.corePoolSize}
cellar.service.batchJob.pool.maxPoolSize=${cellar.service.batchJob.pool.maxPoolSize}
cellar.service.batchJob.pool.queueSize=${cellar.service.batchJob.pool.queueSize}
cellar.service.batchJob.pool.selectionSize=${cellar.service.batchJob.pool.selectionSize}
cellar.service.sparql.timeout=${cellar.service.sparql.timeout}
cellar.service.sparql.max-connections=${cellar.service.sparql.max-connections}
cellar.service.sparql.timeout-connections=${cellar.service.sparql.timeout-connections}
cellar.service.sparql.uri=${cellar.service.sparql.uri}
cellar.service.licenseHolder.enabled=${cellar.service.licenseHolder.enabled}
cellar.service.licenseHolder.worker.cron.settings=${cellar.service.licenseHolder.worker.cron.settings}
cellar.service.licenseHolder.worker.taskExecutor.corePoolSize=${cellar.service.licenseHolder.worker.taskExecutor.corePoolSize}
cellar.service.licenseHolder.worker.taskExecutor.maxPoolSize=${cellar.service.licenseHolder.worker.taskExecutor.maxPoolSize}
cellar.service.licenseHolder.worker.taskExecutor.queueCapacity=${cellar.service.licenseHolder.worker.taskExecutor.queueCapacity}
cellar.service.licenseHolder.worker.archivingBatchExecutor.corePoolSize=${cellar.service.licenseHolder.worker.archivingBatchExecutor.corePoolSize}
cellar.service.licenseHolder.worker.archivingBatchExecutor.maxPoolSize=${cellar.service.licenseHolder.worker.archivingBatchExecutor.maxPoolSize}
cellar.service.licenseHolder.worker.archivingBatchExecutor.queueCapacity=${cellar.service.licenseHolder.worker.archivingBatchExecutor.queueCapacity}
cellar.service.licenseHolder.scheduler.cron.settings=${cellar.service.licenseHolder.scheduler.cron.settings}
cellar.service.licenseHolder.cleaner.cron.settings=${cellar.service.licenseHolder.cleaner.cron.settings}
cellar.service.licenseHolder.keptDays.daily=${cellar.service.licenseHolder.keptDays.daily}
cellar.service.licenseHolder.keptDays.weekly=${cellar.service.licenseHolder.keptDays.weekly}
cellar.service.licenseHolder.keptDays.monthly=${cellar.service.licenseHolder.keptDays.monthly}
cellar.service.licenseHolder.batchSize.perThread=${cellar.service.licenseHolder.batchSize.perThread}
# set cellar.service.licenseHolder.batchSize.folder to 0 or not present to disable the folder size
cellar.service.licenseHolder.batchSize.folder=${cellar.service.licenseHolder.batchSize.folder}
cellar.service.inverseNotices.work.enabled=${cellar.service.inverseNotices.work.enabled}
cellar.service.inverseNotices.expression.enabled=${cellar.service.inverseNotices.expression.enabled}
cellar.service.inverseNotices.manifestation.enabled=${cellar.service.inverseNotices.manifestation.enabled}
cellar.service.inverseNotices.dossier.enabled=${cellar.service.inverseNotices.dossier.enabled}
cellar.service.inverseNotices.event.enabled=${cellar.service.inverseNotices.event.enabled}
cellar.service.inverseNotices.agent.enabled=${cellar.service.inverseNotices.agent.enabled}
cellar.service.inverseNotices.toplevelevent.enabled=${cellar.service.inverseNotices.toplevelevent.enabled}
cellar.service.inverseNotices.limit=${cellar.service.inverseNotices.limit}
cellar.service.inverseNotices.removeNode.enabled=${cellar.service.inverseNotices.removeNode.enabled}
cellar.service.external.startup.timeout.seconds=${cellar.service.external.startup.timeout.seconds}
cellar.service.dissemination.inNoticePropertiesOnly.enabled=${cellar.service.dissemination.inNoticePropertiesOnly.enabled}
cellar.service.dissemination.databaseOptimization.enabled=${cellar.service.dissemination.databaseOptimization.enabled}
cellar.service.dissemination.contentStream.maxSize=${cellar.service.dissemination.contentStream.maxSize}
cellar.service.dissemination.notices.sort.enabled=${cellar.service.dissemination.notices.sort.enabled}
cellar.service.dissemination.decoding.default=${cellar.service.dissemination.decoding.default}
cellar.service.dissemination.memento.enabled=${cellar.service.dissemination.memento.enabled}
cellar.service.dissemination.retrieve.embargoed.resource.enabled=${cellar.service.dissemination.retrieve.embargoed.resource.enabled}
cellar.service.rdfStoreCleaner.enabled=${cellar.service.rdfStoreCleaner.enabled}
cellar.service.rdfStoreCleaner.mode=${cellar.service.rdfStoreCleaner.mode}
cellar.service.rdfStoreCleaner.hierarchies.batchSize=${cellar.service.rdfStoreCleaner.hierarchies.batchSize}
cellar.service.rdfStoreCleaner.resources.batchSize=${cellar.service.rdfStoreCleaner.resources.batchSize}
cellar.service.rdfStoreCleaner.executor.corePoolSize=${cellar.service.rdfStoreCleaner.executor.corePoolSize}
cellar.service.rdfStoreCleaner.executor.maxPoolSize=${cellar.service.rdfStoreCleaner.executor.maxPoolSize}
cellar.service.rdfStoreCleaner.executor.queueCapacity=${cellar.service.rdfStoreCleaner.executor.queueCapacity}
cellar.service.notification.itemsPerPage=${cellar.service.notification.itemsPerPage}
cellar.service.dissemination.daily.oj.cache.refresh.minutes=${cellar.service.dissemination.daily.oj.cache.refresh.minutes}
cellar.service.aligner.executor.enabled=${cellar.service.aligner.executor.enabled}
cellar.service.aligner.executor.corePoolSize=${cellar.service.aligner.executor.corePoolSize}
cellar.service.aligner.executor.maxPoolSize=${cellar.service.aligner.executor.maxPoolSize}
cellar.service.aligner.executor.queueCapacity=${cellar.service.aligner.executor.queueCapacity}
cellar.service.aligner.mode=${cellar.service.aligner.mode}
cellar.service.aligner.hierarchies.batchSize=${cellar.service.aligner.hierarchies.batchSize}
cellar.service.languages.skos.cachePeriod=${cellar.service.languages.skos.cachePeriod}
cellar.service.fileTypes.skos.cachePeriod=${cellar.service.fileTypes.skos.cachePeriod}
cellar.service.integration.archivist.enabled=${cellar.service.integration.archivist.enabled}
cellar.service.integration.archivist.baseUrl=${cellar.service.integration.archivist.baseUrl}
cellar.service.integration.fixity.enabled=${cellar.service.integration.fixity.enabled}
cellar.service.integration.fixity.baseUrl=${cellar.service.integration.fixity.baseUrl}
cellar.service.integration.validation.baseUrl=${cellar.service.integration.validation.baseUrl}
cellar.service.cellarResource.maxElementsInMemory=${cellar.service.cellarResource.maxElementsInMemory}
cellar.service.cellarResource.timeToLiveSeconds=${cellar.service.cellarResource.timeToLiveSeconds}
# Embedded-notice cache: the max size of the in-memory cache.
cellar.service.embeddedNoticeCache.maxBytesLocalHeap=${cellar.service.embeddedNoticeCache.maxBytesLocalHeap}
# Embedded-notice cache: the max size of the disk cache.
cellar.service.embeddedNoticeCache.maxBytesLocalDisk=${cellar.service.embeddedNoticeCache.maxBytesLocalDisk}
# Embedded-notice cache: the time-to-live of the elements.
cellar.service.embeddedNoticeCache.timeToLiveSeconds=${cellar.service.embeddedNoticeCache.timeToLiveSeconds}
# Embedded-notice cache: enables statistics logging for configuration evaluation purposes.
# Not to be used in production environment. Requires JMX.
cellar.service.embeddedNoticeCache.evaluation.statistics.enabled=${cellar.service.embeddedNoticeCache.evaluation.statistics.enabled}
cellar.service.ontology.inference.path=${cellar.service.ontology.inference.path}
# Enable or disable SIP dependency checking. Defaults to true
cellar.service.sipDependencyChecker.enabled=${cellar.service.sipDependencyChecker.enabled}
# Checks file system to sync with db and submits new packages to dependency checker
cellar.service.sipDependencyChecker.cron.settings=${cellar.service.sipDependencyChecker.cron.settings}
cellar.service.sipDependencyChecker.pool.threads=${cellar.service.sipDependencyChecker.pool.threads}
# priority managing: polls db and schedules new packages:
cellar.service.sipQueueManager.cron.settings=${cellar.service.sipQueueManager.cron.settings}
# The file attribute date type to use for package ingestion prioritization.
# Prioritization by date = Allowed values are 'ACCESS', 'MODIFY', and 'CHANGE'. Defaults to 'CHANGE'.
cellar.service.sipQueueManager.fileAttribute.dateType=${cellar.service.sipQueueManager.fileAttribute.dateType}

# CELLAR METS UPLOAD
cellar.mets.upload.callback.timeout=${cellar.mets.upload.callback.timeout}
cellar.mets.upload.buffer.size=${cellar.mets.upload.buffer.size}

# CELLAR STATUS SERVICE
cellar.status.service.baseUrl=${cellar.status.service.baseUrl}
cellar.status.service.package.level.endpoint=${cellar.status.service.package.level.endpoint}
cellar.status.service.object.level.endpoint=${cellar.status.service.object.level.endpoint}
cellar.status.service.lookup.endpoint=${cellar.status.service.lookup.endpoint}

cellar.service.userAccess.notificationEmail.enabled=${cellar.service.userAccess.notificationEmail.enabled}
cellar.service.email.enabled=${cellar.service.email.enabled}
cellar.service.email.cron=${cellar.service.email.cron}
cellar.service.email.cleanup.maxAge.days=${cellar.service.email.cleanup.maxAge.days}
cellar.service.eulogin.userMigration.enabled=${cellar.service.eulogin.userMigration.enabled}

# Set whether the CELLAR BO instance is dedicated for DORIE.
# For FO instances the value is irrelevant.
cellar.service.dorie.dedicated.enabled=${cellar.service.dorie.dedicated.enabled}

# Graceful shutdown
cellar.shutdown.graceful.enabled=${cellar.shutdown.graceful.enabled}
# The value of the timeout applied for each thread pool. That value
# is used to give a chance to the task to finished in the given time.
# -1 => infinite timeout
cellar.shutdown.graceful.timeout=${cellar.shutdown.graceful.timeout}

# CELLAR DATABASE
cellar.database.readonly=${cellar.database.readonly}
cellar.database.rdf.password=${cellar.database.rdf.password}
cellar.database.rdf.ingestion.optimization.enabled=${cellar.database.rdf.ingestion.optimization.enabled}
cellar.database.inClause.params=${cellar.database.inClause.params}
cellar.database.audit.enabled=${cellar.database.audit.enabled}

# CELLAR DATASOURCES
cellar.datasource.cellar=${cellar.datasource.cellar}
cellar.datasource.cmr=${cellar.datasource.cmr}
cellar.datasource.statistics=${cellar.datasource.statistics}
cellar.datasource.idol=${cellar.datasource.idol}
cellar.datasource.virtuoso=${cellar.datasource.virtuoso}

cellar.monitoring.watch.enabled=${cellar.monitoring.watch.enabled}

cellar.prefix=${cellar.prefix}

# CELLAR FEED
cellar.feed.ingestion.description=${cellar.feed.ingestion.description}
cellar.feed.embargo.description=${cellar.feed.embargo.description}
cellar.feed.nal.description=${cellar.feed.nal.description}
cellar.feed.sparqlload.description=${cellar.feed.sparqlload.description}
cellar.feed.ontology.description=${cellar.feed.ontology.description}
cellar.feed.ingestion.title=${cellar.feed.ingestion.title}
cellar.feed.embargo.title=${cellar.feed.embargo.title}
cellar.feed.nal.title=${cellar.feed.nal.title}
cellar.feed.sparqlload.title=${cellar.feed.sparqlload.title}
cellar.feed.ontology.title=${cellar.feed.ontology.title}
cellar.feed.ingestion.item.title=${cellar.feed.ingestion.item.title}
cellar.feed.embargo.item.title=${cellar.feed.embargo.item.title}
cellar.feed.nal.item.title=${cellar.feed.nal.item.title}
cellar.feed.sparqlload.item.title=${cellar.feed.sparqlload.item.title}
cellar.feed.ontology.item.title=${cellar.feed.ontology.item.title}
cellar.feed.ingestion.entry.summary=${cellar.feed.ingestion.entry.summary}
cellar.feed.embargo.entry.summary=${cellar.feed.embargo.entry.summary}
cellar.feed.ontology.entry.summary=${cellar.feed.ontology.entry.summary}
cellar.feed.nal.entry.summary=${cellar.feed.nal.entry.summary}
cellar.feed.sparqlload.entry.summary=${cellar.feed.sparqlload.entry.summary}




############################
### S3 CONFIGURATION ###
############################

s3.service.ingestion.datastream.renaming.enabled=${s3.service.ingestion.datastream.renaming.enabled}

s3.url=${aws.s3.url}
s3.bucket=${aws.s3.bucket}
s3.region=${aws.s3.region}
s3.credentials.access-key=${aws.s3.access-key}
s3.credentials.secret-key=${aws.s3.secret-key}
s3.client.connection-timeout=${s3.client.connection-timeout}
s3.client.socket-timeout=${s3.client.socket-timeout}
s3.unsafe=${s3.unsafe}
s3.rollback.tag=${s3.rollback.tag}
s3.proxy.enabled=${s3.proxy.enabled}
s3.proxy.host=${aws.s3.proxy.host}
s3.proxy.port=${aws.s3.proxy.port}
s3.proxy.username=${aws.s3.proxy.username}
s3.proxy.password=${aws.s3.proxy.password}
s3.retry.enabled=${s3.retry.enabled}
# The max attempts to retry (including the first failure) for the retry strategy of S3. Defaults to 3. Defaults to 3
s3.retry.max-attempts=${s3.retry.max-attempts}
s3.retry.delay=${s3.retry.delay}
s3.retry.random=${s3.retry.random}
s3.retry.multiplier=${s3.retry.multiplier}
s3.retry.max-delay=${s3.retry.max-delay}

##############################
### VIRTUOSO CONFIGURATION ###
##############################

# Virtuoso
virtuoso.ingestion.enabled=${virtuoso.ingestion.enabled}
# possible values for virtuoso.ingestion.retry.strategy: virtuosoExponentialRetry, virtuosoExponentialBackoffRetry, virtuosoNoRetry
virtuoso.ingestion.retry.strategy=${virtuoso.ingestion.retry.strategy}
virtuoso.ingestion.retry.maxRetries=${virtuoso.ingestion.retry.maxRetries}
virtuoso.ingestion.retry.ceilingDelay=${virtuoso.ingestion.retry.ceilingDelay}
virtuoso.ingestion.retry.base=${virtuoso.ingestion.retry.base}
virtuoso.ingestion.technical.enabled=${virtuoso.ingestion.technical.enabled}
virtuoso.ingestion.backup.enabled=${virtuoso.ingestion.backup.enabled}
virtuoso.ingestion.backup.scheduler.cron.settings=${virtuoso.ingestion.backup.scheduler.cron.settings}
virtuoso.ingestion.backup.concurrencyController.enabled=${virtuoso.ingestion.backup.concurrencyController.enabled}
virtuoso.ingestion.backup.max.results=${virtuoso.ingestion.backup.max.results}
virtuoso.ingestion.backup.log.verbose=${virtuoso.ingestion.backup.log.verbose}
virtuoso.ingestion.normalization.enabled=${virtuoso.ingestion.normalization.enabled}
virtuoso.ingestion.skolemization.enabled=${virtuoso.ingestion.skolemization.enabled}
virtuoso.ingestion.backup.exceptionMessages.cachePeriod=${virtuoso.ingestion.backup.exceptionMessages.cachePeriod}
virtuoso.dissemination.sparql.mapper.enabled=${virtuoso.dissemination.sparql.mapper.enabled}
virtuoso.ingestion.linkrot.realignment.enabled=${virtuoso.ingestion.linkrot.realignment.enabled}

###################################
### MISCELLANEOUS CONFIGURATION ###
###################################

# TEST
test.enabled=${test.enabled}
test.database.enabled=${test.database.enabled}
test.database.cron.settings=${test.database.cron.settings}
test.rollback.enabled=${test.rollback.enabled}
test.virtuoso.rollback.enabled=${test.virtuoso.rollback.enabled}
test.indexing.delayExecution.enabled=${test.indexing.delayExecution.enabled}
test.virtuoso.retry.enabled=${test.virtuoso.retry.enabled}
test.s3.retry.enabled=${test.s3.retry.enabled}
test.process.monitor.phase=${test.process.monitor.phase}
test.process.monitor.phase.reached=${test.process.monitor.phase.reached}

# DEBUG
debug.verbose.logging.enabled=${debug.verbose.logging.enabled}

#MAIL Configuration
# The hostname of the mail server to use.
cellar.mail.host=${cellar.mail.host}
# The port of the mail server to use.
cellar.mail.port=${cellar.mail.port}
# The username to use when logging in the mail server. Mandatory when user authentication is enabled.
cellar.mail.smtp.user=${cellar.mail.smtp.user}
# The password to use when logging in the mail server. Mandatory when user authentication is enabled.
cellar.mail.smtp.password=${cellar.mail.smtp.password}
# Set to "false" to disable user authentication with the mail server; enabled by default otherwise.
cellar.mail.smtp.auth=${cellar.mail.smtp.auth}
# Set to "false" to disable STARTTLS; enabled by default otherwise.
cellar.mail.smtp.starttls.enable=${cellar.mail.smtp.starttls.enable}
# Set to "false" to disable strict requirement for STARTTLS; enabled by default otherwise.
cellar.mail.smtp.starttls.required=${cellar.mail.smtp.starttls.required}
# The sender address for the emails sent by CELLAR. If property "cellar.mail.smtp.user" is defined, then this property should be set to the same value.
cellar.mail.smtp.from=${cellar.mail.smtp.from}

#Actuator properties
# Expose the "health" endpoint via the web
management.endpoints.web.exposure.include=health
# Set the base path for actuator endpoints
management.endpoints.web.base-path=/
# Define the port for accessing actuator endpoints
management.port=8095
