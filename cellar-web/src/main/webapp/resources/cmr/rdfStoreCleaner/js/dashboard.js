onload = (function (){
	$("#numberMetadataResourcesButton").click(function() {
		getAndReplace("numberMetadataResources", "numberMetadataResources");
		return false;
	});
	$("#numberMetadataResourcesDiagnosableButton").click(function() {
		getAndReplace("numberMetadataResourcesDiagnosable", "numberMetadataResourcesDiagnosable");
		return false;
	});
	$("#numberMetadataResourcesFixableButton").click(function() {
		getAndReplace("numberMetadataResourcesFixable", "numberMetadataResourcesFixable");
		return false;
	});
	$("#numberRowsCleanerDifferenceButton").click(function() {
		getAndReplace("numberRowsCleanerDifference", "numberRowsCleanerDifference");
		return false;
	});
	$("#numberMetadataResourcesErrorButton").click(function() {
		getAndReplace("numberMetadataResourcesError", "numberMetadataResourcesError");
		return false;
	});

	$("#stopCleaningButton").click(function () {
		var url = $(this).attr("data-url");
		var allowUrlList = ['/admin/cmr/rdfStoreCleaner/stopCleaning'];

		if (allowUrlList.indexOf(url) > -1) {
			document.location.href = url;
		} else {
			alert("Invalid path for redirection");
		}
	});

	$("#startCleaningButton").click(function () {
		var url = $(this).attr("data-url");
		var allowUrlList = ['/admin/cmr/rdfStoreCleaner/startCleaning'];

		if (allowUrlList.indexOf(url) > -1) {
			document.location.href = url;
		}
	});
});

function getAndReplace(id, query) {
	var s = document.getElementById(id);
	$(s).replaceWith(createLoadingSpan(id));
	
	
	$.ajax({
		type: 'GET',
		url: getBaseUrl() + "/admin/cmr/rdfStoreCleaner/" + query,
		async: true,
		success: function(data) {
			var v = document.getElementById(id);
			$(v).replaceWith(createDataSpan(id, data));
		}
	});
}

function createLoadingSpan(id) {
	var loadingSpan = document.createElement("span");
	loadingSpan.setAttribute("id", id);
	
	var loadingImg = document.createElement("img");
	loadingImg.setAttribute("id", "loading");
	loadingImg.setAttribute("src", getBaseUrl() + "/resources/common/admin/images/loading-small.png");
	
	loadingSpan.appendChild(loadingImg);
	
	return loadingSpan;
}

function createDataSpan(id, data) {
	var dataSpan = document.createElement("span");
	dataSpan.setAttribute("id", id);
	dataSpan.innerHTML = data;
	
	return dataSpan;
}