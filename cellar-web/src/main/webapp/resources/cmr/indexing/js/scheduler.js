$(document).ready(function() {
	
	 /* retrieve example query's */
	  $.getJSON(getBaseUrl() + '/admin/cmr/indexing/scheduler/getQueries', function(data) {
	    var queryOptions = '';
	    $.each(data, function(i, query) {
	      queryOptions += '<option value="' + query.id + '">' + query.name + '</option>\n';
	    });
	    $('.exampleQueryWrapper select[name=exampleQuery]').html(queryOptions);
	  });

	  /* select query */
	  $('.exampleQueryWrapper select[name=exampleQuery]').change(function() {
	    $.post(getBaseUrl() + '/admin/cmr/indexing/scheduler/getQuery', { q: $(this).val() }, function(data) {
	      $('#sparqlQuery').val(data);
	    });
	  });
	
});
