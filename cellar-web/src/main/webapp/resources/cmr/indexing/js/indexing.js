$(document).ready(function() {
	
	
	tableSelectTriggers($("select[id$='_select_div']"));
	
	setInterval(
			function () {
				reloadView();
			}
			,10000);


	
	setupTables($('.existing_indexation_requests_datatable'),$('.processed_indexation_requests_datatable'));


	$("#restartIndexing").click(function () {
		var url = $(this).attr("data-url");
		var allowUrlList = ['/admin/cmr/indexing/restart/failed'];

		if (allowUrlList.indexOf(url) > -1) {
			document.location.href = url;
		} else {
			alert("Invalid path for redirection");
		}
	});

	$("#stopIndexing").click(function () {
		var url = $(this).attr("data-url");
		var allowUrlList = ['/admin/cmr/indexing/configuration/stopIndexing'];

		if (allowUrlList.indexOf(url) > -1) {
			document.location.href = url;
		} else {
			alert("Invalid path for redirection");
		}
	});
	
	
});

//this is used to reset the .change trigger on the tables select box
function tableSelectTriggers(element){
	//keep the selected number of rows in memory
	//table class = NAME
	//table select = NAME_select_div
	element.change(function() {
		var selectId = $(this).attr('id');
		var tableClass = selectId.substr(0, selectId.indexOf('_select_div'));
		var selectedValue = $(this).val();
		Cookies.set(tableClass, selectedValue, { expires: 1, path: '/' });

	});
}


//This function will make a AJAX call to refresh the view
function reloadView() {
	  $.ajax({
			type: "GET",
			url: 'dashboard',
			cache: false,
			 success : function(view) {
				 //the controller returns the entire html page
				 //I'm using jquery to parse it and to retrieve the div that we want to update/replace
				 var responseView = $($.parseHTML(view));
				 var newElement = responseView.find('.dashboard');

				 //replace
				 $('.dashboard').replaceWith(newElement);
				 setupTables(newElement.find('.existing_indexation_requests_datatable'),newElement.find('.processed_indexation_requests_datatable'));

				 tableSelectTriggers(newElement.find("select[id$='_select_div']"));

	       },
	       error : function() {
	      	 console.log('An error occurred when trying to update the view with ajax/jquery');
	       }
		 });




}
//configure the datatables of the page
function setupTables(existing_indexation_requests_element,processing_indexation_requests_element){
	var existing_indexation_Class = existing_indexation_requests_element.attr('class');
	var existing_indexation_datatable_number_of_rows = 5;
	if(Cookies.get(existing_indexation_Class)){
		existing_indexation_datatable_number_of_rows = parseInt(Cookies.get(existing_indexation_Class));
	}
	var cTable=existing_indexation_requests_element.DataTable();
	cTable.clear();

	var cTable2=processing_indexation_requests_element.DataTable();
	cTable2.clear();
	//indexation requests table
	existing_indexation_requests_element.dataTable({
		"iDisplayLength": existing_indexation_datatable_number_of_rows,
		"bSort": true,
		"bRetrieve" : true,
		"sPaginationType": "full_numbers",
		"aoColumns": [//we need to set class names to set a fixed width to each column
		              {  "sClass": "priority" },
		              {  "sClass": "groupURI" },
		              {  "sClass": "timestamp" },
		              { "sClass": "count" },
		          ],
		          "oLanguage": {
		              "sLengthMenu": 'Display <select id="existing_indexation_requests_datatable_select_div">'+
		              '<option value="5">5</option>'+
		              '<option value="10">10</option>'+
		                  '<option value="20">20</option>'+
		                  '<option value="40">40</option>'+
		                  '<option value="100">100</option>'+
		                  '<option value="200">200</option>'+
		                  '</select> records'
		          }
    } );

	var processing_indexation_Class = processing_indexation_requests_element.attr('class');
	var processing_indexation_datatable_number_of_rows = 5;
	if(Cookies.get(processing_indexation_Class)){
		processing_indexation_datatable_number_of_rows = parseInt(Cookies.get(processing_indexation_Class));
	}
	//processed requests table
	processing_indexation_requests_element.dataTable({
		"iDisplayLength": processing_indexation_datatable_number_of_rows,
		"bSort": true,
		"bRetrieve" : true,
		"sPaginationType": "full_numbers",
		"aoColumns": [//we need to set class names to set a fixed width to each column
		              {  "sClass": "priority" },
		              {  "sClass": "groupURI" },
		              { "sClass": "count" },
		          ],
		          "oLanguage": {
		              "sLengthMenu": 'Display <select id="processed_indexation_requests_datatable_select_div">'+
		              '<option value="5">5</option>'+
		              '<option value="10">10</option>'+
		                  '<option value="20">20</option>'+
		                  '<option value="40">40</option>'+
		                  '<option value="100">100</option>'+
		                  '<option value="200">200</option>'+
		                  '</select> records'
		          }
    } );
}

