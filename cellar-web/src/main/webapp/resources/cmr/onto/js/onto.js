$(document).ready(function() {
 //
});


$(function () { 
    'use strict';
    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
    	autoUpload: false,
    	singleFileUploads:false,
    	sequentialUploads: false,//once we adopt the Ontology Policy File this should be set to true
    	dataType: 'json',
    	maxFileSize: 10485760, // 10 MB
    	downloadTemplateId: null,
    	uploadTemplateId: null,
    	uploadTemplate: function (o) {
            var rows = $();
            $.each(o.files, function (index, file) {
                var row = $('<tr class="template-upload fade">' +
                    '<td style="width:20%!important;">'+
                    	'<p class="name"></p>' +
                    	'<div class="error"></div>' +
                    '</td>' +
                    '<td style="width:10%!important;">'+
                    	'<p class="size"></p>' +
                    '</td>' +
                    '<td style="width:70%!important;">'+
                    	'<div class="progress"></div>' +
                    '</td>' +
                    '<td>' +
                    	(!index && !o.options.autoUpload ? '<button class="start" style="display:none;" disabled>Start</button>' : '') +
                        (!index ? '<button class="cancel" style="display:none;" >Cancel</button>' : '') +
                    '</td>' +
                    '</tr>');
                row.find('.name').text(file.name);
                row.find('.size').text(o.formatFileSize(file.size));
                if (file.error) {
                    row.find('.error').text(file.error);
                }
                rows = rows.add(row);
            });
            return rows;
        },
        done: function (e, data) {
        	//adapt progress bar
            $(this).find('.progressbar').progressbar({ value: 100 });
            location.reload(); 
            
        }
    }).bind('fileuploadchange', function (e, data) {clearSelection();});
    
    $('button.cancel').click(function (e) {
    	clearSelection();
    });

});

//clears the content of the files table
//hides the progress bar
//clears messages
function clearSelection(){
	$("table tbody.files").empty();
	$(this).find('.progressbar').hide();
	$("div[class^='userMessage']").hide();
}
