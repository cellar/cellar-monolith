$(document).ready(function() {
  $('#getLog').click(function() {
    //Open Redirect: https://learn.snyk.io/lessons/open-redirect/javascript/
    var values = $('#logSelect').val();
    var regexPattern = /^cmr_.*log$/;
    var allowUrlList = ['/admin/cmr/log/logFile'];

    if (values.length > 0
        && regexPattern.test($('#logSelect').val())
        && allowUrlList.indexOf('/admin/cmr/log/logFile') > -1) {
      values.join(',');
      var url = '/admin/cmr/log/logFile';
      var redirectionUrl = url + '?q=' + values
      window.open(redirectionUrl);
    }
    else {
      alert("Select at least one log file.");
    }
  });

  $('#clearLogs').click(function(e) {
    var input = prompt("Clear all logs before this date: (YYYY-MM-DD).");
    if (validateDate(input)) {
      var clear = confirm("Do you really want to remove all log files before " + input + "?");
      if (clear) {
        $('.content .log-wrapper .log-status').text("Deleting log files...");
        $('.content .log-wrapper .log-status').css("display", "block");
        $.post("log/clear",
               {
                 date: input
               },
               function(data) {
                 $('.content .log-wrapper .log-status').css("display", "none");
                 location.reload(true);
               }
            );
      }
    }
    else {
      alert("Invalid date specified.");
    }
  });

  /* do some very basic date validation */
  function validateDate(date) {
    if (trim(date).length == 10) {
      var year = parseInt(date.substring(0, 4));
      var month = parseInt(date.substring(5, 7));
      var day = parseInt(date.substring(8));
      if ((month > 0 && month <= 12) && (day > 0 && day <= 31) && year > 2000) return true;
    }
    return false;
  }

  function trim(value) {
    value = value.replace(/^\s+/, '');
    value = value.replace(/\s+$/, '');
    return value;
  }

});