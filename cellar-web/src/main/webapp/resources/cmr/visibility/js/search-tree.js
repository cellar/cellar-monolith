onload = (function(){	
	$(document).ready(function() {
		$.ajaxSetup({ cache: false });
	});
	var tree = document.getElementById("tree");
	if(tree){
		
		this.listItem = function(li){
			if(li.getElementsByTagName("ul").length > 0){
				var ul = li.getElementsByTagName("ul")[0];
				ul.style.display = "none";
				var span = document.createElement("span");
				span.className = "collapsed";
				span.onclick = function(){
					var ulParent = ul.parentNode;
					if(this.className == "collapsed" && ulParent.tagName == "LI" && ulParent.className == "notExpandAll") {
						var asyncItems = ulParent.getElementsByTagName("li");
						var asyncItem;
						var i;
						for(i = 0; i < asyncItems.length; i++) {
							asyncItem = asyncItems[i].children[0];
							if($(asyncItem).children("div.visible").is(":empty")) {								
								$(asyncItem).children("div.visible").replaceWith(createLoadingVisibleDiv());
								
								$.ajax({
									type: 'GET',
									url: getBaseUrl() + "/admin/cmr/visibility/search/visible-content?cellarId=" + asyncItems[i].id,
									ajaxI: i,
									async: true,
									success: function(data) {
										i = this.ajaxI;
										var contentDiv = asyncItems[i].children[0];
										if(data == "true") {
											$(contentDiv).children("div.visible").replaceWith(createEnabledVisibleDiv());
											var hideLink = createHideLink(asyncItems[i].id);
											$(contentDiv).children("div.actions").append(hideLink);
										} else {
											$(contentDiv).children("div.visible").replaceWith(createDisabledVisibleDiv());
											var showLink = createShowLink(asyncItems[i].id);
											$(contentDiv).children("div.actions").append(showLink);
										}
									}
								});
							}
						}
					}
					ul.style.display = (ul.style.display == "none") ? "block" : "none";
					this.className = (ul.style.display == "none") ? "collapsed" : "expanded";
				};
				li.appendChild(span);
			};
		};
		
		var items = tree.getElementsByTagName("li");
		for(var i=0;i<items.length;i++){
			listItem(items[i]);
		};
	};
	$("a#expandAll").click(function() {
		var item;
		var ulParent;
		var spanParent;
		var items = tree.getElementsByTagName("ul");
		for(var i=0;i<items.length;i++){
			item = items[i];
			ulParent = item.parentNode;
			if(ulParent.className != "notExpandAll") {
				item.style.display = "block";
			}
		};
		items = tree.getElementsByTagName("span");
		for(var i=0;i<items.length;i++){
			item = items[i];
			spanParent = item.parentNode;
			if(spanParent.className != "notExpandAll") {
				item.className = "expanded";
			}
		};
		return false;
	});
	
	$(".workEmbargoer").click(function() {
		$("#workEmbargoerDialog").data('link', this).dialog("open");
		$("#embargoerText").show();
		$("#dateOfEmbargoErrors").hide();
		$("#embargoerWaiting").hide();
		$("#embargoerError").hide();
		$(".ui-dialog-buttonpane button:contains('Embargo')").button("enable");
		return false;
	});
	
	$("#workEmbargoerDialog").dialog({
		autoOpen: false,
		width: 450,
		modal: true,
		buttons: {
			'Close':function() {
				$(this).dialog('close');
			},
			'Embargo': function() {
				var dialog = $(this);
				var dateOfEmbargo = $("#dateOfEmbargo").val();
				
				if(isDateOfEmbargoValid(dateOfEmbargo)) {
					var path = dialog.data('link').href + "&dateOfEmbargo=" + dateOfEmbargo;
					var cellarId = dialog.data('link').id.substring(8);
	
					$(".ui-dialog-buttonpane button:contains('Embargo')").button("disable");
					$("#embargoerText").hide();
					$("#embargoerWaiting").show();
					
					$.ajax({
						type: 'GET',
						url: path,
						async: true,
						success: function(data) {
							if(data) {
								$("#embargoerWaiting").hide();
								$("#embargoerError").html(data);
								$("#embargoerError").show();
							} else {
								dialog.dialog('close');
								$(location).attr('href', getBaseUrl() + '/admin/cmr/visibility/search?objectId=' + cellarId);
							}
						}
					});
				}
			}
		}
	});
	
	
	

	
	$(".workDisembargoer").click(function() {
		$("#workDisembargoerDialog").data('link', this).dialog("open");
		$("#disembargoerText").show();
		$("#disembargoerWaiting").hide();
		$("#disembargoerError").hide();
		$(".ui-dialog-buttonpane button:contains('Disembargo')").button("enable");
		return false;
	});
	
	$("#workDisembargoerDialog").dialog({
		autoOpen: false,
		width: 450,
		modal: true,
		buttons: {
			'Close':function() {
				$(this).dialog('close');
			},
			'Disembargo': function() {
				var dialog = $(this);
				var path = dialog.data('link').href;
				var cellarId = dialog.data('link').id.substring(8);

				$(".ui-dialog-buttonpane button:contains('Disembargo')").button("disable");
				$("#disembargoerText").hide();
				$("#disembargoerWaiting").show();
				
				$.ajax({
					type: 'GET',
					url: path,
					async: true,
					success: function(data) {
						if(data) {
							$("#disembargoerWaiting").hide();
							$("#disembargoerError").html(data);
							$("#disembargoerError").show();
						} else {
							dialog.dialog('close');
							$(location).attr('href', getBaseUrl() + '/admin/cmr/visibility/search?objectId=' + cellarId);
						}
					}
				});
			}
		}
	});
	
	
	
	$(".contentDeleter").click(function() {
		$("#contentDeleterDialog").data('link', this).dialog("open");
		
		$("#contentDeleterText").show();
		$("#contentDeleterWaiting").hide();
		$("#contentDeleterError").hide();
		$(".ui-dialog-buttonpane button:contains('Delete')").button("enable");
		
		return false;
	});
	
	$("#contentDeleterDialog").dialog({
		autoOpen: false,
		width: 450,
		modal: true,
		buttons: {
			'Close':function() {
				$(this).dialog('close');
			},
			'Delete': function() {
				var dialog = $(this);
				var path = dialog.data('link').href;
				var cellarId = dialog.data('link').id.substring(8);

				$(".ui-dialog-buttonpane button:contains('Delete')").button("disable");
				$("#contentDeleterText").hide();
				$("#contentDeleterWaiting").show();
				
				$.ajax({
					type: 'GET',
					url: path,
					async: true,
					success: function(data) {
						if(data) {
							$("#contentDeleterWaiting").hide();
							$("#contentDeleterError").html(data);
							$("#contentDeleterError").show();
						} else {
							var element = document.getElementById(cellarId);
							element.parentNode.removeChild(element);
							dialog.dialog('close');
						}
					}
				});
			}
		}
	});
	
	$("a#collapseAll").click(function() {
		var items;
		var item;
		
		items = tree.getElementsByTagName("ul");
		for(var i=0;i<items.length;i++){
			item = items[i];
			item.style.display = "none";
		};
		
		items = tree.getElementsByTagName("span");
		for(var i=0;i<items.length;i++){
			item = items[i];
			item.className = "collapsed";
		};
		return false;
	});
	
	$("#pidsDialog").dialog({
		autoOpen: false,
		width: 450,
		modal: true,
		buttons: {
			'Close':function() {
				$(this).dialog('close');
			}
		}
	});

	$(".pidsDialogOpener").click(function() {		
		$.ajax({
			type: 'GET',
			dataType: 'json',
			async: true,
			url: $(this).attr("href"),
			success: function(data, textStatus, jqXHR) {
				$("#pidsDialog").data('link', this).dialog("open");
				var psid = "<ul>";
				jQuery.each(data, function(i, val) {
					psid = psid + "<li>" + val + "</li>";
				});
				psid = psid + "</ul>";
				$("#pidsText").html(psid);
			}
		});
		
		return false;
	});
	
	$("#embargoerDialog").dialog({
		autoOpen: false,
		width: 450,
		modal: true,
		buttons: {
			'Close':function() {
				$("form[id='embargoWorkForm']").submit();
			}
		}
	});
	
	$("#errorDialog").dialog({
		autoOpen: false,
		width: 450,
		modal: true,
		buttons: {
			'Close':function() {
				$(this).dialog('close');
			}
		}
	});

    $('#dateOfEmbargo').datetimepicker({
        dateFormat: 'yymmdd',
        separator: '',
        constrainInput: false,
        timeFormat: 'HHmm',
		showSecond: false,
        showMillisec: false,
		showMicrosec: false,
		showTimezone: false
    });
});

function showError(data) {
	$("#errorDialog").dialog("open");
	$("#errorText").html(data);
}

function createShowLink(cellarId) {
	var showLink = document.createElement("a");
	showLink.setAttribute("class", "contentShower");
	showLink.setAttribute("style", "padding-left:0px;");
	showLink.setAttribute("href", getBaseUrl() + "/admin/cmr/visibility/search/show-content?cellarId=" + cellarId);
	showLink.setAttribute("id", "shower_" + cellarId);
	showLink.setAttribute("title", "Show");
	
	var showImg = document.createElement("img");
	showImg.setAttribute("id", "show");
	showImg.setAttribute("src", getBaseUrl() + "/resources/common/admin/images/show.png");

	showLink.appendChild(showImg);
	
	$(showLink).click(function() {
		var path = this.href;
		var showLink = $(this);
		showLink.hide();
		
		$.ajax({
			type: 'GET',
			url: path,
			async: true,
			success: function(data) {
				if(data) {
					showError(data);
					showLink.show();
				} else {
					var hideLink = createHideLink(cellarId);
					
					showLink.replaceWith(hideLink);
					$(hideLink.parentNode.parentNode).children("div.visible").replaceWith(createEnabledVisibleDiv());
				}
			}
		});
		
		return false;
	});
	
	return showLink;
}

function createHideLink(cellarId) {
	var hideLink = document.createElement("a");
	hideLink.setAttribute("class", "contentHider");
	hideLink.setAttribute("style", "padding-left:0px;");
	hideLink.setAttribute("href", getBaseUrl() + "/admin/cmr/visibility/search/hide-content?cellarId=" + cellarId);
	hideLink.setAttribute("id", "hider_" + cellarId);
	hideLink.setAttribute("title", "Hide");
	
	var hideImg = document.createElement("img");
	hideImg.setAttribute("id", "hide");
	hideImg.setAttribute("src", getBaseUrl() + "/resources/common/admin/images/hide.png");

	hideLink.appendChild(hideImg);
	
	$(hideLink).click(function() {
		var path = this.href;
		var hideLink = $(this);
		hideLink.hide();
		
		$.ajax({
			type: 'GET',
			url: path,
			async: true,
			success: function(data) {
				if(data) {
					showError(data);
					hideLink.show();
				} else {
					var showLink = createShowLink(cellarId);
					
					hideLink.replaceWith(showLink);
					$(showLink.parentNode.parentNode).children("div.visible").replaceWith(createDisabledVisibleDiv());
				}
			}
		});
		
		return false;
	});
	
	return hideLink;
}

function createLoadingVisibleDiv() {
	var loadingVisibleDiv = document.createElement("div");
	loadingVisibleDiv.setAttribute("align", "center");
	loadingVisibleDiv.setAttribute("class", "visible");
	
	var loadingImg = document.createElement("img");
	loadingImg.setAttribute("id", "loading");
	loadingImg.setAttribute("src", getBaseUrl() + "/resources/common/admin/images/loading-small.png");
	
	loadingVisibleDiv.appendChild(loadingImg);
	
	return loadingVisibleDiv;
}

function createEnabledVisibleDiv() {
	var enabledVisibleDiv = document.createElement("div");
	enabledVisibleDiv.setAttribute("align", "center");
	enabledVisibleDiv.setAttribute("class", "visible");
	
	var enabledVisibleImg = document.createElement("img");
	enabledVisibleImg.setAttribute("id", "enabled");
	enabledVisibleImg.setAttribute("src", getBaseUrl() + "/resources/common/admin/images/enabled.png");
	
	enabledVisibleDiv.appendChild(enabledVisibleImg);
	
	return enabledVisibleDiv;
}

function createDisabledVisibleDiv() {
	var disabledVisibleDiv = document.createElement("div");
	disabledVisibleDiv.setAttribute("align", "center");
	disabledVisibleDiv.setAttribute("class", "visible");
	
	var disabledVisibleImg = document.createElement("img");
	disabledVisibleImg.setAttribute("id", "disabled");
	disabledVisibleImg.setAttribute("src", getBaseUrl() + "/resources/common/admin/images/disabled.png");
	
	disabledVisibleDiv.appendChild(disabledVisibleImg);
	
	return disabledVisibleDiv;
}

function isDateOfEmbargoValid(dateOfEmbargo) {
	var isValid = dateOfEmbargo.match("^(?:(?:(?:(?:(?:[13579][26]|[2468][048])00)|(?:[0-9]{2}(?:(?:[13579][26])|(?:[2468][048]|0[48]))))(?:(?:(?:09|04|06|11)(?:0[1-9]|1[0-9]|2[0-9]|30))|(?:(?:01|03|05|07|08|10|12)(?:0[1-9]|1[0-9]|2[0-9]|3[01]))|(?:02(?:0[1-9]|1[0-9]|2[0-9]))))|(?:[0-9]{4}(?:(?:(?:09|04|06|11)(?:0[1-9]|1[0-9]|2[0-9]|30))|(?:(?:01|03|05|07|08|10|12)(?:0[1-9]|1[0-9]|2[0-9]|3[01]))|(?:02(?:[01][0-9]|2[0-8])))))(?:0[0-9]|1[0-9]|2[0-3])(?:[0-5][0-9])$");

	if(!isValid) {
		$("#dateOfEmbargoErrors").show();
		return null;
	}
	return dateOfEmbargo;
}
