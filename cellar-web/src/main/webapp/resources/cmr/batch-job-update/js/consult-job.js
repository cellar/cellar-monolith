onload = (function (){
    $("#dialog").dialog({
        autoOpen: false,
		width: 800,
		height:600
    });

    $("a#dialogOpener").click(function() {
        $("#dialog").dialog("open");
        return false;
    });

    $("#backButton").click(function () {
        var url = $(this).attr("data-url");
        var allowUrlList = ['/admin/cmr/indexing/reindex', '/admin/cmr/export/job', '/admin/cmr/visibility/job',
            '/admin/cmr/update/job/last-modification-date', '/admin/cmr/update/job/sparql-update'];

        if (allowUrlList.indexOf(url) > -1) {
            document.location.href = url;
        } else {
            alert("Invalid path for redirection");
        }
    });
});