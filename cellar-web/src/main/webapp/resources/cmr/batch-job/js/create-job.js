$(document).ready(function() {
  /* retrieve example query's */
  $.getJSON(getBaseUrl() + '/admin/cmr/batchJob/getQueries', function(data) {
    var queryOptions = '';
    $.each(data, function(i, query) {
      queryOptions += '<option value="' + query.id + '">' + query.name + '</option>\n';
    });
    $('.exampleQueryWrapper select[name=exampleQuery]').html(queryOptions);
  });

  /* select query */
  $('.exampleQueryWrapper select[name=exampleQuery]').change(function() {
    $.post(getBaseUrl() + '/admin/cmr/batchJob/getQuery', { q: $(this).val() }, function(data) {
      $('#job #sparqlQuery').val(data);
    });
  });

  $("#cancelButton").click(function () {
    var url = $(this).attr("data-url");
    var allowUrlList = ['/admin/cmr/indexing/reindex', '/admin/cmr/export/job', '/admin/cmr/visibility/job',
      '/admin/cmr/update/job/last-modification-date', '/admin/cmr/update/job/sparql-update', '/admin/cmr/delete/job'];

    if (allowUrlList.indexOf(url) > -1) {
      document.location.href = url;
    } else {
      alert("Invalid path for redirection");
    }
  });
});
