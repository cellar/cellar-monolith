$(document).ready(function() {
    $("#addJobButton").click(function () {
        var url = $(this).attr("data-url");
        var allowUrlList = ['/admin/cmr/indexing/reindex/create', '/admin/cmr/export/job/create', '/admin/cmr/visibility/job/create',
            '/admin/cmr/update/job/last-modification-date/create', '/admin/cmr/update/job/sparql-update/create', '/admin/cmr/delete/job/create'];

        if (allowUrlList.indexOf(url) > -1) {
            document.location.href = url;
        } else {
            alert("Invalid path for redirection");
        }
    });

    $("#stopSchedulers").click(function () {
        var url = $(this).attr("data-url");
        var allowUrlList = ['/admin/cmr/indexing/reindex/stopSchedulers', '/admin/cmr/export/job/stopSchedulers',
            '/admin/cmr/visibility/job/stopSchedulers', '/admin/cmr/update/job/last-modification-date/stopSchedulers',
            '/admin/cmr/update/job/sparql-update/stopSchedulers', '/admin/cmr/delete/job/stopSchedulers'];

        if (allowUrlList.indexOf(url) > -1) {
            document.location.href = url;
        } else {
            alert("Invalid path for redirection");
        }
    });

    $("#restartSchedulers").click(function () {
        var url = $(this).attr("data-url");
        var allowUrlList = ['/admin/cmr/indexing/reindex/restartSchedulers', '/admin/cmr/export/job/restartSchedulers',
            '/admin/cmr/visibility/job/restartSchedulers', '/admin/cmr/update/job/last-modification-date/restartSchedulers',
            '/admin/cmr/update/job/sparql-update/restartSchedulers', '/admin/cmr/delete/job/restartSchedulers'];

        if (allowUrlList.indexOf(url) > -1) {
            document.location.href = url;
        } else {
            alert("Invalid path for redirection");
        }
    });
});
