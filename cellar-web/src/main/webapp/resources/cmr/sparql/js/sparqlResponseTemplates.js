$(document).ready(function() {
    $("#addConfigurationButton").click(function () {
        var url = $(this).attr("data-url");
        var allowUrlList = ['/admin/cmr/sparql/templates/create'];

        if (allowUrlList.indexOf(url) > -1) {
            document.location.href = url;
        } else {
            alert("Invalid path for redirection");
        }
    });
});