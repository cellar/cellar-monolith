$(document).ready(function() {

  $('#check').click(function(e) {
    $('.embargo-list-wrapper input[type=checkbox]').attr('checked', true);
    $('.embargo-list-table tbody .item').addClass('selected');
  });

  $('#uncheck').click(function(e) {
    $('.embargo-list-wrapper input[type=checkbox]').attr('checked', false);
    $('.embargo-list-table tbody .item').removeClass('selected');
  });

  $('.embargo-list-wrapper input[type=checkbox]').click(function(e) {
    if ($(this).is(':checked')) {
      $(this).parent().parent().addClass('selected');
    }
    else {
      $(this).parent().parent().removeClass('selected');
    }
  });

  $('#datetimepicker').datetimepicker({
                                        dateFormat: 'yy-mm-dd',
                                        constrainInput: false,
                                        timeFormat: 'hh:mm:ss'
                                      });

  $('#embargo-search-button').click(function(e) {
    var psid = $('#embargo-search-form input[name=psid]').val();
    $.post("add",
           {
             psid: psid
           },
           function(data) {
             if (data != "") {
               var date = data.substring(0, data.indexOf(';'));
               var psids = data.substring(data.indexOf(';') + 1, data.length);
               $('.embargo-list-table tbody').append('<tr class=\"item\">' +
                                                     '<td class="cellar-id">' + psids + '</td>' +
                                                     '<td class="embargo-date">' + date + '</td>' +
                                                     '</tr>');
             }
           });
    return false;
  });

  $('#changeEmbargoDate').click(function(e) {
    if ($('.embargo-list-wrapper input:checkbox:checked').length == 1) {
      var input = prompt("Set the new embargo date (YYYY-MM-DD hh:mm:ss).");
      var uri = $('.embargo-list-wrapper input:checkbox:checked').attr('name');
      if (input != '' && input != null) {
        if (validateDate(input)) {
          $('.embargo-list-wrapper .embargo-status').css("display", "block");
          $('.embargo-list-wrapper .embargo-status').text('Changing embargo date to: ' + input);
          $.post("list/changeEmbargo",
                 {
                   date: input,
                   uri: uri
                 },
                 function(data) {
                   $('.embargo-list-wrapper .embargo-status').css("display", "none");
                   location.reload(true);
                 }
          );
        }
        else {
          alert("Invalid date specified.");
        }
      }
    }
    else if ($('.embargo-list-wrapper input:checkbox:checked').length > 1) {
      alert("Select no more then one work, to change the embargo date.");
    }
    else {
      alert("At least one work should be selected.");
    }
  });

  /* do some very basic date validation */
  function validateDate(date) {
    return trim(date).length == 19;
  }

  function trim(value) {
    value = value.replace(/^\s+/, '');
    value = value.replace(/\s+$/, '');
    return value;
  }

});