$(document).ready(function() {
	$('#errorTable').dataTable({
		/* initial page length */
		"iDisplayLength": 50,
        /* add page size labels */
        "aLengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
        /* Disable initial sort */
        "aaSorting": [] 
    } );
});