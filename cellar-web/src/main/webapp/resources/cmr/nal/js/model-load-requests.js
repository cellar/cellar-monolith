$(document).ready(function() {
	
	$('#resetModelUri_filter').click(function() {
		$('#modelUri_filter_input').val('');
		$('#modelUri_filter_input').removeAttr("value");
	});
	
	$('#resetCellarId_filter').click(function() {
		$('#cellarId_filter_input').val('');
		$('#cellarId_filter_input').removeAttr("value");
	});

	$("#restartLink").click(function () {
		$('#idSelected').val($(this).attr("data-id"));
		$('#activationDate').val($(this).attr("data-activation-date"));
		$('#restartPopup').css('display','block');
		$('#restartPopup').dialog({
			resizable: false,
			height:100,
			width:400
		});
	});
	
});
	