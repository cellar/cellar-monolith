$(document).ready(function() {
    var oTable = $('#nalversions_table').dataTable({
        "iDisplayLength": 25,
        "bSort": true,
        "sPaginationType": "full_numbers",
        "sDom": 'rtip',
        "initComplete": function () {
            var api = this.api();

            api.columns().indexes().flatten().each( function ( i ) {
                var column = api.column( i );
                var headerId= column.header().id;//blabla_header
                var filterContainerId= headerId.substr(0, headerId.indexOf('_'))+"_filter";

                var filterContainerElement = $('#'+filterContainerId);
                if(filterContainerElement.length){
                    if(headerId=='name_header'){


                        $('#name_filter_input').on( 'change', function () {
                            var val = $(this).val();
                            //column.search( val ? '(.*)'+val+'(.*)' : '', true, false ).draw();//
                            column.search( val? val : '', false, false ).draw();
                        } );
                        $('#resetName_filter').click(function() {
                            column.search('', false, false ).draw();//when only client side : column.search('.*', true, false ).draw();
                        });

                    } else if(headerId=='uri_header'){


                        $('#uri_filter_input').on( 'change', function () {
                            var val = $(this).val();
                            //column.search( val ? '(.*)'+val+'(.*)' : '', true, false ).draw();//
                            column.search( val? val : '', false, false ).draw();
                        } );
                        $('#resetUri_filter').click(function() {
                            column.search('', false, false ).draw();//when only client side : column.search('.*', true, false ).draw();
                        });

                    }
                }
            } );
        },
        "drawCallback" : function() {
            $("input[class^='nal_']").click(function(e) {
                var deleteNalButtonClass = $(this).attr('class');
                var nalName = deleteNalButtonClass.split('_')[1];
                var clear = confirm("Do you really want to delete the NAL '" + nalName + "'?");
                if (clear) {
                    $('#deleteNalName').attr('value', nalName);
                    $('#nal-delete-form').submit();

                }
            });
        }
    } );

    $('#resetName_filter').click(function() {
        $('#name_filter_input').val('');
    });

    $('#resetUri_filter').click(function() {
        $('#uri_filter_input').val('');
    });


});