$(document).ready(function() {
    $("#cancelButton").click(function () {
        var url = $(this).attr("data-url");
        var allowUrlList = ['/admin/cmr/nal/sparqlloadrequests'];

        if (allowUrlList.indexOf(url) > -1) {
            document.location.href = url;
        } else {
            alert("Invalid path for redirection");
        }
    });
});