$(document).ready(function() {

    $(".tabs-menu a").click(function(event) {
        tabTriggers(this, event);
    });

    //a cookie is used to retain the opened tab
    if(Cookies.get('activeTab')){
        var tab = Cookies.get('activeTab');
        var element = $('a[href$="'+ tab +'"]');
        var elementParent = $(element).parent();
        //adapt the li in the ul list
        elementParent.addClass("current");
        elementParent.siblings().removeClass("current");
        var tabContent = $(".tab-content");
        var notTab = tabContent.not(tab);
        notTab.css("display", "none");
        var theTab = $(tab);
        theTab.css("display", "block");
    }

    setupIngestionTables($('.processing_ingestion_datatable'),$('.waiting_ingestion_datatable'));
    setupIndexationTables($('.processing_indexation_datatable'),$('.waiting_indexation_datatable'));

    setInterval(reloadView, 5000);

    tableSelectTriggers($("select[id$='_select_div']"));
});

//This function will make a AJAX call to refresh the view
function reloadView() {
    $.ajax({
        type: "GET",
        url: 'threads',
        cache: false,
        success : function(view) {

            //the controller returns the entire html page
            //I'm using jquery to parse it and to retrieve the div that we want to update/replace
            var responseView = $($.parseHTML(view));
            var newElement = responseView.find('#threadsInfo');
            var oldElement = $("#threadsInfo");

            //a cookie is used to retain the opened tab
            if(Cookies.get('activeTab')){
                var tab = Cookies.get('activeTab');
                var element = responseView.find('a[href$="'+ tab +'"]');
                var elementParent = $(element).parent();
                //adapt the li in the ul list
                elementParent.addClass("current");
                elementParent.siblings().removeClass("current");
                var tabContent = responseView.find(".tab-content");
                var notTab = tabContent.not(tab);
                notTab.css("display", "none");
                var theTab = responseView.find(tab);
                theTab.css("display", "block");
            }

            var processingIngestionTable = newElement.find('.processing_ingestion_datatable');
            var waitingIngestionTable = newElement.find('.waiting_ingestion_datatable');
            var processingIndexationTable = newElement.find('.processing_indexation_datatable');
            var waitingIndexationTable = newElement.find('.waiting_indexation_datatable');

            setupIngestionTables(processingIngestionTable, waitingIngestionTable);
            setupIndexationTables(processingIndexationTable, waitingIndexationTable);

            // save state before update
            var searchState = getSearchState(oldElement);
            var pageState = getPageState(oldElement);

            // replace
            oldElement.replaceWith(newElement);

            // reset state after update
            resetSearchState(searchState, newElement);
            resetPageState(pageState, newElement);

            newElement.find('.tabs-menu a').click(function(event) {
                tabTriggers(this, event);
            });

            tableSelectTriggers(newElement.find("select[id$='_select_div']"));
        },
        error : function() {
            console.log('An error occurred when trying to update the view with ajax/jquery');
        }
    });
}

function getSearchState(oldElement) {
    var searchElements = oldElement.find("input[type='search']");
    var searchState = {
        values: [],
        focus: []
    };

    for(var i = 0; i < searchElements.length; ++i) {
        var searchElement = $(searchElements[i]);
        searchState.values[i] = searchElement.val();
        searchState.focus[i] = searchElement.is(":focus");
    }

    return searchState;
}

function getPageState(oldElement) {
    return [
        oldElement.find('.processing_ingestion_datatable').DataTable().page(),
        oldElement.find('.waiting_ingestion_datatable').DataTable().page(),
        oldElement.find('.processing_indexation_datatable').DataTable().page(),
        oldElement.find('.waiting_indexation_datatable').DataTable().page()
    ];
}

function resetSearchState(searchState, newElement) {
    var newSearchElements = newElement.find("input[type='search']");

    // set input value & focus
    for(var i = 0; i < newSearchElements.length; ++i) {
        var newSearchElement = $(newSearchElements[i]);
        newSearchElement.val(searchState.values[i]);
        if(searchState.focus[i]) {
            newSearchElement.focus();
        }
    }

    // trigger table search
    newElement.find('.processing_ingestion_datatable').DataTable().search(searchState.values[0]);
    newElement.find('.waiting_ingestion_datatable').DataTable().search(searchState.values[1]);
    newElement.find('.processing_indexation_datatable').DataTable().search(searchState.values[2]);
    newElement.find('.waiting_indexation_datatable').DataTable().search(searchState.values[3]);
}

function resetPageState(pageState, newElement) {
	// set table page
    newElement.find('.processing_ingestion_datatable').dataTable().fnPageChange(pageState[0]);
    newElement.find('.waiting_ingestion_datatable').dataTable().fnPageChange(pageState[1]);
    newElement.find('.processing_indexation_datatable').dataTable().fnPageChange(pageState[2]);
    newElement.find('.waiting_indexation_datatable').dataTable().fnPageChange(pageState[3]);
}

//this is used to reset the .click triggers in the tab links
function tabTriggers(element,event) {
    event.preventDefault();
    $(element).parent().addClass("current");
    $(element).parent().siblings().removeClass("current");
    var tab = $(element).attr("href");
    $(".tab-content").not(tab).css("display", "none");
    $(tab).fadeIn();
    //a cookie is used to retain the opened tab
    Cookies.set('activeTab', tab, { expires: 1, path: '/' });
}

//this is used to reset the .change trigger on the tables select box
function tableSelectTriggers(element){
    //keep the selected number of rows in memory
    element.change(function() {
        var selectId = $(this).attr('id');
        var tableClass = selectId.substr(0, selectId.indexOf('_select_div'));
        var selectedValue = $(this).val();
        Cookies.set(tableClass, selectedValue, { expires: 1, path: '/' });

    });
}


//setup ingestion tables
function setupIngestionTables(processingElement,waitingElement){
    var processingClass = processingElement.attr('class');
    var processing_ingestion_datatable_number_of_rows = 5;
    if(Cookies.get(processingClass)){
        processing_ingestion_datatable_number_of_rows = parseInt(Cookies.get(processingClass));
    }
    //processing table has 4 columns
    processingElement.dataTable({
        "iDisplayLength": processing_ingestion_datatable_number_of_rows,
        "bSort": false,
        "sPaginationType": "full_numbers",
        "aoColumns": [//we need to set class names to set a fixed width to each column
            {  "sClass": "package_name" },
            {  "sClass": "start_time" },
            {  "sClass": "priority" },
            { "sClass": "status" },
        ],
        //adding an id to the select to keep its value in memory
        "oLanguage": {
            "sLengthMenu": 'Display <select id="processing_ingestion_datatable_select_div">'+
            '<option value="5">5</option>'+
            '<option value="10">10</option>'+
            '<option value="20">20</option>'+
            '<option value="40">40</option>'+
            '<option value="100">100</option>'+
            '<option value="200">200</option>'+
            '</select> records'
        }
    } );


    var waitingClass = waitingElement.attr('class');
    var waiting_ingestion_datatable_number_of_rows = 5;
    if(Cookies.get(waitingClass)){
        waiting_ingestion_datatable_number_of_rows = parseInt(Cookies.get(waitingClass));
    }

    //waiting table has 3 columns
    waitingElement.dataTable({
        "iDisplayLength": waiting_ingestion_datatable_number_of_rows,
        "bSort": false,
        "sPaginationType": "full_numbers",
        "aoColumns": [
            {  "sClass": "waiting_package_name" },
            {  "sClass": "waiting_creation_time" },
            {  "sClass": "waiting_priority" },
        ],

        "oLanguage": {
            "sLengthMenu": 'Display <select id="waiting_ingestion_datatable_select_div">'+
            '<option value="5">5</option>'+
            '<option value="10">10</option>'+
            '<option value="20">20</option>'+
            '<option value="40">40</option>'+
            '<option value="100">100</option>'+
            '<option value="200">200</option>'+
            '</select> records'
        }
    } );
}

//setup indexation tables
function setupIndexationTables(processingElement,waitingElement){
    var processingClass = processingElement.attr('class');
    var processing_indexation_datatable_number_of_rows = 5;
    if(Cookies.get(processingClass)){
        processing_indexation_datatable_number_of_rows = parseInt(Cookies.get(processingClass));
    }
    //processing table has 4 columns
    processingElement.dataTable({
        "iDisplayLength": processing_indexation_datatable_number_of_rows,
        "bSort": false,
        "sPaginationType": "full_numbers",
        "aoColumns": [//we need to set class names to set a fixed width to each column
            {  "sClass": "groupUri" },
            {  "sClass": "start_time" },
            {  "sClass": "priority" },
            { "sClass": "status" },
        ],
        //adding an id to the select to keep its value in memory
        "oLanguage": {
            "sLengthMenu": 'Display <select id="processing_indexation_datatable_select_div">'+
            '<option value="5">5</option>'+
            '<option value="10">10</option>'+
            '<option value="20">20</option>'+
            '<option value="40">40</option>'+
            '<option value="100">100</option>'+
            '<option value="200">200</option>'+
            '</select> records'
        }
    } );


    var waitingClass = waitingElement.attr('class');
    var waiting_indexation_datatable_number_of_rows = 5;
    if(Cookies.get(waitingClass)){
        waiting_indexation_datatable_number_of_rows = parseInt(Cookies.get(waitingClass));
    }

    //waiting table has 3 columns
    waitingElement.dataTable({
        "iDisplayLength": waiting_indexation_datatable_number_of_rows,
        "bSort": false,
        "sPaginationType": "full_numbers",
        "aoColumns": [
            {  "sClass": "waiting_groupUri" },
            {  "sClass": "waiting_creation_time" },
            {  "sClass": "waiting_priority" },
        ],

        "oLanguage": {
            "sLengthMenu": 'Display <select id="waiting_indexation_datatable_select_div">'+
            '<option value="5">5</option>'+
            '<option value="10">10</option>'+
            '<option value="20">20</option>'+
            '<option value="40">40</option>'+
            '<option value="100">100</option>'+
            '<option value="200">200</option>'+
            '</select> records'
        }
    } );
}