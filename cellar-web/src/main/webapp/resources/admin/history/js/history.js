$(document).ready(function() {
	var base = $('#base').text();
	var oTable = $('#identifiersHistoryTable').dataTable({
		"iDisplayLength": 25,
		"bSort": true,
		"sPaginationType": "full_numbers",
		
		 "processing": true,
	        "serverSide": true,
	        "ajax": base+"/admin/history/ajax",

	    "sDom": 'rtip',
		
		"aoColumns": [//we need to set class names to set a fixed width to each column
		              {  "sClass": "cellarId" ,"sWidth": "20%"},
		              {  "sClass": "productionId" ,"sWidth": "20%"},
		              {  "sClass": "creationDate","sWidth": "20%"},
		              { "sClass": "obsolete" ,"sWidth": "20%"},
		              { "sClass": "delete",
				          "mRender": function(data, type, full) {
				        	  var link = "<a href=\"" + base+"/admin/history/delete/"+data + "\">" +
				        	  		"<img border=\"0\" alt=\"delete record\" src=\""+base+"/resources/common/admin/images/delete.png\">" +
				        	  		"</a>";
				        	  return link;
				          },
				          "bSortable": false,
		            	  "sWidth": "4%"
		              }
		          ],
          "initComplete": function () {
              var api = this.api();
   
              api.columns().indexes().flatten().each( function ( i ) {
                  var column = api.column( i );
                  var headerId= column.header().id;//blabla_header
                  var filterContainerId= headerId.substr(0, headerId.indexOf('_'))+"_filter"; 
                  
                  var filterContainerElement = $('#'+filterContainerId);
                  if(filterContainerElement.length){
                	  if(headerId=='cellarId_header'){
                		  $('#cellarId_filter_input').on( 'change', function () {
                                  var val = $(this).val();
                                  //column.search( val ? '(.*)'+val+'(.*)' : '', true, false ).draw();//
                                  column.search( val? val : '', false, false ).draw();
                              } );
                		  
                		  $('#resetCellarId_filter').click(function() {
                			  column.search('', false, false ).draw();//when only client side : column.search('.*', true, false ).draw();
                		  });
                	  } else if(headerId=='productionId_header'){
                		  $('#productionId_filter_input').on( 'change', function () {
                                  var val = $(this).val();
                                  //column.search( val ? '(.*)'+val+'(.*)' : '', true, false ).draw();//
                                  column.search( val? val : '', false, false ).draw();
                              } );
                		  
                		  $('#resetProductionId_filter').click(function() {
                			  column.search('', false, false ).draw();//when only client side : column.search('.*', true, false ).draw();
                		  });
                	  }else{
                		  $('#'+filterContainerId+' select').on( 'change', function () {
                			  	var val = $(this).val();
                                  //column.search( val ? '^'+val+'$' : '', true, false ).draw();
                                  column.search( val ? val : '', false, false ).draw();
                              } );
           
                	  }
                  }
          } );
      }
    } );
	
	
	$('#resetCellarId_filter').click(function() {
		$('#cellarId_filter_input').val('');
	});
	
	$('#resetProductionId_filter').click(function() {
		$('#productionId_filter_input').val('');
	});
	
});