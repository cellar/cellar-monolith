$(function()
{
    // always use this
    $.ajaxSetup({
        cache : false,        
        error: function(resp,status,xhr,form) {alert('error ' + resp + '  status ' + status);}
    });
    
    
});

//function to change the action submitted in a form.
function toggleContentStatus(chid, contentId) {
	if ($("#ch_img_"+chid).attr('src').indexOf('loading')>-1) {
		alert('The new status of this document is still processed');
		return false;
	}
	
	if ($("#ch_img_"+chid).attr('src').indexOf('stop')>-1) {					
		$("#ch_img_"+chid).attr('src', $("#ch_img_"+chid).attr('src').replace('stop','loading'));
	} else {
		$("#ch_img_"+chid).attr('src', $("#ch_img_"+chid).attr('src').replace('check','loading'));
	}
	
	$.ajax({
		  type: 'POST',
		  url: getBaseUrl() + '/admin/contentVisibility/hide/toggle?contentId='+contentId,
		  success: function(data) {
				if (! data.hidden) {
					$("#ch_img_"+chid).attr('src', $("#ch_img_"+chid).attr('src').replace('loading','check'));
		  		}
				else {
					$("#ch_img_"+chid).attr('src', $("#ch_img_"+chid).attr('src').replace('loading','stop'));
				}				
		  }
		});
	
}

function unHideContent(rowid, contentId) {
	$("#ch_img_"+rowid).attr('src', $("#ch_img_"+rowid).attr('src').replace('stop','loading'));
	
	$.ajax({
		  type: 'POST',
		  url: getBaseUrl() + '/admin/contentVisibility/hide/toggle?contentId='+contentId,
		  success: function(data) {
			$("#row_"+rowid).hide();
		  }
		});
	
}

function deleteContent(rowid, contentId) {
	
	var answer = confirm("Are you sure?");
	if (answer){
		$("#del_img_"+rowid).attr('src', $("#del_img_"+rowid).attr('src').replace('delete','loading'));
		
		$.ajax({
		  type: 'GET',
		  url: getBaseUrl() + '/admin/contentVisibility/delete?contentId='+contentId,
		  success: function(data) {
			  $("#del_img_"+rowid).hide();
			  $("#row_"+rowid).hide();
		  }
		});
	}

}


