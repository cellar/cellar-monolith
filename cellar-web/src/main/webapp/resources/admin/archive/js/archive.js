$(document).ready(function() {

//    tabs
    
    $( "#tabs" ).tabs({
		activate: function( event, ui ) {
			var tab=ui.newTab.context.getAttribute('href');
			Cookies.set('activeTab_archive', tab, { expires: 1, path: '/' });
		}
    }
    ).addClass( "ui-tabs-vertical ui-helper-clearfix" );
    $( "#tabs li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );
    
	
	//a cookie is used to retain the opened tab
	 if(Cookies.get('activeTab_archive')){
	  	var tabHref = Cookies.get('activeTab_archive');//#tabs-1
	  	var tabIndex = tabHref.substring(6, 7); 	  	
	  	$( "#tabs" ).tabs( 'option', 'active', parseInt(tabIndex)-1);
	  }
	
	var archivistBaseUrl = $('#archivistBaseUrl').text()+'service/datatable';
	
	
	var oTable = $('#archivesTable').dataTable({
		"iDisplayLength": 25,
		"bSort": true,
		"sPaginationType": "full_numbers",
		
		 "processing": true,
	        "serverSide": true,
	        "ajax": archivistBaseUrl,

//	    l - Length changing
//	    f - Filtering input
//	    t - The Table!
//	    i - Information
//	    p - Pagination
//	    r - pRocessing
//	    < and > - div elements
//	    <"#id" and > - div with an id
//	    <"class" and > - div with a class
//	    <"#id.class" and > - div with an id and class
		
//		"sDom": 'lfrtip'//default,
	    "sDom": 'rtip',
		
		"aoColumns": [//we need to set class names to set a fixed width to each column
		              { "sClass": "cellarId","sWidth": "15%"},
		              { "sClass": "archivingTime" ,"sWidth": "15%"},
		              { "sClass": "ingestionTime" ,"sWidth": "15%"},
		              { "sClass": "priority" ,"sWidth": "15%"},
		              { "sClass": "failed" ,"sWidth": "15%"}
		          ],
          "initComplete": function () {
              var api = this.api();
   
              api.columns().indexes().flatten().each( function ( i ) {
                  var column = api.column( i );
                  var headerId= column.header().id;//blabla_header
                  var columnName= headerId.substr(0, headerId.indexOf('_'));//+"_filter"; 
                  
                  var filterContainerElement = $('#'+columnName+"_filter");
                  if(filterContainerElement.length){
                	  if(headerId=='cellarId_header'){
                		  $("input[id='"+columnName+"_filter_input']").on( 'keyup click change', function () {
                    			  var val = $(this).val();
                                  column.search( val ? val : '', false, false ).draw();
                            } );
                	  }
                	  
                	  
                	  
                  }
          } );
      }
    } );
	
	$('#reset_cellarId_filter').click(function() {
		$('#cellarId_filter_input').val('');
		triggerSearchAndRedrawTableForStringFields("cellarId",0);
	});
	
	
	//ARCHIVING TIME
	$( "#archivingTime_from" ).datetimepicker({
        dateFormat: 'yy-mm-dd',
        constrainInput: false,
        timeFormat: 'hh:mm:ss',
        changeYear: true,
        onClose: function(selected,evnt) {
        	triggerSearchAndRedrawTable("archivingTime",1);
       }
        
        
    });
	
    $( "#archivingTime_to" ).datetimepicker({
        dateFormat: 'yy-mm-dd',
        constrainInput: false,
        timeFormat: 'hh:mm:ss',
        changeYear: true,
        onClose: function(selected,evnt) {
        	triggerSearchAndRedrawTable("archivingTime",1);
       }
    });
  //INGESTION TIME
	$( "#ingestionTime_from" ).datetimepicker({
        dateFormat: 'yy-mm-dd',
        constrainInput: false,
        timeFormat: 'hh:mm:ss',
        changeYear: true,
        onClose: function(selected,evnt) {
        	triggerSearchAndRedrawTable("ingestionTime",2);
       }
    });
	
    $( "#ingestionTime_to" ).datetimepicker({
        dateFormat: 'yy-mm-dd',
        constrainInput: false,
        changeYear: true,
        timeFormat: 'hh:mm:ss',
        onClose: function(selected,evnt) {
        	triggerSearchAndRedrawTable("ingestionTime",2);
       }
    });
		
    function triggerSearchAndRedrawTable(columnName,index) {
      var beforeDate = $( "#"+columnName+"_from" ).val();
  	  var afterDate =  $( "#"+columnName+"_to" ).val();
  	  if(beforeDate.length==0){
  		  beforeDate="1900-01-01 00:00:00";
  	  }
  	  if(afterDate.length==0){
  		afterDate="9999-01-01 00:00:00";
  	  }
  	  var val = beforeDate+";"+afterDate;
  	  $('#archivesTable').DataTable().column( index ).search( val ? val : '', false, false ).draw();
	}
    
    function triggerSearchAndRedrawTableForStringFields(columnName,index) {
    	  var val = '';
    	  $('#archivesTable').DataTable().column( index ).search( val ? val : '', false, false ).draw();
  	}
    
    $('#reset_ingestionTime_filter').click(function() {
    	var reload = false;
    	if($('#ingestionTime_from').val().length!=0 || $('#ingestionTime_to').val().length!=0){
    		reload = true;
    	}
    	if(reload){
    		$('#ingestionTime_from').val('');
    		$('#ingestionTime_to').val('');
			triggerSearchAndRedrawTable("ingestionTime",2);
		}
	});
    $('#reset_archivingTime_filter').click(function() {
    	var reload = false;
    	if($('#archivingTime_from').val().length!=0 || $('#archivingTime_to').val().length!=0){
    		reload = true;
    	}
    	if(reload){
    		$('#archivingTime_from').val('');
    		$('#archivingTime_to').val('');
			triggerSearchAndRedrawTable("archivingTime",1);
    	}
	});
    
    
    //status
    $("#inner_tbl_DAILY_STATUS").html("");
    $("#inner_tbl_BULK_STATUS").html("");
    $("#inner_tbl_AUTHENTICOJ_STATUS").html("");
    $("#inner_tbl_EMBARGO_STATUS").html("");
    
    var DAILY_STATUS = $.parseJSON($('#DAILY_STATUS').text()); 
    var BULK_STATUS = $.parseJSON($('#BULK_STATUS').text()); 
    var AUTHENTICOJ_STATUS = $.parseJSON($('#AUTHENTICOJ_STATUS').text()); 
    var EMBARGO_STATUS = $.parseJSON($('#EMBARGO_STATUS').text());
    
    $("#inner_tbl_DAILY_STATUS").html(buildTable(DAILY_STATUS));
    $("#inner_tbl_BULK_STATUS").html(buildTable(BULK_STATUS));
    $("#inner_tbl_AUTHENTICOJ_STATUS").html(buildTable(AUTHENTICOJ_STATUS));
    $("#inner_tbl_EMBARGO_STATUS").html(buildTable(EMBARGO_STATUS));
    
    //stats
    $("#inner_tbl_DAILY_STATS").html("");
    $("#inner_tbl_BULK_STATS").html("");
    $("#inner_tbl_AUTHENTICOJ_STATS").html("");
    $("#inner_tbl_EMBARGO_STATS").html("");
    
    var DAILY_STATS = $.parseJSON($('#DAILY_STATS').text()); 
    var BULK_STATS = $.parseJSON($('#BULK_STATS').text()); 
    var AUTHENTICOJ_STATS = $.parseJSON($('#AUTHENTICOJ_STATS').text()); 
    var EMBARGO_STATS = $.parseJSON($('#EMBARGO_STATS').text());
    
    $("#inner_tbl_DAILY_STATS").html(buildTable(DAILY_STATS));
    $("#inner_tbl_BULK_STATS").html(buildTable(BULK_STATS));
    $("#inner_tbl_AUTHENTICOJ_STATS").html(buildTable(AUTHENTICOJ_STATS));
    $("#inner_tbl_EMBARGO_STATS").html(buildTable(EMBARGO_STATS));
    
    function buildTable(a) {
        var e = document.createElement("table"),
            d, b;
        //if (isArray(a)) return buildArray(a);
        for (var c in a) 
    		"object" != typeof a[c] || isArray(a[c]) ?
    			"object" == typeof a[c] && isArray(a[c]) ?
    				(d = e.insertRow(-1),
    					b = d.insertCell(-1),
    						b.colSpan = 2,
    							b.innerHTML = '<div class="td_head">' + encodeText(c) + '</div><table style="width:100%">' 
    							+ $(buildArray(a[c]), !1).html() + "</table>") : (d = e.insertRow(-1), b = d.insertCell(-1),
    							b.innerHTML = "<div class='td_head'>" + encodeText(c) + "</div>", d = d.insertCell(-1), 
    							d.innerHTML = "<div class='td_row_even'>" +
            encodeText(a[c]) + "</div>") : (d = e.insertRow(-1), b = d.insertCell(-1), 
    		b.colSpan = 2, b.innerHTML = '<div class="td_head">' + encodeText(c) 
    		+ '</div><table style="width:100%">' + $(buildTable(a[c]), !1).html() + "</table>");
        return e
    }
    function buildArray(a) {
        var e = document.createElement("table"),
            d, b, c = !1,
            p = !1,
            m = {},
            h = -1,
            n = 0,
            l, td_class;
        l = "";
        if (0 == a.length) return "<div></div>";
        d = e.insertRow(-1);
        for (var f = 0; f < a.length; f++)
            if ("object" != typeof a[f] || isArray(a[f])) "object" == typeof a[f] && isArray(a[f]) ? (b = d.insertCell(h), b.colSpan = 2, b.innerHTML = '<div class="td_head"></div><table style="width:100%">' + $(buildArray(a[f]), !1).html() + "</table>", c = !0) : p || (h += 1, p = !0, b = d.insertCell(h), m.empty = h, b.innerHTML = "<div class='td_head'>&nbsp;</div>");
            else
                for (var k in a[f]) l =
                    "-" + k, l in m || (c = !0, h += 1, b = d.insertCell(h), m[l] = h, b.innerHTML = "<div class='td_head'>" + encodeText(k) + "</div>");
        c || e.deleteRow(0);
        n = h + 1;
        for (f = 0; f < a.length; f++)
            if (d = e.insertRow(-1), td_class = isEven(f) ? "td_row_even" : "td_row_odd", "object" != typeof a[f] || isArray(a[f]))
                if ("object" == typeof a[f] && isArray(a[f]))
                    for (h = m.empty, c = 0; c < n; c++) b = d.insertCell(c), b.className = td_class, l = c == h ? '<table style="width:100%">' + $(buildArray(a[f]), !1).html() + "</table>" : " ", b.innerHTML = "<div class='" + td_class + "'>" + encodeText(l) +
                        "</div>";
                else
                    for (h = m.empty, c = 0; c < n; c++) b = d.insertCell(c), l = c == h ? a[f] : " ", b.className = td_class, b.innerHTML = "<div class='" + td_class + "'>" + encodeText(l) + "</div>";
        else {
            for (c = 0; c < n; c++) b = d.insertCell(c), b.className = td_class, b.innerHTML = "<div class='" + td_class + "'>&nbsp;</div>";
            for (k in a[f]) c = a[f], l = "-" + k, h = m[l], b = d.cells[h], b.className = td_class, "object" != typeof c[k] || isArray(c[k]) ? "object" == typeof c[k] && isArray(c[k]) ? b.innerHTML = '<table style="width:100%">' + $(buildArray(c[k]), !1).html() + "</table>" : b.innerHTML =
                "<div class='" + td_class + "'>" + encodeText(c[k]) + "</div>" : b.innerHTML = '<table style="width:100%">' + $(buildTable(c[k]), !1).html() + "</table>"
        }
        return e
    }

    function encodeText(a) {
        return $("<div />").text(a).html()
    }

    function isArray(a) {
        return "[object Array]" === Object.prototype.toString.call(a)
    }

    function isEven(a) {
        return 0 == a % 2
    }
	
});

function httpGet(theURL){
   $.ajax({
	    url: theURL,
	    dataType: 'jsonp'
   });
   setTimeout(
	   function() { 
			console.log('Executet HTTP GET request: '+theURL);
			location.reload();			   	
	   }, 1000);
}

function update_archivist_properties(theURL){
	var data = $("#update_archivist_properties_fieldset").serializeArray();
	data = JSON.stringify(data);
	data = data.replace(/"name":/g, "\"propertyKey\":");
	data = data.replace(/"value":/g, "\"propertyValue\":");
	$.ajax({
		headers: { 
	        'Accept': 'application/json',
	        'Content-Type': 'application/json' 
	    },
		url: theURL,
	    type: "POST",
	    data: data,
        dataType: "json",
        success: function(json) {
        	var responseMessageDiv = $("#update_archivist_properties_response_message");
        	responseMessageDiv.css("color", "green");
        	responseMessageDiv.html("Properties successfully saved.");
        },
        error: function(json) {
        	var jsonResponse = JSON.parse(json.responseText);
        	var responseMessageDiv = $("#update_archivist_properties_response_message");
        	responseMessageDiv.css("color", "red");
        	responseMessageDiv.html(jsonResponse.message);
        }
	});
}

