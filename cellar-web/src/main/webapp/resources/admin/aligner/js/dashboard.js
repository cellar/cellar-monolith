
$(document).ready(function() {
	
	$('#numberOfResourcesButton').click(function() {
		getAndReplace('numberOfResources', 'numberOfResources');
		return false;
	});
	$('#numberOfResourcesDiagnosableButton').click(function() {
		getAndReplace('numberOfResourcesDiagnosable', 'numberOfResourcesDiagnosable');
		return false;
	});
	$('#numberOfResourcesFixableButton').click(function() {
		getAndReplace('numberOfResourcesFixable', 'numberOfResourcesFixable');
		return false;
	});
	$('#numberOfResourcesInTableButton').click(function() {
		getAndReplace('numberOfResourcesInTable', 'numberOfResourcesInTable');
		return false;
	});

	$("#stopAligningButton").click(function () {
		var url = $(this).attr("data-url");
		var allowUrlList = ['/admin/aligner/dashboard/stopExecutor'];

		if (allowUrlList.indexOf(url) > -1) {
			document.location.href = url;
		} else {
			alert("Invalid path for redirection");
		}
	});

	$("#startAligningButton").click(function () {
		var url = $(this).attr("data-url");
		var allowUrlList = ['/admin/aligner/dashboard/startExecutor'];

		if (allowUrlList.indexOf(url) > -1) {
			document.location.href = url;
		} else {
			alert("Invalid path for redirection");
		}
	});
});



function getAndReplace(id, query) {
	//sets up the loading image
	var loadingElement = createLoadingSpan(id);
	$('#'+id).replaceWith(loadingElement);
	
	//executes the request
	$.ajax({
		type: 'GET',
		url: 'dashboard/'+query,
		async: true,
		success: function(data) {
			var resultElement = createDataSpan(id,data);
			//replace
			 $('#'+id).replaceWith(resultElement);
		}
	});
}

function createLoadingSpan(id) {
	var loadingSpan = document.createElement('span');
	loadingSpan.setAttribute('id', id);
	
	var loadingImg = document.createElement('img');
	loadingImg.setAttribute('id', 'loading');
	loadingImg.setAttribute('src', getBaseUrl() + '/resources/common/admin/images/loading-small.png');
	
	loadingSpan.appendChild(loadingImg);
	
	return loadingSpan;
}

function createDataSpan(id, data) {
	var dataSpan = document.createElement('span');
	dataSpan.setAttribute('id', id);
	dataSpan.innerHTML = data;
	
	return dataSpan;
}