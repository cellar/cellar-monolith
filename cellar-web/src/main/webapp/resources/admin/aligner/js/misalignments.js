$(document).ready(function() {
	
	
	
	var base = $('#base').text();
	var alignerBase = $('#alignerBase').text();
	
	
	var oTable = $('#alignerMisalignmentsTable').dataTable({
		"iDisplayLength": 25,
		"bSort": true,
		"sPaginationType": "full_numbers",
		
		 "processing": true,
	        "serverSide": true,
	        "ajax": base+"/admin/aligner/ajax/misalignments",

//	    l - Length changing
//	    f - Filtering input
//	    t - The Table!
//	    i - Information
//	    p - Pagination
//	    r - pRocessing
//	    < and > - div elements
//	    <"#id" and > - div with an id
//	    <"class" and > - div with a class
//	    <"#id.class" and > - div with an id and class
		
//		"sDom": 'lfrtip'//default,
	    "sDom": 'rtip',
		
		"aoColumns": [//we need to set class names to set a fixed width to each column
		              {  "sClass": "cellarId" ,"sWidth": "24%"},
		              {  "sClass": "operationDate","sWidth": "24%"},
		              {  "sClass": "operationType" ,"sWidth": "24%"},
		              { "sClass": "repository" ,"sWidth": "24%"},
		              { "sClass": "hierarchy",
				          "mRender": function(data, type, full) {
				        	  var link = "<a href=\"" + alignerBase+"/"+data + "\">" +
				        	  		"<img border=\"0\" alt=\"See hierarchy\" src=\""+base+"/resources/common/admin/images/hierarchy.png\">" +
				        	  		"</a>";
				        	  return link;
				          },
				          "bSortable": false,
		            	  "sWidth": "4%"
		              }
		          ],
		         /* "oLanguage": {
		              "sLengthMenu": 'Display <select id="datatable_select_div">'+
		              '<option value="5">5</option>'+
		              '<option value="10">10</option>'+
		                  '<option value="20">20</option>'+
		                  '<option value="40">40</option>'+
		                  '<option value="100">100</option>'+
		                  '<option value="200">200</option>'+
		                  '</select> records'
		          } ,*/
          "initComplete": function () {
              var api = this.api();
   
              api.columns().indexes().flatten().each( function ( i ) {
                  var column = api.column( i );
                  var headerId= column.header().id;//blabla_header
                  var filterContainerId= headerId.substr(0, headerId.indexOf('_'))+"_filter"; 
                  
                  var filterContainerElement = $('#'+filterContainerId);
                  if(filterContainerElement.length){
                	  if(headerId=='cellarId_header'){
                		  
                		  
                		  $('#cellarId_filter_input').on( 'change', function () {
                                  var val = $(this).val();
                                  //column.search( val ? '(.*)'+val+'(.*)' : '', true, false ).draw();//
                                  column.search( val? val : '', false, false ).draw();
                              } );
                		  
                		  $('#resetCellarId_filter').click(function() {
                			  column.search('', false, false ).draw();//when only client side : column.search('.*', true, false ).draw();
                		  });
                	  }else{
                		  $('#'+filterContainerId+' select').on( 'change', function () {
                			  	var val = $(this).val();
                                  //column.search( val ? '^'+val+'$' : '', true, false ).draw();
                                  column.search( val ? val : '', false, false ).draw();
                              } );
           
                	  }
                  }
          } );
      }
    } );
	
	
	$('#resetCellarId_filter').click(function() {
		$('#cellarId_filter_input').val('');
	});
	
});