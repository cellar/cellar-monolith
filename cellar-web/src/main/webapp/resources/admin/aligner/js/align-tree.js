$(document).ready(function() {
	$.ajaxSetup({ cache: false });
	docReady();

	$("#alignerButton").click(function () {
		$("#alignerDialog").data('link', $(this).attr("data-id")).dialog("open");
		$("#alignerText").show();
		$("#alignerWaiting").hide();
		$("#alignerError").hide();
		$(".ui-dialog-buttonpane button:contains('Align')").button("enable");
		return false;
	});
});
	
function docReady() {
	$("#alignerDialog").dialog({
		autoOpen: false,
		width: 450,
		modal: true,
		buttons: {
			'Close':function() {
				$(this).dialog('close');
			},
			'Align': function() {
				var dialog = $(this);
				var cellarId = dialog.data('link');
	
				$(".ui-dialog-buttonpane button:contains('Align')").button("disable");
				$("#alignerText").hide();
				$("#alignerWaiting").show();
				
				$.ajax({
					type: 'GET',
					url: getBaseUrl() + '/admin/aligner/align/fix?objectId=' + cellarId,
					async: true,
					success: function(data) {
						if(data) {
							$("#alignerWaiting").hide();
							$("#alignerError").text(data);
							$("#alignerError").show();
						} else {
							dialog.dialog('close');
							$(location).attr('href', getBaseUrl() + '/admin/aligner/align?objectId=' + cellarId);
						}
					},
					error: function (xhr) {
						$("#alignerWaiting").hide();
						$("#alignerError").text(xhr.responseText);
						$("#alignerError").show();
					}
				});
			}
		}
	});
}