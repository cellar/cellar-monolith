// text of contextual help pop-up boxes for static fields
var decodingLanguageHelpText = "The 3-letters code of the decoding language to use.<br />"
	+ "This is the language used for decoding the NALs associated to the notice.";
var acceptLanguageHelpText = "The 3-letters code of the accept language to use.<br />"
	+ "This will normally be used for filtering the expressions by language.";
var acceptFormatHelpText = "The type of the content stream to search for.";
var excludeInferredHelpText = "If checked, the inferred triples are excluded from the RDF notice.";

// text of contextual help pop-up boxes for dynamic fields
var contentTypeHelpText = new Array();
// case tree notice
contentTypeHelpText[0] = "The <b>Tree notice</b> option allows the user to search for a complete<br />"
	+ "tree notice of the given <i>Work ID</i>, decoded in the given <i>Decoding language</i>.<br /><br />"
	+ "The tree notice returned contains the full hierarchy of the work,<br />"
	+ "here included all the expressions of the work and all the manifestations<br />"
	+ "associated to the expressions.";
// case branch notice
contentTypeHelpText[1] = "The <b>Branch notice</b> option allows the user to search for a complete<br />"
	+ "branch notice of the given <i>Work ID</i>, decoded in the given <i>Decoding language</i>.<br /><br />"
	+ "The branch notice returned contains the work, within the expression<br />"
	+ "in the given <i>Accept language</i>, and all the manifestations associated<br />"
	+ "to that expression.<br /><br />"
	+ "An example:<br>"
	+ "let's suppose the existence of the work with id <i>cellar:xxx</i>, containing the expressions:<br />"
	+ "&nbsp;&nbsp;- <i>cellar:xxx.0001</i> in English, with associated manifestations:<br />"
	+ "&nbsp;&nbsp;&nbsp;&nbsp;- <i>cellar:xxx.0001.01</i><br />"
	+ "&nbsp;&nbsp;&nbsp;&nbsp;- <i>cellar:xxx.0001.02</i><br />"
	+ "&nbsp;&nbsp;- <i>cellar:xxx.0002</i> in French, with associated manifestations:<br />"
	+ "&nbsp;&nbsp;&nbsp;&nbsp;- <i>cellar:xxx.0002.01</i><br />"
	+ "&nbsp;&nbsp;&nbsp;&nbsp;- <i>cellar:xxx.0002.02</i><br />"
	+ "Then, if the user searches for:<br />"
	+ "&nbsp;&nbsp;- <i>Work ID</i>: cellar:xxx<br />"
	+ "&nbsp;&nbsp;- <i>Decoding language</i>: English<br />"
	+ "&nbsp;&nbsp;- <i>Accept language</i>: French<br />"
	+ "the following notice, decoded in English, is returned:<br />"
	+ "&nbsp;&nbsp;- cellar:xxx<br />"
	+ "&nbsp;&nbsp;&nbsp;&nbsp;- cellar:xxx.0002 (that is, the french expression)<br />"
	+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- cellar:xxx.0002.01<br />"
	+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- cellar:xxx.0002.02";
// case object-work notice
contentTypeHelpText[2] = "The <b>Object-Work notice</b> option allows the user to search for the work<br />"
	+ "identified by the given <i>Work ID</i>, decoded in the given <i>Decoding language</i>.";
// case object-expression notice
contentTypeHelpText[3] = "The <b>Object-Expression notice</b> option allows the user to search for the expression<br />"
	+ "of the work identified by the given <i>Work ID</i>, decoded in the given <i>Decoding language</i>.<br /><br />"
	+ "The expression returned, if any, is the one in language <i>Accept language</i>.<br /><br />"
	+ "An example:<br>"
	+ "let's suppose the existence of the work with id <i>cellar:xxx</i>, containing the expressions:<br />"
	+ "&nbsp;&nbsp;- <i>cellar:xxx.0001</i> in English<br />"
	+ "&nbsp;&nbsp;- <i>cellar:xxx.0002</i> in French<br />"
	+ "Then, if the user searches for:<br />"
	+ "&nbsp;&nbsp;- <i>Work ID</i>: cellar:xxx<br />"
	+ "&nbsp;&nbsp;- <i>Decoding language</i>: English<br />"
	+ "&nbsp;&nbsp;- <i>Accept language</i>: French<br />"
	+ "the following notice, decoded in English, is returned:<br />"
	+ "&nbsp;&nbsp;- cellar:xxx.0002 (that is, the french expression)";
// case object-manifestation notice
contentTypeHelpText[4] = "The <b>Object-Manifestation notice</b> option allows the user to search for the manifestation<br />"
	+ "identified by the given <i>Manifestation ID</i>, decoded in the given <i>Decoding language</i>.";
// case RDF-Object notice
contentTypeHelpText[5] = "The <b>RDF-Object notice</b> option allows the user to search for the RDF (Resource Description Framework)<br />"
	+ "content of the object identified by the given <i>Object ID</i>.<br /><br />"
	+ "The object to search for can be a work, an expression, a manifestation, a dossier, an event or an agent.";
//case RDF-Tree notice
contentTypeHelpText[6] = "The <b>RDF-Tree notice</b> option allows the user to search for the full RDF (Resource Description Framework) tree<br />"
	+ "whose root is the object identified by the given <i>Object ID</i>.<br /><br />"
	+ "The object to search for can be a work, a dossier or an agent.";
// case Content stream
contentTypeHelpText[7] = "The <b>Content stream</b> option allows the user to search for the content stream<br />"
	+ "of the manifestation associated to the work identified by the given <i>Work ID</i>,<br />"
	+ "and which belongs to the expression in the given <i>Accept language</i>,<br />"
	+ "and which contains at least 1 content stream of the given <i>Accept format</i>.<br /><br />"
	+ "An example:<br>"
	+ "let's suppose the existence of the work with id <i>cellar:xxx</i>, containing the expressions:<br />"
	+ "&nbsp;&nbsp;- <i>cellar:xxx.0001</i> in English, with associated manifestations:<br />"
	+ "&nbsp;&nbsp;&nbsp;&nbsp;- <i>cellar:xxx.0001.01</i>, with associated content stream:<br />"
	+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- <i>abc.PDF</i><br />"
	+ "&nbsp;&nbsp;&nbsp;&nbsp;- <i>cellar:xxx.0001.02</i>, with associated content stream:<br />"
	+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- <i>def.XML</i><br />"
	+ "&nbsp;&nbsp;- <i>cellar:xxx.0002</i> in French, with associated manifestations:<br />"
	+ "&nbsp;&nbsp;&nbsp;&nbsp;- <i>cellar:xxx.0002.01</i>, with associated content stream:<br />"
	+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- <i>ghi.PDF</i><br />"
	+ "&nbsp;&nbsp;&nbsp;&nbsp;- <i>cellar:xxx.0002.02</i>, with associated content stream:<br />"
	+ "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;- <i>jkl.XML</i><br />"
	+ "Then, if the user searches for:<br />"
	+ "&nbsp;&nbsp;- <i>Work ID</i>: cellar:xxx<br />"
	+ "&nbsp;&nbsp;- <i>Accept language</i>: French<br />"
	+ "&nbsp;&nbsp;- <i>Accept format</i>: PDF<br />"
	+ "the stream <i>ghi.PDF</i> is returned.";
var pidHelpText = new Array();
pidHelpText[0] = "A <i>Work ID</i> in format:<br>"
	+ "ps_identifier:work_identifier<br /><br />"
	+ "Some valid examples:<br>"
	+ "cellar:f2315395-af0d-11e1-9093-01aa75ed71a1<br>"
	+ "celex:32006D0241I<br>"
	+ "oj:JOL_2006_088_R_0063_01I";
pidHelpText[1] = pidHelpText[0];
pidHelpText[2] = pidHelpText[0];
pidHelpText[3] = pidHelpText[0];
pidHelpText[4] = "A <i>Manifestation ID</i> in format:<br>"
	+ "ps_identifier:manifestation_identifier<br /><br />"
	+ "Some valid examples:<br>"
	+ "cellar:f2315395-af0d-11e1-9093-01aa75ed71a1.0001.02<br>"
	+ "celex:32006D0241I.fra.PDF<br>"
	+ "oj:JOL_2006_088_R_0063_01I.fra.PDF";
pidHelpText[5] = "The ID in valid format of the object to search for,<br />"
	+ "which can be a work, an expression, a manifestation, a dossier, an event or an agent.";
pidHelpText[6] = "The ID in valid format of the object to search for,<br />"
	+ "which can be a work, a dossier or an agent.";
pidHelpText[7] = pidHelpText[0];