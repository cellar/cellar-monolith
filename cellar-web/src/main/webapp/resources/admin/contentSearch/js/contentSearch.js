$(document).ready(function() {
	$('#searchDiv img').balloon({ position: "bottom-right" });
});

// presets elements values at the loading of the page
onload = function()
{
	// update form's action and fields
	var contentTypeEl = document.getElementById("contentType");
	changeAction(contentTypeEl.options[contentTypeEl.selectedIndex].value);
	changeFields(contentTypeEl.selectedIndex);
	
	// sets text for contextual help pop-up boxes of static fields
	document.getElementById("decodingLanguageHelpImg").title = decodingLanguageHelpText;
	document.getElementById("acceptLanguageHelpImg").title = acceptLanguageHelpText;
	document.getElementById("acceptFormatHelpImg").title = acceptFormatHelpText;
	document.getElementById("excludeInferredHelpImg").title = excludeInferredHelpText;
}

// changes the form action depending on the current selection of content type to search
function changeAction(myAction) {
	var base = "contentSearch/";
	var currAction = document.forms["searchForm"].action;
	var endIndexOfBase = currAction.indexOf(base) + base.length;
	var context = currAction.substring(0, endIndexOfBase);
	document.forms["searchForm"].action = context + myAction;
}

// event listener for changes in select with id : contentType. Used to fix csp violation
$(function(){
	$("#contentType").change(function(){
		var select=document.getElementById("contentType");
		changeAction(select.options[select.selectedIndex].value);
		changeFields(select.selectedIndex);
	});
});


// changes the state of fields (enabled/disabled) depending on the current selection of content type to search
function changeFields(selectedIndex) {
	// sets text for contextual help pop-up boxes of dynamic fields
	var contentTypeEl = document.getElementById("contentType");
	document.getElementById("contentTypeHelpImg").title = contentTypeHelpText[contentTypeEl.selectedIndex];
	document.getElementById("pidHelpImg").title = pidHelpText[contentTypeEl.selectedIndex];

	// changes fields
	// case tree notice
	if (selectedIndex == 0) {
		document.getElementById("decodingLanguageLabel").style.color = "#000000";
		document.getElementById("decodingLanguage").disabled = null;
		document.getElementById("decodingLanguageHelpImg").style.visibility = "visible";
		
		document.getElementById("acceptLanguageLabel").style.color = "#999999";
        document.getElementById("acceptLanguage").disabled = "true";
		document.getElementById("acceptLanguageHelpImg").style.visibility = "hidden";
		
		document.getElementById("acceptFormatLabel").style.color = "#999999";
        document.getElementById("acceptFormat").disabled = "true";
		document.getElementById("acceptFormatHelpImg").style.visibility = "hidden";
		
		document.getElementById("excludeInferredLabel").style.color = "#999999";
        document.getElementById("excludeInferred").disabled = "true";
		document.getElementById("excludeInferredHelpImg").style.visibility = "hidden";
		
		document.getElementById("pidLabel").innerHTML = "Production System ID/PID:";
	}
	// case branch notice
	else if (selectedIndex == 1) {
		document.getElementById("decodingLanguageLabel").style.color = "#000000";
        document.getElementById("decodingLanguage").disabled = null;
		document.getElementById("decodingLanguageHelpImg").style.visibility = "visible";
		document.getElementById("decodingLanguageHelpImg").title = decodingLanguageHelpText;
		
        document.getElementById("acceptLanguageLabel").style.color = "#000000";
        document.getElementById("acceptLanguage").disabled = null;
		document.getElementById("acceptLanguageHelpImg").style.visibility = "visible";
		
        document.getElementById("acceptFormatLabel").style.color = "#999999";
        document.getElementById("acceptFormat").disabled = "true";
		document.getElementById("acceptFormatHelpImg").style.visibility = "hidden";
		
		document.getElementById("excludeInferredLabel").style.color = "#999999";
        document.getElementById("excludeInferred").disabled = "true";
		document.getElementById("excludeInferredHelpImg").style.visibility = "hidden";
		
		document.getElementById("pidLabel").innerHTML = "Production System ID/PID:";
    }
	// case object-work notice
	else if (selectedIndex == 2) {
		document.getElementById("decodingLanguageLabel").style.color = "#000000";
        document.getElementById("decodingLanguage").disabled = null;
		document.getElementById("decodingLanguageHelpImg").style.visibility = "visible";
		
        document.getElementById("acceptLanguageLabel").style.color = "#999999";
        document.getElementById("acceptLanguage").disabled = "true";
		document.getElementById("acceptLanguageHelpImg").style.visibility = "hidden";
		
        document.getElementById("acceptFormatLabel").style.color = "#999999";
        document.getElementById("acceptFormat").disabled = "true";
		document.getElementById("acceptFormatHelpImg").style.visibility = "hidden";
		
		document.getElementById("excludeInferredLabel").style.color = "#999999";
        document.getElementById("excludeInferred").disabled = "true";
		document.getElementById("excludeInferredHelpImg").style.visibility = "hidden";
		
		document.getElementById("pidLabel").innerHTML = "Production System ID/PID:";
	}
	// case object-expression notice
	else if (selectedIndex == 3) {
		document.getElementById("decodingLanguageLabel").style.color = "#000000";
        document.getElementById("decodingLanguage").disabled = null;
		document.getElementById("decodingLanguageHelpImg").style.visibility = "visible";
		
        document.getElementById("acceptLanguageLabel").style.color = "#000000";
        document.getElementById("acceptLanguage").disabled = null;
		document.getElementById("acceptLanguageHelpImg").style.visibility = "visible";
		
        document.getElementById("acceptFormatLabel").style.color = "#999999";
        document.getElementById("acceptFormat").disabled = "true";
		document.getElementById("acceptFormatHelpImg").style.visibility = "hidden";
		
		document.getElementById("excludeInferredLabel").style.color = "#999999";
        document.getElementById("excludeInferred").disabled = "true";
		document.getElementById("excludeInferredHelpImg").style.visibility = "hidden";
		
		document.getElementById("pidLabel").innerHTML = "Expr ID/PID:";
	}
	// case object-manifestation notice
	else if (selectedIndex == 4) {
		document.getElementById("decodingLanguageLabel").style.color = "#000000";
        document.getElementById("decodingLanguage").disabled = null;
		document.getElementById("decodingLanguageHelpImg").style.visibility = "visible";
		
        document.getElementById("acceptLanguageLabel").style.color = "#999999";
        document.getElementById("acceptLanguage").disabled = "true";
		document.getElementById("acceptLanguageHelpImg").style.visibility = "hidden";
		
        document.getElementById("acceptFormatLabel").style.color = "#999999";
        document.getElementById("acceptFormat").disabled = "true";
		document.getElementById("acceptFormatHelpImg").style.visibility = "hidden";
		
		document.getElementById("excludeInferredLabel").style.color = "#999999";
        document.getElementById("excludeInferred").disabled = "true";
		document.getElementById("excludeInferredHelpImg").style.visibility = "hidden";
		
		document.getElementById("pidLabel").innerHTML = "Manifestation ID:";
	}
	// case RDF-Object notice
	else if (selectedIndex == 5) {
		document.getElementById("decodingLanguageLabel").style.color = "#999999";
        document.getElementById("decodingLanguage").disabled = "true";
		document.getElementById("decodingLanguageHelpImg").style.visibility = "hidden";
		
        document.getElementById("acceptLanguageLabel").style.color = "#999999";
        document.getElementById("acceptLanguage").disabled = "true";
		document.getElementById("acceptLanguageHelpImg").style.visibility = "hidden";
		
        document.getElementById("acceptFormatLabel").style.color = "#999999";
        document.getElementById("acceptFormat").disabled = "true";
		document.getElementById("acceptFormatHelpImg").style.visibility = "hidden";
		
		document.getElementById("excludeInferredLabel").style.color = "#000000";
        document.getElementById("excludeInferred").disabled = null;
		document.getElementById("excludeInferredHelpImg").style.visibility = "visible";
		
		document.getElementById("pidLabel").innerHTML = "Object ID:";
	}
	// case RDF-Tree notice
	else if (selectedIndex == 6) {
		document.getElementById("decodingLanguageLabel").style.color = "#999999";
        document.getElementById("decodingLanguage").disabled = "true";
		document.getElementById("decodingLanguageHelpImg").style.visibility = "hidden";
		
        document.getElementById("acceptLanguageLabel").style.color = "#999999";
        document.getElementById("acceptLanguage").disabled = "true";
		document.getElementById("acceptLanguageHelpImg").style.visibility = "hidden";
		
        document.getElementById("acceptFormatLabel").style.color = "#999999";
        document.getElementById("acceptFormat").disabled = "true";
		document.getElementById("acceptFormatHelpImg").style.visibility = "hidden";
		
		document.getElementById("excludeInferredLabel").style.color = "#000000";
        document.getElementById("excludeInferred").disabled = null;
		document.getElementById("excludeInferredHelpImg").style.visibility = "visible";
		
		document.getElementById("pidLabel").innerHTML = "Object ID:";
	}
	// case content search
	else if (selectedIndex == 7) {
		document.getElementById("decodingLanguageLabel").style.color = "#999999";
        document.getElementById("decodingLanguage").disabled = "true";
		document.getElementById("decodingLanguageHelpImg").style.visibility = "hidden";
		
        document.getElementById("acceptLanguageLabel").style.color = "#000000";
        document.getElementById("acceptLanguage").disabled = null;
		document.getElementById("acceptLanguageHelpImg").style.visibility = "visible";
		
        document.getElementById("acceptFormatLabel").style.color = "#000000";
        document.getElementById("acceptFormat").disabled = null;
		document.getElementById("acceptFormatHelpImg").style.visibility = "visible";
		
		document.getElementById("excludeInferredLabel").style.color = "#999999";
        document.getElementById("excludeInferred").disabled = "true";
		document.getElementById("excludeInferredHelpImg").style.visibility = "hidden";
		
		document.getElementById("pidLabel").innerHTML = "Production System ID/PID:";
	}
}

//map the values of check box 'checkboxName' as "on"->"true", "off"->"false"
// TODO: remove it and migrate <input type="checkbox".. to <form:checkbox..
function transformCheckboxValue(checkboxName) {
	document.getElementById(checkboxName).value = document.getElementById(checkboxName).checked;
}