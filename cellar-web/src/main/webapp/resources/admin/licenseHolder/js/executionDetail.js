onload = (function (){
    $("#dialog").dialog({
        autoOpen: false,
		width: 500
    });

    $("a#dialogOpener").click(function() {
        $("#dialog").dialog("open");
        return false;
    });

    $("#backButton").click(function () {
        var url = $(this).attr("data-url");
        var id = $(this).attr("data-id");
        var redirectionUrl = url + id;
        var allowUrlList = ['/admin/license/configuration?configurationId='];

        if (allowUrlList.indexOf(url) > -1) {
            document.location.href = redirectionUrl;
        } else {
            alert("Invalid path for redirection");
        }
    });
});