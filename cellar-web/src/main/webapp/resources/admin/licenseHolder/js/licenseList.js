onload = (function (){
	$("#deleteDialog").dialog({
		autoOpen: false,
		width: 650,
		modal: true,
		buttons: {
			'Close':function() {
				$(this).dialog('close');
			},
			'Delete configuration': function() {
				var deleteDialog = $(this);
				var path = deleteDialog.data('link').href;
				deleteConfiguration(deleteDialog, path);
			},
			'Delete configuration and all archives': function() {
				var deleteDialog = $(this);
				var path = deleteDialog.data('link').href + '&withFinalArchive=true';
				deleteConfiguration(deleteDialog, path);
			}
		}
	});

	$(".deleteDialogOpener").click(function() {
		$("#deleteDialog").data('link', this).dialog("open");
		$("#deleteText").show();
		$("#deleteWaiting").hide();
		$("#errorDeleteText").hide();
		$(".ui-dialog-buttonpane button:contains('Delete configuration')").button("enable");
		$(".ui-dialog-buttonpane button:contains('Delete configuration and clean all archives')").button("enable");
		return false;
	});
	
	$("#restartDialog").dialog({
		autoOpen: false,
		width: 450,
		modal: true,
		buttons: {
			'Close':function() {
				$(this).dialog('close');
			},
			'Restart': function() {
				var dialog = $(this);
				var path = dialog.data('link').href;

				$(".ui-dialog-buttonpane button:contains('Restart')").button("disable");
				$("#restartText").hide();
				$("#restartWaiting").show();
				
				$.ajax({
					type: 'GET',
					url: path,
					async: true,
					success: function(data) {
						if(data) {
							$("#restartWaiting").hide();
							$("#errorRestartText").html(data);
							$("#errorRestartText").show();
						} else {
							dialog.dialog('close');
							location.reload();
						}
					}
				});
			}
		}
	});
	

	$(".restartDialogOpener").click(function() {
		$("#restartDialog").data('link', this).dialog("open");
		
		$("#restartText").show();
		$("#restartWaiting").hide();
		$("#errorRestartText").hide();
		$(".ui-dialog-buttonpane button:contains('Restart')").button("enable");
		
		return false;
	});
	
	$("#restartStepDialog").dialog({
		autoOpen: false,
		width: 450,
		modal: true,
		buttons: {
			'Close':function() {
				$(this).dialog('close');
			},
			'Restart': function() {
				var dialog = $(this);
				var path = dialog.data('link').href;

				$(".ui-dialog-buttonpane button:contains('Restart')").button("disable");
				$("#restartStepText").hide();
				$("#restartStepWaiting").show();
				
				$.ajax({
					type: 'GET',
					url: path,
					async: true,
					success: function(data) {
						if(data) {
							$("#restartStepWaiting").hide();
							$("#errorRestartStepText").html(data);
							$("#errorRestartStepText").show();
						} else {
							dialog.dialog('close');
							location.reload();
						}
					}
				});
			}
		}
	});
	

	$(".restartStepDialogOpener").click(function() {
		$("#restartStepDialog").data('link', this).dialog("open");
		
		$("#restartStepText").show();
		$("#restartStepWaiting").hide();
		$("#errorRestartStepText").hide();
		$(".ui-dialog-buttonpane button:contains('Restart')").button("enable");
		
		return false;
	});

	$("#jobAddButton").click(function () {
		var url = $(this).attr("data-url");
		var allowUrlList = ['/admin/license/add/job'];

		if (allowUrlList.indexOf(url) > -1) {
			document.location.href = url;
		} else {
			alert("Invalid path for redirection");
		}
	});

	$("#adhocAddButton").click(function () {
		var url = $(this).attr("data-url");
		var allowUrlList = ['/admin/license/add/adhoc'];

		if (allowUrlList.indexOf(url) > -1) {
			document.location.href = url;
		} else {
			alert("Invalid path for redirection");
		}
	});
});

function deleteConfiguration(deleteDialog, path) {
	$(".ui-dialog-buttonpane button:contains('Delete configuration')").button("disable");
	$(".ui-dialog-buttonpane button:contains('Delete configuration and clean all archives')").button("disable");
	$("#deleteText").hide();
	$("#deleteWaiting").show();
	$.ajax({
		type: 'GET',
		url: path,
		async: true,
		success: function(data) {
			if(data) {
				$("#deleteWaiting").hide();
				$("#errorDeleteText").html(data);
				$("#errorDeleteText").show();
			} else {
				deleteDialog.dialog('close');
				$(location).attr('href', getBaseUrl() + '/admin/license');
			}
		}
	});
}