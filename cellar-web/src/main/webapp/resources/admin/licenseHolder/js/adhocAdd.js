$(document).ready(function() {
	$("#addForm").validate();
	$('.changeTarget').change(function() {
		$.post(getBaseUrl() + '/admin/license/add/adhoc/getArchiveName', { language: $('select[name=language]').val() }, function(data) {
			$('input[name=archiveName]').val(data);
		});
	}).change();
	$('#startTime').timepicker({
		nowButtonText: 'Now'
	});

	$("#cancelButton").click(function () {
		var url = $(this).attr("data-url");
		var allowUrlList = ['/admin/license/list'];

		if (allowUrlList.indexOf(url) > -1) {
			document.location.href = url;
		} else {
			alert("Invalid path for redirection");
		}
	});
});