$(document).ready(function() {
  $('.changeTarget').change(function() {
    $.post(getBaseUrl() + '/admin/license/add/job/getArchivePath', { frequency: $('select[name=frequency]').val(), language: $('select[name=language]').val() }, function(data) {
      $('span[id=pathOverview]').text(data);
    });
    $.post(getBaseUrl() + '/admin/license/add/job/getArchiveName', { frequency: $('select[name=frequency]').val(), language: $('select[name=language]').val() }, function(data) {
        $('input[name=archiveName]').val(data);
      });
  }).change();
  $.validator.addMethod(
	    "startDate",
	    function(value, element) {
	    	var isValid = false;
	    	$.ajax({
	    		type: 'POST',
	    		url: getBaseUrl() + '/admin/license/add/job/isStartDateValid',
	    		data: { frequency: $('select[name=frequency]').val(), startDate: $('input[name=startDate]').val()},
	    		async: false,
	    		success: function(data) {
	    			isValid = (data == 'true');
	    		}});
	        return isValid;
	    },
	    "Please enter a valid date."
	);
  $('#addForm').validate({
      rules : {
    	  startDate : {
        	  startDate : true
          }
      }
  });
  $("#startDate").datepicker("setDate", new Date());

	$("#cancelButton").click(function () {
		var url = $(this).attr("data-url");
		var allowUrlList = ['/admin/license/list'];

		if (allowUrlList.indexOf(url) > -1) {
			document.location.href = url;
		} else {
			alert("Invalid path for redirection");
		}
	});
});