// Add a click event listener to the link
window.addEventListener("DOMContentLoaded", function () {
    document.getElementById("toggleJson").addEventListener("click",
        function (event) {
            event.preventDefault();
            changeJson(this);
        });
});

function changeJson(link) {
    event.preventDefault(); // Prevent the link from scrolling to the top of the page
    let jsonTextarea = document.getElementById('json');
    // Toggle the visibility of the textarea
    if (jsonTextarea.style.display === 'none') {
        jsonTextarea.style.display = 'block';
        link.textContent = 'Hide JSON';
    } else {
        jsonTextarea.style.display = 'none';
        link.textContent = 'Show JSON';
    }
}

$(document).ready(function () {
    $('#fileData').on('change', function() {
        // Remove existing error messages
        clearValidationErrorMessages();
    });

    $('#cancel').on('click', function() {
        // Remove existing error messages
        clearValidationErrorMessages();
    });

    $('#upload').on('click', function (event) {
        checkFileSize(event);
    });

    function checkFileSize(event) {
        var input = $('#fileData')[0];
        var errorMessageId = 'fileUploadLimitErrorMessage';
        var errorMessage = $('#' + errorMessageId);

        // Remove existing error message
        errorMessage.remove();

        var fileSize = input.files[0].size; // in bytes
        var maxSize = 10485760;

        if (fileSize > maxSize) {
            // File exceeds upload size limit
            errorMessage = $('<span>').attr('id', errorMessageId).css('color', 'red').text('Validation error. File exceeds file size limit.');

            // Insert the error message after the file input element
            $(input).after(errorMessage);

            // Clear the file input value to allow reselection
            $(input).val('');

            // Prevent the default click event, stopping it from continuing to the server
            event.preventDefault();
        }
    }

    function clearValidationErrorMessages() {
        $('#fileUploadLimitErrorMessage').remove();
        $('#fileData\\.errors').remove();
    }
});
