$(document).ready(function() {
	$("[id^=sv_a_]").click(function () {
		return toggleValidatorEnabled($(this).attr("data-id"));
	});
});

$(function()
{
    // always use this
    $.ajaxSetup({
        cache : false,        
        error: function(resp,status,xhr,form) {alert('error ' + resp + '  status ' + status);}
    });

    
});

//function to change the action submitted in a form.
function toggleValidatorEnabled(vid) {
	$.ajax({
		  type: 'POST',
		  dataType: 'json',
		  url: getBaseUrl() + '/admin/validator/toggleEnabled',
		  data: $("#form_sv_"+vid).serialize(),
		  success: function(data) {
				if (data.check) {
					$('#sv_enabled_'+ vid).val('true');
					$("#sv_img_"+vid).attr('src', $("#sv_img_"+vid).attr('src').replace('remove','checked'));
		  		}
				else {
					$('#sv_enabled_'+ vid).val('false');
					$("#sv_img_"+vid).attr('src', $("#sv_img_"+vid).attr('src').replace('checked','remove'));
				}
		  }
		});
	

}
