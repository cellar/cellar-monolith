$(document).ready(function() {
    $("#cancelButton").click(function () {
        var url = $(this).attr("data-url");
        var allowUrlList = ['/admin/security/access-requests'];

        if (allowUrlList.indexOf(url) > -1) {
            document.location.href = url;
        } else {
            alert("Invalid path for redirection");
        }
    });
});