$(document).ready(function() {
    $("#deleteButton").click(function () {
        var url = $(this).attr("data-url");
        var id = $(this).attr("data-id");
        var redirectionUrl = url + id;
        var allowUrlList = ['/admin/security/group/delete?id='];

        if (allowUrlList.indexOf(url) > -1) {
            document.location.href = redirectionUrl;
        } else {
            alert("Invalid path for redirection");
        }
    });

    $("#cancelButton").click(function () {
        var url = $(this).attr("data-url");
        var allowUrlList = ['/admin/security/group'];

        if (allowUrlList.indexOf(url) > -1) {
            document.location.href = url;
        } else {
            alert("Invalid path for redirection");
        }
    });
});