$(document).ready(function() {
    $("#deleteButton").click(function () {
        var url = $(this).attr("data-url");
        var id = $(this).attr("data-id");
        var redirectionUrl = url + id;
        var allowUrlList = ['/admin/security/user/delete?id='];

        if (allowUrlList.indexOf(url) > -1) {
            document.location.href = redirectionUrl;
        } else {
            alert("Invalid path for redirection");
        }
    });

    $("#cancelButton").click(function () {
        var url = $(this).attr("data-url");
        var allowUrlList = ['/admin/security/user'];

        if (allowUrlList.indexOf(url) > -1) {
            document.location.href = url;
        }
    });
});