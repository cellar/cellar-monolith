$(document).ready(function() {
     $('.deleteLink').on('click', function () {
        var username = $(this).attr("data-username");
        var confirmMessage = "Delete access request for user " + username + " ?";
        if (!confirm(confirmMessage)) {
        	return false;
    	}
    });
});