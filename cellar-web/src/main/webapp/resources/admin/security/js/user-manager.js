$(document).ready(function() {
    $("#addUserButton").click(function () {
        var url = $(this).attr("data-url");
        var allowUrlList = ['/admin/security/user/create'];

        if (allowUrlList.indexOf(url) > -1) {
            document.location.href = url;
        } else {
            alert("Invalid path for redirection");
        }
    });
});