$(document).ready(function() {
    $("#plusButton").click(function () {
        togglePsi($(this), $(this).attr("data-id"));
        return false;
    });

    $("#useButton").click(function () {
        replaceFilename($(this).attr("data-file-id"), $(this).attr("data-suggested-file-id"));
        return false;
    });

    $("#previousButton").click(function () {
        previousPage($(this).attr("data-current-page"));
        return false;
    });

    $("#nextButton").click(function () {
        nextPage($(this).attr("data-current-page"));
        return false;
    });

    $("#replaceAllButton").click(function () {
        replaceAllFilenames();
        return false;
    });
});

function replaceFilename(inputId, suggestedFilename) {
    document.getElementById(inputId).value = suggestedFilename;
}

function replaceAllFilenames() {
    var buttons = document.getElementsByClassName('replace-btn');

    for(var i = 0; i < buttons.length; ++i) {
        var button = buttons.item(i);
        button.click();
    }
}

function togglePsi(toggleButton, listId) {
    var list = document.getElementById(listId);

    if(toggleButton.innerHTML === '+') {
        toggleButton.innerHTML = '-';
        list.className = '';
    } else {
        toggleButton.innerHTML = '+';
        list.className = 'psi-hidden';
    }
}

function previousPage(currentPage) {
    var form = document.forms.search_form;
    form.page.value = currentPage - 1;
    form.submit();
}

function nextPage(currentPage) {
    var form = document.forms.search_form;
    form.page.value = currentPage + 1;
    form.submit();
}
