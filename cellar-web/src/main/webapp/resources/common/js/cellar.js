function getBaseUrl() {
    return document.getElementById('baseUrl').getAttribute('value');
}

$(document).ready(function() {
    $(function() {
        $('.tooltipHelper').balloon({ position: "bottom-right" });
    });

    $(".container .menu ul li").each(function(i) {
    var href = $(this).children(":first-child").attr("href");

    var currentPath=window.location.pathname;

    if (currentPath.search(href) == 0){
        $(this).addClass("selected");
    }else if($(this).text()=='Visibility'){
        if(currentPath.search("/cellar-web/admin/cmr/embargo/list")==0){
            $(this).addClass("selected");
        }
    } else{
        $(this).removeClass("selected");
    }
    });

    $(".datepicker").each(function(i) {
        $(this).datepicker({dateFormat: "dd/mm/yy"});
    });

    //admin/dashboard
    $("#btnLoad").click(function(){
        $("#ajax-content").empty().html('<img src="/resources/common/admin/css/images/ajax-loader-small.gif" />');
        $("#ajax-content").load("dashboard-stats");
    });
});

