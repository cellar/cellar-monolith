$(document).ready(function() {
  /* retrieve example query's */
  $.getJSON('endpoint/getQueries', function(data) {
    var queryOptions;
    $.each(data, function(i, query) {
      queryOptions += '<option value="' + query.id + '">' + query.name + '</option>\n';
    });
    $('.example-query-wrapper select[name=example-query]').html(queryOptions);
  });

  /* select query */
  $('.example-query-wrapper select[name=example-query]').change(function() {
    $.post("endpoint/getQuery", { q: $(this).val() }, function(data) {
      $('#form #query-field').val(data);
    });
  });

  /* set output parameter */
  function setOutput(value) {
    $('#form input[name=output]').val(value);
  }

  /* set output parameter when selecting a stylesheet */
  /*$('#form select[name=stylesheet]').change(function() {
   var mimetype = $(this).find('option:selected').parent().attr('label');
   setOutput(mimetype);
   });*/

  /* set output parameter when checking json checkbox */
  $('#form input[name=json]').click(function() {
    if ($(this).is(':checked')) {
      setOutput($(this).val());
      $('#form select[name=stylesheet]').attr('disabled', 'disabled');
    }
    else {
      setOutput('');
      $('#form select[name=stylesheet]').removeAttr('disabled');
    }
  });

  /* submit query button */
  $('#btnSubmitQuery').click(function() {
    var url = $('#form').attr('action');
    var query = $('#form').serialize();
    var client = new XMLHttpRequest();
    var mimetype = $('#form select[name=stylesheet]').find('option:selected').parent().attr('label');
    client.open("GET", url + "?" + query);
    client.setRequestHeader("Accept", mimetype);
    client.send();
  });

  $('#deleteStylesheet').click(function(e) {
    var values = $('#listXslts').val();
    if (values != null) {
      values = values.join(',');
      var remove = confirm("Do you really want to remove all these file(s): " + values + " ?");
      if (remove) {
        $('.content .sparql .content .wrapper .xslt-container .xslt-status').css('display', 'block');
        $('.content .sparql .content .wrapper .xslt-container .xslt-status').text('Deleting stylesheet(s): ' + values);
        $.post("configuration/deleteStylesheets",
               {
                 stylesheets: values
               },
               function(data) {
                 $('.content .sparql .content .wrapper .xslt-container .xslt-status').css('display', 'none');
                 location.reload(true);
               }
            );
      }
    }
    else {
      alert("Select at least one stylesheet.");
    }
  });

  $('#deleteDefinition').click(function(e) {
    var values = $('#listDefinitions').val();
    if (values != null) {
      values = values.join(',');
      var remove = confirm("Do you really want to remove all these file(s): " + values + " ?");
      if (remove) {
        $('.content .sparql .content .wrapper .definition-container .definition-status').css('display', 'block');
        $('.content .sparql .content .wrapper .definition-container .definition-status').text('Deleting definition(s): ' + values);
        $.post("configuration/deleteDefinitions",
               {
                 definitions: values
               },
               function(data) {
                 $('.content .sparql .content .wrapper .definition-container .definition-status').css('display', 'none');
                 location.reload(true);
               }
            );
      }
    }
    else {
      alert("Select at least one definition.");
    }
  });

  $('#printStylesheet').click(function(e) {
    //Open Redirect: https://learn.snyk.io/lessons/open-redirect/javascript/
    var values = $('#listXslts').val();
    var allowUrlList = ['/admin/cmr/sparql/configuration/printStylesheets'];

    if (values.length > 0 && allowUrlList.indexOf('/admin/cmr/sparql/configuration/printStylesheets') > -1) {
      values = values.join(',');
      var url = '/admin/cmr/sparql/configuration/printStylesheets';
      var redirectionUrl = url + '?q=' + values;
      window.open(redirectionUrl);
    }
    else {
      alert('Select at least one stylesheet.');
    }
  });

  $('#printDefinition').click(function(e) {
    //Open Redirect: https://learn.snyk.io/lessons/open-redirect/javascript/
    var values = $('#listDefinitions').val();
    var allowUrlList = ['/admin/cmr/sparql/configuration/printDefinitions'];

    if (values.length > 0 && allowUrlList.indexOf('/admin/cmr/sparql/configuration/printDefinitions') > -1) {
      values = values.join(',');
      var url = '/admin/cmr/sparql/configuration/printDefinitions';
      var redirectionUrl = url + '?q=' + values;
      window.open(redirectionUrl);
    }
    else {
      alert('Select at least one definition.');
    }
  });

});