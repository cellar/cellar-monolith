<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="app" tagdir="/WEB-INF/tags"%>
<%@ attribute name="node" required="true" type="eu.europa.ec.opoce.cellar.cmr.metsbeans.HierarchyElement"%>
<c:if test="${not empty node}">
	<li id="${node.cellarId.identifier}" <c:if test="${node.type == 'MANIFESTATION'}">class="notExpandAll"</c:if>>
	   <div>
			<c:choose>
				<c:when test="${node.type == 'ITEM'}">
					<a target="_blank" href=<c:url value="/admin/search/content?cellarId=${node.cellarId.identifier}"/>>${node.type}</a>
				</c:when>
				<c:otherwise>
					<a target="_blank" href=<c:url value="/admin/search/object-notice?cellarId=${node.cellarId.identifier}"/>>${node.type}</a>
				</c:otherwise>
			</c:choose>
			
			<div align="center" class="actions">
				<c:choose>
					<c:when test="${node.type != 'ITEM'}">
						<c:choose>
							<c:when test="${!node.underEmbargo}">
								<a class="workEmbargoer" id="emb_wor_${param.objectId}" style="padding-left:0px;" title="<fmt:message key="cmr.visibility.search.tree.command.embargo" />" href=<c:url value="/admin/cmr/visibility/search/embargo?cellarId=${node.cellarId.identifier}"/>>
									<img id="hide" src="${base}/resources/common/admin/images/hide.png" />
								</a>
							</c:when>
							<c:when test="${node.underEmbargo}">
								<a class="workDisembargoer" id="des_wor_${param.objectId}" style="padding-left:0px;" title="<fmt:message key="cmr.visibility.search.tree.command.disembargo" />" href=<c:url value="/admin/cmr/visibility/search/disembargo?cellarId=${node.cellarId.identifier}"/>>
									<img id="show" src="${base}/resources/common/admin/images/show.png" />
								</a>
								<a class="workEmbargoer" id="che_wor_${param.objectId}" style="padding-left:0px;"
								   title="<fmt:message key="cmr.visibility.search.tree.command.changeEmbargo" />"
								   href=<c:url value="/admin/cmr/visibility/search/embargo?cellarId=${node.cellarId.identifier}"/>
								>
									<img id="changeEmbargo" src="${base}/resources/common/admin/images/change-embargo.png" />
								</a>
							</c:when>
						</c:choose>
					</c:when>
					<c:otherwise>
						-
					</c:otherwise>
				</c:choose>
			</div>
			
			<div align="center" class="embargodate">
				<c:choose>
					<c:when test="${node.underEmbargo}">
						<fmt:formatDate value="${node.embargoDate}" pattern="dd/MM/yyyy HH:mm" />
					</c:when>
					<c:otherwise>
						-
					</c:otherwise>
				</c:choose>
			</div>
			
			<div align="center" class="visible">
				<c:choose>
					<c:when test="${node.underEmbargo}">
						<img id="disabled" src="${base}/resources/common/admin/images/disabled.png" />
					</c:when>
					<c:otherwise>
						<img id="enabled" src="${base}/resources/common/admin/images/enabled.png" />
					</c:otherwise>
				</c:choose>
			</div>
			<div align="center" class="pids">
				<a class="pidsDialogOpener" style="padding-left:0px;" title="<fmt:message key="cmr.visibility.search.tree.command.productionIdentifiers" />" href=<c:url value="/admin/search/production-identifiers?cellarId=${node.cellarId.identifier}"/> id="dialogOpener"><img id="enabled" src="${base}/resources/common/admin/images/search.png" /></a>
			</div>
			<div class="cellarid">
				${node.cellarId.identifier}
			</div>
		</div>
		<c:if test="${not empty node.children}">
			<ul>
				<c:forEach items="${node.children}" var="child" varStatus="status">
					<app:visibility-tree-node node="${child}" />
				</c:forEach>
			</ul>			
		</c:if>					
	</li>
</c:if>