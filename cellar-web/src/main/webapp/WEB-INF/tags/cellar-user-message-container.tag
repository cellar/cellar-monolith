<%@ taglib prefix="app" tagdir="/WEB-INF/tags"%><%--
--%><%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%><%--
--%><%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%><%--
--%><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%><%--
--%><c:if test="${not empty sessionScope['scopedTarget.cellarUserMessageContainer']}"><%--
	--%><app:cellar-user-message items="${sessionScope['scopedTarget.cellarUserMessageContainer'].infoMessages}" cssClass="userMessageInfo" /><%--
	--%><app:cellar-user-message items="${sessionScope['scopedTarget.cellarUserMessageContainer'].successMessages}" cssClass="userMessageSuccess" /><%--
	--%><app:cellar-user-message items="${sessionScope['scopedTarget.cellarUserMessageContainer'].errorMessages}" cssClass="userMessageError" /><%--
	--%><app:cellar-user-message items="${sessionScope['scopedTarget.cellarUserMessageContainer'].warningMessages}" cssClass="userMessageWarning" /><%--
--%></c:if>