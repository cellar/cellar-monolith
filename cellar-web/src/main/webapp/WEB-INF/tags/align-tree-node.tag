<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="app" tagdir="/WEB-INF/tags"%>
<%@ attribute name="node" required="true" type="eu.europa.ec.opoce.cellar.cl.domain.aligner.HierarchyNode"%>
<c:if test="${not empty node}">
	<li id="${node.cellarId}">
	   <div>	   		
			<c:choose>
                <c:when test="${!node.aligned}"><a href="#">UNDEFINED</a></c:when>
                <c:when test="${node.type == 'ITEM'}">
                    <a target="_blank" href=<c:url value="/admin/search/content?cellarId=${node.cellarId}"/>>${node.type}</a>
                </c:when>
                <c:otherwise>
                    <a target="_blank" href=<c:url value="/admin/search/object-notice?cellarId=${node.cellarId}"/>>${node.type}</a>
                </c:otherwise>
            </c:choose>
			
			<div align="center" class="aligned">
				<c:choose>
					<c:when test="${!node.aligned}">
						<img id="misaligned" class="tooltipHelper" src="${base}/resources/common/admin/images/disabled.png" 
							title='<spring:message code="aligner.align.missing"/>
								<c:forEach var="missingRepository" items="${node.missingRepositories}" varStatus="status">
									 <br /><spring:message code="aligner.repository.${missingRepository}"/>
				        		</c:forEach>'
				        />
					</c:when>
					<c:when test="${node.aligned}">
						<img id="aligned" src="${base}/resources/common/admin/images/enabled.png" />
					</c:when>
				</c:choose>
			</div>
			<div align="center" class="pids">
				<a class="pidsDialogOpener" style="padding-left:0px;" title="<fmt:message key="aligner.align.tree.command.productionIdentifiers" />" href=<c:url value="/admin/search/production-identifiers?cellarId=${node.cellarId}"/> id="dialogOpener"><img id="enabled" src="${base}/resources/common/admin/images/search.png" /></a>
			</div>
			<div class="cellarid" <c:if test="${node.evicted}">style="color:red;"</c:if>>
				${node.cellarId}
			</div>
		</div>
		<c:if test="${not empty node.children}">
			<ul>
				<c:forEach items="${node.children}" var="child" varStatus="status">
					<app:align-tree-node node="${child}" />
				</c:forEach>
			</ul>			
		</c:if>					
	</li>
</c:if>