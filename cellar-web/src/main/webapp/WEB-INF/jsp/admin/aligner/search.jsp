<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="app" tagdir="/WEB-INF/tags"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/cmr/visibility/js/search-tree.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/admin/aligner/js/align-tree.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/cmr/visibility/css/search-tree.css" type="text/css"/>
<style>



div.cellarid{float:right;width:640px;}
div.pids{float:right;width:50px;}
div.aligned{float:right;width:45px;}

#type{float:right}

</style>

<form:form method="GET" id="alignerSearchObjectForm" modelAttribute="search">
	<fmt:message key="aligner.align.objectId" />
	<form:input id="objectId" path="objectId" value="${param.objectId}" autofocus="autofocus"/>
   	<input type="submit" id="submitButton" value="<fmt:message key="aligner.align.command.search" />"/>
</form:form>

<c:if test="${hierarchy != null}">
	<noscript><p><strong><fmt:message key="common.message.noJavascript" /></strong></p></noscript>
	<br/>
	<br/>
	<a href="#" id="expandAll" class="expand"><fmt:message key="aligner.align.expandAll" /></a>|<a href="#" id="collapseAll" class="expand"><fmt:message key="aligner.align.collapseAll" /></a>
	<br/>
	<ul id="tree" style="min-width: 840px; max-width: 1100px">
		<li><div>Type<div align="center" class="aligned"><fmt:message key="aligner.align.tree.aligned" /></div><div align="center" class="pids"><fmt:message key="aligner.align.tree.productionIdentifiers" /></div><div align="center" class="cellarid"><fmt:message key="aligner.align.tree.cellarId" /></div></div></li>
		<app:align-tree-node node="${hierarchy.root}" />
	</ul>
	<br/>
	<c:if test="${!hierarchy.aligned}">
		<div style="text-align:center;">
			<button class="alignerButton" id="alignerButton" data-id="${hierarchy.cellarId}"><fmt:message key="aligner.align.command.align" /></button>
		</div>
	</c:if>
	
	<div id="pidsDialog" hidden="true" title="<fmt:message key="aligner.align.dialog.productionIdentifiers.title" />">
		<div id="pidsText">
		</div>
	</div>

		
	<div id="alignerDialog" hidden="true" title="<fmt:message key="aligner.align.dialog.aligner.title" />">
		<div id="alignerText">
		    <p><fmt:message key="aligner.align.dialog.aligner.text" /></p>
		</div>
		<div id="alignerWaiting" style="text-align:center;">
			<p><img src="<c:url value="/resources/common/admin/css/images/ajax-loader-medium.gif" />" /></p>
			<p><fmt:message key="aligner.align.dialog.aligner.loading" /></p>
		</div>
		<div id="alignerError">
		</div>
	</div>
</c:if>
