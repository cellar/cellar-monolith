<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<ul>
  <sec:authorize access="hasRole('ROLE_HIERARCHY_ALIGNER')">
  	<li><a href="${pageContext.request.contextPath}/admin/aligner/align"><fmt:message key="aligner.menu.search" /></a></li>
  	<li><a href="${pageContext.request.contextPath}/admin/aligner/dashboard"><fmt:message key="aligner.menu.dashboard" /></a></li>
  	<li><a href="${pageContext.request.contextPath}/admin/aligner/misalignments"><fmt:message key="aligner.menu.misalignments" /></a></li>
  </sec:authorize>
</ul>