<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ taglib prefix="stringutils" uri="stringutils" %>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/common/js/jquery.cookie.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/common/js/jquery.datatables.1.13.1.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/common/css/jquery.dataTables.min.css" type="text/css"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/admin/aligner/js/misalignments.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/admin/aligner/css/misalignments.css" type="text/css"/>

<div id="base" style="display:none;">${base}</div>
<div id="alignerBase" style="display:none;"><spring:url value="/admin/aligner/align"/></div>


<div class="dashboard">
    <fieldset>
        <legend><fmt:message key="aligner.misalignments.title" /></legend>
        </br>
        <spring:message code="aligner.misalignments.info"/>
        </br>
        
        <fieldset id="filterFieldset">
	        <div>
	            <fmt:message key="aligner.misalignments.table.filter.cellarId" />
	            <div id="cellarId_filter">
<!--there's a magnifying glass png on the input, the space is so that the placeholder is not on top of it  -->
		            <input type="text" required id="cellarId_filter_input" placeholder="       Search..." autofocus >
	                <input type="reset" value="x" id="resetCellarId_filter">
	            </div>
	            
	        </div>
	        
	        <div>
	            <fmt:message key="aligner.misalignments.table.filter.operationType" />
	            <div id="operationType_filter">
	               <select>
                       <option value=""></option>
                       <c:forEach var="operationType" varStatus="status" items="${operationTypes}">
                           <option value="${operationType}">${operationType}</option>
                       </c:forEach>
                   </select>
	            </div>
	        </div>
	        
	        <div>
	            <fmt:message key="aligner.misalignments.table.filter.repository" />
	            <div id="repository_filter">
	               <select>
	                   <option value=""></option>
	                   <c:forEach var="alignerRepository" varStatus="status" items="${alignerRepositories}">
		                   <option value="${alignerRepository}">${alignerRepository}</option>
	                   </c:forEach>
                   </select>
	            </div>
	        </div>
        </fieldset>
        
        
     <div id="alignerMisalignmentsTableContainer">
	     <table id="alignerMisalignmentsTable">
	      <thead>
	      <tr>
	        <th id="cellarId_header"><spring:message code="aligner.misalignments.table.column.cellarid"/></th>
	        <th id="operationDate_header"><spring:message code="aligner.misalignments.table.column.operationDate"/></th>     
	        <th id="operationType_header"><spring:message code="aligner.misalignments.table.column.operationType"/></th>
	        <th id="repository_header"><spring:message code="aligner.misalignments.table.column.repository"/></th>
	        <th id="hierarchy_header"><spring:message code="aligner.misalignments.table.column.hierarchy"/></th>
	      </tr>
	      </thead>
	      <tbody>
	      <c:forEach var="alignerMisalignment" varStatus="status" items="${alignerMisalignments}">
	        <tr class="single-nal">
	            <td>${alignerMisalignment.cellarId}</td>
	            <td><fmt:formatDate value="${alignerMisalignment.operationDate}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
	            <td>${stringutils:capitalize(alignerMisalignment.operationType)}</td>
	            <td>${alignerMisalignment.repository}</td>
	            <td>
	                <a href="<spring:url value="/admin/aligner/align"/>/${alignerMisalignment.cellarId}">
	                    <img border="0" alt="See hierarchy" src="${base}/resources/common/admin/images/hierarchy.png">
	                </a>
	            </td>
	        </tr>
	      </c:forEach>
	      </tbody>
	    </table>
    </div>
        
    </fieldset>
</div>