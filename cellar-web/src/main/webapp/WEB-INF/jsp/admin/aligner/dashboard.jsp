<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/admin/aligner/js/dashboard.js"></script>

<div class="dashboard">
    <fieldset>
        <legend><fmt:message key="aligner.dashboard.title" /></legend>
        
        <div>
            <fmt:message key="aligner.dashboard.status" /> <b title="<fmt:message key="aligner.dashboard.status.tooltip.${alignerStatus}" />"><fmt:message key="aligner.dashboard.status.${alignerStatus}" /></b>
   
            <c:choose>
                <c:when test="${alignerStatus == 'WORKING'}">
                	<input id="stopAligningButton" type="button" value="<fmt:message key="aligner.dashboard.stop" />" data-url="${pageContext.request.contextPath}/admin/aligner/dashboard/stopExecutor"/>
                </c:when>
                <c:when test="${alignerStatus == 'NOT_WORKING'}">
                	<input id="startAligningButton" type="button" value="<fmt:message key="aligner.dashboard.start" />" data-url="${pageContext.request.contextPath}/admin/aligner/dashboard/startExecutor"/>
                </c:when>
            </c:choose>
        </div>
        
        <c:set var="alignerMode" value="${cellarConfiguration.cellarServiceAlignerMode}" />
        
        <div>
            <fmt:message key="aligner.dashboard.mode" /> <b><fmt:message key="aligner.dashboard.executor.mode.${alignerMode}" /></b>
        </div>
        <div>
            <fmt:message key="aligner.dashboard.counter.label.resources" /> <b><span id="numberOfResources"> </span></b>
            <input id="numberOfResourcesButton" type="button" value="<fmt:message key="aligner.dashboard.counter.button.label.resources" />" />
        </div>
            
            <div>
                <fmt:message key="aligner.dashboard.counter.label.diagnosable.resources" /> <b><span id="numberOfResourcesDiagnosable"> </span></b>
                <input id="numberOfResourcesDiagnosableButton" type="button" value="<fmt:message key="aligner.dashboard.counter.button.label.diagnosable.resources" />" />
            </div>
        
            <div>
                <fmt:message key="aligner.dashboard.counter.label.error.resources" /> <b><span id="numberOfResourcesFixable"> </span></b>
                <input id="numberOfResourcesFixableButton" type="button" value="<fmt:message key="aligner.dashboard.counter.button.label.error.resources" />" />
            </div>
        
        <div>
            <fmt:message key="aligner.dashboard.counter.label.rows.aligner.difference" /> <b><span id="numberOfResourcesInTable"> </span></b>
            <input id="numberOfResourcesInTableButton" type="button" value="<fmt:message key="aligner.dashboard.counter.button.label.rows.aligner.difference" />" />
        </div>
        
    </fieldset>
</div>