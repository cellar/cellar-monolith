<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/cmr/batch-job-update/js/job-manager.js"></script>


<display:table name="batchJobs" id="batchJob"  pagesize="20" defaultsort="1" defaultorder="ascending" requestURI="">
	<display:column property="jobName" titleKey="cmr.batch-job.manager.jobTitle" sortable="true" headerClass="sortable" href="${pageContext.request.contextPath}${functionalityUrl}/consult" paramId="id" paramProperty="id" />
	<display:column sortProperty="jobStatus" titleKey="cmr.batch-job.manager.status" sortable="true" headerClass="sortable">
		<fmt:message key="cmr.batch-job.job.status.${batchJob.jobStatus}" />
	</display:column>
	<display:column titleKey="cmr.batch-job.manager.createDate">
		<fmt:formatDate value="${batchJob.creationDate}" pattern="dd/MM/yyyy HH:mm" />
	</display:column>
	<display:column titleKey="cmr.batch-job.manager.lastModified">
		<fmt:formatDate value="${batchJob.lastModified}" pattern="dd/MM/yyyy HH:mm" />
	</display:column>
	<display:column property="numberItems" titleKey="cmr.batch-job.manager.numberOfItems" />
	<display:column titleKey="cmr.batch-job.manager.actions">
		<c:if test="${batchJob.deletable}">
			<a href=<c:url value="${functionalityUrl}/delete?id=${batchJob.id}"/>><fmt:message key="cmr.batch-job.manager.command.delete" /></a><br />
		</c:if>
<%-- 		<c:if test="${batchJob.startable}"> --%>
<%-- 			<a href=<c:url value="${functionalityUrl}/launch?id=${batchJob.id}"/>><fmt:message key="cmr.batch-job.manager.command.launch" /></a><br /> --%>
<%-- 		</c:if> --%>
		<c:if test="${batchJob.restartable}">
			<a href=<c:url value="${functionalityUrl}/restart?id=${batchJob.id}"/>><fmt:message key="cmr.batch-job.manager.command.restart" /></a><br />
		</c:if>
		<a href=<c:url value="${functionalityUrl}/edit?id=${batchJob.id}"/>><fmt:message key="cmr.batch-job.manager.command.edit" /></a>
	</display:column>
</display:table>

<div style="text-align:center;">
	<input id="addJobButton" type="button" value="<fmt:message key="cmr.batch-job.manager.command.addJob" />" data-url="${pageContext.request.contextPath}${functionalityUrl}/create" />
	<c:choose>
		<c:when test="${areSchedulersRunning}">
			<input id="stopSchedulers" type="button" value="<fmt:message key="cmr.batch-job.manager.command.stopSchedulers" />" data-url="${pageContext.request.contextPath}${functionalityUrl}/stopSchedulers" />
		</c:when>
		<c:otherwise>
			<input id="restartSchedulers" type="button" value="<fmt:message key="cmr.batch-job.manager.command.restartSchedulers" />" data-url="${pageContext.request.contextPath}${functionalityUrl}/restartSchedulers" />
		</c:otherwise>	
	</c:choose>
</div>