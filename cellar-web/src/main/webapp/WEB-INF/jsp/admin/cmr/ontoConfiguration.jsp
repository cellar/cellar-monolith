<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> 

<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/cmr/onto/css/onto.css" type="text/css"/>


<script type="text/javascript" src="${pageContext.request.contextPath}/resources/cmr/onto/js/onto.js"></script>


<div class="onto-wrapper"> 

  
<form id="fileupload" action="${pageContext.request.contextPath}/admin/cmr/onto/uploadOntologyModel" method="POST" enctype="multipart/form-data">


<div>
    <div class="title"><spring:message code="ontologies.view.title"/></div>
    <div class="explanation"><spring:message code="ontologies.view.title.info"/></div>
</div>
<br>

<div>
    <noscript><input type="hidden" name="redirect" value="${pageContext.request.contextPath}/admin/cmr/onto"></noscript>
    <div class="fileupload-buttonbar">
        <div class="fileupload-buttons">
            
            <span class="fileinput-button">
                <span><spring:message code="ontologies.view.button.add"/></span>
                <input id="files" type="file" name="files[]" multiple="multiple">
            </span>
            <button type="submit" class="start"><spring:message code="ontologies.view.button.start"/></button>
            <button type="reset" class="cancel"><spring:message code="ontologies.view.button.cancel"/></button>
            <span class="fileupload-process"></span>
        </div>
        <div class="fileupload-progress fade" style="display:none">
            <div class="progress" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
            <div class="progress-extended">&nbsp;</div>
        </div>
    </div>
    <table role="presentation" style="width:60%!important;">
        <tbody class="files"></tbody>
    </table>
    
    </div>

  
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <a class="play-pause"></a>
    <ol class="indicator"></ol>
</div>


 
  
  </form>


  <div class="show-ontos">
    <div class="title"><spring:message code="ontologies.view.loaded.ontologies.table.title"/></div>
    <table>
      <thead>
      <tr>
        <th><spring:message code="ontologies.view.loaded.ontologies.table.column.name"/></th>
        <th><spring:message code="ontologies.view.loaded.ontologies.table.column.uri"/></th>
            <th><spring:message code="ontologies.view.loaded.ontologies.table.column.information"/></th>
            <th><spring:message code="ontologies.view.loaded.ontologies.table.column.activationDate"/></th>
        <th><spring:message code="ontologies.view.loaded.ontologies.table.column.externalPid"/></th>
      </tr>
      </thead>
      <tbody>
      <c:forEach items="${versions}" var="version">
          <tr class="single-nal">
              <td class="name"><c:out value="${version.name}"/></td>
              <td class="uri"><c:out value="${version.uri}"/></td>
              <td class="versionNumber"><c:out value="${version.nalOntoVersion.versionNumber}"/></td>
              <td class="versionDate"><c:out value="${version.nalOntoVersion.formattedVersionDate}"/></td>
              <td class="externalPid"><c:out value="${version.nalOntoVersion.externalPid}"/></td>
          </tr>
      </c:forEach>
      </tbody>
    </table>
  </div>

</div>











