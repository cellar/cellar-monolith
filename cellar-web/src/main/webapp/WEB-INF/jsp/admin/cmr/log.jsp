<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
  List<String> logs = (List<String>) request.getAttribute("logs");
  Map<String, Integer> expressionsPerLanguage = (Map<String, Integer>) request.getAttribute("expressionsPerLanguage");
  Map<String, Integer> manifestationsPerType = (Map<String, Integer>) request.getAttribute("manifestationsPerType");
  Map<String, Map<String, Integer>> manifestationsPerTypePerLanguage = (Map<String, Map<String, Integer>>) request.getAttribute("manifestationsPerTypePerLanguage");
  List<String> languagesForTable = (List<String>) request.getAttribute("languagesForTable");
%>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/cmr/log/js/log.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/cmr/log/css/log.css" type="text/css"/>
<div class="log-wrapper">
<%--
  <div class="title">Dashboard</div>
  <div class="separator"><span class="status"># works:</span><span class="text"><br/><c:out value="${totalWorks}"/></span></div>
  <div class="separator"><span class="status"># expressions per language:</span><span class="text"><br/>
    <%
      StringBuilder sb = new StringBuilder();
      for (String language : expressionsPerLanguage.keySet()) {
        Integer total = expressionsPerLanguage.get(language);
        sb.append(language).append(": ").append(total).append(", ");
      }
      String value = "None";
      if (sb.toString().length() >= 2) {
        value = sb.toString().substring(0, sb.toString().length() - 2);
      }
    %>
    <%=value%>
  </span></div>
  <div class="separator"><span class="status"># manifestations per type:</span><span class="text"><br/>
    <%
      sb = new StringBuilder();
      for (String type : manifestationsPerType.keySet()) {
        Integer total = manifestationsPerType.get(type);
        sb.append(type).append(": ").append(total).append(", ");
      }
      value = "None";
      if (sb.toString().length() >= 2) {
        value = sb.toString().substring(0, sb.toString().length() - 2);
      }
    %>
    <%=value%>
  </span></div>
  <div class="separator"><span class="status"># manifestations per language and type:</span><span class="text"><br/>
    <%
      sb = new StringBuilder();
      sb.append("<table class=\"logDashBoardTable\" cellspacing=\"0\" cellpadding=\"0\">");

      /* table headers */
      sb.append("<tr class=\"header\">").append("<td class=\"type\"></td>");
      for (String language : languagesForTable) {
        sb.append("<td>").append(language).append("</td>");
      }
      sb.append("</tr>");

      /* table body */
      Map<String, Integer> globalLanguageCount = new HashMap<String, Integer>();
      for (String type : manifestationsPerTypePerLanguage.keySet()) {
        sb.append("<tr><td class=\"type\">").append(type).append("</td>");
        for (String language : languagesForTable) {
          Integer totalLanguagesForThisType = manifestationsPerTypePerLanguage.get(type).get(language);
          if (null != totalLanguagesForThisType) {
            sb.append("<td>").append(totalLanguagesForThisType).append("</td>");
            if (null != globalLanguageCount.get(language)) {
              globalLanguageCount.put(language, globalLanguageCount.get(language) + totalLanguagesForThisType);
            }
            else {
              globalLanguageCount.put(language, totalLanguagesForThisType);
            }
          }
          else {
            sb.append("<td>").append(0).append("</td>");
          }
        }
        sb.append("</tr>");
      }

      /* table footer */
      sb.append("<tr>").append("<td class=\"total type\">").append("Total").append("</td>");
      for (String language : languagesForTable) {
        if (null != globalLanguageCount.get(language)) sb.append("<td class=\"total\">").append(globalLanguageCount.get(language)).append("</td>");
      }
      sb.append("</tr>");
      sb.append("</table>");
      value = sb.toString();

      if (manifestationsPerTypePerLanguage.isEmpty()) value = "None";
    %>
    <%=value%>
  </span></div>
  --%>
  <div class="title">Log configuration</div>
  <div class="sorting">
    <a href="${pageContext.request.contextPath}<c:out value="${today}"/>" class="first sorting">Today</a>
    <a href="${pageContext.request.contextPath}<c:out value="${yesterday}"/>" class="sorting">Yesterday</a>
    <a href="${pageContext.request.contextPath}<c:out value="${week}"/>" class="sorting">This week</a>
    <a href="${pageContext.request.contextPath}<c:out value="${all}"/>" class="sorting last">All</a>
  </div>
  <div class="log-status"></div>
  <select class="log-select" id="logSelect" multiple size="15">
    <% for (String log : logs) { %>
    <option value="<%=log%>"><%=log%>
    </option>
    <% } %>
  </select>

  <div class="button-wrapper">
    <button name="getLog" id="getLog" url="${pageContext.request.contextPath}<c:out value="${logUrl}"/>">Get</button>
    <button name="clearLogs" id="clearLogs">Clear logs</button>
  </div>
  <div class="title">Log level</div>
  <form action="${pageContext.request.contextPath}/admin/cmr/log/setLogLevel" method="POST">
    <span class="text">Current log level:</span>
    <span class="status"><c:out value="${logLevel}"/></span>
    <select name="logLevel">
      <option selected></option>
      <option value="info">Info</option>
      <option value="debug">Debug</option>
    </select>
    <input type="submit" value="Set log level" class="button"/>
  </form>
</div>