<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<ul>
    <sec:authorize access="hasRole('ROLE_UPDATE')">
        <li><a href="${pageContext.request.contextPath}/admin/cmr/update/job/sparql-update"><fmt:message key="cmr.update.menu.sparql" /></a></li>
    </sec:authorize>
    <sec:authorize access="hasRole('ROLE_UPDATE')">
        <li><a href="${pageContext.request.contextPath}/admin/cmr/update/job/last-modification-date"><fmt:message key="cmr.update.menu.lastmodifdate" /></a></li>
    </sec:authorize>
</ul>