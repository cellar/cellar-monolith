<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/cmr/sparql/js/sparqlResponseTemplates.js"></script>

<div class="title">Response templates configuration</div>
<div style="text-align:center;">
    <input id="addConfigurationButton" type="button" value="Add response template" data-url="${pageContext.request.contextPath}${functionalityUrl}/create" />
</div>
<display:table name="responseTemplates" id="responseTemplate"  pagesize="20" requestURI="">
	<display:column property="uriPattern" title="URI template pattern" />
	<display:column property="sparql" title="SPARQL query" />
	<display:column property="xslt" title="Xslt" />
	<display:column property="sequence" title="Sequence" />
	<display:column title="Actions">
		<a href=<c:url value="${functionalityUrl}/delete?id=${responseTemplate.id}"/>>Delete</a><br />
		<a href=<c:url value="${functionalityUrl}/edit?id=${responseTemplate.id}"/>>Edit</a>
	</display:column>
</display:table>

<div class="subtitle">Add a new stylesheet</div>
<form:form action="${pageContext.request.contextPath}/admin/cmr/sparql/templates/uploadXslt" modelAttribute="uploadXslt" method="post" enctype="multipart/form-data">
  <span class="text">Stylesheet:</span>
  <form:input path="fileData" type="file" accept="xsl/xslt"/>
  <input type="submit" value="Upload"/>
</form:form>