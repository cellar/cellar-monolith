<%@ page import="java.util.Map" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
  Map<String, String> printFiles = (Map<String, String>) request.getAttribute("list");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>Print</title>
  <style type="text/css">
    body {
      margin: 0;
      padding: 0;
      font: 12px Geneva, Verdana, Arial, sans-serif;
      color: #333333;
    }

    .print-item {
      margin: 10px;
    }

    .title {
      font-weight: bold;
      maring: 5px 0;
    }
  </style>
  <script type="text/javascript" src="${pageContext.request.contextPath}/resources/cmr/printSparql.js"></script>
</head>
<body>
<div class="print">
  <% for (String printFile : printFiles.keySet()) { %>
  <div class="print-item">
    <div class="title"><%=printFile%>
    </div>
    <div class="output"><%=printFiles.get(printFile)%>
    </div>
  </div>
  <% } %>
</div>
</body>
</html>