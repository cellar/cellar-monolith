<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/cmr/nal/css/model-load-requests.css" type="text/css"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/common/js/jquery-3.6.1.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/common/js/jquery-ui-1.13.2.custom.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/common/js/jquery.balloon.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/cmr/nal/js/model-load-requests.js"></script>

<div id="restartPopup" title="Restart model-load" style="display:none;">
   <form id="restart-popup-form" method="post" action="${base}/admin/cmr/nal/modelloadrequests/restart">
	   <fmt:message key="cmr.nal.table.column.activationDate" />
	   <input type="hidden" id="idSelected" name="idSelected"/>
	   <input type="text" class="datepicker" id="activationDate" name="activationDate" required="required"/>
	   <input id="restartButton" type="submit" value="<fmt:message key="cmr.nal.command.restart" />"/>
   </form>
</div>
    
<form id="model-load-form" method="post" action="${base}/admin/cmr/nal/modelloadrequests/search">
	<fieldset id="filterFieldset">
	    <div>
	        <fmt:message key="cmr.nal.table.filter.modelUri" />
	        <div id="modelUri_filter">
	            <!--there's a magnifying glass png on the input, the space is so that the placeholder is not on top of it  -->
	            <input type="text" name="modelUri" value="${modelUri}" id="modelUri_filter_input" placeholder="       Search..." autofocus>
	            <input type="reset" value="x" id="resetModelUri_filter">
	        </div>
	    </div>

	    <div>
	        <fmt:message key="cmr.nal.table.filter.submissionStatus" />
	        <div id="operationType_filter">
	           <select name="subStatus">
	               <option value=""></option>
	               <c:forEach var="submissionStatus" varStatus="status" items="${submissionStatuss}">
                       <c:if test="${submissionStatus eq subStatus}">
                           <option value="${submissionStatus}" selected="selected">${submissionStatus}</option>
                       </c:if>
                       <c:if test="${submissionStatus ne subStatus}">
                           <option value="${submissionStatus}">${submissionStatus}</option>
                       </c:if>
	               </c:forEach>
	           </select>
	        </div>
	    </div>
	    <div>
		    <input id="searchButton" type="submit" value="<fmt:message key="cmr.nal.command.search" />"/>
		</div>
	</fieldset>
	<br><br>
	<display:table name="modelLoadRequests" id="modelLoadRequest"  pagesize="20" defaultsort="1" defaultorder="ascending" requestURI="">
	    <display:column property="modelURI" titleKey="cmr.nal.table.column.modelUri" sortable="true" headerClass="sortable" />
	    <display:column titleKey="cmr.nal.table.column.activationDate" sortable="true" headerClass="sortable">
	       <fmt:formatDate value="${modelLoadRequest.activationDate}" pattern="dd/MM/yyyy HH:mm" />
	    </display:column>
	    <display:column property="externalPid" titleKey="cmr.nal.table.column.externalPid" sortable="true" headerClass="sortable" />
	    <display:column titleKey="cmr.nal.table.column.submissionStatus" sortable="true" headerClass="sortable">
	        <c:if test="${modelLoadRequest.executionStatus == 'Success' || modelLoadRequest.executionStatus == 'Failure'}">
	           <span title="${modelLoadRequest.executionStatus} on <fmt:formatDate value="${modelLoadRequest.executionDate}" pattern="dd/MM/yyyy HH:mm" />">${modelLoadRequest.executionStatus}</span>
            </c:if>
            <c:if test="${modelLoadRequest.executionStatus != 'Success' && modelLoadRequest.executionStatus != 'Failure'}">
                <span>${modelLoadRequest.executionStatus}</span>
            </c:if>
	    </display:column>
	    <display:column titleKey="cmr.nal.table.column.actions">
            <a style="padding-left:0px;" title="<fmt:message key="cmr.nal.command.update" />" href=<c:url value="/admin/cmr/nal/modelloadrequests/edit?id=${modelLoadRequest.id}"/> id="editLink"><img id="enabled" src="${base}/resources/common/admin/images/edit.png" /></a>
            <c:if test="${modelLoadRequest.executionStatus == 'Failure'}">
                <a id="restartLink" style="padding-left:0px;" title="<fmt:message key="cmr.nal.command.restart" />" href="#" data-id="${modelLoadRequest.id}" data-activation-date="<fmt:formatDate value="${modelLoadRequest.activationDate}" pattern="dd/MM/yyyy" />">
					<img id="enabled" src="${base}/resources/common/admin/images/load.png" />
				</a>
            </c:if>
            <a style="padding-left:0px;" title="<fmt:message key="cmr.nal.command.delete" />" href=<c:url value="/admin/cmr/nal/modelloadrequests/delete?id=${modelLoadRequest.id}"/> id="deleteLink"><img id="enabled" src="${base}/resources/common/admin/images/delete.png" /></a>
        </display:column>
	</display:table>
</form>