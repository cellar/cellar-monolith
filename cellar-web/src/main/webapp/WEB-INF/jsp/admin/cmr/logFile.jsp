<%@ page import="java.util.Map, java.util.HashMap, java.io.Reader, java.io.InputStream, java.io.InputStreamReader, eu.europa.ec.opoce.cellar.support.TokenReplacingReader" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
  final Map<String, Object> logFiles = (Map<String, Object>) request.getAttribute("logFiles");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>Log</title>
  <style type="text/css">
    body {
      margin: 0;
      padding: 0;
      font: 12px Geneva, Verdana, Arial, sans-serif;
      color: #333333;
    }

    .menu ul {
      border: 0;
      margin: 20px;
      padding: 5px;
      list-style-type: square;
    }

    a {
      text-decoration: none;
      color: #333333;
    }

    a:hover {
      text-decoration: underline;
    }

    .log {
      margin: 20px;
    }

    .log-item {
      margin: 10px 0;
    }

    .log-item a {
      font-weight: bold;
      display: block;
      margin: 10px 0;
    }

    .log-item a:hover {
      text-decoration: none;
    }
  </style>
</head>
<body>
<div class="menu">
  <ul>
    <% for (String log : logFiles.keySet()) { %>
    <li><a href="#<%=log%>"><%=log%>
    </a></li>
    <% } %>
  </ul>
</div>
<div class="log">
  <% for (String log : logFiles.keySet()) { %>
  <div class="log-item">
    <a name="<%=log%>"><%=log%>
    </a>

    <div class="output">
    <%
        final Map<String, String> replacers = new HashMap<String, String>();
        replacers.put("\n", "<br/>");
        replacers.put("\r\n", "<br/>");
        replacers.put("<", "&lt;");
        replacers.put(">", "&gt;");
        replacers.put("\t", "&nbsp;&nbsp;&nbsp;&nbsp;");
        replacers.put(" ERROR ", " <b style=\"color:red\">ERROR</b> ");
        final Reader source = new InputStreamReader((InputStream) logFiles.get(log));
        final Reader reader = new TokenReplacingReader(source, replacers);

        final int BUF_SIZE = 4096;
        char[] buf = new char[BUF_SIZE];
        int charRead = reader.read(buf);
        while (charRead != -1) {
            out.print(buf);
            buf = new char[BUF_SIZE];
            charRead = reader.read(buf);
        }

        source.close();
        reader.close();
    %>
    </div>
  </div>
  <% } %>
</div>
</body>
</html>