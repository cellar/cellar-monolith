<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/cmr/rdfStoreCleaner/js/dashboard.js"></script>

<div class="dashboard">
	<fieldset>
		<legend><fmt:message key="cmr.rdfStoreCleaner.dashboard.cleaningService" /></legend>
		
		<div>
			<fmt:message key="cmr.rdfStoreCleaner.dashboard.cleaningServiceStatus" /> <b><fmt:message key="cmr.rdfStoreCleaner.serviceStatus.${cleaningServiceStatus}" /></b>
			<c:choose>
				<c:when test="${cleaningServiceStatus == 'WORKING' }">
					<input id="stopCleaningButton" type="button" value="<fmt:message key="cmr.rdfStoreCleaner.dashboard.command.stopCleaning" />" data-url="${pageContext.request.contextPath}/admin/cmr/rdfStoreCleaner/stopCleaning" />
				</c:when>
				<c:when test="${cleaningServiceStatus == 'NOT_WORKING' }">
					<input id="startCleaningButton" type="button" value="<fmt:message key="cmr.rdfStoreCleaner.dashboard.command.startCleaning" />" data-url="${pageContext.request.contextPath}/admin/cmr/rdfStoreCleaner/startCleaning" />
				</c:when>
			</c:choose>
		</div>
		
		<div>
			<fmt:message key="cmr.rdfStoreCleaner.dashboard.cleaningServiceMode" /> <b><fmt:message key="cmr.rdfStoreCleaner.mode.${cellarConfiguration.cellarServiceRDFStoreCleanerMode}" /></b>
		</div>
		
		<div>
			<fmt:message key="cmr.rdfStoreCleaner.dashboard.numberMetadataResources" /> <b><span id="numberMetadataResources">-</span></b>
			<input id="numberMetadataResourcesButton" type="button" value="<fmt:message key="cmr.rdfStoreCleaner.dashboard.command.numberMetadataResources" />" />
		</div>
			
		<c:if test="${cellarConfiguration.cellarServiceRDFStoreCleanerMode == 'diagnostic' }">
			<div>
				<fmt:message key="cmr.rdfStoreCleaner.dashboard.numberMetadataResourcesDiagnosable" /> <b><span id="numberMetadataResourcesDiagnosable">-</span></b>
				<input id="numberMetadataResourcesDiagnosableButton" type="button" value="<fmt:message key="cmr.rdfStoreCleaner.dashboard.command.numberMetadataResourcesDiagnosable" />" />
			</div>
		</c:if>
		
		<c:if test="${cellarConfiguration.cellarServiceRDFStoreCleanerMode == 'fix' }">
			<div>
				<fmt:message key="cmr.rdfStoreCleaner.dashboard.numberMetadataResourcesFixable" /> <b><span id="numberMetadataResourcesFixable">-</span></b>
				<input id="numberMetadataResourcesFixableButton" type="button" value="<fmt:message key="cmr.rdfStoreCleaner.dashboard.command.numberMetadataResourcesFixable" />" />
			</div>
		</c:if>
		
		<div>
			<fmt:message key="cmr.rdfStoreCleaner.dashboard.numberMetadataResourcesError" /> <b><span id="numberMetadataResourcesError">-</span></b>
			<input id="numberMetadataResourcesErrorButton" type="button" value="<fmt:message key="cmr.rdfStoreCleaner.dashboard.command.numberMetadataResourcesError" />" />
		</div>
		
		<div>
			<fmt:message key="cmr.rdfStoreCleaner.dashboard.numberRowsCleanerDifference" /> <b><span id="numberRowsCleanerDifference">-</span></b>
			<input id="numberRowsCleanerDifferenceButton" type="button" value="<fmt:message key="cmr.rdfStoreCleaner.dashboard.command.numberRowsCleanerDifference" />" />
		</div>
	</fieldset>
</div>