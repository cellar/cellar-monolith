<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/common/js/jquery.cookie.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/common/js/jquery.datatables.1.13.1.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/cmr/indexing/js/indexing.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/cmr/indexing/css/dashboard.css" type="text/css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/common/css/jquery.dataTables.min.css" type="text/css"/>


<div class="dashboard">
    <fieldset>
        <legend><fmt:message key="cmr.indexing.dashboard.indexingService"/></legend>
        <div>
            <fmt:message key="cmr.indexing.dashboard.indexingQueue"/> <b>${indexingQueue}</b>
        </div>
        <div>
            <fmt:message key="cmr.indexing.dashboard.failedIndexingRequests"/> <b>${failedIndexingRequests}</b>
            <c:if test="${failedIndexingRequests > 0}">
                <input id="restartIndexing" type="button"
                       value="<fmt:message key="cmr.indexing.dashboard.command.restart.failed" />"
                       data-url="${pageContext.request.contextPath}/admin/cmr/indexing/restart/failed"/>
            </c:if>
        </div>
        <div>
            <fmt:message key="cmr.indexing.dashboard.indexingEnabled"/>
            <b><fmt:message key="cmr.indexing.dashboard.indexingEnabled.${indexingEnabled}"/></b>
        </div>
        <div>
            <fmt:message key="cmr.indexing.dashboard.indexingServiceStatus"/>
            <b><fmt:message key="cmr.indexing.serviceStatus.${indexingServiceStatus}"/></b>
            <c:if test="${indexingEnabled}">
                <c:if test="${indexingServiceStatus == 'INDEXING' }">
                    <input id="stopIndexing" type="button"
                           value="<fmt:message key="cmr.indexing.dashboard.command.stopIndexing" />"
                           data-url="${pageContext.request.contextPath}/admin/cmr/indexing/configuration/stopIndexing"/>
                </c:if>
            </c:if>
        </div>

        </br>

        <div class="existing_requests_table_space">
            <div class="title"><spring:message code="cmr.indexing.dashboard.existing.requests.table.title"/></div>
            <table class="existing_indexation_requests_datatable">
                <thead>
                <tr>
                    <th>
                        <spring:message code="cmr.indexing.dashboard.existing.requests.table.column.title.priority"/>
                    </th>
                    <th>
                        <spring:message code="cmr.indexing.dashboard.existing.requests.table.column.title.groupuri"/>
                    </th>
                    <th>
                        <spring:message code="cmr.indexing.dashboard.existing.requests.table.column.title.createdAt"/>
                    </th>
                    <th>
                        <spring:message code="cmr.indexing.dashboard.processed.requests.table.column.title.doneAt"/>
                    </th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="indexationRequest" varStatus="status" items="${newIndexationRequests}">
                    <tr class="single-nal">
                        <td>${indexationRequest.priority}</td>
                        <td>${indexationRequest.groupURI}</td>
                        <td>
                            <fmt:formatDate value="${indexationRequest.createdAt}" pattern="yyyy-MM-dd HH:mm:ss"/>
                        </td>
                        <td>
                            <fmt:formatDate value="${indexationRequest.doneAt}" pattern="yyyy-MM-dd HH:mm:ss"/>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

        <div class="processing_requests_table_space">
            <div class="title">
                <spring:message code="cmr.indexing.dashboard.processed.requests.table.title" arguments="60"/>
            </div>
            <table class="processed_indexation_requests_datatable">
                <thead>
                <tr>
                    <th>
                        <spring:message code="cmr.indexing.dashboard.processed.requests.table.column.title.priority"/>
                    </th>
                    <th>
                        <spring:message code="cmr.indexing.dashboard.processed.requests.table.column.title.groupuri"/>
                    </th>
                    <th>
                        <spring:message code="cmr.indexing.dashboard.processed.requests.table.column.title.createdAt"/>
                    </th>
                    <th>
                        <spring:message code="cmr.indexing.dashboard.processed.requests.table.column.title.doneAt"/>
                    </th>
                </tr>
                </thead>
                <tbody>
                <c:forEach var="indexationRequest" varStatus="status" items="${indexationRequestsProcessedHistory}">
                    <tr class="single-nal">
                        <td>${indexationRequest.priority}</td>
                        <td>${indexationRequest.groupURI}</td>
                        <td>
                            <fmt:formatDate value="${indexationRequest.createdAt}" pattern="yyyy-MM-dd HH:mm:ss"/>
                        </td>
                        <td>
                            <fmt:formatDate value="${indexationRequest.doneAt}" pattern="yyyy-MM-dd HH:mm:ss"/>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </fieldset>
</div>