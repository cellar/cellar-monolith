<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%
  Map<String, String> stylesheets = (Map<String, String>) request.getAttribute("stylesheets");
  List<String> definitions = (List<String>) request.getAttribute("definitions");
%>
<div class="wrapper">
  <div class="title">Sparql configuration</div>
  <div class="xslt-container">
    <div class="xslt-status"></div>
    <span class="text">Xslt configuration:</span>

    <div class="float-wrapper">
      <select class="xslt-select" name="xslt" id="listXslts" multiple size=15>
        <%
          for (String stylesheet : stylesheets.keySet()) {
            String mimetype = stylesheets.get(stylesheet);
        %>
        <option value="<%=stylesheet%>"><%=stylesheet%> (<%=mimetype%>)</option>
        <%
          }
        %>
      </select>
      <button name="deleteStylesheet" id="deleteStylesheet">Remove</button>
      <button name="printStylesheet" id="printStylesheet">Print</button>
    </div>
    <div class="subtitle">Add a new stylesheet</div>
    <form:form action="${pageContext.request.contextPath}/admin/cmr/sparql/configuration/uploadXslt" modelAttribute="uploadXslt" method="post" enctype="multipart/form-data">
      <span class="text">Mimetype</span>
      <form:input path="mimetype"/><br/>
      <span class="text">Stylesheet:</span>
      <form:input path="fileData" type="file" accept="xsl/xslt"/>
      <input type="submit" value="Upload"/>
    </form:form>
    <!--<input type="file" name="xslt-upload" class="upload" size="43"/>-->
    <!--<button name="addStylesheet" id="addStylesheet">Add</button>-->
  </div>
  <div class="definition-container">
    <div class="definition-status"></div>
    <span class="text">Sparql configuration:</span>

    <div class="float-wrapper">
      <select class="definition-select" name="definition" id="listDefinitions" multiple size=15>
        <%
          for (String definition : definitions) {
        %>
        <option value="<%=definition%>"><%=definition%>
        </option>
        <%
          }
        %>
      </select>
      <button name="deleteDefinition" id="deleteDefinition">Remove</button>
      <button name="printDefinition" id="printDefinition">Print</button>
    </div>
    <div class="subtitle">Add a new sparql query:</div>
    <form:form action="${pageContext.request.contextPath}/admin/cmr/sparql/configuration/uploadDefinition" modelAttribute="uploadDefinition" method="post" enctype="multipart/form-data">
      <span class="text">Sparql query:</span>
      <form:input path="fileData" type="file"/>
      <input type="submit" value="Upload"/>
    </form:form>
    <!--<input type="file" name="definition-upload" class="upload" size="43"/>-->
    <!--<button name="addDefinition" id="addDefinition">Add</button>-->
  </div>
  <div class="config-container">
    <span class="subtitle">Sparql configuration file:</span>
    <span class="explanation">
      <br/>* Shut down the sparql endpoint server.<br/>
      * Download the configuration file <a href="${pageContext.request.contextPath}/admin/cmr/sparql/configuration/getConfigFile" target="_blank">here</a>.<br/>
      * Upload and overwrite the configuration file on the server, with the downloaded configuration file. The configuration file on the server is located in the WEB-INF subfolder of the joseki webapplication.<br/>
      * Start the sparql endpoint server.<br/>
    </span>
  </div>
</div>