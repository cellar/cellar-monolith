<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/cmr/batch-job/js/create-job.js"></script>

<form:form method="POST" modelAttribute="job">
	<form:hidden path="id" />
	<table class="items">
    		<tr>
    			<td>
    				<fmt:message key="cmr.delete-job.create.jobName" />
    			</td>
    			<td>
    				<form:input id="jobName" path="jobName" maxlength="255" />
    				<form:errors path="jobName" cssClass="error" />
    			</td>
    		</tr>
    		<tr>
    			<td>
    				<fmt:message key="cmr.delete-job.create.sparqlQuery"/>
    			</td>
    			<td>
    	        	<div>
    					<fmt:message key="cmr.delete-job.create.sparqlQuery.helper" />
    	        	</div>
    	        	<div class="prefixes"><br />
    	        		<b><fmt:message key="cmr.delete-job.create.sparqlQuery.usedPrefixes" /></b>
    				    <ul>
    				    	<li>oracle: &lt;http://www.oracle.com/2009/05/orardf/jena-joseki-ns#&gt;</li>
    				      	<li>rdfs: &lt;http://www.w3.org/2000/01/rdf-schema#&gt;</li>
    				      	<li>rdf: &lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#&gt;</li>
    				      	<li>xsd: &lt;http://www.w3.org/2001/XMLSchema#&gt;</li>
    				      	<li>module: &lt;http://joseki.org/2003/06/module#&gt;</li>
    				      	<li>joseki: &lt;http://joseki.org/2005/06/configuration#&gt;</li>
    				      	<li>ja: &lt;http://jena.hpl.hp.com/2005/11/Assembler#&gt;</li>
    				      	<li>cdm: &lt;http://publications.europa.eu/ontology/cdm#&gt;</li>
    				      	<li>owl: &lt;http://www.w3.org/2002/07/owl#&gt;</li>
    				      	<li>cmr: &lt;http://publications.europa.eu/ontology/cdm/cmr#&gt;</li>
    				    </ul>
    			    </div>
    	        	<div class="exampleQueryWrapper">
    	        		<span class="text"><fmt:message key="cmr.delete-job.create.sparqlQuery.example" /></span> <select name="exampleQuery"></select>
    	        	</div>
    				<form:textarea id="sparqlQuery" path="sparqlQuery" cols="100%" rows="4" />
    				<form:errors path="sparqlQuery" cssClass="error" />
    			</td>
    		</tr>
    	</table>
    <div style="text-align:center;">
   		<input id="submitButton" type="submit" value="<fmt:message key="cmr.batch-job.edit.command.edit" />"/>
		<input id="cancelButton" type="button" value="<fmt:message key="title.command.cancel" />" data-url="${pageContext.request.contextPath}${functionalityUrl}" />
	</div>
</form:form>
