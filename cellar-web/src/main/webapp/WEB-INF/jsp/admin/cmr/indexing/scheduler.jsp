<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="app" tagdir="/WEB-INF/tags"%>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/cmr/indexing/js/scheduler.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/cmr/indexing/css/scheduler.css" type="text/css"/>

<h1><spring:message code="cmr.indexing.scheduler.form.title"/></h1>
<form:form method="POST" id="indexationSchedulerForm" modelAttribute="indexationSchedulerForm" >


 	<fieldset class="fields">
 		<div class="choice">
 			<form:radiobutton path="selectionChoice" value="SPARQL" id="SPARQL_choice"/>
	 		<fmt:message key="cmr.indexing.scheduler.form.sparql" />
	    	<form:errors path="useSparql" cssClass="error" />
    	</div>
    	<div class="choice_details">
   			<div>
				<fmt:message key="cmr.indexing.scheduler.form.sparqlQuery.helper" />
        	</div>
        	<div class="prefixes"><br />
        		<b><fmt:message key="cmr.indexing.scheduler.form.sparqlQuery.usedPrefixes" /></b>
			    <ul>
			    	<li>oracle: &lt;http://www.oracle.com/2009/05/orardf/jena-joseki-ns#&gt;</li>
			      	<li>rdfs: &lt;http://www.w3.org/2000/01/rdf-schema#&gt;</li>
			      	<li>rdf: &lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#&gt;</li>
			      	<li>xsd: &lt;http://www.w3.org/2001/XMLSchema#&gt;</li>
			      	<li>module: &lt;http://joseki.org/2003/06/module#&gt;</li>
			      	<li>joseki: &lt;http://joseki.org/2005/06/configuration#&gt;</li>
			      	<li>ja: &lt;http://jena.hpl.hp.com/2005/11/Assembler#&gt;</li>
			      	<li>cdm: &lt;http://publications.europa.eu/ontology/cdm#&gt;</li>
			      	<li>owl: &lt;http://www.w3.org/2002/07/owl#&gt;</li>
			      	<li>cmr: &lt;http://publications.europa.eu/ontology/cdm/cmr#&gt;</li>
			    </ul>
		    </div>
        	<div class="exampleQueryWrapper">
        		<span class="text"><fmt:message key="cmr.indexing.scheduler.form.sparqlQuery.example" /></span> <select name="exampleQuery"></select>
        	</div>
			<form:textarea id="sparqlQuery" path="sparqlQuery" cols="100%" rows="4" />
			<form:errors path="sparqlQuery" cssClass="error" />
		</div>   
    </fieldset>
    
 	<fieldset class="fields">
 		<div class="choice">
 			<form:radiobutton path="selectionChoice" value="ALL" id="ALL_choice"/>
	 		<fmt:message key="cmr.indexing.scheduler.form.allContent" />
	    	<form:errors path="chooseAllContent" cssClass="error" />
    	</div>
    </fieldset>
    
    
 	<fieldset class="fields">
 		<div class="choice">
			<form:radiobutton path="selectionChoice" value="PARTIAL" id="PARTIAL_choice" />
	 		<fmt:message key="cmr.indexing.scheduler.form.allContentForPeriod" />
	    	<form:errors path="chooseAllContentForPeriod" cssClass="error" />
    	</div>
    	<div class="choice_details">
    		<form:input path="period"/>
			<form:select id="period_unitOfTime" path="periodUnitOfTime" >
				<form:option value="d"><spring:message code="cmr.indexing.scheduler.period.d"/></form:option>
				<form:option value="w"><spring:message code="cmr.indexing.scheduler.period.w"/></form:option>
				<form:option value="m"><spring:message code="cmr.indexing.scheduler.period.m"/></form:option>
			</form:select>			
		</div>   
		<form:errors path="period" cssClass="error" />
    </fieldset>
    <div class="option_fields">
	    <fieldset class="fields">
	    	<div class="option">
		    	<fmt:message key="cmr.indexing.scheduler.form.cronExpression" />
		    	<form:input path="cronExpression"/>
		    	<form:errors path="cronExpression" cssClass="error" />
	    	</div>
	    </fieldset>
	    
	    <fieldset class="fields">
	    	<div class="option">
		    	<fmt:message key="cmr.indexing.scheduler.form.duration" />
		    	<form:input path="duration"/>
				<form:select id="duration_unitOfTime" path="durationUnitOfTime" >
					<form:option value="m"><spring:message code="cmr.indexing.scheduler.duration.m"/></form:option>
					<form:option value="h"><spring:message code="cmr.indexing.scheduler.duration.h"/></form:option>
				</form:select>
				<form:errors path="duration" cssClass="error" />
			</div>
	    </fieldset>
	    
	    <fieldset class="fields">
	    	<div class="option">
		    	<fmt:message key="cmr.indexing.scheduler.form.enabled" />
		    	<form:checkbox path="enabled"/>
	    	</div>
	    </fieldset>
	    
	    <fieldset class="fields">
	    	<div class="option">
	   	 		<input id="submitButton" type="submit" value="<fmt:message key="cmr.indexing.scheduler.form.save" />"/>
			</div>	   	 		
	    </fieldset>
    </div>    
    
    <table id="nextExecutionsTable">
	    <thead>
	    	<tr>
	    		<th>
	    			<spring:message code="cmr.indexing.scheduler.form.table.title"/>
    			</th>
   			</tr>
	    </thead>
	    <tbody>
	    	<c:forEach var="nextExecution" varStatus="status" items="${nextExecutions}">
		    	<tr>
			    	<td>${nextExecution}</td>
		    	</tr>
	    	</c:forEach>
	    </tbody>
    </table>
</form:form>


