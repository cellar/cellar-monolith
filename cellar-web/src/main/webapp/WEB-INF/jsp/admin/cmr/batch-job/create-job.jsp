<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/cmr/batch-job/js/create-job.js"></script>

<form:form method="POST" modelAttribute="job">
	<table class="items">
		<tr>
			<td>
				<fmt:message key="cmr.batch-job.create.jobName" />
			</td>
			<td>
				<form:input id="jobName" path="jobName" maxlength="255" />
				<form:errors path="jobName" cssClass="error" />
			</td>
		</tr>
		<tr>
			<td>
				<fmt:message key="cmr.batch-job.create.sparqlQuery"/>
			</td>
			<td>
	        	<div>
					<fmt:message key="cmr.batch-job.create.sparqlQuery.helper" />
	        	</div>
	        	<div class="prefixes"><br />
	        		<b><fmt:message key="cmr.batch-job.create.sparqlQuery.usedPrefixes" /></b>
				    <ul>
				    	<li>oracle: &lt;http://www.oracle.com/2009/05/orardf/jena-joseki-ns#&gt;</li>
				      	<li>rdfs: &lt;http://www.w3.org/2000/01/rdf-schema#&gt;</li>
				      	<li>rdf: &lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#&gt;</li>
				      	<li>xsd: &lt;http://www.w3.org/2001/XMLSchema#&gt;</li>
				      	<li>module: &lt;http://joseki.org/2003/06/module#&gt;</li>
				      	<li>joseki: &lt;http://joseki.org/2005/06/configuration#&gt;</li>
				      	<li>ja: &lt;http://jena.hpl.hp.com/2005/11/Assembler#&gt;</li>
				      	<li>cdm: &lt;http://publications.europa.eu/ontology/cdm#&gt;</li>
				      	<li>owl: &lt;http://www.w3.org/2002/07/owl#&gt;</li>
				      	<li>cmr: &lt;http://publications.europa.eu/ontology/cdm/cmr#&gt;</li>
				    </ul>
			    </div>
	        	<div class="exampleQueryWrapper">
	        		<span class="text"><fmt:message key="cmr.batch-job.create.sparqlQuery.example" /></span> <select name="exampleQuery"></select>
	        	</div>
				<form:textarea id="sparqlQuery" path="sparqlQuery" cols="100%" rows="4" />
				<form:errors path="sparqlQuery" cssClass="error" />
			</td>
		</tr>
		
		
		
		
		<c:choose>
	  		<c:when test="${batchJobType == 'REINDEX'}">
				<tr>
					<td>
						<fmt:message key="cmr.batch-job.reindex.create.priority" />
					</td>
					<td>
						<form:select id="priorityId" path="priorityId" >
						    <c:forEach var="priority" items="${prioritiesList}">
						        <form:option value="${priority.priorityValue}"><spring:message code="cmr.indexing.indexRequest.priority.${priority.name}"/> (${priority.priorityValue})</form:option>
						    </c:forEach>
						</form:select>
					</td>
				</tr>
				<tr>
					<td>
						<fmt:message key="cmr.batch-job.reindex.create.generateIndexNotice" />
					</td>
					<td>
						<form:checkbox path="generateIndexNotice" />
						<form:errors path="generateIndexNotice" cssClass="error" />
					</td>
				</tr>
				<tr>
					<td>
						<fmt:message key="cmr.batch-job.reindex.create.generateEmbeddedNotice" />
					</td>
					<td>
						<form:checkbox path="generateEmbeddedNotice" />
						<form:errors path="generateEmbeddedNotice" cssClass="error" />
					</td>
				</tr>
	  		</c:when>
	  		<c:when test="${batchJobType == 'EMBARGO'}">
				<tr>
					<td>
						<fmt:message key="cmr.batch-job.embargo.create.embargoDate" />
					</td>
					<td>						
						<form:input id="embargoDate" path="embargoDate" cssClass="datepicker" />
						<form:errors path="embargoDate" cssClass="error" />
					</td>
				</tr>
	  		</c:when>
	  		<c:when test="${batchJobType == 'EXPORT'}">
				<tr>
					<td>
						<fmt:message key="cmr.batch-job.export.create.metadataOnly" />
					</td>
					<td>						
						<form:checkbox id="metadataOnly" path="metadataOnly"/>
						<form:errors path="metadataOnly" cssClass="error" />
					</td>
				</tr>
				<tr>
					<td>
						<fmt:message key="cmr.batch-job.export.create.useAgnosticURL" />
					</td>
					<td>						
						<form:checkbox id="useAgnosticURL" path="useAgnosticURL"/>
						<form:errors path="useAgnosticURL" cssClass="error" />
					</td>
				</tr>
				<tr>
					<td>
						<fmt:message key="cmr.batch-job.export.create.destinationFolder" />
					</td>
					<td>
						<form:input id="destinationFolder" path="destinationFolder" maxlength="255" />
						<img id="destinationFolderHelpImg" class="tooltipHelper" src="${base}/resources/common/admin/images/help-icon.jpg" style="vertical-align:middle" title="<fmt:message key="cmr.batch-job.export.create.destinationFolder.help"><fmt:param value="${baseDestinationFolder}"/><fmt:param value="${defaultFolderNamePattern}"/></fmt:message>"/>
					</td>
				</tr>
	  		</c:when>
		</c:choose>

	</table>
    <div style="text-align:center;">
   		<input id="submitButton" type="submit" value="<fmt:message key="cmr.batch-job.create.command.create" />"/>
		<input id="cancelButton" type="button" value="<fmt:message key="title.command.cancel" />" data-url="${pageContext.request.contextPath}${functionalityUrl}" />
	</div>
</form:form>
