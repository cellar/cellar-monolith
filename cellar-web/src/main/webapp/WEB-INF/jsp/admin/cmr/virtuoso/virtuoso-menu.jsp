<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<ul>

    <sec:authorize access="hasRole('ROLE_VIRTUOSO')">
        <li><a href="${pageContext.request.contextPath}/admin/cmr/virtuoso/sparqlloadrequests"><fmt:message key="cmr.virtuoso.menu.sparqlLoadRequests" /></a></li>
    </sec:authorize>
    <sec:authorize access="hasAnyRole('ROLE_SUPER_ADMIN', 'ROLE_MANAGE_VIRTUOSO_DATA')">
        		<li>
        		<a href="${pageContext.request.contextPath}/admin/cmr/delete/job"><fmt:message key="cmr.virtuoso.menu.delete-job" />
        		</li>
    </sec:authorize>

</ul>
