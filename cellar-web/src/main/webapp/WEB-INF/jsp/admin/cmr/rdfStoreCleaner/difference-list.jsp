<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<form id="filterForm" method="GET" action="${pageContext.request.contextPath}/admin/cmr/rdfStoreCleaner/list">
	<fieldset>
		<span id="cellarIdLabel"><fmt:message key="cmr.rdfStoreCleaner.difference-list.filter.cellarId" /> </span>
		<input id="cellarId" name="cellarId" type="text" size="25" value="${param.cellarId}"/>
		
		<span id="operationTypeLabel"><fmt:message key="cmr.rdfStoreCleaner.difference-list.filter.operationType" /> </span>
		<select id="operationType" name="operationType">
			<option />
			<c:forEach var="operationType" items="${operationTypes}">
				<option value="${operationType}" <c:if test="${operationType == param.operationType }">selected="selected"</c:if> >
					<spring:message code="cmr.rdfStoreCleaner.mode.${operationType}"/>
				</option>
			</c:forEach>
		</select>
		
		<span id="queryTypeLabel"><fmt:message key="cmr.rdfStoreCleaner.difference-list.filter.queryType" /> </span>
		<select id="queryType" name="queryType">
			<option />
			<c:forEach var="queryType" items="${queryTypes}">
				<option value="${queryType}" <c:if test="${queryType == param.queryType }">selected="selected"</c:if> >
					<spring:message code="cmr.rdfStoreCleaner.queryType.${queryType}"/>
				</option>
			</c:forEach>
		</select>
		
		<span id="cmrTableLabel"><fmt:message key="cmr.rdfStoreCleaner.difference-list.filter.cmrTable" /> </span>
		<select id="cmrTable" name="cmrTable">
			<option />
			<c:forEach var="cmrTable" items="${cmrTables}">
				<option value="${cmrTable}" <c:if test="${cmrTable == param.cmrTable }">selected="selected"</c:if> >
					<spring:message code="cmr.rdfStoreCleaner.cmrTable.${cmrTable}"/>
				</option>
			</c:forEach>
		</select>
		
		<input id="filterButton" type="submit" value="<fmt:message key="common.command.ok" />" />
	</fieldset>
</form>
<br/>
<display:table name="differences" id="difference"  pagesize="30" defaultsort="1" defaultorder="ascending" requestURI="">
	<display:column property="cellarId" titleKey="cmr.rdfStoreCleaner.difference-list.cellarId" sortable="true" headerClass="sortable" />
	
	<display:column sortProperty="operationDate" titleKey="cmr.rdfStoreCleaner.difference-list.operationDate" sortable="true" headerClass="sortable">
		<fmt:formatDate value="${difference.operationDate}" pattern="dd/MM/yyyy HH:mm" />
	</display:column>
	
	<display:column sortProperty="operationType" titleKey="cmr.rdfStoreCleaner.difference-list.operationType" sortable="true" headerClass="sortable">
		<fmt:message key="cmr.rdfStoreCleaner.mode.${difference.operationType}" />
	</display:column>
	
	<display:column sortProperty="queryType" titleKey="cmr.rdfStoreCleaner.difference-list.queryType" sortable="true" headerClass="sortable">
		<fmt:message key="cmr.rdfStoreCleaner.queryType.${difference.queryType}" />
	</display:column>
	
	<display:column sortProperty="cmrTable" titleKey="cmr.rdfStoreCleaner.difference-list.cmrTable" sortable="true" headerClass="sortable">
		<fmt:message key="cmr.rdfStoreCleaner.cmrTable.${difference.cmrTable}" />
	</display:column>
	
	<display:column titleKey="cmr.rdfStoreCleaner.difference-list.difference" style="text-align:center">
		<a style="padding-left:0px;" title="<fmt:message key="cmr.rdfStoreCleaner.difference-list.difference" />" target="_blank" href=<c:url value="/admin/cmr/rdfStoreCleaner/list/difference?id=${difference.id}"/> id="differenceLink"><img id="enabled" src="${base}/resources/common/admin/images/search.png" /></a>
	</display:column>
</display:table>