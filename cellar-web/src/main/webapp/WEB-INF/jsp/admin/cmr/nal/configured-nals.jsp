<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%-- <%@ taglib uri="http://displaytag.sf.net" prefix="display" %> --%>


<script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/common/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/common/css/jquery.dataTables.min.css"
      type="text/css"/>


<script type="text/javascript" src="${pageContext.request.contextPath}/resources/cmr/nal/js/nal.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/cmr/nal/css/nal.css" type="text/css"/>

<form id="nal-delete-form" method="post" action="${base}/admin/cmr/nal/delete" name="nal-delete-form">
    <input id="deleteNalName" name="name" hidden="true" value=""/>
</form>

<div class="nal-wrapper">
    <div class="show-nals">


        <fieldset id="filterFieldset">
            <div>
                <fmt:message key="cmr.nal.table.filter.name"/>
                <div id="name_filter">
                    <input type="text" required id="name_filter_input" placeholder="       Search by name ..." autofocus >
                    <input type="reset" value="x" id="resetName_filter">
                </div>
            </div>
            <div>
                <fmt:message key="cmr.nal.table.filter.uri"/>
                <div id="uri_filter">
                    <input type="text" required id="uri_filter_input" placeholder="       Search by URI ...">
                    <input type="reset" value="x" id="resetUri_filter">
                </div>
            </div>
        </fieldset>

        </br></br>


        <table id=nalversions_table>
            <thead>
            <tr>
                <th id="name_header">name</th>
                <th id="uri_header">uri</th>
                <th id="information_header">information</th>
                <th id="activationDate_header">activationDate</th>
                <th id="delete_header"></th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${versions}" var="version">
                <tr>
                    <td><c:out value="${version.name}"/></td>
                    <td><c:out value="${version.uri}"/></td>
                    <td><c:out value="${version.versionNumber}"/></td>
                    <td><c:out value="${version.formattedVersionDate}"/></td>
                    <td >
                        <input class="nal_${version.name}" type="button" value="<fmt:message key="cmr.nal.command.delete" />"/>
                    </td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
    </div>

</div>