<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%-- <%@ taglib uri="http://displaytag.sf.net" prefix="display" %> --%>


<script type="text/javascript" src="${pageContext.request.contextPath}/resources/common/js/jquery.datatables.1.13.1.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/common/css/jquery.dataTables.min.css" type="text/css"/>


<script type="text/javascript" src="${pageContext.request.contextPath}/resources/cmr/nal/js/delete-report.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/cmr/nal/css/delete-report.css" type="text/css" />

<div class="report-wrapper">

	<c:if test="${not empty operationResults}">
		<table id="errorTable">
			<thead>
				<tr>
					<th>Operation</th>
					<th>Result</th>
					<th>Details</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="operationResult" varStatus="status" items="${operationResults}">
					<tr>
						<td>${operationResult.one}</td>
						<td>${operationResult.two}</td>
						<td>${operationResult.three}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		
		
		<i id="nalDeletionErrorInfo">
			<fmt:message key="cmr.nal.delete.error.details" >
				<fmt:param value="${nalName}" />
			</fmt:message>
		</i>
		
		
	</c:if>
	
	
	
	
	
</div>