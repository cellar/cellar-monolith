<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="title">Sparql Endpoint</div>
<div class="wrapper">
    <div class="prefixes">
        <ul>Used prefixes:
            <li>oracle: &lt;http://www.oracle.com/2009/05/orardf/jena-joseki-ns#&gt;</li>
            <li>rdfs: &lt;http://www.w3.org/2000/01/rdf-schema#&gt;</li>
            <li>rdf: &lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#&gt;</li>
            <li>xsd: &lt;http://www.w3.org/2001/XMLSchema#&gt;</li>
            <li>module: &lt;http://joseki.org/2003/06/module#&gt;</li>
            <li>joseki: &lt;http://joseki.org/2005/06/configuration#&gt;</li>
            <li>ja: &lt;http://jena.hpl.hp.com/2005/11/Assembler#&gt;</li>
            <li>cdm: &lt;http://publications.europa.eu/ontology/cdm#&gt;</li>
            <li>owl: &lt;http://www.w3.org/2002/07/owl#&gt;</li>
            <li>cmr: &lt;http://publications.europa.eu/ontology/cdm/cmr#&gt;</li>
        </ul>
    </div>
    <div class="example-query-wrapper">
        <span class="text">Example query:</span>
        <select name="example-query"></select>
    </div>
    <div class="text">Select / Ask</div>
    <form id="form" action="<c:out value="${sparqlServiceUrl}"/>" method="GET" target="_blank">
        <div class="query-wrapper">
            <textarea id="query-field" name="query" autofocus >SELECT ?x ?y ?z WHERE { ?x ?y ?z }</textarea>
            <input type="hidden" name="output" value=""/>
            <!--<input id="submit" type="submit" value="Submit Query"/>-->
            <button id="btnSubmitQuery">Submit Query</button>
        </div>
        <div class="options-container">
            <div class="option-json">
                <span class="text">JSON output:</span>
                <input type="checkbox" name="json" value="json"/>
            </div>
            <div class="option-stylesheet">
                <span class="text">XSLT style sheet :</span>
                <select name="stylesheet"><c:out value="${stylesheets}" escapeXml="false"/></select>
            </div>
        </div>
    </form>
    <div>
        <b>Virtuoso Server version : </b><c:out value="${virtuosoInformation.serverVersion}"/><br/>
        <b>Virtuoso Driver version : </b><c:out value="${virtuosoInformation.driverVersion}"/>
    </div>

</div>