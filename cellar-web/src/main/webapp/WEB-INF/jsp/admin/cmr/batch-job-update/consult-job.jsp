<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/cmr/batch-job-update/js/consult-job.js"></script>
<table class="items">
	<tr>
 		<td><fmt:message key="cmr.batch-job.consult.jobName" /></td>
     	<td colspan="2"><input id="jobName" type="text" class="input" name="jobName" size="100%" readonly="readonly" value="${batchJob.jobName}"/></td>
	</tr>
  	<tr>
      	<td><fmt:message key="cmr.batch-job.consult.sparqlQuery" /></td>
      	<td>
      		<textarea id="sparqlQuery" name="sparqlQuery" cols="100%" rows=4 readonly="readonly">${batchJob.sparqlQuery}</textarea>
      	</td>
  	</tr>
  	<tr>
      	<td><fmt:message key="cmr.batch-job.consult.sparqlUpdateQuery" /></td>
      	<td>
      		<textarea id="sparqlUpdateQuery" name="sparqlUpdateQuery" cols="100%" rows=4 readonly="readonly">${batchJob.sparqlUpdateQuery}</textarea>
      	</td>
  	</tr>
  	<tr>
        <td><fmt:message key="cmr.batch-job.consult.cron" /></td>
        <td colspan="2"><input id="cron" type="text" class="input" name="cron" size="100%" readonly="readonly" value="${batchJob.cron}"/></td>
    </tr>
  	<tr>
      	<td><fmt:message key="cmr.batch-job.consult.status" /></td>
      	<td>
      		<input id="jobStatus" type="text" class="input" name="jobStatus" size="20%" readonly="readonly" value="<spring:message code="cmr.batch-job.job.status.${batchJob.jobStatus}"/>"/>
      		<br />
      		
			<c:if test="${batchJob.errorDescription != null}">
	     		<a href="#" id="dialogOpener">${batchJob.errorDescription}</a>
			  	<div id="dialog" title="<fmt:message key="cmr.batch-job.consult.error" />">
			   		<p>${batchJob.errorStacktrace}</p>
				</div>
			</c:if>
      	</td>
  	</tr>
  	<tr>
  		<td><fmt:message key="cmr.batch-job.consult.creationDate" /></td>
		<td colspan="2"><input id="creationDate" type="text" class="input" name="creationDate" size="100%" readonly="readonly" value="<fmt:formatDate value="${batchJob.creationDate}" pattern="dd/MM/yyyy HH:mm" />"/></td>
  	</tr>
  	<tr>
  		<td><fmt:message key="cmr.batch-job.consult.lastModified" /></td>
  		<td colspan="2"><input id="lastModified" type="text" class="input" name="lastModified" size="100%" readonly="readonly" value="<fmt:formatDate value="${batchJob.lastModified}" pattern="dd/MM/yyyy HH:mm" />"/></td>
  	</tr>
  	<tr>
      	<td><fmt:message key="cmr.batch-job.consult.works" /></td>
      	<td>
			<display:table name="workIdentifiersList" sort="external" partialList="true" size="${workIdentifiersList.fullListSize}" pagesize="${workIdentifiersList.objectsPerPage}" requestURI="">
				<display:column property="workId" title="Work URI" sortable="true" sortName="workId" headerClass="sortable" />
			</display:table>
			<form id="filterForm" method="GET" action="${pageContext.request.contextPath}${functionalityUrl}/consult">
				<fieldset>
					<span id="filterLabel"><fmt:message key="cmr.batch-job.consult.works.filter" /> </span>
	            	<input id="jobId" name="id" type="hidden" value="${batchJob.id}" />
					<input id="workFilter" name="workFilter" class="tooltipHelper" title="<fmt:message key="cmr.batch-job.consult.works.filter.helper" />" type="text" size="25" value="${param.workFilter}"/>
					<input id="filterButton" type="submit" value="<fmt:message key="common.command.ok" />" />
				</fieldset>
			</form>
		</td>
  	</tr>
  	<tr>
      	<td colspan="2">
      		<div style="text-align:center;">
				<input id="backButton" type="button" value="<fmt:message key="common.command.back" />" data-url="${pageContext.request.contextPath}${functionalityUrl}" />
 			</div>
 		</td>
  	</tr>
</table>