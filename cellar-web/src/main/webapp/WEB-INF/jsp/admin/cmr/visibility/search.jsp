<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="app" tagdir="/WEB-INF/tags"%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/cmr/visibility/css/search-tree.css" type="text/css"/>
<link type="text/javascript" src="${pageContext.request.contextPath}/resources/cmr/embargo/js/jquery-ui-timepicker.js"></link>
<script type="stylesheet" src="${pageContext.request.contextPath}/resources/cmr/embargo/css/jquery-ui-timepicker-addon.css"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/cmr/embargo/js/jquery-ui-timepicker.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/cmr/visibility/js/search-tree.js"></script>
<style>



div.cellarid{float:right;width:440px;}
div.pids{float:right;width:50px;}
div.visible{float:right;width:45px;}
div.embargodate{float:right;width:120px;}
div.actions{float:right;width:60px;}

#type{float:right}

</style>

<form:form method="GET" id="visibilitySearchObjectForm" modelAttribute="search">
	<fmt:message key="cmr.visibility.search.objectId" />
	<form:input id="objectId" path="objectId" value="${param.objectId}" autofocus="autofocus" />
   	<input type="submit" id="submitButton" value="<fmt:message key="cmr.visibility.search.command.search" />"/>
</form:form>

<c:if test="${hierarchy != null}">
	<noscript><p><strong><fmt:message key="common.message.noJavascript" /></strong></p></noscript>
	<br/>
	<br/>
	<a href="#" id="expandAll" class="expand"><fmt:message key="cmr.visibility.search.expandAll" /></a>|<a href="#" id="collapseAll" class="expand"><fmt:message key="cmr.visibility.search.collapseAll" /></a>
	<br/>
	<ul id="tree" style="min-width: 840px; max-width: 900px">
		<li>
			<div>
				Type
				<div align="center" class="actions">
					<fmt:message key="cmr.visibility.search.tree.actions" />
				</div>
				<div align="center" class="embargodate">
					<fmt:message key="cmr.visibility.search.tree.embargoDate" />
				</div>
				<div align="center" class="visible">
					<fmt:message key="cmr.visibility.search.tree.visible" />
				</div>
				<div align="center" class="pids">
					<fmt:message key="cmr.visibility.search.tree.productionIdentifiers" />
				</div>
				<div align="center" class="cellarid">
					<fmt:message key="cmr.visibility.search.tree.cellarId" />
				</div>
			</div>
		</li>
		<app:visibility-tree-node node="${hierarchy}" />
	</ul>
	<div id="pidsDialog" hidden="true" title="<fmt:message key="cmr.visibility.search.dialog.productionIdentifiers.title" />">
		<div id="pidsText">
		</div>
	</div>
	
	<div id="workDisembargoerDialog" hidden="true" title="<fmt:message key="cmr.visibility.search.dialog.disembargo.title" />">
		<div id="disembargoerText">
		    <p><fmt:message key="cmr.visibility.search.dialog.disembargo.text" /></p>
		</div>
		<div id="disembargoerWaiting" style="text-align:center;">
			<p><img src="<c:url value="/resources/common/admin/css/images/ajax-loader-medium.gif" />" /></p>
			<p><fmt:message key="cmr.visibility.search.dialog.disembargo.loading" /></p>
		</div>
		<div id="disembargoerError">
		</div>
	</div>	
	<div id="workEmbargoerDialog" hidden="true" title="<fmt:message key="cmr.visibility.search.dialog.embargo.title" />">
		<div id="embargoerWaiting" style="text-align:center;">
			<p><img src="<c:url value="/resources/common/admin/css/images/ajax-loader-medium.gif" />" /></p>
			<p><fmt:message key="cmr.visibility.search.dialog.embargo.loading" /></p>
		</div>
		<div id="embargoerText">
			<form id="embargo-add-form">
				<fieldset>
					<label for="dateOfEmbargo"><fmt:message key="cmr.visibility.search.dialog.embargo.date" /></label>
					<input type="text" name="dateOfEmbargo" id="dateOfEmbargo" value="<c:if test="${hierarchy.underEmbargo}"><fmt:formatDate value="${hierarchy.embargoDate}" pattern="yyyyMMddHHmm" /></c:if>" />
					<span id="dateOfEmbargoErrors" hidden="true" class="error"><fmt:message key="cmr.visibility.search.dialog.embargo.date.invalid" /></span>
				</fieldset>
			</form>
		</div>
		<div id="embargoerError">
		</div>
	</div>
	<div id="errorDialog" hidden="true" title="Error">
		<div id="errorText">
		</div>
	</div>
</c:if>
