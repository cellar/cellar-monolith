<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<ul>
<sec:authorize access="hasRole('ROLE_SPARQL_ENDPOINT')">
  	<li><a href="${pageContext.request.contextPath}/admin/cmr/sparql/endpoint">Sparql Endpoint</a></li>
  </sec:authorize>
  <sec:authorize access="hasRole('ROLE_SPARQL_CONFIGURATION')">
  	<li><a href="${pageContext.request.contextPath}/admin/cmr/sparql/configuration">Configuration</a></li>
  </sec:authorize>
  <sec:authorize access="hasRole('ROLE_SPARQL_TEMPLATES')">
  	<li><a href="${pageContext.request.contextPath}/admin/cmr/sparql/templates">Response templates</a></li>
  </sec:authorize>
</ul>