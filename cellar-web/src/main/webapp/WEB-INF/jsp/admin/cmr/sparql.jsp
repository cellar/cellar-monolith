<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/common/sparql/js/joseki.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/common/sparql/css/joseki.css" type="text/css"/>
<div class="title">Sparql Endpoint</div>
<div class="sparql">
  <div class="menu">
    <tiles:insertAttribute name="sparqlMenu"/>
  </div>
  <div class="content">
    <tiles:insertAttribute name="sparqlBody"/>
  </div>
</div>