<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/cmr/batch-job/js/consult-job.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/cmr/delete-job/css/delete-consult.css"/>
<table class="items">
	<tr>
 		<td><fmt:message key="cmr.batch-job.consult.jobName" /></td>
     	<td colspan="2"><input id="jobName" type="text" class="input" name="jobName" size="100%" readonly="readonly" value="${batchJob.jobName}"/></td>
	</tr>
  	<tr>
      	<td><fmt:message key="cmr.batch-job.consult.sparqlQuery" /></td>
      	<td>
      		<textarea id="sparqlQuery" name="sparqlQuery" cols="100%" rows=4 readonly="readonly">${batchJob.sparqlQuery}</textarea>
      	</td>
  	</tr>
    <tr>
      	<td><fmt:message key="cmr.batch-job.consult.status" /></td>
      	<td>
      		<input id="jobStatus" type="text" class="input" name="jobStatus" size="20%" readonly="readonly" value="<spring:message code="cmr.batch-job.job.status.${batchJob.jobStatus}"/>"/>
      		<br />

			<c:if test="${batchJob.errorDescription != null}">
	     		<a href="#" id="dialogOpener">${batchJob.errorDescription}</a>
	     		<c:choose>
                    <c:when test="${showErrorTextArea}">
                        <br>
                       <div class="editable" id="dialogErrorDiv">
                           <ul>
                            ${batchJob.errorStacktrace}
                           </ul>
                       </div>
                    </c:when>
                    <c:otherwise>
                       <div id="dialog" title="<fmt:message key="cmr.batch-job.consult.error" />">
                            <p>${batchJob.errorStacktrace}</p>
                        </div>
                    </c:otherwise>
                </c:choose>

			</c:if>
      	</td>
  	</tr>
  	<tr>
  		<td><fmt:message key="cmr.batch-job.consult.creationDate" /></td>
		<td colspan="2"><input id="creationDate" type="text" class="input" name="creationDate" size="100%" readonly="readonly" value="<fmt:formatDate value="${batchJob.creationDate}" pattern="dd/MM/yyyy HH:mm" />"/></td>
  	</tr>
  	<tr>
  		<td><fmt:message key="cmr.batch-job.consult.lastModified" /></td>
  		<td colspan="2"><input id="lastModified" type="text" class="input" name="lastModified" size="100%" readonly="readonly" value="<fmt:formatDate value="${batchJob.lastModified}" pattern="dd/MM/yyyy HH:mm" />"/></td>
  	</tr>
  	<tr>
      	<td colspan="2">
      		<div style="text-align:center;">
				<input id="backButton" type="button" value="<fmt:message key="title.command.cancel" />" data-url="${pageContext.request.contextPath}${functionalityUrl}" />
 			</div>
 		</td>
  	</tr>
</table>
