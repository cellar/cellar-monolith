<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/cmr/batch-job/js/consult-job.js"></script>
<table class="items">
	<tr>
 		<td><fmt:message key="cmr.batch-job.consult.jobName" /></td>
     	<td colspan="2"><input id="jobName" type="text" class="input" name="jobName" size="100%" readonly="readonly" value="${batchJob.jobName}"/></td>
	</tr>
  	<tr>
      	<td><fmt:message key="cmr.batch-job.consult.sparqlQuery" /></td>
      	<td>
      		<textarea id="sparqlQuery" name="sparqlQuery" cols="100%" rows=4 readonly="readonly">${batchJob.sparqlQuery}</textarea>
      	</td>
  	</tr>
	<c:choose>
  		<c:when test="${batchJobType == 'REINDEX'}">
			<tr>
		      	<td><fmt:message key="cmr.batch-job.reindex.consult.priority" /></td>
		      	<td colspan="2"><input id="priority" type="text" class="input" name="priority" size="100%" readonly="readonly" value="<spring:message code="cmr.indexing.indexRequest.priority.${batchJob.priority.name}"/> (${batchJob.priority.priorityValue})"/></td>
		  	</tr>
			<tr>
				<td>
					<fmt:message key="cmr.batch-job.reindex.consult.generateIndexNotice" />
				</td>
				<td colspan="2">
					<c:choose>
				  		<c:when test="${batchJob.generateIndexNotice}">
				  			<img id="generateIndexNoticeEnabled" src="${base}/resources/common/admin/images/enabled.png" />
				  		</c:when>
				  		<c:otherwise>
				  			<img id="generateIndexNoticeDisabled" src="${base}/resources/common/admin/images/disabled.png" />
				  		</c:otherwise>
					</c:choose>
				</td>
			</tr>
			<tr>
				<td>
					<fmt:message key="cmr.batch-job.reindex.consult.generateEmbeddedNotice" />
				</td>
				<td colspan="2">
					<c:choose>
				  		<c:when test="${batchJob.generateEmbeddedNotice}">
				  			<img id="generateEmbeddedNoticeEnabled" src="${base}/resources/common/admin/images/enabled.png" />
				  		</c:when>
				  		<c:otherwise>
				  			<img id="generateEmbeddedNoticeDisabled" src="${base}/resources/common/admin/images/disabled.png" />
				  		</c:otherwise>
					</c:choose>
				</td>
			</tr>
  		</c:when>
  		<c:when test="${batchJobType == 'EMBARGO'}">
		  	<tr>
		      	<td><fmt:message key="cmr.batch-job.embargo.consult.embargoDate" /></td>
		      	<td colspan="2"><input id="embargoDate" type="text" class="input" name="embargoDate" size="100%" readonly="readonly" value="<fmt:formatDate value="${batchJob.embargoDate}" pattern="dd/MM/yyyy" />"/></td>
		  	</tr>
  		</c:when>
  		<c:when test="${batchJobType == 'EXPORT'}">
				<tr>
					<td>
						<fmt:message key="cmr.batch-job.export.consult.metadataOnly"/>
					</td>
					<td>						
						<c:choose>
							<c:when test="${batchJob.metadataOnly}">
					  			<img id="metadataOnlyEnabled" src="${base}/resources/common/admin/images/enabled.png" />
					  		</c:when>
					  		<c:otherwise>
					  			<img id="metadataOnlyDisabled" src="${base}/resources/common/admin/images/disabled.png" />
					  		</c:otherwise>
				  		</c:choose>
					</td>
				</tr>
				<tr>
					<td>
						<fmt:message key="cmr.batch-job.export.consult.useAgnosticURL"/>
					</td>
					<td>						
						<c:choose>
							<c:when test="${batchJob.useAgnosticURL}">
					  			<img id="useAgnosticURLEnabled" src="${base}/resources/common/admin/images/enabled.png" />
					  		</c:when>
					  		<c:otherwise>
					  			<img id="useAgnosticURLDisabled" src="${base}/resources/common/admin/images/disabled.png" />
					  		</c:otherwise>
				  		</c:choose>
			  		</td>
				</tr>
			<tr>
				<td>
					<fmt:message key="cmr.batch-job.export.consult.destinationFolder"/>
				</td>
				<td colspan="2">
					<input id="destinationFolder" type="text" class="input" name="destinationFolder" size="100%" readonly="readonly" value="${batchJob.destinationFolder}"/>
					<img id="destinationFolderHelpImg" class="tooltipHelper" src="${base}/resources/common/admin/images/help-icon.jpg" style="vertical-align:middle" title="<fmt:message key="cmr.batch-job.export.consult.destinationFolder.help"><fmt:param value="${baseDestinationFolder}"/><fmt:param value="${defaultFolderName}"/></fmt:message>"/>
				</td>
			</tr>
  		</c:when>
	</c:choose>
  	<tr>
      	<td><fmt:message key="cmr.batch-job.consult.status" /></td>
      	<td>
      		<input id="jobStatus" type="text" class="input" name="jobStatus" size="20%" readonly="readonly" value="<spring:message code="cmr.batch-job.job.status.${batchJob.jobStatus}"/>"/>
      		<br />
      		
			<c:if test="${batchJob.errorDescription != null}">
	     		<a href="#" id="dialogOpener">${batchJob.errorDescription}</a>
			  	<div id="dialog" title="<fmt:message key="cmr.batch-job.consult.error" />">
			   		<p>${batchJob.errorStacktrace}</p>
				</div>
			</c:if>
      	</td>
  	</tr>
  	<tr>
  		<td><fmt:message key="cmr.batch-job.consult.creationDate" /></td>
		<td colspan="2"><input id="creationDate" type="text" class="input" name="creationDate" size="100%" readonly="readonly" value="<fmt:formatDate value="${batchJob.creationDate}" pattern="dd/MM/yyyy HH:mm" />"/></td>
  	</tr>
  	<tr>
  		<td><fmt:message key="cmr.batch-job.consult.lastModified" /></td>
  		<td colspan="2"><input id="lastModified" type="text" class="input" name="lastModified" size="100%" readonly="readonly" value="<fmt:formatDate value="${batchJob.lastModified}" pattern="dd/MM/yyyy HH:mm" />"/></td>
  	</tr>
  	<tr>
      	<td><fmt:message key="cmr.batch-job.consult.works" /></td>
      	<td>
			<display:table name="workIdentifiersList" sort="external" partialList="true" size="${workIdentifiersList.fullListSize}" pagesize="${workIdentifiersList.objectsPerPage}" requestURI="">
				<display:column property="workId" title="Work URI" sortable="true" sortName="workId" headerClass="sortable" />
			</display:table>
			<form id="filterForm" method="GET" action="${pageContext.request.contextPath}${functionalityUrl}/consult">
				<fieldset>
					<span id="filterLabel"><fmt:message key="cmr.batch-job.consult.works.filter" /> </span>
	            	<input id="jobId" name="id" type="hidden" value="${batchJob.id}" />
					<input id="workFilter" name="workFilter" class="tooltipHelper" title="<fmt:message key="cmr.batch-job.consult.works.filter.helper" />" type="text" size="25" value="${param.workFilter}"/>
					<input id="filterButton" type="submit" value="<fmt:message key="common.command.ok" />" />
				</fieldset>
			</form>
		</td>
  	</tr>
  	<tr>
      	<td colspan="2">
      		<div style="text-align:center;">
				<input id="backButton" type="button" value="<fmt:message key="common.command.back" />" data-url="${pageContext.request.contextPath}${functionalityUrl}" />
 			</div>
 		</td>
  	</tr>
</table>
