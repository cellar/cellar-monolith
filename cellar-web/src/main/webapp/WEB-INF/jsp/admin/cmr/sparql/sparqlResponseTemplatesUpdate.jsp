<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/cmr/sparql/js/sparqlResponseTemplatesUpdate.js"></script>

<form:form method="POST" modelAttribute="uriTemplates" action="${pageContext.request.contextPath}/admin/cmr/sparql/templates/update">
	<table class="items">
		<tr>
			<td>
				<fmt:message key="cmr.uri-templates.create.uriPattern" />
			</td>
			<td>
			    <form:hidden id="id" path="id" value="${uriTemplates.id}"/>
				<form:input id="uriPattern" path="uriPattern" size="150" value="${uriTemplates.uriPattern}"/>
				<form:errors path="uriPattern" cssClass="error" />
			</td>
		</tr>
		<tr>
			<td>
				<fmt:message key="cmr.uri-templates.create.sparql"/>
			</td>
			<td>
				<form:textarea id="sparql" path="sparql" cols="100%" rows="4" value="${uriTemplates.sparql}"/>
				<form:errors path="sparql" cssClass="error" />
			</td>
		</tr>
		<tr>
            <td>
                <fmt:message key="cmr.uri-templates.create.xslt"/>
            </td>
            <td>
                <select name="xslt">
                    <c:forEach items="${xslt}" var="xsltValue">
                        <c:if test="${xsltValue eq uriTemplates.xslt}">
							<option label="${xsltValue}" selected="selected" >${xsltValue}</option>
						</c:if>
						<c:if test="${xsltValue ne uriTemplates.xslt}">
							<option label="${xsltValue}">${xsltValue}</option>
						</c:if>
                    </c:forEach>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                <fmt:message key="cmr.uri-templates.create.sequence"/>
            </td>
            <td>
                <form:input id="sequence" path="sequence" maxlength="5" value="${uriTemplates.sequence}"/>
                <form:errors path="sequence" cssClass="error" />
            </td>
        </tr>

	</table>
    <div style="text-align:center;">
   		<input id="submitButton" type="submit" value="<fmt:message key="cmr.uri-templates.create.command.update" />"/>
		<input id="cancelButton" type="button" value="<fmt:message key="title.command.cancel" />" data-url="${pageContext.request.contextPath}${functionalityUrl}" />
	</div>
</form:form>
