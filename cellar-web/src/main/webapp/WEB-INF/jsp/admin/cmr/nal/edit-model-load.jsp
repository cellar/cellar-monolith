<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="app" tagdir="/WEB-INF/tags"%>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/cmr/nal/js/edit-model-load.js"></script>

<form id="edit-model-load-form" method="post" action="${base}/admin/cmr/nal/modelloadrequests/update">
    <input type="hidden" id="modelId" name="modelId" value="${modelId}">
	<table class="items">
		<tr>
			<td>
				<fmt:message key="cmr.nal.table.column.modelUri" />
			</td>
			<td>
				<input type="text" size="100px" id="modelUri" name="modelUri" value="${modelUri}" required="required" readonly="readonly">
			</td>
		</tr>
		<tr>
			<td>
				<fmt:message key="cmr.nal.table.column.externalPid"/>
			</td>
			<td>
				<input type="text" size="100px" id="externalPid" name="externalPid" value="${externalPid}" required="required" readonly="readonly">
			</td>
		</tr>
		<tr>
			<td>
				<fmt:message key="cmr.nal.table.column.activationDate" />
			</td>
			<td>
			    <input type="text" class="datepicker" id="activationDate" name="activationDate" value="${activationDate}"/>
			</td>
		</tr>
		<tr>
			<td>
				<fmt:message key="cmr.nal.table.column.submissionStatus" />
			</td>
			<td>
               <select name="subStatus">
                   <c:forEach var="submissionStatus" varStatus="status" items="${submissionStatuss}">
                       <c:if test="${submissionStatus eq subStatus}">
                           <option value="${submissionStatus}" selected="selected">${submissionStatus}</option>
                       </c:if>
                       <c:if test="${submissionStatus ne subStatus}">
                           <option value="${submissionStatus}">${submissionStatus}</option>
                       </c:if>
                   </c:forEach>
               </select>
			</td>
		</tr>
	</table>
    <div style="text-align:center;">
   		<input id="submitButton" type="submit" value="<fmt:message key="common.command.update" />"/>
		<input id="cancelButton" type="button" value="<fmt:message key="title.command.cancel" />" data-url="${pageContext.request.contextPath}/admin/cmr/nal/modelloadrequests" />
	</div>
</form>
