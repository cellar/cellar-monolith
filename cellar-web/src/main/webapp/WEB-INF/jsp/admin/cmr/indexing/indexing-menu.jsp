<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<ul>

<sec:authorize access="hasRole('ROLE_INDEXING')">
  <li><a href="${pageContext.request.contextPath}/admin/cmr/indexing/dashboard"><fmt:message key="cmr.indexing.menu.dashboard" /></a></li>
  </sec:authorize>
  <sec:authorize access="hasRole('ROLE_INDEXING')">
  <li><a href="${pageContext.request.contextPath}/admin/cmr/indexing/add/target"><fmt:message key="cmr.indexing.menu.addTarget" /></a></li>
  </sec:authorize>
  <sec:authorize access="hasRole('ROLE_INDEXING')">
  <li><a href="${pageContext.request.contextPath}/admin/cmr/indexing/configuration"><fmt:message key="cmr.indexing.menu.configuration" /></a></li>
  </sec:authorize>
  <sec:authorize access="hasRole('ROLE_INDEXING')">
  	<li><a href="${pageContext.request.contextPath}/admin/cmr/indexing/reindex"><fmt:message key="cmr.indexing.menu.reindexJobs" /></a></li>
  </sec:authorize>
  <sec:authorize access="hasRole('ROLE_INDEXING')">
  	<li><a href="${pageContext.request.contextPath}/admin/cmr/indexing/scheduler"><fmt:message key="cmr.indexing.menu.scheduler" /></a></li>
  </sec:authorize>
</ul>