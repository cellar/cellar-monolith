<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="app" tagdir="/WEB-INF/tags"%>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/cmr/indexing/js/configuration.js"></script>

<form:form method="POST" id="indexingConfigurationForm" modelAttribute="configuration">
    <table class="items">
        <tr>
            <td>
                <fmt:message key="cmr.indexing.configuration.indexingEnabled" />
            </td>
            <td>
                <form:checkbox id="indexingEnabled" path="indexingEnabled" />
            </td>
        </tr>
		<tr>
			<td>
				<fmt:message key="cmr.indexing.configuration.minimalPriority" />
			</td>
			<td>
				<form:select id="minimalLevelOfPriorityHandled" path="minimalLevelOfPriorityHandled" >
				    <c:forEach var="priority" items="${indexingPrioritiesList}">
				        <form:option value="${priority}"><spring:message code="cmr.indexing.indexRequest.priority.${priority.name}"/> (${priority.priorityValue})</form:option>
				    </c:forEach>
				</form:select>
			</td>
		</tr>
        
        <tr>
            <td>
                <fmt:message key="cmr.indexing.configuration.linkedIndexingBehavior" />
            </td>
            <td>
                <form:select id="linkedIndexingBehavior" path="linkedIndexingBehavior" >
                    <c:forEach var="behavior" items="${linkedIndexingBehaviorsList}">
                        <form:option value="${behavior}"><spring:message code="cmr.indexing.indexRequest.linkedIndexingBehavior.${behavior}"/></form:option>
                    </c:forEach>
                </form:select>
            </td>
        </tr>
    </table>
    <div style="text-align:center;">
        <input id="submitButton" type="submit" value="<fmt:message key="cmr.indexing.configuration.command.update" />"/>
        <input id="cancelButton" type="button" value="<fmt:message key="title.command.cancel" />" data-url="${pageContext.request.contextPath}/admin/cmr/indexing" />
    </div>
</form:form>
