<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="app" tagdir="/WEB-INF/tags"%>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/cmr/indexing/js/add-target.js"></script>

<form:form method="POST" id="indexingAddWorkForm" modelAttribute="target">
	<table class="items">
		<tr>
			<td>
				<fmt:message key="cmr.indexing.add-target.workId" />
			</td>
			<td>
				<form:input id="workId" path="workId" autofocus="autofocus" />
				<form:errors path="workId" cssClass="error" />
			</td>
		</tr>
		<tr>
			<td>
				<fmt:message key="cmr.indexing.add-target.generateIndexNotice" />
			</td>
			<td>
				<form:checkbox path="generateIndexNotice" />
				<form:errors path="generateIndexNotice" cssClass="error" />
			</td>
		</tr>
		<tr>
			<td>
				<fmt:message key="cmr.indexing.add-target.generateEmbeddedNotice" />
			</td>
			<td>
				<form:checkbox path="generateEmbeddedNotice" />
				<form:errors path="generateEmbeddedNotice" cssClass="error" />
			</td>
		</tr>
		<tr>
			<td>
				<fmt:message key="cmr.indexing.add-target.expressions" />
			</td>
			<td>
				<form:select id="languages" path="languages" multiple="true" cssClass="languages">
					<c:forEach var="language" items="${target.languages}">
						<form:option value="${language}">${language}</form:option>
					</c:forEach>
				</form:select>
			</td>
		</tr>
	</table>
    <div style="text-align:center;">
   		<input id="submitButton" type="submit" value="<fmt:message key="cmr.indexing.add-target.command.add" />"/>
		<input id="cancelButton" type="button" value="<fmt:message key="title.command.cancel" />" data-url="${pageContext.request.contextPath}/admin/cmr/indexing" />
	</div>
</form:form>
