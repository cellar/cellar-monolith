<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<ul>
<sec:authorize access="hasRole('ROLE_NAL')">
  <li><a href="${pageContext.request.contextPath}/admin/cmr/nal/configurednals"><fmt:message key="cmr.nal.menu.configuredNals" /></a></li>
  </sec:authorize>
  
  <sec:authorize access="hasRole('ROLE_NAL')">
  <li><a href="${pageContext.request.contextPath}/admin/cmr/nal/modelloadrequests"><fmt:message key="cmr.nal.menu.modelLoadRequests" /></a></li>
  </sec:authorize>
  
</ul>