<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/cmr/batch-job/js/job-manager.js"></script>


<display:table name="batchJobs" id="batchJob"  pagesize="20" defaultsort="3" defaultorder="descending" requestURI="" sort="list">
	<display:column property="jobName" titleKey="cmr.batch-job.manager.jobTitle" sortable="true" headerClass="sortable" href="${pageContext.request.contextPath}${functionalityUrl}/consult" paramId="id" paramProperty="id" />
	<display:column sortProperty="jobStatus" titleKey="cmr.batch-job.manager.status" sortable="true" headerClass="sortable">
		<fmt:message key="cmr.batch-job.job.status.${batchJob.jobStatus}" />
	</display:column>
	<display:column sortProperty="creationDate" titleKey="cmr.batch-job.manager.createDate" sortable="true" headerClass="sortable">
		<fmt:formatDate value="${batchJob.creationDate}" pattern="dd/MM/yyyy HH:mm" />
	</display:column>
	<display:column titleKey="cmr.batch-job.manager.lastModified">
		<fmt:formatDate value="${batchJob.lastModified}" pattern="dd/MM/yyyy HH:mm" />
	</display:column>
	<display:column property="numberItems" titleKey="cmr.batch-job.manager.numberOfItems" />
	<c:choose>
  		<c:when test="${batchJobType == 'REINDEX'}">
			<display:column titleKey="cmr.batch-job.reindex.manager.priority">
				<fmt:message key="cmr.indexing.indexRequest.priority.${batchJob.priority}" />
			</display:column>
			<display:column titleKey="cmr.batch-job.manager.generateIndexNotice">
				<c:choose>
			  		<c:when test="${batchJob.generateIndexNotice}">
			  			<img id="generateIndexNoticeEnabled" src="${base}/resources/common/admin/images/enabled.png" />
			  		</c:when>
			  		<c:otherwise>
			  			<img id="generateIndexNoticeDisabled" src="${base}/resources/common/admin/images/disabled.png" />
			  		</c:otherwise>
				</c:choose>
			</display:column>
			<display:column titleKey="cmr.batch-job.manager.generateEmbeddedNotice">
				<c:choose>
			  		<c:when test="${batchJob.generateEmbeddedNotice}">
			  			<img id="generateEmbeddedNoticeEnabled" src="${base}/resources/common/admin/images/enabled.png" />
			  		</c:when>
			  		<c:otherwise>
			  			<img id="generateEmbeddedNoticeDisabled" src="${base}/resources/common/admin/images/disabled.png" />
			  		</c:otherwise>
				</c:choose>
			</display:column>
  		</c:when>
  		<c:when test="${batchJobType == 'EMBARGO'}">
			<display:column titleKey="cmr.batch-job.embargo.manager.embargoDate">
				<fmt:formatDate value="${batchJob.embargoDate}" pattern="dd/MM/yyyy HH:mm" />
			</display:column>
  		</c:when>
	</c:choose>
	
	<display:column titleKey="cmr.batch-job.manager.actions">
		<c:if test="${batchJob.deletable}">
			<a href=<c:url value="${functionalityUrl}/delete?id=${batchJob.id}"/>><fmt:message key="cmr.batch-job.manager.command.delete" /></a><br />
		</c:if>
		<c:if test="${batchJob.startable}">
			<a href=<c:url value="${functionalityUrl}/launch?id=${batchJob.id}"/>><fmt:message key="cmr.batch-job.manager.command.launch" /></a><br />
		</c:if>
		<c:if test="${batchJob.restartable}">
			<a href=<c:url value="${functionalityUrl}/restart?id=${batchJob.id}"/>><fmt:message key="cmr.batch-job.manager.command.restart" /></a><br />
		</c:if>
		<c:if test="${batchJob.restartable && batchJobType == 'EXPORT'}">
			<a href=<c:url value="${functionalityUrl}/edit?id=${batchJob.id}"/>><fmt:message key="cmr.batch-job.manager.command.edit" /></a>
		</c:if>
	</display:column>
</display:table>

<div style="text-align:center;">
	<input id="addJobButton" type="button" value="<fmt:message key="cmr.batch-job.manager.command.addJob" />" data-url="${pageContext.request.contextPath}${functionalityUrl}/create" />
	<c:choose>
		<c:when test="${areSchedulersRunning}">
			<input id="stopSchedulers" type="button" value="<fmt:message key="cmr.batch-job.manager.command.stopSchedulers" />" data-url="${pageContext.request.contextPath}${functionalityUrl}/stopSchedulers" />
		</c:when>
		<c:otherwise>
			<input id="restartSchedulers" type="button" value="<fmt:message key="cmr.batch-job.manager.command.restartSchedulers" />" data-url="${pageContext.request.contextPath}${functionalityUrl}/restartSchedulers" />
		</c:otherwise>	
	</c:choose>
</div>