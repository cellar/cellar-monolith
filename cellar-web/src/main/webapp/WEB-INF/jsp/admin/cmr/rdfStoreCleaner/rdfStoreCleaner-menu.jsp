<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<ul>
<sec:authorize access="hasRole('ROLE_RDF_STORE_CLEANER')">
  <li><a href="${pageContext.request.contextPath}/admin/cmr/rdfStoreCleaner/dashboard"><fmt:message key="cmr.rdfStoreCleaner.menu.dashboard" /></a></li>
  </sec:authorize>
  <sec:authorize access="hasRole('ROLE_RDF_STORE_CLEANER')">
  <li><a href="${pageContext.request.contextPath}/admin/cmr/rdfStoreCleaner/list"><fmt:message key="cmr.rdfStoreCleaner.menu.differences" /></a></li>
  </sec:authorize>
</ul>