<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/cmr/inferenceModel/css/infModel.css"
      type="text/css"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/cmr/onto/js/onto.js"></script>

<div>
    <form id="fileupload" action="${pageContext.request.contextPath}/admin/cmr/infModel/upload" method="POST"
          enctype="multipart/form-data">
        <div>
            <div class="title">
                <spring:message code="inferenceModel.view.title"/>
            </div>
            <div class="subtitle">
                <spring:message code="inferenceModel.view.description"/>
            </div>
            <div>
                <spring:message code="inferenceModel.view.description.file.path"/>:
                <span class="description-field"><c:out value="${description.name}"/></span>
            </div>
            <div>
                <spring:message code="inferenceModel.view.description.modificationDate"/>:
                <span class="description-field"><c:out value="${description.activationDate}"/></span>
            </div>
        </div>
        <br>
        <div>
            <div class="subtitle">
                <spring:message code="inferenceModel.view.loading"/>
            </div>
            <noscript>
                <input type="hidden" name="redirect" value="${pageContext.request.contextPath}/admin/cmr/infModel">
            </noscript>
            <div class="fileupload-buttonbar">
                <div class="fileupload-buttons">
                    <span class="fileinput-button">
                        <span><spring:message code="inferenceModel.view.button.add"/></span>
                        <input id="file" type="file" name="file">
                    </span>
                    <button type="submit" class="start"><spring:message
                            code="inferenceModel.view.button.start"/></button>
                    <button type="reset" class="cancel"><spring:message
                            code="inferenceModel.view.button.cancel"/></button>
                    <span class="fileupload-process"></span>
                </div>
                <div class="fileupload-progress fade" style="display:none">
                    <div class="progress" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                    <div class="progress-extended">&nbsp;</div>
                </div>
            </div>
            <table role="presentation" style="width:60%!important;">
                <tbody class="files"></tbody>
            </table>
        </div>

        <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
            <div class="slides"></div>
            <h3 class="title"></h3>
            <a class="prev">‹</a>
            <a class="next">›</a>
            <a class="close">×</a>
            <a class="play-pause"></a>
            <ol class="indicator"></ol>
        </div>
    </form>
</div>











