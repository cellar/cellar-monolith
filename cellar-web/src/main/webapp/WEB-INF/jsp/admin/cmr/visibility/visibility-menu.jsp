<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<ul>
<sec:authorize access="hasRole('ROLE_CONTENT_VISIBILITY')">
  	<li><a href="${pageContext.request.contextPath}/admin/cmr/visibility/search"><fmt:message key="cmr.visibility.menu.search" /></a></li>
  	<li><a href="${pageContext.request.contextPath}/admin/cmr/visibility/job"><fmt:message key="cmr.visibility.menu.job" /></a></li>
  </sec:authorize>
</ul>