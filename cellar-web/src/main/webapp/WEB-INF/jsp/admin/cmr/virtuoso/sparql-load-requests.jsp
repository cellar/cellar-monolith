<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/cmr/nal/css/model-load-requests.css" type="text/css"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/common/js/jquery-3.6.1.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/common/js/jquery-ui-1.13.2.custom.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/common/js/jquery.balloon.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/cmr/nal/js/model-load-requests.js"></script>

<div id="restartPopup" title="Restart sparql-load" style="display:none;">
    <form id="restart-popup-form" method="post" action="${base}/admin/cmr/virtuoso/sparqlloadrequests/restart">
        <fmt:message key="cmr.virtuoso.table.column.activationDate" />
        <input type="hidden" id="idSelected" name="idSelected"/>
        <input type="text" class="datepicker" id="activationDate" name="activationDate" required="required"/>
        <input id="restartButton" type="submit" value="<fmt:message key="cmr.virtuoso.command.restart" />"/>
    </form>
</div>

<form id="sparql-load-form" method="post" action="${base}/admin/cmr/virtuoso/sparqlloadrequests/search">
    <fieldset id="filterFieldset">
        <div>
            <fmt:message key="cmr.virtuoso.table.filter.modelUri" />
            <div id="modelUri_filter">
                <!--there's a magnifying glass png on the input, the space is so that the placeholder is not on top of it  -->
                <input type="text" name="modelUri" value="${modelUri}" id="modelUri_filter_input" placeholder="       Search..." autofocus>
                <input type="reset" value="x" id="resetModelUri_filter">
            </div>
        </div>

        <div>
            <fmt:message key="cmr.virtuoso.table.filter.submissionStatus" />
            <div id="operationType_filter">
                <select name="subStatus">
                    <option value=""></option>
                    <c:forEach var="submissionStatus" varStatus="status" items="${submissionStatus}">
                        <c:if test="${submissionStatus eq subStatus}">
                            <option value="${submissionStatus}" selected="selected">${submissionStatus}</option>
                        </c:if>
                        <c:if test="${submissionStatus ne subStatus}">
                            <option value="${submissionStatus}">${submissionStatus}</option>
                        </c:if>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div>
            <input id="searchButton" type="submit" value="<fmt:message key="cmr.virtuoso.command.search" />"/>
        </div>
    </fieldset>
    <br><br>
    <display:table name="sparqlLoadRequests" id="sparqlLoadRequest"  pagesize="20" defaultsort="1" defaultorder="ascending" requestURI="">
        <display:column property="uri" titleKey="cmr.virtuoso.table.column.uri" sortable="true" headerClass="sortable" />
        <display:column titleKey="cmr.virtuoso.table.column.activationDate" sortable="true" headerClass="sortable">
            <fmt:formatDate value="${sparqlLoadRequest.activationDate}" pattern="dd/MM/yyyy HH:mm" />
        </display:column>
        <display:column property="externalPid" titleKey="cmr.virtuoso.table.column.externalPid" sortable="true" headerClass="sortable" />
        <display:column titleKey="cmr.virtuoso.table.column.submissionStatus" sortable="true" headerClass="sortable">
            <c:if test="${sparqlLoadRequest.executionStatus == 'Success' || sparqlLoadRequest.executionStatus == 'Failure'}">
                <span title="${sparqlLoadRequest.executionStatus} on <fmt:formatDate value="${sparqlLoadRequest.executionDate}" pattern="dd/MM/yyyy HH:mm" />">${sparqlLoadRequest.executionStatus}</span>
            </c:if>
            <c:if test="${sparqlLoadRequest.executionStatus != 'Success' && sparqlLoadRequest.executionStatus != 'Failure'}">
                <span>${sparqlLoadRequest.executionStatus}</span>
            </c:if>
        </display:column>
        <display:column titleKey="cmr.virtuoso.table.column.actions">
            <a style="padding-left:0px;" title="<fmt:message key="cmr.virtuoso.command.update" />" href=<c:url value="/admin/cmr/virtuoso/sparqlloadrequests/edit?id=${sparqlLoadRequest.id}"/> id="editLink"><img id="enabled" src="${base}/resources/common/admin/images/edit.png" /></a>
            <c:if test="${sparqlLoadRequest.executionStatus == 'Failure'}">
                <a id="restartLink" style="padding-left:0px;" title="<fmt:message key="cmr.virtuoso.command.restart" />" href="#" data-id="${modelLoadRequest.id}" data-activation-date="<fmt:formatDate value="${modelLoadRequest.activationDate}" pattern="dd/MM/yyyy" />">
                    <img id="enabled" src="${base}/resources/common/admin/images/load.png" />
                </a>
            </c:if>
            <a style="padding-left:0px;" title="<fmt:message key="cmr.virtuoso.command.delete" />" href=<c:url value="/admin/cmr/virtuoso/sparqlloadrequests/delete?id=${sparqlLoadRequest.id}"/> id="deleteLink"><img id="enabled" src="${base}/resources/common/admin/images/delete.png" /></a>
        </display:column>
    </display:table>
</form>