<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ taglib prefix="stringutils" uri="stringutils" %>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/common/js/jquery.cookie.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/common/js/jquery.datatables.1.13.1.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/common/css/jquery.dataTables.min.css" type="text/css"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/admin/history/js/history.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/admin/history/css/history.css" type="text/css"/>

<div id="base" style="display:none;">${base}</div>

<div class="dashboard">
    <fieldset>
        <legend><fmt:message key="identifiers.history.title" /></legend>
        </br>
       
        <fieldset id="filterFieldset">
	        <div>
	            <fmt:message key="identifiers.history.table.filter.cellarId" />
	            <div id="cellarId_filter">
					<!--there's a magnifying glass png on the input, the space is so that the placeholder is not on top of it  -->
		            <input type="text" required id="cellarId_filter_input" placeholder="       Search..." autofocus >
	                <input type="reset" value="x" id="resetCellarId_filter">
	            </div>
	        </div>
	        
	        <div>
	            <fmt:message key="identifiers.history.table.filter.pid" />
	            <div id="productionId_filter">
					<!--there's a magnifying glass png on the input, the space is so that the placeholder is not on top of it  -->
		            <input type="text" required id="productionId_filter_input" placeholder="       Search...">
	                <input type="reset" value="x" id="resetProductionId_filter">
	            </div>
	        </div>
	        
	         <div>
	            <fmt:message key="identifiers.history.table.filter.obsolete" />
	            <div id="obsolete_filter">
	               <select>
                       <option value=""></option>
                       <option value="TRUE">TRUE</option>
                       <option value="FALSE">FALSE</option>
                   </select>
	            </div>
	        </div>
	        
	        
        </fieldset>
        
        
     <div id="identifierHistoryTableContainer">
	     <table id="identifiersHistoryTable">
	      <thead>
	      <tr>
	        <th id="cellarId_header"><spring:message code="identifiers.history.table.column.cellarid"/></th>
	        <th id="productionId_header"><spring:message code="identifiers.history.table.column.productionId"/></th>     
	        <th id="creationDate_header"><spring:message code="identifiers.history.table.column.creationDate"/></th>
	        <th id="obsolete_header"><spring:message code="identifiers.history.table.column.obsolete"/></th>
	        <th id="delete_header"><spring:message code="identifiers.history.table.column.delete"/></th>
	      </tr>
	      </thead>
	      <tbody>
	      <c:forEach var="identifierHistory" varStatus="status" items="${identifiersHistory}">
	        <tr class="single-nal">
	            <td>${identifierHistory.cellarId}</td>
	            <td>${identifierHistory.productionId}</td>
	            <td><fmt:formatDate value="${identifierHistory.creationDate}" pattern="yyyy-MM-dd HH:mm:ss" /></td>	            
	            <td>${identifierHistory.obsolete}</td>
	            <td>
	                <a href="<spring:url value="/history/delete"/>/${identifierHistory.id}">
	                    <img border="0" alt="Delete record" src="${base}/resources/common/admin/images/delete.png">
	                </a>
	            </td>
	        </tr>
	      </c:forEach>
	      </tbody>
	    </table>
    </div>
        
    </fieldset>
</div>