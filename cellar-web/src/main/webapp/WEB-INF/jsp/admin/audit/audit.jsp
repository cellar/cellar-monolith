<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<form id="audit-form" method="post" action="${base}/admin/audit/load">
	<fieldset>
		
		<div>
			<c:if test="${lastRowNumber gt rowsPerPage}">
				<c:if test="${previousPage > 0}">
					<span><a href='<c:url value="/admin/audit/load?pageNum=${previousPage}&objectId=${selectedObjectId}&logProcess=${selectedLogProcess}"/>'>&lt;&lt;</a></span>
				</c:if>
				${firstRowDisplayed} - ${lastRowDisplayed} / ${lastRowNumber}
				<c:if test="${!(lastRowDisplayed eq lastRowNumber)}">
					<span><a href='<c:url value="/admin/audit/load?pageNum=${nextPage}&objectId=${selectedObjectId}&logProcess=${selectedLogProcess}"/>'>&gt;&gt;</a></span>
				</c:if>
			</c:if>
		</div>
		<table>
			<thead>
				<tr>
					<th class="date">&nbsp;</th> 
					<th>
						<select name="logProcess">
							<c:forEach items="${logProcesses}" var="logProcess" varStatus="status">
								<c:if test="${logProcess eq selectedLogProcess}">
									<option label="${logProcess}" selected="selected" >${logProcess}</option>
								</c:if>
								<c:if test="${logProcess ne selectedLogProcess}">
									<option label="${logProcess}">${logProcess}</option>
								</c:if>
							</c:forEach>
						</select>
					</th>
					<th style="white-space: nowrap;">
						<input type="text" name="objectId" value="${selectedObjectId}" size="45" autofocus />
						<input type="submit" name="loadButton" value='<spring:message code="audit.button.load"/>'>&nbsp;&nbsp;&nbsp;
					</th>					
					<th>
					</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${events}" var="event" varStatus="vs">
					<c:set var="pi">
						<c:if test="${vs.index % 2 == 0}">odd</c:if>
					</c:set>
                    <c:set var="pi">
                        <c:if test="${vs.index % 2 == 0}">odd</c:if>
                    </c:set>
                    <c:choose>
	                    <c:when test="${event.type == 'Fail'}">
                            <tr class="${pi}" style="color: red">
	                    </c:when>
	                    <c:otherwise>
                            <tr class="${pi}">
	                    </c:otherwise>
                    </c:choose>
                        <td class="date"><fmt:formatDate value="${event.date}" pattern="yyyy-MM-dd HH:mm:ss" /></td>                
                        <td>${event.process} (${event.action})</td>
                        <td>${event.objectIdentifier}</td>
                        <td>${event.message}</td>
                    </tr>
                </c:forEach>
			</tbody>
		</table>		
		<div>
			<c:if test="${lastRowNumber gt rowsPerPage}">
				<c:if test="${previousPage > 0}">
					<span><a href='<c:url value="/admin/audit/load?pageNum=${previousPage}&objectId=${selectedObjectId}&logProcess=${selectedLogProcess}"/>'>&lt;&lt;</a></span>
				</c:if>
				${firstRowDisplayed} - ${lastRowDisplayed} / ${lastRowNumber}
				<c:if test="${!(lastRowDisplayed eq lastRowNumber)}">
					<span><a href='<c:url value="/admin/audit/load?pageNum=${nextPage}&objectId=${selectedObjectId}&logProcess=${selectedLogProcess}"/>'>&gt;&gt;</a></span>
				</c:if>
			</c:if>
		</div>
	</fieldset>	
</form>
<br/>
<hr/>
<br/>
<form method="post" action="${base}/admin/audit/download">	
	<fieldset title='<spring:message code="audit.fieldset.donwload"/>'>
		<span>
			<spring:message code="audit.label.date"/>
			<input type="text" class="datepicker" name="dateFrom"/> - <input type="text" class="datepicker" name="dateTo"/>
			<input type="submit" name="downloadButton" value='<spring:message code="audit.button.donwload"/>'/>
		</span>
	</fieldset>
</form>	
<br/>
<hr/>
<br/>
<sec:authorize access="hasRole('ROLE_LOG')">
	<form method="post" action="${base}/admin/audit/delete" >
		<fieldset>
			<span>
				<spring:message code="audit.label.endDate"/>
				<input type="text" class="datepicker" name="endDate"/>
				<input type="submit" name="deleteButton" value='<spring:message code="audit.button.delete"/>'/>
			</span>
		</fieldset>
	</form>
</sec:authorize>


