<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/admin/mets/css/mets-upload.css" type="text/css"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/admin/mets/js/mets-upload.js"></script>
<div class="dashboard" id="mets">
    <c:if test="${metsResponse != null}">
        <p><a href="#" id="toggleJson">Show JSON</a><textarea style="display:none" id="json" cols="100%" rows="3">${metsResponse}</textarea></p>
    </c:if>
    <!-- Title -->
    <div class="title">
        <spring:message code="mets.upload.title"/>
    </div>

    <form:form modelAttribute="metsUploadForm" method="post" enctype="multipart/form-data">
        <div class="form-container flex">
            <div class="form-item-small">
                <span class="text"><spring:message code="mets.upload.priority"/></span>
                <form:select id="priority" name="priority" class="form-item-big" path="priority">
                    <form:option value="AUTHENTICOJ" selected="${priority == 'AUTHENTICOJ'? 'selected':'' }"><spring:message code="mets.upload.priority.authenticoj"/></form:option>
                    <form:option value="DAILY" selected="${priority == 'DAILY'? 'selected':''}"><spring:message code="mets.upload.priority.daily"/></form:option>
                    <form:option value="BULK" selected="${priority == 'BULK' || empty priority ? 'selected': '' }"><spring:message code="mets.upload.priority.bulk"/></form:option>
                    <form:option value="BULK_LOWPRIORITY" selected="${priority == 'BULK_LOWPRIORITY' ? 'selected': ''}"><spring:message code="mets.upload.priority.bulk_low"/></form:option>
                </form:select>


            </div>
            <div class="form-item-big">
                <span class="text"><spring:message code="mets.upload.callback.uri"/></span>
                <form:input id="callbackURI" name="callbackURI" class="form-item-big" path="callbackURI" />
            </div>
        </div>
        <p>
            <span class="text"><spring:message code="mets.upload.file"/></span>
            <form:input path="fileData" id="fileData" class="form-item-big flex" type="file" />
            <form:errors path="fileData" cssClass="error" />
        </p>
        <div class="button-container">
            <input type="reset" id="cancel" value="Cancel"/>
            <input type="submit" id="upload" value="Upload"/>
        </div>
    </form:form>

</div>
