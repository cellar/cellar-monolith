<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ taglib prefix="stringutils" uri="stringutils" %>

<script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/common/js/jquery.dataTables.min.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/common/css/jquery.dataTables.min.css"
      type="text/css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/common/css/jquery-ui-datepicker.css"
      type="text/css"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/admin/fixity/js/fixity.js"></script>
<script type="text/javascript"
        src="${pageContext.request.contextPath}/resources/common/js/jquery-ui-timepicker.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/admin/fixity/css/fixity.css" type="text/css"/>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/common/js/jquery.cookie.js"></script>

<div id="fixityBaseUrl" style="display:none;">${fixityBaseURL}</div>
<div id="FIXITY_STATUS" style="display:none;">${FIXITY_STATUS}</div>
<div id="FIXITY_STATISTICS" style="display:none;">${FIXITY_STATISTICS}</div>

<div class="dashboard">
    <div id="tabs">
        <ul id="menu">
            <li><a id="menu-tab-1" href="#tabs-1">Checksums dashboard</a></li>
            <li><a id="menu-tab-2" href="#tabs-2">Thread pools</a></li>
            <li><a id="menu-tab-3" href="#tabs-3">Statistics</a></li>
            <li><a id="menu-tab-4" href="#tabs-4">SPARQL</a></li>
        </ul>

        <div id="tabs-1">
            <form:form action="" method="GET">
                <h2><br><br></h2>
                <input type="text" id="searchTextField">
                <input type="button" value="Search" id="searchButton" style="height: 34px"/>
                <input type="button" value="Search invalid checksums" id="searchInvalidButton" style="height: 34px"/>
                <input type="button" value="Recheck checksums" id="searchRecheckButton" data-url="/admin/fixity/checksum/recheck" style="height: 34px"/>
                <table width="70%" style="border: 3px;background: rgb(243, 244, 248);">
                    <tr>
                        <td>
                            <table id="checksumTable" class="display" cellspacing="0" width="100%">
                                <thead>
                                <tr>
                                    <th><fmt:message key="fixity.table.filter.itemCellarId"/></th>
                                    <th><fmt:message key="fixity.table.filter.fixityResult"/></th>
                                    <th><fmt:message key="fixity.table.filter.lastChecked"/></th>
                                    <th><fmt:message key="fixity.table.filter.oldChecksum"/></th>
                                    <th><fmt:message key="fixity.table.filter.newChecksum"/></th>
                                </tr>
                                </thead>
                            </table>
                        </td>
                    </tr>
                </table>
            </form:form>
        </div>

        <div id="tabs-2">
            <fieldset>
                <legend><fmt:message key="fixity.threadpool.title"/></legend>
                <c:choose>
                    <c:when test="${isPaused}">
                        <div style="text-align:center;">
                            <input type="button" id="resumeButton" value="<fmt:message key="fixity.resume.threadPool" />"
                                   data-url="${fixityBaseURL}control/resume"/>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div style="text-align:center;">
                            <input type="button" id="pauseButton" value="<fmt:message key="fixity.pause.threadPool" />"
                                   data-url="${fixityBaseURL}control/pause"/>
                        </div>
                    </c:otherwise>
                </c:choose>

                <c:choose>
                    <c:when test="${isShutdown}">
                        <div style="text-align:center;">
                            <input type="button" id="startButton" value="<fmt:message key="fixity.start.scheduler" />"
                                   data-url="${fixityBaseURL}control/start"/>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div style="text-align:center;">
                            <input type="button" id="stopButton" value="<fmt:message key="fixity.stop.scheduler" />"
                                   data-url="${fixityBaseURL}control/stop"/>
                        </div>
                    </c:otherwise>
                </c:choose>
                <div id="inner_tbl_FIXITY_STATUS"></div>
            </fieldset>
        </div>

        <div id="tabs-3">
            <fieldset>
                <legend><fmt:message key="fixity.statistics.title"/></legend>
                <div id="inner_tbl_FIXITY_STATISTICS"></div>
            </fieldset>
        </div>

        <div id="tabs-4">
            <div class="tabHeader">
                <fieldset>
                    <legend><fmt:message key="fixity.sparql.title"/></legend>
                    <div class="wrapper">
                        <div class="prefixes">
                            <ul>Used prefixes:
                                <li>oracle: &lt;http://www.oracle.com/2009/05/orardf/jena-joseki-ns#&gt;</li>
                                <li>rdfs: &lt;http://www.w3.org/2000/01/rdf-schema#&gt;</li>
                                <li>rdf: &lt;http://www.w3.org/1999/02/22-rdf-syntax-ns#&gt;</li>
                                <li>xsd: &lt;http://www.w3.org/2001/XMLSchema#&gt;</li>
                                <li>module: &lt;http://joseki.org/2003/06/module#&gt;</li>
                                <li>joseki: &lt;http://joseki.org/2005/06/configuration#&gt;</li>
                                <li>ja: &lt;http://jena.hpl.hp.com/2005/11/Assembler#&gt;</li>
                                <li>cdm: &lt;http://publications.europa.eu/ontology/cdm#&gt;</li>
                                <li>owl: &lt;http://www.w3.org/2002/07/owl#&gt;</li>
                                <li>cmr: &lt;http://publications.europa.eu/ontology/cdm/cmr#&gt;</li>
                            </ul>
                        </div>
                        <div class="text">
                            SPARQL query which should return a list of CELLAR ID of type ITEM.<br/>
                            All other results will be ignored.
                        </div>
                        <div id="sparql_query_fieldset">
                            <div class="query-wrapper">
                                <textarea id="sparql-query-field" name="query" cols="100%" rows="4">
                                    SELECT ?x WHERE { ?x ?y <http://publications.europa.eu/ontology/cdm#item> }
                                </textarea>
                            </div>
                            <input type="button" id="send_sparql_query" value="Execute SPARQL query"
                                   data-url="${fixityBaseURL}sparql"/>
                            <div id="send_sparql_query_response_message"/>
                        </div>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
</div>