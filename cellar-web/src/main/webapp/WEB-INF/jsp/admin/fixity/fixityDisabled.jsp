<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ page import="eu.europa.ec.opoce.cellar.cl.configuration.enums.CellarConfigurationPropertyKey" %>

<div>
	<h1 style="text-align:center;"><fmt:message key="fixity.disabled.message.header" /></h1>
	<h2 style="text-align:center;"><fmt:message key="fixity.disabled.message.detail" /></h2>
	<ul style="text-align:center; list-style-type: square;">
		<li>${CellarConfigurationPropertyKey.cellarServiceIntegrationFixityEnabled_Key.toString()}</li>
		<li>${CellarConfigurationPropertyKey.cellarServiceIntegrationFixityBaseUrl_Key.toString()}</li>
	</ul>
</div>