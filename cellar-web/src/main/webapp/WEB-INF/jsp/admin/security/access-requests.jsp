<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/admin/security/js/access-requests.js"></script>

<display:table name="userAccessRequests" id="userAccessRequest"  pagesize="20" defaultsort="1" defaultorder="ascending" requestURI="">
	<display:column property="username" titleKey="security.user-access-request.manager.username" sortable="true" headerClass="sortable" />
	<display:column property="email" titleKey="security.user-access-request.manager.email" sortable="true" headerClass="sortable" />
	<display:column titleKey="security.user-access-request.manager.action">
	
		<!-- Construct a "View" Url-->
		<c:url var="viewUrl" value="${pageContext.request.contextPath}/admin/security/access-requests/accept">
			<c:param name ="id" value="${userAccessRequest.id}" />
		</c:url>
				
		<!-- Construct a "Delete" Url-->
		<c:url var="deleteUrl" value="${pageContext.request.contextPath}/admin/security/access-requests/delete">
			<c:param name ="id" value="${userAccessRequest.id}" />
		</c:url>
	
		<a class="viewLink" href="${viewUrl}" >
			<fmt:message key="security.user-access-request.manager.action.view" />
		</a>
		&emsp;|&emsp;
		<a class="deleteLink" href="${deleteUrl}" data-username="${userAccessRequest.username}">
			<fmt:message key="security.user-access-request.manager.action.delete" />
		</a>
	</display:column>
</display:table>