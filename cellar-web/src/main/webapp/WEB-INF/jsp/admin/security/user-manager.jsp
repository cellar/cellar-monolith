<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/admin/security/js/user-manager.js"></script>

<display:table name="users" id="user"  pagesize="20" defaultsort="1" defaultorder="ascending" requestURI="">
	<display:column property="username" titleKey="security.user.manager.username" sortable="true" headerClass="sortable" />
	<display:column property="email" titleKey="security.user.manager.email" sortable="true" headerClass="sortable" />
	<display:column property="group.groupName" titleKey="security.user.manager.group" sortable="true" headerClass="sortable" />
	<display:column titleKey="security.user.manager.enabled">
		<c:choose>
	  		<c:when test="${user.enabled}">
	  			<img id="userEnabled" src="${base}/resources/common/admin/images/enabled.png" />
	  		</c:when>
	  		<c:otherwise>
	  			<img id="userDisabled" src="${base}/resources/common/admin/images/disabled.png" />
	  		</c:otherwise>
		</c:choose>
	</display:column>
	<display:column titleKey="security.user.manager.action" href="${pageContext.request.contextPath}/admin/security/user/edit" paramId="id" paramProperty="id">
		<fmt:message key="security.user.manager.action.edit" />
	</display:column>
</display:table>
<div style="text-align:center;">
	<input id="addUserButton" type="button" value="<fmt:message key="security.user.command.add" />" data-url="${pageContext.request.contextPath}/admin/security/user/create"/>
</div>