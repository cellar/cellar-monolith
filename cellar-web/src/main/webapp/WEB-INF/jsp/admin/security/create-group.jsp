<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/admin/security/js/create-group.js"></script>

<form:form method="POST" modelAttribute="group">
	<table class="items">
		<tr>
			<td>
				<fmt:message key="security.group.create.groupName" />
			</td>
			<td>
				<form:input id="groupName" path="groupName" maxlength="255" />
				<form:errors path="groupName" cssClass="error" />
			</td>
		</tr>
		<tr>
			<td>
				<fmt:message key="security.group.create.privileges" />
			</td>
			<td>
				<form:select id="roles" path="roles" multiple="true" size="10">
				    <c:forEach var="role" items="${rolesList}">
				        <form:option value="${role.id}">${role.roleName} (${role.roleAccess})</form:option>
				    </c:forEach>
				</form:select>
				<form:errors path="roles" cssClass="error" />
			</td>
		</tr>
	</table>
    <div style="text-align:center;">
   		<input id="submitButton" type="submit" value="<fmt:message key="security.group.command.create" />"/>
		<input id="cancelButton" type="button" value="<fmt:message key="title.command.cancel" />" data-url="${pageContext.request.contextPath}/admin/security/group"/>
	</div>
</form:form>
