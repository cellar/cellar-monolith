<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="app" tagdir="/WEB-INF/tags"%>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/admin/security/js/edit-user.js"></script>

<form:form method="POST" modelAttribute="editUserCommand">
	<form:hidden path="username" />
	<table class="items">
		<tr>
			<td>
				<fmt:message key="security.user.update.username" />
			</td>
			<td>
				<input type="text" id="username" name="username" disabled="disabled" value="${editUserCommand.username}">
			</td>
		</tr>
		<c:if test="${cellarConfiguration.cellarServiceEuloginUserMigrationEnabled and isUserNonMigrated}">
			<tr>
				<td>
					<fmt:message key="security.user.update.password"/>
				</td>
				<td>
					<form:password id="userPassword" path="password" maxlength="60" />
					<form:errors path="password" cssClass="error" />
					
					<img id="passwordHelpImg" class="tooltipHelper" src="${base}/resources/common/admin/images/help-icon.jpg" style="vertical-align:middle" title="<fmt:message key="security.user.update.passwordHelper"/>"/>
				</td>
			</tr>
			<tr>
				<td>
					<fmt:message key="security.user.update.passwordConfirm" />
				</td>
				<td>
					<form:password id="userConfirmPassword" path="confirmPassword" maxlength="60" />
					<form:errors path="confirmPassword" cssClass="error" />
					
					<img id="confirmPasswordHelpImg" class="tooltipHelper" src="${base}/resources/common/admin/images/help-icon.jpg" style="vertical-align:middle" title="<fmt:message key="security.user.update.passwordHelper"/>"/>
				</td>
			</tr>
		</c:if>
		<tr>
			<td>
				<fmt:message key="security.user.update.group" />
			</td>
			<td>
				<form:select items="${groupsList}" path="groupId" itemLabel="groupName" itemValue="id" />
			</td>
		</tr>
		<tr>
			<td>
				<fmt:message key="security.user.update.enabled"/>
			</td>
			<td>
				<form:checkbox id="userEnabled" path="enabled" />
			</td>
		</tr>
	</table>
    <div style="text-align:center;">
   		<input id="submitButton" type="submit" value="<fmt:message key="security.user.command.update" />"/>
   		<input id="deleteButton" type="button" value="<fmt:message key="security.user.command.delete" />" data-url="${pageContext.request.contextPath}/admin/security/user/delete?id=" data-id="${editUserCommand.id}"/>
		<input id="cancelButton" type="button" value="<fmt:message key="title.command.cancel" />" data-url="${pageContext.request.contextPath}/admin/security/user"/>
	</div>
</form:form>
