<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<ul>
<sec:authorize access="hasRole('ROLE_SECURITY')">
  	<li><a href="${pageContext.request.contextPath}/admin/security/user"><fmt:message key="security.menu.userManagement" /></a></li>
  	<li><a href="${pageContext.request.contextPath}/admin/security/group"><fmt:message key="security.menu.groupManagement" /></a></li>
  	<li><a href="${pageContext.request.contextPath}/admin/security/access-requests"><fmt:message key="security.menu.userAccessRequestManagement" /></a></li>
  </sec:authorize>
</ul>