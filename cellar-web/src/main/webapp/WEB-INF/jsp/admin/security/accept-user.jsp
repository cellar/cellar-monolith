<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="app" tagdir="/WEB-INF/tags"%>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/admin/security/js/accept-user.js"></script>

<form:form method="POST" action="${pageContext.request.contextPath}/admin/security/access-requests/accept" modelAttribute="acceptUserCommand">
	<table class="items">
		<tr>
			<td>
				<fmt:message key="security.user.create.username" />
			</td>
			<td>
				<form:input id="username" path="username" maxlength="255" readOnly="true"/>
				<form:errors path="username" cssClass="error" />
			</td>
		</tr>
		<tr>
			<td>
				<fmt:message key="security.user.create.group" />
			</td>
			<td>
				<form:select items="${groupsList}" path="groupId" itemLabel="groupName" itemValue="id" />
				<form:errors path="groupId" cssClass="error" />
			</td>
		</tr>
		<tr>
			<td>
				<fmt:message key="security.user.create.enabled"/>
			</td>
			<td>
				<form:checkbox id="userEnabled" path="enabled" />
			</td>
		</tr>
	</table>
	<form:hidden path="userAccessRequestId" />
    <div style="text-align:center;">
   		<input id="submitButton" type="submit" value="<fmt:message key="security.user-access-request.manager.accept.btn" />"/>
		<input id="cancelButton" type="button" value="<fmt:message key="title.command.cancel" />" data-url="${pageContext.request.contextPath}/admin/security/access-requests"/>
	</div>
</form:form>
