<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/admin/security/js/group-manager.js"></script>

<display:table name="groups"  pagesize="20" defaultsort="1" defaultorder="ascending" requestURI="" decorator="eu.europa.ec.opoce.cellar.cl.controller.admin.security.GroupTableDecorator">
	<display:column property="groupName" titleKey="security.group.manager.groupName" sortable="true" headerClass="sortable" />
	<display:column property="roles" titleKey="security.group.manager.activeSections" />
	<display:column titleKey="security.group.manager.action" href="${pageContext.request.contextPath}/admin/security/group/edit" paramId="id" paramProperty="id">
		<fmt:message key="security.group.manager.action.edit" />
	</display:column>
</display:table>
<div style="text-align:center;">
	<input id="addGroupButton" type="button" value="<fmt:message key="security.group.command.add" />" data-url="${pageContext.request.contextPath}/admin/security/group/create"/>
</div>