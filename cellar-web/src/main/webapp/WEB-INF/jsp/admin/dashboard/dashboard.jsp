<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>

<div class="title"><spring:message code="admin.dashboard.title"/></div>

<div class="dashboard">
	<fieldset>
		<legend><spring:message code="dashboard.fieldset.checksums"/></legend>
		<div>
			<label id="statusChecksums" for="statusChecksums"><spring:message code="dashboard.statusChecksum.label"/></label>&nbsp;
			<c:out value="${statusChecksums}"/>
		</div>
	</fieldset>

	<div class="topBock">
		<fieldset class="leftSide">
			<legend><spring:message code="dashboard.fielset.queues"/></legend>
			<div>
				<label id="authenticOJLabel" for="authenticOJ"><spring:message code="dashboard.authenticOJ.label"/></label>&nbsp;
				<c:out value="${authenticOJ}"/>
			</div>
			<div>
				<label id="dailyLabel" for="daily"><spring:message code="dashboard.daily.label"/></label>&nbsp;
				<c:out value="${daily}"/>
			</div>
			<div>
				<label id="bulkLabel" for="bulk"><spring:message code="dashboard.bulk.label"/></label>&nbsp;
				<c:out value="${bulk}"/>
			</div>
			<div>
				<label id="bulkLowPriorityLabel" for="bulkLowPriority"><spring:message code="dashboard.bulkLowPriority.label"/></label>&nbsp;
				<c:out value="${bulkLowPriority}"/>
			</div>
			<div>
				<label id="indexingQueueLabel" for="indexingQueue"><spring:message code="dashboard.indexingQueue.label"/></label>&nbsp;
				<c:out value="${indexingQueue}"/>
			</div>
			<div>
				<label id="failedIndexingRequestLabel" for="failedIndexingRequest"><spring:message code="dashboard.failedIndexingRequest.label"/></label>&nbsp;
				<c:out value="${failedIndexingRequest}"/>
			</div>
			<div>
				<label id="minimalLevel" for="minimalLevel"><spring:message code="dashboard.minimalLevel.label"/></label>&nbsp;
				<c:out value="${minimalLevel.name} (${minimalLevel.priorityValue})"/>
			</div>
			<div>
				<label id="statusIndex" for="statusIndex"><spring:message code="dashboard.statusIndex.label"/></label>&nbsp;
				<fmt:message key="cmr.indexing.serviceStatus.${indexStatus}" />
			</div>
			<div>
				<label id="statusIngestion" for="statusIngestion"><spring:message code="dashboard.statusIngestion.label"/></label>&nbsp;
				<c:out value="${ingestionStatus}"/>
			</div>
			<div>
				<label id="numberEntriesVirtuosoBackupTable" for="numberEntriesVirtuosoBackupTable"><spring:message code="dashboard.numberEntriesVirtuosoBackupTable.label"/></label>&nbsp;
				<c:out value="${numberEntriesVirtuosoBackupTable}"/>
			</div>
		</fieldset>

		<fieldset class="rightSide">
			<legend><spring:message code="dashboard.fieldset.components"/></legend>
			<c:forEach items="${packages}" var="package">
				<div>${package['name']} : ${package['revision']}</div>
			</c:forEach>
		</fieldset>

	</div>

	<div style="clear: both;"></div>

	<fieldset>
		<legend><spring:message code="dashboard.fieldset.ontologies"/></legend>
		<c:forEach items="${ontologies}" var="ontology">
			<div>
				${ontology.name}<br/>
				&nbsp;&nbsp;URI: ${ontology.uri}<br/>
				&nbsp;&nbsp;Activation date: ${ontology.nalOntoVersion.formattedVersionDate}<br/>
				&nbsp;&nbsp;Version: ${ontology.nalOntoVersion.formattedVersionNumber}<br />
			</div>
		</c:forEach>
	</fieldset>

	<div style="clear: both;"></div>

	<fieldset>
		<legend><spring:message code="dashboard.fieldset.digitalObjects"/></legend>
		<div id="ajax-content">
			Please click on the "Refresh" button to load the content.
		</div>
	</fieldset>
	<button id="btnLoad">Refresh</button>

</div>	
