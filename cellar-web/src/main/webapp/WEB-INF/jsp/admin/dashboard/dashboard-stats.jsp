<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Map" %>

<%
  Map<String, Integer> expressionsPerLanguage = (Map<String, Integer>) request.getAttribute("expressionsPerLanguage");
  Map<String, Integer> manifestationsPerType = (Map<String, Integer>) request.getAttribute("manifestationsPerType");
  Map<String, Map<String, Integer>> manifestationsPerTypePerLanguage = (Map<String, Map<String, Integer>>) request.getAttribute("manifestationsPerTypePerLanguage");
  List<String> languagesForTable = (List<String>) request.getAttribute("languagesForTable");
%>
<div class="separator"><span class="status"># works:</span><span class="text"><br/><c:out value="${totalWorks}"/></span></div>
<div class="separator"><span class="status"># dossiers:</span><span class="text"><br/><c:out value="${totalDossiers}"/></span></div>
<div class="separator"><span class="status"># events:</span><span class="text"><br/><c:out value="${totalEvents}"/></span></div>
<div class="separator"><span class="status"># top level events:</span><span class="text"><br/><c:out value="${totalTopLevelEvents}"/></span></div>
<div class="separator"><span class="status"># agents:</span><span class="text"><br/><c:out value="${totalAgents}"/></span></div>
<div class="separator"><span class="status"># expressions per language:</span><span class="text"><br/>
    <%
      StringBuilder sb = new StringBuilder();
      for (String language : expressionsPerLanguage.keySet()) {
        Integer total = expressionsPerLanguage.get(language);
        sb.append(language).append(": ").append(total).append(", ");
      }
      String value = "None";
      if (sb.toString().length() >= 2) {
        value = sb.toString().substring(0, sb.toString().length() - 2);
      }
    %>
   	<%=value%>
</span></div>
<div class="separator"><span class="status"># manifestations per type:</span><span class="text"><br/>
    <%
      sb = new StringBuilder();
      for (String type : manifestationsPerType.keySet()) {
        Integer total = manifestationsPerType.get(type);
        sb.append(type).append(": ").append(total).append(", ");
      }
      value = "None";
      if (sb.toString().length() >= 2) {
        value = sb.toString().substring(0, sb.toString().length() - 2);
      }
    %>
    <%=value%>
</span></div>
<div class="separator"><span class="status"># manifestations per language and type:</span><span class="text"><br/>
    <%
      sb = new StringBuilder();
      sb.append("<table class=\"logDashBoardTable\" cellspacing=\"0\" cellpadding=\"0\">");

      /* table headers */
      sb.append("<tr class=\"header\">").append("<td class=\"type\"></td>");
      for (String language : languagesForTable) {
        sb.append("<td>").append(language).append("</td>");
      }
      sb.append("</tr>");

      /* table body */
      Map<String, Integer> globalLanguageCount = new HashMap<String, Integer>();
      for (String type : manifestationsPerTypePerLanguage.keySet()) {
        sb.append("<tr><td class=\"type\">").append(type).append("</td>");
        for (String language : languagesForTable) {
          Integer totalLanguagesForThisType = manifestationsPerTypePerLanguage.get(type).get(language);
          if (null != totalLanguagesForThisType) {
            sb.append("<td>").append(totalLanguagesForThisType).append("</td>");
            if (null != globalLanguageCount.get(language)) {
              globalLanguageCount.put(language, globalLanguageCount.get(language) + totalLanguagesForThisType);
            }
            else {
              globalLanguageCount.put(language, totalLanguagesForThisType);
            }
          }
          else {
            sb.append("<td>").append(0).append("</td>");
          }
        }
        sb.append("</tr>");
      }

      /* table footer */
      sb.append("<tr>").append("<td class=\"total type\">").append("Total").append("</td>");
      for (String language : languagesForTable) {
        if (null != globalLanguageCount.get(language)) sb.append("<td class=\"total\">").append(globalLanguageCount.get(language)).append("</td>");
      }
      sb.append("</tr>");
      sb.append("</table>");
      value = sb.toString();

      if (manifestationsPerTypePerLanguage.isEmpty()) value = "None";
    %>
    <%=value%>
</span></div>