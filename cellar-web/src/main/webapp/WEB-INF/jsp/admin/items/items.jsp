<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="app" tagdir="/WEB-INF/tags" %>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/admin/items/js/items.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/admin/items/css/items.css" type="text/css"/>

<div class="dashboard" id="items">

    <!-- Title -->
    <div class="title">
        <spring:message code="items.title"/>
    </div>

    <!-- Search form -->
    <form:form method="POST" action="${base}/admin/items/search" modelAttribute="search_form" cssClass="search">
        <form:label path="identifier" for="identifier">
            <fmt:message key="items.search.identifier.label"/>
            <fmt:message key="items.search.identifier.placeholder" var="placeholder"/>
            <form:input id="identifier" path="identifier" placeholder="${placeholder}" style="min-width: 350px" autofocus="autofocus"/>
        </form:label>

        <form:label path="filterIdenticalSuggestion" for="filterIdenticalSuggestion">
            <fmt:message key="items.search.filter.label"/>
            <form:checkbox id="filterIdenticalSuggestion" path="filterIdenticalSuggestion" />
        </form:label>

        <form:input id="page" path="page" value="${search_form.page}" type="hidden" />

        <input type="submit" value="<fmt:message key="items.search.button" />"/>
    </form:form>

    <!-- Update form -->
    <c:if test="${filename_form != null}">
        <form:form method="POST" action="${base}/admin/items/save" modelAttribute="filename_form">

            <!-- Table -->
            <table>
                <!-- Header -->
                <tr>
                    <th><fmt:message key="items.table.psi"/></th>
                    <th><fmt:message key="items.table.identifiers"/></th>
                    <th><fmt:message key="items.table.filename"/></th>
                    <th><fmt:message key="items.table.suggestion"/></th>
                </tr>

                <!-- Rows -->
                <c:forEach items="${filename_form.filenames}" var="filename" varStatus="status">
                    <tr>
                        <td>
                            <button id="plusButton" data-id="psi-${filename.cellarId}">+</button>
                        </td>
                        <td>
                            <span style="white-space: nowrap;">
                                <a target="_blank" href=<c:url value="/admin/search/content?cellarId=${filename.cellarId}"/>>
                                    ${filename.cellarId}
                                </a>
                            </span>
                            <ul id="psi-${filename.cellarId}" class="psi-hidden">
                                <c:forEach items="${filename.pids}" var="pid">
                                    <li>${pid}</li>
                                </c:forEach>
                            </ul>
                        </td>
                        <td>
                            <form:input id="filename-${filename.cellarId}"
                                        type="text"
                                        path="filenames[${status.index}].filename"
                                        value="${filename.filename}"
                            />
                        </td>
                        <td align="right">
                                ${filename.suggestedFilename}
                            <button id="useButton" class="replace-btn" data-file-id="filename-${filename.cellarId}" data-suggested-file-id="${filename.suggestedFilename}">
                                <fmt:message key="items.table.use-filename"/>
                            </button>
                        </td>
                    </tr>

                    <form:input type="hidden" path="filenames[${status.index}].cellarId" value="${filename.cellarId}"/>
                    <form:input type="hidden" path="filenames[${status.index}].pids" value="${filename.pidsStr}"/>
                    <form:input type="hidden" path="filenames[${status.index}].existingFilename"
                                value="${filename.existingFilename}"/>
                    <form:input type="hidden" path="filenames[${status.index}].suggestedFilename"
                                value="${filename.suggestedFilename}"/>
                    <form:input type="hidden" path="filenames[${status.index}].version" value="${filename.version}"/>
                </c:forEach>
            </table>

            <!-- Pagination -->
            <div style="float: right;">
                <c:if test="${search_form.page > 1}">
                    <button type="button" id="previousButton" data-current-page="${search_form.page}">
                        <fmt:message key="items.message.previous" />
                    </button>
                </c:if>
                <c:if test="${filename_form.filenames.size() >= 25}">
                    <button type="button" id="nextButton" data-current-page="${search_form.page}">
                        <fmt:message key="items.message.next" />
                    </button>
                </c:if>
            </div>

            <!-- Submit -->
            <button type="button" id="replaceAllButton">
                    <fmt:message key="items.table.replace-all" />
            </button>
            <input type="submit" style="background: #92C1F0" value="<fmt:message key="items.table.save-all" />"/>

        </form:form>
    </c:if>
</div>
