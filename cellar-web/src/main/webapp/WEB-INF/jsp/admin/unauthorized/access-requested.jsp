<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<div style="text-align:center">
<p>
	<fmt:message key="user.access.request.select.action.unauthorized.access-submited" />
</p>
<c:if test="${cellarConfiguration.cellarServiceUserAccessNotificationEmailEnabled eq 'true'}">
	<p>
		<fmt:message key="user.access.request.select.action.unauthorized.resend-notification" />
	</p>
	<form id="sendEmailForm" method="post" action="${pageContext.request.contextPath}/admin/unauthorized/access-requested">
	    <button id="sendEmailBtn" type="submit"><fmt:message key="user.access.request.select.action.unauthorized.send-email"/></button>
	<form>
</c:if>
</div>