<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<div>
	<p><span><b><fmt:message key="user.migration.request.credentials.description"/></b></span></p>
	<form:form id="userMigrationCredentialsProvisionForm" method="POST" modelAttribute="userMigrationCredentialsCommand">
		<fieldset>
			<ul>
				<li>
					<span>
						<label for="username"><fmt:message key="user.migration.request.credentials.input.username"/></label>
						<form:input id="usernameInput" name="username" type="text" path="username"/>
					</span>
				</li>
				<li style="padding-top:3px">
					<span>
						<label for="password" style="padding-right:4px"><fmt:message key="user.migration.request.credentials.input.password"/></label>
						<form:password id="passwordInput" name="password" path="password"/>
					</span>
				</li>
			</ul>
			<br>
			<span style="margin-left:162px"><form:button id="userMigrationSubmitBtn" type="submit"><fmt:message key="user.migration.request.credentials.button.submit"/></form:button></span>
		</fieldset>
	</form:form>
	<br>
	<span><a href="<c:url value="/admin/unauthorized/select-action"/>"><fmt:message key="user.migration.back"/></a></span>
</div>