<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="display" uri="http://displaytag.sf.net" %>

<div>
	<p>
		<span>
			<b><fmt:message key="user.access.request.select.action.unauthorized.message"/></b>&nbsp<fmt:message key="user.access.request.select.action.unauthorized.choose"/>
		</span>
	</p>
	<br>
	<form id="userAccessRequestCreationForm" method="POST">
		<fieldset>
			<fmt:message key="user.access.request.select.action.unauthorized.request-access.description"/>
			<c:if test="${cellarConfiguration.cellarServiceUserAccessNotificationEmailEnabled}">
				<span>
					(<i style="padding-left:3px"><fmt:message key="user.access.request.select.action.unauthorized.send-notification-email"/></i>
					<input id="sendNotificationEmailCheckbox" type="checkbox" name="sendNotificationEmail" style="vertical-align:middle;margin-right:5px"/>)
				</span>
			</c:if>
			<button id="submitBtn" type="submit" style="margin-left:10px"><fmt:message key="user.access.request.select.action.unauthorized.request-access.btn"/></button>
		</fieldset>
	</form>
	<c:if test="${cellarConfiguration.cellarServiceEuloginUserMigrationEnabled}">
		<br>
		<form id="userMigrationLinkForm" method="GET" action="${pageContext.request.contextPath}/admin/unauthorized/migrate">
			<fieldset>
				<span>
					<fmt:message key="user.access.request.select.action.unauthorized.migrate.description"/>
					<button id="migrateBtn" type="submit" style="margin-left:10px"><fmt:message key="user.access.request.select.action.unauthorized.migrate.btn"/></button>
				</span>
			</fieldset>
		</form>
	</c:if>
</div>