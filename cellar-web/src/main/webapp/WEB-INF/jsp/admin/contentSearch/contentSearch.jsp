<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<div id="searchDiv" class="form">
    <form id="searchForm" method="get" action="${base}/admin/contentSearch/getTreeNotice" onsubmit="transformCheckboxValue('excludeInferred')">
        <fieldset>
            <table class="items">
                <tr>
                    <td><span id="contentTypeLabel">Type of content:</span></td>
                    <td>
                        <select id="contentType" name="contentType">
                            <option value="getTreeNotice">Tree notice</option>
                            <option value="getBranchNotice">Branch notice</option>
                            <option value="getObjectWorkNotice">Object-Work notice</option>
                            <option value="getObjectExpressionNotice">Object-Expression notice</option>
                            <option value="getObjectManifestationNotice">Object-Manifestation notice</option>
                            <option value="getRDF">RDF-Object notice</option>
                            <option value="getRDFTree">RDF-Tree notice</option>
                            <option value="getContentStream">Content stream</option>
                        </select>
                        <img id="contentTypeHelpImg" src="${base}/resources/common/admin/images/help-icon.jpg" style="vertical-align:middle" />
                    </td>
                </tr>
                <tr>
                    <td><span id="decodingLanguageLabel">Decoding language:</span></td>
                    <td>
                        <select id="decodingLanguage" name="decodingLanguage">
                            <option value="">-</option>
                            <c:forEach items="${languages}" var="language">
                                <option value="${language.language}">${language.displayLanguage}</option>
                            </c:forEach>
                        </select>
                        <img id="decodingLanguageHelpImg" src="${base}/resources/common/admin/images/help-icon.jpg" style="vertical-align:middle" />
                    </td>
                </tr>
                <tr>
                    <td><span id="acceptLanguageLabel">Accept language:</span></td>
                    <td>
                        <select id="acceptLanguage" name="acceptLanguage">
                            <option value="">-</option>
                            <c:forEach items="${languages}" var="language">
                                <option value="${language.language}">${language.displayLanguage}</option>
                            </c:forEach>
                        </select>
                        <img id="acceptLanguageHelpImg" src="${base}/resources/common/admin/images/help-icon.jpg" style="vertical-align:middle" />
                    </td>
                </tr>
                <tr>
                    <td><span id="acceptFormatLabel">Accept format:</span></td>
                    <td>
                        <select id="acceptFormat" name="acceptFormat">
                            <option value="">-</option>
                            <c:forEach items="${acceptFormats}" var="acceptFormat">
                                <option value="${acceptFormat}">${acceptFormat}</option>
                            </c:forEach>
                        </select>
                        <img id="acceptFormatHelpImg" src="${base}/resources/common/admin/images/help-icon.jpg" style="vertical-align:middle" />
                    </td>
                </tr>
                <tr>
                    <td><span id="pidLabel" ></span></td>
                    <td>
                        <input id="pid" name="pid" type="text" size="100" autofocus />
                        <img id="pidHelpImg" src="${base}/resources/common/admin/images/help-icon.jpg" style="vertical-align:middle" />
                    </td>
                </tr>
                <tr>
                    <td><span id="excludeInferredLabel">Exclude inferred triples:</span></td>
                    <td>
                        <input id="excludeInferred" name="excludeInferred" type="checkbox" <c:if test="${excludeInferred}">checked</c:if> />
                        <img id="excludeInferredHelpImg" src="${base}/resources/common/admin/images/help-icon.jpg" style="vertical-align:middle" />
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td><input id="searchButton" type="submit" value="Search" /></td>
                </tr>
            </table>
        </fieldset>
    </form>
</div>