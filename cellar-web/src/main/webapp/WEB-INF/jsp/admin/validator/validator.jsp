<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<div class="form">
	<h3>System validators</h3>
	<fieldset>
		<table class="configuration">
			<thead>
				<tr>
					<th>Classname</th>
					<th>Activate?</th>
				</tr>
			</thead>
			<tbody>
			<c:forEach items="${systemValidators}" var="validator">
				<form:form modelAttribute="validator" id="form_sv_${validator.id}">
					<input type="hidden" value="${validator.id}" name="id" />
					<input type="hidden" value="${validator.type}" name="type" />
					<input type="hidden" value="${validator.key}" name="key" />
					<input type="hidden" value="${validator.validatorClass}" name="validatorClass" />
					<input type="hidden" value="${validator.enabled}" name="enabled" id="sv_enabled_${validator.id}" />
					<tr>
						<td id="vc_${validator.id}"><i>${validator.validatorClass}</i></td>
						<td>
							<a id="sv_a_${validator.id}" href="#" data-id="${validator.id}">
								<c:choose>
									<c:when test="${validator.enabled}"><img class="noborder" id="sv_img_${validator.id}" src="${base}/resources/admin/validator/img/16_button_checked.png" /></c:when>
									<c:otherwise><img class="noborder" id="sv_img_${validator.id}" src="${base}/resources/admin/validator/img/16_button_remove.png" /></c:otherwise>
								</c:choose>
								
							</a>
						</td>
					</tr>
				</form:form>
			</c:forEach>
			</tbody>		
		</table>	
	</fieldset>

</div>