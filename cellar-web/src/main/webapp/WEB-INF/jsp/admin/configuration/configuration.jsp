<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<div class="form">
	<c:choose>
		<c:when test="${warningMessage != null}">
            <div style="color: red;">${warningMessage}</div>
		</c:when>
		<c:otherwise>
			<form:form method="POST" modelAttribute="configObject" action="${base}/admin/configuration/update">
                <fieldset>
		            <table class="items">
		                <c:forEach items="${configObject.items}" varStatus="vs">
		                    <tr>
		                        <spring:bind path="configObject.items[${vs.index}].propertyKey">
		                            <td>${status.value}</td>
		                        </spring:bind>
		                        <spring:bind path="configObject.items[${vs.index}].propertyValue">
		                            <td><input type="text" value="${status.value}"
		                                name="${status.expression}" size="90%" /></td>
		                        </spring:bind>
		                    </tr>
		                </c:forEach>
		                <tr>
		                    <td></td>
		                    <td><input type="submit" value="Save" /></td>
		                </tr>
		            </table>
		        </fieldset>
	        </form:form>
		</c:otherwise>
    </c:choose>
</div>