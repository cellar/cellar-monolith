<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/admin/archive/css/archive.css"
	  type="text/css"/>

<!-- TODO retrieve JSON with ajax call-->

<div class="dashboard">

	<div id="tabs">

		<ul id="menu">
			<li><a href="#tabs-1">Global information</a></li>
		</ul>

		<div id="tabs-1">
			<fieldset>
				<legend>Archivist</legend>
				<div>
					<ul>
						<li><span>status :</span>
							<c:choose>
								<c:when test="${archivistStatus.status == 'UP'}">
									<img src="${base}/resources/common/admin/images/enabled.png"/>
								</c:when>
								<c:otherwise>
									<img src="${base}/resources/common/admin/images/disabled.png"/>
								</c:otherwise>
							</c:choose>
						</li>
						<li><span>ping status :</span>
							<c:choose>
								<c:when test="${archivistStatus.pingStatus == 'UP'}">
									<img src="${base}/resources/common/admin/images/enabled.png"/>
								</c:when>
								<c:otherwise>
									<img src="${base}/resources/common/admin/images/disabled.png"/>
								</c:otherwise>
							</c:choose>
						</li>
						<li><span>refresh scope :</span>
							<c:choose>
								<c:when test="${archivistStatus.refreshScope == 'UP'}">
									<img src="${base}/resources/common/admin/images/enabled.png"/>
								</c:when>
								<c:otherwise>
									<img src="${base}/resources/common/admin/images/disabled.png"/>
								</c:otherwise>
							</c:choose>
						</li>
					</ul>
				</div>
			</fieldset>

			<fieldset>
				<legend>Archivist db</legend>
				<div>
					<ul>
						<li><span>status :</span>
							<c:choose>
							<c:when test="${archivistStatus.databaseStatus == 'UP'}">
							<img src="${base}/resources/common/admin/images/enabled.png"/>
							</c:when>
							<c:otherwise>
							<img src="${base}/resources/common/admin/images/disabled.png"/>
							</c:otherwise>
							</c:choose>
						<li><span>vendor :</span>${archivistStatus.database}</li>
						<li><span>result :</span>${archivistStatus.result}</li>
						<li><span>validation query :</span>${archivistStatus.validationQuery}</li>
					</ul>
				</div>
			</fieldset>

			<fieldset>
				<legend>Disk space</legend>
				<div>
					<ul>
						<li><span>status :</span>
							<c:choose>
								<c:when test="${archivistStatus.diskSpaceStatus == 'UP'}">
									<img src="${base}/resources/common/admin/images/enabled.png"/>
								</c:when>
								<c:otherwise>
									<img src="${base}/resources/common/admin/images/disabled.png"/>
								</c:otherwise>
							</c:choose>
						</li>
						<li><span>total :</span>${archivistStatus.total}</li>
						<li><span>free :</span>${archivistStatus.free}</li>
						<li><span>threshold :</span>${archivistStatus.threshold}</li>
					</ul>
				</div>
			</fieldset>
		</div>
	</div>
</div>