<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<%@ page import="org.apache.commons.lang.StringUtils" %>
<%@ taglib prefix="stringutils" uri="stringutils" %>


<script type="text/javascript" src="${pageContext.request.contextPath}/resources/common/js/jquery.cookie.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/common/js/jquery.datatables.1.13.1.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/admin/threads/js/threads.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/admin/threads/css/threads.css" type="text/css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/common/css/jquery.dataTables.min.css" type="text/css"/>



    
<div id="threadsInfo">


 <div id="tabs-container">
    <ul class="tabs-menu">
        <li class="current"><a href="#tab-1"><spring:message code="threads.view.ingestion.tab.title"/></a></li>
        <li class=""><a href="#tab-2"><spring:message code="threads.view.indexation.tab.title"/></a></li>
    </ul>
    <div class="tab">
        <div id="tab-1" class="tab-content" style="">
          <c:if test="${isIngestionRunning==true}">
            <form:form method="POST" action="threads" id="threadsViewForm" modelAttribute="threadsViewForm" >
                <input id="ingestionActive" name="ingestionActive" type="hidden" value="${false}"/>
			    <input type="submit" id="submitButton" value="<fmt:message key="threads.view.ingestion.tab.button.stop" />"/>
		    </form:form>
          </c:if>
          <c:if test="${isIngestionRunning == false}">
            <form:form method="POST" action="threads" id="threadsViewForm" modelAttribute="threadsViewForm" >
                <input id="ingestionActive" name="ingestionActive" type="hidden" value="${true}"/>
                <input type="submit" id="submitButton" value="<fmt:message key="threads.view.ingestion.tab.button.start" />"/>
            </form:form>
          </c:if>
      <br><br>
      
          

  <div >
    <div class="title"><spring:message code="threads.view.ingestion.tab.table.processing.title"/></div>
    <table class="processing_ingestion_datatable">
      <thead>
      <tr>
        <th><spring:message code="threads.view.ingestion.tab.table.processing.column.name"/></th>
        <th><spring:message code="threads.view.ingestion.tab.table.processing.column.start"/></th>     
        <th><spring:message code="threads.view.ingestion.tab.table.processing.column.priority"/></th>
        <th><spring:message code="threads.view.ingestion.tab.table.processing.column.status"/></th>
      </tr>
      </thead>
      <tbody>
      <c:forEach var="sipPackage" varStatus="status" items="${ingestionProcessingQueue}">
        <tr class="single-nal">
            <td class="tooltip">${sipPackage.name}
	            <span>
		            <strong>${sipPackage.name}</strong>
		            <br><spring:message code="threads.view.ingestion.sipwork.creationdate"/>${sipPackage.detectionDate}
		             <br><spring:message code="threads.view.ingestion.sipwork.startdate"/>${sipPackage.startDate}
	            </span>
            </td>
            <td><fmt:formatDate value="${sipPackage.startDate}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
            <td>${sipPackage.type}</td>
            <td > 
              <img style="margin-left:10px;" src="${base}/resources/admin/threads/images/loading.gif" alt="<spring:message code="threads.view.state.ongoing"/>" title="<spring:message code="threads.view.state.ongoing"/>">
            </td>
        </tr>
      </c:forEach>
      </tbody>
    </table>
  </div>

    <div >
    <div class="title"><spring:message code="threads.view.ingestion.tab.table.waiting.title"/></div>
    <table class="waiting_ingestion_datatable">
      <thead>
      <tr>
        <th><spring:message code="threads.view.ingestion.tab.table.waiting.column.name"/></th>
        <th><spring:message code="threads.view.ingestion.tab.table.waiting.column.waiting"/></th>     
        <th><spring:message code="threads.view.ingestion.tab.table.waiting.column.priority"/></th>
      </tr>
      </thead>
      <tbody>
      <c:forEach var="sipPackage" varStatus="status" items="${ingestionWaitingQueue}">
        <tr class="single-nal">
            <td class="tooltip">${sipPackage.name}
                <span>
                    <strong>${sipPackage.name}</strong>
                    <br><spring:message code="threads.view.ingestion.sipwork.creationdate"/>${sipPackage.detectionDate}
                </span>
            </td>
            
            <td><fmt:formatDate value="${sipPackage.detectionDate}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
            <td>${sipPackage.type}</td>
        </tr>
      </c:forEach>
      </tbody>
    </table>
  </div>
  </div><!-- end of ingestion tab  -->
  
  
  
        
        <div id="tab-2" class="tab-content" style="display: none;">
          <c:if test="${isIndexationRunning==true}">
            <form:form method="POST" action="threads" id="threadsViewForm" modelAttribute="threadsViewForm" >
                <input id="indexationActive" name="indexationActive" type="hidden" value="${false}"/>
                <input type="submit" id="submitButton" value="<fmt:message key="threads.view.indexation.tab.button.stop" />"/>
            </form:form>
          </c:if>
          <c:if test="${isIndexationRunning == false}">
            <form:form method="POST" action="threads" id="threadsViewForm" modelAttribute="threadsViewForm" >
                <input id="indexationActive" name="indexationActive" type="hidden" value="${true}"/>
                <input type="submit" id="submitButton" value="<fmt:message key="threads.view.indexation.tab.button.start" />"/>
            </form:form>
          </c:if>
      <br><br>
      
          

  <div >
    <div class="title"><spring:message code="threads.view.indexation.tab.table.processing.title"/></div>
    <table class="processing_indexation_datatable">
      <thead>
      <tr>
        <th><spring:message code="threads.view.indexation.tab.table.processing.column.name"/></th>
        <th><spring:message code="threads.view.indexation.tab.table.processing.column.start"/></th>     
        <th><spring:message code="threads.view.indexation.tab.table.processing.column.priority"/></th>
        <th><spring:message code="threads.view.indexation.tab.table.processing.column.status"/></th>
      </tr>
      </thead>
      <tbody>
      <c:forEach var="cmrIndexRequest" varStatus="status" items="${indexationProcessingQueue}">
        <tr class="single-nal">
            <td class="tooltip">${cmrIndexRequest.groupByUri}
                <span>
                    <strong>${cmrIndexRequest.groupByUri}</strong>
                    <br>
                </span>
            </td>
            <td><fmt:formatDate value="${cmrIndexRequest.executionStartDate}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
            <td>${cmrIndexRequest.priority}</td>
            <td > 
              <img style="margin-left:10px;" src="${base}/resources/admin/threads/images/loading.gif" alt="<spring:message code="threads.view.state.ongoing"/>" title="<spring:message code="threads.view.state.ongoing"/>">
            </td>
        </tr>
      </c:forEach>
      </tbody>
    </table>
  </div>

    <div >
    <div class="title"><spring:message code="threads.view.indexation.tab.table.waiting.title"/></div>
    <table class="waiting_indexation_datatable">
      <thead>
      <tr>
        <th><spring:message code="threads.view.indexation.tab.table.waiting.column.name"/></th>
        <th><spring:message code="threads.view.indexation.tab.table.waiting.column.waiting"/></th>     
        <th><spring:message code="threads.view.indexation.tab.table.waiting.column.priority"/></th>
      </tr>
      </thead>
      <tbody>
      <c:forEach var="cmrIndexRequest" varStatus="status" items="${indexationWaitingQueue}">
        <tr class="single-nal">
            <td class="tooltip">${cmrIndexRequest.groupByUri}
                <span>
                    <strong>${cmrIndexRequest.groupByUri}</strong>
                    <br>
                </span>
            </td>
            
            <td><fmt:formatDate value="${cmrIndexRequest.createdOn}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
            <td>${cmrIndexRequest.priority}</td>
        </tr>
      </c:forEach>
      </tbody>
    </table>
  </div>
  </div><!-- end of indexation tab -->
        
    </div>
</div>
</div>

