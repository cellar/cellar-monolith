<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/admin/licenseHolder/js/jobAdd.js"></script>
<div id="jobAdd" class="jobAdd">
	
	<c:if test="${not empty errorMessage}">
	    <div class="status failed">${errorMessage}</div>
	  </c:if>
	  
	<form id="addForm" action="${pageContext.request.contextPath}/admin/license/add/job/new" method="POST">
	<table class="items">
	    <tr>
	        <td><label for="jobName">Job name</label></td>
	        <td><input id="jobName" type="text" name="jobName" size="100%" class="required" /></td>
	    </tr>
	    <tr>
	        <td><label for="language">Language</label></td>
	        <td>
	        	<select class="changeTarget" name="language">
	        		<c:forEach var="language" items="${languages}">
	        			<option value="${language.isoCode.isoCodeThreeChar}">
	        				${language.code}
	        			</option>
	        		</c:forEach>
	        	</select>
	        </td>
	    </tr>
	    <tr>
	        <td><label for="frequency">Frequency</label></td>
	        <td>
	        	<select class="changeTarget" name="frequency">
	        		<c:forEach var="frequency" items="${frequencies}">
	        			<option value="${frequency}">
							<spring:message code="licenseHolder.frequency.${frequency}"/>
	        			</option>
	        		</c:forEach>
	        	</select>
	        </td>
	    </tr> 
	    <tr>
	        <td><label for="pathOverview">Path</label></td>
	        <td>
	        	<span id="pathOverview"></span> <input class="changeTarget required" id="path" type="text" name="path" size="25%" /> <img id="pathOverviewHelpImg" class="tooltipHelper" src="${base}/resources/common/admin/images/help-icon.jpg" style="vertical-align:middle" title="This is the path in the configured folder (${extractionsFolder}) for extraction jobs."/>
	        </td>
	    </tr>
	    <tr>
	        <td><label for="archiveName">Archive name</label></td>
	        <td>
	        	<input id="archiveName" type="text" name="archiveName" size="100%" readonly="readonly" />
	        </td>
	    </tr>
	    <tr>
	    	<td><label for="startDate">Job start date</label></td>
	    	<td>
				<input id="startDate" type="text" class="datepicker" name="startDate" /> <img id="startDateHelpImg" class="tooltipHelper" src="${base}/resources/common/admin/images/help-icon.jpg" style="vertical-align:middle" title='The start date cannot precede or equal: 
					<c:forEach var="frequency" items="${frequencies}" varStatus="status">
						 <br /><spring:message code="licenseHolder.frequency.${frequency}"/> <fmt:formatDate value="${deadlines[status.index]}" pattern="dd/MM/yyyy" />
	        		</c:forEach>'/>
	    	</td>
	    </tr>
	    <tr>
	        <td><label for="sparqlQuery">SPARQL query</label></td>
	        <td>
	        	The query may contain two placeholders (<b>\${start_date}</b> and <b>\${end_date}</b>) in order to define the range of documents to extract.<br />
	        	The placeholders may define the date format to use at execution. E.g.: "\${start_date "yyyy-MM-dd"}<br />
	        	<textarea id="sparqlQuery" name="sparqlQuery" cols="100%" rows=4 draggable="false" class="required" >
prefix cdm: &lt;http://publications.europa.eu/ontology/cdm#&gt;
prefix xsd: &lt;http://www.w3.org/2001/XMLSchema#&gt;
SELECT ?uri  
WHERE { ?uri cdm:work_date_creation_legacy "\${start_date "yyyy-MM-dd"}"^^xsd:date } 
	        	</textarea>
	        </td>
	    </tr>
	</table>
    <div style="text-align:center;">
   		<input id="submitButton" type="submit" value="Submit"/>
		<input id="cancelButton" type="button" value="Cancel" data-url="${pageContext.request.contextPath}/admin/license/list" />
	</div>
	</form>
</div>