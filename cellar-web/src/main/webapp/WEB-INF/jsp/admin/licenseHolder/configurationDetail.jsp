<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/admin/licenseHolder/js/configurationDetail.js"></script>

<div class="configurationDetail">
	  
	<c:if test="${not empty errorMessage}">
		<div class="status failed">${errorMessage}</div>
	</c:if>
	
	<table class="items">
		<tr>
	 		<td>#</td>
	     	<td><input id="configurationId" type="text" class="input" name="configurationId" size="100%" readonly="readonly" value="${configuration.id}"/></td>
		</tr>
		<tr>
	 		<td>Extraction name</td>
	     	<td><input id="configurationName" type="text" class="input" name="configurationName" size="100%" readonly="readonly" value="${configuration.configurationName}"/></td>
		</tr>
		<tr>
	 		<td>Extraction type</td>
	     	<td><input id="extractionType" type="text" class="input" name="extractionType" size="100%" readonly="readonly" value="<spring:message code="licenseHolder.frequency.${configuration.configurationType}"/>"/></td>
		</tr>
		<tr>
	 		<td>Extraction language</td>
	     	<td><input id="extractionLanguage" type="text" class="input" name="extractionLanguage" size="100%" readonly="readonly" value="${configuration.languageBean.code}"/></td>
		</tr>
		<tr>
	 		<td>Path</td>
	     	<td><input id="path" type="text" class="input" name="path" size="100%" readonly="readonly" value="${configuration.path}"/></td>
		</tr>
		<tr>
	 		<td>Creation date</td>
	     	<td><input id="creationDate" type="text" class="input" name="creationDate" size="100%" readonly="readonly" value="<fmt:formatDate value="${configuration.creationDate}" pattern="dd/MM/yyyy HH:mm" />"/></td>
		</tr>
		<c:if test="${configuration.startDate != null}">
			<tr>
		 		<td>Start date</td>
		     	<td><input id="startDate" type="text" class="input" name="startDate" size="100%" readonly="readonly" value="<fmt:formatDate value="${configuration.startDate}" pattern="dd/MM/yyyy" />"/></td>
			</tr>
		</c:if>
	  	<tr>
	      	<td>SPARQL query</td>
	      	<td>
	      		<textarea id="sparqlQuery" name="sparqlQuery" cols="100%" rows=4 readonly="readonly">${configuration.sparqlQuery}</textarea>
	      	</td>
	  	</tr>
	  	<tr>
	  		<td>Executions</td>
	  		<td>
		  		<table>
					<thead>
						<tr>
							<th>#</th>
							<th>Status</th>
							<th>Execution date</th>
							<th>Start date</th>
							<th>End date</th>
							<th>Nb documents</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${executions}" var="execution" varStatus="status">
							<tr class="executions">
								<td class="executionId"><a href=<c:url value="/admin/license/execution?executionId=${execution[0].id}"/>>${execution[0].id}</a></td>
								<td class="executionStatus"><spring:message code="licenseHolder.execution.status.${execution[0].executionStatus}"/></td>
								<td class="executionDate"><fmt:formatDate value="${execution[0].effectiveExecutionDate}" pattern="dd/MM/yyyy HH:mm" /></td>
								<td class="startDate"><fmt:formatDate value="${execution[0].startDate}" pattern="dd/MM/yyyy HH:mm" /></td>
								<td class="endDate"><fmt:formatDate value="${execution[0].endDate}" pattern="dd/MM/yyyy HH:mm" /></td>
								<td class="numberOfDocuments"><c:if test="${execution[0].executionStatus!='NEW' && execution[0].executionStatus!='PENDING'}"><span<c:if test="${execution[1]==0}"> style="color:red"</c:if>>${execution[1]}</span></c:if></td>
								<td class="actions">
									<c:if test="${executionsOptions[status.index][0]}">
										<a href=<c:url value="/admin/license/execution/restartStep?executionId=${execution[0].id}&configurationId=${execution[0].extractionConfiguration.id}"/>>Restart step</a><br />
									</c:if>
									<c:if test="${executionsOptions[status.index][1]}">
										<a href=<c:url value="/admin/license/execution/restart?executionId=${execution[0].id}&configurationId=${execution[0].extractionConfiguration.id}"/>>Restart</a>
									</c:if>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
	  		</td>
	  	</tr>
	</table>
	<div style="text-align:center;">
		<input id="backButton" type="button" value="Back" data-url="${pageContext.request.contextPath}/admin/license/list" />
	</div>
</div>