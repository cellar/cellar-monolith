<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/admin/licenseHolder/js/executionDetail.js"></script>
<div class="executionDetail">
	  
	<c:if test="${not empty errorMessage}">
		<div class="status failed">${errorMessage}</div>
	</c:if>
	
	<table class="items">
		<tr>
	 		<td>#</td>
	     	<td><input id="executionId" type="text" class="input" name="executionId" size="100%" readonly="readonly" value="${execution.id}"/></td>
		</tr>
		<tr>
	 		<td>Extraction status</td>
	     	<td><input id="executionStatus" type="text" class="input" name="executionStatus" size="100%" readonly="readonly" value="<spring:message code="licenseHolder.execution.status.${execution.executionStatus}"/>"/></td>
		</tr>
		<tr>
	 		<td>Execution date</td>
	     	<td><input id="executionDate" type="text" class="input" name="executionDate" size="100%" readonly="readonly" value="${execution.effectiveExecutionDate}"/></td>
		</tr>
		<tr>
	 		<td>Start date</td>
	     	<td><input id="startDate" type="text" class="input" name="startDate" size="100%" readonly="readonly" value="${execution.startDate}"/></td>
		</tr>
		<tr>
	 		<td>End date</td>
	     	<td><input id="endDate" type="text" class="input" name="endDate" size="100%" readonly="readonly" value="${execution.endDate}"/></td>
		</tr>
		<c:if test="${execution.errorDescription != null}">
			<tr>
		 		<td>Error</td>
		     	<td>
		     		<a href="#" id="dialogOpener">${execution.errorDescription}</a>
				  	<div id="dialog" title="Error stack trace">
				   		<p>${execution.errorStacktrace}</p>
					</div>
		     	</td>
			</tr>
		</c:if>
		<tr>
	 		<td>SPARQL temp file path</td>
	     	<td><input id="sparqlTempFilePath" type="text" class="input" name="sparqlTempFilePath" size="100%" readonly="readonly" style="width:100%" value="${sparqlTempFilePath}"/></td>
		</tr>
		<tr>
	 		<td>Archive temp directory path</td>
	     	<td><input id="archiveTempDirectoryPath" type="text" class="input" name="archiveTempDirectoryPath" size="100%" readonly="readonly" style="width:100%" value="${archiveTempDirectoryPath}"/></td>
		</tr>
		<tr>
	 		<td>Archive file path</td>
	     	<td><input id="archiveFilePath" type="text" class="input" name="archiveFilePath" size="100%" readonly="readonly" style="width:100%" value="${archiveFilePath}"/></td>
		</tr>
		<tr>
			<td>Executed on cellar instance id</td>
			<td><input id="instanceId" type="text" class="input" name="instanceId" size="100%" readonly="readonly" value="${execution.instanceId}"/></td>
		</tr>
		<tr>
			<td>Documents</td>
			<td>
				<display:table name="workIdentifiersList" sort="external" partialList="true" size="${workIdentifiersList.fullListSize}" pagesize="${workIdentifiersList.objectsPerPage}" requestURI="" defaultsort="1" defaultorder="descending">
					<display:column property="workId" title="Work id" sortable="true" sortName="workId" headerClass="sortable" />
				</display:table>
				<form id="filterForm" method="GET" action="${pageContext.request.contextPath}/admin/license/execution">
		        	<fieldset>
		            	<span id="filterLabel">Filter </span>
		            	<input id="executionId" name="executionId" type="hidden" value="${execution.id}" />
		            	<input id="workFilter" name="workFilter" class="tooltipHelper" title="The wildcards '*' (any one or more character(s)) and '?' (any character) are supported." type="text" size="25" value="<c:if test="${not empty workFilter}">${workFilter}</c:if>"/>
		            	<input id="filterButton" type="submit" value="OK" />
		        	</fieldset>
	    		</form>
			</td>
		</tr>
	</table>
	<div style="text-align:center;">
		<input id="backButton" type="button" value="Back" data-url="${pageContext.request.contextPath}/admin/license/configuration?configurationId=" data-id="${execution.extractionConfiguration.id}" />
	</div>
</div>