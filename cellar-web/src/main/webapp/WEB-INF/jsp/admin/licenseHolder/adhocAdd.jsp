<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/common/admin/css/jquery.ui.timepicker.css" type="text/css" />
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/admin/licenseHolder/js/adhocAdd.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/common/js/jquery.ui.timepicker.js"></script>
<div id="jobAdd" class="jobAdd">
	
	<c:if test="${not empty errorMessage}">
	    <div class="status failed">${errorMessage}</div>
	  </c:if>
	  
	<form id="addForm" action="${pageContext.request.contextPath}/admin/license/add/adhoc/new" method="POST">
	<table class="items">
	    <tr>
	        <td>Extraction name</td>
	        <td><input id="jobName" type="text" name="jobName" size="100%" class="required" /></td>
	    </tr>
	    <tr>
	        <td>Language</td>
	        <td>
	        	<select class="changeTarget" name="language">
	        		<c:forEach var="language" items="${languages}">
	        			<option value="${language.isoCode.isoCodeThreeChar}">
	        				${language.code}
	        			</option>
	        		</c:forEach>
	        	</select>
	        </td>
	    </tr>
	    <tr>
	        <td>Path</td>
	        <td>
	        	<input id="path" type="text" name="path" size="40%" class="required" /> <img id="pathOverviewHelpImg" class="tooltipHelper" src="${base}/resources/common/admin/images/help-icon.jpg" style="vertical-align:middle" title="This is the path in the configured folder (${extractionsFolder}) for the ad-hoc extractions."/>
	        </td>
	    </tr>
	    <tr>
	        <td>Archive name</td>
	        <td>
	        	<input id="archiveName" type="text" name="archiveName" size="100%" readonly="readonly" />
	        </td>
	    </tr>
	    <tr>
	    	<td><label for="startDate">Execution start date</label></td>
	    	<td>
				<input id="startDate" type="text" class="datepicker" name="startDate" />
				<input id="startTime" type="text" name="startTime" style="width: 70px;" /> <img id="startDateHelpImg" class="tooltipHelper" src="${base}/resources/common/admin/images/help-icon.jpg" style="vertical-align:middle" title="If the date picker is empty, the execution is immediately started."/>
	    	</td>
	    </tr>
	    <tr>
	        <td>SPARQL query</td>
	        <td>
	        	<textarea id="sparqlQuery" name="sparqlQuery" cols="100%" rows=4 draggable="false" class="required" >SELECT ?uri ?y ?z WHERE { ?uri ?y ?z }</textarea>
	        </td>
	    </tr>
	</table>
   	<div style="text-align:center;">
   		<input id="submitButton" type="submit" value="Submit"/>
		<input id="cancelButton" type="button" value="Cancel" data-url="${pageContext.request.contextPath}/admin/license/list"/>
	</div>
	</form>
</div>