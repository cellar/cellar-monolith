<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/admin/licenseHolder/js/licenseList.js"></script>
<div class="licenseList">
	<div style="text-align:right;">
		Restart failed: 
		<a class="restartDialogOpener" href=<c:url value="/admin/license/execution/restart/all"/> id="restartDialogOpener">executions</a> | 
		<a class="restartStepDialogOpener" href=<c:url value="/admin/license/execution/restartStep/all"/> id="restartStepDialogOpener">steps</a>
	</div>	  
	<c:if test="${not empty errorMessage}">
		<div class="status failed">${errorMessage}</div>
	</c:if>
	
	<div class="subtitle">Extraction jobs</div>
	
	<table>
		<thead>
			<tr>
				<th>Extraction name</th>
				<th>Language</th>
				<th>Frequency</th>
				<th>Last execution</th>
				<th>Next execution</th>
				<th>Path</th>
				<th>Nb executions</th>
				<th>Nb errors</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${jobConfigurations}" var="job" varStatus="status">
				<tr class="jobConfigurations">
					<td class="configurationName"><a href=<c:url value="/admin/license/configuration?configurationId=${job[0]}"/>>${job[1]}</a></td>
					<td class="extractionLanguage">${job[2]}</td>
					<td class="frequency"><spring:message code="licenseHolder.frequency.${job[3]}"/></td>
					<td class="lastExecution"><fmt:formatDate value="${job[4]}" pattern="dd/MM/yyyy HH:mm" /></td>
					<td class="nextExecution"><fmt:formatDate value="${job[5]}" pattern="dd/MM/yyyy" /></td>
					<td class="path">${job[6]}</td>
					<td class="numberOfExecutions">${job[7]}</td>
					<td class="numberOfErrors"><c:if test="${job[8]>0}"><span style="color:red">${job[8]}</span></c:if></td>
					<td class="action"><a class="deleteDialogOpener" href=<c:url value="/admin/license/configuration/delete?configurationId=${job[0]}"/> id="dialogOpener">Delete</a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	
	<div style="text-align:center;">
		<input id="jobAddButton" type="button" value="New job" data-url="${pageContext.request.contextPath}/admin/license/add/job" />
	</div>
	
	<div class="subtitle">Ad-hoc extractions</div>
	
	<table>
		<thead>
			<tr>
				<th>Extraction name</th>
				<th>Language</th>
				<th>Status</th>
				<th>Execution date</th>
				<th>Nb documents</th>
				<th>Path</th>
				<th>Actions</th>
			</tr>
		</thead>
		<tbody>
			<c:forEach items="${adhocConfigurations}" var="adhoc" varStatus="status">
				<tr class="adhocConfigurations">
					<td class="configurationName"><a href=<c:url value="/admin/license/configuration?configurationId=${adhoc[0].extractionConfiguration.id}"/>>${adhoc[0].extractionConfiguration.configurationName}</a></td>
					<td class="extractionLanguage">${adhoc[0].extractionConfiguration.languageBean.code}</td>
					<td class="executionStatus"><spring:message code="licenseHolder.execution.status.${adhoc[0].executionStatus}"/></td>
					<td class="executionDate"><fmt:formatDate value="${adhoc[0].effectiveExecutionDate}" pattern="dd/MM/yyyy HH:mm" /></td>
					<td class="numberOfDocuments"><c:if test="${adhoc[0].executionStatus!='NEW' && adhoc[0].executionStatus!='PENDING'}"><span<c:if test="${adhoc[1]==0}"> style="color:red"</c:if>>${adhoc[1]}</span></c:if></td>
					<td class="path">${adhoc[0].extractionConfiguration.path}</td>
					<td class="actions">
						<c:if test="${adhocConfigurationsOptions[status.index][0]}">
							<a href=<c:url value="/admin/license/execution/restartStep?executionId=${adhoc[0].id}"/>>Restart step</a><br />
						</c:if>
						<c:if test="${adhocConfigurationsOptions[status.index][1]}">
							<a href=<c:url value="/admin/license/execution/restart?executionId=${adhoc[0].id}"/>>Restart</a><br />
						</c:if>
						<a class="deleteDialogOpener" href=<c:url value="/admin/license/configuration/delete?configurationId=${adhoc[0].extractionConfiguration.id}"/> id="dialogOpener">Delete</a>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
	
	
	<div style="text-align:center;">
		<input id="adhocAddButton" type="button" value="New ad-hoc extraction" data-url="${pageContext.request.contextPath}/admin/license/add/adhoc" />
	</div>
	<div id="deleteDialog" title="You are going to delete a configuration">
		<div id="deleteText">
		    <p>Would you also delete files related to execution(s) of this configuration?</p>
		
			<p>Note that if you do not want to delete all the files:</p>
			<ul>
				<li>You are responsible to do it manually later,</li>
				<li>The system may need to overwrite files if they are into conflict with a future execution.</li>
			</ul>
		</div>
		<div id="deleteWaiting" style="text-align:center;">
			<p><img src="<c:url value="/resources/common/admin/css/images/ajax-loader-medium.gif" />" /></p>
			<p>Deleting...</p>
		</div>
		<div id="errorDeleteText">
		</div>
	</div>
	<div id="restartDialog" title="You are going to restart the executions in error">
		<div id="restartText">
		    <p>This action will restart the executions in error. Are you sure?</p>
		</div>
		<div id="restartWaiting" style="text-align:center;">
			<p><img src="<c:url value="/resources/common/admin/css/images/ajax-loader-medium.gif" />" /></p>
			<p>Restarting...</p>
		</div>
		<div id="errorRestartText">
		</div>
	</div>
	<div id="restartStepDialog" title="You are going to restart the execution steps in error">
		<div id="restartStepText">
		    <p>This action will restart the execution steps in error. Are you sure?</p>
		</div>
		<div id="restartStepWaiting" style="text-align:center;">
			<p><img src="<c:url value="/resources/common/admin/css/images/ajax-loader-medium.gif" />" /></p>
			<p>Restarting...</p>
		</div>
		<div id="errorRestartStepText">
		</div>
	</div>
</div>