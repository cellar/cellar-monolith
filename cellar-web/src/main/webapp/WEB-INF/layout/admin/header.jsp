<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<input type="hidden" id="baseUrl" value="<c:set var="base" value="${pageContext.request.contextPath eq '/' ? '' : pageContext.request.contextPath}" scope="request"/>">

<h1><a href="${base}/admin/" title="Cellar Admin Interface">Cellar Admin Interface</a></h1>
<span style="float:right;text-align:right"><a href='<c:url value='/cellar/logout'/>' title="Logout">Logout</a></span>
