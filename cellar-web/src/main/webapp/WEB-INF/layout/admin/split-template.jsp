<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="app" tagdir="/WEB-INF/tags"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>    
  <title><tiles:insertAttribute name="title"/></title>
  <tiles:insertAttribute name="resources"/>
  <tiles:insertAttribute name="specific-resources"/>
</head>
<body>
<div class="container">
	<div class="header">
		<tiles:insertAttribute name="header"/>
	</div>
	<div class="menu">
		<tiles:insertAttribute name="menu"/>
	</div>
	<div class="content">
		<table cellspacing="0" cellpadding="0" style="border: 0px;" >
			<tr>
				<td style="border:0px; width:150px;" >
					<div class="menu">
						<tiles:insertAttribute name="contentMenu" />
					</div>
				</td>
				<td style="border:0px;">
					<c:set var="contentTitleCode"><tiles:insertAttribute name="contentTitleCode" defaultValue=""/></c:set>
					<c:if test="${not empty contentTitleCode}">
						<div class="title">
							<spring:message code="${contentTitleCode}" />
						</div>
					</c:if>
					<app:cellar-user-message-container />
					<div class="body">
						<tiles:insertAttribute name="contentBody"/>
					</div>
				</td>
			</tr>
		</table>
	</div>
	<div class="footer">
	<tiles:insertAttribute name="footer"/>
	</div>
</div>
</body>
</html>