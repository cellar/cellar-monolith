<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="base" value="${pageContext.request.contextPath eq '/' ? '' : pageContext.request.contextPath}" scope="request"/>

<!--  common admin style -->
<link rel="stylesheet" href="${base}/resources/common/admin/css/style.css" type="text/css"/>
<link rel="stylesheet" href="${base}/resources/common/admin/css/jquery-ui-1.13.2.custom.css" type="text/css"/>

<!--  common javascript-->
<script type="text/javascript" src="${base}/resources/common/js/jquery-3.6.1.min.js"></script>
<script type="text/javascript" src="${base}/resources/common/js/cellar.js"></script>
<script type="text/javascript" src="${base}/resources/common/js/jquery.confirm-1.3.js"></script>
<script type="text/javascript" src="${base}/resources/common/js/jquery.validate.min.js"></script>

<script type="text/javascript" src="${base}/resources/common/js/jquery-ui-1.13.2.custom.min.js"></script>
<script type="text/javascript" src="${base}/resources/common/js/jquery.balloon.js"></script>