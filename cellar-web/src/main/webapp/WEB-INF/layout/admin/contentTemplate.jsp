<%@ taglib prefix="app" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:set var="contentTitleCode"><tiles:getAsString name="contentTitleCode"/></c:set>
<div class="title">
	<spring:message code="${contentTitleCode}" />
</div>
<app:cellar-user-message-container />
<div class="body">
	<tiles:insertAttribute name="contentBody"/>
</div>