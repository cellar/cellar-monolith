<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<c:set var="base"
	value="${pageContext.request.contextPath eq '/' ? '' : pageContext.request.contextPath}"
	scope="request" />

<ul>
	<sec:authorize access="hasRole('ROLE_DASHBOARD')">
		<li class="menuItem" id="dashboard"><a
			href="${pageContext.request.contextPath}/admin/dashboard"
			title="Dashboard"><spring:message code="menu.item.dashboard" /></a></li>
	</sec:authorize>
	<sec:authorize access="hasRole('ROLE_AUDIT')">
		<li class="menuItem" id="audit"><a href="${base}/admin/audit"
			title="audit"><spring:message code="menu.item.audit" /></a></li>
	</sec:authorize>
	<sec:authorize
		access="hasAnyRole('ROLE_SPARQL_ENDPOINT','ROLE_SPARQL_CONFIGURATION','ROLE_SPARQL_TEMPLATES')">
		<li class="menuItem" id="sparql"><a
			href="${base}/admin/cmr/sparql" title="Sparql"><spring:message
					code="menu.item.sparql" /></a></li>
	</sec:authorize>
	<sec:authorize
		access="hasAnyRole('ROLE_INDEXING','ROLE_REINDEXING')">
		<li class="menuItem" id="indexing" class="selected"><a
			href="${base}/admin/cmr/indexing" title="Indexing"><spring:message
					code="menu.item.indexing" /></a></li>
	</sec:authorize>
	<sec:authorize access="hasRole('ROLE_LOG')">
		<li class="menuItem" id="log"><a
			href="${pageContext.request.contextPath}/admin/cmr/log" title="Log"><spring:message
					code="menu.item.log" /></a></li>
	</sec:authorize>
	<sec:authorize
		access="hasRole('ROLE_CONFIGURATION')">
		<li class="menuItem" id="configuration"><a
			href="${base}/admin/configuration" title="Config"><spring:message
					code="menu.item.configuration" /></a></li>
	</sec:authorize>
	<sec:authorize access="hasRole('ROLE_ARCHIVE')">
		<li class="menuItem" id="archive"><a
			href="${base}/admin/archive" title="archive"><spring:message
					code="menu.item.archive" /></a></li>
	</sec:authorize>
	<sec:authorize access="hasRole('ROLE_FIXITY')">
		<li class="menuItem" id="fixity"><a
				href="${base}/admin/fixity" title="fixity"><spring:message
				code="menu.item.fixity" /></a></li>
	</sec:authorize>
	<sec:authorize
		access="hasRole('ROLE_CONTENT_SEARCH')">
		<li class="menuItem" id="contentSearch"><a
			href="${base}/admin/contentSearch" title="contentSearch"><spring:message
					code="menu.item.contentSearch" /></a></li>
	</sec:authorize>
	<sec:authorize
		access="hasRole('ROLE_CONTENT_VISIBILITY')">
		<li class="menuItem" id="visibility"><a
			href="${base}/admin/cmr/visibility" title="visibility"><spring:message
					code="menu.item.visibility" /></a></li>
	</sec:authorize>
	<sec:authorize access="hasRole('ROLE_NAL')">
		<li class="menuItem" id="nal"><a
			href="${base}/admin/cmr/nal/configurednals" title="Nal"><spring:message
					code="menu.item.nal" /></a></li>
	</sec:authorize>
	<sec:authorize access="hasRole('ROLE_VIRTUOSO')">
		<li class="menuItem" id="virtuoso"><a
				href="${base}/admin/cmr/virtuoso/sparqlloadrequests" title="Virtuoso"><spring:message
				code="menu.item.virtuoso" /></a></li>
	</sec:authorize>
	<sec:authorize access="hasRole('ROLE_ONTOLOGY')">
		<li class="menuItem" id="onto"><a href="${base}/admin/cmr/onto"
			title="Ontology Configuration"><spring:message
					code="menu.item.onto" /></a></li>
	</sec:authorize>
	<sec:authorize access="hasRole('ROLE_INFERENCE_MODEL')">
		<li class="menuItem" id="infModel"><a href="${base}/admin/cmr/infModel"
										  title="Inference Model"><spring:message
				code="menu.item.inferenceModel" /></a></li>
	</sec:authorize>
	<sec:authorize access="hasRole('ROLE_VALIDATOR')">
		<li class="menuItem" id="validator"><a
			href="${base}/admin/validator" title="validator"><spring:message
					code="menu.item.validator" /></a></li>
	</sec:authorize>
	<sec:authorize access="hasRole('ROLE_SECURITY')">
		<li class="menuItem" id="security"><a
			href="${base}/admin/security" title="security"><spring:message
					code="menu.item.security" /></a></li>
	</sec:authorize>
	<c:if
		test="${cellarConfiguration.cellarServiceLicenseHolderEnabled && !cellarConfiguration.cellarDatabaseReadOnly}">
		<sec:authorize access="hasRole('ROLE_LICENSE')">
			<li class="menuItem" id="license"><a
				href="${base}/admin/license" title="license"><spring:message
						code="menu.item.license" /></a></li>
		</sec:authorize>
	</c:if>
	<c:if
		test="${cellarConfiguration.cellarServiceRDFStoreCleanerEnabled && !cellarConfiguration.cellarDatabaseReadOnly}">
		<sec:authorize
			access="hasRole('ROLE_RDF_STORE_CLEANER')">
			<li class="menuItem" id="cleaner"><a
				href="${base}/admin/cmr/rdfStoreCleaner" title="rdfStoreCleaner"><spring:message
						code="menu.item.rdfStoreCleaner" /></a></li>
		</sec:authorize>
	</c:if>
	<c:if test="${!cellarConfiguration.cellarDatabaseReadOnly}">
		<sec:authorize
			access="hasRole('ROLE_HIERARCHY_ALIGNER')">
			<li class="menuItem" id="aligner"><a
				href="${base}/admin/aligner" title="hierarchyAligner"><spring:message
						code="menu.item.hierarchyAligner" /></a></li>
		</sec:authorize>
	</c:if>
	<sec:authorize access="hasRole('ROLE_THREADS')">
		<li class="menuItem" id="security"><a
			href="${base}/admin/threads" title="threads"><spring:message
					code="menu.item.threads" /></a></li>
	</sec:authorize>
	<sec:authorize access="hasRole('ROLE_EXPORT')">
		<li class="menuItem" id="export"><a
			href="${base}/admin/cmr/export/job" title="export"><spring:message
					code="menu.item.export" /></a></li>
	</sec:authorize>
	<sec:authorize access="hasRole('ROLE_UPDATE')">
		<li class="menuItem" id="update"><a
				href="${base}/admin/cmr/update/job/sparql-update" title="update"><spring:message
				code="menu.item.update" /></a></li>
	</sec:authorize>

	<sec:authorize access="hasRole('ROLE_IDENTIFIERS_HISTORY')">
		<li class="menuItem" id="identifiersHistory"><a
			href="${base}/admin/history" title="identifiersHistory"><spring:message
					code="menu.item.identifiers.history" /></a></li>
	</sec:authorize>

	<sec:authorize access="hasRole('ROLE_ITEMS')">
		<li class="menuItem" id="items">
			<a href="${base}/admin/items" title="items">
				<spring:message code="menu.item.items" />
			</a>
		</li>
	</sec:authorize>

	<sec:authorize access="hasAnyRole('ROLE_SUPER_ADMIN', 'ROLE_INGEST')">
    		<li class="menuItem" id="mets">
    			<a href="${base}/admin/mets" title="mets">
    				<spring:message code="menu.item.mets" />
    			</a>
    		</li>
    	</sec:authorize>

</ul>
