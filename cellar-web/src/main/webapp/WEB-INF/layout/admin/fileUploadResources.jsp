<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="base" value="${pageContext.request.contextPath eq '/' ? '' : pageContext.request.contextPath}" scope="request"/>

<!--  common admin style -->
<link rel="stylesheet" href="${base}/resources/common/admin/css/style.css" type="text/css"/>



<!-- ****************************** FILEUPLOAD JQUERY CSS BEGIN ****************************** -->
<link rel="stylesheet" href="${base}/resources/cmr/onto/fileupload/css/jquery-ui.cupertino.1.13.2.css" type="text/css"/>
<link rel="stylesheet" href="${base}/resources/cmr/onto/fileupload/css/blueimp-gallery.min.css" type="text/css"/>
<link rel="stylesheet" href="${base}/resources/cmr/onto/fileupload/css/jquery.fileupload.css" type="text/css"/>
<link rel="stylesheet" href="${base}/resources/cmr/onto/fileupload/css/jquery.fileupload-ui.css" type="text/css"/>
<link rel="stylesheet" href="${base}/resources/cmr/onto/fileupload/css/jquery.fileupload-noscript.css" type="text/css"/>
<link rel="stylesheet" href="${base}/resources/cmr/onto/fileupload/css/jquery.fileupload-ui-noscript.css" type="text/css"/>
<!-- ****************************** FILEUPLOAD JQUERY CSS END ****************************** -->





<!-- RESPECT THE ORDERING -->
<!-- ****************************** FILEUPLOAD JQUERY JS BEGIN ****************************** -->
<script type="text/javascript" src="${base}/resources/common/js/jquery-3.6.1.min.js"></script>
<script type="text/javascript" src="${base}/resources/cmr/onto/fileupload/js/jquery-ui.1.13.2.min.js"></script>
<script type="text/javascript" src="${base}/resources/cmr/onto/fileupload/js/tmpl.min.js"></script>
<script type="text/javascript" src="${base}/resources/cmr/onto/fileupload/js/load-image.min.js"></script>
<script type="text/javascript" src="${base}/resources/cmr/onto/fileupload/js/canvas-to-blob.min.js"></script>
<script type="text/javascript" src="${base}/resources/cmr/onto/fileupload/js/jquery.blueimp-gallery.min.js"></script>
<script type="text/javascript" src="${base}/resources/cmr/onto/fileupload/js/jquery.iframe-transport.js"></script>
<script type="text/javascript" src="${base}/resources/cmr/onto/fileupload/js/jquery.fileupload.js"></script>
<script type="text/javascript" src="${base}/resources/cmr/onto/fileupload/js/jquery.fileupload-process.js"></script>
<script type="text/javascript" src="${base}/resources/cmr/onto/fileupload/js/jquery.fileupload-image.js"></script>
<script type="text/javascript" src="${base}/resources/cmr/onto/fileupload/js/jquery.fileupload-audio.js"></script>
<script type="text/javascript" src="${base}/resources/cmr/onto/fileupload/js/jquery.fileupload-video.js"></script>
<script type="text/javascript" src="${base}/resources/cmr/onto/fileupload/js/jquery.fileupload-validate.js"></script>
<script type="text/javascript" src="${base}/resources/cmr/onto/fileupload/js/jquery.fileupload-ui.js"></script>
<script type="text/javascript" src="${base}/resources/cmr/onto/fileupload/js/jquery.fileupload-jquery-ui.js"></script>
<!-- ****************************** FILEUPLOAD JQUERY JS END ****************************** -->

<!--  common javascript -->
<script type="text/javascript" src="${base}/resources/common/js/cellar.js"></script>
<script type="text/javascript" src="${base}/resources/common/js/jquery.balloon.js"></script>