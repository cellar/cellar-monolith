<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Logged out</title>
</head>
<body>
<h1>Successful log out</h1>
<hr/>
<p>You have been successfully logged out from the CELLAR AdminUI, <b>but your EU Login session may still be active.</b></p>
<p>Click <a href="<c:url value="/admin/dashboard"/>">here</a> to navigate back to the CELLAR AdminUI.</p>
</body>
</html>