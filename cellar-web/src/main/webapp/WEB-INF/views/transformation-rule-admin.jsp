<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admin Rule</title>

<style type="text/css">
table
{
border-collapse:collapse;
}
table, td, th
{
padding: 5px;
border:1px solid black;
}
</style>

</head>
<body>
	<table>
			<tr>
				<td colspan="7">
					<spring:message code="transofrmation.rule.admin.rules" />
				</td>
			</tr>
			<tr>
				<th><spring:message code="transofrmation.rule.admin.system" /></th>
				<th><spring:message code="transofrmation.rule.admin.source" /></th>
				<th><spring:message code="transofrmation.rule.admin.target" /></th>
				<th><spring:message code="transofrmation.rule.admin.xslsheet" /></th>
				<th colspan="3"><spring:message code="transofrmation.rule.admin.active" /></th>
			</tr>
			<c:forEach items="${rules}" var="rule">
				<form:form action="update" modelAttribute="rule">
					<tr>
						<td><span>${rule.system.systemName}</span></td>
						<td><span>${rule.source.formatName}</span></td>
						<td><span>${rule.target.formatName}</span></td>
						<td><span>${rule.xslUrl}</span></td>
						
						<td><span><label for="y"><spring:message code="common.lang.yes" /></label><input id="y" type="radio" name="active" value="true" ${rule.active?'checked':''} /></span></td>
						<td><span><label for="n"><spring:message code="common.lang.no" /></label><input id="n" type="radio" name="active" value="false" ${not rule.active?'checked':''} /></span></td>
						<td>
							<input type="hidden" value="${rule.system.id}" name="system.id" /> 
							<input type="hidden" value="${rule.source.id}" name="source.id" /> 
							<input type="hidden" value="${rule.target.id}" name="target.id" /> 
							<input type="hidden" value="${rule.xslUrl}" name="xslUrl" /> 
							<input type="hidden" value="${rule.id}" name="id" /> 
							<input type="submit" value="<spring:message code="common.command.update" />" />
						</td>
					</tr>
				</form:form>
			</c:forEach>
	</table>
	
</body>
</html>