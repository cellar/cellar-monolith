<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Access Denied</title>
	</head>
	<body>
		<h1>Access Denied</h1>
		<hr/>
		<p>Access has been denied to the requested resource.</p>
		<sec:authorize access="not hasRole('ROLE_GUEST')">
			<p>You may try accessing the AdminUI <a href="<c:url value="/admin/dashboard"/>">Dashboard</a> page and start navigating from there.</p>
		</sec:authorize>
		<sec:authorize access="hasRole('ROLE_GUEST')">
			<p>A CELLAR user corresponding to the provided EU-Login ID could not be found. Click <a href="<c:url value="/admin/unauthorized/select-action"/>">here</a> to request access or migrate an existing user account.</p>
		</sec:authorize>
	</body>
</html>