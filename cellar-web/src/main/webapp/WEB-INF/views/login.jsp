<%@page contentType="text/html" pageEncoding="MacRoman"%>
<%@ taglib prefix='c' uri='http://java.sun.com/jstl/core_rt'%>
<%@ page import="org.springframework.security.core.AuthenticationException"%>
<c:set var="base" value="${pageContext.request.contextPath eq '/' ? '' : pageContext.request.contextPath}" scope="request"/>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Login Page</title>
</head>
<body>
<h1>Login</h1>
<c:if test="${not empty param.login_error}">
	<font color="red">
	Your login attempt was not successful, please try again.<br/>
	<br/>
	<c:if test="${not empty SPRING_SECURITY_LAST_EXCEPTION.message}">
	   Reason: <c:out value="${SPRING_SECURITY_LAST_EXCEPTION.message}" />.
    </c:if>
	</font>
</c:if>
<br>
<br>
<form name="f" action="${base}/<c:url value='login'/>"
	method="POST">
<fieldset><legend>Log In Form</legend>
<hr />
<ol>
	<li>
		<label for="login">Username:</label>
	 	<input type='text'	name='username' value='' autofocus />
 	</li>
	<li>
		<label for="password">Password:</label>
		<input type='password' name='password' /></li>
	<li>
		<label for="remember_me">Remember me:</label>
		<input type="checkbox" name="_spring_security_remember_me">
	</li>
</ol>

<div>
	<input name="reset" type="reset" value="Reset"
		onclick="return confirm('Are you sure you want to Clear or Reset this form');">
	<input name="submit" type="submit" value="Log In">
</div>
</fieldset>
</form>
</body>
</html>
