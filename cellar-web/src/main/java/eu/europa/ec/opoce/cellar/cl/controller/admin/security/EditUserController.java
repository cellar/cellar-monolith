/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.security
 *             FILE : EditUserController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 11, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.admin.security;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.admin.error.PageNotFoundController;
import eu.europa.ec.opoce.cellar.cl.controller.admin.security.command.EditUserCommand;
import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL;
import eu.europa.ec.opoce.cellar.cl.domain.admin.Group;
import eu.europa.ec.opoce.cellar.cl.domain.admin.User;
import eu.europa.ec.opoce.cellar.cl.service.client.SecurityService;
import org.apache.commons.lang.StringUtils;
import org.jasypt.springsecurity4.crypto.password.PBEPasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;

import java.util.List;

/**
 * <class_description> Edit a user controller.
 *
 * <br/>
 * <br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc. <br/>
 * <br/>
 * ON : Jan 11, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
@RequestMapping(EditUserController.PAGE_URL)
public class EditUserController extends AbstractCellarController {

    public static final String PAGE_URL = "/security/user/edit";

    private static final String JSP_NAME = "security/user/edit";

    private static final String OBJECT_NAME = "editUserCommand";
    
    private static final String IS_USER_NON_MIGRATED_NAME = "isUserNonMigrated";

    /**
     * Security service manager.
     */
    @Autowired
    private SecurityService securityService;

    /**
     * Password encoder/hasher
     */

    @Autowired
    private PBEPasswordEncoder passwordEncoder;

    /**
     * Validator for a created/edited user.
     */
    @Autowired
    @Qualifier("userValidator")
    private Validator userValidator;

    /**
     * Returns all the groups.
     *
     * @return all the groups
     */
    @ModelAttribute("groupsList")
    public List<Group> populateGroupsList() {
        return this.securityService.findAllGroups();
    }

    /**
     * Handle the GET.
     */
    @RequestMapping(method = RequestMethod.GET)
    public String editUserGet(final ModelMap model, @RequestParam final Long id) {
        final User user = this.securityService.findUser(id); // gets the user
        if (user == null) {
            return PageNotFoundController.ERROR_VIEW;
        }

        final EditUserCommand editUser = new EditUserCommand();
        editUser.setEnabled(user.isEnabled());
        editUser.setGroupId(user.getGroup().getId());
        editUser.setId(user.getId());
        editUser.setUsername(user.getUsername());
        model.addAttribute(OBJECT_NAME, editUser);
        
        model.addAttribute(IS_USER_NON_MIGRATED_NAME, StringUtils.isNotBlank(user.getPassword()));
        
        return JSP_NAME;
    }

    /**
     * Handle the POST.
     */
    @RequestMapping(method = RequestMethod.POST)
    public String editUserPost(@ModelAttribute(OBJECT_NAME) final EditUserCommand editedUser, final BindingResult bindingResult,
            final SessionStatus status, final ModelMap model) {

        this.userValidator.validate(editedUser, bindingResult);

        final User user = this.securityService.findUser(editedUser.getId());
        
        if (bindingResult.hasErrors()) { // validates the created group
        	model.addAttribute(IS_USER_NON_MIGRATED_NAME, StringUtils.isNotBlank(user.getPassword()));
        	return JSP_NAME;
        }

        final Group group = new Group();
        group.setId(editedUser.getGroupId());

        
        user.setEnabled(editedUser.isEnabled());
        user.setGroup(group);

        if (this.cellarConfiguration.getCellarServiceEuloginUserMigrationEnabled() &&
        		StringUtils.isNotBlank(editedUser.getPassword()) && StringUtils.isNotBlank(editedUser.getConfirmPassword())) {
            user.setPassword(this.passwordEncoder.encode(editedUser.getPassword()));
        }

        this.securityService.updateUser(user);

        super.addUserMessage(LEVEL.SUCCESS, "common.message.successfullySaved");
        return redirect(UserManagerController.PAGE_URL); // redirects to the
                                                         // user manager
    }
}
