package eu.europa.ec.opoce.cellar.cl.controller.admin.update;

import com.google.common.base.Throwables;
import eu.europa.ec.opoce.cellar.cl.controller.resource.AbstractResourceController;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditBuilder;
import eu.europa.ec.opoce.cellar.exception.DigitalObjectNotFoundException;
import eu.europa.ec.opoce.cellar.server.admin.lastmodification.LastModificationDateUpdateService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;

import static eu.europa.ec.opoce.cellar.cl.domain.audit.Auditable.LogLevel.ERROR;

/**
 * @author ARHS Developments
 */
@Controller
@RequestMapping("/secapi/lastmodificationdate")
public class LastModificationDateController extends AbstractResourceController {

    private static final Logger LOG = LogManager.getLogger(LastModificationDateController.class);

    private final LastModificationDateUpdateService lastModificationDateUpdateService;

    @Autowired
    public LastModificationDateController(LastModificationDateUpdateService lastModificationDateUpdateService) {
        this.lastModificationDateUpdateService = lastModificationDateUpdateService;
    }

    /**
     * Update the last-modification-date of a digital object
     * to the current date or the date in parameter if provided.
     *
     * @param cellarId the id of the digital object to update
     * @param date     the optional last modification date (
     *                 yyyy-MM-dd'T'HH:mm:ss)
     * @return an empty {@link ResponseEntity} with the HTTP
     * code 204 (No Content) or 500 if something wrong happened.
     */
    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<Void> updateLastModificationDate(@RequestParam(value = "cellarId") final String cellarId,
                                                           @RequestParam(value = "date", required = false)
                                                           @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") Date date) {
        final Date newDate = date != null ? date : new Date();
        try {
            lastModificationDateUpdateService.updateLastModificationDate(cellarId, newDate);
        } catch (DigitalObjectNotFoundException notFound) {
            LOG.error("Cannot update [{}] with date [{}]; {}", cellarId, newDate, Throwables.getRootCause(notFound));
            return ResponseEntity.notFound().build();
        } catch (Exception e) {
            AuditBuilder.get()
                    .withLogLevel(ERROR)
                    .withLogger(LOG)
                    .withMessage("Cannot update [{}] with date [{}]; {}")
                    .withMessageArgs(cellarId, newDate)
                    .withException(e)
                    .logEvent();
            return ResponseEntity.status(500).build();
        }
        return ResponseEntity.noContent().build();
    }
}
