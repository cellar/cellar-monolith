/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.security.validator
 *             FILE : UserValidator.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 14, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.admin.security.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.controller.admin.security.command.AcceptUserCommand;
import eu.europa.ec.opoce.cellar.cl.controller.admin.security.command.BaseUserCommand;
import eu.europa.ec.opoce.cellar.cl.controller.admin.security.command.EditUserCommand;
import eu.europa.ec.opoce.cellar.cl.domain.admin.UserAccessRequest;
import eu.europa.ec.opoce.cellar.cl.service.client.UserAccessRequestService;

/**
 * <class_description> User validator.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 14, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component("userValidator")
public class UserValidator implements Validator {

	@Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;
	
	@Autowired
	private UserAccessRequestService userAccessRequestService;
	
    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(final Class<?> clazz) {
        return BaseUserCommand.class.isAssignableFrom(clazz);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validate(final Object target, final Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "range.security.user.username", "Username is required.");

        if (this.cellarConfiguration.getCellarServiceEuloginUserMigrationEnabled() && (target instanceof EditUserCommand)) {
        	final EditUserCommand user = (EditUserCommand) target;
            if ((user.getPassword() == null && user.getConfirmPassword() != null)
                    || (user.getConfirmPassword() == null && user.getPassword() != null) || (user.getConfirmPassword() != null
                            && user.getPassword() != null && !user.getConfirmPassword().equals(user.getPassword()))) {
                errors.rejectValue("password", "match.security.user.password", "Password and Confirm password don't match.");
            }
        }
        
        if (target instanceof AcceptUserCommand) {
        	final AcceptUserCommand user = (AcceptUserCommand) target;
        	Long uarId = user.getUserAccessRequestId();
        	if (uarId == null) {
        		errors.rejectValue("userAccessRequestId", "security.user-access-request.manager.accept.invalid.uarId", "The submitted user access request ID is invalid.");
        		return;
        	}
        	UserAccessRequest uar = this.userAccessRequestService.findUserAccessRequest(user.getUserAccessRequestId());
        	if (uar == null) {
        		errors.rejectValue("userAccessRequestId", "security.user-access-request.manager.accept.invalid.uarId", "The submitted user access request ID is invalid.");
        	}
        	else if (!uar.getUsername().equals(user.getUsername())) {
        		errors.rejectValue("username", "security.user-access-request.manager.accept.invalid.username", "The submitted username does not correspond to a user access request.");
        	}
        }
        
        final BaseUserCommand user = (BaseUserCommand) target;
        if (user.getGroupId() == null) {
            errors.rejectValue("groupId", "required.security.user.groupId", "A group is required.");
        }
    }

}
