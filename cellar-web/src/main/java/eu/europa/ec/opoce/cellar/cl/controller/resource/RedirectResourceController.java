/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.resource
 *             FILE : RedirectResourceController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Mar 31, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.resource;

import eu.europa.ec.opoce.cellar.cl.domain.validator.ContentIdsValidator;
import eu.europa.ec.opoce.cellar.common.http.headers.CellarHttpHeaders;
import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.server.dissemination.AcceptLanguage;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.AcceptNegotiationRequest;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.DisseminationRequest;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.Negotiate;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.NegotiationRequest;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.IDisseminationResolver;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.RedirectResolver;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;
import eu.europa.ec.opoce.cellar.server.service.DisseminationException;
import eu.europa.ec.opoce.cellar.server.service.DisseminationService;
import eu.europa.ec.opoce.cellar.support.dissemination.editor.CellarResourceBeanEditor;
import eu.europa.ec.opoce.cellar.support.dissemination.editor.DisseminationRequestEditor;
import eu.europa.ec.opoce.cellar.support.dissemination.editor.NegotiateEditor;
import eu.europa.ec.opoce.cellar.support.dissemination.editor.NegotiationRequestEditor;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.HttpHeaders;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Mar 31, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
public class RedirectResourceController extends AbstractResourceController {

    protected final static Logger LOGGER = LogManager.getLogger(RedirectResourceController.class);

    private final static String DECODING_REQUEST_PARAM = "language";

    private final static String FILTERED_REQUEST_PARAM = "filter";

    /** The dissemination service. */
    @Autowired
    protected DisseminationService disseminationService;

    /** The content ids validator. */
    @Qualifier("contentIdsValidator")
    private @Autowired ContentIdsValidator contentIdsValidator;

    /** The fallback redirect resource controller. */
    @Autowired
    protected FallbackRedirectResourceController fallbackRedirectResourceController;

    /**
     * Post data.
     *
     * @param request the request
     * @param response the response
     * @return the response entity
     */
    @RequestMapping(value = "/**", method = {
            RequestMethod.POST})
    public void postData(final HttpServletRequest request, final HttpServletResponse response) {
        // Load PSI REGEX to check if the request must be dispatch to Resource or Sparql
        final List<Pattern> patterns = this.contentIdsValidator.getPatterns();
        final String contextPath = request.getContextPath();
        final String servletPath = request.getServletPath();
        final String requestURI = request.getRequestURI();
        final String pathInfo = requestURI.replaceAll(contextPath + servletPath, "");
        final String resource = pathInfo.substring(1).replaceFirst("/", ":");
        // If the request matches with a pattern then it is a resource request
        for (final Pattern pattern : patterns) {
            if (Pattern.matches(pattern.pattern(), resource)) {
                throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class)
                        .withHttpStatus(HttpStatus.METHOD_NOT_ALLOWED)
                        .withMessage("Post requests are not allowed in dissemination, only GET and HEAD requests are valid.").build();
            }
        }
    }

    /**
     * Gets the resource.
     *
     * @param request the request
     * @param response the response
     * @param negotiationRequest the negotiationRequest
     * @param decoding the decoding
     * @param rel the rel
     * @param filtered the filtered
     * @param negotiate the negotiate
     * @param acceptDateTime the acceptDateTime
     *
     * @return  the resource
     */
    @RequestMapping(value = "/**", method = {
            RequestMethod.GET, RequestMethod.HEAD})
    public ResponseEntity<?> dispatchRequest(final HttpServletRequest request, final HttpServletResponse response, //
            @RequestHeader(value = HttpHeaders.ACCEPT, required = false, defaultValue = "*/*") final NegotiationRequest negotiationRequest, //
            @RequestParam(value = RedirectResourceController.DECODING_REQUEST_PARAM, required = false) final String decoding, //
            @RequestParam(value = "rel", required = false) final String rel, //
            @RequestParam(value = RedirectResourceController.FILTERED_REQUEST_PARAM, required = false, defaultValue = "false") final boolean filtered, //
            @RequestHeader(value = CellarHttpHeaders.NEGOTIATE, required = false, defaultValue = "false") final Negotiate negotiate, //
            @RequestHeader(value = CellarHttpHeaders.ACCEPT_DATETIME, required = false) final Date acceptDateTime) { //Used in the case of Memento dissemination requests

        // Load PSI REGEX to check if the request must be dispatch to Resource or Sparql
        final List<Pattern> patterns = this.contentIdsValidator.getPatterns();
        final String contextPath = request.getContextPath();
        final String servletPath = request.getServletPath();
        final String requestURI = request.getRequestURI();
        final String pathInfo = requestURI.replaceFirst(contextPath + servletPath, "");
        final String resource = pathInfo.substring(1).replaceFirst("/", ":");
        // If the request matches with a pattern then it is a resource request
        for (final Pattern pattern : patterns) {
            if (Pattern.matches(pattern.pattern(), resource)) {
                final String prefix = resource.substring(0, resource.indexOf(":"));
                final String identifier = getIdentifier(request, prefix);
                final CellarResourceBean cellarResource = this.disseminationService.resolveCellarResourceBean(prefix, identifier);
                return getResource(request, cellarResource, negotiationRequest, decoding, rel, negotiate, acceptDateTime);
            }
        }
        // Else it is a sparql request
        return this.fallbackRedirectResourceController.executeSparqlQuery(request, response);
    }

    /**
     * Gets the identifier.
     *
     * @param request the request
     * @param prefix the prefix
     * @return the identifier
     */
    private String getIdentifier(final HttpServletRequest request, final String prefix) {
        final String resourcePath = getUrlPathHelper().getLookupPathForRequest(request);
        final String identifier = AbstractResourceController.getIdentifier(resourcePath, prefix);

        if (StringUtils.isBlank(identifier)) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage("Resource identifier part is missing.").build();
        }
        return identifier;
    }

    /**
     * Gets the resource.
     *
     * @param request the request
     * @param cellarResource the cellar resource
     * @param negotiationRequest the negotiation request
     * @param decoding the decoding
     * @param rel the rel
     * @param negotiate the negotiate
     * @param acceptDateTime the accept date time
     * @return the resource
     */
    public ResponseEntity<?> getResource(final HttpServletRequest request, final CellarResourceBean cellarResource,
            final NegotiationRequest negotiationRequest, final String decoding, final String rel, final Negotiate negotiate,
            final Date acceptDateTime) {
        // TODO CELLARM-457 step 2
        // The filtered Parameter is deprecated; it will be removed during the implementation of CELLARM-457/CELLAR-724 step 2

        ResponseEntity<?> responseEntity = null;
        //the highest ranked exception has to be kept because it can be used at the end of the requests treatment
        DisseminationException highestRankedErrorResponse = null;

        //iterate each dissemination request
        for (final AcceptNegotiationRequest anr : negotiationRequest) {
            final DisseminationRequest disseminationRequest = anr.getDisseminationRequestParameter().getDisseminationRequest();

            final AcceptLanguage acceptLanguage = anr.getAcceptLanguage();

            //try to retrieve the redirection
            try {
                final String acceptValueString = anr.getDisseminationRequestParameter().getAcceptValueString();
                final boolean alternatesEnabled = negotiate.isAlternatesEnabled();

                //TODO: please remove dead code in resolvers and dissemination service
                final IDisseminationResolver iDisseminationResolver = RedirectResolver.get(cellarResource, decoding, disseminationRequest,
                        acceptValueString, acceptLanguage, acceptDateTime, rel, alternatesEnabled);

                responseEntity = iDisseminationResolver.handleDisseminationRequest();
                //if a successful redirection is retrieved we can stop the loop
                final HttpStatus statusCode = responseEntity.getStatusCode();
                if (HttpStatus.SEE_OTHER.equals(statusCode) || HttpStatus.MULTIPLE_CHOICES.equals(statusCode)) {
                    break;
                }
            } catch (final DisseminationException e) {
                // if there is an exception keep the highest ranked http status so that it can be returned in the end if necessary
                final HttpStatus exceptionHttpStatus = e.getHttpStatus();
                if (exceptionHttpStatus != null) {
                    //keep it stored for possible later use
                    if ((highestRankedErrorResponse == null)
                            || (exceptionHttpStatus.compareTo(highestRankedErrorResponse.getHttpStatus()) > 0)) {
                        highestRankedErrorResponse = e;
                    }
                }
            }
        }
        //if there is no response than trigger the highest ranked exception
        if (responseEntity == null) {
            final HttpStatusAwareExceptionBuilder<DisseminationException> httpStatusAwareExceptionBuilder = HttpStatusAwareExceptionBuilder
                    .getInstance(DisseminationException.class);
            if (highestRankedErrorResponse != null) {
                httpStatusAwareExceptionBuilder.withHttpStatus(highestRankedErrorResponse.getHttpStatus());
                httpStatusAwareExceptionBuilder.withMessageArgs(highestRankedErrorResponse.getMessage());
            }
            throw httpStatusAwareExceptionBuilder
                    .withMessage("None of the requests returned successfully a redirection. The following exception was thrown: [{}]")
                    .build();
        }
        // This element is never contains a body (--> SeeOtherServiceImpl), so we don't need to care about GET or HEAD here.
        return responseEntity;
    }

    /**
     * Inits the binder.
     *
     * @param request the request
     * @param binder the binder
     */
    @InitBinder
    public void initBinder(final HttpServletRequest request, final ServletRequestDataBinder binder) {
        binder.registerCustomEditor(NegotiationRequest.class, new NegotiationRequestEditor(request));
        binder.registerCustomEditor(CellarResourceBean.class, new CellarResourceBeanEditor(request, getUrlPathHelper()));
        binder.registerCustomEditor(Negotiate.class, new NegotiateEditor());

        // to be kept, as it is still used by this controller (see services getConcept*)
        binder.registerCustomEditor(DisseminationRequest.class, new DisseminationRequestEditor());
    }

}
