/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.licenseHolder
 *             FILE : LicenseHolderDetailController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Nov 27, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.admin.licenseHolder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import eu.europa.ec.opoce.cellar.cl.domain.common.CellarPaginatedList;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionWorkIdentifier;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Nov 27, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
public class LicenseHolderDetailController extends LicenseHolderController {

    @RequestMapping(value = "/license/configuration", method = RequestMethod.GET)
    public ModelAndView configuration(final HttpServletRequest request, final HttpServletResponse response,
            @RequestParam(value = "configurationId", required = true) final Long configurationId) {

        return this.constructConfigurationDetail(configurationId, null);
    }

    @RequestMapping(value = "/license/execution", method = RequestMethod.GET)
    public ModelAndView execution(final HttpServletRequest request, final HttpServletResponse response,
            @RequestParam(value = "executionId", required = true) final Long executionId,
            @RequestParam(value = "workFilter", required = false) final String workFilter,
            @RequestParam(value = "dir", required = false) final String dir,
            @RequestParam(value = "page", required = false) final Integer page) {

        final ModelAndView modelAndView = new ModelAndView("executionDetail");
        final ExtractionExecution extractionExecution = this.licenseHolderService.findExtractionExecution(executionId);
        modelAndView.addObject("execution", extractionExecution);
        modelAndView.addObject("sparqlTempFilePath", this.licenseHolderService.getAbsoluteSparqlTempFilePath(extractionExecution));
        modelAndView.addObject("archiveTempDirectoryPath",
                this.licenseHolderService.getAbsoluteArchiveTempDirectoryPath(extractionExecution));
        modelAndView.addObject("archiveFilePath", this.licenseHolderService.getAbsoluteArchiveFilePath(extractionExecution));

        String workFilterWilcards = "";
        if (workFilter != null) {
            workFilterWilcards = workFilter.replace('*', '%');
            workFilterWilcards = workFilterWilcards.replace('?', '_');
        }

        CellarPaginatedList<ExtractionWorkIdentifier> workIdentifiersList = new CellarPaginatedList<ExtractionWorkIdentifier>(page,
                "workId", dir, workFilterWilcards);

        workIdentifiersList = this.licenseHolderService.findWorkIdentifiers(executionId, workIdentifiersList);

        modelAndView.addObject("workIdentifiersList", workIdentifiersList);
        if (StringUtils.isNotBlank(workFilter))
            modelAndView.addObject("workFilter", workFilter);
        return modelAndView;
    }
}
