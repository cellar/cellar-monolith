/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.export
 *             FILE : ExportController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 25-Oct-2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2016 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.admin.export;

import com.google.common.base.Strings;
import eu.europa.ec.opoce.cellar.cl.controller.resource.AbstractResourceController;
import eu.europa.ec.opoce.cellar.cl.controller.util.ControllerUtil;
import eu.europa.ec.opoce.cellar.cl.domain.export.ExportedPackage;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cl.service.export.IExportService;
import eu.europa.ec.opoce.cellar.common.http.headers.HttpHeadersBuilder;
import eu.europa.ec.opoce.cellar.core.dao.FileDao;
import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.server.service.DisseminationException;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.xml.bind.JAXBException;


import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

/**
 * The Class ExportController.
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 25-Oct-2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
@RequestMapping
public class ExportController extends AbstractResourceController {

    private final IExportService exportService;
    private final IdentifierService identifierService;
    private final FileDao fileDao;

    @Autowired
    public ExportController(IExportService exportService, @Qualifier("nonTransactionalReadOnlyPidManagerService") IdentifierService identifierService,
                            FileDao fileDao) {
        this.exportService = exportService;
        this.identifierService = identifierService;
        this.fileDao = fileDao;
    }

    /**
     * Export package.
     *
     * @param workId         the work id
     * @param metadataOnly   the metadata only
     * @param useAgnosticURL the use agnostic url
     * @param destination    the destination folder of the exported package
     *                       (on the local filesystem)
     * @return the response entity
     * @throws JAXBException the JAXB exception
     * @throws IOException   Signals that an I/O exception has occurred.
     */
    @RequestMapping(method = RequestMethod.GET, value = "/secapi/export")
    public ResponseEntity<ExportedPackage> exportPackage(
            @RequestParam(value = "id") final String workId,
            @RequestParam(required = false, value = "metadataOnly", defaultValue = "false") final boolean metadataOnly,
            @RequestParam(required = false, value = "useAgnosticURL", defaultValue = "true") final boolean useAgnosticURL,
            @RequestParam(required = false, value = "destination") final String destination)
            throws JAXBException, IOException {

        final File dest = Strings.isNullOrEmpty(destination) ? fileDao.getArchiveFolderExportRest() : Paths.get(destination).toFile();
        final String cellarId = resolveCellarId(workId);
        final ExportedPackage exportedPackage = this.exportService.export(cellarId, metadataOnly, dest, useAgnosticURL);
        final HttpHeaders httpHeaders = HttpHeadersBuilder.get().withDateNow().withContentType(MediaType.APPLICATION_XML).getHttpHeaders();
        return new ResponseEntity<>(exportedPackage, httpHeaders, HttpStatus.OK);
    }

    /**
     * Export package by embodying it on the response's stream.
     *
     * @param workId          the work id
     * @param metadataOnly    the metadata only
     * @param useAgnosticURL  the use agnostic url
     * @param embodiedPackage if true, the exported package is streamed to the response's body
     * @return the response entity
     * @throws JAXBException the JAXB exception
     * @throws IOException   Signals that an I/O exception has occurred.
     */
    @RequestMapping(method = RequestMethod.GET, value = "/secapi/export", params = "embodiedPackage=true")
    public ResponseEntity<?> exportEmbodiedPackage(
            @RequestParam(value = "id") final String workId, //
            @RequestParam(required = false, value = "metadataOnly", defaultValue = "false") final boolean metadataOnly,
            @RequestParam(required = false, value = "useAgnosticURL", defaultValue = "true") final boolean useAgnosticURL,
            @RequestParam(value = "embodiedPackage", defaultValue = "false") final boolean embodiedPackage
    ) throws JAXBException, IOException {

        // export the package onto file-system
        final String cellarId = resolveCellarId(workId);
        final ExportedPackage exportedPackage = this.exportService.export(cellarId, metadataOnly, useAgnosticURL);

        // build the entity to return
        final File exportedPackageFile = new File(exportedPackage.getExportedPackagePath());
        final ResponseEntity<?> responseEntity = ControllerUtil.makeResponse(exportedPackageFile,
        		MediaType.APPLICATION_OCTET_STREAM.toString(), false, exportedPackageFile.getName());

        // remove package file from file-system
        FileUtils.forceDelete(exportedPackageFile);
        // return entity
        return responseEntity;
    }


    private String resolveCellarId(final String workId) {
        try {
            return identifierService.getCellarPrefixed(workId);
        } catch (IllegalArgumentException e) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class)
                    .withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage("Resource '{}' does not exist")
                    .withMessageArgs(workId)
                    .build();
        }
    }
}
