/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.security.authentication.details
 *        FILE : PrincipalAwareJ2eeBasedPreAuthenticatedWebAuthenticationDetailsSource.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 10-03-2023
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2023 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.security.authentication.details;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.web.authentication.preauth.PreAuthenticatedGrantedAuthoritiesWebAuthenticationDetails;
import org.springframework.security.web.authentication.preauth.j2ee.J2eeBasedPreAuthenticatedWebAuthenticationDetailsSource;

/**
 * Authentication details source that builds a <code>PrincipalAwarePreAuthenticatedGrantedAuthoritiesWebAuthenticationDetails</code>
 * authentication details object.
 * 
 * @author EUROPEAN DYNAMICS S.A.
 */
public class PrincipalAwareJ2eeBasedPreAuthenticatedWebAuthenticationDetailsSource extends J2eeBasedPreAuthenticatedWebAuthenticationDetailsSource {

	@Override
	public PreAuthenticatedGrantedAuthoritiesWebAuthenticationDetails buildDetails(HttpServletRequest context) {
		PreAuthenticatedGrantedAuthoritiesWebAuthenticationDetails details = super.buildDetails(context);
		return new PrincipalAwarePreAuthenticatedGrantedAuthoritiesWebAuthenticationDetails(context, details.getGrantedAuthorities());
	}

}
