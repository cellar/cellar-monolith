package eu.europa.ec.opoce.cellar.cmr.web.controller.webapi;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.server.dissemination.converter.NoticeHttpMessageConverter;
import eu.europa.ec.opoce.cellar.server.service.DisseminationException;
import eu.europa.ec.opoce.cellar.server.service.DisseminationService;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;
import org.w3c.dom.Document;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.net.URLDecoder;
import java.util.List;

@SuppressWarnings("deprecation")
@Controller
public class WebApiController extends AbstractCellarController {

	private static final Logger LOG = LogManager.getLogger(WebApiController.class);

	private final DisseminationService disseminationService;

	private final RequestMappingHandlerAdapter requestMappingHandlerAdapter;

	@Autowired
	public WebApiController(DisseminationService disseminationService,
							RequestMappingHandlerAdapter requestMappingHandlerAdapter) {
		this.disseminationService = disseminationService;
		this.requestMappingHandlerAdapter = requestMappingHandlerAdapter;
	}

	@PostConstruct
	private void init() {
		final List<HttpMessageConverter<?>> messageConverters = this.requestMappingHandlerAdapter.getMessageConverters();
		messageConverters.add(0, new NoticeHttpMessageConverter());
		requestMappingHandlerAdapter
				.setMessageConverters(messageConverters);
	}

	/**
	 * uri to create a notice for a list of identifiers
	 * 
	 * @deprecated: Use WEMI service "identifiers" instead; this method will be
	 *              erased; to be clarified with OP when exactly. See
	 *              CELLARM-429/CELLAR-689
	 * 
	 * @param body
	 * @return
	 * @throws IOException
	 */
	@Deprecated
	@RequestMapping(value = "/getIdentifierList", method = RequestMethod.POST)
	public ResponseEntity<Document> identifierNotice(@RequestBody final String body) throws IOException {
		return disseminationService.identifierNotice(normalizeBody(body));
	}

	private String normalizeBody(final String body) throws IOException {
		String normalizedBody = URLDecoder.decode(body, "UTF-8");
		normalizedBody = StringUtils.removeEnd(normalizedBody, "=");
		return normalizedBody;
	}

	@ExceptionHandler(DisseminationException.class)
	private void handleException(final DisseminationException exception, final HttpServletResponse response)
			throws IOException {
		LOG.error(exception.toString(), exception);

		HttpStatus status = exception.getHttpStatus();
		if (status == null) {
			status = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		writeStringToResponse(response, exception.getMessage(), status);
	}

	@ExceptionHandler(Exception.class)
	private void handleException(final Exception exception, final HttpServletResponse response) throws Exception {
		LOG.error(exception.getMessage(), exception);

		writeStringToResponse(response, exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	private void writeStringToResponse(final HttpServletResponse response, final String message,
			final HttpStatus status) throws IOException {
		response.setStatus(status != null ? status.value() : HttpStatus.INTERNAL_SERVER_ERROR.value());

		final Writer writer = response.getWriter();
		writer.write(message != null ? message : StringUtils.EMPTY);
		writer.flush();
	}
}
