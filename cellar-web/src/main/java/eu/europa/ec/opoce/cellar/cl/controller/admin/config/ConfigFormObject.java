package eu.europa.ec.opoce.cellar.cl.controller.admin.config;

import java.util.List;

import eu.europa.ec.opoce.cellar.cl.domain.admin.ConfigurationProperty;

public class ConfigFormObject {

    private List<ConfigurationProperty> items;

    public ConfigFormObject() {
    }

    public ConfigFormObject(final List<ConfigurationProperty> items) {
        super();
        this.items = items;
    }

    public List<ConfigurationProperty> getItems() {
        return items;
    }

    public void setItems(final List<ConfigurationProperty> items) {
        this.items = items;
    }
}
