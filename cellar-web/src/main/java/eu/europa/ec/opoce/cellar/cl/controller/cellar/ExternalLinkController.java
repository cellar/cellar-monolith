/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.externalLink
 *             FILE : ExternalLinkController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 16, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.cellar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import eu.europa.ec.opoce.cellar.cl.controller.util.ControllerUtil;
import eu.europa.ec.opoce.cellar.cl.domain.externalLink.ExternalLink;
import eu.europa.ec.opoce.cellar.cl.domain.externalLink.ExternalLinkPatterns;
import eu.europa.ec.opoce.cellar.cl.service.client.ExternalLinkPatternCacheService;
import eu.europa.ec.opoce.cellar.cl.service.client.ExternalLinkService;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.ExternalLinkException;

/**
 * <class_description> External link RESTful service controller.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 16, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
public class ExternalLinkController {

    /**
     * External link patterns cache manager.
     */
    @Autowired
    private ExternalLinkPatternCacheService externalLinkPatternService;

    /**
     * External link generation manager.
     */
    @Autowired(required = true)
    private ExternalLinkService externalLinkService;

    /**
     * Generates an external link according to the document identifier and the language.
     * @param id the document identifier
     * @param lang the language code
     * @return the generated external link
     */
    @RequestMapping(value = "/mappingService/convert", method = RequestMethod.GET)
    public ResponseEntity<Object> convertExternalLink(@RequestParam(value = "id", required = true) final String id,
            @RequestParam(value = "lang", required = true) final String lang) {
        final ExternalLink externalLink = this.externalLinkService.convert(id, lang);

        if (externalLink == null)
            return ControllerUtil.makeErrorResponseObject(
                    ExceptionBuilder.get(ExternalLinkException.class).withMessage("Document id {} not found").withMessageArgs(id).build(),
                    HttpStatus.NOT_FOUND);

        return new ResponseEntity<Object>(externalLink, HttpStatus.OK);
    }

    /**
     * Gets the list of all the external link patterns.
     * @return the list of all the external link patterns
     */
    @RequestMapping(value = "/mappingService/patterns", method = RequestMethod.GET)
    public ResponseEntity<ExternalLinkPatterns> getExternalLinkPatterns() {
        return new ResponseEntity<ExternalLinkPatterns>(new ExternalLinkPatterns(this.externalLinkPatternService.getExternalLinkList()),
                HttpStatus.OK);
    }

    /**
     * ExternalLinkException handler.
     * @param exception the exception to handle
     * @return a XML error response with the details of the exception
     */
    @ExceptionHandler(ExternalLinkException.class)
    private ResponseEntity<Object> handleExternalLinkException(final ExternalLinkException exception) {
        return ControllerUtil.makeErrorResponseObject(exception, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
