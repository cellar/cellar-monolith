/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.security
 *             FILE : GroupTableDecorator.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 15, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.admin.security;

import java.util.Set;

import org.displaytag.decorator.TableDecorator;

import eu.europa.ec.opoce.cellar.cl.domain.admin.Group;
import eu.europa.ec.opoce.cellar.cl.domain.admin.Role;

/**
 * <class_description> Groups displaytag decorator.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 15, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class GroupTableDecorator extends TableDecorator {

    private final static String SEPARATOR = "; ";

    /**
     * Returns the roles in a string.
     * @return the roles in a string
     */
    public String getRoles() {
        final Set<Role> roles = ((Group) getCurrentRowObject()).getRoles();

        final StringBuilder rolesSb = new StringBuilder();

        for (Role r : roles) {
            rolesSb.append(r.getRoleName());
            rolesSb.append(SEPARATOR);
        }

        return rolesSb.toString();
    }
}
