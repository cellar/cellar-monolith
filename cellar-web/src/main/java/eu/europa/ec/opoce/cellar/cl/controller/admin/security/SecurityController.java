/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.security
 *             FILE : SecurityController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 14, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.admin.security;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;

/**
 * <class_description> Main security controller.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 14, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
@RequestMapping(value = SecurityController.PAGE_URL)
public class SecurityController extends AbstractCellarController {

    public static final String PAGE_URL = "/security";

    /**
     * Handle the GET.
     */
    @RequestMapping(method = RequestMethod.GET)
    public String securityGet(final ModelMap model) {
        return redirect(UserManagerController.PAGE_URL); // redirects to the user manager
    }
}
