/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.support.security
 *             FILE : CellarJdbcTokenRepositoryImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 5, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.support.security;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;

/**
 * <class_description> Cellar remember-me repository service. The service checks the database readonly property before the token processing.
 * If the database is in readonly mode, the processing is simply ignored and the remember-me functionality doesn't work.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 5, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class CellarJdbcTokenRepositoryImpl extends JdbcTokenRepositoryImpl {

    /**
     * Cellar configuration.
     */
    @Autowired
    @Qualifier("cellarConfiguration")
    protected ICellarConfiguration cellarConfiguration;

    /**
     * Default constructor.
     */
    public CellarJdbcTokenRepositoryImpl() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void createNewToken(final PersistentRememberMeToken token) {
        if (!this.cellarConfiguration.isCellarDatabaseReadOnly()) {
            super.createNewToken(token);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PersistentRememberMeToken getTokenForSeries(final String seriesId) {
        if (!this.cellarConfiguration.isCellarDatabaseReadOnly()) {
            return super.getTokenForSeries(seriesId);
        }

        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeUserTokens(final String username) {
        if (!this.cellarConfiguration.isCellarDatabaseReadOnly()) {
            super.removeUserTokens(username);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCreateTableOnStartup(final boolean createTableOnStartup) {
        super.setCreateTableOnStartup(createTableOnStartup);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateToken(final String series, final String tokenValue, final Date lastUsed) {
        if (!this.cellarConfiguration.isCellarDatabaseReadOnly()) {
            super.updateToken(series, tokenValue, lastUsed);
        }
    }
}
