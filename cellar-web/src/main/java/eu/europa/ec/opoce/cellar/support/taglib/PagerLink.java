package eu.europa.ec.opoce.cellar.support.taglib;

/**
 * Entity for a <code>Pager</code> link.
 * 
 * @author omeurice
 * 
 */
public class PagerLink {

    /**
     * Link value.
     */
    private int value;

    /**
     * Link label.
     */
    private String label;

    /**
     * Default constructor.
     */
    public PagerLink() {
    }

    /**
     * Create a new <code>Pager</code> instance.
     * 
     * @param value
     *            Link value.
     * @param label
     *            Link label.
     */
    public PagerLink(final int value, final String label) {
        super();
        this.value = value;
        this.label = label;
    }

    /**
     * Get the link value.
     * 
     * @return The link value.
     */
    public int getValue() {
        return value;
    }

    /**
     * Set the link value.
     * 
     * @param value
     *            The value to set.
     */
    public void setValue(final int value) {
        this.value = value;
    }

    /**
     * Get the link label.
     * 
     * @return Get the link label.
     */
    public String getLabel() {
        return label;
    }

    /**
     * Set the link label.
     * 
     * @param label
     *            The label to set.
     */
    public void setLabel(final String label) {
        this.label = label;
    }
}
