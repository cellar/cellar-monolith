/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.validator
 *             FILE : CreateEmbargoJobValidator.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 11, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.command.CreateEmbargoJob;
import eu.europa.ec.opoce.cellar.common.util.TimeUtils;

/**
 * <class_description> Embargo batch job command validator.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 11, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component("embargoJobValidator")
public class CreateEmbargoJobValidator extends CreateJobValidator {

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(final Class<?> clazz) {
        return CreateEmbargoJob.class.isAssignableFrom(clazz);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validate(final Object target, final Errors errors) {
        super.validate(target, errors);

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "embargoDate", "required.batch-job.embargo.job.embargoDate",
                "An embargo date is required.");

        final CreateEmbargoJob createdEmbargoJob = (CreateEmbargoJob) target;

        if (TimeUtils.getDefaultDateSilently(createdEmbargoJob.getEmbargoDate(), true) == null) {
            errors.rejectValue("embargoDate", "required.batch-job.embargo.job.embargoDate", "An embargo date is required.");
        }
    }
}
