/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.indexing
 *             FILE : EditConfigurationController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 24, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.indexing;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL;
import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService;
import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService.LinkedIndexingBehavior;
import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService.Priority;
import eu.europa.ec.opoce.cellar.cmr.indexing.service.IIndexRequestSchedulerService;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestGenerationService;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.indexing.command.EditConfiguration;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;

/**
 * <class_description> Edit indexing configuration controller.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 24, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
@RequestMapping(EditConfigurationController.PAGE_URL)
public class EditConfigurationController extends AbstractCellarController {

    /** The Constant PAGE_URL. */
    public static final String PAGE_URL = "/cmr/indexing/configuration";

    /** The Constant STOP_INDEXING_URL. */
    public static final String STOP_INDEXING_URL = "/stopIndexing";

    public static final String FORCE_INDEXING_URL = "/forceIndexing";

    /** The Constant JSP_NAME. */
    private static final String JSP_NAME = "cmr/indexing/configuration";

    /** The Constant OBJECT_NAME. */
    private static final String OBJECT_NAME = "configuration";

    /**
     * Indexing service.
     */
    @Autowired
    private IIndexingService indexingService;

    @Autowired
    private IIndexRequestSchedulerService indexRequestSchedulerService;

    @Autowired
    private CmrIndexRequestGenerationService cmrIndexRequestGenerationService;

    /**
     * Returns all behaviors available for the indexing of linked documents.
     * @return all the behaviors
     */
    @ModelAttribute("linkedIndexingBehaviorsList")
    public List<LinkedIndexingBehavior> populateLinkedIndexingBehaviorsList() {
        return Arrays.asList(LinkedIndexingBehavior.values());
    }

    /**
     * Returns all indexing priorities available.
     * @return all the priorities
     */
    @ModelAttribute("indexingPrioritiesList")
    public List<Priority> populateIndexingPrioritiesList() {
        return Arrays.asList(Priority.values());
    }

    /**
     * Handle the GET.
     *
     * @param model the model
     * @return the string
     */
    @RequestMapping(method = RequestMethod.GET)
    public String editConfigurationGet(final ModelMap model) {

        final EditConfiguration editConfiguration = new EditConfiguration();
        editConfiguration.setIndexingEnabled(this.indexingService.isIndexingEnabled());
        editConfiguration.setLinkedIndexingBehavior(this.cmrIndexRequestGenerationService.getLinkedIndexingBehavior());
        editConfiguration.setMinimalLevelOfPriorityHandled(this.indexingService.getMinimalLevelOfPriorityHandled());

        model.addAttribute(OBJECT_NAME, editConfiguration);

        return JSP_NAME;
    }

    /**
     * Handle the POST.
     *
     * @param editedConfiguration the edited configuration
     * @param bindingResult the binding result
     * @param status the status
     * @return the string
     */
    @RequestMapping(method = RequestMethod.POST)
    public String editConfigurationPost(@ModelAttribute(OBJECT_NAME) final EditConfiguration editedConfiguration,
            final BindingResult bindingResult, final SessionStatus status) {

        if (bindingResult.hasErrors()) { // validates the configuration
            return JSP_NAME;
        }

        // update only if the new value is different

        if (this.indexingService.isIndexingEnabled() != editedConfiguration.isIndexingEnabled()) {
            this.indexingService.setIndexingEnabled(editedConfiguration.isIndexingEnabled());
        }

        if (this.cmrIndexRequestGenerationService.getLinkedIndexingBehavior() != editedConfiguration.getLinkedIndexingBehavior()) {
            this.cmrIndexRequestGenerationService.setLinkedIndexingBehavior(editedConfiguration.getLinkedIndexingBehavior());
        }

        if (this.indexingService.getMinimalLevelOfPriorityHandled() != editedConfiguration.getMinimalLevelOfPriorityHandled()) {
            this.indexingService.setMinimalLevelOfPriorityHandled(editedConfiguration.getMinimalLevelOfPriorityHandled());
        }

        super.addUserMessage(LEVEL.SUCCESS, "common.message.successfullySaved");

        return redirect(PAGE_URL);
    }

    /**
     * Handle the stop indexing GET.
     *
     * @return the string
     */
    @RequestMapping(value = EditConfigurationController.STOP_INDEXING_URL, method = RequestMethod.GET)
    public String stopIndexingGet() {
        this.indexingService.setIndexingEnabled(false);
        super.addUserMessage(LEVEL.SUCCESS, "common.message.successfullyProcessed");
        return redirect(DashboardController.PAGE_URL);
    }

    /**
     * This mapping forces the indexation to start on all records older than NOW.
     */
    @RequestMapping(value = EditConfigurationController.FORCE_INDEXING_URL, method = RequestMethod.GET)
    public String forceIndexingGet() {
        this.indexRequestSchedulerService.forceScheduleIndexRequests();
        super.addUserMessage(LEVEL.SUCCESS, "common.message.successfullyProcessed");
        return redirect(DashboardController.PAGE_URL);
    }
}
