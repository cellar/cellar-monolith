/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.manager
 *             FILE : LastModificationDateJobManagerController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 7, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-06-07 07:49:00 +0200 (Wed, 07 Jun 2017) $
 *          VERSION : $LastChangedRevision: $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.manager;

import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * @author ARHS Developments
 */
@Controller
@RequestMapping(LastModificationDateJobManagerController.PAGE_URL)
public class LastModificationDateJobManagerController extends BatchJobManagerController {

    /** The Constant PAGE_URL. */
    public static final String PAGE_URL = "/cmr/update/job/last-modification-date";

    /** The Constant JSP_NAME. */
    private static final String JSP_NAME = "cmr/update/job/last-modification-date";

    @Override
    protected String getPageUrl() {
        return PAGE_URL;
    }

    @Override
    protected List<BatchJob> getBatchJobs() {
        return this.batchJobService.findBatchJobs(BatchJob.BATCH_JOB_TYPE.LAST_MODIFICATION_DATE_UPDATE);
    }

    @Override
    protected String getFunctionalityUrl() {
        return PAGE_URL;
    }

    @Override
    protected BatchJob.BATCH_JOB_TYPE getBatchJobType() {
        return BatchJob.BATCH_JOB_TYPE.LAST_MODIFICATION_DATE_UPDATE;
    }

    @Override
    protected String getJspName() {
        return JSP_NAME;
    }
}
