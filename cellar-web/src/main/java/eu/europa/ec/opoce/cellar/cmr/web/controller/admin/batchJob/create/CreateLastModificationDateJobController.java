/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create
 *             FILE : CreateLastModificationDateJobController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 7, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-06-07 07:49:00 +0200 (Wed, 07 Jun 2017) $
 *          VERSION : $LastChangedRevision: $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create;

import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.command.CreateJob;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.command.CreateLastModificationDateJob;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.validator.CreateLastModificationDateUpdateValidator;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.manager.LastModificationDateJobManagerController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;

/**
 * @author ARHS Developments
 */
@Controller
@RequestMapping("/cmr/update/job/last-modification-date/create")
public class CreateLastModificationDateJobController extends CreateJobController {

    private static final String JSP_NAME = "cmr/update/job/last-modification-date/create-job";

    private final CreateLastModificationDateUpdateValidator validator;

    @Autowired
    public CreateLastModificationDateJobController(CreateLastModificationDateUpdateValidator validator) {
        this.validator = validator;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String createJobGet(final ModelMap model) {
        model.addAttribute("job", new CreateLastModificationDateJob());
        return JSP_NAME;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String createJobPost(ModelMap model, @ModelAttribute("job") CreateLastModificationDateJob createdJob,
                                BindingResult bindingResult, SessionStatus status) {

        return super.createJob(model, createdJob, bindingResult, status);
    }

    @Override
    protected String getFunctionalityUrl() {
        return LastModificationDateJobManagerController.PAGE_URL;
    }

    @Override
    protected String getJspName() {
        return JSP_NAME;
    }

    @Override
    protected String getSuccessPageUrl() {
        return LastModificationDateJobManagerController.PAGE_URL;
    }

    @Override
    protected Validator getCreateJobValidator() {
        return validator;
    }

    @Override
    protected BatchJob constructBatchJob(CreateJob createJob) {
        final CreateLastModificationDateJob job = (CreateLastModificationDateJob) createJob;
        return BatchJob.create(BatchJob.BATCH_JOB_TYPE.LAST_MODIFICATION_DATE_UPDATE, job.getJobName(), job.getSparqlQuery()).build();
    }

    @Override
    protected BatchJob.BATCH_JOB_TYPE getBatchJobType() {
        return BatchJob.BATCH_JOB_TYPE.LAST_MODIFICATION_DATE_UPDATE;
    }
}
