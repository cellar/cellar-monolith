/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.edit
 *             FILE : EditJobController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 29, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.edit;

import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.common.BatchJobController;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.command.CreateJob;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.support.SessionStatus;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 29, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
public abstract class EditJobController extends BatchJobController {

    /**
     * Gets the success path url corresponding to the controller.
     * @return the success path url corresponding to the controller
     */
    protected abstract String getSuccessPageUrl();

    /**
     * Gets the batch job validator corresponding to the controller.
     * @return the batch job validator corresponding to the controller
     */
    protected abstract Validator getEditJobValidator();

    /**
     * Constructs the batch job based on a command instance.
     * @param createJob the command to import
     * @return the batch job
     */
    protected abstract BatchJob constructBatchJob(final CreateJob editedJob);

    /**
     * Create job generic logic:
     *  - Validate job
     *  - Construct batch job
     *  - Save the batch job
     *  - Add a success message and redirect
     * @param model the model of the corresponding view
     * @param createdJob the command
     * @param bindingResult the bind result of the command
     * @param status the session status
     * @return the JSP name or the redirect value
     */
    protected String editJob(final ModelMap model, final CreateJob editedJob, final BindingResult bindingResult,
            final SessionStatus status) {

        getEditJobValidator().validate(editedJob, bindingResult); // validates the created group

        if (bindingResult.hasErrors()) {
            return getJspName();
        }

        final BatchJob newBatchJob = constructBatchJob(editedJob);

        final BatchJob existingBatchJob = this.batchJobService.getBatchJob(newBatchJob.getId(), newBatchJob.getJobType());

        existingBatchJob.setJobName(newBatchJob.getJobName());
        existingBatchJob.setSparqlQuery(newBatchJob.getSparqlQuery());
        existingBatchJob.setSparqlUpdateQuery(newBatchJob.getSparqlUpdateQuery());
        existingBatchJob.setCron(newBatchJob.getCron());
        existingBatchJob.setMetadataOnly(newBatchJob.getMetadataOnly());
        existingBatchJob.setUseAgnosticURL(newBatchJob.getUseAgnosticURL());
        existingBatchJob.setDestinationFolder(newBatchJob.getDestinationFolder());

        this.batchJobService.updateBatchJob(existingBatchJob);

        super.addUserMessage(LEVEL.SUCCESS, "common.message.successfullySaved");
        return redirect(getSuccessPageUrl());
    }
}
