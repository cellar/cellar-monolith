/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.spring.user
 *        FILE : UserDetailsLoader.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 02-03-2023
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2023 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.security.user.details;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedGrantedAuthoritiesUserDetailsService;

import eu.cec.digit.ecas.client.j2ee.tomcat.EcasPrincipal;
import eu.europa.ec.opoce.cellar.cl.domain.admin.Role;
import eu.europa.ec.opoce.cellar.cl.domain.admin.User;
import eu.europa.ec.opoce.cellar.cl.service.client.SecurityService;
import eu.europa.ec.opoce.cellar.security.support.AuthenticationDetailsHelper;

/**
 * Contains functionality for associating a pre-authenticated EU-Login principal
 * with a CELLAR user.
 * 
 * @author EUROPEAN DYNAMICS S.A.
 */
public class UserDetailsLoader extends PreAuthenticatedGrantedAuthoritiesUserDetailsService {
	
	private static final Logger LOG = LogManager.getLogger(UserDetailsLoader.class);
	/**
	 * 
	 */
	private static final String USER_PASSWORD = "N/A";
	/**
	 *
	 */
	private static final String ROLE_SECURITY = "ROLE_SECURITY";
	/**
	 * Non-manually-assignable role for identifying pre-authenticated EU-Login principals that
	 * do not have a matching CELLAR user.
	 */
	private static final String ROLE_GUEST = "ROLE_GUEST";

	/**
	 * 
	 */
	private SecurityService securityService;
	
	
	@Autowired
	public UserDetailsLoader(SecurityService securityService) {
		this.securityService = securityService;
	}

	/**
	 * Creates and returns a UserDetails object based on the pre-authenticated EU-Login principal.
	 */
	@Override
	public UserDetails createUserDetails(Authentication token, Collection<? extends GrantedAuthority> authorities) {
		EcasPrincipal containerPrincipal = AuthenticationDetailsHelper.getAuthenticationDetails(token).getPrincipalDetails();
		String containerPrincipalName = token.getName();
		Set<String> containerAuthoritiesNames = getContainerAuthoritiesNames(authorities);
		
		// Retrieve the CELLAR user from the USERS table whose username matches the EU-Login principal name.
		User cellarUser = this.securityService.findUserByUsername(containerPrincipalName);
		if (cellarUser != null) {
			updateCellarUser(containerPrincipal, cellarUser);
			return createUserFromExistingCellarUser(cellarUser, containerAuthoritiesNames);
		}
		else if (containsRoleSecurity(containerAuthoritiesNames)) {
			LOG.warn("Unauthorized principal [{}] has been temporarily granted with {} access.", containerPrincipalName, ROLE_SECURITY);
			return createPrivilegedTransientUser(containerPrincipalName, containerAuthoritiesNames);
		}
		LOG.warn("Unauthorized principal [{}] has been temporarily granted with {} access.", containerPrincipalName, ROLE_GUEST);
		return createGuestTransientUser(containerPrincipalName);
	}
	
	private Set<String> getContainerAuthoritiesNames(Collection<? extends GrantedAuthority> authorities) {
		return authorities.stream()
				.map(GrantedAuthority::getAuthority)
				.collect(Collectors.toSet());
	}
	
	private Set<String> getCellarUserAuthoritiesNames(User cellarUser) {
		return cellarUser.getGroup().getRoles().stream()
				.map(Role::getRoleAccess)
				.collect(Collectors.toSet());
	}
	
	private Collection<GrantedAuthority> toGrantedAuthorities(Collection<String> authoritiesNames) {
		return authoritiesNames.stream()
				.map(SimpleGrantedAuthority::new)
				.collect(Collectors.toSet());
	}

	private boolean containsRoleSecurity(Collection<String> authoritiesNames) {
		return authoritiesNames.contains(ROLE_SECURITY);
	}
	
	/**
	 * Creates a new UserDetails object based on the CELLAR User retrieved from the USER table.
	 * The authorities granted to the UserDetails object is the union of the authorities assigned
	 * to the CELLAR user and those assigned to the matching EU-Login principal at the EU-Login/container level.
	 * @param cellarUser the CELLAR user retrieved from the USER table and corresponding to the EU-Login principal name.
	 * @param containerAuthoritiesNames the authorities assigned to the EU-Login principal at the EU-Login/container level
	 * @return the UserDetails.
	 */
	private org.springframework.security.core.userdetails.User createUserFromExistingCellarUser(User cellarUser, Collection<String> containerAuthoritiesNames) {
		Set<String> cellarAuthorities = getCellarUserAuthoritiesNames(cellarUser);
		cellarAuthorities.addAll(containerAuthoritiesNames);
		return new org.springframework.security.core.userdetails.User(
				cellarUser.getUsername(), USER_PASSWORD, cellarUser.isEnabled(), true, true, true, toGrantedAuthorities(cellarAuthorities));
	}
	
	/**
	 * Creates a new UserDetails object based solely on the EU-Login principal.
	 * The authorities granted to the UserDetails object are those assigned
	 * to the EU-Login principal at the EU-Login/container level.
	 * @param principalName the EU-Login principal name.
	 * @param containerAuthoritiesNames the authorities assigned to the EU-Login principal at the EU-Login/container level
	 * @return the UserDetails.
	 */
	private org.springframework.security.core.userdetails.User createPrivilegedTransientUser(String principalName, Collection<String> containerAuthoritiesNames) {
		return new org.springframework.security.core.userdetails.User(
				principalName, USER_PASSWORD, true, true, true, true, toGrantedAuthorities(containerAuthoritiesNames));
	}
	
	/**
	 * Creates a new UserDetails object based solely on the EU-Login principal.
	 * A single <code>ROLE_TRANSIENT</code> authority is granted so as to mark the principal
	 * as not having a matching CELLAR user.
	 * @param principalName the EU-Login principal name.
	 * @return the UserDetails.
	 */
	private org.springframework.security.core.userdetails.User createGuestTransientUser(String principalName) {
		return new org.springframework.security.core.userdetails.User(
				principalName, USER_PASSWORD, true, true, true, true, toGrantedAuthorities(Arrays.asList(ROLE_GUEST)));
	}
	
	/**
	 * Updates the email of the existing CELLAR user with the email of the matching EU-Login principal,
	 * and sets the user's password to {@code null}.
	 * @param containerPrincipal the EU-Login principal.
	 * @param cellarUser the CELLAR user to update.
	 */
	private void updateCellarUser(EcasPrincipal containerPrincipal, User cellarUser) {
		cellarUser.setPassword(null);
		cellarUser.setEmail(containerPrincipal.getEmail());
		this.securityService.updateUser(cellarUser);
	}
	
}
