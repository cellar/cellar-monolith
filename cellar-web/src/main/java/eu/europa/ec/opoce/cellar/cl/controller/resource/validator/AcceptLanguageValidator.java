package eu.europa.ec.opoce.cellar.cl.controller.resource.validator;

import eu.europa.ec.opoce.cellar.cl.controller.resource.validator.client.SimpleContentRequestValidator;
import eu.europa.ec.opoce.cellar.cl.controller.util.ControllerUtil;
import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;
import eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.nal.service.LanguageService;
import eu.europa.ec.opoce.cellar.server.dissemination.AcceptLanguage;
import eu.europa.ec.opoce.cellar.server.dissemination.DisseminationRequestUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpServerErrorException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

/**
 * Validator for the content request that validates the Accept-Language header parameter.
 */
//@Component
public class AcceptLanguageValidator implements SimpleContentRequestValidator {

    /**
     * Give access to CELLAR resources in the persistence layer.
     */
    @Autowired
    private CellarResourceDao cellarResourceDao;

    /**
     * Give access to the PID tables.
     */
    @Autowired
    @Qualifier("pidManagerService")
    private PidManagerService pidManagerService;

    /**
     * Utility class that returns language codes in different available formats.
     */
    @Autowired
    private LanguageService languageService;

    /**
     * {@inheritDoc}
     */
    @Override
    public HttpStatus validate(final HttpServletRequest request, final HttpServletResponse response,
            final CellarIdentifier resolveToCellarId, final String datastreamId, CellarResource resource) throws Exception {
        List<AcceptLanguage> acceptLanguages = DisseminationRequestUtils.parseAcceptLanguageHeader(request);

        if (acceptLanguages.isEmpty())
            return HttpStatus.OK;

        List<String> expressionLanguages = getExpressionLanguages(resolveToCellarId.getUuid());
        for (AcceptLanguage acceptLanguage : acceptLanguages) {
            LanguageBean languageBean = languageService.getByTwoOrThreeChar(acceptLanguage.getLanguageCode());
            if (languageBean != null && expressionLanguages.contains(languageBean.getIsoCodeThreeChar())) {
                return HttpStatus.OK;
            }
        }

        ControllerUtil.setLastModifiedDateResponse(response, null);
        ControllerUtil.setExpiredDateResponse(response, Calendar.getInstance(TimeZone.getTimeZone("GMT-0")).getTime());
        ControllerUtil.setResponseError(response,
                new HttpServerErrorException(HttpStatus.BAD_REQUEST,
                        "Header language(s)[" + StringUtils.join(acceptLanguages, ", ")
                                + "] \nis/are not compliant with the resource language [" + StringUtils.join(expressionLanguages, ", ")
                                + "] corresponding to this identifier : " + resolveToCellarId.getUuid()),
                HttpStatus.BAD_REQUEST);
        return HttpStatus.BAD_REQUEST;
    }

    /**
     * Get the languages of an expression
     * 
     * @param uuid
     *            Expression or manifestation CELLAR id.
     * @return A list of languages.
     */
    private List<String> getExpressionLanguages(final String uuid) {
        List<String> languages = null;
        String pid = uuid;
        if (pidManagerService.isManifestation(pid)) {
            pid = StringUtils.substringBeforeLast(pid, ".");
        }

        if (pidManagerService.isExpressionOrEvent(pid)) {
            CellarResource cellarResource = cellarResourceDao.findCellarId(pid);
            languages = cellarResource.getLanguages();
        }
        return languages;
    }
}
