/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob
 *             FILE : ConsultJobController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 13, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.consult;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import eu.europa.ec.opoce.cellar.cl.controller.admin.error.PageNotFoundController;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJobWorkIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.common.CellarPaginatedList;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.common.BatchJobController;

/**
 * <class_description> Consult batch job controller.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 13, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
public abstract class ConsultJobController extends BatchJobController {

    private final static String OBJECT_NAME = "batchJob";

    /**
     * Handle the GET.
     */
    @RequestMapping(method = RequestMethod.GET)
    public String editUserGet(final ModelMap model, @RequestParam final Long id, @RequestParam(required = false) final String workFilter,
            @RequestParam(required = false) final String dir, @RequestParam(required = false) final Integer page) {

        final BatchJob batchJob = this.batchJobService.getBatchJob(id, this.getBatchJobType());

        if (batchJob == null) {
            return PageNotFoundController.ERROR_VIEW; // job not found
        }

        // use the SQL wildcards
        String workFilterWilcards = "";
        if (workFilter != null) {
            workFilterWilcards = workFilter.replace('*', '%');
            workFilterWilcards = workFilterWilcards.replace('?', '_');
        }

        final CellarPaginatedList<BatchJobWorkIdentifier> workIdentifiersList = new CellarPaginatedList<BatchJobWorkIdentifier>(page,
                "workId", dir, workFilterWilcards);

        this.batchJobService.findWorkIdentifiers(id, this.getBatchJobType(), workIdentifiersList);

        model.addAttribute("workIdentifiersList", workIdentifiersList);
        model.addAttribute(OBJECT_NAME, batchJob);

        return this.getJspName();
    }
}
