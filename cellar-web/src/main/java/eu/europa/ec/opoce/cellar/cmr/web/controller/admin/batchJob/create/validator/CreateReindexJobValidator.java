/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.validator
 *             FILE : CreateReindexJobValidator.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 11, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.validator;

import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.command.CreateReindexJob;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

/**
 * <class_description> Reindex batch job command validator.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 11, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component("reindexJobValidator")
public class CreateReindexJobValidator extends CreateJobValidator {

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(final Class<?> clazz) {
        return CreateReindexJob.class.isAssignableFrom(clazz);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validate(final Object target, final Errors errors) {
        super.validate(target, errors);

        final CreateReindexJob createdReindexJob = (CreateReindexJob) target;

        if (!createdReindexJob.isGenerateEmbeddedNotice() && !createdReindexJob.isGenerateIndexNotice()) {
            errors.rejectValue("generateIndexNotice", "required.batch-job.reindex.job.generateIndexNotice",
                    "At least, a checkbox must be checked");
            errors.rejectValue("generateEmbeddedNotice", "required.batch-job.reindex.job.generateEmbeddedNotice",
                    "At least, a checkbox must be checked");
        }
    }
}
