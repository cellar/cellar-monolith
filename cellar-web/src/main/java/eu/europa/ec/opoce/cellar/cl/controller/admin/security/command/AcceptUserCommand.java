/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.security.command
 *        FILE : AcceptUserCommand.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 15-03-2023
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2023 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.admin.security.command;

/**
 * Form-backing object for the User Access Request Manager
 * functionality.
 * @author EUROPEAN DYNAMICS S.A
 */
public class AcceptUserCommand extends BaseUserCommand {

	/**
	 * The user access request ID.
	 */
	private Long userAccessRequestId;

	public AcceptUserCommand() {
		this.enabled = true;
	}
	
	public Long getUserAccessRequestId() {
		return userAccessRequestId;
	}

	public void setUserAccessRequestId(Long userAccessRequestId) {
		this.userAccessRequestId = userAccessRequestId;
	}
	
}
