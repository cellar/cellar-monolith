/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.support.dissemination.editor
 *             FILE : EncodedIdentifierEditor.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 16, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.support.dissemination.editor;

import eu.europa.ec.opoce.cellar.cl.controller.resource.AbstractResourceController;
import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;
import eu.europa.ec.opoce.cellar.server.service.DisseminationException;
import eu.europa.ec.opoce.cellar.server.service.DisseminationService;

import java.beans.PropertyEditorSupport;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.http.HttpStatus;
import org.springframework.web.util.UrlPathHelper;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Apr 16, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Configurable
public class CellarResourceBeanEditor extends PropertyEditorSupport {

    /**
    * the logger.
    */
    @SuppressWarnings("unused")
    private static final Logger LOG = Logger.getLogger(CellarResourceBeanEditor.class);

    /** The dissemination service. */
    @Autowired
    protected DisseminationService disseminationService;

    /** The request. */
    private final HttpServletRequest request;

    /** The url path helper. */
    private final UrlPathHelper urlPathHelper;

    /**
     * Instantiates a new cellar resource bean editor.
     *
     * @param request the request
     * @param urlPathHelper the url path helper
     */
    public CellarResourceBeanEditor(final HttpServletRequest request, final UrlPathHelper urlPathHelper) {
        this.request = request;
        this.urlPathHelper = urlPathHelper;
    }

    /** {@inheritDoc} */
    @Override
    public void setAsText(final String prefix) {
        final String resourcePath = this.urlPathHelper.getLookupPathForRequest(this.request);
        final String identifier = AbstractResourceController.getIdentifier(resourcePath, prefix);

        if (StringUtils.isBlank(identifier)) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage("Resource identifier part is missing.").build();
        }

        setValue(resolveCellarResourceBean(prefix, identifier));
    }

    /**
     * Resolve cellar resource bean.
     *
     * @param prefix the prefix
     * @param identifier the identifier
     * @return the cellar resource bean
     */
    private CellarResourceBean resolveCellarResourceBean(final String prefix, final String identifier) {
        final CellarResourceBean cellarResource = this.disseminationService.resolveCellarResourceBean(prefix, identifier);
        return cellarResource;
    }

}
