/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.resource
 *             FILE : ContentStreamResourceController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : May 14, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.resource;

import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.DisseminationMethod;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.DisseminationRequest;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.Negotiate;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.NegotiationRequest;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;
import eu.europa.ec.opoce.cellar.server.service.DisseminationException;
import eu.europa.ec.opoce.cellar.support.annotation.DisseminationMethodBinder;
import eu.europa.ec.opoce.cellar.support.dissemination.editor.CellarResourceBeanEditor;
import eu.europa.ec.opoce.cellar.support.dissemination.editor.NegotiationRequestEditor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.HttpHeaders;
import java.util.regex.Matcher;

/**
 * <class_description> This is a helper class to deal with content stream
 * requests based on the cellar ID. For a cellar content stream ID, there are 2
 * requests possible:<br>
 * a) get the actual content stream, ex: curl
 * "http://cellar-dev.publications.europa.eu/resource/cellar/55473516-da9c-11e3-920c-01aa75ed71a1.0002.01/DOC_1"
 * <br>
 * b) get the identifiers (i.e.: sameAs resources) of the item, ex: curl -H
 * "Accept:application/xml;notice=identifiers"
 * "http://cellar-dev.publications.europa.eu/resource/cellar/55473516-da9c-11e3-920c-01aa75ed71a1.0001.01/DOC_1"
 * -L<br>
 * Both cases uses exactly the same URL
 * (.../cellar/{identifier}/{datastreamId:.+}), so the URL cannot be used of for
 * distinguishing between both requests. Unfortunately, Spring is not able to
 * evaluate the complete header String 'application/xml;notice=identifiers'; it
 * treats ';' and '=' as separators so that an automatic header based
 * differentiation is not applicable.<br>
 * Thus, this class was set up to manage the differentiation between the
 * requests.
 * <p>
 * <br/>
 * <br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc. <br/>
 * <br/>
 * ON : May 14, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
public class DataStreamResourceController extends AbstractResourceController {

    @Autowired
    private RedirectResourceController redirectResourceController;

    @Autowired
    private RetrieveResourceController retrieveResourceController;

    @RequestMapping(method = {RequestMethod.GET, RequestMethod.HEAD},
            value = "/" + ContentIdentifier.CELLAR_PREFIX + "/{identifier}/{datastreamId:.+}")
    public ResponseEntity<?> handleContentRequestWithCellarID(final HttpServletRequest request,
                                                              final HttpServletResponse response,
                                                              @PathVariable final String identifier,
                                                              @PathVariable final String datastreamId,
                                                              @DisseminationMethodBinder final DisseminationMethod method,
                                                              @RequestHeader(value = HttpHeaders.ACCEPT, required = false, defaultValue = "*/*") final String accept,
                                                              @RequestHeader(value = HttpHeaders.IF_NONE_MATCH, required = false) final String eTag,
                                                              @RequestHeader(value = HttpHeaders.IF_MODIFIED_SINCE, required = false) final String dateVersion)
            throws Exception {

        return this.internalHandleContentRequestWithCellarID(request, response, identifier, datastreamId, null, method, accept, eTag, dateVersion);
    }

    /**
     * Couldn't find a way to avoid the duplication of methods by using Optional to express non-mandatory path variables,
     * as explained <a href="https://goo.gl/rzdtkV">here</a>.<br>
     * Whoever has some time to spare, please try to fix it.
     */
    @RequestMapping(method = {RequestMethod.GET, RequestMethod.HEAD},
            value = "/" + ContentIdentifier.CELLAR_PREFIX + "/{identifier}/{datastreamId:.+}/{compressedEntryName}")
    public ResponseEntity<?> handleContentRequestWithCellarID(final HttpServletRequest request,
                                                              final HttpServletResponse response,
                                                              @PathVariable final String identifier,
                                                              @PathVariable final String datastreamId,
                                                              @PathVariable final String compressedEntryName,
                                                              @DisseminationMethodBinder final DisseminationMethod method,
                                                              @RequestHeader(value = HttpHeaders.ACCEPT, required = false, defaultValue = "*/*") final String accept,
                                                              @RequestHeader(value = HttpHeaders.IF_NONE_MATCH, required = false) final String eTag,
                                                              @RequestHeader(value = HttpHeaders.IF_MODIFIED_SINCE, required = false) final String dateVersion)
            throws Exception {

        return this.internalHandleContentRequestWithCellarID(request, response, identifier, datastreamId, compressedEntryName, method, accept, eTag, dateVersion);
    }

    /**
     * Spring is not able to check the Accept tokens (i.e.: the part behind the
     * semicolon) of an Accept header. This we have no other choice than using
     * this approach with the if statement
     */
    private ResponseEntity<?> internalHandleContentRequestWithCellarID(final HttpServletRequest request,
                                                                       final HttpServletResponse response,
                                                                       final String identifier,
                                                                       final String datastreamId,
                                                                       final String compressedEntryName,
                                                                       final DisseminationMethod method,
                                                                       final String accept,
                                                                       final String eTag,
                                                                       final String dateVersion)
            throws Exception {

        // redirect
        if (isInitialRequestForContentStreamIdentifiers(accept)) {
            return this.redirectContentStreamIdentifiersRequest(request, response, accept,
                    method.isResponseBodyNeeded());
        }
        // retrieve
        else {
            this.retrieveResourceController.getContentResource(request, response, identifier, datastreamId,
                    compressedEntryName, method.isResponseBodyNeeded(), eTag, dateVersion);
            return null;
        }
    }

    private static boolean isInitialRequestForContentStreamIdentifiers(final String acceptHeaderValue) {
        final Matcher matcher = DisseminationRequest.IDENTIFIER_XML.getPattern().matcher(acceptHeaderValue);
        return matcher.matches();
    }

    private ResponseEntity<?> redirectContentStreamIdentifiersRequest(final HttpServletRequest request,
                                                                      final HttpServletResponse response, final String accept, final boolean isResponseBodyNeeded) {

        if (!isResponseBodyNeeded) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class)
                    .withHttpStatus(HttpStatus.METHOD_NOT_ALLOWED)
                    .withMessage("HEAD method not supported for getting identifiers of a content stream.").build();
        }

        final CellarResourceBeanEditor cellarResourceBeanEditor = new CellarResourceBeanEditor(request,
                this.getUrlPathHelper());
        cellarResourceBeanEditor.setAsText(ContentIdentifier.CELLAR_PREFIX);

        final NegotiationRequestEditor negotiationRequestEditor = new NegotiationRequestEditor(request);
        negotiationRequestEditor.setAsText(accept);

        final NegotiationRequest negotiationRequest = (NegotiationRequest) negotiationRequestEditor.getValue();
        final CellarResourceBean cellarResource = (CellarResourceBean) cellarResourceBeanEditor.getValue();
        final String decoding = null;
        return this.redirectResourceController.getResource(request, cellarResource, negotiationRequest, decoding, null,
                Negotiate.OTHER, null);
    }
}
