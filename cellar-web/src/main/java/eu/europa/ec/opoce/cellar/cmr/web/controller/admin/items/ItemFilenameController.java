package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.items;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.items.form.Filename;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.items.form.FilenameForm;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.items.form.SearchForm;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.items.service.ItemFilenameService;
import eu.europa.ec.opoce.cellar.common.util.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @author ARHS Developments
 */
@Controller
public class ItemFilenameController extends AbstractCellarController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ItemFilenameController.class);
    public static final String PAGE_URL = "/items";
    private static final String JSP_NAME = "items";
    private static final String SEARCH_FORM = "search_form";
    private static final String FILENAME_FORM = "filename_form";

    private final ItemFilenameService itemFilenameService;

    @Autowired
    public ItemFilenameController(final ItemFilenameService itemFilenameService) {
        this.itemFilenameService = itemFilenameService;
    }

    @RequestMapping(value = ItemFilenameController.PAGE_URL,method = RequestMethod.GET)
    public String home(final ModelMap model) {
        model.addAttribute(SEARCH_FORM, new SearchForm());
        return JSP_NAME;
    }

    @RequestMapping(value = ItemFilenameController.PAGE_URL+"/search",method = RequestMethod.POST)
    public String search(final @ModelAttribute(SEARCH_FORM) SearchForm searchForm,
                         final BindingResult bindingResult,
                         final ModelMap model) {
        LOGGER.debug("Searching for items of '{}'", searchForm);
        model.addAttribute(SEARCH_FORM, searchForm);

        if (StringUtils.isBlank(searchForm.getIdentifier())) {
            super.addUserMessage(ICellarUserMessage.LEVEL.ERROR, "items.message.empty", "Identifier is empty");
            return JSP_NAME;
        }

        try {
            final List<Filename> filenames = getFilenames(searchForm);
            model.addAttribute(FILENAME_FORM, new FilenameForm(filenames));
        } catch (IllegalArgumentException e) {
            super.addUserMessage(ICellarUserMessage.LEVEL.ERROR, "items.message.not-found", "Not found");
        }

        return JSP_NAME;
    }

    private List<Filename> getFilenames(final SearchForm searchForm) {
        final String id = searchForm.getIdentifier();

        if (searchForm.isFilterIdenticalSuggestion()) {
            return itemFilenameService.getFilteredFilenames(id, searchForm.getPage());
        } else {
            return itemFilenameService.getFilenames(id, searchForm.getPage());
        }
    }

    @RequestMapping(value = ItemFilenameController.PAGE_URL+"/save",method = RequestMethod.POST)
    public String save(final @ModelAttribute(FILENAME_FORM) FilenameForm filenameForm,
                       final ModelMap model) {
        LOGGER.debug("Saving filenames: ", filenameForm.getFilenames());
        model.addAttribute(SEARCH_FORM, new SearchForm());
        model.addAttribute(FILENAME_FORM, filenameForm);

        try {
            final List<Filename> updatedFilenames = itemFilenameService.updateItemFilenames(filenameForm.getFilenames());
            filenameForm.getFilenames().removeAll(updatedFilenames);

            super.addUserMessage(ICellarUserMessage.LEVEL.SUCCESS, "items.message.success", "Success");
        } catch (Exception e) {
            super.addUserMessage(ICellarUserMessage.LEVEL.ERROR, null, e.getMessage());
        }

        return JSP_NAME;
    }

}
