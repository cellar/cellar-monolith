package eu.europa.ec.opoce.cellar.cl.controller.admin;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;

/**
 * <class_description> Admin home controller.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 29, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
public class AdminController extends AbstractCellarController {

    /**
     * Handle the / GET.
     */
    @RequestMapping(method = RequestMethod.GET, value = "/")
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {

        return new ModelAndView("redirect:/admin/dashboard");
    }
}
