/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.support.dissemination.editor
 *             FILE : NegotiationRequestEditor.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : May 8, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.support.dissemination.editor;

import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.server.dissemination.AcceptLanguage;
import eu.europa.ec.opoce.cellar.server.dissemination.SortedAcceptLanguages;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.AcceptNegotiationRequest;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.NegotiationRequest;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.param.DisseminationRequestParameter;
import eu.europa.ec.opoce.cellar.server.service.DisseminationException;

import java.beans.PropertyEditorSupport;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.HttpHeaders;

import org.springframework.http.HttpStatus;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : May 8, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class NegotiationRequestEditor extends PropertyEditorSupport {

    private final HttpServletRequest request;

    public NegotiationRequestEditor(final HttpServletRequest request) {
        this.request = request;
    }

    @Override
    public void setAsText(final String accept) {

        //TODO: please improve

        final String acceptLanguage = this.request.getHeader(HttpHeaders.ACCEPT_LANGUAGE);

        /*
         * Accept header
         */
        final List<DisseminationRequestParameter> disseminationRequests = new ArrayList<DisseminationRequestParameter>();
        final String[] textDisseminationRequests = accept.split(",");
        for (final String textDisseminationRequest : textDisseminationRequests) {
            final DisseminationRequestParameter disseminationRequestParameter = DisseminationRequestParameter
                    .parseAcceptHeader(textDisseminationRequest);
            disseminationRequests.add(disseminationRequestParameter);
        }
        Collections.sort(disseminationRequests, Collections.reverseOrder());

        /*
         * Accept-language header
         */
        SortedAcceptLanguages acceptLanguages = null;
        try {
            acceptLanguages = AcceptLanguage.parseAcceptLanguages(acceptLanguage);
            AcceptLanguage.sortByQualityValue(acceptLanguages);
        } catch (final Exception e) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withCause(e).withMessage("Illegal accept-language header: {}").withMessageArgs(e.getMessage()).build();
        }

        // build ordered list of dissemination requests

        final NegotiationRequest negotiationRequest = new NegotiationRequest();

        //the negotiationRequest object is a linked list that keeps the insertion order, the order must be kept as is
        // first iterate each language and the negotiation after
        if ((acceptLanguage == null) || acceptLanguage.isEmpty() || acceptLanguages.isEmpty()) {
            for (final DisseminationRequestParameter drp : disseminationRequests) {
                negotiationRequest.add(new AcceptNegotiationRequest(null, drp));
            }
        } else {
            for (final AcceptLanguage al : acceptLanguages) {
                for (final DisseminationRequestParameter drp : disseminationRequests) {
                    negotiationRequest.add(new AcceptNegotiationRequest(al, drp));
                }
            }
        }

        this.setValue(negotiationRequest);
    }
}
