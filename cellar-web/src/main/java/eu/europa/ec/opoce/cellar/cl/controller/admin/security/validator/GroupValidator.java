/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.security.validator
 *             FILE : GroupValidator.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 14, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.admin.security.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import eu.europa.ec.opoce.cellar.cl.controller.admin.security.command.CreateEditGroup;

/**
 * <class_description> Group validator.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 14, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component("groupValidator")
public class GroupValidator implements Validator {

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(final Class<?> clazz) {
        return CreateEditGroup.class.isAssignableFrom(clazz);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validate(final Object target, final Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "groupName", "range.security.group.groupName", "Group name is required.");

        final CreateEditGroup group = (CreateEditGroup) target;

        if (group.getRoles() == null || group.getRoles().isEmpty()) {
            errors.rejectValue("roles", "required.security.group.roles", "At least one selected role is required.");
        }
    }

}
