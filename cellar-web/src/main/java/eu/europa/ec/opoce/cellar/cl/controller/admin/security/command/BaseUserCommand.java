/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.security.command
 *        FILE : BaseUserCommand.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 15-03-2023
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2023 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.admin.security.command;

/**
 * Base form-backing object for user-related functionalities.
 * @author EUROPEAN DYNAMICS S.A
 */
public abstract class BaseUserCommand {

    /**
     * The user name.
     */
    protected String username;
    /**
     * The group of the user.
     */
    protected Long groupId;
    /**
     * Indicates if the user account is enabled.
     */
    protected boolean enabled;
    
	public String getUsername() {
		return username;
	}
	
	public Long getGroupId() {
		return groupId;
	}
	
	public boolean isEnabled() {
		return enabled;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
}
