/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.unauthorized
 *        FILE : UserCredentialsValidator.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 15-03-2023
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2023 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.unauthorized;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * Performs validation of the <code>UserCredentialsCommand</code>
 * form-backing object.
 * @author EUROPEAN DYNAMICS S.A.
 */
@Component
public class UserCredentialsValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return UserCredentialsCommand.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "user.migration.cellar.user.username.blank", "");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "user.migration.cellar.user.password.blank", "");
	}

}
