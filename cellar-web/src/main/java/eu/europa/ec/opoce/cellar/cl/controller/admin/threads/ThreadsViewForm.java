package eu.europa.ec.opoce.cellar.cl.controller.admin.threads;

import java.io.Serializable;

/**
 * The Class ThreadsViewForm.
 */
public class ThreadsViewForm implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -3578661366992341426L;

    /** The ingestion active. */
    private Boolean ingestionActive;

    /** The indexation active. */
    private Boolean indexationActive;

    /**
     * Gets the indexation active.
     *
     * @return the indexation active
     */
    public Boolean getIndexationActive() {
        return this.indexationActive;
    }

    /**
     * Gets the ingestion active.
     *
     * @return the ingestion active
     */
    public Boolean getIngestionActive() {
        return this.ingestionActive;
    }

    /**
     * Sets the indexation active.
     *
     * @param indexationActive the new indexation active
     */
    public void setIndexationActive(final Boolean indexationActive) {
        this.indexationActive = indexationActive;
    }

    /**
     * Sets the ingestion active.
     *
     * @param ingestionActive the new ingestion active
     */
    public void setIngestionActive(final Boolean ingestionActive) {
        this.ingestionActive = ingestionActive;
    }

}
