/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.security
 *             FILE : EditGroupController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 11, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.admin.security;

import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.admin.error.PageNotFoundController;
import eu.europa.ec.opoce.cellar.cl.controller.admin.security.command.CreateEditGroup;
import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL;
import eu.europa.ec.opoce.cellar.cl.domain.admin.Group;
import eu.europa.ec.opoce.cellar.cl.domain.admin.Role;
import eu.europa.ec.opoce.cellar.cl.service.client.SecurityService;

/**
 * <class_description> Edit a group controller.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 11, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
@RequestMapping(EditGroupController.PAGE_URL)
public class EditGroupController extends AbstractCellarController {

    public static final String PAGE_URL = "/security/group/edit";

    private static final String JSP_NAME = "security/group/edit";

    private static final String OBJECT_NAME = "group";

    /**
     * Security service manager.
     */
    @Autowired
    private SecurityService securityService;

    /**
     * Validator for a created/edited group.
     */
    @Autowired
    @Qualifier("groupValidator")
    private Validator groupValidator;

    /**
     * Returns all the roles.
     * @return all the roles
     */
    @ModelAttribute("rolesList")
    public List<Role> populateRolesList() {
        return this.securityService.findAllRoles();
    }

    /**
     * Handle the GET.
     */
    @RequestMapping(method = RequestMethod.GET)
    public String editGroupGet(final ModelMap model, @RequestParam final Long id) {
        final Group group = this.securityService.findGroup(id); // gets the group

        if (group == null) {
            return PageNotFoundController.ERROR_VIEW;
        }

        final List<Long> roles = new LinkedList<Long>();

        for (Role role : group.getRoles()) {
            roles.add(role.getId());
        }

        final CreateEditGroup editGroup = new CreateEditGroup();
        editGroup.setGroupName(group.getGroupName());
        editGroup.setId(group.getId());
        editGroup.setRoles(roles);

        model.addAttribute(OBJECT_NAME, editGroup);

        return JSP_NAME;
    }

    /**
     * Handle the POST.
     */
    @RequestMapping(method = RequestMethod.POST)
    public String editGroupPost(@ModelAttribute(OBJECT_NAME) final CreateEditGroup editedGroup, final BindingResult bindingResult,
            final SessionStatus status) {

        this.groupValidator.validate(editedGroup, bindingResult); // validates the created group

        if (bindingResult.hasErrors()) {
            return JSP_NAME;
        }

        final Set<Role> roles = new LinkedHashSet<Role>();
        for (Long id : editedGroup.getRoles()) {
            roles.add(new Role(id));
        }

        final Group group = this.securityService.findGroup(editedGroup.getId());
        group.setGroupName(editedGroup.getGroupName());
        group.setRoles(roles);

        this.securityService.updateGroup(group);

        super.addUserMessage(LEVEL.SUCCESS, "common.message.successfullySaved");
        return redirect(GroupManagerController.PAGE_URL); // redirects to the group manager
    }
}
