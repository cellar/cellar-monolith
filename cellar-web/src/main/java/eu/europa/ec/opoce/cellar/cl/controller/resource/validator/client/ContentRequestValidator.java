package eu.europa.ec.opoce.cellar.cl.controller.resource.validator.client;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpStatus;

import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;

public interface ContentRequestValidator {

    HttpStatus validateContentRequest(HttpServletRequest request, HttpServletResponse response, CellarIdentifier id, String datastreamId)
            throws Exception;
}
