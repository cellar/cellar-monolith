/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.command
 *             FILE : CreateEmbargoJob.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 11, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.command;

/**
 * <class_description> Create embargo batch job command.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 11, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class CreateEmbargoJob extends CreateJob {

    /**
     * Embargo date used by the job.
     */
    private String embargoDate;

    /**
     * Default constructor.
     */
    public CreateEmbargoJob() {
        super();
    }

    /**
     * @return the embargoDate
     */
    public String getEmbargoDate() {
        return this.embargoDate;
    }

    /**
     * @param embargoDate the embargoDate to set
     */
    public void setEmbargoDate(final String embargoDate) {
        this.embargoDate = embargoDate;
    }

}
