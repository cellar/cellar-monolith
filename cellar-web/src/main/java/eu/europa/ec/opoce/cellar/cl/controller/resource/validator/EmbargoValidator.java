package eu.europa.ec.opoce.cellar.cl.controller.resource.validator;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.controller.resource.validator.client.SimpleContentRequestValidator;
import eu.europa.ec.opoce.cellar.cl.controller.util.ControllerUtil;
import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;
import eu.europa.ec.opoce.cellar.cl.exception.ValidationException;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.service.EmbargoService;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileNotFoundException;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

@Component
public class EmbargoValidator implements SimpleContentRequestValidator {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmbargoValidator.class);

    private final ICellarConfiguration cellarConfiguration;

    private final EmbargoService embargoService;

    @Autowired
    public EmbargoValidator(ICellarConfiguration cellarConfiguration, EmbargoService embargoService) {
        this.cellarConfiguration = cellarConfiguration;
        this.embargoService = embargoService;
    }

    @Override
    public HttpStatus validate(final HttpServletRequest request, final HttpServletResponse response,
                               final CellarIdentifier resolveToCellarId, final String datastreamId, final CellarResource resource) {
        if (this.cellarConfiguration.isCellarServiceDisseminationRetrieveEmbargoedResourceEnabled()) {
            return HttpStatus.OK;
        }

        final String uuid = resolveToCellarId.getUuid();

        try {
            if (embargoService.isUnderEmbargo(resource)) {
                decorateResponse(response, notFoundException(uuid));
                return HttpStatus.NOT_FOUND;
            } else {
                return HttpStatus.OK;
            }
        } catch (final Exception e) {
            LOGGER.warn("Failed to retrieve the embargo status of content : {}. Cause message : {}", uuid, e.getMessage());
            decorateResponse(response, embargoException(uuid, datastreamId, e));
            return HttpStatus.NOT_FOUND;
        }
    }

    private static void decorateResponse(final HttpServletResponse response, final Exception e) {
        ControllerUtil.setLastModifiedDateResponse(response, null);
        ControllerUtil.setExpiredDateResponse(response, getExpirationDate());
    }

    private static Date getExpirationDate() {
        return Calendar.getInstance(TimeZone.getTimeZone("GMT-0")).getTime();
    }

    private static FileNotFoundException notFoundException(final String uuid) {
        return new FileNotFoundException("Resource with Cellar ID '" + uuid + "' is not available");
    }

    private static ValidationException embargoException(final String uuid, final String ds, final Exception e) {
        return ExceptionBuilder
                .get(ValidationException.class)
                .withMessage("Unexpected exception when trying to check if the content [{}/{}] is under embargo")
                .withMessageArgs(uuid, ds)
                .withCause(e)
                .build();
    }
}
