/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.consult
 *             FILE : ConsultExportJobController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 2, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.consult;

import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob.BATCH_JOB_TYPE;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor.ExportBatchJobProcessor;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.manager.ExportJobManagerController;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * <class_description> Consult export job controller.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 2, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
@RequestMapping(ConsultExportJobController.PAGE_URL)
public class ConsultExportJobController extends ConsultJobController {

    /** The Constant PAGE_URL. */
    public static final String PAGE_URL = "/cmr/export/job/consult";

    /** The Constant JSP_NAME. */
    private static final String JSP_NAME = "cmr/export/job/consult-job";

    /** The Constant BASE_DESTINATION_FOLDER. */
    private static final String BASE_DESTINATION_FOLDER = "baseDestinationFolder";

    /** The Constant DEFAULT_FOLDER_NAME. */
    private static final String DEFAULT_FOLDER_NAME = "defaultFolderName";

    /**
     * {@inheritDoc}
     */
    @Override
    @RequestMapping(method = RequestMethod.GET)
    public String editUserGet(final ModelMap model, @RequestParam final Long id, @RequestParam(required = false) final String workFilter,
            @RequestParam(required = false) final String dir, @RequestParam(required = false) final Integer page) {
        model.addAttribute(BASE_DESTINATION_FOLDER, this.cellarConfiguration.getCellarFolderExportBatchJob());
        model.addAttribute(DEFAULT_FOLDER_NAME, ExportBatchJobProcessor.getDefaultFolderName(id));
        return super.editUserGet(model, id, workFilter, dir, page);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getFunctionalityUrl() {
        return ExportJobManagerController.PAGE_URL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BATCH_JOB_TYPE getBatchJobType() {
        return BATCH_JOB_TYPE.EXPORT;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getJspName() {
        return JSP_NAME;
    }
}
