package eu.europa.ec.opoce.cellar.cl.controller.webapi;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.util.ControllerUtil;
import eu.europa.ec.opoce.cellar.cl.service.client.ResponseService;
import eu.europa.ec.opoce.cellar.exception.NotImplementedExceptionBuilder;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;

/**
 * Controller handling production system operations.
 *
 * @author dsavares, phvdveld
 */
@Controller
public class CellarController extends AbstractCellarController {

	/**
	 * Logger for this class.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(CellarController.class);

	/**
	 * Service for generating a request response.
	 */
	@Autowired
	private ResponseService responseService;

	/**
	 * READ operation for the production systems.
	 *
	 * @param objectName
	 *            the name of the sipObject to read
	 * @return ResponseEntity instance.
	 */
	@RequestMapping(method = RequestMethod.PUT, value = "/metsRead/{objectName}")
	public ResponseEntity<String> read(@PathVariable final String objectName) {
		return ControllerUtil.makeErrorResponse(NotImplementedExceptionBuilder.get().build());
	}

	/**
	 * Handler for the PUT operation.
	 *
	 * @param sipObjectStream
	 *            the sipObject to update
	 * @param objectName
	 *            the name of the sipObject to update
	 * @return ResponseEntity instance.
	 */
	@RequestMapping(method = RequestMethod.PUT, value = "/metsUpdate/{objectName}")
	public ResponseEntity<String> update(@RequestBody final byte[] sipObjectStream,
			@PathVariable final String objectName) {
		return ControllerUtil.makeErrorResponse(NotImplementedExceptionBuilder.get().build());
	}

	/**
	 * Handler for the DELETE operation.
	 *
	 * @param objectName
	 *            the name of the sipObject to delete
	 * @return ResponseEntity instance.
	 */
	@RequestMapping(method = RequestMethod.DELETE, value = "/delete/{objectName}")
	public ResponseEntity<String> delete(@PathVariable final String objectName) {
		return ControllerUtil.makeErrorResponse(NotImplementedExceptionBuilder.get().build());
	}

	/**
	 * Handler for Retrieving the authenticOJ ingestion response for a specific
	 * mets id .
	 *
	 * @param response
	 *            HTTP Servlet response.
	 * @param metsId
	 *            METS document id.
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/getauthenticojresponse/{metsId}")
	public void getAuthenticOJResponse(final HttpServletResponse response, @PathVariable final String metsId) {
		this.getResponse(response, metsId, TYPE.AUTHENTICOJ);
	}

	/**
	 * Handler for Retrieving the daily ingestion response for a specific mets
	 * id .
	 *
	 * @param response
	 *            HTTP Servlet response.
	 * @param metsId
	 *            METS document id.
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/getdailyresponse/{metsId}")
	public void getDailyResponse(final HttpServletResponse response, @PathVariable final String metsId) {
		this.getResponse(response, metsId, TYPE.DAILY);
	}

	/**
	 * Handler for Retrieving the bulk ingestion response for a specific mets id
	 * .
	 *
	 * @param response
	 *            HTTP Servlet response.
	 * @param metsId
	 *            METS document id.
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/getbulkresponse/{metsId}")
	public void getBulkResponse(final HttpServletResponse response, @PathVariable final String metsId) {
		this.getResponse(response, metsId, TYPE.BULK);
	}
	
	/**
	 * Handler for Retrieving the low priority bulk ingestion response for a specific mets id
	 * .
	 *
	 * @param response
	 *            HTTP Servlet response.
	 * @param metsId
	 *            METS document id.
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/getbulklowpriorityresponse/{metsId}")
	public void getBulkLowPriorityResponse(final HttpServletResponse response, @PathVariable final String metsId) {
		this.getResponse(response, metsId, TYPE.BULK_LOWPRIORITY);
	}

	private void getResponse(final HttpServletResponse response, final String metsId, final TYPE type) {
		try {
			File sipFileResponse = null;

			switch (type) {
			case AUTHENTICOJ:
				sipFileResponse = responseService.getSIPAuthenticOJResponse(metsId);
				break;
			case DAILY:
				sipFileResponse = responseService.getSIPDailyResponse(metsId);
				break;
			case BULK:
				sipFileResponse = responseService.getSIPBulkResponse(metsId);
				break;
			case BULK_LOWPRIORITY:
				sipFileResponse = responseService.getSIPBulkLowPriorityResponse(metsId);
				break;
			}

			response.setContentType("application/zip");
			try(FileReader fr=new FileReader(sipFileResponse)) {
				OutputStream stream = response.getOutputStream();
				IOUtils.copy(fr, stream, (Charset) null);
				IOUtils.closeQuietly(stream);
			}
		} catch (IOException e) {
			try {
				LOGGER.warn("Execution of the service to return the sip response file for " + metsId + " has failed",
						e);
				response.sendError(HttpServletResponse.SC_NOT_FOUND);
			} catch (IOException ioe) {
				LOGGER.warn("Unable to send response");
			}
		}
	}
}
