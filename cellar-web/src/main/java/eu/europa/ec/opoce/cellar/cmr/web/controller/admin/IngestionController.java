/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin
 *        FILE : IngestionController.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 14-06-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.ingestion.service.QueueWatcher;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import static javax.ws.rs.core.HttpHeaders.CONTENT_TYPE;

/**
 * <class_description> Controller for the ingestion process.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 14-06-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller(value = "ingestionController")
public class IngestionController extends AbstractCellarController {

    @SuppressWarnings("unused")
    private static final Logger LOG = LogManager.getLogger(IngestionController.class);

    /** The sip watcher. */
    @Autowired
    private QueueWatcher queueWatcher;

    /**
     * Checks if is ingesting.
     *
     * @return the response entity
     */
    @RequestMapping(value = "/secapi/ingestion/isIngesting", method = RequestMethod.GET)
    public ResponseEntity<Boolean> isIngesting() {
        // check if the Cellar is currently ingesting
        boolean isIngesting = !this.queueWatcher.isIngestionTreatmentSetEmpty();

        // returns the response
        final HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add(CONTENT_TYPE, MediaType.TEXT_PLAIN + ";charset=UTF-8");
        return new ResponseEntity<Boolean>(isIngesting, responseHeaders, HttpStatus.OK);
    }

}
