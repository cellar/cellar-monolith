package eu.europa.ec.opoce.cellar.cl.controller.admin.contentVisibility;

import java.util.List;
import java.util.Map;

public class WorkContentStreams {

    private String workId;

    private List<String> workPids;

    private Map<String, List<ContentStream>> expressionContentIdListMap;

    public WorkContentStreams() {
    }

    public WorkContentStreams(final String workId, final List<String> workPids,
            final Map<String, List<ContentStream>> expressionContentIdListMap) {
        super();
        this.workId = workId;
        this.workPids = workPids;
        this.expressionContentIdListMap = expressionContentIdListMap;
    }

    public String getWorkId() {
        return workId;
    }

    public List<String> getWorkPids() {
        return workPids;
    }

    public Map<String, List<ContentStream>> getExpressionContentIdListMap() {
        return expressionContentIdListMap;
    }
}
