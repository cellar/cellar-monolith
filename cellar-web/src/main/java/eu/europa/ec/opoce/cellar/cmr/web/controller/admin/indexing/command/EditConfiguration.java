/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.indexing.command
 *             FILE : CreateEditConfiguration.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 24, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.indexing.command;

import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService.LinkedIndexingBehavior;
import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService.Priority;

/**
 * <class_description> Edit configuration command.
 *  This represents the bean used in the view form
 * <br/><br/>
 * ON : Jan 24, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class EditConfiguration {

    /**
     * Indicates if the indexing service is enabled.
     */
    private boolean indexingEnabled;

    /** The indexing behavior of linked documents. */
    private LinkedIndexingBehavior linkedIndexingBehavior;

    /**
     * The minimal level of priority handled by the indexing service.
     */
    private Priority minimalLevelOfPriorityHandled;

    /**
     * Default constructor.
     */
    public EditConfiguration() {

    }

    /**
     * Checks if is indexing enabled.
     *
     * @return the indexingEnabled
     */
    public boolean isIndexingEnabled() {
        return this.indexingEnabled;
    }

    /**
     * Sets the indexing enabled.
     *
     * @param indexingEnabled the indexingEnabled to set
     */
    public void setIndexingEnabled(final boolean indexingEnabled) {
        this.indexingEnabled = indexingEnabled;
    }

    /**
     * Gets the linked indexing behavior.
     *
     * @return the linkedIndexingBehavior
     */
    public LinkedIndexingBehavior getLinkedIndexingBehavior() {
        return this.linkedIndexingBehavior;
    }

    /**
     * Sets the linked indexing behavior.
     *
     * @param linkedIndexingBehavior the new linked indexing behavior
     */
    public void setLinkedIndexingBehavior(final LinkedIndexingBehavior linkedIndexingBehavior) {
        this.linkedIndexingBehavior = linkedIndexingBehavior;
    }

    /**
     * @return the minimalLevelOfPriorityHandled
     */
    public Priority getMinimalLevelOfPriorityHandled() {
        return minimalLevelOfPriorityHandled;
    }

    /**
     * @param minimalLevelOfPriorityHandled the minimalLevelOfPriorityHandled to set
     */
    public void setMinimalLevelOfPriorityHandled(Priority minimalLevelOfPriorityHandled) {
        this.minimalLevelOfPriorityHandled = minimalLevelOfPriorityHandled;
    }

    @Override
    public String toString() {
        return "EditConfiguration{" +
                "indexingEnabled=" + indexingEnabled +
                ", linkedIndexingBehavior=" + linkedIndexingBehavior +
                ", minimalLevelOfPriorityHandled=" + minimalLevelOfPriorityHandled +
                '}';
    }
}
