/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.manager
 *             FILE : UpdateJobManagerController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 2, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.manager;

import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob.BATCH_JOB_TYPE;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * <class_description> Update job manager controller.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 2, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
@RequestMapping(UpdateJobManagerController.PAGE_URL)
public class UpdateJobManagerController extends BatchJobManagerController {

    public static final String PAGE_URL = "/cmr/update/job/sparql-update";

    private static final String JSP_NAME = "cmr/update/job/sparql-update";

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getPageUrl() {
        return PAGE_URL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<BatchJob> getBatchJobs() {
        return this.batchJobService.findBatchJobs(BATCH_JOB_TYPE.UPDATE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getFunctionalityUrl() {
        return PAGE_URL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BATCH_JOB_TYPE getBatchJobType() {
        return BATCH_JOB_TYPE.UPDATE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getJspName() {
        return JSP_NAME;
    }
}
