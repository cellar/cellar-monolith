/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.resource
 *             FILE : ConsolidationResourceController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 15, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.resource;

import eu.europa.ec.opoce.cellar.common.http.headers.CellarHttpHeaders;
import eu.europa.ec.opoce.cellar.server.dissemination.consolidation.service.IConsolidationService;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.HttpHeaders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 15, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
@RequestMapping(value = "/consolidation")
public class ConsolidationResourceController extends AbstractResourceController {

    @Autowired
    private IConsolidationService consolidationService;

    @RequestMapping(value = "/memento/{identifier:.+}", method = {
            RequestMethod.GET})
    public ResponseEntity<?> getConsolidationMemento(//
            final HttpServletRequest request, //
            @PathVariable(value = "identifier") final String identifier, //
            @RequestParam(value = "rel", required = false) final String rel, //
            @RequestHeader(value = HttpHeaders.ACCEPT, required = false) final String accept, //
            @RequestHeader(value = CellarHttpHeaders.ACCEPT_DATETIME, required = false) final String acceptDatetime) {

        return this.consolidationService.getConsolidationMemento(identifier, rel, accept, acceptDatetime);
    }

    @RequestMapping(value = "/data/{identifier:.+}", method = {
            RequestMethod.GET})
    public ResponseEntity<?> getConsolidationData(//
            final HttpServletRequest request, //
            @PathVariable(value = "identifier") final String identifier) {

        return this.consolidationService.getConsolidationData(identifier);
    }

}
