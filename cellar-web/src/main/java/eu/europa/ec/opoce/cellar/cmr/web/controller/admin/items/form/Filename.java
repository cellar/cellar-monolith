package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.items.form;

import com.google.common.base.MoreObjects;

import java.util.List;
import java.util.Objects;

/**
 * @author ARHS Developments
 */
public class Filename {

    private String cellarId;
    private List<String> pids;
    private String filename;
    private String suggestedFilename;
    private String existingFilename;
    private String version; // S3 file version on S3 (CMR_CELLAR_RESOURCE_MD.FILE)

    public Filename() {
    }

    public Filename(final String cellarId, final String filename, final String suggestedFilename,
                    final List<String> pids, final String version) {
        this.cellarId = cellarId;
        this.filename = filename;
        this.suggestedFilename = suggestedFilename;
        this.existingFilename = filename;
        this.pids = pids;
        this.version = version;
    }

    public String getCellarId() {
        return cellarId;
    }

    public void setCellarId(final String cellarId) {
        this.cellarId = cellarId;
    }

    public List<String> getPids() {
        return pids;
    }

    public String getPidsStr() {
        return String.join(",", pids);
    }

    public void setPids(final List<String> pids) {
        this.pids = pids;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(final String filename) {
        this.filename = filename;
    }

    public String getSuggestedFilename() {
        return suggestedFilename;
    }

    public void setSuggestedFilename(final String suggestedFilename) {
        this.suggestedFilename = suggestedFilename;
    }

    public String getExistingFilename() {
        return existingFilename;
    }

    public void setExistingFilename(final String existingFilename) {
        this.existingFilename = existingFilename;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(final String version) {
        this.version = version;
    }

    public boolean filenameChanged() {
        return !Objects.equals(filename, existingFilename);
    }

    public boolean differFromSuggestion() {
        return !Objects.equals(filename, suggestedFilename);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("cellarId", cellarId)
                .add("filename", filename)
                .toString();
    }
}
