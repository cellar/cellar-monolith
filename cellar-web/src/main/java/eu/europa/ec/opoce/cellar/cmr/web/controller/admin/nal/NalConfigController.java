/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.nal
 *             FILE : NalConfigController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Sep 16, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$$
 *          VERSION : $LastChangedRevision$$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.nal;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cmr.database.dao.NalConfigDao;
import eu.europa.ec.opoce.cellar.common.Namespace;
import eu.europa.ec.opoce.cellar.configuration.OntologyConfiguration;
import eu.europa.ec.opoce.cellar.nal.dao.NalConfig;
import eu.europa.ec.opoce.cellar.nal.service.NalSnippetDbGateway;
import eu.europa.ec.opoce.cellar.server.admin.nal.NalAdminConfigurationService;
import eu.europa.ec.opoce.cellar.server.admin.nal.NalOntoVersion;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Set;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Sep 16, 2015
 *
 * @author ARHS Developments
 * @version $Revision$$
 */
@Controller
public class NalConfigController extends AbstractCellarController {
    private static final Logger LOG = LogManager.getLogger(NalConfigController.class);

    private NalAdminConfigurationService nalAdminConfigurationService;
    private OntologyConfiguration ontologyConfiguration;
    private NalConfigDao nalConfigDao;
    private NalSnippetDbGateway nalSnippetDbGateway;

    @Autowired
    public NalConfigController(NalAdminConfigurationService nalAdminConfigurationService, OntologyConfiguration ontologyConfiguration,
                               NalConfigDao nalConfigDao, NalSnippetDbGateway nalSnippetDbGateway) {
        this.nalAdminConfigurationService = nalAdminConfigurationService;
        this.ontologyConfiguration = ontologyConfiguration;
        this.nalConfigDao = nalConfigDao;
        this.nalSnippetDbGateway = nalSnippetDbGateway;
    }

    /**
     * Gets the nal config.
     *
     * @param request the request
     * @param response the response
     * @return the nal config
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @RequestMapping(value = "/cmr/nal/configurednals", method = RequestMethod.GET)
    public ModelAndView getNalConfig(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        final ModelAndView modelAndView = new ModelAndView("cmr/nal/configured-nals");
        modelAndView.addObject("nalOntologyUri", this.ontologyConfiguration.getNalOntologyUri());
        modelAndView.addObject("nalBaseUri", Namespace.at);
        final Set<NalOntoVersion> nalVersions = this.nalAdminConfigurationService.getNalVersions();
        modelAndView.addObject("versions", nalVersions);
        return modelAndView;
    }

    @RequestMapping(value = "/cmr/nal/delete", method = RequestMethod.POST)
    public ModelAndView deleteNal(@RequestParam(value = "name") String name) throws IOException {
        LOG.info("Starting delete of NAL '{}'", name);
        final ModelAndView modelAndView = new ModelAndView("cmr/nal/delete-report");
        modelAndView.getModel().put("operationResults", nalAdminConfigurationService.deleteNAL(name));
        modelAndView.getModel().put("nalName", name);
        LOG.info("Finished delete of NAL '{}'", name);
        return modelAndView;
    }

    @RequestMapping(value = "/cmr/nal/delete-all", method = RequestMethod.POST)
    public void deleteAllNal() {
        // there is some weird relations for eurovoc and we cannot delete it from NAL_SNIPPET because some records
        // stay in RELATED_SNIPPET, we need to first remove all RELATED_SNIPPET to be able to process the delete
        this.nalSnippetDbGateway.deleteAllRelatedSnippet();

        // properly delete the rest of the NAL using the delete process
        List<NalConfig> nalConfigList = nalConfigDao.findAll();
        if (nalConfigList != null) {
            int totalNalCount = nalConfigList.size();
            int deleteCount = 0;
            LOG.info("Starting the removal of {} NAL.", totalNalCount);
            for (NalConfig nalConfig : nalConfigList) {
                String nalName = nalConfig.getName();
                LOG.info("Starting delete of NAL '{}', uri : {}", nalName, nalConfig.getUri());
                nalAdminConfigurationService.deleteNAL(nalName);
                LOG.info("Finished delete of NAL '{}', uri : {} ({}/{}).", nalName, nalConfig.getUri(), ++deleteCount, totalNalCount);
            }
        }
    }
}
