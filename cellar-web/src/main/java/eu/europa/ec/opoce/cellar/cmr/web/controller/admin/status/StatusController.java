package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.status;

import eu.europa.ec.opoce.cellar.ccr.enums.status.StatusType;
import eu.europa.ec.opoce.cellar.ccr.service.client.StatusService;
import eu.europa.ec.opoce.cellar.ccr.utils.StatusServiceUtils;
import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.util.ControllerUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpStatusCodeException;

import java.io.IOException;
import java.time.LocalDateTime;

import static eu.europa.ec.opoce.cellar.ccr.utils.StatusServiceUtils.StatusServiceConstants.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * This Controller class acts as a proxy and handles all HTTP requests for Status Service operations.
 *
 * @author EUROPEAN DYNAMICS S.A.
 */
@Controller
@RequestMapping(CELLAR_STATUS_V1_BASE_URL)
public class StatusController extends AbstractCellarController {
    private static final Logger log = LogManager.getLogger(StatusController.class);

    public static final String CORRELATION_HEADER = "x-correlation-id";

    private final StatusService statusService;

    public StatusController(StatusService statusService) {
        this.statusService = statusService;
    }

    /**
     * Acts as a proxy and handles HTTP requests for retrieving a Status response at package-level.
     *
     * @param id         The id (UUID) of the resource.
     * @param ingestion  A boolean value specifying whether to include ingestion-specific status information in the response or not.
     * @param indexation A boolean value specifying whether to include indexation-specific status information in the response or not.
     * @param verbosity  An integer specifying the level of verbosity of a response, with 1 corresponding to a coarse level of granularity and 2 to a detailed level of information.
     * @return A {@link ResponseEntity} containing the response of the Status Service.
     */
    @GetMapping(value = CELLAR_STATUS_PACKAGE_ENDPOINT, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getStatusPackageLevel(
            @RequestParam(value = "id") final String id,
            @RequestParam(required = false, value = "ingestion", defaultValue = "false") final boolean ingestion,
            @RequestParam(required = false, value = "indexation", defaultValue = "false") final boolean indexation,
            @RequestParam(required = false, value = "verbosity", defaultValue = "1") final int verbosity) {
        try {
            return new ResponseEntity<>(statusService.getStatusResponse(id, ingestion, indexation, null, verbosity, StatusType.PACKAGE_LEVEL), HttpStatus.OK);
        } catch (Exception e) {
            return this.handleApiException(e);
        }
    }

    /**
     * Acts as a proxy and handles HTTP requests for retrieving a Status response at object-level.
     *
     * @param id         The id (PID) of the resource.
     * @param ingestion  A boolean value specifying whether to include ingestion-specific status information in the response or not.
     * @param indexation A boolean value specifying whether to include indexation-specific status information in the response or not.
     * @param datetime   The datetime acts as a filter on the time the corresponding package was received. Cellar returns
     *                   the latest status information less than or equal the given timestamp. If not specified, the latest status information for the object is returned.
     * @param verbosity  An integer specifying the level of verbosity of a response, with 1 corresponding to a coarse level of granularity and 2 to a detailed level of information.
     * @return A {@link ResponseEntity} containing the response of the Status Service.
     */
    @GetMapping(value = CELLAR_STATUS_OBJECT_ENDPOINT, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getStatusObjectLevel(
            @RequestParam(value = "id") final String id,
            @RequestParam(required = false, value = "ingestion", defaultValue = "false") final boolean ingestion,
            @RequestParam(required = false, value = "indexation", defaultValue = "false") final boolean indexation,
            @RequestParam(required = false, value = "datetime") @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") final LocalDateTime datetime,
            @RequestParam(required = false, value = "verbosity", defaultValue = "1") final int verbosity) {
        try {
            return new ResponseEntity<>(statusService.getStatusResponse(id, ingestion, indexation, datetime, verbosity, StatusType.OBJECT_LEVEL), HttpStatus.OK);
        } catch (Exception e) {
            return this.handleApiException(e);
        }
    }

    /**
     * Acts as a proxy and handles HTTP requests for retrieving a Status response at object-level.
     *
     * @param id The name of the input package.
     * @return A {@link ResponseEntity} containing the response of the Status Service.
     */
    @GetMapping(value = CELLAR_STATUS_LOOKUP_ENDPOINT, produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getStatusLookup(@RequestParam(value = "id") final String id) {
        try {
            return new ResponseEntity<>(statusService.getStatusResponse(id, false, false, null, 0, StatusType.LOOKUP), HttpStatus.OK);
        } catch (Exception e) {
            return this.handleApiException(e);
        }
    }

    private ResponseEntity<String> handleApiException(Exception e) {

        // connectivity issues exception
        if (e.getCause() instanceof IOException) {
            log.error(STATUS_SERVICE_API_EXCEPTION_LOG_MESSAGE_WITH_REASON, "Status Service is unreachable or currently unavailable.", e);
            return ControllerUtil.makeJsonErrorResponse(e, HttpStatus.GATEWAY_TIMEOUT);
        }

        // Error response from Status Service exception
        if (e instanceof HttpStatusCodeException) {
            //proxy forward the response from Status Service (response body response, http headers and http status code)
            HttpStatusCodeException hsce = (HttpStatusCodeException) e;
            HttpHeaders correlationHeader = StatusServiceUtils.extractHeader(hsce.getResponseHeaders(), CORRELATION_HEADER);
            String correlationHeaderValue = StatusServiceUtils.extractHeaderValue(correlationHeader, CORRELATION_HEADER);

            log.error(STATUS_SERVICE_API_EXCEPTION_LOG_MESSAGE_WITH_REASON_AND_CORRELATION_ID, "Status Service responded with an error response.", correlationHeaderValue, e);

            return new ResponseEntity<>(hsce.getResponseBodyAsString(), correlationHeader, hsce.getStatusCode());
        }

        // fallback-generic exception
        log.error(STATUS_SERVICE_API_EXCEPTION_LOG_MESSAGE_WITH_REASON, "A generic error occurred.", e);
        return ControllerUtil.makeJsonErrorResponse(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
