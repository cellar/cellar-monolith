/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.search
 *             FILE : SearchController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : May 23, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.search;

import eu.europa.ec.op.cellar.api.service.ICellarResponse;
import eu.europa.ec.op.cellar.api.service.ICellarService;
import eu.europa.ec.op.cellar.api.service.impl.CellarException;
import eu.europa.ec.op.cellar.api.util.WeightedLanguagePair;
import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarResponseController;
import eu.europa.ec.opoce.cellar.cl.controller.util.ControllerUtil;
import eu.europa.ec.opoce.cellar.cl.exception.CellarWebException;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.json.JacksonUtils;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : May 23, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
@RequestMapping(SearchController.PAGE_URL)
public class SearchController extends AbstractCellarResponseController {

    public static final String PAGE_URL = "/search";

    private final static String DECODING_LANGUAGE = "en";

    private static final String OBJECT_NOTICE_URL = "/object-notice";

    private static final String CONTENT_URL = "/content";

    private static final String PRODUCTION_IDENTIFIERS_URL = "/production-identifiers";

    /**
     * Cellar api to retrieve object notice and content stream.
     */
    @Autowired
    private ICellarService cellarApiService;

    /**
     * Production identifiers service.
     */
    @Autowired
    @Qualifier("pidManagerService")
    private IdentifierService identifierService;

    /**
     * Handle the production identifiers GET.
     */
    @RequestMapping(value = SearchController.PRODUCTION_IDENTIFIERS_URL, method = RequestMethod.GET)
    public void productionIdentifiersGet(final HttpServletRequest request, final HttpServletResponse response,
            @RequestParam(value = "cellarId", required = true) final String cellarId) throws IOException {

        final List<String> productionIdentifiers = this.identifierService.getIdentifier(cellarId).getPids();
        JacksonUtils.streamAsJson(response.getOutputStream(), productionIdentifiers, true); // production identifiers to JSON
    }

    /**
     * Handle the object notice GET.
     */
    @RequestMapping(value = SearchController.OBJECT_NOTICE_URL, method = RequestMethod.GET)
    public ResponseEntity<String> objectNoticeGet(final HttpServletRequest request, final HttpServletResponse httpResponse,
            @RequestParam(value = "cellarId", required = true) final String cellarId) throws IOException {

        ICellarResponse response = null;
        try {
            response = this.cellarApiService.getObjectNotice(cellarId, null, new WeightedLanguagePair(null, new Locale(DECODING_LANGUAGE)));
        } catch (final CellarException e) {
            final CellarWebException exc = HttpStatusAwareExceptionBuilder.getInstance(CellarWebException.class)
                    .withHttpStatus(ControllerUtil.resolveHttpStatus(e.getResponseCode().getResponseCode())).withMessage(e.getMessage())
                    .withCause(e).build();
            return ControllerUtil.makeErrorResponse(exc, exc.getHttpStatus());
        }

        return super.retrieveStringResponse(response);
    }

    /**
     * Handle the content stream GET.
     */
    @RequestMapping(value = SearchController.CONTENT_URL, method = RequestMethod.GET)
    public ResponseEntity<byte[]> contentGet(final HttpServletRequest request, final HttpServletResponse httpResponse,
            @RequestParam(value = "cellarId", required = true) final String cellarId) throws IOException {
        final int contentSeparatorIndex = cellarId.lastIndexOf("/");
        final String manifestationId = cellarId.substring(0, contentSeparatorIndex);
        final String contentId = cellarId.substring(contentSeparatorIndex + 1);

        ICellarResponse response = null;
        try {
            response = this.cellarApiService.retrieveContentStream(manifestationId, contentId);
        } catch (final CellarException e) {
            final CellarWebException exc = HttpStatusAwareExceptionBuilder.getInstance(CellarWebException.class)
                    .withHttpStatus(ControllerUtil.resolveHttpStatus(e.getResponseCode().getResponseCode())).withMessage(e.getMessage())
                    .withCause(e).build();
            final ResponseEntity<String> responseErrorStr = ControllerUtil.makeErrorResponse(exc, exc.getHttpStatus());
            return ControllerUtil.toBytesResponseEntity(responseErrorStr);
        }

        return super.retrieveBytesResponse(response, cellarId);
    }
}
