package eu.europa.ec.opoce.cellar.cl.controller.resource.validator.client;

import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import org.springframework.http.HttpStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface SimpleContentRequestValidator {

    HttpStatus validate(HttpServletRequest request, HttpServletResponse response, CellarIdentifier id, String datastreamId, CellarResource resource)
            throws Exception;
}
