/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.support.cellarapi
 *        FILE : CellarConfigurationService.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 30-01-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.support.cellarapi;

import eu.europa.ec.op.cellar.api.service.ICellarConfigurationService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * <class_description> Cellar APIs configuration service.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 02-08-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
@Service(CellarConfigurationService.ID)
public class CellarConfigurationService implements ICellarConfigurationService {

    /**
     *  Bean identifier.
     */
    public static final String ID = "app.eurlex.frontoffice.service.cellarConfigurationService";

    @Value("$${cellar-api.baseURI}")
    private String baseURI;

    @Value("$${cellar-api.host_header}")
    private String hostHeader;

    /**
     * @see eu.europa.ec.op.cellar.api.service.ICellarConfigurationService#getCellarBaseUri()
     */
    @Override
    public String getCellarBaseUri() {
        if(baseURI == null) {
            return null;
        }

        if(baseURI.endsWith("/")) {
            return baseURI.substring(0, baseURI.length() - 1);
        }

        return baseURI;
    }

    /**
     * @see eu.europa.ec.op.cellar.api.service.ICellarConfigurationService#getCellarHeader()
     */
    @Override
    public String getCellarHeader() {
        return this.hostHeader;
    }

    /**
     * @see eu.europa.ec.op.cellar.api.service.ICellarConfigurationService#getCellarNALBaseUri()
     */
    @Override
    public String getCellarNALBaseUri() {
        return null;
    }

    /**
     * @see eu.europa.ec.op.cellar.api.service.ICellarConfigurationService#getCellarNALHeader()
     */
    @Override
    public String getCellarNALHeader() {
        return null;
    }

    /**
     * @see eu.europa.ec.op.cellar.api.service.ICellarConfigurationService#getCellarSparqlBaseUri()
     */
    @Override
    public String getCellarSparqlBaseUri() {
        return null;
    }

    /**
     * @see eu.europa.ec.op.cellar.api.service.ICellarConfigurationService#getCellarSparqlHeader()
     */
    @Override
    public String getCellarSparqlHeader() {
        return null;
    }

}
