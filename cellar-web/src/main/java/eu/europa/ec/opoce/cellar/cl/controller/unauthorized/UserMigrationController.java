/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.unauthorized
 *        FILE : UserMigrationController.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 15-03-2023
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2023 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.unauthorized;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL;
import eu.europa.ec.opoce.cellar.cl.service.client.UserMigrationService;
import eu.europa.ec.opoce.cellar.cl.service.impl.UserMigrationServiceImpl.InvalidPasswordException;
import eu.europa.ec.opoce.cellar.cl.service.impl.UserMigrationServiceImpl.UserAlreadyMigratedException;
import eu.europa.ec.opoce.cellar.cl.service.impl.UserMigrationServiceImpl.UserNotFoundException;

/**
 * 
 * @author EUROPEAN DYNAMICS S.A
 */
@Controller
@RequestMapping(value = UserAccessRequestController.PAGE_URL)
public class UserMigrationController extends AbstractCellarController {

	/**
	 * 
	 */
	private static final Logger LOG = LogManager.getLogger(UserMigrationController.class);
	/**
	 * 
	 */
	static final String PAGE_URL = "/unauthorized";
    /**
     * 
     */
	private static final String USER_MIGRATION_URL = "/migrate";
	/**
	 * 
	 */
	private static final String JSP_MIGRATION_CREDENTIALS_PROVISION = "unauthorized/migrate-request-credentials";
	/**
	 * 
	 */
	private static final String FORM_BACKING_OBJECT_NAME = "userMigrationCredentialsCommand";
	/**
	 * 
	 */
	private static final String DEFAULT_VALIDATION_REJECTION_MESSAGE = "The provided input has been rejected.";
	
	/**
	 * 
	 */
	private final UserMigrationService userMigrationService;
	/**
	 * 
	 */
	private final UserCredentialsValidator userCredentialsValidatorValidator;
	
	@Autowired
	public UserMigrationController(UserMigrationService userMigrationService, UserCredentialsValidator userCredentialsValidatorValidator) {
		this.userMigrationService = userMigrationService;
		this.userCredentialsValidatorValidator = userCredentialsValidatorValidator;
	}

	
	@GetMapping(value = UserMigrationController.USER_MIGRATION_URL)
	public String displayUserMigrationForm(final HttpServletRequest request, final HttpServletResponse response, final Model model) {
		return JSP_MIGRATION_CREDENTIALS_PROVISION;
	}
	
	@PostMapping(value = UserMigrationController.USER_MIGRATION_URL)
	public String doUserMigration(@ModelAttribute(FORM_BACKING_OBJECT_NAME) UserCredentialsCommand command, final BindingResult result,
			final HttpServletRequest request, final HttpServletResponse response, final Model model) {
		this.userCredentialsValidatorValidator.validate(model, result);
		if (result.hasErrors()) {
			addUserMessage(LEVEL.ERROR, result.getFieldError().getCode(), DEFAULT_VALIDATION_REJECTION_MESSAGE);
			return redirectToGet();
		}
		
		String principalUsername = request.getRemoteUser();
		if (principalUsername == null) {
			LOG.error("User is not authenticated or is anonymous.");
			return redirect("/access-denied");
		}
		
		try {
			this.userMigrationService.migrateUser(principalUsername, command.getUsername(), command.getPassword());
		}
		catch (UserNotFoundException e) {
			addUserMessage(LEVEL.ERROR, "user.migration.cellar.user.not.exist");
			return redirectToGet();
		}
		catch (UserAlreadyMigratedException e) {
			addUserMessage(LEVEL.ERROR, "user.migration.cellar.user.already.migrated");
			return redirectToGet();
		}
		catch (InvalidPasswordException e) {
			addUserMessage(LEVEL.ERROR, "user.migration.cellar.password.invalid");
			return redirectToGet();
		}

		addUserMessage(LEVEL.SUCCESS, "user.migration.success");
		return redirectToGet();
	}
	
	@ModelAttribute(FORM_BACKING_OBJECT_NAME)
	private UserCredentialsCommand addUserMigrationCredentialsCommand () {
		return new UserCredentialsCommand();
	}
	
	private String redirectToGet() {
		return redirect(PAGE_URL + USER_MIGRATION_URL);
	}
	
}
