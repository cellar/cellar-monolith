package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.mets.validator;

import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.mets.form.MetsUploadForm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component("metsUploadFormValidator")
public class MetsUploadValidator implements Validator {
    /**
     * @param clazz
     * @return
     */
    @Override
    public boolean supports(Class<?> clazz) {
        return MetsUploadForm.class.isAssignableFrom(clazz);
    }

    /**
     * @param target
     * @param errors
     */
    @Override
    public void validate(Object target, Errors errors) {
        if(((MetsUploadForm) target).getFileData()==null||((MetsUploadForm) target).getFileData().isEmpty()){
            errors.rejectValue("fileData","mets.upload.empty.file.error");
        }
    }
}
