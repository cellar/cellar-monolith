/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin
 *             FILE : InferenceModelController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 09 05, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-09-05 11:20:27 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.semantic.ontology.InferenceModelCacheService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;

import static eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL.ERROR;
import static eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL.SUCCESS;

/**
 * @author ARHS Developments
 * @since 8.0
 */
@Controller
public class InferredModelController extends AbstractCellarController {

    private static final Logger LOG = LoggerFactory.getLogger(InferredModelController.class);

    private final InferenceModelCacheService inferredModelService;

    @Autowired
    public InferredModelController(InferenceModelCacheService inferredModelService) {
        this.inferredModelService = inferredModelService;
    }

    /**
     * Gets the resource current inference model
     *
     * @return the resource config model
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @RequestMapping(value = "/cmr/infModel", method = RequestMethod.GET)
    public ModelAndView getResourceConfigModel() throws IOException {
        return new ModelAndView("inferenceModel").addObject("description", inferredModelService.getDescription());
    }

    /**
     * Upload the new inferred model.
     *
     * @param request the request which contains the inference model
     * @return the view
     */
    @RequestMapping(value = "/cmr/infModel/upload", method = RequestMethod.POST)
    public ModelAndView uploadInferredModel(final MultipartHttpServletRequest request) {
        final MultipartFile inferenceModel = request.getFile("file");
        if (inferenceModel != null) {
            LOG.debug("Upload new inferred model: {}, ({} bytes), content-type: {}", inferenceModel.getName(),
                    inferenceModel.getSize(), inferenceModel.getContentType());
            if (inferredModelService.setInferenceModelFile(inferenceModel)) {
                addUserMessage(SUCCESS, "");
            } else {
                addUserMessage(ERROR, "");
            }
        } else {
            LOG.warn("File not found in the request, check if the request contains the parameter \"file\"");
        }
        return new ModelAndView("");
    }
}
