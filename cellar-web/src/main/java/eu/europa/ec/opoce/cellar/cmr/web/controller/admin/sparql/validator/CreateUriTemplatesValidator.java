/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.validator
 *             FILE : CreateEmbargoJobValidator.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 11, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.sparql.validator;

import eu.europa.ec.opoce.cellar.cl.domain.sparql.UriTemplates;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * <class_description> Embargo batch job command validator.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 11, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component("uriTemplatesValidator")
public class CreateUriTemplatesValidator implements Validator {

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(final Class<?> clazz) {
        return UriTemplates.class.isAssignableFrom(clazz);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validate(final Object target, final Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "uriPattern", "required.uri-templates.uriPattern",
                "A URI pattern name is required.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "sparql", "required.uri-templates.sparql", "A SPARQL query is required.");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "sequence", "required.uri-templates.sequence", "A sequence is required.");

        final UriTemplates uriTemplates = (UriTemplates) target;

        if (!(uriTemplates.getSequence() instanceof Long)) {
            errors.rejectValue("sequence", "required.uri-templates.sequence", "The sequence must be a number.");
        }
    }
}
