/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob
 *             FILE : CreateJobController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 11, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.common;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.BatchJobService;
import eu.europa.ec.opoce.cellar.semantic.json.JacksonUtils;
import eu.europa.ec.opoce.cellar.server.admin.sparql.Query;
import eu.europa.ec.opoce.cellar.support.SparqlUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * <class_description> Batch job resource manager controller.
 *
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 11, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
@RequestMapping(BatchJobResourceController.PAGE_URL)
public class BatchJobResourceController extends AbstractCellarController {

    public static final String PAGE_URL = "/cmr/batchJob";

    private static final String GET_QUERIES_URL = "/getQueries";

    private static final String GET_QUERY_URL = "/getQuery";

    /**
     * Batch job service.
     */
    @Autowired
    private BatchJobService batchJobService;

    /**
     * Handler method to get the query templates.
     * @param request the http request
     * @param response the http response
     * @return the query templates
     */
    @RequestMapping(value = BatchJobResourceController.GET_QUERIES_URL, method = RequestMethod.GET)
    public void getQueries(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        final Query[] queries = this.batchJobService.getQueries();
        JacksonUtils.streamAsJson(response.getOutputStream(), queries, true);
    }

    /**
     * Handler method to get the query template corresponding to the query identifier.
     * @param request the http request
     * @param response the http response
     * @param queryId the query identifier
     * @return the query templates
     */
    @RequestMapping(value = BatchJobResourceController.GET_QUERY_URL, method = RequestMethod.POST)
    public void getQuery(final HttpServletRequest request, final HttpServletResponse response,
            @RequestParam(value = "q", required = true) final String queryId) throws IOException {
        final String query = this.batchJobService.getQuery(queryId);
        response.setContentType(SparqlUtils.CONTENT_TYPE_QUERY);
        response.getOutputStream().write(query.getBytes(StandardCharsets.UTF_8));
        response.getOutputStream().flush();
    }
}
