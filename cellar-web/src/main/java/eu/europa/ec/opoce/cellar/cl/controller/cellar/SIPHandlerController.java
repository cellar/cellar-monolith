package eu.europa.ec.opoce.cellar.cl.controller.cellar;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.util.ControllerUtil;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.cl.service.client.FileService;
import eu.europa.ec.opoce.cellar.cl.service.client.MetsAnalyzer;
import eu.europa.ec.opoce.cellar.cl.service.client.WorkflowService;
import eu.europa.ec.opoce.cellar.cl.service.impl.WorkflowServiceOperationTypeVisitor;
import eu.europa.ec.opoce.cellar.domain.content.mets.OperationType;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.MetsException;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;

@Controller
@RequestMapping("/internal")
public class SIPHandlerController extends AbstractCellarController {

    private final Logger logger = LoggerFactory.getLogger(SIPHandlerController.class);

    private @Autowired WorkflowService workflowService;
    private @Autowired MetsAnalyzer metsAnalyzer;
    private @Autowired FileService fileService;

    /**
     * This Handler is invoked to notify that an archive has been uncompressed and is ready to be
     * processed.
     * 
     * @param location
     *            the location where the archive has been uncompressed
     * @return the a {@link ResponseEntity} with {@link HttpStatus} OK if the operation is notified
     *         with success.
     */
    @RequestMapping(method = RequestMethod.POST, value = "/notify/{location}/{sipArchiveName}/{metsId:.+}/{sipType}")
    public ResponseEntity<String> notify(@PathVariable final String location, @PathVariable final String sipArchiveName,
            @PathVariable final String metsId, @PathVariable final TYPE sipType) {
        logger.debug("Starting workflow");

        SIPResource sipResource = new SIPResource(metsId, location, sipArchiveName, sipType);

        File metsFile;
        try {
            metsFile = fileService.getMetsFileFromTemporaryWorkFolder(location);
        } catch (IOException e) {
            throw ExceptionBuilder.get(MetsException.class).withCode(CoreErrors.E_021).withMessage("Folder: {}").withMessageArgs(location)
                    .build();
        }

        final OperationType type = metsAnalyzer.getMetsTypeFromTemporaryWorkFolder(metsFile);
        type.accept(new WorkflowServiceOperationTypeVisitor(sipResource, this.workflowService), null);

        logger.debug("Worflow completed");

        return ControllerUtil.makeResponse("");
    }
}
