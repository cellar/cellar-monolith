/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.resource
 *        FILE : ResourceController.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 03-01-2013
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.resource;

import com.amazonaws.services.s3.Headers;
import eu.europa.ec.opoce.cellar.ccr.service.client.ContentLanguageService;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.controller.resource.validator.client.ContentRequestValidator;
import eu.europa.ec.opoce.cellar.cl.controller.util.ControllerUtil;
import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditBuilder;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventProcess;
import eu.europa.ec.opoce.cellar.cl.exception.CellarWebException;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierResolver;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.common.http.headers.CellarHttpHeaders;
import eu.europa.ec.opoce.cellar.common.http.headers.HttpHeadersBuilder;
import eu.europa.ec.opoce.cellar.common.http.headers.directives.CacheControl;
import eu.europa.ec.opoce.cellar.common.util.ZipUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareException;
import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import eu.europa.ec.opoce.cellar.server.conditionalRequest.ConditionalRequestVersions;
import eu.europa.ec.opoce.cellar.server.conditionalRequest.ETagType;
import eu.europa.ec.opoce.cellar.server.dissemination.DisseminationRequestUtils;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.DisseminationMethod;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.FilterVariance;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.InferenceVariance;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.Structure;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.retrieve.contentPool.ContentPoolListRetrieveResolver;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.retrieve.contentPool.ContentPoolZipRetrieveResolver;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.retrieve.identifiers.IdentifiersRetrieveResolver;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.retrieve.link.TimeMapLinkRetrieveResolver;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.retrieve.rdf.RdfRetrieveResolver;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.retrieve.xml.XmlRetrieveResolver;
import eu.europa.ec.opoce.cellar.server.filter.DisseminationLogHelper;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;
import eu.europa.ec.opoce.cellar.server.service.DisseminationException;
import eu.europa.ec.opoce.cellar.server.service.IConditionalRequestService;
import eu.europa.ec.opoce.cellar.support.annotation.DisseminationMethodBinder;
import eu.europa.ec.opoce.cellar.support.dissemination.editor.FilterVarianceEditor;
import eu.europa.ec.opoce.cellar.support.dissemination.editor.InferenceVarianceEditor;
import eu.europa.ec.opoce.cellar.support.dissemination.editor.StructureEditor;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.HttpHeaders;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Map;

/**
 * <class_description> Spring MVC Controller class that provides Http access to Cellar content and metadata resources
 * via content negotiation and uri resolution.</br>
 * It implements a 'HTTP GET REST' service to extract data streams and/or metadata from Cellar using a PS-uri or cellar-uri,
 * depending on HTTP headers (accept mime type, language), and depending on the specified parameters (decoding language) and type.
 * </br></br>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * ON : 03-01-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
@RequestMapping
public class RetrieveResourceController extends AbstractResourceController {

    private static final Logger LOG = LogManager.getLogger(RetrieveResourceController.class);

    private final ContentStreamService contentStreamService;

    private final IdentifierResolver identifierResolver;

    private final ContentRequestValidator contentRequestValidator;

    private final ContentLanguageService contentLanguageService;

    private final IConditionalRequestService conditionalRequestService;

    private final ICellarConfiguration configuration;

    private final CellarResourceDao cellarResourceDao;

    @Autowired
    public RetrieveResourceController(ContentStreamService contentStreamService, IdentifierResolver identifierResolver,
                                      ContentRequestValidator contentRequestValidator, ContentLanguageService contentLanguageService,
                                      IConditionalRequestService conditionalRequestService, @Qualifier("cellarConfiguration") ICellarConfiguration configuration,
                                      CellarResourceDao cellarResourceDao) {
        this.contentStreamService = contentStreamService;
        this.identifierResolver = identifierResolver;
        this.contentRequestValidator = contentRequestValidator;
        this.contentLanguageService = contentLanguageService;
        this.conditionalRequestService = conditionalRequestService;
        this.configuration = configuration;
        this.cellarResourceDao = cellarResourceDao;
    }

    /**
     * Do content stream size checks.
     *
     * @param acceptMaxCsSizeHeader the accept max cs size header
     * @param contentStreamBytes    the content stream bytes
     */
    private void doContentStreamSizeChecks(final String acceptMaxCsSizeHeader, final long contentStreamBytes) {

        final long contentStreamMaxBytesCellar = this.cellarConfiguration.getCellarServiceDisseminationContentStreamMaxSize();
        long contentStreamMaxBytesRequest;

        if (StringUtils.isBlank(acceptMaxCsSizeHeader)) {
            contentStreamMaxBytesRequest = contentStreamMaxBytesCellar;
        } else {
            try {
                contentStreamMaxBytesRequest = Long.parseLong(acceptMaxCsSizeHeader);
            } catch (final Exception e) {
                contentStreamMaxBytesRequest = -1;
            }
        }

        if (contentStreamMaxBytesRequest < 0) {
            final String message = "Could not parse value of header Accept-Max-Cs-Size to a positive integer! Was: '"
                    + acceptMaxCsSizeHeader + "' ";
            throwContentStreamException(HttpStatus.BAD_REQUEST, message);
        }

        if (contentStreamBytes > contentStreamMaxBytesRequest) {
            final String message = "The content stream size (" + contentStreamBytes + " bytes) is bigger than "
                    + "maximum specified in the header (" + contentStreamMaxBytesRequest + ")";
            throwContentStreamException(HttpStatus.NOT_ACCEPTABLE, message);
        }

        if (contentStreamBytes > contentStreamMaxBytesCellar) {
            final String message = "The content stream size (" + contentStreamBytes + " bytes) is bigger than "
                    + "the maximum Cellar can handle (" + contentStreamMaxBytesCellar + ")";
            throwContentStreamException(HttpStatus.UNPROCESSABLE_ENTITY, message);
        }

    }

    /**
     * Handler for the READ operation on a datastream content.
     * <p>
     * This mapping was erased as the Cellar ID based content streams must be treated specially
     *
     * @param request             the request
     * @param response            the response
     * @param id                  the identifier without the namespace
     * @param datastreamId        the desired resource's datastream
     * @param provideResponseBody the provide response body
     * @param eTag                the e tag
     * @param lastModified        the last modified
     * @return a ResponseEntity
     * @throws Exception the exception
     * @ RequestMapping(method = RequestMethod.GET, value = "/cellar/{id}/{datastreamId:.+}")
     */
    public void getContentResource(final HttpServletRequest request, final HttpServletResponse response, final String id,
                                   final String datastreamId, final String compressedEntryName, final boolean provideResponseBody, final String eTag,
                                   final String lastModified) throws Exception {

        //TODO: if-modified-since

        // validates the request
        String myDatastreamId = datastreamId;
        CellarIdentifier cellarId;
        HttpStatus status = HttpStatus.OK;
        try {
            cellarId = this.identifierResolver.resolveToCellarId(ContentIdentifier.CELLAR_PREFIX, id);
            if ((myDatastreamId != null) && !ContentType.isCellarContentType(myDatastreamId)) {
                myDatastreamId = this.identifierResolver.getDatastreamId(id, myDatastreamId);
            } else {
                status = this.contentRequestValidator.validateContentRequest(request, response, cellarId, myDatastreamId);
                if (status == HttpStatus.NOT_FOUND) {
                    throw new IllegalArgumentException();
                }
            }
        } catch (final Exception e) {
            if (e instanceof IllegalArgumentException) {
                throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                        .withMessage("Resource [system 'cellar' - id '{}/{}'] not found.").withMessageArgs(id, myDatastreamId).build();

            } else {
                throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                        .withCause(e).build();
            }
        }

        if (HttpStatus.OK.equals(status)) {
            final CellarResource resource = cellarResourceDao.findCellarId(cellarId.getUuid() + "/" + datastreamId);
            final ConditionalRequestVersions conditionalRequestVersions = this.conditionalRequestService
                    .calculateVersionWithoutEmbedding(resource, ETagType.CONTENT_STREAM, eTag, lastModified);

            // sets the status to 304 NOT MODIFIED in case the content is obtainable from cache
            if (!conditionalRequestVersions.isChanged()) {
                status = HttpStatus.NOT_MODIFIED;
            }

            final String contentLanguages = this.contentLanguageService.getContentLanguages(resource);
            final HttpHeadersBuilder httpHeadersBuilder = HttpHeadersBuilder.get()
                    .withETag(conditionalRequestVersions.getNewETag())
                    .withLastModified(conditionalRequestVersions.getNewLastModified())
                    .withCacheControl(CacheControl.NO_CACHE)
                    .withDateNow()
                    .withContentLanguage(contentLanguages);

            response.setStatus(status.value());

            DisseminationLogHelper.extendDisseminationLogEntries(new CellarResourceBean(resource), null, resource.getMimeType(), cellarResourceDao);
            
            if (HttpStatus.OK.equals(status)) { // gets the stream only if it is still a 200 OK status, and not a 304 NOT MODIFIED
                getResource(request, response, status, httpHeadersBuilder, cellarId, myDatastreamId, compressedEntryName, provideResponseBody, resource);
            } else {
                ControllerUtil.setHttpHeaders(response, httpHeadersBuilder.getHttpHeaders());
            }
        }
    }

    /**
     * Handler for getting all identifiers (i.e., sameAs resources) for a content stream cellar ID
     *
     * @param request      the request
     * @param response     the response
     * @param identifier   the identifier without the namespace and without the content stream part (ex.: DOC_1)
     * @param datastreamId the content stream part of the identifier (ex.: DOC_1)
     * @param eTag         the eTag for caching purposes
     * @param lastModified the last modified
     * @return the identifiers resource content stream
     */
    @RequestMapping(method = RequestMethod.GET, value = "/cellar/{identifier:.+}/{datastreamId:.+}/identifiers")
    public ResponseEntity<?> getIdentifiersResourceContentStream(final HttpServletRequest request, final HttpServletResponse response, //
                                                                 @PathVariable final String identifier, //
                                                                 @PathVariable final String datastreamId, //
                                                                 @RequestHeader(value = HttpHeaders.IF_NONE_MATCH, required = false) final String eTag,
                                                                 @RequestHeader(value = HttpHeaders.IF_MODIFIED_SINCE, required = false) final String lastModified) {
        return IdentifiersRetrieveResolver.get(identifier + "/" + datastreamId, eTag, lastModified).handleDisseminationRequest();
    }

    /**
     * Handler for getting all identifiers (i.e., sameAs resources) for a non-content stream cellar ID
     *
     * @param request      the request
     * @param response     the response
     * @param identifier   the identifier without the namespace
     * @param eTag         the eTag for caching purposes
     * @param lastModified the last modified
     * @return the identifiers resource standard
     */
    @RequestMapping(method = RequestMethod.GET, value = "/cellar/{identifier:.+}/identifiers")
    public ResponseEntity<?> getIdentifiersResourceStandard(final HttpServletRequest request, final HttpServletResponse response, //
                                                            @PathVariable final String identifier, //
                                                            @RequestHeader(value = HttpHeaders.IF_NONE_MATCH, required = false) final String eTag,
                                                            @RequestHeader(value = HttpHeaders.IF_MODIFIED_SINCE, required = false) final String lastModified) {
        return IdentifiersRetrieveResolver.get(identifier, eTag, lastModified).handleDisseminationRequest();
    }

    /**
     * Gets the list resource.
     *
     * @param request      the request
     * @param response     the response
     * @param identifier   the identifier
     * @param eTag         the e tag
     * @param lastModified the last modified
     * @return the list resource
     */
    @RequestMapping(method = RequestMethod.GET, value = "/cellar/{identifier:.+}/list")
    public ResponseEntity<?> getListResource(final HttpServletRequest request, final HttpServletResponse response, //
                                             @PathVariable final String identifier, //
                                             @RequestHeader(value = HttpHeaders.IF_NONE_MATCH, required = false) final String eTag, //
                                             @RequestHeader(value = HttpHeaders.IF_MODIFIED_SINCE, required = false) final String lastModified) {

        return ContentPoolListRetrieveResolver.get(identifier, eTag, lastModified).handleDisseminationRequest();
    }

    /**
     * Gets a resource attached to a cellar id with a given datastream id.
     *
     * @param request             the request
     * @param response            the response
     * @param status              the status
     * @param httpHeadersBuilder  the http headers builder
     * @param cellarId            the cellar id
     * @param datastreamId        the datastream id
     * @param compressedEntryName the name of the optional compressed entry of the stream, if it is a zip
     * @param provideResponseBody the provide response body
     * @return the resource
     */
    private void getResource(final HttpServletRequest request, final HttpServletResponse response, final HttpStatus status,
                             final HttpHeadersBuilder httpHeadersBuilder, final CellarIdentifier cellarId, final String datastreamId,
                             final String compressedEntryName, final boolean provideResponseBody, CellarResource resource) {

        if (!configuration.isCellarServiceDisseminationRetrieveEmbargoedResourceEnabled()) {
            Assert.isTrue(!resource.isUnderEmbargo(), "[E-112] The digital object production ID is invalid");
        }

        Map<String, Object> metadata = contentStreamService.getMetadata(resource.getCellarId(), resource.getVersion(ContentType.FILE).orElse(null), ContentType.FILE);
        final long contentStreamBytes = (long) metadata.get(Headers.CONTENT_LENGTH);
        final Date lastModificationDate = (Date) metadata.get(Headers.LAST_MODIFIED);
        httpHeadersBuilder.withLastModified(lastModificationDate);

        final String acceptMaxCsSizeHeader = DisseminationRequestUtils.getAcceptCsMaxSize(request);
        doContentStreamSizeChecks(acceptMaxCsSizeHeader, contentStreamBytes);

        final String acceptType = (String) metadata.get(Headers.CONTENT_TYPE);
        httpHeadersBuilder.withContentType(acceptType); // application/pdf;type=pdf1x

        ControllerUtil.setHttpHeaders(response, httpHeadersBuilder.getHttpHeaders());
        if (!provideResponseBody) {
            // All needed headers are set but the body is not needed
            return;
        }

        try (InputStream datastreamInputStream = contentStreamService.getContent(resource.getCellarId(), resource.getVersion(ContentType.FILE).orElse(null), ContentType.FILE)
                .orElseThrow(() -> HttpStatusAwareExceptionBuilder.getInstance(CellarWebException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                        .withMessage("Resource with id [{}/{}] is unavailable.").withMessageArgs(cellarId.getUuid(), datastreamId).build())) {

            // try to retrieve the compressed entry of the stream, if the stream is a zip and compressedEntryName is specified
            if (compressedEntryName != null) {
                if (acceptType.startsWith(DisseminationRequestUtils.ZIP.toString())) {
                    final boolean entryFound = ZipUtils.extractEntry(datastreamInputStream, compressedEntryName, response.getOutputStream());
                    if (!entryFound) {
                        throw HttpStatusAwareExceptionBuilder.getInstance(CellarWebException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                                .withMessage("Resource with id [{}/{}/{}] is unavailable.")
                                .withMessageArgs(cellarId.getUuid(), datastreamId, compressedEntryName).build();
                    }
                } else {
                    throw HttpStatusAwareExceptionBuilder.getInstance(CellarWebException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                            .withMessage("Cannot retrieve resource with id [{}/{}/{}] because [{}/{}] is not a compressed content stream.")
                            .withMessageArgs(cellarId.getUuid(), datastreamId, compressedEntryName, cellarId.getUuid(), datastreamId).build();
                }
            }
            // otherwise retrieve the content stream
            else {
                IOUtils.copy(datastreamInputStream, response.getOutputStream());
            }
        } catch (final HttpStatusAwareException ex) {
            logHttpResponseException(ex, response, ex.getHttpStatus());
        } catch (final Exception ex) {
            logHttpResponseException(ex, response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Log http response exception.
     *
     * @param e          the e
     * @param response   the response
     * @param httpStatus the http status
     */
    private void logHttpResponseException(final Throwable e, final HttpServletResponse response, final HttpStatus httpStatus) {
        ControllerUtil.setResponseError(response, e, httpStatus);

        AuditBuilder.get(AuditTrailEventProcess.Dissemination) //
                .withException(e)//
                .withLogger(LOG) //
                .logEvent();
    }

    /**
     * Gets the xml resource.
     *
     * @param request        the request
     * @param response       the response
     * @param identifier     the identifier
     * @param structure      the structure
     * @param variance       the variance
     * @param method         the method
     * @param eTag           the e tag
     * @param lastModified   the last modified
     * @param acceptDateTime the accept date time
     * @return the xml resource
     */
    @RequestMapping(method = {
            RequestMethod.GET, RequestMethod.HEAD}, value = "/cellar/{identifier:.+}/rdf/{structure}/{variance}")
    public ResponseEntity<?> getXmlResource(final HttpServletRequest request, final HttpServletResponse response, //
                                            @PathVariable final String identifier, //
                                            @PathVariable final Structure structure, //
                                            @PathVariable final InferenceVariance variance, //
                                            @DisseminationMethodBinder final DisseminationMethod method, //
                                            @RequestHeader(value = HttpHeaders.IF_NONE_MATCH, required = false) final String eTag, //
                                            @RequestHeader(value = HttpHeaders.IF_MODIFIED_SINCE, required = false) final String lastModified, //
                                            @RequestHeader(value = CellarHttpHeaders.ACCEPT_DATETIME, required = false) final Date acceptDateTime) {

        return RdfRetrieveResolver.get(identifier, structure, variance, eTag, lastModified, method.isResponseBodyNeeded(), acceptDateTime)
                .handleDisseminationRequest();
    }

    /**
     * Gets the xml resource.
     * For normalized notices.
     * @param request        the request
     * @param response       the response
     * @param identifier     the identifier
     * @param structure      the structure
     * @param variance       the variance
     * @param method         the method
     * @param eTag           the e tag
     * @param lastModified   the last modified
     * @param acceptDateTime the accept date time
     */
    @RequestMapping(method = {
            RequestMethod.GET, RequestMethod.HEAD}, value = "/cellar/{identifier:.+}/rdf/{structure}/{variance}/normalized")
    public ResponseEntity<?> getXmlResourceNormalized(final HttpServletRequest request, final HttpServletResponse response, //
                                            @PathVariable final String identifier, //
                                            @PathVariable final Structure structure, //
                                            @PathVariable final InferenceVariance variance, //
                                            @DisseminationMethodBinder final DisseminationMethod method, //
                                            @RequestHeader(value = HttpHeaders.IF_NONE_MATCH, required = false) final String eTag, //
                                            @RequestHeader(value = HttpHeaders.IF_MODIFIED_SINCE, required = false) final String lastModified, //
                                            @RequestHeader(value = CellarHttpHeaders.ACCEPT_DATETIME, required = false) final Date acceptDateTime) {

        return RdfRetrieveResolver.get(identifier, structure, variance, eTag, lastModified, method.isResponseBodyNeeded(), acceptDateTime,true)
                .handleDisseminationRequest();
    }




    /**
     * Handler for the READ operation on a datastream content.
     *
     * @param request the request
     * @param response the response
     * @param identifier the identifier
     * @param structure the structure
     * @param decoding the decoding
     * @param method the method
     * @param eTag the e tag
     * @param lastModified the last modified
     * @return a ResponseEntity
     */

    /**
     * Gets the xml resource.
     *
     * @param request    the request
     * @param response   the response
     * @param identifier the identifier
     * @param structure  the structure
     * @param decoding   the decoding
     * @param eTag       the e tag
     * @return the xml resource
     */
    @RequestMapping(method = {
            RequestMethod.GET, RequestMethod.HEAD}, value = "/cellar/{identifier:.+}/xml/{structure}")
    public ResponseEntity<?> getXmlResource(final HttpServletRequest request, final HttpServletResponse response, //
                                            @PathVariable final String identifier, //
                                            @PathVariable final Structure structure, //
                                            @RequestParam(value = "decoding", required = true) final String decoding, //
                                            @DisseminationMethodBinder final DisseminationMethod method, //
                                            @RequestHeader(value = HttpHeaders.IF_NONE_MATCH, required = false) final String eTag, //
                                            @RequestHeader(value = HttpHeaders.IF_MODIFIED_SINCE, required = false) final String lastModified) {

        return XmlRetrieveResolver
                .get(identifier, structure, FilterVariance.FILTERED, decoding, eTag, lastModified, method.isResponseBodyNeeded())
                .handleDisseminationRequest();
    }

    //FIXME JRM: Remove this RequestMapping after the transition period (see CELLAR-724)

    /**
     * Gets the xml resource with variance.
     *
     * @param request      the request
     * @param response     the response
     * @param identifier   the identifier
     * @param structure    the structure
     * @param variance     the variance
     * @param decoding     the decoding
     * @param method       the method
     * @param eTag         the e tag
     * @param lastModified the last modified
     * @return the xml resource with variance
     */
    @RequestMapping(method = {
            RequestMethod.GET, RequestMethod.HEAD}, value = "/cellar/{identifier:.+}/xml/{structure}/{variance}")
    public ResponseEntity<?> getXmlResourceWithVariance(final HttpServletRequest request, final HttpServletResponse response, //
                                                        @PathVariable final String identifier, //
                                                        @PathVariable final Structure structure, //
                                                        @PathVariable final FilterVariance variance, //
                                                        @RequestParam(value = "decoding", required = true) final String decoding, //
                                                        @DisseminationMethodBinder final DisseminationMethod method, //
                                                        @RequestHeader(value = HttpHeaders.IF_NONE_MATCH, required = false) final String eTag, //
                                                        @RequestHeader(value = HttpHeaders.IF_MODIFIED_SINCE, required = false) final String lastModified) {

        // [CELLARM-457] - CELLAR-724: Always return the filtered notice; after a transition period, this parameter will be erased
        return this.getXmlResource(request, response, identifier, structure, decoding, method, eTag, lastModified);
    }

    /**
     * Gets the link resource.
     *
     * @param request    the request
     * @param response   the response
     * @param identifier the identifier
     * @return the link resource
     */
    @RequestMapping(method = {
            RequestMethod.GET}, value = "/cellar/{identifier:.+}/link-format")
    public ResponseEntity<?> getLinkResource(final HttpServletRequest request, final HttpServletResponse response, //
                                             @PathVariable final String identifier) {

        return TimeMapLinkRetrieveResolver.get(identifier, null, null, true).handleDisseminationRequest();
    }

    /**
     * Gets the zip resource.
     *
     * @param request      the request
     * @param response     the response
     * @param identifier   the identifier
     * @param eTag         the e tag
     * @param lastModified the last modified
     * @return the zip resource
     */
    @RequestMapping(method = RequestMethod.GET, value = "/cellar/{identifier:.+}/zip")
    public void getZipResource(final HttpServletRequest request, final HttpServletResponse response, //
                               @PathVariable final String identifier, //
                               @RequestHeader(value = HttpHeaders.IF_NONE_MATCH, required = false) final String eTag, //
                               @RequestHeader(value = HttpHeaders.IF_MODIFIED_SINCE, required = false) final String lastModified) {

        LOG.debug("Handle zip stream request [resource '{}']", identifier);

        final ResponseEntity<?> result = ContentPoolZipRetrieveResolver.get(identifier, eTag, lastModified).handleDisseminationRequest();

        final HttpStatus status = result.getStatusCode();
        final File zipFile = (File) result.getBody();

        InputStream datastreamInput = null;
        try {

            ControllerUtil.setHttpHeaders(response, result.getHeaders());
            response.setStatus(status.value());

            if (HttpStatus.OK.equals(status)) {
                datastreamInput = new FileInputStream(zipFile);
                IOUtils.copy(datastreamInput, response.getOutputStream());
            }

        } catch (final IOException ioe) {
            ControllerUtil.setResponseError(response, ioe, HttpStatus.INTERNAL_SERVER_ERROR);
            LOG.error(ioe.getMessage(), ioe);
        } finally {
            // The file is available as long as it is not transferred completely (but cannot be deleted by another process/program).
            // After the transfer is done, the deletion is done.
            IOUtils.closeQuietly(datastreamInput);
            FileUtils.deleteQuietly(zipFile);
        }
    }

    /**
     * Inits the binder.
     *
     * @param request the request
     * @param binder  the binder
     */
    @InitBinder
    public void initBinder(final HttpServletRequest request, final ServletRequestDataBinder binder) {
        binder.registerCustomEditor(Structure.class, new StructureEditor());
        binder.registerCustomEditor(FilterVariance.class, new FilterVarianceEditor());
        binder.registerCustomEditor(InferenceVariance.class, new InferenceVarianceEditor());
    }

    /**
     * Throw content stream exception.
     *
     * @param httpStatus the http status
     * @param message    the message
     */
    private static void throwContentStreamException(final HttpStatus httpStatus, final String message) {
        throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class)
                .withHttpStatus(httpStatus)
                .withMessage(message)
                .build();
    }
    
}
