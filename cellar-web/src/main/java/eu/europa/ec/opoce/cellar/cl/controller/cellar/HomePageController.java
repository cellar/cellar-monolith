/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.cellar
 *             FILE : HomePageController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Nov 6, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.cellar;

import eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLockStatistic;
import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.service.client.IConcurrencyController;
import eu.europa.ec.opoce.cellar.splash.SplashScreen;
import eu.europa.ec.opoce.cellar.splash.SplashUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page. <br/>
 * <br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc. <br/>
 * <br/>
 * ON : Nov 6, 2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
public class HomePageController extends AbstractCellarController {

    private final static int RESOURCE_IDENTIFIER_COLUMN_SIZE = 60;

    private final static int LOCK_COUNT_COLUMN_SIZE = 11;

    private final static int LOCK_OBJECT_REF_COLUMN_SIZE = 16;

    private final static int LOCK_SESSIONS_COLUMN_SIZE = 120;

    private final static int TOTAL_COLUMN_SIZE = RESOURCE_IDENTIFIER_COLUMN_SIZE + LOCK_COUNT_COLUMN_SIZE + LOCK_OBJECT_REF_COLUMN_SIZE
            + LOCK_SESSIONS_COLUMN_SIZE + 5;

    @Autowired
    private SplashScreen splashScreen;

    /**
     * The concurrency controller service used to get the concurrency statistics.
     */
    @Autowired
    private IConcurrencyController concurrencyController;

    @RequestMapping(method = RequestMethod.GET, value = "/")
    public ModelAndView getHome() throws IOException {
        this.splashScreen.refresh();
        final Map<String, String> content = new HashMap<String, String>();
        final String splash = this.splashScreen.getHtmlSplash();
        content.put("content", splash);
        return new ModelAndView("home", content);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/concurrency")
    public @ResponseBody Map<String, List<ICellarLockStatistic>> getConcurrency() {
        return this.concurrencyController.getConcurrencyControllerStatistics();
    }

    @Deprecated
    @RequestMapping(method = RequestMethod.GET, value = "/concurrency/splash")
    public ModelAndView getConcurrencySplash() throws IOException {
        final Map<String, String> content = new HashMap<String, String>();
        final StringBuilder sb = new StringBuilder();

        final Map<String, List<ICellarLockStatistic>> cellarLockStatistics = this.concurrencyController
                .getConcurrencyControllerStatistics();

        for (Map.Entry<String, List<ICellarLockStatistic>> entry : cellarLockStatistics.entrySet()) {

            // header
            sb.append(StringUtils.center(" " + entry.getKey() + " ", TOTAL_COLUMN_SIZE, '#'));
            sb.append("\n");

            sb.append('#');
            sb.append(StringUtils.rightPad("Resource identifier", RESOURCE_IDENTIFIER_COLUMN_SIZE, ' '));
            sb.append('|');
            sb.append(StringUtils.rightPad("Lock count", LOCK_COUNT_COLUMN_SIZE, ' '));
            sb.append('|');
            sb.append(StringUtils.rightPad("Lock object ref", LOCK_OBJECT_REF_COLUMN_SIZE, ' '));
            sb.append('|');
            sb.append(StringUtils.rightPad("Lock sessions", LOCK_SESSIONS_COLUMN_SIZE, ' '));
            sb.append('#');
            sb.append("\n");

            sb.append(StringUtils.rightPad("", TOTAL_COLUMN_SIZE, '#'));
            sb.append("\n");

            // body
            for (ICellarLockStatistic cellarLockStatistic : entry.getValue()) {
                sb.append('#');
                sb.append(StringUtils.rightPad(cellarLockStatistic.getResourceIdentifier(), RESOURCE_IDENTIFIER_COLUMN_SIZE, ' '));
                sb.append('|');
                sb.append(StringUtils.rightPad(Integer.toString(cellarLockStatistic.getLockCount()), LOCK_COUNT_COLUMN_SIZE, ' '));
                sb.append('|');
                sb.append(StringUtils.rightPad(cellarLockStatistic.getLockObjectRef(), LOCK_OBJECT_REF_COLUMN_SIZE, ' '));
                sb.append('|');
                sb.append(StringUtils.rightPad(cellarLockStatistic.getCellarLockSessions().toString(), LOCK_SESSIONS_COLUMN_SIZE, ' '));
                sb.append('#');
                sb.append("\n");
            }

            // footer
            sb.append(StringUtils.rightPad("", TOTAL_COLUMN_SIZE, '#'));

            sb.append("\n");
            sb.append("\n");
        }

        // to html
        final String splash = SplashUtils.justifyHtmlSplash(sb.toString());

        content.put("content", splash);
        return new ModelAndView("home", content);
    }

}
