/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.indexing
 *             FILE : DashboardController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 28, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.indexing;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL;
import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService;
import eu.europa.ec.opoce.cellar.cmr.indexing.IndexationMonitoringService;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestsCount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * <class_description> Indexing dashboard controller.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 28, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller("indexingDashboardController")
@RequestMapping(DashboardController.PAGE_URL)
public class DashboardController extends AbstractCellarController {

    /** The Constant PAGE_URL. */
    public static final String PAGE_URL = "/cmr/indexing";

    /** The Constant DASHBOARD_URL. */
    public static final String DASHBOARD_URL = "/dashboard";

    public static final String RESTART_FAILED_URL = "/restart/failed";

    /** The Constant JSP_NAME. */
    private static final String JSP_NAME = "cmr/indexing/dashboard";

    /**
     * Indexing service.
     */
    private final IIndexingService indexingService;

    private final IndexationMonitoringService indexationMonitoringService;

    @Autowired
    public DashboardController(IIndexingService indexingService, IndexationMonitoringService indexationMonitoringService) {
        this.indexingService = indexingService;
        this.indexationMonitoringService = indexationMonitoringService;
    }

    /**
     * Handle the GET.
     *
     * @return the string
     */
    @RequestMapping(method = RequestMethod.GET)
    public String indexingGet() {
        return redirect(PAGE_URL + DASHBOARD_URL);
    }

    /**
     * Handle the GET.
     *
     * @param model the model
     * @return the string
     */
    @RequestMapping(value = DashboardController.DASHBOARD_URL, method = RequestMethod.GET)
    public String dashboardGet(final ModelMap model) {

        final List<CmrIndexRequestsCount> newIndexationRequests = indexationMonitoringService.getInProgressRequests(20);
        final List<CmrIndexRequestsCount> history = indexationMonitoringService.getHistory(20);

        model.addAttribute("newIndexationRequests", newIndexationRequests);
        model.addAttribute("indexationRequestsProcessedHistory", history);
        model.addAttribute("indexingQueue", indexingService.getCountIndexRequestsToHandle());
        model.addAttribute("failedIndexingRequests", indexingService.getCountFailedIndexRequests());
        model.addAttribute("indexingServiceStatus", indexingService.getIndexingStatus());
        model.addAttribute("indexingEnabled", indexingService.isIndexingEnabled());

        return JSP_NAME;
    }

    @RequestMapping(value = DashboardController.RESTART_FAILED_URL, method = RequestMethod.GET)
    public String restartFailedGet() {
        this.indexingService.restartFailedCmrIndexRequests();
        super.addUserMessage(LEVEL.SUCCESS, "common.message.successfullyProcessed");
        return redirect(PAGE_URL);
    }
}
