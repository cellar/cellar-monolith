/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.rdfStoreCleaner
 *             FILE : DifferenceListController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 10, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.rdfStoreCleaner;

import eu.europa.ec.opoce.cellar.cl.configuration.enums.CellarServiceRDFStoreCleanerMode;
import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner.CleanerDifference;
import eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner.CleanerDifference.CmrTable;
import eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner.CleanerDifference.QueryType;
import eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner.CleanerDifferencePaginatedList;
import eu.europa.ec.opoce.cellar.cl.service.rdfStoreCleaner.RDFStoreCleanerService;
import eu.europa.ec.opoce.cellar.common.util.EnumUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 10, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller("rdfStoreCleanerDifferenceListController")
@RequestMapping(DifferenceListController.PAGE_URL)
public class DifferenceListController extends AbstractCellarController {

    public static final String PAGE_URL = "/cmr/rdfStoreCleaner/list";

    public static final String DIFFERENCE_URL = "/difference";

    private static final String JSP_NAME = "cmr/rdfStoreCleaner/list";

    @Autowired
    private RDFStoreCleanerService rdfStoreCleanerService;

    /**
     * Handle the GET.
     */
    @RequestMapping(method = RequestMethod.GET)
    public String dashboardGet(final ModelMap model, @RequestParam(value = "cellarId", required = false) final String cellarId,
            @RequestParam(value = "operationType", required = false) final String operationType,
            @RequestParam(value = "queryType", required = false) final String queryType,
            @RequestParam(value = "cmrTable", required = false) final String cmrTable,
            @RequestParam(value = "sort", required = false, defaultValue = "cellarId") final String sort,
            @RequestParam(value = "dir", required = false) final String dir,
            @RequestParam(value = "page", required = false) final Integer page) {

        model.addAttribute("operationTypes", CellarServiceRDFStoreCleanerMode.values());
        model.addAttribute("queryTypes", QueryType.values());
        model.addAttribute("cmrTables", CmrTable.values());

        final CleanerDifferencePaginatedList cdpl = new CleanerDifferencePaginatedList(page, sort, dir, cellarId,
                EnumUtils.resolve(CellarServiceRDFStoreCleanerMode.class, operationType, null),
                EnumUtils.resolve(QueryType.class, queryType, null),
                EnumUtils.resolve(CmrTable.class, cmrTable, null));
        this.rdfStoreCleanerService.findCleanerDifferences(cdpl);

        model.addAttribute("differences", cdpl);

        return JSP_NAME;
    }

    @RequestMapping(value = DifferenceListController.DIFFERENCE_URL, method = RequestMethod.GET)
    public void productionIdentifiersGet(final HttpServletRequest request, final HttpServletResponse response,
            @RequestParam(value = "id", required = true) final Long id) throws IOException {
        final CleanerDifference cd = this.rdfStoreCleanerService.getCleanerDifference(id);

        response.setContentType(CONTENT_TYPE_TEXT);
        if (cd != null) {
            response.getOutputStream().write(cd.getDifference().getBytes(StandardCharsets.UTF_8));
        }
        response.getOutputStream().flush();
    }
}
