/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.resource
 *             FILE : RedirectResourceController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Mar 31, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.resource;

import eu.europa.ec.opoce.cellar.cl.domain.sparql.UriTemplates;
import eu.europa.ec.opoce.cellar.cl.service.client.UriTemplatesService;
import eu.europa.ec.opoce.cellar.common.http.headers.HttpHeadersBuilder;
import eu.europa.ec.opoce.cellar.common.http.headers.directives.CacheControl;
import eu.europa.ec.opoce.cellar.common.http.headers.directives.Tcn;
import eu.europa.ec.opoce.cellar.semantic.helper.MessageFormatter;
import eu.europa.ec.opoce.cellar.server.service.DisseminationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.util.UriTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Mar 31, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
public class FallbackRedirectResourceController extends AbstractResourceController {

    protected final static Logger LOG = LogManager.getLogger(FallbackRedirectResourceController.class);

    @Autowired(required = true)
    private UriTemplatesService uriTemplateService;

    @Autowired(required = true)
    protected DisseminationService disseminationService;

    public ResponseEntity<?> executeSparqlQuery(final HttpServletRequest request, final HttpServletResponse response) {
        ResponseEntity<?> result = null;
        if (cellarConfiguration.isVirtuosoDisseminationSparqlMapperEnabled()) {
            UriTemplates uriTemplates = null;
            final List<UriTemplates> uriTemplatesList = this.uriTemplateService.findAllOrderedBySequence();
            final String base = this.cellarConfiguration.getCellarUriDisseminationBase() + "resource";
            final String undecodedPathInfo = request.getRequestURI().replaceFirst(request.getServletPath(), "");
            final String urlRequest = base + undecodedPathInfo;
            for (final UriTemplates ut : uriTemplatesList) {
                final UriTemplate template = new UriTemplate(ut.getUriPattern());
                if (template.matches(urlRequest)) {
                    uriTemplates = ut;
                    break;
                }
            }

            if (uriTemplates != null) {
                result = disseminationService.doGetSparql(String.valueOf(uriTemplates.getId()), undecodedPathInfo);
            } else {
                // Standard Headers
                final HttpHeadersBuilder httpHeadersBuilder = HttpHeadersBuilder.get().withCacheControl(CacheControl.NO_STORE).withDateNow()
                        .withTcn(Tcn.CHOICE);
                String systemName = undecodedPathInfo.substring(1).substring(0, undecodedPathInfo.substring(1).indexOf("/"));
                final org.springframework.http.HttpHeaders httpHeaders = httpHeadersBuilder.getHttpHeaders();
                result = new ResponseEntity<String>(MessageFormatter.format("Resource [system '{}'] not found.", systemName), httpHeaders, HttpStatus.NOT_FOUND);
            }

        } else {
            result = new ResponseEntity<String>("SPARQL mapper is disabled", HttpStatus.NOT_FOUND);
        }
        return result;
    }

}
