/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.validator
 *             FILE : CreateUpdateJobValidator.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 2, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.validator;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.BatchJobService;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.command.CreateUpdateJob;

import java.util.List;

import org.quartz.CronExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

/**
 * <class_description> Create update job validator.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 2, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component("updateJobValidator")
public class CreateUpdateJobValidator extends CreateJobValidator {

    /**
     * Batch job service.
     */
    @Autowired(required = true)
    private BatchJobService batchJobService;

    /**
     * The Cellar configuration storing runtime parameters.
     */
    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(final Class<?> clazz) {
        return CreateUpdateJob.class.isAssignableFrom(clazz);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validate(final Object target, final Errors errors) {
        super.validate(target, errors);

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "sparqlUpdateQuery", "required.batch-job.job.sparqlQuery",
                "A SPARQL query is required.");

        final CreateUpdateJob createUpdateJob = (CreateUpdateJob) target;
        if (!CronExpression.isValidExpression(createUpdateJob.getCron())) {
            errors.rejectValue("cron", "required.batch-job.job.cron.invalid", "The CRON expression is not a valid expression.");
        }
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "cron", "required.batch-job.job.cron", "A CRON expression is required.");

        final List<String> batchJobCrons = this.batchJobService.findUpdateBatchJobCrons();
        final Integer cellarServiceBatchJobSparqlUpdateMaxCron = Integer
                .valueOf(this.cellarConfiguration.getCellarServiceBatchJobSparqlUpdateMaxCron());
        if (batchJobCrons.size() >= cellarServiceBatchJobSparqlUpdateMaxCron) {
            errors.reject("The maximum of different Cron expression in database is " + cellarServiceBatchJobSparqlUpdateMaxCron,
                    "The maximum of different Cron expression in database is " + cellarServiceBatchJobSparqlUpdateMaxCron);
        }
    }
}
