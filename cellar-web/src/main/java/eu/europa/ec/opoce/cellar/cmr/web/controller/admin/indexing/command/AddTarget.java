/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.indexing.command
 *             FILE : AddWork.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 28, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.indexing.command;

import java.util.ArrayList;
import java.util.List;

/**
 * <class_description> Add work command.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 28, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class AddTarget {

    private String workId;

    private boolean generateIndexNotice;

    private boolean generateEmbeddedNotice;

    private List<String> languages = new ArrayList<>();

    public AddTarget() {
    }

    public String getWorkId() {
        return workId;
    }

    public void setWorkId(String workId) {
        this.workId = workId;
    }

    public boolean isGenerateIndexNotice() {
        return generateIndexNotice;
    }

    public void setGenerateIndexNotice(boolean generateIndexNotice) {
        this.generateIndexNotice = generateIndexNotice;
    }

    public boolean isGenerateEmbeddedNotice() {
        return generateEmbeddedNotice;
    }

    public void setGenerateEmbeddedNotice(boolean generateEmbeddedNotice) {
        this.generateEmbeddedNotice = generateEmbeddedNotice;
    }

    public List<String> getLanguages() {
        return languages;
    }

    public void setLanguages(List<String> languages) {
        this.languages = languages;
    }
}
