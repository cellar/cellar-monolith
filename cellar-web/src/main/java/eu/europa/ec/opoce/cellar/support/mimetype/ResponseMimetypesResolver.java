/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.config
 *        FILE : ResponseMimetypesResolver.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 22-05-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.support.mimetype;

import eu.europa.ec.op.cellar.api.service.ICellarResponse;
import eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration;
import eu.europa.ec.opoce.cellar.common.IResolver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.regex.Pattern;

/**
 * <class_description> This component resolves all in-browser viewable mimetypes
 * against the provided map, the key of which is a regex-based mimetype, and the
 * value is the mimetype to be converted to, in order that the browser could
 * display the document properly. All non-listed content types (such as
 * application/pdf) are presented as attachments to be downloaded, instead of
 * being displayed in-browser The map is forced to be a
 * <code>LinkedHashMap</code> because the order of elements in the conversion
 * map is important, as it defines priority! <br/>
 * <br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc. <br/>
 * <br/>
 * ON : 22-05-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
@Component
public class ResponseMimetypesResolver implements IResolver<ICellarResponse, String> {

	@SuppressWarnings("unused")
	private static final Logger LOG = LogManager.getLogger(ResponseMimetypesResolver.class);

	@Autowired
	@Value("#{responseMimetypesConversionMap}")
	private LinkedHashMap<String, String> responseMimetypesConversionMap;

	private LinkedHashMap<Pattern, String> internalMimetypesConversionMap;

	@PostConstruct
	private void init() {
		this.internalMimetypesConversionMap = new LinkedHashMap<Pattern, String>();
		for (String currKey : this.responseMimetypesConversionMap.keySet()) {
			this.internalMimetypesConversionMap.put(Pattern.compile(currKey),
					this.responseMimetypesConversionMap.get(currKey));
		}
		LOG.info(IConfiguration.CONFIG, "{} MIME types registered: {}", internalMimetypesConversionMap.size(),
				internalMimetypesConversionMap);
	}

	/**
	 * It solves the response's content type against the in-browser viewable
	 * mimetypes.
	 * 
	 * @param the
	 *            response
	 * @return the response mimetype, or <code>null</code> if no mimetype is
	 *         resolved
	 * 
	 * @see eu.europa.ec.opoce.cellar.common.IResolver#resolve(java.lang.Object)
	 */
	public String resolve(final ICellarResponse response) {
		final List<String> responseMimetypes = response.getHeaders().get("Content-Type");
		String resolvedResponseMimetype = null;
		for (String responseMimetype : responseMimetypes) {
			for (Pattern currMimetypeRegex : this.internalMimetypesConversionMap.keySet()) {
				if (currMimetypeRegex.matcher(responseMimetype).find()) {
					resolvedResponseMimetype = this.internalMimetypesConversionMap.get(currMimetypeRegex);
					break;
				}
			}
			if (resolvedResponseMimetype != null) {
				break;
			}
		}
		return resolvedResponseMimetype;
	}

}
