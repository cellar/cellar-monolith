/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.resource
 *             FILE : AbstractResourceController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 2, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.resource;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.server.dissemination.converter.*;
import eu.europa.ec.opoce.cellar.server.service.DisseminationException;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;
import org.springframework.web.util.UrlPathHelper;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Writer;
import java.util.Arrays;
import java.util.List;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page. <br/>
 * <br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc. <br/>
 * <br/>
 * ON : Apr 2, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@SuppressWarnings("deprecation")
@Controller
public abstract class AbstractResourceController extends AbstractCellarController {

    protected final static Logger LOG = LogManager.getLogger(AbstractResourceController.class);

    private final static String SLASH = "/";

    private final static List<HttpStatus> DISSEMINATION_STACK_TRACE_IGNORE = Arrays.asList(HttpStatus.NOT_FOUND, HttpStatus.NOT_ACCEPTABLE);

    @Autowired(required = true)
    private RequestMappingHandlerAdapter requestMappingHandlerAdapter;

    private UrlPathHelper urlPathHelper;

    @PostConstruct
    private void init() {
        final List<HttpMessageConverter<?>> messageConverters = this.requestMappingHandlerAdapter.getMessageConverters();
        messageConverters.add(0, new JenaModelHttpMessageConverter());
        messageConverters.add(0, new NoticeHttpMessageConverter());
        messageConverters.add(0, new ContentStreamsHttpMessageConverter());
        messageConverters.add(0, new ContentStreamListHttpMessageConverter());
        messageConverters.add(0, new CellarResourceHttpMessageConverter());

        this.requestMappingHandlerAdapter
                .setMessageConverters(messageConverters);

        this.urlPathHelper = new UrlPathHelper();
        this.urlPathHelper.setUrlDecode(false);
    }

    /**
     * Gets the url path helper.
     *
     * @return the url path helper
     */
    protected UrlPathHelper getUrlPathHelper() {
        return this.urlPathHelper;
    }

    /**
     * Handle dissemination exception.
     *
     * @param exception the exception
     * @param response the response
     * @param request the request
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @ExceptionHandler(DisseminationException.class)
    public void handleDisseminationException(final DisseminationException exception, final HttpServletResponse response,
                                             final HttpServletRequest request) throws IOException {
        if (DISSEMINATION_STACK_TRACE_IGNORE.contains(exception.getHttpStatus())) {
            LOG.warn(exception.toString());
        } else {
            LOG.warn(exception.toString(), exception);
        }

        writeMessageStatusToResponse(response, exception.getMessage(), exception.getHttpStatus());
    }

    /**
     * Handle cellar exception.
     *
     * @param exception the exception
     * @param response the response
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @ExceptionHandler(CellarException.class)
    public void handleCellarException(final CellarException exception, final HttpServletResponse response) throws IOException {
        LOG.error("Resource retrieving failed", exception);

        writeMessageStatusToResponse(response, exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Handle missing servlet request parameter exception.
     *
     * @param e the e
     * @param response the response
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public void handleMissingServletRequestParameterException(final MissingServletRequestParameterException e,
            final HttpServletResponse response) throws IOException {

        final Exception exception = HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class)
                .withHttpStatus(HttpStatus.BAD_REQUEST).withCause(e).withMessageArgs("Missing required request parameter.").build();

        LOG.warn("Resource retrieving failed", exception);

        writeMessageStatusToResponse(response, exception.getMessage(), HttpStatus.BAD_REQUEST);
    }

    /**
     * Handle exception.
     *
     * @param exception the exception
     * @param response the response
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @ExceptionHandler(Exception.class)
    public void handleException(final Exception exception, final HttpServletResponse response) throws IOException {
        LOG.error("Resource retrieving failed", exception);

        writeMessageStatusToResponse(response, exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /**
     * Gets the identifier.
     *
     * @param resourcePath the resource path
     * @param prefix the prefix
     * @return the identifier
     */
    public static String getIdentifier(final String resourcePath, final String prefix) {
        final String prefixPath = SLASH + prefix;
        return StringUtils.removeStart(resourcePath, prefixPath + SLASH);
    }

    /**
     * Write messange status to response.
     *
     * @param response the response
     * @param message the message
     * @param status the status
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private static void writeMessageStatusToResponse(final HttpServletResponse response, final String message, final HttpStatus status)
            throws IOException {
        if (status != null) {
            response.setStatus(status.value());
        } else {
            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        }

        final Writer writer = response.getWriter();
        if (message != null) {
            writer.write(message);
        }

        writer.flush();
        writer.close();
    }

}
