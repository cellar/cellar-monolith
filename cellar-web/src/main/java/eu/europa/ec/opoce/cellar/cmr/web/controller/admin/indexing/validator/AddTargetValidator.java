/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.indexing.validator
 *             FILE : AddWorkValidator.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 28, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.indexing.validator;

import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.indexing.command.AddTarget;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

/**
 * <class_description> Add work validator.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 28, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component("indexingAddTargetValidator")
public class AddTargetValidator implements Validator {

    @Override
    public boolean supports(final Class<?> clazz) {
        return AddTarget.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(final Object target, final Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "workId", "required.indexing.addTarget.workId", "A work identifier is required.");
        final AddTarget addTarget = (AddTarget) target;
        if (!addTarget.isGenerateEmbeddedNotice() && !addTarget.isGenerateIndexNotice()) {
            errors.rejectValue("generateIndexNotice", "required.batch-job.reindex.job.generateIndexNotice",
                    "At least, a checkbox must be checked");
            errors.rejectValue("generateEmbeddedNotice", "required.batch-job.reindex.job.generateEmbeddedNotice",
                    "At least, a checkbox must be checked");
        }
    }
}
