/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : CellarIdentifier.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 9 mars 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.history.datatable;

import eu.europa.ec.opoce.cellar.cl.service.datatable.AbstractJdbcDatatableMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

/**
 * The Class IdentifiersHistoryDatatableMapper.
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 9 mars 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class IdentifiersHistoryDatatableMapper extends AbstractJdbcDatatableMapper {

    private static final String TABLE_NAME = "HISTORY_PID_CID";

    /** The Constant COLUMNS. */
    private static final String[] COLUMNS = new String[] {
            "cellarId", "productionId", "creationDate", "obsolete", "id"};

    /** The Constant FILTERED_ROWS. */
    private static final Boolean[] FILTERED_ROWS = new Boolean[] {
            Boolean.TRUE, Boolean.TRUE, Boolean.FALSE, Boolean.TRUE, Boolean.FALSE};

    enum DB_MAPPING {
        cellarId("CELLAR_IDENTIFIER"), productionId("PRODUCTION_IDENTIFIER"), creationDate("CREATION_DATE"), obsolete("OBSOLETE"), id("ID");

        private final String columnName;

        private DB_MAPPING(String columnName) {
            this.columnName = columnName;
        }

        public String getColumnName() {
            return columnName;
        }
    }

    /** {@inheritDoc} */
    @Override
    protected String[] getRowNames() {
        return COLUMNS;
    }

    @Override
    protected String getRowName(String columnId) {
        return DB_MAPPING.valueOf(columnId).getColumnName();
    }

    /** {@inheritDoc} */
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    /** {@inheritDoc} */
    @Override
    protected RowMapper<String[]> getRowMapper() {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final RowMapper<String[]> result = new RowMapper<String[]>() {

            @Override
            public String[] mapRow(ResultSet rs, int rowNum) throws SQLException {
                final String cellarId = rs.getString(1);
                final String productionId = rs.getString(2);
                final String format = simpleDateFormat.format(rs.getDate(3));
                final Boolean isObsolete = rs.getBoolean(4);
                final String obsolete = isObsolete != null ? Boolean.toString(isObsolete) : "N/A";
                final String id = Long.toString(rs.getLong(5));
                final String[] identifiersHistoryRow = new String[] {
                        cellarId, productionId, format, obsolete, id};
                return identifiersHistoryRow;
            }
        };
        return result;
    }

    /** {@inheritDoc} */
    @Override
    protected Boolean[] getFilteredRows() {
        return FILTERED_ROWS;
    }

    /** {@inheritDoc} */
    @Override
    protected Object getTransformedValue(final int columnIndex, final String columnValue) {
        Object result = null;
        if (StringUtils.isNotBlank(columnValue)) {
            switch (columnIndex) {
            case 0:
            case 1:
                result = "%" + columnValue + "%";
                break;
            case 3:
            case 4:
                result = Boolean.parseBoolean(columnValue);
                break;
            default:
                break;
            }
        }
        return result;
    }

    /** {@inheritDoc} */
    @Override
    protected String getComparisonType(final int columnIndex, final String columnValue) {
        ComparisonType result = ComparisonType.EQUALS;
        if (StringUtils.isNotBlank(columnValue)) {
            switch (columnIndex) {
            case 0:
            case 1:
                result = ComparisonType.LIKE;
                break;
            default:
                break;
            }
        }
        return result.getSqlStatement();
    }

}
