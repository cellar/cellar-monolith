/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.licenseHolder
 *             FILE : LicenseHolderAddController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Nov 27, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.admin.licenseHolder;

import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionConfiguration.CONFIGURATION_TYPE;
import eu.europa.ec.opoce.cellar.common.util.TimeUtils;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.server.admin.resource.ResourceConfigInvoker;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Nov 27, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
public class LicenseHolderAddController extends LicenseHolderController {

    @Autowired(required = true)
    private ResourceConfigInvoker languageService;

    @ModelAttribute("languages")
    public List<LanguageBean> exposeLanguages() {
        return this.languageService.getLanguages();
    }

    @RequestMapping(value = "/license/add/job", method = RequestMethod.GET)
    public ModelAndView addJob(final HttpServletRequest request, final HttpServletResponse response) {
        return this.constructJobAdd(null);
    }

    @RequestMapping(value = "/license/add/job/new", method = RequestMethod.POST)
    public ModelAndView addNewjob(final HttpServletRequest request, final HttpServletResponse response,
            @RequestParam(value = "jobName") final String jobName, @RequestParam(value = "frequency") final String frequency,
            @RequestParam(value = "language") final String language, @RequestParam(value = "path") final String path,
            @RequestParam(value = "startDate") final String startDate, @RequestParam(value = "sparqlQuery") final String sparqlQuery) {

        if (StringUtils.isBlank(jobName) || StringUtils.isBlank(frequency) || StringUtils.isBlank(language) || StringUtils.isBlank(path)
                || StringUtils.isBlank(sparqlQuery)) {
            return this.constructJobAdd(
                    "Cannot add the job. The job name, the frequency, the language, the path and the query are mandatory.");
        }

        Date start = null;
        try {
            if (StringUtils.isNotBlank(startDate)) {
                start = TimeUtils.getDefaultDate(startDate, true);
            }
        } catch (final ParseException exception) {
            return this.constructJobAdd(exception.getMessage());
        }

        try {
            this.licenseHolderService.addExtractionConfiguration(jobName, frequency, language, path, start, sparqlQuery);
        } catch (final Exception exception) {
            return this.constructJobAdd("Cannot add the job. The path must be unique.");
        }

        return new ModelAndView(new RedirectView(URL_LIST, true));
    }

    @RequestMapping(value = "/license/add/job/getArchiveName", method = RequestMethod.POST)
    public void getJobArchiveName(final HttpServletRequest request, final HttpServletResponse response,
            @RequestParam(value = "frequency") final String frequency, @RequestParam(value = "language") final String language) // isoThreeChar
                    throws IOException {

        CONFIGURATION_TYPE frequencyType = null;
        if (StringUtils.isNotBlank(frequency)) {
            frequencyType = CONFIGURATION_TYPE.valueOf(frequency);
        }

        final String archiveName = this.licenseHolderService.getArchiveNameOverview(frequencyType, language);
        response.setContentType(CONTENT_TYPE_TEXT);
        response.getOutputStream().write(archiveName.getBytes(StandardCharsets.UTF_8));
        response.getOutputStream().flush();
    }

    @RequestMapping(value = "/license/add/job/getArchivePath", method = RequestMethod.POST)
    public void getJobArchivePath(final HttpServletRequest request, final HttpServletResponse response,
            @RequestParam(value = "frequency") final String frequency, @RequestParam(value = "language") final String language)
                    throws IOException {

        CONFIGURATION_TYPE frequencyType = null;
        if (StringUtils.isNotBlank(frequency)) {
            frequencyType = CONFIGURATION_TYPE.valueOf(frequency);
        }

        final String archiveName = this.licenseHolderService.getArchivePathOverview(frequencyType, language);
        response.setContentType(CONTENT_TYPE_TEXT);
        response.getOutputStream().write(archiveName.getBytes(StandardCharsets.UTF_8));
        response.getOutputStream().flush();
    }

    @RequestMapping(value = "/license/add/adhoc", method = RequestMethod.GET)
    public ModelAndView addAdhoc(final HttpServletRequest request, final HttpServletResponse response) {
        return this.constructAdhocAdd(null);
    }

    @RequestMapping(value = "/license/add/adhoc/new", method = RequestMethod.POST)
    public ModelAndView addNewAdhoc(final HttpServletRequest request, final HttpServletResponse response,
            @RequestParam(value = "jobName") final String jobName, @RequestParam(value = "language") final String language,
            @RequestParam(value = "path") final String path, @RequestParam(value = "startDate") final String startDate,
            @RequestParam(value = "startTime") final String startTime, @RequestParam(value = "sparqlQuery") final String sparqlQuery) {
        Date startExecutionDate = null;

        if (StringUtils.isBlank(jobName) || StringUtils.isBlank(language) || StringUtils.isBlank(path)
                || StringUtils.isBlank(sparqlQuery)) {
            return this.constructAdhocAdd("Cannot add the job. The job name, the language, the path and the query are mandatory.");
        }

        try {
            if (StringUtils.isNotBlank(startDate)) {
                if (StringUtils.isNotBlank(startTime)) {
                    startExecutionDate = TimeUtils.getDefaultDateTime(startDate, startTime, true);
                } else {
                    startExecutionDate = TimeUtils.getDefaultDate(startDate, true);
                }
            }
        } catch (final ParseException exception) {
            return this.constructAdhocAdd(exception.getMessage());
        }

        try {
            this.licenseHolderService.addAdhocExtractionConfiguration(jobName, language, path, startExecutionDate, sparqlQuery);
        } catch (final Exception exception) {
            return this.constructAdhocAdd("Cannot add the ad-hoc extraction. The path must be unique.");
        }
        return new ModelAndView(new RedirectView(URL_LIST, true));
    }

    @RequestMapping(value = "/license/add/adhoc/getArchiveName", method = RequestMethod.POST)
    public void getAdhocArchiveName(final HttpServletRequest request, final HttpServletResponse response,
            @RequestParam(value = "language") final String language // isoThreeChar
    ) throws IOException {

        final String archiveName = this.licenseHolderService.getArchiveNameOverview(CONFIGURATION_TYPE.STD, language);
        response.setContentType(CONTENT_TYPE_TEXT);
        response.getOutputStream().write(archiveName.getBytes(StandardCharsets.UTF_8));
        response.getOutputStream().flush();
    }

    @RequestMapping(value = "/license/add/job/isStartDateValid", method = RequestMethod.POST)
    public void isStartDateValid(final HttpServletRequest request, final HttpServletResponse response,
            @RequestParam(value = "frequency") final String frequency, @RequestParam(value = "startDate") final String startDate)
                    throws IOException {

        Boolean isValid = false;

        if (StringUtils.isNotBlank(frequency) && StringUtils.isNotBlank(startDate)) {
            final CONFIGURATION_TYPE frequencyType = CONFIGURATION_TYPE.valueOf(frequency);

            Date start = null;
            try {
                start = TimeUtils.getDefaultDate(startDate, true);
                isValid = this.licenseHolderService.isStartDateValid(frequencyType, start);
            } catch (final ParseException exception) {
                isValid = false;
            }
        }

        response.setContentType(CONTENT_TYPE_TEXT);
        response.getOutputStream().write(isValid.toString().getBytes(StandardCharsets.UTF_8));
        response.getOutputStream().flush();
    }

    private ModelAndView constructJobAdd(final String errorMessage) {
        final ModelAndView modelAndView = new ModelAndView("jobAdd");
        if (errorMessage != null) {
            modelAndView.addObject("errorMessage", errorMessage);
        }

        modelAndView.addObject("frequencies", this.licenseHolderService.getFrequencies()); // gets the frequencies
        modelAndView.addObject("deadlines", this.licenseHolderService.getDeadlines()); // gets the deadlines
        modelAndView.addObject("extractionsFolder", super.cellarConfiguration.getCellarFolderLicenseHolderArchiveExtraction());

        return modelAndView;
    }

    private ModelAndView constructAdhocAdd(final String errorMessage) {
        final ModelAndView modelAndView = new ModelAndView("adhocAdd");
        if (errorMessage != null) {
            modelAndView.addObject("errorMessage", errorMessage);
        }

        modelAndView.addObject("extractionsFolder", super.cellarConfiguration.getCellarFolderLicenseHolderArchiveAdhocExtraction());

        return modelAndView;
    }
}
