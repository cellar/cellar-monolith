/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.security
 *             FILE : GroupManager.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 13, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.admin.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL;
import eu.europa.ec.opoce.cellar.cl.service.client.SecurityService;

/**
 * <class_description> Group manager controller.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 13, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
@RequestMapping(value = GroupManagerController.PAGE_URL)
public class GroupManagerController extends AbstractCellarController {

    public static final String PAGE_URL = "/security/group";

    public static final String DELETE_URL = "/delete";

    private static final String JSP_NAME = "security/group";

    /**
     * Security service manager.
     */
    @Autowired
    private SecurityService securityService;

    /**
     * Handle the GET.
     */
    @RequestMapping(method = RequestMethod.GET)
    public String groupManagerGet(final ModelMap model) {

        model.addAttribute("groups", this.securityService.findAllGroups()); // gets all groups
        return JSP_NAME;
    }

    /**
     * Handle the delete GET.
     */
    @RequestMapping(value = GroupManagerController.DELETE_URL, method = RequestMethod.GET)
    public String groupManagerDelete(@RequestParam(required = true) Long id) {
        try {
            this.securityService.deleteGroup(id); // deletes the group
        } catch (Exception e) {
            super.addUserMessage(LEVEL.ERROR, "security.group.manager.groupHasUser",
                    "The group cannot be deleted because it is related to user(s).");
            return redirect(EditGroupController.PAGE_URL, "id", id);
        }

        super.addUserMessage(LEVEL.SUCCESS, "common.message.successfullyProcessed");
        return redirect(GroupManagerController.PAGE_URL); // redirects to the group manager
    }
}
