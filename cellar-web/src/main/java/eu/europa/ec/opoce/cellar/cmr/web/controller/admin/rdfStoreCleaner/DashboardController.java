/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.rdfStoreCleaner
 *             FILE : DashboardController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Sep 24, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.rdfStoreCleaner;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL;
import eu.europa.ec.opoce.cellar.cl.service.rdfStoreCleaner.RDFStoreCleanerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Sep 24, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller("rdfStoreCleanerDashboardController")
@RequestMapping(DashboardController.PAGE_URL)
public class DashboardController extends AbstractCellarController {

    public static final String PAGE_URL = "/cmr/rdfStoreCleaner";

    public static final String DASHBOARD_URL = "/dashboard";

    public static final String START_CLEANING_URL = "/startCleaning";

    public static final String STOP_CLEANING_URL = "/stopCleaning";

    public static final String NUMBER_RESOURCES = "/numberMetadataResources";

    public static final String NUMBER_RESOURCES_DIAGNOSABLE = "/numberMetadataResourcesDiagnosable";

    public static final String NUMBER_RESOURCES_FIXABLE = "/numberMetadataResourcesFixable";

    public static final String NUMBER_RESOURCES_ERROR = "/numberMetadataResourcesError";

    public static final String NUMBER_ROWS_CLEANER_DIFFERENCE = "/numberRowsCleanerDifference";

    private static final String JSP_NAME = "cmr/rdfStoreCleaner/dashboard";

    @Autowired
    private RDFStoreCleanerService rdfStoreCleanerService;

    /**
     * Handle the GET.
     */
    @RequestMapping(method = RequestMethod.GET)
    public String indexingGet() {
        return redirect(PAGE_URL + DASHBOARD_URL);
    }

    /**
     * Handle the GET.
     */
    @RequestMapping(value = DashboardController.DASHBOARD_URL, method = RequestMethod.GET)
    public String dashboardGet(final ModelMap model) {
        model.addAttribute("cleaningServiceStatus", this.rdfStoreCleanerService.getCleaningStatus());
        return JSP_NAME;
    }

    @RequestMapping(value = DashboardController.NUMBER_RESOURCES, method = RequestMethod.GET)
    public void numberMetadataResourcesGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        response.setContentType(CONTENT_TYPE_TEXT);
        response.getOutputStream().write(Long.toString(this.rdfStoreCleanerService.getNumberMetadataResources()).getBytes(StandardCharsets.UTF_8));
        response.getOutputStream().flush();
    }

    @RequestMapping(value = DashboardController.NUMBER_RESOURCES_DIAGNOSABLE, method = RequestMethod.GET)
    public void numberMetadataResourcesDiagnosableGet(final HttpServletRequest request, final HttpServletResponse response)
            throws IOException {
        response.setContentType(CONTENT_TYPE_TEXT);
        response.getOutputStream().write(Long.toString(this.rdfStoreCleanerService.getNumberMetadataResourcesDiagnosable()).getBytes(StandardCharsets.UTF_8));
        response.getOutputStream().flush();
    }

    @RequestMapping(value = DashboardController.NUMBER_RESOURCES_FIXABLE, method = RequestMethod.GET)
    public void numberMetadataResourcesFixableGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        response.setContentType(CONTENT_TYPE_TEXT);
        response.getOutputStream().write(Long.toString(this.rdfStoreCleanerService.getNumberMetadataResourcesFixable()).getBytes(StandardCharsets.UTF_8));
        response.getOutputStream().flush();
    }

    @RequestMapping(value = DashboardController.NUMBER_RESOURCES_ERROR, method = RequestMethod.GET)
    public void numberMetadataResourcesErrorGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        response.setContentType(CONTENT_TYPE_TEXT);
        response.getOutputStream().write(Long.toString(this.rdfStoreCleanerService.getNumberMetadataResourcesError()).getBytes(StandardCharsets.UTF_8));
        response.getOutputStream().flush();
    }

    @RequestMapping(value = DashboardController.NUMBER_ROWS_CLEANER_DIFFERENCE, method = RequestMethod.GET)
    public void numberRowsCleanerDifferenceGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        response.setContentType(CONTENT_TYPE_TEXT);
        response.getOutputStream().write(Long.toString(this.rdfStoreCleanerService.getNumberRowsCleanerDifference()).getBytes(StandardCharsets.UTF_8));
        response.getOutputStream().flush();
    }

    @RequestMapping(value = DashboardController.START_CLEANING_URL, method = RequestMethod.GET)
    public String startGet() {
        this.rdfStoreCleanerService.startCleaning();
        super.addUserMessage(LEVEL.SUCCESS, "common.message.successfullyProcessed");
        return redirect(PAGE_URL);
    }

    @RequestMapping(value = DashboardController.STOP_CLEANING_URL, method = RequestMethod.GET)
    public String stopGet() {
        this.rdfStoreCleanerService.stopCleaning();
        super.addUserMessage(LEVEL.SUCCESS, "common.message.successfullyProcessed");
        return redirect(PAGE_URL);
    }
}
