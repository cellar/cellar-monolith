package eu.europa.ec.opoce.cellar.cmr.web.controller.admin;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.HierarchyElement;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.server.admin.embargo.EmbargoHandlingServiceInvoker;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;

@Deprecated
@Controller
public class EmbargoController extends AbstractCellarController {

	private static String URL_LIST = "/admin/cmr/embargo/list";
	private static String URL_ADD = "/admin/cmr/embargo/add";

	@Autowired(required = true)
	EmbargoHandlingServiceInvoker embargoService;

	@RequestMapping(value = "/cmr/embargo", method = RequestMethod.GET)
	public ModelAndView getEmbargoInterface(final HttpServletRequest request, final HttpServletResponse response)
			throws IOException {
		super.addUserMessage(LEVEL.WARNING, "common.message.deprecated");
		return new ModelAndView(new RedirectView("/admin/cmr/embargo/add", true));
	}

	@RequestMapping(value = "/cmr/embargo/list", method = RequestMethod.GET)
	public ModelAndView getEmbargoInterfaceList(final HttpServletRequest request, final HttpServletResponse response)
			throws IOException {
		final ModelAndView m = new ModelAndView("embargoList");
		m.addObject("list", embargoService.getEmbargoHierarchyElements());
		super.addUserMessage(LEVEL.WARNING, "common.message.deprecated");
		return m;
	}

	@RequestMapping(value = "/cmr/embargo/list/remove", method = RequestMethod.POST)
	public ModelAndView removeFromEmbargo(final HttpServletRequest request, final HttpServletResponse response)
			throws IOException {
		@SuppressWarnings("serial")
		final Set<String> uris = new HashSet<String>() {

			{
				addAll(request.getParameterMap().keySet());
			}
		};
		embargoService.removeFromEmbargo(uris);
		super.addUserMessage(LEVEL.WARNING, "common.message.deprecated");
		return new ModelAndView(new RedirectView(URL_LIST, true));
	}

	@RequestMapping(value = "/cmr/embargo/list/changeEmbargo", method = RequestMethod.POST)
	public ModelAndView changeEmbargo(final HttpServletRequest request, final HttpServletResponse response)
			throws IOException {
		final String uri = request.getParameter("uri");
		final String date = request.getParameter("date");
		embargoService.changeEmbargo(uri, date);
		super.addUserMessage(LEVEL.WARNING, "common.message.deprecated");
		return new ModelAndView(new RedirectView(URL_LIST, true));
	}

	@RequestMapping(value = "/cmr/embargo/add", method = RequestMethod.GET)
	public ModelAndView getEmbargoInterfaceAdd(final HttpServletRequest request, final HttpServletResponse response)
			throws IOException {
		super.addUserMessage(LEVEL.WARNING, "common.message.deprecated");
		return new ModelAndView("embargoAdd");
	}

	@RequestMapping(value = "/cmr/embargo/add", method = RequestMethod.POST)
	public void getEmbargoInterfaceAddPost(final HttpServletRequest request, final HttpServletResponse response,
			@RequestParam(value = "psid") final String psid) throws IOException {
		final StringBuilder sb = new StringBuilder();
		if (embargoService.checkPsid(psid)) {
			final HierarchyElement<?, ?> hierarchyElement = embargoService.search(psid);
			if (hierarchyElement != null) {
				sb.append(hierarchyElement.getEmbargoDate()).append(";");
				sb.append(StringUtils.join(hierarchyElement.getProductionUris(), ","));
			}
		}
		response.setContentType("text/plain");
		response.getOutputStream().write(sb.toString().getBytes(StandardCharsets.UTF_8));
		response.getOutputStream().flush();
	}

	@RequestMapping(value = "/cmr/embargo/add/work", method = RequestMethod.POST)
	public ModelAndView addToEmbargo(final HttpServletRequest request, final HttpServletResponse response)
			throws IOException {
		final String uri = request.getParameter("uri");
		final String date = request.getParameter("date");
		embargoService.addToEmbargo(uri, date);
		super.addUserMessage(LEVEL.WARNING, "common.message.deprecated");
		return new ModelAndView(new RedirectView(URL_LIST, true));
	}

	@RequestMapping(value = "/cmr/embargo/search", method = RequestMethod.GET)
	public ModelAndView getEmbargoInterfaceSearch(final HttpServletRequest request, final HttpServletResponse response)
			throws IOException {
		super.addUserMessage(LEVEL.WARNING, "common.message.deprecated");
		return new ModelAndView("embargoSearch");
	}

	@RequestMapping(value = "/cmr/embargo/search/list", method = RequestMethod.POST)
	public ModelAndView getEmbargoInterfaceSearchList(final HttpServletRequest request,
			final HttpServletResponse response) throws IOException {
		final String psid = request.getParameter("psid");
		final boolean doesPsidExist = embargoService.checkPsid(psid);
		final ModelAndView modelAndView = new ModelAndView();
		if (doesPsidExist) {
			modelAndView.addObject(DigitalObjectType.WORK.toString(), embargoService.search(psid));
		}
		modelAndView.addObject("status", doesPsidExist);
		modelAndView.addObject("psid", psid);
		modelAndView.setView(new RedirectView(URL_ADD, true));
		super.addUserMessage(LEVEL.WARNING, "common.message.deprecated");
		return modelAndView;
	}
}
