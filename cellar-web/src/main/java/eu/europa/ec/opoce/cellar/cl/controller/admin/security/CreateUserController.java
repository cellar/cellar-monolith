/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.security
 *             FILE : CreateUserController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 10, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.admin.security;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.admin.security.command.CreateUserCommand;
import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL;
import eu.europa.ec.opoce.cellar.cl.domain.admin.Group;
import eu.europa.ec.opoce.cellar.cl.domain.admin.User;
import eu.europa.ec.opoce.cellar.cl.service.client.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;

import java.util.List;

/**
 * <class_description> Create a new user controller.
 *
 * <br/>
 * <br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc. <br/>
 * <br/>
 * ON : Jan 10, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
@RequestMapping(CreateUserController.PAGE_URL)
public class CreateUserController extends AbstractCellarController {

    public static final String PAGE_URL = "/security/user/create";

    private static final String JSP_NAME = "security/user/create";

    private static final String OBJECT_NAME = "createUserCommand";

    /**
     * Security service manager.
     */
    @Autowired
    private SecurityService securityService;

    /**
     * Validator for a created/edited user.
     */
    @Autowired
    @Qualifier("userValidator")
    private Validator userValidator;

    /**
     * Returns all the groups.
     *
     * @return all the groups
     */
    @ModelAttribute("groupsList")
    public List<Group> populateGroupsList() {
        return this.securityService.findAllGroups();
    }

    /**
     * Handle the GET.
     */
    @RequestMapping(method = RequestMethod.GET)
    public String createUserGet(final ModelMap model) {
        model.addAttribute(OBJECT_NAME, new CreateUserCommand()); // empty user 
        return JSP_NAME;
    }

    /**
     * Handle the POST.
     */
    @RequestMapping(method = RequestMethod.POST)
    public String createUserPost(final ModelMap model, @ModelAttribute(OBJECT_NAME) final CreateUserCommand createdUser,
            final BindingResult bindingResult, final SessionStatus status) {
        this.userValidator.validate(createdUser, bindingResult);
        if (bindingResult.hasErrors()) {
            return JSP_NAME;
        }

        final Group group = new Group();
        group.setId(createdUser.getGroupId());
        final User user = new User();
        user.setEnabled(createdUser.isEnabled());
        user.setGroup(group);
        user.setUsername(createdUser.getUsername());

        try {
            this.securityService.createUser(user); // saves the user
        } catch (final Exception exception) {
            bindingResult.rejectValue("username", "unique.security.user.username", "The username is already in use.");
            return JSP_NAME;
        }

        super.addUserMessage(LEVEL.SUCCESS, "common.message.successfullySaved");
        return redirect(UserManagerController.PAGE_URL); // redirects to the user manager
    }
}
