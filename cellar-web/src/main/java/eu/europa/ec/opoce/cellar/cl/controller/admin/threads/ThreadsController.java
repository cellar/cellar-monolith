package eu.europa.ec.opoce.cellar.cl.controller.admin.threads;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.ConfigurationPropertyService.EXISTINGPROPS_MODE;
import eu.europa.ec.opoce.cellar.cl.configuration.impl.CellarPersistentConfiguration;
import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.dao.SIPPackageDao;
import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.SIPPackage;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CmrIndexRequestDao;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest;
import eu.europa.ec.opoce.cellar.exception.CellarConfigurationException;
import eu.europa.ec.opoce.cellar.ingestion.service.QueueWatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * The Class ThreadsController.
 */
@Controller
public class ThreadsController extends AbstractCellarController {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger(ThreadsController.class);

    /** The sip watcher. */
    @Autowired
    private QueueWatcher queueWatcher;
    
    /** The cellar persistent configuration. */
    @Autowired
    @Qualifier("cellarConfiguration")
    private CellarPersistentConfiguration cellarPersistentConfiguration;

    /** The cmr index request dao. */
    @Autowired
    private CmrIndexRequestDao cmrIndexRequestDao;

    @Autowired
    private SIPPackageDao sipPackageDao;
    
    /**
     * Restart ingestion.
     *
     * @param request the request
     * @param response the response
     * @param threadsViewForm the threads view form
     * @param result the result
     * @return the model and view
     * @throws Exception the exception
     */
    @RequestMapping(method = RequestMethod.POST, value = "/threads")
    public ModelAndView actionOverThreadsView(final HttpServletRequest request, final HttpServletResponse response,
            @ModelAttribute("threadsViewForm") ThreadsViewForm threadsViewForm, final BindingResult result) throws Exception {
        final Boolean ingestionActive = threadsViewForm.getIngestionActive();
        if (ingestionActive != null) {
            this.changeIngestion(ingestionActive.booleanValue());
        }
        final Boolean indexationActive = threadsViewForm.getIndexationActive();
        if (indexationActive != null) {
            this.changeIndexation(indexationActive.booleanValue());
        }

        final Map<String, Object> ingestionModel = this.createIngestionModel();
        final Map<String, Object> indexationModel = this.createIndexationModel();
        final Map<String, Object> model = new HashMap<String, Object>();
        model.putAll(ingestionModel);
        model.putAll(indexationModel);
        threadsViewForm = new ThreadsViewForm();
        model.put("threadsViewForm", threadsViewForm);
        return new ModelAndView("threads", model);
    }

    /**
     * Change indexation.
     * used to start/stop indexation
     *
     * @param value the value
     */
    public void changeIndexation(final boolean value) {
        try {
            LOG.info("Re-Starting ingestion.");
            //this will enable ingestion
            this.cellarConfiguration.setCellarServiceIndexingEnabled(value, EXISTINGPROPS_MODE.TO_DB);
        } catch (final CellarConfigurationException e) {
            // just ignore the exception if it is due to a write attempt to a read-only database
            if (!CommonErrors.READONLY_DATABASE.equals(e.getCode())) {
                throw e;
            }
        }
    }

    /**
     * Change ingestion.
     * used to start stop ingestion 
     *
     * @param value the value
     */
    public void changeIngestion(final boolean value) {
        try {
            LOG.info("Re-Starting ingestion.");
            //this will enable ingestion
            this.cellarConfiguration.setCellarServiceIngestionEnabled(value, EXISTINGPROPS_MODE.TO_DB);
        } catch (final CellarConfigurationException e) {
            // just ignore the exception if it is due to a write attempt to a read-only database
            if (!CommonErrors.READONLY_DATABASE.equals(e.getCode())) {
                throw e;
            }
        }
    }

    /**
     * Creates the indexation model.
     *
     * @return the map
     */
    private Map<String, Object> createIndexationModel() {
        final Map<String, Object> model = new HashMap<String, Object>();
        final List<CmrIndexRequest> waitingCmrIndexRequests = this.cmrIndexRequestDao.getWaitingIndexationRequestGroupedByCellarGroup();
        final List<CmrIndexRequest> processingCmrIndexRequests = this.cmrIndexRequestDao
                .getProcessingIndexationRequestGroupedByCellarGroup();
        model.put("indexationProcessingQueue", processingCmrIndexRequests);
        model.put("indexationWaitingQueue", waitingCmrIndexRequests);
        final boolean cellarServiceIndexationEnabled = this.cellarPersistentConfiguration.isCellarServiceIndexingEnabled();
        model.put("isIndexationRunning", cellarServiceIndexationEnabled);
        return model;
    }

    /**
     * Creates the model.
     *
     * @return the map
     */
    private Map<String, Object> createIngestionModel() {
        // The view's iterator is a "weakly consistent" iterator that will never throw ConcurrentModificationException,
        // and guarantees to traverse elements as they existed upon construction of the iterator, and may (but is not
        // guaranteed to) reflect any modifications subsequent to construction.
    	
    	final List<SIPPackage> submittedToExecutor = this.queueWatcher.getIngestionTreatments().keySet().stream()
    			.map(SIPPackage::toSIPPackage)
    			.collect(Collectors.toList());
        final List<SIPPackage> scheduledPackages = this.sipPackageDao.findScheduledOrdered();


        final Map<String, Object> model = new HashMap<String, Object>();
        model.put("ingestionProcessingQueue", submittedToExecutor);
        model.put("ingestionWaitingQueue", scheduledPackages);

        final boolean cellarServiceIngestionEnabled = this.cellarPersistentConfiguration.isCellarServiceIngestionEnabled();
        model.put("isIngestionRunning", cellarServiceIngestionEnabled);

        return model;
    }

    /**
     * Show threads view.
     *
     * @param request the request
     * @param response the response
     * @return the model and view
     * @throws Exception the exception
     */
    @RequestMapping(method = RequestMethod.GET, value = "/threads")
    public ModelAndView showThreadsView(final HttpServletRequest request, final HttpServletResponse response) throws Exception {

        final Map<String, Object> ingestionModel = this.createIngestionModel();
        final Map<String, Object> indexationModel = this.createIndexationModel();
        final Map<String, Object> model = new HashMap<String, Object>();
        model.putAll(ingestionModel);
        model.putAll(indexationModel);

        final ThreadsViewForm threadsViewForm = new ThreadsViewForm();
        model.put("threadsViewForm", threadsViewForm);
        return new ModelAndView("threads", model);
    }
}
