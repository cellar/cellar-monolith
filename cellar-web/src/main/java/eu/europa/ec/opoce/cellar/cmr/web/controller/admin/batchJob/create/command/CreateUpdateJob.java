/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.command
 *             FILE : CreateExportJob.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 2, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.command;

/**
 * <class_description> Create export job.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 2, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class CreateUpdateJob extends CreateJob {

    /**
     * The cron.
     */
    protected String cron;

    /**
     * The SPARQL update query of the job.
     */
    protected String sparqlUpdateQuery;

    /**
     * Default constructor.
     */
    public CreateUpdateJob() {
        super();
    }

    /**
     * @return the cron
     */
    public String getCron() {
        return this.cron;
    }

    /**
     * @param cron the cron to set
     */
    public void setCron(final String cron) {
        this.cron = cron;
    }

    /**
     * @return the sparqlUpdateQuery
     */
    public String getSparqlUpdateQuery() {
        return this.sparqlUpdateQuery;
    }

    /**
     * @param sparqlUpdateQuery the sparqlUpdateQuery to set
     */
    public void setSparqlUpdateQuery(final String sparqlUpdateQuery) {
        this.sparqlUpdateQuery = sparqlUpdateQuery;
    }

}
