/* ----------------------------------------------------------------------------

 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.resource
 *             FILE : RetrieveOJListController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jul 8, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.cellar;

import eu.europa.ec.opoce.cellar.cmr.oj.OfficialJournals;
import eu.europa.ec.opoce.cellar.cmr.service.IRetrieveOJListService;
import eu.europa.ec.opoce.cellar.common.http.headers.HttpHeadersBuilder;
import eu.europa.ec.opoce.cellar.server.conditionalRequest.ConditionalRequestVersions;
import eu.europa.ec.opoce.cellar.server.conditionalRequest.ETagType;
import eu.europa.ec.opoce.cellar.server.service.IConditionalRequestService;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.HttpHeaders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * <class_description>  .
 * <br/><br/>
 * <notes>  .
 * <br/><br/>
 * ON : Jul 8, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
@RequestMapping
public class RetrieveOJListController {

    /** The retrieve oj list service. */
    @Autowired
    private IRetrieveOJListService retrieveOJListService;

    /** The conditional request service. */
    @Autowired
    private IConditionalRequestService conditionalRequestService;

    /**
     * Gets the list of oj series.
     *
     * @param request the request
     * @param response the response
     * @param year the year
     * @param eTag the e tag
     * @param lastModified the last modified
     * @return the list of oj series
     */
    @RequestMapping(method = RequestMethod.GET, value = "/oj/list/series/{year}")
    public ResponseEntity<OfficialJournals> getListOfOjSeries(final HttpServletRequest request, final HttpServletResponse response, //
            @PathVariable final int year, //
            @RequestHeader(value = HttpHeaders.IF_NONE_MATCH, required = false) final String eTag, //
            @RequestHeader(value = HttpHeaders.IF_MODIFIED_SINCE, required = false) final String lastModified) {

        final OfficialJournals list = retrieveOJListService.getTheListOfOjSeries(year);
        final ResponseEntity<OfficialJournals> result = getResponseEntity(list, eTag, lastModified);
        return result;
    }

    /**
     * Gets the list of oj sub series.
     *
     * @param request the request
     * @param response the response
     * @param year the year
     * @param eTag the e tag
     * @param lastModified the last modified
     * @return the list of oj sub series
     */
    @RequestMapping(method = RequestMethod.GET, value = "/oj/list/subseries/{year}")
    public ResponseEntity<OfficialJournals> getListOfOjSubSeries(final HttpServletRequest request, final HttpServletResponse response, //
            @PathVariable final int year, //
            @RequestHeader(value = HttpHeaders.IF_NONE_MATCH, required = false) final String eTag, //
            @RequestHeader(value = HttpHeaders.IF_MODIFIED_SINCE, required = false) final String lastModified) {

        final OfficialJournals list = retrieveOJListService.getTheListOfOjSubSeries(year);
        final ResponseEntity<OfficialJournals> result = getResponseEntity(list, eTag, lastModified);
        return result;
    }

    /**
     * Gets the most recent oj date.
     *
     * @param request the request
     * @param response the response
     * @param year the year
     * @param eTag the e tag
     * @param lastModified the last modified
     * @return the most recent oj date
     */
    @RequestMapping(method = RequestMethod.GET, value = "/oj/list/{year}")
    public ResponseEntity<OfficialJournals> getMostRecentOjDate(final HttpServletRequest request, final HttpServletResponse response, //
            @PathVariable final int year, //
            @RequestHeader(value = HttpHeaders.IF_NONE_MATCH, required = false) final String eTag, //
            @RequestHeader(value = HttpHeaders.IF_MODIFIED_SINCE, required = false) final String lastModified) {

        final OfficialJournals list = retrieveOJListService.getTheDateOfTheMostRecentOj(year);
        final ResponseEntity<OfficialJournals> result = getResponseEntity(list, eTag, lastModified);
        return result;
    }

    /**
     * Gets the OJ list.
     *
     * @param request the request
     * @param response the response
     * @param year the year
     * @param month the month
     * @param day the day
     * @param eTag the e tag
     * @param lastModified the last modified
     * @return the OJ list
     */
    @RequestMapping(method = RequestMethod.GET, value = "/oj/period/{year}/{month}/{day}")
    public ResponseEntity<OfficialJournals> getOJList(final HttpServletRequest request, final HttpServletResponse response, //
            @PathVariable final int year, //
            @PathVariable final int month, //
            @PathVariable final int day, //
            @RequestHeader(value = HttpHeaders.IF_NONE_MATCH, required = false) final String eTag, //
            @RequestHeader(value = HttpHeaders.IF_MODIFIED_SINCE, required = false) final String lastModified) {

        final OfficialJournals list = retrieveOJListService.list(year, month, day);
        return getResponseEntity(list, eTag, lastModified);
    }

    /**
     * Gets the OJ list.
     *
     * @param request the request
     * @param response the response
     * @param year the year
     * @param month the month
     * @param eTag the e tag
     * @param lastModified the last modified
     * @return the OJ list
     */
    @RequestMapping(method = RequestMethod.GET, value = "/oj/period/{year}/{month}")
    public ResponseEntity<OfficialJournals> getOJList(final HttpServletRequest request, final HttpServletResponse response, //
            @PathVariable final int year, //
            @PathVariable final int month, //
            @RequestHeader(value = HttpHeaders.IF_NONE_MATCH, required = false) final String eTag, //
            @RequestHeader(value = HttpHeaders.IF_MODIFIED_SINCE, required = false) final String lastModified) {

        return this.getOJList(request, response, year, month, 0, eTag, lastModified);
    }

    /**
     * Gets the OJ list.
     *
     * @param request the request
     * @param response the response
     * @param year the year
     * @param eTag the e tag
     * @param lastModified the last modified
     * @return the OJ list
     */
    @RequestMapping(method = RequestMethod.GET, value = "/oj/period/{year}")
    public ResponseEntity<OfficialJournals> getOJList(final HttpServletRequest request, final HttpServletResponse response, //
            @PathVariable final int year, //
            @RequestHeader(value = HttpHeaders.IF_NONE_MATCH, required = false) final String eTag, //
            @RequestHeader(value = HttpHeaders.IF_MODIFIED_SINCE, required = false) final String lastModified) {

        return this.getOJList(request, response, year, 0, 0, eTag, lastModified);
    }

    /**
     * Gets the OJ list.
     *
     * @param request the request
     * @param response the response
     * @param year the year
     * @param series the series
     * @param subseries the subseries
     * @param number the number
     * @param eTag the e tag
     * @param lastModified the last modified
     * @return the OJ list
     */
    @RequestMapping(method = RequestMethod.GET, value = "/oj/series/{year}/{series}/{subseries}/{number}")
    public ResponseEntity<OfficialJournals> getOJList(final HttpServletRequest request, final HttpServletResponse response, //
            @PathVariable final int year, //
            @PathVariable final String series, //
            @PathVariable final String subseries, //
            @PathVariable final int number, //
            @RequestHeader(value = HttpHeaders.IF_NONE_MATCH, required = false) final String eTag, //
            @RequestHeader(value = HttpHeaders.IF_MODIFIED_SINCE, required = false) final String lastModified) {

        final OfficialJournals list = retrieveOJListService.list(year, series, subseries, number);
        return getResponseEntity(list, eTag, lastModified);
    }

    /**
     * Gets the OJ list.
     *
     * @param request the request
     * @param response the response
     * @param year the year
     * @param series the series
     * @param eTag the e tag
     * @param lastModified the last modified
     * @return the OJ list
     */
    @RequestMapping(method = RequestMethod.GET, value = "/oj/series/{year}/{series}")
    public ResponseEntity<OfficialJournals> getOJList(final HttpServletRequest request, final HttpServletResponse response, //
            @PathVariable final int year, //
            @PathVariable final String series, //
            @RequestHeader(value = HttpHeaders.IF_NONE_MATCH, required = false) final String eTag, //
            @RequestHeader(value = HttpHeaders.IF_MODIFIED_SINCE, required = false) final String lastModified) {

        return this.getOJList(request, response, year, series, null, 0, eTag, lastModified);
    }

    /**
     * Gets the OJ list.
     *
     * @param request the request
     * @param response the response
     * @param year the year
     * @param series the series
     * @param subseries the subseries
     * @param eTag the e tag
     * @param lastModified the last modified
     * @return the OJ list
     */
    @RequestMapping(method = RequestMethod.GET, value = "/oj/series/{year}/{series}/{subseries}")
    public ResponseEntity<OfficialJournals> getOJList(final HttpServletRequest request, final HttpServletResponse response, //
            @PathVariable final int year, //
            @PathVariable final String series, //
            @PathVariable final String subseries, //
            @RequestHeader(value = HttpHeaders.IF_NONE_MATCH, required = false) final String eTag, //
            @RequestHeader(value = HttpHeaders.IF_MODIFIED_SINCE, required = false) final String lastModified) {

        return this.getOJList(request, response, year, series, subseries, 0, eTag, lastModified);
    }

    /**
     * Gets the response entity.
     *
     * @param officialJournals the official journals
     * @param eTag the e tag
     * @param lastModified the last modified
     * @return the response entity
     */
    private ResponseEntity<OfficialJournals> getResponseEntity(final OfficialJournals officialJournals, final String eTag,
            final String lastModified) {
        final ConditionalRequestVersions conditionalRequestVersions = conditionalRequestService.calculateVersionNow(ETagType.OJ_LIST, eTag,
                lastModified);
        final HttpStatus httpStatus = conditionalRequestVersions.isChanged() ? HttpStatus.OK : HttpStatus.NOT_MODIFIED;
        final String newETag = conditionalRequestVersions.getNewETag();
        final Date newLastModified = conditionalRequestVersions.getNewLastModified();
        final HttpHeadersBuilder httpHeadersBuilder = HttpHeadersBuilder.get().withDefaultNoticeHeaders(newETag, newLastModified,
                MediaType.APPLICATION_XML);
        org.springframework.http.HttpHeaders httpHeaders = httpHeadersBuilder.getHttpHeaders();
        ResponseEntity<OfficialJournals> result = new ResponseEntity<OfficialJournals>(officialJournals, httpHeaders, httpStatus);
        return result;
    }

}
