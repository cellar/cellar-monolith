/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.security
 *             FILE : UserController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 10, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.admin.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL;
import eu.europa.ec.opoce.cellar.cl.domain.admin.UserAccessRequest;
import eu.europa.ec.opoce.cellar.cl.service.client.UserAccessRequestService;

/**
 * <class_description> User Access Request manager controller.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 22, 2023
 *
 * @author EUROPEAN DYNAMICS S.A.
 */
@Controller
@RequestMapping(value = UserAccessRequestManagerController.PAGE_URL)
public class UserAccessRequestManagerController extends AbstractCellarController {

	/**
	 * 
	 */
    public static final String PAGE_URL = "/security/access-requests";
    /**
     * 
     */
    private static final String JSP_NAME = "security/access-requests";
    /**
     * 
     */
    private static final String OBJECT_NAME = "userAccessRequests";
    /**
     * User access request service.
     */
    private final UserAccessRequestService userAccessRequestService;
    
    @Autowired
    public UserAccessRequestManagerController(UserAccessRequestService userAccessRequestService) {
		this.userAccessRequestService = userAccessRequestService;
	}
   

    /**
     * Handle the GET.
     */
    @GetMapping
    public String displayUserAccessRequestManager(final ModelMap model) {
    	model.addAttribute(OBJECT_NAME, this.userAccessRequestService.findAllUserAccessRequests()); // gets all user access requests
        return JSP_NAME;
    }
    
    /**
     * Handle the "delete access-request" GET.
     */
    @GetMapping(value = "/delete")
    public String deleteUserAccessRequest(@RequestParam Long id) {
    	try {
    		UserAccessRequest uarToDelete = this.userAccessRequestService.findUserAccessRequest(id);
    		this.userAccessRequestService.deleteUserAccessRequest(uarToDelete);
    	}
    	catch (Exception e) {
    		addUserMessage(LEVEL.ERROR, "security.user-access-request.manager.action.delete.failure");
    	}
    	addUserMessage(LEVEL.SUCCESS, "security.user-access-request.manager.action.delete.success");
    	return redirect(PAGE_URL);
    }
}
