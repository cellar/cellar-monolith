package eu.europa.ec.opoce.cellar.cl.controller.resource.validator;

import eu.europa.ec.opoce.cellar.cl.controller.resource.validator.client.SimpleContentRequestValidator;
import eu.europa.ec.opoce.cellar.cl.controller.util.ControllerUtil;
import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;
import eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.server.dissemination.DisseminationRequestUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpServerErrorException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

@Component
public class AcceptMimeTypeValidator implements SimpleContentRequestValidator {

    private final PidManagerService pidManagerService;

    @Autowired
    public AcceptMimeTypeValidator(@Qualifier("pidManagerService") PidManagerService pidManagerService) {
        this.pidManagerService = pidManagerService;
    }

    @Override
    public HttpStatus validate(final HttpServletRequest request, final HttpServletResponse response,
            final CellarIdentifier resolveToCellarId, final String datastreamId, CellarResource resource) throws Exception {
        final List<String> acceptTypes = DisseminationRequestUtils.filterRdfContentStreamAcceptHeader(
        		DisseminationRequestUtils.toListOfStrings(
        				DisseminationRequestUtils.parseAcceptHeader(request)));
        
        if (acceptTypes.isEmpty() || acceptTypes.contains(MediaType.ALL.toString())) {
            return HttpStatus.OK;
        }

        final String uuid = StringUtils.substringBeforeLast(resolveToCellarId.getUuid(), "/");

        if (pidManagerService.isManifestation(uuid)) {

            for (final String mimetype : acceptTypes) {
                if (StringUtils.equalsIgnoreCase(mimetype, resource.getMimeType())) {
                    return HttpStatus.OK;
                }
            }

            final HttpStatus httpStatusError = HttpStatus.NOT_ACCEPTABLE;
            ControllerUtil.setLastModifiedDateResponse(response, null);
            ControllerUtil.setExpiredDateResponse(response, Calendar.getInstance(TimeZone.getTimeZone("GMT-0")).getTime());
            ControllerUtil.setResponseError(response,
                    new HttpServerErrorException(httpStatusError,
                            "Header mimetype(s)[" + StringUtils.join(acceptTypes, ", ")
                                    + "] \nis/are not compliant with the resource mimetype [" + resource.getMimeType()
                                    + "] corresponding to this identifier : " + uuid),
                    httpStatusError);
            return httpStatusError;
        }
        return HttpStatus.OK;
    }
}
