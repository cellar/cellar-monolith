/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create
 *             FILE : CreateExportJobController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 1, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create;

import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob.BATCH_JOB_TYPE;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor.ExportBatchJobProcessor;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.command.CreateExportJob;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.command.CreateJob;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.manager.ExportJobManagerController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;

/**
 * <class_description> Create export job controller.
 * <p>
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 1, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
@RequestMapping(CreateExportJobController.PAGE_URL)
public class CreateExportJobController extends CreateJobController {

    /**
     * The Constant PAGE_URL.
     */
    public static final String PAGE_URL = "/cmr/export/job/create";

    /**
     * The Constant JSP_NAME.
     */
    private static final String JSP_NAME = "cmr/export/job/create-job";

    /**
     * The Constant OBJECT_NAME.
     */
    private static final String OBJECT_NAME = "job";

    /**
     * The Constant BASE_DESTINATION_FOLDER.
     */
    private static final String BASE_DESTINATION_FOLDER = "baseDestinationFolder";

    /**
     * The Constant DEFAULT_FOLDER_NAME_PATTERN.
     */
    private static final String DEFAULT_FOLDER_NAME_PATTERN = "defaultFolderNamePattern";

    /**
     * Validator for a created job.
     */
    @Autowired
    @Qualifier("exportJobValidator")
    private Validator jobValidator;

    /**
     * Handle the GET.
     *
     * @param model the model
     * @return the string
     */
    @RequestMapping(method = RequestMethod.GET)
    public String createJobGet(final ModelMap model) {

        model.addAttribute(OBJECT_NAME, new CreateExportJob()); // empty job
        model.addAttribute(BASE_DESTINATION_FOLDER, this.cellarConfiguration.getCellarFolderExportBatchJob());
        model.addAttribute(DEFAULT_FOLDER_NAME_PATTERN, ExportBatchJobProcessor.DEFAULT_FOLDER_NAME_PATTERN);

        return JSP_NAME;
    }

    /**
     * Handle the POST.
     *
     * @param model         the model
     * @param createdJob    the created job
     * @param bindingResult the binding result
     * @param status        the status
     * @return the string
     */
    @RequestMapping(method = RequestMethod.POST)
    public String createJobPost(final ModelMap model, @ModelAttribute(OBJECT_NAME) final CreateExportJob createdJob,
                                final BindingResult bindingResult, final SessionStatus status) {

        return super.createJob(model, createdJob, bindingResult, status);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Validator getCreateJobValidator() {
        return this.jobValidator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BatchJob constructBatchJob(final CreateJob createJob) {
        final CreateExportJob job = (CreateExportJob) createJob;
        return BatchJob.create(BATCH_JOB_TYPE.EXPORT, job.getJobName(), job.getSparqlQuery())
                .metadataOnly(job.getMetadataOnly())
                .destinationFolder(job.getDestinationFolder())
                .useAgnosticURL(job.getUseAgnosticURL())
                .destinationFolder(job.getDestinationFolder())
                .build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getJspName() {
        return JSP_NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getSuccessPageUrl() {
        return ExportJobManagerController.PAGE_URL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getFunctionalityUrl() {
        return ExportJobManagerController.PAGE_URL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BATCH_JOB_TYPE getBatchJobType() {
        return BATCH_JOB_TYPE.EXPORT;
    }

}
