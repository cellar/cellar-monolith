/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.fixity
 *             FILE : FixityController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 06 29, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-06-29 11:03:41 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.admin.fixity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditBuilder;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventAction;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventProcess;
import eu.europa.ec.opoce.cellar.cl.domain.audit.Auditable;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UncheckedIOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static eu.europa.ec.opoce.cellar.CommonErrors.SERVICE_NOT_AVAILABLE;

/**
 * @author ARHS Developments
 */
@Controller(value = "fixityController")
public class FixityController extends AbstractCellarController {

    private static final Logger LOG = LogManager.getLogger(FixityController.class);
    private final CloseableHttpClient httpClient = HttpClientBuilder.create().build();
    private static final ObjectMapper MAPPER = new ObjectMapper();
    private static final String EMPTY_TABLE;

    static {
        try {
            ChecksumJsonObject checksum = new ChecksumJsonObject();
            checksum.setiTotalDisplayRecords(0);
            checksum.setiTotalPages(0);
            checksum.setAaData(Collections.emptyList());
            checksum.setiTotalRecords(0);
            EMPTY_TABLE = MAPPER.writeValueAsString(checksum);
        } catch (JsonProcessingException e) {
            LOG.debug("Cannot initialize an empty ChecksumJsonObject");
            throw new UncheckedIOException(e);
        }
    }

    private static final ImmutableMap<String, String> fixityResultMap =
            new ImmutableMap.Builder<String, String>()
                    .put("toCheckCount", "Number of items with TO_CHECK result")
                    .put("toRecheckCount", "Number of items with TO_RECHECK result")
                    .put("unavailableCount", "Number of items with UNAVAILABLE result")
                    // invalidCount must be replaced before validCount otherwise it gets replaced
                    .put("invalidCount", "Number of items with INVALID result")
                    .put("validCount", "Number of items with VALID result")
                    .put("errorCount", "Number of items with ERROR result")
                    .build();

    @RequestMapping(method = RequestMethod.GET, value = "/fixity")
    public ModelAndView list(final HttpServletRequest request, final HttpServletResponse response) {
        final boolean enabled = this.cellarConfiguration.getCellarServiceIntegrationFixityEnabled();
        ModelAndView result;

        if (enabled) {
            final String fixityBaseUrl = this.cellarConfiguration.getCellarServiceIntegrationFixityBaseUrl();
            final Map<String, Object> model = new HashMap<String, Object>();
            model.put("fixityBaseURL", fixityBaseUrl);

            try {
                addFixityStatusToModel(model);
                addFixityStatisticsToModel(model);
                result = new ModelAndView("fixity", model);
            } catch (final Exception e) {
                this.addUserMessage(ICellarUserMessage.LEVEL.ERROR, "error.fixity.not.accessible");

                // not possible to throw a WARN if we encapsulate the exception e as it will be forced to be logged as ERROR
                AuditBuilder.get(AuditTrailEventProcess.Administration)
                        .withAction(AuditTrailEventAction.Undefined)
                        .withCode(SERVICE_NOT_AVAILABLE)
                        .withMessage("Fixity is not accessible: {}")
                        .withMessageArgs(e)
                        .withLogger(LOG)
                        .withLogLevel(Auditable.LogLevel.WARN)
                        .logEvent();
                result = new ModelAndView("fixityDisabled");
            }
        } else {
            result = new ModelAndView("fixityDisabled");
        }
        return result;
    }

    @RequestMapping(value = "/fixity/checksum", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public String getChecksum(HttpServletRequest request) {
        String searchParameter = request.getParameter("sSearch");
        if (searchParameter.isEmpty()) {
            return EMPTY_TABLE;
        }

        int pageNumber = 0;
        if (request.getParameter("iDisplayStart") != null) {
            pageNumber = (Integer.valueOf(request.getParameter("iDisplayStart")) / 10);
            pageNumber = pageNumber > 1 ? (pageNumber - 1) : pageNumber;
        }


        int pageDisplayLength = Integer.valueOf(request.getParameter("iDisplayLength"));
        try {
            ChecksumJsonObject obj;
            if(searchParameter.equals("%RECHECK%")) {
                obj = findInvalidChecksums(pageNumber, pageDisplayLength);
            } else {
                obj = findChecksums(searchParameter, pageNumber, pageDisplayLength);
            }
            return MAPPER.writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @RequestMapping(value = "/fixity/checksum/recheck", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public void recheck(HttpServletRequest request, HttpServletResponse response) {
        try {
            recomputeChecksum();
        } catch (Exception e) {
            response.setStatus(500);
        }
    }

    private ChecksumJsonObject findChecksums(String searchParam, int page, int maxResults) throws Exception {
        Map<String, String> params = new HashMap<>();
        params.put("search", searchParam);
        params.put("page", Integer.toString(page));
        params.put("max", Integer.toString(maxResults));
        return findChecksum(params);
    }

    private ChecksumJsonObject findInvalidChecksums(int page, int maxResults) throws Exception {
        Map<String, String> params = new HashMap<>();
        params.put("search", "%");
        params.put("page", Integer.toString(page));
        params.put("max", Integer.toString(maxResults));
        params.put("filter", "invalid");
        return findChecksum(params);
    }

    private String recomputeChecksum() throws Exception {
        try {
            return getJsonResponse("/control/recheck", Collections.emptyMap());
        } catch (Exception e) {
            LOG.info("Cannot compute checksum in Fixity", e);
            throw e;
        }

    }

    private ChecksumJsonObject findChecksum(Map<String, String> params) throws Exception {
        try {
            String checksums = getJsonResponse("service/checksum", params);
            return mapChecksum(checksums);
        } catch (Exception e) {
            LOG.error("Error retrieving checksum from Fixity", e);
            throw e;
        }
    }

    private static ChecksumJsonObject mapChecksum(String checksums) throws IOException {
        JsonNode nodes = MAPPER.readTree(checksums);
        JsonNode n = nodes.get("checksums");
        ChecksumJsonObject json = new ChecksumJsonObject();
        List<Checksum> check = MAPPER.readValue(n.toString(), new TypeReference<List<Checksum>>() {
        });
        json.setAaData(check);
        json.setiTotalDisplayRecords(nodes.get("totalResults").asInt());
        json.setiTotalRecords(nodes.get("totalResults").asInt());
        json.setRecordsFiltered(json.getiTotalDisplayRecords());
        json.setiTotalPages(nodes.get("totalPages").asInt());
        return json;
    }

    public static class Checksum {

        private String cellarId = "";
        private String s3Checksum = "";
        private String computedChecksum = "";
        private String lastCheck = "";
        private String status = "";


        public String getCellarId() {
            return cellarId;
        }

        public void setCellarId(String cellarId) {
            this.cellarId = cellarId;
        }

        public String getS3Checksum() {
            return s3Checksum;
        }

        public void setS3Checksum(String s3Checksum) {
            this.s3Checksum = s3Checksum;
        }

        public String getComputedChecksum() {
            return computedChecksum;
        }

        public void setComputedChecksum(String computedChecksum) {
            this.computedChecksum = computedChecksum;
        }

        public String getLastCheck() {
            return lastCheck;
        }

        public void setLastCheck(String lastCheck) {
            this.lastCheck = lastCheck;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }

    static class ChecksumJsonObject {

        private int iTotalRecords;
        private int recordsFiltered;
        private int iTotalDisplayRecords;
        private int iTotalPages;
        private String sEcho;
        private String sColumns;
        private List<Checksum> aaData;

        ChecksumJsonObject() {
        }

        public long getiTotalRecords() {
            return iTotalRecords;
        }

        public void setiTotalRecords(int iTotalRecords) {
            this.iTotalRecords = iTotalRecords;
        }

        public int getRecordsFiltered() {
            return recordsFiltered;
        }

        public void setRecordsFiltered(int recordsFiltered) {
            this.recordsFiltered = recordsFiltered;
        }

        public int getiTotalDisplayRecords() {
            return iTotalDisplayRecords;
        }

        public void setiTotalDisplayRecords(int iTotalDisplayRecords) {
            this.iTotalDisplayRecords = iTotalDisplayRecords;
        }

        public int getiTotalPages() {
            return iTotalPages;
        }

        public void setiTotalPages(int iTotalPages) {
            this.iTotalPages = iTotalPages;
        }

        public String getsEcho() {
            return sEcho;
        }

        public void setsEcho(String sEcho) {
            this.sEcho = sEcho;
        }

        public String getsColumns() {
            return sColumns;
        }

        public void setsColumns(String sColumns) {
            this.sColumns = sColumns;
        }

        public List<Checksum> getAaData() {
            return aaData;
        }

        public void setAaData(List<Checksum> aaData) {
            this.aaData = aaData;
        }
    }

    private void addFixityStatisticsToModel(final Map<String, Object> model) throws Exception {
        String statistics = getJsonResponse("checksums/statistics", Collections.emptyMap());
        if (StringUtils.isNotBlank(statistics)) {
            for (Map.Entry<String, String> entry : fixityResultMap.entrySet()) {
                statistics = statistics.replace(entry.getKey(), entry.getValue());
            }
            model.put("FIXITY_STATISTICS", statistics);
        }
    }

    private void addFixityStatusToModel(final Map<String, Object> model) throws Exception {
        final String status = getJsonResponse("service/status", Collections.emptyMap());
        if (StringUtils.isNotBlank(status)) {
            final JsonParser jsonParser = new JsonParser();
            final JsonElement json = jsonParser.parse(status);
            final JsonObject asJsonObject = json.getAsJsonObject();

            final JsonObject threadPoolExecutorJson = asJsonObject.getAsJsonObject("threadPoolExecutor");
            final JsonObject fixityTaskSchedulerJson = asJsonObject.getAsJsonObject("fixityTaskScheduler");
            final JsonObject fixityFeedSchedulerJson = asJsonObject.getAsJsonObject("fixityFeedScheduler");
            final JsonObject threadPoolConfigurationJson = asJsonObject.getAsJsonObject("threadPoolConfiguration");

            putJsonInModel(threadPoolExecutorJson, fixityTaskSchedulerJson, fixityFeedSchedulerJson, threadPoolConfigurationJson, model);
        }
    }

    private void putJsonInModel(final JsonObject threadPoolExecutorJson,
                                final JsonObject fixityTaskSchedulerJson, final JsonObject fixityFeedSchedulerJson,
                                final JsonObject threadPoolConfigurationJson, final Map<String, Object> model) {

        final JsonObject json = new JsonObject();
        model.put("isPaused", isPaused(threadPoolExecutorJson));
        boolean feedSchedulerShutdown = isShutdown(fixityFeedSchedulerJson);
        boolean taskSchedulerShutdown = isShutdown(fixityTaskSchedulerJson);
        model.put("isShutdown", feedSchedulerShutdown && taskSchedulerShutdown);
        json.add("threadPoolExecutor", threadPoolExecutorJson);
        json.add("fixityTaskScheduler", fixityTaskSchedulerJson);
        json.add("fixityFeedScheduler", fixityFeedSchedulerJson);
        json.add("threadPoolConfiguration", threadPoolConfigurationJson);
        model.put("FIXITY_STATUS", json.toString());
    }

    private static boolean isPaused(final JsonObject jsonObject) {
        final String paused = jsonObject.get("paused").toString();
        return Boolean.parseBoolean(paused);
    }

    private static boolean isShutdown(final JsonObject jsonObject) {
        final String paused = jsonObject.get("shutdown").toString();
        return Boolean.parseBoolean(paused);
    }

    private String getJsonResponse(String endpoint, Map<String, String> queryParam) throws Exception {
    	
        final String disseminationURL = this.cellarConfiguration.getCellarServiceIntegrationFixityBaseUrl() + endpoint;
        final HttpGet httpGet = new HttpGet(disseminationURL);
    	httpGet.setHeader(HttpHeaders.ACCEPT_CHARSET, "UTF-8");

        if (!queryParam.isEmpty()) {
        	URIBuilder uriBuilder = new URIBuilder(httpGet.getURI());
        	queryParam.entrySet().stream().forEach(e->uriBuilder.addParameter(e.getKey(), e.getValue()));
        	httpGet.setURI(uriBuilder.build());
        }
        
        CloseableHttpResponse httpResponse = httpClient.execute(httpGet);
    	HttpEntity httpEntity = httpResponse.getEntity();

    	try(InputStream bodyStream = httpEntity.getContent();final StringWriter writer = new StringWriter()) {

    		if (httpResponse.getStatusLine().getStatusCode() == 200) { // "OK" HTTP Response Code
    			IOUtils.copy(bodyStream, writer, "utf-8");
    			return writer.toString();
    		}
    		else
    			throw new IllegalStateException("The HTTP request for " + disseminationURL + " returned the following status code " + httpResponse.getStatusLine().getStatusCode());
    	}
    	finally {
    		EntityUtils.consume(httpEntity);
    		httpResponse.close();
    	}
    }

    public long getNumberOfInvalidResult() {
        long invalidCount = -1;
        try {
            final String statistics = getJsonResponse("checksums/statistics", Collections.emptyMap());
            if (StringUtils.isNotBlank(statistics)) {
                final JsonParser jsonParser = new JsonParser();
                final JsonElement json = jsonParser.parse(statistics);
                final JsonObject asJsonObject = json.getAsJsonObject();
                final JsonPrimitive invalidResultValueFromJson = asJsonObject.getAsJsonPrimitive("invalidCount");
                invalidCount = invalidResultValueFromJson.getAsLong();
            }
        } catch (Exception e) {
            LOG.warn("Could not retrieve statistics from fixity: {}", e.toString());
        }
        return invalidCount;
    }
}
