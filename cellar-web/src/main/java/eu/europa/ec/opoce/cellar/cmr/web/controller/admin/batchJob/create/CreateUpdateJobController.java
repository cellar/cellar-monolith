/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create
 *             FILE : CreateUpdateJobController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 1, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create;

import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob.BATCH_JOB_TYPE;
import eu.europa.ec.opoce.cellar.cl.scheduling.SparqlUpdateProcessingScheduler;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.BatchJobProcessingService;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.command.CreateJob;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.command.CreateUpdateJob;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.manager.UpdateJobManagerController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;

import java.util.List;

/**
 * <class_description> Create update job controller.
 *
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 1, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
@RequestMapping(CreateUpdateJobController.PAGE_URL)
public class CreateUpdateJobController extends CreateJobController {

    public static final String PAGE_URL = "/cmr/update/job/sparql-update/create";

    private static final String JSP_NAME = "cmr/update/job/sparql-update/create-job";

    private static final String OBJECT_NAME = "job";

    /**
     * Validator for a created job.
     */
    @Autowired
    @Qualifier("updateJobValidator")
    private Validator jobValidator;

    @Autowired(required = true)
    private BatchJobProcessingService batchJobProcessingService;

    /**
     * Handle the GET.
     */
    @RequestMapping(method = RequestMethod.GET)
    public String createJobGet(final ModelMap model) {

        model.addAttribute(OBJECT_NAME, new CreateUpdateJob()); // empty job

        return JSP_NAME;
    }

    /**
     * Handle the POST.
     */
    @RequestMapping(method = RequestMethod.POST)
    public String createJobPost(final ModelMap model, @ModelAttribute(OBJECT_NAME) final CreateUpdateJob createdJob,
            final BindingResult bindingResult, final SessionStatus status) {

        final String createJob = super.createJob(model, createdJob, bindingResult, status);

        if (!bindingResult.hasErrors()) {
            final List<String> crons = this.batchJobScheduler.getCrons();
            final String cron = createdJob.getCron();
            if (!crons.contains(cron)) {
                final SparqlUpdateProcessingScheduler sparqlUpdateProcessingScheduler = new SparqlUpdateProcessingScheduler(
                        this.batchJobService, this.batchJobProcessingService, cron);
                new ConcurrentTaskScheduler().schedule(sparqlUpdateProcessingScheduler, new CronTrigger(cron));
            }
        }

        return createJob;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Validator getCreateJobValidator() {
        return this.jobValidator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BatchJob constructBatchJob(final CreateJob createJob) {
        final CreateUpdateJob job = (CreateUpdateJob) createJob;
        return BatchJob.create(BATCH_JOB_TYPE.UPDATE, job.getJobName(), job.getSparqlQuery())
                .sparqlUpdateQuery(job.getSparqlUpdateQuery())
                .jobStatus(BatchJob.STATUS.AUTO)
                .cron(job.getCron())
                .build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getJspName() {
        return JSP_NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getSuccessPageUrl() {
        return UpdateJobManagerController.PAGE_URL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getFunctionalityUrl() {
        return UpdateJobManagerController.PAGE_URL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BATCH_JOB_TYPE getBatchJobType() {
        return BATCH_JOB_TYPE.UPDATE;
    }

}
