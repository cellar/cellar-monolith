/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.security.command
 *        FILE : CreateUserCommand.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 15-03-2023
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2023 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.admin.security.command;

/**
 * Form-backing object for the Create User functionality.
 * @author EUROPEAN DYNAMICS S.A
 */
public class CreateUserCommand extends BaseUserCommand {
    
	public CreateUserCommand() {
		this.enabled = true;
	}
    
}
