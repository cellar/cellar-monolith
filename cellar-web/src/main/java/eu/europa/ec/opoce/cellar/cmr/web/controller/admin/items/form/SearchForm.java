package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.items.form;

import com.google.common.base.MoreObjects;

/**
 * @author ARHS Developments
 */
public class SearchForm {

    private String identifier;
    private boolean filterIdenticalSuggestion = true;
    private int page = 1;

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public boolean isFilterIdenticalSuggestion() {
        return filterIdenticalSuggestion;
    }

    public void setFilterIdenticalSuggestion(boolean filterIdenticalSuggestion) {
        this.filterIdenticalSuggestion = filterIdenticalSuggestion;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("identifier", identifier)
                .add("filterIdenticalSuggestion", filterIdenticalSuggestion)
                .add("page", page)
                .toString();
    }
}
