package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.mets;

import eu.europa.ec.opoce.cellar.ccr.mets.response.MetsResponse;
import eu.europa.ec.opoce.cellar.ccr.service.client.MetsUploadService;
import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage;
import eu.europa.ec.opoce.cellar.cl.controller.util.ControllerUtil;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.mets.form.MetsUploadForm;
import eu.europa.ec.opoce.cellar.ingestion.service.SIPWatcher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import static eu.europa.ec.opoce.cellar.ccr.utils.MetsUploadUtils.MetsUploadConstants.METS_UPLOAD_API_EXCEPTION_LOG_MESSAGE;
import static eu.europa.ec.opoce.cellar.ccr.utils.MetsUploadUtils.MetsUploadConstants.METS_UPLOAD_API_EXCEPTION_LOG_MESSAGE_WITH_REASON;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

/**
 * This Controller handles all HTTP requests for METS Upload operations.
 *
 * @author EUROPEAN DYNAMICS S.A.
 */
@Controller
public class MetsUploadController extends AbstractCellarController {

    private static final Logger log = LogManager.getLogger(MetsUploadController.class);

    public static final String PAGE_URL = "/mets";
    private static final String JSP_NAME = "mets";

    private final MetsUploadService metsUploadService;
    private final SIPWatcher sipWatcher;

    private final Validator metsFormValidator;
    public MetsUploadController(MetsUploadService metsUploadService, SIPWatcher sipWatcher, @Qualifier("metsUploadFormValidator")Validator metsFormValidator) {
        this.metsUploadService = metsUploadService;
        this.sipWatcher = sipWatcher;
        this.metsFormValidator=metsFormValidator;
    }

    @GetMapping(value = PAGE_URL)
    public ModelAndView home(final ModelMap model) {
        model.addAttribute(new MetsUploadForm());
        return new ModelAndView(JSP_NAME, model);
    }

    /**
     * Handles all user-to-machine (u2m) HTTP requests for uploading a METS file.
     *
     * @param metsUploadForm the data-form containing all necessary request parameters according to {@link MetsUploadForm}
     * @return the sipObject as byte stream
     */
    @PostMapping(value = PAGE_URL)
    public String metsUploadPage(@ModelAttribute("metsUploadForm") final MetsUploadForm metsUploadForm, BindingResult result, ModelMap model) {
        metsFormValidator.validate(metsUploadForm, result);
        if (result.hasErrors()) {
        	String validationErrorMessage = buildValidationErrorMessage(result);
            log.error(METS_UPLOAD_API_EXCEPTION_LOG_MESSAGE_WITH_REASON, validationErrorMessage);
            model.addAttribute("priority", metsUploadForm.getPriority());
            return JSP_NAME;
        }
        try {
            final MetsResponse metsResponse = metsUploadService.metsUpload(metsUploadForm.getFileData(),
                    metsUploadForm.getPriority(), metsUploadForm.getCallbackURI());
            invokeSipWatcherIfDorieInstance();
            super.addUserMessage(ICellarUserMessage.LEVEL.SUCCESS, "mets.upload.success",
                    "Success", new String[]{metsResponse.getReceived(), metsResponse.getStatus_endpoint()});
            return redirect(PAGE_URL);
        } catch (Exception e) {
            log.error(METS_UPLOAD_API_EXCEPTION_LOG_MESSAGE, e);
            super.addUserMessage(ICellarUserMessage.LEVEL.ERROR, "mets.upload.failure", "Package upload failed.", new String[]{e.getMessage()});
            return redirect(PAGE_URL);
        }
    }
    
    /**
     * Handles all machine-to-machine (m2m) HTTP requests for uploading a METS file.
     *
     * @param fileData    the file to upload
     * @param priorityStr the priority of the upload (defining the reception folder)
     * @param callbackURI the callback URI to send the callback request after completing the process (optional)
     * @return A {@link ResponseEntity} containing the {@link MetsResponse} of the uploaded file.
     */
    @PostMapping(value = "/v1/secapi/mets", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> metsUpload(@RequestParam(value = "fileData") final MultipartFile fileData,
                                        @RequestParam(required = false, value = "priority", defaultValue = "BULK") final String priorityStr,
                                        @RequestParam(required = false, value = "callback") final String callbackURI) {
        try {
            if (fileData == null || fileData.isEmpty()) {
                return ControllerUtil.makeJsonErrorResponse(new IllegalArgumentException("A file should be added in the fileData parameter."),
                        HttpStatus.BAD_REQUEST);
            }
            MetsResponse metsResponse = metsUploadService.metsUpload(fileData, priorityStr, callbackURI);
            invokeSipWatcherIfDorieInstance();
            return new ResponseEntity<>(metsResponse, HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            log.error(METS_UPLOAD_API_EXCEPTION_LOG_MESSAGE, e);
            return ControllerUtil.makeJsonErrorResponse(e, HttpStatus.BAD_REQUEST);
        } catch (Exception e) {
            log.error(METS_UPLOAD_API_EXCEPTION_LOG_MESSAGE, e);
            return ControllerUtil.makeJsonErrorResponse(e, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * If the instance is dedicated to DORIE ingestions, invoke SIPWatcher directly.
     */
    private void invokeSipWatcherIfDorieInstance() {
        if (this.cellarConfiguration.isCellarServiceDorieDedicatedEnabled()) {
            log.debug("Invoking SIPWatcher directly.");
            this.sipWatcher.watchFoldersAsync();
        }
    }
    
    /**
     * Concatenates all validation error messages into a single {@code String}
     * @param result the validation {@code BindingResult}
     * @return the {@code String} containing all error messages concatenated.
     */
    private String buildValidationErrorMessage(BindingResult result) {
    	StringBuilder sb = new StringBuilder();
    	if (!result.getAllErrors().isEmpty()) {
        	for (ObjectError oe : result.getAllErrors()) {
        		sb.append(oe);
        		sb.append(", ");
        	}
        	sb.delete(sb.length() - 2, sb.length());
    	}
    	return sb.toString();
    }
    
}
