/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.security.command
 *        FILE : EditUserCommand.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 15-03-2023
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2023 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.admin.security.command;

/**
 * Form-backing object for the Edit User functionality.
 * @author EUROPEAN DYNAMICS S.A
 */
public class EditUserCommand extends BaseUserCommand {

    /**
     * Internal id.
     */
    private Long id;
    /**
     * The password of the user.
     */
    private String password;
    /**
     * The password confirmation of the user.
     */
    private String confirmPassword;
    
	public EditUserCommand() {

	}

	public Long getId() {
		return id;
	}

	public String getPassword() {
		return password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}
	
}
