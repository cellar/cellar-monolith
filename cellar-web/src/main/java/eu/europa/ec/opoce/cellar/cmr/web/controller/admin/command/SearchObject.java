/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.visibility.command
 *             FILE : SearchObject.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 19, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.command;

/**
 * <class_description> Search command.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 19, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class SearchObject {

    /**
     * Searched object identifier.
     */
    private String objectId;

    /**
     * Default constructor.
     */
    public SearchObject() {

    }

    /**
     * Constructor.
     * @param objectId searched object identifier
     */
    public SearchObject(final String objectId) {
        this.objectId = objectId;
    }

    /**
     * @return the objectId
     */
    public String getObjectId() {
        return this.objectId;
    }

    /**
     * @param objectId the objectId to set
     */
    public void setObjectId(final String objectId) {
        this.objectId = objectId;
    }
}
