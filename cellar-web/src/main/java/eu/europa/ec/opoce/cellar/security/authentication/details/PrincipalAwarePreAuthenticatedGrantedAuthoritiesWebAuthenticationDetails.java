/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.security.authentication.details
 *        FILE : PrincipalAwarePreAuthenticatedGrantedAuthoritiesWebAuthenticationDetails.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 10-03-2023
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2023 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.security.authentication.details;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedGrantedAuthoritiesWebAuthenticationDetails;

import eu.cec.digit.ecas.client.j2ee.tomcat.EcasPrincipal;

/**
 * A <code>PreAuthenticatedGrantedAuthoritiesWebAuthenticationDetails</code> implementation
 * which stores the pre-authenticated EU-Login principal.
 * 
 * @author EUROPEAN DYNMICS S.A.
 */
public class PrincipalAwarePreAuthenticatedGrantedAuthoritiesWebAuthenticationDetails extends PreAuthenticatedGrantedAuthoritiesWebAuthenticationDetails {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */
	private EcasPrincipal principalDetails;
	
	public PrincipalAwarePreAuthenticatedGrantedAuthoritiesWebAuthenticationDetails(HttpServletRequest request, Collection<? extends GrantedAuthority> authorities) {
		super(request, authorities);
		this.principalDetails = extractPrincipal(request);
	}


	public EcasPrincipal getPrincipalDetails() {
		return principalDetails;
	}

	/**
	 * Obtains the <code>EcasPrincipal</code> from the provided <code>HttpServletRequest</code>.
	 * @param request the<code>HttpServletRequest</code>.
	 * @return the <code>EcasPrincipal</code>.
	 */
	private EcasPrincipal extractPrincipal(HttpServletRequest request) {
		Object principal = request.getUserPrincipal();
		if (principal == null) {
			throw new PrincipalNotFoundException("Principal is null.");
		}
		if (!(principal instanceof EcasPrincipal)) {
			throw new InvalidPrincipalException("Principal is not an [EcasPrincipal].");
		}
		return (EcasPrincipal) principal;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof PrincipalAwarePreAuthenticatedGrantedAuthoritiesWebAuthenticationDetails) {
			PrincipalAwarePreAuthenticatedGrantedAuthoritiesWebAuthenticationDetails other = (PrincipalAwarePreAuthenticatedGrantedAuthoritiesWebAuthenticationDetails) obj;
			if (super.equals(obj)) {
				if (this.principalDetails == null && other.getPrincipalDetails() != null) {
					return false;
				}
				if (this.principalDetails != null && other.getPrincipalDetails() == null) {
					return false;
				}
				if (this.principalDetails != null && other.getPrincipalDetails() != null) {
					return this.principalDetails.getName().equals(other.getPrincipalDetails().getName());
				}
				return true;
			}
			return false;
		}
		return false;
	}

	@Override
	public int hashCode() {
		int hash = super.hashCode();
		if (this.principalDetails != null) {
			hash = hash * (this.principalDetails.getName().hashCode() % 7);
		}
		return hash;
	}
	
	
	public static class InvalidPrincipalException extends AuthenticationException {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		private InvalidPrincipalException(String message) {
			super(message);
		}
		
	}
	
	public static class PrincipalNotFoundException extends AuthenticationException {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		private PrincipalNotFoundException(String message) {
			super(message);
		}
		
	}

}
