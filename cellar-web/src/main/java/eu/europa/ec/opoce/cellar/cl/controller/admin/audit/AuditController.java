package eu.europa.ec.opoce.cellar.cl.controller.admin.audit;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.util.ControllerUtil;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEvent;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventProcess;
import eu.europa.ec.opoce.cellar.cl.service.client.AuditTrailEventService;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * MVC controller for downloading log/audit entries.
 * 
 * @author omeurice
 * 
 */
@Controller(value = "auditController")
public class AuditController extends AbstractCellarController {

    private static final String DATE_FORMAT = "dd/MM/yyyy";

    private static final String DATETIME_FORMAT = "dd/MM/yyyy HH:mm:ss";

    @Autowired
    private AuditTrailEventService auditTrailEventService;

    /**
     * Class logger.
     */
    private transient final static Logger LOG = LogManager.getLogger(AuditController.class);

    @RequestMapping(method = RequestMethod.GET, value = "/audit")
    public ModelAndView loadPage(final HttpServletRequest request, final HttpServletResponse response) {
        return new ModelAndView("audit", initModel());
    }

    /**
     * Method for displaying the initial view.
     * 
     * @param request
     *            HTTP servlet request.
     * @param response
     *            HTTP servlet response
     * @return ModelAndView instance.
     */
    @RequestMapping(method = {
            RequestMethod.GET, RequestMethod.POST}, value = "/audit/load")
    public ModelAndView display(final HttpServletRequest request, final HttpServletResponse response) {
        final int ROWS_PER_PAGE = 50;
        final Map<String, Object> model = initModel();
        String logProcess = request.getParameter("logProcess");
        if (StringUtils.isBlank(logProcess)) {
            logProcess = "ALL";
        }
        final String objectId = request.getParameter("objectId");

        final Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(0L);

        final AuditTrailEventProcess eventProcess = "ALL".equals(logProcess) ? null : AuditTrailEventProcess.valueOf(logProcess);

        final long totalRows = this.auditTrailEventService.countLogsBetweenDates(calendar.getTime(), new Date(), eventProcess, objectId);
        long pages = totalRows / ROWS_PER_PAGE;
        if ((totalRows % ROWS_PER_PAGE) > 0) {
            pages++;
        }

        final String pageNumStr = request.getParameter("pageNum");
        int pageNum = 1;
        if (!StringUtils.isEmpty(pageNumStr)) {
            pageNum = Integer.parseInt(pageNumStr);
        }
        if (pageNum > pages) {
            pageNum = (int) pages;
        } else if (pageNum < 0) {
            pageNum = 1;
        }
        final int firstRowDisplayed = ((pageNum - 1) * ROWS_PER_PAGE) + 1;
        final int lastRowDisplayed = (firstRowDisplayed + ROWS_PER_PAGE) - 1;

        List<AuditTrailEvent> events = new ArrayList<AuditTrailEvent>();

        if (StringUtils.isNotBlank(logProcess) && !"ALL".equals(logProcess)) {
            if (StringUtils.isNotBlank(objectId)) {
                if (objectId.contains("*")) {
                    events = this.auditTrailEventService.getLogsByProcessAndIdentifierWildcard(AuditTrailEventProcess.valueOf(logProcess),
                            objectId);
                } else {
                    events = this.auditTrailEventService.getLogsByProcessAndIdentifier(AuditTrailEventProcess.valueOf(logProcess),
                            objectId);
                }
            } else {
                events = this.auditTrailEventService.getLogsByProcess(AuditTrailEventProcess.valueOf(logProcess));
            }
        } else if ("ALL".equals(logProcess)) {
            if (StringUtils.isNotBlank(objectId)) {
                if (objectId.contains("*")) {
                    events = this.auditTrailEventService.getLogsByIdentifierWildcard(objectId);
                } else {
                    events = this.auditTrailEventService.getLogsByIdentifier(objectId);
                }
            } else {
                events = this.auditTrailEventService.getLogs(calendar.getTime(), new Date(), firstRowDisplayed, lastRowDisplayed);
            }
        }
        model.put("selectedLogProcess", logProcess);
        model.put("selectedObjectId", objectId);
        model.put("events", events);
        model.put("lastRowNumber", totalRows);
        model.put("firstRowDisplayed", firstRowDisplayed);
        model.put("lastRowDisplayed", lastRowDisplayed > totalRows ? totalRows : lastRowDisplayed);
        model.put("pageNum", pageNum);
        model.put("previousPage", pageNum > 0 ? pageNum - 1 : pageNum);
        model.put("nextPage", pageNum >= pages ? pages : pageNum + 1);
        model.put("rowsPerPage", ROWS_PER_PAGE);
        return new ModelAndView("audit", model);
    }

    /**
     * Method for downloading the log entries to a file.
     * 
     * @param request
     *            HTTP servlet request.
     * @param response
     *            HTTP servlet response.
     */
    @RequestMapping(method = RequestMethod.POST, value = "/audit/download")
    public void downloadLog(final HttpServletRequest request, final HttpServletResponse response) {
        final String dateFrom = request.getParameter("dateFrom");
        final String dateTo = request.getParameter("dateTo");
        final SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        response.setCharacterEncoding("UTF-8");

        try {
            final Date fromDate = sdf.parse(dateFrom);
            final Date toDate = sdf.parse(dateTo);
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment; filename=cellarlog.csv");
            final OutputStream out = response.getOutputStream();
            writeFile(out, fromDate, toDate);
            out.close();
        } catch (final IOException e) {
            try {
                response.sendRedirect(request.getContextPath() + "/admin/audit");
            } catch (final IOException e1) {
                LOG.error(e.getMessage());
            }
        } catch (final ParseException e) {
            try {
                response.sendRedirect(request.getContextPath() + "/admin/audit");
            } catch (final IOException e1) {
                LOG.error(e.getMessage());
            }
        }

    }

    @RequestMapping(method = RequestMethod.POST, value = "/audit/delete")
    public void delete(final HttpServletRequest request, final HttpServletResponse response) {
        final SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        final String endDate = request.getParameter("endDate");
        try {
            final Date endDateValue = sdf.parse(endDate);
            this.auditTrailEventService.deleteEvent(endDateValue);
            response.sendRedirect(request.getContextPath() + "/admin/audit");
        } catch (final IOException e) {
            LOG.error(e.getMessage());
        } catch (final ParseException e) {
            LOG.error(e.getMessage());
            try {
                response.sendRedirect(request.getContextPath() + "/admin/audit");
            } catch (final IOException e1) {
                LOG.error(e1.getMessage());
            }
        }
    }

    /**
     * Handler for Retrieving the logs for a specific object identifier.
     *
     * @param objectIdentifier    the object identifier.
     * @return ResponseEntity instance.
     */
    @RequestMapping(method = RequestMethod.GET, value = "/getlogentry/{objectIdentifier:.+}")
    public ResponseEntity<String> getLogEntry(@PathVariable final String objectIdentifier) {
        final StringBuffer result = this.auditTrailEventService.getXMLLogsFromIdentifier(objectIdentifier);
        return ControllerUtil.makeXMLResponse(result.toString());
    }

    private Map<String, Object> initModel() {
        final Map<String, Object> model = new HashMap<String, Object>();
        final List<String> logProcessNames = new ArrayList<String>();
        logProcessNames.add("ALL");
        logProcessNames.addAll(AuditTrailEventProcess.getValueNames());
        logProcessNames.remove(AuditTrailEventProcess.Undefined.toString());
        model.put("logProcesses", logProcessNames);
        return model;
    }

    private void writeFile(final OutputStream out, final Date dfrom, final Date dto) throws IOException {
        final List<AuditTrailEvent> events = this.auditTrailEventService.getLogs(dfrom, dto);
        final SimpleDateFormat sdf = new SimpleDateFormat(DATETIME_FORMAT);
        for (final AuditTrailEvent event : events) {
            out.write(sdf.format(event.getDate()).getBytes(StandardCharsets.UTF_8));
            out.write(';');
            out.write(event.getObjectIdentifier() != null ? event.getObjectIdentifier().getBytes(StandardCharsets.UTF_8) : " ".getBytes(StandardCharsets.UTF_8));
            out.write(';');
            out.write(event.getMessage().replaceAll("\\r\\n", " ").getBytes(StandardCharsets.UTF_8));
            out.write(';');
            out.write("\r\n".getBytes(StandardCharsets.UTF_8));
        }
    }
}
