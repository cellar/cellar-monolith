/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.aligner
 *             FILE : AlignerController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : May 21, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.aligner;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import eu.europa.ec.opoce.cellar.cl.configuration.enums.CellarServiceRDFStoreCleanerMode;
import eu.europa.ec.opoce.cellar.cl.configuration.impl.CellarPersistentConfiguration;
import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL;
import eu.europa.ec.opoce.cellar.cl.dao.aligner.AlignerMisalignmentDao;
import eu.europa.ec.opoce.cellar.cl.domain.aligner.AlignerMisalignment;
import eu.europa.ec.opoce.cellar.cl.domain.aligner.Hierarchy;
import eu.europa.ec.opoce.cellar.cl.domain.datatable.DataTableBodyResponse;
import eu.europa.ec.opoce.cellar.cl.enums.aligner.Repository;
import eu.europa.ec.opoce.cellar.cl.service.aligner.IHierarchyAlignerService;
import eu.europa.ec.opoce.cellar.cl.service.aligner.IHierarchyLoaderService;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.command.SearchObject;
import eu.europa.ec.opoce.cellar.support.datatable.MisalignmentsDatatableMapper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Map;

/**
 * The Class HierarchyAlignerController.
 */
@Controller
public class HierarchyAlignerController extends AbstractCellarController {

    private static final Logger LOG = LoggerFactory.getLogger(HierarchyAlignerController.class);

    /** The Constant MISALIGNMENTS_DATATABLE_MAPPER. */
    private static final MisalignmentsDatatableMapper MISALIGNMENTS_DATATABLE_MAPPER = new MisalignmentsDatatableMapper();

    /** The hierarchy loader service. */
    @Autowired
    private IHierarchyLoaderService hierarchyLoaderService;

    /** The cellar persistent configuration. */
    @Autowired
    @Qualifier("cellarConfiguration")
    private CellarPersistentConfiguration cellarPersistentConfiguration;

    /** The aligner misalignment dao. */
    @Autowired
    private AlignerMisalignmentDao alignerMisalignmentDao;

    /** The cellar resource dao. */
    @Autowired
    private CellarResourceDao cellarResourceDao;

    /** The hierarchy aligner service. */
    @Autowired
    private IHierarchyAlignerService hierarchyAlignerService;

    /**
     * Hierarchy aligner fix get.
     *
     * @param request the request
     * @param response the response
     * @param objectId the object id
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @RequestMapping(method = RequestMethod.GET, value = "/aligner/align/fix")
    public void hierarchyAlignerFixGet(final HttpServletRequest request, final HttpServletResponse response,
            @RequestParam(value = "objectId", required = false) final String objectId) throws IOException {
        response.setContentType(CONTENT_TYPE_TEXT);
        if (StringUtils.isNotBlank(objectId)) {
            try {
                this.hierarchyAlignerService.alignHierarchy(objectId);
            } catch (final Exception exception) {
                if (StringUtils.isNotBlank(exception.getMessage())) {
                    response.getOutputStream().write(exception.getMessage().getBytes(StandardCharsets.UTF_8));
                    LOG.error("An exception occurred during hierarchyAlignerFixGet", exception);
                }
            }
        }
        response.getOutputStream().flush(); // return the error or ""
    }

    /**
     * Handle the GET.
     */
    @RequestMapping(value = "/aligner", method = RequestMethod.GET)
    public String indexingGet() {
        return redirect("/aligner/align");
    }

    /**
     * Show aligner search tab.
     *
     * @param model the model
     * @param objectId the object id
     * @return the string
     */
    @RequestMapping(value = "/aligner/align", method = RequestMethod.GET)
    public String showAlignerSearchTab(final ModelMap model, @RequestParam(required = false) final String objectId) {
        return this.showAlignerSearchForCellarId(model, objectId == null ? "" : objectId);
    }

    /**
     * Show aligner search for cellar id.
     *
     * @param model the model
     * @param cellarId the cellar id
     * @return the string
     */
    @RequestMapping(value = "/aligner/align/{cellarId}", method = RequestMethod.GET)
    public String showAlignerSearchForCellarId(final ModelMap model, @PathVariable(value = "cellarId") final String cellarId) {
        final SearchObject searchObject = new SearchObject(cellarId);
        model.addAttribute("search", searchObject);
        if (StringUtils.isNotBlank(cellarId)) {
            this.getHierarchy(model, cellarId);
        }
        return "aligner/search";
    }

    /**
     * Gets the hierarchy.
     *
     * @param model the model
     * @param cellarId the cellar id
     * @return the hierarchy
     */
    private void getHierarchy(final ModelMap model, final String cellarId) {
        try {
            final Hierarchy hierarchy = this.hierarchyLoaderService.getHierarchy(cellarId);
            model.addAttribute("hierarchy", hierarchy);
            if (hierarchy.getNodes().isEmpty()) { // tree doesn't exist
                model.remove("hierarchy");
                super.addUserMessage(LEVEL.ERROR, null, "Cellar identifier '" + cellarId + "' not found.");
            }
        } catch (final Exception exception) {
            super.addUserMessage(LEVEL.ERROR, null, exception.getLocalizedMessage());
        }
    }

    /**
     * Check if the hierarchy retrieved based on the cellar ID is
     * aligned.
     *
     * @return HttpCode 200 if the hierarchy is aligned, or 400 if
     * not.
     */
    @RequestMapping(value = "/aligner/align/{cellarId}/check", method = RequestMethod.GET)
    public ResponseEntity<Void> isAligned(@PathVariable(value = "cellarId") final String cellarId) {
        try {
            Hierarchy hierarchy = this.hierarchyLoaderService.getHierarchy(cellarId);
            if (hierarchy.isAligned()) {
                return ResponseEntity.ok().build();
            }
        } catch (Exception e) {
            LOG.error("Impossible to retrieve the hierarchy for : " + cellarId, e);
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    /**
     * Show aligner dashboard tab.
     *
     * @param model the model
     * @return the string
     */
    @RequestMapping(value = "/aligner/dashboard", method = RequestMethod.GET)
    public String showAlignerDashboardTab(final ModelMap model) {

        model.addAttribute("alignerStatus", this.hierarchyAlignerService.getManagerStatus());
        final boolean cellarServiceAlignerExecutorEnabled = this.cellarPersistentConfiguration.isCellarServiceAlignerExecutorEnabled();
        model.addAttribute("alignerEnabled", cellarServiceAlignerExecutorEnabled);

        return "aligner/dashboard";
    }

    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/aligner/ajax/misalignments", method = RequestMethod.GET)
    @ResponseBody
    public String ajaxCallToAlignerMisalignmentsTab(final ModelMap model, final HttpServletRequest httpServletRequest) {
        final Map<String, String[]> parametersMap = httpServletRequest.getParameterMap();
        final Gson gson = new GsonBuilder().create();
        final DataTableBodyResponse<String[]> dataTableObject = MISALIGNMENTS_DATATABLE_MAPPER.getDataTableBodyResponse(parametersMap);
        final String json = gson.toJson(dataTableObject);
        final String result = json;
        return result;
    }

    /**
     * Show aligner misalignments tab.
     *
     * @param model the model
     * @return the string
     */
    @RequestMapping(value = "/aligner/misalignments", method = RequestMethod.GET)
    public String showAlignerMisalignmentsTab(final ModelMap model) {
        model.addAttribute("alignerMisalignments", new ArrayList<AlignerMisalignment>());
        model.addAttribute("alignerRepositories", Repository.values());
        model.addAttribute("operationTypes", CellarServiceRDFStoreCleanerMode.values());
        return "aligner/misalignments";
    }

    /**
     * Enable aligner executor.
     *
     * @param model the model
     * @return the string
     */
    @RequestMapping(value = "/aligner/dashboard/startExecutor", method = RequestMethod.GET)
    public String startAlignerExecutor(final ModelMap model) {
        final boolean cellarServiceAlignerExecutorEnabled = this.cellarPersistentConfiguration.isCellarServiceAlignerExecutorEnabled();
        if (cellarServiceAlignerExecutorEnabled) {
            this.hierarchyAlignerService.startAligning();
            super.addUserMessage(LEVEL.SUCCESS, "aligner.dashboard.success.action");
            model.addAttribute("alignerStatus", this.hierarchyAlignerService.getManagerStatus());
        } else {
            super.addUserMessage(LEVEL.ERROR, "aligner.dashboard.disabled.action");
        }
        model.addAttribute("alignerEnabled", cellarServiceAlignerExecutorEnabled);

        return redirect("/aligner/dashboard");
    }

    /**
     * Disable aligner executor.
     *
     * @param model the model
     * @return the string
     */
    @RequestMapping(value = "/aligner/dashboard/stopExecutor", method = RequestMethod.GET)
    public String stopAlignerExecutor(final ModelMap model) {
        final boolean cellarServiceAlignerExecutorEnabled = this.cellarPersistentConfiguration.isCellarServiceAlignerExecutorEnabled();
        if (cellarServiceAlignerExecutorEnabled) {
            this.hierarchyAlignerService.stopAligning();
            model.addAttribute("alignerStatus", this.hierarchyAlignerService.getManagerStatus());
            super.addUserMessage(LEVEL.SUCCESS, "aligner.dashboard.success.stop.action");
            model.addAttribute("alignerEnabled", cellarServiceAlignerExecutorEnabled);
        } else {
            super.addUserMessage(LEVEL.ERROR, "aligner.dashboard.disabled.action");
        }

        return redirect("/aligner/dashboard");
    }

    /**
     * Gets the number of resources.
     *
     * @param model the model
     * @return the number of resources
     */
    @ResponseBody
    @RequestMapping(value = "/aligner/dashboard/numberOfResources", method = RequestMethod.GET)
    public Long getNumberOfResources(final ModelMap model) {
        return this.cellarResourceDao.getNumberOfMetadataResources();
    }

    /**
     * Gets the number of resources diagnosable.
     *
     * @param model the model
     * @return the number of resources diagnosable
     */
    @ResponseBody
    @RequestMapping(value = "/aligner/dashboard/numberOfResourcesDiagnosable", method = RequestMethod.GET)
    public Long getNumberOfResourcesDiagnosable(final ModelMap model) {
        return this.cellarResourceDao.getAlignerResourcesDiagnosableCount();
    }

    /**
     * Gets the number of resources fixable.
     *
     * @param model the model
     * @return the number of resources fixable
     */
    @ResponseBody
    @RequestMapping(value = "/aligner/dashboard/numberOfResourcesFixable", method = RequestMethod.GET)
    public Long getNumberOfResourcesFixable(final ModelMap model) {
        return this.cellarResourceDao.getAlignerResourcesInErrorCount();
    }

    /**
     * Gets the number of resources in table.
     *
     * @param model the model
     * @return the number of resources in table
     */
    @ResponseBody
    @RequestMapping(value = "/aligner/dashboard/numberOfResourcesInTable", method = RequestMethod.GET)
    public Long getNumberOfResourcesInTable(final ModelMap model) {
        return this.alignerMisalignmentDao.count();
    }

}
