/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob
 *             FILE : BatchJobManagerController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 12, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.manager;

import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.common.BatchJobController;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * <class_description> Batch jobs manager.
 *
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 12, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
public abstract class BatchJobManagerController extends BatchJobController {

    /** The Constant DELETE_URL. */
    private static final String DELETE_URL = "/delete";

    /** The Constant LAUNCH_URL. */
    private static final String LAUNCH_URL = "/launch";

    /** The Constant RESTART_URL. */
    private static final String RESTART_URL = "/restart";

    /** The Constant STOP_SCHEDULERS_URL. */
    private static final String STOP_SCHEDULERS_URL = "/stopSchedulers";

    /** The Constant RESTART_SCHEDULERS_URL. */
    private static final String RESTART_SCHEDULERS_URL = "/restartSchedulers";

    /**
     * Gets the page url.
     * @return the page url
     */
    protected abstract String getPageUrl();

    /**
     * Gets the batch jobs of the appropriate type.
     * @return the batch jobs of the appropriate type
     */
    protected abstract List<BatchJob> getBatchJobs();

    /**
     * Handle the GET.
     *
     * @param model the model
     * @return the string
     */
    @RequestMapping(method = RequestMethod.GET)
    public String reindexJobManagerGet(final ModelMap model) {
        final boolean areSchedulersRunning = batchJobScheduler.areSchedulersRunning();
        model.addAttribute("areSchedulersRunning", areSchedulersRunning);
        model.addAttribute("batchJobs", this.getBatchJobs()); // adds the batch jobs
        return this.getJspName();
    }

    /**
     * Stop schedulers.
     *
     * @return the string
     */
    @RequestMapping(value = BatchJobManagerController.STOP_SCHEDULERS_URL, method = RequestMethod.GET)
    public String stopSchedulers() {
        batchJobScheduler.stopSchedulers();
        return redirect(this.getPageUrl());
    }

    /**
     * Restart schedulers.
     *
     * @return the string
     */
    @RequestMapping(value = BatchJobManagerController.RESTART_SCHEDULERS_URL, method = RequestMethod.GET)
    public String restartSchedulers() {
        batchJobScheduler.restartSchedulers();
        return redirect(this.getPageUrl());
    }

    /**
     * Handle the delete.
     *
     * @param id the id
     * @return the string
     */
    @RequestMapping(value = BatchJobManagerController.DELETE_URL, method = RequestMethod.GET)
    public String batchJobDelete(final Long id) {
        this.batchJobService.deleteBatchJob(id); // deletes job
        super.addUserMessage(LEVEL.SUCCESS, "common.message.successfullyProcessed");
        return redirect(this.getPageUrl());
    }

    /**
     * Handle the launch.
     *
     * @param id the id
     * @return the string
     */
    @RequestMapping(value = BatchJobManagerController.LAUNCH_URL, method = RequestMethod.GET)
    public String batchJobLaunch(final Long id) {
        this.batchJobService.startBatchJob(id); // starts job
        super.addUserMessage(LEVEL.SUCCESS, "common.message.successfullyProcessed");
        return redirect(this.getPageUrl());
    }

    /**
     * Handle the restart.
     *
     * @param id the id
     * @return the string
     */
    @RequestMapping(value = BatchJobManagerController.RESTART_URL, method = RequestMethod.GET)
    public String batchJobRestart(final Long id) {
        this.batchJobService.restartBatchJob(id); // restarts job
        super.addUserMessage(LEVEL.SUCCESS, "common.message.successfullyProcessed");
        return redirect(this.getPageUrl());
    }
}
