/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.archive
 *             FILE : ArchiveController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 4 juil. 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.admin.archive;

import com.google.gson.*;
import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditBuilder;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventAction;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventProcess;
import eu.europa.ec.opoce.cellar.cl.domain.audit.Auditable;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static eu.europa.ec.opoce.cellar.CommonErrors.SERVICE_NOT_AVAILABLE;

/**
 * The Class ArchiveController.
 * <class_description> The controller for the Archive view.
 * <br/><br/>
 * <notes> .
 * <br/><br/>
 * ON : 4 juil. 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller(value = "archiveController")
public class ArchiveController extends AbstractCellarController {

    private static final Logger LOG = LogManager.getLogger(ArchiveController.class);
    
    private final CloseableHttpClient httpClient = HttpClientBuilder.create().build();
    
    /**
     * List.
     *
     * @param request  the request
     * @param response the response
     * @return the model and view
     */
    @RequestMapping(method = RequestMethod.GET, value = "/archive")
    public ModelAndView list(final HttpServletRequest request, final HttpServletResponse response) {

        if (this.cellarConfiguration.getCellarServiceIntegrationArchivistEnabled()) {
            final String cellarServiceIntegrationCaBaseUrl = this.cellarConfiguration.getCellarServiceIntegrationArchivistBaseUrl();
            final Map<String, Object> model = new HashMap<>();
            model.put("archivistBaseURL", cellarServiceIntegrationCaBaseUrl);

            try {
                final String archivistStatusUrl = this.cellarConfiguration.getCellarServiceIntegrationArchivistBaseUrl() + "actuator/health";
                final String jsonResponse = getJsonResponseNew(archivistStatusUrl);
                model.put("archivistStatus", new Gson().fromJson(jsonResponse, ArchivistStatus.class));

                return new ModelAndView("archive", model);
            } catch (final Exception e) {
                LOG.error("An error occurred when trying to retrieve the Archivist's information", e);

                this.addUserMessage(LEVEL.ERROR, "error.archivist.not.accessible");

                // not possible to throw a WARN if we encapsulate the exception e as it will be forced to be logged as ERROR
                AuditBuilder.get(AuditTrailEventProcess.Administration)
                        .withAction(AuditTrailEventAction.Archive)
                        .withCode(SERVICE_NOT_AVAILABLE)
                        .withMessage("Archivist is not accessible: {}")
                        .withMessageArgs(e)
                        .withLogger(LOG)
                        .withLogLevel(Auditable.LogLevel.WARN)
                        .logEvent();
                return new ModelAndView("archivistDisabled");
            }
        }
        return new ModelAndView("archivistDisabled");
    }
    
    private String getJsonResponseNew(final String archivistStatusUrl) throws Exception {

    	final HttpGet httpGet = new HttpGet(archivistStatusUrl);
    	httpGet.setHeader(HttpHeaders.ACCEPT_CHARSET, "UTF-8");
    	CloseableHttpResponse httpResponse = httpClient.execute(httpGet);
    	HttpEntity httpEntity = httpResponse.getEntity();

    	
    	try(InputStream bodyStream = httpEntity.getContent();final StringWriter writer = new StringWriter()) {

    		if (httpResponse.getStatusLine().getStatusCode() == 200) { // "OK" HTTP Response Code
    			IOUtils.copy(bodyStream, writer, "utf-8");
    			return writer.toString();
    		}
    		else
    			throw new IllegalStateException("The HTTP request for " + archivistStatusUrl + " returned the following status code " + httpResponse.getStatusLine().getStatusCode());
    	}
    	finally {
    		EntityUtils.consume(httpEntity);
    		httpResponse.close();
    	}
 
    }
    
    /**
     * Adds the archivist status to model.
     *
     * @param model the model
     * @throws Exception the exception
     */
    private void addArchivistStatusToModel(final Map<String, Object> model) throws Exception {
        final String status = getJsonResponse("service/status");
        if (StringUtils.isNotBlank(status)) {
            final JsonParser jsonParser = new JsonParser();
            final JsonElement json = jsonParser.parse(status);
            final JsonObject asJsonObject = json.getAsJsonObject();

            final JsonObject threadPoolExecutorsJson = asJsonObject.getAsJsonObject("threadPoolExecutors");
            final JsonObject threadPoolTaskSchedulersJson = asJsonObject.getAsJsonObject("threadPoolTaskSchedulers");

            putJsonInModel(threadPoolExecutorsJson, threadPoolTaskSchedulersJson, TYPE.DAILY.toString(), model, "isDailyPaused",
                    "isDailyShutdown", "DAILY_STATUS");
            putJsonInModel(threadPoolExecutorsJson, threadPoolTaskSchedulersJson, TYPE.BULK.toString(), model, "isBulkPaused",
                    "isBulkShutdown", "BULK_STATUS");
            putJsonInModel(threadPoolExecutorsJson, threadPoolTaskSchedulersJson, TYPE.AUTHENTICOJ.toString(), model, "isAuthenticPaused",
                    "isAuthenticShutdown", "AUTHENTICOJ_STATUS");
            putJsonInModel(threadPoolExecutorsJson, threadPoolTaskSchedulersJson, "EMBARGO", model, "isEmbargoPaused",
                    "isEmbargoShutdown", "EMBARGO_STATUS");

        }
    }

    private void addArchivistPropsToModel(final Map<String, Object> model) throws Exception {
        final String propsJsonBody = getJsonResponse("service/getProperties");
        if (StringUtils.isNotBlank(propsJsonBody)) {
            final JsonParser jsonParser = new JsonParser();
            final JsonElement json = jsonParser.parse(propsJsonBody);
            final JsonArray jsonArray = json.getAsJsonArray();

            final Collection<Map<String, Object>> properties = new ArrayList<Map<String, Object>>();
            jsonArray.forEach(currJsonElem -> {
                final JsonObject currJsonObject = currJsonElem.getAsJsonObject();
                final Map<String, Object> currProperty = new HashMap<String, Object>();
                currProperty.put("id", currJsonObject.get("id").getAsLong());
                currProperty.put("propertyKey", currJsonObject.get("propertyKey").getAsString());
                currProperty.put("propertyValue", currJsonObject.get("propertyValue").getAsString());
                properties.add(currProperty);
            });

            model.put("archivistProperties", properties);
        }
    }

    /**
     * Put json in model.
     *
     * @param threadPoolExecutorsJson the thread pool executors json
     * @param threadPoolTaskSchedulersJson the thread pool task schedulers json
     * @param priority the priority
     * @param model the model
     * @param pausedString the paused string
     * @param shutdownString the shutdown string
     * @param statusString the status string
     */
    private void putJsonInModel(final JsonObject threadPoolExecutorsJson, final JsonObject threadPoolTaskSchedulersJson,
            final String priority, final Map<String, Object> model, final String pausedString, final String shutdownString,
            final String statusString) {

        final JsonObject json = new JsonObject();
        final JsonObject threadPoolExecutor = threadPoolExecutorsJson.getAsJsonObject(priority);
        model.put(pausedString, isPaused(threadPoolExecutor));
        json.add("threadPoolExecutor", threadPoolExecutor);
        final JsonObject threadPoolTaskScheduler = threadPoolTaskSchedulersJson.getAsJsonObject(priority);
        json.add("threadPoolTaskScheduler", threadPoolTaskScheduler);
        model.put(statusString, json.toString());
        model.put(shutdownString, isShutdown(threadPoolTaskScheduler));
    }

    /**
     * Adds the archivist stats to model.
     *
     * @param model the model
     * @throws Exception the exception
     */
    private void addArchivistStatsToModel(final Map<String, Object> model) throws Exception {
        final String stats = getJsonResponse("service/stats");
        if (StringUtils.isNotBlank(stats)) {
            final JsonParser jsonParser = new JsonParser();
            final JsonElement json = jsonParser.parse(stats);
            final JsonObject asJsonObject = json.getAsJsonObject();

            final JsonObject dailyJsonElement = asJsonObject.getAsJsonObject(TYPE.DAILY.toString());
            model.put("DAILY_STATS", dailyJsonElement.toString());

            final JsonObject bulkJsonElement = asJsonObject.getAsJsonObject(TYPE.BULK.toString());
            model.put("BULK_STATS", bulkJsonElement.toString());

            final JsonObject authenticojJsonElement = asJsonObject.getAsJsonObject(TYPE.AUTHENTICOJ.toString());
            model.put("AUTHENTICOJ_STATS", authenticojJsonElement.toString());
            
            final JsonObject embargoJsonElement = asJsonObject.getAsJsonObject("EMBARGO");
            model.put("EMBARGO_STATS", embargoJsonElement.toString());
        }
    }

    /**
     * Checks if is paused.
     *
     * @param asJsonObject the as json object
     * @return true, if is paused
     */
    private boolean isPaused(final JsonObject asJsonObject) {
        final JsonObject threadPoolStatusJson = asJsonObject.getAsJsonObject("threadPoolExecutor");
        final String paused = threadPoolStatusJson.get("paused").toString();
        final boolean result = Boolean.parseBoolean(paused);
        return result;
    }

    /**
     * Checks if is shutdown.
     *
     * @param asJsonObject the as json object
     * @return true, if is shutdown
     */
    private boolean isShutdown(final JsonObject asJsonObject) {
        final String isShutdown = asJsonObject.get("shutdown").toString();
        final boolean result = Boolean.parseBoolean(isShutdown);
        return result;
    }

    /**
     * Gets the json response.
     *
     * @param archivistBaseUrlPreffix the archivist base url preffix
     * @return the json response
     * @throws Exception the exception
     */
    private String getJsonResponse(final String archivistBaseUrlPreffix) throws Exception {

    	final String disseminationURL = this.cellarConfiguration.getCellarServiceIntegrationArchivistBaseUrl()
    			+ archivistBaseUrlPreffix;

    	final HttpGet httpGet = new HttpGet(disseminationURL);
    	httpGet.setHeader(HttpHeaders.ACCEPT_CHARSET, "UTF-8");
    	CloseableHttpResponse httpResponse = httpClient.execute(httpGet);
    	HttpEntity httpEntity = httpResponse.getEntity();


    	try(InputStream bodyStream = httpEntity.getContent();final StringWriter writer = new StringWriter()) {

    		if (httpResponse.getStatusLine().getStatusCode() == 200) { // "OK" HTTP Response Code
    			IOUtils.copy(bodyStream, writer, "utf-8");
    			return writer.toString();
    		}
    		else
    			throw new IllegalStateException("The HTTP request for " + disseminationURL + " returned the following status code " + httpResponse.getStatusLine().getStatusCode());

    	}
    	finally {
    		EntityUtils.consume(httpEntity);
    		httpResponse.close();
    	}
    }

}
