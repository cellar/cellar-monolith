package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.virtuoso;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import eu.europa.ec.opoce.cellar.ExecutionStatus;
import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.admin.error.PageNotFoundController;
import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage;
import eu.europa.ec.opoce.cellar.cl.status.IServiceStatus;
import eu.europa.ec.opoce.cellar.cmr.database.dao.SparqlLoadRequestRepository;
import eu.europa.ec.opoce.cellar.exception.CellarConfigurationException;
import eu.europa.ec.opoce.cellar.modelload.service.SparqlLoadRequestSchedulerService;
import eu.europa.ec.opoce.cellar.sparql.SparqlLoadRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static javax.ws.rs.core.HttpHeaders.CONTENT_TYPE;

@Controller
public class VirtuosoController extends AbstractCellarController {

    private static final Logger LOG = LogManager.getLogger(VirtuosoController.class);

    private static final String SPARQL_PAGE_URL = "/cmr/virtuoso/sparqlloadrequests";
    private static final String SPARQL_JSP_NAME = "cmr/virtuoso/sparql-load-requests";
    private static final String SPARQL_EDIT_JSP = "cmr/virtuoso/edit-sparql-load-requests";
    private static final String DATE_FORMAT = "dd/MM/yyyy";

    private final SparqlLoadRequestSchedulerService sparqlLoadRequestSchedulerService;
    private final SparqlLoadRequestRepository sparqlLoadRequestRepository;
    private final IServiceStatus virtuosoDataSourceStatus;

    @Autowired
    public VirtuosoController(SparqlLoadRequestSchedulerService sparqlLoadRequestSchedulerService, SparqlLoadRequestRepository sparqlLoadRequestRepository,
                              @Qualifier("virtuosoDataSourceStatus") IServiceStatus virtuosoDataSourceStatus) {
        this.sparqlLoadRequestSchedulerService = sparqlLoadRequestSchedulerService;
        this.sparqlLoadRequestRepository = sparqlLoadRequestRepository;
        this.virtuosoDataSourceStatus = virtuosoDataSourceStatus;
    }


    @RequestMapping(value = SPARQL_PAGE_URL, method = RequestMethod.GET)
    public String getSparqlLoadRequestGet(final ModelMap model) {
        model.addAttribute("submissionStatus", ExecutionStatus.values());
        model.addAttribute("sparqlLoadRequests", this.sparqlLoadRequestRepository.findAll());
        return SPARQL_JSP_NAME;
    }

    @RequestMapping(method = {
            RequestMethod.GET, RequestMethod.POST
    }, value = SPARQL_PAGE_URL + "/search")
    public ModelAndView searchMmodelLoadRequest(final HttpServletRequest request, final HttpServletResponse response) {
        String modelURI = request.getParameter("modelUri");
        String executionStatus = request.getParameter("subStatus");
        ModelMap model = new ModelMap()
                .addAttribute("uri", modelURI)
                .addAttribute("subStatus", executionStatus)
                .addAttribute("submissionStatus", ExecutionStatus.values())
                .addAttribute("sparqlLoadRequests", findRequests(modelURI, executionStatus));
        return new ModelAndView(SPARQL_JSP_NAME, model);
    }

    private List<SparqlLoadRequest> findRequests(String uri, String executionStatus) {
        boolean uriEmpty = Strings.isNullOrEmpty(uri);
        boolean statusEmpty = Strings.isNullOrEmpty(executionStatus);
        if (uriEmpty && statusEmpty) {
            return Lists.newArrayList(sparqlLoadRequestRepository.findAll());
        } else if (uriEmpty) {
            return sparqlLoadRequestRepository.findByExecutionStatus(ExecutionStatus.valueOf(executionStatus));
        } else if (statusEmpty) {
            return sparqlLoadRequestRepository.findByUri(uri)
                    .map(Collections::singletonList)
                    .orElse(Collections.emptyList());
        } else {
            return sparqlLoadRequestRepository.findByUriAndExecutionStatus(uri, ExecutionStatus.valueOf(executionStatus));
        }

    }

    @RequestMapping(method = {
            RequestMethod.GET, RequestMethod.POST
    }, value = SPARQL_PAGE_URL + "/restart")
    public String restartModelLoadRequest(final HttpServletRequest request, final HttpServletResponse response) throws ParseException {
        String id = request.getParameter("idSelected");
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        Date activationDate = sdf.parse(request.getParameter("activationDate"));
        sparqlLoadRequestSchedulerService.restart(Long.valueOf(id), activationDate);
        addUserMessage(ICellarUserMessage.LEVEL.SUCCESS, "common.message.successfullyProcessed");
        LOG.info("The model '" + id + "' has been restarted");
        return redirect(SPARQL_PAGE_URL);
    }

    @RequestMapping(method = RequestMethod.GET, value = SPARQL_PAGE_URL + "/edit")
    public ModelAndView updateSparqlLoadRequestGet(final HttpServletRequest request, final HttpServletResponse response) {
        return sparqlLoadRequestRepository.findById(Long.valueOf(request.getParameter("id")))
                .map(r -> new ModelAndView(SPARQL_EDIT_JSP, new ModelMap()
                        .addAttribute("modelId", r.getId())
                        .addAttribute("modelUri", r.getUri())
                        .addAttribute("externalPid", r.getExternalPid())
                        .addAttribute("activationDate", new SimpleDateFormat(DATE_FORMAT)
                                .format(r.getActivationDate()))
                        .addAttribute("subStatus", r.getExecutionStatus().name())
                        .addAttribute("submissionStatus", ExecutionStatus.values())))
                .orElse(new ModelAndView(PageNotFoundController.ERROR_VIEW, new ModelMap()));


    }

    @RequestMapping(method = RequestMethod.POST, value = SPARQL_PAGE_URL + "/update")
    public String updateModelLoadRequestPost(final HttpServletRequest request, final HttpServletResponse response) {
        sparqlLoadRequestRepository.findById(Long.valueOf(request.getParameter("modelId")))
                .ifPresent(r -> {
                    r.setExecutionStatus(ExecutionStatus.valueOf(request.getParameter("subStatus")));
                    try {
                        r.setActivationDate(new SimpleDateFormat(DATE_FORMAT).parse(request.getParameter("activationDate")));
                    } catch (ParseException e) {
                        throw new RuntimeException(e);
                    }
                    SparqlLoadRequest saved = sparqlLoadRequestRepository.save(r);
                    addUserMessage(ICellarUserMessage.LEVEL.SUCCESS, "common.message.successfullySaved");
                    LOG.info("The model '" + saved.getUri() + "' has been updated (" + saved.getExecutionStatus() + ")");
                });

        return redirect(SPARQL_PAGE_URL);
    }

    /**
     * Checks if is available.
     *
     * @return the response entity
     */
    @RequestMapping(value = "/secapi/configuration/virtuoso/isAvailable", method = RequestMethod.GET)
    public ResponseEntity<Boolean> isIngesting() {
        final HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add(CONTENT_TYPE, MediaType.TEXT_PLAIN + ";charset=UTF-8");
        return new ResponseEntity<>(isVirtuosoAvailable(), responseHeaders, HttpStatus.OK);
    }

    private boolean isVirtuosoAvailable() {
        if (cellarConfiguration.isVirtuosoIngestionEnabled()) {
            try {
                this.virtuosoDataSourceStatus.check();
            } catch (CellarConfigurationException e) {
                LOG.warn("Virtuoso check failed", e);
                return false;
            }
        }
        return false;

    }

}
