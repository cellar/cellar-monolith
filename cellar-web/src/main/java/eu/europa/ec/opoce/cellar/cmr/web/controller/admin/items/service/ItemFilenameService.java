package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.items.service;

import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.items.form.Filename;
import eu.europa.ec.opoce.cellar.common.util.StringUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import eu.europa.ec.opoce.cellar.s3.S3ContentStreamService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author ARHS Developments
 */
@Service
public class ItemFilenameService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ItemFilenameService.class);
    public static final int PAGE_SIZE = 100;

    private IdentifierService identifierService;
    private CellarResourceDao cellarResourceDao;
    private S3ContentStreamService s3ContentStreamService;

    @Autowired
    public ItemFilenameService(@Qualifier("pidManagerService") final IdentifierService identifierService,
                               final S3ContentStreamService s3ContentStreamService,
                               final CellarResourceDao cellarResourceDao) {
        this.identifierService = identifierService;
        this.s3ContentStreamService = s3ContentStreamService;
        this.cellarResourceDao = cellarResourceDao;
    }

    public List<Filename> getFilenames(final String id, final int page) {
        Assert.isTrue(page > 0, "Page must be positive");

        return getFilenames(id)
                .stream()
                .skip((page - 1) * PAGE_SIZE)
                .limit(PAGE_SIZE)
                .collect(Collectors.toList());
    }

    public List<Filename> getFilteredFilenames(final String id, final int page) {
        Assert.isTrue(page > 0, "Page must be positive");

        return getFilenames(id)
                .stream()
                .filter(Filename::differFromSuggestion) // paginate after filter to get complete pages
                .skip((page - 1) * PAGE_SIZE)
                .limit(PAGE_SIZE)
                .collect(Collectors.toList());
    }

    private List<Filename> getFilenames(final String id) {
        final String cellarId = this.identifierService.getCellarPrefixed(id);
        return cellarResourceDao.findStartingWithSortByCellarId(cellarId)
                .stream()
                .filter(r -> r.getCellarType() == DigitalObjectType.ITEM)
                .map(this::toFilename)
                .collect(Collectors.toList());
    }

    private Filename toFilename(final CellarResource resource) {
        final String cellarId = resource.getCellarId();
        final List<String> pids = identifierService.getIdentifier(cellarId).getPids();
        final String version = resource.getVersion(ContentType.FILE).orElse(null);
        final String filename = getFilenameFromS3(cellarId, version);
        final String suggestedFilename = computeSuggestedFilename(pids, cellarId);

        return new Filename(cellarId, filename, suggestedFilename, pids, version);
    }

    private String getFilenameFromS3(final String cellarId, final String version) {
        final Map<String, Object> metadata = s3ContentStreamService.getMetadata(cellarId, version, ContentType.FILE);
        return (String) metadata.get(ContentStreamService.Metadata.FILENAME);
    }

    private String computeSuggestedFilename(final List<String> pids, final String itemCellarId) {
        final String manifestationCellarId = itemCellarId.substring(0, 51);
        final Identifier manifestation = identifierService.getIdentifier(manifestationCellarId);
        final String manifestationPid = manifestation.getPids().stream()
                .filter(pid -> !pid.startsWith("oj:"))
                .findAny()
                .orElse(null);

        if (manifestationPid == null) {
            return "none";
        }

        return pids.stream()
                .filter(pid -> pid.startsWith(manifestationPid))
                .map(pid -> pid.replace(manifestationPid, "")) // .filename.pdf
                .map(pid -> pid.substring(1))                              // filename.pdf
                .findFirst()
                .orElse("none");
    }

    @Transactional
    public List<Filename> updateItemFilenames(final List<Filename> filenames) {
        return filenames.stream()
                .filter(Filename::filenameChanged)
                .peek(filename -> {
                    LOGGER.info("Updating filename of '{}'", filename);
                    s3ContentStreamService.updateMetadata(
                            filename.getCellarId(),
                            StringUtils.defaultIfBlank(filename.getVersion(), null),
                            ContentType.FILE,
                            Collections.singletonMap(ContentStreamService.Metadata.FILENAME, filename.getFilename())
                    );
                }).collect(Collectors.toList());
    }
}
