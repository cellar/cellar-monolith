/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.command
 *             FILE : CreateExportJob.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 2, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.command;

/**
 * <class_description> Create export job.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 2, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 24 mars 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class CreateExportJob extends CreateJob {

    /** The metadata only. */
    private Boolean metadataOnly;

    /** The desitnation folder. */
    private String destinationFolder;

    /** the use agnostic resource URLs. */
    private Boolean useAgnosticURL = Boolean.TRUE;//by default its true

    /**
     * Default constructor.
     */
    public CreateExportJob() {
        super();
    }

    /**
     * Gets the metadata only.
     *
     * @return the metadata only
     */
    public Boolean getMetadataOnly() {
        return this.metadataOnly;
    }

    /**
     * Sets the metadata only.
     *
     * @param metadataOnly the new metadata only
     */
    public void setMetadataOnly(final Boolean metadataOnly) {
        this.metadataOnly = metadataOnly;
    }

    /**
     * Gets the use agnostic url.
     *
     * @return the use agnostic url
     */
    public Boolean getUseAgnosticURL() {
        return this.useAgnosticURL;
    }

    /**
     * Sets the use agnostic url.
     *
     * @param useAgnosticURL the new use agnostic url
     */
    public void setUseAgnosticURL(final Boolean useAgnosticURL) {
        this.useAgnosticURL = useAgnosticURL;
    }

    /**
     * Gets the destination folder.
     *
     * @return the destination folder
     */
    public String getDestinationFolder() {
        return this.destinationFolder;
    }

    /**
     * Sets the destination folder.
     * @param destinationFolder the new destination folder
     */
    public void setDestinationFolder(final String destinationFolder) {
        this.destinationFolder = destinationFolder;
    }
}
