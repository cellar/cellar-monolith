/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.unauthorized
 *        FILE : UserCredentialsCommand.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 15-03-2023
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2023 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.unauthorized;

/**
 * User credentials form-backing object.
 * @author EUROPEAN DYNAMICS S.A
 */
public class UserCredentialsCommand {

	/**
	 * 
	 */
	private String username;
	/**
	 * 
	 */
	private String password;
	
	public UserCredentialsCommand() {

	}
	
	public UserCredentialsCommand(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
}
