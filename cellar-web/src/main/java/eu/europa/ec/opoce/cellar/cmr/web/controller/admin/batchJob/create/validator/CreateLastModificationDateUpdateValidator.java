/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.validator
 *             FILE : CreateLastModificationDateUpdateValidator.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 7, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-06-07 07:49:00 +0200 (Wed, 07 Jun 2017) $
 *          VERSION : $LastChangedRevision: $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.validator;

import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.command.CreateLastModificationDateJob;
import org.springframework.stereotype.Component;

/**
 * @author ARHS Developments
 */
@Component
public class CreateLastModificationDateUpdateValidator extends CreateJobValidator {

    @Override
    public boolean supports(final Class<?> clazz) {
        return CreateLastModificationDateJob.class.isAssignableFrom(clazz);
    }
}
