package eu.europa.ec.opoce.cellar.cmr.web.controller.admin;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.semantic.json.JacksonUtils;
import eu.europa.ec.opoce.cellar.server.admin.sparql.*;
import eu.europa.ec.opoce.cellar.virtuoso.VirtuosoService;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;

@Controller
public class SparqlController extends AbstractCellarController {

    private static final Logger LOG = LogManager.getLogger(SparqlController.class);
    private static final String CONTENT_TYPE_QUERY = "text/plain";

    private static final String URL_SPARQL_ENDPOINT = "/admin/cmr/sparql/endpoint";
    private static final String URL_CONFIGURATION = "/admin/cmr/sparql/configuration";

    @Autowired
    private XsltResolverService xsltResolverService;

    @Autowired
    private VirtuosoService virtuosoService;

    @RequestMapping(value = "/cmr/sparql", method = RequestMethod.GET)
    public ModelAndView getSparqlInterface(final HttpServletRequest request, final HttpServletResponse response) {
        return new ModelAndView(new RedirectView(URL_SPARQL_ENDPOINT, true));
    }

    @RequestMapping(value = "/cmr/sparql/endpoint", method = RequestMethod.GET)
    public ModelAndView getSparqlEndpoint(final HttpServletRequest request, final HttpServletResponse response) {
        final ModelAndView modelAndView = new ModelAndView("sparqlEndpoint");
        modelAndView.addObject("virtuosoInformation", virtuosoService.getVirtuosoInformation());
        modelAndView.addObject("sparqlServiceUrl", this.cellarConfiguration.getCellarServiceSparqlUri());
        modelAndView.addObject("stylesheets", this.xsltResolverService.getStylesheetOptions());
        return modelAndView;
    }

    @RequestMapping(value = "/cmr/sparql/configuration", method = RequestMethod.GET)
    public ModelAndView getSparqlConfiguration(final HttpServletRequest request, final HttpServletResponse response) {
        final ModelAndView modelAndView = new ModelAndView("sparqlConfiguration");
        modelAndView.addObject(new UploadXslt());
        modelAndView.addObject(new UploadDefinition());
        modelAndView.addObject("stylesheets", this.xsltResolverService.getStylesheetsWithMimetype());
        modelAndView.addObject("definitions", this.xsltResolverService.getDefinitions());
        return modelAndView;
    }

    @RequestMapping(value = "/cmr/sparql/configuration/deleteDefinitions", method = RequestMethod.POST)
    public ModelAndView deleteDefinitions(final HttpServletRequest request, final HttpServletResponse response,
            @RequestParam(value = "definitions") final String definitions) {
        final List<String> definitionList = Arrays.asList(StringUtils.split(definitions, ","));
        this.xsltResolverService.deleteDefintions(definitionList);
        return new ModelAndView(new RedirectView(URL_CONFIGURATION, true));
    }

    @RequestMapping(value = "/cmr/sparql/configuration/deleteStylesheets", method = RequestMethod.POST)
    public ModelAndView deleteStylesheets(final HttpServletRequest request, final HttpServletResponse response,
            @RequestParam(value = "stylesheets") final String stylesheets) {
        final List<String> stylesheetList = Arrays.asList(StringUtils.split(stylesheets, ","));
        this.xsltResolverService.deleteStylesheets(stylesheetList);
        return new ModelAndView(new RedirectView(URL_CONFIGURATION, true));
    }

    @RequestMapping(value = "/cmr/sparql/configuration/printStylesheets", method = RequestMethod.GET)
    public ModelAndView printStylesheets(final HttpServletRequest request, final HttpServletResponse response,
            @RequestParam(value = "q") final String stylesheets) {
        final String[] stylesheetList = StringUtils.split(stylesheets, ",");
        final Map<String, String> stylesheetMap = new HashMap<>();
        for (final String stylesheet : stylesheetList) {
            stylesheetMap.put(stylesheet, this.xsltResolverService.getStylesheet(stylesheet));
        }
        final ModelAndView modelAndView = new ModelAndView("printSparql");
        modelAndView.addObject("list", stylesheetMap);
        return modelAndView;
    }

    @RequestMapping(value = "/cmr/sparql/configuration/printDefinitions", method = RequestMethod.GET)
    public ModelAndView printDefinitions(final HttpServletRequest request, final HttpServletResponse response,
            @RequestParam(value = "q") final String definitions) {
        final String[] definitionList = StringUtils.split(definitions, ",");
        final Map<String, String> definitionMap = new HashMap<>();
        for (final String definition : definitionList) {
            definitionMap.put(definition, this.xsltResolverService.getDefinition(definition));
        }
        final ModelAndView modelAndView = new ModelAndView("printSparql");
        modelAndView.addObject("list", definitionMap);
        return modelAndView;
    }

    @RequestMapping(value = "/cmr/sparql/configuration/uploadXslt", method = RequestMethod.POST)
    public ModelAndView createXslt(final HttpServletRequest request, final UploadXslt uploadItem, final BindingResult result) {
        if (result.hasErrors()) {
            for (final ObjectError error : result.getAllErrors()) {
                LOG.error("Error while uploading file: {} - {}", error.getCode(), error.getDefaultMessage());
            }
        }
        this.xsltResolverService.saveStylesheet(uploadItem);
        return new ModelAndView(new RedirectView(URL_CONFIGURATION, true));
    }

    @RequestMapping(value = "/cmr/sparql/configuration/uploadDefinition", method = RequestMethod.POST)
    public ModelAndView createXslt(final HttpServletRequest request, final UploadDefinition uploadItem, final BindingResult result) {
        if (result.hasErrors()) {
            for (final ObjectError error : result.getAllErrors()) {
                LOG.error("Error while uploading file: {} - {}", error.getCode(), error.getDefaultMessage());
            }
        }
        this.xsltResolverService.saveDefinition(uploadItem);
        return new ModelAndView(new RedirectView(URL_CONFIGURATION, true));
    }

    @RequestMapping(value = "/cmr/sparql/endpoint/getQueries", method = RequestMethod.GET)
    public void getQueries(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        final Query[] queries = this.xsltResolverService.getQueries();
        JacksonUtils.streamAsJson(response.getOutputStream(), queries, true);
    }

    @RequestMapping(value = "/cmr/sparql/endpoint/getQuery", method = RequestMethod.POST)
    public void getQuery(final HttpServletRequest request, final HttpServletResponse response,
            @RequestParam(value = "q") final String query) throws IOException {
        final String s = this.xsltResolverService.getQuery(query);
        response.setContentType(CONTENT_TYPE_QUERY);
        response.getOutputStream().write(s.getBytes(StandardCharsets.UTF_8));
        response.getOutputStream().flush();
    }

}
