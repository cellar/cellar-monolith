/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.security
 *        FILE : AcceptUserAccessRequestController.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 15-03-2023
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2023 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.admin.security;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.admin.error.PageNotFoundController;
import eu.europa.ec.opoce.cellar.cl.controller.admin.security.command.AcceptUserCommand;
import eu.europa.ec.opoce.cellar.cl.controller.admin.security.validator.UserValidator;
import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL;
import eu.europa.ec.opoce.cellar.cl.domain.admin.Group;
import eu.europa.ec.opoce.cellar.cl.domain.admin.User;
import eu.europa.ec.opoce.cellar.cl.domain.admin.UserAccessRequest;
import eu.europa.ec.opoce.cellar.cl.service.client.SecurityService;
import eu.europa.ec.opoce.cellar.cl.service.client.UserAccessRequestService;

/**
 * 
 * @author EUROPEAN DYNAMICS S.A
 */
@Controller
@RequestMapping(value = AcceptUserAccessRequestController.PAGE_URL)
public class AcceptUserAccessRequestController extends AbstractCellarController {

	/**
	 * 
	 */
    protected static final String PAGE_URL = "/security/access-requests";
    /**
     * 
     */
    protected static final String ACCEPT_URL = "/accept";
    /**
     * 
     */
    private static final String OBJECT_NAME = "acceptUserCommand";
    /**
     * 
     */
    private static final String JSP_ACCEPT_USER = "security/user/accept";
    /**
     * 
     */
    private static final String DEFAULT_VALIDATION_REJECTION_MESSAGE = "The provided input is invalid.";

    /**
     * User access request service.
     */
    private final UserAccessRequestService userAccessRequestService;
    /**
     * Security service manager.
     */
    private final SecurityService securityService;
	/**
     * Validator for a created/edited user.
     */
    private final UserValidator userValidator;
    
	@Autowired
    public AcceptUserAccessRequestController(SecurityService securityService, UserAccessRequestService userAccessRequestService, UserValidator userValidator) {
		this.securityService = securityService;
		this.userAccessRequestService = userAccessRequestService;
		this.userValidator = userValidator;
	}


    @ModelAttribute("groupsList")
    public List<Group> populateGroupsList() {
        return this.securityService.findAllGroups();
    }

    @GetMapping(value = ACCEPT_URL)
    public String displayUserAccessRequest(final HttpServletRequest request, final HttpServletResponse response, final ModelMap model, @RequestParam final Long id) {
        final UserAccessRequest userAccessRequest = this.userAccessRequestService.findUserAccessRequest(id);
        if (userAccessRequest == null) {
            return PageNotFoundController.ERROR_VIEW;
        }

        final AcceptUserCommand userToAccept = new AcceptUserCommand();
        userToAccept.setUserAccessRequestId(id);
        userToAccept.setUsername(userAccessRequest.getUsername());
        model.addAttribute(OBJECT_NAME, userToAccept);
        
        return JSP_ACCEPT_USER;
    }
    
    @PostMapping(value = ACCEPT_URL)
    public String acceptUserAccessRequest(final HttpServletRequest request, final HttpServletResponse response,
    		@ModelAttribute(OBJECT_NAME) final AcceptUserCommand acceptedUser, final BindingResult result) {
        this.userValidator.validate(acceptedUser, result);
        if (result.hasErrors()) {
        	return handlePostValidationErrors(result, acceptedUser);
        }

        final Group group = new Group();
        group.setId(acceptedUser.getGroupId());
        final User user = new User();
        user.setEnabled(acceptedUser.isEnabled());
        user.setGroup(group);
        user.setUsername(acceptedUser.getUsername());
        user.setPassword(null);

        try {
        	this.userAccessRequestService.acceptUserAccessRequest(user, acceptedUser.getUserAccessRequestId());
        }
        catch (Exception e) {
        	addUserMessage(LEVEL.ERROR, "security.user-access-request.manager.accept.failure");
        	return redirect(PAGE_URL);
        }
        super.addUserMessage(LEVEL.SUCCESS, "security.user-access-request.manager.accept.success");
        return redirect(UserManagerController.PAGE_URL);
    }
    
    private String handlePostValidationErrors(final BindingResult result, final AcceptUserCommand acceptedUser) {
    	addUserMessage(LEVEL.ERROR, result.getFieldError().getCode(), DEFAULT_VALIDATION_REJECTION_MESSAGE);
    	if (CollectionUtils.isNotEmpty(result.getFieldErrors("userAccessRequestId"))) {
    		return redirect(PAGE_URL);
    	}
    	return redirect(PAGE_URL + ACCEPT_URL, "id", acceptedUser.getUserAccessRequestId());
    }
    
}
