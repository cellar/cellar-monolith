/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.visibility
 *             FILE : VisibilityController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 19, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.visibility;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;

/**
 * <class_description> Visibility tab controller.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 19, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
@RequestMapping(value = VisibilityController.PAGE_URL)
public class VisibilityController extends AbstractCellarController {

    public static final String PAGE_URL = "/cmr/visibility";

    /**
     * Handle the GET.
     */
    @RequestMapping(method = RequestMethod.GET)
    public String securityGet(final ModelMap model) {
        return redirect(VisibilitySearchManagerController.PAGE_URL);
    }
}
