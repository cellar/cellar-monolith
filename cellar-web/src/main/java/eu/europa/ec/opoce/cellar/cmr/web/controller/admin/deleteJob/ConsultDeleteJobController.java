/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.consult
 *             FILE : ConsultUpdateJobController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 2, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.deleteJob;

import eu.europa.ec.opoce.cellar.cl.controller.admin.error.PageNotFoundController;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob.BATCH_JOB_TYPE;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.consult.ConsultJobController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import static eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor.DeleteJobProcessor.SOME_GRAPHS_FAILED_TO_BE_DELETED;

/**
 * <class_description> Consult update job controller.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 2, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
@RequestMapping(ConsultDeleteJobController.PAGE_URL)
public class ConsultDeleteJobController extends ConsultJobController {

    public static final String PAGE_URL = "/cmr/delete/job/consult";
    public static final String SHOW_ERROR_TEXT_AREA = "showErrorTextArea";
    private static final String OBJECT_NAME = "batchJob";
    private static final String JSP_NAME = "cmr/delete/job/consult-job";

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getFunctionalityUrl() {
        return DeleteJobManagerController.PAGE_URL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BATCH_JOB_TYPE getBatchJobType() {
        return BATCH_JOB_TYPE.DELETE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getJspName() {
        return JSP_NAME;
    }

    @Override
    @GetMapping()
    public String editUserGet(final ModelMap model, @RequestParam final Long id, @RequestParam(required = false) final String workFilter,
                              @RequestParam(required = false) final String dir, @RequestParam(required = false) final Integer page) {
        final BatchJob batchJob = this.batchJobService.getBatchJob(id, this.getBatchJobType());

        if (batchJob == null) {
            return PageNotFoundController.ERROR_VIEW; // job not found
        }
        model.addAttribute(SHOW_ERROR_TEXT_AREA, isToShowErrorInTextArea(batchJob.getErrorDescription()));
        model.addAttribute(OBJECT_NAME, batchJob);

        return this.getJspName();
    }

    private boolean isToShowErrorInTextArea(String errorDescription) {
        return errorDescription != null && errorDescription.equals(SOME_GRAPHS_FAILED_TO_BE_DELETED);
    }
}
