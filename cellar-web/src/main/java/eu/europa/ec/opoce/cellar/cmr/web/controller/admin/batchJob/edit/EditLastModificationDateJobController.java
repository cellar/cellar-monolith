/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.edit
 *             FILE : EditLastModificationDateJobController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 7, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-06-07 07:49:00 +0200 (Wed, 07 Jun 2017) $
 *          VERSION : $LastChangedRevision: $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.edit;

import eu.europa.ec.opoce.cellar.cl.controller.admin.error.PageNotFoundController;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.command.CreateJob;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.command.CreateLastModificationDateJob;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.validator.CreateLastModificationDateUpdateValidator;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.manager.LastModificationDateJobManagerController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;

/**
 * @author ARHS Developments
 */
@Controller
@RequestMapping(EditLastModificationDateJobController.PAGE_URL)
public class EditLastModificationDateJobController extends EditJobController {

    public static final String PAGE_URL = "/cmr/update/job/last-modification-date/edit";

    private static final String JSP_NAME = "cmr/update/job/last-modification-date/edit-job";

    private static final String OBJECT_NAME = "job";

    private final Validator validator;

    @Autowired
    public EditLastModificationDateJobController(CreateLastModificationDateUpdateValidator validator) {
        this.validator = validator;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String createJobGet(final ModelMap model, @RequestParam final Long id) {

        final BatchJob batchJob = batchJobService.getBatchJob(id, BatchJob.BATCH_JOB_TYPE.LAST_MODIFICATION_DATE_UPDATE);
        if (batchJob == null) {
            return PageNotFoundController.ERROR_VIEW;
        }

        CreateLastModificationDateJob job = new CreateLastModificationDateJob();
        job.setId(batchJob.getId());
        job.setJobName(batchJob.getJobName());
        job.setSparqlQuery(batchJob.getSparqlQuery());

        model.addAttribute(OBJECT_NAME, job);
        return JSP_NAME;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String createJobPost(ModelMap model, @ModelAttribute(OBJECT_NAME) CreateLastModificationDateJob createdJob,
                                BindingResult bindingResult, SessionStatus status) {
        return super.editJob(model, createdJob, bindingResult, status);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Validator getEditJobValidator() {
        return validator;
    }

    @Override
    protected BatchJob constructBatchJob(final CreateJob createJob) {
        final CreateLastModificationDateJob job = (CreateLastModificationDateJob) createJob;
        return BatchJob.create(BatchJob.BATCH_JOB_TYPE.LAST_MODIFICATION_DATE_UPDATE, job.getJobName(), job.getSparqlQuery())
                .id(job.getId())
                .build();
    }

    @Override
    protected String getJspName() {
        return JSP_NAME;
    }

    @Override
    protected String getSuccessPageUrl() {
        return LastModificationDateJobManagerController.PAGE_URL;
    }

    @Override
    protected String getFunctionalityUrl() {
        return LastModificationDateJobManagerController.PAGE_URL;
    }

    @Override
    protected BatchJob.BATCH_JOB_TYPE getBatchJobType() {
        return BatchJob.BATCH_JOB_TYPE.LAST_MODIFICATION_DATE_UPDATE;
    }
}
