/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob
 *             FILE : BatchReindexManagerController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 12, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.manager;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob.BATCH_JOB_TYPE;

/**
 * <class_description> Reindex batch jobs manager.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 12, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
@RequestMapping(ReindexJobManagerController.PAGE_URL)
public class ReindexJobManagerController extends BatchJobManagerController {

    public static final String PAGE_URL = "/cmr/indexing/reindex";

    private static final String JSP_NAME = "cmr/indexing/reindex";

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getPageUrl() {
        return PAGE_URL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getFunctionalityUrl() {
        return PAGE_URL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BATCH_JOB_TYPE getBatchJobType() {
        return BATCH_JOB_TYPE.REINDEX;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getJspName() {
        return JSP_NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<BatchJob> getBatchJobs() {
        return this.batchJobService.findBatchJobs(BATCH_JOB_TYPE.REINDEX);
    }
}
