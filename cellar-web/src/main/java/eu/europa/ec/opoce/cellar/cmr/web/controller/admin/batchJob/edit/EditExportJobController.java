/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.edit
 *             FILE : EditExportJobController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 29, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.edit;

import eu.europa.ec.opoce.cellar.cl.controller.admin.error.PageNotFoundController;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob.BATCH_JOB_TYPE;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor.ExportBatchJobProcessor;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.command.CreateExportJob;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.command.CreateJob;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.manager.ExportJobManagerController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;

/**
 * <class_description> Edit export job controller.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 29, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
@RequestMapping(EditExportJobController.PAGE_URL)
public class EditExportJobController extends EditJobController {

    /** The Constant PAGE_URL. */
    public static final String PAGE_URL = "/cmr/export/job/edit";

    /** The Constant JSP_NAME. */
    private static final String JSP_NAME = "cmr/export/job/edit-job";

    /** The Constant OBJECT_NAME. */
    private static final String OBJECT_NAME = "job";

    /** The Constant BASE_DESTINATION_FOLDER. */
    private static final String BASE_DESTINATION_FOLDER = "baseDestinationFolder";

    /** The Constant DEFAULT_FOLDER_NAME. */
    private static final String DEFAULT_FOLDER_NAME = "defaultFolderName";

    /**
     * Validator for a created job.
     */
    @Autowired
    @Qualifier("exportJobValidator")
    private Validator jobValidator;

    /**
     * Handle the GET.
     *
     * @param model the model
     * @param id the id
     * @return the string
     */
    @RequestMapping(method = RequestMethod.GET)
    public String createJobGet(final ModelMap model, @RequestParam final Long id) {

        final BatchJob batchJob = this.batchJobService.getBatchJob(id, BATCH_JOB_TYPE.EXPORT); // gets the batch job

        if (batchJob == null) {
            return PageNotFoundController.ERROR_VIEW;
        }
        final CreateExportJob editExportJob = new CreateExportJob();

        editExportJob.setId(batchJob.getId());
        editExportJob.setJobName(batchJob.getJobName());
        editExportJob.setSparqlQuery(batchJob.getSparqlQuery());
        final Boolean metadataOnly = batchJob.getMetadataOnly();
        editExportJob.setMetadataOnly(metadataOnly != null && metadataOnly);
        final Boolean useAgnosticURL = batchJob.getUseAgnosticURL();
        editExportJob.setUseAgnosticURL(useAgnosticURL);
        editExportJob.setDestinationFolder(batchJob.getDestinationFolder());
        model.addAttribute(OBJECT_NAME, editExportJob);
        model.addAttribute(BASE_DESTINATION_FOLDER, this.cellarConfiguration.getCellarFolderExportBatchJob());
        model.addAttribute(DEFAULT_FOLDER_NAME, ExportBatchJobProcessor.getDefaultFolderName(batchJob.getId()));

        return JSP_NAME;
    }

    /**
     * Handle the POST.
     *
     * @param model the model
     * @param createdJob the created job
     * @param bindingResult the binding result
     * @param status the status
     * @return the string
     */
    @RequestMapping(method = RequestMethod.POST)
    public String createJobPost(final ModelMap model, @ModelAttribute(OBJECT_NAME) final CreateExportJob createdJob,
            final BindingResult bindingResult, final SessionStatus status) {

        return super.editJob(model, createdJob, bindingResult, status);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Validator getEditJobValidator() {
        return this.jobValidator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BatchJob constructBatchJob(final CreateJob createJob) {
        final CreateExportJob createExportJob = (CreateExportJob) createJob;
        final Boolean metadataOnly = createExportJob.getMetadataOnly();
        final boolean metadataOnlyBoolean = metadataOnly != null && metadataOnly;
        final Boolean useAgnosticURL = createExportJob.getUseAgnosticURL();
        final boolean useAgnosticURLBoolean = useAgnosticURL != null && useAgnosticURL;
        final Long id = createExportJob.getId();
        final String jobName = createExportJob.getJobName();
        final String sparqlQuery = createExportJob.getSparqlQuery();
        final String destinationFolder = createExportJob.getDestinationFolder();

        return BatchJob.create(BATCH_JOB_TYPE.EXPORT, jobName, sparqlQuery)
                .id(id)
                .metadataOnly(metadataOnlyBoolean)
                .destinationFolder(destinationFolder)
                .useAgnosticURL(useAgnosticURLBoolean)
                .build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getJspName() {
        return JSP_NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getSuccessPageUrl() {
        return ExportJobManagerController.PAGE_URL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getFunctionalityUrl() {
        return ExportJobManagerController.PAGE_URL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BATCH_JOB_TYPE getBatchJobType() {
        return BATCH_JOB_TYPE.EXPORT;
    }
}
