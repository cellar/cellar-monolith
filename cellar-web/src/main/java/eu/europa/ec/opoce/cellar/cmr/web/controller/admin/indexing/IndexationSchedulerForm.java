/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.indexing.form
 *             FILE : IndexationSchedulerForm.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 20 sept. 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2016 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.indexing;

import java.io.Serializable;

/**
 * The Class IndexationSchedulerForm.
 * <class_description> This class represents the indexation scheduler form object.
 * <br/><br/>
 * <notes>
 * <br/><br/>
 * ON : 20 sept. 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class IndexationSchedulerForm implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -7328728978779054745L;

    private SelectionChoice selectionChoice;

    /** The sparql query. */
    private String sparqlQuery;

    /** The period. */
    private String period;

    /** The period unit of time. */
    private String periodUnitOfTime;

    /** The cron expression. */
    private String cronExpression;

    /** The duration. */
    private String duration;

    /** The duration unit of time. */
    private String durationUnitOfTime;

    /** The enabled. */
    private boolean enabled;

    enum SelectionChoice {
        SPARQL, //
        ALL, //
        PARTIAL;
    }

    public SelectionChoice getSelectionChoice() {
        return this.selectionChoice;
    }

    public void setSelectionChoice(final SelectionChoice selectionChoice) {
        this.selectionChoice = selectionChoice;
    }

    /**
     * Checks if is use sparql.
     *
     * @return true, if is use sparql
     */
    public boolean isUseSparql() {
        return SelectionChoice.SPARQL.equals(this.selectionChoice);
    }

    /**
     * Sets the use sparql.
     *
     * @param useSparql the new use sparql
     */
    public void setUseSparql(final boolean useSparql) {
        if (useSparql) {
            setSelectionChoice(SelectionChoice.SPARQL);
        }
    }

    /**
     * Checks if is choose all content.
     *
     * @return true, if is choose all content
     */
    public boolean isChooseAllContent() {
        return SelectionChoice.ALL.equals(this.selectionChoice);
    }

    /**
     * Sets the choose all content.
     *
     * @param chooseAllContent the new choose all content
     */
    public void setChooseAllContent(final boolean chooseAllContent) {
        if (chooseAllContent) {
            setSelectionChoice(SelectionChoice.ALL);
        }
    }

    /**
     * Checks if is choose all content for period.
     *
     * @return true, if is choose all content for period
     */
    public boolean isChooseAllContentForPeriod() {
        return SelectionChoice.PARTIAL.equals(this.selectionChoice);
    }

    /**
     * Sets the choose all content for period.
     *
     * @param chooseAllContentForPeriod the new choose all content for period
     */
    public void setChooseAllContentForPeriod(final boolean chooseAllContentForPeriod) {
        if (chooseAllContentForPeriod) {
            setSelectionChoice(SelectionChoice.PARTIAL);
        }
    }

    /**
     * Gets the period.
     *
     * @return the period
     */
    public String getPeriod() {
        return this.period;
    }

    /**
     * Sets the period.
     *
     * @param period the new period
     */
    public void setPeriod(final String period) {
        this.period = period;
    }

    /**
     * Gets the period unit of time.
     *
     * @return the period unit of time
     */
    public String getPeriodUnitOfTime() {
        return this.periodUnitOfTime;
    }

    /**
     * Sets the period unit of time.
     *
     * @param periodUnitOfTime the new period unit of time
     */
    public void setPeriodUnitOfTime(final String periodUnitOfTime) {
        this.periodUnitOfTime = periodUnitOfTime;
    }

    /**
     * Gets the cron expression.
     *
     * @return the cron expression
     */
    public String getCronExpression() {
        return this.cronExpression;
    }

    /**
     * Sets the cron expression.
     *
     * @param cronExpression the new cron expression
     */
    public void setCronExpression(final String cronExpression) {
        this.cronExpression = cronExpression;
    }

    /**
     * Gets the duration.
     *
     * @return the duration
     */
    public String getDuration() {
        return this.duration;
    }

    /**
     * Sets the duration.
     *
     * @param duration the new duration
     */
    public void setDuration(final String duration) {
        this.duration = duration;
    }

    /**
     * Gets the duration unit of time.
     *
     * @return the duration unit of time
     */
    public String getDurationUnitOfTime() {
        return this.durationUnitOfTime;
    }

    /**
     * Sets the duration unit of time.
     *
     * @param durationUnitOfTime the new duration unit of time
     */
    public void setDurationUnitOfTime(final String durationUnitOfTime) {
        this.durationUnitOfTime = durationUnitOfTime;
    }

    /**
     * Gets the sparql query.
     *
     * @return the sparql query
     */
    public String getSparqlQuery() {
        return this.sparqlQuery;
    }

    /**
     * Sets the sparql query.
     *
     * @param sparqlQuery the new sparql query
     */
    public void setSparqlQuery(final String sparqlQuery) {
        this.sparqlQuery = sparqlQuery;
    }

    /**
     * Checks if is enabled.
     *
     * @return true, if is enabled
     */
    public boolean isEnabled() {
        return this.enabled;
    }

    /**
     * Sets the enabled.
     *
     * @param enabled the new enabled
     */
    public void setEnabled(final boolean enabled) {
        this.enabled = enabled;
    }

}
