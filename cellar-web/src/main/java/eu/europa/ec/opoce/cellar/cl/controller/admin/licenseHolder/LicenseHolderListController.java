/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.licenseHolder
 *             FILE : LicenseHolderListController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Nov 27, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.admin.licenseHolder;

import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Nov 27, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
public class LicenseHolderListController extends LicenseHolderController {

    @RequestMapping(value = "/license", method = RequestMethod.GET)
    public ModelAndView license(final HttpServletRequest request, final HttpServletResponse response) {
        return new ModelAndView(new RedirectView(URL_LIST, true));
    }

    @RequestMapping(value = "/license/list", method = RequestMethod.GET)
    public ModelAndView licenseList(final HttpServletRequest request, final HttpServletResponse response) {
        return this.constructLicenseList(null);
    }

    @RequestMapping(value = "/license/configuration/delete", method = RequestMethod.GET)
    public void configurationDelete(final HttpServletRequest request, final HttpServletResponse response,
            @RequestParam(value = "configurationId", required = true) final Long configurationId,
            @RequestParam(value = "withFinalArchive", required = false) final boolean cleanFinalArchive) throws IOException {
        response.setContentType(CONTENT_TYPE_TEXT);
        try {
            this.licenseHolderService.deleteExtractionConfiguration(configurationId, cleanFinalArchive);
        } catch (Exception exception) {
            response.getOutputStream().write(exception.getMessage().getBytes(StandardCharsets.UTF_8));
        }
        response.getOutputStream().flush();
    }

    @RequestMapping(value = "/license/execution/restartStep", method = RequestMethod.GET)
    public ModelAndView configurationExecutionRestartStep(final HttpServletRequest request, final HttpServletResponse response,
            @RequestParam(value = "executionId", required = true) final Long executionId,
            @RequestParam(value = "configurationId", required = false) final Long configurationId) {
        try {
            this.licenseHolderService.restartStepExtractionExecution(executionId);

            if (configurationId != null) {
                return new ModelAndView(new RedirectView(URL_CONFIGURATION + configurationId, true));
            } else {
                return new ModelAndView(new RedirectView(URL_LIST, true));
            }
        } catch (CellarException exception) {
            LOG.error("Cannot restart step", exception);
            if (configurationId != null) {
                return this.constructConfigurationDetail(configurationId, exception.getMessage());
            } else {
                return this.constructLicenseList(exception.getMessage());
            }
        }
    }

    @RequestMapping(value = "/license/execution/restartStep/all", method = RequestMethod.GET)
    public void configurationExecutionRestartStep(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        response.setContentType(CONTENT_TYPE_TEXT);
        try {
            this.licenseHolderService.restartStepExtractionExecution();
            super.addUserMessage(LEVEL.SUCCESS, "common.message.successfullyProcessed");
        } catch (Exception exception) {
            response.getOutputStream().write(exception.getMessage().getBytes(StandardCharsets.UTF_8));
        }
        response.getOutputStream().flush();
    }

    @RequestMapping(value = "/license/execution/restart", method = RequestMethod.GET)
    public ModelAndView configurationExecutionRestart(final HttpServletRequest request, final HttpServletResponse response,
            @RequestParam(value = "executionId", required = true) final Long executionId,
            @RequestParam(value = "configurationId", required = false) final Long configurationId) {
        try {
            this.licenseHolderService.restartExtractionExecution(executionId);

            if (configurationId != null)
                return new ModelAndView(new RedirectView(URL_CONFIGURATION + configurationId, true));
            else
                return new ModelAndView(new RedirectView(URL_LIST, true));
        } catch (CellarException exception) {
            LOG.error("Cannot restart", exception);
            if (configurationId != null) {
                return this.constructConfigurationDetail(configurationId, exception.getMessage());
            } else {
                return this.constructLicenseList(exception.getMessage());
            }
        }
    }

    @RequestMapping(value = "/license/execution/restart/all", method = RequestMethod.GET)
    public void configurationExecutionRestart(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        response.setContentType(CONTENT_TYPE_TEXT);
        try {
            this.licenseHolderService.restartExtractionExecution();
            super.addUserMessage(LEVEL.SUCCESS, "common.message.successfullyProcessed");
        } catch (Exception exception) {
            response.getOutputStream().write(exception.getMessage().getBytes(StandardCharsets.UTF_8));
        }
        response.getOutputStream().flush();
    }

    private ModelAndView constructLicenseList(final String errorMessage) {
        final ModelAndView modelAndView = new ModelAndView("licenseList");
        if (errorMessage != null)
            modelAndView.addObject("errorMessage", errorMessage);

        modelAndView.addObject("jobConfigurations", this.licenseHolderService.findJobExtractionConfigurations());
        final List<Object> adhocConfigurations = this.licenseHolderService.findAdhocExtractionConfigurations();
        modelAndView.addObject("adhocConfigurations", adhocConfigurations);
        modelAndView.addObject("adhocConfigurationsOptions",
                this.licenseHolderService.getExtractionExecutionsOptions(adhocConfigurations, 0));

        return modelAndView;
    }
}
