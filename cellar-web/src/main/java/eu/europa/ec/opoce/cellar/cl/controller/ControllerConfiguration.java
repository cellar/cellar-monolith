package eu.europa.ec.opoce.cellar.cl.controller;

import com.google.common.collect.Lists;
import eu.europa.ec.opoce.cellar.cl.controller.resource.validator.AcceptMimeTypeValidator;
import eu.europa.ec.opoce.cellar.cl.controller.resource.validator.EmbargoValidator;
import eu.europa.ec.opoce.cellar.cl.controller.resource.validator.client.SimpleContentRequestValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class ControllerConfiguration {

    @Bean
    public List<SimpleContentRequestValidator> requestValidators(AcceptMimeTypeValidator acceptMimeTypeValidator,
                                                                 EmbargoValidator embargoValidator) {
        return Lists.newArrayList(acceptMimeTypeValidator, embargoValidator);
    }
}
