/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.edit
 *             FILE : EditUpdateJobController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 29, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.deleteJob;

import eu.europa.ec.opoce.cellar.cl.controller.admin.error.PageNotFoundController;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob.BATCH_JOB_TYPE;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.BatchJobProcessingService;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.command.CreateJob;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.edit.EditJobController;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.deleteJob.form.DeleteJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;

/**
 * <class_description> Edit update job controller.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 29, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
@RequestMapping(EditDeleteJobController.PAGE_URL)
public class EditDeleteJobController extends EditJobController {

    public static final String PAGE_URL = "/cmr/delete/job/edit";

    private static final String JSP_NAME = "cmr/delete/job/edit-job";

    private static final String OBJECT_NAME = "job";

    /**
     * Validator for a created job.
     */
    @Autowired
    @Qualifier("deleteJobValidator")
    private Validator jobValidator;

    @Autowired(required = true)
    private BatchJobProcessingService batchJobProcessingService;

    /**
     * Handle the GET.
     */
    @RequestMapping(method = RequestMethod.GET)
    public String createJobGet(final ModelMap model, @RequestParam final Long id) {

        final BatchJob batchJob = this.batchJobService.getBatchJob(id, BATCH_JOB_TYPE.DELETE); // gets the batch job

        if (batchJob == null) {
            return PageNotFoundController.ERROR_VIEW;
        }
        final DeleteJob deleteJob = new DeleteJob();

        deleteJob.setId(batchJob.getId());
        deleteJob.setJobName(batchJob.getJobName());
        deleteJob.setSparqlQuery(batchJob.getSparqlQuery());

        model.addAttribute(OBJECT_NAME, deleteJob);

        return JSP_NAME;
    }

    /**
     * Handle the POST.
     */
    @RequestMapping(method = RequestMethod.POST)
    public String createJobPost(final ModelMap model, @ModelAttribute(OBJECT_NAME) final DeleteJob createdJob,
            final BindingResult bindingResult, final SessionStatus status) {

        final String editJob = super.editJob(model, createdJob, bindingResult, status);

        return editJob;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Validator getEditJobValidator() {
        return this.jobValidator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BatchJob constructBatchJob(final CreateJob createJob) {
        final DeleteJob job = (DeleteJob) createJob;
        return BatchJob.create(BATCH_JOB_TYPE.DELETE, job.getJobName(), job.getSparqlQuery())
                .id(job.getId())
                .build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getJspName() {
        return JSP_NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getSuccessPageUrl() {
        return DeleteJobManagerController.PAGE_URL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getFunctionalityUrl() {
        return DeleteJobManagerController.PAGE_URL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BATCH_JOB_TYPE getBatchJobType() {
        return BATCH_JOB_TYPE.DELETE;
    }
}
