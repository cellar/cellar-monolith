/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin
 *        FILE : LogController.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 30-01-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import eu.europa.ec.opoce.cellar.admin.log.LogService;
import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cmr.CellarIdentifierResourceService;

/**
 * <class_description> Controller class for reading logs.
 *
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * ON : 02-08-2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
public class LogController extends AbstractCellarController {

    private static final String URL_LOG_TODAY = "/admin/cmr/log/today";
    private static final String URL_LOG_YESTERDAY = "/admin/cmr/log/yesterday";
    private static final String URL_LOG_THIS_WEEK = "/admin/cmr/log/week";
    private static final String URL_LOG_ALL = "/admin/cmr/log";
    private static final String URL_LOG_FILE = "/admin/cmr/log/logFile";

    @Autowired(required = true)
    LogService logService;

    @Autowired(required = true)
    private CellarIdentifierResourceService cellarIdentifierResourceService;

    @RequestMapping(value = "/cmr/log", method = RequestMethod.GET)
    public ModelAndView getLogModel(HttpServletRequest request, HttpServletResponse response) throws IOException {
        ModelAndView modelAndView = new ModelAndView("log");
        modelAndView.addObject("totalWorks", cellarIdentifierResourceService.getTotalNumberOfWorks());
        modelAndView.addObject("expressionsPerLanguage", cellarIdentifierResourceService.getExpressionsPerLanguage());
        modelAndView.addObject("manifestationsPerType", cellarIdentifierResourceService.getManifestationsPerType());
        modelAndView.addObject("manifestationsPerTypePerLanguage", cellarIdentifierResourceService.getManifestationsPerTypePerLanguage());
        modelAndView.addObject("languagesForTable", cellarIdentifierResourceService.getLanguagesForTable());
        modelAndView.addObject("logs", logService.getLogs());
        modelAndView.addObject("today", URL_LOG_TODAY);
        modelAndView.addObject("yesterday", URL_LOG_YESTERDAY);
        modelAndView.addObject("week", URL_LOG_THIS_WEEK);
        modelAndView.addObject("all", URL_LOG_ALL);
        modelAndView.addObject("logUrl", URL_LOG_FILE);
        modelAndView.addObject("logLevel", logService.getLogLevel());
        return modelAndView;
    }

    @RequestMapping(value = "/cmr/log/today", method = RequestMethod.GET)
    public ModelAndView getLogModelToday(HttpServletRequest request, HttpServletResponse response) throws IOException {
        ModelAndView modelAndView = new ModelAndView("log");
        modelAndView.addObject("totalWorks", cellarIdentifierResourceService.getTotalNumberOfWorks());
        modelAndView.addObject("expressionsPerLanguage", cellarIdentifierResourceService.getExpressionsPerLanguage());
        modelAndView.addObject("manifestationsPerType", cellarIdentifierResourceService.getManifestationsPerType());
        modelAndView.addObject("manifestationsPerTypePerLanguage", cellarIdentifierResourceService.getManifestationsPerTypePerLanguage());
        modelAndView.addObject("languagesForTable", cellarIdentifierResourceService.getLanguagesForTable());
        modelAndView.addObject("logs", logService.getLogsToday());
        modelAndView.addObject("today", URL_LOG_TODAY);
        modelAndView.addObject("yesterday", URL_LOG_YESTERDAY);
        modelAndView.addObject("week", URL_LOG_THIS_WEEK);
        modelAndView.addObject("all", URL_LOG_ALL);
        modelAndView.addObject("logUrl", URL_LOG_FILE);
        modelAndView.addObject("logLevel", logService.getLogLevel());
        return modelAndView;
    }

    @RequestMapping(value = "/cmr/log/yesterday", method = RequestMethod.GET)
    public ModelAndView getLogModelYesterday(HttpServletRequest request, HttpServletResponse response) throws IOException {
        ModelAndView modelAndView = new ModelAndView("log");
        modelAndView.addObject("totalWorks", cellarIdentifierResourceService.getTotalNumberOfWorks());
        modelAndView.addObject("expressionsPerLanguage", cellarIdentifierResourceService.getExpressionsPerLanguage());
        modelAndView.addObject("manifestationsPerType", cellarIdentifierResourceService.getManifestationsPerType());
        modelAndView.addObject("manifestationsPerTypePerLanguage", cellarIdentifierResourceService.getManifestationsPerTypePerLanguage());
        modelAndView.addObject("languagesForTable", cellarIdentifierResourceService.getLanguagesForTable());
        modelAndView.addObject("logs", logService.getLogsYesterday());
        modelAndView.addObject("today", URL_LOG_TODAY);
        modelAndView.addObject("yesterday", URL_LOG_YESTERDAY);
        modelAndView.addObject("week", URL_LOG_THIS_WEEK);
        modelAndView.addObject("all", URL_LOG_ALL);
        modelAndView.addObject("logUrl", URL_LOG_FILE);
        modelAndView.addObject("logLevel", logService.getLogLevel());
        return modelAndView;
    }

    @RequestMapping(value = "/cmr/log/week", method = RequestMethod.GET)
    public ModelAndView getLogModelThisWeek(HttpServletRequest request, HttpServletResponse response) throws IOException {
        ModelAndView modelAndView = new ModelAndView("log");
        modelAndView.addObject("totalWorks", cellarIdentifierResourceService.getTotalNumberOfWorks());
        modelAndView.addObject("expressionsPerLanguage", cellarIdentifierResourceService.getExpressionsPerLanguage());
        modelAndView.addObject("manifestationsPerType", cellarIdentifierResourceService.getManifestationsPerType());
        modelAndView.addObject("manifestationsPerTypePerLanguage", cellarIdentifierResourceService.getManifestationsPerTypePerLanguage());
        modelAndView.addObject("languagesForTable", cellarIdentifierResourceService.getLanguagesForTable());
        modelAndView.addObject("logs", logService.getLogsThisWeek());
        modelAndView.addObject("today", URL_LOG_TODAY);
        modelAndView.addObject("yesterday", URL_LOG_YESTERDAY);
        modelAndView.addObject("week", URL_LOG_THIS_WEEK);
        modelAndView.addObject("all", URL_LOG_ALL);
        modelAndView.addObject("logUrl", URL_LOG_FILE);
        modelAndView.addObject("logLevel", logService.getLogLevel());
        return modelAndView;
    }

    @RequestMapping(value = "/cmr/log/logFile", method = RequestMethod.GET)
    public ModelAndView getLogFile(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "q", required = true) String logs) throws IOException {
        List<String> logList = Arrays.asList(StringUtils.split(logs, ","));
        Map<String, Object> logFiles = new HashMap<String, Object>();
        for (String log : logList) {
            final Object currLog = logService.getLog(log);
            logFiles.put(log, currLog);
        }
        ModelAndView modelAndView = new ModelAndView("logFile");
        modelAndView.addObject("logFiles", logFiles);
        return modelAndView;
    }

    @RequestMapping(value = "/cmr/log/clear", method = RequestMethod.POST)
    public ModelAndView clearLogs(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String date = request.getParameter("date");
        logService.clearLogs(date);
        return new ModelAndView(new RedirectView("/admin/cmr/log", true));
    }

    @RequestMapping(value = "/cmr/log/get", method = RequestMethod.POST)
    public ModelAndView getSpecifiedLogModel(HttpServletRequest request, HttpServletResponse response,
            @RequestParam(value = "q", required = true) String logs) throws IOException {
        List<String> logList = Arrays.asList(StringUtils.split(logs, ","));
        Map<String, Object> logFiles = new HashMap<String, Object>();
        for (String log : logList) {
            final Object currLog = logService.getLog(log);
            logFiles.put(log, currLog);
        }
        ModelAndView modelAndView = new ModelAndView("logFile");
        modelAndView.addObject("logFiles", logFiles);
        return modelAndView;
    }

    @RequestMapping(value = "/cmr/log/setLogLevel", method = RequestMethod.POST)
    public ModelAndView setLogLevel(HttpServletRequest request, HttpServletResponse response) throws IOException {
        logService.setLogLevel(request.getParameter("logLevel"));
        return new ModelAndView(new RedirectView(URL_LOG_ALL, true));
    }

}
