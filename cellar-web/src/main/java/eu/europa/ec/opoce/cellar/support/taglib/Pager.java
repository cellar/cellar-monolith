package eu.europa.ec.opoce.cellar.support.taglib;

import java.util.ArrayList;
import java.util.List;

/**
 * Bean for managing the pagination of the PagerTag
 * 
 * @author omeurice
 * 
 */
public class Pager {

    /**
     * Number of link to be displayed on both side of the current page link.
     */
    public static final int OFFSET = 5;

    /**
     * Keyword for the 'previous' link.
     */
    public static final String PREV_LINK = "PREV";

    /**
     * Keyword for the 'next' link.
     */
    public static final String NEXT_LINK = "NEXT";

    /**
     * Current page number.
     */
    private int currentPage;

    /**
     * Number of the results or lines or whatever per page.
     */
    private int resultsPerPage;

    /**
     * Total number of items (results,...).
     */
    private int totalResults;

    /**
     * Method that has to be called by the PagerTag to build the Pager widget.
     * 
     * @return A <code>List</code> of <code>PagerLink</code> instances containing label and values
     *         for all the pager links.
     */
    public List<PagerLink> createPageLinks() {
        List<PagerLink> pageLinks = new ArrayList<PagerLink>();
        int addToRight = ((currentPage - OFFSET) < 0) ? Math.abs(currentPage - OFFSET) : 0;
        int lastPage = ((currentPage + OFFSET + addToRight) > (totalResults / resultsPerPage)) ? totalResults / resultsPerPage
                : currentPage + OFFSET + addToRight;
        // Add the last page with the rest of the division
        int aRest = totalResults % resultsPerPage > 0 ? 1 : 0;
        if (aRest > 0 && currentPage > lastPage - OFFSET) {
            lastPage = lastPage + 1;
        }
        // If we reached the right limit for the last page, add pages to the left to keep same
        // number of links (2 * OFFSET)
        int addToLeft = currentPage + OFFSET > lastPage ? currentPage + OFFSET - lastPage : 0;
        int firstPage = ((currentPage - OFFSET) < 1) ? 1 : currentPage - OFFSET;
        firstPage = (firstPage - addToLeft <= 0) ? 1 : firstPage - addToLeft;
        if (currentPage > 1) {
            pageLinks.add(new PagerLink(currentPage - 1, PREV_LINK));
        }
        for (int i = firstPage; i <= lastPage; i++) {
            pageLinks.add(new PagerLink(i, Integer.toString(i)));
        }
        if (currentPage < lastPage) {
            pageLinks.add(new PagerLink(currentPage + 1, NEXT_LINK));
        }
        return pageLinks;
    }

    /**
     * Default constructor.
     */
    public Pager() {
        super();
    }

    /**
     * Create a new <code>Pager</code> instance.
     * 
     * @param currentPage
     *            The current page to be displayed.
     * @param resultsPerPage
     *            Number of items per page.
     * @param totalResults
     *            The total number of items.
     * @param uri
     *            Controller URI
     */
    public Pager(final int currentPage, final int resultsPerPage, final int totalResults) {
        super();
        this.currentPage = currentPage;
        this.resultsPerPage = resultsPerPage;
        this.totalResults = totalResults;
    }

    /**
     * Get the current page.
     * 
     * @return The current page.
     */
    public int getCurrentPage() {
        return currentPage;
    }

    /**
     * Set the current page.
     * 
     * @param currentPage
     *            The page number to set.
     */
    public void setCurrentPage(final int currentPage) {
        this.currentPage = currentPage;
    }

    /**
     * Get the number of items per page.
     * 
     * @return The number of items per page.
     */
    public int getResultsPerPage() {
        return resultsPerPage;
    }

    /**
     * Set the number of items per page.
     * 
     * @param resultsPerPage
     *            The number of items per page to be set.
     */
    public void setResultsPerPage(final int resultsPerPage) {
        this.resultsPerPage = resultsPerPage;
    }

    /**
     * Get the total number of items.
     * 
     * @return The total number of items.
     */
    public int getTotalResults() {
        return totalResults;
    }

    /**
     * Set the total number of items.
     * 
     * @param totalResults
     *            The total number of items to set.
     */
    public void setTotalResults(final int totalResults) {
        this.totalResults = totalResults;
    }

}
