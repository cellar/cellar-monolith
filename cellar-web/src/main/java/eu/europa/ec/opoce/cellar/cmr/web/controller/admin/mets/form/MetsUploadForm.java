package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.mets.form;

import org.springframework.web.multipart.MultipartFile;

public class MetsUploadForm {
    private MultipartFile fileData;
    private String priority;
    private String callbackURI;

    /**
     * <p>Getter for the field <code>priority</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getPriority() {
        return priority;
    }

    /**
     * <p>Setter for the field <code>priority</code>.</p>
     *
     * @param priority a {@link java.lang.String} object.
     */
    public void setPriority(String priority) {
        this.priority = priority;
    }

    /**
     * <p>Getter for the field <code>callbackURI</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCallbackURI() {
        return callbackURI;
    }

    /**
     * <p>Setter for the field <code>callbackURI</code>.</p>
     *
     * @param callbackURI a {@link java.lang.String} object.
     */
    public void setCallbackURI(String callbackURI) {
        this.callbackURI = callbackURI;
    }

    /**
     * <p>Getter for the field <code>fileData</code>.</p>
     *
     * @return a {@link org.springframework.web.multipart.MultipartFile} object.
     */
    public MultipartFile getFileData() {
        return fileData;
    }

    /**
     * <p>Setter for the field <code>fileData</code>.</p>
     *
     * @param fileData a {@link org.springframework.web.multipart.MultipartFile} object.
     */
    public void setFileData(MultipartFile fileData) {
        this.fileData = fileData;
    }
}
