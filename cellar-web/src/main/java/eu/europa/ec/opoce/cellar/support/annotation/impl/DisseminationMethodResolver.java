/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.support.annotation.impl
 *             FILE : RequestMethodBingResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 3, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.support.annotation.impl;

import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.DisseminationMethod;
import eu.europa.ec.opoce.cellar.support.annotation.DisseminationMethodBinder;
import org.springframework.core.MethodParameter;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.support.WebArgumentResolver;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.servlet.http.HttpServletRequest;
import java.lang.annotation.Annotation;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 3, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class DisseminationMethodResolver implements HandlerMethodArgumentResolver {

    @Override
    public boolean supportsParameter(MethodParameter methodParameter) {
        return true;
    }

    @Override
    public Object resolveArgument(final MethodParameter param, @Nullable ModelAndViewContainer var2, final NativeWebRequest request, @Nullable WebDataBinderFactory var4) throws Exception {

        final Annotation[] paramAnns = param.getParameterAnnotations();

        for (final Annotation paramAnn : paramAnns) {
            if (DisseminationMethodBinder.class.isInstance(paramAnn)) {
                return DisseminationMethod.valueOf(request.getNativeRequest(HttpServletRequest.class).getMethod().toUpperCase());
            }
        }

        return WebArgumentResolver.UNRESOLVED;
    }
}
