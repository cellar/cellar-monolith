/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.security
 *             FILE : CreateGroupController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 11, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.admin.security;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.admin.security.command.CreateEditGroup;
import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL;
import eu.europa.ec.opoce.cellar.cl.domain.admin.Group;
import eu.europa.ec.opoce.cellar.cl.domain.admin.Role;
import eu.europa.ec.opoce.cellar.cl.service.client.SecurityService;

/**
 * <class_description> Create a new group controller.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 11, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
@RequestMapping(CreateGroupController.PAGE_URL)
public class CreateGroupController extends AbstractCellarController {

    public static final String PAGE_URL = "/security/group/create";

    private static final String JSP_NAME = "security/group/create";

    private static final String OBJECT_NAME = "group";

    /**
     * Security service manager.
     */
    @Autowired
    private SecurityService securityService;

    /**
     * Validator for a created/edited group.
     */
    @Autowired
    @Qualifier("groupValidator")
    private Validator groupValidator;

    /**
     * Returns all the roles.
     * @return all the roles
     */
    @ModelAttribute("rolesList")
    public List<Role> populateRolesList() {
        return this.securityService.findAllRoles();
    }

    /**
     * Handle the GET.
     */
    @RequestMapping(method = RequestMethod.GET)
    public String createGroupGet(final ModelMap model) {
        model.addAttribute(OBJECT_NAME, new CreateEditGroup()); // empty group

        return JSP_NAME;
    }

    /**
     * Handle the POST.
     */
    @RequestMapping(method = RequestMethod.POST)
    public String createGroupPost(@ModelAttribute(OBJECT_NAME) final CreateEditGroup createdGroup, final BindingResult bindingResult,
            final SessionStatus status) {

        this.groupValidator.validate(createdGroup, bindingResult); // validates the created group

        if (bindingResult.hasErrors()) {
            return JSP_NAME;
        }

        final Set<Role> roles = new LinkedHashSet<Role>();
        for (Long id : createdGroup.getRoles()) {
            roles.add(new Role(id));
        }

        final Group group = new Group();
        group.setGroupName(createdGroup.getGroupName());
        group.setRoles(roles);

        this.securityService.createGroup(group); // saves the group

        super.addUserMessage(LEVEL.SUCCESS, "common.message.successfullySaved");
        return redirect(GroupManagerController.PAGE_URL); // redirects to the group manager
    }

}
