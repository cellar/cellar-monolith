/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.error
 *             FILE : PageNotFoundController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 22, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.admin.error;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;

/**
 * <class_description> Page not found controller.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 22, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
@RequestMapping(PageNotFoundController.PAGE_URL)
public class PageNotFoundController extends AbstractCellarController {

    public static final String PAGE_URL = "/security/error/page-not-found";

    public static final String ERROR_VIEW = "page-not-found";

    /**
     * Handle the GET.
     */
    @RequestMapping(method = RequestMethod.GET)
    public String pageNotFoundGet() {
        return ERROR_VIEW;
    }
}
