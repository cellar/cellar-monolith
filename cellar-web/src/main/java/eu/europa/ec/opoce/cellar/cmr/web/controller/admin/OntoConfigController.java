package eu.europa.ec.opoce.cellar.cmr.web.controller.admin;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.domain.ontology.FileListMetadata;
import eu.europa.ec.opoce.cellar.cl.domain.ontology.FileMetadata;
import eu.europa.ec.opoce.cellar.ontology.exception.IllegalOntologyFileNameException;
import eu.europa.ec.opoce.cellar.ontology.exception.OntologyException;
import eu.europa.ec.opoce.cellar.ontology.service.OntologyService;
import eu.europa.ec.opoce.cellar.server.admin.nal.OntoAdminConfigurationService;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL.ERROR;
import static eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL.SUCCESS;

/**
 * The Class OntoConfigController.
 */
@Controller
public class OntoConfigController extends AbstractCellarController {

    private static final Logger LOG = LogManager.getLogger(OntoConfigController.class);

    private final OntologyService ontologyService;

    /**
     * The onto admin configuration service.
     */
    private final OntoAdminConfigurationService ontoAdminConfigurationService;

    @Autowired
    public OntoConfigController(OntoAdminConfigurationService ontoAdminConfigurationService, OntologyService ontologyService) {
        this.ontoAdminConfigurationService = ontoAdminConfigurationService;
        this.ontologyService = ontologyService;
    }

    /**
     * Gets the resource config model.
     *
     * @param request  the request
     * @param response the response
     * @return the resource config model
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @RequestMapping(value = "/cmr/onto", method = RequestMethod.GET)
    public ModelAndView getResourceConfigModel(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        final ModelAndView modelAndView = new ModelAndView("ontoConfiguration");
        modelAndView.addObject("versions", this.ontoAdminConfigurationService.getGroupedOntologyVersions());
        return modelAndView;
    }

    /**
     * Upload ontology model.
     *
     * @param request  the request
     * @param response the response
     * @return the file list metadata
     */
    @RequestMapping(value = "/cmr/onto/uploadOntologyModel", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public FileListMetadata uploadOntologyModel(MultipartHttpServletRequest request, HttpServletResponse response) {
        List<File> itemFiles = Collections.emptyList();
        try {
            itemFiles = request.getFiles("files[]").stream()
                    .map(this::toFile)
                    .collect(Collectors.toList());
            try {
                ontologyService.update(itemFiles,null);
                addUserMessage(SUCCESS, "ontologies.view.success.message", "The upload of the files has finished " +
                        "successfully.Please check on the logs that the background process is performing correctly.");
                return response(itemFiles);
            } catch (IllegalOntologyFileNameException e) {
                LOG.catching(e);
                addUserMessage(ERROR, "ontologies.view.error.message.file.format");
            } catch (OntologyException e) {
                addUserMessage(ERROR, "ontologies.view.error.message");
            }
        } catch (UncheckedIOException e) {
            LOG.error("Unexpected exception was thrown", e);
        } finally {
            itemFiles.forEach(FileUtils::deleteQuietly);
        }
        return new FileListMetadata();
    }

    private FileListMetadata response(List<File> itemFiles) {
        return new FileListMetadata(itemFiles.stream()
                .map(f -> new FileMetadata(f.getName(), f.length()))
                .collect(Collectors.toList()));
    }

    private File toFile(final MultipartFile multipart) throws IllegalStateException {
        final File tmpFolder = new File(cellarConfiguration.getCellarFolderTemporaryWork());
        final File tmpFile = new File(tmpFolder, multipart.getOriginalFilename());
        try {
            if (tmpFile.createNewFile()) {
                multipart.transferTo(tmpFile);
            } else {
                throw new IOException("Cannot create file " + tmpFile);
            }
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
        return tmpFile;
    }

}
