/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob
 *             FILE : CreateJobController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 11, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.support.SessionStatus;

import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.common.BatchJobController;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.command.CreateJob;

/**
 * <class_description> Create batch job controller.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 11, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
public abstract class CreateJobController extends BatchJobController {

    /**
     * Gets the success path url corresponding to the controller.
     * @return the success path url corresponding to the controller
     */
    protected abstract String getSuccessPageUrl();

    /**
     * Gets the batch job validator corresponding to the controller.
     * @return the batch job validator corresponding to the controller
     */
    protected abstract Validator getCreateJobValidator();

    /**
     * Constructs the batch job based on a command instance.
     * @param createJob the command to import
     * @return the batch job
     */
    protected abstract BatchJob constructBatchJob(final CreateJob createJob);

    /**
     * Create job generic logic:
     *  - Validate job
     *  - Construct batch job
     *  - Save the batch job
     *  - Add a success message and redirect
     * @param model the model of the corresponding view
     * @param createdJob the command
     * @param bindingResult the bind result of the command
     * @param status the session status
     * @return the JSP name or the redirect value
     */
    protected String createJob(final ModelMap model, final CreateJob createdJob, final BindingResult bindingResult,
            final SessionStatus status) {

        this.getCreateJobValidator().validate(createdJob, bindingResult); // validates the created group

        if (bindingResult.hasErrors()) {
            return this.getJspName();
        }

        final BatchJob batchJob = this.constructBatchJob(createdJob);

        this.batchJobService.addBatchJob(batchJob);

        super.addUserMessage(LEVEL.SUCCESS, "common.message.successfullySaved");
        return redirect(this.getSuccessPageUrl());
    }
}
