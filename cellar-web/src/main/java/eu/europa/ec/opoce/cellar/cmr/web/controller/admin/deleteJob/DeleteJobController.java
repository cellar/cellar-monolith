package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.deleteJob;

import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.CreateJobController;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.command.CreateJob;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.deleteJob.form.DeleteJob;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.support.SessionStatus;

import static eu.europa.ec.opoce.cellar.cmr.web.controller.admin.deleteJob.DeleteJobController.PAGE_URL;

@Controller
@RequestMapping(PAGE_URL)
public class DeleteJobController extends CreateJobController {

        public static final String PAGE_URL = "/cmr/delete/job/create";

        private static final String JSP_NAME = "cmr/delete/job/create-job";
        private static final String DELETE_JOB = "delete_job";


        @Autowired
        @Qualifier("deleteJobValidator")
        private Validator jobValidator;

        @GetMapping
        public String createDeleteJob(final ModelMap model) {
                model.addAttribute(DELETE_JOB, new DeleteJob()); // empty job
                return JSP_NAME;
        }

        @PostMapping
        public String createJobPost(final ModelMap model, @ModelAttribute(DELETE_JOB) final DeleteJob createdJob,
                                    final BindingResult bindingResult, final SessionStatus status) {

                return super.createJob(model, createdJob, bindingResult, status);
        }

        @Override
        protected String getFunctionalityUrl() {
                return DeleteJobManagerController.PAGE_URL;
        }

        @Override
        protected BatchJob.BATCH_JOB_TYPE getBatchJobType() {
                return BatchJob.BATCH_JOB_TYPE.DELETE;
        }

        @Override
        protected String getJspName() {
                return JSP_NAME;
        }

        @Override
        protected String getSuccessPageUrl() {
                return DeleteJobManagerController.PAGE_URL;
        }

        @Override
        protected Validator getCreateJobValidator() {
                return jobValidator;
        }

        @Override
        protected BatchJob constructBatchJob(CreateJob createJob) {
                final DeleteJob job = (DeleteJob) createJob;
                return BatchJob.create(BatchJob.BATCH_JOB_TYPE.DELETE, job.getJobName(), job.getSparqlQuery()).build();
        }
}
