package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.items.form;

import com.google.common.base.MoreObjects;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ARHS Developments
 */
public class FilenameForm {

    private List<Filename> filenames;

    public FilenameForm() {
        this(new ArrayList<>());
    }

    public FilenameForm(final List<Filename> filenames) {
        this.filenames = filenames;
    }

    public List<Filename> getFilenames() {
        return filenames;
    }

    public void setFilenames(final List<Filename> filenames) {
        this.filenames = filenames;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("filenames", filenames)
                .toString();
    }
}
