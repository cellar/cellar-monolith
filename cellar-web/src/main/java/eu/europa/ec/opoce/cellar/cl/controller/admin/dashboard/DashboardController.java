package eu.europa.ec.opoce.cellar.cl.controller.admin.dashboard;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.admin.fixity.FixityController;
import eu.europa.ec.opoce.cellar.cl.controller.util.ControllerUtil;
import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService;
import eu.europa.ec.opoce.cellar.cmr.CellarIdentifierResourceService;
import eu.europa.ec.opoce.cellar.ingestion.service.QueueWatcher;
import eu.europa.ec.opoce.cellar.server.admin.nal.OntoAdminConfigurationService;
import eu.europa.ec.opoce.cellar.support.FileExtensionUtils;
import eu.europa.ec.opoce.cellar.virtuosobackup.service.IVirtuosoBackupService;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Controller for displaying the dashboard.
 *
 * @author omeurice
 *
 */
@Controller(value = "dashboardController")
public class DashboardController extends AbstractCellarController {
    @Autowired
    private IIndexingService indexingService;

    /** The cellar identifier resource service. */
    @Autowired
    private CellarIdentifierResourceService cellarIdentifierResourceService;

    @Autowired
    private IVirtuosoBackupService virtuosoBackupService;

    /** The sip watcher. */
    @Autowired
    private QueueWatcher queueWatcher;

    /** The onto admin configuration service. */
    @Autowired
    private OntoAdminConfigurationService ontoAdminConfigurationService;

    @Autowired
    private FixityController fixityController;

    /**
     * Display the dashboard page.
     *
     * @param request            Http servlet request
     * @param response            Http servlet response
     * @return dashboard view with model data.
     * @throws IOException             May be raised by ControllerUtil.getJarVersions().
     */
    @RequestMapping(method = RequestMethod.GET, value = "/dashboard")
    public ModelAndView display(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        final Map<String, Object> model = new HashMap<String, Object>();
        model.put("authenticOJ", this.getNumberInAuthenticOJQueue());
        model.put("daily", this.getNumberInDailyQueue());
        model.put("bulk", this.getNumberInBulkQueue());
        model.put("bulkLowPriority", this.getNumberInBulkLowPriorityQueue());
        model.put("indexingQueue", this.indexingService.getCountIndexRequestsToHandle());
        model.put("failedIndexingRequest", this.indexingService.getCountFailedIndexRequests());
        model.put("minimalLevel", this.indexingService.getMinimalLevelOfPriorityHandled());
        model.put("packages", ControllerUtil.getJarVersions());
        model.put("indexStatus", this.indexingService.getIndexingStatus());
        model.put("ingestionStatus", this.queueWatcher.isIngestionTreatmentSetEmpty() ? "Not ingesting" : "Ingesting");
        model.put("statusChecksums", this.fixityController.getNumberOfInvalidResult());
        model.put("numberEntriesVirtuosoBackupTable", this.virtuosoBackupService.countAllResyncable());
        model.put("ontologies", this.ontoAdminConfigurationService.getGroupedOntologyVersions());
        return new ModelAndView("dashboard", model);
    }

    /**
     * Display the stats about the digital objects. This is intended to be load asynchronously because of the computation time required to display the information.
     * @param request Http servlet request
     * @param response Http servlet response
     * @return dashboard view with model data.
     */
    @RequestMapping(method = RequestMethod.GET, value = "/dashboard-stats")
    public ModelAndView displayStats(final HttpServletRequest request, final HttpServletResponse response) {
        final Map<String, Object> model = new HashMap<String, Object>();
        model.put("languagesForTable", this.cellarIdentifierResourceService.getLanguagesForTable());
        model.put("totalWorks", this.cellarIdentifierResourceService.getTotalNumberOfWorks());
        model.put("expressionsPerLanguage", this.cellarIdentifierResourceService.getExpressionsPerLanguage());
        model.put("manifestationsPerType", this.cellarIdentifierResourceService.getManifestationsPerType());
        model.put("manifestationsPerTypePerLanguage", this.cellarIdentifierResourceService.getManifestationsPerTypePerLanguage());
        model.put("totalDossiers", this.cellarIdentifierResourceService.getTotalNumberOfDossiers());
        model.put("totalEvents", this.cellarIdentifierResourceService.getTotalNumberOfEvents());
        model.put("totalAgents", this.cellarIdentifierResourceService.getTotalNumberOfAgents());
        model.put("totalTopLevelEvents", this.cellarIdentifierResourceService.getTotalNumberOfTopLEvelEvents());
        return new ModelAndView("dashboard-stats", model);
    }

    /**
     * Get the number of files in the authenticOJ queue.
     *
     * @return Number of files in authenticOJ queue.
     */
    private int getNumberInAuthenticOJQueue() {
        final File authenticOJ = new File(this.cellarConfiguration.getCellarFolderAuthenticOJReception());
        final String[] files = authenticOJ.list(new SuffixFileFilter(FileExtensionUtils.SIP));
        if (files != null) {
            return files.length;
        } else {
            return 0;
        }
    }

    /**
     * Get the number of files in the daily queue.
     *
     * @return Number of files in daily queue.
     */
    private int getNumberInDailyQueue() {
        final File daily = new File(this.cellarConfiguration.getCellarFolderDailyReception());
        final String[] files = daily.list(new SuffixFileFilter(FileExtensionUtils.SIP));
        if (files != null) {
            return files.length;
        } else {
            return 0;
        }
    }

    /**
     * Get the number of files in the bulk queue.
     *
     * @return Number of files in the bulk queue.
     */
    private int getNumberInBulkQueue() {
        final File bulk = new File(this.cellarConfiguration.getCellarFolderBulkReception());
        final String[] files = bulk.list(new SuffixFileFilter(FileExtensionUtils.SIP));
        if (files != null) {
            return files.length;
        } else {
            return 0;
        }
    }
    
    /**
     * Get the number of files in the low priority bulk queue.
     *
     * @return Number of files in the low priority bulk queue.
     */
    private int getNumberInBulkLowPriorityQueue() {
        final File bulk = new File(this.cellarConfiguration.getCellarFolderBulkLowPriorityReception());
        final String[] files = bulk.list(new SuffixFileFilter(FileExtensionUtils.SIP));
        if (files != null) {
            return files.length;
        } else {
            return 0;
        }
    }
    
}
