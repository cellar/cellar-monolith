/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob
 *             FILE : CreateReindexJobController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 11, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create;

import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob.BATCH_JOB_TYPE;
import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService.Priority;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.command.CreateJob;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.command.CreateReindexJob;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.manager.ReindexJobManagerController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;

import java.util.List;

/**
 * <class_description> Create reindex batch job controller.
 *
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 11, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
@RequestMapping(CreateReindexJobController.PAGE_URL)
public class CreateReindexJobController extends CreateJobController {

    public static final String PAGE_URL = "/cmr/indexing/reindex/create";

    private static final String JSP_NAME = "cmr/indexing/reindex/create-job";

    private static final String OBJECT_NAME = "job";

    /**
     * Validator for a created job.
     */
    @Autowired
    @Qualifier("reindexJobValidator")
    private Validator jobValidator;

    /**
     * Returns all the priorities.
     * @return all the priorities
     */
    @ModelAttribute("prioritiesList")
    public List<Priority> populateGroupsList() {
        return this.batchJobService.getPriorities();
    }

    /**
     * Handle the GET.
     */
    @RequestMapping(method = RequestMethod.GET)
    public String createJobGet(final ModelMap model) {

        model.addAttribute(OBJECT_NAME, new CreateReindexJob()); // empty job

        return JSP_NAME;
    }

    /**
     * Handle the POST.
     */
    @RequestMapping(method = RequestMethod.POST)
    public String createJobPost(final ModelMap model, @ModelAttribute(OBJECT_NAME) final CreateReindexJob createdJob,
            final BindingResult bindingResult, final SessionStatus status) {

        return super.createJob(model, createdJob, bindingResult, status);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Validator getCreateJobValidator() {
        return this.jobValidator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BatchJob constructBatchJob(final CreateJob createJob) {
        final CreateReindexJob createReindexJob = (CreateReindexJob) createJob;
        return BatchJob.create(BATCH_JOB_TYPE.REINDEX, createReindexJob.getJobName(), createReindexJob.getSparqlQuery())
                .indexingPriority(Priority.findByPriorityValue(createReindexJob.getPriorityId()))
                .generateIndexNotice(createReindexJob.isGenerateIndexNotice())
                .generateEmbeddedNotice(createReindexJob.isGenerateEmbeddedNotice())
                .build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getJspName() {
        return JSP_NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getSuccessPageUrl() {
        return ReindexJobManagerController.PAGE_URL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getFunctionalityUrl() {
        return ReindexJobManagerController.PAGE_URL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BATCH_JOB_TYPE getBatchJobType() {
        return BATCH_JOB_TYPE.REINDEX;
    }
}
