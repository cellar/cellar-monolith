/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.support.datatable
 *             FILE : MisalignmentsDatatableMapper.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 07-Mar-2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.support.datatable;

import eu.europa.ec.opoce.cellar.cl.configuration.enums.CellarServiceRDFStoreCleanerMode;
import eu.europa.ec.opoce.cellar.cl.enums.aligner.Repository;
import eu.europa.ec.opoce.cellar.cl.service.datatable.AbstractJdbcDatatableMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.RowMapper;

/**
 * The Class MisalignmentsDatatableMapper.
 *
 * @author ARHS Developments
 * @version $$Revision$$$
 */
public class MisalignmentsDatatableMapper extends AbstractJdbcDatatableMapper {

    private static final String TABLE_NAME = "ALIGNER_MISALIGNMENT";

    /** The Constant COLUMNS. */
    private static final String[] COLUMNS = new String[] {
            "cellarId", "operationDate", "operationType", "repository", "cellarId"};

    /** The Constant FILTERED_ROWS. */
    private static final Boolean[] FILTERED_ROWS = new Boolean[] {
            Boolean.TRUE, Boolean.FALSE, Boolean.TRUE, Boolean.TRUE, Boolean.FALSE};

    /** {@inheritDoc} */
    @Override
    protected String[] getRowNames() {
        return COLUMNS;
    }

    /** {@inheritDoc} */
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    /** {@inheritDoc} */
    @Override
    protected String getRowName(String columnId) {
        return DB_MAPPING.valueOf(columnId).getColumnName();
    }

    enum DB_MAPPING {
        cellarId("CELLAR_ID"), operationDate("OPERATION_DATE"), operationType("OPERATION_TYPE"), repository("REPOSITORY"), id("ID");

        private final String columnName;

        private DB_MAPPING(String columnName) {
            this.columnName = columnName;
        }

        /**
         * @return the columnName
         */
        public String getColumnName() {
            return columnName;
        }
    }

    /** {@inheritDoc} */
    @Override
    protected RowMapper<String[]> getRowMapper() {
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final RowMapper<String[]> result = new RowMapper<String[]>() {

            @Override
            public String[] mapRow(ResultSet rs, int rowNum) throws SQLException {
                final String[] alignerMisalignmentRow = new String[] {
                        rs.getString(1), simpleDateFormat.format(rs.getDate(2)),
                        CellarServiceRDFStoreCleanerMode.values()[rs.getInt(3)].toString(), Repository.values()[rs.getInt(4)].toString(),
                        rs.getString(5)};
                return alignerMisalignmentRow;
            }
        };
        return result;
    }

    /** {@inheritDoc} */
    @Override
    protected Boolean[] getFilteredRows() {
        return FILTERED_ROWS;
    }

    /** {@inheritDoc} */
    @Override
    protected Object getTransformedValue(final int columnIndex, final String columnValue) {
        Object result = null;
        if (StringUtils.isNotBlank(columnValue)) {
            switch (columnIndex) {
            case 0:
                result = "%" + columnValue + "%";
                break;
            case 2:
                result = CellarServiceRDFStoreCleanerMode.valueOf(columnValue).ordinal();
                break;
            case 3:
                result = Repository.valueOf(columnValue).ordinal();
                break;
            default:
                break;
            }
        }
        return result;
    }

    /** {@inheritDoc} */
    @Override
    protected String getComparisonType(final int columnIndex, final String columnValue) {
        ComparisonType result = ComparisonType.EQUALS;
        if (StringUtils.isNotBlank(columnValue)) {
            switch (columnIndex) {
            case 0:
            case 5:
                result = ComparisonType.LIKE;
                break;
            default:
                break;
            }
        }
        return result.getSqlStatement();
    }

}
