/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.editor.dissemination
 *             FILE : InferenceVarianceEditor.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 14, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.support.dissemination.editor;

import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.InferenceVariance;
import eu.europa.ec.opoce.cellar.server.service.DisseminationException;

import java.beans.PropertyEditorSupport;

import org.springframework.http.HttpStatus;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Apr 14, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class InferenceVarianceEditor extends PropertyEditorSupport {

    @Override
    public void setAsText(final String text) {

        final InferenceVariance inferenceVariance = InferenceVariance.parseInferenceVariance(text);
        this.setValue(inferenceVariance);
        if (inferenceVariance == null) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage("Illegal Inference Variance type: {}").withMessageArgs(text).build();
        }
    }
}
