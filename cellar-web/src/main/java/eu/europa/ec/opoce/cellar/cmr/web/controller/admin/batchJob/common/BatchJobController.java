/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob
 *             FILE : BatchJobController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 12, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.common;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob.BATCH_JOB_TYPE;
import eu.europa.ec.opoce.cellar.cl.scheduling.BatchJobScheduler;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.BatchJobService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 * <class_description> Batch job super controller.
 *
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 12, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
public abstract class BatchJobController extends AbstractCellarController {

    /** Batch job service. */
    @Autowired
    protected BatchJobService batchJobService;

    /** The batch job scheduler. */
    @Autowired
    protected BatchJobScheduler batchJobScheduler;

    /**
     * Gets the functionality url of the batch job specialization.
     * @return the functionality url of the batch job specialization
     */
    protected abstract String getFunctionalityUrl();

    /**
     * Gets the batch job type of the batch job specialization.
     * @return the batch job type of the batch job specialization
     */
    protected abstract BATCH_JOB_TYPE getBatchJobType();

    /**
     * Gets the JSP name of the batch job functionality.
     * @return the JSP name of the batch job functionality
     */
    protected abstract String getJspName();

    /**
     * Returns functionality url.
     * @return functionality url
     */
    @ModelAttribute("functionalityUrl")
    public String populateFunctionalityUrl() {
        return super.prefixedUrl(this.getFunctionalityUrl());
    }

    /**
     * Returns batch job type.
     * @return batch job type
     */
    @ModelAttribute("batchJobType")
    public BATCH_JOB_TYPE populateBatchJobType() {
        return this.getBatchJobType();
    }
}
