/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.indexing
 *             FILE : IndexingSecapiController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 29, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.indexing;

import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService;
import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService.LinkedIndexingBehavior;
import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService.Priority;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestGenerationService;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.indexing.command.EditConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.nio.charset.StandardCharsets;

import static javax.ws.rs.core.HttpHeaders.CONTENT_TYPE;

/**
 * <class_description> Indexing SECAPI controller.
 * <p>
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 29, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
public class IndexingSecapiController {

    /**
     * Indexing service.
     */
    @Autowired
    private IIndexingService indexingService;

    @Autowired
    private CmrIndexRequestGenerationService cmrIndexRequestGenerationService;

    /**
     * Handle isIndexing GET.
     */
    @RequestMapping(value = "/secapi/cmr/indexing/isIndexing", method = RequestMethod.GET)
    public ResponseEntity<String> isIndexingGet() {
        // check if the Cellar is currently indexing
        final Boolean isIndexing = this.indexingService.isIndexingRunning();

        // returns the response
        final HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(new MediaType("text","plain", StandardCharsets.UTF_8));
        return new ResponseEntity<String>(isIndexing.toString(), responseHeaders, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/secapi/cmr/indexing/configuration")
    public ResponseEntity<String> updateConfiguration(
            @RequestParam(value = "indexEnabled") boolean indexEnabled,
            @RequestParam(value = "minPriority") int minPriority,
            @RequestParam(value = "linkedBehavior") int linkedBehavior) {
        final HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add(CONTENT_TYPE, MediaType.TEXT_PLAIN + ";charset=UTF-8");
        EditConfiguration editConfiguration = new EditConfiguration();

        boolean validArguments = validateEditConfigurationArgs(minPriority, linkedBehavior);
        if (!validArguments)
            return new ResponseEntity<>(editConfiguration.toString(), responseHeaders, HttpStatus.BAD_REQUEST);

        editConfiguration.setIndexingEnabled(indexEnabled);
        editConfiguration.setMinimalLevelOfPriorityHandled(Priority.findByPriorityValue(minPriority));
        editConfiguration.setLinkedIndexingBehavior(LinkedIndexingBehavior.findByPriorityValue(linkedBehavior));

        updateConfig(editConfiguration);
        return new ResponseEntity<>(editConfiguration.toString(), responseHeaders, HttpStatus.OK);
    }

    private boolean validateEditConfigurationArgs(final int minPriority, final int linkedBehavior) {
        Priority priority = Priority.findByPriorityValue(minPriority);
        if (priority == null)
            return false;

        LinkedIndexingBehavior linkedIndexingBehavior = LinkedIndexingBehavior.findByPriorityValue(linkedBehavior);
        if (linkedIndexingBehavior == null)
            return false;

        return true;
    }

    private void updateConfig(EditConfiguration editedConfiguration) {
        // update only if the new value is different
        if (this.indexingService.isIndexingEnabled() != editedConfiguration.isIndexingEnabled()) {
            this.indexingService.setIndexingEnabled(editedConfiguration.isIndexingEnabled());
        }

        if (this.cmrIndexRequestGenerationService.getLinkedIndexingBehavior() != editedConfiguration.getLinkedIndexingBehavior()) {
            this.cmrIndexRequestGenerationService.setLinkedIndexingBehavior(editedConfiguration.getLinkedIndexingBehavior());
        }

        if (this.indexingService.getMinimalLevelOfPriorityHandled() != editedConfiguration.getMinimalLevelOfPriorityHandled()) {
            this.indexingService.setMinimalLevelOfPriorityHandled(editedConfiguration.getMinimalLevelOfPriorityHandled());
        }
    }
}
