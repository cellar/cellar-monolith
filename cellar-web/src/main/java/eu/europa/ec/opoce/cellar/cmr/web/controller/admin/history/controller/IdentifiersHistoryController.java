/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : CellarIdentifier.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 9 mars 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.history.controller;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.domain.datatable.DataTableBodyResponse;
import eu.europa.ec.opoce.cellar.cl.domain.history.IdentifiersHistory;
import eu.europa.ec.opoce.cellar.cl.domain.history.service.IdentifiersHistoryService;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.history.datatable.IdentifiersHistoryDatatableMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * The Class IdentifiersHistoryController.
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 9 mars 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
public class IdentifiersHistoryController extends AbstractCellarController {

    /** The Constant IDENTIFIERS_HISTORY_DATATABLE_MAPPER. */
    private static final IdentifiersHistoryDatatableMapper IDENTIFIERS_HISTORY_DATATABLE_MAPPER = new IdentifiersHistoryDatatableMapper();

    /** The identifiers history service. */
    @Autowired
    private IdentifiersHistoryService identifiersHistoryService;

    /**
     * Gets the identifiers history records.
     *
     * @param model the model
     * @return the identifiers history records
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @RequestMapping(method = RequestMethod.GET, value = "/history")
    public String getIdentifiersHistoryRecords(final ModelMap model) throws IOException {
        model.addAttribute("identifiersHistory", new ArrayList<IdentifiersHistory>());
        return "history";
    }

    /**
     * Delete identifiers history record.
     *
     * @param request the request
     * @param response the response
     * @param id the id
     * @return the model and view
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @RequestMapping(method = RequestMethod.GET, value = "/history/delete/{id}")
    public ModelAndView deleteIdentifiersHistoryRecord(final HttpServletRequest request, final HttpServletResponse response,
            @PathVariable(value = "id") final Long id) throws IOException {
        identifiersHistoryService.delete(id);
        final RedirectView view = new RedirectView("/admin/history", true);
        final ModelAndView modelAndView = new ModelAndView(view);
        return modelAndView;
    }

    /**
     * Ajax call to get identifiers history.
     *
     * @param model the model
     * @param httpServletRequest the http servlet request
     * @return the string
     */
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/history/ajax", method = RequestMethod.GET)
    @ResponseBody
    public String ajaxCallToGetIdentifiersHistory(final ModelMap model, final HttpServletRequest httpServletRequest) {
        final Map<String, String[]> parametersMap = httpServletRequest.getParameterMap();
        final Gson gson = new GsonBuilder().create();
        final DataTableBodyResponse<String[]> dataTableObject = IDENTIFIERS_HISTORY_DATATABLE_MAPPER
                .getDataTableBodyResponse(parametersMap);
        final String json = gson.toJson(dataTableObject);
        final String result = json;
        return result;
    }

}
