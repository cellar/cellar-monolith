/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.licenseHolder
 *             FILE : LicenseHolderController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 12, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.admin.licenseHolder;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.service.client.LicenseHolderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 12, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
public class LicenseHolderController extends AbstractCellarController {

    protected final static Logger LOG = LogManager.getLogger(LicenseHolderController.class);

    protected final static String URL_LIST = "/admin/license/list";
    protected final static String URL_CONFIGURATION = "/admin/license/configuration?configurationId=";

    protected final static String CONTENT_TYPE_TEXT = "text/plain";

    @Autowired(required = true)
    protected LicenseHolderService licenseHolderService;

    protected ModelAndView constructConfigurationDetail(final Long configurationId, final String errorMessage) {
        final ModelAndView modelAndView = new ModelAndView("configurationDetail");

        if (errorMessage != null)
            modelAndView.addObject("errorMessage", errorMessage);

        modelAndView.addObject("configuration", this.licenseHolderService.findExtractionConfiguration(configurationId));

        final List<Object> executionsNumberOfDocuments = this.licenseHolderService
                .findExtractionExecutionsNumberOfDocuments(configurationId);
        modelAndView.addObject("executions", executionsNumberOfDocuments);
        modelAndView.addObject("executionsOptions",
                this.licenseHolderService.getExtractionExecutionsOptions(executionsNumberOfDocuments, 0));
        return modelAndView;
    }
}
