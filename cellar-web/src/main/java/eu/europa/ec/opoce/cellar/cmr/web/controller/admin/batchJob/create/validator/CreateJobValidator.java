/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.validator
 *             FILE : CreateJobValidator.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 11, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.command.CreateJob;

/**
 * <class_description> Batch job command validator.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 11, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class CreateJobValidator implements Validator {

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean supports(final Class<?> clazz) {
        return CreateJob.class.isAssignableFrom(clazz);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validate(final Object target, final Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "jobName", "required.batch-job.job.jobName", "A job name is required.");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "sparqlQuery", "required.batch-job.job.sparqlQuery",
                "A SPARQL query is required.");
    }
}
