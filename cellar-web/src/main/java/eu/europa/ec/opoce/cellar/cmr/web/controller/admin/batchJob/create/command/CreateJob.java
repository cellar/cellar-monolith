/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.command
 *             FILE : CreateJob.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 11, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.command;

/**
 * <class_description> Create batch job command.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 11, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public abstract class CreateJob {

    /**
     * The identifier.
     */
    private Long id;

    /**
     * The name of the job.
     */
    protected String jobName;

    /**
     * The SPARQL query of the job.
     */
    protected String sparqlQuery;

    /**
     * Default constructor.
     */
    protected CreateJob() {

    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the jobName
     */
    public String getJobName() {
        return this.jobName;
    }

    /**
     * @param jobName the jobName to set
     */
    public void setJobName(final String jobName) {
        this.jobName = jobName;
    }

    /**
     * @return the sparqlQuery
     */
    public String getSparqlQuery() {
        return this.sparqlQuery;
    }

    /**
     * @param sparqlQuery the sparqlQuery to set
     */
    public void setSparqlQuery(final String sparqlQuery) {
        this.sparqlQuery = sparqlQuery;
    }
}
