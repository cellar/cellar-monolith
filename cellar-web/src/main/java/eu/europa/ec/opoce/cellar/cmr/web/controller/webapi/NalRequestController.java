package eu.europa.ec.opoce.cellar.cmr.web.controller.webapi;

import com.google.common.annotations.VisibleForTesting;
import com.google.gson.Gson;
import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.common.util.IOUtils;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.service.NalConfigurationService;
import eu.europa.ec.opoce.cellar.semantic.helper.IOHelper;
import eu.europa.ec.opoce.cellar.server.service.DisseminationException;
import org.apache.jena.riot.RDFFormat;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.List;

import static javax.ws.rs.core.HttpHeaders.CONTENT_TYPE;

@Controller
public class NalRequestController extends AbstractCellarController {
    private final static Logger LOG = LogManager.getLogger(NalRequestController.class);

    private NalConfigurationService nalConfigurationService;

    @Autowired
    public NalRequestController(NalConfigurationService nalConfigurationService) {
        this.nalConfigurationService = nalConfigurationService;
    }

    @RequestMapping(value = "/nal/list", method = RequestMethod.GET)
    public void getAllNalUri(HttpServletResponse response) {
        LOG.info("Retrieving the list of all NAL configured in CELLAR...");
        List<String> nalConfigList = nalConfigurationService.getAllNalUriList();
        LOG.info("Found '{}' NAL configured in CELLAR.", nalConfigList.size());

        try {
            String json = new Gson().toJson(nalConfigList);
            response.getWriter().write(json);
            response.setStatus(HttpStatus.OK.value());
            response.setHeader(CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);
        } catch (IOException ioe) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class)
                    .withHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                    .withMessage("The list of all NAL URI in the system could not be written to the response : {}")
                    .withMessageArgs(ioe.getMessage())
                    .build();
        }
    }

    @RequestMapping(value = "/nal/get", method = RequestMethod.GET)
    public void getNal(@RequestParam("nalUri") String nalUri, @RequestParam(value = "format", required = false) String format,
                       HttpServletResponse response) {
        LOG.info("Starting the process of retrieving '{}' from CELLAR.", nalUri);

        String nal = "";
        try {
            nal = nalConfigurationService.getNal(nalUri, false, resolveFormat(format));
        } catch (CellarException ce) {
            LOG.info("The NAL '{}' could not be found in the system.", nalUri);
            try {
                String errorMessage = "The NAL '" + nalUri + "' could not be found in the system.";
                ServletOutputStream servletOutputStream = response.getOutputStream();
                IOUtils.copy(new ByteArrayInputStream(errorMessage.getBytes(StandardCharsets.UTF_8)), servletOutputStream);
                IOHelper.flushAndClose(servletOutputStream);
                response.setStatus(HttpStatus.BAD_REQUEST.value());
                response.setHeader(CONTENT_TYPE, MediaType.TEXT_PLAIN_VALUE);
            } catch (IOException ioe) {
                LOG.error("Could not write the error for NAL '{}' in the response : {}", nalUri, ioe.getMessage());
                return;
            }
        }

        try {
            ServletOutputStream servletOutputStream = response.getOutputStream();
            IOUtils.copy(new ByteArrayInputStream(nal.getBytes(StandardCharsets.UTF_8)), servletOutputStream);
            IOHelper.flushAndClose(servletOutputStream);
            response.setStatus(HttpStatus.OK.value());
            response.setHeader(CONTENT_TYPE, MediaType.TEXT_PLAIN_VALUE);
        } catch (IOException e) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class)
                    .withHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR)
                    .withMessage("The NAL '{}' in the system could not be written to the response : {}")
                    .withMessageArgs(nalUri, e.getMessage())
                    .build();
        }
    }

    @VisibleForTesting
    public static RDFFormat resolveFormat(String format) {
        if (format == null) {
            return RDFFormat.RDFXML_PLAIN;
        }
        switch (format) {
            case "nt":
                return RDFFormat.NTRIPLES_UTF8;
            case "json":
                return RDFFormat.RDFJSON;
            case "jsonld":
                return RDFFormat.JSONLD;
            default:
                return RDFFormat.RDFXML_PLAIN;
        }
    }
}
