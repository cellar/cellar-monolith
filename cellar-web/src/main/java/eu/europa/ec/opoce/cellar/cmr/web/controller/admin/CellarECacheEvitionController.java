/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin
 *             FILE : CellarECacheEvitionController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 20 Jan 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.indexing.DashboardController;
import net.sf.ehcache.CacheManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * <class_description> This class provides the handler to clear the existing  objects from the cache managed by Spring.
 * <br/><br/>
 * <notes> .
 * <br/><br/>
 * ON : 20 Jan 2017
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
public class CellarECacheEvitionController extends AbstractCellarController {

    private static final Logger LOG = LogManager.getLogger(CellarECacheEvitionController.class);

    /** The cache manager. */
    @Autowired
    @Qualifier("realCacheManager")
    private CacheManager cacheManager;

    /**
     * Evict cache.
     */
    @RequestMapping(value = "/secapi/cache/clear", method = RequestMethod.GET)
    public String evictCache() {
        LOG.info("Evicting all instances of EhCacheFactoryBean");
        cacheManager.clearAll();
        return redirect(DashboardController.DASHBOARD_URL);
    }

}
