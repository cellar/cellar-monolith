package eu.europa.ec.opoce.cellar.cl.controller.admin.contentVisibility;

import org.apache.commons.lang.StringUtils;

import java.util.List;

public class ContentStream implements Comparable<ContentStream> {

    private String contentId;

    private List<String> contentPids;

    public ContentStream(final String contentId, final List<String> contentPids) {
        super();
        this.contentId = contentId;
        this.contentPids = contentPids;
    }

    public String getContentId() {
        return contentId;
    }

    public List<String> getContentPids() {
        return contentPids;
    }

    public String getWorkIndex() {
        return StringUtils.substringBefore(contentId, ".");
    }

    public String getExpressionIndex() {
        String expressionIndex = StringUtils.substringAfter(contentId, ".");
        return StringUtils.substringBefore(expressionIndex, ".");
    }

    public String getManifestationIndex() {
        String manifestationIndex = StringUtils.substringAfterLast(contentId, ".");
        return StringUtils.substringBefore(manifestationIndex, "/");
    }

    public String getManifestationDatastreamName() {
        return StringUtils.substringAfter(contentId, "/");
    }

    @Override
    public int compareTo(ContentStream cs) {
        return getContentId().compareTo(cs.getContentId());

    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

}
