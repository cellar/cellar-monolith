/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob
 *             FILE : CreateEmbargoJobController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 12, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create;

import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob.BATCH_JOB_TYPE;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.command.CreateEmbargoJob;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.create.command.CreateJob;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.manager.EmbargoJobManagerController;
import eu.europa.ec.opoce.cellar.common.util.TimeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;

/**
 * <class_description> Create embargo batch job controller.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 12, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
@RequestMapping(CreateEmbargoJobController.PAGE_URL)
public class CreateEmbargoJobController extends CreateJobController {

    public static final String PAGE_URL = "/cmr/visibility/job/create";

    private static final String JSP_NAME = "cmr/visibility/job/create-job";

    private static final String OBJECT_NAME = "job";

    /**
     * Validator for a created job.
     */
    @Autowired
    @Qualifier("embargoJobValidator")
    private Validator jobValidator;

    /**
     * Handle the GET.
     */
    @RequestMapping(method = RequestMethod.GET)
    public String createJobGet(final ModelMap model) {

        model.addAttribute(OBJECT_NAME, new CreateEmbargoJob()); // empty job

        return JSP_NAME;
    }

    /**
     * Handle the POST.
     */
    @RequestMapping(method = RequestMethod.POST)
    public String createJobPost(final ModelMap model, @ModelAttribute(OBJECT_NAME) final CreateEmbargoJob createdJob,
            final BindingResult bindingResult, final SessionStatus status) {

        return super.createJob(model, createdJob, bindingResult, status);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Validator getCreateJobValidator() {
        return this.jobValidator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BatchJob constructBatchJob(final CreateJob createJob) {
        final CreateEmbargoJob job = (CreateEmbargoJob) createJob;
        return BatchJob.create(BATCH_JOB_TYPE.EMBARGO, job.getJobName(), job.getSparqlQuery())
                .embargoDate(TimeUtils.getDefaultDateSilently(job.getEmbargoDate(), true))
                .build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getJspName() {
        return JSP_NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getSuccessPageUrl() {
        return EmbargoJobManagerController.PAGE_URL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getFunctionalityUrl() {
        return EmbargoJobManagerController.PAGE_URL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BATCH_JOB_TYPE getBatchJobType() {
        return BATCH_JOB_TYPE.EMBARGO;
    }
}
