/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.consult
 *             FILE : ConsultUpdateJobController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 2, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.consult;

import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob.BATCH_JOB_TYPE;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.batchJob.manager.UpdateJobManagerController;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <class_description> Consult update job controller.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 2, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
@RequestMapping(ConsultUpdateJobController.PAGE_URL)
public class ConsultUpdateJobController extends ConsultJobController {

    public static final String PAGE_URL = "/cmr/update/job/consult";

    private static final String JSP_NAME = "cmr/update/job/consult-job";

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getFunctionalityUrl() {
        return UpdateJobManagerController.PAGE_URL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BATCH_JOB_TYPE getBatchJobType() {
        return BATCH_JOB_TYPE.UPDATE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getJspName() {
        return JSP_NAME;
    }
}
