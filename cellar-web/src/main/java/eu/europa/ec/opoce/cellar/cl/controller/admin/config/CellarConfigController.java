/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.config
 *        FILE : CellarConfigController.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 30-01-2012
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.admin.config;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.bootstrap.CellarHooks;
import eu.europa.ec.opoce.cellar.cl.configuration.ConfigurationPropertyService.EXISTINGPROPS_MODE;
import eu.europa.ec.opoce.cellar.cl.configuration.enums.CellarConfigurationPropertyKey;
import eu.europa.ec.opoce.cellar.cl.configuration.impl.CellarPersistentConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.impl.CellarStaticConfiguration;
import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.util.ControllerUtil;
import eu.europa.ec.opoce.cellar.cl.domain.admin.ConfigurationProperty;
import eu.europa.ec.opoce.cellar.cl.exception.CellarWebException;
import eu.europa.ec.opoce.cellar.common.util.SystemUtils;
import eu.europa.ec.opoce.cellar.exception.CellarConfigurationException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static javax.ws.rs.core.HttpHeaders.CONTENT_TYPE;

/**
 * <class_description> Controller for the CELLAR configuration parameters management view.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 30-01-2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller(value = "cellarConfigController")
public class CellarConfigController extends AbstractCellarController {

    @Autowired
    @Qualifier("cellarConfiguration")
    private CellarPersistentConfiguration cellarPersistentConfiguration;

    @ModelAttribute("configObject")
    public ConfigFormObject initForm() {
        final Collection<ConfigurationProperty> properties = this.cellarPersistentConfiguration.getConfigurationPropertyService().getProperties();
        final List<ConfigurationProperty> knownProperties = new ArrayList<>();
        for (final ConfigurationProperty property : properties) {
            if (CellarConfigurationPropertyKey.get(property.getPropertyKey()) != null) {
                knownProperties.add(property);
            }
        }
        return new ConfigFormObject(knownProperties);
    }

    /**
     * Handler method for displaying the main security view.
     *
     * @return The ModelAndView instance with the security view.
     */
    @RequestMapping({
            "/configuration", "/configuration/update"})
    public ModelAndView loadPage() {
        final ModelAndView modelAndView = new ModelAndView("cellarConfig");
        if (this.cellarPersistentConfiguration.isCellarDatabaseReadOnly()) {
            final CellarConfigurationException readonlyDatabaseExc = ExceptionBuilder.get(CellarConfigurationException.class)
                    .withCode(CommonErrors.READONLY_DATABASE).build();
            modelAndView.addObject("warningMessage", readonlyDatabaseExc.getHeader());
        }
        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/configuration/update")
    public ModelAndView update(@ModelAttribute("configObject") final ConfigFormObject config) {
        try {
            for (final ConfigurationProperty configurationProperty : config.getItems()) {
                this.cellarPersistentConfiguration.synchronizeProperty(configurationProperty.getPropertyKey(),
                        configurationProperty.getPropertyValue(), EXISTINGPROPS_MODE.TO_DB);
            }
        } catch (final CellarConfigurationException e) {
            // just ignore the exception if it is due to a write attempt to a read-only database
            if (!CommonErrors.READONLY_DATABASE.equals(e.getCode())) {
                throw e;
            }
        }

        final RedirectView view = new RedirectView("/admin/configuration", true);
        return new ModelAndView(view);
    }

    /**
     * Get all configuration properties.</br>
     * <b>Possible improvement</b>: it can be replaced by the standard configuration properties of Spring
     * (see https://cloud.spring.io/spring-cloud-config).
     *
     * @return the list of properties
     */
    @RequestMapping(method = RequestMethod.GET, value = "/secapi/configuration/getProperties")
    @ResponseBody
    public Map<String, Map<String, Object>> getAllConfigurationProperties() {
        return this.cellarPersistentConfiguration.getAllConfigurationProperties();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/secapi/configuration/getProperty")
    public ResponseEntity<String> getConfigurationProperty(@RequestParam(required = true, value = "propertyKey") final String propertyKey) {
        // consistency checks
        if (StringUtils.isBlank(propertyKey)) {
            final CellarWebException exc = HttpStatusAwareExceptionBuilder.getInstance(CellarWebException.class)
                    .withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage(
                            "You need to specify the request parameter 'propertyKey' set with the name of one of the properties from file '{}'. Example: {}")
                    .withMessageArgs(this.cellarPersistentConfiguration.getConfigurationPath(),
                            "/admin/secapi/configuration/getProperty?propertyKey=cellar.service.ingestion.enabled")
                    .build();
            return ControllerUtil.makeErrorResponse(exc, exc.getHttpStatus());
        }
        if (!this.cellarPersistentConfiguration.getPropertyManager().existsProperty(propertyKey)) {
            final CellarWebException exc = HttpStatusAwareExceptionBuilder.getInstance(CellarWebException.class)
                    .withHttpStatus(HttpStatus.NOT_FOUND).withMessage("Property '{}' does not exist.").withMessageArgs(propertyKey).build();
            return ControllerUtil.makeErrorResponse(exc, exc.getHttpStatus());
        }

        // gets property
        final String propertyValue = this.cellarPersistentConfiguration.getPropertyManager().getProperty(propertyKey);
        final HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add(CONTENT_TYPE, MediaType.TEXT_PLAIN + ";charset=UTF-8");
        return new ResponseEntity<>(propertyValue, responseHeaders, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/secapi/configuration/updateProperty")
    public ResponseEntity<String> updateConfigurationProperty(
            @RequestParam(required = true, value = "propertyKey") final String propertyKey,
            @RequestParam(required = true, value = "propertyValue") final String propertyValue) {
        // consistency checks
        // check if property's key is blank
        if (StringUtils.isBlank(propertyKey)) {
            final CellarWebException exc = HttpStatusAwareExceptionBuilder.getInstance(CellarWebException.class)
                    .withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage(
                            "You need to specify the request parameter 'propertyKey' set with the name of one of the properties from file '{}'. Example: {}")
                    .withMessageArgs(this.cellarPersistentConfiguration.getConfigurationPath(),
                            "/admin/secapi/configuration/getProperty?propertyKey=cellar.service.ingestion.enabled")
                    .build();
            return ControllerUtil.makeErrorResponse(exc, exc.getHttpStatus());
        }
        // check if property exists
        if (!this.cellarPersistentConfiguration.getPropertyManager().existsProperty(propertyKey)) {
            final CellarWebException exc = HttpStatusAwareExceptionBuilder.getInstance(CellarWebException.class)
                    .withHttpStatus(HttpStatus.NOT_FOUND).withMessage("Property '{}' does not exist.").withMessageArgs(propertyKey).build();
            return ControllerUtil.makeErrorResponse(exc, exc.getHttpStatus());
        }
        // check if property's value is blank
        if (StringUtils.isBlank(propertyValue)) {
            final CellarWebException exc = HttpStatusAwareExceptionBuilder.getInstance(CellarWebException.class)
                    .withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage(
                            "You need to specify the request parameter 'propertyValue' set with the desired value for property '{}'. Example: {}")
                    .withMessageArgs(propertyKey,
                            "/admin/secapi/configuration/getProperty?propertyKey=" + propertyKey + "&amp;propertyValue=some_value")
                    .build();
            return ControllerUtil.makeErrorResponse(exc, exc.getHttpStatus());
        }
        // check if property is persistent
        if (!this.cellarPersistentConfiguration.isPersistent(propertyKey)) {
            final CellarWebException exc = HttpStatusAwareExceptionBuilder.getInstance(CellarWebException.class)
                    .withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage("The property '{}' cannot be updated because it is not configured as persistent."
                            + " For more information about persistent properties, please consult the file located at '{}'.")
                    .withMessageArgs(propertyKey, "classpath:/" + CellarStaticConfiguration.PERSISTENT_CONF_PATH).build();
            return ControllerUtil.makeErrorResponse(exc, exc.getHttpStatus());
        }

        // persists property to database
        try {
            this.cellarPersistentConfiguration.synchronizeProperty(propertyKey, propertyValue, EXISTINGPROPS_MODE.TO_DB);
        } catch (final CellarConfigurationException cause) {
            final CellarWebException exc = HttpStatusAwareExceptionBuilder.getInstance(CellarWebException.class)
                    .withMessage(cause.toString()).withCause(cause).build();
            return ControllerUtil.makeErrorResponse(exc, exc.getHttpStatus());
        }

        // returns response
        final HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.add(CONTENT_TYPE, MediaType.TEXT_PLAIN + ";charset=UTF-8");
        return new ResponseEntity<>("Property '" + propertyKey + "' has been updated correctly with value '" + propertyValue + "'.",
                responseHeaders, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/secapi/pid")
    public ResponseEntity<Integer> getPID() {
        return ResponseEntity.ok(SystemUtils.getPid());
    }

    @PostMapping("/secapi/hook/debug")
    public ResponseEntity<?> setDebugContext(@RequestBody String debugContext) {
        CellarHooks.setDebugContext(debugContext);
        return ResponseEntity.ok().build();
    }
}
