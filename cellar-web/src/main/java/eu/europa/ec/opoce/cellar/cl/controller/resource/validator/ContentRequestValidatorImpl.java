package eu.europa.ec.opoce.cellar.cl.controller.resource.validator;

import com.google.common.base.Strings;
import eu.europa.ec.opoce.cellar.cl.controller.resource.validator.client.ContentRequestValidator;
import eu.europa.ec.opoce.cellar.cl.controller.resource.validator.client.SimpleContentRequestValidator;
import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

/**
 * Execute the content request validation chain. Any new validator may be added in the bean
 * definition.
 *
 * @author omeurice
 */
@Component
public class ContentRequestValidatorImpl implements ContentRequestValidator {

    /**
     * List of validator in the validation execution chain. Edit the bean definition to add or
     * remove a validator.
     */
    private final List<SimpleContentRequestValidator> validators = new ArrayList<>();
    private final CellarResourceDao cellarResourceDao;

    @Autowired
    public ContentRequestValidatorImpl(CellarResourceDao cellarResourceDao, List<SimpleContentRequestValidator> requestValidators) {
        this.cellarResourceDao = cellarResourceDao;
        this.validators.addAll(requestValidators);
    }

    /**
     * Execute the validation execution chain.
     *
     * @param request      Content request.
     * @param response     HTTP response.
     * @param id           Cellar identifier of the requested content
     * @param datastreamId Datastream name of the requested content.
     * @return The HTTP status that must be returned to the client.
     * @throws Exception Any exception from the <code>SimpleRequestValidator</code> instances.
     */
    @Override
    public HttpStatus validateContentRequest(final HttpServletRequest request, final HttpServletResponse response,
                                             final CellarIdentifier id, final String datastreamId) throws Exception {
        final String datastream = Strings.isNullOrEmpty(datastreamId) ? "" : "/" + datastreamId;
        final CellarResource resource = cellarResourceDao.findCellarId(id.getUuid() + datastream);
        if (resource != null) {
            for (final SimpleContentRequestValidator validator : this.validators) {
                final HttpStatus status = validator.validate(request, response, id, datastreamId, resource);
                if (!HttpStatus.OK.equals(status)) {
                    return status;
                }
            }
            return HttpStatus.OK;
        }
        return HttpStatus.NOT_FOUND;

    }

}
