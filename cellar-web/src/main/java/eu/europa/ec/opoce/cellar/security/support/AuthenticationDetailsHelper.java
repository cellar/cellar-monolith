/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.security.support
 *        FILE : AuthenticationDetailsHelper.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 10-03-2023
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2023 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.security.support;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import eu.europa.ec.opoce.cellar.security.authentication.details.PrincipalAwarePreAuthenticatedGrantedAuthoritiesWebAuthenticationDetails;

/**
 * Contains helper methods for obtaining the EU-Login principal.
 * 
 * @author EUROPEAN DYNAMICS S.A.
 */
public class AuthenticationDetailsHelper {
	
	private AuthenticationDetailsHelper() {
		
	}
	
	/**
	 * Obtains the <code>PrincipalAwarePreAuthenticatedGrantedAuthoritiesWebAuthenticationDetails</code> from
	 * the provided <code>Authentication</code> token.
	 * @param token the <code>Authentication</code> token.
	 * @return the <code>PrincipalAwarePreAuthenticatedGrantedAuthoritiesWebAuthenticationDetails</code>.
	 */
	public static PrincipalAwarePreAuthenticatedGrantedAuthoritiesWebAuthenticationDetails getAuthenticationDetails(Authentication token) {
		Object details = token.getDetails();
		if (details == null) {
			throw new DetailsNotFoundException("The authentication details object is null.");
		}
		if (!(details instanceof PrincipalAwarePreAuthenticatedGrantedAuthoritiesWebAuthenticationDetails)) {
			throw new InvalidDetailsException("The authentication details object is not of type [PreAuthenticatedPrincipalGrantedAuthoritiesWebAuthenticationDetails].");
		}
		return (PrincipalAwarePreAuthenticatedGrantedAuthoritiesWebAuthenticationDetails) details;
	}

	
	public static class InvalidDetailsException extends AuthenticationException {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		private InvalidDetailsException(String message) {
			super(message);
		}
		
	}
	
	public static class DetailsNotFoundException extends AuthenticationException {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		private DetailsNotFoundException (String message) {
			super(message);
		}
		
	}
	
}
