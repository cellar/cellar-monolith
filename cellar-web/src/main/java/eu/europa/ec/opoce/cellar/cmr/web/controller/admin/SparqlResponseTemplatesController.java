package eu.europa.ec.opoce.cellar.cmr.web.controller.admin;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL;
import eu.europa.ec.opoce.cellar.cl.domain.sparql.UriTemplates;
import eu.europa.ec.opoce.cellar.cl.service.client.UriTemplatesService;
import eu.europa.ec.opoce.cellar.server.admin.sparql.UploadXslt;
import eu.europa.ec.opoce.cellar.server.admin.sparql.XsltResolverService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.List;

@Controller
public class SparqlResponseTemplatesController extends AbstractCellarController {

    private static final Logger LOG = LogManager.getLogger(SparqlResponseTemplatesController.class);

    private static final String URL_RESPONSE_TEMPLATES = "/cmr/sparql/templates";
    private static final String PAGE_RESPONSE_TEMPLATES_CREATE = "sparqlResponseTemplatesCreate";
    private static final String PAGE_RESPONSE_TEMPLATES_UPDATE = "sparqlResponseTemplatesUpdate";

    /**
     * Constant <code>XSLT_PATH_LOCAL="sparql/xslt"</code>
     */
    private static final String RESPONSE_TEMPLATES_PATH_LOCAL = "sparql/response-templates";

    @Autowired(required = true)
    private UriTemplatesService uriTemplateService;

    @Autowired(required = true)
    private XsltResolverService xsltResolverService;

    /**
     * Validator for a created job.
     */
    @Autowired
    @Qualifier("uriTemplatesValidator")
    private Validator uriTemplatesValidator;

    @RequestMapping(value = "/cmr/sparql/templates", method = RequestMethod.GET)
    public ModelAndView getSparqlResponseTemplates(final HttpServletRequest request, final HttpServletResponse response)
            throws IOException {
        final ModelAndView modelAndView = new ModelAndView("sparqlResponseTemplates");
        modelAndView.addObject(new UploadXslt());
        modelAndView.addObject("responseTemplates", this.uriTemplateService.findAllOrderedBySequence());
        return modelAndView;
    }

    @RequestMapping(value = "/cmr/sparql/templates/create", method = RequestMethod.GET)
    public ModelAndView getSparqlResponseTemplatesCreate(final HttpServletRequest request, final HttpServletResponse response)
            throws IOException {
        final ModelAndView modelAndView = new ModelAndView("sparqlResponseTemplatesCreate");
        modelAndView.addObject("uriTemplates", new UriTemplates()); // empty uriTemplates
        modelAndView.addObject("xslt", this.uriTemplateService.getXslt());
        return modelAndView;
    }

    @RequestMapping(value = "/cmr/sparql/templates/new", method = RequestMethod.POST)
    public String createSparqlResponseTemplates(final HttpServletRequest request, final HttpServletResponse response,
            @ModelAttribute("uriTemplates") final UriTemplates uriTemplates, final BindingResult bindingResult) throws IOException {
        this.uriTemplatesValidator.validate(uriTemplates, bindingResult); // validates the created uriTemplates
        if (bindingResult.hasErrors()) {
            return PAGE_RESPONSE_TEMPLATES_CREATE;
        }

        //Shift the sequence of URI templates
        final List<UriTemplates> uriTemplatesList = this.uriTemplateService.findUriTemplatesBySequence(uriTemplates.getSequence());
        Long sequence = uriTemplates.getSequence();
        for (final UriTemplates ut : uriTemplatesList) {
            sequence++;
            ut.setSequence(sequence);
            this.uriTemplateService.updatelUriTemplates(ut);
        }

        this.uriTemplateService.savelUriTemplates(uriTemplates);
        super.addUserMessage(LEVEL.SUCCESS, "common.message.successfullySaved");
        return redirect(URL_RESPONSE_TEMPLATES);
    }

    @RequestMapping(value = "/cmr/sparql/templates/delete", method = RequestMethod.GET)
    public String deleteSparqlResponseTemplates(@RequestParam(value = "id", required = true) final Long id) {
        final UriTemplates uriTemplates = this.uriTemplateService.getUriTemplatesById(id);
        this.uriTemplateService.deleteUriTemplates(uriTemplates);

        super.addUserMessage(LEVEL.SUCCESS, "common.message.successfullyProcessed");
        return redirect(URL_RESPONSE_TEMPLATES);
    }

    @RequestMapping(value = "/cmr/sparql/templates/edit", method = RequestMethod.GET)
    public ModelAndView getSparqlResponseTemplatesEdit(final HttpServletRequest request, final HttpServletResponse response,
            @RequestParam(value = "id", required = true) final Long id) throws IOException {
        final ModelAndView modelAndView = new ModelAndView("sparqlResponseTemplatesUpdate");
        final UriTemplates uriTemplates = this.uriTemplateService.getUriTemplatesById(id);
        modelAndView.addObject("uriTemplates", uriTemplates);
        modelAndView.addObject("xslt", this.uriTemplateService.getXslt());
        return modelAndView;
    }

    @RequestMapping(value = "/cmr/sparql/templates/update", method = RequestMethod.POST)
    public String editJob(final HttpServletRequest request, final HttpServletResponse response,
            @ModelAttribute("uriTemplates") final UriTemplates uriTemplates, final BindingResult bindingResult) {
        this.uriTemplatesValidator.validate(uriTemplates, bindingResult); // validates the edited uriTemplates

        if (bindingResult.hasErrors()) {
            return PAGE_RESPONSE_TEMPLATES_UPDATE;
        }

        //Shift the sequence of URI templates
        final UriTemplates uriTemplatesOld = this.uriTemplateService.getUriTemplatesById(uriTemplates.getId());
        if (!uriTemplatesOld.getSequence().equals(uriTemplates.getSequence())) {
            final List<UriTemplates> uriTemplatesList = this.uriTemplateService.findUriTemplatesBySequence(uriTemplates.getSequence());
            Long sequence = uriTemplates.getSequence();
            for (final UriTemplates ut : uriTemplatesList) {
                sequence++;
                ut.setSequence(sequence);
                this.uriTemplateService.updatelUriTemplates(ut);
            }
        }

        this.uriTemplateService.updatelUriTemplates(uriTemplates);

        super.addUserMessage(LEVEL.SUCCESS, "common.message.successfullyProcessed");
        return redirect(URL_RESPONSE_TEMPLATES);
    }

    /**
     * Returns functionality url.
     * @return functionality url
     */
    @ModelAttribute("functionalityUrl")
    public String populateFunctionalityUrl() {
        return super.prefixedUrl(URL_RESPONSE_TEMPLATES);
    }

    @RequestMapping(value = "/cmr/sparql/templates/uploadXslt", method = RequestMethod.POST)
    public String createXslt(final HttpServletRequest request, final UploadXslt uploadItem, final BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            for (final ObjectError error : bindingResult.getAllErrors()) {
                LOG.error("Error while uploading file: {} - {}", error.getCode(), error.getDefaultMessage());
            }
        }
        final File outputFolder = new File(this.cellarConfiguration.getCellarFolderRoot(), RESPONSE_TEMPLATES_PATH_LOCAL);
        final boolean success = this.xsltResolverService.saveStylesheet(uploadItem, outputFolder);
        if (success) {
            super.addUserMessage(LEVEL.SUCCESS, "common.message.successfullyProcessed");
        }
        return redirect(URL_RESPONSE_TEMPLATES);
    }

}
