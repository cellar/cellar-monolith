/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.contentSearch
 *        FILE : ContentSearchController.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 22-05-2012
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$$
 *     VERSION : $LastChangedRevision$$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.admin.contentSearch;

import eu.europa.ec.op.cellar.api.service.ICellarResponse;
import eu.europa.ec.op.cellar.api.service.ICellarService;
import eu.europa.ec.op.cellar.api.service.impl.CellarException;
import eu.europa.ec.op.cellar.api.service.impl.ManifestationType;
import eu.europa.ec.op.cellar.api.util.WeightedLanguagePair;
import eu.europa.ec.op.cellar.api.util.WeightedLanguagePairs;
import eu.europa.ec.op.cellar.api.util.WeightedManifestationTypes;
import eu.europa.ec.opoce.cellar.ccr.service.FileTypesNalSkosLoaderService;
import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarResponseController;
import eu.europa.ec.opoce.cellar.cl.controller.util.ControllerUtil;
import eu.europa.ec.opoce.cellar.cl.domain.mimeType.MimeTypeMapping;
import eu.europa.ec.opoce.cellar.cl.exception.CellarWebException;
import eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService;
import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.server.admin.resource.ResourceConfigInvoker;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 * <class_description> Controller for searching the following content: all notices, RDF and content streams.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 22-05-2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller(value = "contentSearchController")
public class ContentSearchController extends AbstractCellarResponseController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ContentSearchController.class);

    @Autowired
    private ICellarService cellarApiService;

    // PID manager that checks if the passed object has the correct nature (ie: work, expression or event, manifestation, or manifestation content)
    // objectNatureUnawarePidManagerService does not perform this check: if such check is needed in future, change the qualifier to genericPidManagerService.
    @Autowired
    @Qualifier("objectNatureUnawarePidManagerService")
    private PidManagerService pidManagerService;

    @Autowired
    private FileTypesNalSkosLoaderService fileTypesNalSkosLoaderService;

    @Autowired
    private ResourceConfigInvoker languageService;

    @RequestMapping(method = RequestMethod.GET, value = "/contentSearch")
    public ModelAndView getContentSearchModel(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        final ModelAndView modelAndView = new ModelAndView("contentSearch");

        // gets all available languages and sets them as request attribute
        final List<LanguageBean> languageBeans = this.languageService.getLanguages();
        final List<Locale> languages = new ArrayList<Locale>();
        for (final LanguageBean languageBean : languageBeans) {
            languages.add(new Locale(languageBean.getIsoCodeThreeChar()));
        }
        modelAndView.addObject("languages", languages);

        // gets all acceptable formats (that is, the extensions of mimetypes) and sets them as request attribute
        final Set<String> allPossibleExtensions = this.fileTypesNalSkosLoaderService.getAllPossibleExtensions();
        modelAndView.addObject("acceptFormats", allPossibleExtensions);

        // get the excludeInferred property and sets it as request attribute
        modelAndView.addObject("excludeInferred", false);

        return modelAndView;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/contentSearch/getTreeNotice")
    public ResponseEntity<String> getTreeNotice(@RequestParam(required = true, value = "pid") final String pid,
                                                @RequestParam(required = true, value = "decodingLanguage") final String decodingLanguage) {
        return this.retrieveStringResponse(new TreeNoticeDelegator(pid, decodingLanguage));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/contentSearch/getBranchNotice")
    public ResponseEntity<String> getBranchNotice(@RequestParam(required = true, value = "pid") final String pid,
                                                  @RequestParam(required = true, value = "decodingLanguage") final String decodingLanguage,
                                                  @RequestParam(required = true, value = "acceptLanguage") final String acceptLanguage) {
        return this.retrieveStringResponse(new BranchNoticeDelegator(pid, decodingLanguage, acceptLanguage));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/contentSearch/getObjectWorkNotice")
    public ResponseEntity<String> getObjectWorkNotice(@RequestParam(required = true, value = "pid") final String pid,
                                                      @RequestParam(required = true, value = "decodingLanguage") final String decodingLanguage) {
        return this.retrieveStringResponse(new ObjectWorkNoticeDelegator(pid, decodingLanguage));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/contentSearch/getObjectExpressionNotice")
    public ResponseEntity<String> getObjectExpressionNotice(@RequestParam(required = true, value = "pid") final String pid,
                                                            @RequestParam(required = true, value = "decodingLanguage") final String decodingLanguage,
                                                            @RequestParam(required = false, value = "acceptLanguage") final String acceptLanguage) {
        return this.retrieveStringResponse(new ObjectExpressionNoticeDelegator(pid, decodingLanguage, acceptLanguage));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/contentSearch/getObjectManifestationNotice")
    public ResponseEntity<String> getObjectManifestationNotice(@RequestParam(required = true, value = "pid") final String pid,
                                                               @RequestParam(required = true, value = "decodingLanguage") final String decodingLanguage) {
        return this.retrieveStringResponse(new ObjectManifestationNoticeDelegator(pid, decodingLanguage));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/contentSearch/getRDF")
    public ResponseEntity<String> getRDF(@RequestParam(required = true, value = "pid") final String pid,
                                         @RequestParam(required = true, value = "excludeInferred", defaultValue = "false") final boolean excludeInferred) {
        return this.retrieveStringResponse(new RDFDelegator(pid, excludeInferred));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/contentSearch/getRDFTree")
    public ResponseEntity<String> getRDFTree(@RequestParam(required = true, value = "pid") final String pid,
                                             @RequestParam(required = true, value = "excludeInferred", defaultValue = "false") final boolean excludeInferred) {
        return this.retrieveStringResponse(new RDFTreeDelegator(pid, excludeInferred));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/contentSearch/getContentStream")
    public ResponseEntity<byte[]> getContentStream(@RequestParam(required = true, value = "pid") final String pid,
                                                   @RequestParam(required = true, value = "acceptFormat") final String acceptFormat,
                                                   @RequestParam(required = true, value = "acceptLanguage") final String acceptLanguage) {
        return this.retrieveBytesResponse(new ContentStreamDelegator(pid, acceptLanguage, acceptFormat), "content-stream." + acceptFormat);
    }

    private ResponseEntity<String> retrieveStringResponse(final ContentSearchDelegator delegator) {
        // checks for blank mandatory params
        final List<String> blankParams = delegator.checkBlankParams();
        if (blankParams != null && blankParams.size() > 0) {
            final CellarWebException exc = HttpStatusAwareExceptionBuilder.getInstance(CellarWebException.class)
                    .withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage("The following parameters are mandatory, but you did not fill them: '{}'.")
                    .withMessageArgs(StringUtils.join(blankParams, ", ")).build();
            return ControllerUtil.makeErrorResponse(exc, exc.getHttpStatus());
        }

        // checks for unconsistent params
        final List<String> unconsistentParams = delegator.checkUnconsistentParams();
        if (unconsistentParams != null && unconsistentParams.size() > 0) {
            final CellarWebException exc = HttpStatusAwareExceptionBuilder.getInstance(CellarWebException.class)
                    .withHttpStatus(HttpStatus.BAD_REQUEST).withMessage("The following parameters are not consistent:\n'{}'.")
                    .withMessageArgs(StringUtils.join(unconsistentParams, "\n")).build();
            return ControllerUtil.makeErrorResponse(exc, exc.getHttpStatus());
        }

        // retrieves the response
        ICellarResponse response = null;
        try {
            response = delegator.retrieveResponse();
        } catch (final CellarException e) {
            final CellarWebException exc = HttpStatusAwareExceptionBuilder.getInstance(CellarWebException.class)
                    .withHttpStatus(ControllerUtil.resolveHttpStatus(e.getResponseCode().getResponseCode())).withMessage(e.getMessage())
                    .withCause(e).build();
            return ControllerUtil.makeErrorResponse(exc, exc.getHttpStatus());
        } catch (final Exception e) {
            final CellarWebException exc = HttpStatusAwareExceptionBuilder.getInstance(CellarWebException.class).withMessage(e.getMessage())
                    .withCause(e).build();
            return ControllerUtil.makeErrorResponse(exc, exc.getHttpStatus());
        }

        return super.retrieveStringResponse(response);
    }

    private ResponseEntity<byte[]> retrieveBytesResponse(final ContentSearchDelegator delegator, final String attachmentFilename) {
        // checks for blank mandatory params
        final List<String> blankParams = delegator.checkBlankParams();
        if (blankParams != null && blankParams.size() > 0) {
            final CellarWebException exc = HttpStatusAwareExceptionBuilder.getInstance(CellarWebException.class)
                    .withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage("The following parameters are mandatory, but you did not fill them: '{}'.")
                    .withMessageArgs(StringUtils.join(blankParams, ", ")).build();
            final ResponseEntity<String> responseErrorStr = ControllerUtil.makeErrorResponse(exc, exc.getHttpStatus());
            return ControllerUtil.toBytesResponseEntity(responseErrorStr);
        }

        // checks for unconsistent params
        final List<String> unconsistentParams = delegator.checkUnconsistentParams();
        if (unconsistentParams != null && unconsistentParams.size() > 0) {
            final CellarWebException exc = HttpStatusAwareExceptionBuilder.getInstance(CellarWebException.class)
                    .withHttpStatus(HttpStatus.BAD_REQUEST).withMessage("The following parameters are not consistent:\n'{}'.")
                    .withMessageArgs(StringUtils.join(unconsistentParams, "\n")).build();
            final ResponseEntity<String> responseErrorStr = ControllerUtil.makeErrorResponse(exc, exc.getHttpStatus());
            return ControllerUtil.toBytesResponseEntity(responseErrorStr);
        }

        // retrieves the response
        ICellarResponse response = null;
        try {
            response = delegator.retrieveResponse();
        } catch (final CellarException e) {
            final CellarWebException exc = HttpStatusAwareExceptionBuilder.getInstance(CellarWebException.class)
                    .withHttpStatus(ControllerUtil.resolveHttpStatus(e.getResponseCode().getResponseCode())).withMessage(e.getMessage())
                    .withCause(e).build();
            final ResponseEntity<String> responseErrorStr = ControllerUtil.makeErrorResponse(exc, exc.getHttpStatus());
            return ControllerUtil.toBytesResponseEntity(responseErrorStr);
        } catch (final Exception e) {
            final CellarWebException exc = HttpStatusAwareExceptionBuilder.getInstance(CellarWebException.class).withMessage(e.getMessage())
                    .withCause(e).build();
            final ResponseEntity<String> responseErrorStr = ControllerUtil.makeErrorResponse(exc, exc.getHttpStatus());
            return ControllerUtil.toBytesResponseEntity(responseErrorStr);
        }

        return super.retrieveBytesResponse(response, delegator.pid, attachmentFilename);
    }

    private abstract class ContentSearchDelegator {

        protected String pid;
        protected boolean pidRequired;
        protected String decodingLanguage;
        protected boolean decodingLanguageRequired;
        protected String acceptLanguage;
        protected boolean acceptLanguageRequired;
        protected String acceptFormat;
        protected boolean acceptFormatRequired;

        ContentSearchDelegator(final String pid, final boolean pidRequired, final String decodingLanguage,
                               final boolean decodingLanguageRequired, final String acceptLanguage, final boolean acceptLanguageRequired,
                               final String acceptFormat, final boolean acceptFormatRequired) {
            this.pid = pid;
            this.pidRequired = pidRequired;
            this.decodingLanguage = decodingLanguage;
            this.decodingLanguageRequired = decodingLanguageRequired;
            this.acceptLanguage = acceptLanguage;
            this.acceptLanguageRequired = acceptLanguageRequired;
            this.acceptFormat = acceptFormat;
            this.acceptFormatRequired = acceptFormatRequired;
        }

        List<String> checkBlankParams() {
            final List<String> blankParams = new ArrayList<String>();
            if (this.pidRequired && StringUtils.isBlank(this.pid)) {
                blankParams.add("Work ID");
            }
            if (this.decodingLanguageRequired && StringUtils.isBlank(this.decodingLanguage)) {
                blankParams.add("Decoding language");
            }
            if (this.acceptLanguageRequired && StringUtils.isBlank(this.acceptLanguage)) {
                blankParams.add("Accept language");
            }
            if (this.acceptFormatRequired && StringUtils.isBlank(this.acceptFormat)) {
                blankParams.add("Accept format");
            }
            return blankParams;
        }

        List<String> checkUnconsistentParams() {
            return null;
        }

        abstract ICellarResponse retrieveResponse() throws CellarException;
    }

    private class TreeNoticeDelegator extends ContentSearchDelegator {

        TreeNoticeDelegator(final String pid, final String decodingLanguage) {
            super(pid, true, decodingLanguage, true, null, false, null, false);
        }

        @Override
        List<String> checkUnconsistentParams() {
            final List<String> unconsistentParams = new ArrayList<String>();
            if (!pidManagerService.isWork(this.pid)) {
                unconsistentParams.add("- Work ID: it does not identify a Work");
            }
            return unconsistentParams;
        }

        @Override
        ICellarResponse retrieveResponse() throws CellarException {
            final WeightedLanguagePair langPairs = new WeightedLanguagePair(null, new Locale(this.decodingLanguage));
            return cellarApiService.getTreeNotice(this.pid, langPairs);
        }
    }

    private class BranchNoticeDelegator extends ContentSearchDelegator {

        BranchNoticeDelegator(final String pid, final String decodingLanguage, final String acceptLanguage) {
            super(pid, true, decodingLanguage, true, acceptLanguage, true, null, false);
        }

        @Override
        List<String> checkUnconsistentParams() {
            final List<String> unconsistentParams = new ArrayList<String>();
            if (!pidManagerService.isWork(this.pid)) {
                unconsistentParams.add("- Work ID: it does not identify a Work");
            }
            return unconsistentParams;
        }

        @Override
        ICellarResponse retrieveResponse() throws CellarException {
            final WeightedLanguagePairs langPairs = new WeightedLanguagePairs();
            langPairs.addLanguagePair(null, new Locale(this.acceptLanguage));
            final WeightedLanguagePair decoding = new WeightedLanguagePair(null, new Locale(this.decodingLanguage));
            return cellarApiService.getBranchNotice(this.pid, langPairs, decoding);
        }
    }

    private class ObjectWorkNoticeDelegator extends ContentSearchDelegator {

        ObjectWorkNoticeDelegator(final String pid, final String decodingLanguage) {
            super(pid, true, decodingLanguage, true, null, false, null, false);
        }

        @Override
        List<String> checkUnconsistentParams() {
            final List<String> unconsistentParams = new ArrayList<String>();
            if (!pidManagerService.isWork(this.pid)) {
                unconsistentParams.add("- Work ID: it does not identify a Work");
            }
            return unconsistentParams;
        }

        @Override
        ICellarResponse retrieveResponse() throws CellarException {
            return cellarApiService
                    .getObjectNotice(this.pid, null,
                                     new WeightedLanguagePair(null,
                                                              new Locale(this.decodingLanguage)));
        }
    }

    private class ObjectExpressionNoticeDelegator extends ContentSearchDelegator {

        ObjectExpressionNoticeDelegator(final String pid, final String decodingLanguage, final String acceptLanguage) {
            super(pid, true, decodingLanguage, true, acceptLanguage, true, null, false);
        }

        @Override
        List<String> checkUnconsistentParams() {
            final List<String> unconsistentParams = new ArrayList<String>();
            if (!pidManagerService.isWork(this.pid)) {
                unconsistentParams.add("- Work ID: it does not identify a Work");
            }
            return unconsistentParams;
        }

        @Override
        ICellarResponse retrieveResponse() throws CellarException {
            final WeightedLanguagePairs langPairs = new WeightedLanguagePairs();
            langPairs.addLanguagePair(null, new Locale(this.decodingLanguage));
            langPairs.addLanguagePair(1D, new Locale(this.acceptLanguage));
            return cellarApiService
                    .getObjectNotice(this.pid, langPairs,
                                     new WeightedLanguagePair(null,
                                                              new Locale(this.decodingLanguage)));
        }
    }

    private class ObjectManifestationNoticeDelegator extends ObjectWorkNoticeDelegator {

        ObjectManifestationNoticeDelegator(final String pid, final String decodingLanguage) {
            super(pid, decodingLanguage);
        }

        @Override
        List<String> checkBlankParams() {
            final List<String> blankParams = new ArrayList<String>();
            if (StringUtils.isBlank(this.pid)) {
                blankParams.add("Manifestation ID");
            }
            if (StringUtils.isBlank(this.decodingLanguage)) {
                blankParams.add("Decoding language");
            }
            return blankParams;
        }

        @Override
        List<String> checkUnconsistentParams() {
            final List<String> unconsistentParams = new ArrayList<String>();
            if (!pidManagerService.isManifestationContent(this.pid)) {
                unconsistentParams.add("- Manifestation ID: it does not identify a Manifestation");
            }
            return unconsistentParams;
        }
    }

    private class RDFDelegator extends ContentSearchDelegator {

        protected boolean excludeInferred;

        RDFDelegator(final String pid, final boolean excludeInferred) {
            super(pid, true, null, false, null, false, null, false);
            this.excludeInferred = excludeInferred;
        }

        @Override
        List<String> checkBlankParams() {
            final List<String> blankParams = new ArrayList<String>();
            if (StringUtils.isBlank(this.pid)) {
                blankParams.add("Object ID");
            }
            return blankParams;
        }

        @Override
        List<String> checkUnconsistentParams() {
            final List<String> unconsistentParams = new ArrayList<String>();
            if (!pidManagerService.isWork(this.pid) && !pidManagerService
                    .isExpressionOrEvent(this.pid) && !pidManagerService.isManifestation(this.pid)) {
                unconsistentParams
                        .add("- Object ID: it does not identify a Work, nor an Expression, "
                             + "nor a Manifestation, nor a Dossier, nor an Event, nor an Agent, "
                             + "nor a TopLevelEvent");
            }
            return unconsistentParams;
        }

        @Override
        ICellarResponse retrieveResponse() throws CellarException {
            final String acceptHeader = "application/rdf+xml" + (this.excludeInferred ? ";notice=non-inferred" : "");
            return cellarApiService.retrieveResource(this.pid, acceptHeader);
        }
    }

    private class RDFTreeDelegator extends RDFDelegator {

        RDFTreeDelegator(final String pid, final boolean excludeInferred) {
            super(pid, excludeInferred);
        }

        @Override
        List<String> checkUnconsistentParams() {
            final List<String> unconsistentParams = new ArrayList<String>();
            if (!pidManagerService.isWork(this.pid)) {
                unconsistentParams.add("- Object ID: it does not identify a Work, nor a Dossier, nor an Agent nor a TopLevelEvent");
            }
            return unconsistentParams;
        }

        @Override
        ICellarResponse retrieveResponse() throws CellarException {
            final String acceptHeader = "application/rdf+xml;notice=" + (this.excludeInferred ? "non-inferred-tree" : "tree");
            return cellarApiService.retrieveResource(this.pid, acceptHeader);
        }
    }

    private class ContentStreamDelegator extends ContentSearchDelegator {

        ContentStreamDelegator(final String pid, final String acceptLanguage, final String acceptFormat) {
            super(pid, true, null, false, acceptLanguage, true, acceptFormat, true);
        }

        @Override
        List<String> checkUnconsistentParams() {
            final List<String> unconsistentParams = new ArrayList<String>();
            if (!pidManagerService.isWork(this.pid)) {
                unconsistentParams.add("- Work ID: it does not identify a Work");
            }
            return unconsistentParams;
        }

        @Override
        ICellarResponse retrieveResponse() throws CellarException {
            final WeightedManifestationTypes contentTypes = new WeightedManifestationTypes();
            final List<MimeTypeMapping> mimeTypeMappings = fileTypesNalSkosLoaderService
                    .getMimeTypeMappingsByExtension(this.acceptFormat);
            for (final MimeTypeMapping mimeTypeMapping : mimeTypeMappings) {
                Double weight = null;
                if (!contentTypes.getWeightedManifestationTypes().isEmpty()) {
                    weight = 1D;
                }
                try {
                    final ManifestationType manifestationType = ManifestationType
                            .valueOf(mimeTypeMapping.getManifestationType().toUpperCase());
                    contentTypes.addWeightedManifestation(manifestationType, weight);
                } catch (final IllegalArgumentException e) {
                    LOGGER.warn("No manifestation type '" + mimeTypeMapping.getManifestationType() + "' is available: ignoring it.");
                }
            }
            if (contentTypes.getWeightedManifestationTypes().isEmpty()) {
                LOGGER.warn("No matching manifestation type was found.");
                contentTypes.addWeightedManifestation(ManifestationType.DEFAULT, null);
            }

            final WeightedLanguagePairs langPairs = new WeightedLanguagePairs();
            langPairs.addLanguagePair(null, new Locale(this.acceptLanguage));
            return cellarApiService.getManifestation(this.pid, contentTypes, langPairs);
        }
    }

}
