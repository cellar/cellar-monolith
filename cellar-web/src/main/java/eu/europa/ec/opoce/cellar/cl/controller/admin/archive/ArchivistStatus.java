package eu.europa.ec.opoce.cellar.cl.controller.admin.archive;

public class ArchivistStatus {
    private String status;
    private Components components;

    public String getDatabaseStatus() {
        return getDb().getStatus();
    }

    public String getDatabase() {
        return getDb().getDetails().getDatabase();
    }

    private Database getDb() {
        return getComponents().getDb();
    }

    public String getResult() {
        return getDb().getDetails().getResult();
    }

    public String getValidationQuery() {
        return getDb().getDetails().getValidationQuery();
    }

    public String getDiskSpaceStatus() {
        return getDiskSpace().getStatus();
    }

    public float getTotal() {
        return getDiskSpace().getDetails().getTotal();
    }

    public float getFree() {
        return getDiskSpace().getDetails().getFree();
    }

    private DiskSpace getDiskSpace() {
        return getComponents().getDiskSpace();
    }

    public float getThreshold() {
        return getDiskSpace().getDetails().getThreshold();
    }

    public String getPingStatus() {
        return getComponents().getPing().getStatus();
    }

    public String getRefreshScope() {
        return getComponents().getRefreshScope().getStatus();
    }

    public String getStatus() {
        return status;
    }

    public Components getComponents() {
        return components;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setComponents(Components componentsObject) {
        this.components = componentsObject;
    }

    private static class Components {

        private Database db;
        private DiskSpace diskSpace;
        private Status ping;
        private Status refreshScope;

        public Database getDb() {
            return db;
        }

        public void setDb(Database db) {
            this.db = db;
        }

        public DiskSpace getDiskSpace() {
            return diskSpace;
        }

        public void setDiskSpace(DiskSpace diskSpace) {
            this.diskSpace = diskSpace;
        }

        public Status getPing() {
            return ping;
        }

        public void setPing(Status ping) {
            this.ping = ping;
        }

        public Status getRefreshScope() {
            return refreshScope;
        }

        public void setRefreshScope(Status refreshScope) {
            this.refreshScope = refreshScope;
        }
    }

    private static class Database {
        private String status;
        private DatabaseDetails details;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public DatabaseDetails getDetails() {
            return details;
        }

        public void setDetails(DatabaseDetails details) {
            this.details = details;
        }
    }

    private static class DiskSpace {
        private String status;
        private DiskSpaceDetails details;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public DiskSpaceDetails getDetails() {
            return details;
        }

        public void setDetails(DiskSpaceDetails details) {
            this.details = details;
        }
    }

    private static class Status {
        private String status;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }
    }

    private static class DatabaseDetails {
        private String database;
        private String result;
        private String validationQuery;

        public String getDatabase() {
            return database;
        }

        public void setDatabase(String database) {
            this.database = database;
        }

        public String getResult() {
            return result;
        }

        public void setResult(String result) {
            this.result = result;
        }

        public String getValidationQuery() {
            return validationQuery;
        }

        public void setValidationQuery(String validationQuery) {
            this.validationQuery = validationQuery;
        }
    }

    private static class DiskSpaceDetails {
        private float total;
        private float free;
        private float threshold;

        public float getTotal() {
            return total;
        }

        public void setTotal(float total) {
            this.total = total;
        }

        public float getFree() {
            return free;
        }

        public void setFree(float free) {
            this.free = free;
        }

        public float getThreshold() {
            return threshold;
        }

        public void setThreshold(float threshold) {
            this.threshold = threshold;
        }
    }
}