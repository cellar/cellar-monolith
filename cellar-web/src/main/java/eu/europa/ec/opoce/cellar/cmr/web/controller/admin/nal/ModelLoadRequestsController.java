/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.nal
 *             FILE : ModelLoadRequestsController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 28, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.nal;

import eu.europa.ec.opoce.cellar.ExecutionStatus;
import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.admin.error.PageNotFoundController;
import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL;
import eu.europa.ec.opoce.cellar.modelload.domain.ModelLoadRequest;
import eu.europa.ec.opoce.cellar.modelload.service.impl.ModelLoadRequestServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

/**
 * <class_description> Add work (reindex) controller.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 28, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
public class ModelLoadRequestsController extends AbstractCellarController {

    private static final Logger LOG = LogManager.getLogger(ModelLoadRequestsController.class);

    public static final String PAGE_URL = "/cmr/nal/modelloadrequests";

    private static final String JSP_NAME = "cmr/nal/model-load-requests";

    private static final String EDIT_JSP_NAME = "cmr/nal/edit-model-load-requests";

    private static final String DATE_FORMAT = "dd/MM/yyyy";

    /** The aligner modelLoadRequest dao. */
    @Autowired
    private ModelLoadRequestServiceImpl modelLoadRequestService;

    /**
     * Handle the GET.
     */
    @RequestMapping(value = ModelLoadRequestsController.PAGE_URL, method = RequestMethod.GET)
    public String showModelLoadRequestGet(final ModelMap model) {
        model.addAttribute("submissionStatuss", ExecutionStatus.values());
        model.addAttribute("modelLoadRequests", this.modelLoadRequestService.findAll());
        return JSP_NAME;
    }

    /**
     * Handle the GET.
     */
    @RequestMapping(method = {
            RequestMethod.GET, RequestMethod.POST}, value = PAGE_URL + "/search")
    public ModelAndView searchMmodelLoadRequest(final HttpServletRequest request, final HttpServletResponse response) {
        ModelMap model = new ModelMap();
        String modelURI = request.getParameter("modelUri");
        String executionStatus = request.getParameter("subStatus");
        model.addAttribute("modelUri", modelURI);
        model.addAttribute("subStatus", executionStatus);
        model.addAttribute("submissionStatuss", ExecutionStatus.values());
        model.addAttribute("modelLoadRequests", this.modelLoadRequestService.find(modelURI, executionStatus));
        return new ModelAndView(JSP_NAME, model);
    }

    /**
     * Restart Model load
     * @throws ParseException 
     */
    @RequestMapping(method = {
            RequestMethod.GET, RequestMethod.POST}, value = PAGE_URL + "/restart")
    public String restartModelLoadRequest(final HttpServletRequest request, final HttpServletResponse response) throws ParseException {
        String id = request.getParameter("idSelected");
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        Date activationDate = sdf.parse(request.getParameter("activationDate"));
        this.modelLoadRequestService.restart(id, activationDate);
        super.addUserMessage(LEVEL.SUCCESS, "common.message.successfullyProcessed");
        LOG.info("The model '" + id + "' has been restarted");
        return redirect(PAGE_URL);
    }

    /**
     * Delete Model load
     */
    @RequestMapping(method = {
            RequestMethod.GET, RequestMethod.POST}, value = PAGE_URL + "/delete")
    public String deleteModelLoadRequest(final HttpServletRequest request, final HttpServletResponse response) {
        String id = request.getParameter("id");
        this.modelLoadRequestService.delete(id);
        super.addUserMessage(LEVEL.SUCCESS, "common.message.successfullyProcessed");
        return redirect(PAGE_URL);
    }

    /**
     * Update Model load
     */
    @RequestMapping(method = {
            RequestMethod.GET}, value = PAGE_URL + "/edit")
    public ModelAndView updateModelLoadRequestGet(final HttpServletRequest request, final HttpServletResponse response) {
        ModelMap model = new ModelMap();
        String id = request.getParameter("id");
        Optional<ModelLoadRequest> mlr = this.modelLoadRequestService.findById(id);
        if (!mlr.isPresent()) {
            return new ModelAndView(PageNotFoundController.ERROR_VIEW, model);
        }

        ModelLoadRequest modelLoadRequest = mlr.get();
        model.addAttribute("modelId", modelLoadRequest.getId());
        model.addAttribute("modelUri", modelLoadRequest.getModelURI());
        model.addAttribute("pid", modelLoadRequest.getExternalPid());

        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        String activationDate = sdf.format(modelLoadRequest.getActivationDate());
        model.addAttribute("activationDate", activationDate);
        model.addAttribute("subStatus", modelLoadRequest.getExecutionStatus().name());
        model.addAttribute("submissionStatuss", ExecutionStatus.values());

        return new ModelAndView(EDIT_JSP_NAME, model);
    }

    /**
     * Update Model load
     * @throws ParseException 
     */
    @RequestMapping(method = {
            RequestMethod.POST}, value = PAGE_URL + "/update")
    public String updateModelLoadRequestPost(final HttpServletRequest request, final HttpServletResponse response) throws ParseException {
        String id = request.getParameter("modelId");
        String modelURI = request.getParameter("modelUri");
        String pid = request.getParameter("pid");
        String executionStatus = request.getParameter("subStatus");
        String strActivationDate = request.getParameter("activationDate");
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        Date activationDate = sdf.parse(strActivationDate);
        this.modelLoadRequestService.update(id, modelURI, pid, executionStatus, activationDate);
        super.addUserMessage(LEVEL.SUCCESS, "common.message.successfullySaved");
        LOG.info("The model '" + id + "' has been updated (" + executionStatus + ")");
        return redirect(PAGE_URL);
    }

}
