/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.indexing
 *             FILE : IndexingAddTargetController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 28, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.indexing;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.indexing.command.AddTarget;
import eu.europa.ec.opoce.cellar.nal.service.LanguageService;
import eu.europa.ec.opoce.cellar.server.admin.indexing.IndexingInvoker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @author ARHS Developments
 */
@Controller
@RequestMapping(IndexingAddTargetController.PAGE_URL)
public class IndexingAddTargetController extends AbstractCellarController {

    public static final String PAGE_URL = "/cmr/indexing/add/target";
    private static final String JSP_NAME = "cmr/indexing/add-target";
    private static final String OBJECT_NAME = "target";

    private final IndexingInvoker indexingInvoker;

    private final Validator addTargetValidator;

    private final LanguageService languageService;

    @Autowired
    public IndexingAddTargetController(IndexingInvoker indexingInvoker, @Qualifier("indexingAddTargetValidator") Validator addTargetValidator,
                                       LanguageService languageService) {
        this.indexingInvoker = indexingInvoker;
        this.addTargetValidator = addTargetValidator;
        this.languageService = languageService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String addTarget(final ModelMap model) {
        AddTarget target = new AddTarget();
        target.setLanguages(getLanguages());
        model.addAttribute(OBJECT_NAME, target);
        return JSP_NAME;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String addTargetPost(@ModelAttribute(OBJECT_NAME) AddTarget addTarget, BindingResult bindingResult) {

        addTargetValidator.validate(addTarget, bindingResult);

        if (!bindingResult.hasErrors()) {
            try {
                indexingInvoker.addWork(addTarget.getWorkId(), addTarget.isGenerateIndexNotice(), addTarget.isGenerateEmbeddedNotice(),
                        addTarget.getLanguages());
                addUserMessage(LEVEL.SUCCESS, "common.message.successfullySaved");
                return redirect(PAGE_URL);
            } catch (Exception e) {
                // The work id doesn't exist, this is an item, ...
                addUserMessage(LEVEL.ERROR, null, e.getLocalizedMessage());
            }
        }
        return JSP_NAME;
    }

    @RequestMapping(method = RequestMethod.GET, path = "languages")
    public List<String> getLanguages() {
        return languageService.getRequiredLanguageCodes();
    }
}
