/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.unauthorized
 *        FILE : UserAccessRequestController.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 23-02-2023
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2023 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.unauthorized;

import eu.cec.digit.ecas.client.j2ee.tomcat.EcasPrincipal;
import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL;
import eu.europa.ec.opoce.cellar.cl.service.client.UserAccessRequestService;
import eu.europa.ec.opoce.cellar.security.support.AuthenticationDetailsHelper;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 
 * @author EUROPEAN DYNAMICS S.A.
 */
@Controller
@RequestMapping(value = UserAccessRequestController.PAGE_URL)
public class UserAccessRequestController extends AbstractCellarController {

	/**
	 * 
	 */
	private static final Logger LOG = LogManager.getLogger(UserAccessRequestController.class);
	/**
	 * 
	 */
	static final String PAGE_URL = "/unauthorized";
    /**
     * 
     */
	private static final String SELECT_ACTION_URL = "/select-action";
    /**
     * 
     */
	private static final String ACCESS_REQUESTED_URL = "/access-requested";
    /**
     * 
     */
    private static final String JSP_SELECT_ACTION = "unauthorized/select-action";
    /**
     * 
     */
    private static final String JSP_ACCESS_REQUESTED = "unauthorized/access-requested";
    /**
     * 
     */
    private static final String NOTIFICATION_EMAIL_SUCCESS_CODE = "user.access.request.notification.email.success";
    /**
     * 
     */
    private static final String NOTIFICATION_UAR_CREATION_SUCCESS_CODE = "user.access.request.notification.creation.success";
    /**
     * 
     */
    private static final String IS_SEND_NOTIFICATION_EMAIL_PARAM = "sendNotificationEmail";
    /**
     * 
     */
    private static final String ERR_UNAUTHENTICATED_OR_ANONYMOUS = "User is not authenticated or is anonymous.";
    /**
     * 
     */
    private UserAccessRequestService userAccessRequestService;
    
    @Autowired
    public UserAccessRequestController(UserAccessRequestService userAccessRequestService) {
		this.userAccessRequestService = userAccessRequestService;
	}

    
	@GetMapping(value = UserAccessRequestController.SELECT_ACTION_URL)
    public String selectAction(final HttpServletRequest request, final HttpServletResponse response, final Model model) {
		String username = request.getRemoteUser();
		if (username == null) {
			LOG.error(ERR_UNAUTHENTICATED_OR_ANONYMOUS);
			return redirectToAccessDenied();
		}
		// A user access request already exists for the currently logged-in user.
		if (this.userAccessRequestService.findByUsername(username) != null) {
			return redirectToAccessRequsted();
		}
        return JSP_SELECT_ACTION;
    }

    @PostMapping(value = UserAccessRequestController.SELECT_ACTION_URL)
    public String createUserAccessRequest(Authentication authentication, final HttpServletRequest request, final HttpServletResponse response, final Model model) {
    	EcasPrincipal principal = null;
    	try {
    		principal = AuthenticationDetailsHelper.getAuthenticationDetails(authentication).getPrincipalDetails();
    	}
    	catch (Exception e) {
    		LOG.error(ERR_UNAUTHENTICATED_OR_ANONYMOUS);
    		return redirectToAccessDenied();
    	}
    	
    	this.userAccessRequestService.createUserAccessRequest(principal.getUid(), principal.getEmail());
    	addUserMessage(LEVEL.SUCCESS, NOTIFICATION_UAR_CREATION_SUCCESS_CODE);
    	
    	String isSendNotificationEmail = request.getParameter(IS_SEND_NOTIFICATION_EMAIL_PARAM);
    	if (isSendNotificationEmail != null && isSendNotificationEmail.equals("on")) {
    		this.userAccessRequestService.notifyAdminsViaEmail(buildEmailModel(principal));
    		addUserMessage(LEVEL.SUCCESS, NOTIFICATION_EMAIL_SUCCESS_CODE);
    	}
    	return redirectToAccessRequsted();
    }
    
    @GetMapping(value = UserAccessRequestController.ACCESS_REQUESTED_URL)
    public String displayAccessRequested(final HttpServletRequest request, final HttpServletResponse response, final Model model) {
    	String username = request.getRemoteUser();
		if (username == null) {
			LOG.error(ERR_UNAUTHENTICATED_OR_ANONYMOUS);
			return redirectToAccessDenied();
		}
		// A user access request does not exist for the currently logged-in user.
    	if (this.userAccessRequestService.findByUsername(username) == null) {
			return redirectToSelectAction();
		}
        return JSP_ACCESS_REQUESTED;
    }
    
    @PostMapping(value = UserAccessRequestController.ACCESS_REQUESTED_URL)
    public String resendNotificationEmail(Authentication authentication, final HttpServletRequest request, final HttpServletResponse response, final Model model) {
    	EcasPrincipal principal = null;
    	try {
    		principal = AuthenticationDetailsHelper.getAuthenticationDetails(authentication).getPrincipalDetails();
    	}
    	catch (Exception e) {
    		LOG.error(ERR_UNAUTHENTICATED_OR_ANONYMOUS);
    		return redirectToAccessDenied();
    	}

    	this.userAccessRequestService.notifyAdminsViaEmail(buildEmailModel(principal));
    	addUserMessage(LEVEL.SUCCESS, NOTIFICATION_EMAIL_SUCCESS_CODE);
    	return redirectToAccessRequsted();
    }


    private Map<String, Object> buildEmailModel(EcasPrincipal principal) {
    	Map<String, Object> userInfo = new HashMap<>();
    	userInfo.put("euLoginId", principal.getUid());
    	userInfo.put("firstName", principal.getFirstName());
    	userInfo.put("lastName", principal.getLastName());
    	userInfo.put("email", principal.getEmail());
    	return userInfo;
    }
    
    private String redirectToAccessDenied() {
    	return redirect("/access-denied");
    }
    
    private String redirectToAccessRequsted() {
    	return redirect(UserAccessRequestController.PAGE_URL + UserAccessRequestController.ACCESS_REQUESTED_URL);
    }
    
    private String redirectToSelectAction() {
    	return redirect(UserAccessRequestController.PAGE_URL + UserAccessRequestController.SELECT_ACTION_URL);
    }
    
}
