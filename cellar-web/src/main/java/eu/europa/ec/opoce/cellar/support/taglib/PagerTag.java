package eu.europa.ec.opoce.cellar.support.taglib;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang.StringUtils;

/**
 * Pager JSP Tag.
 * 
 * @author omeurice
 * 
 */
public class PagerTag extends TagSupport {

    /**
     * Generated serial uid.
     */
    private static final long serialVersionUID = -2474611816099885476L;

    /**
     * The pager form Id.
     */
    private String pagerFormId;

    /**
     * Current page number.
     */
    private int currentPage;

    /**
     * Number of the results or lines or whatever per page.
     */
    private int resultsPerPage;

    /**
     * Total number of items (results,...).
     */
    private int totalResults;

    /**
     * Label for the 'Previous' link that may be internationalized.
     */
    private String previous;

    /**
     * Label for the 'Next' link that may be internationalized.
     */
    private String next;

    /**
     * Bean containing data to pass in parameter
     */
    private Map<String, Object> parameters;

    /**
     * Action that manages the pagination
     */
    private String action;

    /**
     * doStartTag method implementation.
     * 
     * @throws JspException
     *             May be raised if a problem occurs during tag generation.
     */
    @Override
    public int doStartTag() throws JspException {
        Pager pager = new Pager(currentPage, resultsPerPage, totalResults);
        List<PagerLink> links = pager.createPageLinks();
        JspWriter out = pageContext.getOut();
        String linkLabel = "";
        String className = "";
        if (StringUtils.isEmpty(pagerFormId)) {
            pagerFormId = "pager_form";
        }

        try {

            if (StringUtils.isNotEmpty(action)) {
                out.println("<form method=\"post\" action=\"" + action + "\" id=\"" + pagerFormId + "\" >");
            } else {
                out.println("<form method=\"post\" id=\"" + pagerFormId + "\" >");
            }

            // copy each parameters in hidden input
            if (parameters != null) {
                for (String key : parameters.keySet()) {
                    Object values = parameters.get(key);
                    if (values != null) {
                        if (values instanceof Iterable) {
                            for (Object value : (Iterable<?>) values) {
                                out.println("<input type=\"hidden\" name=\"" + key + "\" value=\"" + value.toString() + "\" />");
                            }
                        } else {
                            out.println("<input type=\"hidden\" name=\"" + key + "\" value=\"" + values.toString() + "\" />");
                        }
                    }
                }
            }

            out.println("<input type=\"hidden\" name=\"currentPage\" value=\"\" />");
            out.println("<input type=\"hidden\" name=\"resultsPerPage\" value=\"" + resultsPerPage + "\" />");

            if (links != null) {
                for (PagerLink link : links) {
                    if (Pager.PREV_LINK.equals(link.getLabel())) {
                        linkLabel = previous;
                        className = "prevNextPagerLink";
                    } else if (Pager.NEXT_LINK.equals(link.getLabel())) {
                        linkLabel = next;
                        className = "prevNextPagerLink";
                    } else {
                        linkLabel = link.getLabel();
                        className = "anyPagerLink";
                    }
                    if (currentPage == link.getValue()) {
                        out.println("<span class=\"currentPagerLink\">" + linkLabel + "</span>");
                    } else {
                        out.println("<span><a class=\"" + className + "\" " + "href=\"javascript:document.forms." + pagerFormId
                                + ".currentPage.value=" + "'" + link.getValue() + "';" + "document.forms." + pagerFormId + ".submit();\" >"
                                + linkLabel + "</a>" + "</span>");
                    }
                }
            }
            out.println("</form>");

        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }

    /**
     * doEndTag implementation.
     */
    @Override
    public int doEndTag() {
        return EVAL_PAGE;
    }

    /**
     * Get the current page.
     * 
     * @return The current page.
     */
    public int getCurrentPage() {
        return currentPage;
    }

    /**
     * Set the current page.
     * 
     * @param currentPage
     *            The page number to set.
     */
    public void setCurrentPage(final int currentPage) {
        this.currentPage = currentPage;
    }

    /**
     * Get the number of items per page.
     * 
     * @return The number of items per page.
     */
    public int getResultsPerPage() {
        return resultsPerPage;
    }

    /**
     * Set the number of items per page.
     * 
     * @param resultsPerPage
     *            The number of items per page to be set.
     */
    public void setResultsPerPage(final int resultsPerPage) {
        this.resultsPerPage = resultsPerPage;
    }

    /**
     * Get the total number of items.
     * 
     * @return The total number of items.
     */
    public int getTotalResults() {
        return totalResults;
    }

    /**
     * Set the total number of items.
     * 
     * @param totalResults
     *            The total number of items to set.
     */
    public void setTotalResults(final int totalResults) {
        this.totalResults = totalResults;
    }

    /**
     * Get the label for the 'Previous' link.
     * 
     * @return The label of the 'Previous' link.
     */
    public String getPrevious() {
        return previous;
    }

    /**
     * Set the label for the 'Previous' link.
     * 
     * @param previous
     *            The label to set.
     */
    public void setPrevious(final String previous) {
        this.previous = previous;
    }

    /**
     * Get the label for the 'Next' link.
     * 
     * @return The label of the 'Next' link.
     */
    public String getNext() {
        return next;
    }

    /**
     * Set the label for the 'Next' link.
     * 
     * @param next
     *            The label to set.
     */
    public void setNext(final String next) {
        this.next = next;
    }

    /**
     * @return the action
     */
    public String getAction() {
        return action;
    }

    /**
     * Set the action to be performed on user click.
     * @param action Action name to set.
     */
    public void setAction(final String action) {
        this.action = action;
    }

    /**
     * @return the parameters
     */
    public Map<String, Object> getParameters() {
        return parameters;
    }

    /**
     * @param parameters the parameters to set
     */
    public void setParameters(Map<String, Object> parameters) {
        this.parameters = parameters;
    }

    /**
     * @return the action
     */
    public String getPagerFormId() {
        return pagerFormId;
    }

    /**
     * Set the form id.
     * @param formId to set.
     */
    public void setPagerFormId(final String pagerFormId) {
        this.pagerFormId = pagerFormId;
    }

}
