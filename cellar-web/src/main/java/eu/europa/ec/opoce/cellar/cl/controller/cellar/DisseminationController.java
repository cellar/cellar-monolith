package eu.europa.ec.opoce.cellar.cl.controller.cellar;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.util.ControllerUtil;
import eu.europa.ec.opoce.cellar.exception.NotImplementedExceptionBuilder;

@Controller
public class DisseminationController extends AbstractCellarController {

    /**
     * Handler for the APPLY operation (=dissemination).
     *
     * @param uuid
     * @return
     */
    @RequestMapping(method = RequestMethod.GET, value = "/apply/{uuid}/{args}")
    public ResponseEntity<String> apply(@PathVariable final String uuid) {
        return ControllerUtil.makeErrorResponse(NotImplementedExceptionBuilder.get().build());
    }

}
