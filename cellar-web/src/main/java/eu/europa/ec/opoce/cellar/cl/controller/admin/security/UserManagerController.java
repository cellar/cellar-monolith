/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.security
 *             FILE : UserController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 10, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.controller.admin.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL;
import eu.europa.ec.opoce.cellar.cl.domain.admin.User;
import eu.europa.ec.opoce.cellar.cl.service.client.SecurityService;

/**
 * <class_description> User manager controller.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 10, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
@RequestMapping(value = UserManagerController.PAGE_URL)
public class UserManagerController extends AbstractCellarController {

    public static final String PAGE_URL = "/security/user";

    public static final String DELETE_URL = "/delete";

    private static final String JSP_NAME = "security/user";

    /**
     * Security service manager.
     */
    @Autowired
    private SecurityService securityService;

    /**
     * Handle the GET.
     */
    @RequestMapping(method = RequestMethod.GET)
    public String userManagerGet(final ModelMap model) {

        model.addAttribute("users", this.securityService.findAllUsers()); // gets all users
        return JSP_NAME;
    }

    /**
     * Handle the delete GET.
     */
    @RequestMapping(value = UserManagerController.DELETE_URL, method = RequestMethod.GET)
    public String userManagerDelete(@RequestParam(required = true) Long id) {
        boolean success = false;
        boolean valid = validateUserDeletion(id);
        if(valid) {
            this.securityService.deleteUser(id); // deletes user
            super.addUserMessage(LEVEL.SUCCESS, "common.message.successfullyProcessed");
        }
        return redirect(UserManagerController.PAGE_URL); // redirects to the user manager
    }

    /**
     * Verifies if the user is attempting to delete himself
     * Adds a user message in case of error that should be displayed in the screen
     * @param id the id of the user being deleted
     * @return true if the logged in user is different than the one being deleted
     */
    private boolean validateUserDeletion(Long id) {
        boolean errorFound = false;

        User userToBeDeleted = securityService.findUser(id);
        if(userToBeDeleted!=null){
            String toBeDeleted = userToBeDeleted.getUsername();
            SecurityContext securityContext = SecurityContextHolder.getContext();
            if(securityContext!=null){
                Authentication authentication = securityContext.getAuthentication();
                if(authentication!=null) {
                    String currentPrincipalName = authentication.getName();
                    if(toBeDeleted.equalsIgnoreCase(currentPrincipalName)){
                        super.addUserMessage(LEVEL.ERROR, "security.user.delete.himself.error");
                        errorFound = true;
                    }
                }
            }
        }

        return !errorFound;
    }
}
