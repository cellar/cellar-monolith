/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.indexing.validator
 *             FILE : IndexationSchedulerFormValidator.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 20 sept. 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2016 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.indexing.validator;

import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.indexing.IndexationSchedulerForm;

import org.apache.commons.lang.StringUtils;
import org.quartz.CronExpression;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * The Class IndexationSchedulerFormValidator.
 * <class_description> This class is the implementation of the validation for the form used in the Indexation Scheduler.
 * <br/><br/>
 * <notes>
 * <br/><br/>
 * ON : 20 sept. 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component("indexationSchedulerFormValidator")
public class IndexationSchedulerFormValidator implements Validator {

    /** {@inheritDoc} */
    @Override
    public boolean supports(final Class<?> clazz) {
        return IndexationSchedulerForm.class.isAssignableFrom(clazz);
    }

    /** {@inheritDoc} */
    @Override
    public void validate(final Object target, final Errors errors) {
        final IndexationSchedulerForm indexationSchedulerForm = (IndexationSchedulerForm) target;
        final boolean chooseAllContent = indexationSchedulerForm.isChooseAllContent();
        final boolean chooseAllContentForPeriod = indexationSchedulerForm.isChooseAllContentForPeriod();
        final String period = indexationSchedulerForm.getPeriod();
        final boolean useSparql = indexationSchedulerForm.isUseSparql();
        final String sparqlQuery = indexationSchedulerForm.getSparqlQuery();
        final String duration = indexationSchedulerForm.getDuration();
        final String cronExpression = indexationSchedulerForm.getCronExpression();
        // no choice made
        if (!chooseAllContent && !chooseAllContentForPeriod && !useSparql) {
            errors.rejectValue("useSparql", "required.cmr.indexing.scheduler.multipleChoice.none");
            errors.rejectValue("chooseAllContentForPeriod", "required.cmr.indexing.scheduler.multipleChoice.none");
            errors.rejectValue("chooseAllContent", "required.cmr.indexing.scheduler.multipleChoice.none");
        }
        // multiple choices active
        if (chooseAllContent && chooseAllContentForPeriod && useSparql) {
            errors.rejectValue("useSparql", "required.cmr.indexing.scheduler.multipleChoice.multiple");
            errors.rejectValue("chooseAllContentForPeriod", "required.cmr.indexing.scheduler.multipleChoice.multiple");
            errors.rejectValue("chooseAllContent", "required.cmr.indexing.scheduler.multipleChoice.multiple");
        }
        //period is blank or non numeric
        if (chooseAllContentForPeriod && (StringUtils.isBlank(period) || !StringUtils.isNumeric(period))) {
            errors.rejectValue("period", "required.cmr.indexing.scheduler.chooseAllContentForPeriod");
        }
        //duration is blank or non numeric
        if (StringUtils.isBlank(duration) || !StringUtils.isNumeric(duration)) {
            errors.rejectValue("duration", "required.cmr.indexing.scheduler.duration");
        }
        //cron is blank or non valid
        if (StringUtils.isBlank(cronExpression) || !CronExpression.isValidExpression(cronExpression)) {
            errors.rejectValue("cronExpression", "required.cmr.indexing.scheduler.cronExpression");
        }
        //sparql is blank
        if (useSparql) {
            if (StringUtils.isBlank(sparqlQuery)) {
                errors.rejectValue("sparqlQuery", "required.cmr.indexing.scheduler.useSparql");
            }
        }

    }

}
