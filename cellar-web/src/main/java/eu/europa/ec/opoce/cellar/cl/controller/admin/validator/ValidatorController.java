package eu.europa.ec.opoce.cellar.cl.controller.admin.validator;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.domain.admin.ValidatorRule;
import eu.europa.ec.opoce.cellar.cl.service.client.ValidatorRuleService;

@Controller(value = "validatorController")
@RequestMapping("/validator")
public class ValidatorController extends AbstractCellarController {

    private @Autowired ValidatorRuleService validatorRuleService;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView list(HttpServletRequest request, HttpServletResponse response) throws Exception {

        List<ValidatorRule> systemValidatorList = new LinkedList<ValidatorRule>();

        addSystemValidatorList(systemValidatorList);

        Map<String, Object> map = new HashMap<String, Object>();
        map.put("systemValidators", systemValidatorList);

        return new ModelAndView("validator", map);
    }

    /**
     * Update a rule accordingly with the persistent instance created from the command parameter.
     * 
     * @param rule
     *            the command parameter
     * @return redirect to the list of rules
     */
    @RequestMapping("/update")
    public @ResponseBody Map<String, ? extends Object> update(final ValidatorRule rule) {
        System.out.println("UPDATE this rule = " + rule.getValidatorClass() + " - " + rule.isEnabled());
        validatorRuleService.updateRule(rule);
        return Collections.singletonMap("" + rule.getId(), rule);

    }

    /**
     * Toggle the enabled value of this rule accordingly with the persistent instance created from the command parameter.
     * 
     * @param rule
     *            the command parameter
     * @return redirect to the list of rules
     */
    @RequestMapping("/toggleEnabled")
    public @ResponseBody Map<String, ? extends Object> toggleCheck(final ValidatorRule rule) {
        System.out.println("TOGGLE this rule = " + rule.getValidatorClass() + " - " + rule.isEnabled());
        rule.setEnabled(!rule.isEnabled());
        validatorRuleService.updateRule(rule);
        return Collections.singletonMap("check", rule.isEnabled());

    }

    /**
     * Delete a rule accordingly with the persistent instance created from the command parameter.
     * 
     * @param rule
     *            the command parameter
     * @return redirect to the list of rules
     */
    @RequestMapping("/delete")
    public ModelAndView delete(final ValidatorRule rule) {
        System.out.println("DELETE this rule = " + rule);
        validatorRuleService.deleteRule(rule);
        return new ModelAndView("redirect:");
    }

    private void addSystemValidatorList(List<ValidatorRule> systemValidatorList) {
        systemValidatorList.addAll(validatorRuleService.getSystemValidators());
    }

}
