/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.indexing.form
 *             FILE : IndexingSchedulerController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 20 sept. 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2016 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.indexing.form;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL;
import eu.europa.ec.opoce.cellar.cl.domain.index.ScheduledReindexation;
import eu.europa.ec.opoce.cellar.cl.service.AutomaticReindexationService;
import eu.europa.ec.opoce.cellar.cl.service.ScheduledReindexationService;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.indexing.IndexationSchedulerForm;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.indexing.validator.IndexationSchedulerFormValidator;
import eu.europa.ec.opoce.cellar.logging.LogContext;
import eu.europa.ec.opoce.cellar.semantic.json.JacksonUtils;
import eu.europa.ec.opoce.cellar.server.admin.sparql.Query;
import eu.europa.ec.opoce.cellar.support.SparqlUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.CronExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.util.Date;
import java.util.LinkedList;

import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.CMR_RE_INDEXING;

/**
 * The Class IndexingSchedulerController.
 * <class_description> This class is the controller that should handle all possible http calls related to the indexation scheduler.
 * <br/><br/>
 * ON : 20 sept. 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
@Transactional
public class IndexingSchedulerController extends AbstractCellarController {

    private static final Logger LOG = LogManager.getLogger(IndexingSchedulerController.class);

    /** The indexation scheduler form validator. */
    @Autowired
    private IndexationSchedulerFormValidator indexationSchedulerFormValidator;

    /** The scheduled reindexation service. */
    @Autowired
    private ScheduledReindexationService scheduledReindexationService;

    /** The automatic reindexation service. */
    @Autowired
    private AutomaticReindexationService automaticReindexationService;

    /**
     * Show indexation scheduler form.
     *
     * @param model the model
     * @return the string
     */
    @RequestMapping(value = "/cmr/indexing/scheduler", method = RequestMethod.GET)
    @LogContext(CMR_RE_INDEXING)
    public String showIndexationSchedulerForm(final ModelMap model) {
        ScheduledReindexation scheduledReindexationConfiguration = scheduledReindexationService
                .getScheduledReindexationConfiguration();
        if (scheduledReindexationConfiguration == null) {
            scheduledReindexationConfiguration = new ScheduledReindexation();
        }

        final IndexationSchedulerForm indexationSchedulerForm = buildForm(scheduledReindexationConfiguration);

        model.addAttribute("indexationSchedulerForm", indexationSchedulerForm);

        final LinkedList<String> nextExecutions = getNextExecutionsSet(scheduledReindexationConfiguration);
        model.addAttribute("nextExecutions", nextExecutions);

        return "cmr/indexing/scheduler";
    }

    /**
     * Update configuration.
     *
     * @param indexationSchedulerForm the indexation scheduler form
     * @param bindingResult the binding result
     * @param status the status
     * @param model the model
     * @return the string
     */
    @RequestMapping(value = "/cmr/indexing/scheduler", method = RequestMethod.POST)
    @LogContext(CMR_RE_INDEXING)
    public String updateConfiguration(@ModelAttribute("indexationSchedulerForm") final IndexationSchedulerForm indexationSchedulerForm,
                                      final BindingResult bindingResult, final SessionStatus status, final ModelMap model) {
        indexationSchedulerFormValidator.validate(indexationSchedulerForm, bindingResult);
        ScheduledReindexation scheduledReindexationConfiguration = scheduledReindexationService
                .getScheduledReindexationConfiguration();
        if (scheduledReindexationConfiguration == null) {
            scheduledReindexationConfiguration = new ScheduledReindexation();
        }
        if (!bindingResult.hasErrors()) {
            updateConfiguration(indexationSchedulerForm, scheduledReindexationConfiguration);
            if (scheduledReindexationConfiguration.isEnabled()) {
                automaticReindexationService.enable();
            } else {
                automaticReindexationService.disable();
            }
            addUserMessage(LEVEL.SUCCESS, "common.message.successfullySaved");
        } else {
            addUserMessage(LEVEL.WARNING, "common.message.unsuccessfullySaved");
        }

        final LinkedList<String> nextExecutions = getNextExecutionsSet(scheduledReindexationConfiguration);
        model.addAttribute("nextExecutions", nextExecutions);

        return "cmr/indexing/scheduler";
    }

    /**
     * Stop running thread.
     *
     * @return the string
     */
    @RequestMapping(value = "/cmr/indexing/scheduler/stop", method = RequestMethod.GET)
    @LogContext(CMR_RE_INDEXING)
    public String stopRunningThread() {
        if (automaticReindexationService.isRunning()) {
            automaticReindexationService.stop();
            addUserMessage(LEVEL.INFO, "cmr.indexing.scheduler.stopped");
        } else {
            LOG.info("The reindexing thread is not running.");
            addUserMessage(LEVEL.WARNING, "cmr.indexing.scheduler.not.running");
        }
        return redirect("/cmr/indexing/scheduler");
    }

    /**
     * Pause running thread.
     *
     * @return the string
     */
    @RequestMapping(value = "/cmr/indexing/scheduler/pause", method = RequestMethod.GET)
    @LogContext(CMR_RE_INDEXING)
    public String pauseRunningThread() {
        if (automaticReindexationService.isRunning()) {
            automaticReindexationService.pause();
            addUserMessage(LEVEL.INFO, "cmr.indexing.scheduler.paused");
        } else {
            LOG.info("The reindexing thread is not running.");
            addUserMessage(LEVEL.INFO, "cmr.indexing.scheduler.not.running");
            addUserMessage(LEVEL.WARNING, "cmr.indexing.scheduler.not.paused");
        }
        return redirect("/cmr/indexing/scheduler");
    }

    /**
     * Resumed paused thread.
     *
     * @return the string
     */
    @RequestMapping(value = "/cmr/indexing/scheduler/resume", method = RequestMethod.GET)
    @LogContext(CMR_RE_INDEXING)
    public String resumedPausedThread() {
        if (automaticReindexationService.isPaused()) {
            automaticReindexationService.resume();
            addUserMessage(LEVEL.INFO, "cmr.indexing.scheduler.resumed");
        } else {
            LOG.info("The reindexing thread is not running.");
            addUserMessage(LEVEL.WARNING, "cmr.indexing.scheduler.not.running");
        }
        return redirect("/cmr/indexing/scheduler");
    }

    /**
     * Disable scheduler.
     *
     * @return the string
     */
    @RequestMapping(value = "/cmr/indexing/scheduler/shutdown", method = RequestMethod.GET)
    @LogContext(CMR_RE_INDEXING)
    public String disableScheduler() {
        automaticReindexationService.disable();
        return redirect("/cmr/indexing/scheduler");
    }

    /**
     * Disable scheduler.
     *
     * @return the string
     */
    @RequestMapping(value = "/cmr/indexing/scheduler/reschedule", method = RequestMethod.GET)
    @LogContext(CMR_RE_INDEXING)
    public String enableScheduler() {
        final ScheduledReindexation scheduledReindexationConfiguration = scheduledReindexationService
                .getScheduledReindexationConfiguration();

        if (scheduledReindexationConfiguration != null) {
            if (scheduledReindexationService.isValid(scheduledReindexationConfiguration)) {
                automaticReindexationService.enable();
                addUserMessage(LEVEL.WARNING, "cmr.indexing.scheduler.enabled");
            } else {
                addUserMessage(LEVEL.WARNING, "cmr.indexing.scheduler.enabled.errors");
            }
        } else {
            addUserMessage(LEVEL.WARNING, "cmr.indexing.scheduler.notFound");
        }
        return redirect("/cmr/indexing/scheduler");
    }

    /**
     * Gets the queries.
     *
     * @param request the request
     * @param response the response
     * @return the queries
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @RequestMapping(value = "/cmr/indexing/scheduler/getQueries", method = RequestMethod.GET)
    public void getQueries(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        final Query[] queries = this.automaticReindexationService.getExampleQueries();
        JacksonUtils.streamAsJson(response.getOutputStream(), queries, true);
    }

    /**
     * Gets the query.
     *
     * @param request the request
     * @param response the response
     * @param queryId the query id
     * @return the query
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @RequestMapping(value = "/cmr/indexing/scheduler/getQuery", method = RequestMethod.POST)
    public void getQuery(final HttpServletRequest request, final HttpServletResponse response,
            @RequestParam(value = "q") final String queryId) throws IOException {
        final String query = this.automaticReindexationService.getQuery(queryId);
        response.setContentType(SparqlUtils.CONTENT_TYPE_QUERY);
        response.getOutputStream().write(query.getBytes(StandardCharsets.UTF_8));
        response.getOutputStream().flush();
    }

    /**
     * Update configuration.
     *
     * @param indexationSchedulerForm the indexation scheduler form
     * @param scheduledReindexationConfiguration the scheduled reindexation configuration
     */
    private void updateConfiguration(final IndexationSchedulerForm indexationSchedulerForm,
            final ScheduledReindexation scheduledReindexationConfiguration) {
        scheduledReindexationConfiguration.setAllEligibleContent(indexationSchedulerForm.isChooseAllContent());
        scheduledReindexationConfiguration.setAllEligibleContentWithinPeriod(indexationSchedulerForm.isChooseAllContentForPeriod());
        scheduledReindexationConfiguration.setUseSparql(indexationSchedulerForm.isUseSparql());
        scheduledReindexationConfiguration.setCron(indexationSchedulerForm.getCronExpression());
        if (StringUtils.isNotBlank(indexationSchedulerForm.getDuration())) {
            final String duration = indexationSchedulerForm.getDuration() + indexationSchedulerForm.getDurationUnitOfTime();
            scheduledReindexationConfiguration.setDuration(duration);
        }
        scheduledReindexationConfiguration.setEnabled(indexationSchedulerForm.isEnabled());
        if (StringUtils.isNotBlank(indexationSchedulerForm.getPeriod())) {
            scheduledReindexationConfiguration.setPeriod(indexationSchedulerForm.getPeriod() + indexationSchedulerForm.getPeriodUnitOfTime());
        }
        scheduledReindexationConfiguration.setSparqlQuery(indexationSchedulerForm.getSparqlQuery());
        this.scheduledReindexationService.saveOrUpdate(scheduledReindexationConfiguration);
        LOG.info("Configuration saved: {}", scheduledReindexationConfiguration);
    }

    /**
     * Gets the next executions set.
     *
     * @param scheduledReindexationConfiguration the scheduled reindexation configuration
     * @return the next executions set
     */
    private LinkedList<String> getNextExecutionsSet(final ScheduledReindexation scheduledReindexationConfiguration) {
        final LinkedList<String> nextExecutions = new LinkedList<>();
        final String cronString = scheduledReindexationConfiguration.getCron();
        if (StringUtils.isNotBlank(cronString)) {
            try {
                Date targetDate = new Date();

                final CronExpression cron = new CronExpression(cronString);
                //list the next 5 triggering dates
                for (int i = 0; i < 5; i++) {
                    targetDate = cron.getNextValidTimeAfter(targetDate);
                    final String format = DateFormatUtils.ISO_DATETIME_FORMAT.format(targetDate);
                    nextExecutions.add(format);
                }
            } catch (final ParseException e) {
                LOG.error("Unexpected exception was thrown", e);
                addUserMessage(LEVEL.ERROR, "cmr.indexing.scheduler.form.table.error");
            }
        }
        return nextExecutions;
    }

    /**
     * Builds the form.
     *
     * @param scheduledReindexationConfiguration the scheduled reindexation configuration
     * @return the indexation scheduler form
     */
    private IndexationSchedulerForm buildForm(final ScheduledReindexation scheduledReindexationConfiguration) {
        final IndexationSchedulerForm result = new IndexationSchedulerForm();

        result.setChooseAllContent(scheduledReindexationConfiguration.isAllEligibleContent());
        result.setChooseAllContentForPeriod(scheduledReindexationConfiguration.isAllEligibleContentWithinPeriod());
        result.setUseSparql(scheduledReindexationConfiguration.isUseSparql());

        result.setCronExpression(scheduledReindexationConfiguration.getCron());

        final String duration = scheduledReindexationConfiguration.getDuration();
        if (StringUtils.isNotBlank(duration)) {
            final int length = duration.length();
            final String amount = duration.substring(0, length - 1);
            final String unitOfTime = duration.substring(length - 1, length);
            result.setDuration(amount);
            result.setDurationUnitOfTime(unitOfTime);
        }

        result.setEnabled(scheduledReindexationConfiguration.isEnabled());

        final String period = scheduledReindexationConfiguration.getPeriod();
        if (StringUtils.isNotBlank(period)) {
            final int length = period.length();
            final String amount = period.substring(0, length - 1);
            final String unitOfTime = period.substring(length - 1, length);
            result.setPeriod(amount);
            result.setPeriodUnitOfTime(unitOfTime);
        }

        result.setSparqlQuery(scheduledReindexationConfiguration.getSparqlQuery());
        return result;
    }

}
