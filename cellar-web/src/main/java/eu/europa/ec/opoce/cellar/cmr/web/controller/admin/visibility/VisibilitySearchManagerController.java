/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin.visibility
 *             FILE : VisibilityManagerController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 18, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.visibility;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarResponseController;
import eu.europa.ec.opoce.cellar.cl.controller.message.ICellarUserMessage.LEVEL;
import eu.europa.ec.opoce.cellar.cl.service.client.VisibilityTreeService;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement;
import eu.europa.ec.opoce.cellar.cmr.scheduling.Scheduler;
import eu.europa.ec.opoce.cellar.cmr.service.EmbargoService;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.command.SearchObject;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Date;

/**
 * <class_description> Visibility tree controller.
 * <p>
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 18, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Controller
@RequestMapping(VisibilitySearchManagerController.PAGE_URL)
public class VisibilitySearchManagerController extends AbstractCellarResponseController {

    public static final String PAGE_URL = "/cmr/visibility/search";
    private static final String JSP_NAME = "cmr/visibility/search";
    private final static String CONTENT_TYPE_TEXT = "text/plain";
    private static final String EMBARGO_URL = "/embargo";
    private static final String DISEMBARGO_URL = "/disembargo";
    private static final String OBJECT_NAME = "search";

    private final VisibilityTreeService visibilityTreeService;
    private final EmbargoService embargoService;
    @Autowired
    private Scheduler scheduler;

    @Autowired
    public VisibilitySearchManagerController(VisibilityTreeService visibilityTreeService, EmbargoService embargoService) {
        this.visibilityTreeService = visibilityTreeService;
        this.embargoService = embargoService;
    }

    /**
     * Handle the GET.
     */
    @RequestMapping(method = RequestMethod.GET)
    public String visibilitySearchManagerGet(final ModelMap model, @RequestParam(required = false) final String objectId) {

        final SearchObject searchObject = new SearchObject(objectId);
        model.addAttribute(OBJECT_NAME, searchObject);

        if (StringUtils.isNotBlank(objectId)) { // we want the tree
            try {
                final MetsElement element = this.visibilityTreeService.getHierarchy(objectId);
                model.addAttribute("hierarchy", element);
                if (element == null) { // tree doesn't exist
                    super.addUserMessage(LEVEL.ERROR, "cmr.visibility.element.notExist", "The element does not exist.");
                }
            } catch (final Exception exception) {
                super.addUserMessage(LEVEL.ERROR, null, exception.getLocalizedMessage());
            }
        }

        return JSP_NAME;
    }

    /**
     * Handle the embargoing GET.
     */
    @RequestMapping(value = VisibilitySearchManagerController.EMBARGO_URL, method = RequestMethod.GET)
    public void embargoGet(final HttpServletRequest request, final HttpServletResponse response,
                           @RequestParam(value = "cellarId") final String cellarId,
                           @RequestParam(value = "dateOfEmbargo") @DateTimeFormat(pattern = "yyyyMMddHHmm") Date dateOfEmbargo)
            throws IOException {
        response.setContentType(CONTENT_TYPE_TEXT);
        try {
            this.embargoService.changeEmbargo(cellarId, dateOfEmbargo);
            scheduler.reschedule(false);
        } catch (Exception exception) {
            response.getOutputStream().write(exception.getMessage().getBytes(StandardCharsets.UTF_8));
        }
        response.getOutputStream().flush(); // return the error or ""
    }

    /**
     * Handle the disembargoing GET.
     */
    @RequestMapping(value = VisibilitySearchManagerController.DISEMBARGO_URL, method = RequestMethod.GET)
    public void disembargoGet(final HttpServletRequest request, final HttpServletResponse response,
                              @RequestParam(value = "cellarId") final String cellarId) throws IOException {
        response.setContentType(CONTENT_TYPE_TEXT);
        try {
            this.embargoService.changeEmbargo(cellarId, new Date());
            scheduler.reschedule(false);

        } catch (final Exception exception) {
            response.getOutputStream().write(exception.getMessage().getBytes(StandardCharsets.UTF_8));
        }
        response.getOutputStream().flush(); // return the error or ""
    }
}
