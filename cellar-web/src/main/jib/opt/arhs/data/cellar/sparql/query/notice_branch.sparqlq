CONSTRUCT  
   {
     ?cellarW a ?objectType .
     ?cellarW owl:sameAs ?anyPsW .
     ?cellarW cdm:embargo ?embargo .
     ?cellarW cmr:metsStructSubDiv ?cellarE .
     ?cellarE owl:sameAs ?anyPsE .
     ?cellarE cdm:expression_uses_language ?language .
     ?cellarE cmr:lang ?langIsoCode .
     ?cellarE cmr:metsStructSubDiv ?cellarM .
     ?cellarM owl:sameAs ?anyPsM .
     ?cellarM cmr:metsStructSubDiv ?cellarI .
     ?cellarI cmr:manifestationMimeType ?mime .
   }
WHERE 
{
# make one of the LET statements depending whether it is a cellar or production system URI (contains "resource/cellar/" or "resource/{PS}/"
# for language, 2 or 3 char ISO code may be used
   {
      LET ( ?psW := <http://publications.europa.eu/resource/celex/32006D0241> ) 
      LET ( ?langIsoCode := "deu"^^<http://www.w3.org/2001/XMLSchema#language> )
   } .
   {
      LET ( ?cellarW := <http://publications.europa.eu/resource/cellar/14f51b65-573e-430d-aad1-cc5c107d3a28> ) 
      LET ( ?langIsoCode := "deu"^^<http://www.w3.org/2001/XMLSchema#language> )
   } .
   {
      LET ( ?psE := <http://publications.europa.eu/resource/celex/32006D0241.ger> ) 
   } .
   {
      LET ( ?cellarE := <http://publications.europa.eu/resource/cellar/14f51b65-573e-430d-aad1-cc5c107d3a28.0002> ) 
   } .
#   ?psW a ?objectType .    <<< was error in some cases
   ?anyPsW a ?objectType .
   ?objectType cmr:metsDivType ?dt .
# the following pattern is only relevant and required if a PS work URI (psW) is given on input  
#   { ?cellarW owl:sameAs ?psW .  FILTER (regex(str(?cellarW),"resource/cellar") && (?cellarW != ?psW) ) } .
   { ?cellarW owl:sameAs ?anyPsW .  FILTER (regex(str(?cellarW),"resource/cellar") && (?cellarW != ?anyPsW) ) } .
   OPTIONAL { ?anyPsW cdm:embargo ?embargo . } .
   ?cellarW cmr:metsStructSubDiv ?cellarE .
# the following pattern is only relevant and required if a PS expression URI (psE) is given on input  
   { ?cellarE owl:sameAs ?psE .  FILTER (regex(str(?cellarE),"resource/cellar") && (?cellarE != ?psE) ) } .
   { ?cellarE owl:sameAs ?anyPsE .  FILTER (regex(str(?cellarE),"resource/cellar") && (?cellarE != ?anyPsE) ) } .
   ?cellarE cmr:lang ?langIsoCode  .
   ?anyPsE cdm:expression_uses_language ?language .
# optional because expression need not have manifestations
   OPTIONAL {
      ?cellarE cmr:metsStructSubDiv ?cellarM .
      { ?cellarM owl:sameAs ?anyPsM .   FILTER (regex(str(?cellarM),"resource/cellar") && (?cellarM != ?anyPsM) ) } .
# manifestation must have at least 1 item - requires that item triples are generated during ingestion
      ?cellarM cmr:metsStructSubDiv ?cellarI .
      { ?cellarI owl:sameAs ?anyPsI .   FILTER (regex(str(?cellarI),"resource/cellar") && (?cellarI != ?anyPsI) ) } .
      ?cellarI cmr:manifestationMimeType ?mime .
      } 
   }
