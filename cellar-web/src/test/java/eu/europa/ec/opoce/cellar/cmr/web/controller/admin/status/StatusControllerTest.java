package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.status;

import eu.europa.ec.opoce.cellar.ccr.enums.status.StatusType;
import eu.europa.ec.opoce.cellar.ccr.service.client.StatusService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpStatusCodeException;

import java.io.IOException;
import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * @author EUROPEAN DYNAMICS S.A.
 */
public class StatusControllerTest {

    @Mock
    private StatusService statusService;

    @InjectMocks
    private StatusController statusController;

    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetStatusPackageLevel_Success() {
        // Mocking the required data
        String id = "some-uuid";
        boolean ingestion = true;
        boolean indexation = false;
        int verbosity = 2;
        String expectedResponse = "Package Level Status Response";
        when(statusService.getStatusResponse(id, ingestion, indexation, null, verbosity, StatusType.PACKAGE_LEVEL))
                .thenReturn(expectedResponse);

        // Test the getStatusPackageLevel endpoint
        ResponseEntity<String> responseEntity = statusController.getStatusPackageLevel(id, ingestion, indexation, verbosity);

        // Verify the method calls and the response
        verify(statusService).getStatusResponse(id, ingestion, indexation, null, verbosity, StatusType.PACKAGE_LEVEL);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
    }

    @Test
    public void testGetStatusObjectLevel_Success() {
        // Mocking the required data
        String id = "some-pid";
        boolean ingestion = true;
        boolean indexation = false;
        int verbosity = 2;
        LocalDateTime datetime = LocalDateTime.of(2023, 8, 8, 8, 26, 49);
        String expectedResponse = "Object Level Status Response";
        when(statusService.getStatusResponse(id, ingestion, indexation, datetime, verbosity, StatusType.OBJECT_LEVEL))
                .thenReturn(expectedResponse);

        // Test the getStatusObjectLevel endpoint
        ResponseEntity<String> responseEntity = statusController.getStatusObjectLevel(id, ingestion, indexation, datetime, verbosity);

        // Verify the method calls and the response
        verify(statusService).getStatusResponse(id, ingestion, indexation, datetime, verbosity, StatusType.OBJECT_LEVEL);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
    }

    @Test
    public void testGetStatusLookup_Success() {
        // Mocking the required data
        String packageName = "some-package-name";
        String expectedResponse = "Lookup Status Response";
        when(statusService.getStatusResponse(packageName, false, false, null, 0, StatusType.LOOKUP))
                .thenReturn(expectedResponse);

        // Test the getStatusLookup endpoint
        ResponseEntity<String> responseEntity = statusController.getStatusLookup(packageName);

        // Verify the method calls and the response
        verify(statusService).getStatusResponse(packageName, false, false, null, 0, StatusType.LOOKUP);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(expectedResponse, responseEntity.getBody());
    }

    @Test
    public void testGetStatusPackageLevel_UnsupportedStatusType() {
        // Mocking the required data
        String id = "some-uuid";
        boolean ingestion = true;
        boolean indexation = false;
        int verbosity = 2;
        when(statusService.getStatusResponse(id, ingestion, indexation, null, verbosity, StatusType.PACKAGE_LEVEL))
                .thenThrow(new RuntimeException("Unsupported StatusType"));

        // Test the getStatusPackageLevel endpoint with unsupported StatusType
        ResponseEntity<String> responseEntity = statusController.getStatusPackageLevel(id, ingestion, indexation, verbosity);

        // Verify the method calls and the response
        verify(statusService).getStatusResponse(id, ingestion, indexation, null, verbosity, StatusType.PACKAGE_LEVEL);
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getStatusCode());
    }

    @Test
    public void testGetStatusServiceException_HttpStatusCodeException() {
        // Mocking the required data
        int mockedStatusCode = 404;
        String mockedReason = "Not Found";

        when(statusService.getStatusResponse(any(), anyBoolean(), anyBoolean(), any(), anyInt(), any()))
                .thenThrow(new HttpStatusCodeException(HttpStatus.valueOf(mockedStatusCode), mockedReason) {
                    @Override
                    public String getResponseBodyAsString() {
                        return "Error response body from Status Service";
                    }
                });

        // Test the getStatusPackageLevel endpoint with HttpStatusCodeException
        ResponseEntity<String> responseEntity = statusController.getStatusPackageLevel("some-uuid", true, false, 2);

        // Verify the method calls and the response
        assertEquals(mockedStatusCode, responseEntity.getStatusCodeValue());
        assertEquals("Error response body from Status Service", responseEntity.getBody());
    }

    @Test
    public void testGetStatusServiceException_ConnectException() {
        // Mocking the required data
        when(statusService.getStatusResponse(any(), anyBoolean(), anyBoolean(), any(), anyInt(), any()))
                .thenThrow(new RuntimeException(new IOException("Service Unavailable")));

        // Test the getStatusPackageLevel endpoint with ConnectException
        ResponseEntity<String> responseEntity = statusController.getStatusPackageLevel("some-uuid", true, false, 2);

        // Verify the method calls and the response
        assertEquals(HttpStatus.GATEWAY_TIMEOUT, responseEntity.getStatusCode());
    }

    @Test
    public void testGetStatusServiceException_OtherExceptions() {
        // Mocking the required data
        when(statusService.getStatusResponse(any(), anyBoolean(), anyBoolean(), any(), anyInt(), any()))
                .thenThrow(new RuntimeException("Some other exception"));

        // Test the getStatusPackageLevel endpoint with other exceptions
        ResponseEntity<String> responseEntity = statusController.getStatusPackageLevel("some-uuid", true, false, 2);

        // Verify the method calls and the response
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, responseEntity.getStatusCode());
    }
}
