package eu.europa.ec.opoce.cellar.cl.controller.webapi;

import org.junit.Test;

import java.io.File;
import java.net.URL;

import static org.junit.Assert.assertEquals;

public class CellarControllerTest {

    @Test
    public void resolveMetsIdTest() throws Exception {
        final URL url = CellarControllerTest.class.getClassLoader().getResource("mets.zip");
        final File file = new File(url.toURI());

        final String metsId = CellarController.resolveMetsId(file);
        assertEquals("filename", metsId);
    }
}