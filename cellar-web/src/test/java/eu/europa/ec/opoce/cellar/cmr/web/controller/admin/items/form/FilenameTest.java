package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.items.form;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class FilenameTest {

    @Test
    public void differFromSuggestionTest() {
        final Filename filename = new Filename("cellar:id", "new_filename.pdf", "suggested_filename.pdf", null, "version");
        assertTrue(filename.differFromSuggestion());
    }

    @Test
    public void sameAsSuggestionTest() {
        final Filename filename = new Filename("cellar:id", "filename.pdf", "filename.pdf", null, "version");
        assertFalse(filename.differFromSuggestion());
    }

    @Test
    public void sameFilenameTest() {
        final Filename filename = new Filename("cellar:id", "filename.pdf", "filename.pdf", null, "version");
        assertFalse(filename.filenameChanged());
    }

    @Test
    public void filenameChangedTest() {
        final Filename filename = new Filename("cellar:id", "filename.pdf", "filename.pdf", null, "version");
        filename.setFilename("change.pdf");
        assertTrue(filename.filenameChanged());
    }

}