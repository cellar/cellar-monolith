package eu.europa.ec.opoce.cellar.cl.controller.resource;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.sparql.UriTemplates;
import eu.europa.ec.opoce.cellar.cl.service.client.UriTemplatesService;
import eu.europa.ec.opoce.cellar.server.service.DisseminationService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.util.ReflectionTestUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class FallbackRedirectResourceControllerTest {

    private static final UriTemplates URI_TEMPLATE = new UriTemplates("http://localhost/resource/{table}/{concept:.*}", "sparql", "xslt", 0L);

    private ICellarConfiguration cellarConfigurationMock;
    private UriTemplatesService uriTemplatesServiceMock;
    private DisseminationService disseminationServiceMock;
    private FallbackRedirectResourceController controller;

    @Before
    public void setup() {
        cellarConfigurationMock = mock(ICellarConfiguration.class);
        uriTemplatesServiceMock = mock(UriTemplatesService.class);
        disseminationServiceMock = mock(DisseminationService.class);
        controller = new FallbackRedirectResourceController();

        ReflectionTestUtils.setField(controller, "cellarConfiguration", cellarConfigurationMock);
        ReflectionTestUtils.setField(controller, "uriTemplateService", uriTemplatesServiceMock);
        ReflectionTestUtils.setField(controller, "disseminationService", disseminationServiceMock);
    }

    @Test
    public void executeSparqlQueryTestWithoutEncodedCharacters() {
        executeSparql("/resource/authority/fd_335/ART", "/authority/fd_335/ART");
    }

    @Test
    public void executeSparqlQueryTestWithEncodedCharacters() {
        executeSparql("/resource/authority/fd_335/MA%2FPART", "/authority/fd_335/MA%2FPART");
    }

    private void executeSparql(final String requestUri, final String expectedPath) {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        when(request.getRequestURI()).thenReturn(requestUri);
        when(request.getServletPath()).thenReturn("/resource");

        when(cellarConfigurationMock.isVirtuosoDisseminationSparqlMapperEnabled()).thenReturn(true);
        when(uriTemplatesServiceMock.findAllOrderedBySequence()).thenReturn(Collections.singletonList(URI_TEMPLATE));
        when(cellarConfigurationMock.getCellarUriDisseminationBase()).thenReturn("http://localhost/");

        controller.executeSparqlQuery(request, response);
        verify(cellarConfigurationMock).isVirtuosoDisseminationSparqlMapperEnabled();
        verify(uriTemplatesServiceMock).findAllOrderedBySequence();
        verify(cellarConfigurationMock).getCellarUriDisseminationBase();
        verify(disseminationServiceMock).doGetSparql(any(), eq(expectedPath));
        verifyNoMoreInteractions(cellarConfigurationMock, uriTemplatesServiceMock, disseminationServiceMock);
    }

    private void executeSparqlFor404(final String requestUri, final String expectedMessage) {
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        when(request.getRequestURI()).thenReturn(requestUri);
        when(request.getServletPath()).thenReturn("/resource");

        when(cellarConfigurationMock.isVirtuosoDisseminationSparqlMapperEnabled()).thenReturn(true);
        when(uriTemplatesServiceMock.findAllOrderedBySequence()).thenReturn(Collections.emptyList());
        when(cellarConfigurationMock.getCellarUriDisseminationBase()).thenReturn("http://localhost/resource/");

        final ResponseEntity<?> responseEntity = controller.executeSparqlQuery(request, response);
        assertEquals(responseEntity.getStatusCode(), HttpStatus.NOT_FOUND);
        assertEquals(responseEntity.getBody().toString(), expectedMessage);
    }

    @Test
    public void executeSparqlQueryTestWithUnknownPsi() {
        executeSparqlFor404("/resource/unknown/anything", "Resource [system 'unknown'] not found.");
        executeSparqlFor404("/resource/test-404/anything", "Resource [system 'test-404'] not found.");
    }
}
