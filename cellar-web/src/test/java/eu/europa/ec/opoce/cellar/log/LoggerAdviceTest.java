/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.logging
 *             FILE : LoggerAdviceTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 11 09, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-11-09 13:20:51 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.log;

import eu.europa.ec.opoce.cellar.common.spring.CloseableConcurrentTaskScheduler;
import eu.europa.ec.opoce.cellar.logging.LogContext;
import eu.europa.ec.opoce.cellar.logging.LoggerAdvice;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.apache.logging.log4j.core.LoggerContext;
import org.junit.AfterClass;
import org.junit.Assume;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.CountDownLatch;

import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.*;
import static eu.europa.ec.opoce.cellar.logging.LoggerAdvice.CELLAR_CONTEXT;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * @author ARHS Developments
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = LoggerAdviceTest.ConfigurationTest.class)
public class LoggerAdviceTest {

    private static final String CELLAR_ROOT_LOGGER = "eu.europa.ec.opoce.cellar";
    
    private static final String LOG_PATH = System.getenv("CELLAR_LOG_PATH");
    
    private static final String CMR_INGESTION_LOG_FILENAME = "cmr_ingestion.log";
    
    private static final String CMR_CONCURRENCY_LOG_FILENAME = "cmr_concurrency.log";
    
    private static final String CMR_DISSEMINATION_LOG_FILENAME = "cmr_dissemination.log";
    
    private static final String SPRING_TEST_CMR_INGESTION_CTX_LOG_ENTRY = "Spring_test_log_entry_from_cmr_ingestion_context.";

    private static final String SPRING_TEST_CMR_CONCURRENCY_CTX_LOG_ENTRY = "Spring_test_log_entry_from_cmr_concurrency_context.";
    
    private static final String BYTEBUDDY_TEST_CMR_DISSEMINATION_CTX_LOG_ENTRY = "ByteBuddy_test_log_entry_from_cmr_dissemination_context.";

    private static final String BYTEBUDDY_TEST_CMR_INGESTION_CTX_LOG_ENTRY = "ByteBuddy_test_log_entry_from_cmr_ingestion_context.";
    
    private static final String BYTEBUDDY_TEST_CMR_INGESTION_CTX_LOG_ENTRY_2 = "ByteBuddy_test_log_entry_from_cmr_ingestion_context_2.";

    private static final String BYTEBUDDY_TEST_CMR_CONCURRENCY_CTX_LOG_ENTRY = "ByteBuddy_test_log_entry_from_cmr_concurrency_context.";
    
    
    static {
        System.setProperty("log4j2.disable.jmx", "true");
    }

    @Autowired
    private ConfigurationTest.SpringTest springTest;

    
    @BeforeClass
    public static void init() {
    	Assert.isTrue(StringUtils.isNotBlank(LOG_PATH), "The CELLAR_LOG_PATH environment variable is not defined.");
    	cleanTmpLogDir();
        System.setProperty("log4j.configurationFile", "src/main/webapp/WEB-INF/log4j2.xml");
        LoggerContext.getContext(false).reconfigure();
        LoggerAdvice.init();
    }
    
    @AfterClass
    public static void destroy() {
    	LoggerContext.getContext(false).terminate();
    	cleanTmpLogDir();
    }
    
    public static void cleanTmpLogDir() {
    	Path tempLogPath = Paths.get(LOG_PATH);
    	if (Files.exists(tempLogPath)) {
    		boolean cleanupSuccess = true;
    		try {
				FileUtils.deleteDirectory(new File(LOG_PATH));
			} catch (IOException e) {
				e.printStackTrace();
				cleanupSuccess = false;
			}
    		Assume.assumeTrue(cleanupSuccess);
    	}
    }
    
    
    @Test
    public void testWithSpringBean() throws InterruptedException, IOException {
            springTest.await();
            springTest.test();

            Map<String, String> expectedLogFilesAndContents = new HashMap<>();
            expectedLogFilesAndContents.put(CMR_INGESTION_LOG_FILENAME, SPRING_TEST_CMR_INGESTION_CTX_LOG_ENTRY);
            expectedLogFilesAndContents.put(CMR_CONCURRENCY_LOG_FILENAME, SPRING_TEST_CMR_CONCURRENCY_CTX_LOG_ENTRY);
            checkLogFilesAndContents(expectedLogFilesAndContents);
    }

    @Test
    public void logContextMustRedirectToASpecificFile() throws IOException {
        Foo foo = new Foo();
        foo.foo(BYTEBUDDY_TEST_CMR_INGESTION_CTX_LOG_ENTRY);
        foo.bar(BYTEBUDDY_TEST_CMR_DISSEMINATION_CTX_LOG_ENTRY);
        foo.baz(BYTEBUDDY_TEST_CMR_CONCURRENCY_CTX_LOG_ENTRY);

        Map<String, String> expectedLogFilesAndContents = new HashMap<>();
        expectedLogFilesAndContents.put(CMR_INGESTION_LOG_FILENAME, BYTEBUDDY_TEST_CMR_INGESTION_CTX_LOG_ENTRY);
        expectedLogFilesAndContents.put(CMR_DISSEMINATION_LOG_FILENAME, BYTEBUDDY_TEST_CMR_DISSEMINATION_CTX_LOG_ENTRY);
        expectedLogFilesAndContents.put(CMR_CONCURRENCY_LOG_FILENAME, BYTEBUDDY_TEST_CMR_CONCURRENCY_CTX_LOG_ENTRY);
        checkLogFilesAndContents(expectedLogFilesAndContents);
    }

    @Test(expected = IllegalArgumentException.class)
    public void logContextMustRedirectToASpecificFileFailure() throws IOException {
        Foo foo = new Foo();
        foo.bar(BYTEBUDDY_TEST_CMR_INGESTION_CTX_LOG_ENTRY_2);
        
        Map<String, String> expectedLogFilesAndContents = new HashMap<>();
        expectedLogFilesAndContents.put(CMR_INGESTION_LOG_FILENAME, BYTEBUDDY_TEST_CMR_INGESTION_CTX_LOG_ENTRY_2);
        checkLogFilesAndContents(expectedLogFilesAndContents);
    }

    private static void checkLogFilesAndContents(Map<String, String> expectedLogFilesAndContents) throws IOException {
    	File tmpLogDir = new File(LOG_PATH);
        Assert.isTrue(tmpLogDir.isDirectory(), "The CELLAR_LOG_PATH environment variable does not correspond to a directory.");
        
        Set<String> tmpLogDirContents = new HashSet<>(Arrays.asList(tmpLogDir.list()));
        Assert.isTrue(tmpLogDirContents.containsAll(expectedLogFilesAndContents.keySet()), "The required log files could not be found in the temp log directory.");
        
        for (Entry<String, String> expectedEntry : expectedLogFilesAndContents.entrySet()) {
    		String logFile = expectedEntry.getKey();
    		String contentToFind = expectedEntry.getValue();
			List<String> lines = Files.readAllLines(Paths.get(LOG_PATH + File.separator + logFile));
			boolean found = lines.stream().anyMatch(l -> l.contains(contentToFind));
			Assert.isTrue(found, "Log file " + logFile + " does not contain expected entry " + contentToFind + ".");
        }
    }
    
    @Test
    public void mustRestorePreviousContextOnLeave() {
        LoggerAdvice.leave();
        assertNull(ThreadContext.get(CELLAR_CONTEXT));
        
        LoggerAdvice.leave("previous");
        assertEquals(ThreadContext.get(CELLAR_CONTEXT), "previous");
    }

    
    @org.springframework.context.annotation.Configuration
    @EnableAspectJAutoProxy
    static class ConfigurationTest {

        @Bean
        public SpringTest springTest() {
            return new SpringTest();
        }

        @Bean
        public LoggerAdvice loggerAdvice() {
            return new LoggerAdvice();
        }

        public static class SpringTest {

            static final Logger LOG = LogManager.getLogger(SpringTest.class);

            private CloseableConcurrentTaskScheduler taskScheduler;
            private final CountDownLatch latch = new CountDownLatch(1);

            @PostConstruct
            public void postConstruct() {
                taskScheduler = CloseableConcurrentTaskScheduler.newScheduler("test-scheduler");
            }

            @LogContext(CMR_INGESTION)
            public void test() { // must be public
                LOG.info(SPRING_TEST_CMR_INGESTION_CTX_LOG_ENTRY);
            }

            void await() throws InterruptedException {
                taskScheduler.scheduleWithFixedDelay(new RunnableTask(latch), 5000L);
                latch.await();
                taskScheduler.shutdownNow();
            }
        }

        @LogContext
        static class RunnableTask implements Runnable {

            static final Logger LOG = LogManager.getLogger(RunnableTask.class);
            private final CountDownLatch latch;

            RunnableTask(CountDownLatch latch) {
                this.latch = latch;
            }

            @Override
            @LogContext(CMR_CONCURRENCY)
            public void run() {
                LOG.info(SPRING_TEST_CMR_CONCURRENCY_CTX_LOG_ENTRY);
                latch.countDown();
            }
        }
    }

    @LogContext
    static class Foo {

        static final Logger LOG = LogManager.getLogger(CELLAR_ROOT_LOGGER);

        @LogContext(CMR_INGESTION)
        public void foo(String msg) {
            LOG.info(msg);
        }

        @LogContext(CMR_DISSEMINATION)
        public void bar(String msg) {
            LOG.info(msg);
        }

        @LogContext(CMR_CONCURRENCY)
        public void baz(String msg) {
            LOG.info(msg);
        }
    }
}
