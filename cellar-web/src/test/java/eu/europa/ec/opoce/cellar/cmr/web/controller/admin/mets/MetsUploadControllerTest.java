package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.mets;

import eu.europa.ec.opoce.cellar.ccr.mets.response.MetsResponse;
import eu.europa.ec.opoce.cellar.ccr.service.client.MetsUploadService;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.package_history.PackageHistory;
import eu.europa.ec.opoce.cellar.ingestion.service.SIPWatcher;
import eu.europa.ec.opoce.cellar.semantic.helper.DateFormats;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.when;

/**
 * @author EUROPEAN DYNAMICS S.A.
 */
public class MetsUploadControllerTest {

    @Mock
    private MetsUploadService metsUploadService;

    @Mock
    private ICellarConfiguration cellarConfiguration;

    @Mock
    private SIPWatcher sipWatcher;

    @InjectMocks
    private MetsUploadController metsUploadController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        // Mock the cellarConfiguration behavior for isCellarServiceDorieDedicatedEnabled()
        when(cellarConfiguration.isCellarServiceDorieDedicatedEnabled()).thenReturn(true);

        // Create the MetsUploadController and inject the mocked cellarConfiguration using ReflectionTestUtils
        metsUploadController = new MetsUploadController(metsUploadService, sipWatcher,null);
        ReflectionTestUtils.setField(metsUploadController, "cellarConfiguration", cellarConfiguration);
    }

    @Test
    public void testMetsUpload_Success() {
        // Mock the multipart file
        byte[] testData = "Test data content".getBytes();
        MultipartFile fileData = new MockMultipartFile("test.zip", testData);

        // Mock the MetsResponse
        PackageHistory packageHistory = new PackageHistory();
        packageHistory.setReceivedDate(new Date());
        packageHistory.setUuid("123456");

        MetsResponse metsResponse = new MetsResponse();
        metsResponse.setReceived(DateFormats.formatXmlDateTime(packageHistory.getReceivedDate()));
        metsResponse.setStatus_endpoint("http://localhost:8080/status?id=123456");

        // Mock the metsUploadService behavior
        when(metsUploadService.metsUpload(any(MultipartFile.class), anyString(), anyString()))
                .thenReturn(metsResponse);

        // Mock the cellarConfiguration behavior
        when(cellarConfiguration.isCellarServiceDorieDedicatedEnabled()).thenReturn(true);

        // Perform the test
        ResponseEntity<?> responseEntity = metsUploadController.metsUpload(fileData, "DAILY", "http://example.com/callback");

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(metsResponse, responseEntity.getBody());
    }

    @Test
    public void testMetsUpload_Failure() {
        // Mock the multipart file
        byte[] testData = "Test data content".getBytes();
        MultipartFile fileData = new MockMultipartFile("test.zip", testData);

        // Mock the metsUploadService behavior to throw an exception
        when(metsUploadService.metsUpload(any(MultipartFile.class), anyString(), anyString()))
                .thenThrow(new IllegalArgumentException());

        // Mock the cellarConfiguration behavior
        when(cellarConfiguration.isCellarServiceDorieDedicatedEnabled()).thenReturn(true);

        // Perform the test
        ResponseEntity<?> responseEntity = metsUploadController.metsUpload(fileData, "INVALID", "http://example.com/callback");

        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
    }
}
