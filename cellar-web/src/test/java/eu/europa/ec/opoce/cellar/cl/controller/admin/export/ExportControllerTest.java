package eu.europa.ec.opoce.cellar.cl.controller.admin.export;

import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cl.service.export.IExportService;
import eu.europa.ec.opoce.cellar.core.dao.FileDao;
import eu.europa.ec.opoce.cellar.server.service.DisseminationException;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;

import java.io.File;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.*;

public class ExportControllerTest {

    private IExportService exportServiceMock;
    private IdentifierService identifierServiceMock;
    private FileDao fileDaoMock;
    private ExportController exportController;

    @Before
    public void setup() {
        exportServiceMock = mock(IExportService.class);
        identifierServiceMock = mock(IdentifierService.class);
        fileDaoMock = mock(FileDao.class);
        exportController = new ExportController(exportServiceMock, identifierServiceMock, fileDaoMock);
    }

    @Test
    public void exportUnknownId() throws Exception {
        when(identifierServiceMock.getCellarPrefixed("oj:unknown")).thenThrow(new IllegalArgumentException());
        when(fileDaoMock.getArchiveFolderExportRest()).thenReturn(mock(File.class));

        try {
            exportController.exportPackage("oj:unknown", false, false, null);
            assertFalse(true);
        } catch (DisseminationException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getHttpStatus());
            assertEquals("Resource 'oj:unknown' does not exist", e.getMessage());
            verify(identifierServiceMock).getCellarPrefixed("oj:unknown");
            verify(fileDaoMock).getArchiveFolderExportRest();
            verifyNoMoreInteractions(exportServiceMock, identifierServiceMock, fileDaoMock);
        }
    }

    @Test
    public void exportEmboddiedUnknownId() throws Exception {
        when(identifierServiceMock.getCellarPrefixed("oj:unknown")).thenThrow(new IllegalArgumentException());

        try {
            exportController.exportEmbodiedPackage("oj:unknown", false, false, true);
            assertFalse(true);
        } catch (DisseminationException e) {
            assertEquals(HttpStatus.BAD_REQUEST, e.getHttpStatus());
            assertEquals("Resource 'oj:unknown' does not exist", e.getMessage());
            verify(identifierServiceMock).getCellarPrefixed("oj:unknown");
            verifyNoMoreInteractions(exportServiceMock, identifierServiceMock, fileDaoMock);
        }
    }
}
