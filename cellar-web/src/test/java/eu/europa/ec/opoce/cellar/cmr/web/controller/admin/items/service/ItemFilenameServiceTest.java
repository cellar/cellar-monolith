package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.items.service;

import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.web.controller.admin.items.form.Filename;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import eu.europa.ec.opoce.cellar.s3.S3ContentStreamService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.ArgumentCaptor;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType.ITEM;
import static junit.framework.TestCase.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.*;

public class ItemFilenameServiceTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private IdentifierService identifierServiceMock;
    private S3ContentStreamService s3ContentStreamServiceMock;
    private CellarResourceDao cellarResourceDaoMock;
    private ItemFilenameService itemFilenameService;

    @Before
    public void setup() {
        identifierServiceMock = mock(IdentifierService.class);
        s3ContentStreamServiceMock = mock(S3ContentStreamService.class);
        cellarResourceDaoMock = mock(CellarResourceDao.class);
        itemFilenameService = new ItemFilenameService(identifierServiceMock, s3ContentStreamServiceMock, cellarResourceDaoMock);
    }

    @Test
    public void identifierNotFound_getItemsTest() {
        expectedException.expect(IllegalArgumentException.class);
        when(identifierServiceMock.getCellarPrefixed("id")).thenThrow(new IllegalArgumentException());
        itemFilenameService.getFilenames("id", 1);
    }

    @Test
    public void filterExceptItems_getItemsTest() {
        CellarResource work = mockResource(DigitalObjectType.WORK);
        CellarResource expression = mockResource(DigitalObjectType.EXPRESSION);
        CellarResource manifestation = mockResource(DigitalObjectType.MANIFESTATION);

        when(identifierServiceMock.getCellarPrefixed("id"))
                .thenReturn("cellar:id");
        when(cellarResourceDaoMock.findStartingWithSortByCellarId("cellar:id"))
                .thenReturn(Arrays.asList(work, expression, manifestation));

        final List<Filename> items = itemFilenameService.getFilenames("id", 1);
        assertTrue(items.isEmpty());
    }

    @Test
    public void withSuggestion_getItemsTest() {
        CellarResource itemResource = mockResource("cellar:0853de0f-fecf-11e9-8c1f-01aa75ed71a1.0001.01/DOC_1", ITEM, "v1");
        Identifier itemIdentifier = mockIdentifier("uriserv:OJ.C_.2019.371.01.0014.01.BUL.pdfa2a.c_37120191104bg00140015.pdf");
        Identifier manifestationIdentifier = mockIdentifier("uriserv:OJ.C_.2019.371.01.0014.01.BUL.pdfa2a");

        when(identifierServiceMock.getCellarPrefixed("uriserv:OJ.C_.2019.371.01.0014.01"))
                .thenReturn("cellar:0853de0f-fecf-11e9-8c1f-01aa75ed71a1");
        when(cellarResourceDaoMock.findStartingWithSortByCellarId("cellar:0853de0f-fecf-11e9-8c1f-01aa75ed71a1"))
                .thenReturn(Collections.singletonList(itemResource));
        when(identifierServiceMock.getIdentifier("cellar:0853de0f-fecf-11e9-8c1f-01aa75ed71a1.0001.01/DOC_1"))
                .thenReturn(itemIdentifier);
        when(s3ContentStreamServiceMock.getMetadata("cellar:0853de0f-fecf-11e9-8c1f-01aa75ed71a1.0001.01/DOC_1", "v1", ContentType.FILE))
                .thenReturn(Collections.singletonMap(ContentStreamService.Metadata.FILENAME, "filename1.txt"));
        when(identifierServiceMock.getIdentifier("cellar:0853de0f-fecf-11e9-8c1f-01aa75ed71a1.0001.01"))
                .thenReturn(manifestationIdentifier);

        final List<Filename> items = itemFilenameService.getFilenames("uriserv:OJ.C_.2019.371.01.0014.01", 1);
        assertEquals(1, items.size());
        assertEquals("filename1.txt", items.get(0).getFilename());
        assertEquals("cellar:0853de0f-fecf-11e9-8c1f-01aa75ed71a1.0001.01/DOC_1", items.get(0).getCellarId());
        assertEquals("filename1.txt", items.get(0).getExistingFilename());
        assertEquals("c_37120191104bg00140015.pdf", items.get(0).getSuggestedFilename());
        assertEquals("v1", items.get(0).getVersion());
    }

    @Test
    public void noSuggestion_getItemsTest() {
        CellarResource itemResource = mockResource("cellar:0853de0f-fecf-11e9-8c1f-01aa75ed71a1.0001.01/DOC_1", ITEM, "v1");
        Identifier itemIdentifier = mockIdentifier("uriserv:OJ.C_.2019.371.01.0014.01.BUL.pdfa2a.c_37120191104bg00140015.pdf");
        Identifier manifestationIdentifier = mockIdentifier("oj:JOC_2019_371_R_0007.BUL.pdfa2a");

        when(identifierServiceMock.getCellarPrefixed("oj:JOC_2019_371_R_0007"))
                .thenReturn("cellar:0853de0f-fecf-11e9-8c1f-01aa75ed71a1");
        when(cellarResourceDaoMock.findStartingWithSortByCellarId("cellar:0853de0f-fecf-11e9-8c1f-01aa75ed71a1"))
                .thenReturn(Collections.singletonList(itemResource));
        when(identifierServiceMock.getIdentifier("cellar:0853de0f-fecf-11e9-8c1f-01aa75ed71a1.0001.01/DOC_1"))
                .thenReturn(itemIdentifier);
        when(s3ContentStreamServiceMock.getMetadata("cellar:0853de0f-fecf-11e9-8c1f-01aa75ed71a1.0001.01/DOC_1", "v1", ContentType.FILE))
                .thenReturn(Collections.singletonMap(ContentStreamService.Metadata.FILENAME, "filename1.txt"));
        when(identifierServiceMock.getIdentifier("cellar:0853de0f-fecf-11e9-8c1f-01aa75ed71a1.0001.01"))
                .thenReturn(manifestationIdentifier);

        final List<Filename> items = itemFilenameService.getFilenames("oj:JOC_2019_371_R_0007", 1);
        assertEquals(1, items.size());
        assertEquals("filename1.txt", items.get(0).getFilename());
        assertEquals("cellar:0853de0f-fecf-11e9-8c1f-01aa75ed71a1.0001.01/DOC_1", items.get(0).getCellarId());
        assertEquals("filename1.txt", items.get(0).getExistingFilename());
        assertEquals("none", items.get(0).getSuggestedFilename());
        assertEquals("v1", items.get(0).getVersion());
    }

    @Test
    public void pageException_getItemsTest() {
        try {
            itemFilenameService.getFilenames("id", 0);
            fail("Expected an to be thrown");
        } catch (IllegalArgumentException e) {
        }

        verifyNoMoreInteractions(identifierServiceMock, s3ContentStreamServiceMock, cellarResourceDaoMock);
    }

    @Test
    public void pagination_getItemsTest() {
        when(identifierServiceMock.getCellarPrefixed("oj:JOC_2019_371_R_0007"))
                .thenReturn("cellar:0853de0f-fecf-11e9-8c1f-01aa75ed71a1");

        final List<CellarResource> itemResources = IntStream.range(0, 300)
                .mapToObj(i -> mockResource("cellar:0853de0f-fecf-11e9-8c1f-01aa75ed71a1.0001.01/DOC_" + i, ITEM, "v1"))
                .collect(Collectors.toList());
        when(cellarResourceDaoMock.findStartingWithSortByCellarId("cellar:0853de0f-fecf-11e9-8c1f-01aa75ed71a1"))
                .thenReturn(itemResources);

        for(int i = 0; i < itemResources.size(); ++i) {
            final Identifier itemIdentifier = mockIdentifier("uriserv:OJ.C_.2019.371.01.0014.01.BUL.pdfa2a." + i +".pdf");
            when(identifierServiceMock.getIdentifier("cellar:0853de0f-fecf-11e9-8c1f-01aa75ed71a1.0001.01/DOC_" + i))
                    .thenReturn(itemIdentifier);
            when(s3ContentStreamServiceMock.getMetadata("cellar:0853de0f-fecf-11e9-8c1f-01aa75ed71a1.0001.01/DOC_" + i, "v1", ContentType.FILE))
                    .thenReturn(Collections.singletonMap(ContentStreamService.Metadata.FILENAME, "filename" + i + ".txt"));
        }

        final Identifier manifestationIdentifier = mockIdentifier("oj:JOC_2019_371_R_0007.BUL.pdfa2a");
        when(identifierServiceMock.getIdentifier("cellar:0853de0f-fecf-11e9-8c1f-01aa75ed71a1.0001.01"))
                .thenReturn(manifestationIdentifier);

        final List<Filename> items = itemFilenameService.getFilenames("oj:JOC_2019_371_R_0007", 3);

        assertEquals(100, items.size());
        assertEquals("filename200.txt", items.get(0).getFilename());
        assertEquals("cellar:0853de0f-fecf-11e9-8c1f-01aa75ed71a1.0001.01/DOC_200", items.get(0).getCellarId());
        assertEquals("filename200.txt", items.get(0).getExistingFilename());
        assertEquals("none", items.get(0).getSuggestedFilename());
        assertEquals("v1", items.get(0).getVersion());
    }


    private CellarResource mockResource(final DigitalObjectType type) {
        CellarResource resource = mock(CellarResource.class);
        when(resource.getCellarType()).thenReturn(type);
        return resource;
    }

    private CellarResource mockResource(final String cellarId, final DigitalObjectType type, final String version) {
        CellarResource resource = mock(CellarResource.class);
        when(resource.getCellarType()).thenReturn(type);
        when(resource.getCellarId()).thenReturn(cellarId);
        when(resource.getVersion(ContentType.FILE)).thenReturn(Optional.ofNullable(version));
        return resource;
    }

    private Identifier mockIdentifier(final String... pids) {
        Identifier identifier = mock(Identifier.class);
        when(identifier.getPids()).thenReturn(Arrays.asList(pids));
        return identifier;
    }

    @Test
    public void updateModifiedFilename_updateItemFilenamesTest() {
        final Filename filename = new Filename("cellar:id", "new_filename.pdf", "suggested_filename.pdf", null, "version");
        filename.setExistingFilename("existing_filename.pdf");

        final List<Filename> updatedFilenames = itemFilenameService.updateItemFilenames(Collections.singletonList(filename));

        ArgumentCaptor<Map> metadataCaptor = ArgumentCaptor.forClass(Map.class);
        verify(s3ContentStreamServiceMock).updateMetadata(eq("cellar:id"), eq("version"),
                eq(ContentType.FILE), metadataCaptor.capture());
        assertEquals("new_filename.pdf", metadataCaptor.getValue().get(ContentStreamService.Metadata.FILENAME));
        assertEquals(1, updatedFilenames.size());
    }

    @Test
    public void noUpdateForUnmodifiedFilename_updateItemFilenamesTest() {
        final Filename filename = new Filename("cellar:id", "filename.pdf", "suggested_filename.pdf", null, "version");

        itemFilenameService.updateItemFilenames(Collections.singletonList(filename));

        verifyNoMoreInteractions(s3ContentStreamServiceMock);
    }

    @Test
    public void emptyVersion_updateItemFilenamesTest() {
        final Filename filename = new Filename("cellar:id", "new_filename.pdf", "suggested_filename.pdf", null, "");
        filename.setExistingFilename("existing_filename.pdf");

        final List<Filename> updatedFilenames = itemFilenameService.updateItemFilenames(Collections.singletonList(filename));

        ArgumentCaptor<Map> metadataCaptor = ArgumentCaptor.forClass(Map.class);
        verify(s3ContentStreamServiceMock).updateMetadata(eq("cellar:id"), eq(null),
                eq(ContentType.FILE), metadataCaptor.capture());
        assertEquals("new_filename.pdf", metadataCaptor.getValue().get(ContentStreamService.Metadata.FILENAME));
        assertEquals(1, updatedFilenames.size());
    }
}