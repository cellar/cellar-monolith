package eu.europa.ec.opoce.cellar.cmr.web.controller.admin.items.form;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class SearchFormTest {

    @Test
    public void filterEnabledByDefaultTest() {
        final SearchForm searchForm = new SearchForm();
        assertTrue(searchForm.isFilterIdenticalSuggestion());
    }
}