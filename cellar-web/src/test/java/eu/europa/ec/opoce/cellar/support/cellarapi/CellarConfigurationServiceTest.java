package eu.europa.ec.opoce.cellar.support.cellarapi;

import org.junit.Assert;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

public class CellarConfigurationServiceTest {

    @Test
    public void getCellarBaseUriWithTrailingSlashTest() throws Exception {
        final CellarConfigurationService service = new CellarConfigurationService();
        Whitebox.setInternalState(service, "baseURI", "http://localhost:8080/");
        Assert.assertEquals("http://localhost:8080", service.getCellarBaseUri());
    }

    @Test
    public void getCellarBaseUriWithoutTrailingSlashTest() throws Exception {
        final CellarConfigurationService service = new CellarConfigurationService();
        Whitebox.setInternalState(service, "baseURI", "http://localhost:8080");
        Assert.assertEquals("http://localhost:8080", service.getCellarBaseUri());
    }
}