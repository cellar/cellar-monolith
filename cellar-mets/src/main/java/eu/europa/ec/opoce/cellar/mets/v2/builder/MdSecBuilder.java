/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.mets.v2.builder
 *             FILE : MdSecBuilder.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 8, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.mets.v2.builder;

import eu.europa.ec.opoce.cellar.common.builder.IBuilder;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.MdSecType;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.MdSecType.MdRef;

/**
 * <class_description> {@link MdSecType} builder.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 8, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class MdSecBuilder implements IBuilder<MdSecType> {

    private final MdSecType mdSec;

    private MdSecBuilder() {
        this.mdSec = new MdSecType();
    }

    public static MdSecBuilder get() {
        return new MdSecBuilder();
    }

    public MdSecBuilder withID(final String id) {
        this.mdSec.setID(id);
        return this;
    }

    public MdSecBuilder withMdRef(final MdRef mdRef) {
        this.mdSec.setMdRef(mdRef);
        return this;
    }

    /* (non-Javadoc)
     * @see eu.europa.ec.opoce.cellar.common.builder.IBuilder#build()
     */
    @Override
    public MdSecType build() {
        return this.mdSec;
    }
}
