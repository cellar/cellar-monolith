package eu.europa.ec.opoce.cellar.xmlvalidation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

/**
 * <p>NalValidationService class.</p>
 * This service validates the NAL on the basis of its XSD.<br />
 */
@Service("nalValidationService")
public class NalValidationService extends AbstractValidationService {

    @Autowired
    private MetsXsdFileConfiguration xsdFileConfiguration;

    @Override
    protected Resource resolveResource() {
        return this.xsdFileConfiguration.getNalMetsXsdResource();
    }

}
