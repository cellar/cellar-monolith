/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.domain.content.mets
 *        FILE : MetsBehavior.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 04-01-2016
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2016 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.mets.parser;

import com.google.common.collect.Lists;
import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.PrefixConfigurationService;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import eu.europa.ec.opoce.cellar.common.util.URLUtils;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.NotImplementedExceptionBuilder;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class represents a METS behavior.</br>
 * </br>
 * ON : 04-01-2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class MetsBehavior {

    private static final String RESOLVE_ENCODING = "UTF-8";
    private static final String RESOURCE_RESOLVE_REGEX = "(&resource;)(\\w+)/";

    private static final PrefixConfigurationService prefixConfigurationService = ServiceLocator
            .getService(PrefixConfigurationService.class);

    private final String link;
    private Map<String, List<String>> params;
    private BehaviourType type;

    public MetsBehavior(final String link) {
        this.link = (link == null ? "" : link);
        this.params = new HashMap<String, List<String>>();

        this.init();
    }

    private void init() {
        // retrieves the behavior type on the basis of the link
        for (final BehaviourType type : BehaviourType.values()) {
            if (this.link.startsWith(type.getPrefix())) {
                this.type = type;
                break;
            }
        }
        if (this.type == null) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.ERROR_WHILE_PARSING_THE_METS_DOCUMENT)
                    .withMessage("The link '{}' does not match any known behavior.").withMessageArgs(this.link).build();
        }

        // retrieves all params
        try {
            this.params = URLUtils.getQueryParams(this.link);
        } catch (final UnsupportedEncodingException e) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.ERROR_WHILE_PARSING_THE_METS_DOCUMENT)
                    .withMessage("Link '{}' is invalid: unsupported encoding.").withMessageArgs(this.link).withCause(e).root(true).build();
        }

        // check that all params are acceptable
        this.checkAcceptableParams(this.params.keySet());
    }

    String getParamValue(final String paramName) {
        // check that the param is valid for the given behavior
        this.checkAcceptableParam(paramName);

        // retrieves the param value
        final List<String> paramValues = this.params.get(paramName);
        if (paramValues == null || paramValues.size() == 0) {
            return null;
        } else if (paramValues.size() == 1) {
            return resolveParamValue(paramValues.get(0));
        } else {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.ERROR_WHILE_PARSING_THE_METS_DOCUMENT)
                    .withMessage("Found multiple values for parameter '{}' in link '{}'.").withMessageArgs(paramName, this.link).build();
        }
    }

    void checkAcceptableParam(final String paramName) {
        final List<String> paramsToCheck = new ArrayList<String>();
        paramsToCheck.add(paramName);
        this.checkAcceptableParams(paramsToCheck);
    }

    void checkAcceptableParams(final Collection<String> paramNames) {
        final List<String> unacceptableParams = new ArrayList<String>();

        for (final String paramName : paramNames) {
            if (!this.type.isAcceptableParam(paramName)) {
                unacceptableParams.add(paramName);
            }
        }

        if (unacceptableParams.size() > 0) {
            throw ExceptionBuilder.get(CellarException.class) //
                    .withCode(CommonErrors.ERROR_WHILE_PARSING_THE_METS_DOCUMENT) //
                    .withMessage("Parameter(s) {} is(are) not acceptable for behavior '{}'. Supported parameters are: {}") //
                    .withMessageArgs(unacceptableParams.toArray(), this, this.type.acceptableParams.toArray()) //
                    .build();
        }
    }

    BehaviourType getType() {
        return this.type;
    }

    static String resolveParamValue(final String paramValue) {
        if (paramValue == null) {
            return null;
        }

        String resolvedParamValue = null;
        try {
            resolvedParamValue = URLDecoder.decode(paramValue, RESOLVE_ENCODING);

            final Pattern pattern = Pattern.compile(RESOURCE_RESOLVE_REGEX);
            final Matcher matcher = pattern.matcher(resolvedParamValue);
            while (matcher.find()) {
                final String matchedResource = matcher.group();
                final String uriPrefix = matcher.group(2);
                resolvedParamValue = resolvedParamValue.replaceAll(matchedResource, prefixConfigurationService.getPrefixUri(uriPrefix));
            }
        } catch (final UnsupportedEncodingException uee) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Could not encode/decode with encoding '{}'")
                    .withMessageArgs(RESOLVE_ENCODING).withCause(uee).root(true).build();
        } catch (final Exception ex) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("Unexpected exception while resolving behavior parameter: see cause for details").withCause(ex).build();
        }

        return resolvedParamValue;
    }

    @Override
    public String toString() {
        return this.link;
    }

    public enum BehaviourType {
        transformation(null, "type"), // currently supported by METS V1 only
        delete(null, "query", "file"), // currently supported by METS V1 and V2
        modelLoad("model-load", "model", "date"), // currently supported by METS V1 and V2
        ontologyLoad("ontology-load"), // currently supported by METS V1 and V2
        sparqlLoad("sparql-load", "model", "date");

        final String name;
        final List<String> acceptableParams;

        BehaviourType(final String name, final String... acceptableParams) {
            this.name = name;
            this.acceptableParams = Lists.newArrayList(acceptableParams);
        }

        @Override
        public String toString() {
            return this.name == null ? super.toString() : this.name;
        }

        public String getPrefix() {
            return "cellar-mets:" + this + "-behavior";
        }

        /**
         * @return the acceptableParams
         */
        public String getAcceptableParamsByName(final String name) {
            for (final String param : this.acceptableParams) {
                if (param.equals(name)) {
                    return param;
                }
            }

            throw NotImplementedExceptionBuilder.get().withCode(CommonErrors.ERROR_WHILE_PARSING_THE_METS_DOCUMENT)
                    .withMessage("Param identified by name '{}' is not supported.").withMessageArgs(name).build();
        }

        boolean isAcceptableParam(final String paramName) {
            for (final String acceptableParam : this.acceptableParams) {
                if (acceptableParam.equals(paramName)) {
                    return true;
                }
            }
            return false;
        }

        static BehaviourType getByName(final String name) {
            for (final BehaviourType behaviourType : BehaviourType.values()) {
                if (behaviourType.toString().equals(name)) {
                    return behaviourType;
                }
            }

            throw NotImplementedExceptionBuilder.get().withCode(CommonErrors.ERROR_WHILE_PARSING_THE_METS_DOCUMENT)
                    .withMessage("Behavior's identified by name '{}' is not supported.").withMessageArgs(name).build();
        }
    }

}
