package eu.europa.ec.opoce.cellar.mets.parser;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.domain.content.mets.*;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import org.xml.sax.Attributes;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

/**
 * SAX2 event handler that parses a METS document version 1.
 * Parses and validates the mime types of the mets document.
 */
public class MetsV1DocumentParser extends MetsV2DocumentParser {

    /**
     * give all needed input to parse a metsPackage to a metsDocument
     *
     * @param metspackage         the metsPackage that needs to be parsed
     */
    public MetsV1DocumentParser(final MetsPackage metspackage) {
        super(metspackage);
    }

    @Override
    protected void startStructMap(final Attributes attributes) {
        final MetsDocument metsDocument = this.getMetsDocument();
        this.setCurrentStructMap(new StructMap(metsDocument));
        final StructMap currentStructMap = this.getCurrentStructMap();

        if (attributes.getIndex("ID") == -1) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.MISSING_ID_ON_STRUCTMAP)
                    .withMessage("Missing ID attribute on StructMap in mets document {}").withMessageArgs(metsDocument.getDocumentId())
                    .build();
        }
        currentStructMap.setId(attributes.getValue("ID"));

        // set the StructMap's type attributes only if the METS document is set with a read or update operation
        if (metsDocument.getType() == OperationType.Read || metsDocument.getType() == OperationType.Update) {
            if (attributes.getIndex("TYPE") == -1) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.MISSING_TYPE_ON_STRUCTMAP)
                        .withMessage("Missing TYPE attribute on StructMap in case of read/update in mets document {}")
                        .withMessageArgs(metsDocument.getDocumentId()).build();
            }
            final String type = attributes.getValue("TYPE");

            if (type.indexOf('.') == -1) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.INVALID_TYPE_ON_STRUCTMAP)
                        .withMessage("Invalid TYPE attribute {} on StructMap in case of read/update in mets document {}")
                        .withMessageArgs(type, metsDocument.getDocumentId()).build();
            }
            final String structmaptype = type.substring(0, type.indexOf('.'));
            final String postfix = type.substring(type.indexOf('.') + 1);

            if (postfix.equalsIgnoreCase("all")) {
                currentStructMap.setContentOperationType(StructMapType.getByXmlValue(structmaptype));
                currentStructMap.setMetadataOperationType(StructMapType.getByXmlValue(structmaptype));
            } else if (postfix.equalsIgnoreCase("metadata")) {
                currentStructMap.setMetadataOperationType(StructMapType.getByXmlValue(structmaptype));
            } else if (postfix.equalsIgnoreCase("content")) {
                currentStructMap.setContentOperationType(StructMapType.getByXmlValue(structmaptype));
            } else {
                throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.INVALID_TYPE_ON_STRUCTMAP)
                        .withMessage("Unknown postfix for TYPE attribute {} on StructMap in case of read/update in mets document {}")
                        .withMessageArgs(type, metsDocument.getDocumentId()).build();
            }
        }

        metsDocument.addStructMap(currentStructMap);
    }

    @Override
    protected void startDiv(final Attributes attributes) {
        final StructMap currentStructMap = this.getCurrentStructMap();
        final DigitalObject digitalobject = new DigitalObject(currentStructMap);
        final MetsDocument metsDocument = this.getMetsDocument();

        DigitalObjectType digitalObjectType = DigitalObjectType.getByValue(attributes.getValue("TYPE"));
        if (currentStructMap.getDigitalObject() == null && DigitalObjectType.EVENT.equals(digitalObjectType)) {
            digitalObjectType = DigitalObjectType.TOPLEVELEVENT;
        }
        digitalobject.setType(digitalObjectType);

        if (metsDocument.getType() != OperationType.Read) {
            final Collection<DigitalObjectType> allowedRoots = Arrays.asList(DigitalObjectType.DOSSIER, DigitalObjectType.AGENT,
                    DigitalObjectType.WORK, DigitalObjectType.TOPLEVELEVENT);
            if (currentStructMap.getDigitalObject() == null && !allowedRoots.contains(digitalObjectType)) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.INVALID_STRUCTURE_OF_DIGITAL_OBJECTS)
                        .withMessage("Root element is of type '{}', allowed root elements are {}.")
                        .withMessageArgs(digitalObjectType, allowedRoots).build();
            }
            if (digitalObjectType == DigitalObjectType.EXPRESSION && this.getCurrentDigitalObject().getType() != DigitalObjectType.WORK) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.INVALID_STRUCTURE_OF_DIGITAL_OBJECTS)
                        .withMessage("'expression' expect 'work' as parent.").build();
            }
            if (digitalObjectType == DigitalObjectType.MANIFESTATION
                    && this.getCurrentDigitalObject().getType() != DigitalObjectType.EXPRESSION) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.INVALID_STRUCTURE_OF_DIGITAL_OBJECTS)
                        .withMessage("'manifestation' expects 'expression' as parent.").build();
            }
            if (digitalObjectType == DigitalObjectType.EVENT && this.getCurrentDigitalObject().getType() != DigitalObjectType.DOSSIER) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.INVALID_STRUCTURE_OF_DIGITAL_OBJECTS)
                        .withMessage("'event' expects 'dossier' as parent.").build();
            }
        }

        if (attributes.getIndex("DMDID") != -1) {
            final Metadata metadata = new Metadata(digitalobject, MetadataType.DMD, attributes.getValue("DMDID"));
            digitalobject.setBusinessMetadata(metadata);
        }

        if (attributes.getIndex("ADMID") != -1) {
            final Metadata metadata = new Metadata(digitalobject, MetadataType.TECHNICAL, attributes.getValue("ADMID"));
            digitalobject.setTechnicalMetadata(metadata);
        }

        // Check existence of Label (last modification date)
        if (attributes.getIndex("LABEL") != -1) {
            if (metsDocument.getType().equals(OperationType.Create)) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.LABEL_ATTRIBUTE_NOT_EXCEPTED)
                        .withMessage("Mets parsing: Label attribute on div not acceptable for a mets message {} of type create")
                        .withMessageArgs(metsDocument.getDocumentId()).build();
            }
            try {
                final java.text.DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

                // explicitly set timezone of input if needed
                df.setTimeZone(java.util.TimeZone.getTimeZone("Zulu"));

                // 2001-12-17T09:30:57Z
                final Date date = df.parse(attributes.getValue("LABEL"));
                // getLabel() = on update, the previous modification date provided by the mets package in div/@LABEL
                digitalobject.setLabel(date);
            } catch (final ParseException e) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.LABEL_ATTRIBUTE_NOT_CORRECT_DATEFORMAT).withCause(e)
                        .withMessage(
                                "Mets parsing: Label attribute {} on div not is not of correct date time format ZULU (yyyy-MM-dd'T'HH:mm:ss'Z') in mets message {}")
                        .withMessageArgs(attributes.getValue("LABEL"), metsDocument.getDocumentId()).build();
            }
        }
        // getLastModificationDate() = on update or create, the new created last modification date
        digitalobject.setLastModificationDate(new Date());

        //validate CONTENTIDS format
        final String contentids = attributes.getValue("CONTENTIDS");
        validateContentIds(contentids);
        this.setContentIds(digitalobject, contentids);

        if (currentStructMap.getDigitalObject() == null) {
            currentStructMap.setDigitalObject(digitalobject);
            this.setCurrentDigitalObject(digitalobject);
        } else {
            this.checkCellarIdHierarchy(this.getCurrentDigitalObject(), digitalobject.getContentids(),
                    this.getCurrentDigitalObject().getType(), digitalobject.getType());
            this.getCurrentDigitalObject().addChildObject(digitalobject);
            this.setCurrentDigitalObject(digitalobject);
        }

        this.getDigitalObjects().put(attributes.getValue("ID"), digitalobject);
    }

    @Override
    protected void startBehavior(final Attributes attributes) {
        final String structMapId = attributes.getValue("STRUCTID");
        if (null == structMapId) {
            return;
        }

        this.handleTransformationBehavior(structMapId);
    }

    @Override
    protected void startMechanism(final Attributes attributes) {
        final String mechanismLabel = attributes.getValue("LABEL");

        if ("Transformation Mechanism".equalsIgnoreCase(mechanismLabel)) {
            this.handleTransformationMechanism(attributes);
        }
    }

    private void handleTransformationMechanism(final Attributes attributes) {
        if (this.getCurrentBehaviorTargetManifestation() != null && this.getCurrentBehaviorTargetContentStreamList() != null) {
            final ManifestationTransformator manifestationTransformator = new ManifestationTransformator();
            manifestationTransformator.setContentStreamList(this.getCurrentBehaviorTargetContentStreamList());
            manifestationTransformator.setTechnicalMetadataList(this.getCurrentBehaviorTargetTechnicalMetadataList());
            manifestationTransformator.setHref(attributes.getValue(XLINK, "href"));
            this.getCurrentBehaviorTargetManifestation().addTransformator(manifestationTransformator);
        }
    }

}
