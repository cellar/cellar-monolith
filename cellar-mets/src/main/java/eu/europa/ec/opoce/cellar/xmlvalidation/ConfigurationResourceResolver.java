package eu.europa.ec.opoce.cellar.xmlvalidation;

import eu.europa.ec.opoce.cellar.common.util.FileSystemUtils;
import eu.europa.ec.opoce.cellar.common.util.StringUtils;
import eu.europa.ec.opoce.cellar.semantic.spring.ResourceHelper;

import java.io.InputStream;
import java.io.Reader;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSResourceResolver;

/**
 * <p>ConfigurationResourceResolver class.</p>
 */
public class ConfigurationResourceResolver implements LSResourceResolver {

    public static final String DEFAULT_CP_ROOT = "xsd";

    private Resource resource;

    public ConfigurationResourceResolver(final Resource resource) {
        this.resource = resource;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LSInput resolveResource(String type, String namespaceURI, String publicId, String systemId, String baseURI) {
        if (null == systemId) {
            return null;
        }
        return new ConfigurationResourceInput(systemId, resource);
    }

    private static class ConfigurationResourceInput implements LSInput {

        private final String extResource;

        private ConfigurationResourceInput(final String systemId, final Resource resource) {
            String resourceClasspathFolder = DEFAULT_CP_ROOT;
            if (resource instanceof ClassPathResource) {
                final String resourceClasspath = ((ClassPathResource) resource).getPath();
                resourceClasspathFolder = StringUtils.removeEnd(resourceClasspath, "/" + resource.getFilename());
            }
            this.extResource = resourceClasspathFolder + (systemId.startsWith("/") ? systemId : "/" + systemId);
        }

        @Override
        public Reader getCharacterStream() {
            return null;
        }

        @Override
        public void setCharacterStream(Reader characterStream) {
        }

        @Override
        public InputStream getByteStream() {
            return ResourceHelper.getInputStream(FileSystemUtils.getResource(extResource));
        }

        @Override
        public void setByteStream(InputStream byteStream) {
        }

        @Override
        public String getStringData() {
            return null;
        }

        @Override
        public void setStringData(String stringData) {
        }

        @Override
        public String getSystemId() {
            return null;
        }

        @Override
        public void setSystemId(String systemId) {
        }

        @Override
        public String getPublicId() {
            return null;
        }

        @Override
        public void setPublicId(String publicId) {
        }

        @Override
        public String getBaseURI() {
            return null;
        }

        @Override
        public void setBaseURI(String baseURI) {
        }

        @Override
        public String getEncoding() {
            return null;
        }

        @Override
        public void setEncoding(String encoding) {
        }

        @Override
        public boolean getCertifiedText() {
            return false;
        }

        @Override
        public void setCertifiedText(boolean certifiedText) {
        }
    }
}
