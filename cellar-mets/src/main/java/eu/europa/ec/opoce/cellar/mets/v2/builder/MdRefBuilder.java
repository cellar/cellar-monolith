/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.mets.v2.builder
 *             FILE : MdRefBuilder.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 9, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.mets.v2.builder;

import eu.europa.ec.opoce.cellar.common.builder.IBuilder;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.MdSecType.MdRef;
import eu.europa.ec.opoce.cellar.mets.v2.builder.enums.LocType;
import eu.europa.ec.opoce.cellar.mets.v2.builder.enums.MdType;

/**
 * <class_description> {@link MdRef} builder.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 9, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class MdRefBuilder implements IBuilder<MdRef> {

    private final MdRef mdRef;

    private MdRefBuilder() {
        this.mdRef = new MdRef();
    }

    public static MdRefBuilder get() {
        return new MdRefBuilder();
    }

    public MdRefBuilder withLOCTYPE(final LocType locType) {
        this.mdRef.setLOCTYPE(locType.getValue());
        return this;
    }

    public MdRefBuilder withMIMETYPE(final String mimeType) {
        this.mdRef.setMIMETYPE(mimeType);
        return this;
    }

    public MdRefBuilder withMDTYPE(final MdType mdType) {
        this.mdRef.setMDTYPE(mdType.getValue());
        return this;
    }

    public MdRefBuilder withOTHERMDTYPE(final String otherMdType) {
        this.mdRef.setOTHERMDTYPE(otherMdType);
        return this;
    }

    public MdRefBuilder withHref(final String href) {
        this.mdRef.setHref(href);
        return this;
    }

    public MdRefBuilder withCHECKSUM(final String checksum) {
        this.mdRef.setCHECKSUM(checksum);
        return this;
    }

    public MdRefBuilder withCHECKSUMTYPE(final String checksumType) {
        this.mdRef.setCHECKSUMTYPE(checksumType);
        return this;
    }

    /* (non-Javadoc)
     * @see eu.europa.ec.opoce.cellar.common.builder.IBuilder#build()
     */
    @Override
    public MdRef build() {
        return this.mdRef;
    }
}
