package eu.europa.ec.opoce.cellar.configuration;

import eu.europa.ec.opoce.cellar.MetsErrors;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

/**
 * <p>OracleConfiguration interface.</p>
 */
public interface OracleConfiguration {

    /**
     * <p>getOracleLoadType.</p>
     *
     * @return a {@link eu.europa.ec.opoce.cellar.configuration.OracleConfiguration.OracleLoadType} object.
     */
    OracleLoadType getOracleLoadType();

    public enum OracleLoadType {
        BATCH {

            @Override
            protected <T> T acceptInternal(OracleLoadtypeVisitor<T> visitor) throws Exception {
                return visitor.visitBatch();
            }
        },
        BULK {

            @Override
            protected <T> T acceptInternal(OracleLoadtypeVisitor<T> visitor) throws Exception {
                return visitor.visitBulk();
            }
        },
        CONTEXT {

            @Override
            protected <T> T acceptInternal(OracleLoadtypeVisitor<T> visitor) throws Exception {
                return visitor.visitContextLoad();
            }
        },
        INLINE {

            @Override
            protected <T> T acceptInternal(OracleLoadtypeVisitor<T> visitor) throws Exception {
                return visitor.visitInline();
            }
        };

        protected abstract <T> T acceptInternal(OracleLoadtypeVisitor<T> visitor) throws Exception;

        public <T> T accept(OracleLoadtypeVisitor<T> visitor) {
            try {
                return acceptInternal(visitor);
            } catch (Exception e) {
                throw ExceptionBuilder.get(CellarException.class).withCode(MetsErrors.ERROR_WHILE_PROCESSING_DATA).withCause(e).build();
            }
        }

    }

    public interface OracleLoadtypeVisitor<T> {

        public T visitBatch() throws Exception;

        public T visitBulk() throws Exception;

        public T visitInline() throws Exception;

        public T visitContextLoad() throws Exception;
    }
}
