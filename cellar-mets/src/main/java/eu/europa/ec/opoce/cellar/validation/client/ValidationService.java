/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.validation.client
 *             FILE : MetsValidationService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Nov 29, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.validation.client;

import java.io.InputStream;

/**
 * <class_description> Mets validation service.
 * XmlValidationService: Validates the mets only with the XSDs.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Nov 29, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface ValidationService {

    /**
     * <p>validate.</p>
     *
     * @param input a {@link java.io.InputStream} object.
     */
    void validate(final InputStream input);
}
