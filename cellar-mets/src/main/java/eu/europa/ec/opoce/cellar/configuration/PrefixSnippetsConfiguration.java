package eu.europa.ec.opoce.cellar.configuration;

import com.google.common.base.MoreObjects;
import eu.europa.ec.opoce.cellar.domain.content.mets.PrefixConfiguration;

import java.util.Collections;
import java.util.Map;

/**
 * <p>PrefixSnippetsConfiguration class.</p>
 */
public class PrefixSnippetsConfiguration implements PrefixConfiguration {

    private Map<String, String> map;

    public PrefixSnippetsConfiguration(Map<String, String> map) {
        this.map = Collections.unmodifiableMap(map);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPrefixUri(String prefix) {
        return map.get(prefix);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, String> getPrefixUris() {
        return map;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("map", map)
                .toString();
    }
}
