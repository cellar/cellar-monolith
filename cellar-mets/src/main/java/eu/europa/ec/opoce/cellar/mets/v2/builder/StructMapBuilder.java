/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.mets.v2.builder
 *             FILE : StructMapBuilder.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 12, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.mets.v2.builder;

import eu.europa.ec.opoce.cellar.common.builder.IBuilder;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.DivType;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.DivType.Fptr;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.StructMapType;

import org.apache.commons.lang.NotImplementedException;

/**
 * <class_description> {@link StructMapType} builder.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 12, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class StructMapBuilder implements IBuilder<StructMapType>, IDivFptrAware<StructMapBuilder> {

    private final StructMapType structMap;

    private StructMapBuilder() {
        this.structMap = new StructMapType();
    }

    public static StructMapBuilder get() {
        return new StructMapBuilder();
    }

    public StructMapBuilder withID(final String id) {
        this.structMap.setID(id);
        return this;
    }

    @Override
    public StructMapBuilder withDiv(final DivType div) {
        this.structMap.setDiv(div);
        return this;
    }

    /* (non-Javadoc)
     * @see eu.europa.ec.opoce.cellar.common.builder.IBuilder#build()
     */
    @Override
    public StructMapType build() {
        return this.structMap;
    }

    /* (non-Javadoc)
     * @see eu.europa.ec.opoce.cellar.mets.v2.builder.IDivAware#withFptr(eu.europa.ec.opoce.cellar.mets.v2.DivType.Fptr)
     */
    @Override
    public StructMapBuilder withFptr(Fptr child) {
        throw new NotImplementedException("Not supported: it is not possible to attach a Fptr to a Structmap.");
    }
}
