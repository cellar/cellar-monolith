/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.mets.v2.builder
 *             FILE : FileBuilder.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 8, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.mets.v2.builder;

import eu.europa.ec.opoce.cellar.common.builder.IBuilder;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.FileType;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.FileType.FLocat;

/**
 * <class_description> {@link FileType} builder.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 8, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class FileBuilder implements IBuilder<FileType> {

    private final FileType file;

    private FileBuilder() {
        this.file = new FileType();
    }

    public static FileBuilder get() {
        return new FileBuilder();
    }

    public FileBuilder withID(final String id) {
        this.file.setID(id);
        return this;
    }

    public FileBuilder withMIMETYPE(final String mimeType) {
        this.file.setMIMETYPE(mimeType);
        return this;
    }

    public FileBuilder withCHECKSUM(final String checksum) {
        this.file.setCHECKSUM(checksum);
        return this;
    }

    public FileBuilder withCHECKSUMTYPE(final String checksumType) {
        this.file.setCHECKSUMTYPE(checksumType);
        return this;
    }

    public FileBuilder withFLocat(final FLocat flocat) {
        this.file.getFLocat().add(flocat);
        return this;
    }

    /* (non-Javadoc)
     * @see eu.europa.ec.opoce.cellar.common.builder.IBuilder#build()
     */
    @Override
    public FileType build() {
        return this.file;
    }
}
