package eu.europa.ec.opoce.cellar.configuration;

import com.thoughtworks.xstream.XStream;
import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.PrefixConfigurationService;
import eu.europa.ec.opoce.cellar.cl.domain.validator.ContentIdsValidator;
import eu.europa.ec.opoce.cellar.common.util.FileSystemUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.PrefixConfiguration;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.spring.ResourceHelper;
import eu.europa.ec.opoce.cellar.semantic.xstream.Regex;
import eu.europa.ec.opoce.cellar.semantic.xstream.Regexs;
import eu.europa.ec.opoce.cellar.semantic.xstream.StringList;
import eu.europa.ec.opoce.cellar.xmlvalidation.MetsXsdFileConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.*;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Service
/**
 * <p>MetsConfigurationService class.</p>
 */
public class MetsConfigurationService
        implements MetsXsdFileConfiguration, BaseUriConfiguration, PrefixConfigurationService, OracleConfiguration, OntologyConfiguration {

    private static final Logger LOG = LogManager.getLogger(MetsConfigurationService.class);

    private MetsConfigurationSettings settings;
    private PrefixSnippetsConfiguration prefixSnippetsConfiguration;
    private Resource metsXsdV1Resource;
    private Resource metsXsdV2Resource;
    private Resource nalMetsXsdResource;
    private OracleLoadType oracleLoadType;

    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    /**
     * <p>initialize.</p>
     */
    @PostConstruct
    public void initialize() throws IOException {
        Locale.setDefault(Locale.US);
        final Resource configResource = FileSystemUtils.getResource("config.xml");
        final XStream fromXml = new XStream();
        fromXml.allowTypes(new Class[]{
           eu.europa.ec.opoce.cellar.configuration.MetsConfigurationSettings.class
        });
        fromXml.setMode(XStream.NO_REFERENCES);
        fromXml.registerConverter(MimetypeSnippetConfiguration.CONVERTOR);
        fromXml.registerConverter(StringList.createConverter("uri"));
        fromXml.alias("cellar-configuration", MetsConfigurationSettings.class);
        try(InputStream is=ResourceHelper.getInputStream(configResource);) {
            this.settings = (MetsConfigurationSettings) fromXml.fromXML(is);
        }
        this.metsXsdV1Resource = FileSystemUtils.getResource(settings.getMetsXsdV1FilePath());
        this.metsXsdV2Resource = FileSystemUtils.getResource(settings.getMetsXsdV2FilePath());
        this.nalMetsXsdResource = FileSystemUtils.getResource(settings.getNalMetsXsdFilePath());

        final String cellarCmrOracleLoadType = settings.getCellarCmrOracleLoadType();
        this.oracleLoadType = OracleLoadType.valueOf(cellarCmrOracleLoadType.toUpperCase());

        final File cellarFolderRoot = new File(cellarConfiguration.getCellarFolderRoot());
        final File psiFile = new File(cellarFolderRoot, ContentIdsValidator.PSI_XML);
        final XStream fromXmlPsi = new XStream();
        fromXmlPsi.setMode(XStream.NO_REFERENCES);
        fromXmlPsi.processAnnotations(Regexs.class);
        fromXmlPsi.processAnnotations(Regex.class);
        //XStream 1.4.7 and onwards requires whitelisting deserialization entry points
        fromXmlPsi.allowTypes(new Class[]{
                eu.europa.ec.opoce.cellar.semantic.xstream.Regexs.class
        });
        try(InputStream psiResource = new FileInputStream(psiFile);) {
            Regexs psi = (Regexs) fromXmlPsi.fromXML(psiResource);
            Map<String, String> mapPrefixUris = psi.getPrefixUris(cellarConfiguration.getCellarUriDisseminationBase());
            prefixSnippetsConfiguration = new PrefixSnippetsConfiguration(mapPrefixUris);

            LOG.info(IConfiguration.CONFIG, "Mets configured with file [{}]: {}", psiFile, prefixSnippetsConfiguration);

        } catch (FileNotFoundException e) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.ENTITY_RESOLVING_ERROR)
                    .withMessage("the basedirectory for the application is not configured correctly: {} is not found in basedirectory {}")
                    .withMessageArgs(ContentIdsValidator.PSI_XML, cellarFolderRoot).withCause(e).build();
        }
    }

    /**
     * <p>getOwlCatalog.</p>
     *
     * @return file that contains the catalog for the owl-files
     */
    public File getPsiFile() {
        return new File(cellarConfiguration.getCellarFolderRoot(), ContentIdsValidator.PSI_XML);
    }

    /**
     * <p>getOwlCatalog.</p>
     *
     * @return file that contains the catalog for the owl-files
     */
    public String getPsiPath() {
        return cellarConfiguration.getCellarFolderRoot() + ContentIdsValidator.PSI_XML;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getBaseUri() {
        return cellarConfiguration.getCellarUriDisseminationBase();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Resource getMetsXsdV1Resource() {
        return this.metsXsdV1Resource;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Resource getMetsXsdV2Resource() {
        return this.metsXsdV2Resource;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Resource getNalMetsXsdResource() {
        return nalMetsXsdResource;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PrefixConfiguration getPrefixConfiguration() {
        return prefixSnippetsConfiguration;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPrefixUri(String prefix) {
        return prefixSnippetsConfiguration.getPrefixUri(prefix);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, String> getPrefixUris() {
        return prefixSnippetsConfiguration.getPrefixUris();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OracleLoadType getOracleLoadType() {
        return oracleLoadType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCdmOntologyUri() {
        return settings.getCdmOntologyUri();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getUpdatableCdmOntologyUris() {
        return settings.getUpdatableCdmOntologyUris();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getTdmOntologyUri() {
        return settings.getTdmOntologyUri();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getUpdatableTdmOntologyUris() {
        return settings.getUpdatableTdmOntologyUris();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getUpdatableFallbackOntologyUris() {
        return settings.getUpdatableFallbackOntologyUris();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getNalOntologyUri() {
        return settings.getNalOntologyUri();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getUpdatableNalOntologyUris() {
        return settings.getUpdatableNalOntologyUris();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getFallbackOntologyUri() {
        return settings.getFallbackOntologyUri();
    }

}
