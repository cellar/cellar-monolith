/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.mets.v2.builder.enums
 *             FILE : Loctype.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 15, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.mets.v2.builder.enums;

/**
 * <class_description> LocType enum.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 15, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public enum LocType {

    ARK("ARK"), URN("URN"), URL("URL"), PURL("PURL"), HANDLE("HANDLE"), DOI("DOI"), OTHER("OTHER");

    private final String value;

    private LocType(final String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
