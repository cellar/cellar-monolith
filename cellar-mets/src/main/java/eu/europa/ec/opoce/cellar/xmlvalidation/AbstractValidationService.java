package eu.europa.ec.opoce.cellar.xmlvalidation;

import eu.europa.ec.opoce.cellar.MetsErrors;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditBuilder;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventAction;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventProcess;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventType;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.helper.StringHelper;
import eu.europa.ec.opoce.cellar.semantic.spring.ResourceHelper;
import eu.europa.ec.opoce.cellar.validation.client.ValidationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.Resource;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXParseException;

import javax.annotation.PostConstruct;
import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.InputStream;

import static eu.europa.ec.opoce.cellar.AppConstants.NEWLINE;

/**
 * <p>AbstractValidationService class.</p>
 * This service validates a resource on the basis of its XSD.<br />
 */
public abstract class AbstractValidationService implements ValidationService {

    private static final Logger LOG = LogManager.getLogger(AbstractValidationService.class);

    private ValidatorThreadLocal validator;

    protected abstract Resource resolveResource();

    @PostConstruct
    private void init() {
        this.validator = new ValidatorThreadLocal(this.resolveResource());
    }

    /**
     * @see eu.europa.ec.opoce.cellar.validation.client.ValidationService#validate(java.io.InputStream)
     */
    @Override
    public void validate(final InputStream input) {
        validate(validator.get(), input);
    }

    /**
     * <p>validate.</p>
     *
     * @param validator a {@link javax.xml.validation.Validator} object.
     * @param input     a {@link java.io.InputStream} object.
     */
    private static void validate(final Validator validator, final InputStream input) {
        final long startTime = System.currentTimeMillis();
        //Log the start of process
        AuditBuilder.get(AuditTrailEventProcess.Ingestion).withAction(AuditTrailEventAction.AbstractValidation)
                .withType(AuditTrailEventType.Start)
                .withMessage("Validating...")
                .withLogger(LOG).withLogDatabase(false).logEvent();
        final ValidationErrorHandler errorHandler = new ValidationErrorHandler();
        validate(validator, input, errorHandler);

        //Log the end of process
        AuditBuilder.get(AuditTrailEventProcess.Ingestion).withAction(AuditTrailEventAction.AbstractValidation)
                .withType(AuditTrailEventType.End).withDuration(System.currentTimeMillis() - startTime)
                .withMessage("Validation done in {} ms.")
                .withMessageArgs(System.currentTimeMillis() - startTime)
                .withLogger(LOG).withLogDatabase(false).logEvent();
        if (null != errorHandler.error)
            throw ExceptionBuilder.get(CellarException.class).withCode(MetsErrors.XML_VALIDATOR_DETECTED_PROBLEMS)
                    .withMessage(errorHandler.error).build();
    }

    /**
     * <p>validate.</p>
     *
     * @param validator    a {@link javax.xml.validation.Validator} object.
     * @param input        a {@link java.io.InputStream} object.
     * @param errorHandler a {@link org.xml.sax.ErrorHandler} object.
     */
    private static void validate(final Validator validator, final InputStream input, final ErrorHandler errorHandler) {
        validator.reset();
        validator.setErrorHandler(errorHandler);
        try {
            validator.validate(new StreamSource(input));
        } catch (Exception e) {
            throw ExceptionBuilder.get(CellarException.class).withCode(MetsErrors.FAILED_TO_RUN_VALIDATOR).withCause(e)
                    .withMessage("Failed to run validator").build();
        }
    }

    private static class ValidatorThreadLocal extends ThreadLocal<Validator> {

        private Resource resource;

        private ValidatorThreadLocal(final Resource resource) {
            this.resource = resource;
        }

        @Override
        protected Validator initialValue() {
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            schemaFactory.setResourceResolver(new ConfigurationResourceResolver(this.resource));
            try(InputStream is= ResourceHelper.getInputStream(this.resource)) {
                Source schemaSource = new StreamSource(is);
                Schema schema = schemaFactory.newSchema(schemaSource);
                return schema.newValidator();
            } catch (Exception e) {
                throw ExceptionBuilder.get(CellarException.class).withCode(MetsErrors.FAILED_TO_INITIALIZE_VALIDATOR).withCause(e)
                        .withMessage(e.getMessage()).build();
            }
        }
    }

    private static class ValidationErrorHandler implements ErrorHandler {

        private String error;

        public void warning(SAXParseException exception) {
            LOG.warn(toSAXParseException2String(exception));
        }

        public void error(SAXParseException exception) {
            error = toSAXParseException2String(exception);
        }

        public void fatalError(SAXParseException exception) {
            error = toSAXParseException2String(exception);
        }

        private String toSAXParseException2String(SAXParseException exception) {
            String message = NEWLINE + "{}: {}." + NEWLINE + "\tPUBLIC {} SYSTEM {}" + NEWLINE + "\tLine {}, column {}.";

            return StringHelper.format(message, exception.getClass().getSimpleName(), exception.getMessage(),
                    exception.getPublicId() == null ? "unknown" : exception.getPublicId(),
                    exception.getSystemId() == null ? "unknown" : exception.getSystemId(), exception.getLineNumber(),
                    exception.getColumnNumber());
        }

    }

}
