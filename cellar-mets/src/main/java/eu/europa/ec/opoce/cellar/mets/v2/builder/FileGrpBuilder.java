/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.mets.v2.builder
 *             FILE : FileGrpBuilder.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 12, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.mets.v2.builder;

import eu.europa.ec.opoce.cellar.common.builder.IBuilder;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.FileType;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.MetsType.FileSec.FileGrp;

/**
 * <class_description> {@link FileGrp} builder.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 12, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class FileGrpBuilder implements IBuilder<FileGrp> {

    private final FileGrp fileGrp;

    private FileGrpBuilder() {
        this.fileGrp = new FileGrp();
    }

    public static FileGrpBuilder get() {
        return new FileGrpBuilder();
    }

    public FileGrpBuilder withFile(final FileType file) {
        this.fileGrp.getFile().add(file);
        return this;
    }

    /* (non-Javadoc)
     * @see eu.europa.ec.opoce.cellar.common.builder.IBuilder#build()
     */
    @Override
    public FileGrp build() {
        return this.fileGrp;
    }
}
