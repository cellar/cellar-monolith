/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.mets.v2.builder
 *             FILE : IContentIdAware.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 25, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.mets.v2.builder;

/**
 * <class_description> ID aware interface: Metadata and content streams have identifiers.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 25, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IContentIdAware<R> {

    R withCONTENTID(final String contentId);
}
