package eu.europa.ec.opoce.cellar.xmlvalidation;

import org.springframework.core.io.Resource;

/**
 * <p>MetsXsdFileConfiguration interface.</p>
 */
public interface MetsXsdFileConfiguration {

    /**
     * <p>getMetsXsdV1Resource.</p>
     *
     * @return a {@link org.springframework.core.io.Resource} object.
     */
    Resource getMetsXsdV1Resource();

    /**
     * <p>getMetsXsdV2Resource.</p>
     *
     * @return a {@link org.springframework.core.io.Resource} object.
     */
    Resource getMetsXsdV2Resource();

    /**
     * <p>getNalMetsXsdResource.</p>
     *
     * @return a {@link org.springframework.core.io.Resource} object.
     */
    Resource getNalMetsXsdResource();
}
