/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.mets.v2.builder
 *             FILE : FLocatBuilder.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 12, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.mets.v2.builder;

import eu.europa.ec.opoce.cellar.common.builder.IBuilder;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.FileType.FLocat;
import eu.europa.ec.opoce.cellar.mets.v2.builder.enums.LocType;

/**
 * <class_description> {@link FLocat} builder.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 12, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class FLocatBuilder implements IBuilder<FLocat> {

    private FLocat flocat;

    private FLocatBuilder() {
        this.flocat = new FLocat();
    }

    public static FLocatBuilder get() {
        return new FLocatBuilder();
    }

    public FLocatBuilder withLOCTYPE(final LocType locType) {
        this.flocat.setLOCTYPE(locType.getValue());
        return this;
    }

    public FLocatBuilder withHref(final String href) {
        this.flocat.setHref(href);
        return this;
    }

    /* (non-Javadoc)
     * @see eu.europa.ec.opoce.cellar.common.builder.IBuilder#build()
     */
    @Override
    public FLocat build() {
        return this.flocat;
    }
}
