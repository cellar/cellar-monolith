/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.mets.v2.builder
 *             FILE : AmdSecBuilder.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 8, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.mets.v2.builder;

import eu.europa.ec.opoce.cellar.common.builder.IBuilder;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.AmdSecType;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.MdSecType;

/**
 * <class_description> {@link AmdSecType} builder.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 8, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class AmdSecBuilder implements IBuilder<AmdSecType> {

    private final AmdSecType amdSec;

    private AmdSecBuilder() {
        this.amdSec = new AmdSecType();
    }

    public static AmdSecBuilder get() {
        return new AmdSecBuilder();
    }

    public AmdSecBuilder withID(final String id) {
        this.amdSec.setID(id);
        return this;
    }

    public AmdSecBuilder withMdRef(final MdSecType mdSec) {
        this.amdSec.getTechMD().add(mdSec);
        return this;
    }

    /* (non-Javadoc)
     * @see eu.europa.ec.opoce.cellar.common.builder.IBuilder#build()
     */
    @Override
    public AmdSecType build() {
        return this.amdSec;
    }
}
