/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.mets.v2.builder
 *             FILE : FptrBuilder.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 8, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.mets.v2.builder;

import eu.europa.ec.opoce.cellar.common.builder.IBuilder;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.DivType.Fptr;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.FileType;

/**
 * <class_description> {@link Fptr} builder.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 8, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class FptrBuilder implements IBuilder<Fptr>, IContentIdAware<FptrBuilder> {

    private final Fptr fptr;

    private FptrBuilder() {
        this.fptr = new Fptr();
    }

    public static FptrBuilder get() {
        return new FptrBuilder();
    }

    public FptrBuilder withFILEID(final FileType file) {
        this.fptr.setFILEID(file);
        return this;
    }

    @Override
    public FptrBuilder withCONTENTID(final String contentId) {
        this.fptr.getCONTENTIDS().add(contentId);
        return this;
    }

    /* (non-Javadoc)
     * @see eu.europa.ec.opoce.cellar.common.builder.IBuilder#build()
     */
    @Override
    public Fptr build() {
        return this.fptr;
    }
}
