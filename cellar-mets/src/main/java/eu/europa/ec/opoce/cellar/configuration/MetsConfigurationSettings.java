package eu.europa.ec.opoce.cellar.configuration;

import eu.europa.ec.opoce.cellar.domain.content.mets.PrefixConfiguration;
import eu.europa.ec.opoce.cellar.semantic.xstream.StringList;

import java.util.List;

/**
 * <p>MetsConfigurationSettings class.</p>
 */
public class MetsConfigurationSettings {

    private PrefixSnippetsConfiguration snippetsConfiguration;
    private String metsXsdV1FilePath;
    private String metsXsdV2FilePath;
    private String nalMetsXsdFilePath;
    private String cellarCmrOracleLoadType;
    private String cdmOntologyUri;
    private StringList updatableCdmOntologyUris;
    private String tdmOntologyUri;
    private StringList updatableTdmOntologyUris;
    private String fallbackOntologyUri;
    private StringList updatableFallbackOntologyUris;
    private String nalOntologyUri;
    private StringList updatableNalOntologyUris;

    /**
     * <p>Getter for the field <code>snippetsConfiguration</code>.</p>
     *
     * @return a {@link eu.europa.ec.opoce.cellar.domain.content.mets.PrefixConfiguration} object.
     */
    public PrefixConfiguration getSnippetsConfiguration() {
        return snippetsConfiguration;
    }

    /**
     * <p>Getter for the field <code>metsXsdV1FilePath</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getMetsXsdV1FilePath() {
        return this.metsXsdV1FilePath;
    }

    /**
     * <p>Getter for the field <code>metsXsdV2FilePath</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getMetsXsdV2FilePath() {
        return this.metsXsdV2FilePath;
    }

    /**
     * <p>Getter for the field <code>nalMetsXsdFilePath</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getNalMetsXsdFilePath() {
        return nalMetsXsdFilePath;
    }

    /**
     * <p>Getter for the field <code>cellarCmrOracleLoadType</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCellarCmrOracleLoadType() {
        return cellarCmrOracleLoadType;
    }

    /**
     * <p>Getter for the field <code>cdmOntologyUri</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCdmOntologyUri() {
        return cdmOntologyUri;
    }

    /**
     * <p>Getter for the field <code>updatableCdmOntologyUris</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<String> getUpdatableCdmOntologyUris() {
        return updatableCdmOntologyUris;
    }

    /**
     * <p>Getter for the field <code>tdmOntologyUri</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getTdmOntologyUri() {
        return tdmOntologyUri;
    }

    /**
     * <p>Getter for the field <code>updatableTdmOntologyUris</code>.</p>
     *
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.xstream.StringList} object.
     */
    public StringList getUpdatableTdmOntologyUris() {
        return updatableTdmOntologyUris;
    }

    /**
     * <p>Getter for the field <code>fallbackOntologyUri</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFallbackOntologyUri() {
        return fallbackOntologyUri;
    }

    /**
     * <p>Getter for the field <code>updatableFallbackOntologyUris</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<String> getUpdatableFallbackOntologyUris() {
        return updatableFallbackOntologyUris;
    }

    /**
     * <p>Getter for the field <code>nalOntologyUri</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getNalOntologyUri() {
        return nalOntologyUri;
    }

    /**
     * <p>Getter for the field <code>updatableNalOntologyUris</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<String> getUpdatableNalOntologyUris() {
        return updatableNalOntologyUris;
    }

}
