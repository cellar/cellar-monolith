/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.mets.v2.builder
 *             FILE : DivBuilder.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 8, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.mets.v2.builder;

import eu.europa.ec.opoce.cellar.common.builder.IBuilder;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.DivType;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.DivType.Fptr;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.MdSecType;

/**
 * <class_description> {@link DivType} builder.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 8, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class DivBuilder implements IBuilder<DivType>, IDivFptrAware<DivBuilder>, IContentIdAware<DivBuilder> {

    private final DivType div;

    private DivBuilder() {
        this.div = new DivType();
    }

    public static DivBuilder get() {
        return new DivBuilder();
    }

    public DivBuilder withTYPE(final DigitalObjectType type) {
        this.div.setTYPE(type.getLabel().toLowerCase());
        return this;
    }

    @Override
    public DivBuilder withCONTENTID(final String contentId) {
        this.div.getCONTENTIDS().add(contentId);
        return this;
    }

    public DivBuilder withDMDID(final MdSecType dmd) {
        this.div.getDMDID().add(dmd);
        return this;
    }

    public DivBuilder withADMID(final MdSecType amd) {
        this.div.getADMID().add(amd);
        return this;
    }

    @Override
    public DivBuilder withDiv(final DivType child) {
        this.div.getDiv().add(child);
        return this;
    }

    @Override
    public DivBuilder withFptr(final Fptr child) {
        this.div.getFptr().add(child);
        return this;
    }

    /* (non-Javadoc)
     * @see eu.europa.ec.opoce.cellar.common.builder.IBuilder#build()
     */
    @Override
    public DivType build() {
        return this.div;
    }
}
