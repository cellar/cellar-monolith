package eu.europa.ec.opoce.cellar;

import eu.europa.ec.opoce.cellar.MessageCode.TYPE;

/**
 * <p>MetsMessageCode class.</p>
 */
public final class MetsErrors extends CommonErrors {

    /**
     * Constant <code>CELLAR_BUILD_NOT_FOUND</code>
     */
    public static final MessageCode CELLAR_BUILD_NOT_FOUND = new MessageCode("E-380");
    /**
     * Constant <code>CELLAR_VERSION_NOT_FOUND</code>
     */
    public static final MessageCode CELLAR_VERSION_NOT_FOUND = new MessageCode("E-381");
    /**
     * Constant <code>FAILED_TO_LOAD_PROPERTIES</code>
     */
    public static final MessageCode FAILED_TO_LOAD_PROPERTIES = new MessageCode("E-382");
    /**
     * Constant <code>FAILED_TO_LOAD_VERSION_PROPERTIES</code>
     */
    public static final MessageCode FAILED_TO_LOAD_VERSION_PROPERTIES = new MessageCode("E-383");
    /**
     * Constant <code>CODE_DIRECTORY_NOT_SET</code>
     */
    public static final MessageCode CODE_DIRECTORY_NOT_SET = new MessageCode("E-384");
    /**
     * Constant <code>FAILED_TO_RUN_VALIDATOR</code>
     */
    public static final MessageCode FAILED_TO_RUN_VALIDATOR = new MessageCode("E-385");
    /**
     * Constant <code>XML_VALIDATOR_DETECTED_PROBLEMS</code>
     */
    public static final MessageCode XML_VALIDATOR_DETECTED_PROBLEMS = new MessageCode("E-386", TYPE.FUNCTIONAL_ERROR);
    /**
     * Constant <code>FAILED_TO_INITIALIZE_VALIDATOR</code>
     */
    public static final MessageCode FAILED_TO_INITIALIZE_VALIDATOR = new MessageCode("E-387");
    /**
     * Constant <code>GIVEN_RESOURCE_DOES_NOT_EXIST</code>
     */
    public static final MessageCode GIVEN_RESOURCE_DOES_NOT_EXIST = new MessageCode("E-388");
    /**
     * Constant <code>ERROR_IN_CONFIGFILE</code>
     */
    public static final MessageCode ERROR_IN_CONFIGFILE = new MessageCode("E-389");
    /**
     * Constant <code>ERROR_WHILE_PROCESSING_DATA</code>
     */
    public static final MessageCode ERROR_WHILE_PROCESSING_DATA = new MessageCode("E-390");

}
