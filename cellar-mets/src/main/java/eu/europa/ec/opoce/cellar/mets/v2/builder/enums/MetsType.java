/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.mets.v2.builder.enums
 *             FILE : MetsType.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 16, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.mets.v2.builder.enums;

/**
 * <class_description> METS type enum.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 16, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public enum MetsType {
    create, //
    update, //
    delete;
}
