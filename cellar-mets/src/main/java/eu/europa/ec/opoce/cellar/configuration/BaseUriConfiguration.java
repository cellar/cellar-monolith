package eu.europa.ec.opoce.cellar.configuration;

/**
 * <p>BaseUriConfiguration interface.</p>
 */
public interface BaseUriConfiguration {

    /**
     * <p>getBaseUri.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    String getBaseUri();
}
