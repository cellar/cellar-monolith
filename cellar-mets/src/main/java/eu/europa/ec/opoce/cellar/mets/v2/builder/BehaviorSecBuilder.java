/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.mets.v2.builder
 *             FILE : StructMapBuilder.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 12, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.mets.v2.builder;

import eu.europa.ec.opoce.cellar.common.builder.IBuilder;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.BehaviorSecType;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.BehaviorType;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.StructMapType;

/**
 * <class_description> {@link StructMapType} builder.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 12, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class BehaviorSecBuilder implements IBuilder<BehaviorSecType> {

    private final BehaviorSecType behaviorSec;

    private BehaviorSecBuilder() {
        this.behaviorSec = new BehaviorSecType();
    }

    public static BehaviorSecBuilder get() {
        return new BehaviorSecBuilder();
    }

    public BehaviorSecBuilder withBehavior(final BehaviorType behavior) {
        this.behaviorSec.getBehavior().add(behavior);
        return this;
    }

    /* (non-Javadoc)
     * @see eu.europa.ec.opoce.cellar.common.builder.IBuilder#build()
     */
    @Override
    public BehaviorSecType build() {
        return this.behaviorSec;
    }

}
