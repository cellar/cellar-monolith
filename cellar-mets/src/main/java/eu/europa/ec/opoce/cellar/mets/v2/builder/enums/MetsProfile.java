/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.mets.v2.builder.enums
 *             FILE : MetsProfile.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 16, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.mets.v2.builder.enums;

/**
 * <class_description> METS profile (supported) enum.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 16, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public enum MetsProfile {

    // METS v1 is not supported by Mets builders

    v2("http://publications.europa.eu/resource/mets/op-sip-profile_002");

    private String value;

    private MetsProfile(final String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
