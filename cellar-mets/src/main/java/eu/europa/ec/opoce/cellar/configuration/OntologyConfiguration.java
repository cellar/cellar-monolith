package eu.europa.ec.opoce.cellar.configuration;

import java.util.List;

/**
 * <p>OntologyConfiguration interface.</p>
 */
public interface OntologyConfiguration {

    /**
     * <p>getCdmOntologyUri.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCdmOntologyUri();

    /**
     * <p>getUpdatableCdmOntologyUris.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<String> getUpdatableCdmOntologyUris();

    /**
     * <p>getTdmOntologyUri.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getTdmOntologyUri();

    /**
     * <p>getUpdatableTdmOntologyUris.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<String> getUpdatableTdmOntologyUris();

    /**
     * <p>getFallbackOntologyUri.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFallbackOntologyUri();

    /**
     * <p>getUpdatableFallbackOntologyUris.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<String> getUpdatableFallbackOntologyUris();

    /**
     * <p>getNalOntologyUri.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getNalOntologyUri();

    /**
     * <p>getUpdatableNalOntologyUris.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<String> getUpdatableNalOntologyUris();

}
