/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.mets.v2.builder
 *             FILE : MetsBuilder.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 8, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.mets.v2.builder;

import eu.europa.ec.opoce.cellar.common.builder.IBuilder;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.AmdSecType;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.BehaviorSecType;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.MdSecType;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.Mets;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.MetsType.FileSec;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.MetsType.FileSec.FileGrp;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.MetsType.MetsHdr;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.MetsType.MetsHdr.MetsDocumentID;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.StructMapType;
import eu.europa.ec.opoce.cellar.mets.v2.builder.enums.MetsProfile;
import eu.europa.ec.opoce.cellar.mets.v2.builder.enums.MetsType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * <class_description> {@link Mets} builder.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 8, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class MetsBuilder implements IBuilder<Mets> {

    private static final Logger LOG = LogManager.getLogger(MetsBuilder.class);

    /**
     * The mets.
     */
    private final Mets mets;

    /**
     * The mets hdr.
     */
    private MetsHdr metsHdr;

    /**
     * The mets document id.
     */
    private MetsDocumentID metsDocumentID;

    /**
     * The file sec.
     */
    private final FileSec fileSec;

    /**
     * The use agnostic url.
     */
    private Boolean useAgnosticURL;

    /**
     * Instantiates a new mets builder.
     */
    private MetsBuilder() {
        this.mets = new Mets();
        this.metsHdr = null;
        this.metsDocumentID = null;
        this.fileSec = new FileSec();
        this.mets.getFileSec().add(this.fileSec);
    }

    /**
     * Gets the.
     *
     * @return the mets builder
     */
    public static MetsBuilder create() {
        return new MetsBuilder();
    }

    /**
     * With profile.
     *
     * @param profile the profile
     * @return the mets builder
     */
    public MetsBuilder withPROFILE(final MetsProfile profile) {
        this.mets.setPROFILE(profile.getValue());
        return this;
    }

    /**
     * With type.
     *
     * @param type the type
     * @return the mets builder
     */
    public MetsBuilder withTYPE(final MetsType type) {
        this.mets.setTYPE(type.toString());
        return this;
    }

    /**
     * With createdate.
     *
     * @param createDate the create date
     * @return the mets builder
     */
    public MetsBuilder withCREATEDATE(final Date createDate) {
        getMetsHdrInstance().setCREATEDATE(fromDate(createDate));
        return this;
    }

    public static XMLGregorianCalendar fromDate(Date date) {
        final GregorianCalendar cal = new GregorianCalendar();
        cal.setTime(date);

        XMLGregorianCalendar c = null;
        try {
            c = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
        } catch (final DatatypeConfigurationException e) {
            LOG.error(e);
        }
        return c;
    }

    /**
     * With dmd sec.
     *
     * @param dmd the dmd
     * @return the mets builder
     */
    public MetsBuilder withDmdSec(final MdSecType dmd) {
        this.mets.getDmdSec().add(dmd);
        return this;
    }

    public MetsBuilder withMetsHdr(MetsHdr metsHdr) {
        this.metsHdr = metsHdr;
        return this;
    }

    /**
     * With amd sec.
     *
     * @param amd the amd
     * @return the mets builder
     */
    public MetsBuilder withAmdSec(final AmdSecType amd) {
        this.mets.getAmdSec().add(amd);
        return this;
    }

    /**
     * With struct map.
     *
     * @param structMap the struct map
     * @return the mets builder
     */
    public MetsBuilder withStructMap(final StructMapType structMap) {
        this.mets.getStructMap().add(structMap);
        return this;
    }

    /**
     * With mets document id.
     *
     * @param metsDocumentId the mets document id
     * @return the mets builder
     */
    public MetsBuilder withMetsDocumentID(final String metsDocumentId) {
        getMetsDocumentID().setValue(metsDocumentId);
        return this;
    }

    /**
     * With behavior sec.
     *
     * @param behaviorSec the behavior sec
     * @return the mets builder
     */
    public MetsBuilder withBehaviorSec(final BehaviorSecType behaviorSec) {
        this.mets.getBehaviorSec().add(behaviorSec);
        return this;
    }

    /**
     * With file grp.
     *
     * @param fileGrp the file grp
     * @return the mets builder
     */
    public MetsBuilder withFileGrp(final FileGrp fileGrp) {
        this.fileSec.getFileGrp().add(fileGrp);
        return this;
    }

    /**
     * Without file sec.
     *
     * @return the mets builder
     */
    public MetsBuilder withoutFileSec() {
        this.mets.getFileSec().clear();
        return this;
    }

    /**
     * With agnostic resource url.
     *
     * @param useAgnosticURL the use agnostic url
     * @return the mets builder
     */
    public MetsBuilder withAgnosticResourceURL(final Boolean useAgnosticURL) {
        this.useAgnosticURL = useAgnosticURL;
        return this;
    }

    /**
     * Gets the mets document id.
     *
     * @return the mets document id
     */
    private MetsDocumentID getMetsDocumentID() {
        if (this.metsDocumentID == null) {
            this.metsDocumentID = new MetsDocumentID();
        }

        return this.metsDocumentID;
    }

    /**
     * Gets the mets hdr instance.
     *
     * @return the mets hdr instance
     */
    private MetsHdr getMetsHdrInstance() {
        if (this.metsHdr == null) {
            this.metsHdr = new MetsHdr();
        }

        return this.metsHdr;
    }

    /**
     * Gets the use agnostic url.
     *
     * @return the use agnostic url
     */
    public Boolean getUseAgnosticURL() {
        return this.useAgnosticURL;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Mets build() {

        if (this.fileSec.getFileGrp().isEmpty()) {
            withFileGrp(FileGrpBuilder.get().build());
        }

        if (this.metsDocumentID != null) {
            getMetsHdrInstance().setMetsDocumentID(getMetsDocumentID());
        }

        if (this.metsHdr != null) {
            this.mets.setMetsHdr(this.metsHdr);
        }

        return this.mets;
    }
}
