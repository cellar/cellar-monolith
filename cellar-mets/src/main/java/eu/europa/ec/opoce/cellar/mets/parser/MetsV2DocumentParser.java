package eu.europa.ec.opoce.cellar.mets.parser;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.MetsErrors;
import eu.europa.ec.opoce.cellar.cl.domain.validator.ContentIdsValidator;
import eu.europa.ec.opoce.cellar.cl.domain.validator.ValidationDetails;
import eu.europa.ec.opoce.cellar.cl.domain.validator.ValidationResult;
import eu.europa.ec.opoce.cellar.cl.exception.ValidationException;
import eu.europa.ec.opoce.cellar.cl.service.client.ValidationService;
import eu.europa.ec.opoce.cellar.common.util.FileSystemUtils;
import eu.europa.ec.opoce.cellar.common.util.StringUtils;
import eu.europa.ec.opoce.cellar.common.util.TimeUtils;
import eu.europa.ec.opoce.cellar.common.util.XMLUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.*;
import eu.europa.ec.opoce.cellar.domain.content.mets.impl.GetAllDigitalObjectsStrategy;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.mets.parser.MetsBehavior.BehaviourType;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.w3c.dom.Document;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.xpath.XPath;
import java.io.IOException;
import javax.xml.xpath.XPathVariableResolver;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * SAX2 event handler that parses a METS document version 2. Parses and
 * validates the mime types of the mets document.
 */
public class MetsV2DocumentParser extends DocumentParser implements IMetsDocumentParser {

    private static final Logger LOG = LogManager.getLogger(MetsV2DocumentParser.class);

    private final MetsPackage metsPackage;
    private final MetsDocument metsDocument;

    private MetadataStream currentMetadataStream;
    private ContentStream currentContentStream;
    private String mimetype;
    private String checksum;
    private String checksumType;
    private String identifier;
    private StringBuilder text;
    private StructMap currentStructMap;
    private DigitalObject currentDigitalObject;

    // map of digital objects
    private Map<String, DigitalObject> digitalObjects;

    // temp objects for handling behavior in case it has a transformation
    // mechanism
    private DigitalObject currentBehaviorTargetManifestation;
    private List<ContentStream> currentBehaviorTargetContentStreamList;
    private List<Metadata> currentBehaviorTargetTechnicalMetadataList;

    // temp objects for handling digital objects that have a delete behavior
    private Collection<DigitalObject> deleteDigitalObjects;

    // Temp object for handling model-load on a digital object
    private DigitalObject modelLoadDigitalObject;

    // Temp object for handling ontology-load on a digital object
    private DigitalObject ontologyLoadDigitalObject;

    private DigitalObject sparqlLoadDigitalObject;

    // Temp object for identifying the behavior BTYPE
    private BehaviourType currentBehaviorType;

    /**
     * The validation service.
     */
    @Autowired
    private ValidationService validationService;

    @Autowired
    @Qualifier("contentIdsValidator")
    private ContentIdsValidator contentIdsValidator;

    /**
     * give all needed input to parse a metsPackage to a metsDocument
     *
     * @param metspackage         the metsPackage that needs to be parsed
     * @param prefixConfiguration configuration of the prefixes that are allowed to be used
     */
    public MetsV2DocumentParser(final MetsPackage metspackage) {
        this.metsPackage = metspackage;
        this.metsDocument = new MetsDocument();
    }

    /**
     * @see eu.europa.ec.opoce.cellar.domain.content.mets.IMetsDocumentParser#execute()
     */
    @Override
    public MetsDocument execute() {
        this.digitalObjects = new HashMap<String, DigitalObject>();
        this.parse();
        return this.metsDocument;
    }

    /**
     * @see org.xml.sax.helpers.DefaultHandler#startElement(java.lang.String,
     * java.lang.String, java.lang.String, org.xml.sax.Attributes)
     */
    @Override
    public void startElement(final String uri, final String localName, final String qName, final Attributes attributes)
            throws SAXException {
        this.text = new StringBuilder();
        if (!METS.equals(uri)) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.URI_NAMESPACE_SHOULD_BE_OF_METS)
                    .withMessage("Only supported namespace in manifest file is {}").withMessageArgs(METS).build();
        }

        if (qName.equals("mets")) {
            this.startMets(attributes);
        } else if (qName.equals("metsHdr")) {
            this.startMetsHdr(attributes);
        } else if (qName.equals("metsDocumentID")) {
            this.startMetsDocumentID(attributes);
        } else if (qName.equals("dmdSec")) {
            this.startDmdSec(attributes);
        } else if (qName.equals("techMD")) {
            this.startTechMD(attributes);
        } else if (qName.equals("mdRef")) {
            this.startMdRef(attributes);
        } else if (qName.equals("file")) {
            this.startFile(attributes);
        } else if (qName.equals("FLocat")) {
            this.startFLocat(attributes);
        } else if (qName.equals("structMap")) {
            this.startStructMap(attributes);
        } else if (qName.equals("div")) {
            this.startDiv(attributes);
        } else if (qName.equals("fptr")) {
            this.startFptr(attributes);
        } else if (qName.equals("behavior")) {
            this.startBehavior(attributes);
        } else if (qName.equals("mechanism")) {
            this.startMechanism(attributes);
        } else if (qName.equals("mdWrap")) {
            this.startMdWrap(attributes);
        }
    }

    /**
     * @see org.xml.sax.helpers.DefaultHandler#endElement(java.lang.String,
     * java.lang.String, java.lang.String)
     */
    @Override
    public void endElement(final String uri, final String localName, final String qName) throws SAXException {
        if (qName.equals("metsDocumentID")) {
            this.metsDocument.setDocumentId(this.text.toString());
        } else if (qName.equals("dmdSec")) {
            this.metsDocument.addMetadataStream(this.identifier, this.currentMetadataStream);
        } else if (qName.equals("techMD")) {
            this.metsDocument.addMetadataStream(this.identifier, this.currentMetadataStream);
        } else if (qName.equals("fptr")) {
            if (this.currentDigitalObject.getContentStreams().size() == 0) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.WHEN_FILEPOINTER_DEFINED_CONTENTSTREAM_IS_NEEDED)
                        .withMessage("Abnormal situation: no content stream on current digital object {}")
                        .withMessageArgs(this.currentDigitalObject.getCellarId().getIdentifier()).build();
            }
        } else if (qName.equals("file")) {
            this.metsDocument.addContentStream(this.identifier, this.currentContentStream);
            this.currentContentStream = null;
            this.identifier = null;
            this.mimetype = null;
            this.checksum = null;
            this.checksumType = null;
        } else if (qName.equals("structMap")) {
            this.currentStructMap = null;
        } else if (qName.equals("div")) {
            this.currentDigitalObject = this.currentDigitalObject.getParentObject();
        } else if (qName.equals("behavior")) {
            this.currentBehaviorTargetContentStreamList = null;
            this.currentBehaviorTargetManifestation = null;
            this.currentBehaviorType = null;
        }
        this.text = null;
    }

    /**
     * @see org.xml.sax.helpers.DefaultHandler#characters(char[], int, int)
     */
    @Override
    public void characters(final char[] ch, final int start, final int length) {
        if (null != this.text) {
            this.text.append(ch, start, length);
        }
    }

    /**
     * Validates MIMETYPE attribute of mdWrap.
     *
     * @param attributes the list of attributes of mdWrap
     */
    protected void startMdWrap(final Attributes attributes) {
        super.validateMimeType(attributes);
    }

    /**
     * get attributes of behavior
     */
    protected void startBehavior(final Attributes attributes) {
        final String structMapId = attributes.getValue("STRUCTID");
        if (null == structMapId) {
            return;
        }

        final String behaviorType = attributes.getValue("BTYPE");
        this.currentBehaviorType = BehaviourType.getByName(behaviorType);

        switch (this.currentBehaviorType) {
            case delete:
                handleDeleteBehavior(structMapId);
                break;
            case transformation:
                handleTransformationBehavior(structMapId);
                break;
            case modelLoad:
                handleModelLoadBehavior(structMapId);
                break;
            case ontologyLoad:
                handleOntologyLoadBehavior(structMapId);
                break;
            case sparqlLoad:
                handleSparqlLoadBehavior(structMapId);
                break;
            default:
                LOG.error("Unsupported behaviorType " + currentBehaviorType);
        }
    }

    protected void startMechanism(final Attributes attributes) {
        final String linkValue = attributes.getValue(XLINK, "href");
        final MetsBehavior behavior = new MetsBehavior(linkValue);
        this.handleMechanism(behavior);
    }

    /**
     * get attributes of Mets
     */
    protected void startMets(final Attributes attributes) {
        final String profile = attributes.getValue("PROFILE");
        this.metsDocument.setProfile(MetsProfile.getByXmlValue(profile));

        final String type = attributes.getValue("TYPE");
        this.metsDocument.setType(OperationType.getByXmlValue(type));
    }

    /**
     * get attributes of MetsHdr
     */
    protected void startMetsHdr(final Attributes attributes) {
        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        final String dateString = attributes.getValue("CREATEDATE");
        if (null == dateString) {
            return;
        }
        try {
            final Date createDate = dateFormat.parse(dateString);
            this.metsDocument.setCreateDate(createDate);
        } catch (final ParseException ignore) {
        }
    }

    /**
     * get attributes of dmdSec
     */
    protected void startDmdSec(final Attributes attributes) {
        this.currentMetadataStream = null;
        this.identifier = attributes.getValue("ID");
    }

    /**
     * get attributes of StructMap
     */
    protected void startStructMap(final Attributes attributes) {
        this.currentStructMap = new StructMap(this.metsDocument);
        if (attributes.getIndex("ID") == -1) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.MISSING_ID_ON_STRUCTMAP)
                    .withMessage("Missing ID attribute on StructMap in mets document {}").withMessageArgs(this.metsDocument.getDocumentId())
                    .build();
        }
        this.currentStructMap.setId(attributes.getValue("ID"));
        this.metsDocument.addStructMap(this.currentStructMap);
    }

    /**
     * get attributes of techMD
     */
    protected void startTechMD(final Attributes attributes) {
        this.currentMetadataStream = null;
        this.identifier = attributes.getValue("ID");
    }

    /**
     * get attributes of file
     */
    protected void startFile(final Attributes attributes) {
        this.currentContentStream = null;
        this.identifier = attributes.getValue("ID");
        this.mimetype = attributes.getValue("MIMETYPE");
        super.validateMimeType(this.mimetype);
        this.checksum = attributes.getValue("CHECKSUM");
        this.checksumType = attributes.getValue("CHECKSUMTYPE");
    }

    /**
     * get attributes of Flocat
     */
    protected void startFLocat(final Attributes attributes) {

        final String flocat = attributes.getValue(XLINK, "href");
        String osSanitizedRefFilename = StringUtils.replaceBackslashesWithSlashes(flocat);
        // check if FLocat href is valid: for instance, white space is not
        // accepted
        try {
            new URI(osSanitizedRefFilename);
        } catch (final URISyntaxException e) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.INVALID_FILEPOINTER_FOR_CONTENTSTREAM)
                    .withMessage("Mets parsing: invalid FLocat href '{}'").withMessageArgs(flocat).build();
        }

        this.currentContentStream = new ContentStream(this.mimetype, flocat);
        this.currentContentStream.setChecksum(this.checksum);
        this.currentContentStream.setChecksumType(this.checksumType);

        if (attributes.getIndex("LOCTYPE") != -1) {
            final ContentStreamLocType contentStreamLocType = ContentStreamLocType.getByXmlValue(attributes.getValue("LOCTYPE"));
            this.currentContentStream.setLocationType(contentStreamLocType);
        }
        if (attributes.getIndex("OTHERLOCTYPE") != -1) {
            this.currentContentStream.setOtherLocType(attributes.getValue("OTHERLOCTYPE"));
        }
        if (attributes.getIndex("USE") != -1) {
            this.currentContentStream.setUse(attributes.getValue("USE"));
        }

        if (ContentStreamLocType.OTHER.equals(this.currentContentStream.getLocationType())) {
            if (attributes.getValue("OTHERLOCTYPE") == null || !attributes.getValue("OTHERLOCTYPE").equals("poCurie")) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.ONLY_URLS_SUPPORTED_AS_CONTENT)
                        .withMessage("Mets parsing: only expect URL to content").build();
            }
        } else {
            if (!attributes.getValue("LOCTYPE").equals("URL")) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.ONLY_URLS_SUPPORTED_AS_CONTENT)
                        .withMessage("Mets parsing: only expect URL to content").build();
            }
        }
    }

    /**
     * get attributes of mdRef
     */
    protected void startMdRef(final Attributes attributes) {
        final String mimeType = attributes.getValue("MIMETYPE");
        super.validateMimeType(mimeType);
        this.currentMetadataStream = new MetadataStream(mimeType, attributes.getValue(XLINK, "href"));
        if (!attributes.getValue("LOCTYPE").equals("URL")) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.ONLY_URLS_SUPPORTED_AS_METADATA)
                    .withMessage("Mets parsing: only expect URL to metadata").build();
        }
    }

    /**
     * get attributes of MetsDocumentID
     */
    protected void startMetsDocumentID(final Attributes attributes) {
        this.text = new StringBuilder();
    }

    /**
     * get attributes of div
     */
    protected void startDiv(final Attributes attributes) {
        final DigitalObject digitalobject = new DigitalObject(this.currentStructMap);

        DigitalObjectType digitalObjectType = DigitalObjectType.getByValue(attributes.getValue("TYPE"));
        if (this.currentStructMap.getDigitalObject() == null && DigitalObjectType.EVENT.equals(digitalObjectType)) {
            digitalObjectType = DigitalObjectType.TOPLEVELEVENT;
        }
        digitalobject.setType(digitalObjectType);

        if (this.metsDocument.getType() != OperationType.Read) {
            final Collection<DigitalObjectType> allowedRoots = Arrays.asList(DigitalObjectType.DOSSIER, DigitalObjectType.AGENT,
                    DigitalObjectType.WORK, DigitalObjectType.TOPLEVELEVENT);
            if (this.currentStructMap.getDigitalObject() == null && !allowedRoots.contains(digitalObjectType)) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.INVALID_STRUCTURE_OF_DIGITAL_OBJECTS)
                        .withMessage("Root element is of type '{}', allowed root elements are {}.")
                        .withMessageArgs(digitalObjectType, allowedRoots).build();
            }
            if (digitalObjectType == DigitalObjectType.EXPRESSION && this.currentDigitalObject.getType() != DigitalObjectType.WORK) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.INVALID_STRUCTURE_OF_DIGITAL_OBJECTS)
                        .withMessage("'expression' expect 'work' as parent.").build();
            }
            if (digitalObjectType == DigitalObjectType.MANIFESTATION
                    && this.currentDigitalObject.getType() != DigitalObjectType.EXPRESSION) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.INVALID_STRUCTURE_OF_DIGITAL_OBJECTS)
                        .withMessage("'manifestation' expects 'expression' as parent.").build();
            }
            if (digitalObjectType == DigitalObjectType.EVENT && this.currentDigitalObject.getType() != DigitalObjectType.DOSSIER) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.INVALID_STRUCTURE_OF_DIGITAL_OBJECTS)
                        .withMessage("'event' expects 'dossier' as parent.").build();
            }
        }

        if (attributes.getIndex("DMDID") != -1) {
            final Metadata metadata = new Metadata(digitalobject, MetadataType.DMD, attributes.getValue("DMDID"));
            digitalobject.setBusinessMetadata(metadata);
            this.currentStructMap.setMetadataOperationType(StructMapType.Node);
        }

        if (attributes.getIndex("DMDID") == -1 && this.isDeleteBehaviorPresentForDivOrItsStructMap(attributes)) {
            final Metadata metadata = new Metadata(digitalobject, MetadataType.ABSENT, null);
            digitalobject.setBusinessMetadata(metadata);

            this.currentStructMap.setMetadataOperationType(StructMapType.Node);
        }

        if (attributes.getIndex("ADMID") != -1) {
            final Metadata metadata = new Metadata(digitalobject, MetadataType.TECHNICAL, attributes.getValue("ADMID"));
            digitalobject.setTechnicalMetadata(metadata);
            this.currentStructMap.setContentOperationType(StructMapType.Node);
        }

        // Check existence of Label (last modification date)
        if (attributes.getIndex("LABEL") != -1) {
            if (this.metsDocument.getType().equals(OperationType.Create)) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.LABEL_ATTRIBUTE_NOT_EXCEPTED)
                        .withMessage("Mets parsing: Label attribute on div not acceptable for a mets message {} of type create")
                        .withMessageArgs(this.metsDocument.getDocumentId()).build();
            }
            try {
                final java.text.DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

                // explicitly set timezone of input if needed
                df.setTimeZone(java.util.TimeZone.getTimeZone("Zulu"));

                // 2001-12-17T09:30:57Z
                final Date date = df.parse(attributes.getValue("LABEL"));
                // getLabel() = on update, the previous modification date
                // provided by the mets package in div/@LABEL
                digitalobject.setLabel(date);
            } catch (final ParseException e) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.LABEL_ATTRIBUTE_NOT_CORRECT_DATEFORMAT)
                        .withMessage(
                                "Mets parsing: Label attribute {} on div not is not of correct date time format ZULU (yyyy-MM-dd'T'HH:mm:ss'Z') in mets message {}")
                        .withMessageArgs(attributes.getValue("LABEL"), this.metsDocument.getDocumentId()).build();
            }
        }
        // getLastModificationDate() = on update or create, the new created last
        // modification date
        digitalobject.setLastModificationDate(new Date());

        // validate CONTENTIDS format
        final String contentids = attributes.getValue("CONTENTIDS");
        validateContentIds(contentids);
        this.setContentIds(digitalobject, contentids);

        if (this.currentStructMap.getDigitalObject() == null) {
            this.currentStructMap.setDigitalObject(digitalobject);
            this.currentDigitalObject = digitalobject;
        } else {
            this.checkCellarIdHierarchy(this.currentDigitalObject, digitalobject.getContentids(), this.currentDigitalObject.getType(),
                    digitalobject.getType());
            this.currentDigitalObject.addChildObject(digitalobject);
            this.currentDigitalObject = digitalobject;
        }

        this.digitalObjects.put(attributes.getValue("ID"), digitalobject);
    }

    protected void checkCellarIdHierarchy(final DigitalObject digitalobjectParent, final List<ContentIdentifier> contentidsChild,
                                          final DigitalObjectType digitalobjectTypeParent, final DigitalObjectType digitalobjectTypeChild) {
        String cellarIdParent = StringUtils.EMPTY;
        boolean atLeastOneBusinessIdParent = false;
        int nbCellarIdParent = 0;
        final List<ContentIdentifier> contentidsParent = digitalobjectParent.getContentids();
        for (final ContentIdentifier contentIdentifier : contentidsParent) {
            if (contentIdentifier.getIdentifier().startsWith(ContentIdentifier.CELLAR_PREFIX)) {
                cellarIdParent = contentIdentifier.getIdentifier();
                nbCellarIdParent++;
            } else {
                atLeastOneBusinessIdParent = true;
            }
        }
        String cellarIdChild = StringUtils.EMPTY;
        boolean atLeastOneBusinessIdChild = false;
        int nbCellarIdChild = 0;
        for (final ContentIdentifier contentIdentifier : contentidsChild) {
            if (contentIdentifier.getIdentifier().startsWith(ContentIdentifier.CELLAR_PREFIX)) {
                cellarIdChild = contentIdentifier.getIdentifier();
                nbCellarIdChild++;
            } else {
                atLeastOneBusinessIdChild = true;
            }
        }
        if (!cellarIdChild.startsWith(cellarIdParent)) {
            throw ExceptionBuilder
                    .get(CellarException.class).withCode(CommonErrors.INVALID_CELLARID_HIERARCHY).withMessage("Cellar id child '"
                            + cellarIdChild + "' doesn't start with Cellar id parent '" + cellarIdParent + "'. They are different.")
                    .build();
        }
        if (!atLeastOneBusinessIdParent) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.AT_LEAST_ONE_BUSINESS_ID).build();
        }
        if (!atLeastOneBusinessIdChild) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.AT_LEAST_ONE_BUSINESS_ID).build();
        }
        if (nbCellarIdParent > 1) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.TOO_MANY_CELLAR_ID).build();
        }
        if (nbCellarIdChild > 1) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.TOO_MANY_CELLAR_ID).build();
        }

        this.checkCellarIdFormat(digitalobjectTypeParent, cellarIdParent);
        this.checkCellarIdFormat(digitalobjectTypeChild, cellarIdChild);
    }

    protected void checkCellarIdFormat(final DigitalObjectType digitalobjectType, final String cellarId) {
        // if there is no cellarId in contentIds of DigitalObject
        if (cellarId.equals(StringUtils.EMPTY)) {
            return;
        }
        switch (digitalobjectType) {
            case WORK:
            case DOSSIER:
            case AGENT:
            case TOPLEVELEVENT:
                if (!DigitalObjectType.isWorkDossierTopLevelEventOrAgent(cellarId)) {
                    throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.INVALID_CELLARID_FORMAT)
                            .withMessage("Cellar id '" + cellarId + "' doesn't match with the WORK/DOSSIER/AGENT Cellar id format.").build();
                }
                break;
            case EXPRESSION:
            case EVENT:
                if (!DigitalObjectType.isExpressionOrEvent(cellarId)) {
                    throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.INVALID_CELLARID_FORMAT)
                            .withMessage("Cellar id child '" + cellarId + "' doesn't match with the EXPRESSION/EVENT Cellar id format.")
                            .build();
                }
                break;
            case MANIFESTATION:
                if (!DigitalObjectType.isManifestation(cellarId)) {
                    throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.INVALID_CELLARID_FORMAT)
                            .withMessage("Cellar id child '" + cellarId + "' doesn't match with the MANIFESTATION Cellar id format.").build();
                }
                break;
            case ITEM:
                if (!DigitalObjectType.isItem(cellarId)) {
                    throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.INVALID_CELLARID_FORMAT)
                            .withMessage("Cellar id child '" + cellarId + "' doesn't match with the ITEM Cellar id format.").build();
                }
                break;
            default:
                throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.UNSUPPORTED_DIGITAL_OBJECT_TYPE)
                        .withMessage("Type [" + digitalobjectType + "]").build();
        }
    }

    private boolean isDeleteBehaviorPresentForDivOrItsStructMap(final Attributes attributes) {
            boolean behaviorEvaluation = false;
            try(InputStream is = this.metsPackage.getMetsStream()) {
            	final DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
            	domFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
                domFactory.setFeature("http://xml.org/sax/features/external-general-entities", false);
                domFactory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
                domFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
                domFactory.setXIncludeAware(false);
                domFactory.setExpandEntityReferences(false);
                Document document = domFactory.newDocumentBuilder().parse(is);

                final XPath xPath = XMLUtils.newXPathFactory().newXPath();

                // check if there is a behavior associated to the digital object
                // first
                String divOrStructMapId = attributes.getValue("ID");
                behaviorEvaluation = isDeleteBehaviorPresent(divOrStructMapId, document, xPath);

                // then - in case the check above is false - check if there is a
                // behavior associated to the structmap
                if (!behaviorEvaluation) {
                    divOrStructMapId = xPath.compile("//structMap/@ID").evaluate(document);
                    behaviorEvaluation = isDeleteBehaviorPresent(divOrStructMapId, document, xPath);
                }
            }

        catch (final Exception ex) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Unforeseen exception: see cause for details").withCause(ex)
                    .build();
        }
        // return if the delete behavior is present
        return behaviorEvaluation;
    }

    /**
     * get attributes of fptr
     */
    protected void startFptr(final Attributes attributes) {
        // check digitalobject is a manifestation
        if (this.currentDigitalObject.getType() != DigitalObjectType.MANIFESTATION) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.CONTENTSTREAMS_MUST_HAVE_MANIFESTATION_AS_PARENT)
                    .withMessage("Mets parsing: fptr expects manifestation as div parent element").build();
        }

        ContentStream contentstream;
        final String fileId = attributes.getValue("FILEID");
        if (null != fileId) {
            contentstream = this.metsDocument.getContentStream(fileId);
            if (contentstream == null) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.INVALID_FILEPOINTER_FOR_CONTENTSTREAM)
                        .withMessage("Mets parsing: unknown FILEID '{}'.").withMessageArgs(fileId).build();
            }
        } else {
            contentstream = new ContentStream();
        }

        // validate CONTENTIDS format
        final String contentids = attributes.getValue("CONTENTIDS");
        validateContentIds(contentids);
        this.setContentIds(contentstream, contentids);
        this.checkCellarIdHierarchy(this.currentDigitalObject, contentstream.getContentids(), this.currentDigitalObject.getType(),
                DigitalObjectType.ITEM);
        this.currentDigitalObject.addContentStream(contentstream);
    }

    protected void validateContentIds(String contentIds) {
        final String[] contentIdsSplit = contentIds.split(" ");
        ValidationDetails validationDetails = new ValidationDetails();
        ValidationResult metsNameValidatorResults = null;
        for (String contentId : contentIdsSplit) {
            validationDetails.setToValidate(contentId);
            metsNameValidatorResults = this.validationService.executeSystemValidator(validationDetails, this.contentIdsValidator);
            if (!metsNameValidatorResults.isValid()) {
                throw ExceptionBuilder.get(ValidationException.class).withCode(MetsErrors.XML_VALIDATOR_DETECTED_PROBLEMS)
                        .withMessage("XSD Validation error with 'CONTENTIDS' tag. The value '" + contentId + "' is invalid.").build();
            }
        }
    }

    protected void setContentIds(final CellarIdentifiedObject cellarIdentifiedObject, final String contentids) {
        final List<ContentIdentifier> contentidentifiers = new ArrayList<ContentIdentifier>();

        final StringTokenizer st = new StringTokenizer(contentids);
        while (st.hasMoreTokens()) {
            final String contentid = st.nextToken();
            final ContentIdentifier contentIdentifier = new ContentIdentifier(contentid);

            if (contentid.startsWith(ContentIdentifier.CELLAR_PREFIX)) {
                cellarIdentifiedObject.setCellarId(contentIdentifier);
            }
            contentidentifiers.add(contentIdentifier);
        }
        cellarIdentifiedObject.setContentids(contentidentifiers);
    }

    private void parse() {
        InputStream metsStream = null;
        try {
            final SAXParserFactory parserFactory = SAXParserFactory.newInstance();
            parserFactory.setFeature("http://xml.org/sax/features/external-general-entities", false);
            parserFactory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            parserFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            parserFactory.setNamespaceAware(true);
            final SAXParser parser = parserFactory.newSAXParser();
            metsStream = this.metsPackage.getMetsStream();
            parser.parse(metsStream, this);
        } catch (final CellarException e) {
            throw e;
        } catch (final Exception e) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.ERROR_WHILE_PARSING_THE_METS_DOCUMENT).withCause(e)
                    .build();
        } finally {
            IOUtils.closeQuietly(metsStream);
        }
    }

    protected void handleTransformationBehavior(final String structMapId) {
        final String[] smids = StringUtils.split(structMapId, " ");

        final String targetManifestationId = smids[smids.length - 1];
        final DigitalObject targetManifestation = this.digitalObjects.get(targetManifestationId);
        if (targetManifestation == null) {
            return;
        }

        final List<ContentStream> contentStreamToTransformList = new LinkedList<ContentStream>();
        final List<Metadata> technicalMetadataList = new LinkedList<Metadata>();
        for (int i = 0; i < smids.length - 1; i++) {
            final DigitalObject fromManifestation = this.digitalObjects.get(smids[i]);
            if (fromManifestation != null) {
                contentStreamToTransformList.addAll(fromManifestation.getContentStreams());
                technicalMetadataList.add(fromManifestation.getTechnicalMetadata());
            } else {
                return;
            }
        }

        this.currentBehaviorTargetManifestation = targetManifestation;
        this.currentBehaviorTargetContentStreamList = contentStreamToTransformList;
        this.currentBehaviorTargetTechnicalMetadataList = technicalMetadataList;
    }

    private void handleDeleteBehavior(final String behaviorId) {
        this.deleteDigitalObjects = new ArrayList<DigitalObject>();

        StructMap deleteStructMap = null;
        if ((deleteStructMap = this.metsDocument.getStructMap(behaviorId)) != null) {
            final GetAllDigitalObjectsStrategy getAllDigitalObjectsStrategy = new GetAllDigitalObjectsStrategy();
            deleteStructMap.getDigitalObject().applyStrategyOnSelfAndChildren(getAllDigitalObjectsStrategy);
            this.deleteDigitalObjects.addAll(getAllDigitalObjectsStrategy.getDigitalObjects());
        } else if (this.digitalObjects.containsKey(behaviorId)) {
            this.deleteDigitalObjects.add(this.digitalObjects.get(behaviorId));
        }
    }

    private void handleModelLoadBehavior(final String behaviorId) {
        this.modelLoadDigitalObject = this.digitalObjects.get(behaviorId);

        if (this.modelLoadDigitalObject == null) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.ERROR_WHILE_PARSING_THE_METS_DOCUMENT)
                    .withMessage("Model-load behavior identified by '{}' is not related to an existing digital object.")
                    .withMessageArgs(behaviorId).build();
        } else if (this.modelLoadDigitalObject.getType() != DigitalObjectType.MANIFESTATION) { // the
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.ERROR_WHILE_PARSING_THE_METS_DOCUMENT)
                    .withMessage("Model-load behavior identified by '{}' is not related to a MANIFESTATION.").withMessageArgs(behaviorId)
                    .build();
        }
    }

    private void handleOntologyLoadBehavior(final String behaviorId) {
        this.ontologyLoadDigitalObject = this.digitalObjects.get(behaviorId);

        if (this.ontologyLoadDigitalObject == null) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.ERROR_WHILE_PARSING_THE_METS_DOCUMENT)
                    .withMessage("Ontology-load behavior identified by '{}' is not related to an existing digital object.")
                    .withMessageArgs(behaviorId).build();
        } else if (this.ontologyLoadDigitalObject.getType() != DigitalObjectType.MANIFESTATION) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.ERROR_WHILE_PARSING_THE_METS_DOCUMENT)
                    .withMessage("Ontology-load behavior identified by '{}' is not related to a MANIFESTATION.").withMessageArgs(behaviorId)
                    .build();
        }
    }

    private void handleSparqlLoadBehavior(final String behaviorId) {
        this.sparqlLoadDigitalObject = this.digitalObjects.get(behaviorId);
        if (sparqlLoadDigitalObject == null) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.ERROR_WHILE_PARSING_THE_METS_DOCUMENT)
                    .withMessage("Sparql-load behavior identified by '{}' is not related to an existing digital object.")
                    .withMessageArgs(behaviorId).build();
        }
    }

    private void handleMechanism(final MetsBehavior behavior) {
        if (!behavior.getType().equals(this.currentBehaviorType)) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.ERROR_WHILE_PARSING_THE_METS_DOCUMENT).withMessage(
                    "The behavior with BTYPE '{}' is not compatible with the xlink:href '{}' specified in its underlying mechanism.") //
                    .withMessageArgs(this.currentBehaviorType, behavior).build();
        }

        switch (behavior.getType()) {
            case delete:
                handleDeleteMechanism(behavior);
                break;
            case transformation:
                handleTransformationMechanism(behavior);
                break;
            case modelLoad:
                handleModelLoadMechanism(behavior);
                break;
            case ontologyLoad:
                handleOntologyLoadMechanism(behavior);
                break;
            case sparqlLoad:
                handleSparqlLoadBehavior(behavior);
                break;
        }
    }

    private void handleTransformationMechanism(final MetsBehavior behavior) {
        if (this.currentBehaviorTargetManifestation != null && this.currentBehaviorTargetContentStreamList != null) {
            final ManifestationTransformator manifestationTransformator = new ManifestationTransformator();
            manifestationTransformator.setContentStreamList(this.currentBehaviorTargetContentStreamList);
            manifestationTransformator.setTechnicalMetadataList(this.currentBehaviorTargetTechnicalMetadataList);
            manifestationTransformator.setHref(behavior.getParamValue("type"));
            this.currentBehaviorTargetManifestation.addTransformator(manifestationTransformator);
        }
    }

    private void handleDeleteMechanism(final MetsBehavior behaviour) {
        if (this.deleteDigitalObjects == null) {
            return;
        }

        for (final DigitalObject deleteDigitalObject : this.deleteDigitalObjects) {
            deleteDigitalObject.setDeleteModelString(this.resolveDeleteModelString(behaviour));
            deleteDigitalObject.setDeleteSparqlQuery(behaviour.getParamValue("query"));
        }
    }

    private void handleModelLoadMechanism(final MetsBehavior behavior) {
        if (this.modelLoadDigitalObject == null) {
            return;
        }

        if (this.modelLoadDigitalObject.getModelLoadURL() != null) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.ERROR_WHILE_PARSING_THE_METS_DOCUMENT)
                    .withMessage("The model-load URL cannot be added to the digital object: the model-load URL '{}' is already present.")
                    .withMessageArgs(this.modelLoadDigitalObject.getModelLoadURL()).build();
        }

        final String modelLoadURL = behavior.getParamValue("model");
        this.modelLoadDigitalObject.setModelLoadURL(modelLoadURL);
        this.modelLoadDigitalObject.setModelLoadActivationDate(getModelLoadActivationDate(behavior));
    }

    private void handleOntologyLoadMechanism(final MetsBehavior behavior) {
        if (this.ontologyLoadDigitalObject == null) {
            return;
        }

        this.ontologyLoadDigitalObject.setOntologyLoad(true);
    }

    private void handleSparqlLoadBehavior(MetsBehavior behavior) {
        if (this.sparqlLoadDigitalObject == null) {
            return;
        }

        if (this.sparqlLoadDigitalObject.getSparqlLoadURL() != null) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.ERROR_WHILE_PARSING_THE_METS_DOCUMENT)
                    .withMessage("The sparql-load URL cannot be added to the digital object: the sparql-load URL '{}' is already present.")
                    .withMessageArgs(this.sparqlLoadDigitalObject.getSparqlLoadURL()).build();
        }

        final String modelLoadURL = behavior.getParamValue("model");
        this.sparqlLoadDigitalObject.setSparqlLoadURL(modelLoadURL);
        this.sparqlLoadDigitalObject.setSparqlLoadActivationDate(getModelLoadActivationDate(behavior));
    }

    private String resolveDeleteModelString(final MetsBehavior behaviour) {
        final String metadataLink = behaviour.getParamValue("file");
        if (metadataLink == null) {
            return null;
        }

        try(final InputStream metadataStream = this.metsPackage.getFileStream(metadataLink);) {
            if (metadataStream == null) {
                throw ExceptionBuilder.get(CellarException.class).withMessage("Inputstream is null for delete file '{}'")
                        .withMessageArgs(metadataLink).build();
            }

            return FileSystemUtils.inputStreamToString(metadataStream);
        }
        catch(IOException e){
            throw ExceptionBuilder.get(CellarException.class).withMessage("An error occured while processing InputStream for delete file '{}'")
                    .withMessageArgs(metadataLink).build();
        }
    }

    private static boolean isDeleteBehaviorPresent(final String divOrStructMapId, final Document document, final XPath xPath) {
        if (StringUtils.isBlank(divOrStructMapId)) {
            return false;
        }

        try {
            //Use setXPathVariableResolver in order to parameterized input and mitigate any
            //XPath injection risks
            final String behaviorExpression = "//behavior[@BTYPE='delete']/@STRUCTID=$ID";
            xPath.setXPathVariableResolver(new XPathVariableResolver() {
                @Override
                public Object resolveVariable(QName variableName) {
                    if(variableName.getLocalPart().equals("ID")){
                        return divOrStructMapId;
                    }else{
                        return null;
                    }
                }
            });
            final Boolean behaviorEvaluation = Boolean.valueOf(xPath.compile(behaviorExpression).evaluate(document));

            // return if the delete behavior is present
            return behaviorEvaluation;
        } catch (final Exception ex) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Unforeseen exception: see cause for details").withCause(ex)
                    .build();
        }
    }

    private static Date getModelLoadActivationDate(final MetsBehavior behavior) {
        final String dateStr = behavior.getParamValue("date");

        if (dateStr == null) {
            return new Date(); // default date is NOW
        }

        try {
            return TimeUtils.toISO8601Date(dateStr);
        } catch (final ParseException e) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.ERROR_WHILE_PARSING_THE_METS_DOCUMENT)
                    .withMessage("Model-load behavior '{}' is invalid: date param '{}' is invalid.").withMessageArgs(behavior, dateStr)
                    .withCause(e).root(true).build();
        }
    }

    protected MetsPackage getMetsPackage() {
        return this.metsPackage;
    }

    protected MetsDocument getMetsDocument() {
        return this.metsDocument;
    }

    protected MetadataStream getCurrentMetadataStream() {
        return this.currentMetadataStream;
    }

    protected ContentStream getCurrentContentStream() {
        return this.currentContentStream;
    }

    protected String getMimetype() {
        return this.mimetype;
    }

    protected String getChecksum() {
        return this.checksum;
    }

    protected String getChecksumType() {
        return this.checksumType;
    }

    protected String getIdentifier() {
        return this.identifier;
    }

    protected StringBuilder getText() {
        return this.text;
    }

    protected StructMap getCurrentStructMap() {
        return this.currentStructMap;
    }

    protected void setCurrentStructMap(final StructMap currentStructMap) {
        this.currentStructMap = currentStructMap;
    }

    protected DigitalObject getCurrentDigitalObject() {
        return this.currentDigitalObject;
    }

    protected void setCurrentDigitalObject(final DigitalObject currentDigitalObject) {
        this.currentDigitalObject = currentDigitalObject;
    }

    protected Map<String, DigitalObject> getDigitalObjects() {
        return this.digitalObjects;
    }

    protected DigitalObject getCurrentBehaviorTargetManifestation() {
        return this.currentBehaviorTargetManifestation;
    }

    protected List<ContentStream> getCurrentBehaviorTargetContentStreamList() {
        return this.currentBehaviorTargetContentStreamList;
    }

    protected List<Metadata> getCurrentBehaviorTargetTechnicalMetadataList() {
        return this.currentBehaviorTargetTechnicalMetadataList;
    }

    protected Collection<DigitalObject> getDeleteDigitalObjects() {
        return this.deleteDigitalObjects;
    }

}
