/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.mets.v2.builder
 *             FILE : StructMapBuilder.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 12, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.mets.v2.builder;

import eu.europa.ec.opoce.cellar.common.builder.IBuilder;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.BehaviorType;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.ObjectType;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.StructMapType;
import eu.europa.ec.opoce.cellar.mets.parser.MetsBehavior.BehaviourType;

/**
 * <class_description> {@link StructMapType} builder.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 12, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class BehaviorBuilder implements IBuilder<BehaviorType> {

    private final BehaviorType behavior;

    private BehaviorBuilder() {
        this.behavior = new BehaviorType();
    }

    public static BehaviorBuilder get() {
        return new BehaviorBuilder();
    }

    /* (non-Javadoc)
     * @see eu.europa.ec.opoce.cellar.common.builder.IBuilder#build()
     */
    @Override
    public BehaviorType build() {
        return this.behavior;
    }

    public BehaviorBuilder withStructId(final StructMapType structId) {
        this.behavior.getSTRUCTID().add(structId);
        return this;
    }

    public BehaviorBuilder withBtype(final BehaviourType btype) {
        this.behavior.setBTYPE(btype.toString());
        return this;
    }

    public BehaviorBuilder withMechanism(final ObjectType mechanism) {
        this.behavior.setMechanism(mechanism);
        return this;
    }

}
