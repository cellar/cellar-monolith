package eu.europa.ec.opoce.cellar.configuration;

import eu.europa.ec.opoce.cellar.MetsErrors;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * <p>MimetypeSnippetConfiguration class.</p>
 *
 * @author Joeri.Moreno
 */
public class MimetypeSnippetConfiguration {

    /**
     * Constant <code>CONVERTOR</code>
     */
    public static Converter CONVERTOR = new MimetypeSnippetConvertor();

    private Map<String, String> map;

    /**
     * <p>Constructor for MimetypeSnippetConfiguration.</p>
     */
    private MimetypeSnippetConfiguration() {
    }

    public static class MimetypeSnippetConvertor implements Converter {

        @Override
        public void marshal(Object sourceObj, HierarchicalStreamWriter writer, MarshallingContext context) {
            MimetypeSnippetConfiguration mimetypeSnippetConfiguration = (MimetypeSnippetConfiguration) sourceObj;

            for (Map.Entry<String, String> entry : mimetypeSnippetConfiguration.map.entrySet()) {
                writer.startNode("entry");
                writer.addAttribute("long", entry.getKey());
                writer.addAttribute("short", entry.getValue());
                writer.endNode();
            }
        }

        @Override
        public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
            Map<String, String> map = new HashMap<String, String>();
            while (reader.hasMoreChildren()) {
                reader.moveDown();
                String longType = reader.getAttribute("long");
                String shortType = reader.getAttribute("short");
                if (null == longType || null == shortType || !reader.getNodeName().equals("entry") || map.containsKey(longType)) {
                    throw ExceptionBuilder.get(CellarException.class).withCode(MetsErrors.ERROR_IN_CONFIGFILE)
                            .withMessage("Config file error").build();
                }
                map.put(longType, shortType);
                reader.moveUp();
            }
            MimetypeSnippetConfiguration mimetypeSnippetConfiguration = new MimetypeSnippetConfiguration();
            mimetypeSnippetConfiguration.map = Collections.unmodifiableMap(map);
            return mimetypeSnippetConfiguration;
        }

        @SuppressWarnings("rawtypes")
        @Override
        public boolean canConvert(Class type) {
            return type.equals(MimetypeSnippetConfiguration.class);
        }
    }
}
