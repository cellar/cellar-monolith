package eu.europa.ec.opoce.cellar.xmlvalidation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;

/**
 * <p>MetsV1ValidationService class.</p>
 * This service validates the METS version 1 on the basis of its XSD.
 */
@Service("metsV1ValidationService")
public class MetsV1ValidationService extends AbstractValidationService {

    @Autowired
    private MetsXsdFileConfiguration xsdFileConfiguration;

    @Override
    protected Resource resolveResource() {
        return this.xsdFileConfiguration.getMetsXsdV1Resource();
    }

}
