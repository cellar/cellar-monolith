/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.mets.v2.builder.enums
 *             FILE : MdType.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 15, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.mets.v2.builder.enums;

/**
 * <class_description> MdType enum.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 15, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public enum MdType {
    MARC("MARC"),
    MODS("MODS"),
    EAD("EAD"),
    DC("DC"),
    NISOIMG("NISOIMG"),
    LC_AV("LC-AV"),
    VRA("VRA"),
    TEIHDR("TEIHDR"),
    DDI("DDI"),
    FGDC("FGDC"),
    LOM("LOM"),
    PREMIS("PREMIS"),
    PREMIS_OBJECT("PREMIS:OBJECT"),
    PREMIS_AGENT("PREMIS:AGENT"),
    PREMIS_RIGHTS("PREMIS:RIGHTS"),
    PREMIS_EVENT("PREMIS:EVENT"),
    TEXTMD("TEXTMD"),
    METSRIGHTS("METSRIGHTS"),
    ISO_19115_2003_NAP("ISO 19115:2003 NAP"),
    OTHER("OTHER");

    private final String value;

    private MdType(final String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
