/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.mets
 *             FILE : IMetsDocumentParser.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 04-01-2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2016 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.mets.parser;

import eu.europa.ec.opoce.cellar.domain.content.mets.MetsDocument;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 04-01-2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IMetsDocumentParser {

    /**
     * Execute the parsing.
     * @return the in-memory-datastructure that is created after parsing the metsPackage
     */
    MetsDocument execute();

}
