package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.cl.service.client.MetsProcessorService;
import eu.europa.ec.opoce.cellar.common.util.XMLUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsDocument;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsPackage;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsProfile;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsProfile.MetsVersion;
import eu.europa.ec.opoce.cellar.domain.content.mets.ResolvedMetsPackage;
import eu.europa.ec.opoce.cellar.exception.CellarValidationException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.mets.parser.IMetsDocumentParser;
import eu.europa.ec.opoce.cellar.mets.parser.MetsV1DocumentParser;
import eu.europa.ec.opoce.cellar.mets.parser.MetsV2DocumentParser;
import eu.europa.ec.opoce.cellar.validation.client.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import java.io.InputStream;

/**
 * <p>MetsProcessorServiceImpl class.</p>
 */
@Service("MetsProcessorServiceServer")
public class MetsProcessorServiceImpl implements MetsProcessorService {

    private static final String PROFILE_ATTR_PATH = "//mets/@PROFILE";

    @Autowired(required = true)
    @Qualifier("metsV1ValidationService")
    private ValidationService metsV1ValidationService;

    @Autowired(required = true)
    @Qualifier("metsV2ValidationService")
    private ValidationService metsV2ValidationService;

    /**
     * {@inheritDoc}
     */
    @Override
    public MetsDocument convert(final MetsPackage metsPackage) {
        // convert to resolved METS package
        final MetsPackage resolvedMetsPackage = new ResolvedMetsPackage(metsPackage);
        InputStream metsStream=null;
        // choose the right validation service and parser on the basis of the METS version
        try {
            metsStream = resolvedMetsPackage.getMetsStream();
            final MetsVersion metsVersion = resolveMetsVersion(metsStream);
            ValidationService metsValidationService = null;
            IMetsDocumentParser parser = null;
            switch (metsVersion) {
                case V1:
                    metsValidationService = this.metsV1ValidationService;
                    parser = new MetsV1DocumentParser(resolvedMetsPackage);
                    break;
                case V2:
                    metsValidationService = this.metsV2ValidationService;
                    parser = new MetsV2DocumentParser(resolvedMetsPackage);
                    break;
                default:
                    // cannot get here
                    throw ExceptionBuilder.get(CellarValidationException.class).withCode(CommonErrors.UNKNOWN_METS_PROFILE)
                            .withMessage("Unknown METS version '{}'.").withMessageArgs(metsVersion).build();
            }

            // validate
            metsValidationService.validate(metsStream);

            // parse and return the parsed document
            return parser.execute();
        }
        finally{
            org.apache.commons.io.IOUtils.closeQuietly(metsStream);
        }
    }

    private static MetsVersion resolveMetsVersion(final InputStream input) {
        String metsProfileStr = null;

        final DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
        domFactory.setNamespaceAware(false);
        DocumentBuilder builder = null;
        try {
            domFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            domFactory.setFeature("http://xml.org/sax/features/external-general-entities", false);
            domFactory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            domFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            domFactory.setXIncludeAware(false);
            domFactory.setExpandEntityReferences(false);
            
            builder = domFactory.newDocumentBuilder();
            final Document doc = builder.parse(input);
            final XPathExpression expr = XMLUtils.newXPathFactory().newXPath().compile(PROFILE_ATTR_PATH);
            metsProfileStr = (String) expr.evaluate(doc, XPathConstants.STRING);
            // this is necessary in order to allow the validation to re-read the input stream from scratch
            input.reset();
        } catch (final Exception e) {
            throw ExceptionBuilder.get(CellarValidationException.class).withCode(CommonErrors.ERROR_WHILE_PARSING_THE_METS_DOCUMENT)
                    .withMessage("An error occurred while reading the value of attribute '{}' from METS document")
                    .withMessageArgs(PROFILE_ATTR_PATH).withCause(e).build();
        }

        return MetsProfile.getByXmlValue(metsProfileStr).getVersion();
    }

}
