/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.mets.v2.builder
 *             FILE : IDivAware.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 25, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.mets.v2.builder;

import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.DivType;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.DivType.Fptr;

/**
 * <class_description> Parent node aware of Div and/or Fptr.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 25, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IDivFptrAware<R> {

    R withDiv(final DivType child);

    R withFptr(final Fptr child);
}
