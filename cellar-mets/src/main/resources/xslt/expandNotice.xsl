<?xml version='1.0'?>
<xsl:stylesheet version="2.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:xs="http://www.w3.org/2001/XMLSchema"
                xmlns:expandClass="eu.europa.ec.opoce.cellar.cmr.notice.xslt.function.XsltEmbeddedNoticeCalculator"
                exclude-result-prefixes="xs expandClass"
>
  <xsl:output method="xml" encoding="UTF-8" indent="yes"/>
  <xsl:strip-space elements="*"/>
  <!--
  Function: transform a notice such that the for expanding properties, embedded notices are included.
  - expanding properties have an attribute @embed.
    - the value of this attribute is a cellar CURIE of a work, a dossier or an agent.
      the xslt uses a java extension function to retrieve all the components of the embedded notice
  - expanding properties may specify a content language with attribute @content-lang.
    if the expanding property refers a work, it indicates the desired language for the embedded notice.
    when the language is not specified, the decoding language is taken as a default.
  - The embbed notice is compiled from a set of notice components by selecting the correct decoding.
    (the embedded notice components contain all available languages.
  - xml:lang attribute is filtered out.
  - rdf:type is cleaned and filtered
    - differentiation is made between rdf:type and cdm:type based on value (rdf or cdm classe name indicates rdf:type)
    - only keep rdf:type under WORK
    - remove duplicate rdf:type
  -->

  <!--xsl:param name="cmd-lang" as="xs:string" select="'dan'"/-->
  <xsl:param name="cmd-lang" as="xs:string"/>
  <!-- the decoding language.  As the language is not found in the indexing notice itself, it can be provided on the command line. -->

  <xsl:template match="/">
    <xsl:apply-templates>
      <xsl:with-param name="lang" select="if (*/@decoding) then */@decoding else $cmd-lang" tunnel="yes" as="xs:string"/>
    </xsl:apply-templates>
  </xsl:template>

  <!-- expand where an embed instruction is found -->
  <xsl:template match="*[@embed]">
    <xsl:param name="lang" tunnel="yes" as="xs:string"/>
    <xsl:variable name="exp-lang" select="if (@content-lang) then @content-lang else $lang" as="xs:string"/>
    <xsl:copy>
      <xsl:apply-templates select="@type">
        <xsl:with-param name="lang" select="$exp-lang" tunnel="yes" as="xs:string"/>
      </xsl:apply-templates>
      <EMBEDDED_NOTICE>
        <xsl:apply-templates select="expandClass:getEmbeddedNotice(@embed,$exp-lang)">
          <xsl:with-param name="lang" select="$lang" tunnel="yes" as="xs:string"/>
        </xsl:apply-templates>
      </EMBEDDED_NOTICE>
      <xsl:apply-templates select="*">
        <xsl:with-param name="lang" select="$exp-lang" tunnel="yes" as="xs:string"/>
      </xsl:apply-templates>
      <xsl:call-template name="check-type"/>
    </xsl:copy>
  </xsl:template>

  <!--
  - filter out unwanted language variants (typical for decoding)
  - filter out xml:lang
  -->
  <xsl:template match="*[@xml:lang]">
    <xsl:param name="lang" tunnel="yes" as="xs:string"/>
    <xsl:if test="@xml:lang=$lang">
      <xsl:copy>
        <xsl:for-each select="@*">
          <xsl:if test="not(name(.) = 'xml:lang')">
            <xsl:copy-of select="."/>
          </xsl:if>
        </xsl:for-each>
        <xsl:apply-templates select="node()"/>
        <xsl:call-template name="check-type"/>
      </xsl:copy>
    </xsl:if>
  </xsl:template>

  <xsl:template match="@* | node()">
    <xsl:copy>
      <xsl:apply-templates select="@*"/>
      <xsl:apply-templates select="node()"/>
      <xsl:call-template name="check-type"/>
    </xsl:copy>
  </xsl:template>

  <!-- type is handled in a named template - skip it on normal matching -->
  <xsl:template match="TYPE"/>

  <!-- Filter and transform TYPE when needed -->
  <xsl:template name="check-type">
    <xsl:variable name="parent" select="name()" as="xs:string"/>
    <xsl:for-each-group select="TYPE" group-by="string()">
      <xsl:choose>
        <xsl:when test="starts-with(current-grouping-key(),'http://publications.europa.eu/ontology/cdm#')">
          <xsl:if test="$parent = 'WORK' or $parent = 'DOSSIER' or $parent = 'AGENT'">
            <TYPE>
              <xsl:value-of select="replace(current-grouping-key(),'http://publications.europa.eu/ontology/cdm#','cdm:')"/>
            </TYPE>
          </xsl:if>
        </xsl:when>
        <xsl:when test="current-grouping-key() = 'http://www.w3.org/2000/01/rdf-schema#Resource'"/>
        <xsl:otherwise>
          <TYPE>
            <xsl:value-of select="current-grouping-key()"/>
          </TYPE>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each-group>
  </xsl:template>

</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2007. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="yes" name="Scenario1" userelativepaths="yes" externalpreview="no" url="..\..\..\..\Test\cellar\cellar-66\notice_index_expression.xml" htmlbaseurl="" outputurl="..\..\..\..\Test\cellar\cellar-66\notice_index_expression-exp.xml"
		          processortype="saxon8" useresolver="yes" profilemode="0" profiledepth="" profilelength="" urlprofilexml="" commandline=" net.sf.saxon.Transform -o %3 %1 %2" additionalpath="C:\Program Files\Java\jdk1.5.0_06\jre\bin\java"
		          additionalclasspath="C:\xml\saxon8-6;C:\xml\jaxp\jaxp-1_3-20060207\jaxp-api.jar;C:\xml\jaxp\jaxp-1_3-20060207\dom.jar;C:\xml\jaxp\jaxp-1_3-20060207;C:\xml\saxon8-6\saxon8sa.jar;C:\xml\saxon8-6\saxon8-dom.jar;C:\xml\saxon8-6\saxon8-jdom.jar;C:\xml\saxon8-6\saxon8-sql.jar;C:\xml\saxon8-6\saxon8-xom.jar;C:\xml\saxon8-6\saxon8-xpath.jar;C:\xml\saxon8-6\saxon8.jar"
		          postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator="">
			<parameterValue name="cmd-lang" value="'nld'"/>
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="false"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="0"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->