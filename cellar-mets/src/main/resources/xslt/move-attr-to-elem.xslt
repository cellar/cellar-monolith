<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:at="http://publications.europa.eu/authority/schema#"
 xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
 >
<xsl:output method="xml" encoding="UTF-8"/>

<xsl:template match="*[@at:start.use | @at:end.use | @at:comment.date | @at:eea.entry.date | @at:efta.entry.date | @at:eu.application.date | @at:eu.entry.date | @at:deprecated | @at:pub | @at:currency.euro | @at:celex]">
  <xsl:copy> 
  	<xsl:for-each select="@*">
		<xsl:if test="not(namespace-uri(.)='http://publications.europa.eu/authority/schema#') or 
		           not(local-name(.)=('start.use','end.use','comment.data','eea.entry.date','efta.entry.date','eu.application.date','eu.entry.date','deprecated','pub','currency.euro','celex'))">
			<xsl:apply-templates select="."/>
		</xsl:if>
	</xsl:for-each>
    <xsl:apply-templates select="@at:start.use | @at:end.use | @at:comment.date | @at:eea.entry.date | @at:efta.entry.date | @at:eu.application.date | @at:eu.entry.date | @at:deprecated | @at:pub | @at:currency.euro | @at:celex"/> 
    <xsl:apply-templates select="node()" /> 
  </xsl:copy> 
</xsl:template>

<xsl:template match="@at:start.use | @at:end.use | @at:comment.date | @at:eea.entry.date | @at:efta.entry.date | @at:eu.application.date | @at:eu.entry.date">
	<xsl:element name="{name()}" namespace="http://publications.europa.eu/authority/schema#">
		<xsl:attribute name="rdf:datatype">http://www.w3.org/2001/XMLSchema#date</xsl:attribute>
		<xsl:value-of select="."/>
	</xsl:element>
</xsl:template>

<xsl:template match="at:start.use | at:end.use | at:comment.date | at:eea.entry.date | at:efta.entry.date | at:eu.application.date | at:eu.entry.date">
	<xsl:element name="{name()}" namespace="http://publications.europa.eu/authority/schema#">
		<xsl:attribute name="rdf:datatype">http://www.w3.org/2001/XMLSchema#date</xsl:attribute>
		<xsl:value-of select="."/>
	</xsl:element>
</xsl:template>

<xsl:template match="@at:deprecated | @at:pub | @at:currency.euro | @at:celex">
	<xsl:element name="{name()}" namespace="http://publications.europa.eu/authority/schema#">
		<xsl:attribute name="rdf:datatype">http://www.w3.org/2001/XMLSchema#boolean</xsl:attribute>
		<xsl:value-of select="."/>
	</xsl:element>
</xsl:template>

<xsl:template match="at:alternative.name"/>

<xsl:template match="@* | node() "> 
  <xsl:copy> 
     <xsl:apply-templates select="@* | node()" /> 
  </xsl:copy> 
</xsl:template> 
 
</xsl:stylesheet><!-- Stylus Studio meta-information - (c) 2004-2007. Progress Software Corporation. All rights reserved.

<metaInformation>
	<scenarios>
		<scenario default="no" name="language" userelativepaths="yes" externalpreview="no" url="..\Abox\nal\languages-skos-v2-orig.rdf" htmlbaseurl="" outputurl="..\Abox\nal\languages-skos-v2.rdf" processortype="saxon8" useresolver="yes" profilemode="0"
		          profiledepth="" profilelength="" urlprofilexml="" commandline=" net.sf.saxon.Transform -o %3 %1 %2" additionalpath="C:\Program Files\Java\jdk1.5.0_06\jre\bin\java"
		          additionalclasspath="C:\xml\saxon8-6;C:\xml\jaxp\jaxp-1_3-20060207\jaxp-api.jar;C:\xml\jaxp\jaxp-1_3-20060207\dom.jar;C:\xml\jaxp\jaxp-1_3-20060207;C:\xml\saxon8-6\saxon8sa.jar;C:\xml\saxon8-6\saxon8-dom.jar;C:\xml\saxon8-6\saxon8-jdom.jar;C:\xml\saxon8-6\saxon8-sql.jar;C:\xml\saxon8-6\saxon8-xom.jar;C:\xml\saxon8-6\saxon8-xpath.jar;C:\xml\saxon8-6\saxon8.jar"
		          postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bSchemaAware" value="false"/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="iErrorHandling" value="0"/>
		</scenario>
		<scenario default="yes" name="country" userelativepaths="yes" externalpreview="no" url="..\Abox\nal\countries-skos-v4-orig.rdf" htmlbaseurl="" outputurl="..\Abox\nal\countries-skos-v4.rdf" processortype="saxon8" useresolver="yes" profilemode="0"
		          profiledepth="" profilelength="" urlprofilexml="" commandline=" net.sf.saxon.Transform -o %3 %1 %2" additionalpath="C:\Program Files\Java\jdk1.5.0_06\jre\bin\java"
		          additionalclasspath="C:\xml\saxon8-6;C:\xml\jaxp\jaxp-1_3-20060207\jaxp-api.jar;C:\xml\jaxp\jaxp-1_3-20060207\dom.jar;C:\xml\jaxp\jaxp-1_3-20060207;C:\xml\saxon8-6\saxon8sa.jar;C:\xml\saxon8-6\saxon8-dom.jar;C:\xml\saxon8-6\saxon8-jdom.jar;C:\xml\saxon8-6\saxon8-sql.jar;C:\xml\saxon8-6\saxon8-xom.jar;C:\xml\saxon8-6\saxon8-xpath.jar;C:\xml\saxon8-6\saxon8.jar"
		          postprocessortype="none" postprocesscommandline="" postprocessadditionalpath="" postprocessgeneratedext="" validateoutput="no" validator="internal" customvalidator="">
			<advancedProp name="sInitialMode" value=""/>
			<advancedProp name="bXsltOneIsOkay" value="true"/>
			<advancedProp name="bSchemaAware" value="false"/>
			<advancedProp name="bXml11" value="false"/>
			<advancedProp name="iValidation" value="0"/>
			<advancedProp name="bExtensions" value="true"/>
			<advancedProp name="iWhitespace" value="0"/>
			<advancedProp name="sInitialTemplate" value=""/>
			<advancedProp name="bTinyTree" value="true"/>
			<advancedProp name="bWarnings" value="true"/>
			<advancedProp name="bUseDTD" value="false"/>
			<advancedProp name="iErrorHandling" value="0"/>
		</scenario>
	</scenarios>
	<MapperMetaTag>
		<MapperInfo srcSchemaPathIsRelative="yes" srcSchemaInterpretAsXML="no" destSchemaPath="" destSchemaRoot="" destSchemaPathIsRelative="yes" destSchemaInterpretAsXML="no"/>
		<MapperBlockPosition></MapperBlockPosition>
		<TemplateContext></TemplateContext>
		<MapperFilter side="source"></MapperFilter>
	</MapperMetaTag>
</metaInformation>
-->