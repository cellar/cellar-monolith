# Cellar v8.10.2

CELLAR is a semantic repository built by the Publications Office. It is an infrastructure service that includes large amounts of legal content, EU publications and reference datasets, serving as the central dissemination repository of the Publication Office. Cellar supports major public services, such as EUR-Lex, EU Publications and EU Vocabularies, and provides for both human-readable and machine access to the core content of these services. To automate their access to data or for research purposes, private citizens and companies can access CELLAR using the public [SPARQL](http://publications.europa.eu/webapi/rdf/sparql) endpoint.

The architecture behind [CELLAR](https://op.europa.eu/en/publication-detail/-/publication/50ecce27-857e-11e8-ac6a-01aa75ed71a1/language-en/format-PDF/source-256659994) is based on semantic technologies and a framework of several standards enabling data sharing and reuse. From the outset, despite the monolith architecture of Cellar, the platform was designed to be scalable and to support interoperability of services by providing a common framework for the storage of content and metadata.

https://code.europa.eu/cellar/cellar-monolith repository contains a source code of Cellar.

## Getting started

Information and examples on how to access content files and metadata from the Cellar are available from the Cellar [booklet](https://op.europa.eu/en/publication-detail/-/publication/50ecce27-857e-11e8-ac6a-01aa75ed71a1/language-en/format-PDF/source-256659994)

## Deployment and Installation

Cellar needs the following components in order to run:  Java 8, Virtuoso, Oracle, Tomcat, Metadata validation service, and Amazon S3. Cellar itself is packaged as WAR archive and deployed into Tomcat.

Detailed installation requirements and instructions for the public users are under the preparation and and will be published here as soon as they are available.


## Roadmap
New generation of Cellar based on async, non-blocking microservices. Dockerization of Cellar for quick deployment and installation.


## Authors
Cellar Team

## License
Copyright the European Union 2022. Licensed under the EUPL-1.2 or later.

## Project status
Live
