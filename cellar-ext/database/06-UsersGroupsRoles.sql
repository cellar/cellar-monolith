-- Initialize groups and roles

Insert into ROLES (ID,ROLE_NAME,ROLE_ACCESS) values (1,'Dashboard','ROLE_DASHBOARD');
Insert into ROLES (ID,ROLE_NAME,ROLE_ACCESS) values (2,'Audit','ROLE_AUDIT');
Insert into ROLES (ID,ROLE_NAME,ROLE_ACCESS) values (3,'Sparql Endpoint','ROLE_SPARQL_ENDPOINT');
Insert into ROLES (ID,ROLE_NAME,ROLE_ACCESS) values (4,'Sparql Configuration','ROLE_SPARQL_CONFIGURATION');
Insert into ROLES (ID,ROLE_NAME,ROLE_ACCESS) values (5,'Indexing','ROLE_INDEXING');
Insert into ROLES (ID,ROLE_NAME,ROLE_ACCESS) values (6,'Log','ROLE_LOG');
Insert into ROLES (ID,ROLE_NAME,ROLE_ACCESS) values (7,'Configuration','ROLE_CONFIGURATION');
Insert into ROLES (ID,ROLE_NAME,ROLE_ACCESS) values (8,'SIP tracker','ROLE_SIP_TRACKER');
Insert into ROLES (ID,ROLE_NAME,ROLE_ACCESS) values (9,'Archive','ROLE_ARCHIVE');
Insert into ROLES (ID,ROLE_NAME,ROLE_ACCESS) values (10,'Content Search','ROLE_CONTENT_SEARCH');
Insert into ROLES (ID,ROLE_NAME,ROLE_ACCESS) values (11,'Content Visibility','ROLE_CONTENT_VISIBILITY');
Insert into ROLES (ID,ROLE_NAME,ROLE_ACCESS) values (12,'Work Embargo','ROLE_EMBARGO');
Insert into ROLES (ID,ROLE_NAME,ROLE_ACCESS) values (13,'NALs','ROLE_NAL');
Insert into ROLES (ID,ROLE_NAME,ROLE_ACCESS) values (14,'Ontologies','ROLE_ONTOLOGY');
Insert into ROLES (ID,ROLE_NAME,ROLE_ACCESS) values (15,'Languages','ROLE_RESOURCE');
Insert into ROLES (ID,ROLE_NAME,ROLE_ACCESS) values (16,'Validator','ROLE_VALIDATOR');
Insert into ROLES (ID,ROLE_NAME,ROLE_ACCESS) values (17,'Security','ROLE_SECURITY');
Insert into ROLES (ID,ROLE_NAME,ROLE_ACCESS) values (18,'Indexing Reindex','ROLE_REINDEXING');
Insert into ROLES (ID,ROLE_NAME,ROLE_ACCESS) values (19,'License Holder','ROLE_LICENSE');
Insert into ROLES (ID,ROLE_NAME,ROLE_ACCESS) values (20,'Secapi','ROLE_SECAPI');
Insert into ROLES (ID,ROLE_NAME,ROLE_ACCESS) values (21,'RDF Store Cleaner','ROLE_RDF_STORE_CLEANER');
Insert into ROLES (ID,ROLE_NAME,ROLE_ACCESS) values (22,'Hierarchy Aligner','ROLE_HIERARCHY_ALIGNER');
Insert into ROLES (ID,ROLE_NAME,ROLE_ACCESS) values (23,'Threads','ROLE_THREADS');
Insert into ROLES (ID,ROLE_NAME,ROLE_ACCESS) values (24,'Export','ROLE_EXPORT');
Insert into ROLES (ID,ROLE_NAME,ROLE_ACCESS) values (25,'Sparql Response Templates','ROLE_SPARQL_TEMPLATES');
Insert into ROLES (ID,ROLE_NAME,ROLE_ACCESS) values (26,'Update','ROLE_UPDATE');
 
Insert into GROUPS (ID,GROUP_NAME) values (1,'Admin');
Insert into GROUPS (ID,GROUP_NAME) values (2,'Operator');
Insert into GROUPS (ID,GROUP_NAME) values (3,'Business');
Insert into GROUPS (ID,GROUP_NAME) values (4,'Guest');
Insert into GROUPS (ID,GROUP_NAME) values (5,'License holder');

Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (1,1);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (1,2);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (1,3);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (1,4);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (1,5);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (1,6);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (1,7);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (1,8);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (1,9);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (1,10);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (1,11);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (1,12);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (1,13);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (1,14);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (1,15);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (1,16);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (1,17);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (1,18);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (1,19);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (1,20);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (1,21);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (1,22);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (1,23);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (1,24);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (1,26);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (2,1);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (2,2);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (2,3);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (2,5);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (2,6);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (2,8);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (2,9);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (2,10);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (2,11);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (2,17);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (2,18);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (2,20);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (3,1);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (3,2);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (3,3);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (3,5);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (3,6);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (3,8);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (3,9);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (3,10);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (4,1);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (4,2);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (4,3);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (4,10);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (5,1);
Insert into GROUP_ROLES (GROUP_ID,ROLE_ID) values (5,19);

Insert into USERS (ID,USERNAME,PASSWORD,ENABLED,GROUP_ID) values (1,'fsanmartin','mLknrgRHvMc/pY8VWVHMcg==',1,1);
Insert into USERS (ID,USERNAME,PASSWORD,ENABLED,GROUP_ID) values (2,'mkuster','hsVD5jk1r08NL1sTrBLy7A==',1,1);
Insert into USERS (ID,USERNAME,PASSWORD,ENABLED,GROUP_ID) values (3,'jcneisius','8Vk96yYgkgo68mNQ/GkJMQ==',1,1);
Insert into USERS (ID,USERNAME,PASSWORD,ENABLED,GROUP_ID) values (4,'autbackup','iZM2DfpfN9hNEKErznqpuJhsZsu5sh9J',1,1);
Insert into USERS (ID,USERNAME,PASSWORD,ENABLED,GROUP_ID) values (5,'r4admin','HRgt+SBZ/a7HIiddPZGAt+RDZSKyjk6d',1,1);
Insert into USERS (ID,USERNAME,PASSWORD,ENABLED,GROUP_ID) values (6,'pschmitz','+GD0NvhnRxn2XBPxKkcTcg==',1,1);
Insert into USERS (ID,USERNAME,PASSWORD,ENABLED,GROUP_ID) values (7,'mscherbaum','26uWSNc9kshCZwO0E9lwOQ==',1,1);
Insert into USERS (ID,USERNAME,PASSWORD,ENABLED,GROUP_ID) values (8,'pgratz','z2SdsxxUgdJgb9LD0WFFYg==',1,1);
Insert into USERS (ID,USERNAME,PASSWORD,ENABLED,GROUP_ID) values (9,'jploucheur','9rh56DbayUIQVH0cU1B1+g==',1,2);
Insert into USERS (ID,USERNAME,PASSWORD,ENABLED,GROUP_ID) values (10,'npincon','iC3esZ6jznjrFQoK8CgLLA==',1,2);
Insert into USERS (ID,USERNAME,PASSWORD,ENABLED,GROUP_ID) values (11,'guest','jDC9OaIG65QLaoRfIYiA4w==',1,4);

commit;
