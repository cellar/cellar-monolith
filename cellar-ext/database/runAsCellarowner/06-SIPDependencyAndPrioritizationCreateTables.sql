CREATE TABLE "SIP_PACKAGE" (
	"ID"				    NUMBER(19)			NOT NULL,
	"NAME"				    VARCHAR2(255 CHAR)	NOT NULL,
	"TYPE"				    VARCHAR2(20 CHAR)	NOT NULL,
	"OPERATION"			    VARCHAR2(20 CHAR)	NOT NULL,
	"DETECTION_DATE"	    TIMESTAMP(6)		NOT NULL,
	"STATUS"			    VARCHAR2(10 CHAR)	NOT NULL,
	"PACKAGE_HISTORY_ID"    NUMBER(19),
	CONSTRAINT "SIP_PCKG_PK" PRIMARY KEY ("ID"),
	CONSTRAINT "SIP_PCKG_NAME_UNIQUE" UNIQUE ("NAME"),
	CONSTRAINT "SIP_PCKG_HSTR_ID_UNIQUE" UNIQUE ("PACKAGE_HISTORY_ID")
);

CREATE TABLE "RELATION_PROD_ID" (
	"ID"				NUMBER(19)			NOT NULL,
	"SIP_ID"			NUMBER(19)			NOT NULL,
	"PRODUCTION_ID"				VARCHAR2(700 CHAR)	NOT NULL,
	"IS_DIRECT_RELATION_ONLY"	VARCHAR2(1 CHAR)	NOT NULL,
	CONSTRAINT "REL_PROD_ID_PK" PRIMARY KEY ("ID"),
	CONSTRAINT "REL_PROD_ID_FK_SIP_PCKG" FOREIGN KEY ("SIP_ID") REFERENCES "SIP_PACKAGE"("ID")
);

CREATE TABLE "PACKAGE_HAS_PARENT_PACKAGE" (
	"ID"				NUMBER(19)	NOT NULL,
	"SIP_ID"			NUMBER(19)	NOT NULL,
	"PARENT_SIP_ID"		NUMBER(19)	NULL,
	CONSTRAINT "PCKG_HAS_PARENT_PCKG_ID_PK" PRIMARY KEY ("ID"),
	CONSTRAINT "PCKG_HAS_PARENT_PCKG_SIP_ID_FK_SIP_PCKG" FOREIGN KEY ("SIP_ID") REFERENCES "SIP_PACKAGE"("ID"),
	CONSTRAINT "PCKG_HAS_PARENT_PCKG_PARENT_SIP_ID_FK_SIP_PCKG" FOREIGN KEY ("PARENT_SIP_ID") REFERENCES "SIP_PACKAGE"("ID")
);

CREATE SEQUENCE "SIP_PCKG_SEQ";
CREATE SEQUENCE "REL_PROD_ID_SEQ";
CREATE SEQUENCE "PCKG_HAS_PARENT_PCKG_SEQ";

CREATE INDEX "SIP_PCKG_STATUS_IDX" ON "SIP_PACKAGE"("STATUS");
CREATE INDEX "SIP_PCKG_TYPE_DETECTION_DATE_IDX" ON "SIP_PACKAGE"('TYPE', 'DETECTION_DATE');
CREATE INDEX "REL_PROD_ID_SIP_ID_IDX" ON "RELATION_PROD_ID"("SIP_ID");
CREATE INDEX "REL_PROD_ID_PRDUCTION_ID_IDX" ON "RELATION_PROD_ID"("PRODUCTION_ID");
CREATE INDEX "REL_PROD_ID_IS_DIRECT_RELATION_ONLY_IDX" ON "RELATION_PROD_ID"("IS_DIRECT_RELATION_ONLY");
CREATE INDEX "PCKG_HAS_PARENT_PCKG_SIP_ID_IDX" ON "PACKAGE_HAS_PARENT_PACKAGE"("SIP_ID");
CREATE INDEX "PCKG_HAS_PARENT_PCKG_PARENT_SIP_ID_IDX" ON "PACKAGE_HAS_PARENT_PACKAGE"("PARENT_SIP_ID");


CREATE OR REPLACE TYPE t_sip_id_array IS TABLE OF NUMBER(19);
/
CREATE OR REPLACE TYPE t_extended_relation_prod_id_row AS OBJECT (
	PRODUCTION_ID VARCHAR2(700 CHAR),
	IS_READ_ONLY VARCHAR2(1 CHAR)
);
/
CREATE OR REPLACE TYPE t_extended_relation_prod_ids IS TABLE OF t_extended_relation_prod_id_row;
/


-- Returns the production identifiers and whether these are read-only or not, for the packages IDs provided as argument
CREATE OR REPLACE FUNCTION get_extended_relation_prod_ids (sip_ids_in IN t_sip_id_array) RETURN t_extended_relation_prod_ids AUTHID CURRENT_USER IS
    l_extended_relation_prod_ids_result t_extended_relation_prod_ids := t_extended_relation_prod_ids();
	BEGIN
        -- Retrieve the non-normalized root/children and direct relation PIDs
        -- only for the packages whose IDs are provided as argument.
        WITH ROOT_CHILDREN_DIRECT_NON_NORMALIZED AS (
            SELECT r1.SIP_ID, r1.IS_DIRECT_RELATION_ONLY, r1.PRODUCTION_ID
            FROM RELATION_PROD_ID r1
                INNER JOIN SIP_PACKAGE sp1 ON sp1.ID = r1.SIP_ID
            WHERE sp1.ID IN (SELECT * FROM TABLE(sip_ids_in))
        ),
        -- Retrieve the root/children and direct relation PIDs (normalized and non-normalized).
	    ROOT_CHILDREN_DIRECT AS (
            -- Retrieve the previously computed non-normalized root/children and direct relations.
            SELECT *
            FROM ROOT_CHILDREN_DIRECT_NON_NORMALIZED rcdnn1
            UNION ALL
            -- Append the normalized CELLAR IDs, if any.
            SELECT rcdnn2.SIP_ID, rcdnn2.IS_DIRECT_RELATION_ONLY,
                -- Convert CELLAR-ID to base-CELLAR-ID.
                CASE
                    WHEN INSTR(c1.UUID, '.') = 0 THEN c1.UUID
                    ELSE SUBSTR(c1.UUID, 0, INSTR(c1.UUID, '.') - 1)
                END AS PRODUCTION_ID
            FROM ROOT_CHILDREN_DIRECT_NON_NORMALIZED rcdnn2
                INNER JOIN PRODUCTION_IDENTIFIER p1 ON p1.PRODUCTION_ID = rcdnn2.PRODUCTION_ID
                INNER JOIN CELLAR_IDENTIFIER c1 ON c1.ID = p1.CELLAR_IDENTIFIER_ID
        ),
        -- Retrieve the non-normalized inverse relations.
        INVERSE_NON_NORMALIZED AS (
            -- Retrieve the non-normalized inverse-on-source relations.
            SELECT rcd.SIP_ID,
                -- Convert CELLAR-ID to base-CELLAR-ID, if applicable.
                CASE
                    WHEN INSTR(inv1.TARGET_ID, 'cellar:') = 0 THEN inv1.TARGET_ID
                    WHEN INSTR(inv1.TARGET_ID, '.') != 0 THEN SUBSTR(inv1.TARGET_ID, 0, INSTR(inv1.TARGET_ID, '.') - 1)
                    ELSE inv1.TARGET_ID
                END AS PRODUCTION_ID
            FROM CMROWNER.CMR_INVERSE_RELATIONS inv1
                INNER JOIN ROOT_CHILDREN_DIRECT rcd ON rcd.PRODUCTION_ID = inv1.SOURCE_ID
            WHERE rcd.IS_DIRECT_RELATION_ONLY = 'N'
            UNION ALL
            -- Retrieve the non-normalized inverse-on-target relations.
            SELECT rcd.SIP_ID,
                -- Convert CELLAR-ID to base-CELLAR-ID, if applicable.
                CASE
                    WHEN INSTR(inv2.SOURCE_ID, 'cellar:') = 0 THEN inv2.SOURCE_ID
                    WHEN INSTR(inv2.SOURCE_ID, '.') != 0 THEN SUBSTR(inv2.SOURCE_ID, 0, INSTR(inv2.SOURCE_ID, '.') - 1)
                    ELSE inv2.SOURCE_ID
                END AS PRODUCTION_ID
            FROM CMROWNER.CMR_INVERSE_RELATIONS inv2
                INNER JOIN ROOT_CHILDREN_DIRECT rcd ON rcd.PRODUCTION_ID = inv2.TARGET_ID
            WHERE rcd.IS_DIRECT_RELATION_ONLY = 'N'
        ),
        -- Retrieve the normalized inverse relations.
        INVERSE_NORMALIZED AS (
            SELECT inn2.SIP_ID,
                -- Convert CELLAR-ID to base-CELLAR-ID.
                CASE
                    WHEN INSTR(c2.UUID, '.') = 0 THEN c2.UUID
                    ELSE SUBSTR(c2.UUID, 0, INSTR(c2.UUID, '.') - 1)
                END AS PRODUCTION_ID
            FROM INVERSE_NON_NORMALIZED inn2
                INNER JOIN PRODUCTION_IDENTIFIER p2 ON p2.PRODUCTION_ID = inn2.PRODUCTION_ID
                INNER JOIN CELLAR_IDENTIFIER c2 ON c2.ID = p2.CELLAR_IDENTIFIER_ID
        )
	    -- Stores and returns all applicable production identifiers along with their read-only status.
	    -- A production-id is read-only if it is a relation and an actual PID (not a cellar-identifier).
	    SELECT t_extended_relation_prod_id_row(PRODUCTION_ID, IS_READ_ONLY)
	    BULK COLLECT INTO l_extended_relation_prod_ids_result
	    FROM (
	     	-- The non-normalized inverse relations.
		    SELECT inn1.PRODUCTION_ID,
		    	CASE
		    		WHEN INSTR(inn1.PRODUCTION_ID, 'cellar:') = 0 THEN 'Y'
		    		ELSE 'N'
		    	END AS IS_READ_ONLY
		    FROM INVERSE_NON_NORMALIZED inn1
		    UNION ALL
		    -- Append the normalized inverse relations, if any.
		    SELECT in1.PRODUCTION_ID,
		    	CASE
		    		WHEN INSTR(in1.PRODUCTION_ID, 'cellar:') = 0 THEN 'Y'
		    		ELSE 'N'
		    	END AS IS_READ_ONLY
		    FROM INVERSE_NORMALIZED in1
		    UNION ALL
		    -- Append the normalized and non-normalized root/children and direct relation PIDs.
		    SELECT rcd2.PRODUCTION_ID,
		    	CASE WHEN rcd2.IS_DIRECT_RELATION_ONLY = 'Y' AND INSTR(rcd2.PRODUCTION_ID, 'cellar:') = 0 THEN 'Y'
		    	ELSE 'N'
		    	END AS IS_READ_ONLY
		    FROM ROOT_CHILDREN_DIRECT rcd2
	    );
	    RETURN l_extended_relation_prod_ids_result;
	END;
/

-- Contains the IDs of all scheduled packages, following the required ordering.
-- The view is built via a hierarchical query on table PACKAGE_HAS_PARENT_PACKAGE.
CREATE OR REPLACE VIEW "PACKAGE_LIST" ("SIP_ID", "ORDERING") AS 
	SELECT  SIP_ID, ROWNUM
	FROM PACKAGE_HAS_PARENT_PACKAGE
	START WITH PARENT_SIP_ID IS NULL
	CONNECT BY PRIOR SIP_ID = PARENT_SIP_ID
WITH READ ONLY;

-- Returns the ID of the first (in terms of ordering/priority) package (SIP_PACKAGE.ID) that can be ingested
-- in a conflict-free manner with regards to any packages being ingested at that point.
CREATE OR REPLACE FUNCTION id_next_package_ingestion_ready RETURN NUMBER AUTHID CURRENT_USER IS
	-- The number of conflicting production identifiers between the packages being ingested
	-- and the scheduled package under consideration.
	l_num_conflicting_pids NUMBER;

	-- The production identifiers of the packages being ingested.
	l_ingesting_extended_relation_prod_ids t_extended_relation_prod_ids := t_extended_relation_prod_ids();

	-- The production identifiers of the scheduled package being considered for ingestion.
	l_scheduled_pkg_extended_relation_prod_ids t_extended_relation_prod_ids := t_extended_relation_prod_ids();

	-- The IDs of the packages being ingested.
	l_ingesting_sip_ids t_sip_id_array := t_sip_id_array();

	-- The ID of the package considered for ingetion.
    l_scheduled_sip_id t_sip_id_array := t_sip_id_array();

    -- The STATUS of the package considered for ingestion.
    l_scheduled_sip_status VARCHAR2(10);


    -- The ordered/prioritized list of package references (contains references with status 'SCHEDULED'/'INGESTING'/'FINISHED').
	CURSOR c_scheduled_packages_list IS
		SELECT *
		FROM PACKAGE_LIST pl
		ORDER BY pl.ORDERING;

	BEGIN

		-- Retrieves the IDs (SIP_PACKAGE.ID) of the packages being ingested.
		SELECT sp.ID
		BULK COLLECT INTO l_ingesting_sip_ids
		FROM SIP_PACKAGE sp
		WHERE sp.STATUS = 'I';

		-- Retrieves the production identifiers of the packages being ingested.
		SELECT t_extended_relation_prod_id_row(PRODUCTION_ID, IS_READ_ONLY)
		BULK COLLECT INTO l_ingesting_extended_relation_prod_ids
		FROM TABLE(get_extended_relation_prod_ids(l_ingesting_sip_ids));

		-- Iterates over the ordered/prioritized list of scheduled packages IDs.
		FOR scheduled_package IN c_scheduled_packages_list LOOP

            -- Stores the STATUS of the currently evaluated package reference.
            SELECT sp.STATUS
            COLLECT INTO l_scheduled_sip_status
            FROM SIP_PACKAGE sp
            WHERE sp.ID = scheduled_package.SIP_ID;

            -- If the currently evaluated package reference is not 'SCHEDULED',
            -- then proceed to evaluate the next candidate package reference.
            IF (l_scheduled_sip_status != 'S') THEN
                CONTINUE;
            END IF;

			-- Stores the package ID of the current scheduled package.
            SELECT scheduled_package.SIP_ID
            BULK COLLECT INTO l_scheduled_sip_id
            FROM DUAL;

            -- Retrieves the production identifiers of the scheduled package being
            -- currently considered for ingestion.
			SELECT t_extended_relation_prod_id_row(PRODUCTION_ID, IS_READ_ONLY)
			BULK COLLECT INTO l_scheduled_pkg_extended_relation_prod_ids
			FROM TABLE(get_extended_relation_prod_ids(l_scheduled_sip_id));

			-- Checks whether there are any conflicts between the production identifiers of the packages being ingested
			-- and the scheduled package being currently considered.
			SELECT COUNT(1)
			INTO l_num_conflicting_pids
			FROM TABLE(l_ingesting_extended_relation_prod_ids) ingesting
				INNER JOIN TABLE(l_scheduled_pkg_extended_relation_prod_ids) scheduled ON ingesting.PRODUCTION_ID = scheduled.PRODUCTION_ID
					AND NOT (ingesting.IS_READ_ONLY = scheduled.IS_READ_ONLY AND ingesting.IS_READ_ONLY = 'Y');

			-- If no conflicts are detected, then this package can be submitted for ingestion,
			-- and its ID is returned.
			IF (l_num_conflicting_pids = 0) THEN
				RETURN scheduled_package.SIP_ID;
			END IF;

		END LOOP;
		-- If no suitable package is found or there aren't any scheduled packages available,
		-- return -1.
		RETURN -1;
	END;
/

-- Contains the next conflict-free package to be submitted for ingestion.
CREATE OR REPLACE VIEW "PACKAGE_QUEUE" ("SIP_ID", "NAME", "TYPE", "DETECTION_DATE", "PACKAGE_HISTORY_ID") AS
	SELECT sip.ID, sip.NAME, sip.TYPE, sip.DETECTION_DATE, sip.PACKAGE_HISTORY_ID
	FROM SIP_PACKAGE sip
	WHERE sip.ID = id_next_package_ingestion_ready
WITH READ ONLY;
