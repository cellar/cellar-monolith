-- Virtuoso exception message initlization

INSERT INTO VIRT_EXCP_MESSAGES (ID, MESSAGE) VALUES (1, 'SR214: Out of server threads. Server temporarily unavailable. Transaction rolled back');
INSERT INTO VIRT_EXCP_MESSAGES (ID, MESSAGE) VALUES (2, 'Unable to get managed connection for VirtuosoDS');
INSERT INTO VIRT_EXCP_MESSAGES (ID, MESSAGE) VALUES (3, 'Virtuoso Communications Link Failure (timeout) : Connection reset');
INSERT INTO VIRT_EXCP_MESSAGES (ID, MESSAGE) VALUES (4, 'SR012: Function aref needs a string or an array as argument 1, not an arg of type INTEGER (189)');
INSERT INTO VIRT_EXCP_MESSAGES (ID, MESSAGE) VALUES (5, 'Connection failed: Connection refused');
INSERT INTO VIRT_EXCP_MESSAGES (ID, MESSAGE) VALUES (6, 'SR172: Transaction deadlocked');
INSERT INTO VIRT_EXCP_MESSAGES (ID, MESSAGE) VALUES (7, 'Problem during serialization : Broken pipe');
INSERT INTO VIRT_EXCP_MESSAGES (ID, MESSAGE) VALUES (8, 'TR071: Replication account missing for server ''virtuoso-RDFNGBO-TK'', account ''__rdf_repl'' in logging replication.');
INSERT INTO VIRT_EXCP_MESSAGES (ID, MESSAGE) VALUES (9, 'Virtuoso Communications Link Failure (timeout) : Connection to the server lost');
INSERT INTO VIRT_EXCP_MESSAGES (ID, MESSAGE) VALUES (10, 'SR337: Transaction aborted because the server is out of memory');
INSERT INTO VIRT_EXCP_MESSAGES (ID, MESSAGE) VALUES (11, 'Problem during serialization : Software caused connection abort: socket write error');
INSERT INTO VIRT_EXCP_MESSAGES (ID, MESSAGE) VALUES (12, 'Virtuoso Communications Link Failure (timeout) : Connection to the server lost');
INSERT INTO VIRT_EXCP_MESSAGES (ID, MESSAGE) VALUES (13, 'virtuoso.jdbc4.VirtuosoException: Connection failed: Connection refused: connect');
INSERT INTO VIRT_EXCP_MESSAGES (ID, MESSAGE) VALUES (14, '08004 LI100: Number of licensed connections exceeded');
INSERT INTO VIRT_EXCP_MESSAGES (ID, MESSAGE) VALUES (15, 'Unable to get managed connection for');
