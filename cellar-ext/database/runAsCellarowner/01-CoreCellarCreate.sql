-- Tables for Cellar

-- create tables 
CREATE TABLE cellar_identifier 
  ( 
     id            NUMBER(19, 0) NOT NULL, 
     file_name     VARCHAR2(255 CHAR), 
     object_status VARCHAR2(255 CHAR) DEFAULT NULL, 
     uuid          VARCHAR2(255 CHAR), 
     doc_index     NUMBER(19, 0) DEFAULT 0, 
     PRIMARY KEY (id), 
     UNIQUE (uuid) 
  ); 

CREATE TABLE configuration_property 
  ( 
     id             NUMBER(19, 0) NOT NULL, 
     property_key   VARCHAR2(255 CHAR), 
     property_value VARCHAR2(255 CHAR), 
     PRIMARY KEY (id) 
  ); 

CREATE TABLE users 
  ( 
     id       NUMBER(19, 0) NOT NULL, 
     username VARCHAR2(255 CHAR) NOT NULL, 
     password VARCHAR2(512 CHAR), 
	 email VARCHAR2(384 CHAR),
     enabled  NUMBER(1, 0) NOT NULL, 
     group_id NUMBER(19, 0) NOT NULL, 
     PRIMARY KEY (id), 
     UNIQUE(username),
	 CONSTRAINT USERS_UNIQUE_EMAIL_CON UNIQUE (email)
  );

CREATE TABLE user_access_request
  ( 
     id       NUMBER(19, 0) NOT NULL, 
     username VARCHAR2(255 CHAR) NOT NULL,
	 email    VARCHAR2(384 CHAR) NOT NULL,
     PRIMARY KEY (id), 
     UNIQUE(username),
	 CONSTRAINT UAR_UNIQUE_EMAIL_CON UNIQUE (email)
  );

CREATE TABLE GROUPS 
  ( 
     id         NUMBER(19, 0) NOT NULL, 
     group_name VARCHAR2(255 CHAR) NOT NULL, 
     PRIMARY KEY (id) 
  ); 

CREATE TABLE ROLES 
  ( 
     id          NUMBER(19, 0) NOT NULL, 
     role_name   VARCHAR2(255 CHAR) NOT NULL, 
     role_access VARCHAR2(255 CHAR) NOT NULL, 
     PRIMARY KEY (id) 
  ); 

CREATE TABLE group_roles 
  ( 
     group_id NUMBER(19, 0) NOT NULL, 
     role_id  NUMBER(19, 0) NOT NULL, 
     PRIMARY KEY(group_id, role_id) 
  );

CREATE TABLE production_identifier 
  ( 
     id                   NUMBER(19, 0) NOT NULL, 
     creation_date        TIMESTAMP, 
     production_id        VARCHAR2(700 CHAR), 
     cellar_identifier_id NUMBER(19, 0), 
     PRIMARY KEY (id), 
     UNIQUE (production_id) 
  ); 

CREATE TABLE sip_mets_processing 
  ( 
     id                        NUMBER(19, 0) NOT NULL, 
     digital_object_count      NUMBER(10, 0), 
     mets_folder               VARCHAR2(255 CHAR), 
     priority                  NUMBER(10, 0), 
     reception_folder          VARCHAR2(255 CHAR), 
     sip_name                  VARCHAR2(255 CHAR), 
     sip_processing_end_date   TIMESTAMP, 
     sip_processing_start_date TIMESTAMP, 
     structmap_count           NUMBER(10, 0), 
     mets_type                 VARCHAR2(255 CHAR), 
     PRIMARY KEY (id) 
  ); 

CREATE TABLE validator_rule 
  ( 
     id                NUMBER(19, 0) NOT NULL, 
     validator_enabled NUMBER(1, 0) NOT NULL, 
     validator_key     VARCHAR2(255 CHAR), 
     validator_type    VARCHAR2(255 CHAR), 
     validator_class   VARCHAR2(255 CHAR), 
     PRIMARY KEY (id) 
  ); 

CREATE TABLE batch_job 
  ( 
     id                       NUMBER(19, 0) NOT NULL, 
     job_type                 VARCHAR2(255 CHAR) NOT NULL, 
     job_name                 VARCHAR2(255 CHAR) NOT NULL, 
     job_status               VARCHAR2(255 CHAR) NOT NULL, 
     priority                 VARCHAR2(255 CHAR), 
     generate_index_notice    NUMBER(1, 0), 
     generate_embedded_notice NUMBER(1, 0), 
     embargo_date             TIMESTAMP, 
     error_description        CLOB, 
     error_stacktrace         CLOB, 
     creation_date            TIMESTAMP NOT NULL, 
     last_modified            TIMESTAMP NOT NULL, 
     number_items             NUMBER(19, 0), 
     sparql_query             CLOB, 
     sparql_update_query      CLOB, 
     cron_config              VARCHAR2(255 CHAR), 
     destination_folder       VARCHAR2(512 CHAR), 
     PRIMARY KEY (id) 
  ) 
lob (sparql_query, sparql_update_query, error_description, error_stacktrace) 
store AS (TABLESPACE cellar_lob_01); 

CREATE TABLE batch_job_work_identifier 
  ( 
     id           NUMBER(19, 0) NOT NULL, 
     work_id      VARCHAR2(255 CHAR) NOT NULL, 
     batch_job_id NUMBER(19, 0) NOT NULL, 
     PRIMARY KEY(id) 
  ); 

CREATE TABLE extraction_configuration 
  ( 
     id                  NUMBER(19, 0) NOT NULL, 
     configuration_name  VARCHAR2(255 CHAR) NOT NULL, 
     configuration_type  VARCHAR2(255 CHAR) NOT NULL, 
     extraction_language VARCHAR2(255 CHAR) NOT NULL, 
     path                VARCHAR2(512 CHAR) NOT NULL, 
     creation_date       TIMESTAMP NOT NULL, 
     start_date          TIMESTAMP, 
     sparql_query        CLOB NOT NULL, 
     PRIMARY KEY (id), 
     UNIQUE (path) 
  ) 
lob (sparql_query) store AS (TABLESPACE cellar_lob_01); 

CREATE TABLE extraction_execution 
  ( 
     id                          NUMBER(19, 0) NOT NULL, 
     execution_status            VARCHAR2(255 CHAR) NOT NULL, 
     effective_execution_date    TIMESTAMP, 
     start_date                  TIMESTAMP, 
     end_date                    TIMESTAMP, 
     error_description           CLOB, 
     error_stacktrace            CLOB, 
     sparql_temp_file_path       VARCHAR2(512 CHAR), 
     archive_temp_directory_path VARCHAR2(512 CHAR), 
     archive_file_path           VARCHAR2(512 CHAR), 
     next_execution_generated    NUMBER(1, 0) NOT NULL, 
     instance_id                 VARCHAR2(255 CHAR), 
     extraction_configuration_id NUMBER(19, 0) NOT NULL, 
     PRIMARY KEY (id) 
  ) 
lob (error_description, error_stacktrace) store AS (TABLESPACE cellar_lob_01); 

CREATE TABLE extraction_work_identifier 
  ( 
     id                      NUMBER(19, 0) NOT NULL, 
     work_id                 VARCHAR2(255 CHAR) NOT NULL, 
     extraction_execution_id NUMBER(19, 0) NOT NULL, 
     PRIMARY KEY(id) 
  ); 

CREATE TABLE external_link_pattern 
  ( 
     id            NUMBER(19, 0) NOT NULL, 
     match_pattern VARCHAR2(512 CHAR) NOT NULL, 
     url_pattern   VARCHAR2(4000 CHAR) NOT NULL, 
     PRIMARY KEY (id) 
  ); 

CREATE TABLE ingestion_history 
  ( 
     id             NUMBER NOT NULL PRIMARY KEY, 
     cellar_id      VARCHAR2(255) NOT NULL, 
     root_cellar_id VARCHAR2(255),
     relatives      NUMBER(1, 0),
     wemi_class     VARCHAR2(255) NOT NULL, 
     action_type    VARCHAR2(25) NOT NULL, 
     feed_action_type    VARCHAR2(25) NOT NULL,
     action_date    TIMESTAMP(3) NOT NULL, 
     priority       VARCHAR2(50 CHAR), 
     TYPE           VARCHAR2(500),
     classes        CLOB,
     identifiers    CLOB
  ); 

CREATE TABLE nal_history 
  ( 
     id          NUMBER NOT NULL PRIMARY KEY, 
     cellar_id      VARCHAR2(255), 
     identifiers    CLOB,
     uri         VARCHAR2(255) NOT NULL, 
     version     VARCHAR2(255) NOT NULL, 
     action_date TIMESTAMP(3) NOT NULL 
  ); 

CREATE TABLE ontology_history 
  ( 
     id          NUMBER NOT NULL PRIMARY KEY,
     cellar_id      VARCHAR2(255), 
     identifiers    CLOB,
     uri         VARCHAR2(255) NOT NULL, 
     version     VARCHAR2(255) NOT NULL, 
     action_date TIMESTAMP(3) NOT NULL 
  ); 

CREATE TABLE sparql_load_history (
   ID               NUMBER(19, 0) NOT NULL,
   URI              VARCHAR2(255 CHAR),
   ACTION_DATE      TIMESTAMP,
   CELLAR_ID        VARCHAR2(255 CHAR),
   VERSION          VARCHAR2(255 CHAR),
   IDENTIFIERS    CLOB,
   PRIMARY KEY (ID)
);
  
CREATE TABLE cleaner_difference 
  ( 
     id             NUMBER(19, 0) NOT NULL, 
     operation_type NUMBER(1, 0) NOT NULL, 
     query_type     NUMBER(1, 0) NOT NULL, 
     cmr_table      NUMBER(1, 0) NOT NULL, 
     cellar_id      VARCHAR2(500 byte) NOT NULL, 
     difference     CLOB NOT NULL, 
     operation_date TIMESTAMP NOT NULL, 
     PRIMARY KEY (id) 
  ) 
lob (difference) store AS (TABLESPACE cellar_cleaner_lob_01); 

CREATE TABLE aligner_misalignment 
  ( 
     id             NUMBER(19, 0) NOT NULL, 
     operation_type NUMBER(1, 0) NOT NULL, 
     repository     NUMBER(1, 0) NOT NULL, 
     cellar_id      VARCHAR2(500 byte) NOT NULL, 
     operation_date TIMESTAMP NOT NULL, 
     PRIMARY KEY (id) 
  ); 

CREATE TABLE history_pid_cid 
  ( 
     id                    NUMBER(19, 0) NOT NULL, 
     cellar_identifier     VARCHAR2(255 CHAR), 
     production_identifier VARCHAR2(255 CHAR), 
     obsolete              VARCHAR2(1), 
     creation_date         TIMESTAMP, 
     PRIMARY KEY (id) 
  ); 

CREATE TABLE uri_templates 
  ( 
     id          NUMBER(19, 0) NOT NULL, 
     uri_pattern VARCHAR2(500 byte) NOT NULL, 
     sparql      CLOB NOT NULL, 
     xslt        VARCHAR2(500 byte), 
     SEQUENCE    NUMBER(8, 0) NOT NULL, 
     PRIMARY KEY (id) 
  ) 
lob (sparql) store AS (TABLESPACE cellar_lob_01); 

CREATE TABLE virt_excp_messages 
  ( 
     id      NUMBER(19, 0) NOT NULL, 
     message VARCHAR2(500 CHAR), 
     PRIMARY KEY (id), 
     CONSTRAINT strlen CHECK(Length(message) > 10) 
  ); 

CREATE TABLE scheduled_reindex 
  ( 
     id                 NUMBER(19) NOT NULL, 
     cron_expression    VARCHAR2(25 CHAR) NOT NULL, 
     duration           VARCHAR2(50 CHAR) NOT NULL, 
     sparql_query       VARCHAR2(500 CHAR), 
     all_content        VARCHAR2(1) DEFAULT 0, 
     all_content_period VARCHAR2(1) DEFAULT 0, 
     use_sparql         VARCHAR2(1) DEFAULT 0, 
     period             VARCHAR2(50 CHAR), 
     enabled            VARCHAR2(1) DEFAULT 0, 
     PRIMARY KEY (id) 
  ); 

-- Create PACKAGE_HISTORY table
CREATE TABLE PACKAGE_HISTORY
(
     id                          NUMBER(19, 0) NOT NULL,
     package_uuid                VARCHAR2(255 CHAR) NOT NULL,
     received_date               TIMESTAMP(6) NOT NULL,
     callback_url                VARCHAR2(255 CHAR),
     mets_package_name           VARCHAR2(255 CHAR) NOT NULL,
     preprocess_ingestion_status VARCHAR2(5 BYTE) NOT NULL,
     PRIMARY KEY (id),
     UNIQUE (package_uuid),
     CONSTRAINT preprocess_ingestion_status_chk CHECK (preprocess_ingestion_status IN ('NA', 'W', 'R', 'S', 'F')) ENABLE
);

-- Create STRUCTMAP_STATUS_HISTORY table
CREATE TABLE STRUCTMAP_STATUS_HISTORY
( 
	id                        NUMBER(19, 0) NOT NULL,
    structmap_name            VARCHAR2(255 CHAR),
	package_id                NUMBER(19, 0) NOT NULL,
	ingestion_status          VARCHAR2(5 BYTE) NOT NULL,
	cellar_id                 VARCHAR2(500 CHAR),
	indx_execution_status     VARCHAR2(5 BYTE),
	indx_execution_date       TIMESTAMP(6),
	indx_execution_start_date TIMESTAMP(6),
	indx_created_on           TIMESTAMP(6),
	languages                 VARCHAR2(255 CHAR),
	shacl_report              CLOB,
	PRIMARY KEY (id),
    CONSTRAINT ingestion_status_chk CHECK (ingestion_status IN ('NA', 'W', 'R', 'S', 'F')) ENABLE,
    CONSTRAINT indx_execution_status_chk CHECK (indx_execution_status IN ('N', 'P', 'X', 'D', 'E', 'R')) ENABLE,
	CONSTRAINT fk_package_history FOREIGN KEY (package_id) REFERENCES PACKAGE_HISTORY(id)
);

-- Create STRUCTMAP_PID table
CREATE TABLE STRUCTMAP_PID
( 
	id            NUMBER(19, 0) NOT NULL,
	structmap_id  NUMBER(19, 0) NOT NULL,
	production_id VARCHAR2(700 CHAR) NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_structmap_status_history FOREIGN KEY (structmap_id) REFERENCES STRUCTMAP_STATUS_HISTORY(id)
);

CREATE OR replace TRIGGER scheduled_reindex_uniq 
  BEFORE INSERT ON scheduled_reindex 
  FOR EACH ROW 
DECLARE 
    a NUMBER; 
BEGIN 
    SELECT Count(*) 
    INTO   a 
    FROM   scheduled_reindex; 

    IF ( a != 0 ) THEN 
      Raise_application_error(-20100, 'Can not insert more than one row'); 
    END IF; 
END; 

/
-- create EMAIL table
CREATE TABLE EMAIL
  (
     ID                 NUMBER(19,0) NOT NULL,
	 ADDRESS            VARCHAR2(384 BYTE) NOT NULL,
	 SUBJECT            VARCHAR2(512 BYTE),
	 CONTENT            VARCHAR2(4000 BYTE),
	 STATUS             VARCHAR2(1 BYTE) NOT NULL,
	 CREATED_ON         TIMESTAMP (6) NOT NULL,
	 SENT_ON            TIMESTAMP (6),
	 PRIMARY KEY (ID)
   );

-- create index 
CREATE INDEX CONTENT_FILE_NAME 
  ON cellar_identifier(file_name);   

CREATE INDEX production_identifier_idx 
  ON production_identifier(cellar_identifier_id); 

CREATE INDEX speedup_smp_1 
  ON sip_mets_processing (sip_name, sip_processing_end_date) 
TABLESPACE cellarapp_index; 

CREATE INDEX cleaner_operation_type_idx 
  ON cleaner_difference(operation_type) 
TABLESPACE cellar_cleaner_index_01; 

CREATE INDEX cleaner_query_type_idx 
  ON cleaner_difference(query_type) 
TABLESPACE cellar_cleaner_index_01; 

CREATE INDEX cleaner_cmr_table_idx 
  ON cleaner_difference(cmr_table) 
TABLESPACE cellar_cleaner_index_01; 

CREATE INDEX cleaner_cellar_id_idx 
  ON cleaner_difference(cellar_id) 
TABLESPACE cellar_cleaner_index_01; 

CREATE INDEX cleaner_operation_date_idx 
  ON cleaner_difference(operation_date) 
TABLESPACE cellar_cleaner_index_01; 

CREATE INDEX ingestion_history_wc_idx 
  ON ingestion_history (wemi_class) 
TABLESPACE cellarapp_index; 

CREATE INDEX ingestion_history_at_idx 
  ON ingestion_history (action_type) 
TABLESPACE cellarapp_index; 

CREATE INDEX ingestion_history_fat_idx
    ON ingestion_history (feed_action_type)
TABLESPACE cellarapp_index;

CREATE INDEX ingestion_history_ad_idx 
  ON ingestion_history (action_date) 
TABLESPACE cellarapp_index; 

CREATE INDEX nal_history_ad_idx 
  ON nal_history (action_date) 
TABLESPACE cellarapp_index; 

CREATE INDEX ontology_history_ad_idx 
  ON ontology_history (action_date) 
TABLESPACE cellarapp_index; 

CREATE INDEX aligner_operation_type_idx 
  ON aligner_misalignment(operation_type) 
TABLESPACE cellar_aligner_index_01; 

CREATE INDEX aligner_cellar_id_idx 
  ON aligner_misalignment(cellar_id) 
TABLESPACE cellar_aligner_index_01; 

CREATE INDEX aligner_operation_date_idx 
  ON aligner_misalignment(operation_date) 
TABLESPACE cellar_aligner_index_01; 

CREATE INDEX aligner_repository_idx 
  ON aligner_misalignment(repository) 
TABLESPACE cellar_aligner_index_01; 

CREATE INDEX i_su_hpidcid_1 
  ON history_pid_cid(production_identifier, obsolete, id) 
TABLESPACE cellarapp_index; 

CREATE INDEX virt_excp_messages_idx 
  ON virt_excp_messages(message); 

-- alter tables 
ALTER TABLE users 
  ADD CONSTRAINT fk_user_group FOREIGN KEY (group_id) REFERENCES GROUPS; 

ALTER TABLE group_roles 
  ADD CONSTRAINT fk_group_role_group FOREIGN KEY (group_id) REFERENCES GROUPS ON 
  DELETE CASCADE; 

ALTER TABLE group_roles 
  ADD CONSTRAINT fk_group_role_role FOREIGN KEY (role_id) REFERENCES ROLES ON 
  DELETE CASCADE; 

ALTER TABLE production_identifier 
  ADD CONSTRAINT fk75a913ef52d4d9c4 FOREIGN KEY (cellar_identifier_id) 
  REFERENCES cellar_identifier; 

ALTER TABLE batch_job_work_identifier 
  ADD CONSTRAINT fk_batch_job_work_identifier FOREIGN KEY (batch_job_id) 
  REFERENCES batch_job ON DELETE CASCADE; 

ALTER TABLE extraction_work_identifier 
  ADD CONSTRAINT fk_extraction_execution_work FOREIGN KEY ( 
  extraction_execution_id) REFERENCES extraction_execution ON DELETE CASCADE; 

ALTER TABLE extraction_execution 
  ADD CONSTRAINT fk_extraction_execution FOREIGN KEY ( 
  extraction_configuration_id) REFERENCES extraction_configuration ON DELETE 
  CASCADE; 

ALTER TABLE batch_job 
  ADD "METADATA_ONLY" VARCHAR2(1); 

ALTER TABLE cellar_identifier 
  ADD "READ_ONLY" VARCHAR2(1) DEFAULT 0; 

ALTER TABLE batch_job 
  ADD "USE_AGNOSTIC_URL" VARCHAR2(1) DEFAULT 1; 

-- triggers 
CREATE OR replace TRIGGER history_pid_cid_del_trg 
  BEFORE DELETE ON production_identifier 
  FOR EACH ROW 
BEGIN 
    BEGIN 
        INSERT INTO history_pid_cid 
                    (id, 
                     cellar_identifier, 
                     production_identifier, 
                     creation_date, 
                     obsolete) 
        VALUES      ( hibernate_sequence.NEXTVAL, 
                     (SELECT uuid 
                      FROM   cellar_identifier 
                      WHERE  cellar_identifier.id = :old.cellar_identifier_id), 
                     :old.production_id, 
                     :old.creation_date, 
                     0 ); 
    END; 
END; 

/ 
ALTER TRIGGER history_pid_cid_del_trg ENABLE; 

-- sequences 
CREATE SEQUENCE hibernate_sequence; 
CREATE SEQUENCE "CELLAROWNER"."URI_TEMPLATES_SEQ";
