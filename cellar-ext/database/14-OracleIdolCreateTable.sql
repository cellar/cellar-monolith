-- Idol tables

-- create tables
CREATE TABLE TB_CELLAR_MODIFICATION
(
  CRM_ID                    NUMBER(19)          NOT NULL,
  CRM_OPERATION_CD          VARCHAR2(1 CHAR)    NOT NULL,
  CRM_CELLAR_URI_NM         VARCHAR2(240 CHAR)  NOT NULL,
  CRM_CREATED_ON            DATE                NOT NULL,
  CRM_PRIORITY_NO           NUMBER(1)           NOT NULL,
  CRM_LANGUAGE_ISO_CODE_NM  VARCHAR2(2 CHAR)    NOT NULL,
  CRM_METADATA_LOB          CLOB,
  CRM_CONTENT_URLS_LOB      CLOB
)
LOB (CRM_METADATA_LOB) STORE AS (
  TABLESPACE IDOL_DATA
  DISABLE      STORAGE IN ROW
  CHUNK       16384
  RETENTION
  NOCACHE
  LOGGING)
LOB (CRM_CONTENT_URLS_LOB) STORE AS (
  TABLESPACE IDOL_DATA
  ENABLE       STORAGE IN ROW
  CHUNK       8192
  RETENTION
  NOCACHE
  LOGGING)
TABLESPACE IDOL_DATA
PCTUSED    0
PCTFREE    10
INITRANS   10
MAXTRANS   255
LOGGING
PARTITION BY LIST (CRM_LANGUAGE_ISO_CODE_NM)
(  
  PARTITION P_BG VALUES ('bg')
    NOLOGGING
    COMPRESS BASIC 
    TABLESPACE TS_BG
    LOB (CRM_METADATA_LOB) STORE AS (
      TABLESPACE TS_LOBS_BG
      DISABLE      STORAGE IN ROW
      CHUNK       16384
      RETENTION
      NOCACHE
      LOGGING)
    LOB (CRM_CONTENT_URLS_LOB) STORE AS (
      TABLESPACE TS_LOBS_BG
      ENABLE       STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING)
    PCTFREE    0
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          1M
                NEXT             10M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_CS VALUES ('cs')
    NOLOGGING
    COMPRESS BASIC 
    TABLESPACE TS_CS
    LOB (CRM_METADATA_LOB) STORE AS (
      TABLESPACE TS_LOBS_CS
      DISABLE      STORAGE IN ROW
      CHUNK       16384
      RETENTION
      NOCACHE
      LOGGING)
    LOB (CRM_CONTENT_URLS_LOB) STORE AS (
      TABLESPACE TS_LOBS_CS
      DISABLE      STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING)
    PCTFREE    0
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          1M
                NEXT             10M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_DA VALUES ('da')
    NOLOGGING
    COMPRESS BASIC 
    TABLESPACE TS_DA
    LOB (CRM_METADATA_LOB) STORE AS (
      TABLESPACE TS_LOBS_DA
      DISABLE      STORAGE IN ROW
      CHUNK       16384
      RETENTION
      NOCACHE
      LOGGING)
    LOB (CRM_CONTENT_URLS_LOB) STORE AS (
      TABLESPACE TS_LOBS_DA
      DISABLE      STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING)
    PCTFREE    0
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          1M
                NEXT             10M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_DE VALUES ('de')
    NOLOGGING
    COMPRESS BASIC 
    TABLESPACE TS_DE
    LOB (CRM_METADATA_LOB) STORE AS (
      TABLESPACE TS_LOBS_DE
      DISABLE      STORAGE IN ROW
      CHUNK       16384
      RETENTION
      NOCACHE
      LOGGING)
    LOB (CRM_CONTENT_URLS_LOB) STORE AS (
      TABLESPACE TS_LOBS_DE
      ENABLE       STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING)
    PCTFREE    0
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          1M
                NEXT             10M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_EL VALUES ('el')
    NOLOGGING
    COMPRESS BASIC 
    TABLESPACE TS_EL
    LOB (CRM_METADATA_LOB) STORE AS (
      TABLESPACE TS_LOBS_EL
      DISABLE      STORAGE IN ROW
      CHUNK       16384
      RETENTION
      NOCACHE
      LOGGING)
    LOB (CRM_CONTENT_URLS_LOB) STORE AS (
      TABLESPACE TS_LOBS_EL
      DISABLE      STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING)
    PCTFREE    0
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          1M
                NEXT             10M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_EN VALUES ('en')
    NOLOGGING
    COMPRESS BASIC 
    TABLESPACE TS_EN
    LOB (CRM_METADATA_LOB) STORE AS (
      TABLESPACE TS_LOBS_EN
      DISABLE      STORAGE IN ROW
      CHUNK       16384
      RETENTION
      NOCACHE
      LOGGING)
    LOB (CRM_CONTENT_URLS_LOB) STORE AS (
      TABLESPACE TS_LOBS_EN
      ENABLE       STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING)
    PCTFREE    0
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          1M
                NEXT             10M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_ES VALUES ('es')
    NOLOGGING
    COMPRESS BASIC 
    TABLESPACE TS_ES
    LOB (CRM_METADATA_LOB) STORE AS (
      TABLESPACE TS_LOBS_ES
      DISABLE      STORAGE IN ROW
      CHUNK       16384
      RETENTION
      NOCACHE
      LOGGING)
    LOB (CRM_CONTENT_URLS_LOB) STORE AS (
      TABLESPACE TS_LOBS_ES
      ENABLE       STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING)
    PCTFREE    0
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          1M
                NEXT             10M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_ET VALUES ('et')
    NOLOGGING
    COMPRESS BASIC 
    TABLESPACE TS_ET
    LOB (CRM_METADATA_LOB) STORE AS (
      TABLESPACE TS_LOBS_ET
      DISABLE      STORAGE IN ROW
      CHUNK       16384
      RETENTION
      NOCACHE
      LOGGING)
    LOB (CRM_CONTENT_URLS_LOB) STORE AS (
      TABLESPACE TS_LOBS_ET
      ENABLE       STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING)
    PCTFREE    0
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          1M
                NEXT             10M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_FI VALUES ('fi')
    NOLOGGING
    COMPRESS BASIC 
    TABLESPACE TS_FI
    LOB (CRM_METADATA_LOB) STORE AS (
      TABLESPACE TS_LOBS_FI
      DISABLE      STORAGE IN ROW
      CHUNK       16384
      RETENTION
      NOCACHE
      LOGGING)
    LOB (CRM_CONTENT_URLS_LOB) STORE AS (
      TABLESPACE TS_LOBS_FI
      ENABLE       STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING)
    PCTFREE    0
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          1M
                NEXT             10M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_FR VALUES ('fr')
    NOLOGGING
    COMPRESS BASIC 
    TABLESPACE TS_FR
    LOB (CRM_METADATA_LOB) STORE AS (
      TABLESPACE TS_LOBS_FR
      DISABLE      STORAGE IN ROW
      CHUNK       16384
      RETENTION
      NOCACHE
      LOGGING)
    LOB (CRM_CONTENT_URLS_LOB) STORE AS (
      TABLESPACE TS_LOBS_FR
      ENABLE       STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING)
    PCTFREE    0
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          1M
                NEXT             10M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_GA VALUES ('ga')
    NOLOGGING
    COMPRESS BASIC 
    TABLESPACE TS_GA
    LOB (CRM_METADATA_LOB) STORE AS (
      TABLESPACE TS_LOBS_GA
      DISABLE      STORAGE IN ROW
      CHUNK       16384
      RETENTION
      NOCACHE
      LOGGING)
    LOB (CRM_CONTENT_URLS_LOB) STORE AS (
      TABLESPACE TS_LOBS_GA
      ENABLE       STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING)
    PCTFREE    0
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          1M
                NEXT             10M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_HU VALUES ('hu')
    NOLOGGING
    COMPRESS BASIC 
    TABLESPACE TS_HU
    LOB (CRM_METADATA_LOB) STORE AS (
      TABLESPACE TS_LOBS_HU
      DISABLE      STORAGE IN ROW
      CHUNK       16384
      RETENTION
      NOCACHE
      LOGGING)
    LOB (CRM_CONTENT_URLS_LOB) STORE AS (
      TABLESPACE TS_LOBS_HU
      ENABLE       STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING)
    PCTFREE    0
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          1M
                NEXT             10M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_IT VALUES ('it')
    NOLOGGING
    COMPRESS BASIC 
    TABLESPACE TS_IT
    LOB (CRM_METADATA_LOB) STORE AS (
      TABLESPACE TS_LOBS_IT
      DISABLE      STORAGE IN ROW
      CHUNK       16384
      RETENTION
      NOCACHE
      LOGGING)
    LOB (CRM_CONTENT_URLS_LOB) STORE AS (
      TABLESPACE TS_LOBS_IT
      ENABLE       STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING)
    PCTFREE    0
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          1M
                NEXT             10M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_LT VALUES ('lt')
    NOLOGGING
    COMPRESS BASIC 
    TABLESPACE TS_LT
    LOB (CRM_METADATA_LOB) STORE AS (
      TABLESPACE TS_LOBS_LT
      DISABLE      STORAGE IN ROW
      CHUNK       16384
      RETENTION
      NOCACHE
      LOGGING)
    LOB (CRM_CONTENT_URLS_LOB) STORE AS (
      TABLESPACE TS_LOBS_LT
      ENABLE       STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING)
    PCTFREE    0
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          1M
                NEXT             10M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_LV VALUES ('lv')
    NOLOGGING
    COMPRESS BASIC 
    TABLESPACE TS_LV
    LOB (CRM_METADATA_LOB) STORE AS (
      TABLESPACE TS_LOBS_LV
      DISABLE      STORAGE IN ROW
      CHUNK       16384
      RETENTION
      NOCACHE
      LOGGING)
    LOB (CRM_CONTENT_URLS_LOB) STORE AS (
      TABLESPACE TS_LOBS_LV
      ENABLE       STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING)
    PCTFREE    0
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          1M
                NEXT             10M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_MT VALUES ('mt')
    NOLOGGING
    COMPRESS BASIC 
    TABLESPACE TS_MT
    LOB (CRM_METADATA_LOB) STORE AS (
      TABLESPACE TS_LOBS_MT
      DISABLE      STORAGE IN ROW
      CHUNK       16384
      RETENTION
      NOCACHE
      LOGGING)
    LOB (CRM_CONTENT_URLS_LOB) STORE AS (
      TABLESPACE TS_LOBS_MT
      ENABLE       STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING)
    PCTFREE    0
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          1M
                NEXT             10M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_NL VALUES ('nl')
    NOLOGGING
    COMPRESS BASIC 
    TABLESPACE TS_NL
    LOB (CRM_METADATA_LOB) STORE AS (
      TABLESPACE TS_LOBS_NL
      DISABLE      STORAGE IN ROW
      CHUNK       16384
      RETENTION
      NOCACHE
      LOGGING)
    LOB (CRM_CONTENT_URLS_LOB) STORE AS (
      TABLESPACE TS_LOBS_NL
      ENABLE       STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING)
    PCTFREE    0
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          1M
                NEXT             10M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_PL VALUES ('pl')
    NOLOGGING
    COMPRESS BASIC 
    TABLESPACE TS_PL
    LOB (CRM_METADATA_LOB) STORE AS (
      TABLESPACE TS_LOBS_PL
      DISABLE      STORAGE IN ROW
      CHUNK       16384
      RETENTION
      NOCACHE
      LOGGING)
    LOB (CRM_CONTENT_URLS_LOB) STORE AS (
      TABLESPACE TS_LOBS_PL
      ENABLE       STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING)
    PCTFREE    0
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          1M
                NEXT             10M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_PT VALUES ('pt')
    NOLOGGING
    COMPRESS BASIC 
    TABLESPACE TS_PT
    LOB (CRM_METADATA_LOB) STORE AS (
      TABLESPACE TS_LOBS_PT
      DISABLE      STORAGE IN ROW
      CHUNK       16384
      RETENTION
      NOCACHE
      LOGGING)
    LOB (CRM_CONTENT_URLS_LOB) STORE AS (
      TABLESPACE TS_LOBS_PT
      ENABLE       STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING)
    PCTFREE    0
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          1M
                NEXT             10M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_RO VALUES ('ro')
    NOLOGGING
    COMPRESS BASIC 
    TABLESPACE TS_RO
    LOB (CRM_METADATA_LOB) STORE AS (
      TABLESPACE TS_LOBS_RO
      DISABLE      STORAGE IN ROW
      CHUNK       16384
      RETENTION
      NOCACHE
      LOGGING)
    LOB (CRM_CONTENT_URLS_LOB) STORE AS (
      TABLESPACE TS_LOBS_RO
      ENABLE       STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING)
    PCTFREE    0
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          1M
                NEXT             10M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_SK VALUES ('sk')
    NOLOGGING
    COMPRESS BASIC 
    TABLESPACE TS_SK
    LOB (CRM_METADATA_LOB) STORE AS (
      TABLESPACE TS_LOBS_SK
      DISABLE      STORAGE IN ROW
      CHUNK       16384
      RETENTION
      NOCACHE
      LOGGING)
    LOB (CRM_CONTENT_URLS_LOB) STORE AS (
      TABLESPACE TS_LOBS_SK
      ENABLE       STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING)
    PCTFREE    0
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          1M
                NEXT             10M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_SL VALUES ('sl')
    NOLOGGING
    COMPRESS BASIC 
    TABLESPACE TS_SL
    LOB (CRM_METADATA_LOB) STORE AS (
      TABLESPACE TS_LOBS_SL
      DISABLE      STORAGE IN ROW
      CHUNK       16384
      RETENTION
      NOCACHE
      LOGGING)
    LOB (CRM_CONTENT_URLS_LOB) STORE AS (
      TABLESPACE TS_LOBS_SL
      ENABLE       STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING)
    PCTFREE    0
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          1M
                NEXT             10M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_SV VALUES ('sv')
    NOLOGGING
    COMPRESS BASIC 
    TABLESPACE TS_SV
    LOB (CRM_METADATA_LOB) STORE AS (
      TABLESPACE TS_LOBS_SV
      DISABLE      STORAGE IN ROW
      CHUNK       16384
      RETENTION
      NOCACHE
      LOGGING)
    LOB (CRM_CONTENT_URLS_LOB) STORE AS (
      TABLESPACE TS_LOBS_SV
      ENABLE       STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING)
    PCTFREE    0
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          1M
                NEXT             10M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),
  PARTITION P_HR VALUES ('hr')
    NOLOGGING
    COMPRESS BASIC 
    TABLESPACE TS_HR
    LOB (CRM_METADATA_LOB) STORE AS (
      TABLESPACE TS_LOBS_HR
      DISABLE      STORAGE IN ROW
      CHUNK       16384
      RETENTION
      NOCACHE
      LOGGING)
    LOB (CRM_CONTENT_URLS_LOB) STORE AS (
      TABLESPACE TS_LOBS_HR
      ENABLE       STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING)
    PCTFREE    0
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          1M
                NEXT             10M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),
  PARTITION P_NO VALUES ('no')
    NOLOGGING
    COMPRESS BASIC 
    TABLESPACE TS_NO
    LOB (CRM_METADATA_LOB) STORE AS (
      TABLESPACE TS_LOBS_NO
      DISABLE      STORAGE IN ROW
      CHUNK       16384
      RETENTION
      NOCACHE
      LOGGING)
    LOB (CRM_CONTENT_URLS_LOB) STORE AS (
      TABLESPACE TS_LOBS_NO
      ENABLE       STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING)
    PCTFREE    0
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          1M
                NEXT             10M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),
  PARTITION P_IS VALUES ('is')
    NOLOGGING
    COMPRESS BASIC 
    TABLESPACE TS_IS
    LOB (CRM_METADATA_LOB) STORE AS (
      TABLESPACE TS_LOBS_IS
      DISABLE      STORAGE IN ROW
      CHUNK       16384
      RETENTION
      NOCACHE
      LOGGING)
    LOB (CRM_CONTENT_URLS_LOB) STORE AS (
      TABLESPACE TS_LOBS_IS
      ENABLE       STORAGE IN ROW
      CHUNK       8192
      RETENTION
      NOCACHE
      LOGGING)
    PCTFREE    0
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          1M
                NEXT             10M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               )
)
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

-- create indexes
CREATE INDEX CRM_IX_OPERATION_CD ON TB_CELLAR_MODIFICATION
(CRM_OPERATION_CD)
  TABLESPACE IDOL_INDEX
  PCTFREE    10
  INITRANS   10
  MAXTRANS   255
  STORAGE    (
              INITIAL          64K
              NEXT             1M
              MINEXTENTS       1
              MAXEXTENTS       UNLIMITED
              PCTINCREASE      0
             )
LOGGING
LOCAL (  
  PARTITION P_BG
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_CS
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_DA
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_DE
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_EL
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_EN
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_ES
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_ET
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_FI
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_FR
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_GA
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_HU
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_IT
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_LT
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_LV
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_MT
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_NL
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_PL
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_PT
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_RO
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_SK
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_SL
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_SV
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_HR
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_NO
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_IS
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   10
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               )
)
NOPARALLEL;

CREATE INDEX CRM_IX_PRIORITY_NO ON TB_CELLAR_MODIFICATION
(CRM_PRIORITY_NO)
  TABLESPACE IDOL_INDEX
  PCTFREE    10
  INITRANS   10
  MAXTRANS   255
  STORAGE    (
              INITIAL          64K
              NEXT             1M
              MINEXTENTS       1
              MAXEXTENTS       UNLIMITED
              PCTINCREASE      0
             )
LOGGING
LOCAL (  
  PARTITION P_BG
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_CS
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_DA
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_DE
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_EL
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_EN
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_ES
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_ET
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_FI
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_FR
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_GA
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_HU
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_IT
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_LT
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_LV
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_MT
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_NL
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_PL
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_PT
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_RO
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_SK
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_SL
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_SV
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_HR
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_NO
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_IS
    LOGGING
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               )
)
NOPARALLEL;

CREATE UNIQUE INDEX PK_TB_CELLAR_MODIFICATION ON TB_CELLAR_MODIFICATION
(CRM_ID)
LOGGING
TABLESPACE IDOL_INDEX
PCTFREE    10
INITRANS   10
MAXTRANS   255
STORAGE    (
            INITIAL          64K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;

CREATE INDEX SPEEDUP_I_PART ON TB_CELLAR_MODIFICATION
(CRM_LANGUAGE_ISO_CODE_NM)
  TABLESPACE IDOL_INDEX
  PCTFREE    10
  INITRANS   10
  MAXTRANS   255
  STORAGE    (
              INITIAL          64K
              NEXT             1M
              MINEXTENTS       1
              MAXEXTENTS       UNLIMITED
              PCTINCREASE      0
             )
LOGGING
LOCAL (  
  PARTITION P_BG
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_CS
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_DA
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_DE
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_EL
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_EN
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_ES
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_ET
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_FI
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_FR
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_GA
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_HU
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_IT
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_LT
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_LV
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_MT
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_NL
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_PL
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_PT
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_RO
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_SK
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_SL
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_SV
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_HR
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_NO
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_IS
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               )
)
NOPARALLEL;

CREATE INDEX SPEEDUP_I_PART_1 ON TB_CELLAR_MODIFICATION
(CRM_CELLAR_URI_NM, CRM_ID)
  TABLESPACE IDOL_INDEX
  PCTFREE    10
  INITRANS   10
  MAXTRANS   255
LOCAL (  
  PARTITION P_BG
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_CS
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_DA
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_DE
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_EL
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_EN
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_ES
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_ET
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_FI
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_FR
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_GA
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_HU
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_IT
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_LT
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_LV
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_MT
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_NL
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_PL
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_PT
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_RO
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_SK
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_SL
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_SV
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_HR
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_NO
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_IS
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               )
)
NOPARALLEL;

CREATE INDEX SPEEDUP_TBCMOD_URI ON TB_CELLAR_MODIFICATION
(CRM_CELLAR_URI_NM, CRM_LANGUAGE_ISO_CODE_NM)
  TABLESPACE IDOL_INDEX
  PCTFREE    10
  INITRANS   10
  MAXTRANS   255
  STORAGE    (
              INITIAL          64K
              NEXT             1M
              MINEXTENTS       1
              MAXEXTENTS       UNLIMITED
              PCTINCREASE      0
             )
LOGGING
LOCAL (  
  PARTITION P_BG
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_CS
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_DA
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_DE
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_EL
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_EN
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_ES
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_ET
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_FI
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_FR
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_GA
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_HU
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_IT
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_LT
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_LV
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_MT
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_NL
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_PL
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_PT
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_RO
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_SK
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_SL
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_SV
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_HR
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_NO
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               ),  
  PARTITION P_IS
    LOGGING
    NOCOMPRESS 
    TABLESPACE IDOL_INDEX
    PCTFREE    10
    INITRANS   2
    MAXTRANS   255
    STORAGE    (
                INITIAL          64K
                NEXT             1M
                MINEXTENTS       1
                MAXEXTENTS       UNLIMITED
                BUFFER_POOL      DEFAULT
               )
)
NOPARALLEL;

ALTER TABLE TB_CELLAR_MODIFICATION ADD (
  CONSTRAINT PK_TB_CELLAR_MODIFICATION
  PRIMARY KEY
  (CRM_ID)
  USING INDEX PK_TB_CELLAR_MODIFICATION);

-- create sequences
CREATE SEQUENCE CRM_IDOL_TABLE_SEQ MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE ;
  
-- create triggers
CREATE OR REPLACE TRIGGER CRM_IDOL_TABLE_TRIGGER BEFORE INSERT ON TB_CELLAR_MODIFICATION
FOR EACH ROW
BEGIN
SELECT CRM_IDOL_TABLE_SEQ.NEXTVAL INTO :NEW.CRM_ID FROM DUAL;
END;
/
