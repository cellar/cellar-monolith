##### Migration update sql scripts based on the modifications applied in database script files for Cellar 8.10.0 release.
# in case a new file is created call the file explicitly using ./yourFIle.sql
# In case the change includes modifications in existing sql scripts then write the update Db scripts here so that they are used in migration.
-- Login with user CMROWNER and execute the statements mentioned bellow
--------------------------------------------------------------------------
sqlplus cmrowner/cmr06owner12
-- Remove not null constraints in order to avoid issues with existing Data
ALTER TABLE SPARQL_LOAD_REQUEST MODIFY CELLAR_ID NULL;
ALTER TABLE MODEL_LOAD_REQUEST MODIFY CELLAR_ID NULL;
commit;
exit;