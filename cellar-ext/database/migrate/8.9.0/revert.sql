##### Revert to previous release sql scripts (8.8.2.1), based on the modifications applied in database script files.

-- Login with user sysdba and execute the statements mentioned below
----------------------------------------------------------------------
sqlplus "/as sysdba"

-- Revoke create materialized view priviledge from cmrowner
-------------------------------------------------------------------------
REVOKE CREATE MATERIALIZED VIEW FROM cmrowner;
commit;
exit;


-- Login with user CMROWNER and execute the statements mentioned below
----------------------------------------------------------------------
sqlplus cmrowner/cmr06owner12

-- CLLR10815-772
DROP MATERIALIZED VIEW RELATED_NAL_SNIPPET;
DROP MATERIALIZED VIEW LOG ON CMROWNER.NAL_SNIPPET;
DROP MATERIALIZED VIEW LOG ON CMROWNER.RELATED_SNIPPET;


-- CLLR10815-274
DROP SEQUENCE CMR_ITEM_TECH_MD_SEQ;
DROP TABLE CMR_ITEM_TECHNICAL_MD;


-- CLLR10815-495/CLLR10815-507/CLLR10815-508
ALTER TABLE MODEL_LOAD_REQUEST DROP COLUMN CELLAR_ID;
ALTER TABLE SPARQL_LOAD_REQUEST DROP COLUMN CELLAR_ID;

---------------------------------------- Ticket CLLR10815-885 ----------------------------------------------------------
--------------------- Status API - Status implementation activities on Cellar Monolith ---------------------------------
-- Revert back updates to CMR_INDEX_REQUEST table of CMROWNER schema
ALTER TABLE CMR_INDEX_REQUEST DROP COLUMN STRUCTMAP_STATUS_HISTORY_ID;

commit;
exit;



-- Login with user CELLAROWNER and execute the statements mentioned below
-------------------------------------------------------------------------
sqlplus cellarowner/cellar06owner12

-- EULOGIN
UPDATE USERS SET PASSWORD = 'AfTTn/s0P+vIKdgXcqb+EDg1An/a5TQbQzXTehdp3a8=' WHERE PASSWORD IS NULL;
commit;
ALTER TABLE USERS MODIFY PASSWORD VARCHAR2(512 CHAR) NOT NULL;
ALTER TABLE USERS DROP COLUMN EMAIL;

DROP TABLE USER_ACCESS_REQUEST;

DROP TABLE EMAIL;

CREATE TABLE PERSISTENT_LOGINS 
  ( 
     username  VARCHAR(64) NOT NULL, 
     series    VARCHAR(64) PRIMARY KEY, 
     token     VARCHAR(64) NOT NULL, 
     last_used TIMESTAMP NOT NULL 
  ); 


DELETE FROM USERS WHERE USERNAME IN ('j9ba5g8', 'j9ba5gh', 'j905jbj', 'j905jbk');
commit;
DELETE FROM GROUP_ROLES WHERE GROUP_ID IN (SELECT ID FROM GROUPS WHERE GROUP_NAME IN ('Test tool', 'Archivist'));
commit;
DELETE FROM GROUPS WHERE GROUP_NAME IN ('Test tool', 'Archivist');
commit;


-- CLLR10815-421
DROP INDEX ingestion_history_fat_idx;
ALTER TABLE INGESTION_HISTORY DROP COLUMN FEED_ACTION_TYPE; 


-- CLLR10815-495/CLLR10815-507/CLLR10815-508
ALTER TABLE TABLENAL_HISTORY DROP COLUMN CELLAR_ID;
ALTER TABLE NAL_HISTORY DROP COLUMN IDENTIFIERS;
ALTER TABLE ONTOLOGY_HISTORY DROP COLUMN CELLAR_ID;
ALTER TABLE ONTOLOGY_HISTORY DROP COLUMN IDENTIFIERS;
ALTER TABLE SPARQL_LOAD_HISTORY DROP COLUMN IDENTIFIERS;
ALTER TABLE SPARQL_LOAD_HISTORY DROP COLUMN VERSION;


-- CLLR10815-250
ALTER TABLE INGESTION_HISTORY DROP COLUMN RELATIVES;


-- CLLR10815-484
DELETE FROM GROUP_ROLES WHERE ROLE_ID IN (SELECT ID FROM ROLES WHERE ROLE_ACCESS IN ('ROLE_MANAGE_VIRTUOSO_DATA'));
commit;
DELETE FROM ROLES WHERE ROLE_ACCESS = 'ROLE_MANAGE_VIRTUOSO_DATA';
commit;

-- CLLR10815-741
DELETE FROM GROUP_ROLES WHERE ROLE_ID IN (SELECT ID FROM ROLES WHERE ROLE_ACCESS IN ('ROLE_INGEST'));
DELETE FROM GROUP_ROLES WHERE ROLE_ID IN (SELECT ID FROM ROLES WHERE ROLE_ACCESS IN ('ROLE_STATUS_SERVICE'));
commit;
DELETE FROM ROLES WHERE ROLE_ACCESS = 'ROLE_INGEST';
DELETE FROM ROLES WHERE ROLE_ACCESS = 'ROLE_STATUS_SERVICE';
commit;

---------------------------------------- Ticket CLLR10815-885 ----------------------------------------------------------
--------------------- Status API - Status implementation activities on Cellar Monolith ---------------------------------

-- Revert back update to materialized view PACKAGE_QUEUE
CREATE OR REPLACE VIEW "PACKAGE_QUEUE" ("SIP_ID", "NAME", "TYPE", "DETECTION_DATE") AS
SELECT sip.ID, sip.NAME, sip.TYPE, sip.DETECTION_DATE
FROM SIP_PACKAGE sip
WHERE sip.ID = id_next_package_ingestion_ready
    WITH READ ONLY;

-- Revert back updates to SIP_PACKAGE table of CELLAROWNER schema
ALTER TABLE SIP_PACKAGE DROP COLUMN PACKAGE_HISTORY_ID;

-- Revert back updates to AUDIT_TRAIL_EVENT table of CELLAROWNER schema
ALTER TABLE AUDIT_TRAIL_EVENT DROP COLUMN PACKAGE_ID;
ALTER TABLE AUDIT_TRAIL_EVENT DROP COLUMN STRUCTMAP_ID;

-- Drop three new database tables (in reverse order) of CELLAROWNER schema
DROP TABLE STRUCTMAP_PID;
DROP TABLE STRUCTMAP_STATUS_HISTORY;
DROP TABLE PACKAGE_HISTORY;

commit;

-- CLLR10815-915
-- Returns the production identifiers and whether these are read-only or not, for the packages IDs provided as argument
CREATE OR REPLACE FUNCTION get_extended_relation_prod_ids (sip_ids_in IN t_sip_id_array) RETURN t_extended_relation_prod_ids IS
    l_extended_relation_prod_ids_result t_extended_relation_prod_ids := t_extended_relation_prod_ids();
	BEGIN
        -- Retrieve the non-normalized root/children and direct relation PIDs
        -- only for the packages whose IDs are provided as argument.
        WITH ROOT_CHILDREN_DIRECT_NON_NORMALIZED AS (
            SELECT r1.SIP_ID, r1.IS_DIRECT_RELATION_ONLY, r1.PRODUCTION_ID
            FROM RELATION_PROD_ID r1
                INNER JOIN SIP_PACKAGE sp1 ON sp1.ID = r1.SIP_ID
            WHERE sp1.ID IN (SELECT * FROM TABLE(sip_ids_in))
        ),
        -- Retrieve the root/children and direct relation PIDs (normalized and non-normalized).
	    ROOT_CHILDREN_DIRECT AS (
            -- Retrieve the previously computed non-normalized root/children and direct relations.
            SELECT *
            FROM ROOT_CHILDREN_DIRECT_NON_NORMALIZED rcdnn1
            UNION ALL
            -- Append the normalized CELLAR IDs, if any.
            SELECT rcdnn2.SIP_ID, rcdnn2.IS_DIRECT_RELATION_ONLY,
                -- Convert CELLAR-ID to base-CELLAR-ID.
                CASE
                    WHEN INSTR(c1.UUID, '.') = 0 THEN c1.UUID
                    ELSE SUBSTR(c1.UUID, 0, INSTR(c1.UUID, '.') - 1)
                END AS PRODUCTION_ID
            FROM ROOT_CHILDREN_DIRECT_NON_NORMALIZED rcdnn2
                INNER JOIN PRODUCTION_IDENTIFIER p1 ON p1.PRODUCTION_ID = rcdnn2.PRODUCTION_ID
                INNER JOIN CELLAR_IDENTIFIER c1 ON c1.ID = p1.CELLAR_IDENTIFIER_ID
        ),
        -- Retrieve the non-normalized inverse relations.
        INVERSE_NON_NORMALIZED AS (
            -- Retrieve the non-normalized inverse-on-source relations.
            SELECT rcd.SIP_ID,
                -- Convert CELLAR-ID to base-CELLAR-ID, if applicable.
                CASE
                    WHEN INSTR(inv1.TARGET_ID, 'cellar:') = 0 THEN inv1.TARGET_ID
                    WHEN INSTR(inv1.TARGET_ID, '.') != 0 THEN SUBSTR(inv1.TARGET_ID, 0, INSTR(inv1.TARGET_ID, '.') - 1)
                    ELSE inv1.TARGET_ID
                END AS PRODUCTION_ID
            FROM CMROWNER.CMR_INVERSE_RELATIONS inv1
                INNER JOIN ROOT_CHILDREN_DIRECT rcd ON rcd.PRODUCTION_ID = inv1.SOURCE_ID
            WHERE rcd.IS_DIRECT_RELATION_ONLY = 'N'
            UNION ALL
            -- Retrieve the non-normalized inverse-on-target relations.
            SELECT rcd.SIP_ID,
                -- Convert CELLAR-ID to base-CELLAR-ID, if applicable.
                CASE
                    WHEN INSTR(inv2.SOURCE_ID, 'cellar:') = 0 THEN inv2.SOURCE_ID
                    WHEN INSTR(inv2.SOURCE_ID, '.') != 0 THEN SUBSTR(inv2.SOURCE_ID, 0, INSTR(inv2.SOURCE_ID, '.') - 1)
                    ELSE inv2.SOURCE_ID
                END AS PRODUCTION_ID
            FROM CMROWNER.CMR_INVERSE_RELATIONS inv2
                INNER JOIN ROOT_CHILDREN_DIRECT rcd ON rcd.PRODUCTION_ID = inv2.TARGET_ID
            WHERE rcd.IS_DIRECT_RELATION_ONLY = 'N'
        ),
        -- Retrieve the normalized inverse relations.
        INVERSE_NORMALIZED AS (
            SELECT inn2.SIP_ID,
                -- Convert CELLAR-ID to base-CELLAR-ID.
                CASE
                    WHEN INSTR(c2.UUID, '.') = 0 THEN c2.UUID
                    ELSE SUBSTR(c2.UUID, 0, INSTR(c2.UUID, '.') - 1)
                END AS PRODUCTION_ID
            FROM INVERSE_NON_NORMALIZED inn2
                INNER JOIN PRODUCTION_IDENTIFIER p2 ON p2.PRODUCTION_ID = inn2.PRODUCTION_ID
                INNER JOIN CELLAR_IDENTIFIER c2 ON c2.ID = p2.CELLAR_IDENTIFIER_ID
        )
	    -- Stores and returns all applicable production identifiers along with their read-only status.
	    -- A production-id is read-only if it is a relation and an actual PID (not a cellar-identifier).
	    SELECT t_extended_relation_prod_id_row(PRODUCTION_ID, IS_READ_ONLY)
	    BULK COLLECT INTO l_extended_relation_prod_ids_result
	    FROM (
	     	-- The non-normalized inverse relations.
		    SELECT inn1.PRODUCTION_ID,
		    	CASE
		    		WHEN INSTR(inn1.PRODUCTION_ID, 'cellar:') = 0 THEN 'Y'
		    		ELSE 'N'
		    	END AS IS_READ_ONLY
		    FROM INVERSE_NON_NORMALIZED inn1
		    UNION ALL
		    -- Append the normalized inverse relations, if any.
		    SELECT in1.PRODUCTION_ID,
		    	CASE
		    		WHEN INSTR(in1.PRODUCTION_ID, 'cellar:') = 0 THEN 'Y'
		    		ELSE 'N'
		    	END AS IS_READ_ONLY
		    FROM INVERSE_NORMALIZED in1
		    UNION ALL
		    -- Append the normalized and non-normalized root/children and direct relation PIDs.
		    SELECT rcd2.PRODUCTION_ID,
		    	CASE WHEN rcd2.IS_DIRECT_RELATION_ONLY = 'Y' AND INSTR(rcd2.PRODUCTION_ID, 'cellar:') = 0 THEN 'Y'
		    	ELSE 'N'
		    	END AS IS_READ_ONLY
		    FROM ROOT_CHILDREN_DIRECT rcd2
	    );
	    RETURN l_extended_relation_prod_ids_result;
	END;
/

-- CLLR10815-915
-- Returns the ID of the first (in terms of ordering/priority) package (SIP_PACKAGE.ID) that can be ingested
-- in a conflict-free manner with regards to any packages being ingested at that point.
CREATE OR REPLACE FUNCTION id_next_package_ingestion_ready RETURN NUMBER IS
	-- The number of conflicting production identifiers between the packages being ingested
	-- and the scheduled package under consideration.
	l_num_conflicting_pids NUMBER;

	-- The production identifiers of the packages being ingested.
	l_ingesting_extended_relation_prod_ids t_extended_relation_prod_ids := t_extended_relation_prod_ids();

	-- The production identifiers of the scheduled package being considered for ingestion.
	l_scheduled_pkg_extended_relation_prod_ids t_extended_relation_prod_ids := t_extended_relation_prod_ids();

	-- The IDs of the packages being ingested.
	l_ingesting_sip_ids t_sip_id_array := t_sip_id_array();

	-- The ID of the package considered for ingetion.
    l_scheduled_sip_id t_sip_id_array := t_sip_id_array();

    -- The STATUS of the package considered for ingestion.
    l_scheduled_sip_status VARCHAR2(10);


    -- The ordered/prioritized list of package references (contains references with status 'SCHEDULED'/'INGESTING'/'FINISHED').
	CURSOR c_scheduled_packages_list IS
		SELECT *
		FROM PACKAGE_LIST pl
		ORDER BY pl.ORDERING;

	BEGIN

		-- Retrieves the IDs (SIP_PACKAGE.ID) of the packages being ingested.
		SELECT sp.ID
		BULK COLLECT INTO l_ingesting_sip_ids
		FROM SIP_PACKAGE sp
		WHERE sp.STATUS = 'I';

		-- Retrieves the production identifiers of the packages being ingested.
		SELECT t_extended_relation_prod_id_row(PRODUCTION_ID, IS_READ_ONLY)
		BULK COLLECT INTO l_ingesting_extended_relation_prod_ids
		FROM TABLE(get_extended_relation_prod_ids(l_ingesting_sip_ids));

		-- Iterates over the ordered/prioritized list of scheduled packages IDs.
		FOR scheduled_package IN c_scheduled_packages_list LOOP

            -- Stores the STATUS of the currently evaluated package reference.
            SELECT sp.STATUS
            COLLECT INTO l_scheduled_sip_status
            FROM SIP_PACKAGE sp
            WHERE sp.ID = scheduled_package.SIP_ID;

            -- If the currently evaluated package reference is not 'SCHEDULED',
            -- then proceed to evaluate the next candidate package reference.
            IF (l_scheduled_sip_status != 'S') THEN
                CONTINUE;
            END IF;

			-- Stores the package ID of the current scheduled package.
            SELECT scheduled_package.SIP_ID
            BULK COLLECT INTO l_scheduled_sip_id
            FROM DUAL;

            -- Retrieves the production identifiers of the scheduled package being
            -- currently considered for ingestion.
			SELECT t_extended_relation_prod_id_row(PRODUCTION_ID, IS_READ_ONLY)
			BULK COLLECT INTO l_scheduled_pkg_extended_relation_prod_ids
			FROM TABLE(get_extended_relation_prod_ids(l_scheduled_sip_id));

			-- Checks whether there are any conflicts between the production identifiers of the packages being ingested
			-- and the scheduled package being currently considered.
			SELECT COUNT(1)
			INTO l_num_conflicting_pids
			FROM TABLE(l_ingesting_extended_relation_prod_ids) ingesting
				INNER JOIN TABLE(l_scheduled_pkg_extended_relation_prod_ids) scheduled ON ingesting.PRODUCTION_ID = scheduled.PRODUCTION_ID
					AND NOT (ingesting.IS_READ_ONLY = scheduled.IS_READ_ONLY AND ingesting.IS_READ_ONLY = 'Y');

			-- If no conflicts are detected, then this package can be submitted for ingestion,
			-- and its ID is returned.
			IF (l_num_conflicting_pids = 0) THEN
				RETURN scheduled_package.SIP_ID;
			END IF;

		END LOOP;
		-- If no suitable package is found or there aren't any scheduled packages available,
		-- return -1.
		RETURN -1;
	END;
/


commit;
exit;
