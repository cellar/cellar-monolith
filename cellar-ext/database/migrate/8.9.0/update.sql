##### Migration update sql scripts based on the modifications applied in database scrip files.
# in case a new file is created call the file explicitly using ./yourFIle.sql
# In case the change includes modifications in existing sql scripts then write the update Db scripts here so that they are used in migration.

-- Login with user sysdba and execute the statements mentioned below
----------------------------------------------------------------------
sqlplus "/as sysdba"

-- Grant CREATE MATERIALIZED VIEW privilege to cmrowner
-------------------------------------------------------------------------
GRANT CREATE MATERIALIZED VIEW TO cmrowner;
commit;
exit;


-- Login with user CMROWNER and execute the statements mentioned below
----------------------------------------------------------------------
sqlplus cmrowner/cmr06owner12

-- CLLR10815-772
-- Materialized join view built on top of NAL_SNIPPET and RELATED_SNIPPET tables, automatically refreshed every 5min.
CREATE MATERIALIZED VIEW LOG ON CMROWNER.NAL_SNIPPET WITH ROWID FOR FAST REFRESH;
CREATE MATERIALIZED VIEW LOG ON CMROWNER.RELATED_SNIPPET WITH ROWID FOR FAST REFRESH;

CREATE MATERIALIZED VIEW RELATED_NAL_SNIPPET
REFRESH FAST START WITH SYSDATE NEXT SYSDATE + 5/1440
AS
SELECT S1.RESOURCE_URI AS RESOURCE_URI,
    S1.CONCEPTSCHEME_URI AS CONCEPTSCHEME_URI,
    S1.RDFSNIPPET AS RDFSNIPPET, 
    S2.RESOURCE_URI AS RELATED_RESOURCE_URI,
    S2.CONCEPTSCHEME_URI AS RELATED_CONCEPTSCHEME_URI,
    S2.RDFSNIPPET AS RELATED_RDFSNIPPET,
    S1.ROWID AS SRC_ROWID, 
    R.ROWID AS REF_ROWID,
    S2.ROWID AS TGT_ROWID
    FROM NAL_SNIPPET S1, RELATED_SNIPPET R, NAL_SNIPPET S2
    WHERE S1.RESOURCE_URI = R.RESOURCE_URI (+) AND R.RELATED_RESOURCE_URI = S2.RESOURCE_URI (+);

CREATE INDEX CMR_RELATED_NAL_SNIPPET_RESOURCE_URI_IDX ON RELATED_NAL_SNIPPET(RESOURCE_URI);


-- CLLR10815-274
CREATE TABLE CMR_ITEM_TECHNICAL_MD
(
ID NUMBER(19,0) NOT NULL,
CELLAR_ID VARCHAR2(255) NOT NULL,
STREAM_NAME VARCHAR2(255),
STREAM_SIZE NUMBER(19,0),
STREAM_ORDER NUMBER(19,0),
CHECKSUM VARCHAR2(255),
CHECKSUM_ALGORITHM VARCHAR2(255),
LAST_MODIFICATION_DATE TIMESTAMP(6) NOT NULL,
CONSTRAINT CELLAR_ID_UNIQUE UNIQUE (CELLAR_ID)
);

CREATE SEQUENCE "CMR_ITEM_TECH_MD_SEQ" MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE ;

-- CLLR10815-495/CLLR10815-507/CLLR10815-508
-- Update on Cellar 8.10.0 release - The "NOT NULL" constrain contained in Cellar 8.9.0 release has been removed after issues on DB update
ALTER TABLE SPARQL_LOAD_REQUEST ADD CELLAR_ID VARCHAR(255);
ALTER TABLE MODEL_LOAD_REQUEST ADD CELLAR_ID VARCHAR(255);

---------------------------------------- Ticket CLLR10815-885 ----------------------------------------------------------
--------------------- Status API - Status implementation activities on Cellar Monolith ---------------------------------
-- Updates to CMR_INDEX_REQUEST table of CMROWNER schema (no foreign key constraint though)
ALTER TABLE CMR_INDEX_REQUEST ADD (structmap_status_history_id NUMBER(19, 0));


commit;
exit;



-- Login with user CELLAROWNER and execute the statements mentioned below
-------------------------------------------------------------------------
sqlplus cellarowner/cellar06owner12

-- EULOGIN
ALTER TABLE USERS MODIFY PASSWORD VARCHAR2(512 CHAR) NULL;
ALTER TABLE USERS ADD EMAIL VARCHAR2(384 CHAR); 
ALTER TABLE USERS ADD CONSTRAINT USERS_UNIQUE_EMAIL_CON UNIQUE (EMAIL);

CREATE TABLE USER_ACCESS_REQUEST
  ( 
     ID       NUMBER(19, 0) NOT NULL, 
     USERNAME VARCHAR2(255 CHAR) NOT NULL,
	 EMAIL    VARCHAR2(384 CHAR) NOT NULL,
     PRIMARY KEY (ID), 
     UNIQUE(USERNAME),
	 CONSTRAINT UAR_UNIQUE_EMAIL_CON UNIQUE (EMAIL)
  );

CREATE TABLE EMAIL
  (
     ID                 NUMBER(19,0) NOT NULL,
	 ADDRESS            VARCHAR2(384 BYTE) NOT NULL,
	 SUBJECT            VARCHAR2(512 BYTE),
	 CONTENT            VARCHAR2(4000 BYTE),
	 STATUS             VARCHAR2(1 BYTE) NOT NULL,
	 CREATED_ON         TIMESTAMP (6) NOT NULL,
	 SENT_ON            TIMESTAMP (6),
	 PRIMARY KEY (ID)
   );
  
DROP TABLE PERSISTENT_LOGINS;

-- Create new groups
INSERT INTO GROUPS(ID, GROUP_NAME) SELECT MAX(ID)+1, 'Test tool' FROM GROUPS;
commit;
INSERT INTO GROUPS(ID, GROUP_NAME) SELECT MAX(ID)+1, 'Archivist' FROM GROUPS;
commit;

-- Associate roles with the "Test-Tool" group
INSERT INTO GROUP_ROLES(GROUP_ID, ROLE_ID)
	SELECT
		(SELECT ID FROM GROUPS WHERE GROUP_NAME = 'Test tool') as GROUP_ID,
		(SELECT ID FROM ROLES WHERE ROLE_ACCESS = 'ROLE_DASHBOARD') as ROLE_ID
	FROM DUAL;
INSERT INTO GROUP_ROLES(GROUP_ID, ROLE_ID)
	SELECT
		(SELECT ID FROM GROUPS WHERE GROUP_NAME = 'Test tool') as GROUP_ID,
		(SELECT ID FROM ROLES WHERE ROLE_ACCESS = 'ROLE_SECAPI') as ROLE_ID
	FROM DUAL;
INSERT INTO GROUP_ROLES(GROUP_ID, ROLE_ID)
	SELECT
		(SELECT ID FROM GROUPS WHERE GROUP_NAME = 'Test tool') as GROUP_ID,
		(SELECT ID FROM ROLES WHERE ROLE_ACCESS = 'ROLE_CONTENT_SEARCH') as ROLE_ID
	FROM DUAL;
INSERT INTO GROUP_ROLES(GROUP_ID, ROLE_ID)
	SELECT
		(SELECT ID FROM GROUPS WHERE GROUP_NAME = 'Test tool') as GROUP_ID,
		(SELECT ID FROM ROLES WHERE ROLE_ACCESS = 'ROLE_VALIDATOR') as ROLE_ID
	FROM DUAL;
INSERT INTO GROUP_ROLES(GROUP_ID, ROLE_ID)
	SELECT
		(SELECT ID FROM GROUPS WHERE GROUP_NAME = 'Test tool') as GROUP_ID,
		(SELECT ID FROM ROLES WHERE ROLE_ACCESS = 'ROLE_HIERARCHY_ALIGNER') as ROLE_ID
	FROM DUAL;
INSERT INTO GROUP_ROLES(GROUP_ID, ROLE_ID)
	SELECT
		(SELECT ID FROM GROUPS WHERE GROUP_NAME = 'Test tool') as GROUP_ID,
		(SELECT ID FROM ROLES WHERE ROLE_ACCESS = 'ROLE_INDEXING') as ROLE_ID
	FROM DUAL;
INSERT INTO GROUP_ROLES(GROUP_ID, ROLE_ID)
	SELECT
		(SELECT ID FROM GROUPS WHERE GROUP_NAME = 'Test tool') as GROUP_ID,
		(SELECT ID FROM ROLES WHERE ROLE_ACCESS = 'ROLE_CONTENT_VISIBILITY') as ROLE_ID
	FROM DUAL;
INSERT INTO GROUP_ROLES(GROUP_ID, ROLE_ID)
	SELECT
		(SELECT ID FROM GROUPS WHERE GROUP_NAME = 'Test tool') as GROUP_ID,
		(SELECT ID FROM ROLES WHERE ROLE_ACCESS = 'ROLE_SPARQL_ENDPOINT') as ROLE_ID
	FROM DUAL;
commit;
-- Associate roles with the "Archivist" group
INSERT INTO GROUP_ROLES(GROUP_ID, ROLE_ID)
	SELECT
		(SELECT ID FROM GROUPS WHERE GROUP_NAME = 'Archivist') as GROUP_ID,
		(SELECT ID FROM ROLES WHERE ROLE_ACCESS = 'ROLE_SECAPI') as ROLE_ID
	FROM DUAL;
commit;

-- EULOGIN-PROD Test-Tool user.
INSERT INTO USERS(ID,USERNAME,ENABLED,GROUP_ID)
	SELECT
		(SELECT MAX(ID)+1 FROM USERS) as ID,
		'j9ba5g8',
		1,
		(SELECT ID FROM GROUPS WHERE GROUP_NAME = 'Test tool') as GROUP_ID
	FROM DUAL;
commit;
-- EULOGIN-PROD Archivist user.
INSERT INTO USERS(ID,USERNAME,ENABLED,GROUP_ID)
	SELECT
		(SELECT MAX(ID)+1 FROM USERS) as ID,
		'j9ba5gh',
		1,
		(SELECT ID FROM GROUPS WHERE GROUP_NAME = 'Archivist') as GROUP_ID
	FROM DUAL;
commit;
-- EULOGIN-ACC Test-Tool user.
INSERT INTO USERS(ID,USERNAME,ENABLED,GROUP_ID)
	SELECT
		(SELECT MAX(ID)+1 FROM USERS) as ID,
		'j905jbj',
		1,
		(SELECT ID FROM GROUPS WHERE GROUP_NAME = 'Test tool') as GROUP_ID
	FROM DUAL;
commit;
-- EULOGIN-ACC Archivist user.
INSERT INTO USERS(ID,USERNAME,ENABLED,GROUP_ID)
	SELECT
		(SELECT MAX(ID)+1 FROM USERS) as ID,
		'j905jbk',
		1,
		(SELECT ID FROM GROUPS WHERE GROUP_NAME = 'Archivist') as GROUP_ID
	FROM DUAL;
commit;


-- CLLR10815-421
ALTER TABLE INGESTION_HISTORY ADD FEED_ACTION_TYPE VARCHAR2(25);
UPDATE INGESTION_HISTORY SET FEED_ACTION_TYPE = ACTION_TYPE;
ALTER TABLE INGESTION_HISTORY MODIFY (FEED_ACTION_TYPE NOT NULL);
CREATE INDEX ingestion_history_fat_idx ON ingestion_history (feed_action_type) TABLESPACE cellarapp_index;


-- CLLR10815-495/CLLR10815-507/CLLR10815-508
ALTER TABLE NAL_HISTORY ADD CELLAR_ID VARCHAR(255);
ALTER TABLE NAL_HISTORY ADD IDENTIFIERS CLOB;
ALTER TABLE ONTOLOGY_HISTORY ADD CELLAR_ID VARCHAR(255);
ALTER TABLE ONTOLOGY_HISTORY ADD IDENTIFIERS CLOB;
ALTER TABLE SPARQL_LOAD_HISTORY ADD IDENTIFIERS CLOB;
ALTER TABLE SPARQL_LOAD_HISTORY ADD VERSION VARCHAR(255);


-- CLLR10815-250
ALTER TABLE INGESTION_HISTORY ADD RELATIVES NUMBER(1, 0);


-- CLLR10815-484
Insert into ROLES (ID,ROLE_NAME,ROLE_ACCESS) SELECT MAX(ID)+1, 'Delete', 'ROLE_MANAGE_VIRTUOSO_DATA' FROM ROLES;
commit;

INSERT INTO GROUP_ROLES(GROUP_ID, ROLE_ID)
SELECT
    (SELECT ID FROM GROUPS WHERE GROUP_NAME = 'Admin') as GROUP_ID,
    (SELECT ID FROM ROLES WHERE ROLE_ACCESS = 'ROLE_MANAGE_VIRTUOSO_DATA') as ROLE_ID
FROM DUAL;

INSERT INTO GROUP_ROLES(GROUP_ID, ROLE_ID)
SELECT
    (SELECT ID FROM GROUPS WHERE GROUP_NAME = 'Test tool') as GROUP_ID,
    (SELECT ID FROM ROLES WHERE ROLE_ACCESS = 'ROLE_MANAGE_VIRTUOSO_DATA') as ROLE_ID
FROM DUAL;

commit;

-- CLLR10815-741
Insert into ROLES (ID,ROLE_NAME,ROLE_ACCESS) SELECT MAX(ID)+1, 'Remote ingestion', 'ROLE_INGEST' FROM ROLES;
Insert into ROLES (ID,ROLE_NAME,ROLE_ACCESS) SELECT MAX(ID)+1, 'Status service','ROLE_STATUS_SERVICE' FROM ROLES;
commit;

INSERT INTO GROUP_ROLES(GROUP_ID, ROLE_ID)
SELECT
    (SELECT ID FROM GROUPS WHERE GROUP_NAME = 'Admin') as GROUP_ID,
    (SELECT ID FROM ROLES WHERE ROLE_ACCESS = 'ROLE_INGEST') as ROLE_ID
FROM DUAL;

INSERT INTO GROUP_ROLES(GROUP_ID, ROLE_ID)
SELECT
    (SELECT ID FROM GROUPS WHERE GROUP_NAME = 'Admin') as GROUP_ID,
    (SELECT ID FROM ROLES WHERE ROLE_ACCESS = 'ROLE_STATUS_SERVICE') as ROLE_ID
FROM DUAL;

INSERT INTO GROUP_ROLES(GROUP_ID, ROLE_ID)
SELECT
    (SELECT ID FROM GROUPS WHERE GROUP_NAME = 'Test tool') as GROUP_ID,
    (SELECT ID FROM ROLES WHERE ROLE_ACCESS = 'ROLE_INGEST') as ROLE_ID
FROM DUAL;

INSERT INTO GROUP_ROLES(GROUP_ID, ROLE_ID)
SELECT
    (SELECT ID FROM GROUPS WHERE GROUP_NAME = 'Test tool') as GROUP_ID,
    (SELECT ID FROM ROLES WHERE ROLE_ACCESS = 'ROLE_STATUS_SERVICE') as ROLE_ID
FROM DUAL;

commit;

---------------------------------------- Ticket CLLR10815-885 ----------------------------------------------------------
--------------------- Status API - Status implementation activities on Cellar Monolith ---------------------------------
-- Create PACKAGE_HISTORY table in CELLAROWNER schema
CREATE TABLE PACKAGE_HISTORY
(
    id                          NUMBER(19, 0) NOT NULL,
    package_uuid                VARCHAR2(255 CHAR) NOT NULL,
    received_date               TIMESTAMP(6) NOT NULL,
    callback_url                VARCHAR2(255 CHAR),
    mets_package_name           VARCHAR2(255 CHAR) NOT NULL,
    preprocess_ingestion_status VARCHAR2(5 BYTE) NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (package_uuid),
    CONSTRAINT preprocess_ingestion_status_chk CHECK (preprocess_ingestion_status IN ('NA', 'W', 'R', 'S', 'F')) ENABLE
);

-- Create STRUCTMAP_STATUS_HISTORY table in CELLAROWNER schema
CREATE TABLE STRUCTMAP_STATUS_HISTORY
(
    id                        NUMBER(19, 0) NOT NULL,
    structmap_name            VARCHAR2(255 CHAR),
    package_id                NUMBER(19, 0) NOT NULL,
    ingestion_status          VARCHAR2(5 BYTE) NOT NULL,
    cellar_id                 VARCHAR2(500 CHAR),
    indx_execution_status     VARCHAR2(5 BYTE),
    indx_execution_date       TIMESTAMP(6),
    indx_execution_start_date TIMESTAMP(6),
    indx_created_on           TIMESTAMP(6),
    languages                 VARCHAR2(255 CHAR),
    shacl_report              CLOB,
    PRIMARY KEY (id),
    CONSTRAINT ingestion_status_chk CHECK (ingestion_status IN ('NA', 'W', 'R', 'S', 'F')) ENABLE,
    CONSTRAINT indx_execution_status_chk CHECK (indx_execution_status IN ('N', 'P', 'X', 'D', 'E', 'R')) ENABLE,
    CONSTRAINT fk_package_history FOREIGN KEY (package_id) REFERENCES PACKAGE_HISTORY(id)
);

-- Create STRUCTMAP_PID table in CELLAROWNER schema
CREATE TABLE STRUCTMAP_PID
(
    id            NUMBER(19, 0) NOT NULL,
    structmap_id  NUMBER(19, 0) NOT NULL,
    production_id VARCHAR2(700 CHAR) NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_structmap_status_history FOREIGN KEY (structmap_id) REFERENCES STRUCTMAP_STATUS_HISTORY(id)
);

-- Updates to AUDIT_TRAIL_EVENT table of CELLAROWNER schema
ALTER TABLE AUDIT_TRAIL_EVENT ADD (structmap_id NUMBER(19, 0));
ALTER TABLE AUDIT_TRAIL_EVENT ADD CONSTRAINT fk_audit_to_structmap_status_history FOREIGN KEY (structmap_id) REFERENCES STRUCTMAP_STATUS_HISTORY(id);
ALTER TABLE AUDIT_TRAIL_EVENT ADD (package_id NUMBER(19, 0));
ALTER TABLE AUDIT_TRAIL_EVENT ADD CONSTRAINT fk_audit_to_package_history FOREIGN KEY (package_id) REFERENCES PACKAGE_HISTORY(id);

-- Updates to SIP_PACKAGE table of CELLAROWNER schema (no foreign key constraint though)
ALTER TABLE SIP_PACKAGE ADD (package_history_id NUMBER(19, 0));
ALTER TABLE SIP_PACKAGE ADD CONSTRAINT sip_pckg_hstr_id_unique UNIQUE (package_history_id);

-- Update to materialized view PACKAGE_QUEUE (i.e. "PACKAGE_HISTORY_ID" as extra argument)
CREATE OR REPLACE VIEW "PACKAGE_QUEUE" ("SIP_ID", "NAME", "TYPE", "DETECTION_DATE", "PACKAGE_HISTORY_ID") AS
SELECT sip.ID, sip.NAME, sip.TYPE, sip.DETECTION_DATE, sip.PACKAGE_HISTORY_ID
FROM SIP_PACKAGE sip
WHERE sip.ID = id_next_package_ingestion_ready
    WITH READ ONLY;

commit;

-- CLLR10815-915
-- Returns the production identifiers and whether these are read-only or not, for the packages IDs provided as argument
CREATE OR REPLACE FUNCTION get_extended_relation_prod_ids (sip_ids_in IN t_sip_id_array) RETURN t_extended_relation_prod_ids AUTHID CURRENT_USER IS
    l_extended_relation_prod_ids_result t_extended_relation_prod_ids := t_extended_relation_prod_ids();
	BEGIN
        -- Retrieve the non-normalized root/children and direct relation PIDs
        -- only for the packages whose IDs are provided as argument.
        WITH ROOT_CHILDREN_DIRECT_NON_NORMALIZED AS (
            SELECT r1.SIP_ID, r1.IS_DIRECT_RELATION_ONLY, r1.PRODUCTION_ID
            FROM RELATION_PROD_ID r1
                INNER JOIN SIP_PACKAGE sp1 ON sp1.ID = r1.SIP_ID
            WHERE sp1.ID IN (SELECT * FROM TABLE(sip_ids_in))
        ),
        -- Retrieve the root/children and direct relation PIDs (normalized and non-normalized).
	    ROOT_CHILDREN_DIRECT AS (
            -- Retrieve the previously computed non-normalized root/children and direct relations.
            SELECT *
            FROM ROOT_CHILDREN_DIRECT_NON_NORMALIZED rcdnn1
            UNION ALL
            -- Append the normalized CELLAR IDs, if any.
            SELECT rcdnn2.SIP_ID, rcdnn2.IS_DIRECT_RELATION_ONLY,
                -- Convert CELLAR-ID to base-CELLAR-ID.
                CASE
                    WHEN INSTR(c1.UUID, '.') = 0 THEN c1.UUID
                    ELSE SUBSTR(c1.UUID, 0, INSTR(c1.UUID, '.') - 1)
                END AS PRODUCTION_ID
            FROM ROOT_CHILDREN_DIRECT_NON_NORMALIZED rcdnn2
                INNER JOIN PRODUCTION_IDENTIFIER p1 ON p1.PRODUCTION_ID = rcdnn2.PRODUCTION_ID
                INNER JOIN CELLAR_IDENTIFIER c1 ON c1.ID = p1.CELLAR_IDENTIFIER_ID
        ),
        -- Retrieve the non-normalized inverse relations.
        INVERSE_NON_NORMALIZED AS (
            -- Retrieve the non-normalized inverse-on-source relations.
            SELECT rcd.SIP_ID,
                -- Convert CELLAR-ID to base-CELLAR-ID, if applicable.
                CASE
                    WHEN INSTR(inv1.TARGET_ID, 'cellar:') = 0 THEN inv1.TARGET_ID
                    WHEN INSTR(inv1.TARGET_ID, '.') != 0 THEN SUBSTR(inv1.TARGET_ID, 0, INSTR(inv1.TARGET_ID, '.') - 1)
                    ELSE inv1.TARGET_ID
                END AS PRODUCTION_ID
            FROM CMROWNER.CMR_INVERSE_RELATIONS inv1
                INNER JOIN ROOT_CHILDREN_DIRECT rcd ON rcd.PRODUCTION_ID = inv1.SOURCE_ID
            WHERE rcd.IS_DIRECT_RELATION_ONLY = 'N'
            UNION ALL
            -- Retrieve the non-normalized inverse-on-target relations.
            SELECT rcd.SIP_ID,
                -- Convert CELLAR-ID to base-CELLAR-ID, if applicable.
                CASE
                    WHEN INSTR(inv2.SOURCE_ID, 'cellar:') = 0 THEN inv2.SOURCE_ID
                    WHEN INSTR(inv2.SOURCE_ID, '.') != 0 THEN SUBSTR(inv2.SOURCE_ID, 0, INSTR(inv2.SOURCE_ID, '.') - 1)
                    ELSE inv2.SOURCE_ID
                END AS PRODUCTION_ID
            FROM CMROWNER.CMR_INVERSE_RELATIONS inv2
                INNER JOIN ROOT_CHILDREN_DIRECT rcd ON rcd.PRODUCTION_ID = inv2.TARGET_ID
            WHERE rcd.IS_DIRECT_RELATION_ONLY = 'N'
        ),
        -- Retrieve the normalized inverse relations.
        INVERSE_NORMALIZED AS (
            SELECT inn2.SIP_ID,
                -- Convert CELLAR-ID to base-CELLAR-ID.
                CASE
                    WHEN INSTR(c2.UUID, '.') = 0 THEN c2.UUID
                    ELSE SUBSTR(c2.UUID, 0, INSTR(c2.UUID, '.') - 1)
                END AS PRODUCTION_ID
            FROM INVERSE_NON_NORMALIZED inn2
                INNER JOIN PRODUCTION_IDENTIFIER p2 ON p2.PRODUCTION_ID = inn2.PRODUCTION_ID
                INNER JOIN CELLAR_IDENTIFIER c2 ON c2.ID = p2.CELLAR_IDENTIFIER_ID
        )
	    -- Stores and returns all applicable production identifiers along with their read-only status.
	    -- A production-id is read-only if it is a relation and an actual PID (not a cellar-identifier).
	    SELECT t_extended_relation_prod_id_row(PRODUCTION_ID, IS_READ_ONLY)
	    BULK COLLECT INTO l_extended_relation_prod_ids_result
	    FROM (
	     	-- The non-normalized inverse relations.
		    SELECT inn1.PRODUCTION_ID,
		    	CASE
		    		WHEN INSTR(inn1.PRODUCTION_ID, 'cellar:') = 0 THEN 'Y'
		    		ELSE 'N'
		    	END AS IS_READ_ONLY
		    FROM INVERSE_NON_NORMALIZED inn1
		    UNION ALL
		    -- Append the normalized inverse relations, if any.
		    SELECT in1.PRODUCTION_ID,
		    	CASE
		    		WHEN INSTR(in1.PRODUCTION_ID, 'cellar:') = 0 THEN 'Y'
		    		ELSE 'N'
		    	END AS IS_READ_ONLY
		    FROM INVERSE_NORMALIZED in1
		    UNION ALL
		    -- Append the normalized and non-normalized root/children and direct relation PIDs.
		    SELECT rcd2.PRODUCTION_ID,
		    	CASE WHEN rcd2.IS_DIRECT_RELATION_ONLY = 'Y' AND INSTR(rcd2.PRODUCTION_ID, 'cellar:') = 0 THEN 'Y'
		    	ELSE 'N'
		    	END AS IS_READ_ONLY
		    FROM ROOT_CHILDREN_DIRECT rcd2
	    );
	    RETURN l_extended_relation_prod_ids_result;
	END;
/

-- CLLR10815-915
-- Returns the ID of the first (in terms of ordering/priority) package (SIP_PACKAGE.ID) that can be ingested
-- in a conflict-free manner with regards to any packages being ingested at that point.
CREATE OR REPLACE FUNCTION id_next_package_ingestion_ready RETURN NUMBER AUTHID CURRENT_USER IS
	-- The number of conflicting production identifiers between the packages being ingested
	-- and the scheduled package under consideration.
	l_num_conflicting_pids NUMBER;

	-- The production identifiers of the packages being ingested.
	l_ingesting_extended_relation_prod_ids t_extended_relation_prod_ids := t_extended_relation_prod_ids();

	-- The production identifiers of the scheduled package being considered for ingestion.
	l_scheduled_pkg_extended_relation_prod_ids t_extended_relation_prod_ids := t_extended_relation_prod_ids();

	-- The IDs of the packages being ingested.
	l_ingesting_sip_ids t_sip_id_array := t_sip_id_array();

	-- The ID of the package considered for ingetion.
    l_scheduled_sip_id t_sip_id_array := t_sip_id_array();

    -- The STATUS of the package considered for ingestion.
    l_scheduled_sip_status VARCHAR2(10);


    -- The ordered/prioritized list of package references (contains references with status 'SCHEDULED'/'INGESTING'/'FINISHED').
	CURSOR c_scheduled_packages_list IS
		SELECT *
		FROM PACKAGE_LIST pl
		ORDER BY pl.ORDERING;

	BEGIN

		-- Retrieves the IDs (SIP_PACKAGE.ID) of the packages being ingested.
		SELECT sp.ID
		BULK COLLECT INTO l_ingesting_sip_ids
		FROM SIP_PACKAGE sp
		WHERE sp.STATUS = 'I';

		-- Retrieves the production identifiers of the packages being ingested.
		SELECT t_extended_relation_prod_id_row(PRODUCTION_ID, IS_READ_ONLY)
		BULK COLLECT INTO l_ingesting_extended_relation_prod_ids
		FROM TABLE(get_extended_relation_prod_ids(l_ingesting_sip_ids));

		-- Iterates over the ordered/prioritized list of scheduled packages IDs.
		FOR scheduled_package IN c_scheduled_packages_list LOOP

            -- Stores the STATUS of the currently evaluated package reference.
            SELECT sp.STATUS
            COLLECT INTO l_scheduled_sip_status
            FROM SIP_PACKAGE sp
            WHERE sp.ID = scheduled_package.SIP_ID;

            -- If the currently evaluated package reference is not 'SCHEDULED',
            -- then proceed to evaluate the next candidate package reference.
            IF (l_scheduled_sip_status != 'S') THEN
                CONTINUE;
            END IF;

			-- Stores the package ID of the current scheduled package.
            SELECT scheduled_package.SIP_ID
            BULK COLLECT INTO l_scheduled_sip_id
            FROM DUAL;

            -- Retrieves the production identifiers of the scheduled package being
            -- currently considered for ingestion.
			SELECT t_extended_relation_prod_id_row(PRODUCTION_ID, IS_READ_ONLY)
			BULK COLLECT INTO l_scheduled_pkg_extended_relation_prod_ids
			FROM TABLE(get_extended_relation_prod_ids(l_scheduled_sip_id));

			-- Checks whether there are any conflicts between the production identifiers of the packages being ingested
			-- and the scheduled package being currently considered.
			SELECT COUNT(1)
			INTO l_num_conflicting_pids
			FROM TABLE(l_ingesting_extended_relation_prod_ids) ingesting
				INNER JOIN TABLE(l_scheduled_pkg_extended_relation_prod_ids) scheduled ON ingesting.PRODUCTION_ID = scheduled.PRODUCTION_ID
					AND NOT (ingesting.IS_READ_ONLY = scheduled.IS_READ_ONLY AND ingesting.IS_READ_ONLY = 'Y');

			-- If no conflicts are detected, then this package can be submitted for ingestion,
			-- and its ID is returned.
			IF (l_num_conflicting_pids = 0) THEN
				RETURN scheduled_package.SIP_ID;
			END IF;

		END LOOP;
		-- If no suitable package is found or there aren't any scheduled packages available,
		-- return -1.
		RETURN -1;
	END;
/


commit;
exit;