-- ##### Migration update sql scripts based on the modifications applied in database script files for Cellar 8.10.1 release.
--    # in case a new file is created call the file explicitly using ./yourFIle.sql
--    # In case the change includes modifications in existing sql scripts then write the update Db scripts here so that they are used in migration.


-- CLLR10815-1142
------------------------------------------------------------------------
-------------------------------------------------------------------------
-- Login with user sysdba and execute the statements mentioned below
-- sqlplus "/as sysdba"

-- Revoke all system-privileges from CELLAROWNER, CMROWNER, CELLARLEX
REVOKE ALL PRIVILEGES FROM CELLAROWNER;
REVOKE ALL PRIVILEGES FROM CMROWNER;
REVOKE ALL PRIVILEGES FROM CELLARLEX;

-- Grant minimum privileges to CELLAROWNER
GRANT CREATE SESSION to CELLAROWNER;
GRANT UNLIMITED TABLESPACE TO CELLAROWNER;
GRANT CREATE TABLE TO CELLAROWNER;
GRANT CREATE VIEW TO CELLAROWNER;
GRANT CREATE PROCEDURE TO CELLAROWNER;
GRANT CREATE TRIGGER TO CELLAROWNER;
GRANT CREATE TYPE TO CELLAROWNER;
GRANT CREATE SEQUENCE TO CELLAROWNER;

-- Grant minimum privileges to CMROWNER
GRANT CREATE SESSION to CMROWNER;
GRANT UNLIMITED TABLESPACE TO CMROWNER;
GRANT CREATE TABLE TO CMROWNER;
GRANT CREATE MATERIALIZED VIEW TO CMROWNER;
GRANT CREATE PROCEDURE TO CMROWNER;
GRANT CREATE TRIGGER TO CMROWNER;
GRANT CREATE SEQUENCE TO CMROWNER;

-- Grant minimum privileges to CELLARLEX
GRANT CREATE SESSION to CELLARLEX;
GRANT UNLIMITED TABLESPACE TO CELLARLEX;
GRANT CREATE TABLE TO CELLARLEX;
GRANT CREATE TRIGGER TO CELLARLEX;
GRANT CREATE SEQUENCE TO CELLARLEX;

-------------------------------------------------------------------------
-- -- Login with user CMROWNER and execute the statements mentioned bellow
-- sqlplus cmrowner/cmr06owner12

GRANT SELECT ON CMROWNER.CMR_INDEX_REQUEST TO CELLAROWNER;
GRANT SELECT ON CMROWNER.CMR_INVERSE_RELATIONS TO CELLAROWNER;
-------------------------------------------------------------------------
-------------------------------------------------------------------------
