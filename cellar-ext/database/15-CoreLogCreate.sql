-- Log table

-- create tables
CREATE TABLE CELLAROWNER.AUDIT_TRAIL_EVENT
(
  ID                 NUMBER(19)                 NOT NULL,
  PROCESS            VARCHAR2(31 CHAR)          NOT NULL,
  ACTION             VARCHAR2(255 CHAR)         NOT NULL,
  TYPE               VARCHAR2(5 CHAR)           NOT NULL,
  OBJECT_IDENTIFIER  VARCHAR2(700 CHAR)         NOT NULL,
  EVENT_DATE         TIMESTAMP(6)               NOT NULL,
  MESSAGE            VARCHAR2(4000 CHAR),
  THREAD_NAME        VARCHAR2(255 CHAR),
  CONSTRAINT "AUDIT_TRAIL_EVENT_TYPE_CHK" CHECK (TYPE IN ('Start', 'End', 'Fail')) ENABLE
)
COMPRESS FOR OLTP
TABLESPACE CELLAR_AUDITRAIL_DATA
RESULT_CACHE (MODE DEFAULT)
PCTUSED    0
PCTFREE    0
INITRANS   10
MAXTRANS   255
STORAGE    (
            BUFFER_POOL      DEFAULT
            FLASH_CACHE      DEFAULT
            CELL_FLASH_CACHE DEFAULT
           )
LOGGING
PARTITION BY RANGE (EVENT_DATE)
(
  PARTITION ATE_Y2015_M1 VALUES LESS THAN (TIMESTAMP' 2015-02-01 00:00:00')
    LOGGING
    COMPRESS FOR OLTP
    TABLESPACE CELLAR_AUDITRAIL_DATA
    PCTFREE    0
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),
  PARTITION ATE_Y2015_M2 VALUES LESS THAN (TIMESTAMP' 2015-03-01 00:00:00')
    LOGGING
    COMPRESS FOR OLTP
    TABLESPACE CELLAR_AUDITRAIL_DATA
    PCTFREE    0
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),
  PARTITION ATE_Y2015_M3 VALUES LESS THAN (TIMESTAMP' 2015-04-01 00:00:00')
    LOGGING
    COMPRESS FOR OLTP
    TABLESPACE CELLAR_AUDITRAIL_DATA
    PCTFREE    0
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),
  PARTITION ATE_Y2015_M4 VALUES LESS THAN (TIMESTAMP' 2015-05-01 00:00:00')
    LOGGING
    COMPRESS FOR OLTP
    TABLESPACE CELLAR_AUDITRAIL_DATA
    PCTFREE    0
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),
  PARTITION ATE_Y2015_M5 VALUES LESS THAN (TIMESTAMP' 2015-06-01 00:00:00')
    LOGGING
    COMPRESS FOR OLTP
    TABLESPACE CELLAR_AUDITRAIL_DATA
    PCTFREE    0
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),
  PARTITION ATE_Y2015_M6 VALUES LESS THAN (TIMESTAMP' 2015-07-01 00:00:00')
    LOGGING
    COMPRESS FOR OLTP
    TABLESPACE CELLAR_AUDITRAIL_DATA
    PCTFREE    0
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),
  PARTITION ATE_Y2015_M7 VALUES LESS THAN (TIMESTAMP' 2015-08-01 00:00:00')
    LOGGING
    COMPRESS FOR OLTP
    TABLESPACE CELLAR_AUDITRAIL_DATA
    PCTFREE    0
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),
  PARTITION ATE_Y2015_M8 VALUES LESS THAN (TIMESTAMP' 2015-09-01 00:00:00')
    LOGGING
    COMPRESS FOR OLTP
    TABLESPACE CELLAR_AUDITRAIL_DATA
    PCTFREE    0
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),
  PARTITION ATE_Y2015_M9 VALUES LESS THAN (TIMESTAMP' 2015-10-01 00:00:00')
    LOGGING
    COMPRESS FOR OLTP
    TABLESPACE CELLAR_AUDITRAIL_DATA
    PCTFREE    0
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),
  PARTITION ATE_Y2015_M10 VALUES LESS THAN (TIMESTAMP' 2015-11-01 00:00:00')
    LOGGING
    COMPRESS FOR OLTP
    TABLESPACE CELLAR_AUDITRAIL_DATA
    PCTFREE    0
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),
  PARTITION ATE_Y2015_M11 VALUES LESS THAN (TIMESTAMP' 2015-12-01 00:00:00')
    LOGGING
    COMPRESS FOR OLTP
    TABLESPACE CELLAR_AUDITRAIL_DATA
    PCTFREE    0
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),
  PARTITION ATE_Y2015_M12 VALUES LESS THAN (TIMESTAMP' 2016-01-01 00:00:00')
    LOGGING
    COMPRESS FOR OLTP
    TABLESPACE CELLAR_AUDITRAIL_DATA
    PCTFREE    0
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),
  PARTITION ATE_Y2016_M1 VALUES LESS THAN (TIMESTAMP' 2016-02-01 00:00:00')
    LOGGING
    COMPRESS FOR OLTP
    TABLESPACE CELLAR_AUDITRAIL_DATA
    PCTFREE    0
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),
  PARTITION ATE_Y2016_M2 VALUES LESS THAN (TIMESTAMP' 2016-03-01 00:00:00')
    LOGGING
    COMPRESS FOR OLTP
    TABLESPACE CELLAR_AUDITRAIL_DATA
    PCTFREE    0
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),
  PARTITION ATE_Y2016_M3 VALUES LESS THAN (TIMESTAMP' 2016-04-01 00:00:00')
    LOGGING
    COMPRESS FOR OLTP
    TABLESPACE CELLAR_AUDITRAIL_DATA
    PCTFREE    0
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),
  PARTITION ATE_Y2016_M4 VALUES LESS THAN (TIMESTAMP' 2016-05-01 00:00:00')
    LOGGING
    COMPRESS FOR OLTP
    TABLESPACE CELLAR_AUDITRAIL_DATA
    PCTFREE    0
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),
  PARTITION ATE_Y2016_M5 VALUES LESS THAN (TIMESTAMP' 2016-06-01 00:00:00')
    LOGGING
    COMPRESS FOR OLTP
    TABLESPACE CELLAR_AUDITRAIL_DATA
    PCTFREE    0
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),
  PARTITION ATE_Y2016_M6 VALUES LESS THAN (TIMESTAMP' 2016-07-01 00:00:00')
    LOGGING
    COMPRESS FOR OLTP
    TABLESPACE CELLAR_AUDITRAIL_DATA
    PCTFREE    0
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),
  PARTITION ATE_Y2016_M7 VALUES LESS THAN (TIMESTAMP' 2016-08-01 00:00:00')
    LOGGING
    COMPRESS FOR OLTP
    TABLESPACE CELLAR_AUDITRAIL_DATA
    PCTFREE    0
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),
  PARTITION ATE_Y2016_M8 VALUES LESS THAN (TIMESTAMP' 2016-09-01 00:00:00')
    LOGGING
    COMPRESS FOR OLTP
    TABLESPACE CELLAR_AUDITRAIL_DATA
    PCTFREE    0
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),
  PARTITION ATE_Y2016_M9 VALUES LESS THAN (TIMESTAMP' 2016-10-01 00:00:00')
    LOGGING
    COMPRESS FOR OLTP
    TABLESPACE CELLAR_AUDITRAIL_DATA
    PCTFREE    0
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),
  PARTITION ATE_Y2016_M10 VALUES LESS THAN (TIMESTAMP' 2016-11-01 00:00:00')
    LOGGING
    COMPRESS FOR OLTP
    TABLESPACE CELLAR_AUDITRAIL_DATA
    PCTFREE    0
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),
  PARTITION ATE_Y2016_M11 VALUES LESS THAN (TIMESTAMP' 2016-12-01 00:00:00')
    LOGGING
    COMPRESS FOR OLTP
    TABLESPACE CELLAR_AUDITRAIL_DATA
    PCTFREE    0
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),
  PARTITION ATE_Y2016_M12 VALUES LESS THAN (TIMESTAMP' 2017-01-01 00:00:00')
    LOGGING
    COMPRESS FOR OLTP
    TABLESPACE CELLAR_AUDITRAIL_DATA
    PCTFREE    0
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               ),
  PARTITION YMAX VALUES LESS THAN (MAXVALUE)
    LOGGING
    COMPRESS FOR OLTP
    TABLESPACE CELLAR_AUDITRAIL_DATA
    PCTFREE    0
    INITRANS   1
    MAXTRANS   255
    STORAGE    (
                BUFFER_POOL      DEFAULT
                FLASH_CACHE      DEFAULT
                CELL_FLASH_CACHE DEFAULT
               )
)
NOCACHE
NOPARALLEL
MONITORING;

-- create index
CREATE INDEX CELLAROWNER.SPEEDUP_ATE_EVT_DATE ON CELLAROWNER.AUDIT_TRAIL_EVENT
(EVENT_DATE)
  TABLESPACE CELLAR_AUDITRAIL_INDEX
  PCTFREE    0
  INITRANS   2
  MAXTRANS   255
  STORAGE    (
              BUFFER_POOL      DEFAULT
              FLASH_CACHE      DEFAULT
              CELL_FLASH_CACHE DEFAULT
             )
LOCAL
COMPRESS;

CREATE INDEX CELLAROWNER.SPEEDUP_ATE_EVT_1 ON CELLAROWNER.AUDIT_TRAIL_EVENT
(TYPE, ID)
  TABLESPACE CELLAR_AUDITRAIL_INDEX
  PCTFREE    0
  INITRANS   2
  MAXTRANS   255
  STORAGE    (
              BUFFER_POOL      DEFAULT
              FLASH_CACHE      DEFAULT
              CELL_FLASH_CACHE DEFAULT
             )
LOCAL
COMPRESS 1;

CREATE INDEX CELLAROWNER.SPEEDUP_AUDTE_I_1 ON CELLAROWNER.AUDIT_TRAIL_EVENT
(OBJECT_IDENTIFIER)
  TABLESPACE CELLAR_AUDITRAIL_INDEX
  PCTFREE    0
  INITRANS   2
  MAXTRANS   255
  STORAGE    (
              BUFFER_POOL      DEFAULT
              FLASH_CACHE      DEFAULT
              CELL_FLASH_CACHE DEFAULT
             )
LOCAL
COMPRESS 1;

CREATE UNIQUE INDEX CELLAROWNER.PK_ID ON CELLAROWNER.AUDIT_TRAIL_EVENT
(ID)
  TABLESPACE CELLAR_AUDITRAIL_INDEX
  PCTFREE    0
  INITRANS   10
  MAXTRANS   255
  STORAGE    (
              BUFFER_POOL      DEFAULT
              FLASH_CACHE      DEFAULT
              CELL_FLASH_CACHE DEFAULT
             )
LOGGING
REVERSE;

ALTER TABLE CELLAROWNER.AUDIT_TRAIL_EVENT ADD CONSTRAINT PK_ID PRIMARY KEY(ID) using index PK_ID ENABLE VALIDATE;

CREATE INDEX I_AUDIT_TRAIL_EVENT ON CELLAROWNER.AUDIT_TRAIL_EVENT(THREAD_NAME) TABLESPACE CELLARAPP_INDEX;
