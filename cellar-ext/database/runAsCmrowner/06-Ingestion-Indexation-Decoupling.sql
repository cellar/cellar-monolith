CREATE TABLE "CMR_INDEXATION_CALC_EMBEDDED_NOTICE"
(
    "ID" NUMBER (19,0) NOT NULL,
    "CELLAR_ID" VARCHAR2 (500 BYTE) NOT NULL,
    "CELLAR_BASE_ID" VARCHAR2 (500 BYTE) NOT NULL,
    "EMBEDDED_NOTICE" CLOB,
    "EMBEDDED_NOTICE_CREATION_DATE" TIMESTAMP(6),
    CONSTRAINT "CMR_ICEN_PK" PRIMARY KEY ("ID")
);

CREATE SEQUENCE "CMR_INDEXATION_CALC_EMBEDDED_NOTICE_SEQ" MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER NOCYCLE ;
