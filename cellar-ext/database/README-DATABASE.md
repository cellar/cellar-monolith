# 🎛️Database of Cellar Monolith
The Cellar Monolith utilizes an Oracle database to store and manage data. Here you will find useful instructions.

## Table Of Contents
- [🛠️Create Cellar Database From Scratch](#create-cellar-database-from-scratch)
  - [⚠️IMPORTANT NOTE for Local PDBs](#important-note-for-local-pdbs)
- [✈️Migrating Between Cellar Versions](#migrating-between-cellar-versions)
- [📜Useful SQL Commands](#useful-sql-commands)
    - [Show all system-privileges](#show-all-system-privileges)
    - [Show all object-privileges](#show-all-object-privileges)
    - [Show all role-privileges](#show-all-role-privileges)
    - [Show all tablespaces](#show-all-tablespaces)
    - [Drop a specific tablespace](#drop-a-specific-tablespace)
    - [Show all users that have unlimited quota on a tablespace](#show-all-users-that-have-unlimited-quota-on-a-tablespace)
    - [Verify RDF store installation](#verify-rdf-store-installation)
    - [Change password and/or unlock account](#change-password-andor-unlock-account)
    - [Create semantic networks](#create-semantic-networks)
    - [Drop semantic networks](#drop-semantic-networks)


## 🛠️Create Cellar Database From Scratch
* 📌http://redmine.eurodyn.com/projects/cellar/wiki/Cellar_database
* 📌https://eurodyn.atlassian.net/browse/OUT-653

Order of script execution:
* As `SYSTEM` execute the following scripts:
  ```
  runAsSystem/01-Create-Cellar-Tablespace.sql    
  runAsSystem/02-Create-Cmr-Tablespace.sql
  runAsSystem/03-Create-oracle-spatial-network.sql
  ```
* As `SYS` execute the following scripts:
  ```
  runAsSys/01-Grant-execute-on-DBMS_LOCK.sql
  ```
* As `SYSTEM` execute the following scripts:
  ```
  runAsSystem/04-Create-AutonomyIdol-Tablespace.sql
  runAsSystem/05-Create-Log-Tablespace.sql
  ```
* As `CELLAROWNER` execute the following scripts:
  ```
  runAsCellarowner/01-CoreCellarCreate.sql
  runAsCellarowner/02-UsersGroupsRoles.sql
  runAsCellarowner/03-VirtuosoExceptionMessages.sql
  runAsCellarowner/04-LobIndexCoreCellar.sql
  ```
* As `CMROWNER` execute the following scripts:
  ```
  runAsCmrowner/01-OracleRdfCreateTables.sql  
  runAsCmrowner/02-Language_Config.sql   
  runAsCmrowner/03-NalRequest-ModelLoad.sql  
  runAsCmrowner/04-NALCreateTables.sql
  runAsCmrowner/05-ConfigTables.sql
  ```
* As `CELLARLEX` execute the following scripts:
  ```
  runAsCellarlex/01-OracleIdolCreateTable.sql
  ```
* As `CELLAROWNER` execute the following scripts:
  ```
  runAsCellarowner/05-CoreLogCreate.sql
  runAsCellarowner/06-SIPDependencyAndPrioritizationCreateTables.sql
  ```
* As `CMROWNER` execute the following scripts:
  ```
  runAsCmrowner/06-Ingestion-Indexation-Decoupling.sql
  ```

### ⚠️IMPORTANT NOTE for Local PDBs
For local databases create the datafile under `/local122/localX` directory (e.g. for local1 PDB use `/u01/app/oracle/oradata/local122/local1/DATA/statusappdata.dat`)

An example follows bellow.

From:
```oracle
-- From:
CREATE TABLESPACE CELLARAPP_DATA
DATAFILE '/u01/app/oracle/oradata/EE/cellarappdata.dat' SIZE 8M REUSE
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
SEGMENT SPACE MANAGEMENT AUTO;

-- To:
CREATE TABLESPACE CELLARAPP_DATA
DATAFILE '/u01/app/oracle/oradata/local122/local1/DATA/cellarappdata.dat' SIZE 8M REUSE
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
SEGMENT SPACE MANAGEMENT AUTO;
```

_Use any text editor like `**Notepad++** to easily do a multi-replacement with 1 click._

## ✈️Migrating Between Cellar Versions
To migrate to a next or a previous version in Cellar, database migration is almost always needed.
You can find the migration scripts under `migrate/` directory for each Cellar version.
Use the `update.sql` and `revert.sql` scripts accordingly.

## 📜Useful SQL Commands

### Show all system-privileges
```oracle
SELECT * FROM DBA_SYS_PRIVS WHERE GRANTEE='CELLAROWNER';
SELECT * FROM DBA_SYS_PRIVS WHERE GRANTEE='CMROWNER';
SELECT * FROM DBA_SYS_PRIVS WHERE GRANTEE='CELLARLEX';
```

### Show all object-privileges
```oracle
SELECT * FROM DBA_TAB_PRIVS WHERE GRANTEE='CELLAROWNER';
SELECT * FROM DBA_TAB_PRIVS WHERE GRANTEE='CMROWNER';
SELECT * FROM DBA_TAB_PRIVS WHERE GRANTEE='CELLARLEX';
```

### Show all role-privileges
```oracle
SELECT * FROM DBA_TAB_PRIVS WHERE GRANTEE='CELLAROWNER';
SELECT * FROM DBA_TAB_PRIVS WHERE GRANTEE='CELLAROWNER';
SELECT * FROM DBA_TAB_PRIVS WHERE GRANTEE='CELLARLEX';
```

### Show all tablespaces
```oracle
SELECT TABLESPACE_NAME, STATUS, CONTENTS FROM USER_TABLESPACES;
```

### Drop a specific tablespace
```oracle
DROP TABLESPACE CELLAR_ALIGNER_INDEX_01 INCLUDING CONTENTS CASCADE CONSTRAINTS;
```

### Show all users that have unlimited quota on a tablespace
```oracle
--If the MAX_BYTES values is -1, that user has unlimited quota on the corresponding tablespace
SELECT * FROM DBA_TS_QUOTAS;
```

### Verify RDF store installation
```oracle
SELECT * FROM MDSYS.RDF_PARAMETER WHERE ATTRIBUTE = 'SEM_VERSION';
```

### Change password and/or unlock account
```oracle
-- change password of an account
ALTER USER cellarowner IDENTIFIED BY cellar06owner12;
ALTER USER cmrowner IDENTIFIED BY cmr06owner12;
ALTER USER cellarlex IDENTIFIED BY cellar06lex12;

-- unlock an account
ALTER USER cellarowner ACCOUNT UNLOCK;
ALTER USER cmrowner ACCOUNT UNLOCK;
ALTER USER cellarlex ACCOUNT UNLOCK;
```

### Create semantic networks
```oracle
EXECUTE SEM_APIS.CREATE_SEM_MODEL('CMR_METADATA','CMR_METADATA','TRIPLE');
EXECUTE SEM_APIS.CREATE_SEM_MODEL('CMR_INVERSE','CMR_INVERSE','TRIPLE');
EXECUTE SEM_APIS.CREATE_SEM_MODEL('CMR_METADATA_EMBARGO','CMR_METADATA_EMBARGO','TRIPLE');
EXECUTE SEM_APIS.CREATE_SEM_MODEL('CMR_INVERSE_EMBARGO','CMR_INVERSE_EMBARGO','TRIPLE');
EXECUTE SEM_APIS.CREATE_SEM_MODEL('CMR_BLANK_NODE','CMR_BLANK_NODE','TRIPLE');
```

### Drop semantic networks
```oracle
EXECUTE SEM_APIS.DROP_SEM_MODEL('CMR_METADATA');
EXECUTE SEM_APIS.DROP_SEM_MODEL('CMR_INVERSE');
EXECUTE SEM_APIS.DROP_SEM_MODEL('CMR_METADATA_EMBARGO');
EXECUTE SEM_APIS.DROP_SEM_MODEL('CMR_INVERSE_EMBARGO');
EXECUTE SEM_APIS.DROP_SEM_MODEL('CMR_BLANK_NODE');
```
