-- Tablespace for AutonomyIdol

create tablespace IDOL_DATA
datafile '/u01/app/oracle/oradata/EE/idol/idoldata.dat' SIZE 8M REUSE
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
SEGMENT SPACE MANAGEMENT AUTO;

create tablespace IDOL_INDEX
datafile '/u01/app/oracle/oradata/EE/idol/idolindex.dat' SIZE 8M REUSE
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
SEGMENT SPACE MANAGEMENT AUTO;

create tablespace TS_LOBS_BG
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_lobs_BG_01.dbf' size 8M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 4m; 

create tablespace TS_BG
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_BG_01.dbf' size 16M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 1m;

create tablespace TS_LOBS_CS
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_lobs_CS_01.dbf' size 8M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 4m; 

create tablespace TS_CS
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_CS_01.dbf' size 16M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 1m;

create tablespace TS_LOBS_DA
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_lobs_DA_01.dbf' size 8M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 4m; 

create tablespace TS_DA
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_DA_01.dbf' size 16M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 1m;

create tablespace TS_LOBS_DE
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_lobs_DE_01.dbf' size 8M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 4m; 

create tablespace TS_DE
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_DE_01.dbf' size 16M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 1m;

create tablespace TS_LOBS_EL
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_lobs_EL_01.dbf' size 8M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 4m; 

create tablespace TS_EL
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_EL_01.dbf' size 16M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 1m;

create tablespace TS_LOBS_EN
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_lobs_EN_01.dbf' size 8M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 4m; 

create tablespace TS_EN
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_EN_01.dbf' size 16M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 1m;

create tablespace TS_LOBS_ES
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_lobs_ES_01.dbf' size 8M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 4m; 

create tablespace TS_ES
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_ES_01.dbf' size 16M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 1m;

create tablespace TS_ET
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_ET_01.dbf' size 16M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 1m;

create tablespace TS_LOBS_ET
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_lobs_ET_01.dbf' size 8M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 4m;

create tablespace TS_LOBS_FI
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_lobs_FI_01.dbf' size 8M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 4m; 

create tablespace TS_FI
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_FI_01.dbf' size 16M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 1m;

create tablespace TS_LOBS_FR
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_lobs_FR_01.dbf' size 8M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 4m; 

create tablespace TS_FR
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_FR_01.dbf' size 16M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 1m;

create tablespace TS_LOBS_GA
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_lobs_GA_01.dbf' size 8M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 4m; 

create tablespace TS_GA
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_GA_01.dbf' size 16M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 1m;

create tablespace TS_LOBS_HU
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_lobs_HU_01.dbf' size 8M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 4m; 

create tablespace TS_HU
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_HU_01.dbf' size 16M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 1m;

create tablespace TS_LOBS_IT
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_lobs_IT_01.dbf' size 8M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 4m; 

create tablespace TS_IT
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_IT_01.dbf' size 16M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 1m;

create tablespace TS_LOBS_LT
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_lobs_LT_01.dbf' size 8M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 4m; 

create tablespace TS_LT
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_LT_01.dbf' size 16M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 1m;

create tablespace TS_LOBS_LV
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_lobs_LV_01.dbf' size 8M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 4m; 

create tablespace TS_LV
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_LV_01.dbf' size 16M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 1m;

create tablespace TS_LOBS_MT
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_lobs_MT_01.dbf' size 8M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 4m; 

create tablespace TS_MT
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_MT_01.dbf' size 16M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 1m;

create tablespace TS_LOBS_NL
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_lobs_NL_01.dbf' size 8M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 4m; 

create tablespace TS_NL
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_NL_01.dbf' size 16M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 1m;

create tablespace TS_LOBS_PL
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_lobs_PL_01.dbf' size 8M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 4m; 

create tablespace TS_PL
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_PL_01.dbf' size 16M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 1m;

create tablespace TS_LOBS_PT
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_lobs_PT_01.dbf' size 8M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 4m; 

create tablespace TS_PT
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_PT_01.dbf' size 16M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 1m;

create tablespace TS_LOBS_RO
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_lobs_RO_01.dbf' size 8M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 4m; 

create tablespace TS_RO
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_RO_01.dbf' size 16M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 1m;

create tablespace TS_LOBS_SK
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_lobs_SK_01.dbf' size 8M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 4m; 

create tablespace TS_SK
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_SK_01.dbf' size 16M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 1m;

create tablespace TS_LOBS_SL
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_lobs_SL_01.dbf' size 8M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 4m; 

create tablespace TS_SL
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_SL_01.dbf' size 16M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 1m;

create tablespace TS_LOBS_SV
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_lobs_SV_01.dbf' size 8M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 4m; 

create tablespace TS_SV
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_SV_01.dbf' size 16M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 1m;

create tablespace TS_LOBS_HR
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_lobs_HR_01.dbf' size 8M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 4m; 

create tablespace TS_HR
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_HR_01.dbf' size 16M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 1m;

create tablespace TS_LOBS_NO
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_lobs_NO_01.dbf' size 8M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 4m; 

create tablespace TS_NO
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_NO_01.dbf' size 16M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 1m;

create tablespace TS_LOBS_IS
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_lobs_IS_01.dbf' size 8M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 4m; 

create tablespace TS_IS
datafile '/u01/app/oracle/oradata/EE/idol/CELLARM_ts_IS_01.dbf' size 16M
AUTOEXTEND ON NEXT 8M MAXSIZE UNLIMITED
extent management local
segment space management auto
uniform size 1m;

CREATE USER cellarlex IDENTIFIED BY cellar06lex12 DEFAULT TABLESPACE IDOL_DATA;
-- Grant minimum privileges to CELLARLEX
GRANT CREATE SESSION to CELLARLEX;
GRANT UNLIMITED TABLESPACE TO CELLARLEX;
GRANT CREATE TABLE TO CELLARLEX;
GRANT CREATE TRIGGER TO CELLARLEX;
GRANT CREATE SEQUENCE TO CELLARLEX;

alter user cellarlex
quota unlimited on TS_LOBS_BG
quota unlimited on TS_LOBS_CS
quota unlimited on TS_LOBS_DA
quota unlimited on TS_LOBS_DE
quota unlimited on TS_LOBS_EL
quota unlimited on TS_LOBS_EN
quota unlimited on TS_LOBS_ES
quota unlimited on TS_LOBS_ET
quota unlimited on TS_LOBS_FI
quota unlimited on TS_LOBS_FR
quota unlimited on TS_LOBS_GA
quota unlimited on TS_LOBS_HU
quota unlimited on TS_LOBS_IT
quota unlimited on TS_LOBS_LT
quota unlimited on TS_LOBS_LV
quota unlimited on TS_LOBS_MT
quota unlimited on TS_LOBS_NL
quota unlimited on TS_LOBS_PL
quota unlimited on TS_LOBS_PT
quota unlimited on TS_LOBS_RO
quota unlimited on TS_LOBS_SK
quota unlimited on TS_LOBS_SL
quota unlimited on TS_LOBS_SV
quota unlimited on TS_LOBS_HR
quota unlimited on TS_LOBS_NO
quota unlimited on TS_LOBS_IS
quota unlimited on TS_BG
quota unlimited on TS_CS
quota unlimited on TS_DA
quota unlimited on TS_DE
quota unlimited on TS_EL
quota unlimited on TS_EN
quota unlimited on TS_ES
quota unlimited on TS_ET
quota unlimited on TS_FI
quota unlimited on TS_FR
quota unlimited on TS_GA
quota unlimited on TS_HU
quota unlimited on TS_IT
quota unlimited on TS_LT
quota unlimited on TS_LV
quota unlimited on TS_MT
quota unlimited on TS_NL
quota unlimited on TS_PL
quota unlimited on TS_PT
quota unlimited on TS_RO
quota unlimited on TS_SK
quota unlimited on TS_SL
quota unlimited on TS_SV
quota unlimited on TS_HR
quota unlimited on TS_NO
quota unlimited on TS_IS;
