package eu.europa.ec.opoce.cellar.feed.event;

import eu.europa.ec.opoce.cellar.feed.domain.impl.ingestion.IngestionHistory;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

public class IngestionEventListenerTest {

    @Test
    public void onIngestionTest() {
        final IngestionEventListener ingestionEventListener = new IngestionEventListener();

        final IngestionHistory history = new IngestionHistory();
        history.setActionDate(null);

        ingestionEventListener.onIngestion(history);

        // the date must be set right before to commit the transaction
        assertNotNull(history.getActionDate());
    }
}