/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.notification.feed.velocity
 *             FILE : NotificationFeedMarshallerImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 1, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2016-09-01 17:41:53 +0200 (Thu, 01 Sep 2016) $
 *          VERSION : $LastChangedRevision: 11523 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.feed.controller.marshaller.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.common.util.StringUtils;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.feed.controller.marshaller.NotificationFeedMarshaller;
import eu.europa.ec.opoce.cellar.feed.domain.Feed;
import eu.europa.ec.opoce.cellar.feed.domain.FeedItem;
import eu.europa.ec.opoce.cellar.feed.domain.enums.FeedFormat;
import eu.europa.ec.opoce.cellar.feed.domain.enums.FeedType;
import eu.europa.ec.opoce.cellar.feed.exception.NotificationException;
import eu.europa.ec.opoce.cellar.util.DateUtils;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;

import java.io.StringWriter;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;

@Component
public class NotificationFeedMarshallerImpl implements NotificationFeedMarshaller {

    public static final String VELOCITY_PATH = "velocity/notification/";
    /** the velocity engine to build the feed from the template. */
    @Autowired
    @Qualifier("notificationVelocityEngine")
    private VelocityEngine velocityEngine;

    @Autowired
    ICellarConfiguration cellarConfiguration;

    private static final String MARSHALLER_MODEL_NAME = "feed";

    private static final String ENVIRONMENT_LINK="environmentLink";

    private static final String MAX_UPDATED ="maxUpdated";

    private static final String webApiUri="webapi/notification/";

    private static final String embargoUri="admin/secapi/notification/embargo";


    private String resolveSuffix(FeedType feedType){
        switch(feedType){
            case Ingestion:
            case Nal:
            case Ontology:
                return webApiUri+feedType.toString().toLowerCase();
            case Embargo:
                return embargoUri;
            case SparqlLoad:
                return webApiUri+"sparql-load";
            default:
                return null;
        }
    }
    /**
     * {@inheritDoc}
     */
    @Override
    public String marshal(FeedFormat feedType, Feed<? extends FeedItem> feed) {
        try {
            ModelMap model = new ModelMap();
            Optional<Date> mostRecentDate = feed.getFeedItems().stream()
                    .map(FeedItem::getCreationDate)
                    .filter(Objects::nonNull)
                    .max(Date::compareTo);
            String formatedDate;
            if(mostRecentDate.isPresent()) {
                 formatedDate = DateUtils.formatDate(mostRecentDate.get());
            }else {
                 formatedDate = DateTime.now().toString();
            }
            model.put(MAX_UPDATED,formatedDate);
            String viewName = assembleViewName(feedType, feed.getFeedType());
            String envirnomentPrefix=cellarConfiguration.getCellarUriDisseminationBase();
            String environmentSuffix=resolveSuffix(feed.getFeedType());
            model.put(MARSHALLER_MODEL_NAME, feed);
            model.put(ENVIRONMENT_LINK,envirnomentPrefix+environmentSuffix);
            final StringWriter result = new StringWriter();
            final VelocityContext velocityContext = new VelocityContext(model);
            velocityContext.put("StringUtils", StringUtils.class);
            velocityEngine.mergeTemplate(viewName, RuntimeConstants.ENCODING_DEFAULT, velocityContext, result);
            return result.toString();
        } catch (Exception e) {
            throw ExceptionBuilder.get(NotificationException.class)
                    .withMessage("Unable to marshal the " + feedType.name() + " feed (type: " + feed.getFeedType().name() + ")")
                    .withCause(e).build();
        }
    }

    private String assembleViewName(FeedFormat feedFormat, FeedType feedType) {
        return VELOCITY_PATH + feedFormat.getValue() + "-feed-" + feedType.getPersistenceChannel() + ".vm";
    }

    public void setVelocityEngine(final VelocityEngine velocityEngine) {
        this.velocityEngine = velocityEngine;
    }

}
