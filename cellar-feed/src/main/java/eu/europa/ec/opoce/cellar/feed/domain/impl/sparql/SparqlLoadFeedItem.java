package eu.europa.ec.opoce.cellar.feed.domain.impl.sparql;

import eu.europa.ec.opoce.cellar.feed.domain.FeedItem;

/**
 * @author ARHS Developments
 */
public class SparqlLoadFeedItem extends FeedItem {

    public String getNalUri() {
        return nalUri;
    }

    public void setNalUri(String nalUri) {
        this.nalUri = nalUri;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    String nalUri;


    String version;



}
