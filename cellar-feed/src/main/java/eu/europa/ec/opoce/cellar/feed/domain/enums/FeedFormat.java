/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.feed.domain.enums
 *             FILE : FeedFormat.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 1, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2016-03-09 13:57:52 +0100 (Wed, 09 Mar 2016) $
 *          VERSION : $LastChangedRevision: 10409 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.feed.domain.enums;

public enum FeedFormat {

    RSS("rss"), ATOM("atom");

    private String value;

    private FeedFormat(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
