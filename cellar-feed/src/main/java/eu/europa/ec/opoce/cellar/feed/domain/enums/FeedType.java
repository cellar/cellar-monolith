/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.feed.domain.enums
 *             FILE : FeedType.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 1, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2016-07-29 14:24:22 +0200 (Fri, 29 Jul 2016) $
 *          VERSION : $LastChangedRevision: 11244 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.feed.domain.enums;

import eu.europa.ec.opoce.cellar.common.util.StringUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;

import java.util.ArrayList;
import java.util.Collection;

import static eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType.AGENT;
import static eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType.DOSSIER;
import static eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType.EVENT;
import static eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType.EXPRESSION;
import static eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType.ITEM;
import static eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType.MANIFESTATION;
import static eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType.TOPLEVELEVENT;
import static eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType.WORK;
import static eu.europa.ec.opoce.cellar.feed.domain.enums.FeedItemType.*;
import static eu.europa.ec.opoce.cellar.feed.domain.enums.FeedPriority.*;

/**
 * The Enum FeedType.
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 20 juin 2016
 *
 * @author ARHS Developments
 * @version $Revision: 11244 $
 */
public enum FeedType {

    /**
     * Configuration for the Ingestion channel:
     * - accepted action types: create, update, delete, embargo, disembargo
     * - accepted priorities:   AUTHENTICOJ, DAILY, BULK, EMBARGO, NA
     * - accepted wemi classes: all
     * - persistence channel:   ingestion
     */
    Ingestion {{ //accept action type:embargo, disembargo and priority: EMBARGO so that Ingestion feed can pick up all items and present their resolved feed_action_type
        acceptedActionType(create).acceptedActionType(update).acceptedActionType(delete).acceptedActionType(embargo).acceptedActionType(disembargo).
                acceptedPriority(AUTHENTICOJ).acceptedPriority(DAILY).acceptedPriority(BULK).acceptedPriority(EMBARGO).acceptedPriority(NA).
                acceptedWemiClass(WORK).acceptedWemiClass(EXPRESSION).acceptedWemiClass(MANIFESTATION).
                acceptedWemiClass(ITEM).acceptedWemiClass(DOSSIER).acceptedWemiClass(EVENT).
                acceptedWemiClass(AGENT).acceptedWemiClass(TOPLEVELEVENT);
    }},

    /**
     * Configuration for the Embargo channel:
     * - accepted action types: embargo, disembargo, delete_embargoed
     * - accepted priorities:   EMBARGO, NA
     * - accepted wemi classes: all
     * - persistence channel:   ingestion
     */
    Embargo {{
        acceptedActionType(embargo).acceptedActionType(disembargo).acceptedActionType(delete_embargoed).
                acceptedPriority(EMBARGO).acceptedPriority(AUTHENTICOJ).acceptedPriority(DAILY).acceptedPriority(BULK).acceptedPriority(NA).
                acceptedWemiClass(WORK).acceptedWemiClass(EXPRESSION).acceptedWemiClass(MANIFESTATION).
                acceptedWemiClass(ITEM).acceptedWemiClass(DOSSIER).acceptedWemiClass(EVENT).
                acceptedWemiClass(AGENT).acceptedWemiClass(TOPLEVELEVENT).
                persistenceChannel("ingestion");
    }},

    /**
     * Configuration for the NAL channel:
     * - persistence channel: nal
     */
    Nal,

    /**
     * Configuration for the Ontology channel:
     * - persistence channel: ontology
     */
    Ontology,

    SparqlLoad;

    private Collection<FeedItemType> actionTypes;
    private Collection<FeedPriority> priorities;
    private Collection<DigitalObjectType> wemiClasses;
    private String persistenceChannel;

    /**
     * Instantiates a new feed data type.
     */
    FeedType() {
        this.actionTypes = new ArrayList<>();
        this.priorities = new ArrayList<>();
        this.wemiClasses = new ArrayList<>();
        this.persistenceChannel = StringUtils.uncapitalize(this.toString());
    }

    FeedType acceptedActionType(final FeedItemType actionType) {
        this.actionTypes.add(actionType);
        return this;
    }

    FeedType acceptedPriority(final FeedPriority priority) {
        this.priorities.add(priority);
        return this;
    }

    FeedType acceptedWemiClass(final DigitalObjectType wemiClass) {
        this.wemiClasses.add(wemiClass);
        return this;
    }

    FeedType persistenceChannel(final String templateName) {
        this.persistenceChannel = templateName;
        return this;
    }


    public Collection<FeedItemType> getActionTypes() {
        return this.actionTypes;
    }

    public Collection<FeedPriority> getPriorities() {
        return this.priorities;
    }

    public Collection<DigitalObjectType> getWemiClasses() {
        return this.wemiClasses;
    }

    public String getPersistenceChannel() {
        return this.persistenceChannel;
    }

}
