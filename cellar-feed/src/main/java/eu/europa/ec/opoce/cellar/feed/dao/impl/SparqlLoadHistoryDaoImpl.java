package eu.europa.ec.opoce.cellar.feed.dao.impl;

import eu.europa.ec.opoce.cellar.feed.dao.BaseHistoryDao;
import eu.europa.ec.opoce.cellar.feed.domain.impl.sparql.SparqlLoadHistory;
import org.springframework.stereotype.Repository;

@Repository
public class SparqlLoadHistoryDaoImpl extends BaseHistoryDaoImpl<SparqlLoadHistory, Long>
        implements BaseHistoryDao<SparqlLoadHistory, Long> {
}
