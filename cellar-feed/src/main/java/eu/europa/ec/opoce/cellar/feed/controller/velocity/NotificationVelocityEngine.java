/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.notification.feed.velocity
 *             FILE : NotificationVelocityEngine.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 1, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2016-03-09 13:57:52 +0100 (Wed, 09 Mar 2016) $
 *          VERSION : $LastChangedRevision: 10409 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.feed.controller.velocity;

import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.springframework.stereotype.Component;

import java.util.Properties;

@Component("notificationVelocityEngine")
public class NotificationVelocityEngine extends VelocityEngine {

    public NotificationVelocityEngine() {
        super(getProperties());
    }

    private static Properties getProperties() {
        Properties props = new Properties();
        props.put("resource.loader", "class");
        props.put("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        props.put("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        props.put("class.resource.loader.path", "classpath:eu/europa/ec/opoce/cellar/cmr/velocity/");
        return props;
    }

}
