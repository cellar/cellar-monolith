/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.feed
 *             FILE : OntologyBridge.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 02 07, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-02-07 15:14:15 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.feed.util;

import org.apache.jena.ontology.OntModel;

/**
 * TODO remove after cellar modularization
 * @author ARHS Developments
 */
public interface OntologyBridge {

    OntModel getOntModel();
}
