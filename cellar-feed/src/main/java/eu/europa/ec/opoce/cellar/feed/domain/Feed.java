/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.notification.feed
 *             FILE : Feed.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 1, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2016-07-29 14:24:22 +0200 (Fri, 29 Jul 2016) $
 *          VERSION : $LastChangedRevision: 11244 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.feed.domain;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.feed.domain.enums.FeedType;
import eu.europa.ec.opoce.cellar.util.DateUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * The Class Feed.
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 20 juin 2016
 *
 * @author ARHS Developments
 * @version $Revision: 11244 $
 * @param <FI> the generic type
 */
public class Feed<FI extends FeedItem> {

    /** The feed data type. */
    private final FeedType feedType;

    /** The start date. */
    private Date startDate;

    /** The end date. */
    private Date endDate;

    /** The page. */
    private int page;

    /** The feed items. */
    private Collection<FI> feedItems;

    /** The more entries. */
    private boolean hasMoreEntries;


    private String description;

    private String title;
    /**
     * Instantiates a new feed.
     *
     * @param feedType the feed type
     */
    public Feed(final FeedType feedType) {
        this.feedType = feedType;
        this.feedItems = new ArrayList<FI>();
    }

    /**
     * Gets the feed data type.
     *
     * @return the feed data type
     */
    public FeedType getFeedType() {
        return feedType;
    }

    /**
     * Gets the start date.
     *
     * @return the start date
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * Sets the start date.
     *
     * @param startDate the new start date
     */
    public void setStartDate(final Date startDate) {
        this.startDate = startDate;
    }

    /**
     * Gets the end date.
     *
     * @return the end date
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * Sets the end date.
     *
     * @param endDate the new end date
     */
    public void setEndDate(final Date endDate) {
        this.endDate = endDate;
    }

    /**
     * Gets the page.
     *
     * @return the page
     */
    public int getPage() {
        return page;
    }

    /**
     * Sets the page.
     *
     * @param page the new page
     */
    public void setPage(final int page) {
        this.page = page;
    }

    /**
     * Gets the feed items.
     *
     * @return the feed items
     */
    public Collection<FI> getFeedItems() {
        return feedItems;
    }

    /**
     * Sets the feed items.
     *
     * @param feedItems the new feed items
     */
    public void setFeedItems(final Collection<FI> feedItems) {
        this.feedItems = feedItems;
    }

    public boolean isHasMoreEntries() {
        return hasMoreEntries;
    }

    public void setHasMoreEntries(boolean hasMoreEntries) {
        this.hasMoreEntries = hasMoreEntries;
    }

    /**
     * Gets the title.
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    public void setTitle(ICellarConfiguration cellarConfiguration){
        switch(feedType){
            case Ingestion:
                this.title=cellarConfiguration.getCellarFeedIngestionTitle();
                break;
            case Embargo:
                this.title=cellarConfiguration.getCellarFeedEmbargoTitle();
                break;
            case Nal:
                this.title=cellarConfiguration.getCellarFeedNalTitle();
                break;
            case SparqlLoad:
                this.title=cellarConfiguration.getCellarFeedSparqlLoadTitle();
                break;
            case Ontology:
                this.title=cellarConfiguration.getCellarFeedOntologyTitle();
                break;
            default:
                return;
        }
    }

    /**
     * Gets the start date iso8601 formatted.
     *
     * @return the start date iso8601 formatted
     */
    public String getStartDateIso8601Formatted() {
        return DateUtils.formatDate(this.getStartDate());
    }

    /**
     * Gets the end date iso8601 formatted.
     *
     * @return the end date iso8601 formatted
     */
    public String getEndDateIso8601Formatted() {
        return DateUtils.formatDate(this.getEndDate());
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(ICellarConfiguration cellarConfiguration) {
        switch(feedType){
            case Ingestion:
                this.description=cellarConfiguration.getCellarFeedIngestionDescription();
                break;
            case Embargo:
                this.description=cellarConfiguration.getCellarFeedEmbargoDescription();
                break;
            case Nal:
                this.description=cellarConfiguration.getCellarFeedNalDescription();
                break;
            case SparqlLoad:
                this.description=cellarConfiguration.getCellarFeedSparqlLoadDescription();
                break;
            case Ontology:
                this.description=cellarConfiguration.getCellarFeedOntologyDescription();
                break;
            default:
                return;
        }
    }
}
