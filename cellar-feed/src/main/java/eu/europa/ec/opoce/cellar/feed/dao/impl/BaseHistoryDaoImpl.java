/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.feed.dao.impl
 *             FILE : BaseHistoryDaoImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 1, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2016-09-01 17:43:08 +0200 (Thu, 01 Sep 2016) $
 *          VERSION : $LastChangedRevision: 11524 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.feed.dao.impl;

import eu.europa.ec.opoce.cellar.cl.dao.impl.BaseDaoImpl;
import eu.europa.ec.opoce.cellar.feed.dao.BaseHistoryDao;
import eu.europa.ec.opoce.cellar.feed.domain.HistoryEntity;
import eu.europa.ec.opoce.cellar.feed.util.Params;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;

import java.io.Serializable;
import java.util.Collection;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Mar 11, 2014
 *
 * @author ARHS Developments
 * @version $Revision: 11524 $
 */
public abstract class BaseHistoryDaoImpl<ENTITY extends HistoryEntity, ID extends Serializable> extends BaseDaoImpl<ENTITY, ID>
        implements BaseHistoryDao<ENTITY, ID> {

    /**
     * @see eu.europa.ec.opoce.cellar.feed.dao.BaseHistoryDao#getHistory(Params, int, int)
     */
    @Override
    public Collection<ENTITY> getHistory(final Params params, final int startIndex, final int pageSize) {
        final String query = resolveNamedQuery(params);
        return super.findObjectsByNamedQueryWithNamedParamsLimit(query, params.asMap(), startIndex, pageSize);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.feed.dao.BaseHistoryDao#getHistorySize(Params)
     */
    @Override
    public Long getHistorySize(final Params params) {
        return getHibernateTemplate().execute(session -> {
            final String query = resolveNamedQuery(params) + "Size";
            final Query queryObject = buildNamedQuery(session, query, params.asMap());
            return (Long) queryObject.uniqueResult();
        });
    }

    private static String resolveNamedQuery(final Params params) {
        return "get" + StringUtils.capitalize(params.getFeedType().getPersistenceChannel()) + "History";
    }

}
