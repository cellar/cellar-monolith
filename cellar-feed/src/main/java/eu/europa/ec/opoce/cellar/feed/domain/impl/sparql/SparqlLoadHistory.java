package eu.europa.ec.opoce.cellar.feed.domain.impl.sparql;

import eu.europa.ec.opoce.cellar.feed.domain.HistoryEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

/**
 * @author ARHS Developments
 */
@Entity
@Table(name = "SPARQL_LOAD_HISTORY")
@NamedQueries({
        @NamedQuery(name = "getSparqlLoadHistory", query =
                "from SparqlLoadHistory " +
                        "where actionDate between :startDate and :endDate " +
                        "order by actionDate"
        ),
        @NamedQuery(name = "getSparqlLoadHistorySize", query =
                "select count(*) as cnt " +
                        "from SparqlLoadHistory " +
                        "where actionDate between :startDate and :endDate"
        )
})
public class SparqlLoadHistory implements HistoryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "URI", nullable = false)
    private String uri;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ACTION_DATE", nullable = false)
    private Date actionDate;

    @Column(name="CELLAR_ID")
    private String cellarId;

    @Column(name="IDENTIFIERS")
    private String identifiers;

    @Column(name="VERSION")
    private String version;

    public SparqlLoadHistory() {
        actionDate = new Date();
    }

    public SparqlLoadHistory(String uri,String cellarId,String identifiers,String version) {
        this();
        this.uri = uri;
        this.cellarId=cellarId;
        this.identifiers=identifiers;
        this.version=version;
    }

    public String getIdentifiers(){
        return  identifiers;
    }

    public void setIdentifiers(String identifiers){
        this.identifiers=identifiers;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    @Override
    public Date getActionDate() {
        return actionDate;
    }

    @Override
    public void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }


    public String getCellarId() {
        return cellarId;
    }

    public void setCellarId(String cellarId) {
        this.cellarId = cellarId;
    }


    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
