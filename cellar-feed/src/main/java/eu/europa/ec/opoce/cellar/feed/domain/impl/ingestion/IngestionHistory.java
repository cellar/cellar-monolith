package eu.europa.ec.opoce.cellar.feed.domain.impl.ingestion;

import eu.europa.ec.opoce.cellar.common.util.CellarIdUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.feed.domain.HistoryEntity;
import eu.europa.ec.opoce.cellar.feed.domain.enums.FeedItemType;
import eu.europa.ec.opoce.cellar.feed.domain.enums.FeedPriority;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

/**
 * <class_description> Object which defines the history of Cellar's identifiers.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 6, 2013
 *
 * @author ARHS Developments
 * @version $Revision: 10409 $
 */
@Entity
@Table(name = "INGESTION_HISTORY")
@NamedQueries({
        @NamedQuery(name = "getIngestionHistory", query =
                "from IngestionHistory " +
                        "where actionDate between :startDate and :endDate " +
                        "and actionType in (:actionTypes) " +
                        "and priority in (:priorities) " +
                        "and wemiClass in (:wemiClasses) " +
                        "order by actionDate"
        ),
        @NamedQuery(name = "getIngestionHistorySize", query =
                "select count(*) as cnt " +
                        "from IngestionHistory " +
                        "where actionDate between :startDate and :endDate " +
                        "and actionType in (:actionTypes) " +
                        "and priority in (:priorities) " +
                        "and wemiClass in (:wemiClasses)"
        )
})
public class IngestionHistory implements HistoryEntity {

    private Long id;

    private String cellarId;

    private String rootCellarId;

    private DigitalObjectType wemiClass;

    private Boolean relatives;

    private FeedItemType actionType;

    private FeedItemType feedActionType;

    private Date actionDate;

    private FeedPriority priority;

    private String type;

    private String classes;

    private String identifiers;

    public IngestionHistory() {
        this(null);
    }

    public IngestionHistory(final String cellarId) {
        this.actionDate = new Date();
        this.cellarId = cellarId;
        this.rootCellarId = CellarIdUtils.getRootCellarId(this.getCellarId());
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    @Column(name = "CELLAR_ID", nullable = false)
    public String getCellarId() {
        return this.cellarId;
    }

    public void setCellarId(String cellarId) {
        this.cellarId = cellarId;
    }

    @Column(name = "ROOT_CELLAR_ID")
    public String getRootCellarId() {
        return this.rootCellarId;
    }

    public void setRootCellarId(String rootCellarId) {
        this.rootCellarId = rootCellarId;
    }

    @Column(name = "WEMI_CLASS", nullable = false)
    @Enumerated(EnumType.STRING)
    public DigitalObjectType getWemiClass() {
        return wemiClass;
    }

    @Column(name = "RELATIVES")
    public Boolean isRelatives() {
        return this.relatives;
    }

    public void setRelatives(final Boolean relatives) {
        this.relatives = relatives;
    }

    public void setWemiClass(DigitalObjectType wemiClass) {
        this.wemiClass = wemiClass;
    }

    @Column(name = "ACTION_TYPE", nullable = false)
    @Enumerated(EnumType.STRING)
    public FeedItemType getActionType() {
        return this.actionType;
    }

    public void setActionType(final FeedItemType actionType) {
        this.actionType = actionType;
    }

    @Column(name = "FEED_ACTION_TYPE", nullable = false)
    @Enumerated(EnumType.STRING)
    public FeedItemType getFeedActionType() {
        return this.feedActionType;
    }

    public void setFeedActionType(final FeedItemType feedActionType) {
        this.feedActionType = feedActionType;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ACTION_DATE", nullable = false)
    @Override
    public Date getActionDate() {
        return this.actionDate;
    }

    @Override
    public void setActionDate(final Date actionDate) {
        this.actionDate = actionDate;
    }

    @Column(name = "PRIORITY")
    @Enumerated(EnumType.STRING)
    public FeedPriority getPriority() {
        return this.priority;
    }

    public void setPriority(final FeedPriority priority) {
        this.priority = priority;
    }

    @Column(name = "TYPE")
    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Column(name = "CLASSES")
    public String getClasses() {
        return this.classes;
    }

    public void setClasses(String classes) {
        this.classes = classes;
    }

    @Column(name = "IDENTIFIERS")
    public String getIdentifiers() {
        return this.identifiers;
    }

    public void setIdentifiers(String identifiers) {
        this.identifiers = identifiers;
    }
}
