/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.audit
 *        FILE : AuditBuilder.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 09-01-2013
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.feed.util;

import com.google.common.base.Splitter;
import eu.europa.ec.opoce.cellar.common.util.StringUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.feed.domain.enums.FeedItemType;
import eu.europa.ec.opoce.cellar.feed.domain.enums.FeedPriority;
import eu.europa.ec.opoce.cellar.feed.domain.enums.FeedType;
import eu.europa.ec.opoce.cellar.util.DateUtils;
import org.apache.commons.collections.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * <class_description> Builder class for conveniently building params for history search.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 09-01-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class Params {

    private final FeedType feedType;
    private final Date requestDate;

    private Date startDate;
    private Date endDate;
    private Collection<FeedItemType> actionTypes;
    private Collection<FeedPriority> priorities;
    private Collection<DigitalObjectType> wemiClasses;

    private Params(FeedType feedType) {
        this.feedType = feedType;
        this.requestDate = new Date();
        this.endDate = this.requestDate;
    }

    public static Params getFor(FeedType feedType) {
        return new Params(feedType);
    }

    public Params startDate(final String startDate) {
        this.startDate = DateUtils.parseDate(startDate);
        if (this.startDate == null) {
            ExceptionUtil.buildAndThrowBadRequest("The 'startDate' parameter must be set.");
        }
        if (this.startDate.after(this.requestDate)) {
            ExceptionUtil.buildAndThrowBadRequest("The 'startDate' parameter cannot be in the future: '{}'.", this.startDate);
        }
        if (this.endDate != null && this.startDate.after(this.endDate)) {
            ExceptionUtil.buildAndThrowBadRequest("The 'startDate' parameter ('{}') cannot be after 'endDate' parameter ('{}').", this.startDate, this.endDate);
        }

        return this;
    }

    public Params endDate(final String endDate) {
        this.endDate = DateUtils.parseDate(endDate);
        if (this.endDate == null || this.endDate.after(this.requestDate)) {
            this.endDate = this.requestDate;
        }
        if (this.startDate != null && this.startDate.after(this.endDate)) {
            ExceptionUtil.buildAndThrowBadRequest("The 'startDate' parameter ('{}') cannot be after 'endDate' parameter ('{}').", this.startDate, this.endDate);
        }

        return this;
    }

    public Params actionTypes(final String actionType) {
        this.actionTypes = this.resolveActionTypes(actionType);
        return this;
    }

    public Params priorities(final String priority) {
        this.priorities = this.resolvePriorities(priority);
        return this;
    }

    public Params wemiClasses(final String wemiClasses) {
        this.wemiClasses = this.resolveWemiClasses(wemiClasses);
        return this;
    }

    /**
     * Build the map of needed params on the base of the feed type.
     *
     * @return the map of parameters
     */
    public Map<String, Object> asMap() {
        final Map<String, Object> params = new HashMap<>();
        params.put("startDate", startDate);
        params.put("endDate", endDate);
        if (FeedType.Ingestion.equals(this.getFeedType()) || FeedType.Embargo.equals(this.getFeedType())) {
            params.put("actionTypes", CollectionUtils.isEmpty(this.actionTypes) ? this.getFeedType().getActionTypes() : this.actionTypes);
            params.put("priorities", CollectionUtils.isEmpty(this.priorities) ? this.getFeedType().getPriorities() : this.priorities);
            params.put("wemiClasses", CollectionUtils.isEmpty(this.wemiClasses) ? this.getFeedType().getWemiClasses() : this.wemiClasses);
        }
        return params;
    }

    public FeedType getFeedType() {
        return feedType;
    }

    public Date getStartDate() {
        return this.startDate;
    }

    public Date getEndDate() {
        return this.endDate;
    }

    // private methods

    private Collection<FeedItemType> resolveActionTypes(final String csvList) {
        final Collection<FeedItemType> resolvedCollection = new ArrayList<>();
        if (StringUtils.isEmpty(csvList)) {
            return resolvedCollection;
        }

        Splitter.on(",").omitEmptyStrings().trimResults().split(csvList).forEach(token -> {
            Optional<FeedItemType> resolvedObject = resolve(token, this.getFeedType().getActionTypes(), FeedItemType.class);
            if (resolvedObject.isPresent()) {
                resolvedCollection.add(resolvedObject.get());
            } else {
                ExceptionUtil.buildAndThrowBadRequest("The action type '{}' is not supported for channel '{}'. Supported action types are: {}.",
                        token, this.getFeedType(), this.getFeedType().getActionTypes());
            }
        });
        return resolvedCollection;
    }

    private Collection<FeedPriority> resolvePriorities(final String csvList) {
        final Collection<FeedPriority> resolvedCollection = new ArrayList<>();
        if (StringUtils.isEmpty(csvList)) {
            return resolvedCollection;
        }

        Splitter.on(",").omitEmptyStrings().trimResults().split(csvList).forEach(token -> {
            Optional<FeedPriority> resolvedObject = resolve(token, this.getFeedType().getPriorities(), FeedPriority.class);
            if (resolvedObject.isPresent()) {
                resolvedCollection.add(resolvedObject.get());
            } else {
                ExceptionUtil.buildAndThrowBadRequest("The priority '{}' is not supported for channel '{}'. Supported priorities are: {}.",
                        token, this.getFeedType(), this.getFeedType().getPriorities());
            }
        });
        return resolvedCollection;
    }

    private Collection<DigitalObjectType> resolveWemiClasses(final String csvList) {
        final Collection<DigitalObjectType> resolvedCollection = new ArrayList<>();
        if (StringUtils.isEmpty(csvList)) {
            return resolvedCollection;
        }

        Splitter.on(",").omitEmptyStrings().trimResults().split(csvList).forEach(token -> {
            Optional<DigitalObjectType> resolvedObject = resolve(token, this.getFeedType().getWemiClasses(), DigitalObjectType.class);
            if (resolvedObject.isPresent()) {
                resolvedCollection.add(resolvedObject.get());
            } else {
                ExceptionUtil.buildAndThrowBadRequest("The WEMI class '{}' is not supported for channel '{}'. Supported WEMI classes are: {}.",
                        token, this.getFeedType(), this.getFeedType().getWemiClasses());
            }
        });
        return resolvedCollection;
    }

    private static <T extends Enum<T>> Optional<T> resolve(final String str, Collection<T> resolveFromCollection, final Class<T> enumType) {
        if (StringUtils.isEmpty(str)) {
            return Optional.empty();
        }

        T resolvedObject = null;
        try {
            resolvedObject = T.valueOf(enumType, str.toLowerCase());
        } catch (IllegalArgumentException exc) {
            // ignore
        }
        try {
            resolvedObject = T.valueOf(enumType, str.toUpperCase());
        } catch (IllegalArgumentException exc) {
            // ignore
        }
        if (resolvedObject != null && !resolveFromCollection.contains(resolvedObject)) {
            resolvedObject = null;
        }

        return Optional.ofNullable(resolvedObject);
    }

}
