package eu.europa.ec.opoce.cellar.feed.dao;

import eu.europa.ec.opoce.cellar.cl.dao.BaseDao;
import eu.europa.ec.opoce.cellar.feed.domain.HistoryEntity;
import eu.europa.ec.opoce.cellar.feed.util.Params;

import java.io.Serializable;
import java.util.Collection;

/**
 * <class_description> Global data access object definition for accessing the history data.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 6, 2013
 *
 * @author ARHS Developments
 * @version $Revision: 10409 $
 */
public interface BaseHistoryDao<ENTITY extends HistoryEntity, ID extends Serializable> extends BaseDao<ENTITY, ID> {

    /**
     * Get the historic ingestion actions in the given period, filtered for the given paramaters
     *
     * @param params     The parameters used for the search
     * @param startIndex The start index for the pagination
     * @param pageSize   The end index for the pagination
     * @return The historic actions
     */
    Collection<ENTITY> getHistory(final Params params, final int startIndex, final int pageSize);

    /**
     * Get the size historic ingestion actions in the given period, filtered for the given paramaters
     *
     * @param params The parameters used for the search
     * @return The historic actions
     */
    Long getHistorySize(final Params params);

}
