/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.history
 *             FILE : NalHistory.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 1, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2016-03-09 13:57:52 +0100 (Wed, 09 Mar 2016) $
 *          VERSION : $LastChangedRevision: 10409 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.feed.domain.impl.nal;

import eu.europa.ec.opoce.cellar.feed.domain.HistoryEntity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "NAL_HISTORY")
@NamedQueries({
        @NamedQuery(name = "getNalHistory", query =
                "from NalHistory " +
                        "where actionDate between :startDate and :endDate " +
                        "order by actionDate"
        ),
        @NamedQuery(name = "getNalHistorySize", query =
                "select count(*) as cnt " +
                        "from NalHistory " +
                        "where actionDate between :startDate and :endDate"
        )
})
public class NalHistory implements HistoryEntity {

    private Long id;

    private String uri;

    private String version;

    private Date actionDate;

    public String cellarId;

    public String identifiers;

    public NalHistory() {
        this.actionDate = new Date();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    @Column(name = "URI", nullable = false)
    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    @Column(name = "VERSION", nullable = false)
    public String getVersion() {
        return version;
    }

    @Column(name="CELLAR_ID")
    public String getCellarId(){return cellarId;}

    public void setCellarId(String cellarId){
        this.cellarId=cellarId;
    }

    @Column(name="IDENTIFIERS")
    public String identifiers(){
        return identifiers;
    }

    public void setIdentifiers(String identifiers){
        this.identifiers=identifiers;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ACTION_DATE", nullable = false)
    @Override
    public Date getActionDate() {
        return actionDate;
    }

    @Override
    public void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }

    public String getIdentifiers() {
        return identifiers;
    }

}
