package eu.europa.ec.opoce.cellar.feed.util;

import eu.europa.ec.opoce.cellar.cmr.ContentType;

import java.util.Optional;

/**
 * @author ARHS Developments
 * TODO remove after CELLAR modularization
 */
public interface ContentStreamServiceBridge {

    Optional<String> getContentAsString(String cellarID, ContentType contentType);

    Optional<String> getContentAsString(String cellarID, String version, ContentType contentType);
}
