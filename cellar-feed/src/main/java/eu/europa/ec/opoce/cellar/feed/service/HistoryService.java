/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.client
 *             FILE : HistoryService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 1, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2016-07-29 14:24:22 +0200 (Fri, 29 Jul 2016) $
 *          VERSION : $LastChangedRevision: 11244 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.feed.service;

import eu.europa.ec.opoce.cellar.cl.domain.admin.ProductionIdentifier;
import eu.europa.ec.opoce.cellar.feed.domain.HistoryEntity;
import eu.europa.ec.opoce.cellar.feed.domain.impl.ingestion.IngestionHistory;
import eu.europa.ec.opoce.cellar.feed.domain.impl.nal.NalHistory;
import eu.europa.ec.opoce.cellar.feed.domain.impl.ontology.OntologyHistory;
import eu.europa.ec.opoce.cellar.feed.domain.impl.sparql.SparqlLoadHistory;
import eu.europa.ec.opoce.cellar.feed.util.Params;

import java.util.Collection;

/**
 * <class_description> Provides information from the different history tables.
 * <p>
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Mar 20, 2014
 *
 * @author ARHS Developments
 * @version $Revision: 11244 $
 */
public interface HistoryService {

    /**
     * Store the given {@code historyEntity}
     *
     * @param historyEntity the history entity to store
     */
    void store(final HistoryEntity historyEntity);

    /**
     * Get the historic ingestion actions in the given period, filtered by the given parameters
     *
     * @param params     The parameters used for the search
     * @param startIndex The start index for the pagination
     * @param pageSize   The size of the page
     * @return The historic actions
     */
    Collection<IngestionHistory> getIngestionHistory(final Params params, final int startIndex, final int pageSize);

    /**
     * Get count historic ingestion actions in the given period, filtered by the given parameters
     *
     * @param params The parameters used for the search
     * @return The historic actions
     */
    Long getIngestionHistorySize(final Params params);

    /**
     * Get all historic NAL actions in the given period, filtered by the given parameters
     *
     * @param params     The parameters used for the search
     * @param startIndex The start index for the pagination
     * @param pageSize   The size of the page
     * @return The historic actions
     */
    Collection<NalHistory> getNalHistory(final Params params, final int startIndex, final int pageSize);

    /**
     * Get count historic ingestion actions in the given period, filtered by the given parameters
     *
     * @param params The parameters used for the search
     * @return The historic actions
     */
    Long getNalHistorySize(final Params params);

    /**
     * Get all historic sparql-load actions in the given period, filtered by the given parameters
     *
     * @param params     The parameters used for the search
     * @param startIndex The start index for the pagination
     * @param pageSize   The size of the page
     * @return The historic actions
     */
    Collection<SparqlLoadHistory> getSparqlLoadHistory(Params params, int startIndex, int pageSize);

    /**
     * Get count historic sparql-load actions in the given period, filtered by the given parameters
     *
     * @param params The parameters used for the search
     * @return The historic actions
     */
    Long getSparqlLoadHistorySize(Params params);

    /**
     * Get all historic ontology actions in the given period, filtered by the given parameters
     *
     * @param params     The parameters used for the search
     * @param startIndex The start index for the pagination
     * @param pageSize   The size of the page
     * @return The historic actions
     */
    Collection<OntologyHistory> getOntologyHistory(final Params params, final int startIndex, final int pageSize);

    /**
     * Get count historic ingestion actions in the given period, filtered by the given parameters
     *
     * @param params The parameters used for the search
     * @return The historic actions
     */
    Long getOntologyHistorySize(final Params params);

    /**
     * Retrieve the class and all super classes for the given cellar ID and return them as an ordered list.
     * The order starts with the most specific class and ends with the most generic class.
     *
     * @param cellarId The cellar ID
     * @return The classes as an ordered list, beginning with the most specific class.
     */
    Collection<String> getOwlClassesForCellarId(final String cellarId, String inferredVersion);

    /**
     * Get all {@link ProductionIdentifier} names related to the given cellar ID
     *
     * @param cellarId The cellar ID
     * @return The names of the related {@link ProductionIdentifier} names
     */
    Collection<String> getProductionIdentifierNamesForCellarId(final String cellarId);

}
