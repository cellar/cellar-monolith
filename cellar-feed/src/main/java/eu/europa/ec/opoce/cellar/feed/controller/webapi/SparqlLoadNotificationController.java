package eu.europa.ec.opoce.cellar.feed.controller.webapi;

import eu.europa.ec.opoce.cellar.feed.controller.BaseNotificationController;
import eu.europa.ec.opoce.cellar.feed.domain.enums.FeedType;
import eu.europa.ec.opoce.cellar.feed.domain.impl.sparql.SparqlLoadFeedItem;
import eu.europa.ec.opoce.cellar.feed.domain.impl.sparql.SparqlLoadHistory;
import eu.europa.ec.opoce.cellar.feed.service.HistoryService;
import eu.europa.ec.opoce.cellar.feed.util.Params;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * @author ARHS Developments
 */
@Controller
@RequestMapping(value = BaseNotificationController.PAGE_URL + "/sparql-load")
public class SparqlLoadNotificationController extends BaseNotificationController<SparqlLoadHistory, SparqlLoadFeedItem> {

    private final HistoryService historyService;

    @Autowired
    public SparqlLoadNotificationController(HistoryService historyService) {
        this.historyService = historyService;
    }

    @Override
    protected Collection<SparqlLoadHistory> getHistory(Params params, int startIndex, int pageSize) {
        return historyService.getSparqlLoadHistory(params, startIndex, pageSize);
    }

    @Override
    protected Long getHistorySize(Params params) {
        return historyService.getSparqlLoadHistorySize(params);
    }

    @Override
    protected SparqlLoadFeedItem createFeedItem(SparqlLoadHistory historyElement) {
        SparqlLoadFeedItem item = new SparqlLoadFeedItem();
        item.setId(historyElement.getUri());
        item.setCreationDate(historyElement.getActionDate());
        item.setCellarId(historyElement.getCellarId());
        item.setVersion(historyElement.getVersion());
        item.setNalUri(historyElement.getUri());
        if(historyElement.getIdentifiers()!=null) {
            item.setIdentifiers(Arrays.stream(historyElement.getIdentifiers().split(",")).collect(Collectors.toList()));
        }
        return item;
    }

    @Override
    protected FeedType resolveFeedType() {
        return FeedType.SparqlLoad;
    }
}
