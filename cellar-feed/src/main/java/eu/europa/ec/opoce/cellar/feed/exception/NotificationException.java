/* ----------------------------------------------------------------------------
*          PROJECT : CELLAR maintenance
*
*          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.notification
*             FILE : NotificationException.java
*
*       CREATED BY : ARHS Developments
*               ON : Apr 1, 2014
*
*      MODIFIED BY : ARHS Developments
*               ON : $LastChangedDate 2014-03-19 14:41:03 +0100 (Wed, 19 Mar 2014) $
*          VERSION : $LastChangedRevision 6168 $
*
* ----------------------------------------------------------------------------
* Copyright (c) 2011-2014 European Commission - Publications Office
* ----------------------------------------------------------------------------
*/
package eu.europa.ec.opoce.cellar.feed.exception;

import eu.europa.ec.opoce.cellar.exception.BuildableException;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

public class NotificationException extends CellarException {

    private static final long serialVersionUID = 1064435545804448067L;

    /**
     * Constructs a new exception with its associated builder.   
     * 
     * @param builder the builder to use for building the exception
     */
    public NotificationException(final ExceptionBuilder<? extends BuildableException> builder) {
        super(builder);
    }

}
