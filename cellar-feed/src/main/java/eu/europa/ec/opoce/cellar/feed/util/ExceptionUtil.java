/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.audit
 *        FILE : AuditBuilder.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 09-01-2013
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.feed.util;

import eu.europa.ec.opoce.cellar.cl.exception.CellarWebException;
import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import org.springframework.http.HttpStatus;

/**
 * <class_description> Builder class for conveniently building params for history search.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 09-01-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class ExceptionUtil {

    public static void buildAndThrowBadRequest(final String message, final Object... messageArgs) {
        throw HttpStatusAwareExceptionBuilder.getInstance(CellarWebException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                .withMessage(message).withMessageArgs(messageArgs).build();
    }

}
