/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.dao.impl
 *             FILE : NalHistoryDaoImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 1, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2016-09-01 17:43:08 +0200 (Thu, 01 Sep 2016) $
 *          VERSION : $LastChangedRevision: 11524 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.feed.dao.impl;

import eu.europa.ec.opoce.cellar.feed.dao.BaseHistoryDao;
import eu.europa.ec.opoce.cellar.feed.domain.impl.nal.NalHistory;
import org.springframework.stereotype.Repository;

@Repository
public class NalHistoryDaoImpl extends BaseHistoryDaoImpl<NalHistory, Long> implements BaseHistoryDao<NalHistory, Long> {
}
