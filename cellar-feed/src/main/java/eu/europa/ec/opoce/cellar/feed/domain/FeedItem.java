/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.notification.feed
 *             FILE : FeedItem.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 1, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2016-03-09 13:57:52 +0100 (Wed, 09 Mar 2016) $
 *          VERSION : $LastChangedRevision: 10409 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.feed.domain;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.feed.domain.enums.FeedItemType;
import eu.europa.ec.opoce.cellar.feed.domain.enums.FeedType;
import eu.europa.ec.opoce.cellar.util.DateUtils;

import java.util.Date;
import java.util.List;

public abstract class FeedItem {

    protected String id;

    protected FeedItemType feedItemType;

    private Date creationDate;


    public void setTitle(String title) {
        this.title = title;
    }

    private String cellarUri;

    private String cellarId;

    private List<String> identifiers;

    private String title;


    private String entrySummary;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public FeedItemType getFeedItemType() {
        return feedItemType;
    }

    public void setFeedItemType(FeedItemType feedItemType) {
        this.feedItemType = feedItemType;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationDateIso8601Formatted() {
        return DateUtils.formatDate(this.getCreationDate());
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(ICellarConfiguration cellarConfiguration, FeedType feedType) {
        switch(feedType){
            case Ingestion:
                this.title=cellarConfiguration.getCellarFeedIngestionItemTitle();
                break;
            case Embargo:
                this.title=cellarConfiguration.getCellarFeedEmbargoItemTitle();
                break;
            case Nal:
                this.title=cellarConfiguration.getCellarFeedNalItemTitle();
                break;
            case SparqlLoad:
                this.title=cellarConfiguration.getCellarFeedSparqlLoadItemTitle();
                break;
            case Ontology:
                this.title=cellarConfiguration.getCellarFeedOntologyItemTitle();
                break;
            default:
                return;
        }
    }

    public String getEntrySummary() {
        return entrySummary;
    }

    public void setEntrySummary(ICellarConfiguration cellarConfiguration, FeedType feedType) {
        switch(feedType){
            case Ingestion:
                this.entrySummary=cellarConfiguration.getCellarFeedIngestionEntrySummary();
                break;
            case Embargo:
                this.entrySummary=cellarConfiguration.getCellarFeedEmbargoEntrySummary();
                break;
            case Nal:
                this.entrySummary=cellarConfiguration.getCellarFeedNalEntrySummary();
                break;
            case SparqlLoad:
                this.entrySummary=cellarConfiguration.getCellarFeedSparqlLoadEntrySummary();
                break;
            case Ontology:
                this.entrySummary=cellarConfiguration.getCellarFeedOntologyEntrySummary();
                break;
            default:
                return;
        }
    }

    public String getCellarId() {
        return cellarId;
    }

    public void setCellarId(String cellarId) {
        this.cellarId = cellarId;
    }

    public List<String> getIdentifiers() {
        return identifiers;
    }

    public void setIdentifiers(List<String> identifiers) {
        this.identifiers = identifiers;
    }

    public String getCellarUri() {
        return cellarUri;
    }

    public void setCellarUri(String cellarUri) {
        this.cellarUri = cellarUri;
    }


}
