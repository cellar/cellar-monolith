/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.notification.feed.velocity
 *             FILE : NotificationFeedMarshaller.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 1, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2016-03-09 13:57:52 +0100 (Wed, 09 Mar 2016) $
 *          VERSION : $LastChangedRevision: 10409 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.feed.controller.marshaller;

import eu.europa.ec.opoce.cellar.feed.domain.Feed;
import eu.europa.ec.opoce.cellar.feed.domain.FeedItem;
import eu.europa.ec.opoce.cellar.feed.domain.enums.FeedFormat;

public interface NotificationFeedMarshaller {

    String marshal(FeedFormat feedType, Feed<? extends FeedItem> toMarshal);

}
