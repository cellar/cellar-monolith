/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.notification.feed
 *             FILE : IngestionFeedItem.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 1, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2016-07-29 14:24:22 +0200 (Fri, 29 Jul 2016) $
 *          VERSION : $LastChangedRevision: 11244 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.feed.domain.impl.ingestion;

import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.feed.domain.FeedItem;
import eu.europa.ec.opoce.cellar.feed.domain.enums.FeedPriority;

import java.util.Collection;

public class IngestionFeedItem extends FeedItem {

    private String cellarId;

    private String rootCellarId;

    private DigitalObjectType wemiClass;

    private Collection<String> classes;

    private Collection<String> rootIdentifiers;

    private FeedPriority priority;

    // generic purpose field to store the manifestation type, mime type, ... depending on the wemi class
    private String type;

    public String getCellarId() {
        return cellarId;
    }

    public void setCellarId(String cellarId) {
        this.cellarId = cellarId;
    }

    public String getRootCellarId() { return rootCellarId; }

    public void setRootCellarId(String rootCellarId) {
        this.rootCellarId = rootCellarId;
    }

    public DigitalObjectType getWemiClass() { return wemiClass; }

    public void setWemiClass(DigitalObjectType wemiClass) { this.wemiClass = wemiClass; }

    public Collection<String> getClasses() {
        return classes;
    }

    public void setClasses(Collection<String> classes) {
        this.classes = classes;
    }


    public Collection<String> getRootIdentifiers() {
        return rootIdentifiers;
    }

    public void setRootIdentifiers(Collection<String> rootIdentifiers) {
        this.rootIdentifiers = rootIdentifiers;
    }

    public FeedPriority getPriority() {
        return priority;
    }

    public void setPriority(FeedPriority priority) {
        this.priority = priority;
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }
}
