/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.controller.admin.notification.feed
 *             FILE : NalFeedItem.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 1, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2016-03-09 13:57:52 +0100 (Wed, 09 Mar 2016) $
 *          VERSION : $LastChangedRevision: 10409 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.feed.domain.impl.nal;

import eu.europa.ec.opoce.cellar.feed.domain.FeedItem;

public class NalFeedItem extends FeedItem {

    private String version;

    private String nalUri;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getNalUri() {
        return nalUri;
    }

    public void setNalUri(String nalUri) {
        this.nalUri = nalUri;
    }

}
