/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.feed.controller.webapi
 *             FILE : NalNotificationController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 1, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-01-02 11:32:44 +0100 (Mon, 02 Jan 2017) $
 *          VERSION : $LastChangedRevision: 12298 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.feed.controller.webapi;

import eu.europa.ec.opoce.cellar.feed.controller.BaseNotificationController;
import eu.europa.ec.opoce.cellar.feed.domain.enums.FeedType;
import eu.europa.ec.opoce.cellar.feed.domain.impl.nal.NalFeedItem;
import eu.europa.ec.opoce.cellar.feed.domain.impl.nal.NalHistory;
import eu.europa.ec.opoce.cellar.feed.util.Params;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * The notification controller for NALs.
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 20 juin 2016
 *
 * @author ARHS Developments
 * @version $Revision: 12298 $
 */
@Controller
@RequestMapping(value = BaseNotificationController.PAGE_URL + "/nal")
public class NalNotificationController extends BaseNotificationController<NalHistory, NalFeedItem> {

    /**
     * @see eu.europa.ec.opoce.cellar.feed.controller.BaseNotificationController#getHistory(Params, int, int)
     */
    @Override
    protected Collection<NalHistory> getHistory(final Params params, final int startIndex, final int pageSize) {
        return this.historyService.getNalHistory(params, startIndex, pageSize);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.feed.controller.BaseNotificationController#getHistorySize(Params)
     */
    @Override
    protected Long getHistorySize(final Params params) {
        return this.historyService.getNalHistorySize(params);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.feed.controller.BaseNotificationController#createFeedItem(eu.europa.ec.opoce.cellar.feed.domain.HistoryEntity)
     */
    @Override
    protected NalFeedItem createFeedItem(final NalHistory historyElement) {
        NalFeedItem item = new NalFeedItem();
        item.setId(historyElement.getUri());
        item.setVersion(historyElement.getVersion());
        item.setCellarId(historyElement.getCellarId());
        item.setCreationDate(historyElement.getActionDate());
        item.setNalUri(historyElement.getUri());
        if(historyElement.getIdentifiers()!=null) {
            item.setIdentifiers(Arrays.stream(historyElement.getIdentifiers().split(",")).collect(Collectors.toList()));
        }
        return item;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.feed.controller.BaseNotificationController#resolveFeedType()
     */
    @Override
    protected FeedType resolveFeedType() {
        return FeedType.Nal;
    }

}
