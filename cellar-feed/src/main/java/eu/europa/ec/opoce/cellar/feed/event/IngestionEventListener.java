package eu.europa.ec.opoce.cellar.feed.event;

import eu.europa.ec.opoce.cellar.feed.domain.HistoryEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionalEventListener;

import java.util.Date;

import static org.springframework.transaction.event.TransactionPhase.BEFORE_COMMIT;

@Service
public class IngestionEventListener {

    private static final Logger logger = LogManager.getLogger(IngestionEventListener.class);


    @TransactionalEventListener(phase = BEFORE_COMMIT)
    void onIngestion(final HistoryEntity historyEntity) {
        final Date date = new Date();
        logger.debug("Setting action date to {}", date);
        historyEntity.setActionDate(date);
    }
}
