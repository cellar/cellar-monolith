/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.history
 *             FILE : OntologyHistory.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 1, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2016-03-09 13:57:52 +0100 (Wed, 09 Mar 2016) $
 *          VERSION : $LastChangedRevision: 10409 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.feed.domain.impl.ontology;

import eu.europa.ec.opoce.cellar.feed.domain.HistoryEntity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "ONTOLOGY_HISTORY")
@NamedQueries({
        @NamedQuery(name = "getOntologyHistory", query =
                "from OntologyHistory " +
                        "where actionDate between :startDate and :endDate " +
                        "order by actionDate"
        ),
        @NamedQuery(name = "getOntologyHistorySize", query =
                "select count(*) as cnt " +
                        "from OntologyHistory " +
                        "where actionDate between :startDate and :endDate"
        )
})
public class OntologyHistory implements HistoryEntity {

    private Long id;

    private String uri;

    private String version;

    private Date actionDate;

    private String cellarId;

    private String identifiers;

    public OntologyHistory() {
        this.actionDate = new Date();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    @Column(name = "URI", nullable = false)
    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    @Column(name = "VERSION", nullable = false)
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ACTION_DATE", nullable = false)
    @Override
    public Date getActionDate() {
        return actionDate;
    }

    @Column(name="CELLAR_ID")
    public String getCellarId(){return cellarId;}

    public void setCellarId(String cellarId){this.cellarId=cellarId;}

    @Column(name="identifiers")
    public String getIdentifiers(){return identifiers;}

    public void setIdentifiers(String identifiers){
        this.identifiers=identifiers;
    }

    @Override
    public void setActionDate(Date actionDate) {
        this.actionDate = actionDate;
    }

}
