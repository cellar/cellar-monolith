/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *             FILE : HistoryServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 4, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2016-07-29 14:24:22 +0200 (Fri, 29 Jul 2016) $
 *          VERSION : $LastChangedRevision: 11244 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.feed.service.impl;

import eu.europa.ec.opoce.cellar.ccr.service.database.DefaultTransactionService;
import eu.europa.ec.opoce.cellar.cl.dao.ProductionIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.domain.admin.ProductionIdentifier;
import eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.feed.dao.BaseHistoryDao;
import eu.europa.ec.opoce.cellar.feed.domain.HistoryEntity;
import eu.europa.ec.opoce.cellar.feed.domain.impl.ingestion.IngestionHistory;
import eu.europa.ec.opoce.cellar.feed.domain.impl.nal.NalHistory;
import eu.europa.ec.opoce.cellar.feed.domain.impl.ontology.OntologyHistory;
import eu.europa.ec.opoce.cellar.feed.domain.impl.sparql.SparqlLoadHistory;
import eu.europa.ec.opoce.cellar.feed.service.HistoryService;
import eu.europa.ec.opoce.cellar.feed.util.ContentStreamServiceBridge;
import eu.europa.ec.opoce.cellar.feed.util.OntologyBridge;
import eu.europa.ec.opoce.cellar.feed.util.Params;
import eu.europa.ec.opoce.cellar.semantic.helper.MessageFormatter;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.NodeIterator;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Apr 4, 2014
 *
 * @author ARHS Developments
 * @version $Revision: 11244 $
 */
@Service
public class HistoryServiceImpl extends DefaultTransactionService implements HistoryService {

    private static final Logger LOG = LogManager.getLogger(HistoryServiceImpl.class);

    @Autowired
    private BaseHistoryDao<IngestionHistory, Long> ingestionHistoryDao;

    @Autowired
    private BaseHistoryDao<NalHistory, Long> nalHistoryDao;

    @Autowired
    private BaseHistoryDao<OntologyHistory, Long> ontologyHistoryDao;

    @Autowired
    private BaseHistoryDao<SparqlLoadHistory, Long> sparqlLoadHistoryDao;

    @Autowired
    @Qualifier("pidManagerService")
    private PidManagerService pidManagerService;

    @Autowired
    private ProductionIdentifierDao productionIdentifierDao;

    @Autowired
    @Lazy
    private OntologyBridge ontologyBridge;

    @Autowired
    private ContentStreamServiceBridge contentStreamServiceBridge;

    /**
     * @see eu.europa.ec.opoce.cellar.feed.service.HistoryService#store(eu.europa.ec.opoce.cellar.feed.domain.HistoryEntity)
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public void store(final HistoryEntity historyEntity) {
        if (historyEntity instanceof IngestionHistory) {
            ingestionHistoryDao.saveObject((IngestionHistory) historyEntity);
        } else if (historyEntity instanceof NalHistory) {
            nalHistoryDao.saveObject((NalHistory) historyEntity);
        } else if (historyEntity instanceof OntologyHistory) {
            ontologyHistoryDao.saveObject((OntologyHistory) historyEntity);
        } else if (historyEntity instanceof SparqlLoadHistory) {
            sparqlLoadHistoryDao.saveObject((SparqlLoadHistory) historyEntity);
        }
    }

    /**
     * @see eu.europa.ec.opoce.cellar.feed.service.HistoryService#getIngestionHistory(Params, int, int)
     */
    @Override
    public Collection<IngestionHistory> getIngestionHistory(final Params params, final int startIndex, final int pageSize) {
        return this.ingestionHistoryDao.getHistory(params, startIndex, pageSize);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.feed.service.HistoryService#getIngestionHistorySize(Params)
     */
    @Override
    public Long getIngestionHistorySize(final Params params) {
        return this.ingestionHistoryDao.getHistorySize(params);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.feed.service.HistoryService#getNalHistory(Params, int, int)
     */
    @Override
    public Collection<NalHistory> getNalHistory(final Params params, final int startIndex, final int pageSize) {
        return this.nalHistoryDao.getHistory(params, startIndex, pageSize);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.feed.service.HistoryService#getNalHistorySize(Params)
     */
    @Override
    public Long getNalHistorySize(final Params params) {
        return this.nalHistoryDao.getHistorySize(params);
    }

    /**
     * Get all historic sparql-load actions in the given period, filtered by the given parameters
     *
     * @param params     The parameters used for the search
     * @param startIndex The start index for the pagination
     * @param pageSize   The size of the page
     * @return The historic actions
     */
    @Override
    public Collection<SparqlLoadHistory> getSparqlLoadHistory(Params params, int startIndex, int pageSize) {
        return sparqlLoadHistoryDao.getHistory(params, startIndex, pageSize);
    }

    /**
     * Get count historic sparql-load actions in the given period, filtered by the given parameters
     *
     * @param params The parameters used for the search
     * @return The historic actions
     */
    @Override
    public Long getSparqlLoadHistorySize(Params params) {
        return sparqlLoadHistoryDao.getHistorySize(params);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.feed.service.HistoryService#getOntologyHistory(Params, int, int)
     */
    @Override
    public Collection<OntologyHistory> getOntologyHistory(final Params params, final int startIndex, final int pageSize) {
        return this.ontologyHistoryDao.getHistory(params, startIndex, pageSize);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.feed.service.HistoryService#getOntologyHistorySize(Params)
     */
    @Override
    public Long getOntologyHistorySize(final Params params) {
        return this.ontologyHistoryDao.getHistorySize(params);
    }

    @Override
    public Collection<String> getOwlClassesForCellarId(String cellarId, String inferredVersion) {
        if (!this.pidManagerService.isCellarIdentifierExists(cellarId) || this.pidManagerService.isManifestationContent(cellarId)) {
            return Collections.emptyList();
        }

        Model model = null;
        try {
            final String modelString = contentStreamServiceBridge.getContentAsString(cellarId, inferredVersion, ContentType.DIRECT_INFERRED).orElse("");
            if (!modelString.isEmpty()) {
                model = JenaUtils.read(modelString, Lang.NT);

                final Set<Resource> sameAsResources = this.getSameAsResourcesFromCellarId(model, cellarId);
                final Set<String> classURIs = this.getClassURIsOfResources(model, sameAsResources);
                return this.getClassURIsInAscendingOrder(classURIs);
            }
        } catch (final Exception exc) {
            LOG.error(MessageFormatter.format("An error occurred while fetching the ontology classes of object with cellar id {}.", cellarId), exc);
            return Collections.emptyList();
        } finally {
            JenaUtils.closeQuietly(model);
        }
        return Collections.emptyList();
    }

    /**
     * @see eu.europa.ec.opoce.cellar.feed.service.HistoryService#getProductionIdentifierNamesForCellarId(java.lang.String)
     */
    @Override
    public Collection<String> getProductionIdentifierNamesForCellarId(final String cellarId) {
        if (!this.pidManagerService.isCellarIdentifierExists(cellarId)) {
            return Collections.emptyList();
        }

        final Collection<String> piNames = new ArrayList<>();
        for (final ProductionIdentifier pi : this.productionIdentifierDao.getProductionIdentifiersByCellarId(cellarId)) {
            piNames.add(pi.getProductionId());
        }

        return piNames;
    }

    private Set<Resource> getSameAsResourcesFromCellarId(final Model model, final String cellarId) {
        final Resource cellarRes = model.createResource(this.pidManagerService.getURI(cellarId));
        final Property sameAsProp = model.createProperty(OWL.sameAs.getURI());
        final Set<Resource> sameAses = new HashSet<>();
        final NodeIterator iter = model.listObjectsOfProperty(cellarRes, sameAsProp);
        try {
            while (iter.hasNext()) {
                final Resource sameAsResource = iter.next().asResource();
                sameAses.add(sameAsResource);
            }
        } finally {
            iter.close();
        }
        return sameAses;
    }

    private Set<String> getClassURIsOfResources(final Model model, final Set<Resource> sameAsResources) {
        final Set<String> classURIs = new HashSet<>();
        final Property classProp = model.createProperty(RDF.type.getURI());
        for (final Resource resource : sameAsResources) {
            final NodeIterator iter = model.listObjectsOfProperty(resource, classProp);
            try {
                while (iter.hasNext()) {
                    final Resource classResource = iter.next().asResource();
                    classURIs.add(classResource.getURI());
                }
            } finally {
                iter.close();
            }
        }
        return classURIs;
    }

    private Collection<String> getClassURIsInAscendingOrder(final Set<String> classURIs) {
        final OntModel ontModel = ontologyBridge.getOntModel();

        final Map<Integer, String> ontClasses = new TreeMap<>();
        for (final String classURI : classURIs) {
            final Resource ontResource = ontModel.getResource(classURI);
            if (ontResource.canAs(OntClass.class)) {
                final OntClass ontClass = ontResource.as(OntClass.class);
                final ExtendedIterator<OntClass> iter = ontClass.listSuperClasses();
                try {
                    int superClassCount = 0;
                    while (iter.hasNext()) {
                        final OntClass superClass = iter.next();
                        if (classURIs.contains(superClass.getURI())) {
                            superClassCount++;
                        }
                    }
                    ontClasses.put(superClassCount, classURI);
                } finally {
                    iter.close();
                }
            }
        }

        final List<String> orderedResources = new ArrayList<>(ontClasses.values());
        Collections.reverse(orderedResources);

        return orderedResources;
    }

}
