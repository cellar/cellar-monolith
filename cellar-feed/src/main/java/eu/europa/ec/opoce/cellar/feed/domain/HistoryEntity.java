package eu.europa.ec.opoce.cellar.feed.domain;

import java.util.Date;

public interface HistoryEntity {

    Date getActionDate();

    void setActionDate(Date actionDate);
}
