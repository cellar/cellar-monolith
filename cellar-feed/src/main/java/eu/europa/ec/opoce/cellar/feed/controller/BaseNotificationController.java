/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.feed.controller
 *             FILE : BaseNotificationController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 1, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-01-02 11:32:44 +0100 (Mon, 02 Jan 2017) $
 *          VERSION : $LastChangedRevision: 12298 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.feed.controller;

import eu.europa.ec.opoce.cellar.cl.controller.AbstractCellarController;
import eu.europa.ec.opoce.cellar.cl.controller.util.ControllerUtil;
import eu.europa.ec.opoce.cellar.cl.exception.CellarWebException;
import eu.europa.ec.opoce.cellar.common.http.headers.HttpHeadersBuilder;
import eu.europa.ec.opoce.cellar.common.util.StringUtils;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.feed.controller.marshaller.NotificationFeedMarshaller;
import eu.europa.ec.opoce.cellar.feed.domain.Feed;
import eu.europa.ec.opoce.cellar.feed.domain.FeedItem;
import eu.europa.ec.opoce.cellar.feed.domain.HistoryEntity;
import eu.europa.ec.opoce.cellar.feed.domain.enums.FeedFormat;
import eu.europa.ec.opoce.cellar.feed.domain.enums.FeedItemType;
import eu.europa.ec.opoce.cellar.feed.domain.enums.FeedType;
import eu.europa.ec.opoce.cellar.feed.domain.impl.ingestion.IngestionHistory;
import eu.europa.ec.opoce.cellar.feed.service.HistoryService;
import eu.europa.ec.opoce.cellar.feed.util.ExceptionUtil;
import eu.europa.ec.opoce.cellar.feed.util.Params;
import eu.europa.ec.opoce.cellar.util.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * The base notification controller: all controllers aimed at notifying feeds must be based upon this class.
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 20 juin 2016
 *
 * @author ARHS Developments
 * @version $Revision: 12298 $
 */
@Component
public abstract class BaseNotificationController<HE extends HistoryEntity, FI extends FeedItem> extends AbstractCellarController {

    /**
     * The Constant PAGE_URL.
     */
    public static final String PAGE_URL = "/notification";

    @Autowired
    protected HistoryService historyService;

    @Autowired
    private NotificationFeedMarshaller notificationFeedBuilder;

    /**
     * Gets the notification.
     *
     * @param request     the request
     * @param startDate   the start date
     * @param endDate     the end date
     * @param actionTypes the types
     * @param priorities  the priority
     * @param wemiClasses the wemi classes
     * @param page        the page
     * @return the notification
     */
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<String> getNotification(final HttpServletRequest request,
                                                  @RequestParam(value = "startDate") final String startDate,
                                                  @RequestParam(required = false, value = "endDate") final String endDate,
                                                  @RequestParam(required = false, value = "type") final String actionTypes,
                                                  @RequestParam(required = false, value = "priority") final String priorities,
                                                  @RequestParam(required = false, value = "wemiClasses") final String wemiClasses,
                                                  @RequestParam(required = false, value = "page") final String page,
                                                  @RequestParam(required = false, value = "relatives") final Boolean relatives) {
        try {
            // resolve input params
            final FeedType feedType = this.resolveFeedType();
            final Params params = Params.getFor(feedType)
                    .startDate(startDate).endDate(endDate)
                    .actionTypes(actionTypes).priorities(priorities).wemiClasses(wemiClasses);

            // resolve page indexes
            final int parsedPage = resolvePage(page);
            final int pageSize = this.cellarConfiguration.getCellarServiceNotificationItemsPerPage();
            final int startIndex = (parsedPage * pageSize) - pageSize;

            // get paged history and items and total history size
            final Collection<HE> history = this.getHistory(params, startIndex, pageSize);
            final Long historySize = this.getHistorySize(params);

            // resolve feed
            final Feed<FI> feed = new Feed<>(feedType);
            feed.setStartDate(params.getStartDate());
            feed.setEndDate(params.getEndDate());
            feed.setPage(parsedPage);
            feed.setDescription(cellarConfiguration);
            feed.setTitle(cellarConfiguration);
            this.addFeedItemsAndMoreEntriesInfo(feed, history, parsedPage, historySize, pageSize, relatives);
            final String feedString = this.notificationFeedBuilder.marshal(parseFeedType(request), feed);

            // build response
            final HttpHeadersBuilder httpHeadersBuilder = HttpHeadersBuilder.get()
                    .withContentType(MediaType.TEXT_XML_VALUE + ";charset=UTF-8");
            return new ResponseEntity<>(feedString, httpHeadersBuilder.getHttpHeaders(), HttpStatus.OK);
        } catch (final CellarException exc) {
            return prepareErrorResponse(exc);
        }
    }

    protected abstract Collection<HE> getHistory(final Params params, final int startIndex, final int pageSize);

    protected abstract Long getHistorySize(final Params params);

    protected abstract FI createFeedItem(final HE historyElement);

    protected abstract FeedType resolveFeedType();

    private void addFeedItemsAndMoreEntriesInfo(final Feed<FI> feed, final Collection<HE> history, final int page,
                                                final Long historySize, final int pageSize, Boolean relatives) {
        final Collection<FI> feedItems = new ArrayList<>();

        for (final HE historyElement : history) {
            if(relatives != null && historyElement instanceof IngestionHistory) { //when URI parameter relatives is null present the whole feed
                IngestionHistory ingestionHistory = (IngestionHistory) historyElement; //cast it to IngestionHistory to get relatives

                if((Boolean.TRUE.equals(relatives) && !Boolean.TRUE.equals(ingestionHistory.isRelatives())) ||
                        (Boolean.FALSE.equals(relatives) && Boolean.TRUE.equals(ingestionHistory.isRelatives()))) {
                    //when URI parameter relatives is true then exclude items from feed that are directly modified (0, null)
                    //when URI parameter relatives is false exclude items from feed that are not directly modified, but related to modified objects (1)
                    continue;
                }
            }

            final FI feedItem = this.createFeedItem(historyElement);

            if (feedItem.getFeedItemType() != FeedItemType.no_feed) {
                feedItem.setTitle(cellarConfiguration,feed.getFeedType());
                feedItem.setEntrySummary(cellarConfiguration,feed.getFeedType());
                if(feedItem.getCellarId()!=null) {
                    String cellarUriSuffix = feedItem.getCellarId().replace(":", "/");
                    feedItem.setCellarUri(cellarConfiguration.getCellarUriDisseminationBase() + "resource/" + cellarUriSuffix);
                }
                feedItems.add(feedItem);
            }
        }
        feed.setFeedItems(feedItems);

        final int startIndex = (page * pageSize) - pageSize;
        final int endIndex = startIndex + history.size();
        final boolean hasMoreEntries = endIndex < historySize;
        feed.setHasMoreEntries(hasMoreEntries);
    }

    /**
     * Parses the feed type.
     *
     * @param request the request
     * @return the feed type
     */
    private static FeedFormat parseFeedType(final HttpServletRequest request) {
        final String acceptValue = request.getHeader("accept");
        if (StringUtils.equalsIgnoreCase(DateUtils.ACCEPT_RSS, acceptValue)) {
            return FeedFormat.RSS;
        } else if (StringUtils.equalsIgnoreCase(DateUtils.ACCEPT_ATOM, acceptValue)) {
            return FeedFormat.ATOM;
        } else if (StringUtils.isEmpty(acceptValue) || StringUtils.equals(acceptValue, DateUtils.ACCEPT_ALL)) {
            return FeedFormat.RSS;
        } else {
            ExceptionUtil.buildAndThrowBadRequest("Invalid accept header value: '{}'", acceptValue);
            return null;
        }
    }

    private static int resolvePage(final String page) {
        if (StringUtils.isEmpty(page)) {
            return 1;
        }

        int parsedPage = 0;
        try {
            parsedPage = Integer.parseInt(page);
            if (parsedPage <= 0) {
                throw new ParseException("Integer value too low", 0);
            }
        } catch (final ParseException pe) {
            ExceptionUtil.buildAndThrowBadRequest("Page parameter must be a positive integer >= 1, cannot deal with '{}'", page);
        }

        return parsedPage;
    }

    private static ResponseEntity<String> prepareErrorResponse(final CellarException exc) {
        CellarWebException cwe;
        if (exc instanceof CellarWebException) {
            cwe = (CellarWebException) exc;
        } else {
            cwe = HttpStatusAwareExceptionBuilder.getInstance(CellarWebException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage(exc.getMessage()).withCause(exc).build();
        }
        return ControllerUtil.makeErrorResponse(cwe, cwe.getHttpStatus());
    }

}
