/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.feed.controller.webapi
 *             FILE : IngestionNotificationController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 1, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-01-02 11:32:44 +0100 (Mon, 02 Jan 2017) $
 *          VERSION : $LastChangedRevision: 12298 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.feed.controller.webapi;

import eu.europa.ec.opoce.cellar.common.util.StringUtils;
import eu.europa.ec.opoce.cellar.feed.controller.BaseNotificationController;
import eu.europa.ec.opoce.cellar.feed.domain.enums.FeedPriority;
import eu.europa.ec.opoce.cellar.feed.domain.enums.FeedType;
import eu.europa.ec.opoce.cellar.feed.domain.impl.ingestion.IngestionFeedItem;
import eu.europa.ec.opoce.cellar.feed.domain.impl.ingestion.IngestionHistory;
import eu.europa.ec.opoce.cellar.feed.util.Params;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 * The notification controller for ingestions.
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 20 juin 2016
 *
 * @author ARHS Developments
 * @version $Revision: 12298 $
 */
@Controller
@RequestMapping(value = BaseNotificationController.PAGE_URL + "/ingestion")
public class IngestionNotificationController extends BaseNotificationController<IngestionHistory, IngestionFeedItem> {

    /**
     * @see eu.europa.ec.opoce.cellar.feed.controller.BaseNotificationController#getHistory(Params, int, int)
     */
    @Override
    protected Collection<IngestionHistory> getHistory(final Params params, final int startIndex, final int pageSize) {
        return this.historyService.getIngestionHistory(params, startIndex, pageSize);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.feed.controller.BaseNotificationController#getHistorySize(Params)
     */
    @Override
    protected Long getHistorySize(final Params params) {
        return this.historyService.getIngestionHistorySize(params);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.feed.controller.BaseNotificationController#createFeedItem(eu.europa.ec.opoce.cellar.feed.domain.HistoryEntity)
     */
    @Override
    protected IngestionFeedItem createFeedItem(final IngestionHistory historyElement) {
        IngestionFeedItem retItem = new IngestionFeedItem();
        retItem.setId(historyElement.getId().toString());
        retItem.setCellarId(historyElement.getCellarId());
        retItem.setRootCellarId(historyElement.getRootCellarId());
        retItem.setFeedItemType(historyElement.getFeedActionType());
        retItem.setPriority(historyElement.getPriority().equals(FeedPriority.EMBARGO) ? FeedPriority.NA : historyElement.getPriority());
        retItem.setCreationDate(historyElement.getActionDate());
        retItem.setWemiClass(historyElement.getWemiClass());
        retItem.setType(historyElement.getType());
        retItem.setClasses(historyElement.getClasses() != null ?
                Arrays.asList(StringUtils.split(historyElement.getClasses(), ',')) :
                Collections.emptyList());
        retItem.setIdentifiers(historyElement.getIdentifiers() != null ?
                Arrays.asList(StringUtils.split(historyElement.getIdentifiers(), ',')) :
                Collections.emptyList());

        // the rootIdentifiers must be always calculated on-the-fly. However, their footprint on the performances is negligible
        // as opposed to that of the classes (1 part in 1500, in average), so it is not a big deal to keep them this way
        retItem.setRootIdentifiers(this.historyService.getProductionIdentifierNamesForCellarId(historyElement.getRootCellarId()));
        return retItem;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.feed.controller.BaseNotificationController#resolveFeedType()
     */
    @Override
    protected FeedType resolveFeedType() {
        return FeedType.Ingestion;
    }

}
