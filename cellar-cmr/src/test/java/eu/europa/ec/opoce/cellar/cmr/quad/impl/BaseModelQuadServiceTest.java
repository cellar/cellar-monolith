package eu.europa.ec.opoce.cellar.cmr.quad.impl;

/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.quad.impl
 *             FILE : BaseModelQuadServiceTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 8, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2016-09-01 17:46:05 +0200 (Thu, 01 Sep 2016) $
 *          VERSION : $LastChangedRevision: 11526 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */

import com.google.common.collect.Lists;
import eu.europa.ec.opoce.cellar.common.util.Pair;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static eu.europa.ec.opoce.cellar.cmr.quad.impl.BaseModelQuadService.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 8, 2017
 *
 * @author ARHS Developments
 * @version $Revision: 11526 $
 */
public class BaseModelQuadServiceTest {

    private static List<EmbargoableObject> embargoableObjects;

    @BeforeClass
    public static void initTestClass() throws Exception {
        embargoableObjects = Lists.newArrayList(
                ObjectBuilder.get("w").build(),
                ObjectBuilder.get("e1").withFromPrivate(true).build(),
                ObjectBuilder.get("m11").build(),
                ObjectBuilder.get("m12").withMetadata(null).build(),
                ObjectBuilder.get("e2").build(),
                ObjectBuilder.get("m21").withFromPrivate(true).build(),
                ObjectBuilder.get("m22").withPrivates(true, true).build(),
                ObjectBuilder.get("m23").withPrivates(false, false).build()
        );
    }

    @Test
    @SuppressWarnings("Duplicates")
    public void testRemovableObjects() throws Exception {
        final Map<Boolean, List<String>> collectedMap = collectRemovableObjects(embargoableObjects);
        assertTrue(checkMapSize(collectedMap));

        collectedMap.forEach((isFromPrivate, cellarIds) -> {
            if (isFromPrivate) {
                assertFalse(cellarIds.contains("w"));
                assertTrue(cellarIds.contains("e1"));
                assertFalse(cellarIds.contains("m11"));
                assertFalse(cellarIds.contains("m12"));
                assertFalse(cellarIds.contains("e2"));
                assertTrue(cellarIds.contains("m21"));
                assertTrue(cellarIds.contains("m22"));
                assertFalse(cellarIds.contains("m23"));
            } else {
                assertTrue(cellarIds.contains("w"));
                assertFalse(cellarIds.contains("e1"));
                assertTrue(cellarIds.contains("m11"));
                assertFalse(cellarIds.contains("m12"));
                assertTrue(cellarIds.contains("e2"));
                assertFalse(cellarIds.contains("m21"));
                assertFalse(cellarIds.contains("m22"));
                assertTrue(cellarIds.contains("m23"));
            }
        });
    }

    @Test
    @SuppressWarnings("Duplicates")
    public void testMovableObjects() throws Exception {
        final Map<Boolean, List<String>> collectedMap = collectMovableObjects(embargoableObjects);
        assertTrue(checkMapSize(collectedMap));

        collectedMap.forEach((isFromPrivate, cellarIds) -> {
            if (isFromPrivate) {
                assertFalse(cellarIds.contains("w"));
                assertTrue(cellarIds.contains("e1"));
                assertFalse(cellarIds.contains("m11"));
                assertFalse(cellarIds.contains("m12"));
                assertFalse(cellarIds.contains("e2"));
                assertTrue(cellarIds.contains("m21"));
                assertFalse(cellarIds.contains("m22"));
                assertFalse(cellarIds.contains("m23"));
            } else {
                assertTrue(cellarIds.contains("w"));
                assertFalse(cellarIds.contains("e1"));
                assertTrue(cellarIds.contains("m11"));
                assertTrue(cellarIds.contains("m12"));
                assertTrue(cellarIds.contains("e2"));
                assertFalse(cellarIds.contains("m21"));
                assertFalse(cellarIds.contains("m22"));
                assertFalse(cellarIds.contains("m23"));
            }
        });
    }

    @Test
    @SuppressWarnings("Duplicates")
    public void testInsertableObjectsWithCellarId() throws Exception {
        final Map<Boolean, List<EmbargoableObject>> collectedMap = collectInsertableObjects(embargoableObjects);
        assertTrue(checkMapSize(collectedMap));

        collectedMap.forEach((isToPrivate, pairsList) -> {
            final List<String> cellarIds = pairsList.stream().map(EmbargoableObject::getCellarId).collect(Collectors.toList());
            if (isToPrivate) {
                assertTrue(cellarIds.contains("w"));
                assertFalse(cellarIds.contains("e1"));
                assertTrue(cellarIds.contains("m11"));
                assertFalse(cellarIds.contains("m12"));
                assertTrue(cellarIds.contains("e2"));
                assertFalse(cellarIds.contains("m21"));
                assertTrue(cellarIds.contains("m22"));
                assertFalse(cellarIds.contains("m23"));
            } else {
                assertFalse(cellarIds.contains("w"));
                assertTrue(cellarIds.contains("e1"));
                assertFalse(cellarIds.contains("m11"));
                assertFalse(cellarIds.contains("m12"));
                assertFalse(cellarIds.contains("e2"));
                assertTrue(cellarIds.contains("m21"));
                assertFalse(cellarIds.contains("m22"));
                assertTrue(cellarIds.contains("m23"));
            }
        });
    }

    @Test
    @SuppressWarnings("Duplicates")
    public void testInsertableObjectsWithQuads() throws Exception {
        final Map<Boolean, List<EmbargoableObject>> collectedMap = collectInsertableObjects(embargoableObjects);
        assertTrue(checkMapSize(collectedMap));

        collectedMap.forEach((isToPrivate, pairsList) -> {
            final List<String> quadTwos = pairsList.stream().flatMap(l -> l.getQuads().stream()).map(Pair::getTwo).collect(Collectors.toList());
            if (isToPrivate) {
                assertTrue(quadTwos.contains("w"));
                assertFalse(quadTwos.contains("e1"));
                assertTrue(quadTwos.contains("m11"));
                assertFalse(quadTwos.contains("m12"));
                assertTrue(quadTwos.contains("e2"));
                assertFalse(quadTwos.contains("m21"));
                assertTrue(quadTwos.contains("m22"));
                assertFalse(quadTwos.contains("m23"));
            } else {
                assertFalse(quadTwos.contains("w"));
                assertTrue(quadTwos.contains("e1"));
                assertFalse(quadTwos.contains("m11"));
                assertFalse(quadTwos.contains("m12"));
                assertFalse(quadTwos.contains("e2"));
                assertTrue(quadTwos.contains("m21"));
                assertFalse(quadTwos.contains("m22"));
                assertTrue(quadTwos.contains("m23"));
            }
        });
    }

    private static <T> boolean checkMapSize(final Map<Boolean, List<T>> collectedMap) {
        return collectedMap.size() == 2 &&
                !collectedMap.get(true).isEmpty() &&
                !collectedMap.get(false).isEmpty();
    }

    private static class ObjectBuilder {

        private final String cellarId;
        private Model metadata;
        private boolean fromPrivate;
        private boolean toPrivate;

        private ObjectBuilder(final String cellarId) {
            this.cellarId = cellarId;
            this.metadata = ModelFactory.createDefaultModel();
            this.metadata.add(
                    this.metadata.createResource(cellarId),
                    this.metadata.createProperty("p"),
                    this.metadata.createResource("0")
            );
            this.fromPrivate = false;
            this.toPrivate = true;
        }

        static ObjectBuilder get(final String cellarId) {
            return new ObjectBuilder(cellarId);
        }

        ObjectBuilder withMetadata(final Model metadata) {
            this.metadata = metadata;
            return this;
        }

        ObjectBuilder withFromPrivate(final boolean fromPrivate) {
            this.fromPrivate = fromPrivate;
            this.toPrivate = !fromPrivate;
            return this;
        }

        ObjectBuilder withToPrivate(final boolean toPrivate) {
            this.toPrivate = toPrivate;
            this.fromPrivate = !toPrivate;
            return this;
        }

        ObjectBuilder withPrivates(final boolean fromPrivate, final boolean toPrivate) {
            this.fromPrivate = fromPrivate;
            this.toPrivate = toPrivate;
            return this;
        }

        EmbargoableObject build() {
            final EmbargoableObject retObject = new EmbargoableObject(this.cellarId, null);
            retObject.setMetadata(this.metadata);
            retObject.setFromPrivate(this.fromPrivate);
            retObject.setToPrivate(this.toPrivate);
            return retObject;
        }

    }

}
