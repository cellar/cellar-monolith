/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.nal.service.impl
 *             FILE : NalApiServiceTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 03 14, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-03-14 07:32:55 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.nal.service.impl;

import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.nal.NalOntoUtils;
import eu.europa.ec.opoce.cellar.nal.service.JenaGatewayNalApi;
import eu.europa.ec.opoce.cellar.nal.service.LanguageService;
import eu.europa.ec.opoce.cellar.nal.service.NalConfigurationService;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.junit.Before;
import org.junit.Test;

import static eu.europa.ec.opoce.cellar.nal.NalOntoUtils.AUTHORITY_ONTOLOGY_URI;
import static eu.europa.ec.opoce.cellar.nal.NalOntoUtils.NAL_LANGUAGE_URI;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

/**
 * @author ARHS Developments
 */
public class NalApiServiceTest {
    private final static String EMPTY_MODELSTR = "" +
            "<?xml version=\"1.0\"?>\n" +
            "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"></rdf:RDF>";
    private JenaGatewayNalApi jenaGatewayNalApi;
    private NalConfigurationService nalConfigurationService;
    private NalApiService nalApiService;

    @Before
    public void setUp() throws Exception {
        LanguageService languageService = mock(LanguageServiceImpl.class);
        jenaGatewayNalApi = mock(JenaGatewayNalApiImpl.class);
        nalConfigurationService = mock(NalConfigurationServiceImpl.class);
        when(nalConfigurationService.getNal(anyString(), anyBoolean())).thenReturn(EMPTY_MODELSTR);
        when(nalConfigurationService.getAllNalModel(anyBoolean(), eq(AUTHORITY_ONTOLOGY_URI))).thenReturn(ModelFactory.createDefaultModel());
        nalApiService = new NalApiService(jenaGatewayNalApi, languageService, nalConfigurationService);
    }

    @Test(expected = CellarException.class)
    public void conceptSchemeArgCannotBeBlank() throws Exception {
        nalApiService.getConceptScheme(null);
        nalApiService.getConceptScheme("");
    }

    @Test
    public void testRetrievalConceptScheme() throws Exception {
        nalApiService.getConceptScheme(NalOntoUtils.NAL_LANGUAGE_URI);
        verify(nalConfigurationService, times(1)).getNalModel(NalOntoUtils.NAL_LANGUAGE_URI, true);
        verify(jenaGatewayNalApi, times(1))
                .getConceptScheme(nullable(Model.class), eq(NalOntoUtils.getBaseUri(NalOntoUtils.NAL_LANGUAGE_URI)));
    }

    @Test
    public void allNalForAuthorityOntoAreRetrievedToGetAllConceptSchemes() throws Exception {
        nalApiService.getConceptSchemes(null);
        verify(nalConfigurationService, times(1)).getAllNalModel(true, AUTHORITY_ONTOLOGY_URI);
        verify(jenaGatewayNalApi, times(1)).getConceptSchemesOneQuery(any(Model.class), any());
    }

    @Test(expected = CellarException.class)
    public void topConceptsArgsValidation() throws Exception {
        nalApiService.getTopConcepts(null, null);
        nalApiService.getTopConcepts("", "");
        nalApiService.getTopConcepts("", null);
        nalApiService.getTopConcepts(null, "");
    }

    @Test
    public void testRetrievalTopConcepts() throws Exception {
        nalApiService.getTopConcepts(NAL_LANGUAGE_URI, "eng");
        verify(jenaGatewayNalApi, times(1)).getTopConcepts(nullable(Model.class), eq(NAL_LANGUAGE_URI), eq("eng"));
    }

    @Test(expected = CellarException.class)
    public void conceptRelativeArgsValidation() throws Exception {
        nalApiService.getConceptRelatives(null, null, null);
        nalApiService.getConceptRelatives("", "", "");
        nalApiService.getConceptRelatives(null, null, "");
        nalApiService.getConceptRelatives(null, "", null);
        nalApiService.getConceptRelatives("", null, null);
        nalApiService.getConceptRelatives(null, "", "");
        nalApiService.getConceptRelatives("", "", null);
        nalApiService.getConceptRelatives("", null, "");
    }

    @Test
    public void testRetrievalConceptRelatives() throws Exception {
        nalApiService.getConceptRelatives(NAL_LANGUAGE_URI, "relation", "eng");
        verify(jenaGatewayNalApi, times(1))
                .getRelatedConceptsOneQuery(nullable(Model.class), eq(NAL_LANGUAGE_URI), anyString(), eq("eng"));
    }

    @Test(expected = CellarException.class)
    public void conceptArgsValidation() throws Exception {
        nalApiService.getConcept(null, null);
        nalApiService.getConcept(null, "");
        nalApiService.getConcept("", null);
        nalApiService.getConcept("", "");
    }

    @Test
    public void testConceptRetrieval() throws Exception {
        nalApiService.getConcept(NAL_LANGUAGE_URI, "eng");
        verify(jenaGatewayNalApi, times(1)).getConcept(nullable(Model.class),
                eq(NAL_LANGUAGE_URI), eq("eng"));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void domainsAreNotValidForAuthorityNal() throws Exception {
        nalApiService.getDomains();
    }
}