package eu.europa.ec.opoce.cellar.virtuoso;

import eu.europa.ec.opoce.cellar.cmr.model.ModelUtils;
import org.apache.commons.lang.CharEncoding;
import org.apache.jena.datatypes.TypeMapper;
import org.apache.jena.rdf.model.*;
import org.springframework.util.Assert;
import virtuoso.jdbc4.VirtuosoDataSource;
import virtuoso.jena.driver.VirtDataset;

public class VirtuosoTestDate {

    private static VirtuosoDataSource virtuosoDataSource;

    public static void main(String[] args) throws Exception {
        final String graphName = "http://cellar-dev.publications.europa.eu/resource/cellar/BLABLABLA";
        final Resource sIn = ResourceFactory.createResource("http://cellar-dev.publications.europa.eu/resource/oj/JOA_1958_007_R_TOC");
        final Property pIn = ResourceFactory.createProperty("http://publications.europa.eu/ontology/cdm#work_date_creation_legacy");
        final Literal oIn = ResourceFactory.createTypedLiteral("2101-01-01",
                TypeMapper.getInstance().getTypeByName("http://www.w3.org/2001/XMLSchema#date"));

        write(graphName, sIn, pIn, oIn);
        final Literal oOut = readModel(graphName, sIn, pIn, null).listStatements().next().getObject().asLiteral();
        Assert.isTrue(oIn.getString().equals(oOut.getString()));
    }

    private static VirtuosoDataSource getVirtuosoDataSource() throws Exception {
        if (virtuosoDataSource != null) {
            return virtuosoDataSource;
        }

        final String serverName = "10.2.0.244";
        final int serverPort = 1114;
        virtuosoDataSource = new VirtuosoDataSource();
        virtuosoDataSource.setUser("dba");
        virtuosoDataSource.setPassword("dba");
        virtuosoDataSource.setDataSourceName("jdbc:virtuoso://" + serverName + ":" + serverPort + "/charset=UTF-8");
        virtuosoDataSource.setServerName(serverName);
        virtuosoDataSource.setPortNumber(serverPort);
        virtuosoDataSource.setCharset(CharEncoding.UTF_8);
        return virtuosoDataSource;
    }

    private static void write(final String graphName, final Resource s, final Property p, final RDFNode o) throws Exception {
        VirtDataset dataset = null;
        try {
            // gets the connection
            dataset = new VirtDataset(getVirtuosoDataSource());

            // get direct model of digital objects that are supposed to be inserted
            Model existingModel = null;
            Model newModel = null;
            try {
                // load existing model
                existingModel = dataset.getNamedModel(graphName);
                if (existingModel == null) {
                    existingModel = ModelFactory.createDefaultModel();
                    dataset.addNamedModel(graphName, existingModel);
                }

                // update new model
                newModel = ModelFactory.createDefaultModel();
                newModel.add(ResourceFactory.createStatement(s, p, o));
                existingModel.removeAll();
                existingModel.add(newModel);
            } finally {
                ModelUtils.closeQuietly(existingModel, newModel);
            }
        } finally {
            if (dataset != null) {
                dataset.close();
            }
        }
    }

    private static Model readModel(final String graphName, final Resource s, final Property p, final RDFNode o) throws Exception {
        final Model retModel = ModelFactory.createDefaultModel();

        VirtDataset dataset = null;
        Model existingModel = null;
        try {
            dataset = new VirtDataset(getVirtuosoDataSource());
            existingModel = dataset.getNamedModel(graphName);
            if (existingModel != null) {
                retModel.add(existingModel.listStatements(s, p, o).toList());
            }
        } finally {
            ModelUtils.closeQuietly(existingModel);
            if (dataset != null) {
                dataset.close();
            }
        }

        return retModel;
    }

}
