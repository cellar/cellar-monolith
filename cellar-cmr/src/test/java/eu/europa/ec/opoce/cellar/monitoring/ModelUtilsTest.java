/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.model
 *             FILE : ModelUtilsTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 09 07, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-09-07 09:01:57 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.monitoring;

import eu.europa.ec.opoce.cellar.cl.configuration.PrefixConfigurationService;
import eu.europa.ec.opoce.cellar.cmr.model.ModelUtils;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import eu.europa.ec.opoce.cellar.common.util.Pair;
import eu.europa.ec.opoce.cellar.domain.content.mets.*;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.HierarchyProperty.*;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.cdm_item_identifierP;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.cmr_manifestationMimeTypeP;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarType.cdm_itemR;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * @author ARHS Developments
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(ServiceLocator.class)
@PowerMockIgnore({"org.w3c.*", "org.xml.sax.*", "javax.xml.*", "javax.management.*"})
public class ModelUtilsTest {

    private static final String BASE_URI = "http://cellar-dev.publications.europa.eu/resource/";
    public static final String APPLICATION_XML_TYPE_FMX_3 = "application/xml;type=fmx3";

    @Before
    @SuppressWarnings("Duplicates")
    public void setupClass() throws Exception {
        PrefixConfiguration prefixConfiguration = mock(PrefixConfiguration.class);
        when(prefixConfiguration.getPrefixUri("cellar")).thenReturn(BASE_URI + "cellar");
        when(prefixConfiguration.getPrefixUri("celex")).thenReturn(BASE_URI + "celex");
        when(prefixConfiguration.getPrefixUri("oj")).thenReturn(BASE_URI + "oj");

        PowerMockito.mockStatic(ServiceLocator.class);
        when(ServiceLocator.getService(PrefixConfigurationService.class)).thenReturn(mock(PrefixConfigurationService.class));
        when(ServiceLocator.getService(PrefixConfigurationService.class).getPrefixConfiguration()).thenReturn(prefixConfiguration);
    }

    @Test
    public void testRenewItemTriples() {
        System.out.println(Lang.RDFXML.getLabel());
        // create package
        final Pair<DigitalObject, Resource> work = createDigitalObjectAndResource("oj:JOL_2006_088_R_0063_01", DigitalObjectType.WORK);

        final Pair<DigitalObject, Resource> expr = createDigitalObjectAndResource("oj:JOL_2006_088_R_0063_01.fra", DigitalObjectType.EXPRESSION);
        work.getOne().addChildObject(expr.getOne());

        Pair<DigitalObject, Resource> manf = createDigitalObjectAndResource("oj:JOL_2006_088_R_0063_01.fra.FORMEX", DigitalObjectType.MANIFESTATION);
        expr.getOne().addChildObject(manf.getOne());

        final Pair<ContentStream, Resource> cs1 = createContentStreamAndResource("oj:JOL_2006_088_R_0063_01.fra.FORMEX.1", APPLICATION_XML_TYPE_FMX_3);
        manf.getOne().addContentStream(cs1.getOne());
        final Pair<ContentStream, Resource> cs2 = createContentStreamAndResource("oj:JOL_2006_088_R_0063_01.fra.FORMEX.2", APPLICATION_XML_TYPE_FMX_3);
        manf.getOne().addContentStream(cs2.getOne());
        final Pair<ContentStream, Resource> cs3 = createContentStreamAndResource("oj:JOL_2006_088_R_0063_01.fra.FORMEX.3", APPLICATION_XML_TYPE_FMX_3);
        manf.getOne().addContentStream(cs3.getOne());

        final Model model = ModelFactory.createDefaultModel();
        model.add(expr.getTwo(), cdm_expression_belongs_to_workP, work.getTwo());
        model.add(manf.getTwo(), cdm_manifestation_manifests_expressionP, expr.getTwo());

        ModelUtils.renewItemTriples(model, work.getOne());

        // update package
        expr.getOne().getChildObjects().remove(manf.getOne());
        manf = createDigitalObjectAndResource("oj:JOL_2006_088_R_0063_01.fra.FORMEX", DigitalObjectType.MANIFESTATION);
        expr.getOne().addChildObject(manf.getOne());

        manf.getOne().addContentStream(cs1.getOne());
        final Pair<ContentStream, Resource> cs4 = createContentStreamAndResource("oj:JOL_2006_088_R_0063_01.fra.FORMEX.4", APPLICATION_XML_TYPE_FMX_3);
        manf.getOne().addContentStream(cs4.getOne());

        ModelUtils.renewItemTriples(model, work.getOne());

        // check
        checkContentStreamInModel(cs1, manf, model, true);
        checkContentStreamInModel(cs2, manf, model, false);
        checkContentStreamInModel(cs3, manf, model, false);
        checkContentStreamInModel(cs4, manf, model, true);
    }

    private static void checkContentStreamInModel(final Pair<ContentStream, Resource> contentStream, final Pair<DigitalObject, Resource> manifestation, final Model model, final boolean contains) {
        Assert.assertTrue(contains == model.contains(contentStream.getTwo(), cdm_item_belongs_to_manifestationP, manifestation.getTwo()));
        Assert.assertTrue(contains == model.contains(contentStream.getTwo(), cdm_item_identifierP, contentStream.getOne().getCellarId().getIdentifier()));
        Assert.assertTrue(contains == model.contains(contentStream.getTwo(), cmr_manifestationMimeTypeP, APPLICATION_XML_TYPE_FMX_3));
        Assert.assertTrue(contains == model.contains(contentStream.getTwo(), RDF.type, cdm_itemR));
    }

    private static void checkSameAsItemInModel(Model model,Resource cs,boolean contains){
        Assert.assertTrue(contains==model.contains(cs,OWL.sameAs));
        }


    private static Pair<DigitalObject, Resource> createDigitalObjectAndResource(final String id, final DigitalObjectType type) {
        final DigitalObject myDo = new DigitalObject(null);
        myDo.setType(type);
        final ContentIdentifier ci = new ContentIdentifier(id);
        myDo.setCellarId(ci);

        final Resource resource = ResourceFactory.createResource(myDo.getCellarId().getUri());

        return new Pair<>(myDo, resource);
    }

    private static Pair<ContentStream, Resource> createContentStreamAndResource(final String id, final String mimetype) {
        final ContentStream cs = new ContentStream(mimetype, null);
        cs.setCellarId(new ContentIdentifier(id));

        final Resource resource = ResourceFactory.createResource(cs.getCellarId().getUri());

        return new Pair<>(cs, resource);
    }



    @Test
    public void testRemoveItemTriples(){
        System.out.println(Lang.RDFXML.getLabel());
        // create package
        final Pair<DigitalObject, Resource> work = createDigitalObjectAndResource("oj:JOL_2006_088_R_0063_01", DigitalObjectType.WORK);

        final Pair<DigitalObject, Resource> expr = createDigitalObjectAndResource("oj:JOL_2006_088_R_0063_01.fra", DigitalObjectType.EXPRESSION);
        work.getOne().addChildObject(expr.getOne());

        Pair<DigitalObject, Resource> manf = createDigitalObjectAndResource("oj:JOL_2006_088_R_0063_01.fra.FORMEX", DigitalObjectType.MANIFESTATION);
        expr.getOne().addChildObject(manf.getOne());

        final Pair<ContentStream, Resource> cs1 = createContentStreamAndResource("oj:JOL_2006_088_R_0063_01.fra.FORMEX.1", APPLICATION_XML_TYPE_FMX_3);
        manf.getOne().addContentStream(cs1.getOne());
        final Pair<ContentStream, Resource> cs2 = createContentStreamAndResource("oj:JOL_2006_088_R_0063_01.fra.FORMEX.2", APPLICATION_XML_TYPE_FMX_3);
        manf.getOne().addContentStream(cs2.getOne());
        final Pair<ContentStream, Resource> cs3 = createContentStreamAndResource("oj:JOL_2006_088_R_0063_01.fra.FORMEX.3", APPLICATION_XML_TYPE_FMX_3);
        manf.getOne().addContentStream(cs3.getOne());
        final Pair<ContentStream, Resource> cs4 = createContentStreamAndResource("oj:JOL_2006_088_R_0063_01.fra.FORMEX.4", APPLICATION_XML_TYPE_FMX_3);
        manf.getOne().addContentStream(cs4.getOne());

        final Model model = ModelFactory.createDefaultModel();
        model.add(expr.getTwo(), cdm_expression_belongs_to_workP, work.getTwo());
        model.add(manf.getTwo(), cdm_manifestation_manifests_expressionP, expr.getTwo());

        work.getOne().getAllChilds(true).stream()
                .filter(digitalObject -> digitalObject.getType().equals(DigitalObjectType.MANIFESTATION))
                .forEach(manifestationDo -> {
                            // for each item, add back the item-related triples
                            final Resource manifestation = ResourceFactory.createResource(manifestationDo.getCellarId().getUri());
                            for (final ContentStream doContentStream : manifestationDo.getContentStreams()) {
                                final Resource contentStream = ResourceFactory.createResource(doContentStream.getCellarId().getUri());
                                model.add(contentStream, cdm_item_belongs_to_manifestationP, manifestation);
                                model.add(contentStream, cdm_item_identifierP, doContentStream.getCellarId().getIdentifier()
                                        .substring(doContentStream.getCellarId().getIdentifier().lastIndexOf("/") + 1));
                                model.add(contentStream, cmr_manifestationMimeTypeP, doContentStream.getMimeType());
                                model.add(contentStream, RDF.type, cdm_itemR);
                                model.add(contentStream, OWL.sameAs,"oj:JOL_2006_088_R_0063_01.fra.FORMEX.sameAsCsTest");
                            }
                        }
                );
        //Check if model has item-related triples

        checkContentStreamInModel(cs1, manf, model, true);
        checkContentStreamInModel(cs2, manf, model, true);
        checkContentStreamInModel(cs3, manf, model, true);
        checkContentStreamInModel(cs4, manf, model, true);

        checkSameAsItemInModel(model,cs1.getTwo(),true);
        checkSameAsItemInModel(model,cs2.getTwo(),true);
        checkSameAsItemInModel(model,cs3.getTwo(),true);
        checkSameAsItemInModel(model,cs4.getTwo(),true);

        ModelUtils.removeItemTriples(model);

        //Check that the item-related triples were correctly removed

        checkContentStreamInModel(cs1, manf, model, false);
        checkContentStreamInModel(cs2, manf, model, false);
        checkContentStreamInModel(cs3, manf, model, false);
        checkContentStreamInModel(cs4, manf, model, false);

        checkSameAsItemInModel(model,cs1.getTwo(),false);
        checkSameAsItemInModel(model,cs1.getTwo(),false);
        checkSameAsItemInModel(model,cs1.getTwo(),false);
        checkSameAsItemInModel(model,cs1.getTwo(),false);


    }

}
