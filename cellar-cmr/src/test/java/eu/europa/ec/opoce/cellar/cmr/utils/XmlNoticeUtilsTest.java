package eu.europa.ec.opoce.cellar.cmr.utils;

import com.google.common.collect.Sets;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.OWL;
import org.junit.Test;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class XmlNoticeUtilsTest {

    @Test
    public void tagizeTest() {
        final Property simpleProperty = ResourceFactory.createProperty("http://localhost/property");
        final Property propertyWithAnchor = ResourceFactory.createProperty("http://localhost/path#property");

        assertEquals("PROPERTY", XmlNoticeUtils.tagize(simpleProperty));
        assertEquals("PROPERTY", XmlNoticeUtils.tagize(propertyWithAnchor));
    }

    @Test
    public void decoreUriTest() {
        final String uri = XmlNoticeUtils.decodeUri("http://myUrl?param1=value1&param2=value2%21");
        assertEquals("http://myUrl?param1=value1&param2=value2!", uri);
    }

    @Test
    public void getSameAsResourcesTest() {
        final String resourceUri = "http://localhost/resource/1";
        final String sameAsUri1 = "http://localhost/resource/sameAs1";
        final String sameAsUri2 = "http://localhost/resource/sameAs2";

        final Resource resource = ResourceFactory.createResource(resourceUri);
        final Resource sameAs1 = ResourceFactory.createResource(sameAsUri1);
        final Resource sameAs2 = ResourceFactory.createResource(sameAsUri2);

        final Model model = ModelFactory.createDefaultModel();
        model.add(resource, OWL.sameAs, sameAs1);
        model.add(resource, OWL.sameAs, sameAs2);

        final Set<Resource> sameAses = XmlNoticeUtils.getSameAsResources(model.getResource(resourceUri));
        final List<String> sameAsUris = sameAses.stream()
                .map(Resource::getURI)
                .sorted()
                .collect(Collectors.toList());
        assertEquals(2, sameAsUris.size());
        assertEquals(sameAsUri1, sameAsUris.get(0));
        assertEquals(sameAsUri2, sameAsUris.get(1));
    }

    @Test
    public void getAllStatementsTest() {
        final String resourceUri = "http://localhost/resource/1";
        final String sameAsUri1 = "http://localhost/resource/sameAs1";
        final String sameAsUri2 = "http://localhost/resource/sameAs2";

        final Resource resource = ResourceFactory.createResource(resourceUri);
        final Resource sameAs1 = ResourceFactory.createResource(sameAsUri1);
        final Resource sameAs2 = ResourceFactory.createResource(sameAsUri2);

        final Model model = ModelFactory.createDefaultModel();
        model.add(resource, OWL.sameAs, sameAs1);
        model.add(sameAs1, OWL.versionInfo, "value1");
        model.add(resource, OWL.sameAs, sameAs2);
        model.add(sameAs2, OWL.versionInfo, "value2");

        final Set<Statement> statements = XmlNoticeUtils.getAllStatements(
                model.getResource(resourceUri),
                Sets.newHashSet(model.getResource(sameAsUri1), model.getResource(sameAsUri2))
        );
        assertEquals(4, statements.size());
    }

    @Test
    public void getResourceFromModelTest() {
        final String cellarUri = "http://localhost/resource/cellar";
        final String sameAsUri = "http://localhost/resource/sameAs";

        final Resource cellarResource = ResourceFactory.createResource(cellarUri);
        final Resource sameAsResource = ResourceFactory.createResource(sameAsUri);

        final Model model = ModelFactory.createDefaultModel();
        model.add(cellarResource, OWL.sameAs, sameAsResource);

        final Resource resource = XmlNoticeUtils.getResourceFromModel(cellarResource, model);
        assertEquals(cellarUri, resource.getURI());
    }

}