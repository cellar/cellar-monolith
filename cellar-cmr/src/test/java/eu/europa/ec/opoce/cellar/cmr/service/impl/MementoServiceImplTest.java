package eu.europa.ec.opoce.cellar.cmr.service.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.service.IMementoQueryConfiguration;
import eu.europa.ec.opoce.cellar.cmr.sparql.utils.QueryUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.util.Assert;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(QueryUtils.class)
public class MementoServiceImplTest {

    private static final String CELLAR_URI = "http://cellar-dev.publications.europa.eu/resource/oj/JOL_2012_000_R_0000";
    private static final String QUERY = "QUERY";
    private static final String SPARQL_URI = "http://localhost:8890/sparql";

    @Mock
    private ICellarConfiguration cellarConfiguration;

    @Mock
    private IdentifierService identifierService;

    @Mock
    private IMementoQueryConfiguration mementoQueryConfiguration;

    private MementoServiceImpl mementoService;

    @Before
    public void setup() {
        PowerMockito.mockStatic(QueryUtils.class);
        mementoService = new MementoServiceImpl(cellarConfiguration, identifierService, mementoQueryConfiguration);
    }

    @Test
    public void isEvolutiveWorkTest() {
        when(mementoQueryConfiguration.getQueryForEvolutiveWork(any())).thenReturn(QUERY);
        when(cellarConfiguration.getCellarServiceSparqlUri()).thenReturn(SPARQL_URI);

        when(QueryUtils.executeSelectQuery(SPARQL_URI, QUERY)).thenReturn(Collections.emptyList());
        Assert.isTrue(!mementoService.isEvolutiveWork(CELLAR_URI));

        when(QueryUtils.executeSelectQuery(SPARQL_URI, QUERY)).thenReturn(Collections.singletonList(null));
        Assert.isTrue(mementoService.isEvolutiveWork(CELLAR_URI));
    }

    @Test
    public void isEvolutiveWorkOrMementoTest() {
        when(mementoQueryConfiguration.getQueryForEvolutiveWorkOrMemento(any())).thenReturn(QUERY);
        when(cellarConfiguration.getCellarServiceSparqlUri()).thenReturn(SPARQL_URI);

        when(QueryUtils.executeSelectQuery(SPARQL_URI, QUERY)).thenReturn(Collections.emptyList());
        Assert.isTrue(!mementoService.isEvolutiveWorkOrMemento(CELLAR_URI));

        when(QueryUtils.executeSelectQuery(SPARQL_URI, QUERY)).thenReturn(Collections.singletonList(null));
        Assert.isTrue(mementoService.isEvolutiveWorkOrMemento(CELLAR_URI));
    }
}