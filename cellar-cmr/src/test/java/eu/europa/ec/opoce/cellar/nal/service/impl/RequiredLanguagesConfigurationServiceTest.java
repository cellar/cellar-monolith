/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.nal.service.impl
 *             FILE : RequiredLanguagesConfigurationServiceTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 03 14, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-03-14 09:53:07 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.nal.service.impl;

import eu.europa.ec.opoce.cellar.ccr.service.LanguagesNalSkosLoaderService;
import eu.europa.ec.opoce.cellar.nal.dao.RequiredLanguageDbRecord;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.nal.repository.RequiredLanguageDbGateway;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author ARHS Developments
 */
public class RequiredLanguagesConfigurationServiceTest {
    private LanguagesNalSkosLoaderService languagesNalSkosLoaderService;
    private RequiredLanguageDbGateway requiredLanguageDbGateway;
    private RequiredLanguagesConfigurationService requiredLanguagesConfigurationService;

    @Before
    public void setUp() throws Exception {
        languagesNalSkosLoaderService = mock(LanguagesNalSkosLoaderService.class);
        when(languagesNalSkosLoaderService.getLanguageBeanByThreeCharCode("bul")).thenReturn(new LanguageBean());
        when(languagesNalSkosLoaderService.getLanguageBeanByThreeCharCode("ces")).thenReturn(new LanguageBean());

        requiredLanguageDbGateway = mock(RequiredLanguageDbGateway.class);

        List<RequiredLanguageDbRecord> languageDbRecordBulList = Arrays.asList(
                new RequiredLanguageDbRecord("http://publications.europa.eu/resource/authority/language/DEU"),
                new RequiredLanguageDbRecord("http://publications.europa.eu/resource/authority/language/ENG"),
                new RequiredLanguageDbRecord("http://publications.europa.eu/resource/authority/language/FRA")
        );

        List<RequiredLanguageDbRecord> languageDbRecordCesList = Arrays.asList(
                new RequiredLanguageDbRecord("http://publications.europa.eu/resource/authority/language/ENG"),
                new RequiredLanguageDbRecord("http://publications.europa.eu/resource/authority/language/FRA"),
                new RequiredLanguageDbRecord("http://publications.europa.eu/resource/authority/language/DEU")
        );

        Map<String, List<RequiredLanguageDbRecord>> map = new HashMap<>();
        map.put("http://publications.europa.eu/resource/authority/language/BUL", languageDbRecordBulList);
        map.put("http://publications.europa.eu/resource/authority/language/CES", languageDbRecordCesList);

        when(requiredLanguageDbGateway.selectAll()).thenReturn(map);

        requiredLanguagesConfigurationService = new RequiredLanguagesConfigurationService(languagesNalSkosLoaderService,
                requiredLanguageDbGateway);
    }

    @Test
    public void testLanguagesRetrieval() throws Exception {
        List<LanguageBean> languageBeanList = requiredLanguagesConfigurationService.getLanguages();
        assertNotNull(languageBeanList);
        assertEquals(2, languageBeanList.size());
    }
}