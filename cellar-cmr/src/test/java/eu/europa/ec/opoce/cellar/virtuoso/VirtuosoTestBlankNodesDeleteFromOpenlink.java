package eu.europa.ec.opoce.cellar.virtuoso;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.StmtIterator;
import virtuoso.jena.driver.VirtModel;

import java.io.StringReader;

public class VirtuosoTestBlankNodesDeleteFromOpenlink {

    static String src = "@prefix foaf:    <http://xmlns.com/foaf/0.1/> .\n"
            + "<http://www.hp.com/people/Ian_Dickinson> foaf:mbox_sha1sum '896dfb5980f37c47ada8c2a2538888d0c39e582d' .\n"
            + "[] foaf:name 'Ian Dickinson'  ;\n" + " foaf:p1 'v1'; \n" + " foaf:p1 'v2'; \n" + " foaf:p1 'v3'; \n" + " foaf:p1 'v4'; \n"
            + " foaf:p1 'v5'; \n" + " foaf:p1 'v6'; \n" + " foaf:p1 'v7'; \n" + " foaf:p1 'v8'; \n" + " foaf:p1 'v9'; \n" + ".";

    static String rem = "@prefix foaf:    <http://xmlns.com/foaf/0.1/> .\n" + "[] foaf:p1 'v1'; \n" + " foaf:p1 'v2'; \n"
            + " foaf:p1 'v3'; \n" + ".";

    public static void main(String[] args) throws Exception {
        String host = "10.2.0.224:1116";
        String uid = "dba";
        String pwd = "dba";

        VirtModel vm = VirtModel.openDatabaseModel("test1", host, uid, pwd);

        vm.removeAll();
        vm.read(new StringReader(src), null, "N3");

        System.out.println("====== Database Model data ========");
        StmtIterator si = vm.listStatements();
        while (si.hasNext()) {
            System.out.println(si.nextStatement());
        }
        System.out.println("\n");

        Model modelForDel = ModelFactory.createDefaultModel();
        modelForDel.read(new StringReader(rem), null, "N3");
        si.close();
        System.out.println("====== data for remove ========");
        si = modelForDel.listStatements();
        while (si.hasNext()) {
            System.out.println(si.nextStatement());
        }
        System.out.println("\n");

        vm.remove(modelForDel);
        si.close();
        System.out.println("====== Database Model data, after remove ========");
        si = vm.listStatements();
        while (si.hasNext()) {
            System.out.println(si.nextStatement());
        }
        System.out.println("\n");

        si.close();
        vm.close();

    }

}
