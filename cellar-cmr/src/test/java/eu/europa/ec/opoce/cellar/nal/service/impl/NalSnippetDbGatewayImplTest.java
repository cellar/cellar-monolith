package eu.europa.ec.opoce.cellar.nal.service.impl;

import eu.europa.ec.opoce.cellar.database.Database;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.jdbc.support.lob.LobHandler;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class NalSnippetDbGatewayImplTest {

    @Mock
    private Database database;
    @Mock
    private LobHandler lobHandler;

    private NalSnippetDbGatewayImpl nalSnippetDbGateway;

    @Before
    public void init() {
        nalSnippetDbGateway = new NalSnippetDbGatewayImpl(database, lobHandler);
    }

    @Test
    public void getConceptSchemeUriPatternForEurovoc() {
        String conceptSchemeUri = "eurovoc.europa.eu/100185";
        assertEquals("http://eurovoc.europa.eu%", nalSnippetDbGateway.getConceptSchemeUriPattern(conceptSchemeUri));
    }

    @Test
    public void getConceptSchemeUriPatternForNonEurovoc() {
        String conceptSchemeUri = "authority/fd_700/";
        assertEquals("authority/fd_700/%", nalSnippetDbGateway.getConceptSchemeUriPattern(conceptSchemeUri));
    }
}