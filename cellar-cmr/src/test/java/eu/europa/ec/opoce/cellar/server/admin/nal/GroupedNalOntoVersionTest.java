package eu.europa.ec.opoce.cellar.server.admin.nal;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class GroupedNalOntoVersionTest {

    private static final String NAME = "eurovoc";
    private static final String URI = "http://eurovoc.europa.eu/100141";
    private static final NalOntoVersion VERSION = new NalOntoVersion();

    @Test
    public void test() {
        final GroupedNalOntoVersion version = new GroupedNalOntoVersion(NAME, URI, VERSION);

        assertEquals(version.getName(), NAME);
        assertEquals(version.getUri(), URI);
        assertEquals(version.getNalOntoVersion(), VERSION);
    }
}