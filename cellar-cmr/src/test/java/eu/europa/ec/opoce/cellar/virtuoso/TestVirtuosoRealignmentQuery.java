/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : CellarIdentifier.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 16 mars 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.virtuoso;

import eu.europa.ec.opoce.cellar.cmr.model.ModelUtils;
import org.apache.commons.lang.CharEncoding;
import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.rdf.model.Statement;
import virtuoso.jdbc4.VirtuosoDataSource;
import virtuoso.jena.driver.VirtDataset;
import virtuoso.jena.driver.VirtuosoUpdateFactory;
import virtuoso.jena.driver.VirtuosoUpdateRequest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The Class VirtuosoRealignmentQueryTest.
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 16 mars 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class TestVirtuosoRealignmentQuery {

    /** The virtuoso data source. */
    private static VirtuosoDataSource virtuosoDataSource;

    /**
     * The main method.
     *
     * @param args the arguments
     * @throws Exception the exception
     */
    public static void main(final String[] args) throws Exception {
        final String cellarId = "http://cellar-dev.publications.europa.eu/resource/cellar/REALIGNMENT";
        write(cellarId);
        List<Statement> read = read(cellarId);
        System.out.println(Arrays.toString(read.toArray(new Statement[] {})));
        realignSubject();
        read = read(cellarId);
        System.out.println(Arrays.toString(read.toArray(new Statement[] {})));
        realignObject();
        read = read(cellarId);
        System.out.println(Arrays.toString(read.toArray(new Statement[] {})));
    }

    /**
     * Gets the virtuoso data source.
     *
     * @return the virtuoso data source
     * @throws Exception the exception
     */
    private static VirtuosoDataSource getVirtuosoDataSource() throws Exception {
        if (virtuosoDataSource != null) {
            return virtuosoDataSource;
        }

        final String serverName = "10.2.0.224";
        final int serverPort = 1116;
        virtuosoDataSource = new VirtuosoDataSource();
        virtuosoDataSource.setUser("dba");
        virtuosoDataSource.setPassword("dba");
        virtuosoDataSource.setDataSourceName("jdbc:virtuoso://" + serverName + ":" + serverPort + "/charset=UTF-8");
        virtuosoDataSource.setServerName(serverName);
        virtuosoDataSource.setPortNumber(serverPort);
        virtuosoDataSource.setCharset(CharEncoding.UTF_8);
        return virtuosoDataSource;
    }

    /**
     * Write.
     *
     * @param cellarId the cellar id
     * @throws Exception the exception
     */
    private static void write(final String cellarId) throws Exception {
        VirtDataset dataset = null;
        try {
            // gets the connection
            dataset = new VirtDataset(getVirtuosoDataSource());

            // get direct model of digital objects that are supposed to be inserted
            Model existingModel = null;
            Model newModel = null;
            try {
                // load existing model
                existingModel = dataset.getNamedModel(cellarId);
                if (existingModel == null) {
                    existingModel = ModelFactory.createDefaultModel();
                    dataset.addNamedModel(cellarId, existingModel);
                }

                // update new model
                newModel = ModelFactory.createDefaultModel();
                newModel.add(ResourceFactory.createStatement(
                        //
                        newModel.createResource("http://cellar-dev.publications.europa.eu/resource/cellar/OLD.SUBJECT"), //
                        newModel.createProperty("RANDOM_PROPERTY"), //
                        newModel.createResource("RANDOM_OBJECT")));

                newModel.add(ResourceFactory.createStatement(
                        //
                        newModel.createResource("RANDOM_SUBJECT"), //
                        newModel.createProperty("RANDOM_PROPERTY"), //
                        newModel.createResource("http://cellar-dev.publications.europa.eu/resource/cellar/OLD.OBJECT")));

                existingModel.removeAll();
                existingModel.add(newModel);
            } catch (final Exception e) {
                System.out.println(e.getMessage());
            } finally {
                ModelUtils.closeQuietly(existingModel, newModel);
            }
        } finally {
            if (dataset != null) {
                dataset.close();
            }
        }
    }

    /**
     * Realign subject.
     *
     * @throws Exception the exception
     */
    private static void realignSubject() throws Exception {
        VirtDataset dataset = null;
        try {
            // gets the connection
            dataset = new VirtDataset(getVirtuosoDataSource());
            try {
                final String queryString = "DELETE {  GRAPH ?g { <http://cellar-dev.publications.europa.eu/resource/cellar/OLD.SUBJECT> ?p ?o .}  } "
                        + "INSERT {  GRAPH ?g { <http://cellar-dev.publications.europa.eu/resource/cellar/NEW.SUBJECT> ?p ?o }  } "
                        + "WHERE { GRAPH ?g {  <http://cellar-dev.publications.europa.eu/resource/cellar/OLD.SUBJECT> ?p ?o .} "
                        + " BIND(<http://cellar-dev.publications.europa.eu/resource/cellar/NEW.SUBJECT> as ?s)    }";

                final VirtuosoUpdateRequest vur = VirtuosoUpdateFactory.create(queryString, (Dataset) dataset);
                vur.exec();

            } catch (final Exception e) {
                System.out.println(e.getMessage());
            }
        } finally {
            if (dataset != null) {
                dataset.close();
            }
        }
    }

    /**
     * Realign object.
     *
     * @throws Exception the exception
     */
    private static void realignObject() throws Exception {
        VirtDataset dataset = null;
        try {
            // gets the connection
            dataset = new VirtDataset(getVirtuosoDataSource());
            try {
                final String queryString = "DELETE {  GRAPH ?g { ?s ?p <http://cellar-dev.publications.europa.eu/resource/cellar/OLD.OBJECT> }  } "
                        + "INSERT {  GRAPH ?g { ?s ?p <http://cellar-dev.publications.europa.eu/resource/cellar/NEW.OBJECT> }  } "
                        + "WHERE { GRAPH ?g {  ?s ?p <http://cellar-dev.publications.europa.eu/resource/cellar/OLD.OBJECT> .} "
                        + " BIND(<http://cellar-dev.publications.europa.eu/resource/cellar/NEW.OBJECT> as ?o)    }";

                final VirtuosoUpdateRequest vur = VirtuosoUpdateFactory.create(queryString, (Dataset) dataset);
                vur.exec();

            } catch (final Exception e) {
                System.out.println(e.getMessage());
            }
        } finally {
            if (dataset != null) {
                dataset.close();
            }
        }
    }

    /**
     * Read.
     *
     * @param cellarId the cellar id
     * @return the list
     * @throws Exception the exception
     */
    private static List<Statement> read(final String cellarId) throws Exception {
        final List<Statement> retStatements = new ArrayList<Statement>();

        VirtDataset dataset = null;
        Model existingModel = null;
        try {
            dataset = new VirtDataset(getVirtuosoDataSource());
            existingModel = dataset.getNamedModel(cellarId);
            if (existingModel != null) {
                retStatements.addAll(existingModel.listStatements().toList());
            }
        } finally {
            ModelUtils.closeQuietly(existingModel);
            if (dataset != null) {
                dataset.close();
            }
        }

        return retStatements;
    }

}
