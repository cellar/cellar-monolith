package eu.europa.ec.opoce.cellar.cmr;

import org.junit.Before;
import org.junit.Test;

import static eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType.EXPRESSION;
import static eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType.WORK;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class RelatedObjectTest {
    private RelatedObject relatedObjectOne;
    private RelatedObject relatedObjectTwo;
    @Before
    public void setup() throws Exception {
        relatedObjectOne = new RelatedObject("", WORK, "");
    }

    @Test
    public void objectShouldNotEqualNull() throws Exception {
        assertFalse(relatedObjectOne.equals(null));
    }

    @Test
    public void objectShouldEqualItself() throws Exception {
        assertTrue(relatedObjectOne.equals(relatedObjectOne));
    }

    @Test
    public void twoObjectsWithDifferentResourceUriShouldNotBeEqual() throws Exception {
        relatedObjectOne = new RelatedObject("ressourceUriOne", WORK, "");
        relatedObjectTwo = new RelatedObject("resourceUriTwo", WORK, "");
        assertFalse(relatedObjectOne.equals(relatedObjectTwo));
    }

    @Test
    public void twoObjectsWithDifferentTypeShouldNotBeEqual() throws Exception {
        relatedObjectOne = new RelatedObject("", WORK, "");
        relatedObjectTwo = new RelatedObject("", EXPRESSION, "");
        assertFalse(relatedObjectOne.equals(relatedObjectTwo));
    }

    @Test
    public void twoObjectsWithDifferentPropertyShouldNotBeEqual() throws Exception {
        relatedObjectOne = new RelatedObject("", WORK, "propertyOne");
        relatedObjectTwo = new RelatedObject("", WORK, "propertyTwo");
        assertFalse(relatedObjectOne.equals(relatedObjectTwo));
    }

    @Test
    public void twoObjectsWithAllParamsEqualShouldBeEqual() throws Exception {
        relatedObjectOne = new RelatedObject("resource", WORK, "property");
        relatedObjectTwo = new RelatedObject("resource", WORK, "property");
        assertTrue(relatedObjectOne.equals(relatedObjectTwo));
    }
}
