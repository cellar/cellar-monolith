/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.modelload.domain
 *             FILE : ModelLoadRequestTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 01 09, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-01-09 10:40:45 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.modelload.domain;

import eu.europa.ec.opoce.cellar.ExecutionStatus;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author ARHS Developments
 */
public class ModelLoadRequestTest {
    @Test
    public void defaultExecutionStatusIsPending() throws Exception {
        ModelLoadRequest mlr = new ModelLoadRequest();
        assertEquals(ExecutionStatus.Pending, mlr.getExecutionStatus());
    }

}