package eu.europa.ec.opoce.cellar.virtuoso;

import org.apache.jena.sparql.engine.http.QueryEngineHTTP;

/**
 * Created by kosterar on 2015-11-25.
 */
public class VirtuosoTestMalformedInput {

    public static final String SPARQL_ENDPOINT = "http://op-eurlex-cm-dev6.aris-lux.lan:8899/sparql";

    public static final String QUERY = "SELECT ?p where { <${s}> ?p ?o.}";

    public static void main(String... args) {
        test();
    }

    public static void test() {

        final QueryEngineHTTP queryEngine = new QueryEngineHTTP(SPARQL_ENDPOINT, QUERY);
        queryEngine.execSelect();
    }
}
