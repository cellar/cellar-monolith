package eu.europa.ec.opoce.cellar.server.conditionalRequest;

import org.junit.Assert;
import org.junit.Test;

import java.time.Instant;
import java.util.Date;

public class ETagTypeTest {

    @Test
    public void buildETagTest() throws Exception {
        final Instant instant = Instant.parse("2018-12-25T11:22:33.444Z");
        final Date date = Date.from(instant);

        Assert.assertEquals("\"TDo-20181225122233444\"", ETagType.TREE_DOSSIER.buildETag(date));
        Assert.assertEquals("\"TWo-20181225122233444\"", ETagType.TREE_WORK.buildETag(date));
        Assert.assertEquals("\"TAg-20181225122233444\"", ETagType.TREE_AGENT.buildETag(date));
        Assert.assertEquals("\"TTe-20181225122233444\"", ETagType.TREE_TOPLEVELEVENT.buildETag(date));
        Assert.assertEquals("\"BEx-20181225122233444\"", ETagType.BRANCH_EXPRESSION.buildETag(date));
        Assert.assertEquals("\"BEv-20181225122233444\"", ETagType.BRANCH_EVENT.buildETag(date));
        Assert.assertEquals("\"Obj-20181225122233444\"", ETagType.OBJECT.buildETag(date));
        Assert.assertEquals("\"RTW-20181225122233444\"", ETagType.RDF_TREE_WORK.buildETag(date));
        Assert.assertEquals("\"RTD-20181225122233444\"", ETagType.RDF_TREE_DOSSIER.buildETag(date));
        Assert.assertEquals("\"RTA-20181225122233444\"", ETagType.RDF_TREE_AGENT.buildETag(date));
        Assert.assertEquals("\"RTT-20181225122233444\"", ETagType.RDF_TREE_TOPLEVELEVENT.buildETag(date));
        Assert.assertEquals("\"ROb-20181225122233444\"", ETagType.RDF_OBJECT.buildETag(date));
        Assert.assertEquals("\"Cls-20181225122233444\"", ETagType.LIST_CONTENT.buildETag(date));
        Assert.assertEquals("\"Czp-20181225122233444\"", ETagType.ZIP_CONTENT.buildETag(date));
        Assert.assertEquals("\"Con-20181225122233444\"", ETagType.CONTENT_STREAM.buildETag(date));
        Assert.assertEquals("\"Ide-20181225122233444\"", ETagType.IDENTIFIERS.buildETag(date));
        Assert.assertEquals("\"OJl-20181225122233444\"", ETagType.OJ_LIST.buildETag(date));
        Assert.assertEquals("\"Cos-20181225122233444\"", ETagType.CONCEPT_SCHEME.buildETag(date));
    }
}