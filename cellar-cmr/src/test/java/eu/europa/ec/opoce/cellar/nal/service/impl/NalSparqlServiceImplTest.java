/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.nal.service.impl
 *             FILE : NalSparqlServiceImplTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 03 14, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-03-14 09:48:49 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.nal.service.impl;

import org.apache.jena.query.Query;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * @author ARHS Developments
 */
public class NalSparqlServiceImplTest {
    private NalSparqlServiceImpl nalSparqlService;

    @Before
    public void setUp() throws Exception {
        nalSparqlService = new NalSparqlServiceImpl();
    }

    @Test
    public void sparqlQueriesFileIsCorrectlyProcessed() throws Exception {
        List<Query> queries = nalSparqlService.getSkosInferenceSparqlList();
        assertNotNull(queries);
        assertEquals(38, queries.size());
    }
}