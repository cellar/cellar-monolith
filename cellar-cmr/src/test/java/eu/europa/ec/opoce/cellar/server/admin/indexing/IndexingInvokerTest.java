package eu.europa.ec.opoce.cellar.server.admin.indexing;

import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestGenerationService;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.UUID;

import static eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService.Priority.HIGH;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class IndexingInvokerTest {

    @Mock
    private CmrIndexRequestGenerationService indexRequestService;

    @Mock
    private IdentifierService identifierService;

    @Mock
    private CellarResourceDao cellarResourceDao;

    private IndexingInvoker invoker;

    @Before
    public void init() {
        invoker = new IndexingInvoker(indexRequestService, identifierService, cellarResourceDao);
    }

    @Test
    public void createIndexRequestForWork() {
        when(identifierService.getCellarPrefixed("workId")).thenReturn("cellarId");
        when(cellarResourceDao.findHierarchy("cellarId")).thenReturn(Arrays.asList(
                cellarResource(DigitalObjectType.WORK, null),
                cellarResource(DigitalObjectType.EXPRESSION, "en")
        ));

        invoker.addWork("workId", true, true, HIGH, Arrays.asList("en", "fr"));

        verify(identifierService, times(1)).getCellarPrefixed("workId");
        verify(cellarResourceDao, times(1)).findHierarchy("cellarId");
        verify(indexRequestService, times(1)).generateIndexRequest(any(), any(), any(), any(), any());
    }

    private static CellarResource cellarResource(final DigitalObjectType type, final String lang) {
        final CellarResource cellarResource = new CellarResource();
        cellarResource.setCellarId(UUID.randomUUID().toString());
        cellarResource.setCellarType(type);
        cellarResource.setLanguages(Collections.singletonList(lang));
        return cellarResource;
    }
}
