package eu.europa.ec.opoce.cellar.virtuoso;

import eu.europa.ec.opoce.cellar.cmr.model.ModelUtils;
import org.apache.commons.lang.CharEncoding;
import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.update.UpdateAction;
import org.apache.jena.update.UpdateFactory;
import org.apache.jena.update.UpdateRequest;
import virtuoso.jdbc4.VirtuosoDataSource;
import virtuoso.jena.driver.VirtDataset;

import java.util.ArrayList;
import java.util.List;

public class TestVirtuoso {

    private static VirtuosoDataSource virtuosoDataSource;

    public static void main(final String[] args) throws Exception {
        final String cellarId = "http://cellar-dev.publications.europa.eu/resource/cellar/XYZ";
        write(cellarId);
        read(cellarId);
    }

    private static VirtuosoDataSource getVirtuosoDataSource() throws Exception {
        if (virtuosoDataSource != null) {
            return virtuosoDataSource;
        }

        final String serverName = "10.2.0.224";
        final int serverPort = 1116;
        virtuosoDataSource = new VirtuosoDataSource();
        virtuosoDataSource.setUser("dba");
        virtuosoDataSource.setPassword("dba");
        virtuosoDataSource.setDataSourceName("jdbc:virtuoso://" + serverName + ":" + serverPort + "/charset=UTF-8");
        virtuosoDataSource.setServerName(serverName);
        virtuosoDataSource.setPortNumber(serverPort);
        virtuosoDataSource.setCharset(CharEncoding.UTF_8);
        return virtuosoDataSource;
    }

    private static void write(final String cellarId) throws Exception {
        VirtDataset dataset = null;
        try {
            // gets the connection
            dataset = new VirtDataset(getVirtuosoDataSource());

            // get direct model of digital objects that are supposed to be inserted
            Model existingModel = null;
            Model newModel = null;
            try {
                // load existing model
                existingModel = dataset.getNamedModel(cellarId);
                if (existingModel == null) {
                    existingModel = ModelFactory.createDefaultModel();
                    dataset.addNamedModel(cellarId, existingModel);
                }

                // update new model
                newModel = ModelFactory.createDefaultModel();
                newModel.add(ResourceFactory.createStatement(
                        //
                        newModel.createResource("http://cellar-dev.publications.europa.eu/resource/celex/32006D0241I.fra"), //
                        newModel.createProperty("http://publications.europa.eu/ontology/cdm#expression_title"), //
                        newModel.createLiteral(
                                "2006/241/CE: Décision de la Commission du  24 mars 2006  concernant certaines mesures de protection relatives à certains produits d'origine animale, à l'exclusion des produits de la pêche, originaires de Madagascar  [notifiée sous le numéro C(2006) 888]   (Texte présentant de l'intérêt pour l'EEE)")));
                existingModel.removeAll();
                existingModel.add(newModel);

                final String queryString = "DELETE {  GRAPH ?g { ?s ?p ?o }  }"
                        + " INSERT {  GRAPH ?g { ?s ?p <http://cellar-dev.publications.europa.eu/resource/cellar/820561f0-ea8c-11e5-bb0b-01aa75ed71a1.0001.02> }"
                        + "  } WHERE { GRAPH ?g {  ?s ?p <http://cellar-dev.publications.europa.eu/resource/cellar/211e38c3-e9fb-11e5-b8fd-01aa75ed71a1.0001.02> ."
                        + "  BIND(<http://cellar-dev.publications.europa.eu/resource/cellar/211e38c3-e9fb-11e5-b8fd-01aa75ed71a1.0001.02> as ?o)  }  }";

                final UpdateRequest updateRequest = UpdateFactory.create(queryString);
                UpdateAction.execute(updateRequest, (Dataset) dataset);
            } catch (final Exception e) {
                System.out.println(e.getMessage());
            } finally {
                ModelUtils.closeQuietly(existingModel, newModel);
            }
        } finally {
            if (dataset != null) {
                dataset.close();
            }
        }
    }

    private static List<Statement> read(final String cellarId) throws Exception {
        final List<Statement> retStatements = new ArrayList<Statement>();

        VirtDataset dataset = null;
        Model existingModel = null;
        try {
            dataset = new VirtDataset(getVirtuosoDataSource());
            existingModel = dataset.getNamedModel(cellarId);
            if (existingModel != null) {
                retStatements.addAll(existingModel.listStatements().toList());
            }
        } finally {
            ModelUtils.closeQuietly(existingModel);
            if (dataset != null) {
                dataset.close();
            }
        }

        return retStatements;
    }

}
