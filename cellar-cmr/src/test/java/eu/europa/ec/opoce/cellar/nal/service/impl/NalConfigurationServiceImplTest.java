/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.nal.service.impl
 *             FILE : NalConfigurationServiceImplTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 03 14, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-03-14 07:44:12 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.nal.service.impl;

import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.database.dao.NalConfigDao;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.nal.NalOntoUtils;
import eu.europa.ec.opoce.cellar.nal.dao.NalConfig;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import org.apache.jena.riot.RDFFormat;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static eu.europa.ec.opoce.cellar.nal.NalOntoUtils.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

/**
 * @author ARHS Developments
 */
public class NalConfigurationServiceImplTest {

    private static final String NAL_URI_AUTHORITY_ONE = "http://publications.europa.eu/resource/authority/case-status";
    private static final String NAL_URI_AUTHORITY_ONE_VERSION = "gmTHQ8hEie_geVmqWEQe5xpC3rauhT10";
    private static final String NAL_URI_AUTHORITY_ONE_VERSION_INFERRED = "JOCTXPtmYLx_7Lrwzh.07IQS2ecqe6TF";
    private static final String NAL_URI_AUTHORITY_TWO = "http://publications.europa.eu/resource/authority/label-type";
    private static final String NAL_URI_AUTHORITY_TWO_VERSION = "gmTHQ8hEie_geVmqWEQe5xpC3rauhT12";
    private static final String NAL_URI_AUTHORITY_TWO_VERSION_INFERRED = "JOCTXPtmYLx_7Lrwz2";
    private static final String NAL_URI_EUROVOC_THREE = "http://eurovoc.europa.eu/100141";
    private static final String NAL_URI_EUROVOC_THREE_VERSION = "gmTHQ8hEie_geVmqWEQe5xpC3rauhT13";
    private static final String NAL_URI_EUROVOC_THREE_VERSION_INFERRED = "JOCTXPtmYLx_7Lrwz3";
    private NalConfigDao nalConfigDao;
    private ContentStreamService contentStreamService;
    private NalConfigurationServiceImpl nalConfigurationService;
    private List<NalConfig> authorityNalConfigCollection;
    private NalConfig nalConfigAuthorityOne;
    private NalConfig nalConfigAuthorityTwo;
    private NalConfig nalConfigEurovocThree;


    @Before
    public void setUp() {
        nalConfigDao = mock(NalConfigDao.class);
        contentStreamService = mock(ContentStreamService.class);
        nalConfigurationService = new NalConfigurationServiceImpl(nalConfigDao, contentStreamService);

        nalConfigAuthorityOne = new NalConfig();
        nalConfigAuthorityOne.setUri(NAL_URI_AUTHORITY_ONE);
        nalConfigAuthorityOne.setVersion(NAL_URI_AUTHORITY_ONE_VERSION);
        nalConfigAuthorityOne.setVersionInferred(NAL_URI_AUTHORITY_ONE_VERSION_INFERRED);
        nalConfigAuthorityOne.setName(UUID.randomUUID().toString());
        nalConfigAuthorityOne.setOntologyUri(NalOntoUtils.AUTHORITY_ONTOLOGY_URI);
        nalConfigAuthorityOne.setExternalPid(NalOntoUtils.NAL_PREFIX + nalConfigAuthorityOne.getName());
        when(nalConfigDao.findByUri(NAL_URI_AUTHORITY_ONE)).thenReturn(Optional.of(nalConfigAuthorityOne));

        nalConfigAuthorityTwo = new NalConfig();
        nalConfigAuthorityTwo.setUri(NAL_URI_AUTHORITY_TWO);
        nalConfigAuthorityTwo.setVersion(NAL_URI_AUTHORITY_TWO_VERSION);
        nalConfigAuthorityTwo.setVersionInferred(NAL_URI_AUTHORITY_TWO_VERSION_INFERRED);
        nalConfigAuthorityTwo.setName(UUID.randomUUID().toString());
        nalConfigAuthorityTwo.setOntologyUri(NalOntoUtils.AUTHORITY_ONTOLOGY_URI);
        nalConfigAuthorityTwo.setExternalPid(NalOntoUtils.NAL_PREFIX + nalConfigAuthorityTwo.getName());
        when(nalConfigDao.findByUri(NAL_URI_AUTHORITY_TWO)).thenReturn(Optional.of(nalConfigAuthorityTwo));

        authorityNalConfigCollection = Arrays.asList(nalConfigAuthorityOne, nalConfigAuthorityTwo);
        when(nalConfigDao.findByOntologyLike(AUTHORITY_ONTOLOGY_URI)).thenReturn(authorityNalConfigCollection);

        nalConfigEurovocThree = new NalConfig();
        nalConfigEurovocThree.setUri(NAL_URI_EUROVOC_THREE);
        nalConfigEurovocThree.setVersion(NAL_URI_EUROVOC_THREE_VERSION);
        nalConfigEurovocThree.setVersionInferred(NAL_URI_EUROVOC_THREE_VERSION_INFERRED);
        nalConfigEurovocThree.setName(UUID.randomUUID().toString());
        nalConfigEurovocThree.setOntologyUri(NalOntoUtils.EUROVOC_ONTOLOGY_URI);
        nalConfigEurovocThree.setExternalPid(NalOntoUtils.NAL_PREFIX + nalConfigEurovocThree.getName());
        when(nalConfigDao.findByOntologyLike(EUROVOC_ONTOLOGY_URI)).thenReturn(Collections.singletonList(nalConfigEurovocThree));
        when(nalConfigDao.findByUri(EUROVOC_CONCEPTSCHEME_URI)).thenReturn(Optional.of(nalConfigEurovocThree));
    }

    @Test
    public void testRetrievalOfAllNalUri() {
        when(nalConfigDao.findAll()).thenReturn(authorityNalConfigCollection);
        List<String> nalUris = nalConfigurationService.getAllNalUriList();
        assertNotNull(nalUris);
        assertEquals(2, nalUris.size());
        assertEquals(Arrays.asList(NAL_URI_AUTHORITY_ONE, NAL_URI_AUTHORITY_TWO), nalUris);
    }

    @Test
    public void nalNotSetupThenReturnEmptyList() {
        when(nalConfigDao.findAll()).thenReturn(Collections.emptyList());
        List<String> nalUris = nalConfigurationService.getAllNalUriList();
        assertNotNull(nalUris);
        assertEquals(0, nalUris.size());
    }

    @Test
    public void testPositiveRetrievalFromS3() {
        when(contentStreamService.getContentAsString(anyString(), anyString(), any(ContentType.class))).thenReturn(Optional.of(""));

        String pid = NalOntoUtils.NAL_PREFIX + nalConfigAuthorityOne.getName();
        nalConfigurationService.getNal(NAL_URI_AUTHORITY_ONE, false);
        verify(contentStreamService, times(1)).getContentAsString(pid, NAL_URI_AUTHORITY_ONE_VERSION, ContentType.DIRECT);

        nalConfigurationService.getNal(NAL_URI_AUTHORITY_ONE, true);
        verify(contentStreamService, times(1)).getContentAsString(pid, NAL_URI_AUTHORITY_ONE_VERSION_INFERRED, ContentType.DIRECT_INFERRED);
    }

    @Test(expected = CellarException.class)
    public void nalNotFoundInS3EmptyOptional() throws CellarException {
        when(contentStreamService.getContentAsString(any(String.class), eq(ContentType.DIRECT))).thenReturn(Optional.empty());
        when(nalConfigDao.findByUri(NAL_LANGUAGE_URI)).thenReturn(Optional.empty());
        nalConfigurationService.getNal(NAL_LANGUAGE_URI, false);
    }

    @Test
    public void getAllNalRetrievedAllNalFromTheRepository() {
        String nalUriOne = NalOntoUtils.NAL_PREFIX + nalConfigAuthorityOne.getName();
        String nalUriTwo = NalOntoUtils.NAL_PREFIX + nalConfigAuthorityTwo.getName();
        String nalUriThree = NalOntoUtils.NAL_PREFIX + nalConfigEurovocThree.getName();

        when(contentStreamService.getContentAsString(any(String.class), anyString(), any(ContentType.class)))
                .thenReturn(Optional.of(""));

        nalConfigurationService.getAllNalModel(true, NalOntoUtils.EUROVOC_ONTOLOGY_URI);
        verify(contentStreamService, times(1)).getContentAsString(nalUriThree, NAL_URI_EUROVOC_THREE_VERSION_INFERRED, ContentType.DIRECT_INFERRED);

        nalConfigurationService.getAllNalModel(true, NalOntoUtils.AUTHORITY_ONTOLOGY_URI);
        verify(contentStreamService, times(1)).getContentAsString(nalUriOne, NAL_URI_AUTHORITY_ONE_VERSION_INFERRED, ContentType.DIRECT_INFERRED);
        verify(contentStreamService, times(1)).getContentAsString(nalUriTwo, NAL_URI_AUTHORITY_TWO_VERSION_INFERRED, ContentType.DIRECT_INFERRED);
    }

    @Test
    public void getNalFormatCanBeSpecifiedDefaultIsXMLPlain() {
        when(contentStreamService.getContentAsString(any(String.class), anyString(), any(ContentType.class)))
                .thenReturn(Optional.of("_:Bfac0796e75543fbbfbdef70c7d3be03f <http://www.w3.org/2000/01/rdf-schema#label> \"Commission d’enquête chargée d’examiner les allégations d’infraction et de mauvaise administration dans l’application du droit de l’Union en matière de blanchiment de capitaux, d’évasion fiscale et de fraude fiscale\"@fr ."));

        String defaultFormat = nalConfigurationService.getNal(NAL_URI_AUTHORITY_ONE, false);
        Assert.assertTrue(defaultFormat.startsWith("<rdf:RDF"));

        String jsonld = nalConfigurationService.getNal(NAL_URI_AUTHORITY_ONE, false, RDFFormat.JSONLD);
        Assert.assertTrue(jsonld.startsWith("{"));

        String nt = nalConfigurationService.getNal(NAL_URI_AUTHORITY_ONE, false, RDFFormat.NT);
        Assert.assertTrue(nt.endsWith(".\n"));

        String json = nalConfigurationService.getNal(NAL_URI_AUTHORITY_ONE, false, RDFFormat.RDFJSON);
        Assert.assertTrue(json.startsWith("{"));
    }

}