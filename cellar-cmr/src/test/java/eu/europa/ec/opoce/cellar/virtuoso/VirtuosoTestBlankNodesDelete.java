package eu.europa.ec.opoce.cellar.virtuoso;

import org.apache.commons.lang.CharEncoding;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.RDFReader;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.util.Assert;
import virtuoso.jdbc4.VirtuosoDataSource;
import virtuoso.jena.driver.VirtDataset;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;

public class VirtuosoTestBlankNodesDelete {

    private static VirtuosoDataSource virtuosoDataSource;

    public static void main(String[] args) throws Exception {
        final String graphName = "http://cellar-dev.publications.europa.eu/resource/cellar/ddqdqsd";

        // write model
        File file = new File("C:\\Users\\gallinda\\Desktop\\Stuff\\CELLAR\\Virtuoso\\tickets\\21353 (delete of blanknodes)\\existing.xml");
        Model model = read(file);
        final Model fsModel1 = ModelFactory.createDefaultModel().add(model);
        writeModelToVirtuoso(graphName, model);

        // remove model
        file = new File("C:\\Users\\gallinda\\Desktop\\Stuff\\CELLAR\\Virtuoso\\tickets\\21353 (delete of blanknodes)\\toRemove.xml");
        model = read(file);
        final Model fsModel2 = ModelFactory.createDefaultModel().add(model);
        removeModelFromVirtuoso(graphName, model);

        // compare that the new model from Virtuoso is equivalent to the model from the file system
        final Model fsModel = fsModel1.difference(fsModel2);
        final Model virtuosoModel = readModelFromVirtuoso(graphName);
        Assert.isTrue(fsModel.isIsomorphicWith(virtuosoModel));
    }

    private static VirtuosoDataSource getVirtuosoDataSource() throws Exception {
        if (virtuosoDataSource != null) {
            return virtuosoDataSource;
        }

        final String serverName = "10.2.0.224";
        final int serverPort = 1116;
        virtuosoDataSource = new VirtuosoDataSource();
        virtuosoDataSource.setUser("dba");
        virtuosoDataSource.setPassword("dba");
        virtuosoDataSource.setDataSourceName("jdbc:virtuoso://" + serverName + ":" + serverPort + "/charset=UTF-8");
        virtuosoDataSource.setServerName(serverName);
        virtuosoDataSource.setPortNumber(serverPort);
        virtuosoDataSource.setCharset(CharEncoding.UTF_8);
        return virtuosoDataSource;
    }

    private static void writeModelToVirtuoso(final String graphName, final Model newModel) throws Exception {
        VirtDataset dataset = null;
        try {
            // gets the connection
            dataset = new VirtDataset(getVirtuosoDataSource());

            // get direct model of digital objects that are supposed to be inserted
            Model existingModel = null;
            try {
                // load existing model
                existingModel = dataset.getNamedModel(graphName);

                // update new model
                existingModel.removeAll();
                existingModel.add(newModel);
            } finally {
                closeQuietly(existingModel, newModel);
            }
        } finally {
            if (dataset != null) {
                dataset.close();
            }
        }
    }

    private static Model readModelFromVirtuoso(final String graphName) throws Exception {
        Model existingModel = null;

        VirtDataset dataset = null;
        try {
            // gets the connection
            dataset = new VirtDataset(getVirtuosoDataSource());

            // load existing model
            existingModel = dataset.getNamedModel(graphName);
        } finally {
            if (dataset != null) {
                dataset.close();
            }
        }

        return existingModel;
    }

    private static void removeModelFromVirtuoso(final String graphName, final Model modelToRemove) throws Exception {
        VirtDataset dataset = null;
        try {
            // gets the connection
            dataset = new VirtDataset(getVirtuosoDataSource());

            // get direct model of digital objects that are supposed to be inserted
            Model existingModel = null;
            Model modelNotToRemove = null;
            try {
                // load existing model
                existingModel = dataset.getNamedModel(graphName);

                // update new model
                modelNotToRemove = existingModel.difference(modelToRemove);
                existingModel.removeAll();
                existingModel.add(modelNotToRemove);
            } finally {
                closeQuietly(existingModel, modelToRemove, modelNotToRemove);
            }
        } finally {
            if (dataset != null) {
                dataset.close();
            }
        }
    }

    private static void closeQuietly(final Model... models) {
        for (final Model model : models) {
            if (model != null) {
                model.close();
            }
        }
    }

    private static Model read(final File file) throws IOException {
        final Collection<Resource> resources = new ArrayList<Resource>();
        resources.add(new FileSystemResource(file));

        final Model model = ModelFactory.createDefaultModel();

        for (Resource resource : resources) {
            InputStream inputstream = null;
            try {
                inputstream = resource.getInputStream();
                final RDFReader rdfReader = getBasicReader(model, resource);
                rdfReader.read(model, inputstream, null);
            } catch (RuntimeException e) {
                closeQuietly(model);
                throw e;
            } finally {
                if (inputstream != null) {
                    inputstream.close();
                }
            }
        }

        return model;
    }

    private static RDFReader getBasicReader(final Model model, final Resource resource) {
        try {
            final String filename = resource.getFilename();
            if (filename == null) {
                throw new IllegalStateException("resource loaded from byte array does not have a filename");
            }
            final String extension = StringUtils.substringAfterLast(filename, ".").toLowerCase();

            if ("owl".equals(extension)) {
                return model.getReader();
            } else if ("nt".equals(extension)) {
                return model.getReader("N-TRIPLE");
            } else if ("n3".equals(extension) || "ttl".equals(extension)) {
                return model.getReader("TURTLE");
            }

            return model.getReader();
        } catch (IllegalStateException e) {
            return model.getReader();
        }
    }

}
