package eu.europa.ec.opoce.cellar.cmr.notice.core;

import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.nal.service.LanguageService;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import eu.europa.ec.opoce.cellar.semantic.xml.XmlBuilder;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class Rdf2XmlLabelServiceImplTest {

    @Mock
    private LanguageService languageService;

    @Mock
    private LanguageBean enLanguageBean;

    @Mock
    private LanguageBean frLanguageBean;

    private Rdf2XmlLabelServiceImpl rdf2XmlLabelService;
    private XmlBuilder xmlBuilder;
    private Resource resource;

    @Before
    public void setup() {
        rdf2XmlLabelService = new Rdf2XmlLabelServiceImpl(languageService);
        xmlBuilder = new XmlBuilder("", "", "div");
        resource = ModelFactory.createDefaultModel().getResource("http://publications.europa.eu/resource/test");
    }

    @Test
    public void writePrefLabelIfExistsTwoCharTest() {
        resource.addProperty(CellarProperty.skos_prefLabelP, "preflabel", "en");
        when(enLanguageBean.getIsoCodeTwoChar()).thenReturn("en");
        when(enLanguageBean.getIsoCodeThreeChar()).thenReturn("eng");

        rdf2XmlLabelService.writePrefLabelIfExists(xmlBuilder, resource, enLanguageBean);
        assertTrue(xmlBuilder.toString().contains("<PREFLABEL xml:lang=\"eng\">preflabel</PREFLABEL>"));
    }

    @Test
    public void writePrefLabelIfExistsThreeCharTest() {
        resource.addProperty(CellarProperty.skos_prefLabelP, "preflabel", "eng");
        when(enLanguageBean.getIsoCodeThreeChar()).thenReturn("eng");

        rdf2XmlLabelService.writePrefLabelIfExists(xmlBuilder, resource, enLanguageBean);
        assertTrue(xmlBuilder.toString().contains("<PREFLABEL xml:lang=\"eng\">preflabel</PREFLABEL>"));
    }

    @Test
    public void writeAltLabelTwoCharTest() {
        resource.addProperty(CellarProperty.skos_altLabelP, "altlabel", "en");
        when(enLanguageBean.getIsoCodeThreeChar()).thenReturn("en");

        rdf2XmlLabelService.writeAltLabels(xmlBuilder, resource, enLanguageBean);
        assertTrue(xmlBuilder.toString().contains("<ALTLABEL xml:lang=\"en\">altlabel</ALTLABEL>"));
    }

    @Test
    public void writeAltLabelThreeCharTest() {
        resource.addProperty(CellarProperty.skos_altLabelP, "altlabel", "eng");
        when(enLanguageBean.getIsoCodeThreeChar()).thenReturn("eng");

        rdf2XmlLabelService.writeAltLabels(xmlBuilder, resource, enLanguageBean);
        assertTrue(xmlBuilder.toString().contains("<ALTLABEL xml:lang=\"eng\">altlabel</ALTLABEL>"));
    }

    @Test
    public void writePrefLabelNoFallbackTest() {
        when(frLanguageBean.getIsoCodeThreeChar()).thenReturn("fra");
        when(frLanguageBean.getUri()).thenReturn("http://fra");

        rdf2XmlLabelService.writePrefLabel(xmlBuilder, resource, frLanguageBean, new HashMap<>());
        assertTrue(xmlBuilder.toString().contains("<PREFLABEL xml:lang=\"fra\"/>"));
    }

    @Test
    public void writePrefLabelWithCacheFallbackTest() {
        Map<LanguageBean, List<LanguageBean>> cache = new HashMap<>();
        cache.put(frLanguageBean, Arrays.asList(frLanguageBean, enLanguageBean));

        Resource fallbackResource = resource.getModel().createResource("http://fallback");
        fallbackResource.addProperty(CellarProperty.skos_prefLabelP, "preflabel");
        resource.getModel().add(fallbackResource, CellarProperty.cmr_langP, "eng");
        resource.addProperty(CellarProperty.cmr_fallbackP, fallbackResource);

        when(enLanguageBean.getIsoCodeThreeChar()).thenReturn("eng");
        when(frLanguageBean.getIsoCodeThreeChar()).thenReturn("fra");

        rdf2XmlLabelService.writePrefLabel(xmlBuilder, resource, frLanguageBean, cache);
        assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<div>\n" +
                "   <PREFLABEL xml:lang=\"fra\"/>\n" +
                "   <FALLBACK xml:lang=\"fra\">\n" +
                "      <LANG>eng</LANG>\n" +
                "      <PREFLABEL>preflabel</PREFLABEL>\n" +
                "   </FALLBACK>\n" +
                "</div>", xmlBuilder.toString());
    }

    @Test
    public void writePrefLabelWithEmptyCacheFallbackTest() {
        Resource fallbackResource = resource.getModel().createResource("http://fallback");
        fallbackResource.addProperty(CellarProperty.skos_prefLabelP, "preflabel");
        resource.getModel().add(fallbackResource, CellarProperty.cmr_langP, "eng");
        resource.addProperty(CellarProperty.cmr_fallbackP, fallbackResource);

        when(enLanguageBean.getIsoCodeThreeChar()).thenReturn("eng");
        when(frLanguageBean.getIsoCodeThreeChar()).thenReturn("fra");
        when(frLanguageBean.getUri()).thenReturn("http://fra");
        when(languageService.getFallbackLanguagesFor("http://fra")).thenReturn(Collections.singletonList("eng"));
        when(languageService.getByUri("eng")).thenReturn(enLanguageBean);

        rdf2XmlLabelService.writePrefLabel(xmlBuilder, resource, frLanguageBean, new HashMap<>());
        assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<div>\n" +
                "   <PREFLABEL xml:lang=\"fra\"/>\n" +
                "   <FALLBACK xml:lang=\"fra\">\n" +
                "      <LANG>eng</LANG>\n" +
                "      <PREFLABEL>preflabel</PREFLABEL>\n" +
                "   </FALLBACK>\n" +
                "</div>", xmlBuilder.toString());
    }
}