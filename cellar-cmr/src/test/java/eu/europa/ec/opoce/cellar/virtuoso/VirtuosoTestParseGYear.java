/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.virtuoso
 *             FILE : VirtuosoTestParseGYear.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 28, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$$
 *          VERSION : $LastChangedRevision$$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.virtuoso;

import org.apache.commons.lang.CharEncoding;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import virtuoso.jdbc4.VirtuosoDataSource;
import virtuoso.jena.driver.VirtDataset;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.UUID;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 28, 2015
 *
 * @author ARHS Developments
 * @version $Revision$$
 */
public class VirtuosoTestParseGYear {

    /** The virtuoso data source. */
    private static VirtuosoDataSource virtuosoDataSource;

    /**
     * The main method.
     *
     * @param args the arguments
     * @throws Exception the exception
     */
    public static void main(final String[] args) throws Exception {
        final String modelStr = "" + "<rdf:RDF " + "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" "
                + "    xmlns:j.0=\"http://publications.europa.eu/ontology/cdm#\" " + "    xmlns:owl=\"http://www.w3.org/2002/07/owl#\"> "
                + "" + "    <rdf:Description rdf:about=\"http://cellar-dev.publications.europa.eu/resource/XXX\"> "
                + "          <j.0:resource_legal_year rdf:datatype=\"http://www.w3.org/2001/XMLSchema#gYear\">2006</j.0:resource_legal_year> "
                + "    </rdf:Description> " + "</rdf:RDF>";

        final String graphName = "http://cellar-dev.publications.europa.eu/resource/cellar/" + UUID.randomUUID();

        // write model to Virtuoso
        final Model modelIn = read(modelStr, null);
        writeModel(graphName, modelIn);

        // read model from Virtuoso
        final Model modelOut = readModel(graphName);
        modelOut.write(System.out);
    }

    /**
     * Gets the virtuoso data source.
     *
     * @return the virtuoso data source
     * @throws Exception the exception
     */
    private static VirtuosoDataSource getVirtuosoDataSource() throws Exception {
        if (virtuosoDataSource != null) {
            return virtuosoDataSource;
        }

        final String serverName = "10.2.0.224";
        final int serverPort = 1117;
        virtuosoDataSource = new VirtuosoDataSource();
        virtuosoDataSource.setUser("dba");
        virtuosoDataSource.setPassword("dba");
        virtuosoDataSource.setDataSourceName("jdbc:virtuoso://" + serverName + ":" + serverPort + "/charset=UTF-8");
        virtuosoDataSource.setServerName(serverName);
        virtuosoDataSource.setPortNumber(serverPort);
        virtuosoDataSource.setCharset(CharEncoding.UTF_8);
        return virtuosoDataSource;
    }

    /**
     * Write model.
     *
     * @param graphName the graph name
     * @param newModel the new model
     * @throws Exception the exception
     */
    private static void writeModel(final String graphName, final Model newModel) throws Exception {
        VirtDataset dataset = null;
        try {
            // gets the connection
            dataset = new VirtDataset(getVirtuosoDataSource());

            // get direct model of digital objects that are supposed to be inserted
            Model existingModel = null;
            try {
                // load existing model
                existingModel = dataset.getNamedModel(graphName);

                // update new model
                existingModel.removeAll();
                existingModel.add(newModel);
            } finally {
                closeQuietly(existingModel, newModel);
            }
        } finally {
            if (dataset != null) {
                dataset.close();
            }
        }
    }

    /**
     * Read model.
     *
     * @param graphName the graph name
     * @return the model
     * @throws Exception the exception
     */
    private static Model readModel(final String graphName) throws Exception {
        final Model retModel = ModelFactory.createDefaultModel();

        VirtDataset dataset = null;
        Model existingModel = null;
        try {
            dataset = new VirtDataset(getVirtuosoDataSource());
            existingModel = dataset.getNamedModel(graphName);
            retModel.add(existingModel);
        } finally {
            closeQuietly(existingModel);
            if (dataset != null) {
                dataset.close();
            }
        }

        return retModel;
    }

    /**
     * Close quietly.
     *
     * @param models the models
     */
    private static void closeQuietly(final Model... models) {
        for (final Model model : models) {
            if (model != null) {
                model.close();
            }
        }
    }

    /**
     * Read.
     *
     * @param modelStr the model str
     * @param lang the lang
     * @return the model
     */
    private static Model read(final String modelStr, final String lang) {
        final String myModelStr = (modelStr == null ? "" : modelStr);
        final InputStream modelStream = new ByteArrayInputStream(myModelStr.getBytes(Charset.forName("UTF-8")));

        final Model model = ModelFactory.createDefaultModel();
        model.read(modelStream, lang);
        try {
            modelStream.close();
        } catch (final IOException e) {
            System.err.println("A problem occurred while closing model stream.");
        }

        return model;
    }

}
