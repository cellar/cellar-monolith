package eu.europa.ec.opoce.cellar.cmr.service.impl;

import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class EmbargoOperationAbstractServiceImplTest {

    private static final String CELLAR_ID = "cellar:eb53b233-cacc-11e8-bf98-01aa75ed71a1";
    private static final Identifier IDENTIFIER = new Identifier(CELLAR_ID);
    private static final Date EMBARGO_DATE = new Date(0);
    private static final Date LAST_MODIFICATION_DATE = new Date(1);

    @Test
    public void updateDateCellarResource() throws Exception {
        final CellarResource cellarResource = new CellarResource();
        final CellarResourceDao cellarResourceDao = mock(CellarResourceDao.class);

        when(cellarResourceDao.findResourceWithEmbeddedNotice(CELLAR_ID)).thenReturn(cellarResource);
        when(cellarResourceDao.saveOrUpdateObject(any())).thenReturn(null);

        final EmbargoOperationAbstractServiceImpl service = new EmbargoOperationServiceImpl();
        service.cellarResourceDao = cellarResourceDao;
        service.updateDateCellarResource(IDENTIFIER, EMBARGO_DATE, LAST_MODIFICATION_DATE);

        assertEquals(cellarResource.getEmbargoDate(), EMBARGO_DATE);
        assertEquals(cellarResource.getLastModificationDate(), LAST_MODIFICATION_DATE);
    }

}