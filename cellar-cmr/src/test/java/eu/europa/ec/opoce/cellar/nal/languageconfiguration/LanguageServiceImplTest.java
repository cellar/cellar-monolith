/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.nal.languageconfiguration
 *             FILE : LanguageServiceTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 25 Jan 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.nal.languageconfiguration;

import eu.europa.ec.opoce.cellar.LanguageIsoCode;
import eu.europa.ec.opoce.cellar.cl.configuration.PrefixConfigurationService;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Expression;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Work;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.nal.service.LanguageService;
import eu.europa.ec.opoce.cellar.nal.service.impl.LanguageServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import static org.mockito.Mockito.mock;

/**
 * <class_description> Test class to assert the execution of methods in the LanguageService class.
 * <br/><br/>
 * <notes>
 * //Prioritize mono-lingual expressions over multi-lingual ones
 * //Discarding multi-lingual expressions that reference a language used by any mono-lingual expression
 * //keep the “first original”. Discarding any multi-lingual expression that references a language used by any predecessor
 * .
 * <br/><br/>
 * ON : 25 Jan 2017
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(ServiceLocator.class)
@PowerMockIgnore({"javax.management.*", "javax.xml.*", "org.xml.sax.*", "org.w3c.dom.*"})
public class LanguageServiceImplTest {

    /**
     * The french language bean.
     */
    private final LanguageBean frenchLanguageBean = mockLanguageBean("fra");
    /**
     * The english language bean.
     */
    private final LanguageBean englishLanguageBean = mockLanguageBean("eng");
    /**
     * The german language bean.
     */
    private final LanguageBean germanLanguageBean = mockLanguageBean("deu");
    /**
     * The italian language bean.
     */
    private final LanguageBean italianLanguageBean = mockLanguageBean("ita");
    /**
     * The portuguese language bean.
     */
    private final LanguageBean portugueseLanguageBean = mockLanguageBean("por");
    /**
     * The french language iso code.
     */
    private final LanguageIsoCode frenchLanguageIsoCode = mockLanguageIsoCode("fra");
    /**
     * The english language iso code.
     */
    private final LanguageIsoCode englishLanguageIsoCode = mockLanguageIsoCode("eng");
    /**
     * The german language iso code.
     */
    private final LanguageIsoCode germanLanguageIsoCode = mockLanguageIsoCode("deu");
    /**
     * The italian language iso code.
     */
    private final LanguageIsoCode italianLanguageIsoCode = mockLanguageIsoCode("ita");
    /**
     * The portuguese language iso code.
     */
    private final LanguageIsoCode portugueseLanguageIsoCode = mockLanguageIsoCode("por");
    /**
     * The language service.
     */
    private LanguageService languageService;

    /**
     * Inits the ServiceLocator static class.
     */
    @Before
    public void init() {
        final PrefixConfigurationService prefixConfigurationService = mock(PrefixConfigurationService.class);
        Mockito.when(prefixConfigurationService.getPrefixConfiguration()).thenReturn(null);
        PowerMockito.mockStatic(ServiceLocator.class);
        PowerMockito.when(ServiceLocator.getService(PrefixConfigurationService.class)).thenReturn(prefixConfigurationService);

        languageService = new LanguageServiceImpl();
    }

    /**
     * Test filter multi linguistic expressions.
     * the second should be excluded because of the ordering of the expressions is based on the cellar id
     */
    @Test
    public void testFilterMultiLinguisticExpressionsUsingWork_1() {

        final LanguageBean[] languages1 = new LanguageBean[]{
                frenchLanguageBean};
        final LanguageBean[] languages2 = new LanguageBean[]{
                englishLanguageBean, germanLanguageBean, italianLanguageBean};
        final LanguageBean[] languages3 = new LanguageBean[]{
                italianLanguageBean, portugueseLanguageBean};

        final Expression monolingualExpression = mockExpression(languages1, "cellar:xxxx.0001");
        final Expression firstMultiLingualExpression = mockExpression(languages2, "cellar:xxxx.0002");
        final Expression secondMultiLingualExpression = mockExpression(languages3, "cellar:xxxx.0003");
        final Work work = mockWork("cellar:xxxx", monolingualExpression, firstMultiLingualExpression, secondMultiLingualExpression);

        final List<Expression> filterMultiLinguisticExpressions = languageService.filterMultiLinguisticExpressions(work);

        Assert.assertTrue(filterMultiLinguisticExpressions.contains(monolingualExpression));
        Assert.assertTrue(filterMultiLinguisticExpressions.contains(firstMultiLingualExpression));
        Assert.assertFalse(filterMultiLinguisticExpressions.contains(secondMultiLingualExpression));

    }

    /**
     * Test filter multi linguistic expressions using work .
     * the first should be excluded because of the ordering of the expressions is based on the cellar id
     */
    @Test
    public void testFilterMultiLinguisticExpressionsUsingWork_2() {

        final LanguageBean[] languages1 = new LanguageBean[]{
                frenchLanguageBean};
        final LanguageBean[] languages2 = new LanguageBean[]{
                englishLanguageBean, germanLanguageBean, italianLanguageBean};
        final LanguageBean[] languages3 = new LanguageBean[]{
                italianLanguageBean, portugueseLanguageBean};

        final Expression monolingualExpression = mockExpression(languages1, "cellar:xxxx.0001");
        final Expression firstMultiLingualExpression = mockExpression(languages2, "cellar:xxxx.0004");
        final Expression secondMultiLingualExpression = mockExpression(languages3, "cellar:xxxx.0003");
        final Work work = mockWork("cellar:xxxx", monolingualExpression, firstMultiLingualExpression, secondMultiLingualExpression);

        final List<Expression> filterMultiLinguisticExpressions = languageService.filterMultiLinguisticExpressions(work);

        Assert.assertTrue(filterMultiLinguisticExpressions.contains(monolingualExpression));
        Assert.assertFalse(filterMultiLinguisticExpressions.contains(firstMultiLingualExpression));
        Assert.assertTrue(filterMultiLinguisticExpressions.contains(secondMultiLingualExpression));

    }

    /**
     * Test filter multi linguistic expressions using work
     * the first should be excluded because its referencing the language of a monolingual
     */
    @Test
    public void testFilterMultiLinguisticExpressionsUsingWork_3() {

        final LanguageBean[] languages1 = new LanguageBean[]{
                frenchLanguageBean};
        final LanguageBean[] languages2 = new LanguageBean[]{
                englishLanguageBean, germanLanguageBean, italianLanguageBean, frenchLanguageBean};
        final LanguageBean[] languages3 = new LanguageBean[]{
                italianLanguageBean, portugueseLanguageBean};

        final Expression monolingualExpression = mockExpression(languages1, "cellar:xxxx.0001");
        final Expression firstMultiLingualExpression = mockExpression(languages2, "cellar:xxxx.0002");
        final Expression secondMultiLingualExpression = mockExpression(languages3, "cellar:xxxx.0003");
        final Work work = mockWork("cellar:xxxx", monolingualExpression, firstMultiLingualExpression, secondMultiLingualExpression);

        final List<Expression> filterMultiLinguisticExpressions = languageService.filterMultiLinguisticExpressions(work);

        Assert.assertTrue(filterMultiLinguisticExpressions.contains(monolingualExpression));
        Assert.assertFalse(filterMultiLinguisticExpressions.contains(firstMultiLingualExpression));
        Assert.assertTrue(filterMultiLinguisticExpressions.contains(secondMultiLingualExpression));

    }

    /**
     * Test filter multi linguistic expressions using dos
     * the second expression and manifestation should be excluded because of the ordering of the expressions is based on the cellar id
     */
    @Test
    public void testFilterMultiLinguisticExpressionsUsingDos_1() {

        final DigitalObject work = mockDigitalObject(DigitalObjectType.WORK, null, "cellar:xxx");
        final DigitalObject monolingualExpression = mockDigitalObject(DigitalObjectType.EXPRESSION, work, "cellar:xxx.0001",
                frenchLanguageIsoCode);
        final DigitalObject monolingualManifestation = mockDigitalObject(DigitalObjectType.MANIFESTATION, monolingualExpression,
                "cellar:xxx.0001.01");
        final DigitalObject firstMultiLingualExpression = mockDigitalObject(DigitalObjectType.EXPRESSION, work, "cellar:xxx.0002",
                englishLanguageIsoCode, germanLanguageIsoCode, italianLanguageIsoCode);
        final DigitalObject firstMultiLingualManifestation = mockDigitalObject(DigitalObjectType.MANIFESTATION, firstMultiLingualExpression,
                "cellar:xxx.0002.01");
        final DigitalObject secondMultiLingualExpression = mockDigitalObject(DigitalObjectType.EXPRESSION, work, "cellar:xxx.0003",
                italianLanguageIsoCode, portugueseLanguageIsoCode);
        final DigitalObject secondMultiLingualManifestation = mockDigitalObject(DigitalObjectType.MANIFESTATION,
                secondMultiLingualExpression, "cellar:xxx.0003.01");

        final Collection<DigitalObject> filterDigitalObjects = languageService.filterMultiLinguisticExpressions(
                Arrays.asList(work, monolingualExpression, monolingualManifestation, firstMultiLingualExpression,
                        firstMultiLingualManifestation, secondMultiLingualExpression, secondMultiLingualManifestation));

        Assert.assertTrue(filterDigitalObjects.contains(work));
        Assert.assertTrue(filterDigitalObjects.contains(monolingualExpression));
        Assert.assertTrue(filterDigitalObjects.contains(monolingualManifestation));
        Assert.assertTrue(filterDigitalObjects.contains(firstMultiLingualExpression));
        Assert.assertTrue(filterDigitalObjects.contains(firstMultiLingualManifestation));
        Assert.assertFalse(filterDigitalObjects.contains(secondMultiLingualExpression));
        Assert.assertFalse(filterDigitalObjects.contains(secondMultiLingualManifestation));

    }

    /**
     * Test filter multi linguistic expressions using dos .
     * the first should be excluded because of the ordering of the expressions is based on the cellar id
     */
    @Test
    public void testFilterMultiLinguisticExpressionsUsingDos_2() {

        final DigitalObject work = mockDigitalObject(DigitalObjectType.WORK, null, "cellar:xxx");

        final DigitalObject monolingualExpression = mockDigitalObject(DigitalObjectType.EXPRESSION, work, "cellar:xxx.0001",
                frenchLanguageIsoCode);
        final DigitalObject monolingualManifestation = mockDigitalObject(DigitalObjectType.MANIFESTATION, monolingualExpression,
                "cellar:xxx.0001.01");

        final DigitalObject firstMultiLingualExpression = mockDigitalObject(DigitalObjectType.EXPRESSION, work, "cellar:xxx.0004",
                englishLanguageIsoCode, germanLanguageIsoCode, italianLanguageIsoCode);
        final DigitalObject firstMultiLingualManifestation = mockDigitalObject(DigitalObjectType.MANIFESTATION, firstMultiLingualExpression,
                "cellar:xxx.0004.01");

        final DigitalObject secondMultiLingualExpression = mockDigitalObject(DigitalObjectType.EXPRESSION, work, "cellar:xxx.0003",
                italianLanguageIsoCode, portugueseLanguageIsoCode);
        final DigitalObject secondMultiLingualManifestation = mockDigitalObject(DigitalObjectType.MANIFESTATION,
                secondMultiLingualExpression, "cellar:xxx.0003.01");

        final Collection<DigitalObject> filterDigitalObjects = languageService.filterMultiLinguisticExpressions(
                Arrays.asList(work, monolingualExpression, monolingualManifestation, firstMultiLingualExpression,
                        firstMultiLingualManifestation, secondMultiLingualExpression, secondMultiLingualManifestation));

        Assert.assertTrue(filterDigitalObjects.contains(work));
        Assert.assertTrue(filterDigitalObjects.contains(monolingualExpression));
        Assert.assertTrue(filterDigitalObjects.contains(monolingualManifestation));
        Assert.assertFalse(filterDigitalObjects.contains(firstMultiLingualExpression));
        Assert.assertFalse(filterDigitalObjects.contains(firstMultiLingualManifestation));
        Assert.assertTrue(filterDigitalObjects.contains(secondMultiLingualExpression));
        Assert.assertTrue(filterDigitalObjects.contains(secondMultiLingualManifestation));

    }

    /**
     * Test filter multi linguistic expressions using dos .
     * the first should be excluded because its referencing the language of a monolingual
     */
    @Test
    public void testFilterMultiLinguisticExpressionsUsingDos_3() {

        final DigitalObject work = mockDigitalObject(DigitalObjectType.WORK, null, "cellar:xxx");

        final DigitalObject monolingualExpression = mockDigitalObject(DigitalObjectType.EXPRESSION, work, "cellar:xxx.0001",
                frenchLanguageIsoCode);
        final DigitalObject monolingualManifestation = mockDigitalObject(DigitalObjectType.MANIFESTATION, monolingualExpression,
                "cellar:xxx.0001.01");

        final DigitalObject firstMultiLingualExpression = mockDigitalObject(DigitalObjectType.EXPRESSION, work, "cellar:xxx.0002",
                englishLanguageIsoCode, germanLanguageIsoCode, italianLanguageIsoCode, frenchLanguageIsoCode);
        final DigitalObject firstMultiLingualManifestation = mockDigitalObject(DigitalObjectType.MANIFESTATION, firstMultiLingualExpression,
                "cellar:xxx.0002.01");

        final DigitalObject secondMultiLingualExpression = mockDigitalObject(DigitalObjectType.EXPRESSION, work, "cellar:xxx.0003",
                italianLanguageIsoCode, portugueseLanguageIsoCode);
        final DigitalObject secondMultiLingualManifestation = mockDigitalObject(DigitalObjectType.MANIFESTATION,
                secondMultiLingualExpression, "cellar:xxx.0003.01");

        final Collection<DigitalObject> filterDigitalObjects = languageService.filterMultiLinguisticExpressions(
                Arrays.asList(work, monolingualExpression, monolingualManifestation, firstMultiLingualExpression,
                        firstMultiLingualManifestation, secondMultiLingualExpression, secondMultiLingualManifestation));

        Assert.assertTrue(filterDigitalObjects.contains(work));
        Assert.assertTrue(filterDigitalObjects.contains(monolingualExpression));
        Assert.assertTrue(filterDigitalObjects.contains(monolingualManifestation));
        Assert.assertFalse(filterDigitalObjects.contains(firstMultiLingualExpression));
        Assert.assertFalse(filterDigitalObjects.contains(firstMultiLingualManifestation));
        Assert.assertTrue(filterDigitalObjects.contains(secondMultiLingualExpression));
        Assert.assertTrue(filterDigitalObjects.contains(secondMultiLingualManifestation));

    }

    /**
     * Mock digital object.
     *
     * @param digitalObjectType the digital object type
     * @param parentObject      the parent object
     * @param cellarId          the cellar id
     * @param languages         the languages
     * @return the digital object
     */
    private DigitalObject mockDigitalObject(DigitalObjectType digitalObjectType, DigitalObject parentObject, final String cellarId,
                                            LanguageIsoCode... languages) {
        final ContentIdentifier contenIdentifier = mock(ContentIdentifier.class);
        Mockito.when(contenIdentifier.getIdentifier()).thenReturn(cellarId);
        final StructMap structMap = mock(StructMap.class);
        final DigitalObject result = new DigitalObject(structMap);
        result.setType(digitalObjectType);
        result.setCellarId(contenIdentifier);
        if (parentObject != null) {
            result.setParentObject(parentObject);
        }
        if (languages.length != 0) {
            result.setLanguages(Arrays.asList(languages));
        }
        return result;
    }

    /**
     * Mock language bean.
     *
     * @param language the language
     * @return the language bean
     */
    private LanguageBean mockLanguageBean(final String language) {
        LanguageBean languageBean = new LanguageBean();
        languageBean.setCode(language);
        return languageBean;
    }

    /**
     * Mock language iso code.
     *
     * @param language the language
     * @return the language iso code
     */
    private LanguageIsoCode mockLanguageIsoCode(final String language) {
        LanguageIsoCode languageIsoCode= new LanguageIsoCode();
        languageIsoCode.setIsoCodeThreeChar(language);
        return languageIsoCode;
    }

    /**
     * Mock work.
     *
     * @param cellarId    the cellar id
     * @param expressions the expressions
     * @return the work
     */
    private Work mockWork(final String cellarId, final Expression... expressions) {
        final ContentIdentifier contenIdentifier = mock(ContentIdentifier.class);
        Mockito.when(contenIdentifier.getIdentifier()).thenReturn(cellarId);
        final Work result = new Work();
        result.setCellarId(contenIdentifier);
        final Collection<Expression> children = Arrays.asList(expressions);
        result.setChildren(children);
        return result;
    }

    /**
     * Mock expression.
     *
     * @param languages the languages
     * @param cellarId  the cellar id
     * @return the expression
     */
    private Expression mockExpression(final LanguageBean[] languages, final String cellarId) {
        final ContentIdentifier contentIdentifier = mock(ContentIdentifier.class);
        Mockito.when(contentIdentifier.getIdentifier()).thenReturn(cellarId);
        final Expression result = new Expression();
        result.setCellarId(contentIdentifier);
        result.setLangs(new HashSet<>(Arrays.asList(languages)));
        return result;
    }

}
