/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr
 *             FILE : CmrIndexGenerationServiceImplTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 02 13, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-02-13 16:15:46 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr;

import eu.europa.ec.opoce.cellar.cmr.notice.indexing.IndexNotice;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import org.junit.Test;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Collection;

/**
 * @author ARHS Developments
 */
public class CmrIndexGenerationServiceImplTest {

    @Test
    public void calculateExpandedNoticesMustNeverReturnNull() {
        CmrIndexGenerationServiceImpl cmrIndexRequestGenerationService = new CmrIndexGenerationServiceImpl(null,
                null, null, null, null, null,
                null, null, null, null, null);

        CmrIndexRequest expression = new CmrIndexRequest();
        expression.setGroupByUri("cellar:12345678901234567890");
        expression.setObjectType(DigitalObjectType.EXPRESSION);

        Assert.notNull(cmrIndexRequestGenerationService.calculateExpandedNotices(expression, generateNotices()));

        CmrIndexRequest manifestation = new CmrIndexRequest();
        manifestation.setGroupByUri("cellar:12345678901234567890");
        manifestation.setObjectType(DigitalObjectType.MANIFESTATION);

        Assert.notNull(cmrIndexRequestGenerationService.calculateExpandedNotices(manifestation, generateNotices()));
    }

    private static Collection<IndexNotice> generateNotices() {
        Collection<IndexNotice> notices = new ArrayList<>();
        notices.add(newIndexNotice("cellar:12345678901234567890"));
        notices.add(newIndexNotice("cellar:12345678901234567890.01"));
        notices.add(newIndexNotice("cellar:12345678901234567890.02"));
        notices.add(newIndexNotice("cellar:12345678901234567890.01.01"));
        notices.add(newIndexNotice("cellar:12345678901234567890.02.01"));
        return notices;
    }

    private static IndexNotice newIndexNotice(String cellarUri) {
        IndexNotice in = new IndexNotice();
        in.setCellarUri("cellar:12345678901234567890");
        return in;
    }
}
