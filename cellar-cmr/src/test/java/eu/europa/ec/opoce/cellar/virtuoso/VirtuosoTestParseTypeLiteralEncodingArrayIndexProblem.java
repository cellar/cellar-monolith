package eu.europa.ec.opoce.cellar.virtuoso;

import org.apache.commons.lang.CharEncoding;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.RDFReader;
import org.springframework.core.io.FileSystemResource;
import virtuoso.jdbc4.VirtuosoDataSource;
import virtuoso.jena.driver.VirtDataset;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

public class VirtuosoTestParseTypeLiteralEncodingArrayIndexProblem {

    private static VirtuosoDataSource virtuosoDataSource;

    public static void main(String[] args) throws Exception {
        final String cellarId = UUID.randomUUID().toString();
        final String graphName = "http://cellar-dev.publications.europa.eu/resource/cellar/" + cellarId;

        // read model from fs
        final File fileIn = new File(
                "C:\\projects\\CELLAR\\Trunk\\cellar-app\\cellar-cmr\\src\\test\\resources\\VirtuosoTestParseTypeLiteralEncodingArrayIndexProblem.rdf");
        final Model modelIn = read(fileIn);

        // write model to Virtuoso
        writeModel(graphName, modelIn);

        // read model from Virtuoso
        final Model modelOut = readModel(graphName);
        modelOut.write(System.out);
    }

    private static VirtuosoDataSource getVirtuosoDataSource() throws Exception {
        if (virtuosoDataSource != null) {
            return virtuosoDataSource;
        }

        final String serverName = "10.2.0.244";
        final int serverPort = 1114;
        //final String serverName = "10.2.0.224";
        //final int serverPort = 1118;
        virtuosoDataSource = new VirtuosoDataSource();
        virtuosoDataSource.setUser("dba");
        virtuosoDataSource.setPassword("dba");
        virtuosoDataSource.setDataSourceName("jdbc:virtuoso://" + serverName + ":" + serverPort + "/charset=UTF-8");
        virtuosoDataSource.setServerName(serverName);
        virtuosoDataSource.setPortNumber(serverPort);
        virtuosoDataSource.setCharset(CharEncoding.UTF_8);
        return virtuosoDataSource;
    }

    private static void writeModel(final String graphName, final Model newModel) throws Exception {
        VirtDataset dataset = null;
        try {
            // gets the connection
            dataset = new VirtDataset(getVirtuosoDataSource());

            // get direct model of digital objects that are supposed to be inserted
            Model existingModel = null;
            try {
                // load existing model
                existingModel = dataset.getNamedModel(graphName);

                // update new model
                existingModel.removeAll();
                existingModel.add(newModel);
            } finally {
                closeQuietly(existingModel, newModel);
            }
        } finally {
            if (dataset != null) {
                dataset.close();
            }
        }
    }

    private static Model readModel(final String graphName) throws Exception {
        final Model retModel = ModelFactory.createDefaultModel();

        VirtDataset dataset = null;
        Model existingModel = null;
        try {
            dataset = new VirtDataset(getVirtuosoDataSource());
            existingModel = dataset.getNamedModel(graphName);
            retModel.add(existingModel);
        } finally {
            closeQuietly(existingModel);
            if (dataset != null) {
                dataset.close();
            }
        }

        return retModel;
    }

    private static void closeQuietly(final Model... models) {
        for (final Model model : models) {
            if (model != null) {
                model.close();
            }
        }
    }

    private static Model read(final File file) throws IOException {
        final Collection<org.springframework.core.io.Resource> resources = new ArrayList<org.springframework.core.io.Resource>();
        resources.add(new FileSystemResource(file));

        final Model model = ModelFactory.createDefaultModel();

        for (org.springframework.core.io.Resource resource : resources) {
            InputStream inputstream = null;
            try {
                inputstream = resource.getInputStream();
                final RDFReader rdfReader = getBasicReader(model, resource);
                rdfReader.read(model, inputstream, null);
            } catch (RuntimeException e) {
                closeQuietly(model);
                throw e;
            } finally {
                if (inputstream != null) {
                    inputstream.close();
                }
            }
        }

        return model;
    }

    private static RDFReader getBasicReader(final Model model, final org.springframework.core.io.Resource resource) {
        try {
            final String filename = resource.getFilename();
            if (filename == null) {
                throw new IllegalStateException("resource loaded from byte array does not have a filename");
            }
            final String extension = StringUtils.substringAfterLast(filename, ".").toLowerCase();

            if ("owl".equals(extension)) {
                return model.getReader();
            } else if ("nt".equals(extension)) {
                return model.getReader("N-TRIPLE");
            } else if ("n3".equals(extension) || "ttl".equals(extension)) {
                return model.getReader("TURTLE");
            }

            return model.getReader();
        } catch (IllegalStateException e) {
            return model.getReader();
        }
    }

}
