package eu.europa.ec.opoce.cellar.cmr.daobeans;

import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

public class InverseRelationTest {
    private InverseRelation inverseRelationOne;
    private InverseRelation inverseRelationTwo;
    @Before
    public void setup() throws Exception {
        inverseRelationOne = new InverseRelation();
        inverseRelationTwo = new InverseRelation();
    }

    @Test
    public void objectShouldNotEqualNull() throws Exception {
        assertFalse(inverseRelationOne.equals(null));
    }

    @Test
    public void objectShouldEqualItself() throws Exception {
        assertTrue(inverseRelationOne.equals(inverseRelationOne));
    }

    @Test
    public void objectShouldNotThrowNPEWithNullValues() throws Exception {
        inverseRelationOne.setSource(null);
        inverseRelationOne.setTarget(null);
        inverseRelationOne.setSourceType(null);
        inverseRelationOne.setTargetType(null);
        inverseRelationOne.setPropertiesSourceTarget(null);
        inverseRelationOne.setPropertiesTargetSource(null);

        inverseRelationTwo.setSource(null);
        inverseRelationTwo.setTarget(null);
        inverseRelationTwo.setSourceType(null);
        inverseRelationTwo.setTargetType(null);
        inverseRelationTwo.setPropertiesSourceTarget(null);
        inverseRelationTwo.setPropertiesTargetSource(null);
        assertTrue(inverseRelationOne.equals(inverseRelationTwo));
    }

    @Test
    public void twoObjectsWithEqualPropertiesShouldBeEqual() throws Exception {
        inverseRelationOne.setSource("source");
        inverseRelationOne.setTarget("target");
        inverseRelationOne.setSourceType(DigitalObjectType.WORK);
        inverseRelationOne.setTargetType(DigitalObjectType.WORK);
        inverseRelationOne.setPropertiesSourceTarget(new HashSet<>());
        inverseRelationOne.setPropertiesTargetSource(new HashSet<>());

        inverseRelationTwo.setSource("source");
        inverseRelationTwo.setTarget("target");
        inverseRelationTwo.setSourceType(DigitalObjectType.WORK);
        inverseRelationTwo.setTargetType(DigitalObjectType.WORK);
        inverseRelationTwo.setPropertiesSourceTarget(new HashSet<>());
        inverseRelationTwo.setPropertiesTargetSource(new HashSet<>());
        assertTrue(inverseRelationOne.equals(inverseRelationTwo));
    }

    @Test
    public void oneObjectShouldHaveSameHashCode() throws Exception {
        inverseRelationOne.setSource("source");
        inverseRelationOne.setTarget("target");
        inverseRelationOne.setSourceType(DigitalObjectType.WORK);
        inverseRelationOne.setTargetType(DigitalObjectType.WORK);
        inverseRelationOne.setPropertiesSourceTarget(new HashSet<>());
        inverseRelationOne.setPropertiesTargetSource(new HashSet<>());
        assertEquals(inverseRelationOne.hashCode(), inverseRelationOne.hashCode());
    }

    @Test
    public void objectWithNullParamsShouldNotThrowNPEForHashCode() throws Exception {
        inverseRelationOne.setSource(null);
        inverseRelationOne.setTarget(null);
        inverseRelationOne.setSourceType(null);
        inverseRelationOne.setTargetType(null);
        inverseRelationOne.setPropertiesSourceTarget(null);
        inverseRelationOne.setPropertiesTargetSource(null);
        assertEquals(inverseRelationOne.hashCode(), inverseRelationOne.hashCode());
    }

    @Test
    public void twoObjectsWithSameParamsShouldHaveSameHashcode() throws Exception {
        inverseRelationOne.setSource("source");
        inverseRelationOne.setTarget("target");
        inverseRelationOne.setSourceType(DigitalObjectType.WORK);
        inverseRelationOne.setTargetType(DigitalObjectType.WORK);
        inverseRelationOne.setPropertiesSourceTarget(new HashSet<>());
        inverseRelationOne.setPropertiesTargetSource(new HashSet<>());

        inverseRelationTwo.setSource("source");
        inverseRelationTwo.setTarget("target");
        inverseRelationTwo.setSourceType(DigitalObjectType.WORK);
        inverseRelationTwo.setTargetType(DigitalObjectType.WORK);
        inverseRelationTwo.setPropertiesSourceTarget(new HashSet<>());
        inverseRelationTwo.setPropertiesTargetSource(new HashSet<>());

        assertEquals(inverseRelationOne.hashCode(), inverseRelationTwo.hashCode());
    }

    @Test
    public void twoEqualObjectsHaveSameHashCode() throws Exception {
        inverseRelationOne.setSource("source");
        inverseRelationOne.setTarget("target");
        inverseRelationOne.setSourceType(DigitalObjectType.WORK);
        inverseRelationOne.setTargetType(DigitalObjectType.WORK);
        inverseRelationOne.setPropertiesSourceTarget(new HashSet<>());
        inverseRelationOne.setPropertiesTargetSource(new HashSet<>());

        inverseRelationTwo.setSource("source");
        inverseRelationTwo.setTarget("target");
        inverseRelationTwo.setSourceType(DigitalObjectType.WORK);
        inverseRelationTwo.setTargetType(DigitalObjectType.WORK);
        inverseRelationTwo.setPropertiesSourceTarget(new HashSet<>());
        inverseRelationTwo.setPropertiesTargetSource(new HashSet<>());

        assertEquals(inverseRelationOne.hashCode(), inverseRelationTwo.hashCode());
        assertTrue(inverseRelationOne.equals(inverseRelationTwo));
    }

    @Test
    public void twoObjectsWithDifferentValuesHaveDifferentHashCode() throws Exception {
        inverseRelationOne.setTarget("target");
        inverseRelationTwo.setTarget("target2");
        assertNotEquals(inverseRelationOne.hashCode(), inverseRelationTwo.hashCode());
        assertNotEquals(inverseRelationOne, inverseRelationTwo);
    }

    @Test
    public void twoObjectsWithSameTargetAndDifferentFieldsShouldNotBeEqual() throws Exception {
        inverseRelationOne.setTarget("target");
        inverseRelationOne.setSource("sourceOne");

        inverseRelationTwo.setTarget("target");
        inverseRelationTwo.setSource("sourceTwo");
        assertNotEquals(inverseRelationOne.hashCode(), inverseRelationTwo.hashCode());
        assertFalse(inverseRelationOne.equals(inverseRelationTwo));
    }
}
