package eu.europa.ec.opoce.cellar.ontology.service.impl;

import eu.europa.ec.opoce.cellar.cmr.database.dao.OntoConfigDao;
import eu.europa.ec.opoce.cellar.ontology.dao.OntoConfig;
import eu.europa.ec.opoce.cellar.ontology.domain.OntologyUpdate;
import eu.europa.ec.opoce.cellar.ontology.service.OntologyUpdateResult;
import eu.europa.ec.opoce.cellar.ontology.service.OntologyUpdateService;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import eu.europa.ec.opoce.cellar.virtuoso.VirtuosoService;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.ApplicationEventPublisher;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class OntologyUpdateServiceImplTest {

    @Mock
    private OntoConfigDao ontoConfigDao;
    @Mock
    private ContentStreamService contentStreamService;
    @Mock
    private ApplicationEventPublisher eventPublisher;
    @Mock
    private VirtuosoService virtuosoService;

    private OntologyUpdateService ontologyUpdateService;

    @Before
    public void init() {
        ontologyUpdateService = new OntologyUpdateServiceImpl(ontoConfigDao, contentStreamService, eventPublisher, virtuosoService);
    }

    @Test
    public void updateVirtuoso() throws IOException {

        try(final InputStream inputStream = new FileInputStream("src/test/resources/cdm4Example.rdf")) {
            final String cmdUri = "http://publications.europa.eu/ontology/cdm";
            final OntologyUpdate ontologyUpdate = new OntologyUpdate(cmdUri);

            final Model model = ModelFactory.createDefaultModel();
            model.read(inputStream, "UTF-8");

            final OntoConfig ontoConfig = mock(OntoConfig.class);
            when(ontoConfigDao.findByUri(cmdUri)).thenReturn(Optional.of(ontoConfig));

            ontologyUpdate.getUpdates().put(cmdUri, model);

            final OntologyUpdateResult ontologyUpdateResult = ontologyUpdateService.update(ontologyUpdate, true);
            final List<Model> models = ontologyUpdateResult.getModels().entrySet().stream()
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toList());
            Assert.assertTrue(models.contains(model));

            verify(virtuosoService).writeOnto(model, cmdUri, true);
        }
    }

}