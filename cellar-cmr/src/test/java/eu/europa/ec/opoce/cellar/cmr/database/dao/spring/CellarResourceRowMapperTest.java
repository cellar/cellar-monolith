package eu.europa.ec.opoce.cellar.cmr.database.dao.spring;

import eu.europa.ec.opoce.cellar.cmr.AlignerStatus;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class CellarResourceRowMapperTest {

    private static final long ID = 1L;
    private static final String CELLAR_ID = "cellar:eb53b233-cacc-11e8-bf98-01aa75ed71a1";
    private static final String CELLAR_TYPE = "WORK";
    private static final Timestamp EMBARGO_DATE = Timestamp.from(Instant.parse("2018-08-10T01:00:00.00Z"));
    private static final String MIME_TYPE = "pdf";
    private static final String LANGUAGE = "FRA";
    private static final Timestamp LAST_MODIFICATION_DATE = Timestamp.from(Instant.parse("2018-08-10T02:00:00.00Z"));
    private static final Timestamp EMBEDDED_NOTICE_CREATION_DATE = Timestamp.from(Instant.parse("2018-08-10T03:00:00.00Z"));
    private static final String EMBEDDED_NOTICE = "<xml ... />";
    private static final byte CLEANER_STATUS = 0;
    private static final byte ALIGNER_STATUS = 0;
    private static final Timestamp LAST_INDEXATION = Timestamp.from(Instant.parse("2018-08-10T04:00:00.00Z"));

    @Test
    public void mapRowTest() throws SQLException {
        final ResultSet rs = mock(ResultSet.class);

        when(rs.getLong("ID")).thenReturn(ID);
        when(rs.getString("CELLAR_ID")).thenReturn(CELLAR_ID);
        when(rs.getString("CELLAR_TYPE")).thenReturn(CELLAR_TYPE);
        when(rs.getTimestamp("EMBARGO_DATE")).thenReturn(EMBARGO_DATE);
        when(rs.getString("MIME_TYPE")).thenReturn(MIME_TYPE);
        when(rs.getString("LANGUAGE")).thenReturn(LANGUAGE);
        when(rs.getTimestamp("LAST_MODIFICATION_DATE")).thenReturn(LAST_MODIFICATION_DATE);
        when(rs.getTimestamp("EMBEDDED_NOTICE_CREATION_DATE")).thenReturn(EMBEDDED_NOTICE_CREATION_DATE);
        when(rs.getString("EMBEDDED_NOTICE")).thenReturn(EMBEDDED_NOTICE);
        when(rs.getByte("CLEANER_STATUS")).thenReturn(CLEANER_STATUS);
        when(rs.getByte("ALIGNER_STATUS")).thenReturn(ALIGNER_STATUS);
        when(rs.getTimestamp("LAST_INDEXATION")).thenReturn(LAST_INDEXATION);

        final SpringCellarResourceDao.CellarResourceRowMapper rowMapper
                = new SpringCellarResourceDao.CellarResourceRowMapper(Collections.singletonList("EMBEDDED_NOTICE"));
        final CellarResource cellarResource = rowMapper.mapRow(rs, 0);

        assertEquals((long)cellarResource.getId(), ID);
        assertEquals(cellarResource.getCellarId(), CELLAR_ID);
        assertEquals(cellarResource.getCellarType(), DigitalObjectType.WORK);
        assertEquals(cellarResource.getEmbargoDate(), EMBARGO_DATE);
        assertEquals(cellarResource.getMimeType(), MIME_TYPE);
        assertEquals(cellarResource.getLanguages().size(), 1);
        assertEquals(cellarResource.getLanguages().get(0), LANGUAGE);
        assertEquals(cellarResource.getLastModificationDate(), LAST_MODIFICATION_DATE);
        assertEquals(cellarResource.getEmbeddedNoticeCreationDate(), EMBEDDED_NOTICE_CREATION_DATE);
        assertEquals(cellarResource.getEmbeddedNotice(), EMBEDDED_NOTICE);
        assertEquals(cellarResource.getCleanerStatus(), CellarResource.CleanerStatus.UNKNOWN);
        assertEquals(cellarResource.getAlignerStatus(), AlignerStatus.UNKNOWN);
        assertEquals(cellarResource.getLastIndexation(), LAST_INDEXATION);
    }
}