/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.nal.service.impl
 *             FILE : NalSemanticServiceImplTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 03 14, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-03-14 07:09:43 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.nal.service.impl;

import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.database.dao.OntoConfigDao;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.nal.NalOntoUtils;
import eu.europa.ec.opoce.cellar.ontology.dao.OntoConfig;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Optional;

import static org.mockito.Mockito.*;

/**
 * @author ARHS Developments
 */
public class NalSemanticServiceImplTest {

    private static final String ONTO_AT = "onto/at";
    private static final String ONTO_AT_VERSION = "gmTHQ8hEie_geVmqWEQe5xpC3rauhT10";
    private static final String ONTO_EUROVOC = "onto/eurovoc";
    private static final String ONTO_EUROVOC_VERSION = "JOCTXPtmYLx_7Lrwzh.07IQS2ecqe6TF";

    private static ContentStreamService contentStreamService;
    private static OntoConfigDao ontoConfigDao;
    private NalSemanticServiceImpl nalSemanticService;

    @BeforeClass
    public static void oneTimeSetUp() {
        contentStreamService = mock(ContentStreamService.class);
        when(contentStreamService.getContentAsString(ONTO_AT, ONTO_AT_VERSION, ContentType.DIRECT)).thenReturn(Optional.of("" +
                "<http://publications.europa.eu/resource/authority/language/language.classification-OTHER> <http://www.w3.org/2000/01/rdf-schema#label> \"Other\"@en ."));
        when(contentStreamService.getContentAsString(ONTO_EUROVOC, ONTO_EUROVOC_VERSION, ContentType.DIRECT)).thenReturn(Optional.of("" +
                "<http://publications.europa.eu/ontology/euvoc#officeAddress> <http://www.w3.org/2000/01/rdf-schema#subPropertyOf> <http://www.w3.org/2006/vcard/ns#street-address> ."));

        ontoConfigDao = mock(OntoConfigDao.class);
        OntoConfig authorityOnto = new OntoConfig();
        authorityOnto.setExternalPid(ONTO_AT);
        authorityOnto.setVersion(ONTO_AT_VERSION);
        when(ontoConfigDao.findByUri(NalOntoUtils.AUTHORITY_ONTOLOGY_URI)).thenReturn(Optional.of(authorityOnto));

        OntoConfig eurovocOnto = new OntoConfig();
        eurovocOnto.setExternalPid(ONTO_EUROVOC);
        eurovocOnto.setVersion(ONTO_EUROVOC_VERSION);
        when(ontoConfigDao.findByUri(NalOntoUtils.EUROVOC_ONTOLOGY_URI)).thenReturn(Optional.of(eurovocOnto));
    }

    @Before
    public void setUp() {
        nalSemanticService = new NalSemanticServiceImpl(contentStreamService, ontoConfigDao);
    }

    @Test
    public void authorityOntologyUriIsRetrievedFromDbThenFetchFromS3() {
        nalSemanticService.getAuthorityOntology();
        verify(ontoConfigDao, times(1)).findByUri(NalOntoUtils.AUTHORITY_ONTOLOGY_URI);
        verify(contentStreamService, times(1)).getContentAsString(ONTO_AT, ONTO_AT_VERSION, ContentType.DIRECT);
    }

    @Test
    public void eurovocOntologyUriIsRetrievedFromDbThenFetchFromS3() {
        nalSemanticService.getEurovocOntology();
        verify(ontoConfigDao, times(1)).findByUri(NalOntoUtils.EUROVOC_ONTOLOGY_URI);
        verify(contentStreamService, times(1)).getContentAsString(ONTO_EUROVOC, ONTO_EUROVOC_VERSION, ContentType.DIRECT);
    }

    @Test(expected = CellarException.class)
    public void exceptionWhenOntologyNotFoundInDb() {
        when(ontoConfigDao.findByUri(NalOntoUtils.AUTHORITY_ONTOLOGY_URI)).thenReturn(Optional.empty());
        nalSemanticService.getAuthorityOntology();
    }

    @Test(expected = Exception.class)
    public void exceptionWhenOntologyNotExistInS3() {
        when(contentStreamService.getContentAsString(ONTO_AT, ContentType.DIRECT))
                .thenThrow(new Exception("Exception thrown for testing"));
        nalSemanticService.getAuthorityOntology();
    }
}