/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.monitoring
 *             FILE : IndexationMonitoringServiceTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 09 07, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-09-07 09:01:57 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.monitoring;

import com.google.common.collect.Iterables;
import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService;
import eu.europa.ec.opoce.cellar.cmr.indexing.IndexationMonitoringService;
import eu.europa.ec.opoce.cellar.cmr.indexing.service.IndexationMonitoringServiceImpl;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestBatch;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestsCount;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import static eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService.Priority.HIGH;
import static eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService.Priority.LOW;

/**
 * @author ARHS Developments
 */
public class IndexationMonitoringServiceTest {

    private IndexationMonitoringService indexationMonitoringService;

    @Before
    public void setup() {
        indexationMonitoringService = new IndexationMonitoringServiceImpl();
    }

    @Test
    public void registerOrUnregisterNullThrowsAnException() {
        successOnNPE(() -> indexationMonitoringService.register(null));
        successOnNPE(() -> indexationMonitoringService.unregister(null));
    }

    private static void successOnNPE(Runnable runnable) {
        try {
            runnable.run();
            Assert.fail();
        } catch (NullPointerException ignore) {
        }
    }

    @Test
    public void cmrIndexRequestCannotBeDuplicated() {
        CmrIndexRequestBatch request = newCmrIndexRequest("object-uri-1", LOW);
        CmrIndexRequestBatch request2 = newCmrIndexRequest("object-uri-1", HIGH);
        indexationMonitoringService.register(request);
        indexationMonitoringService.register(request2);

        Assert.assertEquals(1, indexationMonitoringService.getInProgressRequests(100).size());
    }

    @Test
    public void newIndexRequestWithSameUriMustReplaceExisting() {
        CmrIndexRequestBatch request = newCmrIndexRequest("object-uri-1", LOW);
        CmrIndexRequestBatch request2 = newCmrIndexRequest("object-uri-1", HIGH);
        indexationMonitoringService.register(request);
        indexationMonitoringService.register(request2);

        Assert.assertEquals(HIGH, Iterables.getFirst(indexationMonitoringService
                .getInProgressRequests(100), null)
                .getPriority());
    }

    @Test
    public void doneAtDateMustBeDoneAtUnregister() throws InterruptedException {
        CmrIndexRequestBatch request = newCmrIndexRequest("object-uri-1", LOW);
        Date createdAt = request.getCreatedOn();
        indexationMonitoringService.register(request);
        TimeUnit.SECONDS.sleep(2);
        indexationMonitoringService.unregister(request);

        CmrIndexRequestsCount first = Iterables.getFirst(indexationMonitoringService.getHistory(100), null);
        Assert.assertNotNull(first.getDoneAt());
        Assert.assertTrue(createdAt.before(first.getDoneAt()));
    }


    private static CmrIndexRequestBatch newCmrIndexRequest(String groupURI, IIndexingService.Priority priority) {
        CmrIndexRequestBatch request = new CmrIndexRequestBatch();
        request.setGroupByUri(groupURI);
        request.setPriority(priority);
        request.setCreatedOn(new Date());
        return request;
    }


}
