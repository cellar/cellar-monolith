package eu.europa.ec.opoce.cellar.server.admin.nal;

import eu.europa.ec.opoce.cellar.cmr.database.dao.OntoConfigDao;
import eu.europa.ec.opoce.cellar.ontology.dao.OntoConfig;
import org.junit.Before;
import org.junit.Test;

import java.time.Instant;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class OntoAdminConfigurationServiceTest {

    private OntoConfigDao ontoConfigDao;

    @Before
    public void setup() {
        this.ontoConfigDao = mock(OntoConfigDao.class);
    }

    @Test
    public void fallbackActivationDateAndVersionTest() {
        final OntoAdminConfigurationService adminConfService = new OntoAdminConfigurationService(ontoConfigDao);

        final OntoConfig ontoConfig1 = newOntoConfig("name1", "uri1", "version1", "2018-01-01T01:00:00.00Z", "pid1");
        final OntoConfig ontoConfig2 = newOntoConfig("name2", "uri2", null, null, "pid2");
        when(ontoConfigDao.findAll()).thenReturn(Arrays.asList(ontoConfig1, ontoConfig2));

        final List<GroupedNalOntoVersion> groupedOntologyVersions = adminConfService.getGroupedOntologyVersions()
                .stream()
                .sorted(Comparator.comparing(GroupedNalOntoVersion::getName))
                .collect(Collectors.toList());
        assertEquals(groupedOntologyVersions.size(), 2);

        final GroupedNalOntoVersion groupedNalOntoVersion1 = groupedOntologyVersions.get(0);
        final GroupedNalOntoVersion groupedNalOntoVersion2 = groupedOntologyVersions.get(1);

        assertEquals(ontoConfig1.getName(), groupedNalOntoVersion1.getName());
        assertEquals(ontoConfig1.getUri(), groupedNalOntoVersion1.getUri());
        assertEquals("20180101-020000", groupedNalOntoVersion1.getNalOntoVersion().getFormattedVersionDate());
        assertEquals(ontoConfig1.getVersionNr(), groupedNalOntoVersion1.getNalOntoVersion().getFormattedVersionNumber());

        assertEquals(ontoConfig2.getName(), groupedNalOntoVersion2.getName());
        assertEquals(ontoConfig2.getUri(), groupedNalOntoVersion2.getUri());
        assertEquals("date not set", groupedNalOntoVersion2.getNalOntoVersion().getFormattedVersionDate());
        assertEquals("version not set", groupedNalOntoVersion2.getNalOntoVersion().getFormattedVersionNumber());
    }

    private static OntoConfig newOntoConfig(final String name,
                                            final String uri,
                                            final String version,
                                            final String activationDate,
                                            final String externalPid) {
        final OntoConfig ontoConfig = new OntoConfig();
        ontoConfig.setName(name);
        ontoConfig.setUri(uri);
        ontoConfig.setVersionNr(version);
        ontoConfig.setVersionDate(activationDate == null ? null : Date.from(Instant.parse(activationDate)));
        ontoConfig.setExternalPid(externalPid);
        return ontoConfig;
    }
}