/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ontology
 *             FILE : OntologyServiceTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 01 17, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-01-17 08:16:18 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ontology;

import eu.europa.ec.opoce.cellar.cl.domain.ontology.OntologyModel;
import eu.europa.ec.opoce.cellar.ontology.exception.OntologyException;
import eu.europa.ec.opoce.cellar.ontology.service.OntologyService;
import eu.europa.ec.opoce.cellar.ontology.service.impl.OntologyServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.powermock.api.mockito.PowerMockito.doNothing;
import static org.powermock.api.support.membermodification.MemberMatcher.method;

/**
 * @author ARHS Developments
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({OntologyServiceImpl.class})
@PowerMockIgnore("javax.management.*")
public class OntologyServiceTest {

    private OntologyService ontologyService;

    @Before
    public void setup() {
        ontologyService = PowerMockito.spy(new OntologyServiceImpl(null, null, null,
                null, null, null, null, null,
                null, null));
    }


    @Test(expected = OntologyException.class)
    public void fileNameMustHaveRdfOrOwlExtension() throws Exception {
        doNothing().when(ontologyService, method(OntologyServiceImpl.class, "parseModel", Map.class, OntologyModel.class))
                .withArguments(ArgumentMatchers.any(HashMap.class), ArgumentMatchers.any(OntologyModel.class));

        List<File> valid = createTempFiles("test.owl", "test.rdf");
        List<File> invalid = createTempFiles("test.txt");
        try {
            try {
                ontologyService.update(valid,null);
            } catch (OntologyException e) {
                Assert.fail();
            }
            ontologyService.update(invalid,null);
        } finally {
            valid.forEach(File::delete);
            invalid.forEach(File::delete);
        }

    }

    private static List<File> createTempFiles(String... names) throws IOException {
        Path tmp = Files.createTempDirectory("onto-" + System.currentTimeMillis());
        List<File> files = new ArrayList<>();
        for (String name : names) {
            files.add(Files.createTempFile(tmp, "", name).toFile());
        }
        return files;
    }
}
