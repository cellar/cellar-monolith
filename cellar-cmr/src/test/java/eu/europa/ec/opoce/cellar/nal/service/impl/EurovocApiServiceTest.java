/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.nal.service.impl
 *             FILE : EurovocApiServiceTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 03 14, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-03-14 09:23:14 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.nal.service.impl;

import eu.europa.ec.opoce.cellar.cmr.database.dao.NalConfigDao;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.nal.NalOntoUtils;
import eu.europa.ec.opoce.cellar.nal.dao.NalConfig;
import eu.europa.ec.opoce.cellar.nal.service.JenaGatewayNalApi;
import eu.europa.ec.opoce.cellar.nal.service.NalConfigurationService;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;
import java.util.Optional;

import static eu.europa.ec.opoce.cellar.nal.NalOntoUtils.EUROVOC_CONCEPTSCHEME_URI;
import static eu.europa.ec.opoce.cellar.nal.NalOntoUtils.EUROVOC_ONTOLOGY_URI;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

/**
 * @author ARHS Developments
 */
public class EurovocApiServiceTest {
    private final static String EMPTY_MODELSTR = "" +
            "<?xml version=\"1.0\"?>\n" +
            "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"></rdf:RDF>";
    private JenaGatewayNalApi jenaGatewayNalApi;
    private NalConfigurationService nalConfigurationService;
    private EurovocApiService eurovocApiService;
    private NalConfigDao nalConfigDao;

    @Before
    public void setUp() throws Exception {
        jenaGatewayNalApi = mock(JenaGatewayNalApiImpl.class);
        nalConfigurationService = mock(NalConfigurationServiceImpl.class);
        nalConfigDao = mock(NalConfigDao.class);
        when(nalConfigurationService.getNal(anyString(), anyBoolean())).thenReturn(EMPTY_MODELSTR);
        when(nalConfigurationService.getAllNalModel(anyBoolean(), eq(EUROVOC_ONTOLOGY_URI)))
                .thenReturn(ModelFactory.createDefaultModel());

        NalConfig eurovoc = new NalConfig();
        eurovoc.setUri(NalOntoUtils.EUROVOC_CONCEPTSCHEME_URI);
        eurovoc.setOntologyUri(NalOntoUtils.EUROVOC_ONTOLOGY_URI);
        when(nalConfigDao.findByUri(NalOntoUtils.EUROVOC_CONCEPTSCHEME_URI)).thenReturn(Optional.of(eurovoc));
        eurovocApiService = new EurovocApiService(jenaGatewayNalApi, nalConfigurationService, nalConfigDao);
    }

    @Test(expected = CellarException.class)
    public void conceptValidationArgs() throws Exception {
        eurovocApiService.getConcept(null, null);
        eurovocApiService.getConcept(null, "");
        eurovocApiService.getConcept("", null);
        eurovocApiService.getConcept("", "");
    }

    @Test
    public void testConceptRetrieval() throws Exception {
        eurovocApiService.getConcept(NalOntoUtils.EUROVOC_CONCEPTSCHEME_URI, "eng");
        verify(jenaGatewayNalApi, times(1)).getConcept(nullable(Model.class), eq(EUROVOC_CONCEPTSCHEME_URI), eq("eng"));
    }

    @Test(expected = CellarException.class)
    public void conceptRelativesValidationArgs() throws Exception {
        eurovocApiService.getConceptRelatives(null, null, "");
        eurovocApiService.getConceptRelatives(null, "", null);
        eurovocApiService.getConceptRelatives("", null, null);
        eurovocApiService.getConceptRelatives(null, "", "");
        eurovocApiService.getConceptRelatives("", "", null);
        eurovocApiService.getConceptRelatives("", null, "");
        eurovocApiService.getConceptRelatives(null, null, null);
        eurovocApiService.getConceptRelatives("", "", "");

    }

    @Test
    public void testConceptRelativesRetrieval() throws Exception {
        eurovocApiService.getConceptRelatives(EUROVOC_CONCEPTSCHEME_URI, "relation", "eng");
        verify(jenaGatewayNalApi, times(1))
                .getRelatedConceptsOneQuery(nullable(Model.class), eq(EUROVOC_CONCEPTSCHEME_URI), anyString(), eq("eng"));
    }

    @Test
    public void blankArgsForConceptSchemeDefaultsToEurovoc() throws Exception {
        eurovocApiService.getConceptScheme(null);
        verify(jenaGatewayNalApi, times(1)).getConceptScheme(nullable(Model.class), eq(EUROVOC_CONCEPTSCHEME_URI));

        eurovocApiService.getConceptScheme("");
        verify(jenaGatewayNalApi, times(2)).getConceptScheme(nullable(Model.class), eq(EUROVOC_CONCEPTSCHEME_URI));
    }

    @Test
    public void testConceptSchemeRetrieval() throws Exception {
        eurovocApiService.getConceptScheme(EUROVOC_CONCEPTSCHEME_URI);
        verify(jenaGatewayNalApi, times(1)).getConceptScheme(nullable(Model.class), eq(EUROVOC_CONCEPTSCHEME_URI));
    }

    @Test
    public void testRetrievalConceptSchemes() throws Exception {
        eurovocApiService.getConceptSchemes(new Date());
        verify(nalConfigurationService, times(1)).getAllNalModel(eq(true), eq(EUROVOC_ONTOLOGY_URI));
        verify(jenaGatewayNalApi, times(1)).getConceptSchemesOneQuery(any(Model.class), any(Date.class));
    }

    @Test
    public void testDomainRetrieval() throws Exception {
        eurovocApiService.getDomains();
        verify(nalConfigurationService, times(1)).getAllNalModel(eq(true), eq(EUROVOC_ONTOLOGY_URI));
        verify(jenaGatewayNalApi, times(1)).getDomainsOneQuery(any(Model.class));
    }

    @Test
    public void testSupportedLanguagesRetrieval() throws Exception {
        eurovocApiService.getSupportedLanguages();
        verify(nalConfigurationService, times(1)).getAllNalModel(eq(true), eq(EUROVOC_ONTOLOGY_URI));
        verify(jenaGatewayNalApi, times(1)).getSupportedLanguages(any(Model.class), eq(EUROVOC_CONCEPTSCHEME_URI));
    }

    @Test
    public void testSupportedLanguagesForConceptSchemeRetrieval() throws Exception {
        eurovocApiService.getSupportedLanguages(EUROVOC_CONCEPTSCHEME_URI);
        verify(nalConfigurationService, times(1)).getAllNalModel(eq(true), eq(EUROVOC_ONTOLOGY_URI));
        verify(jenaGatewayNalApi, times(1)).getSupportedLanguages(any(Model.class), eq(EUROVOC_CONCEPTSCHEME_URI));
    }

    @Test(expected = CellarException.class)
    public void topConceptsValidationArgs() throws Exception {
        eurovocApiService.getTopConcepts(null, null);
        eurovocApiService.getTopConcepts(null, "");
        eurovocApiService.getTopConcepts("", null);
        eurovocApiService.getTopConcepts("", "");
    }

    @Test
    public void testTopConceptsRetrieval() throws Exception {
        eurovocApiService.getTopConcepts(EUROVOC_CONCEPTSCHEME_URI, "eng");
        verify(jenaGatewayNalApi, times(1)).getTopConcepts(nullable(Model.class), eq(EUROVOC_CONCEPTSCHEME_URI), eq("eng"));
    }
}