package eu.europa.ec.opoce.cellar.cmr.notice.core;

import eu.europa.ec.opoce.cellar.configuration.BaseUriConfiguration;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.nal.domain.RequiredLanguageBean;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.nal.service.LanguageService;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import eu.europa.ec.opoce.cellar.semantic.xml.XmlBuilder;
import org.apache.jena.rdf.model.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class Rdf2XmlServiceImplTest {

    @Mock
    private BaseUriConfiguration baseUriConfiguration;

    @Mock
    private LanguageService languageService;

    @Mock
    private Rdf2XmlLabelService rdf2XmlLabelService;

    @Mock
    private Rdf2XmlAnnotationService rdf2XmlAnnotationService;

    private Rdf2XmlServiceImpl rdf2XmlService;
    private XmlBuilder xmlBuilder;

    @Before
    public void setup() {
        rdf2XmlService = new Rdf2XmlServiceImpl(baseUriConfiguration, languageService, rdf2XmlLabelService, rdf2XmlAnnotationService);
        xmlBuilder = new XmlBuilder("", "", "div");

        when(baseUriConfiguration.getBaseUri()).thenReturn("http://publication.europa.eu/");
    }

    @Test
    public void writeUriAndSameAsTest() {
        Model model = ModelFactory.createDefaultModel();
        Resource cellarResource = model.createResource("http://publication.europa.eu/resource/cellar/my-resource");
        HashSet<Resource> sameAs = new HashSet<>();
        sameAs.add(model.createResource("http://publication.europa.eu/resource/oj/sameAs1"));
        sameAs.add(model.createResource("http://publication.europa.eu/resource/oj/sameAs2"));

        rdf2XmlService.writeUriAndSameAs(xmlBuilder, cellarResource, sameAs, model);
        assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<div>\n" +
                "   <URI>\n" +
                "      <VALUE>http://publication.europa.eu/resource/cellar/my-resource</VALUE>\n" +
                "      <TYPE>cellar</TYPE>\n" +
                "      <IDENTIFIER>my-resource</IDENTIFIER>\n" +
                "   </URI>\n" +
                "   <SAMEAS>\n" +
                "      <URI>\n" +
                "         <VALUE>http://publication.europa.eu/resource/oj/sameAs2</VALUE>\n" +
                "         <TYPE>oj</TYPE>\n" +
                "         <IDENTIFIER>sameAs2</IDENTIFIER>\n" +
                "      </URI>\n" +
                "   </SAMEAS>\n" +
                "   <SAMEAS>\n" +
                "      <URI>\n" +
                "         <VALUE>http://publication.europa.eu/resource/oj/sameAs1</VALUE>\n" +
                "         <TYPE>oj</TYPE>\n" +
                "         <IDENTIFIER>sameAs1</IDENTIFIER>\n" +
                "      </URI>\n" +
                "   </SAMEAS>\n" +
                "</div>", xmlBuilder.toString());
    }

    @Test
    public void writeEurovocUriTest() {
        rdf2XmlService.writeUri(xmlBuilder, "http://eurovoc.europa.eu/");
        assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<div>\n" +
                "   <URI>\n" +
                "      <VALUE>http://eurovoc.europa.eu/</VALUE>\n" +
                "      <TYPE>EUROVOC</TYPE>\n" +
                "      <IDENTIFIER/>\n" +
                "   </URI>\n" +
                "</div>", xmlBuilder.toString());
    }

    @Test
    public void writeAuthorityUriTest() {
        rdf2XmlService.writeUri(xmlBuilder, "http://publications.europa.eu/authority/");
        assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<div>\n" +
                "   <URI>\n" +
                "      <VALUE>http://publications.europa.eu/authority/</VALUE>\n" +
                "   </URI>\n" +
                "</div>", xmlBuilder.toString());
    }

    @Test
    public void writeCellarUriTest() {
        rdf2XmlService.writeUri(xmlBuilder, "http://publications.europa.eu/cellar/some_id");
        assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<div>\n" +
                "   <URI>\n" +
                "      <VALUE>http://publications.europa.eu/cellar/some_id</VALUE>\n" +
                "   </URI>\n" +
                "</div>", xmlBuilder.toString());
    }

    @Test
    public void writeConceptTest() {
        Model model = ModelFactory.createDefaultModel();
        Resource concept = model.createResource("http://publications.europa.eu/authority/concept");
        Map<LanguageBean, List<LanguageBean>> cache = Collections.emptyMap();
        LanguageBean frLanguageBean = mock(LanguageBean.class);
        LanguageBean enLanguageBean = mock(LanguageBean.class);
        Set<LanguageBean> languages = new HashSet<>();
        languages.add(frLanguageBean);
        languages.add(enLanguageBean);

        rdf2XmlService.writeConcept(xmlBuilder, concept, cache, languages, NoticeType.tree);

        verify(rdf2XmlLabelService).writePrefLabel(xmlBuilder, concept, frLanguageBean, cache);
        verify(rdf2XmlLabelService).writeAltLabels(xmlBuilder, concept, frLanguageBean);
        verify(rdf2XmlLabelService).writePrefLabel(xmlBuilder, concept, enLanguageBean, cache);
        verify(rdf2XmlLabelService).writeAltLabels(xmlBuilder, concept, enLanguageBean);
        assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<div type=\"concept\">\n" +
                "   <URI>\n" +
                "      <VALUE>http://publications.europa.eu/authority/concept</VALUE>\n" +
                "   </URI>\n" +
                "</div>", xmlBuilder.toString());
    }

    @Test
    public void writeEmbeddedNoticeConceptTest() {
        Model model = ModelFactory.createDefaultModel();
        Resource concept = model.createResource("http://publications.europa.eu/authority/concept");
        Map<LanguageBean, List<LanguageBean>> cache = Collections.emptyMap();

        RequiredLanguageBean frRequiredLanguageBean = mock(RequiredLanguageBean.class);
        RequiredLanguageBean enRequiredLanguageBean = mock(RequiredLanguageBean.class);
        LanguageBean frLanguageBean = mock(LanguageBean.class);
        LanguageBean enLanguageBean = mock(LanguageBean.class);
        List<RequiredLanguageBean> languages = new ArrayList<>();
        languages.add(frRequiredLanguageBean);
        languages.add(enRequiredLanguageBean);

        when(languageService.getRequiredLanguages()).thenReturn(languages);
        when(frRequiredLanguageBean.getUri()).thenReturn("fra");
        when(enRequiredLanguageBean.getUri()).thenReturn("eng");
        when(languageService.getByUri("fra")).thenReturn(frLanguageBean);
        when(languageService.getByUri("eng")).thenReturn(enLanguageBean);

        rdf2XmlService.writeConcept(xmlBuilder, concept, cache, Collections.emptySet(), NoticeType.embedded);

        verify(rdf2XmlLabelService).writePrefLabel(xmlBuilder, concept, frLanguageBean, cache);
        verify(rdf2XmlLabelService).writeAltLabels(xmlBuilder, concept, frLanguageBean);
        verify(rdf2XmlLabelService).writePrefLabel(xmlBuilder, concept, enLanguageBean, cache);
        verify(rdf2XmlLabelService).writeAltLabels(xmlBuilder, concept, enLanguageBean);
        assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<div type=\"concept\">\n" +
                "   <URI>\n" +
                "      <VALUE>http://publications.europa.eu/authority/concept</VALUE>\n" +
                "   </URI>\n" +
                "</div>", xmlBuilder.toString());
    }

    @Test
    public void writeConceptWithOpCodeTest() {
        Model model = ModelFactory.createDefaultModel();
        Resource concept = model.createResource("http://publications.europa.eu/authority/concept");
        concept.addProperty(CellarProperty.at_codeP, "op_code");

        rdf2XmlService.writeConcept(xmlBuilder, concept, Collections.emptyMap(), Collections.emptySet(), NoticeType.embedded);

        assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<div type=\"concept\">\n" +
                "   <URI>\n" +
                "      <VALUE>http://publications.europa.eu/authority/concept</VALUE>\n" +
                "   </URI>\n" +
                "   <OP-CODE>op_code</OP-CODE>\n" +
                "</div>", xmlBuilder.toString());
    }

    @Test
    public void writeConceptWithDcIdentifierTest() {
        Model model = ModelFactory.createDefaultModel();
        Resource concept = model.createResource("http://publications.europa.eu/authority/concept");
        concept.addProperty(CellarProperty.dc_identifierP, "dc_indentifier");

        rdf2XmlService.writeConcept(xmlBuilder, concept, Collections.emptyMap(), Collections.emptySet(), NoticeType.embedded);

        assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<div type=\"concept\">\n" +
                "   <URI>\n" +
                "      <VALUE>http://publications.europa.eu/authority/concept</VALUE>\n" +
                "   </URI>\n" +
                "   <IDENTIFIER>dc_indentifier</IDENTIFIER>\n" +
                "</div>", xmlBuilder.toString());
    }

    @Test
    public void writeAgent_UnitAdministrativeTest() {
        Model model = ModelFactory.createDefaultModel();
        Resource resource = model.createResource("http://publications.europa.eu/resource/cellar/id");
        resource.addProperty(CellarProperty.dc_identifierP, "dc_indentifier_1");
        resource.addProperty(CellarProperty.dc_identifierP, "dc_indentifier_2");
        LanguageBean frLanguageBean = mock(LanguageBean.class);
        LanguageBean enLanguageBean = mock(LanguageBean.class);
        Set<LanguageBean> languages = new HashSet<>();
        languages.add(frLanguageBean);
        languages.add(enLanguageBean);

        rdf2XmlService.writeAgent_UnitAdministrative(xmlBuilder, resource, NoticeType.tree, languages);

        verify(rdf2XmlLabelService).writePrefLabelIfExists(xmlBuilder, resource, frLanguageBean);
        verify(rdf2XmlLabelService).writeAltLabels(xmlBuilder, resource, frLanguageBean);
        verify(rdf2XmlLabelService).writePrefLabelIfExists(xmlBuilder, resource, enLanguageBean);
        verify(rdf2XmlLabelService).writeAltLabels(xmlBuilder, resource, enLanguageBean);
        assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<div>\n" +
                "   <IDENTIFIER>dc_indentifier_2</IDENTIFIER>\n" +
                "   <IDENTIFIER>dc_indentifier_1</IDENTIFIER>\n" +
                "</div>", xmlBuilder.toString());
    }

    @Test
    public void writeAgent_UnitAdministrativeEmbeddedTest() {
        Model model = ModelFactory.createDefaultModel();
        Resource resource = model.createResource("http://publications.europa.eu/resource/cellar/id");
        resource.addProperty(CellarProperty.dc_identifierP, "dc_indentifier_1");
        resource.addProperty(CellarProperty.dc_identifierP, "dc_indentifier_2");

        RequiredLanguageBean frRequiredLanguageBean = mock(RequiredLanguageBean.class);
        RequiredLanguageBean enRequiredLanguageBean = mock(RequiredLanguageBean.class);
        LanguageBean frLanguageBean = mock(LanguageBean.class);
        LanguageBean enLanguageBean = mock(LanguageBean.class);
        List<RequiredLanguageBean> languages = new ArrayList<>();
        languages.add(frRequiredLanguageBean);
        languages.add(enRequiredLanguageBean);

        when(languageService.getRequiredLanguages()).thenReturn(languages);
        when(frRequiredLanguageBean.getUri()).thenReturn("fra");
        when(enRequiredLanguageBean.getUri()).thenReturn("eng");
        when(languageService.getByUri("fra")).thenReturn(frLanguageBean);
        when(languageService.getByUri("eng")).thenReturn(enLanguageBean);

        rdf2XmlService.writeAgent_UnitAdministrative(xmlBuilder, resource, NoticeType.embedded, Collections.emptySet());

        verify(rdf2XmlLabelService).writePrefLabelIfExists(xmlBuilder, resource, frLanguageBean);
        verify(rdf2XmlLabelService).writeAltLabels(xmlBuilder, resource, frLanguageBean);
        verify(rdf2XmlLabelService).writePrefLabelIfExists(xmlBuilder, resource, enLanguageBean);
        verify(rdf2XmlLabelService).writeAltLabels(xmlBuilder, resource, enLanguageBean);
        assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<div>\n" +
                "   <IDENTIFIER>dc_indentifier_2</IDENTIFIER>\n" +
                "   <IDENTIFIER>dc_indentifier_1</IDENTIFIER>\n" +
                "</div>", xmlBuilder.toString());
    }

    @Test
    public void writeUnknownPasnDateTest() {
        rdf2XmlService.writeDate(xmlBuilder, "unknown-past");
        assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<div type=\"date\">\n" +
                "   <VALUE>0001-01-01</VALUE>\n" +
                "   <YEAR>0001</YEAR>\n" +
                "   <MONTH>01</MONTH>\n" +
                "   <DAY>01</DAY>\n" +
                "</div>", xmlBuilder.toString());
    }

    @Test
    public void writeUnknownFutureDateTest() {
        rdf2XmlService.writeDate(xmlBuilder, "unknown-future");
        assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<div type=\"date\">\n" +
                "   <VALUE>9999-12-31</VALUE>\n" +
                "   <YEAR>9999</YEAR>\n" +
                "   <MONTH>12</MONTH>\n" +
                "   <DAY>31</DAY>\n" +
                "</div>", xmlBuilder.toString());
    }

    @Test
    public void writeValidDateTest() {
        rdf2XmlService.writeDate(xmlBuilder, "2019-06-28");
        assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<div type=\"date\">\n" +
                "   <VALUE>2019-06-28</VALUE>\n" +
                "   <YEAR>2019</YEAR>\n" +
                "   <MONTH>06</MONTH>\n" +
                "   <DAY>28</DAY>\n" +
                "</div>", xmlBuilder.toString());
    }

    @Test(expected = CellarException.class)
    public void writeInvalidDateTest() {
        rdf2XmlService.writeDate(xmlBuilder, "invalid date");
    }

    @Test
    public void writeAnnotationTest() {
        Property propery = mock(Property.class);
        RDFNode node = mock(RDFNode.class);
        Model model = mock(Model.class);
        Set<Resource> resources = Collections.emptySet();

        rdf2XmlService.writeAnnotation(xmlBuilder, propery, node, model, resources);

        verify(rdf2XmlAnnotationService).addAnnotations(xmlBuilder, propery, node, model, resources);
    }
}