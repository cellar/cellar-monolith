package eu.europa.ec.opoce.cellar.cmr.database.dao.spring;

import com.google.common.base.Charsets;
import eu.europa.ec.opoce.cellar.cmr.CmrInverseDissemination;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CmrInverseDisseminationDao;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.sql.Clob;
import java.sql.ResultSet;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SpringCmrInverseDisseminationDaoTest {

    private static final String INVERSE_MODEL = read("/cmr_inverse_diss/inverse.rdf");
    private static final String SAME_AS_MODEL = read("/cmr_inverse_diss/same_as.rdf");

    private SpringCmrInverseDisseminationDao cmrInverseDisseminationDao = new SpringCmrInverseDisseminationDao();

    @Test
    public void readClobAsStringWhenCreating() throws Exception {
        ResultSet resultSetMock = mock(ResultSet.class);
        Clob inverseClobMock = mock(Clob.class);
        Clob sameAsClobMock = mock(Clob.class);

        when(inverseClobMock.length()).thenReturn((long) INVERSE_MODEL.length());
        when(inverseClobMock.getSubString(1L, INVERSE_MODEL.length())).thenReturn(INVERSE_MODEL);
        when(sameAsClobMock.length()).thenReturn((long) SAME_AS_MODEL.length());
        when(sameAsClobMock.getSubString(1L, SAME_AS_MODEL.length())).thenReturn(SAME_AS_MODEL);
        when(resultSetMock.getString(CmrInverseDisseminationDao.Column.CONTEXT)).thenReturn("context");
        when(resultSetMock.getClob(CmrInverseDisseminationDao.Column.INVERSE_MODEL)).thenReturn(inverseClobMock);
        when(resultSetMock.getClob(CmrInverseDisseminationDao.Column.SAMEAS_MODEL)).thenReturn(sameAsClobMock);
        when(resultSetMock.getByte(CmrInverseDisseminationDao.Column.IS_BACKLOG)).thenReturn((byte) 1);

        CmrInverseDissemination cmrInverseDissemination = cmrInverseDisseminationDao.create(resultSetMock);

        assertEquals("context", cmrInverseDissemination.getContext());
        assertEquals(6, cmrInverseDissemination.getInverseModel().size());
        assertEquals(2, cmrInverseDissemination.getSameAsModel().size());
        assertTrue(cmrInverseDissemination.isBacklog());
    }

    @Test
    public void skipNullColumnWhenCreating() throws Exception {
        ResultSet resultSetMock = mock(ResultSet.class);
        when(resultSetMock.getString(CmrInverseDisseminationDao.Column.CONTEXT)).thenReturn("context");
        when(resultSetMock.getByte(CmrInverseDisseminationDao.Column.IS_BACKLOG)).thenReturn((byte) 1);

        CmrInverseDissemination cmrInverseDissemination = cmrInverseDisseminationDao.create(resultSetMock);

        assertEquals("context", cmrInverseDissemination.getContext());
        assertNull(cmrInverseDissemination.getInverseModel());
        assertNull(cmrInverseDissemination.getSameAsModel());
        assertTrue(cmrInverseDissemination.isBacklog());
    }

    private static String read(String path) {
        try (InputStream is = SpringCmrInverseDisseminationDaoTest.class.getResourceAsStream(path)) {
            return IOUtils.toString(is, Charsets.UTF_8);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }
}