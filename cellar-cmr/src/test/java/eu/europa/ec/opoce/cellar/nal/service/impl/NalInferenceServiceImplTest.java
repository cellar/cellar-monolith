/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.nal.service.impl
 *             FILE : NalInferenceServiceImplTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 03 14, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-03-14 06:26:15 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.nal.service.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.impl.CellarStaticConfiguration;
import eu.europa.ec.opoce.cellar.nal.service.LanguageService;
import eu.europa.ec.opoce.cellar.nal.service.NalInferenceService;
import eu.europa.ec.opoce.cellar.nal.service.NalSemanticService;
import eu.europa.ec.opoce.cellar.nal.service.NalSparqlService;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

/**
 * @author ARHS Developments
 */
public class NalInferenceServiceImplTest {
    private LanguageService languageService;
    private NalSparqlService nalSparqlService;
    private NalSemanticService nalSemanticService;
    private ICellarConfiguration cellarConfiguration;
    private NalInferenceService nalInferenceService;
    private static final String AUTHORITY_NAL_CONCEPTSCHEME_URI = "http://publications.europa.eu/resource/authority/label-type";
    private static final String EUROVOC_CONCEPTSCHEME_URI = "http://eurovoc.europa.eu/100141";

    @Before
    public void setUp() throws Exception {
        languageService = mock(LanguageServiceImpl.class);
        nalSparqlService = mock(NalSparqlServiceImpl.class);
        nalSemanticService = mock(NalSemanticServiceImpl.class);
        when(nalSemanticService.getAuthorityOntology()).thenReturn(ModelFactory.createDefaultModel());
        when(nalSemanticService.getEurovocOntology()).thenReturn(ModelFactory.createDefaultModel());
        cellarConfiguration = mock(CellarStaticConfiguration.class);
        nalInferenceService = new NalInferenceServiceImpl(languageService, nalSparqlService, nalSemanticService, cellarConfiguration);
    }

    @Test(expected = IllegalArgumentException.class)
    public void validateInputModelArgument() throws Exception {
        nalInferenceService.infer(null, AUTHORITY_NAL_CONCEPTSCHEME_URI);
    }

    @Test(expected = IllegalArgumentException.class)
    public void validateUriArgumentNotNull() throws Exception {
        nalInferenceService.infer(ModelFactory.createDefaultModel(), null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void validateUriArgumentNotEmpty() throws Exception {
        nalInferenceService.infer(ModelFactory.createDefaultModel(), "");
    }

    @Test
    public void authorityOntologyIsAddedToModelForAuthorityNal() throws Exception {
        Model inputModel = ModelFactory.createDefaultModel();

        verify(nalSemanticService, times(0)).getAuthorityOntology();
        verify(nalSemanticService, times(0)).getEurovocOntology();

        nalInferenceService.infer(inputModel, AUTHORITY_NAL_CONCEPTSCHEME_URI);
        verify(nalSemanticService, times(1)).getAuthorityOntology();
        verify(nalSemanticService, times(0)).getEurovocOntology();

        nalInferenceService.infer(inputModel, EUROVOC_CONCEPTSCHEME_URI);
        verify(nalSemanticService, times(1)).getAuthorityOntology();
        verify(nalSemanticService, times(1)).getEurovocOntology();
    }

    @Test
    public void conceptsAreRetrievedForBothEurovocAndNal() throws Exception {
        Model inputModel = ModelFactory.createDefaultModel();
        verify(nalSparqlService, times(0)).getConceptsFor(any(Model.class),
                any(String.class));

        nalInferenceService.infer(inputModel, AUTHORITY_NAL_CONCEPTSCHEME_URI);
        verify(nalSparqlService, times(1)).getConceptsFor(any(Model.class),
                eq(AUTHORITY_NAL_CONCEPTSCHEME_URI));

        nalInferenceService.infer(inputModel, EUROVOC_CONCEPTSCHEME_URI);
        verify(nalSparqlService, times(1)).getConceptsFor(any(Model.class),
                eq(EUROVOC_CONCEPTSCHEME_URI));
    }

    @Test
    public void nalFallbackLanguageAreCalculatedOne() throws Exception {
        Model inputModel = ModelFactory.createDefaultModel();
        verify(languageService, times(0)).getRequiredLanguages();

        nalInferenceService.infer(inputModel, AUTHORITY_NAL_CONCEPTSCHEME_URI);
        verify(nalSparqlService, times(1)).getConceptsFor(any(Model.class), eq(AUTHORITY_NAL_CONCEPTSCHEME_URI));
        verify(languageService, times(1)).getRequiredLanguages();
    }

    @Test
    public void nalFallbackAreCalculatedForConceptsAndMicrothesauriAndDomains() throws Exception {
        Model inputModel = ModelFactory.createDefaultModel();
        verify(languageService, times(0)).getRequiredLanguages();

        nalInferenceService.infer(inputModel, EUROVOC_CONCEPTSCHEME_URI);
        verify(nalSparqlService, times(1)).getConceptsFor(any(Model.class), eq(EUROVOC_CONCEPTSCHEME_URI));
        verify(nalSparqlService, times(1)).getEurovocMicrothesauri(any(Model.class));
        verify(nalSparqlService, times(1)).getEurovocDomains(any(Model.class));
        verify(languageService, times(3)).getRequiredLanguages();
    }

    @Test
    public void inferenceIsLimitedByAConfigurableMaxIterationNumber() throws Exception {
        Model inputModel = ModelFactory.createDefaultModel();
        when(cellarConfiguration.getCellarServiceNalLoadInferenceMaxIteration()).thenReturn(10);
        when(nalSparqlService.getSkosInferenceSparqlList()).
                thenReturn(Collections.singletonList(QueryFactory.create("" +
                        "    PREFIX skos: <http://www.w3.org/2004/02/skos/core#>\n" +
                        "    CONSTRUCT {\n" +
                        "        ?s skos:inScheme ?o.\n" +
                        "    } WHERE {\n" +
                        "        ?s skos:topConceptOf ?o.\n" +
                        "    }" +
                        "")));

        nalInferenceService.infer(inputModel, AUTHORITY_NAL_CONCEPTSCHEME_URI);
        verify(cellarConfiguration, times(1)).getCellarServiceNalLoadInferenceMaxIteration();
        verify(nalSparqlService, times(1)).getSkosInferenceSparqlList();
    }
}