package eu.europa.ec.opoce.cellar.cmr.notice.core;

import com.google.common.collect.Sets;
import eu.europa.ec.opoce.cellar.semantic.xml.XmlBuilder;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.vocabulary.OWL;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class Rdf2XmlAnnotationServiceImplTest {

    private static final String EXPECTED_ANNOTATION =
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<NOTICE>\n" +
            "   <ANNOTATION>\n" +
            "      <ANNOTATIONCHANGED_TO_IDENTIFIER>SOME_VALUE</ANNOTATIONCHANGED_TO_IDENTIFIER>\n" +
            "   </ANNOTATION>\n" +
            "</NOTICE>";

    private Rdf2XmlAnnotationService annotationService;
    private XmlBuilder builder;

    @Before
    public void setup() {
        annotationService = new Rdf2XmlAnnotationServiceImpl();
        builder = new XmlBuilder("NOTICE");
    }

    @Test
    public void addAnnotationOnPidTest() throws Exception {
        final Model model = readResourceAsModel("annotationPidSameAs.rdf");
        final Resource celexResource = model.createResource("http://localhost/celex/00000D0000");
        final Resource jolResource = model.createResource("http://localhost/oj/JOL_2012_000_R_0000");

        annotationService.addAnnotations(builder, OWL.sameAs, celexResource, model, Sets.newHashSet(jolResource));
        assertEquals(EXPECTED_ANNOTATION, builder.toString());
    }

    @Test
    public void addAnnotationOnCellarIdTest() throws Exception {
        final Model model = readResourceAsModel("annotationCellarIdSameAs.rdf");
        final Resource celexResource = model.createResource("http://localhost/celex/00000D0000");
        final Resource cellarResource = model.createResource("http://localhost/cellar/3e18f5e6-3118-11e9-892d-01aa75ed71a1");

        annotationService.addAnnotations(builder, OWL.sameAs, celexResource, model, Sets.newHashSet(cellarResource));
        assertEquals(EXPECTED_ANNOTATION, builder.toString());
    }

    @Test
    public void addSameAsAnnotationsOnPidTest() throws Exception {
        final Model model = readResourceAsModel("annotationPidSameAs.rdf");
        final Resource celexResource = model.createResource("http://localhost/celex/00000D0000");

        annotationService.addSameAsAnnotations(builder, celexResource, model);
        assertEquals(EXPECTED_ANNOTATION, builder.toString());
    }

    @Test
    public void addSameAsAnnotationsOnCellarIdTest() throws Exception {
        final Model model = readResourceAsModel("annotationCellarIdSameAs.rdf");
        final Resource celexResource = model.createResource("http://localhost/celex/00000D0000");

        annotationService.addSameAsAnnotations(builder, celexResource, model);
        assertEquals(EXPECTED_ANNOTATION, builder.toString());
    }

    private static Model readResourceAsModel(final String file) throws IOException {
        final ClassPathResource resource = new ClassPathResource(file);
        return ModelFactory.createDefaultModel().read(resource.getInputStream(), "", Lang.RDFXML.getName());
    }
}