package eu.europa.ec.opoce.cellar.cmr.notice.core;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.common.Namespace;
import eu.europa.ec.opoce.cellar.semantic.ontology.data.FallbackOntologyData;
import eu.europa.ec.opoce.cellar.semantic.ontology.data.OntologyData;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.SortedSet;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newTreeSet;

public class ShouldHandlePropertyVisitorTest {

    private ShouldHandlePropertyVisitor visitor;
    private OntologyData ontologyData;
    private FallbackOntologyData fallbackOntologyData;


    private static final SortedSet<String> EMPTY_ONTO_URIS = newTreeSet();
    private static final SortedSet<String> ONTO_URIS = newTreeSet(newArrayList(
            Namespace.cdm + "fake_cdm",
            Namespace.import_onto + "fake_import",
            Namespace.indexation + "fake_indexation",
            Namespace.deriv + "fake_derived_class",
            Namespace.datatype + "fake_datatype",
            Namespace.annotation + "fake_annotation",
            Namespace.dorie + "fake_dorie",
            Namespace.cmr + "fake"
    ));

    private static final SortedSet<String> EMPTY_FALLBACK_ONTO_URIS = newTreeSet();
    private static final SortedSet<String> FALLBACK_ONTO_URIS = newTreeSet(newArrayList(
            Namespace.cdm + "fallback_fake_cdm",
            Namespace.import_onto + "fallback_fake_import",
            Namespace.indexation + "fallback_fake_indexation",
            Namespace.deriv + "fallback_fake_derived_class",
            Namespace.datatype + "fallback_fake_datatype",
            Namespace.annotation + "fallback_fake_annotation",
            Namespace.cmr + "fallback_fake"
    ));

    @Before
    public void setup() {
        Rdf2Xml rdf2Xml = Mockito.mock(Rdf2Xml.class);
        ontologyData = Mockito.mock(OntologyData.class);
        fallbackOntologyData = Mockito.mock(FallbackOntologyData.class);
        Mockito.when(rdf2Xml.isFilter()).thenReturn(true);
        visitor = new ShouldHandlePropertyVisitor(rdf2Xml, ontologyData, fallbackOntologyData);
        ICellarConfiguration cfg = Mockito.mock(ICellarConfiguration.class);
        Mockito.when(cfg.isCellarServiceDisseminationInNoticePropertiesOnlyEnabled()).thenReturn(true);
        visitor.setCellarConfiguration(cfg);
    }

    @Test
    public void isInNoticeMustReturnTrueIfCellarServiceDisseminationInNoticePropertiesOnlyIsDisabled() {
        ShouldHandlePropertyVisitor v = new ShouldHandlePropertyVisitor(null, null, null);
        ICellarConfiguration cfg = Mockito.mock(ICellarConfiguration.class);
        Mockito.when(cfg.isCellarServiceDisseminationInNoticePropertiesOnlyEnabled()).thenReturn(false);
        v.setCellarConfiguration(cfg);
        Assert.assertTrue(v.isInNotice());
    }

    @Test
    public void isInNoticeMustReturnTrueIfRdfFilterIsDisabled() {
        Rdf2Xml rdf2Xml = Mockito.mock(Rdf2Xml.class);
        Mockito.when(rdf2Xml.isFilter()).thenReturn(false);
        ShouldHandlePropertyVisitor v = new ShouldHandlePropertyVisitor(rdf2Xml, null, null);
        ICellarConfiguration cfg = Mockito.mock(ICellarConfiguration.class);
        Mockito.when(cfg.isCellarServiceDisseminationInNoticePropertiesOnlyEnabled()).thenReturn(true);
        v.setCellarConfiguration(cfg);
        Assert.assertTrue(v.isInNotice());
    }

    @Test
    public void isInNoticeMustReturnTrueIfPropertyIsInOntologyData() {
        Mockito.when(ontologyData.getInNoticePropertyUris()).thenReturn(ONTO_URIS);
        visitor.setCurrentPropertyUri(Namespace.cdm + "fake_cdm");
        Assert.assertTrue(visitor.isInNotice());
        visitor.setCurrentPropertyUri(Namespace.import_onto + "fake_import");
        Assert.assertTrue(visitor.isInNotice());
        visitor.setCurrentPropertyUri(Namespace.indexation + "fake_indexation");
        Assert.assertTrue(visitor.isInNotice());
        visitor.setCurrentPropertyUri(Namespace.deriv + "fake_derived_class");
        Assert.assertTrue(visitor.isInNotice());
        visitor.setCurrentPropertyUri(Namespace.datatype + "fake_datatype");
        Assert.assertTrue(visitor.isInNotice());
        visitor.setCurrentPropertyUri(Namespace.annotation + "fake_annotation");
        Assert.assertTrue(visitor.isInNotice());
        visitor.setCurrentPropertyUri(Namespace.dorie + "fake_dorie");
        Assert.assertTrue(visitor.isInNotice());
        visitor.setCurrentPropertyUri(Namespace.cmr + "fake");
        Assert.assertTrue(visitor.isInNotice());
        visitor.setCurrentPropertyUri(Namespace.cmr + "unknown_property");
        Assert.assertFalse(visitor.isInNotice());
    }

    @Test
    public void isInNoticeMustReturnTrueIfPropertyIsNotInOntologyDataButInFallbackOntologyData() {
        Mockito.when(fallbackOntologyData.getInNoticePropertyUris()).thenReturn(FALLBACK_ONTO_URIS);
        visitor.setCurrentPropertyUri(Namespace.cdm + "fallback_fake_cdm");
        Assert.assertTrue(visitor.isInNotice());
        visitor.setCurrentPropertyUri(Namespace.import_onto + "fallback_fake_import");
        Assert.assertTrue(visitor.isInNotice());
        visitor.setCurrentPropertyUri(Namespace.indexation + "fallback_fake_indexation");
        Assert.assertTrue(visitor.isInNotice());
        visitor.setCurrentPropertyUri(Namespace.deriv + "fallback_fake_derived_class");
        Assert.assertTrue(visitor.isInNotice());
        visitor.setCurrentPropertyUri(Namespace.datatype + "fallback_fake_datatype");
        Assert.assertTrue(visitor.isInNotice());
        visitor.setCurrentPropertyUri(Namespace.annotation + "fallback_fake_annotation");
        Assert.assertTrue(visitor.isInNotice());
        visitor.setCurrentPropertyUri(Namespace.cmr + "fallback_fake");
        Assert.assertTrue(visitor.isInNotice());
        visitor.setCurrentPropertyUri(Namespace.cmr + "unknown_fallback_fake");
        Assert.assertFalse(visitor.isInNotice());
    }

    @Test
    public void isInNoticeMustReturnTrueIfThePropertyDoNotStartsAsExpected() {
        visitor.setCurrentPropertyUri(Namespace.owl + "owl_property");
        Assert.assertTrue(visitor.isInNotice());
    }

    @Test
    public void isInNoticeMustReturnFalseIfOntologyDataOrFallbackOntologyDataIsEmpty() {
        Mockito.when(ontologyData.getInNoticePropertyUris()).thenReturn(EMPTY_ONTO_URIS);
        visitor.setCurrentPropertyUri(Namespace.import_onto + "fake_import");
        Assert.assertFalse(visitor.isInNotice());

        Mockito.when(fallbackOntologyData.getInNoticePropertyUris()).thenReturn(EMPTY_FALLBACK_ONTO_URIS);
        visitor.setCurrentPropertyUri(Namespace.import_onto + "fallback_fake_import");
        Assert.assertFalse(visitor.isInNotice());
    }
}