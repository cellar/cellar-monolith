/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.nal.service.impl
 *             FILE : NalSnippetCreatorServiceImplTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 03 14, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-03-14 05:58:00 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.nal.service.impl;

import eu.europa.ec.opoce.cellar.nal.service.NalSnippetDbGateway;
import eu.europa.ec.opoce.cellar.nal.service.NalSparqlService;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * @author ARHS Developments
 */
public class NalSnippetCreatorServiceImplTest {
    private static final String AUTHORITY_NAL_CONCEPTSCHEME_URI = "http://publications.europa.eu/resource/authority/label-type";
    private static final String EUROVOC_CONCEPTSCHEME_URI = "http://eurovoc.europa.eu/100141";
    private NalSparqlService nalSparqlService;
    private NalSnippetDbGateway nalSnippetDbGateway;
    private NalSnippetCreatorServiceImpl nalSnippetCreatorService;

    @Before
    public void setUp() throws Exception {
        nalSparqlService = mock(NalSparqlServiceImpl.class);
        nalSnippetDbGateway = mock(NalSnippetDbGatewayImpl.class);
        nalSnippetCreatorService = new NalSnippetCreatorServiceImpl(nalSnippetDbGateway, nalSparqlService);
    }

    @Test
    public void snippetsAreDeletedFromRepositoryBeforeCreatingNewOnes() throws Exception {
        nalSnippetCreatorService.generate(ModelFactory.createDefaultModel(), AUTHORITY_NAL_CONCEPTSCHEME_URI);
        verify(nalSnippetDbGateway, times(1)).deleteRelatedSnippetOfRelatedResourceUri(AUTHORITY_NAL_CONCEPTSCHEME_URI);
        verify(nalSnippetDbGateway, times(1)).deleteNalSnippetOfConceptScheme(AUTHORITY_NAL_CONCEPTSCHEME_URI);

        nalSnippetCreatorService.generate(ModelFactory.createDefaultModel(), EUROVOC_CONCEPTSCHEME_URI);
        verify(nalSnippetDbGateway, times(1)).deleteRelatedSnippetOfRelatedResourceUri(EUROVOC_CONCEPTSCHEME_URI);
        verify(nalSnippetDbGateway, times(1)).deleteNalSnippetOfConceptScheme(EUROVOC_CONCEPTSCHEME_URI);
    }

    @Test
    public void conceptsAreRetrievedForAllKindOfNal() throws Exception {
        Model inputModel = ModelFactory.createDefaultModel();
        nalSnippetCreatorService.generate(inputModel, AUTHORITY_NAL_CONCEPTSCHEME_URI);
        verify(nalSparqlService, times(1)).getConceptsFor(inputModel, AUTHORITY_NAL_CONCEPTSCHEME_URI);

        nalSnippetCreatorService.generate(inputModel, EUROVOC_CONCEPTSCHEME_URI);
        verify(nalSparqlService, times(1)).getConceptsFor(inputModel, EUROVOC_CONCEPTSCHEME_URI);
    }

    @Test
    public void eurovocDomainAndMicroThesauriAreOnlyCalculatedForEurovoc() throws Exception {
        Model inputModel = ModelFactory.createDefaultModel();
        nalSnippetCreatorService.generate(inputModel, AUTHORITY_NAL_CONCEPTSCHEME_URI);
        verify(nalSparqlService, times(0)).getEurovocDomains(inputModel);
        verify(nalSparqlService, times(0)).getEurovocMicrothesauri(inputModel);

        nalSnippetCreatorService.generate(inputModel, EUROVOC_CONCEPTSCHEME_URI);
        verify(nalSparqlService, times(1)).getEurovocDomains(inputModel);
        verify(nalSparqlService, times(1)).getEurovocMicrothesauri(inputModel);
    }

    @Test
    public void relatedSnippetsAreBuiltDifferentlyForEurovocAndNalAuthority() throws Exception {
        Model inputModel = ModelFactory.createDefaultModel();
        nalSnippetCreatorService.generate(inputModel, AUTHORITY_NAL_CONCEPTSCHEME_URI);
        verify(nalSparqlService, times(0)).getFacets(inputModel);
        verify(nalSparqlService, times(1)).getBroaderTransitives(inputModel, AUTHORITY_NAL_CONCEPTSCHEME_URI);

        nalSnippetCreatorService.generate(inputModel, EUROVOC_CONCEPTSCHEME_URI);
        verify(nalSparqlService, times(1)).getFacets(inputModel);
        verify(nalSparqlService, times(0)).getBroaderTransitives(inputModel, EUROVOC_CONCEPTSCHEME_URI);
    }

    @Test
    public void nalAuthorityInsertRelations() throws Exception {
        Model inputModel = ModelFactory.createDefaultModel();
        nalSnippetCreatorService.generate(inputModel, AUTHORITY_NAL_CONCEPTSCHEME_URI);
        verify(nalSnippetDbGateway, times(1)).insertRelations(any());
    }

    @Test
    public void eurovocInsertRelations() throws Exception {
        Model inputModel = ModelFactory.createDefaultModel();
        nalSnippetCreatorService.generate(inputModel, EUROVOC_CONCEPTSCHEME_URI);
        verify(nalSnippetDbGateway, times(1)).insertRelations(any());
    }
}