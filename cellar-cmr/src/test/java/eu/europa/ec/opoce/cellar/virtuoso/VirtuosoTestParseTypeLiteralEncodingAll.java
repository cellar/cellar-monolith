package eu.europa.ec.opoce.cellar.virtuoso;

import org.apache.commons.lang.CharEncoding;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.RDFReader;
import org.springframework.core.io.FileSystemResource;
import virtuoso.jdbc4.VirtuosoDataSource;
import virtuoso.jena.driver.VirtDataset;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

public class VirtuosoTestParseTypeLiteralEncodingAll {

    private static VirtuosoDataSource virtuosoDataSource;

    public static void main(String[] args) throws Exception {
        final String sample1Path = "C:\\projects\\CELLAR\\Trunk\\cellar-app\\cellar-cmr\\src\\test\\resources\\VirtuosoTestParseTypeLiteralEncoding_ok.rdf";
        final String sample2Path = "C:\\projects\\CELLAR\\Trunk\\cellar-app\\cellar-cmr\\src\\test\\resources\\VirtuosoTestParseTypeLiteralEncoding_ko.rdf";
        final String sample3Path = "C:\\projects\\CELLAR\\Trunk\\cellar-app\\cellar-cmr\\src\\test\\resources\\VirtuosoTestParseTypeLiteralEncodingArrayIndexProblem_ok.rdf";
        final String sample4Path = "C:\\projects\\CELLAR\\Trunk\\cellar-app\\cellar-cmr\\src\\test\\resources\\VirtuosoTestParseTypeLiteralEncodingArrayIndexProblem_ko.rdf";

        // sample1
        String graphName = "http://cellar-dev.publications.europa.eu/resource/cellar/" + UUID.randomUUID();
        System.out.println("==============================");
        File sample1File = new File(sample1Path);
        final Model sample1In = read(sample1File);
        writeModel(graphName, sample1In);
        final Model sample1Out = readModel(graphName);
        System.out.println("Sample 1 is ok!");
        sample1Out.write(System.out);
        closeQuietly(sample1In, sample1Out);
        System.out.println("==============================");

        // sample2
        graphName = "http://cellar-dev.publications.europa.eu/resource/cellar/" + UUID.randomUUID();
        System.out.println("==============================");
        File sample2File = new File(sample2Path);
        final Model sample2In = read(sample2File);
        writeModel(graphName, sample2In);
        final Model sample2Out = readModel(graphName);
        System.out.println("Sample 2 has encoding problems!");
        sample2Out.write(System.out);
        closeQuietly(sample2In, sample2Out);
        System.out.println("==============================");

        // sample3
        graphName = "http://cellar-dev.publications.europa.eu/resource/cellar/" + UUID.randomUUID();
        System.out.println("==============================");
        File sample3File = new File(sample3Path);
        final Model sample3In = read(sample3File);
        writeModel(graphName, sample3In);
        Model sample3Out = null;
        try {
            sample3Out = readModel(graphName);
            System.out.println("Sample 3 is ok!");
            sample3Out.write(System.out);
        } catch (Exception e) {
            System.out.println("Sample 3 has triggered the following exception!");
            e.printStackTrace();
        }
        closeQuietly(sample3In, sample3Out);
        System.out.println("==============================");

        // sample4
        graphName = "http://cellar-dev.publications.europa.eu/resource/cellar/" + UUID.randomUUID();
        System.out.println("==============================");
        File sample4File = new File(sample4Path);
        final Model sample4In = read(sample4File);
        writeModel(graphName, sample4In);
        Model sample4Out = null;
        try {
            sample4Out = readModel(graphName);
            System.out.println("Sample 4 is ok!");
            sample4Out.write(System.out);
        } catch (Exception e) {
            System.out.println("Sample 4 has triggered the following exception!");
            e.printStackTrace();
        }
        closeQuietly(sample4In, sample4Out);
        System.out.println("==============================");
    }

    private static VirtuosoDataSource getVirtuosoDataSource() throws Exception {
        if (virtuosoDataSource != null) {
            return virtuosoDataSource;
        }

        final String serverName = "10.2.0.244";
        final int serverPort = 1114;
        virtuosoDataSource = new VirtuosoDataSource();
        virtuosoDataSource.setUser("dba");
        virtuosoDataSource.setPassword("dba");
        virtuosoDataSource.setDataSourceName("jdbc:virtuoso://" + serverName + ":" + serverPort + "/charset=UTF-8");
        virtuosoDataSource.setServerName(serverName);
        virtuosoDataSource.setPortNumber(serverPort);
        virtuosoDataSource.setCharset(CharEncoding.UTF_8);
        return virtuosoDataSource;
    }

    private static void writeModel(final String graphName, final Model newModel) throws Exception {
        VirtDataset dataset = null;
        try {
            // gets the connection
            dataset = new VirtDataset(getVirtuosoDataSource());

            // get direct model of digital objects that are supposed to be inserted
            Model existingModel = null;
            try {
                // load existing model
                existingModel = dataset.getNamedModel(graphName);

                // update new model
                existingModel.removeAll();
                existingModel.add(newModel);
            } finally {
                closeQuietly(existingModel, newModel);
            }
        } finally {
            if (dataset != null) {
                dataset.close();
            }
        }
    }

    private static Model readModel(final String graphName) throws Exception {
        final Model retModel = ModelFactory.createDefaultModel();

        VirtDataset dataset = null;
        Model existingModel = null;
        try {
            dataset = new VirtDataset(getVirtuosoDataSource());
            existingModel = dataset.getNamedModel(graphName);
            retModel.add(existingModel);
        } finally {
            closeQuietly(existingModel);
            if (dataset != null) {
                dataset.close();
            }
        }

        return retModel;
    }

    private static void closeQuietly(final Model... models) {
        for (final Model model : models) {
            if (model != null) {
                model.close();
            }
        }
    }

    private static Model read(final File file) throws IOException {
        final Collection<org.springframework.core.io.Resource> resources = new ArrayList<org.springframework.core.io.Resource>();
        resources.add(new FileSystemResource(file));

        final Model model = ModelFactory.createDefaultModel();

        for (org.springframework.core.io.Resource resource : resources) {
            InputStream inputstream = null;
            try {
                inputstream = resource.getInputStream();
                final RDFReader rdfReader = getBasicReader(model, resource);
                rdfReader.read(model, inputstream, null);
            } catch (RuntimeException e) {
                closeQuietly(model);
                throw e;
            } finally {
                if (inputstream != null) {
                    inputstream.close();
                }
            }
        }

        return model;
    }

    private static RDFReader getBasicReader(final Model model, final org.springframework.core.io.Resource resource) {
        try {
            final String filename = resource.getFilename();
            if (filename == null) {
                throw new IllegalStateException("resource loaded from byte array does not have a filename");
            }
            final String extension = StringUtils.substringAfterLast(filename, ".").toLowerCase();

            if ("owl".equals(extension)) {
                return model.getReader();
            } else if ("nt".equals(extension)) {
                return model.getReader("N-TRIPLE");
            } else if ("n3".equals(extension) || "ttl".equals(extension)) {
                return model.getReader("TURTLE");
            }

            return model.getReader();
        } catch (IllegalStateException e) {
            return model.getReader();
        }
    }

}
