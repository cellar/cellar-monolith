package eu.europa.ec.opoce.cellar;

import eu.europa.ec.opoce.cellar.bootstrap.CellarContextLoaderListener;
import eu.europa.ec.opoce.cellar.cmr.model.ModelUtils;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import org.apache.jena.rdf.model.Model;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class JenaRuntimeTest {

    private static final String RAW = "<rdf:RDF" +
            "    xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"" +
            "    xmlns:j.0=\"http://publications.europa.eu/ontology/cdm#\"" +
            "    xmlns:owl=\"http://www.w3.org/2002/07/owl#\"" +
            "    xmlns:rdfs=\"http://www.w3.org/2000/01/rdf-schema#\"" +
            "    xmlns:j.1=\"http://publications.europa.eu/ontology/cdm/cmr#\" > " +
            "  <rdf:Description rdf:about=\"http://cellar-dev.publications.europa.eu/resource/oj/JOL_2006_088_R_0063_01\">" +
            "    <j.0:resource_legal_comment_internal rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">MAN2</j.0:resource_legal_comment_internal>" +
            "  </rdf:Description>" +
            "</rdf:RDF>";

    @BeforeClass
    public static void init() {
        CellarContextLoaderListener.setJenaRuntime();
    }

    @Test
    public void jenaRuntimeMustBeRDF10() {
        Model m = ModelUtils.read(RAW);
        Assert.assertTrue(JenaUtils.toString(m).contains("rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\""));
    }
}
