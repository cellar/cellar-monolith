package eu.europa.ec.opoce.cellar.virtuoso;

import org.apache.commons.lang.CharEncoding;
import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import virtuoso.jdbc4.VirtuosoDataSource;
import virtuoso.jena.driver.VirtDataset;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

public class VirtuosoTestBackup {

    private static final String CELLAR_URI = "http://cellar-dev.publications.europa.eu/resource/cellar/518ccd49-00ae-4e97-8b6d-41330761c1ba.0001";

    private static final String MODEL_STR = "" + // 
            "<rdf:RDF " + //
            "xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" " + //
            "xmlns:j.0=\"http://publications.europa.eu/ontology/cdm#\" " + //
            "xmlns:owl=\"http://www.w3.org/2002/07/owl#\" >  " + //
            "<rdf:Description rdf:about=\"http://cellar-dev.publications.europa.eu/resource/uriserv/n26102.DEU\"> " + //
            "<rdf:type rdf:resource=\"http://publications.europa.eu/ontology/cdm#expression\"/> " + //
            "<j.0:expression_title rdf:datatype=\"http://www.w3.org/2001/XMLSchema#string\">Wiederverwendbarkeit, Recyclingfähigkeit und Verwertbarkeit von Kraftfahrzeugen</j.0:expression_title> "
            + //
            "<j.0:expression_abstract rdf:parseType=\"Literal\"> " + //
            "<wrxml>EUROPA - Zusammenfassungen der EU-Gesetzgebung - Die Hersteller von Kraftfahrzeugen müssen nun bei Neufahrzeugen Mindestanteile für die Wiederverwendung, Recyclingfähigkeit und Verwertbarkeit von Bauteilen und Werkstoffen beachten. Damit soll sichergestellt werden, dass neue Kraftfahrzeuge so konzipiert werden, dass ihre Entsorgung am Ende des Lebenszyklus erleichtert wird.</wrxml> "
            + //
            "</j.0:expression_abstract> " + //
            "<j.0:expression_belongs_to_work rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/uriserv/n26102\"/> " + //
            "<j.0:expression_uses_language rdf:resource=\"http://publications.europa.eu/resource/authority/language/DEU\"/> " + //
            "</rdf:Description> " + //
            "<rdf:Description rdf:about=\"http://cellar-dev.publications.europa.eu/resource/cellar/518ccd49-00ae-4e97-8b6d-41330761c1ba.0001\"> "
            + //
            "<owl:sameAs rdf:resource=\"http://cellar-dev.publications.europa.eu/resource/uriserv/n26102.DEU\"/> " + //
            "</rdf:Description> " + //
            "</rdf:RDF>";

    private static VirtuosoDataSource virtuosoDataSource;

    public static void main(String[] args) throws Exception {
        // write model to Virtuoso
        for (int i = 0; i < 1500; i++) {
            try {
                final Model model = read(MODEL_STR, null);
                processVirtuosoBackup(CELLAR_URI, model);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static VirtuosoDataSource getVirtuosoDataSource() throws Exception {
        if (virtuosoDataSource != null) {
            return virtuosoDataSource;
        }

        final String serverName = "10.2.0.244";
        final int serverPort = 1114;
        virtuosoDataSource = new VirtuosoDataSource();
        virtuosoDataSource.setUser("dba");
        virtuosoDataSource.setPassword("dba");
        virtuosoDataSource.setDataSourceName("jdbc:virtuoso://" + serverName + ":" + serverPort + "/charset=UTF-8");
        virtuosoDataSource.setServerName(serverName);
        virtuosoDataSource.setPortNumber(serverPort);
        virtuosoDataSource.setCharset(CharEncoding.UTF_8);
        return virtuosoDataSource;
    }

    private static void processVirtuosoBackup(final String existingCellarUri, final Model model) throws Exception {
        Dataset dataset = null;
        try {
            // gets the connection to the dataset
            dataset = new VirtDataset(getVirtuosoDataSource());

            // write data in Virtuoso
            updateModel(dataset, existingCellarUri, model);
        } finally {
            if (dataset != null) {
                dataset.close();
            }
        }
    }

    private static void updateModel(final Dataset dataset, final String existingCellarUri, final Model newModel) throws Exception {
        Model existingModel = null;
        try {
            // load existing model
            existingModel = dataset.getNamedModel(existingCellarUri);
            if (existingModel == null) {
                existingModel = ModelFactory.createDefaultModel();
                dataset.addNamedModel(existingCellarUri, existingModel);
            }

            // update existing model with new model
            mergeModels(newModel, existingModel, true);
        } finally {
            closeQuietly(existingModel, newModel);
        }
    }

    private static void mergeModels(final Model sourceModel, final Model destModel, final boolean optimize) {
        if (!optimize) {
            destModel.removeAll();
            destModel.add(sourceModel);
            return;
        }

        Model modelToRemove = null;
        Model modelToAdd = null;
        try {
            if (!destModel.isEmpty()) {
                modelToRemove = destModel.difference(sourceModel);
                modelToAdd = sourceModel.difference(destModel);
            } else {
                modelToRemove = ModelFactory.createDefaultModel();
                modelToAdd = sourceModel;
            }
            destModel.remove(modelToRemove.listStatements());
            destModel.add(modelToAdd);
        } finally {
            closeQuietly(modelToRemove, modelToAdd);
        }
    }

    private static void closeQuietly(final Model... models) {
        for (final Model model : models) {
            if (model != null) {
                model.close();
            }
        }
    }

    private static Model read(final String modelStr, final String lang) {
        final String myModelStr = (modelStr == null ? "" : modelStr);
        final InputStream modelStream = new ByteArrayInputStream(myModelStr.getBytes(Charset.forName("UTF-8")));

        final Model model = ModelFactory.createDefaultModel();
        model.read(modelStream, lang);
        try {
            modelStream.close();
        } catch (final IOException e) {
            System.err.println("A problem occurred while closing model stream.");
        }

        return model;
    }

}
