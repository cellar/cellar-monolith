package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.param;

import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.DisseminationRequest;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DisseminationRequestParameterTest {
    DisseminationRequestParameter paramOne;
    DisseminationRequestParameter paramTwo;

    @Before
    public void setup() throws Exception {
        paramOne = new DisseminationRequestParameter();
        paramTwo = new DisseminationRequestParameter();
    }

    @Test
    public void objectShouldNotEqualNull() throws Exception {
        assertFalse(paramOne.equals(null));
    }

    @Test
    public void objectShouldBeEqualToItself() throws Exception {
        assertTrue(paramOne.equals(paramOne));
    }

    @Test
    public void twoObjectsWithNullValuesForAcceptValueAndDisseminationRequestShouldNotThrowNPEandShouldBeEqual() throws Exception {
        assertTrue(paramOne.equals(paramTwo));
    }

    @Test
    public void twoObjectsWithDifferentDisseminationRequestAreNotEqual() throws Exception {
        paramOne.setDisseminationRequest(DisseminationRequest.TREE_XML);
        paramTwo.setDisseminationRequest(DisseminationRequest.BRANCH_XML);
        assertFalse(paramOne.equals(paramTwo));
    }

    @Test
    public void twoObjectsWithDifferentAcceptValueAreNotEqual() throws Exception {
        paramOne.setAcceptValueString("acceptOne");
        paramTwo.setAcceptValueString("acceptTwo");
        assertFalse(paramOne.equals(paramTwo));
    }

    @Test
    public void twoObjectsWithSameRequestAndAcceptValueAreEqual() throws Exception {
        paramOne.setDisseminationRequest(DisseminationRequest.TREE_XML);
        paramOne.setAcceptValueString("acceptOne");
        paramTwo.setDisseminationRequest(DisseminationRequest.TREE_XML);
        paramTwo.setAcceptValueString("acceptOne");
        assertTrue(paramOne.equals(paramTwo));
    }

    @Test
    public void twoObjectsWithSameRequestButDifferentAcceptValueShouldHaveDifferentHashcode() throws Exception {
        paramOne.setDisseminationRequest(DisseminationRequest.TREE_XML);
        paramOne.setAcceptValueString("acceptOne");
        paramTwo.setDisseminationRequest(DisseminationRequest.TREE_XML);
        paramTwo.setAcceptValueString("acceptTwo");
        assertNotEquals(paramOne.hashCode(), paramTwo.hashCode());
        assertFalse(paramOne.equals(paramTwo));
    }
}
