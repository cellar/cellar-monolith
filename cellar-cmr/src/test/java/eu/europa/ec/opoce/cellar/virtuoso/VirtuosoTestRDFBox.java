package eu.europa.ec.opoce.cellar.virtuoso;

import eu.europa.ec.opoce.cellar.cmr.model.ModelUtils;
import eu.europa.ec.opoce.cellar.common.util.XMLUtils;
import org.apache.commons.lang.CharEncoding;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.rdf.model.*;
import org.springframework.core.io.FileSystemResource;
import org.springframework.util.Assert;
import org.xml.sax.InputSource;
import virtuoso.jdbc4.VirtuosoDataSource;
import virtuoso.jena.driver.VirtDataset;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpression;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;

public class VirtuosoTestRDFBox {

    private static VirtuosoDataSource virtuosoDataSource;

    public static void main(String[] args) throws Exception {
        final String graphName = "http://cellar-dev.publications.europa.eu/resource/cellar/UUUUU";

        // write model
        final File file = new File("src\\test\\resources\\cellarExample.rdf");
        final Model model = read(file);
        writeModel(graphName, model);

        final Resource sIn = ResourceFactory.createResource("http://cellar-dev.publications.europa.eu/resource/uriserv/rx0037.FRA");
        final Property pIn = ResourceFactory.createProperty("http://publications.europa.eu/ontology/cdm#expression_abstract");
        final Literal oOut = readModel(graphName, sIn, pIn, null).listStatements().next().getObject().asLiteral();

        DocumentBuilderFactory df = DocumentBuilderFactory.newInstance();
        df.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        df.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
        DocumentBuilder builder = df.newDocumentBuilder();
        final XPath xpath = XMLUtils.newXPathFactory().newXPath();
        final XPathExpression expression = xpath
                .compile("/*[local-name()='RDF']/*[local-name()='Description']/*[local-name()='expression_abstract']");
        final String oExpected = expression.evaluate(builder.parse(new InputSource("src\\test\\resources\\cellarExample.rdf")));

        Assert.isTrue(oExpected.equals(oOut.getString().replaceAll("\\<.*?>", "")));

        final String graphName2 = "http://cellar-dev.publications.europa.eu/resource/cellar/UUUUUU";
        // write model
        final File file2 = new File("src\\test\\resources\\cdm4Example.rdf");
        final Model model2 = read(file2);
        writeModel(graphName2, model2);

        final Resource sIn2 = ResourceFactory.createResource("http://cellar-dev.publications.europa.eu/resource/uriserv/rx0037.FRA");
        final Literal oOut2 = readModel(graphName2, sIn2, null, null).listStatements().next().getObject().asLiteral();

        final XPath xpath2 = XMLUtils.newXPathFactory().newXPath();

        final XPathExpression expression2 = xpath2
                .compile("/*[local-name()='RDF']/*[local-name()='Description']/*[local-name()='comment']");
        final String oExpected2 = expression2.evaluate(builder.parse(
                new InputSource("src\\test\\resources\\cdm4Example.rdf")));

        Assert.isTrue(oExpected2.equals(oOut2.getString().replaceAll("\\<.*?>", "")));
    }

    private static VirtuosoDataSource getVirtuosoDataSource() throws Exception {
        if (virtuosoDataSource != null) {
            return virtuosoDataSource;
        }

        final String serverName = "10.2.0.224";
        final int serverPort = 1116;
        virtuosoDataSource = new VirtuosoDataSource();
        virtuosoDataSource.setUser("dba");
        virtuosoDataSource.setPassword("dba");
        virtuosoDataSource.setDataSourceName("jdbc:virtuoso://" + serverName + ":" + serverPort + "/charset=UTF-8");
        virtuosoDataSource.setServerName(serverName);
        virtuosoDataSource.setPortNumber(serverPort);
        virtuosoDataSource.setCharset(CharEncoding.UTF_8);
        return virtuosoDataSource;
    }

    private static void writeModel(final String graphName, final Model newModel) throws Exception {
        VirtDataset dataset = null;
        try {
            // gets the connection
            dataset = new VirtDataset(getVirtuosoDataSource());

            // get direct model of digital objects that are supposed to be inserted
            Model existingModel = null;
            try {
                // load existing model
                existingModel = dataset.getNamedModel(graphName);

                // update new model
                existingModel.removeAll();
                existingModel.add(newModel);
            } finally {
                closeQuietly(existingModel, newModel);
            }
        } finally {
            if (dataset != null) {
                dataset.close();
            }
        }
    }

    private static Model readModel(final String graphName, final Resource s, final Property p, final RDFNode o) throws Exception {
        final Model retModel = ModelFactory.createDefaultModel();

        VirtDataset dataset = null;
        Model existingModel = null;
        try {
            dataset = new VirtDataset(getVirtuosoDataSource());
            existingModel = dataset.getNamedModel(graphName);
            if (existingModel != null) {
                retModel.add(existingModel.listStatements(s, p, o).toList());
            }
        } finally {
            ModelUtils.closeQuietly(existingModel);
            if (dataset != null) {
                dataset.close();
            }
        }

        return retModel;
    }

    private static void closeQuietly(final Model... models) {
        for (final Model model : models) {
            if (model != null) {
                model.close();
            }
        }
    }

    private static Model read(final File file) throws IOException {
        final Collection<org.springframework.core.io.Resource> resources = new ArrayList<org.springframework.core.io.Resource>();
        resources.add(new FileSystemResource(file));

        final Model model = ModelFactory.createDefaultModel();

        for (final org.springframework.core.io.Resource resource : resources) {
            InputStream inputstream = null;
            try {
                inputstream = resource.getInputStream();
                final RDFReader rdfReader = getBasicReader(model, resource);
                rdfReader.read(model, inputstream, null);
            } catch (final RuntimeException e) {
                closeQuietly(model);
                throw e;
            } finally {
                if (inputstream != null) {
                    inputstream.close();
                }
            }
        }

        return model;
    }

    private static RDFReader getBasicReader(final Model model, final org.springframework.core.io.Resource resource) {
        try {
            final String filename = resource.getFilename();
            if (filename == null) {
                throw new IllegalStateException("resource loaded from byte array does not have a filename");
            }
            final String extension = StringUtils.substringAfterLast(filename, ".").toLowerCase();

            if ("owl".equals(extension)) {
                return model.getReader();
            } else if ("nt".equals(extension)) {
                return model.getReader("N-TRIPLE");
            } else if ("n3".equals(extension) || "ttl".equals(extension)) {
                return model.getReader("TURTLE");
            }

            return model.getReader();
        } catch (final IllegalStateException e) {
            return model.getReader();
        }
    }

}
