package eu.europa.ec.opoce.cellar.virtuoso;

import org.apache.commons.lang.CharEncoding;
import virtuoso.jdbc4.VirtuosoDataSource;
import virtuoso.jena.driver.VirtDataset;

import java.text.SimpleDateFormat;
import java.util.Date;

public class VirtuosoTestConnectDb {

    private static final String USER = "dba", PASSWORD = "dba";
    //private static final String USER = "cellar", PASSWORD = "cellar";	
    //private static final String USER = "dba", PASSWORD = "dba";

    private static final String[] servers = new String[] {
            "op-eurlex-virtuoso-dev.aris-lux.lan:1111", // virtuoso-old
            "op-eurlex-virtuoso-dev.aris-lux.lan:1114", // virtuoso-dev1@dev
            "op-eurlex-virtuoso-dev.aris-lux.lan:1115", // virtuoso-dev2@dev
            "op-eurlex-cm-dev6.aris-lux.lan:1116", //, virtuoso-dev3@dev6
            "op-eurlex-cm-dev6.aris-lux.lan:1117", // virtuoso-dev4@dev6
            "op-eurlex-cm-dev6.aris-lux.lan:1118", // virtuoso-dev5@dev6
            "op-eurlex-cm-dev6.aris-lux.lan:1119" // virtuoso-dev6@dev6
    };

    private static final SimpleDateFormat SDF = new SimpleDateFormat("HH:mm:ss.SSS");

    public static void main(String[] args) throws Exception {
        //System.out.println("CP: " + System.getProperty("java.class.path").replaceAll(";", "\n"));
        for (int i = 0; i < servers.length; i++)
            tst(i);
    }

    public static void tst(int idx) throws Exception {
        System.out.print(SDF.format(new Date()) + ". Connecting \"" + USER + "\" to " + servers[idx] + " ...");
        VirtuosoDataSource vDS = new VirtuosoDataSource();
        vDS.setUser(USER);
        vDS.setPassword(PASSWORD);
        vDS.setDataSourceName("jdbc:virtuoso://" + servers[idx] + "/charset=UTF-8");
        String[] server = servers[idx].split(":");
        String serverName = server[0];
        int serverPort = Integer.parseInt(server[1]);
        vDS.setServerName(serverName);
        vDS.setPortNumber(serverPort);
        vDS.setCharset(CharEncoding.UTF_8);

        VirtDataset dataset = null;
        try {
            dataset = new VirtDataset(vDS);
            System.out.println(" SUCCESS");
        } catch (Exception ex) {
            System.out.println(" FAILURE:");
            ex.printStackTrace(System.out);
            System.out.println();
        } finally {
            if (dataset != null)
                dataset.close();
        }
    }
}
