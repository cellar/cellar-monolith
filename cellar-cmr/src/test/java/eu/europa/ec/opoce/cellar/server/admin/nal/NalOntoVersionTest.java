package eu.europa.ec.opoce.cellar.server.admin.nal;

import org.junit.Test;

import java.time.Instant;
import java.util.Date;

import static org.junit.Assert.*;

public class NalOntoVersionTest {

    private static final Instant ACTIVATION_DATE = Instant.parse("2018-01-01T01:00:00.00Z");
    private static final String VERSION_NUMBER = "14.1-alpha";

    @Test
    public void getFormattedVersionDateTest() {
        final NalOntoVersion version = new NalOntoVersion();

        version.setVersionDate(null);
        assertNull(version.getVersionDate());
        assertEquals("date not set", version.getFormattedVersionDate());

        version.setVersionDate(Date.from(ACTIVATION_DATE));
        assertNotNull(version.getVersionDate());
        assertEquals("20180101-020000", version.getFormattedVersionDate());
    }

    @Test
    public void getFormattedVersionNumberTest() {
        final NalOntoVersion version = new NalOntoVersion();

        version.setVersionNumber(null);
        assertNull(version.getVersionNumber());
        assertEquals("version not set", version.getFormattedVersionNumber());

        version.setVersionNumber(VERSION_NUMBER);
        assertNotNull(version.getVersionNumber());
        assertEquals(VERSION_NUMBER, version.getFormattedVersionNumber());
    }
}