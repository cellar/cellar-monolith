package eu.europa.ec.opoce.cellar.server.dissemination.negotiation;

import org.junit.Test;

import static eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.DisseminationRequest.*;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * @author ARHS Developments
 */
public class DisseminationRequestTest {


    @Test
    public void acceptHeaderMustAcceptOneOptionalSpaceAfterSemiColon() {
        assertTrue(acceptValueOf("application/xml; notice=object") == OBJECT_XML);
        assertTrue(acceptValueOf("application/xml; notice=object; q=0.2") == OBJECT_XML);
        assertTrue(acceptValueOf("application/xml; notice=object;q=0.2") == OBJECT_XML);
        assertTrue(acceptValueOf("application/xml;notice=object; q=0.2") == OBJECT_XML);
        assertTrue(acceptValueOf("application/xml; notice=branch") == BRANCH_XML);
        assertTrue(acceptValueOf("application/xml; notice=branch; q=0.2") == BRANCH_XML);
        assertTrue(acceptValueOf("application/xml; notice=branch;q=0.2") == BRANCH_XML);
        assertTrue(acceptValueOf("application/xml;notice=branch; q=0.2") == BRANCH_XML);
        assertTrue(acceptValueOf("application/xml; notice=tree") == TREE_XML);
        assertTrue(acceptValueOf("application/xml; notice=tree; q=0.2") == TREE_XML);
        assertTrue(acceptValueOf("application/xml; notice=tree;q=0.2") == TREE_XML);
        assertTrue(acceptValueOf("application/xml;notice=tree; q=0.2") == TREE_XML);
        assertTrue(acceptValueOf("application/rdf+xml; q=0.2") == OBJECT_RDF_FULL);
        assertTrue(acceptValueOf("application/rdf+xml; notice=non-inferred") == OBJECT_RDF_NONINFERRED);
        assertTrue(acceptValueOf("application/rdf+xml; notice=non-inferred; q=0.2") == OBJECT_RDF_NONINFERRED);
        assertTrue(acceptValueOf("application/rdf+xml; notice=non-inferred;q=0.2") == OBJECT_RDF_NONINFERRED);
        assertTrue(acceptValueOf("application/rdf+xml;notice=non-inferred; q=0.2") == OBJECT_RDF_NONINFERRED);
        assertTrue(acceptValueOf("application/rdf+xml; notice=tree") == TREE_RDF_FULL);
        assertTrue(acceptValueOf("application/rdf+xml; notice=tree; q=0.2") == TREE_RDF_FULL);
        assertTrue(acceptValueOf("application/rdf+xml; notice=tree;q=0.2") == TREE_RDF_FULL);
        assertTrue(acceptValueOf("application/rdf+xml;notice=tree; q=0.2") == TREE_RDF_FULL);
        assertTrue(acceptValueOf("application/rdf+xml; notice=non-inferred-tree") == TREE_RDF_NONINFERRED);
        assertTrue(acceptValueOf("application/rdf+xml; notice=non-inferred-tree; q=0.2") == TREE_RDF_NONINFERRED);
        assertTrue(acceptValueOf("application/rdf+xml; notice=non-inferred-tree;q=0.2") == TREE_RDF_NONINFERRED);
        assertTrue(acceptValueOf("application/rdf+xml;notice=non-inferred-tree; q=0.2") == TREE_RDF_NONINFERRED);
        assertTrue(acceptValueOf("application/list; q=0.2") == LIST);
        assertTrue(acceptValueOf("application/list; mtype=fmx4") == LIST);
        assertTrue(acceptValueOf("application/list; mtype=fmx4; q=0.2") == LIST);
        assertTrue(acceptValueOf("application/list; mtype=fmx4;q=0.2") == LIST);
        assertTrue(acceptValueOf("application/list;mtype=fmx4; q=0.2") == LIST);
        assertTrue(acceptValueOf("application/zip; q=0.2") == ZIP);
        assertTrue(acceptValueOf("application/zip; mtype=fmx4") == ZIP);
        assertTrue(acceptValueOf("application/zip; mtype=fmx4; q=0.2") == ZIP);
        assertTrue(acceptValueOf("application/zip; mtype=fmx4;q=0.2") == ZIP);
        assertTrue(acceptValueOf("application/zip;mtype=fmx4; q=0.2") == ZIP);
        assertTrue(acceptValueOf("application/xml; notice=identifiers") == IDENTIFIER_XML);
        assertTrue(acceptValueOf("application/xml; notice=identifiers; q=0.2") == IDENTIFIER_XML);
        assertTrue(acceptValueOf("application/xml; notice=identifiers;q=0.2") == IDENTIFIER_XML);
        assertTrue(acceptValueOf("application/xml;notice=identifiers; q=0.2") == IDENTIFIER_XML);
    }

    @Test
    public void acceptHeaderMustNotAcceptMultipleSpacesAfterSemiColon() {
        assertFalse(acceptValueOf("application/xml;  notice=object") == OBJECT_XML);
        assertFalse(acceptValueOf("application/xml; notice=object;  q=0.2") == OBJECT_XML);
        assertFalse(acceptValueOf("application/xml;  notice=object;q=0.2") == OBJECT_XML);
        assertFalse(acceptValueOf("application/xml;notice=object;  q=0.2") == OBJECT_XML);
        assertFalse(acceptValueOf("application/xml;  notice=branch") == BRANCH_XML);
        assertFalse(acceptValueOf("application/xml;  notice=branch;  q=0.2") == BRANCH_XML);
        assertFalse(acceptValueOf("application/xml;  notice=branch;q=0.2") == BRANCH_XML);
        assertFalse(acceptValueOf("application/xml;notice=branch;  q=0.2") == BRANCH_XML);
        assertFalse(acceptValueOf("application/xml;  notice=tree") == TREE_XML);
        assertFalse(acceptValueOf("application/xml;  notice=tree;  q=0.2") == TREE_XML);
        assertFalse(acceptValueOf("application/xml;  notice=tree;q=0.2") == TREE_XML);
        assertFalse(acceptValueOf("application/xml;notice=tree;  q=0.2") == TREE_XML);
        assertFalse(acceptValueOf("application/rdf+xml;  q=0.2") == OBJECT_RDF_FULL);
        assertFalse(acceptValueOf("application/rdf+xml;  notice=non-inferred") == OBJECT_RDF_NONINFERRED);
        assertFalse(acceptValueOf("application/rdf+xml;  notice=non-inferred;  q=0.2") == OBJECT_RDF_NONINFERRED);
        assertFalse(acceptValueOf("application/rdf+xml;  notice=non-inferred;q=0.2") == OBJECT_RDF_NONINFERRED);
        assertFalse(acceptValueOf("application/rdf+xml;notice=non-inferred;  q=0.2") == OBJECT_RDF_NONINFERRED);
        assertFalse(acceptValueOf("application/rdf+xml;  notice=tree") == TREE_RDF_FULL);
        assertFalse(acceptValueOf("application/rdf+xml;  notice=tree;  q=0.2") == TREE_RDF_FULL);
        assertFalse(acceptValueOf("application/rdf+xml;  notice=tree;q=0.2") == TREE_RDF_FULL);
        assertFalse(acceptValueOf("application/rdf+xml;notice=tree;  q=0.2") == TREE_RDF_FULL);
        assertFalse(acceptValueOf("application/rdf+xml;  notice=non-inferred-tree") == TREE_RDF_NONINFERRED);
        assertFalse(acceptValueOf("application/rdf+xml;  notice=non-inferred-tree;  q=0.2") == TREE_RDF_NONINFERRED);
        assertFalse(acceptValueOf("application/rdf+xml;  notice=non-inferred-tree;q=0.2") == TREE_RDF_NONINFERRED);
        assertFalse(acceptValueOf("application/rdf+xml;notice=non-inferred-tree;  q=0.2") == TREE_RDF_NONINFERRED);
        assertFalse(acceptValueOf("application/list;  q=0.2") == LIST);
        assertFalse(acceptValueOf("application/list;  mtype=fmx4") == LIST);
        assertFalse(acceptValueOf("application/list;   mtype=fmx4;q=0.2") == LIST);
        assertFalse(acceptValueOf("application/list;  mtype=fmx4;q=0.2") == LIST);
        assertFalse(acceptValueOf("application/zip;  q=0.2") == ZIP);
        assertFalse(acceptValueOf("application/zip;  mtype=fmx4") == ZIP);
        assertFalse(acceptValueOf("application/zip;  mtype=fmx4;  q=0.2") == ZIP);
        assertFalse(acceptValueOf("application/zip;  mtype=fmx4;q=0.2") == ZIP);
        assertFalse(acceptValueOf("application/xml;  notice=identifiers") == IDENTIFIER_XML);
        assertFalse(acceptValueOf("application/xml;  notice=identifiers;  q=0.2") == IDENTIFIER_XML);
        assertFalse(acceptValueOf("application/xml;  notice=identifiers;q=0.2") == IDENTIFIER_XML);
    }

    @Test
    public void acceptHeaderMustAcceptNoSpaceAfterSemiColon() {
        assertTrue(acceptValueOf("application/xml;notice=object") == OBJECT_XML);
        assertTrue(acceptValueOf("application/xml;notice=object;q=0.2") == OBJECT_XML);
        assertTrue(acceptValueOf("application/xml;notice=object;q=0.2") == OBJECT_XML);
        assertTrue(acceptValueOf("application/xml;notice=object;q=0.2") == OBJECT_XML);
        assertTrue(acceptValueOf("application/xml;notice=branch") == BRANCH_XML);
        assertTrue(acceptValueOf("application/xml;notice=branch;q=0.2") == BRANCH_XML);
        assertTrue(acceptValueOf("application/xml;notice=branch;q=0.2") == BRANCH_XML);
        assertTrue(acceptValueOf("application/xml;notice=branch;q=0.2") == BRANCH_XML);
        assertTrue(acceptValueOf("application/xml;notice=tree") == TREE_XML);
        assertTrue(acceptValueOf("application/xml;notice=tree;q=0.2") == TREE_XML);
        assertTrue(acceptValueOf("application/xml;notice=tree;q=0.2") == TREE_XML);
        assertTrue(acceptValueOf("application/xml;notice=tree;q=0.2") == TREE_XML);
        assertTrue(acceptValueOf("application/rdf+xml;q=0.2") == OBJECT_RDF_FULL);
        assertTrue(acceptValueOf("application/rdf+xml;notice=non-inferred") == OBJECT_RDF_NONINFERRED);
        assertTrue(acceptValueOf("application/rdf+xml;notice=non-inferred;q=0.2") == OBJECT_RDF_NONINFERRED);
        assertTrue(acceptValueOf("application/rdf+xml;notice=non-inferred;q=0.2") == OBJECT_RDF_NONINFERRED);
        assertTrue(acceptValueOf("application/rdf+xml;notice=non-inferred;q=0.2") == OBJECT_RDF_NONINFERRED);
        assertTrue(acceptValueOf("application/rdf+xml;notice=tree") == TREE_RDF_FULL);
        assertTrue(acceptValueOf("application/rdf+xml;notice=tree;q=0.2") == TREE_RDF_FULL);
        assertTrue(acceptValueOf("application/rdf+xml;notice=tree;q=0.2") == TREE_RDF_FULL);
        assertTrue(acceptValueOf("application/rdf+xml;notice=tree;q=0.2") == TREE_RDF_FULL);
        assertTrue(acceptValueOf("application/rdf+xml;notice=non-inferred-tree") == TREE_RDF_NONINFERRED);
        assertTrue(acceptValueOf("application/rdf+xml;notice=non-inferred-tree;q=0.2") == TREE_RDF_NONINFERRED);
        assertTrue(acceptValueOf("application/rdf+xml;notice=non-inferred-tree;q=0.2") == TREE_RDF_NONINFERRED);
        assertTrue(acceptValueOf("application/rdf+xml;notice=non-inferred-tree;q=0.2") == TREE_RDF_NONINFERRED);
        assertTrue(acceptValueOf("application/list;q=0.2") == LIST);
        assertTrue(acceptValueOf("application/list;mtype=fmx4") == LIST);
        assertTrue(acceptValueOf("application/list;mtype=fmx4;q=0.2") == LIST);
        assertTrue(acceptValueOf("application/list;mtype=fmx4;q=0.2") == LIST);
        assertTrue(acceptValueOf("application/list;mtype=fmx4;q=0.2") == LIST);
        assertTrue(acceptValueOf("application/zip;q=0.2") == ZIP);
        assertTrue(acceptValueOf("application/zip;mtype=fmx4") == ZIP);
        assertTrue(acceptValueOf("application/zip;mtype=fmx4;q=0.2") == ZIP);
        assertTrue(acceptValueOf("application/zip;mtype=fmx4;q=0.2") == ZIP);
        assertTrue(acceptValueOf("application/zip;mtype=fmx4;q=0.2") == ZIP);
        assertTrue(acceptValueOf("application/xml;notice=identifiers") == IDENTIFIER_XML);
        assertTrue(acceptValueOf("application/xml;notice=identifiers;q=0.2") == IDENTIFIER_XML);
        assertTrue(acceptValueOf("application/xml;notice=identifiers;q=0.2") == IDENTIFIER_XML);
        assertTrue(acceptValueOf("application/xml;notice=identifiers;q=0.2") == IDENTIFIER_XML);
    }

}
