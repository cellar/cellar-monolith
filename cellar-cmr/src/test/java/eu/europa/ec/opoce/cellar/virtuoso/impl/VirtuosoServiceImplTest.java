package eu.europa.ec.opoce.cellar.virtuoso.impl;

import eu.europa.ec.opoce.cellar.server.admin.sparql.VirtuosoInformation;
import eu.europa.ec.opoce.cellar.virtuoso.VirtuosoService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import virtuoso.jdbc4.Driver;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class VirtuosoServiceImplTest {

    @Mock
    private DataSource virtuosoDataSource;

    @InjectMocks
    private VirtuosoService virtuosoService = new VirtuosoServiceImpl();

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getVirtuosoInformation() throws SQLException {
        final String serverVersion = "1.0.0";

        final Connection connection = getConnection(true, serverVersion);

        try (Connection con = this.virtuosoDataSource.getConnection()) {
            when(con).thenReturn(connection);

            Driver driver = new Driver();
            final String driverVersion = driver.getMajorVersion() + "." + driver.getMinorVersion();

            final VirtuosoInformation virtuosoInformation = virtuosoService.getVirtuosoInformation();

            assertEquals(serverVersion, virtuosoInformation.getServerVersion());
            assertEquals(driverVersion, virtuosoInformation.getDriverVersion());
        }
    }

    @Test
    public void getDefaultServerForEmptyResultSet() throws SQLException {
        final Connection connection = getConnection(false, null);

        try (Connection con = this.virtuosoDataSource.getConnection()) {
            when(con).thenReturn(connection);

            Driver driver = new Driver();
            final String driverVersion = driver.getMajorVersion() + "." + driver.getMinorVersion();

            final VirtuosoInformation virtuosoInformation = virtuosoService.getVirtuosoInformation();

            assertEquals("unknown", virtuosoInformation.getServerVersion());
            assertEquals(driverVersion, virtuosoInformation.getDriverVersion());
        }
    }

    @Test
    public void getDefaultServerVersionForException() throws SQLException {
        try (Connection con = this.virtuosoDataSource.getConnection()) {
            when(con).thenThrow(new RuntimeException("Test exception"));

            final VirtuosoInformation virtuosoInformation = virtuosoService.getVirtuosoInformation();

            assertEquals("unknown", virtuosoInformation.getServerVersion());
        }
    }

    private Connection getConnection(final boolean hasNext, final String serverVersion) throws SQLException {
        final ResultSet resultSet = mock(ResultSet.class);
        when(resultSet.next()).thenReturn(hasNext);

        if(hasNext) {
            when(resultSet.getString("version")).thenReturn(serverVersion);
        }

        final Statement statement = mock(Statement.class);
        when(statement.executeQuery(any(String.class))).thenReturn(resultSet);

        final Connection connection = mock(Connection.class);
        when(connection.createStatement()).thenReturn(statement);

        return connection;
    }
}
