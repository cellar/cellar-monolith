<?xml version="1.0"?>
<rdf:RDF xmlns="http://publications.europa.eu/ontology/cdm/cmr#"
     xml:base="http://publications.europa.eu/ontology/cdm/cmr"
     xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
     xmlns:xl="http://www.w3.org/2008/05/skos-xl#"
     xmlns:cdm="http://publications.europa.eu/ontology/cdm#"
     xmlns:cmr="http://publications.europa.eu/ontology/cdm/cmr#"
     xmlns:owl="http://www.w3.org/2002/07/owl#"
     xmlns:xsd="http://www.w3.org/2001/XMLSchema#"
     xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
     xmlns:skos="http://www.w3.org/2004/02/skos/core#">
    <owl:Ontology rdf:about="http://publications.europa.eu/ontology/cdm/cmr">
        <owl:imports rdf:resource="http://www.w3.org/2004/02/skos/core"/>
        <owl:imports rdf:resource="http://www.w3.org/2008/05/skos-xl"/>
    </owl:Ontology>
    


    <!-- 
    ///////////////////////////////////////////////////////////////////////////////////////
    //
    // Annotation properties
    //
    ///////////////////////////////////////////////////////////////////////////////////////
     -->

    <owl:AnnotationProperty rdf:about="http://publications.europa.eu/ontology/cdm/cmr#metsDivType">
        <rdfs:label xml:lang="en">mets div type</rdfs:label>
        <rdfs:comment xml:lang="en">The values assigned to a mets/structMap/div/@TYPE

1) validation usage
On ingestion (create and update) this allows to verify consistency between
- the structMap/div/@Type value
- the class assigned in rdf to the structMap/div/@CONTENTIDS

On read, this provides a generic type to generate the value of mets mets/structMap/div/@TYPE

Note, the logic above is applied in only one direction
- if the artifact is in mets/structMap//div, the RDF must assert the corresponding rdf:type constraint
- it is NOT the case that if in RDF:
    x rdf:type classA and classA po-cmr:metsDivType &quot;tA&quot;
  automatically implies that x is provided in a mets div.
  However, if x is provided in a div, it must be with @TYPE= &quot;tA&quot;
To ensure a unique div/@TYPE assignment, point 2) defines some business rules  

2) A business rule should check
2.1) ontology [Tbox] validation
- no to classes can have the same po-cmr:metsDivType
- on a realized class hierarchy, there can be at most one class that has a po-cmr:metsDivType annotation asserted
2.2) KB [Abox] validation
- for any instance x, 
  if
    x a classA and classA po-cmr:metsDivType {tA}
  and
    x a classB and classB po-cmr:metsDivType {tB}
  then
    {tA} shall not be different from {tB} 
</rdfs:comment>
    </owl:AnnotationProperty>
    <owl:AnnotationProperty rdf:about="http://www.w3.org/2000/01/rdf-schema#label"/>
    <owl:AnnotationProperty rdf:about="http://publications.europa.eu/ontology/cdm/cmr#facetAcronym">
        <rdfs:label xml:lang="en">facet acronym</rdfs:label>
        <rdfs:comment xml:lang="en">The facet acronym is set the facet defining properties (sub properties of po-cmr:facet).
The acronym is used as a suffix in the PO XML notice serialization in order to distinguish the colaasifying property (identifying a skos:Concept) and the the facets of that concept.</rdfs:comment>
    </owl:AnnotationProperty>
    <owl:AnnotationProperty rdf:about="http://publications.europa.eu/ontology/cdm/cmr#to_be_indexed">
		<rdfs:label>Indicates if this fields must be indexed for IDOL</rdfs:label>
    </owl:AnnotationProperty>
    <owl:AnnotationProperty rdf:about="http://publications.europa.eu/ontology/cdm/cmr#in_notice">
		<rdfs:label>Write a certain property as present in branch, tree and object notices.</rdfs:label>
    </owl:AnnotationProperty>
    <owl:AnnotationProperty rdf:about="http://www.w3.org/2000/01/rdf-schema#comment"/>
    


    <!-- 
    ///////////////////////////////////////////////////////////////////////////////////////
    //
    // Object Properties
    //
    ///////////////////////////////////////////////////////////////////////////////////////
     -->

    


    <!-- http://publications.europa.eu/ontology/cdm/cmr#facet -->

    <owl:ObjectProperty rdf:about="http://publications.europa.eu/ontology/cdm/cmr#facet">
        <rdfs:label xml:lang="en">facet</rdfs:label>
        <rdfs:comment xml:lang="en">Super property of all specific facet properties.
A facet property may be asserted on any skos:Concept.
Its value is an object defining a facet value for the subject (skos:Concept instance)</rdfs:comment>
        <rdfs:domain rdf:resource="http://www.w3.org/2004/02/skos/core#Concept"/>
		<in_notice>true</in_notice>
    </owl:ObjectProperty>
    


    <!-- http://publications.europa.eu/ontology/cdm/cmr#facet_D -->

    <owl:ObjectProperty rdf:about="http://publications.europa.eu/ontology/cdm/cmr#facet_D">
        <rdfs:label xml:lang="en">facet_D</rdfs:label>
        <facetAcronym>_DOM</facetAcronym>
        <rdfs:comment xml:lang="en">The &quot;DOM&quot; type facet of a concept

In EUROVOC, for each concept [C],
[C] eu:inDomain [D]
 =&gt; [C]  po-cmr:facet_D  [D]

in CMR, a facet_D must be serialized in a notice using the suffix &quot;_DOM&quot;</rdfs:comment>
        <rdfs:subPropertyOf rdf:resource="http://publications.europa.eu/ontology/cdm/cmr#facet"/>
		<in_notice>true</in_notice>
    </owl:ObjectProperty>
    


    <!-- http://publications.europa.eu/ontology/cdm/cmr#facet_M -->

    <owl:ObjectProperty rdf:about="http://publications.europa.eu/ontology/cdm/cmr#facet_M">
        <rdfs:label xml:lang="en">facet_M</rdfs:label>
        <rdfs:comment xml:lang="en">The &quot;MTH&quot; type facet of a concept

In EUROVOC, for each concept [C],
[C] skos:inScheme [S]
[S] a eu:MicroThesaurus
 =&gt; [C]  po-cmr:facet_M  [S]

in CMR, a facet_M must be serialized in a notice using the suffix &quot;_MTH&quot;</rdfs:comment>
        <facetAcronym>_MTH</facetAcronym>
        <rdfs:subPropertyOf rdf:resource="http://publications.europa.eu/ontology/cdm/cmr#facet"/>
		<in_notice>true</in_notice>
    </owl:ObjectProperty>
    


    <!-- http://publications.europa.eu/ontology/cdm/cmr#facet_T -->

    <owl:ObjectProperty rdf:about="http://publications.europa.eu/ontology/cdm/cmr#facet_T">
        <rdfs:label xml:lang="en">facet_T</rdfs:label>
        <facetAcronym>_TT</facetAcronym>
        <rdfs:comment xml:lang="en">The &quot;TT&quot; type facet of a concept

In EUROVOC, for each concept [C],
[C] skos:topConceptOf [S]
[S] a eu:MicroThesaurus
 =&gt; [C]  po-cmr:facet_T  [S]

in CMR, a facet_T must be serialized in a notice using the suffix &quot;_TT&quot;</rdfs:comment>
        <rdfs:subPropertyOf rdf:resource="http://publications.europa.eu/ontology/cdm/cmr#facet"/>
		<in_notice>true</in_notice>
    </owl:ObjectProperty>
    


    <!-- http://publications.europa.eu/ontology/cdm/cmr#fallback -->

    <owl:ObjectProperty rdf:about="http://publications.europa.eu/ontology/cdm/cmr#fallback">
        <rdfs:label xml:lang="en">fallback extension label</rdfs:label>
        <rdfs:comment xml:lang="en">The property provides a fallback label for a concept [C] missing a preferred label in a specific language.

[C]  cmr:fallback _:x  .
   _:x  cmr:prefLabel   &#39;label&#39;@lang  . 
   _:x  cmr:lang        &#39;flang&#39;^^xsd:language    .

where 
- lang is the language for which [C] has no skos:prefLabel
- &#39;label&#39; is the preferred label of the concept in fallback language flang

</rdfs:comment>
        <rdfs:range rdf:resource="http://publications.europa.eu/ontology/cdm/cmr#FallbackLabel"/>
        <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2002/07/owl#topObjectProperty"/>
        <rdfs:domain rdf:resource="http://www.w3.org/2004/02/skos/core#Concept"/>
		<in_notice>true</in_notice>
    </owl:ObjectProperty>
    


    <!-- http://publications.europa.eu/ontology/cdm/cmr#metsStructSubDiv -->

    <owl:ObjectProperty rdf:about="http://publications.europa.eu/ontology/cdm/cmr#metsStructSubDiv">
        <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#InverseFunctionalProperty"/>
        <rdfs:label xml:lang="en">metsStructure sub div</rdfs:label>
        <rdfs:comment xml:lang="en">Indicates the unique hierarchy established by the PO METS protocol among mets div elements under mets/structMap.

the subject is the immediate parent div of the object div.

A) generation during mets processing:
metsStruct
   [ div @CONTENTIDS=x1
         [ div  @CONTENTIDS=x1.1 ... ]
         [ div  @CONTENTIDS=x1.2 ... ]
   ]

will create two statements:
- x1    po-cmr:metsStructSubDiv     x1.1
- x1    po-cmr:metsStructSubDiv     x1.2

B) Abox inference rules
1- after po-frbr based inference on the Abox the following rules must be used
1.1 w work_has_expression e --&gt; w po-cmr:metsStructSubDiv e
1.2 e expression_manifested_by_manifestation m --&gt; e po-cmr:metsStructSubDiv m
1.3 d dossier_contains_event_legal e  --&gt;  d po-cmr:metsStructSubDiv e
2- The hierarchy established by po-cmr:metsStructSubDiv must be a tree (mono hierarchy)

C) the property may be used to calculate
- tree
- branch
- parent
- children
</rdfs:comment>
        <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2002/07/owl#topObjectProperty"/>
		<in_notice>true</in_notice>
    </owl:ObjectProperty>
    


    <!-- http://publications.europa.eu/ontology/cdm/cmr#metsStructSuperDiv -->

    <owl:ObjectProperty rdf:about="http://publications.europa.eu/ontology/cdm/cmr#metsStructSuperDiv">
        <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#FunctionalProperty"/>
        <rdfs:label xml:lang="en">metsStruct super div</rdfs:label>
        <rdfs:comment xml:lang="en">Indicates the unique hierarchy established by the PO METS protocol among mets div elements under mets/structMap.

the subject is the immediate parent div of the object div.

A) generation during mets processing:
metsStruct
   [ div @CONTENTIDS=x1
         [ div  @CONTENTIDS=x1.1 ... ]
         [ div  @CONTENTIDS=x1.2 ... ]
   ]

will create two statements:
- x1.1    po-cmr:metsStructSuperDiv     x1
- x1.2    po-cmr:metsStructSuperDiv     x1

B) Abox inference rules
1- after po-frbr based inference on the Abox the following rules must be used
1.1 w work_has_expression e --&gt; e po-cmr:metsStructSuperDiv w
1.2 e expression_manifested_by_manifestation m --&gt; m po-cmr:metsStructSuperDiv e
1.3 d dossier_contains_event_legal e  --&gt;  e po-cmr:metsStructSuperDiv d
2- The hierarchy established by po-cmr:metsStructSuperDiv must be a tree (mono hierarchy)

C) the property may be used to calculate
- tree
- branch
- parent
- children
</rdfs:comment>
        <owl:inverseOf rdf:resource="http://publications.europa.eu/ontology/cdm/cmr#metsStructSubDiv"/>
        <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2002/07/owl#topObjectProperty"/>
		<in_notice>true</in_notice>
    </owl:ObjectProperty>
    


    <!-- http://www.w3.org/2002/07/owl#topObjectProperty -->

    <owl:ObjectProperty rdf:about="http://www.w3.org/2002/07/owl#topObjectProperty"/>
    


    <!-- 
    ///////////////////////////////////////////////////////////////////////////////////////
    //
    // Data properties
    //
    ///////////////////////////////////////////////////////////////////////////////////////
     -->

    


    <!-- http://publications.europa.eu/ontology/cdm/cmr#lang -->

    <owl:DatatypeProperty rdf:about="http://publications.europa.eu/ontology/cdm/cmr#lang">
        <rdf:type rdf:resource="http://www.w3.org/2002/07/owl#FunctionalProperty"/>
        <rdfs:label xml:lang="en">used language</rdfs:label>
        <rdfs:comment xml:lang="en">May be applied on several objects.

When applied on the cellar URI of an expression, it indicated the used languag of that expression.</rdfs:comment>
        <rdfs:range rdf:resource="http://www.w3.org/2001/XMLSchema#language"/>
        <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2002/07/owl#topDataProperty"/>
		<in_notice>true</in_notice>
    </owl:DatatypeProperty>
    


    <!-- http://publications.europa.eu/ontology/cdm/cmr#lastModificationDate -->

    <owl:DatatypeProperty rdf:about="http://publications.europa.eu/ontology/cdm/cmr#lastModificationDate">
        <rdfs:label xml:lang="en">last CMR modification date</rdfs:label>
        <rdfs:comment xml:lang="en">Modification date in CMR
- each object modified in the CMR must maintain a &#39;METS ingestion modification date&#39;.
- The date must be specified including a time zone
- second fractions are calculated as well.

The modification date is used
- for PO METS protocol (div@LABEL value) and for managing the cache.</rdfs:comment>
        <rdfs:range rdf:resource="http://www.w3.org/2001/XMLSchema#dateTime"/>
        <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2002/07/owl#topDataProperty"/>
		<to_be_indexed>true</to_be_indexed>
		<in_notice>true</in_notice>
    </owl:DatatypeProperty>
    


    <!-- http://publications.europa.eu/ontology/cdm/cmr#creationDate -->

    <owl:DatatypeProperty rdf:about="http://publications.europa.eu/ontology/cdm/cmr#creationDate">
        <rdfs:label xml:lang="en">CMR creation date</rdfs:label>
        <rdfs:comment xml:lang="en">Creation date in CMR
- each object ingered in the CMR must maintain a &#39;METS ingestion creation date&#39;.
- The date must be specified including a time zone
- second fractions are calculated as well.
		</rdfs:comment>
        <rdfs:range rdf:resource="http://www.w3.org/2001/XMLSchema#dateTime"/>
        <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2002/07/owl#topDataProperty"/>
		<to_be_indexed>true</to_be_indexed>
		<in_notice>true</in_notice>
    </owl:DatatypeProperty>

    <!-- http://publications.europa.eu/ontology/cdm/cmr#level -->

    <owl:DatatypeProperty rdf:about="http://publications.europa.eu/ontology/cdm/cmr#level">
        <rdfs:label xml:lang="en">level</rdfs:label>
        <rdfs:comment xml:lang="en">The depth of a concept in its concept scheme.

The depth is the number of broader terms that concept has in its concept scheme

If the concept scheme has poly hierarchy or if it is in more than 1 concept scheme, there can be more than 1 level.</rdfs:comment>
        <rdfs:range rdf:resource="http://www.w3.org/2001/XMLSchema#int"/>
        <rdfs:domain rdf:resource="http://www.w3.org/2004/02/skos/core#Concept"/>
		<in_notice>true</in_notice>
    </owl:DatatypeProperty>
    


    <!-- http://publications.europa.eu/ontology/cdm/cmr#manifestationMimeType -->

    <owl:DatatypeProperty rdf:about="http://publications.europa.eu/ontology/cdm/cmr#manifestationMimeType">
        <rdfs:label xml:lang="en">manifestation MIME-type</rdfs:label>
        <rdfs:comment xml:lang="en">The MIME type associated with a manifestation, and thus with all principal content streams of that manifestation.

&quot;principal&quot; here means the top level content stream MIME type.
(e.g. a principal or content stream may be in xhtml, while this xhtml may carry images in tiff, png or other formats)
Such top level content streams are in the PO metsMessage accessed with:
- mets/fileSec/fileGrp/file
</rdfs:comment>
        <rdfs:range rdf:resource="http://www.w3.org/2001/XMLSchema#string"/>
        <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2002/07/owl#topDataProperty"/>
		<in_notice>true</in_notice>
    </owl:DatatypeProperty>
    


    <!-- http://publications.europa.eu/ontology/cdm/cmr#noticeModificationDate -->

    <owl:DatatypeProperty rdf:about="http://publications.europa.eu/ontology/cdm/cmr#noticeModificationDate">
        <rdfs:label>notice modification date</rdfs:label>
        <rdfs:comment xml:lang="en">The modification date typically is attached to an object that serves to identify a graph (CONTEXT).
For every change in the DB that affects a notice of that object, this modification date is updated.

Intended use: manage cache server interface.</rdfs:comment>
        <rdfs:range rdf:resource="http://www.w3.org/2001/XMLSchema#dateTime"/>
        <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2002/07/owl#topDataProperty"/>
		<in_notice>true</in_notice>
    </owl:DatatypeProperty>
    


    <!-- http://publications.europa.eu/ontology/cdm/cmr#prefLabel -->

    <owl:DatatypeProperty rdf:about="http://publications.europa.eu/ontology/cdm/cmr#prefLabel">
        <rdfs:comment xml:lang="en">The label (a literal value) that serves as a fallback for a missing skos:prefLabel.</rdfs:comment>
        <rdfs:comment xml:lang="en">preferred fallback label</rdfs:comment>
        <rdfs:domain rdf:resource="http://publications.europa.eu/ontology/cdm/cmr#FallbackLabel"/>
        <rdfs:subPropertyOf rdf:resource="http://www.w3.org/2002/07/owl#topDataProperty"/>
		<in_notice>true</in_notice>
    </owl:DatatypeProperty>
    


    <!-- http://www.w3.org/2002/07/owl#topDataProperty -->

    <owl:DatatypeProperty rdf:about="http://www.w3.org/2002/07/owl#topDataProperty"/>
    


    <!-- 
    ///////////////////////////////////////////////////////////////////////////////////////
    //
    // Classes
    //
    ///////////////////////////////////////////////////////////////////////////////////////
     -->

    


    <!-- http://publications.europa.eu/ontology/cdm#agent -->

    <owl:Class rdf:about="http://publications.europa.eu/ontology/cdm#agent">
        <metsDivType>agent</metsDivType>
    </owl:Class>
    


    <!-- http://publications.europa.eu/ontology/cdm#dossier -->

    <owl:Class rdf:about="http://publications.europa.eu/ontology/cdm#dossier">
        <metsDivType>dossier</metsDivType>
    </owl:Class>
    


    <!-- http://publications.europa.eu/ontology/cdm#event -->

    <owl:Class rdf:about="http://publications.europa.eu/ontology/cdm#event">
        <metsDivType>event</metsDivType>
    </owl:Class>
    


    <!-- http://publications.europa.eu/ontology/cdm#expression -->

    <owl:Class rdf:about="http://publications.europa.eu/ontology/cdm#expression">
        <metsDivType>expression</metsDivType>
    </owl:Class>
    


    <!-- http://publications.europa.eu/ontology/cdm#manifestation -->

    <owl:Class rdf:about="http://publications.europa.eu/ontology/cdm#manifestation">
        <metsDivType>manifestation</metsDivType>
    </owl:Class>
    


    <!-- http://publications.europa.eu/ontology/cdm#work -->

    <owl:Class rdf:about="http://publications.europa.eu/ontology/cdm#work">
        <metsDivType>work</metsDivType>
    </owl:Class>
    


    <!-- http://publications.europa.eu/ontology/cdm/cmr#FallbackLabel -->

    <owl:Class rdf:about="http://publications.europa.eu/ontology/cdm/cmr#FallbackLabel">
        <rdfs:label xml:lang="en">Fallback label</rdfs:label>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="http://publications.europa.eu/ontology/cdm/cmr#prefLabel"/>
                <owl:cardinality rdf:datatype="http://www.w3.org/2001/XMLSchema#nonNegativeInteger">1</owl:cardinality>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:subClassOf>
            <owl:Restriction>
                <owl:onProperty rdf:resource="http://publications.europa.eu/ontology/cdm/cmr#lang"/>
                <owl:qualifiedCardinality rdf:datatype="http://www.w3.org/2001/XMLSchema#nonNegativeInteger">1</owl:qualifiedCardinality>
                <owl:onDataRange rdf:resource="http://www.w3.org/2001/XMLSchema#language"/>
            </owl:Restriction>
        </rdfs:subClassOf>
        <rdfs:comment xml:lang="en">The class describes the calculated fallback label used on a concept.
There will be one FallbackLabel for each of the missing preferred labels.

A preferred label is missing if there is no preferred label in one of the required languages.

The list of required languages and their fallback is a configuration artifact.

cmr:lang: The language from which a fallback label has been taken.
E.g. suppose
- we mis a preferred labal in BUL
- BUL has fallback: HUN, ENG, FRA
in case HUN has no prefeered label but ENG does, then the value will be set to &quot;eng&quot;</rdfs:comment>
    </owl:Class>
    


    <!-- http://www.w3.org/2004/02/skos/core#Concept -->

    <owl:Class rdf:about="http://www.w3.org/2004/02/skos/core#Concept"/>
</rdf:RDF>



<!-- Generated by the OWL API (version 3.1.0.20069) http://owlapi.sourceforge.net -->

