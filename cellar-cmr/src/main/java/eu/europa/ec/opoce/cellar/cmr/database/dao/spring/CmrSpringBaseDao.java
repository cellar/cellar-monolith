/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.database.dao.spring
 *             FILE : CmrSpringBaseDao.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 9 August 2011
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */

package eu.europa.ec.opoce.cellar.cmr.database.dao.spring;

import eu.europa.ec.opoce.cellar.cmr.database.DaoObject;
import eu.europa.ec.opoce.cellar.cmr.database.dao.id.DatabaseIdService;

import java.io.Serializable;
import java.util.Collection;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * 
 * <p>Abstract CmrSpringBaseDao class.</p>
 */
public abstract class CmrSpringBaseDao<ENTITY extends DaoObject, ID extends Serializable> extends SpringBaseDao<ENTITY, ID> {

    /**
     * <p>setSpringDatabaseIdService.</p>
     *
     * @param databaseIdService a {@link eu.europa.ec.opoce.cellar.cmr.database.dao.id.DatabaseIdService} object.
     */
    @Autowired(required = true)
    @Qualifier(value = "cmrOracleSequence")
    public void setSpringDatabaseIdService(DatabaseIdService databaseIdService) {
        super.setDatabaseIdService(databaseIdService);
    }

    /**
     * <p>setSpringDataSource.</p>
     *
     * @param dataSource a {@link javax.sql.DataSource} object.
     */
    @Autowired(required = true)
    @Qualifier("cmrDataSource")
    public void setSpringDataSource(DataSource dataSource) {
        super.setDataSource(dataSource);
    }

    /**
     * <p>Constructor for CmrSpringBaseDao.</p>
     *
     * @param tablename   a {@link java.lang.String} object.
     * @param columnNames a {@link java.util.Collection} object.
     */
    protected CmrSpringBaseDao(String tablename, Collection<String> columnNames) {
        super(tablename, columnNames);
    }

    /**
     * <p>Constructor for CmrSpringBaseDao.</p>
     *
     * @param tablename    a {@link java.lang.String} object.
     * @param idColumnName a {@link java.lang.String} object.
     * @param columnNames  a {@link java.util.Collection} object.
     */
    protected CmrSpringBaseDao(String tablename, String idColumnName, Collection<String> columnNames) {
        super(tablename, idColumnName, columnNames);
    }
}
