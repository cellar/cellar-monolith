package eu.europa.ec.opoce.cellar.nal.utils;

import org.apache.jena.vocabulary.RDF;

import java.util.Collection;

import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.at_code;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.at_iso2code;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.at_iso3code;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.at_mappedCode;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.cmr_facet;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.cmr_facet_D;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.cmr_facet_M;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.cmr_facet_T;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.cmr_fallback;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.cmr_lang;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.cmr_prefLabel;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.dc_identifier;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.eu_domain;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.eu_language;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.eu_supportedLanguage;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.skos_altLabel;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.skos_broader;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.skos_broaderTransitive;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.skos_hasTopConcept;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.skos_hiddenLabel;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.skos_inScheme;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.skos_notation;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.skos_prefLabel;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.skos_topConceptOf;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarType.at_Language;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarType.eu_Domain;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarType.eu_MicroThesaurus;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarType.skos_Concept;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarType.skos_ConceptScheme;

public final class NalSparqlQueryTemplate {

    private NalSparqlQueryTemplate() {
    }

    /**
     * <p>conceptQuery.</p>
     *
     * @param conceptSchemeUri a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String conceptQuery(final String conceptSchemeUri) {
        return "SELECT ?c " + "WHERE { " + "?c <" + RDF.type.getURI() +
                "> <" + skos_Concept + ">. " + "?c <" + skos_inScheme + "> <" +
                conceptSchemeUri + "> }";
    }

    /**
     * <p>eurovocMicrothesauriQuery.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public static String eurovocMicrothesauriQuery() {
        return "SELECT ?m " + "WHERE { " + "?m <" + RDF.type.getURI() +
                "> <" + eu_MicroThesaurus + ">. }";
    }

    /**
     * <p>eurovocDomainQuery.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public static String eurovocDomainQuery() {
        return "SELECT ?d " + "WHERE { " + "?d <" + RDF.type.getURI() +
                "> <" + eu_Domain + ">. }";
    }

    /**
     * <p>broaderTransitivesQuery.</p>
     *
     * @param conceptSchemeUri a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String broaderTransitivesQuery(final String conceptSchemeUri) {
        return "SELECT DISTINCT ?c ?b " + "WHERE { " + "?c <" +
                RDF.type.getURI() + "> <" + skos_Concept + ">. " + "?c <" + skos_inScheme +
                "> <" + conceptSchemeUri + ">. " + "?c <" + skos_broaderTransitive + "> ?b. " +
                "?b <" + skos_inScheme + "> <" + conceptSchemeUri + ">. }";
    }

    /**
     * <p>conceptFacetQuery.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public static String conceptFacetQuery() {
        return "SELECT DISTINCT ?c ?f " + "WHERE { " + "?c <" + cmr_facet +
                "> ?f. }";
    }

    /**
     * <p>constructTopConceptOfRelationTo.</p>
     *
     * @param conceptScheme a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String constructTopConceptOfRelationTo(final String conceptScheme) {
        return "CONSTRUCT { " + "?c <" + skos_topConceptOf + "> <" +
                conceptScheme + ">. " + "<" + conceptScheme + "> <" + skos_hasTopConcept +
                "> ?c. } " + "WHERE { " + "?c <" + RDF.type.getURI() + "> <" + skos_Concept +
                ">. " + "?c <" + skos_inScheme + "> <" + conceptScheme + ">. " +
                "OPTIONAL {?c <" + skos_broader + "> ?b. }. " + "OPTIONAL {?c <" + skos_topConceptOf +
                "> ?cs. FILTER(?cs = <" + conceptScheme + ">). }. " + "FILTER (!bound(?b) && !bound(?cs)). " +
                " }";
    }

    /**
     * <p>constructDecodingSnippetWithoutFallback.</p>
     *
     * @param resource                 a {@link java.lang.String} object.
     * @param customDecodingProperties an array of {@link java.lang.String} objects.
     * @return a {@link java.lang.String} object.
     */
    public static String constructDecodingSnippetWithoutFallback(final String resource, final String[] customDecodingProperties) {
        final StringBuilder builder = new StringBuilder("CONSTRUCT { ").append("<").append(resource).append("> <").append(skos_prefLabel)
                .append("> ?preflabel. ").append("<").append(resource).append("> <").append(skos_altLabel).append("> ?altlabel. ");
        for (int i = 0; i < customDecodingProperties.length; i++) {
            final String o = "o" + (i + 1);
            builder.append("<").append(resource).append("> <").append(customDecodingProperties[i]).append("> ?").append(o).append(". ");
        }

        builder.append("} WHERE { ").append("{<").append(resource).append("> <").append(skos_prefLabel)
                .append("> ?preflabel. FILTER(!(str(?preflabel) = '')). } ").append("UNION {<").append(resource).append("> <")
                .append(skos_altLabel).append("> ?altlabel. FILTER(!(str(?altlabel) = '')). } ");
        for (int i = 0; i < customDecodingProperties.length; i++) {
            final String o = "o" + (i + 1);
            builder.append("UNION {<").append(resource).append("> <").append(customDecodingProperties[i]).append("> ?").append(o)
                    .append(" } ");
        }
        builder.append(" }");

        return builder.toString();
    }

    /**
     * <p>constructFallbackSnippet.</p>
     *
     * @param concept a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String constructFallbackSnippet(final String concept) {
        return "CONSTRUCT { <" + concept + "> <" + cmr_fallback +
                "> ?x. ?x <" + cmr_prefLabel + "> ?label. ?x <" + cmr_lang + "> ?lang } " +
                "WHERE { <" + concept + "> <" + cmr_fallback + "> ?x. ?x <" + cmr_prefLabel +
                "> ?label. ?x <" + cmr_lang + "> ?lang }";
    }

    /**
     * <p>askForTypeConceptScheme.</p>
     *
     * @param conceptSchemeUri a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String askForTypeConceptScheme(String conceptSchemeUri) {
        return "ASK {<" + conceptSchemeUri + "> <" + RDF.type.getURI() + "> <" + skos_ConceptScheme + "> }";
    }

    /**
     * <p>askForTypeDomain.</p>
     *
     * @param domainUri a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String askForTypeDomain(String domainUri) {
        return "ASK {<" + domainUri + "> <" + RDF.type.getURI() + "> <" + eu_Domain + "> }";
    }

    /**
     * <p>askForTypeConcept.</p>
     *
     * @param conceptUri a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String askForTypeConcept(String conceptUri) {
        return "ASK {<" + conceptUri + "> <" + RDF.type.getURI() + "> <" + skos_Concept + "> }";
    }

    /**
     * <p>selectSupportedLanguages.</p>
     *
     * @param conceptScheme a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String selectSupportedLanguages(String conceptScheme) {
        return "SELECT DISTINCT ?code " + "WHERE { " +
                "<" + conceptScheme + "> <" + eu_supportedLanguage + "> ?language. " +
                "?language <" + eu_language + "> ?code. " +
                " }";
    }

    /**
     * <p>selectConceptSchemes.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public static String selectConceptSchemes() {
        return "SELECT DISTINCT ?cs " + "WHERE { " +
                "?cs <" + RDF.type.getURI() + "> <" + skos_ConceptScheme + ">. " +
                " }";
    }

    /**
     * <p>selectConceptSchemesOneQuery.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public static String selectConceptSchemesOneQuery() {
        return "SELECT DISTINCT ?cs ?identifier ?label " + "WHERE { " +
                "?cs <" + RDF.type.getURI() + "> <" + skos_ConceptScheme + ">. " +
                "OPTIONAL {?cs <" + dc_identifier + "> ?identifier.} " +
                "OPTIONAL {?cs <" + skos_prefLabel + "> ?label.} " +
                " }";
    }

    /**
     * <p>selectConceptScheme.</p>
     *
     * @param conceptScheme a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String selectConceptScheme(String conceptScheme) {
        return selectPrefLabelsFor(conceptScheme, skos_ConceptScheme);
    }

    /**
     * <p>selectDomains.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public static String selectDomains() {
        return "SELECT DISTINCT ?domain " + "WHERE { " +
                "?domain <" + RDF.type.getURI() + "> <" + eu_Domain + ">. " +
                " }";
    }

    /**
     * <p>selectDomainsOneQuery.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public static String selectDomainsOneQuery() {
        return "SELECT DISTINCT ?domain ?identifier ?cs ?label " + "WHERE { " +
                "?domain <" + RDF.type.getURI() + "> <" + eu_Domain + ">. " +
                "OPTIONAL {?domain <" + dc_identifier + "> ?identifier.} " +
                "OPTIONAL {?domain <" + skos_prefLabel + "> ?label.} " +
                "OPTIONAL {?cs <" + eu_domain + "> ?domain.} " +
                " }";
    }

    /**
     * <p>selectDomain.</p>
     *
     * @param domain a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String selectDomain(String domain) {
        return "SELECT DISTINCT ?identifier ?cs ?label " + "WHERE { " +
                "<" + domain + "> <" + RDF.type.getURI() + "> <" + eu_Domain + ">. " +
                "OPTIONAL {<" + domain + "> <" + dc_identifier + "> ?identifier.} " +
                "OPTIONAL {?cs <" + eu_domain + "> <" + domain + ">.} " +
                "OPTIONAL {<" + domain + "> <" + skos_prefLabel + "> ?label.} " +
                " }";
    }

    /**
     * <p>selectConcept.</p>
     *
     * @param concept  a {@link java.lang.String} object.
     * @param language a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String selectConcept(String concept, String language) {
        return "SELECT DISTINCT ?identifier ?preflabel ?altlabel ?hiddenlabel ?notation " + "WHERE { " +
                "<" + concept + "> <" + RDF.type.getURI() + "> <" + skos_Concept + ">. " +
                "OPTIONAL {<" + concept + "> <" + dc_identifier + "> ?identifier.} " +
                "OPTIONAL {<" + concept + "> <" + skos_notation + "> ?notation.} " +
                "OPTIONAL {<" + concept + "> <" + skos_prefLabel +
                "> ?preflabel. FILTER (lang(?preflabel) = '" + language + "'). } " +
                "OPTIONAL {<" + concept + "> <" + skos_altLabel + "> ?altlabel. FILTER (lang(?altlabel) = '" +
                language + "'). } " +
                "OPTIONAL {<" + concept + "> <" + skos_hiddenLabel +
                "> ?hiddenlabel. FILTER (lang(?hiddenlabel) = '" + language + "'). } " +
                " }";
    }

    /**
     * <p>selectConcept.</p>
     *
     * @param concept     a {@link java.lang.String} object.
     * @param languageOne a {@link java.lang.String} object.
     * @param languageTwo a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String selectConcept(String concept, String languageOne, String languageTwo) {
        return "SELECT DISTINCT ?identifier ?preflabel ?altlabel ?hiddenlabel ?notation " + "WHERE { " +
                "<" + concept + "> <" + RDF.type.getURI() + "> <" + skos_Concept + ">. " +
                "OPTIONAL {<" + concept + "> <" + dc_identifier + "> ?identifier.} " +
                "OPTIONAL {<" + concept + "> <" + skos_notation + "> ?notation.} " +
                "OPTIONAL {<" + concept + "> <" + skos_prefLabel +
                "> ?preflabel. FILTER (lang(?preflabel) = '" + languageOne + "' || lang(?preflabel) = '" +
                languageTwo + "'). } " +
                "OPTIONAL {<" + concept + "> <" + skos_altLabel + "> ?altlabel. FILTER (lang(?altlabel) = '" +
                languageOne + "' || lang(?altlabel) = '" + languageTwo + "'). } " +
                "OPTIONAL {<" + concept + "> <" + skos_hiddenLabel +
                "> ?hiddenlabel. FILTER (lang(?hiddenlabel) = '" + languageOne + "' || lang(?hiddenlabel) = '" +
                languageTwo + "'). } " +
                " }";
    }

    /**
     * <p>selectRelatedConcepts.</p>
     *
     * @param resource a {@link java.lang.String} object.
     * @param relation a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String selectRelatedConcepts(String resource, String relation) {
        return "SELECT DISTINCT ?concept " + "WHERE { " +
                "<" + resource + "> <" + relation + "> ?concept. " +
                "?concept <" + RDF.type.getURI() + "> <" + skos_Concept + ">. " +
                " }";
    }

    /**
     * <p>selectRelatedConceptsOneQuery.</p>
     *
     * @param resource a {@link java.lang.String} object.
     * @param relation a {@link java.lang.String} object.
     * @param language a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String selectRelatedConceptsOneQuery(String resource, String relation, String language) {
        return "SELECT DISTINCT ?concept ?identifier ?preflabel ?altlabel ?hiddenlabel ?notation " + "WHERE { " +
                "<" + resource + "> <" + relation + "> ?concept. " +
                "?concept <" + RDF.type.getURI() + "> <" + skos_Concept + ">. " +
                "OPTIONAL {?concept <" + dc_identifier + "> ?identifier.} " +
                "OPTIONAL {?concept <" + skos_notation + "> ?notation.} " +
                "OPTIONAL {?concept <" + skos_prefLabel + "> ?preflabel. FILTER (lang(?preflabel) = '" + language +
                "'). } " +
                "OPTIONAL {?concept <" + skos_altLabel + "> ?altlabel. FILTER (lang(?altlabel) = '" + language +
                "'). } " +
                "OPTIONAL {?concept <" + skos_hiddenLabel + "> ?hiddenlabel. FILTER (lang(?hiddenlabel) = '" +
                language + "'). } " +
                " }";
    }

    /**
     * <p>selectRelatedConceptsOneQuery.</p>
     *
     * @param resource    a {@link java.lang.String} object.
     * @param relation    a {@link java.lang.String} object.
     * @param languageOne a {@link java.lang.String} object.
     * @param languageTwo a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String selectRelatedConceptsOneQuery(String resource, String relation, String languageOne, String languageTwo) {
        return "SELECT DISTINCT ?concept ?identifier ?preflabel ?altlabel ?hiddenlabel ?notation " + "WHERE { " +
                "<" + resource + "> <" + relation + "> ?concept. " +
                "?concept <" + RDF.type.getURI() + "> <" + skos_Concept + ">. " +
                "OPTIONAL {?concept <" + dc_identifier + "> ?identifier.} " +
                "OPTIONAL {?concept <" + skos_notation + "> ?notation.} " +
                "OPTIONAL {?concept <" + skos_prefLabel + "> ?preflabel. FILTER (lang(?preflabel) = '" +
                languageOne + "' || lang(?preflabel) = '" + languageTwo + "'). } " +
                "OPTIONAL {?concept <" + skos_altLabel + "> ?altlabel. FILTER (lang(?altlabel) = '" + languageOne +
                "' || lang(?altlabel) = '" + languageTwo + "'). } " +
                "OPTIONAL {?concept <" + skos_hiddenLabel + "> ?hiddenlabel. FILTER (lang(?hiddenlabel) = '" +
                languageOne + "' || lang(?hiddenlabel) = '" + languageTwo + "'). } " +
                " }";
    }

    /**
     * <p>selectPrefLabelsFor.</p>
     *
     * @param resourceUri a {@link java.lang.String} object.
     * @param type        a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String selectPrefLabelsFor(String resourceUri, String type) {
        return "SELECT DISTINCT ?label " + "WHERE { " +
                "<" + resourceUri + "> <" + RDF.type.getURI() + "> <" + type + ">. " +
                "<" + resourceUri + "> <" + skos_prefLabel + "> ?label }";
    }

    /**
     * <p>selectFallbackLabelsFor.</p>
     *
     * @param concepts a {@link java.util.Collection} object.
     * @param langOne  a {@link java.lang.String} object.
     * @param langTwo  a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String selectFallbackLabelsFor(Collection<String> concepts, String langOne, String langTwo) {
        StringBuilder filter = new StringBuilder("FILTER (");
        boolean first = true;
        for (String concept : concepts) {
            if (first) {
                first = false;
                filter.append("?concept = <").append(concept).append(">");
            } else {
                filter.append(" || ?concept = <").append(concept).append(">");
            }
        }
        filter.append("). ");

        return "SELECT DISTINCT ?concept ?flabel ?flanguage " + "WHERE { " +
                "{ ?concept <" + cmr_fallback + "> ?fallback. " + filter.toString() + " } " +
                "?fallback <" + cmr_prefLabel + "> ?flabel. FILTER (lang(?flabel) = '" + langOne +
                "' || lang(?flabel) = '" + langTwo + "'). " +
                "?fallback <" + cmr_lang + "> ?flanguage. " +
                " }";
    }

    /**
     * <p>selectFallbackLabelFor.</p>
     *
     * @param concept a {@link java.lang.String} object.
     * @param langOne a {@link java.lang.String} object.
     * @param langTwo a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String selectFallbackLabelFor(String concept, String langOne, String langTwo) {
        return "SELECT DISTINCT ?flabel ?flanguage " + "WHERE { " +
                "<" + concept + "> <" + cmr_fallback + "> ?fallback. " +
                "?fallback <" + cmr_prefLabel + "> ?flabel. FILTER (lang(?flabel) = '" + langOne +
                "' || lang(?flabel) = '" + langTwo + "'). " +
                "?fallback <" + cmr_lang + "> ?flanguage. " +
                " }";
    }

    public static String selectLanguageQuery(final String languageUri) {
        return "SELECT DISTINCT ?code ?mappedCode ?iso2Code ?iso3Code " + "WHERE { " +
                "<" + languageUri + "> <" + RDF.type.getURI() + "> <" + at_Language + ">. " +
                "OPTIONAL {<" + languageUri + "> <" + at_code + "> ?code.} " +
                "OPTIONAL {<" + languageUri + "> <" + at_mappedCode + "> ?mappedCode.} " +
                "OPTIONAL {<" + languageUri + "> <" + at_iso2code + "> ?iso2Code.} " +
                "OPTIONAL {<" + languageUri + "> <" + at_iso3code + "> ?iso3Code.} " +
                " }";
    }

    public static String constructFacetsForTopConcepts() {
        return "CONSTRUCT { " + "?c <" + cmr_facet_T + "> ?c. " + "?c <" +
                cmr_facet + "> ?c. " + "?c <" + cmr_facet_M + "> ?m. " + "?c <" + cmr_facet +
                "> ?m. " + "?c <" + cmr_facet_D + "> ?d. " + "?c <" + cmr_facet + "> ?d. } " +
                "WHERE { " + "?c <" + skos_topConceptOf + "> ?m. " + "?m <" + RDF.type.getURI() +
                "> <" + eu_MicroThesaurus + ">. " + "OPTIONAL { ?m <" + eu_domain + "> ?d. } " +
                " }";

    }

    public static String constructFacets() {
        return "CONSTRUCT { " + "?c <" + cmr_facet_T + "> ?f. " + "?c <" +
                cmr_facet + "> ?f. " + "?c <" + cmr_facet_M + "> ?m. " + "?c <" + cmr_facet +
                "> ?m. " + "?c <" + cmr_facet_D + "> ?d. " + "?c <" + cmr_facet + "> ?d. } " +
                "WHERE { " + "?c <" + skos_broaderTransitive + "> ?f. " + "?f <" + skos_topConceptOf +
                "> ?m. " + "?m <" + RDF.type.getURI() + "> <" + eu_MicroThesaurus + ">. " +
                "OPTIONAL { ?m <" + eu_domain + "> ?d. } " + " }";

    }

    public static String constructInSchemeRelationTo(String conceptScheme) {
        return "CONSTRUCT { " + "?c <" + skos_inScheme + "> <" +
                conceptScheme + "> } " + "WHERE { " + "?c <" + RDF.type.getURI() + "> <" +
                skos_Concept + ">. " + "OPTIONAL {?c <" + skos_inScheme + "> ?cs. FILTER(?cs = <" +
                conceptScheme + ">). }. " + "FILTER (!bound(?cs)). " + " }";

    }
}
