package eu.europa.ec.opoce.cellar.s3.domain;

public enum ContentType {

    DIRECT("direct.nt"),
    DIRECT_INFERRED("direct-inferred.nt"),
    TECHNICAL("technical.nt"),
    SAME_AS("sameas.nt"),
    INVERSE("inverse.nt"),
    EMBEDDED_NOTICE("embedded-notice.nt"),
    SKOS("skos.rdf");

    private final String file;

    ContentType(String file) {
        this.file = file;
    }

    public String file() {
        return file;
    }

}
