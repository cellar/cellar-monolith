/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.modelload.service.impl
 *             FILE : ModelLoadRequestSchedulerImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Mar 18, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.virtuosobackup.service.impl;

import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.ExecutionStatus;
import eu.europa.ec.opoce.cellar.cl.concurrency.Concurrent;
import eu.europa.ec.opoce.cellar.cl.concurrency.IConcurrentArgsResolver;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.database.dao.SparqlLoadRequestRepository;
import eu.europa.ec.opoce.cellar.cmr.database.dao.VirtuosoBackupDao;
import eu.europa.ec.opoce.cellar.cmr.database.util.JdbcUtils;
import eu.europa.ec.opoce.cellar.cmr.model.ModelUtils;
import eu.europa.ec.opoce.cellar.common.util.CellarIdUtils;
import eu.europa.ec.opoce.cellar.common.util.StringUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.domain.content.mets.OffIngestionOperationType;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.VirtuosoBackupException;
import eu.europa.ec.opoce.cellar.exception.VirtuosoOperationException;
import eu.europa.ec.opoce.cellar.semantic.helper.MessageFormatter;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.sparql.SparqlLoadRequest;
import eu.europa.ec.opoce.cellar.virtuoso.VirtuosoService;
import eu.europa.ec.opoce.cellar.virtuoso.retry.annotation.VirtuosoRetry;
import eu.europa.ec.opoce.cellar.virtuoso.util.PrivateGroupOperation;
import eu.europa.ec.opoce.cellar.virtuoso.util.VirtuosoNormalizer;
import eu.europa.ec.opoce.cellar.virtuoso.util.VirtuosoSkolemizer;
import eu.europa.ec.opoce.cellar.virtuoso.util.VirtuosoUtils;
import eu.europa.ec.opoce.cellar.virtuosobackup.BackupType;
import eu.europa.ec.opoce.cellar.virtuosobackup.VirtuosoBackup;
import eu.europa.ec.opoce.cellar.virtuosobackup.VirtuosoBackup.OperationType;
import eu.europa.ec.opoce.cellar.virtuosobackup.service.IVirtuosoBackupService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.Model;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import virtuoso.jena.driver.VirtDataset;

import javax.sql.DataSource;
import java.util.*;
import java.util.stream.Collectors;

import static eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration.VIRTUOSO;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Mar 18, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service("virtuosoBackupService")
public class VirtuosoBackupServiceImpl implements IVirtuosoBackupService, IConcurrentArgsResolver {

    /**
     * The Constant LOG.
     */
    private static final Logger LOG = LogManager.getLogger(VirtuosoBackupServiceImpl.class);

    @Autowired
    private CellarResourceDao resourceDao;
    @Autowired
    @Qualifier("pidManagerService")
    private PidManagerService pidManagerService;

    /**
     * The virtuoso backup dao.
     */
    @Autowired
    private VirtuosoBackupDao virtuosoBackupDao;

    @Autowired
    private SparqlLoadRequestRepository sparqlLoadRequestRepository;

    /**
     * The virtuoso data source.
     */
    @Qualifier("virtuosoDataSource")
    @Autowired
    private DataSource virtuosoDataSource;

    /**
     * The cellar configuration.
     */
    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    /**
     * The virtuoso service.
     */
    @Autowired
    private VirtuosoService virtuosoService;

    /**
     * The identifier service.
     */
    @Autowired
    @Qualifier("pidManagerService")
    private IdentifierService identifierService;

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<VirtuosoBackup> findResyncablesSortedByInsertionDateAsc(final int limit) {
        return this.virtuosoBackupDao.findAllSortedByInsertionDateAsc(limit, true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public long countAllResyncable() {
        return this.virtuosoBackupDao.countAllResyncable();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public VirtuosoBackup findLastBackup(final String objectId) {
        return this.virtuosoBackupDao.findLastBackup(objectId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void populateVirtuosoBackup(final String objectId, final BackupType backupType, final String model,
                                       final Collection<String> productionIds, final Date insertionDate, final OperationType operationType, final boolean isResyncable,
                                       final String reason) throws VirtuosoBackupException {
        // retrieve the last backup with same objectId
        final VirtuosoBackup lastBackup = this.findLastBackup(objectId);

        // if there is already an object with the same objectId, then the resyncable status passed in the signature of the method is ignored
        // and the resyncable status of the last existing object is used
        boolean myIsResyncable = isResyncable;
        if (lastBackup != null) {
            myIsResyncable = lastBackup.isResyncable();
        }

        // backs up
        this.internalPopulateVirtuosoBackup(objectId, backupType, model, productionIds, insertionDate, operationType, myIsResyncable,
                reason);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void populateVirtuosoBackup(final String ontoUri, final BackupType backupType, final String model, final String skosModel,
                                       final String ruleSetQuery, final Date insertionDate, final OperationType operationType, final boolean isResyncable,
                                       final String reason) throws VirtuosoBackupException {
        //retrieve the last backup with the same URI
        final VirtuosoBackup lastBackup = this.findLastBackup(ontoUri);

        boolean shouldResync = isResyncable;
        if (lastBackup != null) {
            shouldResync = lastBackup.isResyncable();
        }

        this.internalPopulateVirtuosoBackup(ontoUri, backupType, model, skosModel, ruleSetQuery, insertionDate, operationType, shouldResync,
                reason);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public boolean populateVirtuosoBackupIfAlreadyBackedup(final String cellarId, final BackupType backupType, final String model,
                                                           final Collection<String> productionIds, final Date insertionDate, final OperationType operationType)
            throws VirtuosoBackupException {
        if (LOG.isDebugEnabled(VIRTUOSO)) {
            LOG.debug(VIRTUOSO, "Backup {} (type={}), productionIds={}, model size={}, operation type={}",
                    cellarId, backupType, JenaUtils.read(model).size(), operationType);
        }

        // retrieve the last backup with same objectId
        final VirtuosoBackup myLastBackup = this.findLastBackup(cellarId);
        if (myLastBackup == null) {
            LOG.debug(VIRTUOSO, "Last backup not found (cellarId={})", cellarId);
            return false;
        }

        // backs up and return
        final String message = MessageFormatter.format(
                "This object has been queued on the backup table for ordering reasons: an object with the same objectId '{}' and insertion date '{}' was already present.",
                myLastBackup.getObjectId(), myLastBackup.getInsertionDate());
        this.internalPopulateVirtuosoBackup(cellarId, backupType, model, productionIds, insertionDate, operationType,
                myLastBackup.isResyncable(), message);
        return true;
    }

    /**
     * Internal populate virtuoso backup.
     *
     * @param cellarId      the cellar id
     * @param backupType    the backup type
     * @param model         the model
     * @param productionIds the production ids
     * @param insertionDate the insertion date
     * @param operationType the operation type
     * @param isResyncable  the is resyncable
     * @param reason        the reason
     * @throws VirtuosoBackupException the virtuoso backup exception
     */
    private void internalPopulateVirtuosoBackup(final String cellarId, final BackupType backupType, final String model,
                                                final Collection<String> productionIds, final Date insertionDate, final OperationType operationType, final boolean isResyncable,
                                                final String reason) throws VirtuosoBackupException {
        try {

            Collection<String> productionIdsPrefixed = getProductionIdsPrefixed(productionIds);
            // prepare the object of backup
            final VirtuosoBackup virtuosoBackup = new VirtuosoBackup(cellarId, backupType, model, productionIdsPrefixed, insertionDate,
                    operationType, isResyncable, StringUtils.abbreviateLeft(reason, 1024));

            // backs up
            LOG.warn("Backing up the digital objet with id '{}'...", cellarId);
            this.virtuosoBackupDao.saveObject(virtuosoBackup);
            LOG.warn("The digital objet with id '{}' has been successfully backed up.", cellarId);
        } catch (final Exception e) {
            throw ExceptionBuilder.get(VirtuosoBackupException.class).withCode(CmrErrors.VIRTUOSO_BACKUP_FAILED)
                    .withMessage(
                            "Unexpected exception thrown while creating the Virtuoso backup with cellar id '{}' and insertion date '{}'")
                    .withMessageArgs(cellarId, insertionDate).withCause(e).build();
        }
    }

    private Collection<String> getProductionIdsPrefixed(Collection<String> productionIds) {
        //Getting only the suffix of the uri to save on VIRTUOSO_BACKUP.PRODUCTION_ID to mactch the size of the column. See CLLR10815-483.
        Collection<String> productionIdsPrefixed = productionIds;
        if (CollectionUtils.isNotEmpty(productionIds)) {
            productionIdsPrefixed = productionIds.stream().map(productionId ->
                    identifierService.getPrefixedFromUri(productionId)).collect(Collectors.toList());
        }
        return productionIdsPrefixed;
    }

    /**
     * Internal populate virtuoso backup, defined similarly as the method defined for digital objects.
     *
     * @param uri           the URI of the ontology to backup
     * @param backupType    the backup type
     * @param model         the ontology model, stringified
     * @param skosModel     the skos model
     * @param ruleSetQuery  the rule set query
     * @param insertionDate the date at which the backup request was made
     * @param operationType the operation type
     * @param isResyncable  specifies whether Virtuoso is available or not
     * @param reason        the initial reason why the Virtuoso insertion failed
     */
    private void internalPopulateVirtuosoBackup(final String uri, final BackupType backupType, final String model, final String skosModel,
                                                final String ruleSetQuery, final Date insertionDate, final OperationType operationType, final boolean isResyncable,
                                                final String reason) {
        try {
            final VirtuosoBackup virtuosoBackup = new VirtuosoBackup(uri, backupType, model, skosModel, ruleSetQuery, insertionDate,
                    operationType, isResyncable, StringUtils.abbreviateLeft(reason, 1024));

            LOG.warn("Backing up the ontology with URI '{}'...", uri);
            this.virtuosoBackupDao.saveObject(virtuosoBackup);
            LOG.warn("The ontology with URI '{}' has been successfully backed up.", uri);
        } catch (final Exception e) {
            throw ExceptionBuilder.get(VirtuosoBackupException.class).withCode(CmrErrors.VIRTUOSO_BACKUP_FAILED)
                    .withMessage(
                            "Unexpected exception thrown while creating the Virtuoso backup with ontology URI '{}' and insertion date '{}'")
                    .withMessageArgs(uri, insertionDate).withCause(e).build();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void delete(final VirtuosoBackup virtuosoBackup) throws VirtuosoBackupException {
        try {
            this.virtuosoBackupDao.deleteObject(virtuosoBackup);
        } catch (final Exception e) {
            throw ExceptionBuilder.get(VirtuosoBackupException.class).withCode(CmrErrors.VIRTUOSO_BACKUP_FAILED)
                    .withMessage(
                            "Unexpected exception thrown while deleting the Virtuoso backup with cellar id '{}' and insertion date '{}'")
                    .withMessageArgs(virtuosoBackup.getObjectId(), virtuosoBackup.getInsertionDate()).withCause(e).build();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Concurrent(locker = OffIngestionOperationType.VIRTUOSO_BACKUP)
    @VirtuosoRetry
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void processVirtuosoBackup(final VirtuosoBackup virtuosoBackup, boolean hasRetryAttempts) throws VirtuosoOperationException {
        try {
            switch (virtuosoBackup.getBackupType()) {
                case DIGITAL_OBJECT:
                    processDOVirtuosoBackup(virtuosoBackup);
                    return;
                case ONTOLOGY:
                case NAL:
                case SPARQL_LOAD:
                    processOntologyVirtuosoBackup(virtuosoBackup);
                    return;
                default:
            }
        } catch (final Exception e) {
            virtuosoService.triggerVirtuosoRetry(e, hasRetryAttempts);
            throw e; //after exceeding retry attempts
        }
    }

    /**
     * Process do virtuoso backup.
     *
     * @param virtuosoBackup the virtuoso backup
     * @throws VirtuosoOperationException the virtuoso operation exception
     */
    private void processDOVirtuosoBackup(final VirtuosoBackup virtuosoBackup) throws VirtuosoOperationException {
        final long currentTimeMillis = System.currentTimeMillis();
        VirtDataset dataset = null;
        Model virtuosoBackupModel = null;
        final String cellarId = virtuosoBackup.getObjectId();
        try {
            // gets the connection to the dataset
            dataset = new VirtDataset(this.virtuosoDataSource);
            // write data in Virtuoso
            final OperationType operationType = virtuosoBackup.getOperationType();
            if (OperationType.INSERT.equals(operationType)) {
                virtuosoBackupModel = JenaUtils.read(virtuosoBackup.getModel());
                final String cellarUri = this.identifierService.getUri(cellarId);
                final Collection<String> pids = virtuosoBackup.getProductionIds();
                final Collection<String> pidUris = getProductionIdsUri(pids);

                final boolean isNormalizeItems = this.cellarConfiguration.isVirtuosoIngestionTechnicalEnabled() && this.pidManagerService.isManifestation(cellarId);
                final Map<String, Collection<String>> itemsCellarIdPidUrisMapping = new HashMap<>();
                if (isNormalizeItems) {
                	buildItemsCellarIdPidUrisMapping(cellarId, itemsCellarIdPidUrisMapping);
                }
  
                // Normalize and skolemize the backup model.
                VirtuosoNormalizer.normalizeModel(virtuosoBackupModel, cellarId, pidUris);
                if (isNormalizeItems) {
                	itemBasedModelNormalization(virtuosoBackupModel, itemsCellarIdPidUrisMapping);
                }
                final VirtuosoSkolemizer virtuosoSkolemizer = new VirtuosoSkolemizer();
                virtuosoBackupModel = virtuosoSkolemizer.skolemizeModel(virtuosoBackupModel, cellarUri);
                // Align existing Virtuoso metadata.
                if (this.cellarConfiguration.isVirtuosoIngestionLinkrotRealignmentEnabled()) {
                    this.virtuosoService.realignment(dataset, cellarId, pids);
                }
                // Merge the backup model in Virtuoso.
                VirtuosoUtils.updateModel(dataset, cellarUri, virtuosoBackupModel);
                // Normalize existing Virtuoso metadata.
                VirtuosoNormalizer.normalizeExistingMetadata(dataset, cellarId, pidUris);
                if (isNormalizeItems) {
                	itemBasedExistingMetadataNormalization(dataset, itemsCellarIdPidUrisMapping);
                }
                
                if (virtuosoBackup.isUnderEmbargo()) {
                    this.virtuosoService.hide(CellarIdUtils.getCellarIdBase(cellarId), virtuosoBackup.getEmbargoDate(), new Date(), true);
                }
            } else if (OperationType.DROP.equals(operationType)) {
                dropModels(dataset, cellarId);
            }
        } catch (final Exception e) {
            throw ExceptionBuilder.get(VirtuosoOperationException.class).withCode(CmrErrors.VIRTUOSO_OPERATION_FAILED).withMessage("Unexpected exception thrown while processing the Virtuoso backup with cellar id '{}' and insertion date '{}'").withMessageArgs(cellarId, virtuosoBackup.getInsertionDate()).withCause(e).build();
        } finally {
            ModelUtils.closeQuietly(virtuosoBackupModel);
            VirtuosoUtils.close(dataset);

            final long delta = System.currentTimeMillis() - currentTimeMillis;
            LOG.info("Ingestion of backup in Virtuoso finished after {} ms.", delta);
        }
    }

    private void buildItemsCellarIdPidUrisMapping(final String manifestationCellarId, final Map<String, Collection<String>> itemsCellarIdPidUrisMapping) {
    	Collection<CellarResource> items = this.resourceDao.findResourceStartWith(manifestationCellarId, DigitalObjectType.ITEM);
        for (CellarResource item : items) {
            Identifier itemCellarIdentifier = this.identifierService.getIdentifier(item.getCellarId());
            Collection<String> itemPidUris = getProductionIdsUri(itemCellarIdentifier.getPids());
            itemsCellarIdPidUrisMapping.put(item.getCellarId(), itemPidUris);
        }
    }
    
    private Collection<String> getProductionIdsUri(Collection<String> productionIds) {
        //Getting back the uri from the suffix stored on VIRTUOSO_BACKUP.PRODUCTION_ID. See CLLR10815-483.
        if (CollectionUtils.isNotEmpty(productionIds)) {
            return productionIds.stream().map(productionId -> identifierService.getUri(productionId)).collect(Collectors.toList());
        }
        return productionIds;
    }

    private void itemBasedModelNormalization(final Model virtuosoBackupModel, final Map<String, Collection<String>> itemsCellarIdPidUrisMapping) {
    	for (Map.Entry<String, Collection<String>> entry : itemsCellarIdPidUrisMapping.entrySet()) {
    		VirtuosoNormalizer.normalizeModel(virtuosoBackupModel, entry.getKey(), entry.getValue());
    	}
    }
    
    private void itemBasedExistingMetadataNormalization(final VirtDataset dataset, final Map<String, Collection<String>> itemsCellarIdPidUrisMapping) {
    	for (Map.Entry<String, Collection<String>> entry : itemsCellarIdPidUrisMapping.entrySet()) {
    		VirtuosoNormalizer.normalizeExistingMetadata(dataset, entry.getKey(), entry.getValue());
    	}
    }
    
    /**
     * Similar to what is done for digital objects, but simplified:
     * - No link rot processing;
     * - No normalization;
     * - No embargoes.
     *
     * @param virtuosoBackup the virtuoso backup
     */
    private void processOntologyVirtuosoBackup(final VirtuosoBackup virtuosoBackup) {
        VirtDataset dataset = null;
        Model virtuosoBackupOntologyModel = null;
        Model virtuosoBackupSkosModel = null;
        final String uri = virtuosoBackup.getObjectId();
        try {
            dataset = new VirtDataset(this.virtuosoDataSource);

            // write data in Virtuoso
            final OperationType operationType = virtuosoBackup.getOperationType();

            if (OperationType.INSERT.equals(operationType)) {
                virtuosoBackupOntologyModel = JenaUtils.read(virtuosoBackup.getModel());
                //update the model
                VirtuosoUtils.updateModel(dataset, uri, virtuosoBackupOntologyModel);
                //update the skos model, if it was provided
                if (null != virtuosoBackup.getSkosModel()) {
                    virtuosoBackupSkosModel = JenaUtils.read(virtuosoBackup.getSkosModel());
                    VirtuosoUtils.updateModel(dataset, uri, virtuosoBackupSkosModel);
                }
                //define the rules set
                if (virtuosoBackup.getRuleSetQuery() != null) {
                    LOG.debug(VIRTUOSO, "Executing Virtuoso rule set query for backup {}: {}", virtuosoBackup.getId(), virtuosoBackup.getRuleSetQuery());
                    try {
                        JdbcUtils.execute(this.virtuosoDataSource, virtuosoBackup.getRuleSetQuery());
                    } catch (Exception e) {
                        LOG.error("Error executing query {}", virtuosoBackup.getRuleSetQuery());
                        throw e;
                    }
                }
                //Update SPARQL_LOAD_REQUEST table entry
                if (virtuosoBackup.getBackupType().equals(BackupType.SPARQL_LOAD)) {
                    List<SparqlLoadRequest> requests = sparqlLoadRequestRepository.findByUriAndExecutionStatus(virtuosoBackup.getObjectId(), ExecutionStatus.Failure);
                    for (SparqlLoadRequest request : requests) {
                        request.setExecutionStatus(ExecutionStatus.Success);
                        sparqlLoadRequestRepository.save(request);
                    }
                }
            } else if (OperationType.DROP.equals(operationType)) {
                dropModels(dataset, uri);
            }
        } catch (final Exception e) {
            throw ExceptionBuilder.get(VirtuosoOperationException.class).withCode(CmrErrors.VIRTUOSO_OPERATION_FAILED)
                    .withMessage("Unexpected exception thrown while processing the Virtuoso backup for URI '{}' and insertion date '{}'")
                    .withMessageArgs(uri, virtuosoBackup.getInsertionDate()).withCause(e).build();
        } finally {
            ModelUtils.closeQuietly(virtuosoBackupOntologyModel);
            ModelUtils.closeQuietly(virtuosoBackupSkosModel);

            VirtuosoUtils.close(dataset);
        }
    }

    /**
     * Drops the models.
     *
     * @param dataset    the dataset
     * @param identifier the identifier
     * @throws Exception the exception
     */
    private void dropModels(final Dataset dataset, final String identifier) throws Exception {
        VirtuosoUtils.dropModels(dataset, identifier);
        final List<String> cellarIds = new ArrayList<String>();
        cellarIds.add(identifier);
        VirtuosoUtils.group(cellarIds, PrivateGroupOperation.REMOVE, virtuosoDataSource);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object[] resolveArgsForConcurrency(final Object[] in) {
        Object[] lockArgs = new Object[]{};

        // resolve concurrency arguments only if concurrency is enabled for virtuoso backup
        if (this.cellarConfiguration.isVirtuosoIngestionBackupConcurrencyControllerEnabled()) {
            lockArgs = new Object[]{
                    ((VirtuosoBackup) in[0]).getObjectId()};
        }

        return lockArgs;
    }

}
