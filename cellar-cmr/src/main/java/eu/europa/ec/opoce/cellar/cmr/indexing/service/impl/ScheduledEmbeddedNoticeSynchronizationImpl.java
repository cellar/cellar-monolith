package eu.europa.ec.opoce.cellar.cmr.indexing.service.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cmr.IndexationCalc;
import eu.europa.ec.opoce.cellar.cmr.database.dao.IndexationCalcDao;
import eu.europa.ec.opoce.cellar.cmr.indexing.service.IScheduledEmbeddedNoticeSynchronization;
import eu.europa.ec.opoce.cellar.logging.LogContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.CMR_INDEXING;

@Service("scheduledEmbeddedNoticeSynchronization")
public class ScheduledEmbeddedNoticeSynchronizationImpl implements IScheduledEmbeddedNoticeSynchronization {

    private static final Logger LOG = LogManager.getLogger(ScheduledEmbeddedNoticeSynchronizationImpl.class);

    /**
     * The processor performing the embedded notice synchronization.
     */
    @Autowired
    private EmbeddedNoticeSynchronizationProcessor synchronizationProcessor;
    /**
     * CMR_INDEXATION_CALC_EMBEDDED_NOTICE DAO.
     */
    @Autowired
    private IndexationCalcDao indexationCalcDao;

	@Autowired
	private ICellarConfiguration cellarConfiguration;
    /**
     * Updates the embedded-notice-related columns of CMR_CELLAR_RESOURCE_MD
     * table according to the entries of the CMR_INDEXATION_CALC_EMBEDDED_NOTICE
     * table, which becomes populated when embedded-notice indexing requests
     * are generated.
     * Configured in scheduler-context.xml
     */
    @LogContext(CMR_INDEXING)
    @Override
    public void scheduleSynchronizationRequests() {
		if(cellarConfiguration.isCellarServiceIndexingEnabled()) {
			// Retrieve all embedded-notice-synchronization requests.
			List<IndexationCalc> indexationCalcs = indexationCalcDao.findAll();
			if (CollectionUtils.isNotEmpty(indexationCalcs)) {
				// Group the embedded-notice-synchronization requests into batches based on their
				// CELLAR_BASE_ID in order to synchronize all resources belonging
				// to the same hierarchy as a single unit.
				Map<String, List<IndexationCalc>> indexationCalcBatches = indexationCalcs.stream()
						.collect(Collectors.groupingBy(IndexationCalc::getCellarBaseId));
				LOG.info("Embedded notice synchronization process started.");
				for (Map.Entry<String, List<IndexationCalc>> batch : indexationCalcBatches.entrySet()) {
					try {
						// Perform synchronization of the embedded notices for all resources
						// belonging to the same hierarchy.
						this.synchronizationProcessor.execute(batch.getKey(), batch.getValue());
					} catch (Exception e) {
						LOG.error("An error occurred while attempting to synchronize embedded notice status in CMR_CELLAR_RESOURCE_MD "
								+ "for batch with Cellar-ID [{}].", batch.getKey(), e);
					}
				}
				LOG.info("Embedded notice synchronization process finished.");
			}
		}
    }
    
}
