/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.nal.rework
 *             FILE : NalOntologyInferenceService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 01 10, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-01-10 14:28:53 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.nal.service;

import org.apache.jena.rdf.model.Model;

/**
 * @author ARHS Developments
 */
public interface NalSemanticService {
    Model getAuthorityOntology();

    Model getEurovocOntology();
}
