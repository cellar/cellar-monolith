/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cmr.ingest.impl.update
 *        FILE : EligibleForDeleteSelector.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 10-04-2014
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.ingest.impl.update;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.SimpleSelector;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import eu.europa.ec.opoce.cellar.common.jena.CombinedSelector;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;

import java.util.Iterator;
import java.util.List;

/**
 * <class_description> Class for selecting out of a model the statements eligible for in-memory deletion.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 10-04-2014
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
class EligibleForDeleteSelector {

    private enum RoleOnTriple {
        SUBJECT, PREDICATE, OBJECT
    }

    private DigitalObject digitalObject;
    private Model inModel;
    private Model outModel;

    public EligibleForDeleteSelector(final DigitalObject digitalObject, final Model inModel) {
        this.digitalObject = digitalObject;
        this.inModel = inModel;
        this.outModel = ModelFactory.createDefaultModel();
    }

    public void select() {
        // add to the output model all statements that have the digital object (or one of its sameAses) as subject
        final CombinedSelector selector = CombinedSelector.get().chain(new DigitalObjectSelector(this.digitalObject, RoleOnTriple.SUBJECT));
        final StmtIterator statsWithDigitalObjectSubjects = this.inModel.listStatements(selector);
        try {
            this.addToModel(statsWithDigitalObjectSubjects, true);
        } finally {
            statsWithDigitalObjectSubjects.close();
        }

        // of those, select the ones that have blank nodes as objects, and recursively add them and their children to the output model
        selector.chain(new BlankSelector(RoleOnTriple.OBJECT));
        final StmtIterator statsWithDigitalObjectSubjectsAndBlankObjects = this.inModel.listStatements(selector);
        try {
            this.handleStatementsWithBlankObjects(statsWithDigitalObjectSubjectsAndBlankObjects.toList());
        } finally {
            statsWithDigitalObjectSubjectsAndBlankObjects.close();
        }

        // finally, select the statements that have as subject a blank node, which uses at least in another statement the digital object as object
        selector.renew().chain(new BlankSelector(RoleOnTriple.SUBJECT))
                .chain(new DigitalObjectSelector(this.digitalObject, RoleOnTriple.OBJECT));
        final StmtIterator statsWithBlankSubjectsAndDigitalObjectObjects = this.inModel.listStatements(selector);
        try {
            while (statsWithBlankSubjectsAndDigitalObjectObjects.hasNext()) {
                final Statement stat = statsWithBlankSubjectsAndDigitalObjectObjects.next();
                if (!this.outModel.contains(stat.getSubject(), null, (RDFNode) null)) {
                    final StmtIterator statsWithBlankSubjects = this.inModel.listStatements(stat.getSubject(), null, (RDFNode) null);
                    try {
                        this.addToModel(statsWithBlankSubjects, false);
                    } finally {
                        statsWithBlankSubjects.close();
                    }
                }
            }
        } finally {
            statsWithBlankSubjectsAndDigitalObjectObjects.close();
        }
    }

    public Model getOutModel() {
        return this.outModel;
    }

    private void handleStatementsWithBlankObjects(final List<Statement> statementsWithBlankObjects) {
        for (Statement statementWithBlankObject : statementsWithBlankObjects) {
            final CombinedSelector selector = CombinedSelector.get()
                    .chain(new SimpleSelector(statementWithBlankObject.getObject().asResource(), null, (RDFNode) null));
            final List<Statement> childrenStatements = this.inModel.listStatements(selector).toList();
            this.addToModel(childrenStatements, true);

            selector.chain(new BlankSelector(RoleOnTriple.OBJECT));
            final List<Statement> childrenStatementsWithBlankObjects = this.inModel.listStatements(selector).toList();
            this.handleStatementsWithBlankObjects(childrenStatementsWithBlankObjects);
        }
    }

    private void addToModel(final Statement stat, final boolean checkIfPresent) {
        if (!checkIfPresent || !this.outModel.contains(stat)) {
            this.outModel.add(stat);
        }
    }

    private void addToModel(final Iterator<Statement> statIter, final boolean checkIfPresent) {
        while (statIter.hasNext()) {
            final Statement stat = statIter.next();
            this.addToModel(stat, checkIfPresent);
        }
    }

    private void addToModel(final Iterable<Statement> stats, final boolean checkIfPresent) {
        this.addToModel(stats.iterator(), checkIfPresent);
    }

    private static boolean hasSameUri(final DigitalObject digitalObject, final RDFNode node) {
        if (!node.isResource() || node.isAnon()) {
            return false;
        }

        final Resource resource = node.asResource();
        for (String uri : digitalObject.getAllUris()) {
            if (resource.getURI().equals(uri)) {
                return true;
            }
        }
        return false;
    }

    private static RDFNode resolveNodeByRole(final Statement s, final RoleOnTriple role) {
        RDFNode node;
        switch (role) {
        case SUBJECT:
            node = s.getSubject();
            break;
        case PREDICATE:
            node = s.getPredicate();
            break;
        case OBJECT:
        default:
            node = s.getObject();
        }
        return node;
    }

    // private methods

    private static class DigitalObjectSelector extends SimpleSelector {

        private DigitalObject digitalObject;
        private RoleOnTriple role;

        private DigitalObjectSelector(final DigitalObject digitalObject, final RoleOnTriple role) {
            super();
            this.digitalObject = digitalObject;
            this.role = role;
        }

        @Override
        public boolean selects(final Statement s) {
            final RDFNode node = resolveNodeByRole(s, this.role);
            return hasSameUri(this.digitalObject, node);
        }
    }

    private static class BlankSelector extends SimpleSelector {

        private RoleOnTriple role;

        private BlankSelector(final RoleOnTriple role) {
            super();
            this.role = role;
        }

        @Override
        public boolean selects(final Statement s) {
            return resolveNodeByRole(s, this.role).isAnon();
        }
    }

}
