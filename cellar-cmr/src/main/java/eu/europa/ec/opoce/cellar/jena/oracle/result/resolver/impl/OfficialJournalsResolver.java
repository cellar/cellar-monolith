/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.jena.oracle.result.resolver.impl
 *             FILE : OfficialJournalsResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Aug 8, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.jena.oracle.result.resolver.impl;

import eu.europa.ec.opoce.cellar.cmr.oj.OfficialJournal;
import eu.europa.ec.opoce.cellar.cmr.oj.OfficialJournals;
import eu.europa.ec.opoce.cellar.jena.oracle.result.resolver.IJenaResultSetResolver;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;

import java.util.LinkedList;
import java.util.List;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Aug 8, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class OfficialJournalsResolver implements IJenaResultSetResolver<OfficialJournals> {

    /** The Constant URI_VAR. */
    private static final String URI_VAR = "uri";

    /** The Constant OJ_CLASS_VAR. */
    private static final String OJ_CLASS_VAR = "ojclass";

    /** The Constant OJ_NUMBER_VAR. */
    private static final String OJ_NUMBER_VAR = "ojnumber";

    /** The Constant OJ_COLLECTION_VAR. */
    private static final String OJ_COLLECTION_VAR = "ojcollection";

    /** The Constant OJ_YEAR_VAR. */
    private static final String OJ_YEAR_VAR = "ojyear";

    /** The Constant WORK_DATE_DOCUMENT_VAR. */
    private static final String WORK_DATE_DOCUMENT_VAR = "workdatedoc";

    /**
    * Convert row to official journal.
    *
    * @param querySolution the query solution
    * @return the official journal
    */
    public static OfficialJournal convertRowToOfficialJournal(final QuerySolution querySolution) {
        final RDFNode resourceUriNode = querySolution.get(URI_VAR);
        final String resourceURI = getStringRepresentation(resourceUriNode);

        final RDFNode subSeriesNode = querySolution.get(OJ_CLASS_VAR);
        final String subSeries = getStringRepresentation(subSeriesNode);

        final RDFNode ojNumberNode = querySolution.get(OJ_NUMBER_VAR);
        final String ojNumberString = getStringRepresentation(ojNumberNode);
        final Integer ojNumber = ojNumberString != null ? Integer.parseInt(ojNumberString) : null;

        final RDFNode seriesNode = querySolution.get(OJ_COLLECTION_VAR);
        final String fullSeriesUri = getStringRepresentation(seriesNode);
        final String seriesIdentifier = StringUtils.substringAfterLast(fullSeriesUri, "/");

        final RDFNode ojYearNode = querySolution.get(OJ_YEAR_VAR);
        final String ojYearString = getStringRepresentation(ojYearNode);
        final Integer ojYear = ojYearString != null ? Integer.parseInt(ojYearString) : null;

        final RDFNode ojWorkDateNode = querySolution.get(WORK_DATE_DOCUMENT_VAR);
        final String ojWorkDate = getStringRepresentation(ojWorkDateNode);

        final OfficialJournal result = new OfficialJournal(resourceURI, subSeries, ojNumber, seriesIdentifier, ojYear, ojWorkDate);

        return result;
    }

    /**
     * Gets the string representation.
     *
     * @param node the node
     * @return the string representation
     */
    private static String getStringRepresentation(final RDFNode node) {
        String result = null;
        if (node != null) {
            if (node.isURIResource()) {
                final Resource asResource = node.asResource();
                result = asResource.getURI();
            } else {
                if (node.isLiteral()) {
                    final Literal asLiteral = node.asLiteral();
                    result = asLiteral.getString();
                }
            }
        }
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public OfficialJournals resolve(final ResultSet resultSet) {

        final List<OfficialJournal> ojs = new LinkedList<>();
        QuerySolution querySolution = null;
        while (resultSet.hasNext()) {
            querySolution = resultSet.next();
            ojs.add(convertRowToOfficialJournal(querySolution));
        }

        return new OfficialJournals(ojs);
    }

}
