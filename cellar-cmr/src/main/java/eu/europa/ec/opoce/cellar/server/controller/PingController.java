package eu.europa.ec.opoce.cellar.server.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

//@Controller

/**
 * <p>PingController class.</p>
 */
public class PingController {

    /**
     * <p>hello.</p>
     *
     * @param httpServletResponse a {@link javax.servlet.http.HttpServletResponse} object.
     * @throws java.io.IOException if any.
     */
    @RequestMapping(value = "/ping", method = RequestMethod.GET)
    public void hello(HttpServletResponse httpServletResponse) throws IOException {
        httpServletResponse.setContentType("text/html");
        ServletOutputStream outputStream = httpServletResponse.getOutputStream();
        outputStream.write("pong".getBytes(StandardCharsets.UTF_8));
        outputStream.flush();
    }
}
