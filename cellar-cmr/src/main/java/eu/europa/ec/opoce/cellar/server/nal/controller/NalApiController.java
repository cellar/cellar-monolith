package eu.europa.ec.opoce.cellar.server.nal.controller;

import eu.europa.ec.opoce.cellar.nal.domain.ConceptScheme;
import eu.europa.ec.opoce.cellar.nal.service.impl.NalApiService;
import eu.europa.ec.opoce.cellar.semantic.helper.MessageFormatter;
import eu.europa.ec.opoce.cellar.semantic.json.JacksonUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.UnsatisfiedServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class NalApiController {

    /**
     * Constant <code>CONTENT_TYPE="application/json;charset=utf-8"</code>
     */
    private static final String CONTENT_TYPE = "application/json;charset=utf-8";

    /**
     * Constant <code>CONCEPTSCHEME_PARAM="concept_scheme"</code>
     */
    private static final String CONCEPTSCHEME_PARAM = "concept_scheme";
    /**
     * Constant <code>CONCEPT_PARAM="concept_uri"</code>
     */
    private static final String CONCEPT_PARAM = "concept_uri";
    /**
     * Constant <code>RELATION_PARAM="relation_uri"</code>
     */
    private static final String RELATION_PARAM = "relation_uri";
    /**
     * Constant <code>LANGUAGE_PARAM="language"</code>
     */
    private static final String LANGUAGE_PARAM = "language";
    /**
     * Constant <code>MODIFIED_SINCE_PARAM="if_modified_since"</code>
     */
    private static final String MODIFIED_SINCE_PARAM = "if_modified_since";

    private static final Logger LOG = LogManager.getLogger(NalApiController.class);

    @Autowired(required = true)
    private NalApiService apiService;

    /**
     * Returns all the languages in which the concept scheme is currently available.
     * The result list is defaulted to the required languages:
     * - when the concept schema URI is not provided or
     * - when the concept schema does not supply itself the list of supported languages.
     *
     * @param conceptScheme OPTIONAL
     * @param response      a {@link javax.servlet.http.HttpServletResponse} object.
     * @throws java.io.IOException if any.
     */
    @RequestMapping(value = "/nal/getSupportedLanguages", method = RequestMethod.GET)
    public void getSupportedLanguages(HttpServletResponse response,
            @RequestParam(value = CONCEPTSCHEME_PARAM, required = false) String conceptScheme) throws IOException {
        responseAsJson(response, apiService.getSupportedLanguages(conceptScheme));
    }

    /**
     * Returns the conceptScheme. Method is used to ask for a specific skos:ConceptScheme
     *
     * @param conceptScheme uri argument is mandatory
     * @param response      a {@link javax.servlet.http.HttpServletResponse} object.
     * @throws java.lang.Exception if any.
     */
    @RequestMapping(value = "/nal/getConceptScheme", method = RequestMethod.GET)
    public void getConceptScheme(HttpServletResponse response, @RequestParam(CONCEPTSCHEME_PARAM) String conceptScheme) throws Exception {
        validateConceptScheme(conceptScheme, true);

        responseAsJson(response, apiService.getConceptScheme(conceptScheme));
    }

    /**
     * Returns all conceptScheme's
     *
     * @param if_modified_since Optional. Filters concept scheme that where modified in CELLAR after the specified date-time.
     *                          See ConceptScheme field lastModified
     * @param response          a {@link javax.servlet.http.HttpServletResponse} object.
     * @throws java.io.IOException if any.
     */
    @RequestMapping(value = "/nal/getConceptSchemes", method = RequestMethod.GET)
    public void getConceptSchemes(HttpServletResponse response,
            @RequestParam(value = MODIFIED_SINCE_PARAM, required = false) String if_modified_since) throws IOException {
        // TODO implement if modified since logic
        ConceptScheme[] conceptSchemes = apiService.getConceptSchemes(null);
        responseAsJson(response, conceptSchemes);
    }

    /**
     * Get the top concepts of the conceptScheme
     *
     * @param conceptScheme uri argument is mandatory
     * @param language      argument is mandatory
     * @param response      a {@link javax.servlet.http.HttpServletResponse} object.
     * @throws java.lang.Exception if any.
     */
    @RequestMapping(value = "/nal/getTopConcepts", method = RequestMethod.GET)
    public void getTopConcepts(HttpServletResponse response, @RequestParam(CONCEPTSCHEME_PARAM) String conceptScheme,
            @RequestParam(LANGUAGE_PARAM) String language) throws Exception {
        validateConceptScheme(conceptScheme, true);
        validateLanguage(language, true);

        responseAsJson(response, apiService.getTopConcepts(conceptScheme, language));
    }

    /**
     * Returns a list of concepts having a specific semantic relation with the given concept.
     * <p/>
     * Note: with URI %escaping skos:broader becomes http://www.w3.org/2004/02/skos/core%23broader
     *
     * @param concept  uri argument is mandatory
     * @param relation uri argument is mandatory. Typical values are:
     *                 - http://www.w3.org/2004/02/skos/core#broader
     *                 - http://www.w3.org/2004/02/skos/core#narrower
     *                 - http://www.w3.org/2004/02/skos/core#related
     * @param language argument is mandatory
     * @param response a {@link javax.servlet.http.HttpServletResponse} object.
     * @throws java.lang.Exception if any.
     */
    @RequestMapping(value = "/nal/getConceptRelatives", method = RequestMethod.GET)
    public void getConceptRelatives(HttpServletResponse response, @RequestParam(CONCEPT_PARAM) String concept,
            @RequestParam(RELATION_PARAM) String relation, @RequestParam(LANGUAGE_PARAM) String language) throws Exception {
        validateConcept(concept, true);
        validateRelation(relation, true);
        validateLanguage(language, true);

        responseAsJson(response, apiService.getConceptRelatives(concept, relation, language));
    }

    /**
     * Get the translation of a given concept into a specified language.
     *
     * @param concept  uri argument is mandatory
     * @param language argument is mandatory
     * @param response a {@link javax.servlet.http.HttpServletResponse} object.
     * @throws java.lang.Exception if any.
     */
    @RequestMapping(value = "/nal/getConcept", method = RequestMethod.GET)
    public void getConcept(HttpServletResponse response, @RequestParam(CONCEPT_PARAM) String concept,
            @RequestParam(LANGUAGE_PARAM) String language) throws Exception {
        validateConcept(concept, true);
        validateLanguage(language, true);

        responseAsJson(response, apiService.getConcept(concept, language));
    }

    /**
     * <p>handleBindingException.</p>
     *
     * @param e        a {@link org.springframework.web.bind.ServletRequestBindingException} object.
     * @param response a {@link javax.servlet.http.HttpServletResponse} object.
     * @throws java.lang.Exception if any.
     */
    @ExceptionHandler({
            ServletRequestBindingException.class, MissingServletRequestParameterException.class,
            UnsatisfiedServletRequestParameterException.class})
    public void handleBindingException(ServletRequestBindingException e, HttpServletResponse response) throws Exception {
        logError(e);

        response.sendError(HttpStatus.BAD_REQUEST.value(), e.getMessage());
    }

    /**
     * <p>handleServletException.</p>
     *
     * @param e        a {@link javax.servlet.ServletException} object.
     * @param response a {@link javax.servlet.http.HttpServletResponse} object.
     * @throws java.lang.Exception if any.
     */
    @ExceptionHandler(ServletException.class)
    public void handleServletException(ServletException e, HttpServletResponse response) throws Exception {
        logError(e);
        //    response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
        throw e;
    }

    /**
     * <p>handleException.</p>
     *
     * @param e        a {@link java.lang.Exception} object.
     * @param response a {@link javax.servlet.http.HttpServletResponse} object.
     * @throws java.lang.Exception if any.
     */
    @ExceptionHandler(Exception.class)
    public void handleException(Exception e, HttpServletResponse response) throws Exception {
        logError(e);

        response.sendError(HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage());
    }

    /**
     * <p>validateLanguage.</p>
     *
     * @param language a {@link java.lang.String} object.
     * @param required a boolean.
     * @throws java.lang.Exception if any.
     */
    private void validateLanguage(String language, boolean required) throws Exception {
        if (required)
            validateRequiredParam(LANGUAGE_PARAM, language);
    }

    /**
     * <p>validateConcept.</p>
     *
     * @param concept  a {@link java.lang.String} object.
     * @param required a boolean.
     * @throws java.lang.Exception if any.
     */
    private void validateConcept(String concept, boolean required) throws Exception {
        if (required)
            validateRequiredParam(CONCEPT_PARAM, concept);
    }

    /**
     * <p>validateConceptScheme.</p>
     *
     * @param conceptScheme a {@link java.lang.String} object.
     * @param required      a boolean.
     * @throws java.lang.Exception if any.
     */
    private void validateConceptScheme(String conceptScheme, boolean required) throws Exception {
        if (required)
            validateRequiredParam(CONCEPTSCHEME_PARAM, conceptScheme);
    }

    /**
     * <p>validateRelation.</p>
     *
     * @param relation a {@link java.lang.String} object.
     * @param required a boolean.
     * @throws java.lang.Exception if any.
     */
    private void validateRelation(String relation, boolean required) throws Exception {
        if (required)
            validateRequiredParam(RELATION_PARAM, relation);
    }

    /**
     * <p>validateRequiredParam.</p>
     *
     * @param paramName  a {@link java.lang.String} object.
     * @param paramValue a {@link java.lang.String} object.
     * @throws java.lang.Exception if any.
     */
    private void validateRequiredParam(String paramName, String paramValue) throws Exception {
        if (StringUtils.isBlank(paramValue)) {
            throw new ServletRequestBindingException(
                    MessageFormatter.format("Invalid blank value for the java.lang.String parameter '{}'.", paramName));
        }
    }

    /**
     * <p>responseAsJson.</p>
     *
     * @param response a {@link javax.servlet.http.HttpServletResponse} object.
     * @param object   a {@link java.lang.Object} object.
     * @throws java.io.IOException if any.
     */
    private void responseAsJson(HttpServletResponse response, Object object) throws IOException {
        response.setContentType(CONTENT_TYPE);
        JacksonUtils.streamAsJson(response.getOutputStream(), object, true);
    }

    /**
     * <p>logError.</p>
     *
     * @param e a {@link java.lang.Exception} object.N
     */
    private void logError(Exception e) {
        LOG.error(e.getMessage(), e);
    }
}
