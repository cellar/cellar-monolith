/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.cmr.ingest.impl
 *        FILE : AbstractStructMapService.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 11-04-2013
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.ingest.impl;

import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData;
import eu.europa.ec.opoce.cellar.cmr.ingest.IMetadataCalculator;
import eu.europa.ec.opoce.cellar.cmr.ingest.IStructMapService;
import eu.europa.ec.opoce.cellar.common.factory.IFactory;
import eu.europa.ec.opoce.cellar.common.metadata.IModelLoader;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentStream;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.OperationSubType;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 11-04-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public abstract class AbstractStructMapService implements IStructMapService {

    @Autowired
    protected IMetadataCalculator metadataCalculator;

    @Autowired
    @Qualifier("modelLoaderFactory")
    private IFactory<IModelLoader<CalculatedData>, OperationSubType> modelLoaderFactory;

    @Autowired
    private ContentStreamService contentStreamService;

    @Autowired
    protected CellarResourceDao cellarResourceDao;

    protected void checkCellarIdFilled(DigitalObject digitalObject) {
        if (digitalObject.getCellarId() == null) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("CellarId in DigitalObject cannot be null").build();
        }
        for (ContentStream contentStream : digitalObject.getContentStreams()) {
            if (contentStream.getCellarId() == null) {
                throw ExceptionBuilder.get(CellarException.class).withMessage("CellarId in ContentStream cannot be null").build();
            }
        }
        for (DigitalObject child : digitalObject.getChildObjects()) {
            checkCellarIdFilled(child);
        }
    }

    protected IModelLoader<CalculatedData> createModelLoader(final StructMap structMap) {
        return this.modelLoaderFactory.create(structMap.resolveOperationSubType());
    }

    protected ContentStreamService getContentStreamService() {
        return contentStreamService;
    }

    protected void log(final Logger log, final StructMap structMap, final String format, final Object... args) {
        log.info(format, args);
    }

}
