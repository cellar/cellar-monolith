package eu.europa.ec.opoce.cellar.server.dissemination.converter;

import eu.europa.ec.opoce.cellar.semantic.xml.DomUtils;

import java.io.IOException;

import org.springframework.http.HttpOutputMessage;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.w3c.dom.Document;

/**
 * <p>NoticeHttpMessageConvertor class.</p>
 */
public class NoticeHttpMessageConverter extends CellarAbstractHttpMessageConverter<Document> {

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean supports(Class<?> clazz) {
        return Document.class.isAssignableFrom(clazz);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    protected void writeInternal(Document document, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
        if (document != null) {
            DomUtils.write(document, outputMessage.getBody());
        }
    }
}
