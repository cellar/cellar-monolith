/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ontology
 *             FILE : OntologyService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 01 10, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-01-10 14:43:03 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ontology.service;

import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.ontology.domain.OntologyWrapper;
import eu.europa.ec.opoce.cellar.ontology.exception.OntologyException;
import org.apache.jena.graph.Triple;

import java.io.File;
import java.util.Collection;
import java.util.List;

/**
 * @author ARHS Developments
 * @since 7.7
 */
public interface OntologyService {

    OntologyWrapper getWrapper();

    byte[] findOntology(String externalPid, ContentType contentType);

    void update(List<File> resources, String cellarId) throws OntologyException;

    void update(List<File> resources,String cellarId, boolean override) throws OntologyException;

    Collection<String> getEmbargoDateProperties(DigitalObjectType digitalObjectType, boolean withSuperProperties);

    List<Triple> getSKOSTriples();
}
