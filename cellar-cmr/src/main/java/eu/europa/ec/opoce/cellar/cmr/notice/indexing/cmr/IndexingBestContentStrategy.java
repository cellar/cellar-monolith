package eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr;

import eu.europa.ec.opoce.cellar.cmr.CellarResource;

import java.util.List;
import java.util.Map;

public interface IndexingBestContentStrategy {

    /**
     *
     * @param manifestations
     * @return
     */
    List<CellarResource> getBestContent(Map<String, List<CellarResource>> manifestations);
}
