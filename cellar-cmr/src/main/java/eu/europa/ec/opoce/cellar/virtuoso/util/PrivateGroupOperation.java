/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.virtuoso.util
 *             FILE : PrivateGroupOperation.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jul 24, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$$
 *          VERSION : $LastChangedRevision$$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.virtuoso.util;

/**
 * This enum lists the virtuoso functions over the private group graph
 * http://www.openlinksw.com/schemas/virtrdf#PrivateGraphs
 *
 * @author ARHS Developments
 * @version $Revision$$
 */
public enum PrivateGroupOperation {
    /** The insert. */
    INSERT("DB.DBA.RDF_GRAPH_GROUP_INS"), /** The remove. */
    REMOVE("DB.DBA.RDF_GRAPH_GROUP_DEL");

    /** The operation. */
    private final String operation;

    /**
     * Instantiates a new private group operation.
     *
     * @param operation the operation
     */
    PrivateGroupOperation(final String operation) {
        this.operation = operation;
    }

    /**
     * Gets the operation.
     *
     * @return the operation
     */
    public String getOperation() {
        return this.operation;
    }
}
