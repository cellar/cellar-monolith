package eu.europa.ec.opoce.cellar.virtuoso.util;

import org.apache.jena.rdf.model.*;
import org.apache.jena.rdf.model.impl.StatementImpl;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import eu.europa.ec.opoce.cellar.common.util.CellarIdUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Skolemizer for the metadata of Virtuoso: converting bnodes into URIs Created
 * by kosterar on 2016-03-30.
 */
public class VirtuosoSkolemizer {

	private static final Logger LOG = LogManager.getLogger(VirtuosoSkolemizer.class);
	/**
	 * <cellar.uri.disseminationBase>.well-known/genid/<graph identifier
	 * (UUID)>/<skolem IRI (UUID)>
	 */
	private static final String SKOLEMIZED_RESOURCE_IDENTIFIER_PATTERN = "%s.well-known/genid/%s/%s";

	/** The Constant cellarConfiguration. */
	private static final ICellarConfiguration cellarConfiguration = ServiceLocator.getService("cellarConfiguration",
			ICellarConfiguration.class);

	/**
	 * The Constant identifierService.
	 */
	private static final IdentifierService identifierService = ServiceLocator.getService("pidManagerService",
			IdentifierService.class);

	private final boolean skolemizationEnabled;

	/**
	 * Constructor.
	 */
	public VirtuosoSkolemizer() {
		this.skolemizationEnabled = cellarConfiguration.isVirtuosoIngestionSkolemizationEnabled();
		LOG.debug(IConfiguration.VIRTUOSO, "Virtuoso skolemization is {}", skolemizationEnabled ? "ENABLED" : "DISABLED");
	}

	/**
	 * Constructor
	 * For use by DisseminationService
	 * @param enabled(always true)
	 */
	public VirtuosoSkolemizer(boolean enabled){
		this.skolemizationEnabled=enabled;
	}

	private static Resource skolemizeBlankNode(final Resource ressource, final Map<String, String> skolemizedBlankNodes,
			final String uri) {
		final String blankNodeId = ressource.getId().getLabelString();
		String skolemizedUri = skolemizedBlankNodes.get(blankNodeId);
		if (skolemizedUri == null) {

			String cellarId = identifierService.getPrefixedFromUri(uri);

			skolemizedUri = String.format(SKOLEMIZED_RESOURCE_IDENTIFIER_PATTERN, //
					cellarConfiguration.getCellarUriDisseminationBase(), //
					CellarIdUtils.getCellarIdSuffix(cellarId), //
					UUID.randomUUID().toString());

			skolemizedBlankNodes.put(blankNodeId, skolemizedUri);
		}

		return ResourceFactory.createResource(skolemizedUri);
	}

	/**
	 * Skolemizes a model. The skolemization is performed only when it is
	 * enabled. Otherwise, nothing is performed.
	 * 
	 * @param model
	 *            the model to skolemize
	 * @param uri
	 *            the named graph to attach the skolemized URI
	 * @return if the skolemization is enabled, the skolemized model. Otherwise,
	 *         the entry model.
	 */
	public Model skolemizeModel(final Model model, final String uri) {

		// Ensures that the skolemization is needed for this skolemization
		// session
		if (!skolemizationEnabled) {
			return model;
		}

		final Model skolemizedModel = ModelFactory.createDefaultModel();

		final Map<String, String> skolemizedBlankNodes = new HashMap<>();

		final StmtIterator iterator = model.listStatements();
		try {
			while (iterator.hasNext()) {
				Statement statement = iterator.nextStatement();
				Resource subject = statement.getSubject();
				Property property = statement.getPredicate();
				RDFNode object = statement.getObject();

				if (subject.isAnon()) {
					subject = skolemizeBlankNode(subject, skolemizedBlankNodes, uri);
				}

				if (object.isAnon()) {
					object = skolemizeBlankNode(object.asResource(), skolemizedBlankNodes, uri);
				}

				skolemizedModel.add(new StatementImpl(subject, property, object));
			}
		} finally {
			iterator.close();
		}

		return skolemizedModel;
	}

	/**
	 * Indicates whether a skolemization session is needed or not.
	 * 
	 * @return true if the skolemization is needed, otherwise false
	 */
	public boolean isSkolemizationEnabled() {
		return this.skolemizationEnabled;
	}
}
