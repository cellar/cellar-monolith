package eu.europa.ec.opoce.cellar.cmr.service.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.service.IMementoQueryConfiguration;
import eu.europa.ec.opoce.cellar.cmr.service.IMementoService;
import eu.europa.ec.opoce.cellar.cmr.sparql.utils.QueryUtils;
import eu.europa.ec.opoce.cellar.common.http.headers.ILink;
import eu.europa.ec.opoce.cellar.common.http.headers.LinkImpl;
import eu.europa.ec.opoce.cellar.common.http.headers.RelType;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.rdf.model.RDFNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.regex.Pattern;

/**
 * Implements the IMementoService interface
 * @author ARHS Developments
 *
 */
@Service("mementoService")
public class MementoServiceImpl implements IMementoService {

    private ICellarConfiguration cellarConfiguration;
    private IdentifierService identifierService;
    private IMementoQueryConfiguration mementoQueryConfiguration;

    @Autowired
    public MementoServiceImpl(@Qualifier("cellarConfiguration") final ICellarConfiguration cellarConfiguration,
                              @Qualifier("pidManagerService") final IdentifierService identifierService,
                              final IMementoQueryConfiguration mementoQueryConfiguration) {
        this.cellarConfiguration = cellarConfiguration;
        this.identifierService = identifierService;
        this.mementoQueryConfiguration = mementoQueryConfiguration;
    }

    @Override
    public boolean isEvolutiveWork(final String uri) {
        final Map<Pattern, Object> placeHoldersMap = QueryUtils.preparePlaceHoldersMap(uri);
        final String query = mementoQueryConfiguration.getQueryForEvolutiveWork(placeHoldersMap);
        final List<QuerySolution> qs = QueryUtils.executeSelectQuery(cellarConfiguration.getCellarServiceSparqlUri(), query);
        //If the query returned a result, we can conclude it's an evolutive work
        return qs.size() > 0;
    }

    @Override
    public boolean isEvolutiveWorkOrMemento(final String uri) {
        final Map<Pattern, Object> placeHoldersMap = QueryUtils.preparePlaceHoldersMap(uri);
        final String query = mementoQueryConfiguration.getQueryForEvolutiveWorkOrMemento(placeHoldersMap);
        final List<QuerySolution> qs = QueryUtils.executeSelectQuery(cellarConfiguration.getCellarServiceSparqlUri(), query);
        //If the query returned a result, we can conclude it's an evolutive work or a memento
        return qs.size() > 0;
    }

    @Override
    public ILink getFirstMementoCellarUri(final String uri) {
        final Map<Pattern, Object> placeHoldersMap = QueryUtils.preparePlaceHoldersMap(uri);
        final String query = mementoQueryConfiguration.getQueryForFirstMemento(placeHoldersMap);
        final List<QuerySolution> querySolutions
                = QueryUtils.executeSelectQuery(cellarConfiguration.getCellarServiceSparqlUri(), query);

        for (QuerySolution qs : querySolutions) {
            final RDFNode individualWorkNode = qs.get("individual_work");
            final RDFNode dateNode = qs.get("date");
            final Date date = QueryUtils.getDateOrNull(dateNode);

            if (individualWorkNode.isURIResource() && date != null) {
                final String firstMementoCellarUri = this.identifierService.getCellarUriOrNull(individualWorkNode.asResource().getURI());
                return new LinkImpl(firstMementoCellarUri, RelType.MEMENTO_FIRST, date);
            }
        }

        return null;
    }

    @Override
    public ILink getLastMementoCellarUri(final String uri) {

        final Map<Pattern, Object> placeHoldersMap = QueryUtils.preparePlaceHoldersMap(uri);
        final String query = mementoQueryConfiguration.getQueryForLastMemento(placeHoldersMap);
        final List<QuerySolution> querySolutions = QueryUtils.executeSelectQuery(cellarConfiguration.getCellarServiceSparqlUri(), query);

        for (QuerySolution qs : querySolutions) {
            final RDFNode individualWorkNode = qs.get("individual_work");
            final RDFNode dateNode = qs.get("date");
            final Date date = QueryUtils.getDateOrNull(dateNode);

            if (individualWorkNode.isURIResource() && date != null) {
                final String firstMementoCellarUri = this.identifierService.getCellarUriOrNull(individualWorkNode.asResource().getURI());
                return new LinkImpl(firstMementoCellarUri, RelType.MEMENTO_LAST, date);
            }
        }

        return null;
    }

    @Override
    public String getNearestInFutureCellarUri(final String uri, Date acceptDateTime) {

        Map<Pattern, Object> placeHoldersMap;
        if (null == acceptDateTime) {
            acceptDateTime = new Date();
        }
        placeHoldersMap = QueryUtils.preparePlaceHoldersMap(uri, acceptDateTime);

        final String query = mementoQueryConfiguration.getQueryForNearestInFuture(placeHoldersMap);
        return getNextLocationCellarUri(query);
    }

    @Override
    public String getNearestInPastCellarUri(final String uri, Date acceptDateTime) {

        Map<Pattern, Object> placeHoldersMap;
        if (null == acceptDateTime) {
            acceptDateTime = new Date();
        }
        placeHoldersMap = QueryUtils.preparePlaceHoldersMap(uri, acceptDateTime);

        final String query = mementoQueryConfiguration.getQueryForNearestInPast(placeHoldersMap);
        return getNextLocationCellarUri(query);
    }

    @Override
    public Date getMementoDateTime(final String uri) {
        final Map<Pattern, Object> placeHoldersMap = QueryUtils.preparePlaceHoldersMap(uri);
        final String query = mementoQueryConfiguration.getQueryForMementoDateTime(placeHoldersMap);
        return QueryUtils.executeSelectQuery(cellarConfiguration.getCellarServiceSparqlUri(), query)
                .stream()
                .findFirst()
                .map(qs -> qs.get("date"))
                .map(QueryUtils::getDateOrNull)
                .orElse(null);
    }

    @Override
    public String getOriginalCellarUri(final String uri) {
        final Map<Pattern, Object> placeHoldersMap = QueryUtils.preparePlaceHoldersMap(uri);
        final String query = mementoQueryConfiguration.getQueryForURIG(placeHoldersMap);
        return QueryUtils.executeSelectQuery(cellarConfiguration.getCellarServiceSparqlUri(), query)
                .stream()
                .findFirst()
                .map(qs -> qs.get("predecessor"))
                .filter(RDFNode::isResource)
                .map(node -> node.asResource().getURI())
                .map(nodeUri -> identifierService.getCellarUriOrNull(nodeUri))
                .orElse(null);
    }

    /**
     * Retrieves the next location based on a provided query
     * @param query the query to execute, provided as a String
     * @return the stringified next location if a result was returned by the SPARQL end-point, null otherwise
     */
    private String getNextLocationCellarUri(final String query) {
        return QueryUtils.executeSelectQuery(cellarConfiguration.getCellarServiceSparqlUri(), query)
                .stream()
                .findFirst()
                .map(qs -> qs.get("successor"))
                .filter(node -> node != null && node.isURIResource())
                .map(node -> node.asResource().getURI())
                .map(location -> identifierService.getCellarUriOrNull(location))
                .orElse(null);
    }

    @Override
    public Collection<ILink> getFullLinks(final String uri, final Date acceptDateTime) {

        final String originalCellarUri = this.getOriginalCellarUri(uri);
        if (null != originalCellarUri) {
            final Collection<ILink> links = new LinkedList<ILink>();
            links.add(new LinkImpl(originalCellarUri, RelType.ORIGINAL_TIMEGATE));
            links.add(new LinkImpl(originalCellarUri + "?rel=timemap", RelType.TIMEMAP));

            final ILink firstMementoCellarUri = this.getFirstMementoCellarUri(originalCellarUri);
            if (firstMementoCellarUri != null) {
                links.add(firstMementoCellarUri);
            }

            final ILink lastMementoCellarUri = this.getLastMementoCellarUri(originalCellarUri);
            if (lastMementoCellarUri != null) {
                links.add(lastMementoCellarUri);
            }
            return links;
        }
        return null;
    }
}
