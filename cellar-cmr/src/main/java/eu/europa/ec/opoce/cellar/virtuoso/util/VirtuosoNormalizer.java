/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.virtuoso.util
 *             FILE : VirtuosoNormalizer.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 8, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.virtuoso.util;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.service.INormalizationQueryConfiguration;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentStream;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import virtuoso.jena.driver.VirtDataset;
import virtuoso.jena.driver.VirtuosoUpdateFactory;
import virtuoso.jena.driver.VirtuosoUpdateRequest;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration.VIRTUOSO;

import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.NodeIterator;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.update.UpdateAction;
import org.apache.jena.update.UpdateFactory;

/**
 * <class_description> Virtuoso normalizer: Replace the business resource ids by the corresponding cellar ids.
 * This principle is applicable for subject resources and object resources.
 *
 * <br/><br/>
 * ON : Oct 8, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class VirtuosoNormalizer {

    private static final Logger LOG = LogManager.getLogger(VirtuosoNormalizer.class);

    /** The Constant cellarConfiguration. */
    private static final ICellarConfiguration cellarConfiguration = ServiceLocator.getService("cellarConfiguration",
            ICellarConfiguration.class);

    /** The Constant identifierService. */
    private static final IdentifierService identifierService = ServiceLocator.getService("pidManagerService", IdentifierService.class);

    /**
     * The Constant normalization queries builder.
     */
    private static final INormalizationQueryConfiguration normalizationQueryConfiguration = ServiceLocator
            .getService("normalizationQueryConfiguration", INormalizationQueryConfiguration.class);

    /**
     * Mapping between digital objects CELLAR uri and productions uri(s).
     */
    private final Map<String, Set<String>> doUriMappings;

    private final boolean normalizationEnabled;

    /**
     * Constructor.
     * @param rawDirectMetadataToInsert the digital object to support
     */
    public VirtuosoNormalizer(final Map<DigitalObject, Model> rawDirectMetadataToInsert) {
        this.doUriMappings = loadDigitalObjectUriMappings(rawDirectMetadataToInsert);
        this.normalizationEnabled = cellarConfiguration.isVirtuosoIngestionNormalizationEnabled();
        if (LOG.isDebugEnabled(VIRTUOSO)) {
            LOG.debug(VIRTUOSO, "VirtuosoNormalizer is {} and initialized with {}",
                    (normalizationEnabled ? "ENABLED" : "DISABLED"), doUriMappings);
        }
    }

    /**
     * Constructor.
     * Used by DisseminationService
     */
    public VirtuosoNormalizer(){
        //Does not matter in dissemination service and is not checked ,but since
        // it is declared as final we need to set a value
        normalizationEnabled=true;
        this.doUriMappings=new HashMap<>();
    }


    /**
     * Gets non normalized uris (business uris)  of the corresponding digital object.
     * @param digitalObject the target digital object
     * @return the non normalized uris (business uris) set
     */
    public Set<String> getNonNormalizedUris(final DigitalObject digitalObject) {
        final String cellarUri = digitalObject.getCellarId().getUri();

        return this.doUriMappings.get(cellarUri);
    }

    /**
     * Indicates whether a normalization sessions is needed or not.
     * @return true if the normalization is needed, otherwise false
     */
    public boolean isNormalizationEnabled() {
        return this.normalizationEnabled;
    }

    /**
     * Normalizes the existing metadata (backlog) referring the digital object.
     * The normalization is performed only when it is enabled. Otherwise, nothing is performed.
     * @param dataset the global Virtuoso dataset
     * @param digitalObject the target digital object
     */
    public void normalizeExistingMetadata(final VirtDataset dataset, final DigitalObject digitalObject) {
        // Ensures that the normalization is needed for this normalization session
        if (normalizationEnabled) {
            final String cellarUri = digitalObject.getCellarId().getUri();
            LOG.debug(VIRTUOSO, "Normalizing existing metadata of '{}'", cellarUri);
            internalNormalizeExistingMetadata(dataset, cellarUri, this.doUriMappings.get(cellarUri));
            LOG.debug(VIRTUOSO, " Normalized existing metadata of '{}'",cellarUri);
        } else {
            LOG.debug(VIRTUOSO, "Normalization is DISABLED");
        }
    }

    /**
     * Normalizes the existing metadata (backlog) referring a collection of business uris.
     * The normalization is performed only when it is enabled. Otherwise, nothing is performed.
     * @param dataset the global Virtuoso dataset
     * @param cellarId the cellar id to use in case of normalization
     * @param productionUris the business uris to normalize
     */
    public static void normalizeExistingMetadata(final VirtDataset dataset, final String cellarId, final Collection<String> productionUris) {

        // Ensures that the normalization is needed
        if (!cellarConfiguration.isVirtuosoIngestionNormalizationEnabled()) {
            return;
        }

        final String cellarUri = identifierService.getUri(cellarId);

        internalNormalizeExistingMetadata(dataset, cellarUri, productionUris);
    }

    /**
     * Normalizes a model.
     * The normalization is performed only when it is enabled. Otherwise, nothing is performed.
     * @param model the model to normalize
     * @param digitalObject the target digital object
     */
    public void normalizeModel(final Model model, final DigitalObject digitalObject) {

        // Ensures that the normalization is needed for this normalization session
        if (!normalizationEnabled) {
            return;
        }

        final String cellarUri = digitalObject.getCellarId().getUri();
        //Normalize the base uri subjects
        normalizeSubjects(model, cellarUri, this.doUriMappings.get(cellarUri));
        if(cellarConfiguration.isVirtuosoIngestionTechnicalEnabled()) {
            //Also normalize the item uri subjects
            if (digitalObject.getType().equals(DigitalObjectType.MANIFESTATION)) {
                for (ContentStream item : digitalObject.getContentStreams()) {
                    final String itemUri = item.getCellarId().getUri();
                    normalizeSubjects(model, itemUri, this.doUriMappings.get(itemUri));
                }
            }
        }
        normalizeObjects(model, this.doUriMappings);

    }
    /**
     * Normalizes a model.For usage by Dissemination Service
     * @param model the model to normalize
     * @param cellarId the cellar id to use in case of normalization
     */
    public void normalizeModel(final Model model,String cellarId){
        String cellarUri=identifierService.getUri(cellarId);
        //Get uri mappings of root object
        Set<String> productionUris=identifierService.getAllPIDIfCellarIdExists(identifierService.getIdentifier(cellarId)).stream()
                        .map(identifierService::getUri)
                        .collect(Collectors.toSet());
        Map<String,Set<String>> uriMappings=new HashMap<>();
        uriMappings.put(cellarUri,productionUris);

        normalizeSubjects(model,uriMappings);

        normalizeObjects(model,cellarUri,productionUris);

    }

    /**
     * Normalizes a model.
     * The normalization is performed only when it is enabled. Otherwise, nothing is performed.
     * @param model the model to normalize
     * @param cellarId the cellar id to use in case of normalization
     * @param productionUris the business uris to normalize
     */
    public static void normalizeModel(final Model model, final String cellarId, final Collection<String> productionUris) {

        // Ensures that the normalization is needed
        if (!cellarConfiguration.isVirtuosoIngestionNormalizationEnabled()) {
            return;
        }


        final String cellarUri = identifierService.getUri(cellarId);
        final Set<String> productionUrisSet = new HashSet<String>(productionUris);

        normalizeSubjects(model, cellarUri, productionUrisSet);
        normalizeObjects(model, cellarUri, productionUrisSet);
    }


    /**
     * Extract the mapping between digital objects CELLAR uri and productions uri(s).
     * @param directMetadataToInsert the digital object to support
     * @return the mapping between digital objects CELLAR uri and productions uri(s)
     */
    private static Map<String, Set<String>> loadDigitalObjectUriMappings(final Map<DigitalObject, Model> directMetadataToInsert) {
        final Map<String, Set<String>> doUriMappings = new HashMap<String, Set<String>>();

        for (final DigitalObject digitalObject : directMetadataToInsert.keySet()) {
            //Create and insert mappings for the base uri
            doUriMappings.put(digitalObject.getCellarId().getUri(), digitalObject.getAllUris(false));
            if(cellarConfiguration.isVirtuosoIngestionTechnicalEnabled()) {
                //Also create and insert uri mappings for any possible item ids.
                //[CLLR10815-869] As technical metadata of items has been added in the manifestation graph of VIRTUOSO
                // in the aforementioned ticket , this has been added here as well
                if (digitalObject.getType().equals(DigitalObjectType.MANIFESTATION)) {
                    for (ContentStream item : digitalObject.getContentStreams()) {
                        doUriMappings.put(item.getCellarId().getUri(), item.getAllUris(false));
                    }
                }
            }
        }


        return doUriMappings;
    }

    /**
     * Normalizes the existing metadata (backlog) referring a collection of business uris.
     * @param dataset the global Virtuoso dataset
     * @param cellarUri the cellar uri to use in case of normalization
     * @param productionUris the business uris to normalize
     */
    private static void internalNormalizeExistingMetadata(final VirtDataset dataset, final String cellarUri,
                                                          final Collection<String> productionUris) {

        final String query = normalizationQueryConfiguration.getQueryForObjectsWithGraphNormalization(cellarUri, productionUris);

        final VirtuosoUpdateRequest vur = VirtuosoUpdateFactory.create(query, (Dataset) dataset);
        LOG.debug(VIRTUOSO, "Virtuoso dataset before normalization query (triples count: {}). Executing query {}",
                dataset.getCount(), query);
        vur.exec();
        LOG.debug(VIRTUOSO, "Virtuoso dataset after normalization query (triples count: {})", dataset.getCount());
    }


    /**
     * Normalizes a model's subjects.
     * @param model the model to normalize
     * @param cellarUri the cellar uri to use in case of normalization
     * @param productionUris the business uris to normalize
     */
    private static void normalizeSubjects(final Model model, final String cellarUri, final Set<String> productionUris) {
        final String query = normalizationQueryConfiguration.getQueryForSubjectsWithoutGraphNormalization(cellarUri, productionUris); // the mapping must exist

        UpdateAction.execute(UpdateFactory.create(query), model);
    }

    /**
     * Normalizes a model's objects.
     * @param model the model to normalize
     * @param cellarUri the cellar uri to use in case of normalization
     * @param productionUris the business uris to normalize
     */
    private static void normalizeObjects(final Model model, final String cellarUri, final Set<String> productionUris) {
        final Map<String, Set<String>> uriMappings = new HashMap<String, Set<String>>();

        uriMappings.put(cellarUri, productionUris);

        normalizeObjects(model, uriMappings);
    }
    /**
     * Normalizes a model's objects.
     * for use by DisseminationService
     * @param model the model to normalize
     * @param uriMappings the mapping of the root object
     */
    private static void normalizeSubjects(final Model model,final Map<String,Set<String>> uriMappings){
        final ResIterator iterator = model.listSubjects();
        RDFNode subject = null;
        try {
            while (iterator.hasNext()) {
                subject = iterator.next();

                if (subject.isURIResource()) { // the subject must be an URI
                    final String uriSubject = subject.asResource().getURI();

                    final String cellarUriSubject = identifierService.getCellarUriOrNull(uriSubject);

                    if (cellarUriSubject != null && !cellarUriSubject.equals(uriSubject)) {
                        Set<String> uris = uriMappings.get(cellarUriSubject);
                        if (uris == null) {
                            uris = new HashSet<>();
                            uriMappings.put(cellarUriSubject, uris);
                        }

                        uris.add(uriSubject);
                    }
                }
            }
        } finally {
            iterator.close();
        }

        for (Map.Entry<String, Set<String>> uriMapping : uriMappings.entrySet()) {
            final String query = normalizationQueryConfiguration.getQueryForSubjectsWithoutGraphNormalization(uriMapping.getKey(),
                    uriMapping.getValue());

            UpdateAction.execute(UpdateFactory.create(query), model);
        }
    }

    /**
     * Normalizes a model's objects.
     * @param model the model to normalize
     * @param uriMappings the uri mappings used by the ingestion session
     */
    private static void normalizeObjects(final Model model, final Map<String, Set<String>> uriMappings) {
        final NodeIterator iterator = model.listObjects();
        try {
            RDFNode object = null;
            while (iterator.hasNext()) {
                object = iterator.next();

                if (object.isURIResource()) { // the object must be an URI
                    final String uriObject = object.asResource().getURI();

                    final String cellarUriObject = identifierService.getCellarUriOrNull(uriObject);

                    if (cellarUriObject != null && !uriObject.equals(cellarUriObject)) {
                        Set<String> uris = uriMappings.get(cellarUriObject);

                        if (uris == null) {
                        	uris = new HashSet<String>();
                            uriMappings.put(cellarUriObject, uris);
                        }

                        uris.add(uriObject);
                    }
                }
            }
            for (Map.Entry<String, Set<String>> uriMapping : uriMappings.entrySet()) {
                final String query = normalizationQueryConfiguration.getQueryForObjectsWithoutGraphNormalization(uriMapping.getKey(),
                        uriMapping.getValue());

                UpdateAction.execute(UpdateFactory.create(query), model);
            }
        }
        finally {
            iterator.close();
        }
    }
}
