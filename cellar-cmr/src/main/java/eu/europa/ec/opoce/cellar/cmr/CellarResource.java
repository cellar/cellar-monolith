/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr
 *             FILE : CellarResource.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 12 sept. 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr;

import eu.europa.ec.opoce.cellar.cl.configuration.enums.CellarServiceRDFStoreCleanerMode;
import eu.europa.ec.opoce.cellar.cmr.database.DaoObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.springframework.beans.factory.annotation.Configurable;

import java.io.Serializable;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * <p>CellarResource class.</p>
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Configurable
public class CellarResource extends DaoObject implements Serializable {

	/**
	 * 
	 */
    private static final long serialVersionUID = -4888541531630281721L;

	public enum CleanerStatus {

        UNKNOWN,
        PROCESSING,
        VALID,
        FIXED,
        CORRUPT,
        NEWLY_INGESTED,
        ERROR;

        public static CleanerStatus getStatus(final byte ordinal) {
            return CleanerStatus.values()[ordinal];
        }

        public static CleanerStatus getDifferenceStatus(final CellarServiceRDFStoreCleanerMode mode) {
            if (mode == CellarServiceRDFStoreCleanerMode.fix) {
                return FIXED;
            } else {
                return CORRUPT;
            }
        }
    }

    private String cellarId;

    private DigitalObjectType cellarType;

    private Date embargoDate;

    private String mimeType;

    private List<String> languages;

    private Date lastModificationDate;

    private Date embeddedNoticeCreationDate;

    private String embeddedNotice;

    private CleanerStatus cleanerStatus;

    private AlignerStatus alignerStatus;

    private Date lastIndexation;

    private Map<ContentType, String> versions = new EnumMap<>(ContentType.class);


    /**
     * Instantiates a new cellar resource.
     */
    public CellarResource() {
        this.cleanerStatus = CleanerStatus.NEWLY_INGESTED;
        this.alignerStatus = AlignerStatus.NEWLY_INGESTED;
    }

    /**
     * <p>Getter for the field <code>cellarId</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCellarId() {
        return this.cellarId;
    }

    /**
     * <p>Setter for the field <code>cellarId</code>.</p>
     *
     * @param cellarId a {@link java.lang.String} object.
     */
    public void setCellarId(final String cellarId) {
        this.cellarId = cellarId;
    }

    /**
     * <p>Getter for the field <code>cellarType</code>.</p>
     *
     * @return a {@link DigitalObjectType} object.
     */
    public DigitalObjectType getCellarType() {
        return this.cellarType;
    }

    /**
     * <p>Setter for the field <code>cellarType</code>.</p>
     *
     * @param cellarType a {@link DigitalObjectType} object.
     */
    public void setCellarType(final DigitalObjectType cellarType) {
        this.cellarType = cellarType;
    }

    /**
     * Checks if is under embargo.
     *
     * @return true, if is under embargo
     */
    public boolean isUnderEmbargo() {
        return this.embargoDate != null && this.embargoDate.after(new Date());
    }

    /**
     * <p>Getter for the field <code>embargoDate</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getEmbargoDate() {
        return this.embargoDate;
    }

    /**
     * <p>Setter for the field <code>embargoDate</code>.</p>
     *
     * @param embargoDate a {@link java.util.Date} object.
     */
    public void setEmbargoDate(final Date embargoDate) {
        this.embargoDate = embargoDate == null
                ? null
                : new Date(embargoDate.getTime());
    }

    /**
     * <p>Getter for the field <code>languages</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<String> getLanguages() {
        return this.languages;
    }

    /**
     * <p>Setter for the field <code>languages</code>.</p>
     *
     * @param languages a {@link java.util.List} object.
     */
    public void setLanguages(final List<String> languages) {
        this.languages = languages;
    }

    /**
     * <p>Getter for the field <code>lastModificationDate</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getLastModificationDate() {
        return this.lastModificationDate;
    }

    /**
     * <p>Setter for the field <code>lastModificationDate</code>.</p>
     *
     * @param lastModificationDate a {@link java.util.Date} object.
     */
    public void setLastModificationDate(final Date lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    /**
     * <p>Getter for the field <code>mimeType</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getMimeType() {
        return this.mimeType;
    }

    /**
     * <p>Setter for the field <code>mimeType</code>.</p>
     *
     * @param mimeType a {@link java.lang.String} object.
     */
    public void setMimeType(final String mimeType) {
        this.mimeType = mimeType;
    }

    /**
     * <p>Getter for the field <code>embeddedNotice</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getEmbeddedNotice() {
        return this.embeddedNotice;
    }

    /**
     * <p>Setter for the field <code>embeddedNotice</code>.</p>
     *
     * @param embeddedNotice a {@link java.lang.String} object.
     */
    public void setEmbeddedNotice(final String embeddedNotice) {
        this.embeddedNotice = embeddedNotice;
    }

    /**
     * <p>Getter for the field <code>embeddedNoticeCreationDate</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getEmbeddedNoticeCreationDate() {
        return this.embeddedNoticeCreationDate;
    }

    /**
     * <p>Setter for the field <code>embeddedNoticeCreationDate</code>.</p>
     *
     * @param embeddedNoticeCreationDate a {@link java.util.Date} object.
     */
    public void setEmbeddedNoticeCreationDate(final Date embeddedNoticeCreationDate) {
        this.embeddedNoticeCreationDate = embeddedNoticeCreationDate;
    }

    /**
     * Sets the cleaner status.
     *
     * @param cleanerStatus the new cleaner status
     */
    public void setCleanerStatus(final CleanerStatus cleanerStatus) {
        this.cleanerStatus = cleanerStatus;
    }

    /**
     * Gets the cleaner status.
     *
     * @return the cleaner status
     */
    public CleanerStatus getCleanerStatus() {
        return this.cleanerStatus;
    }

    /**
     * Gets the aligner status.
     *
     * @return the aligner status
     */
    public AlignerStatus getAlignerStatus() {
        return this.alignerStatus;
    }

    /**
     * Sets the aligner status.
     *
     * @param alignerStatus the new aligner status
     */
    public void setAlignerStatus(final AlignerStatus alignerStatus) {
        this.alignerStatus = alignerStatus;
    }

    /**
     * Gets the last indexation.
     *
     * @return the last indexation
     */
    public Date getLastIndexation() {
        return this.lastIndexation;
    }

    /**
     * Sets the last indexation.
     *
     * @param lastIndexation the new last indexation
     */
    public void setLastIndexation(final Date lastIndexation) {
        this.lastIndexation = lastIndexation;
    }

    public Map<ContentType, String> getVersions() {
        return versions;
    }

    public Optional<String> getVersion(ContentType contentType) {
        return Optional.ofNullable(versions.get(contentType));
    }

    public void setVersions(Map<ContentType, String> versions) {
        this.versions = new EnumMap<>(versions);
    }

    /**
     * @param type       the {@link ContentType}
     * @param newVersion the new version of the object
     * @return the old version or null if the version has not
     * be added before
     */
    public String putVersion(ContentType type, String newVersion) {
        return versions.put(type, newVersion);
    }

    @Override
    public String toString() {
        return "CellarResource{ " + getId() + '\n' + "cellarId='" + this.cellarId + '\'' + ", cellarType=" + this.cellarType
                + ", embargoDate=" + this.embargoDate + ", mimeType='" + this.mimeType + '\'' + ", languages='"
                + StringUtils.join(this.languages, "' '") + '\'' + ", lastModificationDate=" + this.lastModificationDate
                + ", embeddedNoticeCreationDate=" + this.embeddedNoticeCreationDate + ", embeddedNotice="
                + (StringUtils.isEmpty(this.embeddedNotice) ? "empty" : "notice") + ", cleanerStatus=" + this.cleanerStatus
                + ", alignerStatus=" + this.alignerStatus//
                + ", lastIndexation=" + this.lastIndexation + '}';
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

}
