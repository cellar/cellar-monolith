/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.nal.service
 *             FILE : NalInferenceServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 01 03, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-01-03 06:10:57 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.nal.service.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.common.MessageFormatter;
import eu.europa.ec.opoce.cellar.common.util.StringUtils;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.NalOntoUtils;
import eu.europa.ec.opoce.cellar.nal.domain.FallBackNode;
import eu.europa.ec.opoce.cellar.nal.domain.RequiredLanguageBean;
import eu.europa.ec.opoce.cellar.nal.service.FallbackCalculator;
import eu.europa.ec.opoce.cellar.nal.service.LanguageService;
import eu.europa.ec.opoce.cellar.nal.service.NalInferenceService;
import eu.europa.ec.opoce.cellar.nal.service.NalSemanticService;
import eu.europa.ec.opoce.cellar.nal.service.NalSparqlService;
import eu.europa.ec.opoce.cellar.nal.utils.NalSparqlQueryTemplate;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.semantic.jena.template.JenaTemplate;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarType;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.graph.GraphUtil;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.graph.Triple;
import org.apache.jena.graph.impl.LiteralLabel;
import org.apache.jena.graph.impl.LiteralLabelFactory;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.vocabulary.RDF;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static eu.europa.ec.opoce.cellar.AppConstants.NEWLINE;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.cmr_fallbackP;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.cmr_langP;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.cmr_levelP;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.cmr_prefLabelP;

/**
 * @author ARHS Developments
 */
@Service
public class NalInferenceServiceImpl implements NalInferenceService {

    private static final String MISSING_CONCEPT_PATTERN = "Concept with missing PREFLABEL for required language(s) and " +
            "corresponding fallback languages (concept '{}' - required languages '{}' - conceptscheme '{}')";

    private static final String MISSING_DOMAIN_PATTERN = "Domain with missing PREFLABEL for required language(s) and " +
            "corresponding fallback languages (domain '{}' - required languages '{}' - conceptscheme '{}')";

    private static final String MISSING_MICROTHESAURUS_PATTERN = "Microthesaurus with missing PREFLABEL for required " +
            "language(s) and corresponding fallback languages (microthesaurus '{}' - required languages '{}' - " +
            "conceptscheme '{}')";

    private static final Logger LOG = LogManager.getLogger(NalInferenceServiceImpl.class);

    private LanguageService languageService;
    private NalSparqlService nalSparqlService;
    private NalSemanticService nalSemanticService;
    private ICellarConfiguration cellarConfiguration;

    @Autowired
    public NalInferenceServiceImpl(LanguageService languageService, NalSparqlService nalSparqlService,
                                   NalSemanticService nalSemanticService,
                                   @Qualifier("cellarConfiguration") ICellarConfiguration cellarConfiguration) {
        this.languageService = languageService;
        this.nalSparqlService = nalSparqlService;
        this.nalSemanticService = nalSemanticService;
        this.cellarConfiguration = cellarConfiguration;
    }

    @Override
    public void infer(Model model, String conceptSchemeUri) {
        Assert.notNull(model, "The NAL input model cannot be null");
        Assert.hasLength(conceptSchemeUri, "The NAL URI cannot be empty");

        addOntologyToModelAndInfer(model, conceptSchemeUri);
        logMissingInSchemeRelations(conceptSchemeUri, model);

        if (!NalOntoUtils.EUROVOC_CONCEPTSCHEME_URI.equalsIgnoreCase(conceptSchemeUri)) {
            insertMissingTopConceptRelations(conceptSchemeUri, model);
        }

        final List<Resource> concepts = getConcepts(conceptSchemeUri, model);

        final Map<Resource, Set<String>> missingLanguagesPerConcept =
                calculateFallbacksAndReturnMissingLanguagesPerConcept(
                        conceptSchemeUri, model, concepts);
        logMissingPrefLabelForRequiredLanguages(conceptSchemeUri, missingLanguagesPerConcept, MISSING_CONCEPT_PATTERN);


        if (NalOntoUtils.EUROVOC_CONCEPTSCHEME_URI.equalsIgnoreCase(conceptSchemeUri)) {
            logMissingEurovocDomains(conceptSchemeUri, model);
            logMissingEurovocMicrothesauri(conceptSchemeUri, model);
            addFacetsToModel(conceptSchemeUri, model);
        } else {
            calculateLevelAndAddToModel(conceptSchemeUri, model, concepts);
        }
    }

    private void logMissingEurovocMicrothesauri(String conceptSchemeUri, Model inferredNalAndOntologyModel) {
        List<Resource> resources = this.nalSparqlService.getEurovocMicrothesauri(inferredNalAndOntologyModel);
        final Map<Resource, Set<String>> missingLanguagesPerMicrothesaurus =
                calculateFallbacksAndReturnMissingLanguagesPerMicrothesaurus(conceptSchemeUri, inferredNalAndOntologyModel, resources);
        logMissingPrefLabelForRequiredLanguages(conceptSchemeUri, missingLanguagesPerMicrothesaurus, MISSING_MICROTHESAURUS_PATTERN);
    }

    private void logMissingEurovocDomains(String conceptSchemeUri, Model inferredNalAndOntologyModel) {
        List<Resource> resources = this.nalSparqlService.getEurovocDomains(inferredNalAndOntologyModel);
        final Map<Resource, Set<String>> missingLanguagesPerDomain = calculateFallbacksAndReturnMissingEurovocDomains(conceptSchemeUri, inferredNalAndOntologyModel, resources);
        logMissingPrefLabelForRequiredLanguages(conceptSchemeUri, missingLanguagesPerDomain, MISSING_DOMAIN_PATTERN);
    }

    private void calculateLevelAndAddToModel(String conceptSchemeUri, Model inferredNalAndOntologyModel, List<Resource> concepts) {
        long startTime = System.currentTimeMillis();
        LOG.debug(ICellarConfiguration.NAL, "Started calculating levels of the NAL '{}'.", conceptSchemeUri);
        List<Triple> triples = calculateLevels(inferredNalAndOntologyModel, concepts);
        addTriplesToModel(triples, inferredNalAndOntologyModel);
        LOG.debug(ICellarConfiguration.NAL, "Finished calculating levels of the NAL '{}' in {} ms.", conceptSchemeUri, (System.currentTimeMillis() - startTime));
    }

    private void addFacetsToModel(String conceptSchemeUri, Model inferredNalAndOntologyModel) {
        long startTime = System.currentTimeMillis();
        LOG.debug(ICellarConfiguration.NAL, "Started calculating facets... (vocabulary '{}')", conceptSchemeUri);
        List<Triple> triples = this.calculateFacets(inferredNalAndOntologyModel);
        addTriplesToModel(triples, inferredNalAndOntologyModel);
        LOG.debug(ICellarConfiguration.NAL, "Finished calculating facets... (vocabulary '{}') in {} ms.", conceptSchemeUri, (System.currentTimeMillis() - startTime));
    }

    private void logMissingPrefLabelForRequiredLanguages(String conceptSchemeUri, Map<Resource, Set<String>> missingLanguages, String messagePattern) {
        for (final Map.Entry<Resource, Set<String>> entry : missingLanguages.entrySet()) {
            final Resource resource = entry.getKey();
            final Set<String> value = entry.getValue();
            LOG.warn(MessageFormatter.arrayFormat(
                    messagePattern,
                    new String[]{
                            resource.getURI(), StringUtils.join(value, ", "), conceptSchemeUri}));
        }
    }

    private Map<Resource, Set<String>> calculateFallbacksAndReturnMissingLanguagesPerMicrothesaurus(String conceptSchemeUri, Model inferredNalAndOntologyModel, List<Resource> resources) {
        LOG.debug(ICellarConfiguration.NAL, "Starting calculating missing languages per micro thesaurus eurovoc for model URI '{}'", conceptSchemeUri);
        final Map<Resource, Set<String>> missingLanguagesPerMicrothesaurus = this.calculateFallbacks(inferredNalAndOntologyModel,
                resources, null);
        LOG.debug(ICellarConfiguration.NAL, "Finished calculating '{}' missing languages per micro thesaurus eurovoc for model URI '{}'",
                missingLanguagesPerMicrothesaurus.size(), conceptSchemeUri);
        return missingLanguagesPerMicrothesaurus;
    }


    private Map<Resource, Set<String>> calculateFallbacksAndReturnMissingEurovocDomains(String conceptSchemeUri, Model inferredNalAndOntologyModel, List<Resource> resources) {
        LOG.debug(ICellarConfiguration.NAL, "Starting calculating missing languages per domain eurovoc for model URI '{}'", conceptSchemeUri);
        final Map<Resource, Set<String>> missingLanguagesPerDomain = this.calculateFallbacks(inferredNalAndOntologyModel, resources, null);
        LOG.debug(ICellarConfiguration.NAL, "Finished calculating '{}' missing languages per domain eurovoc for model URI '{}'", missingLanguagesPerDomain.size(), conceptSchemeUri);
        return missingLanguagesPerDomain;
    }


    private Map<Resource, Set<String>> calculateFallbacksAndReturnMissingLanguagesPerConcept(String conceptSchemeUri,
                                                                                             Model inferredNalAndOntologyModel,
                                                                                             List<Resource> concepts) {
        List<Triple> triples = new ArrayList<>();
        long startTime = System.currentTimeMillis();
        LOG.debug(ICellarConfiguration.NAL, "Started calculating fallbacks of the NAL '{}'.", conceptSchemeUri);
        final Map<Resource, Set<String>> missingLanguagesPerConcept = this.calculateFallbacks(inferredNalAndOntologyModel, concepts, triples);
        addTriplesToModel(triples, inferredNalAndOntologyModel);
        LOG.debug(ICellarConfiguration.NAL, "Finished calculating fallbacks of the NAL '{}' in {} ms.", conceptSchemeUri, (System.currentTimeMillis() - startTime));
        return missingLanguagesPerConcept;
    }

    private List<Resource> getConcepts(String conceptSchemeUri, Model inferredNalAndOntologyModel) {
        long startTime = System.currentTimeMillis();
        LOG.debug(ICellarConfiguration.NAL, "Starting retrieving concepts for model URI '{}'", conceptSchemeUri);
        final List<Resource> concepts = this.nalSparqlService.getConceptsFor(inferredNalAndOntologyModel, conceptSchemeUri);
        LOG.debug(ICellarConfiguration.NAL, "Finished retrieving concepts for model URI '{}' in {} ms.", conceptSchemeUri, (System.currentTimeMillis() - startTime));
        return concepts;
    }

    private void insertMissingTopConceptRelations(String conceptSchemeUri, Model inferredNalAndOntologyModel) {
        long startTime = System.currentTimeMillis();
        LOG.debug(ICellarConfiguration.NAL, "Started calculating and inserting missing top concept relations for '{}'.", conceptSchemeUri);
        List<Triple> triples = this.calculateMissingTopConceptRelations(inferredNalAndOntologyModel, conceptSchemeUri);
        addTriplesToModel(triples, inferredNalAndOntologyModel);
        LOG.debug(ICellarConfiguration.NAL, "Finished calculating and inserting missing top concept relations for '{}' in {} ms.",
                conceptSchemeUri, (System.currentTimeMillis() - startTime));
    }

    private void logMissingInSchemeRelations(String conceptSchemeUri, Model inferredNalAndOntologyModel) {
        long startTime = System.currentTimeMillis();
        LOG.debug(ICellarConfiguration.NAL, "Started calculating missing inScheme for uri '{}'.", conceptSchemeUri);
        this.calculateMissingInSchemeRelations(inferredNalAndOntologyModel, conceptSchemeUri);
        LOG.debug(ICellarConfiguration.NAL, "Finished calculating missing inScheme for uri '{}' in {} ms.", conceptSchemeUri,
                (System.currentTimeMillis() - startTime));
    }

    private void addOntologyToModelAndInfer(Model model, String conceptSchemeUri) {
        addOntologyToModel(conceptSchemeUri, model);
        inferWithSparql(model, conceptSchemeUri);
    }

    private void addOntologyToModel(String conceptSchemeUri, Model nalAndOntologyModel) {
        if (conceptSchemeUri.contains("eurovoc")) {
            Model eurovocOntology = nalSemanticService.getEurovocOntology();
            nalAndOntologyModel.add(eurovocOntology);
            JenaUtils.closeQuietly(eurovocOntology);
        } else {
            Model authorityOntology = nalSemanticService.getAuthorityOntology();
            nalAndOntologyModel.add(authorityOntology);
            JenaUtils.closeQuietly(authorityOntology);
        }
    }

    private List<Triple> calculateFacets(final Model model) {
        long startTime = System.currentTimeMillis();
        LOG.debug(ICellarConfiguration.NAL, "Starting calculating facets for eurovoc.");
        Model result = null;
        try {
            result = JenaTemplate.construct(model, NalSparqlQueryTemplate.constructFacetsForTopConcepts());
            result.add(JenaTemplate.construct(model, NalSparqlQueryTemplate.constructFacets()));
            LOG.debug(ICellarConfiguration.NAL, "Finished calculating facets for eurovoc in {} ms.", (System.currentTimeMillis() - startTime));
            return GraphUtil.findAll(result.getGraph()).toList();
        } finally {
            JenaUtils.closeQuietly(result);
        }
    }

    private void calculateMissingInSchemeRelations(final Model model, String conceptschemeUri) {
        this.calculateInSchemes(model, conceptschemeUri);
    }

    private void calculateInSchemes(final Model model, final String conceptScheme) {
        Model result = null;
        try {
            result = JenaTemplate.construct(model, NalSparqlQueryTemplate.constructInSchemeRelationTo(conceptScheme));

            final List<Triple> inSchemes = GraphUtil.findAll(result.getGraph()).toList();
            for (final Triple inScheme : inSchemes) {
                LOG.warn("Missing INSCHEME relation: '{}' - '{}'", inScheme.getSubject(), inScheme.getObject());
            }
        } finally {
            JenaUtils.closeQuietly(result);
        }
    }

    private void inferWithSparql(Model model, String nalUri) {
        long startTime = System.currentTimeMillis();
        int maxIteration = this.cellarConfiguration.getCellarServiceNalLoadInferenceMaxIteration();
        boolean newTriplesWereInferred = true;
        boolean maxIterationReached = false;

        if (maxIteration == 0) {
            LOG.warn("The maximum iterations if set to 0 => inference of the NAL is disabled!");
        }

        LOG.debug(ICellarConfiguration.NAL, "Started SKOS inference of '{}', maxIteration : {}.", nalUri, maxIteration);
        for (int i = 0; i < maxIteration && newTriplesWereInferred; ) {
            long inferredTriplesCount = applySparqlQueries(model);
            if (inferredTriplesCount == 0) {
                newTriplesWereInferred = false;
                LOG.debug(ICellarConfiguration.NAL, "Finished SKOS inference of '{}' without reaching the max number of iterations '{}/{}' in {} ms.",
                        nalUri, i, maxIteration, (System.currentTimeMillis() - startTime));
            } else {
                if (++i >= maxIteration) {
                    maxIterationReached = true;
                }
            }
        }
        if (newTriplesWereInferred && maxIterationReached) {
            LOG.warn("Finished SKOS inference of '{}'  but reached the max number of iterations '{}' in {} ms." + NEWLINE +
                            "Some triples might be missing! Consider updating the max iteration property for the NAL inference.",
                    nalUri, maxIteration, (System.currentTimeMillis() - startTime));
        }
    }

    // return the number of triples that were inferred because we stop calling this method from a loop when this number is 0
    private long applySparqlQueries(Model model) {
        long initialModelSize = model.size();

        for (Query query : this.nalSparqlService.getSkosInferenceSparqlList()) {
            QueryExecution queryExecution = QueryExecutionFactory.create(query, model);

            Model constructModel = queryExecution.execConstruct();
            model.add(constructModel);

            JenaUtils.closeQuietly(constructModel);
            queryExecution.close();
        }
        long finalModelSize = model.size();
        return finalModelSize - initialModelSize;
    }

    private void addTriplesToModel(List<Triple> triples, Model model) {
        // will be replaced by GraphUtil.add(inferredModel.getGraph(), triples) with Jena 3
        GraphUtil.add(model.getGraph(), triples);
    }

    private List<Triple> calculateLevels(final Model model, final Collection<Resource> concepts) {
        List<Triple> triples = new ArrayList<>();
        final LevelCalculator levelCalculator = new LevelCalculator(model);

        for (final Resource concept : concepts) {
            levelCalculator.calculateLevel(concept);
        }

        final Map<Resource, Set<Integer>> conceptLevels = levelCalculator.getConceptLevels();
        for (final Map.Entry<Resource, Set<Integer>> resourceSetEntry : conceptLevels.entrySet()) {
            for (final Integer level : resourceSetEntry.getValue()) {
                triples.add(ResourceFactory.createStatement(resourceSetEntry.getKey(), cmr_levelP,
                        ResourceFactory.createTypedLiteral(String.valueOf(level), XSDDatatype.XSDdecimal)).asTriple());
            }
        }
        return triples;
    }

    private Map<Resource, Set<String>> calculateFallbacks(final Model model,
                                                          final Collection<Resource> resources,
                                                          final List<Triple> triples) {
        // get languages
        final List<RequiredLanguageBean> requiredLanguages = this.languageService.getRequiredLanguages();
        if (requiredLanguages == null) {
            throw ExceptionBuilder.get(CellarException.class).build();
        }

        final FallbackCalculator fallbackCalculator = new FallbackCalculatorImpl(languageService, model, requiredLanguages);

        for (final Resource concept : resources) {
            fallbackCalculator.calculateFallbackNodes(concept);
        }

        if (triples == null) {
            return fallbackCalculator.getMissingLanguagesPerResource();
        }

        final Map<Resource, List<FallBackNode>> fallbackNodesPerConcept = fallbackCalculator
                .getFallbackNodesPerResource();
        for (final Map.Entry<Resource, List<FallBackNode>> resourceListEntry : fallbackNodesPerConcept
                .entrySet()) {
            final Node conceptNode = NodeFactory.createURI(resourceListEntry.getKey().getURI());

            for (final FallBackNode fallBackNode : resourceListEntry.getValue()) {
                final Node bnode = NodeFactory.createBlankNode();

                final LiteralLabel fallbackLabel = LiteralLabelFactory.create(fallBackNode.getFallbackLabel(),
                        fallBackNode.getMissingLanguage(), false);

                triples.add(Triple.create(conceptNode, cmr_fallbackP.asNode(), bnode));
                triples.add(Triple.create(bnode, cmr_prefLabelP.asNode(), NodeFactory.createLiteral(fallbackLabel)));
                triples.add(Triple.create(bnode, cmr_langP.asNode(),
                        ResourceFactory.createTypedLiteral(fallBackNode.getFallbackLanguage(), XSDDatatype.XSDlanguage).asNode()));
            }
        }

        return fallbackCalculator.getMissingLanguagesPerResource();
    }

    private List<Triple> calculateMissingTopConceptRelations(final Model model, String conceptSchemeUri) {
        return this.calculateTopConceptOfRelations(model, conceptSchemeUri);
    }

    private List<Triple> calculateTopConceptOfRelations(final Model model, final String conceptScheme) {
        Model result = null;
        try {
            result = JenaTemplate.construct(model, NalSparqlQueryTemplate.constructTopConceptOfRelationTo(conceptScheme));
            return GraphUtil.findAll(result.getGraph()).toList();
        } finally {
            JenaUtils.closeQuietly(result);
        }
    }

    /**
     * Helper class to calculate the SKOS concept levels in a particular model
     * Do not use this class as a service, not Thread save!
     */
    private static class LevelCalculator {
        private Model model;
        private Map<Resource, Set<Integer>> levelsPerConcept = new HashMap<Resource, Set<Integer>>();

        /**
         * <p>Constructor for LevelCalculator.</p>
         *
         * @param model a {@link Model} object.
         */
        LevelCalculator(Model model) {
            if (model == null) {
                throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument model cannot be null").build();
            }

            this.model = model;
        }

        /**
         * <p>reset.</p>
         */
        public void reset() {
            levelsPerConcept.clear();
        }

        /**
         * <p>calculate.</p>
         */
        public void calculate() {
            for (Resource concept : listConcepts()) {
                calculateLevel(concept);
            }
        }

        /**
         * <p>calculateLevel.</p>
         *
         * @param concept a {@link Resource} object.
         * @return a {@link List} object.
         */
        List<Integer> calculateLevel(Resource concept) {
            Set<Integer> levels = levelsPerConcept.get(concept);
            if (levels != null) {
                return new ArrayList<>(levels);
            }

            levels = new HashSet<>();

            List<RDFNode> broaders = getBroaders(concept);
            if (broaders.isEmpty() || isTopConcept(concept)) {
                levels.add(1);
            }
            for (RDFNode node : broaders) {
                for (int level : calculateLevel((Resource) node)) {
                    levels.add(level + 1);
                }
            }

            levelsPerConcept.put(concept, levels);
            return new ArrayList<>(levels);
        }

        /**
         * <p>getConceptLevels.</p>
         *
         * @return a {@link Map} object.
         */
        Map<Resource, Set<Integer>> getConceptLevels() {
            return Collections.unmodifiableMap(levelsPerConcept);
        }

        /**
         * <p>getBroaders.</p>
         *
         * @param concept a {@link Resource} object.
         * @return a {@link List} object.
         */
        private List<RDFNode> getBroaders(Resource concept) {
            return model.listObjectsOfProperty(concept, CellarProperty.skos_broaderP).toList();
        }

        /**
         * <p>isTopConcept.</p>
         *
         * @param concept a {@link Resource} object.
         * @return a boolean.
         */
        private boolean isTopConcept(Resource concept) {
            return !model.listObjectsOfProperty(concept, CellarProperty.skos_topConceptOfP).toList().isEmpty();
        }

        /**
         * <p>listConcepts.</p>
         *
         * @return a {@link List} object.
         */
        private List<Resource> listConcepts() {
            return model.listResourcesWithProperty(RDF.type, CellarType.skos_ConceptR).toList();
        }
    }
}
