/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.graph
 *             FILE : GraphBuilder.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 23, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.graph.impl;

import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.daobeans.InverseRelation;
import eu.europa.ec.opoce.cellar.cmr.graph.IGraph;
import eu.europa.ec.opoce.cellar.cmr.graph.IGraphBuilder;
import eu.europa.ec.opoce.cellar.cmr.service.IInverseRelationService;
import eu.europa.ec.opoce.cellar.common.util.CellarIdUtils;

import java.util.Arrays;
import java.util.Collection;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * <class_description> Graph builder implementation.
 * <br/><br/>
 * ON : Apr 23, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Configurable
public abstract class GraphBuilder implements IGraphBuilder {

    /**
     * Inverse relations service.
     */
    @Autowired
    private IInverseRelationService inverseRelationService;

    /**
     * Identifier manager service.
     */
    @Autowired
    @Qualifier("pidManagerService")
    protected IdentifierService identifierService;

    /**
     * Graph concerned by the builder.
     */
    private IGraph graph;

    /**
     * Add the graph to the builder.
     * @param graph the graph to add
     * @return the graph builder
     */
    public GraphBuilder withGraph(final IGraph graph) {
        this.graph = graph;
        return this;
    }

    /**
     * Initialize the graph builder.
     * @return the graph builder
     */
    @Override
    public GraphBuilder initialize() {
        this.graph = new Graph();
        ((Graph) this.graph).initialize();
        return this;
    }

    /**
     * Initialize the graph builder but keep the base objects.
     * @return the graph builder
     */
    protected GraphBuilder initializeFromObjects() {
        ((Graph) this.graph).initializeFromObjects();
        return this;
    }

    /**
     * Build the inverse relations on target id.
     * @return the graph builder
     */
    public GraphBuilder buildInverseRelationsOnTarget() {
        // retrieve the inverse relations from the database
        final Collection<InverseRelation> relations = this.inverseRelationService.getInverseRelationsOnTarget(this.graph.getObjects());

        Identifier identifier = null;
        for (final InverseRelation relation : relations) {
            identifier = this.constructIdentifier(relation.getSource());
            this.graph.addRelation(identifier);
        }

        return this;
    }

    /**
     * Build the inverse relation on source id.
     * @return the graph builder
     */
    public GraphBuilder buildInverseRelationsOnSource() {
        // retrieve the inverse relations from the database
        final Collection<InverseRelation> relations = this.inverseRelationService.getInverseRelationsOnSource(this.graph.getObjects());

        Identifier identifier = null;
        for (final InverseRelation relation : relations) {
            identifier = this.constructIdentifier(relation.getTarget());
            this.graph.addRelation(identifier);
        }

        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IGraph getGraph() {
        return this.graph;
    }

    /**
     * Construct an identifier object based on a pid.
     * @param pid the production identifier
     * @return an identifier object
     */
    protected Identifier constructIdentifier(final String pid) {
        String cellarIdBase = null;
        if (this.identifierService.isCellarUUID(pid)) {
            cellarIdBase = CellarIdUtils.getCellarIdBase(pid);
            return new Identifier(cellarIdBase, null);
        } else {
            final String cellarId = this.identifierService.getCellarUUIDIfExists(pid);
            if (StringUtils.isNotBlank(cellarId)) { // cellar id exists
                cellarIdBase = CellarIdUtils.getCellarIdBase(cellarId);

                if (!cellarIdBase.equals(cellarId)) {
                    return new Identifier(cellarIdBase, null);
                }
            }

            return new Identifier(cellarId, Arrays.asList(pid));
        }
    }
}
