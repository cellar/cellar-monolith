/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.quad.impl
 *             FILE : BaseModelQuadService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Sep 6, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.quad.impl;

import com.google.common.collect.Lists;
import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.EmbargoDatabaseService;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData;
import eu.europa.ec.opoce.cellar.cmr.model.IFacetedModel;
import eu.europa.ec.opoce.cellar.cmr.model.ModelUtils;
import eu.europa.ec.opoce.cellar.cmr.quad.ModelQuadService;
import eu.europa.ec.opoce.cellar.cmr.service.IInverseRelationService;
import eu.europa.ec.opoce.cellar.common.closure.PClosure;
import eu.europa.ec.opoce.cellar.common.retry.IRetryStrategy;
import eu.europa.ec.opoce.cellar.common.util.CellarIdUtils;
import eu.europa.ec.opoce.cellar.common.util.DigitalObjectUtils;
import eu.europa.ec.opoce.cellar.common.util.Pair;
import eu.europa.ec.opoce.cellar.core.ConvertHistoryMap;
import eu.europa.ec.opoce.cellar.database.CmrTableName;
import eu.europa.ec.opoce.cellar.database.IRDFStoreRelationalGateway;
import eu.europa.ec.opoce.cellar.database.OracleFactory;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.feed.domain.enums.FeedItemType;
import eu.europa.ec.opoce.cellar.feed.domain.enums.FeedPriority;
import eu.europa.ec.opoce.cellar.feed.domain.impl.ingestion.IngestionHistory;
import eu.europa.ec.opoce.cellar.feed.service.HistoryService;
import eu.europa.ec.opoce.cellar.ontology.service.OntologyService;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import oracle.spatial.rdf.client.jena.Oracle;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.jena.graph.BlankNodeId;
import org.apache.jena.graph.GraphUtil;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.graph.Triple;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEventPublisher;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import static eu.europa.ec.opoce.cellar.database.CmrTableName.CMR_INVERSE;
import static eu.europa.ec.opoce.cellar.database.CmrTableName.CMR_INVERSE_EMBARGO;
import static eu.europa.ec.opoce.cellar.database.CmrTableName.CMR_METADATA;
import static eu.europa.ec.opoce.cellar.database.CmrTableName.CMR_METADATA_EMBARGO;
import static eu.europa.ec.opoce.cellar.jena.oracle.OraTemplate.executeAndKeepOpen;

/**
 * <class_description> The base implementation of {@link eu.europa.ec.opoce.cellar.cmr.quad.ModelQuadService}.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Sep 6, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public abstract class BaseModelQuadService implements ModelQuadService {

    @SuppressWarnings("unused")
    private static final Logger LOG = LogManager.getLogger(BaseModelQuadService.class);

    @Autowired
    private OracleFactory oracleFactory;

    @Autowired
    @Qualifier("cmrMetadataRelationalGateway")
    private IRDFStoreRelationalGateway cmrMetadataRelationalGateway;

    @Autowired
    @Qualifier("cellarConfiguration")
    protected ICellarConfiguration cellarConfiguration;

    @Autowired
    private EmbargoDatabaseService embargoDatabaseService;

    @Autowired
    @Qualifier("pidManagerService")
    private IdentifierService identifierService;

    @Autowired
    private CellarResourceDao cellarResourceDao;

    @Autowired
    private OntologyService ontologyService;

    @Autowired
    private HistoryService historyService;

    @Autowired
    private IInverseRelationService inverseRelationService;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    /**
     * Build and return a new {@link eu.europa.ec.opoce.cellar.common.retry.IRetryStrategy}
     *
     * @param oracle the Oracle instance to connect to
     * @return the i retry strategy
     */
    protected abstract IRetryStrategy<Connection> newRetryStrategy(final Oracle oracle);

    /**
     * <p>updateDataInOracle.</p>
     *
     * @param data a {@link eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData} object.
     */
    @Override
    public void updateDataInOracle(final CalculatedData data) {
        executeAndKeepOpen(this.oracleFactory.getOracle(), oracle -> {
            newRetryStrategy(oracle).retry(connection -> {
                // collect info
                final StructMap structMap = data.getStructMap();

                final DigitalObject digitalObject = structMap.getDigitalObject();
                final List<DigitalObject> allChilds = digitalObject.getAllChilds(true);

                final Map<DigitalObject, Model> newDirectAndInferredSnippets = data.getDirectAndInferredSnippetsForOracle();
                final IFacetedModel existingDirectAndInferredModel = data.getExistingDirectAndInferredModel();
                final Map<Pair<Triple, String>, String> existingDirectQuads = existingDirectAndInferredModel.asQuadruplePerRowid();

                final Map<DigitalObject, Model> newInverseSnippets = data.getInverseSnippetsForOracle();
                final IFacetedModel existingInverseModel = data.getExistingInverseModel();
                final Map<Pair<Triple, String>, String> existingInverseQuads = existingInverseModel.asQuadruplePerRowid();

                internalDeleteDigitalObjects(connection, data.getRemovedDigitalObjects());

                final List<EmbargoableObject> embargoableObjects = new ArrayList<>();
                final List<EmbargoableObject> historizableObjects = new ArrayList<>();
                for (final DigitalObject child : allChilds) {
                    final boolean toBeEmbargoed = DigitalObjectUtils.isUnderEmbargo(child);
                    final boolean isSavedPrivate = embargoDatabaseService.isMetadataSavedPrivate(child);
                    final boolean isSavedPublic = embargoDatabaseService.isMetadataSavedPublic(child);

                    final ContentIdentifier cellarId = child.getCellarId();
                    final String identifier = cellarId.getIdentifier();

                    // case object exists in both the embargo and disembargo tables -> error
                    if (isSavedPrivate && isSavedPublic) {
                        throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.EMBARGO_SERVICE_ERROR)
                                .withMessage("The resource {} is inconsistently available in public and private repositories.")
                                .withMessageArgs(identifier).build();
                    }

                    // case object does not exists anywhere -> create it where it belongs and create history it in case it is to be embargoed
                    else if (!isSavedPrivate && !isSavedPublic) {
                        updateDirectAndInverse(connection, child, newDirectAndInferredSnippets, existingDirectQuads,
                                newInverseSnippets, existingInverseQuads, toBeEmbargoed);
                        if (toBeEmbargoed) {
                            historizableObjects
                                    .add(createEmbargoableObject(identifier, null, child.getType(), false, true, null, child.getManifestationMimeType())); //TODO AJ embargo
                        }
                    }

                    // case object must be moved out of embargo -> move and create history it
                    else if (isSavedPrivate && !toBeEmbargoed) {
                        updateDirectAndInverse(connection, child, newDirectAndInferredSnippets, existingDirectQuads,
                                newInverseSnippets, existingInverseQuads, true);
                        final EmbargoableObject object = createEmbargoableObject(identifier, null, child.getType(), true, false, //TODO AJ embargo
                                null, child.getManifestationMimeType());
                        embargoableObjects.add(object);
                        historizableObjects.add(object);
                    }

                    // case it must be moved to embargo -> move and create history it
                    else if (isSavedPublic && toBeEmbargoed) {
                        updateDirectAndInverse(connection, child, newDirectAndInferredSnippets, existingDirectQuads,
                                newInverseSnippets, existingInverseQuads, false);
                        final EmbargoableObject object = createEmbargoableObject(identifier, null, child.getType(), false, true, //TODO AJ embargo
                                null, child.getManifestationMimeType());
                        embargoableObjects.add(object);
                        historizableObjects.add(object);
                    }

                    // case object must be left in its original state -> just update it
                    else {
                        updateDirectAndInverse(connection, child, newDirectAndInferredSnippets, existingDirectQuads,
                                newInverseSnippets, existingInverseQuads, toBeEmbargoed);
                    }
                }

                // move and create history objects
                moveAndCreateHistory(embargoableObjects, historizableObjects, connection, false);
            });
        });
    }

    /**
     * Update data in oracle.
     *
     * @param underEmbargo             the under embargo
     * @param metadataClustersToDelete the metadata clusters to delete
     * @param inverseClustersToDelete  the inverse clusters to delete
     * @param metadataToAdd            the metadata to add
     * @param inverseToAdd             the inverse to add
     * @see eu.europa.ec.opoce.cellar.cmr.quad.ModelQuadService#updateDataInOracle(boolean, java.util.List, java.util.List, java.util.Map, java.util.Map)
     */
    @Override
    public void updateDataInOracle(final boolean underEmbargo, final List<String> metadataClustersToDelete,
                                   final List<String> inverseClustersToDelete, final Map<String, Model> metadataToAdd, final Map<String, Model> inverseToAdd) {
        executeAndKeepOpen(this.oracleFactory.getOracle(), oracle -> {
            newRetryStrategy(oracle).retry(connection -> {
                cmrMetadataRelationalGateway.deleteContexts(connection, metadataClustersToDelete, CMR_METADATA.getEmbargoTable(underEmbargo));
                cmrMetadataRelationalGateway.deleteContexts(connection, inverseClustersToDelete, CMR_INVERSE.getEmbargoTable(underEmbargo));

                List<Pair<Triple, String>> quads = ModelUtils.getQuadsWithString(metadataToAdd);
                cmrMetadataRelationalGateway.insert(connection, quads, CMR_METADATA.getEmbargoTable(underEmbargo));

                quads = ModelUtils.getQuadsWithString(inverseToAdd);
                cmrMetadataRelationalGateway.insert(connection, quads, CMR_INVERSE.getEmbargoTable(underEmbargo));
            });
        });
    }

    /**
     * Update data in oracle.
     *
     * @param underEmbargo     the under embargo
     * @param clustersToDelete the metadata clusters to delete
     * @param metadataToAdd    the metadata to add
     * @param inverseToAdd     the inverse to add
     * @see eu.europa.ec.opoce.cellar.cmr.quad.ModelQuadService#updateDataInOracle(boolean, java.util.List, java.util.List, java.util.Map, java.util.Map)
     */
    @Override
    public void updateDataInOracle(final boolean underEmbargo, final List<String> clustersToDelete,
                                   final Map<String, Model> metadataToAdd, final Map<String, Model> inverseToAdd) {
        executeAndKeepOpen(this.oracleFactory.getOracle(), oracle -> {
            newRetryStrategy(oracle).retry(connection -> {
                cmrMetadataRelationalGateway.deleteContexts(connection, clustersToDelete,
                        CMR_METADATA.getEmbargoTable(underEmbargo), CMR_INVERSE.getEmbargoTable(underEmbargo));

                List<Pair<Triple, String>> quads = ModelUtils.getQuadsWithString(metadataToAdd);
                cmrMetadataRelationalGateway.insert(connection, quads, CMR_METADATA.getEmbargoTable(underEmbargo));

                quads = ModelUtils.getQuadsWithString(inverseToAdd);
                cmrMetadataRelationalGateway.insert(connection, quads, CMR_INVERSE.getEmbargoTable(underEmbargo));
            });
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moveEmbargoedDataInOracle(final String cellarId, final boolean toPrivate, final Date embargoDate, final Date lastModificationDate) {
        executeAndKeepOpen(this.oracleFactory.getOracle(), oracle -> {
            newRetryStrategy(oracle).retry(connection ->
                    move(cellarId, toPrivate, embargoDate, lastModificationDate, connection)
            );
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moveEmbargoedDataInOracle(final Collection<String> cellarIds, final boolean toPrivate,
                                          final Date embargoDate, final Date lastModificationDate) {
        executeAndKeepOpen(this.oracleFactory.getOracle(), oracle -> {
            newRetryStrategy(oracle).retry(connection ->
                    groupMove(cellarIds, toPrivate, embargoDate, lastModificationDate, connection)
            );
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteContexts(final Collection<Identifier> identifiers) {
        executeAndKeepOpen(this.oracleFactory.getOracle(), (PClosure<Oracle>) oracle ->
                newRetryStrategy(oracle).retry(
                        connection -> internalDeleteContexts(connection, identifiers)
                )
        );
    }

    private void internalDeleteDigitalObjects(final Connection connection, final Collection<DigitalObject> dos) {
        // builds the identifiers list based on the digital object's list
        final Collection<Identifier> identifiers = dos.stream()
                .filter(currDo -> currDo.getCellarId() != null)
                .map(currDo -> {
                    final Identifier currId = new Identifier(currDo.getCellarId().getIdentifier());
                    currId.setPids(currDo.getContentids().stream().map(ContentIdentifier::getIdentifier).collect(Collectors.toList()));
                    return currId;
                })
                .collect(Collectors.toList());

        // removes the digital objects
        internalDeleteContexts(connection, identifiers);
    }

    private void internalDeleteContexts(final Connection connection, final Collection<Identifier> identifiers) {
        // removes contexts from metadata and inverses
        final List<String> cellarIds = identifiers.stream().map(Identifier::getCellarId).collect(Collectors.toList());
        cmrMetadataRelationalGateway.deleteContexts(connection, cellarIds,
                CMR_METADATA, CMR_METADATA_EMBARGO, CMR_INVERSE, CMR_INVERSE_EMBARGO);

        // updates the inverse_diss table for all the objects having inverse relations to removed objects
        inverseRelationService.getInverseRelationsOnSource(identifiers).forEach(inverseRelation ->
                cmrMetadataRelationalGateway.syncDissTable(connection, inverseRelation.getTarget(), null, CmrTableName.CMR_INVERSE)
        );
    }

    private void moveAndCreateHistory(final List<EmbargoableObject> movableObjects, final List<EmbargoableObject> historizableObjects,
                                      final Connection connection, final boolean internal) {
        this.move(movableObjects, connection);
        this.createHistory(historizableObjects, internal);
    }

    private void move(final List<EmbargoableObject> objects, final Connection connection) {
        // data removal
        collectRemovableObjects(objects).forEach((isFromPrivate, cellarIds) ->
                cmrMetadataRelationalGateway.deleteContexts(connection, cellarIds, CMR_METADATA.getEmbargoTable(isFromPrivate))
        );

        // data migration to/from embargo
        collectMovableObjects(objects).forEach((isFromPrivate, cellarIds) -> {
            cmrMetadataRelationalGateway.moveContexts(connection, cellarIds, CMR_METADATA.getEmbargoTable(isFromPrivate), CMR_METADATA.getEmbargoTable(!isFromPrivate));
            cmrMetadataRelationalGateway.moveContexts(connection, cellarIds, CMR_INVERSE.getEmbargoTable(isFromPrivate), CMR_INVERSE.getEmbargoTable(!isFromPrivate));
        });

        // data insertion
        collectInsertableObjects(objects).forEach((isToPrivate, pairsList) -> {
            final List<Pair<Triple, String>> quads = pairsList.stream().flatMap(l -> l.getQuads().stream()).collect(Collectors.toList());
            cmrMetadataRelationalGateway.insert(connection, quads, CMR_METADATA.getEmbargoTable(isToPrivate));
        });
    }

    static Map<Boolean, List<String>> collectRemovableObjects(final List<EmbargoableObject> objects) {
        // data removal
        // streams thru the embargoable objects, selects those with some metadata, split them up in 2 sets (embargoed and disembargoed),
        // collects the cellar ids of each set, and finally removes them
        return objects.stream()
                .filter(object -> object.getMetadata() != null)
                .collect(Collectors.partitioningBy(
                        EmbargoableObject::isFromPrivate,
                        Collectors.mapping(EmbargoableObject::getCellarId, Collectors.toList())
                ));
    }

    static Map<Boolean, List<String>> collectMovableObjects(final List<EmbargoableObject> objects) {
        // data migration to/from embargo
        // streams thru the embargoable objects, selects those being either embargoed or disembargoed (not both!),
        // split them up in 2 sets (embargoed and non-embargoed), collects the cellar ids of each set, and finally moves them
        return objects.stream()
                .filter(object -> object.isToPrivate() != object.isFromPrivate())
                .collect(Collectors.partitioningBy(
                        EmbargoableObject::isFromPrivate,
                        Collectors.mapping(EmbargoableObject::getCellarId, Collectors.toList())
                ));
    }

    static Map<Boolean, List<EmbargoableObject>> collectInsertableObjects(final List<EmbargoableObject> objects) {
        // data insertion
        // streams thru the embargoable objects, selects those with some metadata, split them up in 2 sets (embargoed and non-embargoed),
        // collects the quads of each set, and finally inserts them
        return objects.stream()
                .filter(object -> object.getMetadata() != null)
                .collect(Collectors.partitioningBy(EmbargoableObject::isToPrivate));
    }

    private void createHistory(final List<EmbargoableObject> historizableObjects, final boolean internal) {
        this.storeEmbargoHistoryRecord(historizableObjects, false, internal);
        this.storeEmbargoHistoryRecord(historizableObjects, true, internal);
    }

    private void storeEmbargoHistoryRecord(final List<EmbargoableObject> historizableObjects, final boolean toPrivate, final boolean internal) {
        historizableObjects.stream()
                .filter(historizableObject -> toPrivate == historizableObject.isToPrivate())
                .map(o -> {
                    IngestionHistory historyObject = new IngestionHistory(o.getCellarId());
                    historyObject.setWemiClass(o.getType());
                    historyObject.setRelatives(false);
                    historyObject.setActionType(toPrivate ? FeedItemType.embargo : FeedItemType.disembargo);
                    historyObject.setFeedActionType(internal ? resolveFeedActionType(o): FeedItemType.no_feed);
                    historyObject.setPriority(FeedPriority.EMBARGO);
                    historyObject.setType(o.getManifestationType());
                    historyObject.setClasses(String.join(",", historyService.getOwlClassesForCellarId(o.getCellarId(), o.getInferredVersion())));
                    historyObject.setIdentifiers(String.join(",", this.historyService.getProductionIdentifierNamesForCellarId(o.getCellarId())));
                    return historyObject;
                })
                .forEach(h -> {
                    historyService.store(h);
                    applicationEventPublisher.publishEvent(h);
                });
    }

    /**
     * Resolves feed action type
     *
     * @param embargoableObject the embargoable object
     * @return the desired FeedItemType
     */
    private FeedItemType resolveFeedActionType(final EmbargoableObject embargoableObject) {
        boolean wasUnderEmbargo = embargoableObject.isFromPrivate();
        boolean willBeUnderEmbargo = embargoableObject.isToPrivate();

        if(wasUnderEmbargo && !willBeUnderEmbargo) {
            return FeedItemType.create;
        }
        if(!wasUnderEmbargo && willBeUnderEmbargo) {
            return FeedItemType.delete;
        }
        if(!wasUnderEmbargo) {
            return FeedItemType.update;
        }

        return FeedItemType.no_feed;
    }

    private EmbargoableObject createEmbargoableObject(final String cellarId, final String inferredVersion, final DigitalObjectType doType, final boolean fromPrivate,
                                                      final boolean toPrivate, final Model metadata, final String manifestationType) {
        EmbargoableObject embargoableObject = new EmbargoableObject(cellarId, doType);
        embargoableObject.setFromPrivate(fromPrivate);
        embargoableObject.setToPrivate(toPrivate);
        embargoableObject.setMetadata(metadata);
        embargoableObject.setManifestationType(manifestationType);
        embargoableObject.setInferredVersion(inferredVersion);
        return embargoableObject;
    }

    /**
     * The Class EmbargoableObject.
     * <class_description> A meaningful description of the class that will be
     * displayed as the class summary on the JavaDoc package page.
     * <br/><br/>
     * <notes> Includes guaranteed invariants, usage instructions and/or examples,
     * reminders about desired improvements, etc.
     * <br/><br/>
     * ON : 17 août 2016
     *
     * @author ARHS Developments
     * @version $Revision$
     */
    static class EmbargoableObject {

        private String cellarId;

        private String inferredVersion;

        private DigitalObjectType type;

        private boolean fromPrivate;

        private boolean toPrivate;

        private Model metadata;

        private String manifestationType;

        EmbargoableObject(final String cellarId, final DigitalObjectType type) {
            this.cellarId = cellarId;
            this.type = type;
        }

        /**
         * Gets the cellar id.
         *
         * @return the cellar id
         */
        public String getCellarId() {
            return cellarId;
        }

        /**
         * Gets the type.
         *
         * @return the type
         */
        public DigitalObjectType getType() {
            return type;
        }

        /**
         * Checks if is from private.
         *
         * @return true, if is from private
         */
        boolean isFromPrivate() {
            return fromPrivate;
        }

        /**
         * Sets the from private.
         *
         * @param fromPrivate the new from private
         */
        void setFromPrivate(final boolean fromPrivate) {
            this.fromPrivate = fromPrivate;
        }

        /**
         * Checks if is to private.
         *
         * @return true, if is to private
         */
        boolean isToPrivate() {
            return toPrivate;
        }

        /**
         * Sets the to private.
         *
         * @param toPrivate the new to private
         */
        void setToPrivate(final boolean toPrivate) {
            this.toPrivate = toPrivate;
        }

        /**
         * Gets the metadata.
         *
         * @return the metadata
         */
        public Model getMetadata() {
            return metadata;
        }

        /**
         * Sets the metadata.
         *
         * @param metadata the new metadata
         */
        public void setMetadata(final Model metadata) {
            this.metadata = metadata;
        }

        /**
         * Gets the quads.
         *
         * @return the quads
         */
        public List<Pair<Triple, String>> getQuads() {
            final List<Pair<Triple, String>> result = new ArrayList<>();
            final Model model = getMetadata();
            final String cluster = getCellarId();
            if (model != null) {
                final ExtendedIterator<Triple> iterator = GraphUtil.findAll(model.getGraph());
                try {
                    while (iterator.hasNext()) {
                        final Triple triple = iterator.next();
                        result.add(new Pair<>(triple, cluster));
                    }
                } finally {
                    iterator.close();
                }

            }

            return renameBlankNodes(result, cluster);
        }

        public String getManifestationType() {
            return manifestationType;
        }

        public void setManifestationType(final String manifestationType) {
            this.manifestationType = manifestationType;
        }

        public String getInferredVersion() {
            return inferredVersion;
        }

        public void setInferredVersion(String inferredVersion) {
            this.inferredVersion = inferredVersion;
        }
    }

    /**
     * Move.
     *
     * @param cellarId             the cellar id
     * @param toPrivate            the to private
     * @param embargoDate          the embargo date
     * @param lastModificationDate the last modification date
     * @param connection           the connection
     */
    private void move(final String cellarId, final boolean toPrivate, final Date embargoDate,
                      final Date lastModificationDate, final Connection connection) {
        final HashMap<String, ArrayList<String>> manifestationGroup = new HashMap<>();
        final HashMap<String, DigitalObjectType> notManifestationsGroup = new HashMap<>();
        //separate manifestations from the rest - the item metadata is stored in the manifestation metadata
        final List<Identifier> subTree = identifierService.getTreeIdentifiers(cellarId);
        for (final Identifier identifier : subTree) {
            final String identifierCellarId = identifier.getCellarId();
            populateGroupsByDigitalObjectType(manifestationGroup, notManifestationsGroup, identifierCellarId);
        }
        internalMove(toPrivate, embargoDate, lastModificationDate, connection, manifestationGroup, notManifestationsGroup);
    }

    /**
     * Group move.
     *
     * @param cellarIds            the cellar ids
     * @param toPrivate            the to private
     * @param embargoDate          the embargo date
     * @param lastModificationDate the last modification date
     * @param connection           the connection
     */
    private void groupMove(final Collection<String> cellarIds, final boolean toPrivate, final Date embargoDate,
                           final Date lastModificationDate, final Connection connection) {
        final HashMap<String, ArrayList<String>> manifestationGroup = new HashMap<>();
        final HashMap<String, DigitalObjectType> notManifestationsGroup = new HashMap<>();
        //separate manifestations from the rest - the item metadata is stored in the manifestation metadata
        cellarIds.forEach(cellarId ->
                populateGroupsByDigitalObjectType(manifestationGroup, notManifestationsGroup, cellarId)
        );

        internalMove(toPrivate, embargoDate, lastModificationDate, connection, manifestationGroup, notManifestationsGroup);
    }

    private void populateGroupsByDigitalObjectType(final HashMap<String, ArrayList<String>> manifestationGroup,
                                                   final HashMap<String, DigitalObjectType> notManifestationsGroup, String cellarId) {
        final CellarResource cellarResource = cellarResourceDao.findCellarId(cellarId);
        final DigitalObjectType cellarType = cellarResource.getCellarType();
        switch (cellarType) {
            case MANIFESTATION: {
                manifestationGroup.computeIfAbsent(cellarId, k -> new ArrayList<>());
                break;
            }
            case ITEM: {
                final String parentCellarId = CellarIdUtils.getParentCellarId(cellarId);
                ArrayList<String> itemsCellarIds = manifestationGroup.computeIfAbsent(parentCellarId, k -> new ArrayList<>());
                itemsCellarIds.add(cellarId);
                break;
            }
            default:
                notManifestationsGroup.put(cellarId, cellarType);
                break;
        }
    }

    private void internalMove(final boolean toPrivate, final Date embargoDate, final Date lastModificationDate, final Connection connection,
                              final HashMap<String, ArrayList<String>> manifestationGroup, final HashMap<String, DigitalObjectType> notManifestationsGroup) {
        final Collection<String> lastmodificationdateProperties = Collections.singletonList(CellarProperty.cmr_lastmodificationdate);
        List<EmbargoableObject> embargoableObjects = new ArrayList<>();
        for (final Entry<String, DigitalObjectType> entry : notManifestationsGroup.entrySet()) {
            final String key = entry.getKey();
            final DigitalObjectType value = entry.getValue();
            final Collection<String> embargoProperties = this.ontologyService.getEmbargoDateProperties(value, true);
            final boolean wasStoredInPrivateDatabase = this.embargoDatabaseService.isMetadataSavedPrivate(key);
            Model metadata = this.embargoDatabaseService.getMetadata(key, wasStoredInPrivateDatabase);
            final String uri = identifierService.getUri(key);
            ModelUtils.updateDateModel(uri, embargoDate, metadata, embargoProperties);
            ModelUtils.updateDateModel(uri, lastModificationDate, metadata, lastmodificationdateProperties);
            final EmbargoableObject createEmbargoableObject = createEmbargoableObject(key, null, value, wasStoredInPrivateDatabase, toPrivate, //TODO AJ embargo
                    metadata, "");
            embargoableObjects.add(createEmbargoableObject);
        }

        moveAndCreateHistory(embargoableObjects, embargoableObjects, connection, true);
        embargoableObjects.forEach(embargoableObject -> JenaUtils.closeQuietly(embargoableObject.getMetadata()));

        final Collection<String> manifestationEmbargoProperties = ontologyService.getEmbargoDateProperties(DigitalObjectType.MANIFESTATION, true);
        final Collection<String> itemEmbargoProperties = ontologyService.getEmbargoDateProperties(DigitalObjectType.ITEM, true);
        embargoableObjects = new ArrayList<>();
        for (final Entry<String, ArrayList<String>> entry : manifestationGroup.entrySet()) {
            Model metadata;
            final String manifestationCellarId = entry.getKey();
            final List<String> itemsCellarIds = entry.getValue();
            final boolean wasStoredInPrivateDatabase = this.embargoDatabaseService.isMetadataSavedPrivate(manifestationCellarId);
            metadata = this.embargoDatabaseService.getMetadata(manifestationCellarId, wasStoredInPrivateDatabase);
            final String uri = identifierService.getUri(manifestationCellarId);
            ModelUtils.updateDateModel(uri, embargoDate, metadata, manifestationEmbargoProperties);
            ModelUtils.updateDateModel(uri, lastModificationDate, metadata, lastmodificationdateProperties);
            for (final String itemCellarId : itemsCellarIds) {
                final String itemCellarIdUri = identifierService.getUri(itemCellarId);
                ModelUtils.updateDateModel(itemCellarIdUri, embargoDate, metadata, itemEmbargoProperties);
                ModelUtils.updateDateModel(itemCellarIdUri, lastModificationDate, metadata, lastmodificationdateProperties);
            }
            CellarResource cellarResource = this.cellarResourceDao.findCellarId(manifestationCellarId);
            final EmbargoableObject createEmbargoableObject = createEmbargoableObject(manifestationCellarId, null, //TODO AJ embargo
                    DigitalObjectType.MANIFESTATION, wasStoredInPrivateDatabase, toPrivate, metadata, cellarResource.getMimeType());
            embargoableObjects.add(createEmbargoableObject);
        }
        moveAndCreateHistory(embargoableObjects, embargoableObjects, connection, true);
        embargoableObjects.forEach(embargoableObject -> JenaUtils.closeQuietly(embargoableObject.getMetadata()));
    }

    /**
     * Update cmr tables.
     *
     * @param connection    the connection
     * @param digitalObject the digital object
     * @param newData       the new data
     * @param existingQuads the existing quads
     * @param table         the table
     */
    private void updateCmrTables(final Connection connection, final DigitalObject digitalObject, final Model newData,
                                 final Map<Pair<Triple, String>, String> existingQuads, final CmrTableName table) {
        if (this.cellarConfiguration.isCellarDatabaseRdfIngestionOptimizationEnabled()) {
            updateQuads(connection, digitalObject, newData, existingQuads, table);
        } else {
            removeAndAddQuads(connection, digitalObject, newData, table);
        }
    }

    /**
     * <p>removeAndAddQuads removes all triples and add all triples in context in the table with name.</p>
     *
     * @param connection    a {@link java.sql.Connection} object.
     * @param digitalObject the digital object
     * @param model         the model
     * @param table         a {@link eu.europa.ec.opoce.cellar.database.CmrTableName} object.
     */
    private void removeAndAddQuads(final Connection connection, final DigitalObject digitalObject, final Model model,
                                   final CmrTableName table) {
        final ContentIdentifier cellarId = digitalObject.getCellarId();
        final String identifier = cellarId.getIdentifier();
        this.cmrMetadataRelationalGateway.deleteContexts(connection, Lists.newArrayList(identifier), table);

        final List<Pair<Triple, String>> quads = getQuads(digitalObject, model);
        this.cmrMetadataRelationalGateway.insert(connection, quads, table);
    }

    /**
     * <p>updateMetadataQuads remove only the removed triples out of a context and add only the new ones in that context in metadata table.</p>
     *
     * @param connection    a {@link java.sql.Connection} object.
     * @param digitalObject the digital object
     * @param model         the model
     * @param oldQuads      a {@link java.util.Map} object.
     * @param table         the table
     */
    private void updateQuads(final Connection connection, final DigitalObject digitalObject, final Model model,
                             final Map<Pair<Triple, String>, String> oldQuads, final CmrTableName table) {
        // collect informations
        final String identifier = digitalObject.getCellarId().getIdentifier();

        final List<Pair<Triple, String>> addQuads = new ArrayList<>();
        final List<Pair<Triple, String>> removeQuads = new ArrayList<>();

        final Collection<Pair<Triple, String>> oldQuadList = CollectionUtils.select(oldQuads.keySet(),
                object -> identifier.equals(object.getTwo()));
        final Collection<Pair<Triple, String>> newQuadList = getQuads(digitalObject, model);
        final Collection<Pair<Triple, String>> intersection = CollectionUtils.intersection(oldQuadList, newQuadList);
        addQuads.addAll(CollectionUtils.subtract(newQuadList, intersection));//the intersection does not change
        removeQuads.addAll(CollectionUtils.subtract(oldQuadList, intersection));

        // remove from the main table the delta of quads to remove, and add the delta of quads to add (without synchronizing the dissemination table)
        this.cmrMetadataRelationalGateway.remove(connection, removeQuads, oldQuads, table, false);
        this.cmrMetadataRelationalGateway.insert(connection, addQuads, table, false);

        // Synchronize the dissemination table relatively to the contexts involved in all quads
        // The dissemination will be update during the disembargo. If the digital object is under
        // embargo during the ingestion, we don't update the table CMR_INVERSE_DISS
        if (digitalObject.getEmbargoDate() == null || !digitalObject.getEmbargoDate().after(new Date())) {
            final List<Pair<Triple, String>> allQuads = new ArrayList<>(addQuads);
            allQuads.addAll(removeQuads);
            this.cmrMetadataRelationalGateway.syncDissTable(connection, allQuads, table);
        }
    }

    /**
     * Gets the quads.
     *
     * @param digitalObject the digital object
     * @param model         the model
     * @return the quads
     */
    private static List<Pair<Triple, String>> getQuads(final DigitalObject digitalObject, final Model model) {
        final List<Pair<Triple, String>> quads = new ArrayList<>(1000);
        quads.addAll(getQuads(digitalObject.getCellarId().getIdentifier(), model));
        return quads;
    }

    /**
     * <p>getQuads get all quads that are can be found in the given Model, set context to clustername.</p>
     *
     * @param cluster the cluster where the triples of given model need to be put in
     * @param model   the model that contains the triples
     * @return list of quads (triple and context)
     */
    private static List<Pair<Triple, String>> getQuads(final String cluster, final Model model) {
        final List<Pair<Triple, String>> result = new ArrayList<>();
        if (model != null) {
            final ExtendedIterator<Triple> iterator = GraphUtil.findAll(model.getGraph());
            try {
                while (iterator.hasNext()) {
                    final Triple triple = iterator.next();
                    result.add(new Pair<>(triple, cluster));
                }
            } finally {
                iterator.close();
            }
        }

        return renameBlankNodes(result, cluster);
    }

    /**
     * <p>renameBlankNodes so that they are unique in the complete database.</p>
     *
     * @param quads   a {@link java.util.List} object.
     * @param cluster a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    private static List<Pair<Triple, String>> renameBlankNodes(final List<Pair<Triple, String>> quads, final String cluster) {
        final List<Pair<Triple, String>> newQuads = new ArrayList<>();
        final NodeConversionMap nodeConversionMap = new NodeConversionMap(cluster);
        for (final Pair<Triple, String> quad : quads) {
            final Triple triple = quad.getOne();
            final Node subject = triple.getSubject();
            final Node object = triple.getObject();
            final Node convertedValue = nodeConversionMap.getConvertedValue(subject);
            final Node convertedObject = nodeConversionMap.getConvertedValue(object);
            final Triple one = new Triple(convertedValue, triple.getPredicate(), convertedObject);
            final Pair<Triple, String> add = new Pair<>(one, quad.getTwo());
            newQuads.add(add);
        }
        return newQuads;
    }

    private void updateDirectAndInverse(final Connection connection, final DigitalObject digitalObject,
                                        final Map<DigitalObject, Model> newDirectAndInferredSnippets,
                                        final Map<Pair<Triple, String>, String> existingDirectQuads, final Map<DigitalObject, Model> newInverseSnippets,
                                        final Map<Pair<Triple, String>, String> existingInverseQuads, final boolean privateTables) {
        final CmrTableName direct = CMR_METADATA.getEmbargoTable(privateTables);
        final CmrTableName inverse = CMR_INVERSE.getEmbargoTable(privateTables);
        updateCmrTable(connection, digitalObject, newDirectAndInferredSnippets, existingDirectQuads, direct);
        updateCmrTable(connection, digitalObject, newInverseSnippets, existingInverseQuads, inverse);
    }

    private void updateCmrTable(final Connection connection, final DigitalObject digitalObject,
                                final Map<DigitalObject, Model> newSnippets, final Map<Pair<Triple, String>, String> existingQuads,
                                final CmrTableName tableName) {
        final Model model = newSnippets.get(digitalObject);
        final String identifier = digitalObject.getCellarId().getIdentifier();
        if (model != null) {
            final Map<Pair<Triple, String>, String> existingSelection = new HashMap<>();
            for (final Entry<Pair<Triple, String>, String> entry : existingQuads.entrySet()) {
                final Pair<Triple, String> key = entry.getKey();
                final String value = entry.getValue();
                if (key.getTwo().equals(identifier)) {
                    existingSelection.put(key, value);
                }
            }
            updateCmrTables(connection, digitalObject, model, existingSelection, tableName);
        }
    }

    /**
     * <class_description> A meaningful description of the class that will be
     * displayed as the class summary on the JavaDoc package page.
     * <br/><br/>
     * <notes> Includes guaranteed invariants, usage instructions and/or examples,
     * reminders about desired improvements, etc.
     * <br/><br/>
     * ON : Sep 2, 2015
     *
     * @author ARHS Developments
     * @version $Revision$$
     */
    private static class NodeConversionMap extends ConvertHistoryMap<Node, Node> {

        /**
         * The identifier.
         */
        private final String identifier;

        /**
         * The number.
         */
        private int number = 0;

        /**
         * Instantiates a new node conversion map.
         *
         * @param context the context
         */
        private NodeConversionMap(final String context) {
            this.identifier = context.startsWith(IdentifierService.PREFIX_CELLAR)
                    ? context.substring(IdentifierService.PREFIX_CELLAR.length()) : context;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Node convert(final Node input) {
            if (input.isBlank()) {
                return NodeFactory.createBlankNode(BlankNodeId.create(this.number++ + ".id." + this.identifier));
            }
            return input;
        }
    }

}
