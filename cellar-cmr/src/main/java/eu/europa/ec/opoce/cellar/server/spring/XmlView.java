package eu.europa.ec.opoce.cellar.server.spring;

import java.nio.charset.StandardCharsets;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.util.FileCopyUtils;
import org.springframework.web.servlet.view.AbstractUrlBasedView;

/**
 * <p>XmlView class.</p>
 */
public class XmlView extends AbstractUrlBasedView {

    private final String xml;

    /**
     * <p>Constructor for XmlView.</p>
     *
     * @param xml a {@link java.lang.String} object.
     */
    public XmlView(String xml) {
        this.xml = xml;
        setContentType("text/xml;charset=UTF-8");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        byte[] output = xml.getBytes(StandardCharsets.UTF_8);

        response.setContentType(getContentType());
        response.setContentLength(output.length);

        FileCopyUtils.copy(output, response.getOutputStream());
    }
}
