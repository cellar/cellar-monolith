package eu.europa.ec.opoce.cellar.nal.domain;

import java.util.Objects;

/**
 * <p>NalRelatedBean class.</p>
 */
public class NalRelatedBean {

    private String uri;
    private String relatedUri;

    /**
     * <p>Constructor for NalRelatedBean.</p>
     *
     * @param uri        a {@link java.lang.String} object.
     * @param relatedUri a {@link java.lang.String} object.
     */
    public NalRelatedBean(String uri, String relatedUri) {
        this.uri = uri;
        this.relatedUri = relatedUri;
    }

    /**
     * <p>Getter for the field <code>uri</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getUri() {
        return uri;
    }

    /**
     * <p>Getter for the field <code>relatedUri</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getRelatedUri() {
        return relatedUri;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NalRelatedBean)) return false;
        NalRelatedBean that = (NalRelatedBean) o;
        return Objects.equals(getUri(), that.getUri()) &&
                Objects.equals(getRelatedUri(), that.getRelatedUri());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUri(), getRelatedUri());
    }

    @Override
    public String toString() {
        return "NalRelatedBean{" +
                "uri='" + uri + '\'' +
                ", relatedUri='" + relatedUri + '\'' +
                '}';
    }
}
