package eu.europa.ec.opoce.cellar.database;

import org.apache.commons.lang.StringUtils;

public enum CmrTableName {

    CMR_METADATA(CmrTableMode.NORMAL), //
    CMR_METADATA_EMBARGO(CmrTableMode.EMBARGO), //

    CMR_INVERSE(CmrTableMode.NORMAL), //
    CMR_INVERSE_EMBARGO(CmrTableMode.EMBARGO), //
    CMR_INVERSE_DISS(CmrTableMode.DISS), //

    CMR_BLANK_NODE(CmrTableMode.NORMAL);

    public enum CmrTableMode {
        NORMAL, EMBARGO, DISS
    }

    private CmrTableMode mode;

    CmrTableName(final CmrTableMode mode) {
        this.mode = mode;
    }

    public CmrTableMode getMode() {
        return this.mode;
    }

    public CmrTableName getNormalTable() {
        return this.getTable(CmrTableMode.NORMAL);
    }

    public CmrTableName getEmbargoTable() {
        return this.getTable(CmrTableMode.EMBARGO);
    }

    public CmrTableName getDisseminationTable() {
        return this.getTable(CmrTableMode.DISS);
    }

    public CmrTableName getEmbargoTable(final boolean embargo) {
        return embargo ? this.getEmbargoTable() : this.getNormalTable();
    }

    private CmrTableName getTable(final CmrTableMode mode) {
        String tableName = StringUtils.removeEnd(this.toString(), "_" + CmrTableMode.EMBARGO.toString());
        tableName = StringUtils.removeEnd(tableName, "_" + CmrTableMode.DISS.toString());

        CmrTableName cmrTableName = null;
        try {
            cmrTableName = CmrTableName.valueOf(tableName + (mode == CmrTableMode.NORMAL ? "" : "_" + mode.toString()));
        } catch (IllegalArgumentException e) {
            // do nothing
        }

        return cmrTableName;
    }
}
