package eu.europa.ec.opoce.cellar.server.dissemination.converter;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.converter.HttpMessageNotWritableException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * <p>ContentStreamsHttpMessageConverter class.</p>
 */
public class ContentStreamsHttpMessageConverter extends CellarAbstractHttpMessageConverter<DisseminationContentStreams> {


    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean supports(final Class<?> clazz) {
        return DisseminationContentStreams.class.isAssignableFrom(clazz);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    protected void writeInternal(final DisseminationContentStreams dataStream, final HttpOutputMessage outputMessage)
            throws IOException, HttpMessageNotWritableException {
        if (dataStream != null && dataStream.getDisseminationContentStreamDataList().size() > 1) {
            final List<DisseminationContentStreamData> dataStreams = dataStream.getDisseminationContentStreamDataList();

            final SortedMap<String, List<DisseminationContentStreamData>> map = new TreeMap<String, List<DisseminationContentStreamData>>();
            for (final DisseminationContentStreamData disseminationContentStreamData : dataStreams) {
                final String ownerId = disseminationContentStreamData.getOwnerId();
                List<DisseminationContentStreamData> contentStreamDatas = map.get(ownerId);
                if (contentStreamDatas == null) {
                    contentStreamDatas = new ArrayList<DisseminationContentStreamData>();
                    map.put(ownerId, contentStreamDatas);
                }
                contentStreamDatas.add(disseminationContentStreamData);
            }

            //if the different order values sets a natural ordering
            final Comparator<DisseminationContentStreamData> comparatorByOrder = new Comparator<DisseminationContentStreamData>() {

                @Override
                public int compare(final DisseminationContentStreamData o1, final DisseminationContentStreamData o2) {
                    return o1.getOrder() == o2.getOrder() ? o1.getFirstPage() - o2.getFirstPage() : o1.getOrder() - o2.getOrder();
                }

            };
            //if the different order values does not set a natural ordering
            final Comparator<DisseminationContentStreamData> comparatorByDocIndex = new Comparator<DisseminationContentStreamData>() {

                @Override
                public int compare(final DisseminationContentStreamData o1, final DisseminationContentStreamData o2) {
                    return o1.getDocIndex() == o2.getDocIndex() ? o1.getFirstPage() - o2.getFirstPage()
                            : o1.getDocIndex() - o2.getDocIndex();
                }

            };

            final StringBuilder sb = new StringBuilder();
            sb.append("<html><head><title>300 Multiple-Choice Response</title></head><body> List of URI's:");
            sb.append("<ul>");
            for (final Entry<String, List<DisseminationContentStreamData>> entry : map.entrySet()) {

                final String key = entry.getKey();
                final List<DisseminationContentStreamData> value = entry.getValue();

                //the set holds unique integers
                final Set<Integer> differentOrders = new HashSet<Integer>();
                for (final DisseminationContentStreamData disseminationContentStreamData : value) {
                    differentOrders.add(disseminationContentStreamData.getOrder());
                }
                //if all order values establish a natural/absolute order 1-2-3-4
                // the number of order values will be the same as the number of content streams
                //whenever there is no order between the values order by docIndex
                boolean absoluteOrdering = false;
                if (differentOrders.size() == value.size()) {
                    absoluteOrdering = true;
                }
                if (absoluteOrdering) {
                    Collections.sort(value, comparatorByOrder);
                } else {
                    Collections.sort(value, comparatorByDocIndex);
                }

                sb.append("<li title=\"manifestation\">").append(key);
                sb.append("<ul>");
                for (final DisseminationContentStreamData data : value) {
                    sb.append("<li title=\"item\">").append("<a href=\"").append(data.getUrl()).append("\">")
                            .append("<span class=\"stream_page_physical_first\">")
                            .append(data.getFirstPage() == -1 ? "" : data.getFirstPage()).append("</span>").append("&amp;nbsp;-&amp;nbsp;")
                            .append("<span class=\"stream_page_physical_last\">").append(data.getLastPage() == -1 ? "" : data.getLastPage())
                            .append("</span>").append("&amp;nbsp;").append("<span class=\"url\">(").append(data.getUrl()).append(")</span>")
                            .append("</a>");

                    final String streamName = data.getStreamName();
                    final String streamLabel = data.getStreamLabel();
                    final Set<String> workIds = data.getWorkIds();

                    final int streamOrder = data.getOrder();
                    sb.append("<ul>");
                    if (StringUtils.isNotBlank(streamName)) {
                        if (StringUtils.isNotBlank(streamName)) {
                            sb.append("<li title=\"stream_name\">").append(streamName).append("</li>");
                        }
                    }
                    if (workIds != null && !workIds.isEmpty()) {
                        for (final String workId : workIds) {
                            sb.append("<li title=\"published_in\">").append(workId).append("</li>");
                        }
                    }
                    if (StringUtils.isNotBlank(streamLabel)) {
                        sb.append("<li title=\"stream_label\">").append(streamLabel).append("</li>");
                    }
                    if (streamOrder != -1) {
                        sb.append("<li title=\"stream_order\" id=\"streamOrder\">").append(streamOrder).append("</li>");
                    }
                    sb.append("</ul>");

                    sb.append("</li>");
                }

                sb.append("</ul>");
                sb.append("</li>");
            }
            sb.append("</ul>");

            sb.append("</body></html>");
            IOUtils.write(sb.toString().getBytes(StandardCharsets.UTF_8), outputMessage.getBody());
        }
    }
}
