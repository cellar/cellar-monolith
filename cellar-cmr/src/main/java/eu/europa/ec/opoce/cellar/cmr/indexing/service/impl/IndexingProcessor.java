/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.indexing.service.impl
 *             FILE : IndexingProcess.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 10 25, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-10-25 09:47:24 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.indexing.service.impl;

import eu.europa.ec.opoce.cellar.cl.concurrency.Concurrent;
import eu.europa.ec.opoce.cellar.cl.concurrency.IConcurrentArgsResolver;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cmr.indexing.IndexationMonitoringService;
import eu.europa.ec.opoce.cellar.cmr.indexing.processor.impl.IndexRequestProcessorManager;
import eu.europa.ec.opoce.cellar.cmr.indexing.service.IndexRequestManagerService;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestBatch;
import eu.europa.ec.opoce.cellar.common.util.ThreadUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.OffIngestionOperationType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.ExecutionStatus.Execution;

/**
 * @author ARHS Developments
 */
@Component
public class IndexingProcessor implements IConcurrentArgsResolver {

    private final IndexRequestProcessorManager indexRequestProcessorManager;
    private final IndexRequestManagerService indexRequestManagerService;
    private final ICellarConfiguration cellarConfiguration;
    private final IndexationMonitoringService indexationMonitoringService;

    @Autowired
    public IndexingProcessor(IndexRequestProcessorManager indexRequestProcessorManager, IndexRequestManagerService indexRequestManagerService,
                             ICellarConfiguration cellarConfiguration, IndexationMonitoringService indexationMonitoringService) {
        this.indexRequestProcessorManager = indexRequestProcessorManager;
        this.indexRequestManagerService = indexRequestManagerService;
        this.cellarConfiguration = cellarConfiguration;
        this.indexationMonitoringService = indexationMonitoringService;
    }

    @Concurrent(locker = OffIngestionOperationType.INDEXING)
    public void updateBatchStatusAndProceed(CmrIndexRequestBatch cmrIndexRequest) {
        indexationMonitoringService.register(cmrIndexRequest);
        indexRequestManagerService.updateBatchStatus(cmrIndexRequest, Execution);
        indexRequestManagerService.updateStructMapIndxStatusAndRespectiveDatetime(cmrIndexRequest.getCmrIndexRequests(), Execution);
        delayExecutionForTesting();
        indexRequestProcessorManager.execute(cmrIndexRequest);
    }

    @Override
    public Object[] resolveArgsForConcurrency(Object[] in) {
        Object[] lockArgs = new Object[]{};
        // resolve concurrency arguments only if concurrency is enabled for indexing
        if (cellarConfiguration.isCellarServiceIndexingConcurrencyControllerEnabled()) {
            lockArgs = new Object[]{((CmrIndexRequestBatch) in[0]).getGroupByUri()};
        }
        return lockArgs;
    }
    
    /**
     * Delays the execution of the indexing process by 1min for testing purposes.
     */
    private void delayExecutionForTesting() {
    	if (this.cellarConfiguration.isTestEnabled() && this.cellarConfiguration.isTestIndexingDelayExecutionEnabled()) {
    		ThreadUtils.waitFor(60000, false);
    	}
    }
    
}
