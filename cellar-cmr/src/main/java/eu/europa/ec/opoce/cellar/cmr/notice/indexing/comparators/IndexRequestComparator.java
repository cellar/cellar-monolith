package eu.europa.ec.opoce.cellar.cmr.notice.indexing.comparators;

import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest;

import java.util.Comparator;

/**
 * <p>IndexRequestComparator class.</p>
 */
public class IndexRequestComparator implements Comparator<CmrIndexRequest> {

    /**
     * Constant <code>instance</code>
     */
    public static final IndexRequestComparator instance = new IndexRequestComparator();

    /**
     * {@inheritDoc}
     */
    @Override
    public int compare(CmrIndexRequest one, CmrIndexRequest other) {
        return one.getCreatedOn().compareTo(other.getCreatedOn());
    }
}
