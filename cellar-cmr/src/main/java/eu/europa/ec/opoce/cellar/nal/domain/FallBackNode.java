/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.nal.domain
 *             FILE : FallBackNode.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 03 02, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-03-02 07:42:50 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.nal.domain;

import java.util.Objects;

/**
 * @author ARHS Developments
 */
public class FallBackNode {

    private String fallbackLabel;
    private String fallbackLanguage;

    private String missingLanguage;

    public FallBackNode(String fallbackLabel, String fallbackLanguage, String missingLanguage) {
        this.fallbackLabel = fallbackLabel;
        this.fallbackLanguage = fallbackLanguage;
        this.missingLanguage = missingLanguage;
    }

    public String getFallbackLabel() {
        return fallbackLabel;
    }

    public String getFallbackLanguage() {
        return fallbackLanguage;
    }

    public String getMissingLanguage() {
        return missingLanguage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FallBackNode)) return false;
        FallBackNode that = (FallBackNode) o;
        return Objects.equals(getFallbackLabel(), that.getFallbackLabel()) &&
                Objects.equals(getFallbackLanguage(), that.getFallbackLanguage()) &&
                Objects.equals(getMissingLanguage(), that.getMissingLanguage());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFallbackLabel(), getFallbackLanguage(), getMissingLanguage());
    }

    @Override
    public String toString() {
        return "FallBackNode{" +
                "fallbackLabel='" + fallbackLabel + '\'' +
                ", fallbackLanguage='" + fallbackLanguage + '\'' +
                ", missingLanguage='" + missingLanguage + '\'' +
                '}';
    }
}
