/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr
 *             FILE : CmrIndexRequestGenerationService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 5, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$$
 *          VERSION : $LastChangedRevision$$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.structmap_status_history.StructMapStatusHistory;
import eu.europa.ec.opoce.cellar.cl.service.client.StructMapStatusHistoryService;
import eu.europa.ec.opoce.cellar.cmr.*;
import org.apache.jena.rdf.model.Model;
import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService.LinkedIndexingBehavior;
import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService.Priority;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.daobeans.InverseRelation;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CmrIndexRequestDao;
import eu.europa.ec.opoce.cellar.cmr.database.dao.InverseRelationDao;
import eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData;
import eu.europa.ec.opoce.cellar.cmr.model.ModelUtils;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.RequestType;
import eu.europa.ec.opoce.cellar.common.util.CellarIdUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsDocument;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.monitoring.Watch;
import eu.europa.ec.opoce.cellar.nal.service.LanguageService;
import eu.europa.ec.opoce.cellar.ontology.service.OntologyService;
import eu.europa.ec.opoce.cellar.semantic.helper.MessageFormatter;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

import static eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.Action.Remove;
import static eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.Action.Update;
import static eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.Reason.DirectlyLinkedObject;

/**
 * The Class CmrIndexRequestGenerationServiceImpl.
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 14 sept. 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class CmrIndexRequestGenerationServiceImpl implements CmrIndexRequestGenerationService {

    private static final Logger LOG = LogManager.getLogger(CmrIndexRequestGenerationServiceImpl.class);
    
    /** The cellar condiguration. */
    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    /** The related object calculator. */
    @Autowired
    private RelatedObjectCalculator relatedObjectCalculator;

    /** The index request dao. */
    @Autowired
    private CmrIndexRequestDao indexRequestDao;

    /** The identifier service. */
    @Autowired
    @Qualifier("pidManagerService")
    private IdentifierService identifierService;

    /** The inverse relation dao. */
    @Autowired
    private InverseRelationDao inverseRelationDao;

    /** The cellar resource dao. */
    @Autowired
    private CellarResourceDao cellarResourceDao;

    /** The ontology service. */
    @Autowired
    private OntologyService ontologyService;

    /** The linked indexing behavior. */
    private LinkedIndexingBehavior linkedIndexingBehavior;

    /** The language service. */
    @Autowired
    private LanguageService languageService;
    
    /** The structmap status history service. */
    @Autowired
    private StructMapStatusHistoryService structMapStatusHistoryService;

    /**
     * Inits the.
     */
    @PostConstruct
    public void init() {
        this.linkedIndexingBehavior = LinkedIndexingBehavior.LOWEST;
    }

    /** {@inheritDoc} */
    @Override
    public LinkedIndexingBehavior getLinkedIndexingBehavior() {
        return this.linkedIndexingBehavior;
    }

    /** {@inheritDoc} */
    @Override
    public void setLinkedIndexingBehavior(final LinkedIndexingBehavior linkedIndexingBehavior) {
        this.linkedIndexingBehavior = linkedIndexingBehavior;
    }

    @Watch(value = "CMR Index generation", arguments = {
            @Watch.WatchArgument(name = "cellar ID", expression = "calculatedData.rootCellarId")
    })
    @Override
    public void insertIndexingRequestsFromCreateIngest(CalculatedData calculatedData) {
        final Set<DigitalObject> relatedThatNeedUpdate = new HashSet<>();
        relatedThatNeedUpdate.addAll(calculatedData.getAddedDigitalObjects());
        languageService.filterNonEuLanguages(relatedThatNeedUpdate);
        final Collection<DigitalObject> targetedObjects = languageService.filterMultiLinguisticExpressions(relatedThatNeedUpdate);
        insertIndexingRequestsFromIngest(calculatedData, targetedObjects, CmrIndexRequest.Action.Create);
    }

    /** {@inheritDoc} */
    @Override
    public void insertIndexingRequestsFromUpdateIngest(CalculatedData calculatedData,
            final Set<DigitalObject> objectsWithChangedUris) {
        final Set<DigitalObject> relatedThatNeedUpdate = new HashSet<>();
        relatedThatNeedUpdate.addAll(objectsWithChangedUris);
        relatedThatNeedUpdate.addAll(calculatedData.getAddedDigitalObjects());
        relatedThatNeedUpdate.addAll(calculatedData.getRemovedDigitalObjects());
        languageService.filterNonEuLanguages(relatedThatNeedUpdate);
        final Collection<DigitalObject> targetedObjects = languageService.filterMultiLinguisticExpressions(relatedThatNeedUpdate);
        insertIndexingRequestsFromIngest(calculatedData, targetedObjects, Update);
    }

    /** {@inheritDoc} */
    @Override
    public void insertIndexingRequestsFromDeleteIngest(CalculatedData calculatedData, final Model directAndInferredModel) {
        final Collection<DigitalObject> deletedDigitalObjects = new ArrayList<>(calculatedData.getRemovedDigitalObjects());
        languageService.filterNonEuLanguages(deletedDigitalObjects);
        final Collection<DigitalObject> targetedObjects = languageService.filterMultiLinguisticExpressions(deletedDigitalObjects);

        //        final DigitalObject workDigitalObject = calculatedData.getStructMap().getDigitalObject();
        final String rootCellarId = calculatedData.getRootCellarId();
        final ContentIdentifier rootContentIdentifier = new ContentIdentifier(rootCellarId);
        final DigitalObjectType rootDigitalObjectType = calculatedData.getStructMap().getDigitalObject().getType();

        final Set<String> deletedCellarIds = new HashSet<>();
        for (final DigitalObject digitObj : targetedObjects) {
            deletedCellarIds.add(digitObj.getCellarId().getIdentifier());
        }

        // If the work/dossier/agent itself is deleted, fire a 'work/dossier/agent-DELETE-index-request',
        // otherwise (for all sub element deletions) a 'work/dossier/agent-UPDATE-index-request' is enough
        CmrIndexRequest.Action action = null;
        // define priority based on the reception folder
        Priority priority = null;
        switch (calculatedData.getMetsPackage().getSipType()) {
        case AUTHENTICOJ:
            priority = Priority.HIGH;
            break;
        case DAILY:
            priority = Priority.NORMAL;
            break;
        case BULK:
            priority = Priority.LOW;
            break;
        case BULK_LOWPRIORITY:
            priority = Priority.LOWEST;
            break;
        default:
            priority = Priority.LOWEST;
            break;
        }

        if (deletedCellarIds.contains(rootCellarId)) {
            action = Remove;
        } else {
            action = Update;
        }

        LOG.info("Start generation of index notice requests for delete ingestion...");
        final Set<CmrIndexRequest> indexRequests = new HashSet<>();

        final String requestedBy = "deletion ingest";

        indexRequests.add(generateDeleteIndexRequest(priority, rootContentIdentifier, rootDigitalObjectType, action, requestedBy,
                calculatedData.getStructMapStatusHistory() != null ? calculatedData.getStructMapStatusHistory().getId() : null));

        indexRequests.addAll(calculateDirectRelatedIndexRequests(rootContentIdentifier, requestedBy, priority, directAndInferredModel));
        indexRequests.add(this.generateInverseRequest(rootContentIdentifier, rootDigitalObjectType, requestedBy,
                calculatedData.getStructMap(), priority));
        
        this.saveQuietly(indexRequests);
        LOG.info("Done generation of {} index notice requests for delete ingestion of {}", indexRequests.size(), rootCellarId);
        // Update INDX_EXECUTION_STATUS (to WAITING) and INDX_CREATED_ON fields of current STRUCTMAP_STATUS_HISTORY entry
        this.updateStructMapWaitingIndexationStatusAndCreatedOn(indexRequests, calculatedData);
    }

    /**
     * <p>generateDeleteIndexRequest.</p>
     *
     * @param priority the priority
     * @param rootContentIdentifier the root content identifier
     * @param rootDigitalObjectType the root digital object type
     * @param action       a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.Action} object.
     * @param requestedBy  a {@link java.lang.String} object.
     * @param structMapStatusHistoryId a {@link java.lang.Long} object
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest} object.
     */
    @SuppressWarnings("serial")
    private CmrIndexRequest generateDeleteIndexRequest(final Priority priority, final ContentIdentifier rootContentIdentifier,
            final DigitalObjectType rootDigitalObjectType, final CmrIndexRequest.Action action, final String requestedBy,
                                                       Long structMapStatusHistoryId) {
        return new CmrIndexRequest() {

            {
                setMetsDocumentId(null);
                setStructmapId(null);
                setConceptScheme(null);
                setObjectUri(rootContentIdentifier.getUri());
                setGroupByUri(rootContentIdentifier.getUri());
                setObjectType(rootDigitalObjectType);
                setCreatedOn(new Date());
                setReason(Reason.IngestedObject);
                setRequestedBy(requestedBy);
                this.setPriority(priority);
                setAction(action);
                setRequestType(RequestType.CalcNotice);
                setStructMapStatusHistoryId(structMapStatusHistoryId);
            }
        };
    }

    /**
     * <p>insertIndexingRequestsFromIngest.</p>
     *
     * @param calculatedData        a {@link eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData} object.
     * @param relatedThatNeedUpdate a {@link java.util.Collection} object.
     * @param action                a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.Action} object.
     */
    private void insertIndexingRequestsFromIngest(CalculatedData calculatedData,
            final Collection<DigitalObject> relatedThatNeedUpdate, final CmrIndexRequest.Action action) {
        final DigitalObject digitalobject = calculatedData.getStructMap().getDigitalObject();

        if ((digitalobject.getType() != DigitalObjectType.WORK) && (digitalobject.getType() != DigitalObjectType.AGENT)
                && (digitalobject.getType() != DigitalObjectType.DOSSIER) && (digitalobject.getType() != DigitalObjectType.TOPLEVELEVENT)) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.INDEXING_SERVICE_ERROR)
                    .withMessage("Cannot compile and index something of type '{}', must one of these types [{}]")
                    .withMessageArgs(digitalobject.getType(), StringUtils.join(new DigitalObjectType[] {
                            DigitalObjectType.WORK, DigitalObjectType.DOSSIER, DigitalObjectType.AGENT, DigitalObjectType.TOPLEVELEVENT},
                            ", "))
                    .build();
        }

        LOG.info("start generation index notice requests for ingest...");
        final Set<CmrIndexRequest> indexRequests = calculateIndexRequests(calculatedData, relatedThatNeedUpdate, action);
        LOG.info("save requests");
        this.saveQuietly(indexRequests);
        LOG.info("done generation {} index notice requests for ingest of {}", indexRequests.size(),
                calculatedData.getStructMap().getDigitalObject().getCellarId().getIdentifier());
        // Update INDX_EXECUTION_STATUS (to WAITING) and INDX_CREATED_ON fields of current STRUCTMAP_STATUS_HISTORY entry
        this.updateStructMapWaitingIndexationStatusAndCreatedOn(indexRequests, calculatedData);
    }

    /**
     * <p>calculateIndexRequests.</p>
     *
     * @param calculatedData        a {@link eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData} object.
     * @param relatedThatNeedUpdate a {@link java.util.Collection} object.
     * @param action                a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.Action} object.
     * @return a {@link java.util.Set} object.
     */
    private Set<CmrIndexRequest> calculateIndexRequests(final CalculatedData calculatedData,
            final Collection<DigitalObject> relatedThatNeedUpdate, final CmrIndexRequest.Action action) {
        final StructMap structMap = calculatedData.getStructMap();

        Priority priority = null;
        switch (calculatedData.getMetsPackage().getSipType()) {
        case AUTHENTICOJ:
            priority = Priority.HIGH;
            break;
        case DAILY:
            priority = Priority.NORMAL;
            break;
        case BULK:
            priority = Priority.LOW;
            break;
        case BULK_LOWPRIORITY:
            priority = Priority.LOWEST;
            break;
        default:
            priority = Priority.LOWEST;
            break;
        }

        final Set<CmrIndexRequest> indexRequests = new HashSet<>();
        final String requestedBy = structMap.getMetadataOperationType() != null
                ? "Ingestion-" + structMap.getMetadataOperationType().toString() : "Ingestion";

        final Set<DigitalObject> indexingDigitalObjects = new HashSet<>();
        indexingDigitalObjects.add(calculatedData.getStructMap().getDigitalObject());

        // Calculate and collect all index requests
        indexRequests.addAll(calculateIngestedIndexRequests(calculatedData, indexingDigitalObjects, action, priority, requestedBy));
        final Priority relatedPriority = this.linkedIndexingBehavior.transformToPriority(priority.getPriorityValue());
        if (relatedPriority != null) { //relatedPriority is null if no indexation of related documents is configured
            // Note: expressions and manifestations are also DIRECT relations; but that does not matter as they are not taken into account during the IDOL transfer
            indexRequests.addAll(addIndexRequestForDirectRelation(calculatedData, relatedThatNeedUpdate, relatedPriority, requestedBy));
            indexRequests.addAll(addIndexRequestsForInverseRelation(calculatedData, relatedThatNeedUpdate, relatedPriority, requestedBy));
        }

        return indexRequests;
    }

    /**
     * <p>calculateIngestedIndexRequests.</p>
     *
     * @param calculatedData         a {@link eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData} object.
     * @param indexingDigitalObjects a {@link java.util.Set} object.
     * @param action                 a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.Action} object.
     * @param priority               a {@link Priority} object.
     * @param requestedBy            a {@link java.lang.String} object.
     * @return a {@link java.util.Set} object.
     */
    @SuppressWarnings("serial")
    private Set<CmrIndexRequest> calculateIngestedIndexRequests(final CalculatedData calculatedData,
            final Set<DigitalObject> indexingDigitalObjects, final CmrIndexRequest.Action action, final Priority priority,
            final String requestedBy) {
        final Set<CmrIndexRequest> indexRequests = new HashSet<>();
        for (final DigitalObject indexingDigitalObject : indexingDigitalObjects) {
            final CmrIndexRequest cmrIndexRequest = new CmrIndexRequest() {

                {
                    final ContentIdentifier cellarId = indexingDigitalObject.getCellarId();
                    final String cellarUri = cellarId.getUri();
                    setObjectUri(cellarUri);
                    final String identifier = cellarId.getIdentifier();
                    final DigitalObjectType type = indexingDigitalObject.getType();
                    final String calculateGroupById = CellarIdUtils.getRootCellarId(identifier);
                    final String uri = identifierService.getUri(calculateGroupById);
                    setGroupByUri(uri);
                    setCreatedOn(new Date());
                    final StructMap structMap = calculatedData.getStructMap();
                    final MetsDocument metsDocument = structMap.getMetsDocument();
                    final String documentId = metsDocument.getDocumentId();
                    setMetsDocumentId(documentId);
                    final String id = structMap.getId();
                    setStructmapId(id);
                    setConceptScheme(null);
                    setReason(Reason.IngestedObject);
                    setRequestType(RequestType.CalcNotice);
                    setRequestedBy(requestedBy);
                    this.setPriority(priority);
                    setAction(action);
                    setObjectType(type);
                    setStructMapStatusHistoryId(
                            calculatedData.getStructMapStatusHistory() != null ? calculatedData.getStructMapStatusHistory().getId() : null
                            );
                }
            };
            indexRequests.add(cmrIndexRequest);
        }
        return indexRequests;
    }

    /**
     * <p>addIndexRequestForDirectRelation.</p>
     *
     * @param calculatedData        a {@link eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData} object.
     * @param relatedThatNeedUpdate a {@link java.util.Collection} object.
     * @param priority              a {@link Priority} object.
     * @param requestedBy           a {@link java.lang.String} object.
     * @return a {@link java.util.Set} object.
     */
    private Set<CmrIndexRequest> addIndexRequestForDirectRelation(final CalculatedData calculatedData,
            final Collection<DigitalObject> relatedThatNeedUpdate, final Priority priority, final String requestedBy) {
        final Set<CmrIndexRequest> indexRequests = new HashSet<>();

        final Set<DigitalObject> doneDirectDigitalObjects = new HashSet<>();

        final Model newMetadataModel = calculatedData.getBusinessMetadata().get(ContentType.DIRECT_INFERRED);
        final Model oldMetadataModel = calculatedData.getExistingDirectAndInferredModel().asModel();
        Model metadataModel = null;
        try {
            metadataModel = ModelUtils.create(newMetadataModel, oldMetadataModel);

            for (final DigitalObject digitalObject : relatedThatNeedUpdate) {
                if (!doneDirectDigitalObjects.contains(digitalObject)) {
                    indexRequests
                            .addAll(calculateDirectRelatedIndexRequests(digitalObject.getCellarId(), requestedBy, priority, metadataModel));
                    doneDirectDigitalObjects.add(digitalObject);
                }
            }

            for (final DigitalObject digitalObject : calculatedData.getObjectsWithChangedEmbedded()) {
                if (!doneDirectDigitalObjects.contains(digitalObject)) {
                    indexRequests.addAll(
                            calculateDirectRelatedEmbeddingRequests(digitalObject.getCellarId(), requestedBy, priority, metadataModel));
                    doneDirectDigitalObjects.add(digitalObject);
                }
            }
        } finally {
            ModelUtils.closeQuietly(metadataModel);
        }

        return indexRequests;
    }

    /**
     * <p>addIndexRequestsForInverseRelation.</p>
     *
     * @param calculatedData         a {@link eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData} object.
     * @param relatedThatNeedUpdated a {@link java.util.Collection} object.
     * @param priority               a {@link Priority} object.
     * @param requestedBy            a {@link java.lang.String} object.
     * @return a {@link java.util.Set} object.
     */
    private Set<CmrIndexRequest> addIndexRequestsForInverseRelation(final CalculatedData calculatedData,
            final Collection<DigitalObject> relatedThatNeedUpdated, final Priority priority, final String requestedBy) {
        final Set<CmrIndexRequest> indexRequests = new HashSet<>();
        final Set<DigitalObject> doneInverseDigitalObjects = new HashSet<>();

        for (final DigitalObject digitalObject : relatedThatNeedUpdated) {
            if (!doneInverseDigitalObjects.contains(digitalObject)) {
                indexRequests.add(this.generateInverseRequest(digitalObject, requestedBy, calculatedData.getStructMap(), priority));
                doneInverseDigitalObjects.add(digitalObject);
            }
        }

        for (final DigitalObject digitalObject : calculatedData.getObjectsWithChangedEmbedded()) {
            if (!doneInverseDigitalObjects.contains(digitalObject)) {
                for (final ContentIdentifier contentIdentifier : digitalObject.getContentids()) {
                    final Collection<InverseRelation> relationsByTarget = this.inverseRelationDao
                            .findRelationsBySource(contentIdentifier.getIdentifier());
                    for (final InverseRelation inverseRelation : relationsByTarget) {
                        if (CollectionUtils.containsAny(inverseRelation.getPropertiesTargetSource(),
                                ontologyService.getWrapper().getOntologyData().getEmbeddingNotices())) {
                            indexRequests.add(generateInverseExpandingRequest(inverseRelation.getTarget(), inverseRelation.getTargetType(),
                                    requestedBy, calculatedData.getStructMap(), priority));
                        }
                    }
                }
                doneInverseDigitalObjects.add(digitalObject);
            }
        }
        return indexRequests;
    }

    /**
     * <p>calculateDirectRelatedEmbeddingRequests.</p>
     *
     * @param contentIdentifier a {@link eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier} object.
     * @param requestedBy       a {@link java.lang.String} object.
     * @param priority          a {@link Priority} object.
     * @param dmdIndex          a {@link org.apache.jena.rdf.model.Model} object.
     * @return a {@link java.util.Collection} object.
     */
    private Collection<CmrIndexRequest> calculateDirectRelatedEmbeddingRequests(final ContentIdentifier contentIdentifier,
            final String requestedBy, final Priority priority, final Model dmdIndex) {
        final Set<RelatedObject> directRelations = this.relatedObjectCalculator.calculateDirectRelatedIndexRequests(dmdIndex,
                contentIdentifier);
        CollectionUtils.filter(directRelations, object -> ontologyService.getWrapper().getOntologyData().getEmbeddingNotices()
                .contains(object.getProperty()));
        return generateEmbeddingRequests(linkedIndexingBehavior.transformToPriority(priority.getPriorityValue()),
                requestedBy, Update, DirectlyLinkedObject,
                directRelations, new Date());
    }

    /**
     * <p>calculateDirectRelatedIndexRequests.</p>
     *
     * @param contentIdentifier      a {@link eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier} object.
     * @param requestedBy            a {@link java.lang.String} object.
     * @param priority               a {@link Priority} object.
     * @param directAndInferredModel a {@link org.apache.jena.rdf.model.Model} object.
     * @return a {@link java.util.Collection} object.
     */
    private Collection<CmrIndexRequest> calculateDirectRelatedIndexRequests(final ContentIdentifier contentIdentifier,
            final String requestedBy, final Priority priority, final Model directAndInferredModel) {
        final Set<RelatedObject> directRelations = this.relatedObjectCalculator.calculateDirectRelatedIndexRequests(directAndInferredModel,
                contentIdentifier);

        return generateIndexRequests(
                linkedIndexingBehavior.transformToPriority(priority.getPriorityValue()),
                requestedBy, Update, DirectlyLinkedObject, directRelations, new Date());
    }

    /**
     * <p>generateInverseRequest.</p>
     *
     * @param digitalobject a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject} object.
     * @param requestedBy   a {@link java.lang.String} object.
     * @param structMap     a {@link eu.europa.ec.opoce.cellar.domain.content.mets.StructMap} object.
     * @param priority      a {@link Priority} object.
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest} object.
     */
    private CmrIndexRequest generateInverseRequest(final DigitalObject digitalobject, final String requestedBy, final StructMap structMap,
            final Priority priority) {
        return this.generateInverseRequest(digitalobject.getCellarId(), digitalobject.getType(), requestedBy, structMap, priority);
    }

    /**
     * <p>generateInverseExpandingRequest.</p>
     *
     * @param cellarIdentifier  a {@link java.lang.String} object.
     * @param digitalObjectType a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType} object.
     * @param requestedBy       a {@link java.lang.String} object.
     * @param structMap         a {@link eu.europa.ec.opoce.cellar.domain.content.mets.StructMap} object.
     * @param priority          a {@link Priority} object.
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest} object.
     */
    @SuppressWarnings("serial")
    private CmrIndexRequest generateInverseExpandingRequest(final String cellarIdentifier, final DigitalObjectType digitalObjectType,
            final String requestedBy, final StructMap structMap, final Priority priority) {
        return new CmrIndexRequest() {

            {
                setObjectUri(identifierService.getUri(cellarIdentifier));
                setGroupByUri(identifierService
                        .getUri(CellarIdUtils.getRootCellarId(cellarIdentifier)));
                setCreatedOn(new Date());
                setMetsDocumentId(null);
                setStructmapId(null);
                setConceptScheme(null);
                setReason(Reason.InverseLinkedObject);
                setRequestType(RequestType.CalcExpanded);
                setRequestedBy(requestedBy);
                this.setPriority(linkedIndexingBehavior.transformToPriority(priority.getPriorityValue()));
                setAction(Update);
                setObjectType(digitalObjectType);
            }
        };
    }

    /**
     * <p>generateInverseRequest.</p>
     *
     * @param contentIdentifier a {@link eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier} object.
     * @param digitalObjectType a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType} object.
     * @param requestedBy       a {@link java.lang.String} object.
     * @param structMap         a {@link eu.europa.ec.opoce.cellar.domain.content.mets.StructMap} object.
     * @param priority          a {@link Priority} object.
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest} object.
     */
    @SuppressWarnings("serial")
    private CmrIndexRequest generateInverseRequest(final ContentIdentifier contentIdentifier, final DigitalObjectType digitalObjectType,
            final String requestedBy, final StructMap structMap, final Priority priority) {
        return new CmrIndexRequest() {

            {
                setObjectUri(contentIdentifier.getUri());
                setGroupByUri(identifierService
                        .getUri(CellarIdUtils.getRootCellarId(contentIdentifier.getIdentifier())));
                setCreatedOn(new Date());
                setMetsDocumentId(null);
                setStructmapId(null);
                setConceptScheme(null);
                setReason(Reason.IngestedObject);
                setRequestType(RequestType.CalcInverse);
                setRequestedBy(requestedBy);
                this.setPriority(priority);
                setAction(Update);
                setObjectType(digitalObjectType);
            }
        };
    }

    /**
     * <p>generateEmbeddingRequests.</p>
     *
     * @param priority    a {@link Priority} object.
     * @param requestedBy a {@link java.lang.String} object.
     * @param action      a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.Action} object.
     * @param reason      a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.Reason} object.
     * @param relations   a {@link java.util.Set} object.
     * @param createdOn   a {@link java.util.Date} object.
     * @return a {@link java.util.Collection} object.
     */
    private Collection<CmrIndexRequest> generateEmbeddingRequests(final Priority priority, final String requestedBy,
            final CmrIndexRequest.Action action, final CmrIndexRequest.Reason reason, final Set<RelatedObject> relations,
            final Date createdOn) {
        return generateRequests(priority, requestedBy, action, reason, relations, createdOn, RequestType.CalcExpanded);
    }

    /**
     * <p>generateIndexRequests.</p>
     *
     * @param priority    a {@link Priority} object.
     * @param requestedBy a {@link java.lang.String} object.
     * @param action      a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.Action} object.
     * @param reason      a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.Reason} object.
     * @param relations   a {@link java.util.Set} object.
     * @param createdOn   a {@link java.util.Date} object.
     * @return a {@link java.util.Collection} object.
     */
    private Collection<CmrIndexRequest> generateIndexRequests(final Priority priority, final String requestedBy,
            final CmrIndexRequest.Action action, final CmrIndexRequest.Reason reason, final Set<RelatedObject> relations,
            final Date createdOn) {
        return generateRequests(priority, requestedBy, action, reason, relations, createdOn, RequestType.CalcNotice);
    }

    /**
     * <p>generateRequests.</p>
     *
     * @param priority    a {@link Priority} object.
     * @param requestedBy a {@link java.lang.String} object.
     * @param action      a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.Action} object.
     * @param reason      a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.Reason} object.
     * @param relations   a {@link java.util.Set} object.
     * @param createdOn   a {@link java.util.Date} object.
     * @param requestType the request type
     * @return a {@link java.util.Collection} object.
     */
    @SuppressWarnings("serial")
    private Collection<CmrIndexRequest> generateRequests(final Priority priority, final String requestedBy,
            final CmrIndexRequest.Action action, final CmrIndexRequest.Reason reason, final Set<RelatedObject> relations,
            final Date createdOn, final RequestType requestType) {
        final Set<CmrIndexRequest> indexRequests = new HashSet<>();
        for (final RelatedObject inverseRelation : relations) {

            final String cellarId = this.identifierService.getCellarPrefixedOrNull(inverseRelation.getResourceUri());
            if (StringUtils.isBlank(cellarId)) {
                LOG.warn("Unable to create direct linked index request for {}: Cellar resource does not exist yet!",
                        inverseRelation.getResourceUri());
            } else {
                final DigitalObjectType directType = inverseRelation.getType();
                if (directType == null) {
                    continue;
                }

                indexRequests.add(new CmrIndexRequest() {

                    {
                        setObjectUri(identifierService.getUri(cellarId));
                        setGroupByUri(identifierService
                                .getUri(CellarIdUtils.getRootCellarId(cellarId)));
                        setCreatedOn(createdOn);
                        setMetsDocumentId(null);
                        setStructmapId(null);
                        setConceptScheme(null);
                        setReason(reason);
                        setRequestType(requestType);
                        setRequestedBy(requestedBy);
                        this.setPriority(priority);
                        setAction(action);
                        setObjectType(directType);
                    }
                });
            }
        }
        return indexRequests;
    }

    /** {@inheritDoc} */
    @Override
    public void insertIndexRequestsFromEmbargoChange(final ContentIdentifier cellarId, final Model directAndInferredModel,
            final CmrIndexRequest.Action action) {
        LOG.info("start generation index notice requests for change of embargo...");
        final Set<CmrIndexRequest> indexRequests = new HashSet<>();

        final CellarResource findCellarId = this.cellarResourceDao.findCellarId(cellarId.getIdentifier());

        final String requestedBy = "change of embargo";
        indexRequests.add(generateEmbargoIndexRequest(cellarId, action, requestedBy));

        if (action == Remove) {
            indexRequests.addAll(calculateDirectRelatedIndexRequests(cellarId, requestedBy, Priority.NORMAL, directAndInferredModel));
            indexRequests.add(this.generateInverseRequest(cellarId, findCellarId.getCellarType(), requestedBy, null, Priority.NORMAL));
        } else if (action == CmrIndexRequest.Action.Create) {
            indexRequests.addAll(calculateDirectRelatedIndexRequests(cellarId, requestedBy, Priority.NORMAL, directAndInferredModel));
            indexRequests.add(this.generateInverseRequest(cellarId, findCellarId.getCellarType(), requestedBy, null, Priority.NORMAL));
        }
        this.saveQuietly(indexRequests);
        LOG.info("done generation of {} index notice requests for change of embargo of {}", indexRequests.size(), cellarId);
    }

    /**
     * <p>generateEmbargoIndexRequest.</p>
     *
     * @param cellarId a {@link eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier} object.
     * @param action       a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.Action} object.
     * @param requestedBy  a {@link java.lang.String} object.
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest} object.
     */
    @SuppressWarnings("serial")
    private CmrIndexRequest generateEmbargoIndexRequest(final ContentIdentifier cellarId, final CmrIndexRequest.Action action,
            final String requestedBy) {

        final CellarResource cellarResource = this.cellarResourceDao.findCellarId(cellarId.getIdentifier());

        return new CmrIndexRequest() {

            {
                setMetsDocumentId(null);
                setStructmapId(null);
                setConceptScheme(null);
                setObjectUri(cellarId.getUri());
                setGroupByUri(cellarId.getUri());
                setObjectType(cellarResource.getCellarType());
                setCreatedOn(new Date());
                setReason(Reason.Administrator);
                setRequestedBy(requestedBy);
                this.setPriority(Priority.NORMAL);
                setAction(action);
                setRequestType(RequestType.CalcNotice);
            }
        };
    }

    /** {@inheritDoc} */
    @Override
    public void generateIndexRequest(final String uriOrPrefix, final String userName, final Collection<RequestType> types,
            final Priority priority, final CmrIndexRequest.Reason reason) {

        if (StringUtils.isBlank(uriOrPrefix)) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.INDEXING_REQUEST_SERVICE_ERROR)
                    .withMessage("The resource identifier cannot be blank.").build();
        }

        if (priority == null) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.INDEXING_REQUEST_SERVICE_ERROR)
                    .withMessage("The priority cannot be null.").build();
        }

        final String cellarId = this.identifierService.getCellarPrefixedOrNull(uriOrPrefix);
        if (StringUtils.isBlank(cellarId)) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.INDEXING_REQUEST_SERVICE_ERROR)
                    .withMessage("Unable to create index request for {}: Cellar resource does not exist yet.").withMessageArgs(uriOrPrefix)
                    .build(); // the resource doesn't exist
        }

        final CellarResource cellarResource = this.cellarResourceDao.findCellarId(cellarId);
        if (cellarResource == null) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.INDEXING_REQUEST_SERVICE_ERROR)
                    .withMessage("Production identifier '{}' not found.").withMessageArgs(cellarId).build(); // an item is returned
        }

        if (cellarResource.getCellarType() == DigitalObjectType.ITEM) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.INDEXING_REQUEST_SERVICE_ERROR)
                    .withMessage("Unable to index an item: {}").withMessageArgs(uriOrPrefix).build(); // an item is returned
        }

        final List<CmrIndexRequest> indexRequests = new LinkedList<>();
        for (final RequestType type : types) {
            indexRequests.add(buildCmrIndexRequest(cellarResource, type, reason, Update, userName, priority));
        }
        this.saveQuietly(indexRequests);
    }

    /**
     * Builds the cmr index request.
     *
     * @param cellarResource the cellar resource
     * @param requestType the request type
     * @param reason the reason
     * @param action the action
     * @param userName the user name
     * @param priority the priority
     * @return the cmr index request
     */
    @SuppressWarnings("serial")
    private CmrIndexRequest buildCmrIndexRequest(final CellarResource cellarResource, final RequestType requestType,
            final CmrIndexRequest.Reason reason, final CmrIndexRequest.Action action, final String userName, final Priority priority) {
        return new CmrIndexRequest() {

            {
                setObjectUri(identifierService.getUri(cellarResource.getCellarId()));
                setGroupByUri(identifierService
                        .getUri(CellarIdUtils.getCellarIdBase(cellarResource.getCellarId())));
                setCreatedOn(new Date());
                setMetsDocumentId(null);
                setStructmapId(null);
                setConceptScheme(null);
                setReason(reason);
                setRequestType(requestType);
                setRequestedBy(userName);
                this.setPriority(priority);
                setAction(action);
                setObjectType(cellarResource.getCellarType());
            }
        };
    }

    /**
     * <p>saveQuietly.</p>
     *
     * @param indexRequests a {@link java.util.Collection} object.
     */
    private void saveQuietly(final Collection<CmrIndexRequest> indexRequests) {
        for (final CmrIndexRequest indexRequest : indexRequests) {
            this.saveQuietly(indexRequest);
        }
    }

    /**
     * <p>saveQuietly.</p>
     *
     * @param indexRequest a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest} object.
     */
    private void saveQuietly(final CmrIndexRequest indexRequest) {
        try {
            this.indexRequestDao.saveOrUpdateObject(indexRequest);
        } catch (final Exception e) {
            LOG.error(MessageFormatter.format("Unexpected exception while saving {}: {}", indexRequest, e.getMessage()), e);
        }
    }
    
    /**
     * Update the INDX_EXECUTION_STATUS and INDX_CREATED_ON fields of a STRUCTMAP_STATUS_HISTORY entry
     * @param indexRequests current set of index requests.
     * @param calculatedData current {@link CalculatedData} object.
     */
    private void updateStructMapWaitingIndexationStatusAndCreatedOn(Set<CmrIndexRequest> indexRequests, CalculatedData calculatedData){
        if (calculatedData.getStructMapStatusHistory() != null){
            // Find the NOTICE index requests
            List<CmrIndexRequest> calcNoticeRequests = indexRequests.stream()
                    .filter(indx -> indx.getStructMapStatusHistoryId() != null)
                    .collect(Collectors.toList());
            // Update the CREATED_ON (and the INDX_EXECUTION_STATUS) of the corresponding STRUCTMAP_STATUS_HISTORY
            for (CmrIndexRequest cir : calcNoticeRequests){
                if (Objects.equals(cir.getStructMapStatusHistoryId(), calculatedData.getStructMapStatusHistory().getId())){
                    StructMapStatusHistory structMapStatusHistory = this.structMapStatusHistoryService.updateStructMapIndxStatusAndCreatedOn(calculatedData.getStructMapStatusHistory(), cir.getCreatedOn());
                    calculatedData.setStructMapStatusHistory(structMapStatusHistory);
                    break;
                }
            }
        }
    }
}
