/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.modelload.service.impl
 *             FILE : ModelLoadRequestProcessingServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 01 10, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-01-10 09:15:47 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.modelload.service.impl;

import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.database.dao.NalConfigDao;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.CellarValidationException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.VirtuosoOperationException;
import eu.europa.ec.opoce.cellar.feed.domain.impl.nal.NalHistory;
import eu.europa.ec.opoce.cellar.feed.service.HistoryService;
import eu.europa.ec.opoce.cellar.logging.LogContext;
import eu.europa.ec.opoce.cellar.modelload.domain.ModelLoadRequest;
import eu.europa.ec.opoce.cellar.modelload.service.ModelLoadRequestProcessingService;
import eu.europa.ec.opoce.cellar.nal.NalOntoUtils;
import eu.europa.ec.opoce.cellar.nal.dao.NalConfig;
import eu.europa.ec.opoce.cellar.nal.service.NalInferenceService;
import eu.europa.ec.opoce.cellar.nal.service.NalSnippetCreatorService;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import eu.europa.ec.opoce.cellar.semantic.validation.NalValidationUtils;
import eu.europa.ec.opoce.cellar.virtuoso.VirtuosoService;
import org.apache.commons.io.IOUtils;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.RDFFormat;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

import static eu.europa.ec.opoce.cellar.CmrErrors.NAL_LOAD_REQUEST_SERVICE_ERROR;
import static eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration.NAL;
import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.CMR_NAL_LOAD;
import static eu.europa.ec.opoce.cellar.nal.NalOntoUtils.AUTHORITY_ONTOLOGY_URI;
import static eu.europa.ec.opoce.cellar.nal.NalOntoUtils.NAL_PREFIX;
import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * @author ARHS Developments
 */
@LogContext
@Service
public class ModelLoadRequestProcessingServiceImpl implements ModelLoadRequestProcessingService {

    private static final Logger LOG = LogManager.getLogger(ModelLoadRequestProcessingServiceImpl.class);
    private static final String NAL_COPY_XSLT = "copy.xslt";

    private final NalInferenceService nalInferenceService;
    private final ContentStreamService contentStreamService;
    private final NalSnippetCreatorService nalSnippetCreatorService;
    private final ICellarConfiguration cellarConfiguration;
    private final VirtuosoService virtuosoService;
    private final NalConfigDao nalConfigDao;
    private final HistoryService historyService;
    private final ApplicationEventPublisher applicationEventPublisher;

    private static XMLReader reader;

    @Autowired
    public ModelLoadRequestProcessingServiceImpl(NalInferenceService nalInferenceService, ContentStreamService contentStreamService,
                                                 NalSnippetCreatorService nalSnippetCreatorService, @Qualifier("cellarConfiguration") ICellarConfiguration cellarConfiguration,
                                                 VirtuosoService virtuosoService, NalConfigDao nalConfigDao, HistoryService historyService,
                                                 ApplicationEventPublisher applicationEventPublisher) throws SAXException {
        this.nalInferenceService = nalInferenceService;
        this.contentStreamService = contentStreamService;
        this.nalSnippetCreatorService = nalSnippetCreatorService;
        this.cellarConfiguration = cellarConfiguration;
        this.virtuosoService = virtuosoService;
        this.nalConfigDao = nalConfigDao;
        this.historyService = historyService;
        this.applicationEventPublisher = applicationEventPublisher;

        reader = XMLReaderFactory.createXMLReader();

        EntityResolver ent = new EntityResolver() {

            @Override
            public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {

               return new InputSource();
            }
        };


        reader.setEntityResolver(ent);
    }

    @LogContext(CMR_NAL_LOAD)
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = CellarException.class, noRollbackFor = VirtuosoOperationException.class)
    public void process(ModelLoadRequest modelLoadRequest) {
        Assert.notNull(modelLoadRequest, "The model load request parameter cannot be null");
        String modelUri = modelLoadRequest.getModelURI();
        ThreadContext.put("NAL_URI", modelUri);
        try {
            processNal(modelLoadRequest);
        } finally {
            ThreadContext.remove("NAL_URI");
        }

    }

    private void processNal(ModelLoadRequest modelLoadRequest) {
        String modelUri = modelLoadRequest.getModelURI();
        String nalContent = retrieveNalNonInferred(modelLoadRequest);
        // XSLT transformation require a model with XML format
        Model nalModel = parseRdfModel(JenaUtils.translate(nalContent, RDFFormat.NT, RDFFormat.RDFXML_PLAIN), modelUri);
        LOG.debug(NAL, "Successfully transformed NAL from S3 '{}' to a Jena Model", modelLoadRequest.getExternalPid());

        LOG.debug(NAL, "Starting validation of the NAL model URIs '{}'", modelUri);
        long startTime = System.currentTimeMillis();
        validateUrisInModel(modelUri, nalModel);
        LOG.debug(NAL, "Finished validation of the NAL model URIs '{}' in {} ms.", modelUri, (System.currentTimeMillis() - startTime));

        LOG.debug(NAL, "Starting validation of the NAL skos concepts '{}'", modelUri);
        startTime = System.currentTimeMillis();
        validateSkosConceptsInModel(modelUri, nalModel);
        LOG.debug(NAL, "Finished validation of the NAL skos concepts '{}' in {} ms.", modelUri, (System.currentTimeMillis() - startTime));

        LOG.debug(NAL, "Starting loading of the model in Virtuoso for '{}'", modelUri);
        startTime = System.currentTimeMillis();
        performVirtuosoLoad(nalModel, modelUri);
        LOG.debug(NAL, "Finished loading of the model in Virtuoso for '{}' in {} ms.", modelUri, (System.currentTimeMillis() - startTime));

        startTime = System.currentTimeMillis();
        LOG.debug(NAL, "Starting inference of the NAL model '{}'", modelUri);
        nalInferenceService.infer(nalModel, modelUri);
        LOG.debug(NAL, "Finished inference of the NAL model '{}' in {} ms.", modelUri, (System.currentTimeMillis() - startTime));

        startTime = System.currentTimeMillis();
        LOG.debug(NAL, "Starting the generation of snippets for '{}'.", modelUri);
        nalSnippetCreatorService.generate(nalModel, modelUri);
        LOG.info("Finished the generation of snippets for '{}' in {} ms.", modelUri, (System.currentTimeMillis() - startTime));

        String inferredVersion = updateRepository(modelLoadRequest, nalModel, modelUri);
        NalConfig nalConfig = updateNalConfig(modelLoadRequest, modelUri, nalModel, inferredVersion);
        storeNalHistory(nalConfig,modelLoadRequest);

        JenaUtils.closeQuietly(nalModel);
    }

    private void validateSkosConceptsInModel(String modelUri, Model nalModel){
        if(!NalValidationUtils.validateSkosConcepts(nalModel)){
            throw ExceptionBuilder.get(CellarValidationException.class)
                    .withMessage("Invalid skos concepts found in model '{}").withMessageArgs(modelUri).build();
        }
    }

    private void validateUrisInModel(String modelUri, Model nalModel) {
        if (modelUri.contains(AUTHORITY_ONTOLOGY_URI)) {
            List<String> invalidUriList = NalValidationUtils.validateUris(nalModel);
            if (!invalidUriList.isEmpty()) {
                throw ExceptionBuilder.get(CellarValidationException.class).withCode(CmrErrors.VALIDATION_RDFDATA_ERROR)
                        .withMessage("Invalid URIs found in model '{}' : {}").withMessageArgs(modelUri, invalidUriList).build();
            }
        }
    }

    private void storeNalHistory(NalConfig nalConfig,ModelLoadRequest modelLoadRequest) {
        if (nalConfig.getVersionNr() != null) {
            LOG.debug(NAL, "Save NAL {} into NAL_HISTORY with version={}", nalConfig.getUri(), nalConfig.getVersionNr());
            NalHistory nalHistory = new NalHistory();
            nalHistory.setUri(nalConfig.getUri());
            nalHistory.setVersion(nalConfig.getVersionNr());
            nalHistory.setCellarId(modelLoadRequest.getCellarId());
            String pIds= String.join(",", this.historyService.getProductionIdentifierNamesForCellarId(modelLoadRequest.getCellarId()));
            nalHistory.setIdentifiers(pIds);
            historyService.store(nalHistory);
            applicationEventPublisher.publishEvent(nalHistory);
        } else {
            LOG.warn("Cannot save NAL '{}' in NAL's history, version is not defined", nalConfig.getUri());
        }
    }

    private static String transform(String nal) {
        InputStream xsltInputStream = null;
        InputStream vocInputStream = null;
        try {
            xsltInputStream = ModelLoadRequestProcessingServiceImpl.class.getResourceAsStream(NAL_COPY_XSLT);
            vocInputStream = new ByteArrayInputStream(nal.getBytes(UTF_8));
            final TransformerFactory transformerFactory=TransformerFactory.newInstance();
            SAXSource saxSource=new SAXSource(reader,new InputSource(xsltInputStream));
            final Transformer t = transformerFactory.newTransformer(saxSource);
            final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            t.transform(new StreamSource(vocInputStream), new StreamResult(outputStream));
            return new String(outputStream.toByteArray(), UTF_8);
        } catch (final TransformerException e) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.NAL_LOAD_SERVICE_ERROR).withCause(e)
                    .withMessage("Unable to perform xlst-transformation on data resource: {}").withMessageArgs(e.getMessage()).build();
        } finally {
            IOUtils.closeQuietly(xsltInputStream);
            IOUtils.closeQuietly(vocInputStream);
        }
    }

    private String updateRepository(ModelLoadRequest modelLoadRequest, Model inferredNalModel, String modelUri) throws CellarException {
        byte[] bModel = JenaUtils.toString(inferredNalModel, RDFFormat.NT).getBytes(UTF_8);
        String pid = modelLoadRequest.getExternalPid().replace(":", "/");
        if (!pid.startsWith(NAL_PREFIX)) {
            pid = NAL_PREFIX + pid;
        }
        String newVersion = contentStreamService.writeContent(pid, new ByteArrayResource(bModel), ContentType.DIRECT_INFERRED);
        if (newVersion == null) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("Version of the inferred model has not been retrieved, try to reload the NAL.")
                    .build();
        }
        return newVersion;
    }

    private NalConfig updateNalConfig(ModelLoadRequest modelLoadRequest, String modelUri, Model nalModel, String inferredVersion) {
        NalConfig nalConfig = nalConfigDao.findByUri(modelUri).orElseGet(() -> newNalConfig(modelUri, modelLoadRequest));
        nalConfig.setVersionNr(getNalVersionForNalUri(modelUri, nalModel));
        nalConfig.setExternalPid(modelLoadRequest.getExternalPid());
        nalConfig.setVersion(modelLoadRequest.getVersion());
        nalConfig.setVersionInferred(inferredVersion);
        nalConfig.setCreatedOn(new Date());
        nalConfig.setVersionDate(new Date());
        nalConfigDao.saveOrUpdateObject(nalConfig);
        return nalConfig;
    }

    private static NalConfig newNalConfig(String modelUri, ModelLoadRequest modelLoadRequest) {
        NalConfig nalConfig = new NalConfig();
        nalConfig.setUri(modelUri);
        nalConfig.setName(modelLoadRequest.getNalName());
        nalConfig.setVersion(modelLoadRequest.getVersion());
        if (NalOntoUtils.EUROVOC_CONCEPTSCHEME_URI.equalsIgnoreCase(modelUri)) {
            nalConfig.setOntologyUri(NalOntoUtils.EUROVOC_ONTOLOGY_URI);
        } else {
            nalConfig.setOntologyUri(NalOntoUtils.AUTHORITY_ONTOLOGY_URI);
        }
        return nalConfig;
    }

    private String getNalVersionForNalUri(String modelUri, Model nalModel) {
        String version = CellarProperty.cdm_NALOntoVersionUnknown;

        if (nalModel != null) {
            final Resource s = nalModel.getResource(modelUri);
            final Property p = nalModel.getProperty(CellarProperty.cdm_nalVersionPropertyUrl);
            final StmtIterator iter = nalModel.listStatements(s, p, (RDFNode) null);
            try {
                if (iter.hasNext()) {
                    version = iter.nextStatement().getObject().toString();
                }
            } finally {
                iter.close();
            }
        }
        return version;
    }

    private void performVirtuosoLoad(Model nalModel, String modelUri) {
        if (cellarConfiguration.isVirtuosoIngestionEnabled()) {
            try {
                this.virtuosoService.writeNal(nalModel, modelUri, true);
            } catch (final Exception e) {
                LOG.error("Unexpected exception was thrown while loading the NAL '{}' into Virtuoso", modelUri, e);
            }
        }
    }

    private Model parseRdfModel(String nalContent, String modelUri) {
        String transformedNalContent = nalContent;
        if (!NalOntoUtils.EUROVOC_CONCEPTSCHEME_URI.equalsIgnoreCase(modelUri)) {
            transformedNalContent = transform(nalContent);
        }
        return JenaUtils.read(transformedNalContent);
    }

    private String retrieveNalNonInferred(ModelLoadRequest modelLoadRequest) {
        String pid = modelLoadRequest.getExternalPid().replace(":", "/");
        if (!pid.startsWith(NAL_PREFIX)) {
            pid = NAL_PREFIX + pid;
        }
        final String nalPid = pid;
        return contentStreamService.getContentAsString(nalPid, modelLoadRequest.getVersion(), ContentType.DIRECT)
                .orElseThrow(() -> ExceptionBuilder.get(CellarException.class)
                        .withCode(NAL_LOAD_REQUEST_SERVICE_ERROR)
                        .withMessage("The NAL from the model-load request '{}' ({}) could not be retrieved from S3.")
                        .withMessageArgs(nalPid, modelLoadRequest.getVersion())
                        .build());
    }
}
