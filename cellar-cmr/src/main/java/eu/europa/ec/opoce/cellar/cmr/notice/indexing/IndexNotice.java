package eu.europa.ec.opoce.cellar.cmr.notice.indexing;

import eu.europa.ec.opoce.cellar.cmr.database.DaoObject;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;

import java.util.Date;

/**
 * <p>IndexNotice class.</p>
 */
public class IndexNotice extends DaoObject {

    private String operationType;
    private String cellarUri;
    private String metadata;
    private String contentUrls;
    private Date lastModificationDate;
    private int priority;
    private LanguageBean isoCode;

    /**
     * <p>Constructor for IndexNotice.</p>
     */
    public IndexNotice() {
        this.lastModificationDate = new Date();
    }

    /**
     * <p>Constructor for IndexNotice.</p>
     *
     * @param indexNotice a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.IndexNotice} object.
     */
    public IndexNotice(final IndexNotice indexNotice) {
        this.cellarUri = indexNotice.cellarUri;
        this.contentUrls = indexNotice.contentUrls;
        this.isoCode = indexNotice.isoCode;
        this.lastModificationDate = new Date();
        this.metadata = indexNotice.metadata;
        this.operationType = indexNotice.operationType;
        this.priority = indexNotice.priority;
    }

    /**
     * <p>Getter for the field <code>operationType</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getOperationType() {
        return this.operationType;
    }

    /**
     * <p>Setter for the field <code>operationType</code>.</p>
     *
     * @param operationType a {@link java.lang.String} object.
     */
    public void setOperationType(final String operationType) {
        this.operationType = operationType;
    }

    /**
     * <p>Getter for the field <code>cellarUri</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCellarUri() {
        return this.cellarUri;
    }

    /**
     * <p>Setter for the field <code>cellarUri</code>.</p>
     *
     * @param cellarUri a {@link java.lang.String} object.
     */
    public void setCellarUri(final String cellarUri) {
        this.cellarUri = cellarUri;
    }

    /**
     * <p>Getter for the field <code>metadata</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getMetadata() {
        return this.metadata;
    }

    /**
     * <p>Setter for the field <code>metadata</code>.</p>
     *
     * @param metadata a {@link java.lang.String} object.
     */
    public void setMetadata(final String metadata) {
        this.metadata = metadata;
    }

    /**
     * <p>Getter for the field <code>contentUrls</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getContentUrls() {
        return this.contentUrls;
    }

    /**
     * <p>Setter for the field <code>contentUrls</code>.</p>
     *
     * @param contentUrls a {@link java.lang.String} object.
     */
    public void setContentUrls(final String contentUrls) {
        this.contentUrls = contentUrls;
    }

    /**
     * <p>Getter for the field <code>lastModificationDate</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getLastModificationDate() {
        return this.lastModificationDate;
    }

    /**
     * <p>Setter for the field <code>lastModificationDate</code>.</p>
     *
     * @param lastModificationDate a {@link java.util.Date} object.
     */
    public void setLastModificationDate(final Date lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    /**
     * <p>Getter for the field <code>priority</code>.</p>
     *
     * @return a int.
     */
    public int getPriority() {
        return this.priority;
    }

    /**
     * <p>Setter for the field <code>priority</code>.</p>
     *
     * @param priority a int.
     */
    public void setPriority(final int priority) {
        this.priority = priority;
    }

    /**
     * <p>Getter for the field <code>isoCode</code>.</p>
     *
     * @return a {@link eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean} object.
     */
    public LanguageBean getIsoCode() {
        return this.isoCode;
    }

    /**
     * <p>Setter for the field <code>isoCode</code>.</p>
     *
     * @param isoCode a {@link eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean} object.
     */
    public void setIsoCode(final LanguageBean isoCode) {
        this.isoCode = isoCode;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "IndexNotice [operationType=" + this.operationType + ", cellarUri=" + this.cellarUri + ", contentUrls=" + this.contentUrls
                + ", lastModificationDate=" + this.lastModificationDate + ", priority=" + this.priority + ", isoCode=" + this.isoCode + "]";
    }

}
