/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.indexing.service.impl
 *             FILE : IndexRequestManagingServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Nov 10, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.indexing.service.impl;

import eu.europa.ec.opoce.cellar.ccr.service.database.DefaultTransactionService;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.enums.IndexExecutionStatus;
import eu.europa.ec.opoce.cellar.cl.domain.enums.ProcessMonitorPhase;
import eu.europa.ec.opoce.cellar.cl.domain.structmap_status_history.StructMapStatusHistory;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cl.service.client.StructMapStatusHistoryService;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CmrIndexRequestDao;
import eu.europa.ec.opoce.cellar.cmr.indexing.service.IndexRequestManagerService;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.ExecutionStatus;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestBatch;
import eu.europa.ec.opoce.cellar.common.service.TestingThreadService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration.INDEXATION;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Nov 10, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service("indexRequestManagerService")
public class IndexRequestManagerServiceImpl extends DefaultTransactionService implements IndexRequestManagerService {

    private static final Logger LOG = LogManager.getLogger(IndexRequestManagerServiceImpl.class);

    /**
     * Batch size constant. Used during auto cleaning and failed requests restart.
     */
    private static final int BATCH_SIZE = 1000;

    private final ICellarConfiguration cellarConfiguration;

    private final CmrIndexRequestDao cmrIndexRequestDao;

    private final CellarResourceDao cellarResourceDao;

    private final IdentifierService identifierService;
    
    private final StructMapStatusHistoryService structMapStatusHistoryService;
    
    private final TestingThreadService testingThreadService;
    
    

    @Autowired
    public IndexRequestManagerServiceImpl(@Qualifier("cellarConfiguration") ICellarConfiguration cellarConfiguration,
                                          CmrIndexRequestDao cmrIndexRequestDao, CellarResourceDao cellarResourceDao,
                                          @Qualifier("pidManagerService") IdentifierService identifierService,
                                          StructMapStatusHistoryService structMapStatusHistoryService,
                                          TestingThreadService testingThreadService) {
        this.cellarConfiguration = cellarConfiguration;
        this.cmrIndexRequestDao = cmrIndexRequestDao;
        this.cellarResourceDao = cellarResourceDao;
        this.identifierService = identifierService;
        this.structMapStatusHistoryService = structMapStatusHistoryService;
        this.testingThreadService = testingThreadService;
    }

    @Override
    @Transactional
    public void finalizeCmrIndexRequestBatch(final CmrIndexRequestBatch batch) {
        final Date currentDate = new Date();
        final Collection<CmrIndexRequest> cmrIndexRequests = batch.getCmrIndexRequests();
        for (final CmrIndexRequest request : cmrIndexRequests) {
            request.setExecutionDate(currentDate);
            this.cmrIndexRequestDao.updateObject(request);
        }
        setIndexationDate(currentDate, cmrIndexRequests);
    }

    /**
     * Sets the indexation date.
     *
     * @param currentDate      the current date
     * @param cmrIndexRequests the cmr index requests
     */
    private void setIndexationDate(final Date currentDate, final Collection<CmrIndexRequest> cmrIndexRequests) {
        cmrIndexRequests.stream()
                .map(CmrIndexRequest::getGroupByUri)
                .collect(Collectors.toSet())
                .forEach(groupByUri -> {
                    final String prefixedFromUri = identifierService.getPrefixedFromUri(groupByUri);
                    // in the event of a delete
                    // the records will no longer exist
                    // but an DELETE IDOL NOTICE request will be issued
                    cellarResourceDao.updateIndexationDate(prefixedFromUri, currentDate);
                });
    }

    @Override
    @Transactional
    public void finalizeCmrIndexRequestBatch(final CmrIndexRequestBatch batch, final ExecutionStatus executionStatus, String statusCause) {
        for (CmrIndexRequest request : batch.getCmrIndexRequests()) {
            request.setExecutionDate(new Date());
            if (request.getExecutionStatus() == ExecutionStatus.Execution) {
                request.setExecutionStatus(ExecutionStatus.Error);
            }
            if (statusCause != null && !statusCause.isEmpty()) {
                request.setStatusCause(statusCause);
            }
            cmrIndexRequestDao.updateObject(request);
        }
    }

    @Override
    public Collection<CmrIndexRequest> findIndexRequests(CmrIndexRequestBatch batch) {
        return cmrIndexRequestDao.findByExecutionStatusAndGroupUriAndMinPriorityAndMaxCreatedOn(
                ExecutionStatus.New, batch.getGroupByUri(), batch.getMinimumPriority(), batch.getMaximumCreatedOn());
    }

    @Override
    @Transactional
    public int resetPendingExecutionCmrIndexRequests() {
        int rows = cmrIndexRequestDao.updateExecutionStatus(ExecutionStatus.New, ExecutionStatus.Pending, ExecutionStatus.Execution);
        LOG.info("{} rows updated with the status New.", rows);
        return rows;
    }

    @Override
    @Transactional
    public void resetPendingRequests() {
        int rows = cmrIndexRequestDao.updateExecutionStatus(ExecutionStatus.New, ExecutionStatus.Pending);
        LOG.info("{} rows updated with the status New.", rows);
    }

    @Override
    @Transactional
    public void updateBatchStatus(CmrIndexRequestBatch batch, ExecutionStatus status) {
        batch.getCmrIndexRequests().forEach(r -> updateRequestStatus(r, status));
    }

    @Override
    @Transactional
    public void updateBatchStatus(CmrIndexRequestBatch batch, ExecutionStatus newStatus, String statusCause) {
        batch.getCmrIndexRequests().forEach(r -> updateRequestStatus(r, newStatus, statusCause));
    }

    @Override
    @Transactional
    public void updateRequestStatus(CmrIndexRequest request, ExecutionStatus status) {
        updateRequestStatus(request, status, null);
    }

    @Override
    @Transactional
    public void updateRequestStatus(CmrIndexRequest request, ExecutionStatus status, String statusCause) {
        LOG.debug(INDEXATION, "Request: {}, {}, {} -> " + status.toString().toUpperCase(), request.getObjectUri(), request.getPriority(), request.getExecutionStatus());
        request.setExecutionStatus(status);
        request.setStatusCause(statusCause);
        cmrIndexRequestDao.updateObject(request);
    }

    @Override
    @Transactional
    public void restartFailedCmrIndexRequests() {
        restartCmrIndexRequests(ExecutionStatus.Error);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateStructMapIndxStatusAndRespectiveDatetime(Collection<CmrIndexRequest> cmrIndexRequests, ExecutionStatus newStatus) {
        // Filter the index requests of interest (i.e. the ones that have a non-null StructMapStatusHistoryId value)
        List<CmrIndexRequest> calcNoticeOrExpandedRequests = cmrIndexRequests.stream()
                .filter(indx -> indx.getStructMapStatusHistoryId() != null)
                .collect(Collectors.toList());
        
        StructMapStatusHistory structMapStatusHistory;
        switch (newStatus) {
            case Pending:
                updateStructMapInfoForPending(calcNoticeOrExpandedRequests);
                // Check whether to pause thread execution for testing purposes
                if (this.cellarConfiguration.isTestEnabled()
                        && this.cellarConfiguration.getTestProcessMonitorPhase().equals(ProcessMonitorPhase.Indexation_Pending)) {
                    this.testingThreadService.checkpointFreezingControl(ProcessMonitorPhase.Indexation_Pending);
                }
                break;
            case Execution:
                updateStructMapInfoForExecution(calcNoticeOrExpandedRequests);
                // Check whether to pause thread execution for testing purposes
                if (this.cellarConfiguration.isTestEnabled()
                        && this.cellarConfiguration.getTestProcessMonitorPhase().equals(ProcessMonitorPhase.Indexation_Execution)) {
                    this.testingThreadService.checkpointFreezingControl(ProcessMonitorPhase.Indexation_Execution);
                }
                break;
            case Error:
                updateStructMapInfoForError(calcNoticeOrExpandedRequests);
                break;
            case Redundant:
                updateStructMapInfoForRedundant(calcNoticeOrExpandedRequests);
                // Check whether to pause thread execution for testing purposes
                if (this.cellarConfiguration.isTestEnabled()
                        && this.cellarConfiguration.getTestProcessMonitorPhase().equals(ProcessMonitorPhase.Indexation_Redundant)) {
                    this.testingThreadService.checkpointFreezingControl(ProcessMonitorPhase.Indexation_Redundant);
                }
                break;
            default:
                for (CmrIndexRequest cmrIndexRequest : calcNoticeOrExpandedRequests){
                    structMapStatusHistory = structMapStatusHistoryService.getEntry(cmrIndexRequest.getStructMapStatusHistoryId());
                    if(structMapStatusHistory != null) {
                        structMapStatusHistoryService.updateStructMapIndxStatusAndExecutionDate(structMapStatusHistory,
                                cmrIndexRequest.getExecutionDate(),
                                cmrIndexRequest.getExecutionStatus().equals(CmrIndexRequest.ExecutionStatus.Error) ? IndexExecutionStatus.E : IndexExecutionStatus.D);
                    }
                }
                // Check whether to pause thread execution for testing purposes
                if (this.cellarConfiguration.isTestEnabled()
                        && this.cellarConfiguration.getTestProcessMonitorPhase().equals(ProcessMonitorPhase.Indexation_Complete)) {
                    this.testingThreadService.checkpointFreezingControl(ProcessMonitorPhase.Indexation_Complete);
                }
                break;
        }
    }
    
    /**
     * Update the INDX_EXECUTION_STATUS of the corresponding STRUCTMAP_STATUS_HISTORY entries,
     * in case of Pending
     *
     * @param calcNoticeOrExpandedRequests the index requests that have a non-null structMapStatus_id
     */
    private void updateStructMapInfoForPending(List<CmrIndexRequest> calcNoticeOrExpandedRequests) {
        StructMapStatusHistory structMapStatusHistory;
        for (CmrIndexRequest cmrIndexRequest : calcNoticeOrExpandedRequests) {
            structMapStatusHistory = this.structMapStatusHistoryService.getEntry(cmrIndexRequest.getStructMapStatusHistoryId());
            if (structMapStatusHistory != null) {
                structMapStatusHistoryService.updateStructMapIndxStatusToPending(structMapStatusHistory);
            }
        }
    }
    
    /**
     * Update the INDX_EXECUTION_STATUS and INDX_EXECUTION_DATE of the corresponding STRUCTMAP_STATUS_HISTORY entries,
     * in case of Redundant
     * @param calcNoticeOrExpandedRequests the index requests that have a non-null structMapStatus_id
     */
    private void updateStructMapInfoForRedundant(List<CmrIndexRequest> calcNoticeOrExpandedRequests) {
        StructMapStatusHistory structMapStatusHistory;
        for (CmrIndexRequest cmrIndexRequest : calcNoticeOrExpandedRequests){
            structMapStatusHistory = this.structMapStatusHistoryService.getEntry(cmrIndexRequest.getStructMapStatusHistoryId());
            if(structMapStatusHistory != null) {
                structMapStatusHistoryService.updateStructMapIndxStatusAndExecutionDate(structMapStatusHistory,
                        null, IndexExecutionStatus.R);
            }
        }
    }
    
    /**
     * Update the INDX_EXECUTION_STATUS and INDX_EXECUTION_DATE of the corresponding STRUCTMAP_STATUS_HISTORY entries,
     * in case of Error
     * @param calcNoticeOrExpandedRequests the index requests that have a non-null structMapStatus_id
     */
    private void updateStructMapInfoForError(List<CmrIndexRequest> calcNoticeOrExpandedRequests) {
        StructMapStatusHistory structMapStatusHistory;
        for (CmrIndexRequest cmrIndexRequest : calcNoticeOrExpandedRequests){
            structMapStatusHistory = this.structMapStatusHistoryService.getEntry(cmrIndexRequest.getStructMapStatusHistoryId());
            if(structMapStatusHistory != null) {
                this.structMapStatusHistoryService.updateStructMapIndxStatusAndExecutionDate(structMapStatusHistory,
                        cmrIndexRequest.getExecutionDate() != null ? cmrIndexRequest.getExecutionDate() : new Date(),
                        IndexExecutionStatus.E);
            }
        }
    }
    
    /**
     * Update the INDX_EXECUTION_STATUS and INDX_EXECUTION_START_DATE of the corresponding STRUCTMAP_STATUS_HISTORY entries,
     * in case of Execution
     * @param calcNoticeOrExpandedRequests the index requests that have a non-null structMapStatus_id
     */
    private void updateStructMapInfoForExecution(List<CmrIndexRequest> calcNoticeOrExpandedRequests) {
        StructMapStatusHistory structMapStatusHistory;
        for (CmrIndexRequest cmrIndexRequest : calcNoticeOrExpandedRequests){
            structMapStatusHistory = this.structMapStatusHistoryService.getEntry(cmrIndexRequest.getStructMapStatusHistoryId());
            if(structMapStatusHistory != null) {
                this.structMapStatusHistoryService.updateStructMapIndxStatusAndExecutionStartDate(structMapStatusHistory,
                        cmrIndexRequest.getExecutionStartDate());
            }
        }
    }
    
    /**
     * Restart cmr index requests.
     *
     * @param executionStatus the execution status
     */
    private void restartCmrIndexRequests(final ExecutionStatus executionStatus) {
        int count = this.restartCmrIndexRequests(executionStatus, BATCH_SIZE);

        while (count >= BATCH_SIZE) {
            count = this.restartCmrIndexRequests(executionStatus, BATCH_SIZE);
        }
    }

    /**
     * Restart cmr index requests.
     *
     * @param executionStatus the execution status
     * @param batchSize       the batch size
     * @return the int
     */
    private int restartCmrIndexRequests(final ExecutionStatus executionStatus, final int batchSize) {
        final Collection<CmrIndexRequest> cmrIndexRequests = cmrIndexRequestDao.findByExecutionStatus(executionStatus, batchSize);

        for (CmrIndexRequest cmrIndexRequest : cmrIndexRequests) {
            cmrIndexRequest.setExecutionStatus(ExecutionStatus.New);
            cmrIndexRequestDao.updateObject(cmrIndexRequest);
        }

        LOG.info("{} index requests in '{}' have been restarted.", cmrIndexRequests.size(), executionStatus);
        return cmrIndexRequests.size();
    }

    @Override
    @Transactional
    public void removeOldCmrIndexRequests() {
        if (cellarConfiguration.isCellarServiceIndexingCleanerEnabled()) {
            cleanCmrIndexRequests(ExecutionStatus.Done, cellarConfiguration.getCellarServiceIndexingCleanerDoneMinimumAgeDays());
            cleanCmrIndexRequests(ExecutionStatus.Redundant, cellarConfiguration.getCellarServiceIndexingCleanerRedundantMinimumAgeDays());
            cleanCmrIndexRequests(ExecutionStatus.Error, cellarConfiguration.getCellarServiceIndexingCleanerErrorMinimumAgeDays());
        } else {
            LOG.info("The index requests cleaner is disabled.");
        }
    }

    /**
     * Clean cmr index requests.
     *
     * @param executionStatus the execution status
     * @param minimumAge      the minimum age
     */
    private void cleanCmrIndexRequests(final ExecutionStatus executionStatus, final int minimumAge) {
        final Calendar maxExecution = Calendar.getInstance();
        maxExecution.add(Calendar.DATE, -minimumAge);

        int deleted = cmrIndexRequestDao.deleteIndexRequest(maxExecution.getTime(), executionStatus);
        LOG.info("{} index requests in '{}' and older than '{}' have been removed.", deleted, executionStatus, maxExecution);
    }
    
}
