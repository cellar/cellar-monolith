package eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr;

import eu.europa.ec.opoce.cellar.cmr.metsbeans.Expression;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Work;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.w3c.dom.Document;

/**
 * <p>IndexNoticeWork class.</p>
 */
public class IndexNoticeWork {

    private final Work work;
    private final Map<Expression, HashMap<LanguageBean, Document>> noticePerExpression = new HashMap<Expression, HashMap<LanguageBean, Document>>();
    private final Map<LanguageBean, Document> noticePerLanguage = new HashMap<LanguageBean, Document>();

    /**
     * <p>Constructor for IndexNoticeWork.</p>
     *
     * @param work a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Work} object.
     */
    public IndexNoticeWork(final Work work) {
        this.work = work;
    }

    /**
     * <p>add.</p>
     *
     * @param expression a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Expression} object.
     * @param document   a {@link org.w3c.dom.Document} object.
     */
    public void add(final Expression expression, final LanguageBean languageBean, final Document document) {
        this.noticePerExpression.get(expression).put(languageBean, document);
    }

    /**
     * <p>addNoticePerExpression.</p>
     *
     * @param noticePerExpression a {@link java.util.Map} object.
     */
    public void addNoticePerExpression(final Map<Expression, HashMap<LanguageBean, Document>> noticePerExpression) {
        this.noticePerExpression.putAll(noticePerExpression);
    }

    /**
     * <p>add.</p>
     *
     * @param languageBean a {@link eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean} object.
     * @param document     a {@link org.w3c.dom.Document} object.
     */
    public void add(final LanguageBean languageBean, final Document document) {
        this.noticePerLanguage.put(languageBean, document);
    }

    /**
     * <p>addNoticePerLanguage.</p>
     *
     * @param noticePerLanguage a {@link java.util.Map} object.
     */
    public void addNoticePerLanguage(final Map<LanguageBean, Document> noticePerLanguage) {
        this.noticePerLanguage.putAll(noticePerLanguage);
    }

    /**
     * <p>Getter for the field <code>work</code>.</p>
     *
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Work} object.
     */
    public Work getWork() {
        return this.work;
    }

    /**
     * <p>Getter for the field <code>noticePerExpression</code>.</p>
     *
     * @return a {@link java.util.Map} object.
     */
    public Map<Expression, HashMap<LanguageBean, Document>> getNoticePerExpression() {
        return Collections.unmodifiableMap(this.noticePerExpression);
    }

    /**
     * <p>Getter for the field <code>noticePerLanguage</code>.</p>
     *
     * @return a {@link java.util.Map} object.
     */
    public Map<LanguageBean, Document> getNoticePerLanguage() {
        return Collections.unmodifiableMap(this.noticePerLanguage);
    }
}
