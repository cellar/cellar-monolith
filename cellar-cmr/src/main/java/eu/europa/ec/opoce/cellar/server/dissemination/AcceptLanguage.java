package eu.europa.ec.opoce.cellar.server.dissemination;

import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.server.service.DisseminationException;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedCaseInsensitiveMap;

/**
 * <p>AcceptLanguage class.</p>
 */
public class AcceptLanguage {

    /**
     * Constant <code>PARAM_QUALITY_FACTOR="q"</code>
     */
    private static final String PARAM_QUALITY_FACTOR = "q";
    /**
     * Constant <code>PARAM_CHARSET="charset"</code>
     */
    private static final String PARAM_CHARSET = "charset";

    /**
     * Constant <code>TOKEN</code>
     */
    private static final BitSet TOKEN;

    static {
        // variable names refer to RFC 2616, section 2.2
        final BitSet ctl = new BitSet(128);
        for (int i = 0; i <= 31; i++) {
            ctl.set(i);
        }
        ctl.set(127);

        final BitSet separators = new BitSet(128);
        separators.set('(');
        separators.set(')');
        separators.set('<');
        separators.set('>');
        separators.set('@');
        separators.set(',');
        separators.set(';');
        separators.set(':');
        separators.set('\\');
        separators.set('\"');
        separators.set('/');
        separators.set('[');
        separators.set(']');
        separators.set('?');
        separators.set('=');
        separators.set('{');
        separators.set('}');
        separators.set(' ');
        separators.set('\t');

        TOKEN = new BitSet(128);
        TOKEN.set(0, 128);
        TOKEN.andNot(ctl);
        TOKEN.andNot(separators);
    }

    private final String languageCode;

    private final String countryCode;

    private Map<String, String> parameters;

    /**
     * Create a new {@link AcceptLanguage} for the given languageCode, countryCode, and parameters.
     *
     * @param languageCode the language code
     * @param countryCode  the country code, may be <code>null</code>
     * @param parameters   the parameters, may be <code>null</code>
     * @throws DisseminationException if any of the parameters contain illegal characters
     */
    public AcceptLanguage(final String languageCode, final String countryCode, final Map<String, String> parameters) {
        if (StringUtils.isBlank(languageCode)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("languageCode must not be blank").build();
        }

        this.languageCode = languageCode;
        this.countryCode = countryCode;

        if (!CollectionUtils.isEmpty(parameters)) {
            //create a map that contains the same data as the original map, only to lower case and checked to only have validated data
            final Map<String, String> m = new LinkedCaseInsensitiveMap<String>(parameters.size(), Locale.ENGLISH);
            for (final Map.Entry<String, String> entry : parameters.entrySet()) {
                final String attribute = entry.getKey();
                final String value = entry.getValue();
                checkParameters(attribute, value);
                m.put(attribute, unquote(value));
            }
            this.parameters = Collections.unmodifiableMap(m);
        } else {
            this.parameters = Collections.emptyMap();
        }
    }

    /**
     * <p>checkParameters.</p>
     *
     * @param attribute a {@link java.lang.String} object.
     * @param value     a {@link java.lang.String} object.
     */
    private void checkParameters(final String attribute, String value) {
        if (StringUtils.isBlank(attribute)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("parameter attribute 'accept-language' token must not be blank")
                    .build();
        }
        if (StringUtils.isBlank(value)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("parameter value 'accept-language' token must not be blank")
                    .build();
        }

        //check that the attribute has no invalid characters
        checkToken(attribute);
        if (PARAM_QUALITY_FACTOR.equals(attribute)) {
            //check fro valid double
            value = unquote(value);
            final double d = Double.parseDouble(value);
            if (d < 0D || d > 1D) {
                throw ExceptionBuilder.get(CellarException.class).withMessage("Invalid quality value '{}': should be between 0.0 and 1.0")
                        .withMessageArgs(value).build();
            }
        } else if (PARAM_CHARSET.equals(attribute)) {
            //check for valid charset
            value = unquote(value);
            Charset.forName(value);
        } else if (!isQuotedString(value)) {
            //check that value has no invalid characters
            checkToken(value);
        }
    }

    /**
     * Checks the given token string for illegal characters, as defined in RFC 2616, section 2.2.
     *
     * @param s a {@link java.lang.String} object.
     * @throws CellarException in case of illegal characters
     * @see <a href="http://tools.ietf.org/html/rfc2616#section-2.2">HTTP 1.1, section 2.2</a>
     */
    private void checkToken(final String s) {
        for (int i = 0; i < s.length(); i++) {
            final char ch = s.charAt(i);
            if (!TOKEN.get(ch)) {
                throw ExceptionBuilder.get(CellarException.class).withMessage("Invalid token character '{}' in token \"{}\"")
                        .withMessageArgs(ch, s).build();
            }
        }
    }

    /**
     * <p>isQuotedString gives an indication of the fact that the sting is between quotes or not.</p>
     *
     * @param s a {@link java.lang.String} object.
     * @return a boolean.
     */
    private boolean isQuotedString(final String s) {
        return s.length() > 1 && s.startsWith("\"") && s.endsWith("\"");
    }

    /**
     * <p>unquote makes sure the string is not between quotes.</p>
     *
     * @param s a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    private String unquote(final String s) {
        if (s == null) {
            return null;
        }
        return isQuotedString(s) ? s.substring(1, s.length() - 1) : s;
    }

    /**
     * <p>Getter for the field <code>languageCode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLanguageCode() {
        return languageCode;
    }

    /**
     * <p>Getter for the field <code>countryCode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCountryCode() {
        return countryCode;
    }

    /**
     * <p>getQuality returns the quality of an language.</p>
     *
     * @return a {@link java.lang.Double} object.
     */
    public Double getQuality() {
        String qualityFactory = null;
        if (null != parameters) {
            qualityFactory = parameters.get(PARAM_QUALITY_FACTOR);
        }
        return qualityFactory != null ? Double.parseDouble(qualityFactory) : 1D;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        appendTo(builder);
        return builder.toString();
    }

    /**
     * <p>appendTo fills in all information in a stringbuilder.</p>
     *
     * @param builder a {@link java.lang.StringBuilder} object.
     */
    private void appendTo(final StringBuilder builder) {
        builder.append(this.languageCode);
        if (StringUtils.isNotBlank(this.countryCode)) {
            builder.append("-").append(this.countryCode);
        }
        if (this.parameters != null) {
            appendTo(this.parameters, builder);
        }
    }

    /**
     * Parse the given, comma-seperated string into a list of {@link AcceptLanguage} objects.
     * <p>This method can be used to parse an Accept-Language header.
     *
     * @param acceptLanguages the string to parse
     * @return the list of accept languages
     * @throws CellarException if the string cannot be parsed
     */
    public static SortedAcceptLanguages parseAcceptLanguages(String acceptLanguages) {
        acceptLanguages = StringUtils.trim(acceptLanguages);
        if (StringUtils.isBlank(acceptLanguages) || acceptLanguages.equals("*")) {
            return new SortedAcceptLanguages();
        }
        //split string of languages in tokens/each of them a language
        final String[] tokens = acceptLanguages.split(",\\s*");
        final SortedAcceptLanguages result = new SortedAcceptLanguages(tokens.length);
        for (final String token : tokens) {
            if (!"*".equals(StringUtils.trim(token)) && StringUtils.isNotBlank(token)) {
                result.add(parseAcceptLanguage(token));
            }
        }
        return result;
    }

    /**
     * Parse the given String into a single {@link AcceptLanguage}.
     *
     * @param acceptLanguage the string to parse
     * @return the accept language
     * @throws CellarException if the string cannot be parsed
     */
    public static AcceptLanguage parseAcceptLanguage(final String acceptLanguage) {
        if (StringUtils.isBlank(acceptLanguage)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("'acceptLanguage' must not be blank").build();
        }

        //split acceptlanguage in language en parameters
        final String[] parts = org.springframework.util.StringUtils.tokenizeToStringArray(acceptLanguage, ";");
        //extract language and country
        final String languagePart = parts[0].trim();

        final int index = languagePart.indexOf('-');
        // no country part if languagePart does not contain '-' or does not contain any characters after '-'
        final boolean hasCountryPart = index != -1 && index < languagePart.length() - 1;

        final String languageTag = StringUtils.trim(index == -1 ? languagePart : languagePart.substring(0, index));
        final String countryTag = StringUtils.trim(hasCountryPart ? languagePart.substring(index + 1, languagePart.length()) : null);

        if (languageTag.length() < 2) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Illegal accept-language token '{}'")
                    .withMessageArgs(acceptLanguage).build();
        }
        //extract parameters
        Map<String, String> parameters = null;
        if (parts.length > 1) {
            parameters = new LinkedHashMap<String, String>(parts.length - 1);
            for (int i = 1; i < parts.length; i++) {
                final String parameter = parts[i];
                final int eqIndex = parameter.indexOf('=');
                if (eqIndex != -1) {
                    final String attribute = parameter.substring(0, eqIndex);
                    final String value = parameter.substring(eqIndex + 1, parameter.length());
                    parameters.put(attribute, value);
                }
            }
        }

        return new AcceptLanguage(languageTag, countryTag, parameters);
    }

    /**
     * <p>appendTo fills in all information in a stringbuilder.</p>
     *
     * @param map     a {@link java.util.Map} object.
     * @param builder a {@link java.lang.StringBuilder} object.
     */
    private void appendTo(final Map<String, String> map, final StringBuilder builder) {
        for (final String key : map.keySet()) {
            builder.append(';');
            builder.append(key);
            builder.append('=');
            builder.append(map.get(key));
        }
    }

    /**
     * <p>toString returns string-representation of a collection of acceptlanguages.</p>
     *
     * @param acceptLanguages a {@link java.util.Collection} object.
     * @return a {@link java.lang.String} object.
     */
    public static String toString(List<AcceptLanguage> acceptLanguages) {
        acceptLanguages = new ArrayList<AcceptLanguage>(acceptLanguages);
        sortByQualityValue(acceptLanguages);

        final StringBuilder builder = new StringBuilder();
        for (final Iterator<AcceptLanguage> iterator = acceptLanguages.iterator(); iterator.hasNext();) {
            iterator.next().appendTo(builder);
            if (iterator.hasNext()) {
                builder.append(", ");
            }
        }
        return builder.toString();
    }

    /**
     * <p>getLanguageCodes return an ordered list by quality of the languagecodes.</p>
     *
     * @param acceptLanguages a {@link java.util.Collection} object.
     * @return a {@link java.util.List} object.
     */
    public static List<String> getLanguageCodes(List<AcceptLanguage> acceptLanguages) {
        final List<String> languageCodes = new ArrayList<String>(acceptLanguages.size());

        acceptLanguages = new ArrayList<AcceptLanguage>(acceptLanguages);
        sortByQualityValue(acceptLanguages);
        for (final AcceptLanguage acceptLanguage : acceptLanguages) {
            languageCodes.add(StringUtils.trim(acceptLanguage.getLanguageCode()));
        }
        return languageCodes;
    }

    /**
     * <p>sortByQualityValue.</p>
     *
     * @param acceptLanguages a {@link java.util.List} object.
     */
    public static void sortByQualityValue(final List<AcceptLanguage> acceptLanguages) {
        Assert.notNull(acceptLanguages, "'Accept-language header cannot be null");
        if (acceptLanguages.size() > 1) {
            Collections.sort(acceptLanguages, QUALITY_VALUE_COMPARATOR);
        }
    }

    /**
     * Constant <code>QUALITY_VALUE_COMPARATOR</code>
     */
    private static final Comparator<AcceptLanguage> QUALITY_VALUE_COMPARATOR = new Comparator<AcceptLanguage>() {

        @Override
        public int compare(final AcceptLanguage acceptLanguageHeader, final AcceptLanguage acceptLanguageHeader1) {
            final double quality1 = acceptLanguageHeader.getQuality();
            final double quality2 = acceptLanguageHeader1.getQuality();
            return Double.compare(quality2, quality1);
        }
    };

}
