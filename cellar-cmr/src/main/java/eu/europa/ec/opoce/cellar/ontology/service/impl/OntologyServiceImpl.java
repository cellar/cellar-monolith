/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ontology
 *             FILE : OntologyServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 01 10, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-01-10 15:19:27 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ontology.service.impl;

import com.google.common.base.Stopwatch;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.util.concurrent.FutureCallback;
import com.google.common.util.concurrent.Futures;
import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditBuilder;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventAction;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventProcess;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventType;
import eu.europa.ec.opoce.cellar.cl.domain.ontology.FileMetadata;
import eu.europa.ec.opoce.cellar.cl.domain.ontology.OntologyModel;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.database.dao.OntoConfigDao;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.HierarchyElement;
import eu.europa.ec.opoce.cellar.common.CatalogResolver;
import eu.europa.ec.opoce.cellar.common.concurrent.NamedThreadFactory;
import eu.europa.ec.opoce.cellar.configuration.OntologyConfiguration;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.feed.domain.impl.ontology.OntologyHistory;
import eu.europa.ec.opoce.cellar.feed.service.HistoryService;
import eu.europa.ec.opoce.cellar.feed.util.OntologyBridge;
import eu.europa.ec.opoce.cellar.ontology.dao.OntoConfig;
import eu.europa.ec.opoce.cellar.ontology.domain.OntologyChangedEvent;
import eu.europa.ec.opoce.cellar.ontology.domain.OntologyUpdate;
import eu.europa.ec.opoce.cellar.ontology.domain.OntologyWrapper;
import eu.europa.ec.opoce.cellar.ontology.exception.IllegalOntologyFileNameException;
import eu.europa.ec.opoce.cellar.ontology.exception.OntologyException;
import eu.europa.ec.opoce.cellar.ontology.service.OntologyService;
import eu.europa.ec.opoce.cellar.ontology.service.OntologyUpdateResult;
import eu.europa.ec.opoce.cellar.ontology.service.OntologyUpdateService;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import eu.europa.ec.opoce.cellar.semantic.UriResolver;
import eu.europa.ec.opoce.cellar.semantic.inferredontology.InferredOntologyConfiguration;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaQueries;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.graph.BlankNodeId;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.graph.Triple;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.AnonId;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.vocabulary.RDFS;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.PreDestroy;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import static com.google.common.util.concurrent.MoreExecutors.listeningDecorator;
import static eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration.ONTOLOGY;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.skosXl_literalForm;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.skosXl_prefLabel;
import static java.lang.System.currentTimeMillis;
import static java.util.Objects.requireNonNull;
import static java.util.concurrent.Executors.newSingleThreadExecutor;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.apache.jena.graph.Triple.create;
import static org.apache.jena.vocabulary.OWL2.propertyChainAxiom;
import static org.apache.jena.vocabulary.RDF.Nodes.first;
import static org.apache.jena.vocabulary.RDF.Nodes.nil;
import static org.apache.jena.vocabulary.RDF.Nodes.rest;

/**
 * @author ARHS Developments
 * @since 7.7
 */
@Service
public class OntologyServiceImpl implements OntologyService, OntologyBridge {

    private static final Logger LOG = LogManager.getLogger(OntologyServiceImpl.class);

    private static final String OWL_ONTOLOGY = "http://www.w3.org/2002/07/owl#Ontology";
    private static final String OWL_IMPORTS = "http://www.w3.org/2002/07/owl#imports";
    private static final String NS_TYPE = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";

    private OntologyWrapper ontology;

    private final ListeningExecutorService executor = listeningDecorator(newSingleThreadExecutor(new NamedThreadFactory("ontology-load")));

    private final ApplicationEventPublisher eventPublisher;

    private final ICellarConfiguration cellarConfiguration;

    private final OntologyConfiguration ontologyConfiguration;

    private final OntologyUpdateService ontologyUpdateService;

    private final InferredOntologyConfiguration inferredOntologyConfiguration;

    private final OntoConfigDao ontoConfigDao;

    private final ContentStreamService contentStreamService;

    private final ApplicationContext ctx;

    private final HistoryService historyService;

    private final CatalogResolver catalogResolver;

    @Autowired
    public OntologyServiceImpl(OntologyConfiguration ontologyConfiguration, InferredOntologyConfiguration inferredOntologyConfiguration,
                               @Qualifier("cellarConfiguration") ICellarConfiguration cellarConfiguration, OntoConfigDao ontoConfigDao,
                               HistoryService historyService, CatalogResolver catalogResolver, OntologyUpdateService ontologyUpdateService,
                               ApplicationEventPublisher eventPublisher, ContentStreamService contentStreamService, ApplicationContext ctx) {
        this.ontologyConfiguration = ontologyConfiguration;
        this.inferredOntologyConfiguration = inferredOntologyConfiguration;
        this.cellarConfiguration = cellarConfiguration;
        this.ontoConfigDao = ontoConfigDao;
        this.historyService = historyService;
        this.catalogResolver = catalogResolver;
        this.ontologyUpdateService = ontologyUpdateService;
        this.eventPublisher = eventPublisher;
        this.contentStreamService = contentStreamService;
        this.ctx = ctx;
    }

    @PreDestroy
    void preDestroy() {
        if (!executor.isTerminated()) {
            executor.shutdown();
        }
    }

    @Override
    public OntologyWrapper getWrapper() {
        return getOntologyWrapper();
    }

    @Override
    @Cacheable(value = "ontology-cache", key = "#ontoPid")
    public byte[] findOntology(String ontoPid, ContentType contentType) {
        OntoConfig config = ontoConfigDao.findByPid(ontoPid).orElseThrow(() -> new OntologyException("ONTOCONFIG not found [" + ontoPid + "] "));
        return contentStreamService.getContentAsString(ontoPid.replace(":", "/"), config.getVersion(), contentType)
                .map(s -> s.getBytes(Charset.forName("UTF-8")))
                .orElseThrow(() -> new OntologyException("Retrieve of the ontology [" + ontoPid + "] failed."));
    }

    @Override
    public void update(List<File> resources, String cellarId) throws OntologyException {
        update(resources,cellarId ,true);
    }

    @Override
    public void update(List<File> resources,String cellarId ,boolean override) throws OntologyException {
        LOG.debug(ONTOLOGY, "Update (with override={}) list of files [{}]", override, resources);
        try {
            final Map<String, OntologyModel> loadedObjects = new HashMap<>();
            for (File file : resources) {
                if (!file.getName().endsWith(".rdf") && !file.getName().endsWith(".owl")) {
                    throw new IllegalOntologyFileNameException("The file must end with .rdf or .owl. Current value is " + file.getName());
                }
                OntologyModel model = new OntologyModel();
                model.setModelFile(file);
                model.setFileMetadata(new FileMetadata(file.getName(), file.length()));

                parseModel(loadedObjects, model);
            }
            validateImports(loadedObjects);
            load(loadedObjects,cellarId,override);

        } catch (Exception e) {
            throw new OntologyException("Cannot update or create the " + (resources.size() > 1 ? "ontologies" : "ontology"), e);
        }
    }

    @Override
    public Collection<String> getEmbargoDateProperties(DigitalObjectType digitalObjectType, boolean withSuperProperties) {
        final Collection<String> result = new ArrayList<>();
        result.add(HierarchyElement.getTriple(digitalObjectType).getTwo().getURI());
        if (withSuperProperties) {
            Resource embargoR = getInferredOntologyModel().getResource(HierarchyElement.getTriple(digitalObjectType).getTwo().getURI());
            result.addAll(JenaQueries.getResources(embargoR, RDFS.subPropertyOf, false).stream()
                    .map(input -> input.isURIResource() ? input.getURI() : null)
                    .collect(Collectors.toList()));
        }
        return result;
    }
    private OntologyWrapper initializeWrapper(){
        LOG.debug("Initializing ontology wrapper...");
        Stopwatch elapsed=Stopwatch.createStarted();
        ontology = new OntologyWrapper(ontologyConfiguration, inferredOntologyConfiguration,
                // OntologyUriResolver make the link between Oracle (where the catalog is stored)
                // and S3 (where the datastream are stored)
                // ctx.getBean(): Ugly fix to pass the proxy to the Resolver
                // instead of the real instance. This is done to make the @Cacheable
                // annotation work. To be remove when the ingestion will be refactored
                // and getWrapper no longer need backward compatibility.
                new OntologyUriResolver(catalogResolver, ctx.getBean(OntologyService.class)));
        ontology.getOntologyData();
        ontology.getFallbackOntologyData();
        LOG.debug("Initializing ontology wrapper took : {}s",elapsed.elapsed(MILLISECONDS)/1000.0);
        return ontology;
    }
    private OntologyWrapper getOntologyWrapper() {
        if (ontology == null) {
            synchronized(this){
                //Fix to avoid ConcurrentModification error, this way if the ontology has
                //been already initialized the thread waiting will return
                if(ontology!=null)return ontology;
                initializeWrapper();

            }
        }
        return ontology;
    }

    private Model getInferredOntologyModel() {
        return getOntologyWrapper().getOntology().getInferredOntology();
    }

    /**
     * Load the models First checks which are the model descriptors that do not
     * have any other model descriptor referencing them These will be considered
     * as the main model descriptors to load This will separate the selection by
     * groups In one selection there might be different groups depending on the
     * above mentioned rule
     *
     * @param loadedObjects the loaded objects
     * @return true, if successful
     * @throws IOException if something wrong happened when reading the files
     */
    private boolean load(Map<String, OntologyModel> loadedObjects,String cellarId, boolean override) throws IOException {
        // Identify the loading trees. The base uri that are not listed in any imports list will be
        // considered as the main ontology to load and starting point
        for (Map.Entry<String, OntologyModel> entry : loadedObjects.entrySet()) {
            // Check if this model is referenced by any other model in their imports if so set it on the importedByURIs
            for (Map.Entry<String, OntologyModel> entryAux : loadedObjects.entrySet()) {
                if (entryAux.getValue().getImportURIs().contains(entry.getKey())) {
                    entry.getValue().addImportedByURIs(entryAux.getKey());
                }
            }
        }

        for (Map.Entry<String, OntologyModel> entry : loadedObjects.entrySet()) {
            final OntologyModel model = entry.getValue();
            if (model.getImportedByURIs().isEmpty()) { // only treat those that
                // Do not have model referencing them start loading
                final Map<String, Model> streams = new HashMap<>();
                streams.put(model.getBaseURI(), model.getModel());
                getLoadedImports(model, loadedObjects, streams);

                if (!override) { // internal loading, log is not displayed to avoid misunderstanding (model is built but not necessarily persisted)
                    LOG.info("Loading the ontology for : [{}]", model.getBaseURI());
                }
                scheduleLoad(model.getBaseURI(), cellarId,streams, override);
            }
        }
        return true;
    }

    /**
     * Asynchronously write the new ontology into the datasource.
     * If the process successfully complete, the event {@link OntologyChangedEvent}
     * is send. Every Spring beans which listen this event will be called.
     *
     * @param ontologyUri the URI of the ontology
     * @param streams     the content of the ontology
     */
    private void scheduleLoad(final String ontologyUri, String cellarId,final Map<String, Model> streams, boolean override) {
        ListenableFuture<Optional<OntologyUpdateResult>> f = executor.submit(() -> load(ontologyUri, cellarId,streams, override));
        Futures.addCallback(f, new FutureCallback<Optional<OntologyUpdateResult>>() {
            @Override
            public void onSuccess(Optional<OntologyUpdateResult> result) {
                final AtomicBoolean atLeastOneUpdated = new AtomicBoolean(false);
                result.ifPresent(r -> {
                    // Close all the models
                    r.getModels().forEach((model, updated) -> {
                        if (!model.isClosed()) {
                            model.close();
                        }
                        if (updated) {
                            atLeastOneUpdated.set(true);
                        }
                    });
                    if (atLeastOneUpdated.get()) {
                        LOG.info("Ontology [{}] has been updated.", ontologyUri);
                        ontology=null;
                        eventPublisher.publishEvent(new OntologyChangedEvent(ontologyUri));
                    }
                });
            }

            @Override
            public void onFailure(Throwable t) {
                LOG.error("Unexpected exception during load process ontology", t);
            }
        }, MoreExecutors.directExecutor());
    }

    private Optional<OntologyUpdateResult> load(final String ontologyUri,String cellarId, final Map<String, Model> streams, boolean override) {
        Assert.isTrue(!Strings.isNullOrEmpty(ontologyUri), "Required argument ontologyUri must not be blank");
        Assert.isTrue(!streams.isEmpty(), "Required argument streams must not be empty");

        if (cellarConfiguration.isCellarServiceOntoLoadEnabled()) {
            final long start = currentTimeMillis();
            LOG.debug(ONTOLOGY, "Install new ontology [{}]...", ontologyUri);
            final OntologyUpdate ontologyUpdateInfo = newOntologyUpdate(ontologyUri, streams);
            LOG.debug(ONTOLOGY, "Update ontology [{}] with [{}]", ontologyUri, ontologyUpdateInfo);
            OntologyUpdateResult result = ontologyUpdateService.update(ontologyUpdateInfo, override);
            updateHistory(ontologyUri,cellarId);
            LOG.debug(ONTOLOGY, "Reload process ontology '{}' successfully completed in {} sec.",
                    ontologyUri, MILLISECONDS.toSeconds(currentTimeMillis() - start));
            return Optional.of(result);
        } else {
            LOG.debug(ONTOLOGY, "Ontology Load service is disabled. [{}] has not been loaded.");
        }
        return Optional.empty();
    }

    private void updateHistory(final String ontologyUri,String cellarId) {
        ontoConfigDao.findByUri(ontologyUri)
                .map(cfg -> {
                    OntologyHistory history = new OntologyHistory();
                    history.setUri(ontologyUri);
                    history.setVersion(version(cfg));
                    if(cellarId!=null) {
                        history.setCellarId(cellarId);
                        history.setIdentifiers(String.join(",", this.historyService.getProductionIdentifierNamesForCellarId(cellarId)));
                    }
                    return history;
                })
                .ifPresent(h -> {
                    historyService.store(h);
                    eventPublisher.publishEvent(h);
                });
    }

    private static String version(OntoConfig cfg) {
        if (cfg.getVersionDate().after(new Date(1)) && StringUtils.isNotBlank(cfg.getVersionNr())) {
            return cfg.getVersionNr();
        }
        return CellarProperty.cdm_NALOntoVersionUnknown;
    }

    private static OntologyUpdate newOntologyUpdate(String ontologyUri, Map<String, Model> updatedOntologyResources) {
        final OntologyUpdate ontologyUpdate = new OntologyUpdate(ontologyUri);
        for (Map.Entry<String, Model> entry : updatedOntologyResources.entrySet()) {
            if (Strings.isNullOrEmpty(entry.getKey()) || entry.getValue() == null) {
                throw new IllegalArgumentException("URI or Stream cannot be null");
            }
            ontologyUpdate.add(entry.getKey(), entry.getValue());
        }
        return ontologyUpdate;
    }

    /**
     * Parses the model. To retrieve the base URI of the model descriptor and
     * referenced imports
     *
     * @param loadedObjects the loaded objects
     * @param model         the model
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void parseModel(final Map<String, OntologyModel> loadedObjects, final OntologyModel model) throws IOException {
        try (InputStream is=OntologyUtils.toInputStream(model.getModelFile())) {
            final InputStreamResource byteArrayResource = new InputStreamResource(is);
            final Model parsedModel = JenaUtils.read(byteArrayResource, RDFFormat.RDFXML_PLAIN);
            final String baseURI = getBaseUri(parsedModel);
            LOG.debug(ONTOLOGY, "Parse model [{}]", baseURI);
            if (isSKOS(baseURI)) {
                LOG.debug(ONTOLOGY, "Apply SKOS triples to [{}]", baseURI);
                for (Triple triple : getSKOSTriples()) {
                    parsedModel.add(parsedModel.asStatement(triple));
                }
            }

            model.setImportURIs(getImports(parsedModel));
            model.setBaseURI(baseURI);
            model.setModel(parsedModel);
            loadedObjects.put(baseURI, model);
        }
    }

    private boolean isSKOS(String ontologyUri) {
        return StringUtils.equals(ontologyConfiguration.getNalOntologyUri(), ontologyUri);
    }

    @Override
    public List<Triple> getSKOSTriples() {
        final List<Node> nodes = generateAnonNodes(6);
        return Lists.newArrayList(
                create(NodeFactory.createURI(CellarProperty.skos_prefLabel), propertyChainAxiom.asNode(), nodes.get(0)),
                create(nodes.get(0), first, NodeFactory.createURI(skosXl_prefLabel)),
                create(nodes.get(0), rest, nodes.get(1)),
                create(nodes.get(1), first, NodeFactory.createURI(skosXl_literalForm)),
                create(nodes.get(1), rest, nil),

                create(NodeFactory.createURI(CellarProperty.skos_altLabel), propertyChainAxiom.asNode(), nodes.get(2)),
                create(nodes.get(2), first, NodeFactory.createURI(CellarProperty.skosXl_altLabel)),
                create(nodes.get(2), rest, nodes.get(3)),
                create(nodes.get(3), first, NodeFactory.createURI(skosXl_literalForm)),
                create(nodes.get(3), rest, nil),

                create(NodeFactory.createURI(CellarProperty.skos_hiddenLabel), propertyChainAxiom.asNode(), nodes.get(4)),
                create(nodes.get(4), first, NodeFactory.createURI(CellarProperty.skosXl_hiddenLabel)),
                create(nodes.get(4), rest, nodes.get(5)),
                create(nodes.get(5), first, NodeFactory.createURI(skosXl_literalForm)),
                create(nodes.get(5), rest, nil)
        );
    }

    private static List<Node> generateAnonNodes(int count) {
        List<Node> nodes = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            nodes.add(NodeFactory.createBlankNode(BlankNodeId.create(AnonId.create().getLabelString())));
        }
        return nodes;
    }


    /**
     * Gets the base uri. goes through the model descriptor using jena and
     * retrieves the base URI
     *
     * @param model the parsed model
     * @return the base uri
     */
    private static String getBaseUri(final Model model) {
        StmtIterator stmtIterator = null;
        try {
            // Add the base URI
            stmtIterator = model.listStatements(null, model.createProperty(NS_TYPE), model.createResource(OWL_ONTOLOGY));
            if (stmtIterator.hasNext()) {
                final Statement statement = stmtIterator.next();
                return statement.getSubject().toString();
            }

        } finally {
            if (stmtIterator != null) {
                stmtIterator.close();
            }
        }
        return null;
    }

    /**
     * Gets the imports. retrieves the imports from the model descriptor using
     * Jena
     *
     * @param parsedModel the parsed model
     * @return the imports
     */
    private static List<String> getImports(final Model parsedModel) {
        StmtIterator stmtIterator = null;
        try {
            List<String> imports = new ArrayList<>();
            // add the imports
            stmtIterator = parsedModel.listStatements(null, parsedModel.createProperty(OWL_IMPORTS), (RDFNode) null);
            while (stmtIterator.hasNext()) {
                final Statement statement = stmtIterator.next();
                imports.add(statement.getResource().toString());
            }
            return imports;

        } finally {
            if (stmtIterator != null) {
                stmtIterator.close();
            }
        }
    }

    private static void getLoadedImports(final OntologyModel model, final Map<String, OntologyModel> loadedObjects,
                                         final Map<String, Model> updatedOntologyResources) {
        for (String importedURI : model.getImportURIs()) {
            final OntologyModel importedModel = loadedObjects.get(importedURI);
            if (importedModel != null) {
                updatedOntologyResources.put(importedModel.getBaseURI(), importedModel.getModel());
                // Get children
                getLoadedImports(importedModel, loadedObjects, updatedOntologyResources);
            }
        }
    }

    /**
     * Validate imports. goes through the list of loaded model descriptors and
     * checks if there are missing imports if so a message is addded
     *
     * @param loadedObjects the loaded objects
     */
    private void validateImports(final Map<String, OntologyModel> loadedObjects) {
        // validate that imports references exist
        for (Map.Entry<String, OntologyModel> entry : loadedObjects.entrySet()) {
            OntologyModel model = entry.getValue();
            // Check if this model has references to files that were not uploaded
            for (final String importURI : model.getImportURIs()) {
                if (loadedObjects.get(importURI) == null) {
                    final FileMetadata fileMetadata = model.getFileMetadata();
                    LOG.warn("There are missing imports. The uploaded file: {} is missing the referenced import: {}", fileMetadata.getName(), importURI);
                    audit(fileMetadata.getName(), importURI);
                }
            }
        }
        LOG.debug(ONTOLOGY, "Import validated for [{}]", loadedObjects::keySet);
    }

    private static void audit(String fileName, String importURI) {
        //TODO refactor Audit system to make it less intrusive and verbose.
        AuditBuilder.get(AuditTrailEventProcess.Ontology)
                .withAction(AuditTrailEventAction.Load)
                .withType(AuditTrailEventType.End)
                .withObject(fileName)
                .withMessage("There are missing imports. The uploaded file: {} is missing the referenced import: {}")
                .withMessageArgs(fileName, importURI)
//                .withLogger(LOG)
                .withLogDatabase(true)
                .logEvent();
    }

    /**
     * TODO remove after refactoring cellar modularization
     */
    @Override
    public OntModel getOntModel() {
        return getWrapper().getOntology().getOntModel();
    }

    private static class OntologyUriResolver implements UriResolver {

        private final CatalogResolver catalogResolver;
        private final OntologyService ontologyService;

        OntologyUriResolver(CatalogResolver catalogResolver, OntologyService ontologyService) {
            this.catalogResolver = requireNonNull(catalogResolver);
            this.ontologyService = requireNonNull(ontologyService);
        }

        @Override
        public org.springframework.core.io.Resource resolveUri(String uri) {
            final String pid = catalogResolver.resolve(uri);
            LOG.debug(ONTOLOGY, "Resolved URI [{}] -> [{}] (PID)", uri, pid);
            return new ByteArrayResource(ontologyService.findOntology(pid, ContentType.DIRECT));
        }
    }
}
