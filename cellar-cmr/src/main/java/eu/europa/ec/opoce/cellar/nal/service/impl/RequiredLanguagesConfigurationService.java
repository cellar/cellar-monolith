/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.nal.languageconfiguration
 *             FILE : RequiredLanguagesConfigurationService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 19-Apr-2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.nal.service.impl;

import eu.europa.ec.opoce.cellar.ccr.service.LanguagesNalSkosLoaderService;
import eu.europa.ec.opoce.cellar.common.util.StringUtils;
import eu.europa.ec.opoce.cellar.nal.dao.RequiredLanguageDbRecord;
import eu.europa.ec.opoce.cellar.nal.domain.LanguagesSettings;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.nal.repository.RequiredLanguageDbGateway;
import eu.europa.ec.opoce.cellar.nal.service.LanguagesConfiguration;
import eu.europa.ec.opoce.cellar.nal.service.RequiredLanguagesConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 19-Apr-2017
 * The Class RequiredLanguagesConfigurationService.
 *
 * @author ARHS Developments
 * @version $$Revision$$$
 */
@Service
public class RequiredLanguagesConfigurationService implements RequiredLanguagesConfiguration, LanguagesConfiguration {

    private final LanguagesSettings languageSettings;
    private final LanguagesNalSkosLoaderService languagesNalSkosLoaderService;
    private final RequiredLanguageDbGateway requiredLanguageDbGateway;

    @Autowired
    public RequiredLanguagesConfigurationService(LanguagesNalSkosLoaderService languagesNalSkosLoaderService,
                                                 RequiredLanguageDbGateway requiredLanguageDbGateway) {
        this.languagesNalSkosLoaderService = languagesNalSkosLoaderService;
        this.requiredLanguageDbGateway = requiredLanguageDbGateway;
        this.languageSettings = new LanguagesSettings(getIndexLanguages());
    }

    /**
     * Get index languages.
     *
     * @return index languages
     */
    @Override
    public List<LanguageBean> getIndexLanguages() {
        final List<LanguageBean> languages = new ArrayList<>();
        final Map<String, List<RequiredLanguageDbRecord>> indexingLanguages = requiredLanguageDbGateway.selectAll();
        for (final Map.Entry<String, List<RequiredLanguageDbRecord>> entry : indexingLanguages.entrySet()) {
            final String uri = entry.getKey();
            final String iso3Code = uri.substring(uri.length() - 3);
            LanguageBean languageBean = languagesNalSkosLoaderService.getLanguageBeanByThreeCharCode(StringUtils.lowerCase(iso3Code));
            languages.add(languageBean);
        }
        return languages;
    }

    @Override
    public List<LanguageBean> getLanguages() {
        return this.languageSettings.getLanguages();
    }
}
