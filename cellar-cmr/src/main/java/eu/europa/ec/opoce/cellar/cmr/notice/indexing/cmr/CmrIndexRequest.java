package eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr;

import com.google.common.base.MoreObjects;
import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService.Priority;
import eu.europa.ec.opoce.cellar.cmr.database.DaoObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import org.apache.commons.lang.StringUtils;
import org.springframework.util.Assert;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * <p>CmrIndexRequest class.</p>
 */
/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 11, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class CmrIndexRequest extends DaoObject implements Serializable {

    /**
     * The Enum Action.
     */
    public enum Action {

        /** The Create. */
        Create, /** The Update. */
        Update, /** The Update id. */
        UpdateId, /** The Remove. */
        Remove
    }

    /**
     * The Enum ExecutionStatus.
     */
    public enum ExecutionStatus {

        New("N"), //
        Pending("P"), //
        Execution("X"), //
        Done("D"), //
        Error("E"),
        Waiting("W"),
        Redundant("R");

        /**
         * Find by value.
         *
         * @param value the value
         * @return the execution status
         */
        public static ExecutionStatus findByValue(final String value) {
            if (StringUtils.isBlank(value)) {
                return null;
            }

            for (final ExecutionStatus executionStatus : values()) {
                if (value.equals(executionStatus.value)) {
                    return executionStatus;
                }
            }

            return null;
        }

        /** The value. */
        private final String value;

        /**
         * Instantiates a new execution status.
         *
         * @param value the value
         */
        private ExecutionStatus(final String value) {
            this.value = value;
        }

        /**
         * Gets the value.
         *
         * @return the value
         */
        public String getValue() {
            return this.value;
        }
    }

    /**
     * The Enum Reason.
     */
    public enum Reason {

        CodedByNAL("Coded by NAL"), //The Coded by nal.
        IngestedObject("Ingested object"), //The Ingested object.
        Administrator("Administrator"), //The Administrator.
        DirectlyLinkedObject("Directly linked obj"), //The Directly linked object.
        InverseLinkedObject("Inverse linked obj"), //The Inverse linked object.
        Scheduled("Scheduled");//Scheduled for re-indexation

        /**
         * Find by value.
         *
         * @param value the value
         * @return the reason
         */
        public static Reason findByValue(final String value) {
            if (StringUtils.isBlank(value)) {
                return null;
            }

            for (final Reason reason : values()) {
                if (value.equals(reason.value)) {
                    return reason;
                }
            }

            return null;
        }

        /** The value. */
        private final String value;

        /**
         * Instantiates a new reason.
         *
         * @param value the value
         */
        private Reason(final String value) {
            this.value = value;
        }

        /**
         * Gets the value.
         *
         * @return the value
         */
        public String getValue() {
            return this.value;
        }
    }

    /**
     * The Enum RequestType.
     */
    public enum RequestType {
        CalcInverse("I", 0),
        CalcEmbedded("E", 1),
        CalcNotice("N", 2),
        CalcExpanded("X", 3),
        Skip("S", -1);

        /**
         * Find by value.
         *
         * @param value the value
         * @return the request type
         */
        public static RequestType findByValue(final String value) {
            if (StringUtils.isBlank(value)) {
                return null;
            }

            for (final RequestType requestType : values()) {
                if (value.equals(requestType.value)) {
                    return requestType;
                }
            }

            return null;
        }

        /**
         * Gets the status.
         *
         * @param calcInverse the calc inverse
         * @param calcEmbedded the calc embedded
         * @param calcNotice the calc notice
         * @param calcExpanded the calc expanded
         * @param skip the skip
         * @return the status
         */
        public static Collection<RequestType> getStatus(final boolean calcInverse, final boolean calcEmbedded, final boolean calcNotice,
                final boolean calcExpanded, final boolean skip) {
            final List<RequestType> s = new LinkedList<RequestType>();

            if (calcInverse) {
                s.add(CalcInverse);
            }

            if (calcEmbedded) {
                s.add(CalcEmbedded);
            }

            if (calcNotice) {
                s.add(CalcNotice);
            }

            if (calcExpanded) {
                s.add(CalcExpanded);
            }

            if (skip) {
                s.add(Skip);
            }

            return s;
        }

        /** The value. */
        private final String value;

        /** The order. */
        private final int order;

        /**
         * Instantiates a new request type.
         *
         * @param value the value
         * @param order the order
         */
        private RequestType(final String value, final int order) {
            this.value = value;
            this.order = order;
        }

        /**
         * Gets the order.
         *
         * @return the order
         */
        public int getOrder() {
            return this.order;
        }

        /**
         * Gets the value.
         *
         * @return the value
         */
        public String getValue() {
            return this.value;
        }
    }

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -7802542406535428580L;

    /** The object uri. */
    private String objectUri;

    /** The object type. */
    private DigitalObjectType objectType;

    /** The group by uri. */
    private String groupByUri;

    /** The created on. */
    private Date createdOn;

    /** The mets document id. */
    private String metsDocumentId;

    /** The structmap id. */
    private String structmapId;

    /** The concept scheme. */
    private String conceptScheme;

    /** The reason. */
    private Reason reason;

    /** The requested by. */
    private String requestedBy;

    /** The priority. */
    private Priority priority;

    /** The action. */
    private Action action;

    /** The request type. */
    private RequestType requestType;

    /** The execution status. */
    private ExecutionStatus executionStatus;

    private String statusCause;

    /** The execution date. */
    private Date executionDate;

    /** The execution start date. */
    private Date executionStartDate;
    
    /** The STRUCTMAP_STATUS_HISTORY identifier (in case of 'Ingested Object' requests of 'CalcNotice')*/
    private Long structMapStatusHistoryId;

    /**
     * <p>Constructor for CmrIndexRequest.</p>
     */
    public CmrIndexRequest() {
        this.executionStatus = ExecutionStatus.New;
    }

    /**
     * <p>Constructor for CmrIndexRequest.</p>
     *
     * @param indexRequest a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest} object.
     */
    public CmrIndexRequest(final CmrIndexRequest indexRequest) {
        this.objectUri = indexRequest.objectUri;
        this.objectType = indexRequest.objectType;
        this.groupByUri = indexRequest.groupByUri;
        this.createdOn = indexRequest.createdOn;
        this.metsDocumentId = indexRequest.metsDocumentId;
        this.conceptScheme = indexRequest.conceptScheme;
        this.reason = indexRequest.reason;
        this.requestedBy = indexRequest.requestedBy;
        this.priority = indexRequest.priority;
        this.action = indexRequest.action;
        this.requestType = indexRequest.requestType;
        this.executionStatus = indexRequest.executionStatus;
        this.executionDate = indexRequest.executionDate;
        this.structMapStatusHistoryId = indexRequest.structMapStatusHistoryId;
    }

    /**
     * <p>Getter for the field <code>action</code>.</p>
     *
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.Action} object.
     */
    public Action getAction() {
        return this.action;
    }

    /**
     * <p>Getter for the field <code>conceptScheme</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getConceptScheme() {
        return this.conceptScheme;
    }

    /**
     * <p>Getter for the field <code>createdOn</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getCreatedOn() {
        return new Date(this.createdOn.getTime());
    }

    /**
     * Gets the execution date.
     *
     * @return the executionDate
     */
    public Date getExecutionDate() {
        return this.executionDate;
    }

    /**
     * Gets the execution start date.
     *
     * @return the execution start date
     */
    public Date getExecutionStartDate() {
        return this.executionStartDate;
    }

    /**
     * Gets the execution status.
     *
     * @return the executionStatus
     */
    public ExecutionStatus getExecutionStatus() {
        return this.executionStatus;
    }

    /**
     * <p>Getter for the field <code>groupByUri</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getGroupByUri() {
        return this.groupByUri;
    }

    /**
     * <p>Getter for the field <code>metsDocumentId</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getMetsDocumentId() {
        return this.metsDocumentId;
    }

    /**
     * <p>Getter for the field <code>objectType</code>.</p>
     *
     * @return a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType} object.
     */
    public DigitalObjectType getObjectType() {
        return this.objectType;
    }

    /**
     * <p>Getter for the field <code>objectUri</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getObjectUri() {
        return this.objectUri;
    }

    /**
     * <p>Getter for the field <code>priority</code>.</p>
     *
     * @return a {@link Priority} object.
     */
    public Priority getPriority() {
        return this.priority;
    }

    /**
     * <p>Getter for the field <code>reason</code>.</p>
     *
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.Reason} object.
     */
    public Reason getReason() {
        return this.reason;
    }

    /**
     * <p>Getter for the field <code>requestedBy</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getRequestedBy() {
        return this.requestedBy;
    }

    /**
     * Gets the request type.
     *
     * @return the requestType
     */
    public RequestType getRequestType() {
        return this.requestType;
    }

    /**
     * <p>Getter for the field <code>structmapId</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getStructmapId() {
        return this.structmapId;
    }
    
    /**
     * <p>Getter for the field <code>structMapStatusHistoryId</code>.</p>
     *
     * @return a {@link java.lang.Long} object.
     */
    public Long getStructMapStatusHistoryId() {
        return structMapStatusHistoryId;
    }

    /**
     * <p>Setter for the field <code>action</code>.</p>
     *
     * @param action a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.Action} object.
     */
    public void setAction(final Action action) {
        this.action = action;
    }

    /**
     * <p>Setter for the field <code>conceptScheme</code>.</p>
     *
     * @param conceptScheme a {@link java.lang.String} object.
     */
    public void setConceptScheme(final String conceptScheme) {
        this.conceptScheme = conceptScheme;
    }

    /**
     * <p>Setter for the field <code>createdOn</code>.</p>
     *
     * @param createdOn a {@link java.util.Date} object.
     */
    public void setCreatedOn(final Date createdOn) {
        this.createdOn = new Date(createdOn.getTime());
    }

    /**
     * Sets the execution date.
     *
     * @param executionDate the executionDate to set
     */
    public void setExecutionDate(final Date executionDate) {
        this.executionDate = executionDate;
    }

    /**
     * Sets the execution start date.
     *
     * @param executionStartDate the new execution start date
     */
    public void setExecutionStartDate(final Date executionStartDate) {
        this.executionStartDate = executionStartDate;
    }

    /**
     * Sets the execution status.
     *
     * @param executionStatus the executionStatus to set
     */
    public void setExecutionStatus(final ExecutionStatus executionStatus) {
        this.executionStatus = executionStatus;
    }

    /**
     * <p>Setter for the field <code>groupByUri</code>.</p>
     *
     * @param groupByUri a {@link java.lang.String} object.
     */
    public void setGroupByUri(final String groupByUri) {
        this.groupByUri = groupByUri;
    }

    /**
     * <p>Setter for the field <code>metsDocumentId</code>.</p>
     *
     * @param metsDocumentId a {@link java.lang.String} object.
     */
    public void setMetsDocumentId(final String metsDocumentId) {
        this.metsDocumentId = metsDocumentId;
    }

    /**
     * <p>Setter for the field <code>objectType</code>.</p>
     *
     * @param objectType a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType} object.
     */
    public void setObjectType(final DigitalObjectType objectType) {
        this.objectType = objectType;
    }

    /**
     * <p>Setter for the field <code>objectUri</code>.</p>
     *
     * @param objectUri a {@link java.lang.String} object.
     */
    public void setObjectUri(final String objectUri) {
        this.objectUri = objectUri;
    }

    /**
     * <p>Setter for the field <code>priority</code>.</p>
     *
     * @param priority a int.
     */
    public void setPriority(final int priority) {
        final Priority byPriorityValue = Priority.findByPriorityValue(priority);
        Assert.isTrue(byPriorityValue != null);
        this.priority = byPriorityValue;
    }

    /**
     * <p>Setter for the field <code>priority</code>.</p>
     *
     * @param priority a {@link Priority} object.
     */
    public void setPriority(final Priority priority) {
        this.priority = priority;
    }

    /**
     * <p>Setter for the field <code>reason</code>.</p>
     *
     * @param reason a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.Reason} object.
     */
    public void setReason(final Reason reason) {
        this.reason = reason;
    }

    /**
     * <p>Setter for the field <code>requestedBy</code>.</p>
     *
     * @param requestedBy a {@link java.lang.String} object.
     */
    public void setRequestedBy(final String requestedBy) {
        this.requestedBy = requestedBy;
    }

    /**
     * Sets the request type.
     *
     * @param requestType the requestType to set
     */
    public void setRequestType(final RequestType requestType) {
        this.requestType = requestType;
    }

    /**
     * <p>Setter for the field <code>structmapId</code>.</p>
     *
     * @param structmapId a {@link java.lang.String} object.
     */
    public void setStructmapId(final String structmapId) {
        this.structmapId = structmapId;
    }

    public String getStatusCause() {
        return statusCause;
    }

    public void setStatusCause(String statusCause) {
        this.statusCause = statusCause;
    }
    
    /**
     * <p>Setter for the field <code>structMapStatusHistoryId</code>.</p>
     *
     * @param structMapStatusHistoryId a {@link java.lang.Long} object.
     */
    public void setStructMapStatusHistoryId(Long structMapStatusHistoryId) {
        this.structMapStatusHistoryId = structMapStatusHistoryId;
    }
    
    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("objectUri", objectUri)
                .add("objectType", objectType)
                .add("groupByUri", groupByUri)
                .add("createdOn", createdOn)
                .add("metsDocumentId", metsDocumentId)
                .add("structmapId", structmapId)
                .add("conceptScheme", conceptScheme)
                .add("reason", reason)
                .add("requestedBy", requestedBy)
                .add("priority", priority)
                .add("action", action)
                .add("requestType", requestType)
                .add("executionStatus", executionStatus)
                .add("executionDate", executionDate)
                .add("executionStartDate", executionStartDate)
                .add("statusCause", statusCause)
                .add("structMapStatusHistoryId", structMapStatusHistoryId)
                .toString();
    }
}
