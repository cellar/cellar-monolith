/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.graph.impl
 *             FILE : UpdateGraphBuilder.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : May 2, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.graph.impl;

/**
 * <class_description> Graph builder implementation for update ingestion.
 * <br/><br/>
 * ON : May 2, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class UpdateGraphBuilder extends IngestionGraphBuilder {

    /**
     * @see eu.europa.ec.opoce.cellar.cmr.graph.IGraphBuilder#buildRelations()
     */
    @Override
    public GraphBuilder buildRelations() {
        this.initializeFromObjects();
        return this.buildDirectRelations().buildInverseRelationsOnSource().buildInverseRelationsOnTarget();
    }
}
