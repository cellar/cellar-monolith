/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.cmr.ingest.impl.update
 *        FILE : UpdateModelLoader.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 21-05-2013
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.ingest.impl.update;

import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.structmap_status_history.StructMapStatusHistory;
import eu.europa.ec.opoce.cellar.cl.domain.validation.MetadataValidationRequest;
import eu.europa.ec.opoce.cellar.cl.domain.validation.MetadataValidationResult;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.ingest.IMetadataLoaderService;
import eu.europa.ec.opoce.cellar.cmr.ingest.impl.AbstractModelLoader;
import eu.europa.ec.opoce.cellar.cmr.model.ModelUtils;
import eu.europa.ec.opoce.cellar.common.map.SetBasedMap;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.exception.CellarValidationException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.MetadataValidationResultAwareException;
import eu.europa.ec.opoce.cellar.exception.MetadataValidationResultAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.ontology.service.OntologyService;
import eu.europa.ec.opoce.cellar.semantic.validation.ValidationResultCache;
import eu.europa.ec.opoce.cellar.semantic.validation.NalValidationUtils;
import org.apache.jena.graph.Triple;
import org.apache.jena.rdf.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import static eu.europa.ec.opoce.cellar.CommonErrors.DIRECT_MODEL_VALIDATION_FAILED;
import static eu.europa.ec.opoce.cellar.cl.domain.validation.MetadataValidationResult.ModelType.DIRECT;

/**
 * <class_description> Class for conveniently load models for creation ingests.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 22-05-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class UpdateModelLoader extends AbstractModelLoader {

    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    @Autowired
    private OntologyService ontologyService;

    @Autowired
    private IMetadataLoaderService metadataLoaderService;

    public UpdateModelLoader() {

    }

    @Override
    protected void validateDirectModel() {
        // validates direct model against the ontology
        final Model directModel = this.getBusinessModels().get(ContentType.DIRECT);
        List<String> invalidUriList = NalValidationUtils.validateUris(directModel);
        if (!invalidUriList.isEmpty()) {
            throw ExceptionBuilder.get(CellarValidationException.class)
                    .withCode(CmrErrors.VALIDATION_RDFDATA_ERROR)
                    .withMessage("Invalid URIs found in model : {}")
                    .withMessageArgs(invalidUriList)
                    .build();
        }
        Model directWithoutItem=null;
        if(cellarConfiguration.getCellarServiceIngestionValidationItemExclusionEnabled()) {
            //create copy of model
            directWithoutItem = ModelFactory.createDefaultModel().add(directModel);
            //remove all item related triples
            ModelUtils.removeItemTriples(directWithoutItem);
        }

        final MetadataValidationResult result = validationService.validate(new MetadataValidationRequest(directWithoutItem==null?directModel:directWithoutItem)
                .cellarId(safeCellarId()));
        
        String requiredShaclReport = filterShaclReport(result.getValidationResult());
        if ((requiredShaclReport != null) && (data.getStructMapStatusHistory() != null)){
            StructMapStatusHistory structMapStatusHistory = this.structMapStatusHistoryService.updateStructMapShaclReport(data.getStructMapStatusHistory(), requiredShaclReport);
            data.setStructMapStatusHistory(structMapStatusHistory);
        }
        
        result.setModelType(DIRECT);

        ValidationResultCache.put(getData().getStructMap(), result);

        if (!result.conformModelProvided()) {
            throw MetadataValidationResultAwareExceptionBuilder.getInstance(MetadataValidationResultAwareException.class)
                    .withStructMapId(getData().getStructMap().getId())
                    .withMetadataValidationResult(result)
                    .withMetsDocumentId(getData().getStructMap().getMetsDocument().getDocumentId())
                    .withMessage("Validation of the direct model failed : {}")
                    .withCode(DIRECT_MODEL_VALIDATION_FAILED)
                    .withMessageArgs(result.getErrorMessage())
                    .build();
        }

        // checks the write-once triples on the direct model
        if (cellarConfiguration.isCellarServiceIngestionCheckWriteOncePropertiesEnabled()) {
            final SetBasedMap<String> sameAses = ModelUtils.getSameAses(getData().getStructMap().getDigitalObject());

            // validates the write-once triples
            final Collection<Triple> protectedTriplesExistingInOldButNotInNewModel = ModelUtils.validateProtectedTriples(
                    getData().getStructMap().getMetsDocument().getDocumentId(), ontologyService, directModel,
                    getData().getExistingDirectAndInferredModel().asModel(), sameAses);

            // if there are existing write-once triples but no corresponding triple on the new METS, add them to the DMD model
            for (final Triple triple : protectedTriplesExistingInOldButNotInNewModel) {
                final RDFNode rdfNode = directModel.asRDFNode(triple.getObject());
                directModel.add(ResourceFactory.createResource(triple.getSubject().getURI()),
                        ResourceFactory.createProperty(triple.getPredicate().getURI()), rdfNode);
            }
        }

        // creationDate must be checked in any case.
        validateCreationDate();
    }

    void validateCreationDate() {
        final Model directModel = getBusinessModels().get(ContentType.DIRECT);
        final SetBasedMap<String> sameAses = ModelUtils.getSameAses(getData().getStructMap().getDigitalObject());

        final Collection<Triple> creationDateTriplesExistingInOldButNotInNewModel = ModelUtils.validateTriplesWithCreationDate(
                getData().getStructMap().getMetsDocument().getDocumentId(), directModel,
                getData().getExistingDirectAndInferredModel().asModel(), sameAses);

        // if there are existing creationDate triples but no corresponding triple on the new METS, add them to the DMD model
        for (final Triple triple : creationDateTriplesExistingInOldButNotInNewModel) {
            final RDFNode rdfNode = directModel.asRDFNode(triple.getObject());
            directModel.add(ResourceFactory.createResource(triple.getSubject().getURI()),
                    ResourceFactory.createProperty(triple.getPredicate().getURI()), rdfNode);
        }

        // Add creationDate to new digital objects which don't exist in old model
        final Model existingFullModel = getData().getExistingDirectAndInferredModel().asModel();
        final List<DigitalObject> allChilds = getData().getStructMap().getDigitalObject().getAllChilds(true);
        final Date creationDate = new Date();
        for (final DigitalObject digitalObject : allChilds) {
            final String cellarUri = digitalObject.getCellarId().getUri();
            final Resource cellarResource = ResourceFactory.createResource(cellarUri);
            if (!existingFullModel.containsResource(cellarResource)) {
                //if digitalObject doesn't exist in old model add the creation date
                final Resource cr = directModel.getResource(digitalObject.getCellarId().getUri());
                metadataLoaderService.addCreationDateInModel(cr, directModel, creationDate);
            }
        }
    }

}
