package eu.europa.ec.opoce.cellar.cmr;

import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.ontology.service.OntologyService;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.semantic.jena.template.JenaQueryUtils;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QuerySolutionMap;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResourceFactory;

@Service
public class RelatedObjectCalculator {

    @Autowired
    private OntologyService ontologyService;

    @Autowired
    private StructuredQueryConfiguration structuredQueryConfiguration;

    @Autowired
    private InferenceService inferenceService;

    /**
     * Calculation of all the direct related objects of a certain cellarIdentification
     *
     * @param directAndInferredModel jenaModel where all the direct related object need to be searched in
     * @param contentIdentifier      cellarIdentification of which all the relations need to be searched
     * @return set of all the direct related objects
     */
    public Set<RelatedObject> calculateDirectRelatedIndexRequests(final Model directAndInferredModel,
            final ContentIdentifier contentIdentifier) {
        final Model modelToUse = JenaUtils.create(directAndInferredModel, ontologyService.getWrapper().getFallbackOntology().getModel());

        QuerySolutionMap initialBinding = new QuerySolutionMap();
        initialBinding.add("thisCUri", ResourceFactory.createResource(contentIdentifier.getUri()));

        return queryForRelatedObjects(modelToUse, structuredQueryConfiguration.resolveRelatedObjectsWithTypeQuery(), initialBinding);
    }

    /**
     * Calculation of all the inverse related objects of a certain cellarIdentification
     *
     * @param inverse   jenaModel where all the inverse related object need to be searched in
     * @param cellarUri cellarUri of which all the relations need to be searched
     * @return set of all the inverse related objects
     */
    public Set<RelatedObject> calculateInverseRelatedObjects(Model inverse, String cellarUri) {

        Model modelWithTypeInfo = null;
        Model inferred = null;
        try {
            modelWithTypeInfo = JenaUtils.create(inverse, ontologyService.getWrapper().getFallbackOntology().getModel());
            inferred = inferenceService.calculateInferredModel(modelWithTypeInfo);
            Model modelToUse = inferenceService.calculateInferredSuperclassModel(inferred);

            QuerySolutionMap initialBinding = new QuerySolutionMap();
            initialBinding.add("thisCUri", ResourceFactory.createResource(cellarUri));

            return queryForRelatedObjects(modelToUse, structuredQueryConfiguration.resolveInverseObjectsWithTypeQuery(), initialBinding);
        } finally {
            JenaUtils.closeQuietly(modelWithTypeInfo, inferred);

        }
    }

    /**
     * <p>queryForRelatedObjects.</p>
     *
     * @param modelToUse     a {@link org.apache.jena.rdf.model.Model} object.
     * @param query          a {@link org.apache.jena.query.Query} object.
     * @param initialBinding a {@link org.apache.jena.query.QuerySolutionMap} object.
     * @return a {@link java.util.Set} object.
     */
    private Set<RelatedObject> queryForRelatedObjects(Model modelToUse, Query query, QuerySolutionMap initialBinding) {
        QueryExecution queryExecution = null;
        try {
            queryExecution = JenaQueryUtils.newQueryExecution(modelToUse, query, initialBinding);
            List<Map<String, RDFNode>> maps = JenaQueryUtils.convertToListOfMaps(queryExecution.execSelect());

            return CollectionUtils.collect(maps, MapStringRDFNode2RelatedObject.instance, new HashSet<RelatedObject>());
        } finally {
            JenaQueryUtils.closeQuietly(queryExecution, modelToUse);
        }
    }

    private static class MapStringRDFNode2RelatedObject implements Transformer<Map<String, RDFNode>, RelatedObject> {

        public static Transformer<Map<String, RDFNode>, RelatedObject> instance = new MapStringRDFNode2RelatedObject();

        @Override
        public RelatedObject transform(Map<String, RDFNode> map) {
            return new RelatedObject(JenaUtils.stringize(map.get("d")), JenaUtils.stringize(map.get("t")),
                    JenaUtils.stringize(map.get("p")));
        }
    }
}
