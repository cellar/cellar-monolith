package eu.europa.ec.opoce.cellar.server.admin.sparql;

import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration;
import eu.europa.ec.opoce.cellar.common.util.ZipUtils;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.helper.FileHelper;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Helper class to handle xslt stylesheets for the sparql endpoint
 */
@Service
public class XsltResolverServiceImpl implements XsltResolverService {

    /**
     * Constant <code>log</code>
     */
    private static final Logger LOG = LogManager.getLogger(XsltResolverServiceImpl.class);

    /**
     * Constant <code>XSLT_PATH_LOCAL="sparql/xslt"</code>
     */
    private static final String XSLT_PATH_LOCAL = "sparql/xslt";
    /**
     * Constant <code>QUERY_PATH_LOCAL="sparql/query"</code>
     */
    private static final String QUERY_PATH_LOCAL = "sparql/query";
    /**
     * Constant <code>DEFAULT_STYLESHEET="sparql2html.xsl"</code>
     */
    private static final String DEFAULT_STYLESHEET = "sparql2html.xsl";

    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    private File cellarStylesheetPropertyFile;

    @PostConstruct
    private void checkResources() {
        final File cellarFolderRoot = new File(this.cellarConfiguration.getCellarFolderRoot());
        if (!cellarFolderRoot.exists()) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.GIVEN_PATH_IS_NO_FILEPATH)
                    .withMessage("Invalid path for rootFolder specified: '{}'").withMessageArgs(cellarFolderRoot.getAbsolutePath()).build();
        }

        this.cellarStylesheetPropertyFile = new File(cellarFolderRoot, "sparql/xslt/stylesheet.properties");
        if (!this.cellarStylesheetPropertyFile.exists()) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.GIVEN_PATH_IS_NO_FILEPATH)
                    .withMessage("Invalid path for propertiesFile specified: '{}'")
                    .withMessageArgs(this.cellarStylesheetPropertyFile.getAbsolutePath()).build();
        }

        LOG.info(IConfiguration.CONFIG, "XSLT for SPARQL loaded from {}", cellarStylesheetPropertyFile);

    }

    /**
     * {@inheritDoc}
     * <p/>
     * This method returns an html representation (option element) of the
     * available stylesheets
     */
    @Override
    public String getStylesheetOptions() {
        return this.loadStylesheetOptions(this.readProperties(new FileSystemResource(this.cellarStylesheetPropertyFile)));
    }

    /**
     * {@inheritDoc}
     * <p/>
     * This method returns an array of all available example queries
     */
    @Override
    public Query[] getQueries() {
        Query[] queries = new Query[0];
        if (this.getXsltFolder().isDirectory()) {
            final File[] fileList = this.getQueryFolder().listFiles(getQueryFileFilter());
            queries = new Query[fileList.length + 1];
            int i = 1;
            for (final File file : fileList) {
                final Query q = new Query(file.getName(), file.getName());
                queries[i] = q;
                i++;
            }
        }
        queries[0] = new Query("None", "");
        return queries;
    }

    /**
     * {@inheritDoc}
     * <p/>
     * This method returns the string representation of a specific query
     */
    @Override
    public String getQuery(final String query) throws IOException {
        if (StringUtils.isNotBlank(query)) {
            final FileInputStream stream = new FileInputStream(this.getQueryFile(query));
            try {
                final FileChannel fc = stream.getChannel();
                final MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                return Charset.defaultCharset().decode(bb).toString();
            } finally {
                stream.close();
            }
        }
        return "";
    }

    /**
     * {@inheritDoc}
     * <p/>
     * This method returns a map holding a stylesheet and it's return mimetype.
     * eg. 'sparql2html.xsl' => 'text/html'
     */
    @Override
    public Map<String, String> getStylesheetsWithMimetype() {
        return this.loadStylesheetsWithMimetype(this.readProperties(new FileSystemResource(this.cellarStylesheetPropertyFile)));
    }

    /**
     * {@inheritDoc}
     * <p/>
     * This method returns a list of strings of all available example queries
     */
    @Override
    public List<String> getDefinitions() {
        return this.loadDefinitionOptions();
    }

    /**
     * {@inheritDoc}
     * <p/>
     * This method deletes the specified example queries.
     */
    @Override
    public void deleteDefintions(final List<String> definitionList) {
        if (null != definitionList) {
            LOG.info("Deleting sparql definition(s): '{}'", StringUtils.join(definitionList.toArray(), ", "));
            final List<String> failed = new ArrayList<String>();
            for (final String definition : definitionList) {
                final File defintionFile = new File(this.getQueryFolder(), definition);
                if (defintionFile.exists() && defintionFile.isFile()) {
                    final boolean success = defintionFile.delete();
                    if (!success) {
                        failed.add(defintionFile.getAbsolutePath());
                    }
                }
            }
            if (!failed.isEmpty()) {
                LOG.warn("Unable to delete file(s) : '{}'", StringUtils.join(failed.toArray(), ", "));
            }
        }
    }

    /**
     * {@inheritDoc}
     * <p/>
     * This method deletes the specified stylesheets
     */
    @Override
    public void deleteStylesheets(final List<String> stylesheetList) {
        if (null != stylesheetList) {
            LOG.info("Deleting sparql stylesheet(s): '{}'", StringUtils.join(stylesheetList.toArray(), ", "));
            final List<String> failed = new ArrayList<String>();
            for (final String stylesheet : stylesheetList) {
                final File stylesheetFile = new File(this.getXsltFolder(), stylesheet);
                if (stylesheetFile.exists() && stylesheetFile.isFile()) {
                    if (stylesheetFile.getName().toUpperCase().equals(DEFAULT_STYLESHEET.toUpperCase())) {
                        LOG.warn("Unable to delete default stylesheet '{}'", DEFAULT_STYLESHEET);
                    } else {
                        final boolean success = stylesheetFile.delete();
                        if (!success) {
                            failed.add(stylesheetFile.getAbsolutePath());
                        }
                    }
                }
            }
            if (!failed.isEmpty()) {
                LOG.warn("Unable to delete file(s) : '{}'", StringUtils.join(failed.toArray(), ", "));
            }
        }
    }

    /**
     * {@inheritDoc}
     * <p/>
     * This method returns a specified stylesheet
     */
    @Override
    public String getStylesheet(final String stylesheet) {
        if (StringUtils.isNotBlank(stylesheet)) {
            final File file = new File(this.getXsltFolder(), stylesheet);
            InputStream inputStream=null;
            try {
                inputStream = new FileInputStream(file);
                return StringEscapeUtils.escapeHtml(IOUtils.toString(inputStream, "UTF-8")).replaceAll("(\r\n|\n)", "<br/>");
            } catch (final Exception e) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.GIVEN_PATH_IS_NO_FILEPATH)
                        .withMessage("Error while reading file '{}'").withMessageArgs(LOG).withCause(e).build();
            }
            finally{
                IOUtils.closeQuietly(inputStream);
            }
        }
        return "";
    }

    /**
     * {@inheritDoc}
     * <p/>
     * This method returns a specified example query
     */
    @Override
    public String getDefinition(final String definition) {
        if (StringUtils.isNotBlank(definition)) {
            final File file = new File(this.getQueryFolder(), definition);
            InputStream inputStream=null;
            try {
                inputStream = new FileInputStream(file);
                return StringEscapeUtils.escapeHtml(IOUtils.toString(inputStream, "UTF-8")).replaceAll("(\r\n|\n)", "<br/>");
            } catch (final Exception e) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.GIVEN_PATH_IS_NO_FILEPATH)
                        .withMessage("Error while reading file '{}'").withMessageArgs(LOG).withCause(e).build();
            }
            finally{
                IOUtils.closeQuietly(inputStream);
            }
        }
        return "";
    }

    /**
     * {@inheritDoc}
     * <p/>
     * This method uploads a new example query to the filesystem
     */
    @Override
    public void saveDefinition(final UploadDefinition uploadItem) {
        if (null != uploadItem) {
            if (null != uploadItem.getFileData()) {
                if (StringUtils.endsWithIgnoreCase(uploadItem.getFileData().getOriginalFilename(), ".sparqlq")) {
                    LOG.info("Saving sparql definition '{}'", uploadItem.getFileData().getOriginalFilename());
                    try {
                        final File output = new File(this.getQueryFolder(), uploadItem.getFileData().getOriginalFilename());
                        FileHelper.writeToFile(uploadItem.getFileData().getInputStream(), output);
                    } catch (final IOException e) {
                        throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.CELLAR_INTERNAL_ERROR)
                                .withMessage("Could not open or write file '{}'").withMessageArgs(uploadItem.getFileData().getName())
                                .withCause(e).build();
                    }
                } else {
                    LOG.warn("Error while saving sparql definition '{}'. File is not of type sparqlq",
                            uploadItem.getFileData().getOriginalFilename());
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     * <p/>
     * This method uploads a new stylesheet to the filesystem
     */
    @Override
    public void saveStylesheet(final UploadXslt uploadItem) {
        if (null != uploadItem) {
            if ((null != uploadItem.getFileData()) && StringUtils.isNotBlank(uploadItem.getMimetype())) {
                if (StringUtils.endsWithIgnoreCase(uploadItem.getFileData().getOriginalFilename(), ".xsl")
                        || StringUtils.endsWithIgnoreCase(uploadItem.getFileData().getOriginalFilename(), ".xslt")) {
                    LOG.info("Saving sparql stylesheet '{}'", uploadItem.getFileData().getOriginalFilename());
                    String folder = this.getFolderFromMimetype(uploadItem.getMimetype());
                    boolean adaptPropertiesFile = false;
                    if (null == folder) {
                        folder = uploadItem.getMimetype().replaceAll("[^a-zA-Z 0-9]+", "_");
                        adaptPropertiesFile = true;
                    }
                    final File outputDirectory = new File(this.getXsltFolder(), folder);

                    try {
                        if (adaptPropertiesFile) {
                            final String propFile = IOUtils
                                    .toString(new FileSystemResource(this.cellarStylesheetPropertyFile).getInputStream(), (Charset) null);
                            final String newPropFile = new StringBuilder().append(propFile).append(folder).append(" = ")
                                    .append(uploadItem.getMimetype()).append("\n").toString();
                            FileHelper.writeToFile(new ByteArrayInputStream(newPropFile.getBytes(StandardCharsets.UTF_8)),
                                    this.cellarStylesheetPropertyFile);
                        }
                        final File outputFile = new File(outputDirectory, uploadItem.getFileData().getOriginalFilename());
                        FileHelper.writeToFile(uploadItem.getFileData().getInputStream(), outputFile);
                    } catch (final IOException e) {
                        throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.CELLAR_INTERNAL_ERROR)
                                .withMessage("Could not open or write files '{}' '{}'")
                                .withMessageArgs(uploadItem.getFileData().getName(), this.cellarStylesheetPropertyFile.getAbsoluteFile())
                                .withCause(e).build();
                    }
                } else {
                    LOG.warn("Error while saving stylesheet '{}'. File is not of type xsl or xslt",
                            uploadItem.getFileData().getOriginalFilename());
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     * <p/>
     * This method uploads a new stylesheet to the filesystem
     */
    @Override
    public boolean saveStylesheet(final UploadXslt uploadItem, final File outputFolder) {
        boolean success = true;
        if (null != uploadItem) {
            if (null != uploadItem.getFileData()) {
                if (StringUtils.endsWithIgnoreCase(uploadItem.getFileData().getOriginalFilename(), ".xsl")
                        || StringUtils.endsWithIgnoreCase(uploadItem.getFileData().getOriginalFilename(), ".xslt")) {
                    LOG.info("Saving sparql stylesheet '{}'", uploadItem.getFileData().getOriginalFilename());

                    try {
                        final File outputFile = new File(outputFolder, uploadItem.getFileData().getOriginalFilename());
                        FileHelper.writeToFile(uploadItem.getFileData().getInputStream(), outputFile);
                    } catch (final IOException e) {
                        success = false;
                        throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.CELLAR_INTERNAL_ERROR)
                                .withMessage("Could not open or write files '{}'").withMessageArgs(uploadItem.getFileData().getName())
                                .withCause(e).build();
                    }
                } else {
                    success = false;
                    LOG.warn("Error while saving stylesheet '{}'. File is not of type xsl or xslt",
                            uploadItem.getFileData().getOriginalFilename());
                }
            } else {
                success = false;
            }
        } else {
            success = false;
        }
        return success;
    }

    /**
     * <p>
     * getFolderFromMimetype.
     * </p>
     *
     * @param mimetype
     *            a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    private String getFolderFromMimetype(final String mimetype) {
        if (StringUtils.isNotBlank(mimetype)) {
            final Properties properties = this.readProperties(new FileSystemResource(this.cellarStylesheetPropertyFile));
            for (final Object key : properties.keySet()) {
                final String folder = key.toString();
                final String mimetypeInPropertiesFile = properties.get(folder).toString().trim();
                if (mimetypeInPropertiesFile.equalsIgnoreCase(mimetype.trim())) {
                    return folder;
                }
            }
        }
        return null;
    }

    /**
     * <p>
     * readProperties.
     * </p>
     *
     * @param propertiesFile
     *            a {@link org.springframework.core.io.FileSystemResource}
     *            object.
     * @return a {@link java.util.Properties} object.
     */
    private Properties readProperties(final FileSystemResource propertiesFile) {
        final Properties properties = new Properties();
        InputStream inputStream = null;
        try {
            inputStream = propertiesFile.getInputStream();
            properties.load(inputStream);
            return properties;
        } catch (final IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        } finally {
            IOUtils.closeQuietly(inputStream);
        }
    }

    /**
     * <p>
     * loadStylesheetOptions.
     * </p>
     *
     * @param properties
     *            a {@link java.util.Properties} object.
     * @return a {@link java.lang.String} object.
     */
    private String loadStylesheetOptions(final Properties properties) {
        final StringBuilder sb = new StringBuilder();
        sb.append("<option selected/>\n");
        for (final Object key : properties.keySet()) {
            final String folder = key.toString();
            final String mimetype = properties.get(folder).toString();
            this.loadStylesheets(mimetype, folder, sb);
        }
        return sb.toString();
    }

    /**
     * <p>
     * loadStylesheetsWithMimetype.
     * </p>
     *
     * @param properties
     *            a {@link java.util.Properties} object.
     * @return a {@link java.util.Map} object.
     */
    private Map<String, String> loadStylesheetsWithMimetype(final Properties properties) {
        final Map<String, String> map = new HashMap<String, String>();
        for (final Object key : properties.keySet()) {
            final String folder = key.toString();
            final String mimetype = properties.get(folder).toString();
            final File stylesheetsFolder = new File(this.getXsltFolder(), ZipUtils.validateFilenameInDir(folder));
            if (stylesheetsFolder.exists() && stylesheetsFolder.isDirectory()) {
                final File[] stylesheets = stylesheetsFolder.listFiles(xsltFilenameFilter());
                for (final File stylesheet : stylesheets) {
                    map.put(new StringBuilder().append(folder).append("/").append(stylesheet.getName()).toString(), mimetype);
                }
            }
        }
        return map;
    }

    /**
     * <p>
     * loadDefinitionOptions.
     * </p>
     *
     * @return a {@link java.util.List} object.
     */
    private List<String> loadDefinitionOptions() {
        final List<String> list = new ArrayList<String>();
        final File queryFolder = this.getQueryFolder();
        if (queryFolder.exists() && queryFolder.isDirectory()) {
            final File[] definitions = queryFolder.listFiles(queryFilenameFilter());
            for (final File definition : definitions) {
                list.add(definition.getName());
            }
        }
        return list;
    }

    /**
     * <p>
     * getXsltFolder.
     * </p>
     *
     * @return a {@link java.io.File} object.
     */
    private File getXsltFolder() {
        return new File(this.cellarConfiguration.getCellarFolderRoot(), XSLT_PATH_LOCAL);
    }

    /**
     * <p>
     * getQueryFolder.
     * </p>
     *
     * @return a {@link java.io.File} object.
     */
    private File getQueryFolder() {
        return new File(this.cellarConfiguration.getCellarFolderRoot(), QUERY_PATH_LOCAL);
    }

    /**
     * <p>
     * getQueryFile.
     * </p>
     *
     * @param query
     *            a {@link java.lang.String} object.
     * @return a {@link java.io.File} object.
     */
    private File getQueryFile(final String query) {
        return new File(this.getQueryFolder(), query);
    }

    /**
     * <p>
     * loadStylesheets.
     * </p>
     *
     * @param mimetype
     *            a {@link java.lang.String} object.
     * @param folder
     *            a {@link java.lang.String} object.
     * @param sb
     *            a {@link java.lang.StringBuilder} object.
     */
    private void loadStylesheets(final String mimetype, final String folder, final StringBuilder sb) {
        final File dir = new File(this.getXsltFolder(), folder);
        if (dir.exists() && dir.isDirectory()) {
            final File[] stylesheets = dir.listFiles(xsltFilenameFilter());
            if (stylesheets.length > 0) {
                sb.append("<optgroup label=\"").append(mimetype).append("\">\n");
                for (final File stylesheet : stylesheets) {
                    final String path = new StringBuilder().append(stylesheet.getParentFile().getName()).append("/")
                            .append(stylesheet.getName()).toString();
                    final String name = stylesheet.getName().substring(0, stylesheet.getName().lastIndexOf("."));
                    sb.append("<option value=\"").append(path).append("\">").append(name).append("</option>\n");
                }
                sb.append("</optgroup>");
            }
        }
    }

    /**
     * <p>
     * xsltFilenameFilter.
     * </p>
     *
     * @return a {@link java.io.FilenameFilter} object.
     */
    private static FilenameFilter xsltFilenameFilter() {
        return new FilenameFilter() {

            @Override
            public boolean accept(final File file, final String s) {
                final int delim = s.lastIndexOf(".");
                return (s.substring(delim + 1, s.length()).equalsIgnoreCase("xsl"))
                        || (s.substring(delim + 1, s.length()).equalsIgnoreCase("xslt"));
            }
        };
    }

    /**
     * <p>
     * queryFilenameFilter.
     * </p>
     *
     * @return a {@link java.io.FilenameFilter} object.
     */
    private static FilenameFilter queryFilenameFilter() {
        return new FilenameFilter() {

            @Override
            public boolean accept(final File file, final String s) {
                return StringUtils.endsWithIgnoreCase(s, ".sparqlq");
            }
        };
    }

    /**
     * <p>
     * getQueryFileFilter.
     * </p>
     *
     * @return a {@link java.io.FileFilter} object.
     */
    private static FileFilter getQueryFileFilter() {
        return new FileFilter() {

            @Override
            public boolean accept(final File file) {
                if (file.isFile()) {
                    final int delim = file.getName().lastIndexOf(".");
                    return (file.getName().substring(delim + 1, file.getName().length()).equalsIgnoreCase("sparqlq"));
                }
                return false;
            }
        };
    }

}
