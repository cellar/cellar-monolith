/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cmr.model.deleteHandler
 *        FILE : IModelDeleteHandler.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 14-04-2014
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.model.deleteHandler;

import org.apache.jena.rdf.model.Model;

/**
 * <class_description> Interface for deleting triples from a model.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 14-04-2014
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IModelDeleteHandler<T> {

    /**
     * This method deletes from the given {@link model} the data defined in the given {@link deleteData}.
     * 
     * @param inputModel the model from which to delete data
     * @param deleteData the data to delete
     * @param preserveInputModel if <code>true</code>, the {@link inputModel} is kept unmodified, and only the number of deleted entries will be returned
     * @return the number of entries deleted
     */
    long delete(final Model inputModel, final T deleteData, final boolean preserveInputModel);

}
