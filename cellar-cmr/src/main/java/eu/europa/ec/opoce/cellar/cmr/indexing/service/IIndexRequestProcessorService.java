/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.indexing.service
 *             FILE : IIndexRequestProcessingService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Nov 7, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.indexing.service;

import eu.europa.ec.opoce.cellar.cl.service.client.IIndexRequestPoolService;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestBatch;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Nov 7, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IIndexRequestProcessorService extends IIndexRequestPoolService {

    /**
     * Execute the indexation of the Work in parameter.
     * <p>
     * // 1. remove useless index entries
     * // 2. generate index requests corresponding inverses
     * // 3. generate embedded notices
     * // 4. generate index notices for IDOL
     *
     * @param indexRequestBatch the index request batch
     */
    void submit(CmrIndexRequestBatch indexRequestBatch);

    /**
     * Pause the indexation process by cleaning the internal
     * queue of the thread pool. The running {@link CmrIndexRequestBatch}
     * complete normally and are not interrupted in order to
     * avoid inconsistent states.
     */
    void pause();

    /**
     * Resume the indexing process which was previously paused.
     * @see #pause()
     */
    void resume();

    /**
     * Checks if is indexing running.
     *
     * @return true, if is indexing running
     */
    boolean isIndexingRunning();
}
