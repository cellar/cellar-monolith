/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.virtuoso.annotation
 *             FILE : VirtuosoRetryAdvice.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 12-07-2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.virtuoso.retry.annotation;

import com.google.common.base.Throwables;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration;
import eu.europa.ec.opoce.cellar.common.retry.IRetryAwareConnection;
import eu.europa.ec.opoce.cellar.common.retry.IRetryStrategy;
import eu.europa.ec.opoce.cellar.common.retry.impl.ExponentialBackoffRetryStrategy;
import eu.europa.ec.opoce.cellar.common.retry.impl.ExponentialRetryStrategy;
import eu.europa.ec.opoce.cellar.common.retry.impl.NoRetryStrategy;
import eu.europa.ec.opoce.cellar.common.util.Wrapper;
import eu.europa.ec.opoce.cellar.exception.ExceptionUtils;
import eu.europa.ec.opoce.cellar.virtuoso.retry.VirtuosoStoreRetryAwareConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;

import java.util.Arrays;

/**
 * <class_description> This is an advice that, at each method annotated with {@link VirtuosoRetry}, drives the write operations towards the Virtuoso store by retrying, if necessary.</br>
 * The conditions under which a retry is allowed are defined by {@link eu.europa.ec.opoce.cellar.virtuoso.retry.VirtuosoStoreRetryAwareConnection}.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 12-07-2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Aspect
@Order(3)
public class VirtuosoRetryAdvice {

    private static final Logger LOG = LogManager.getLogger(VirtuosoRetryAdvice.class);

    private static final VirtuosoRetryAdvice instance = new VirtuosoRetryAdvice();

    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    @Around("@annotation(virtuosoRetry)")
    public Object process(final ProceedingJoinPoint pjp, final VirtuosoRetry virtuosoRetry) throws Throwable {
        final Wrapper<Object> retObject = new Wrapper<>();

        try {
            this.chooseStrategy().retry(ignore -> {
                try {
                    retObject.setValue(pjp.proceed(pjp.getArgs()));
                } catch (final Throwable t) {
                    if (t instanceof Exception) {
                        LOG.debug(IConfiguration.VIRTUOSO, "Failure from virtuoso {} -> retry", Throwables.getStackTraceAsString(t));
                        throw (Exception) t;
                    }
                    // something very bad happened here, which can't be handled by the annotation, nor by the retry-strategy
                    else {
                        LOG.error("Fatal error raised while running the join-point of '" + VirtuosoRetryAdvice.class + "'.", t);
                        throw new Exception(t);
                    }
                }
            });
        } catch (Exception e) {
            //catch the retry exception due to end of attempts and execute the caller method once more in order to trigger the backup mechanism
            if(ExceptionUtils.exceptionContainsMessages(e, "The execution of the retry-aware call towards Virtuoso")){
                //call method one more time with modified args to trigger Virtuoso Backup mechanism
                LOG.warn("The attempts of Virtuoso Retry mechanism have been exceeded");
                retObject.setValue(pjp.proceed(setParameterHasRetryAttemptsToFalse(pjp)));
            }else { //catch the retry exception containing un-resolvable messages
                throw e;
            }
        }
        return retObject.getValue();
    }

    /**
     * Needed by AspectJ
     */
    public static VirtuosoRetryAdvice aspectOf() {
        return instance;
    }

    private IRetryStrategy<Void> chooseStrategy() {
        IRetryStrategy<Void> retryStrategy;

        switch (this.cellarConfiguration.getVirtuosoIngestionRetryStrategy()) {
            case virtuosoNoRetry:
                retryStrategy = this.buildNoRetryStrategy();
                break;
            case virtuosoExponentialRetry:
                retryStrategy = this.buildExponentialRetryStrategy();
                break;
            case virtuosoExponentialBackoffRetry:
            default:
                retryStrategy = this.buildExponentialBackoffRetryStrategy();
        }

        return retryStrategy;
    }

    private IRetryStrategy<Void> buildNoRetryStrategy() {
        final IRetryAwareConnection<Void> retryAwareConnection = new VirtuosoStoreRetryAwareConnection();
        return new NoRetryStrategy<Void>().recipientName("Virtuoso").retryAwareConnection(retryAwareConnection);
    }

    private IRetryStrategy<Void> buildExponentialRetryStrategy() {
        final IRetryStrategy<Void> retryStrategy = new ExponentialRetryStrategy<>();
        this.configureRetryStrategy(retryStrategy);
        return retryStrategy;
    }

    private IRetryStrategy<Void> buildExponentialBackoffRetryStrategy() {
        final IRetryStrategy<Void> retryStrategy = new ExponentialBackoffRetryStrategy<>();
        this.configureRetryStrategy(retryStrategy);
        return retryStrategy;
    }

    private void configureRetryStrategy(final IRetryStrategy<Void> retryStrategy) {
        final int maxAttempts = this.cellarConfiguration.getVirtuosoIngestionRetryMaxRetries();
        final long ceilingDelay = this.cellarConfiguration.getVirtuosoIngestionRetryCeilingDelay();
        final int base = this.cellarConfiguration.getVirtuosoIngestionRetryBase();

        final IRetryAwareConnection<Void> retryAwareConnection = new VirtuosoStoreRetryAwareConnection();
        retryStrategy.maxAttempts(maxAttempts).ceilingDelay(ceilingDelay).base(base).recipientName("Virtuoso")
                .retryAwareConnection(retryAwareConnection);
    }

    /**
     * Set parameter hasRetryAttempts to false
     *
     * @param pjp the proceeding join point
     * @return modified arguments with the parameter set to false if it exists
     */
    private Object[] setParameterHasRetryAttemptsToFalse(ProceedingJoinPoint pjp) {
        Object[] modifiedArgs = pjp.getArgs();
        MethodSignature methodSig = (MethodSignature) pjp.getSignature();
        String[] parametersName = methodSig.getParameterNames();
        int idx = Arrays.asList(parametersName).indexOf("hasRetryAttempts");

        if (idx == -1) { //hasRetryAttempts does not exist
            return modifiedArgs;
        }
        //assign false to hasRetryAttempts parameter
        modifiedArgs[idx] = false;

        return modifiedArgs;
    }

}
