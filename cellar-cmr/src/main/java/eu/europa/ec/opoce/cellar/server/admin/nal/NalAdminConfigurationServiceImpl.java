/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.admin.nal
 *             FILE : NalAdminConfigurationService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 10-Jan-2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.admin.nal;

import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.database.dao.NalConfigDao;
import eu.europa.ec.opoce.cellar.common.util.Triple;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.VirtuosoOperationException;
import eu.europa.ec.opoce.cellar.nal.NalOntoUtils;
import eu.europa.ec.opoce.cellar.nal.dao.NalConfig;
import eu.europa.ec.opoce.cellar.nal.service.NalSnippetDbGateway;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import eu.europa.ec.opoce.cellar.virtuoso.VirtuosoService;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import static eu.europa.ec.opoce.cellar.CommonErrors.NAL_MISSING_URI;

/**
 * <p>
 * NalAdminConfigurationService class.
 * </p>
 */
@Component
public class NalAdminConfigurationServiceImpl implements NalAdminConfigurationService {

    private static final Logger LOG = LogManager.getLogger(NalAdminConfigurationServiceImpl.class);

    private final Transformer<NalConfig, NalOntoVersion> nalConfig2ActiveVersion = nalConfig -> {
        NalOntoVersion nalOntoVersion = new NalOntoVersion();
        nalOntoVersion.setName(nalConfig.getName());
        nalOntoVersion.setUri(nalConfig.getUri());
        nalOntoVersion.setVersionNumber(nalConfig.getVersionNr());
        final Date versionDate = nalConfig.getVersionDate();
        if (versionDate != null) {
            nalOntoVersion.setVersionDate(new Date(versionDate.getTime()));
        }
        return nalOntoVersion;
    };

    private final NalConfigDao nalConfigDao;
    private final VirtuosoService virtuosoService;
    private final ContentStreamService contentStreamService;
    private final NalSnippetDbGateway nalSnippetDbGateway;

    @Autowired
    public NalAdminConfigurationServiceImpl(NalConfigDao nalConfigDao, VirtuosoService virtuosoService,
                                            ContentStreamService contentStreamService, NalSnippetDbGateway nalSnippetDbGateway) {
        this.nalConfigDao = nalConfigDao;
        this.virtuosoService = virtuosoService;
        this.contentStreamService = contentStreamService;
        this.nalSnippetDbGateway = nalSnippetDbGateway;
    }

    @Override
    public Set<NalOntoVersion> getNalVersions() {
        final List<NalConfig> nalConfigs = this.nalConfigDao.findAll();
        return CollectionUtils.collect(nalConfigs, this.nalConfig2ActiveVersion, new TreeSet<>());
    }

    @Override
    public List<Triple<String, String, String>> deleteNAL(final String nalName) {
        //TODO place the queries in an application context to wire it and share through the different services
        //load a new nal and delete it to see the difference with an old one
        final List<Triple<String, String, String>> operationResults = new ArrayList<>();

        NalConfig nalConfig = nalConfigDao.findByName(nalName)
                .orElseThrow(() -> ExceptionBuilder.get(CellarException.class).withCode(NAL_MISSING_URI)
                        .withMessage("The NAL referenced by '{}' could not be found in the NALCONFIG table.")
                        .withMessageArgs(nalName)
                        .build());

        purgeNalContentFromOracle(operationResults, nalConfig);

        //drop from virtuoso
        final String uri = nalConfig.getUri();
        try {
            virtuosoService.drop(Collections.singletonList(uri), true, true);
            operationResults.add(new Triple<>("virtuoso", "success", ""));
        } catch (final VirtuosoOperationException e) {
            operationResults.add(new Triple<>("virtuosoService.drop", "failure",
                    "An error occured while dropping the named graph [" + uri + "] from Virtuoso : " + e.getMessage()));
        }

        final String nal = NalOntoUtils.NAL_PREFIX + nalName;
        try {
            contentStreamService.deleteContent(nal, ContentType.DIRECT);
            operationResults.add(new Triple<>("S3 (direct)", "success", ""));
        } catch (Exception e) {
            operationResults.add(new Triple<>("S3 (direct)", "failure", "Failed to delete [" + nal + "] from S3 : " + e.getMessage()));
        }

        try {
            contentStreamService.deleteContent(nal, ContentType.DIRECT_INFERRED);
            operationResults.add(new Triple<>("S3 (inferred)", "success", ""));
        } catch (Exception e) {
            operationResults.add(new Triple<>("S3 (inferred)", "failure", "Failed to delete [" + nal + "] from S3" + e.getMessage()));
        }
        return operationResults;

    }

    private void purgeNalContentFromOracle(final List<Triple<String, String, String>> operationResults,
                                           NalConfig nalConfig) {
        // delete snippets
        this.nalSnippetDbGateway.deleteRelatedSnippetOfRelatedResourceUri(nalConfig.getUri());
        operationResults.add(new Triple<>("Oracle (Related Snippet of related resource uri)", "success", ""));

        this.nalSnippetDbGateway.deleteNalSnippetOfConceptScheme(nalConfig.getUri());
        operationResults.add(new Triple<>("Oracle (Nal Snippet of concept scheme)", "success", ""));

        //delete config
        try {
            nalConfigDao.deleteObject(nalConfig);
            operationResults.add(new Triple<>("Oracle (Nal config)", "success", ""));
        } catch (final Exception e) {
            operationResults.add(new Triple<>("Oracle (Nal config)", "failure",
                    "An error occurred while deleting the NalConfig [" + nalConfig + "] : " + e.getMessage()));
        }
    }

}
