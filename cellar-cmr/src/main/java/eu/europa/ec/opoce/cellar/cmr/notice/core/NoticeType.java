package eu.europa.ec.opoce.cellar.cmr.notice.core;

/**
 * @author ARHS Developments
 */
public enum NoticeType {
    indexing {

        @Override
        public <T> T accept(NoticeTypeVisitor<T> visitor) {
            return visitor.visitIndexing();
        }
    },
    branch {

        @Override
        public <T> T accept(NoticeTypeVisitor<T> visitor) {
            return visitor.visitBranch();
        }
    },
    tree {

        @Override
        public <T> T accept(NoticeTypeVisitor<T> visitor) {
            return visitor.visitTree();
        }
    },
    identifier {

        @Override
        public <T> T accept(NoticeTypeVisitor<T> visitor) {
            return visitor.visitIdentifier();
        }
    },
    object {

        @Override
        public <T> T accept(NoticeTypeVisitor<T> visitor) {
            return visitor.visitObject();
        }
    },
    embedded {

        @Override
        public <T> T accept(NoticeTypeVisitor<T> visitor) {
            return visitor.visitEmbeddedNotice();
        }
    };

    public abstract <T> T accept(NoticeTypeVisitor<T> visitor);
}
