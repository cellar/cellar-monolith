package eu.europa.ec.opoce.cellar.cmr.metsbeans;

import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;

import java.util.Map;

public class TopLevelEvent extends HierarchyElement<MetsElement, MetsElement> {

    private static final long serialVersionUID = 7683462308298008916L;

    public TopLevelEvent() {
        super(DigitalObjectType.TOPLEVELEVENT);
    }

    public TopLevelEvent(Map<ContentType, String> versions) {
        super(DigitalObjectType.TOPLEVELEVENT, versions);
    }

}
