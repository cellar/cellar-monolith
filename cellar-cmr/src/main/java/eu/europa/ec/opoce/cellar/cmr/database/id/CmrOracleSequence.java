package eu.europa.ec.opoce.cellar.cmr.database.id;

import eu.europa.ec.opoce.cellar.cmr.database.QueryService;
import eu.europa.ec.opoce.cellar.cmr.database.dao.id.DatabaseIdService;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
/**
 * <p>CmrOracleSequence class.</p>
 */
public class CmrOracleSequence implements DatabaseIdService {

    @Autowired(required = true)
    @Qualifier(value = "cmrQueryService")
    private QueryService queryService;

    @SuppressWarnings("serial")
    private final Map<String, String> sequences = new HashMap<String, String>() {

        {
            put("CMR_CELLAR_RESOURCE_MD", "CMR_CELLAR_RESOURCE_MD_SEQ");
            put("CMR_INDEXATION_CALC_EMBEDDED_NOTICE", "CMR_INDEXATION_CALC_EMBEDDED_NOTICE_SEQ");
            put("CMR_INDEX_REQUEST", "CMR_INDEX_REQUEST_SEQ");
            put("CMR_IDOL_CACHE", "CRM_IDOL_CACHE_SEQ");
            put("CMR_IDOL_INDEX_REQUEST", "CMR_IDOL_INDEX_REQUEST_SEQ");
            put("CMR_INVERSE_RELATIONS", "CMR_INVERSE_RELATIONS_SEQ");
            put("CMR_INVERSE_DISS", "CMR_INVERSE_DISS_SEQ");
            put("NALCONFIG", "CONFIG_TABLE_SEQ");
            put("ONTOCONFIG", "CONFIG_TABLE_SEQ");
            put("MODEL_LOAD_REQUEST", "MODEL_LOAD_REQ_SEQ");
            put("VIRTUOSO_BACKUP", "VIRTUOSO_BACKUP_SEQ");
        }
    };

    /**
     * {@inheritDoc}
     */
    @Override
    public long getId(String tableName) {
        String sequence = sequences.get(tableName);
        if (StringUtils.isBlank(sequence)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("No sequence defined for table '{}'").withMessageArgs(tableName)
                    .build();
        }

        return queryService.queryForLong("select " + sequence + ".nextval from dual");
    }

    /**
     * <p>Setter for the field <code>queryService</code>.</p>
     *
     * @param queryService a {@link eu.europa.ec.opoce.cellar.cmr.database.QueryService} object.
     */
    public void setQueryService(QueryService queryService) {
        this.queryService = queryService;
    }
}
