/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.resolver.impl
 *             FILE : DisseminationResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 20, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.retrieve;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.service.IMementoService;
import eu.europa.ec.opoce.cellar.common.http.headers.HttpHeadersBuilder;
import eu.europa.ec.opoce.cellar.common.http.headers.ILink;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.ResourceRetrieveAbstractResolver;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collection;
import java.util.Date;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 20, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 * @param <T> the generic type
 */
public abstract class RetrieveResolver extends ResourceRetrieveAbstractResolver {

    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    @Autowired
    private IMementoService mementoService;

    @Autowired
    @Qualifier("pidManagerService")
    private IdentifierService identifierService;

    protected static final Logger LOG = LogManager.getLogger(RetrieveResolver.class);

    /** The identifier. */
    protected final String identifier;

    /** The e tag. */
    protected final String eTag;

    /** The last modified. */
    protected final String lastModified;

    /** The provide response body. */
    protected final boolean provideResponseBody;

    /** Accept-DateTime header passed in the original request */
    protected final Date acceptDateTime;

    /**
     * Instantiates a new retrieve resolver.
     *
     * @param identifier the identifier
     * @param eTag the e tag
     * @param lastModified the last modified
     * @param provideResponseBody the provide response body
     */
    protected RetrieveResolver(final String identifier, final String eTag, final String lastModified, final boolean provideResponseBody) {
        this(identifier, eTag, lastModified, provideResponseBody, null);
    }

    protected RetrieveResolver(final String identifier, final String eTag, final String lastModified, final boolean provideResponseBody,
            final Date acceptDateTime) {
        this.identifier = identifier;
        this.eTag = eTag;
        this.lastModified = lastModified;
        this.provideResponseBody = provideResponseBody;
        this.acceptDateTime = acceptDateTime;
    }

    /** {@inheritDoc} */
    @Override
    protected abstract ResponseEntity<?> doHandleDisseminationRequest();

    /**
     * Handle dissemination request.
     *
     * @return the response entity
     * @see eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.IDisseminationResolver#handleDisseminationRequest()
     */
    @Override
    public ResponseEntity<?> handleDisseminationRequest() {
        this.initDisseminationRequest();
        return handleMementoHeaders(doHandleDisseminationRequest());
    }

    /**
     * If versioning is enabled, a specific "Link" header must be added to the response in the case of Memento queries,
     * along with a "Memento-Datetime" header
     * @param response the initial response
     * @return the original response enriched with the memento headers when necessary 
     */
    private ResponseEntity<?> handleMementoHeaders(ResponseEntity<?> response) {
        final String uri = this.identifierService.getUri(this.cellarResource.getCellarId());
        if (this.cellarConfiguration.isCellarServiceDisseminationMementoEnabled() && this.mementoService.isEvolutiveWorkOrMemento(uri)) {
            final Object body = response.getBody();
            final HttpHeaders headers = response.getHeaders();
            final HttpStatus status = response.getStatusCode();
            final Collection<ILink> links = this.mementoService.getFullLinks(uri, this.acceptDateTime);
            final Date mementoDateTime = this.mementoService.getMementoDateTime(uri);
            final HttpHeadersBuilder httpHeadersBuilder = HttpHeadersBuilder.fromHeaders(headers);
            if ((null != links) && (links.size() > 0)) {
                httpHeadersBuilder.withLink(StringUtils.join(links, ", "));
            }
            if (mementoDateTime != null) {
                httpHeadersBuilder.withMementoDatetime(mementoDateTime);
            }
            response = new ResponseEntity<Object>(body, httpHeadersBuilder.getHttpHeaders(), status);
        }
        return response;
    }

    /** {@inheritDoc} */
    @Override
    protected void initDisseminationRequest() {
        this.cellarResource = this.disseminationService.resolveCellarResourceBean(ContentIdentifier.CELLAR_PREFIX, this.identifier);
    }
}
