/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.xml
 *             FILE : XmlBranchRedirectResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Mar 31, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.xml;

import eu.europa.ec.opoce.cellar.common.http.headers.IURLTokenable;
import eu.europa.ec.opoce.cellar.server.dissemination.AcceptLanguage;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.Structure;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.IDisseminationResolver;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;

import org.springframework.beans.factory.annotation.Configurable;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Mar 31, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Configurable
public class XmlBranchRedirectResolver extends XmlRedirectResolver {

    /**
     * Instantiates a new xml branch redirect resolver.
     *
     * @param cellarResource the cellar resource
     * @param decoding the decoding
     * @param acceptLanguage the accept language
     * @param provideAlternates the provide alternates
     */
    protected XmlBranchRedirectResolver(final CellarResourceBean cellarResource, final String decoding, final AcceptLanguage acceptLanguage,
            final boolean provideAlternates) {
        super(cellarResource, decoding, acceptLanguage, provideAlternates);
    }

    /**
     * Gets the.
     *
     * @param cellarResource the cellar resource
     * @param decoding the decoding
     * @param acceptLanguage the accept language
     * @param provideAlternates the provide alternates
     * @return the i dissemination resolver
     */
    public static IDisseminationResolver get(final CellarResourceBean cellarResource, final String decoding,
            final AcceptLanguage acceptLanguage, final boolean provideAlternates) {
        return new XmlBranchRedirectResolver(cellarResource, decoding, acceptLanguage, provideAlternates);
    }

    /** {@inheritDoc} */
    @Override
    protected void initDisseminationRequest() {
        super.initDisseminationRequest();
    }

    /** {@inheritDoc} */
    @Override
    protected void doHandleWorkDisseminationRequest() {

        this.throwErrorIfHasNoAcceptLanguageBean();

        this.retrieveTargetExpressions();

        setDecodingLanguage();
    }

    /** {@inheritDoc} */
    @Override
    protected void doHandleExpressionDisseminationRequest() {

        this.getLanguageBeanOfExpression(this.cellarResource);

        setDecodingLanguage();
    }

    /** {@inheritDoc} */
    @Override
    protected void doHandleEventDisseminationRequest() {
        this.throwErrorIfHasNoDecodingLanguage();
    }

    /** {@inheritDoc} */
    @Override
    protected IURLTokenable getTypeStructure() {
        return Structure.BRANCH;
    }

}
