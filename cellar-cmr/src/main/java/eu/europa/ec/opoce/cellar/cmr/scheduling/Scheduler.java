package eu.europa.ec.opoce.cellar.cmr.scheduling;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.common.spring.CloseableConcurrentTaskScheduler;
import eu.europa.ec.opoce.cellar.logging.LogContext;
import org.apache.commons.lang.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * This Scheduler makes sure the first run of a scheduled task will be postponed until the complete application is started.
 */
@Service
@Lazy(false)
public class Scheduler {

    private static final Logger LOG = LogManager.getLogger(Scheduler.class);

    private ScheduledFuture<?> disembargoFuture;
    private CloseableConcurrentTaskScheduler disembargoTaskScheduler;

    private static Date currentTimestamp;
    
    private DisembargoScheduler disembargoScheduler;

    @Autowired
    CellarResourceDao cellarResourceDao;

    @Autowired
    ICellarConfiguration cellarConfiguration;

    @PostConstruct
    public void init() {
        //Only schedule if automatic disembargo is enabled
        if(cellarConfiguration.isCellarServiceAutomaticDisembargoEnabled()) {
            disembargoTaskScheduler = CloseableConcurrentTaskScheduler.newScheduler("disembargo");
            disembargoScheduler = new DisembargoScheduler();
            currentTimestamp = DateTime.now().plusSeconds(cellarConfiguration.getCellarServiceDisembargoInitialDelay()).toDate(); //last embargo date of element

            disembargoFuture = disembargoTaskScheduler.schedule(disembargoScheduler, currentTimestamp);

            LOG.info(ICellarConfiguration.CONFIG, "Disembargo ({}) scheduler started.", currentTimestamp);
        }
    }

    @LogContext(LogContext.Context.CMR_EMBARGO)
    public void reschedule(boolean automatic){
        Date timeToCheck=automatic?currentTimestamp:DateTime.now().toDate();
        timeToCheck=DateUtils.addSeconds(timeToCheck,1);
        Date newTimestamp=cellarResourceDao.selectMinEmbargoDate(timeToCheck);
        if(newTimestamp!=null&&newTimestamp.before(DateTime.now().toDate())){
            if(disembargoFuture!=null)disembargoFuture.cancel(false);
            currentTimestamp=newTimestamp;
            disembargoScheduler.run();
            return;

        }
            if(currentTimestamp==null||!currentTimestamp.equals(newTimestamp)){
                if(newTimestamp==null){
                    LOG.info("No disembargo job is currently scheduled");
                    if(disembargoFuture!=null)disembargoFuture.cancel(false);
                    currentTimestamp=DateTime.now().toDate();
                }else {
                    LOG.info( "The next disembargo scheduler execution will happen at ({}).", newTimestamp);
                    if(disembargoFuture!=null)disembargoFuture.cancel(false);
                    disembargoFuture = disembargoTaskScheduler.schedule(disembargoScheduler, newTimestamp);
                    currentTimestamp=newTimestamp;
                }
            }

    }



    public void shutdown(boolean awaitTermination, long timeout, TimeUnit unit) throws InterruptedException {
        disembargoFuture.cancel(!awaitTermination);
        disembargoTaskScheduler.shutdown(awaitTermination, timeout, unit);
    }
}
