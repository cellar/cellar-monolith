/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.graph
 *             FILE : Graph.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 23, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.graph.impl;

import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;
import eu.europa.ec.opoce.cellar.cmr.graph.IGraph;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * <class_description> Graph to be protected by the concurrency controller.
 * <br/><br/>
 * ON : Apr 23, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class Graph implements IGraph {

    /**
     * Cellar ids history to avoid duplicates.
     */
    private Set<String> cellarIdsHistory;

    /**
     * Production ids history to avoid duplicates.
     */
    private Set<String> productionIdsHistory;

    /**
     * Base cellar ids history backup.
     */
    private Set<String> objectCellarIdsHistory;

    /**
     * Base production ids history backup.
     */
    private Set<String> objectProductionIdsHistory;

    /**
     * Base objects.
     */
    private List<Identifier> objects;

    /**
     * Relations.
     */
    private List<Identifier> relations;

    /**
     * Initialization method.
     */
    public void initialize() {
        this.objects = new LinkedList<Identifier>();
        this.relations = new LinkedList<Identifier>();

        this.cellarIdsHistory = new HashSet<String>();
        this.productionIdsHistory = new HashSet<String>();

        this.objectCellarIdsHistory = new HashSet<String>();
        this.objectProductionIdsHistory = new HashSet<String>();
    }

    /**
     * Initialization method that keep the base objects.
     */
    public void initializeFromObjects() {
        this.relations = new LinkedList<Identifier>();

        this.cellarIdsHistory = new HashSet<String>(this.objectCellarIdsHistory);
        this.productionIdsHistory = new HashSet<String>(this.objectProductionIdsHistory);
    }

    /**
     * Add an identifier to the base objects.
     * @param identifier the identifier to add
     * @return true if the identifier is correctly added. Otherwise, false.
     */
    public boolean addObject(final Identifier identifier) {
        if (identifier.cellarIdExists()) {
            if (this.cellarIdsHistory.add(identifier.getCellarId())) {
                this.objectCellarIdsHistory.add(identifier.getCellarId());
                this.objects.add(identifier);
                return true;
            }
        } else if (identifier.getPids() != null) {
            if (this.productionIdsHistory.addAll(identifier.getPids())) {
                this.objectProductionIdsHistory.addAll(identifier.getPids());
                this.objects.add(identifier);
                return true;
            }
        }

        return false; // there is no pids or cellar id
    }

    /**
     * Add an identifier to the relations.
     * @param identifier the identifier to add
     * @return true if the identifier is correctly added. Otherwise, false.
     */
    public boolean addRelation(final Identifier identifier) {
        if (identifier.cellarIdExists()) {
            if (this.cellarIdsHistory.add(identifier.getCellarId())) {
                this.relations.add(identifier);
                return true;
            }
        } else if (identifier.getPids() != null) {
            if (this.productionIdsHistory.addAll(identifier.getPids())) {
                this.relations.add(identifier);
                return true;
            }
        }

        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Identifier> getObjects() {
        return this.objects;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Identifier getRoot() {
        if (this.objects.isEmpty()) {
            return null;
        } else {
            return this.objects.get(0);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Identifier> getChildren() {
        if (this.objects.isEmpty()) {
            return this.objects;
        } else {
            return this.objects.subList(1, this.objects.size());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Identifier> getRelations() {
        return this.relations;
    }
}
