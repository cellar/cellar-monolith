package eu.europa.ec.opoce.cellar.cmr.metsbeans;

import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;

import java.util.Collections;
import java.util.Map;

/**
 * <p>Work class.</p>
 */
public class Work extends HierarchyElement<MetsElement, Expression> {

    private static final long serialVersionUID = 3468198479256494411L;

    public Work() {
        super(DigitalObjectType.WORK, Collections.emptyMap());
    }

    public Work(Map<ContentType, String> versions) {
        super(DigitalObjectType.WORK, versions);
    }

}
