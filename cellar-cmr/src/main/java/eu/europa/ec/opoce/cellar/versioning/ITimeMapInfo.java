package eu.europa.ec.opoce.cellar.versioning;

import java.util.Date;

/**
 * @author ARHS-Developments
 *
 */

public interface ITimeMapInfo {

    /**
     * Retrieves the start date of the TimeMap
     * @return the start date
     */
    public Date getStartDate();

    /**
     * Retrieves the end date of the TimeMap
     * @return the end date
     */
    public Date getEndDate();

    /**
     * Retrieves the type of date (i.e. "date" or "date-time") of the TimeMap
     * @return the type of date
     */
    public String getTypeofDate();
}
