/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.nal.service
 *             FILE : JenaGatewayNalApi.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 03 02, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-03-02 07:26:21 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.nal.service;

import org.apache.jena.rdf.model.Model;
import eu.europa.ec.opoce.cellar.nal.domain.Concept;
import eu.europa.ec.opoce.cellar.nal.domain.ConceptScheme;
import eu.europa.ec.opoce.cellar.nal.domain.Domain;
import eu.europa.ec.opoce.cellar.nal.domain.Language;

import java.util.Date;

/**
 * @author ARHS Developments
 */
public interface JenaGatewayNalApi {
    Language[] getSupportedLanguages(Model model, String conceptScheme);

    Domain[] getDomainsOneQuery(Model model);

    Domain[] getDomains(Model model);

    Domain getDomain(Model model, String domain);

    ConceptScheme[] getConceptSchemes(Model model, Date lastModified);

    ConceptScheme[] getConceptSchemesOneQuery(Model model, Date lastModified);

    ConceptScheme getConceptScheme(Model model, String conceptScheme);

    Concept[] getTopConcepts(Model model, String conceptScheme, String language);

    Concept[] getRelatedConcepts(Model model, String concept, String relation, String language);

    Concept[] getRelatedConceptsOneQuery(Model model, String concept, String relation, String language);

    Concept getConcept(Model model, String concept, String language);
}
