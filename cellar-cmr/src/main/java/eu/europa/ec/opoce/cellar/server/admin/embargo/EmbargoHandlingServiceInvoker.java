/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.server.admin.embargo
 *        FILE : EmbargoHandlingServiceInvoker.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 08-01-2013
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.admin.embargo;

import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.HierarchyElement;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement;
import eu.europa.ec.opoce.cellar.cmr.service.EmbargoService;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.helper.DateFormats;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * <class_description> This class handles all the calls from the
 * EmbargoController.<br/>
 * It works as a service invoker class for the EmbargoHandlingService. <br/>
 * <br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc. <br/>
 * <br/>
 * ON : 08-01-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component
public class EmbargoHandlingServiceInvoker {

    /**
     * Constant <code>log</code>
     */
    private static final Logger LOG = LogManager.getLogger(EmbargoHandlingServiceInvoker.class);

    @Autowired
    private EmbargoService embargoHandlingService;

    @Autowired
    @Qualifier("pidManagerService")
    private IdentifierService identifierService;

    /**
     * <p>
     * getEmbargoWorks.
     * </p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<HierarchyElement<MetsElement, MetsElement>> getEmbargoHierarchyElements() {
        return this.embargoHandlingService.getEmbargoHierarchyElements();
    }

    /**
     * This method accepts a set of uri's and removes it from embargo
     *
     * @param uris:
     *            set of uri's that need to be removed from embargo
     */
    @Async
    public void removeFromEmbargo(final Set<String> uris) {
        if (uris == null || uris.isEmpty()) {
            return;
        }

        try {
            for (final String uri : uris) {
                this.embargoHandlingService.changeEmbargo(uri, null);
            }
        } catch (final CellarException e) {
            throw e;
        } catch (final Exception e) {
            throw ExceptionBuilder.get(CellarException.class).withCause(e).build();
        }
    }

    /**
     * This method changes the date of an item in embargo
     *
     * @param uri:
     *            the uri of the item
     * @param date:
     *            the new embargo date of the item
     */
    public void changeEmbargo(final String uri, final String date) {
        if (StringUtils.isBlank(uri) || StringUtils.isBlank(date)) {
            return;
        }

        try {
            final Date embargoDate = DateFormats.parse(date, DateFormats.Format.READABLEDATETIME);
            if (embargoDate != null) {
                this.embargoHandlingService.changeEmbargo(uri, embargoDate);
            }
        } catch (final CellarException e) {
            throw e;
        } catch (final Exception e) {
            throw ExceptionBuilder.get(CellarException.class).withCause(e).build();
        }
    }

    /**
     * This method adds an item to embargo with an embargo date
     *
     * @param uri:
     *            the uri of the item to add to embargo
     * @param date:
     *            the date of the item to add to embargo
     */
    @Async
    public void addToEmbargo(final String uri, final String date) {
        if (StringUtils.isBlank(uri) || StringUtils.isBlank(date)) {
            return;
        }

        Date embargoDate;

        try {
            embargoDate = DateFormats.parse(date, DateFormats.Format.READABLEDATETIME);
        } catch (final CellarException e) {
            throw e;
        } catch (final Exception e) {
            throw ExceptionBuilder.get(CellarException.class).withCause(e).build();
        }

        this.addToEmbargo(uri, embargoDate);
    }

    /**
     * Adds an item to embargo with an embargo date.
     * 
     * @param uri
     *            the uri of the item to add to embargo
     * @param embargoDate
     *            the date of the item to add to embargo
     */
    public void addToEmbargo(final String uri, final Date embargoDate) {
        if(embargoDate == null) {
            return;
        }

        try {
            this.embargoHandlingService.changeEmbargo(uri, embargoDate);
        } catch(final CellarException e) {
            throw e;
        } catch (final Exception e) {
            throw ExceptionBuilder.get(CellarException.class).withCause(e).build();
        }
    }

    /**
     * This method checks if the given production system id exists
     *
     * @param psid:
     *            the production system id
     * @return true or false, whether the production system id is found
     */
    public boolean checkPsid(final String psid) {
        if (StringUtils.isNotBlank(psid)) {
            try {
                this.identifierService.getCellarPrefixed(psid);
                return true;
            } catch (final Exception e) {
                LOG.warn("psid '{}' not found", psid);
            }
        } else {
            LOG.warn("psid is not set");
        }
        return false;
    }

    /**
     * This method returns a work from a production system id
     *
     * @param psid:
     *            the production system id
     * @return the work asked for or null
     */
    public HierarchyElement<MetsElement, MetsElement> search(final String psid) {
        if (StringUtils.isNotBlank(psid)) {
            return this.embargoHandlingService.getPublicHierarchyElement(psid);
        }
        return null;
    }

}
