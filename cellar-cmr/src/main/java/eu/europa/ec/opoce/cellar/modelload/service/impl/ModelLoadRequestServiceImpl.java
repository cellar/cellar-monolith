/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.modelload.service.impl
 *             FILE : ModelLoadRequestSchedulerImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Mar 18, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.modelload.service.impl;

import eu.europa.ec.opoce.cellar.ExecutionStatus;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.database.dao.ModelLoadRequestDao;
import eu.europa.ec.opoce.cellar.modelload.domain.ModelLoadRequest;
import eu.europa.ec.opoce.cellar.modelload.service.ModelLoadRequestService;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Mar 18, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service("modelLoadRequestService")
public class ModelLoadRequestServiceImpl implements ModelLoadRequestService {
    private static final Logger LOGGER = LogManager.getLogger(ModelLoadRequestServiceImpl.class);

    private final ModelLoadRequestDao modelLoadRequestDao;
    private final ContentStreamService contentStreamService;

    @Autowired
    public ModelLoadRequestServiceImpl(ModelLoadRequestDao modelLoadRequestDao, ContentStreamService contentStreamService) {
        this.modelLoadRequestDao = modelLoadRequestDao;
        this.contentStreamService = contentStreamService;
    }

    public Collection<ModelLoadRequest> findAll() {
        return this.modelLoadRequestDao.findAll();
    }

    public Collection<ModelLoadRequest> find(final String modelURI, final String executionStatus) {
        return this.modelLoadRequestDao.findByCriteria(modelURI, executionStatus);
    }

    public Optional<ModelLoadRequest> findById(final String id) {
        return Optional.ofNullable(this.modelLoadRequestDao.getObject(Long.valueOf(id)));
    }

    @Override
    @Transactional
    public void restart(final String id, final Date activationDate) {
        ModelLoadRequest modelLoadRequest = this.modelLoadRequestDao.getObject(Long.valueOf(id));
        modelLoadRequest.setExecutionStatus(ExecutionStatus.Pending);

        // updating the activation date also updates the pid in the object
        modelLoadRequest.setActivationDate(activationDate);

        LOGGER.info("Restarting model load request with updated activation date '{}': {}", activationDate, modelLoadRequest);
        modelLoadRequestDao.saveOrUpdateObject(modelLoadRequest);
    }

    @Transactional
    public void delete(final String id) {
        ModelLoadRequest modelLoadRequest = this.modelLoadRequestDao.getObject(Long.valueOf(id));
        if (modelLoadRequest != null) {
            contentStreamService.deleteContent(modelLoadRequest.getExternalPid(), ContentType.DIRECT);
            modelLoadRequestDao.deleteObject(modelLoadRequest);
        }
    }

    @Transactional
    public void update(final String id, final String modelURI, final String pid, final String executionStatus,
                       final Date activationDate) {
        ModelLoadRequest modelLoadRequest = this.modelLoadRequestDao.getObject(Long.valueOf(id));
        modelLoadRequest.setExecutionStatus(ExecutionStatus.valueOf(executionStatus));
        modelLoadRequest.setActivationDate(activationDate);

        modelLoadRequestDao.saveOrUpdateObject(modelLoadRequest);
    }

}
