/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.nal.service
 *             FILE : NalInferenceService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 01 03, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-01-03 06:09:52 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.nal.service;

import org.apache.jena.rdf.model.Model;

/**
 * @author ARHS Developments
 */
public interface NalInferenceService {
    void infer(Model model, String conceptSchemeUri);
}
