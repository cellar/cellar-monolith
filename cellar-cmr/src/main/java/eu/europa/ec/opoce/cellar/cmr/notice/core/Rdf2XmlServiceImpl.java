/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice.core
 *             FILE : Rdf2XmlUtils.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 20 Apr 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.notice.core;

import eu.europa.ec.opoce.cellar.cmr.utils.XmlNoticeUtils;
import eu.europa.ec.opoce.cellar.common.Namespace;
import eu.europa.ec.opoce.cellar.configuration.BaseUriConfiguration;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.domain.RequiredLanguageBean;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.nal.service.LanguageService;
import eu.europa.ec.opoce.cellar.semantic.helper.DateFormats;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaQueries;
import eu.europa.ec.opoce.cellar.semantic.ontology.data.FallbackOntologyData;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import eu.europa.ec.opoce.cellar.semantic.xml.XmlBuilder;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.rdf.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import static eu.europa.ec.opoce.cellar.cmr.utils.XmlNoticeUtils.*;

/**
 * @author ARHS Developments
 */
@Service
public class Rdf2XmlServiceImpl implements Rdf2XmlService {

    private static final String EUROVOC_URI = "http://eurovoc.europa.eu/";
    private static final String AUTHORITY = "http://publications.europa.eu/resource/authority/";

    private final BaseUriConfiguration baseUriConfiguration;
    private final LanguageService languageService;
    private final Rdf2XmlLabelService rdf2XmlLabelService;
    private final Rdf2XmlAnnotationService rdf2XmlAnnotationService;

    @Autowired
    public Rdf2XmlServiceImpl(final BaseUriConfiguration baseUriConfiguration,
                              final LanguageService languageService,
                              final Rdf2XmlLabelService rdf2XmlLabelService,
                              final Rdf2XmlAnnotationService rdf2XmlAnnotationService) {
        this.baseUriConfiguration = baseUriConfiguration;
        this.languageService = languageService;
        this.rdf2XmlLabelService = rdf2XmlLabelService;
        this.rdf2XmlAnnotationService = rdf2XmlAnnotationService;
    }

    @Override
    public void writeUriAndSameAs(final XmlBuilder parentBuilder, final Resource resource,
                                  final Set<Resource> sameAsResources, final Model model) {
        final Set<Resource> resources = new HashSet<>(sameAsResources);
        resources.add(resource);

        // write Cellar URIs
        final String cellarResourceUri = getCellarResourceUri("cellar");
        for (final Resource r : resources) {
            if (r.getURI().startsWith(cellarResourceUri)) {
                writeUri(parentBuilder, r.getURI());
            }
        }

        // write sameAs
        for (final Resource r : resources) {
            if (!r.getURI().startsWith(cellarResourceUri)) {
                final XmlBuilder sameAsBuilder = parentBuilder.child("SAMEAS");
                writeUri(sameAsBuilder, r.getURI());
                rdf2XmlAnnotationService.addSameAsAnnotations(sameAsBuilder, r, model);
            }
        }

    }

    @Override
    public void writeUri(final XmlBuilder parentBuilder, final String uri) {
        final String cellarResourceUri = getCellarResourceUri("");
        final XmlBuilder uriBuilder = parentBuilder.child("URI");
        uriBuilder.child("VALUE").text(uri);

        if (uri.startsWith(EUROVOC_URI)) {
            writeTypeAndIdentifier(uriBuilder, "EUROVOC", uri.substring(EUROVOC_URI.length()));
        } else if (uri.startsWith(AUTHORITY)) {
            writeTypeAndIdentifier(uriBuilder, uri.substring(AUTHORITY.length()));
        } else if (uri.startsWith(cellarResourceUri)) {
            writeTypeAndIdentifier(uriBuilder, uri.substring(cellarResourceUri.length()));
        }
    }

    private String getCellarResourceUri(final String suffix) {
        return baseUriConfiguration.getBaseUri()
                + (baseUriConfiguration.getBaseUri().endsWith("/") ? "" : "/")
                + "resource/"
                + suffix;
    }


    private void writeTypeAndIdentifier(final XmlBuilder builder, final String typeAndIdentifier) {
        // prefix/bla/bla/bla
        final int indexOfSlash = typeAndIdentifier.indexOf('/');
        if (indexOfSlash == -1) {
            ExceptionBuilder.get(CellarException.class).build();
        }

        // prefix
        final String type = typeAndIdentifier.substring(0, indexOfSlash);

        // /bla:bla:bla
        final String identifier = typeAndIdentifier.substring(indexOfSlash + 1).replace("/", ":");

        writeTypeAndIdentifier(builder, type, identifier);
    }

    private void writeTypeAndIdentifier(final XmlBuilder builder, final String type, final String identifier) {
        builder.child("TYPE").text(type);
        builder.child("IDENTIFIER").text(decodeUri(identifier));
    }

    @Override
    public void writeConcept(final XmlBuilder conceptBuilder, final Resource conceptResource,
                             final Map<LanguageBean, List<LanguageBean>> cache,
                             final Set<LanguageBean> languages, NoticeType noticeType) {
        conceptBuilder.attribute("type", "concept");
        writeUri(conceptBuilder, conceptResource.getURI());

        for (final String opCode : JenaQueries.getLiteralValues(conceptResource, CellarProperty.at_codeP, false)) {
            conceptBuilder.child("OP-CODE").text(opCode);
        }

        for (final String dcIdentifierValue : JenaQueries.getLiteralValues(conceptResource, CellarProperty.dc_identifierP, false)) {
            conceptBuilder.child("IDENTIFIER").text(dcIdentifierValue);
        }

        if (noticeType == NoticeType.embedded) {
            for (final RequiredLanguageBean requiredLanguageBean : languageService.getRequiredLanguages()) {
                final LanguageBean usedLanguage = languageService.getByUri(requiredLanguageBean.getUri());
                rdf2XmlLabelService.writePrefLabel(conceptBuilder, conceptResource, usedLanguage, cache);
                rdf2XmlLabelService.writeAltLabels(conceptBuilder, conceptResource, usedLanguage);
            }
        } else {
            for (final LanguageBean languageBean : languages) {
                rdf2XmlLabelService.writePrefLabel(conceptBuilder, conceptResource, languageBean, cache);
                rdf2XmlLabelService.writeAltLabels(conceptBuilder, conceptResource, languageBean);
            }
        }
    }

    @Override
    public void writeConceptFacetWithPossibleAnnotation(final XmlBuilder xmlBuilder,
                                                        final Property property,
                                                        final Resource resource,
                                                        final Map<LanguageBean, List<LanguageBean>> cache,
                                                        final Set<LanguageBean> languages,
                                                        final NoticeType noticeType,
                                                        final Model model,
                                                        final Set<Resource> allSubjectResources,
                                                        final FallbackOntologyData fallbackOntologyData) {
        xmlBuilder.attribute("type", "concept_facet");
        final String tagName = XmlNoticeUtils.tagize(property);
        final XmlBuilder childXmlBuilder = xmlBuilder.child(tagName + "_CONCEPT");
        writeConcept(childXmlBuilder, resource, cache, languages, noticeType);
        writeAnnotation(childXmlBuilder, property, resource, model, allSubjectResources);

        for (final Resource facetResource : JenaQueries.getResources(resource, CellarProperty.cmr_facet_TP, false)) {
            final String facetAcronym = fallbackOntologyData.getFacetAcronym(CellarProperty.cmr_facet_T);
            final XmlBuilder childXmlBuilderT = xmlBuilder.child(tagName + facetAcronym);
            writeConcept(childXmlBuilderT, facetResource, cache, languages, noticeType);
            writeAnnotation(childXmlBuilderT, property, facetResource, model, allSubjectResources);
        }
        for (final Resource facetResource : JenaQueries.getResources(resource, CellarProperty.cmr_facet_DP, false)) {
            final String facetAcronym = fallbackOntologyData.getFacetAcronym(CellarProperty.cmr_facet_D);
            final XmlBuilder childXmlBuilderD = xmlBuilder.child(tagName + facetAcronym);
            writeConcept(childXmlBuilderD, facetResource, cache, languages, noticeType);
            writeAnnotation(childXmlBuilderD, property, facetResource, model, allSubjectResources);
        }
        for (final Resource facetResource : JenaQueries.getResources(resource, CellarProperty.cmr_facet_MP, false)) {
            final String facetAcronym = fallbackOntologyData.getFacetAcronym(CellarProperty.cmr_facet_M);
            final XmlBuilder childXmlBuilderM = xmlBuilder.child(tagName + facetAcronym);
            writeConcept(childXmlBuilderM, facetResource, cache, languages, noticeType);
            writeAnnotation(childXmlBuilderM, property, facetResource, model, allSubjectResources);
        }
    }

    @Override
    public void writeAgent_UnitAdministrative(final XmlBuilder auaBuilder, final Resource resource, NoticeType noticeType,
                                              Set<LanguageBean> languages) {
        final List<String> dcIdentifierValues = JenaQueries.getLiteralValues(resource, CellarProperty.dc_identifierP, false);

        for (final String dcIdentifierValue : dcIdentifierValues) {
            auaBuilder.child("IDENTIFIER").text(dcIdentifierValue);
        }

        if (noticeType == NoticeType.embedded) {
            for (final RequiredLanguageBean requiredLanguageBean : languageService.getRequiredLanguages()) {
                final LanguageBean usedLanguage = languageService.getByUri(requiredLanguageBean.getUri());
                rdf2XmlLabelService.writePrefLabelIfExists(auaBuilder, resource, usedLanguage);
                rdf2XmlLabelService.writeAltLabels(auaBuilder, resource, usedLanguage);
            }
        } else {
            for (final LanguageBean languageBean : languages) {
                rdf2XmlLabelService.writePrefLabelIfExists(auaBuilder, resource, languageBean);
                rdf2XmlLabelService.writeAltLabels(auaBuilder, resource, languageBean);
            }
        }
    }

    @Override
    public void writeItem(final XmlBuilder itemBuilder, Resource resource, final NodeHandler nodeHandler, final Model model) {
        resource = getResourceFromModel(resource, model);
        final Set<Resource> sameAsResources = getSameAsResources(resource);
        writeUriAndSameAs(itemBuilder, resource, sameAsResources, model);
        final XmlBuilder techMdBuilder = new XmlBuilder(itemBuilder, "TECHMD");
        final Set<Statement> allStatements = getAllStatements(resource, sameAsResources);
        for (final Statement statement : allStatements) {
            final Property predicate = statement.getPredicate();
            final String currentUri = predicate.getURI();
            if (currentUri.startsWith(Namespace.cdm)) {
                nodeHandler.setCurrent(itemBuilder, predicate);
            } else if (currentUri.startsWith(Namespace.tdm)) {
                nodeHandler.setCurrent(techMdBuilder, predicate);
            } else {
                continue;
            }
            statement.getObject().visitWith(nodeHandler);
        }
    }

    @Override
    public void writeDate(final XmlBuilder dateBuilder, final String dateValue) {
        switch (dateValue) {
            case "unknown-past":
                writeDate(dateBuilder, "0001-01-01", "0001", "01", "01");
                break;
            case "unknown-future":
                writeDate(dateBuilder, "9999-12-31", "9999", "12", "31");
                break;
            default:
                final Date date = DateFormats.parseDate(dateValue);
                if (null == date) {
                    throw ExceptionBuilder.get(CellarException.class)
                            .withMessage("Cannot parse value to a Date: {}")
                            .withMessageArgs(dateValue)
                            .build();
                }
                final Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                writeDate(dateBuilder, dateValue, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1,
                        calendar.get(Calendar.DAY_OF_MONTH));
                break;
        }
    }

    private void writeDate(final XmlBuilder dateBuilder, final String fullDate, final Object year, final Object month,
                           final Object day) {
        dateBuilder.attribute("type", "date");
        dateBuilder.child("VALUE").text(fullDate);
        dateBuilder.child("YEAR").text(StringUtils.leftPad(String.valueOf(year), 4, '0'));
        dateBuilder.child("MONTH").text(StringUtils.leftPad(String.valueOf(month), 2, '0'));
        dateBuilder.child("DAY").text(StringUtils.leftPad(String.valueOf(day), 2, '0'));
    }

    @Override
    public void writeAnnotation(final XmlBuilder parentBuilder, final Property property,
                                final RDFNode object, final Model model, final Set<Resource> allSubjectResources) {
        rdf2XmlAnnotationService.addAnnotations(parentBuilder, property, object, model, allSubjectResources);
    }
}
