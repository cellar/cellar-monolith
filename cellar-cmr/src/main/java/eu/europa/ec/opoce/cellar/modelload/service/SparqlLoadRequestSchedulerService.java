package eu.europa.ec.opoce.cellar.modelload.service;

import java.util.Date;

/**
 * @author ARHS Developments
 */
public interface SparqlLoadRequestSchedulerService {

    /**
     *
     */
    void scheduleSparqlLoadRequests();

    /**
     *
     * @param id
     * @param newActivationDate
     */
    void restart(Long id, Date newActivationDate);
}
