package eu.europa.ec.opoce.cellar.server.dissemination.converter;

import com.github.rutledgepaulv.prune.Tree;
import eu.europa.ec.opoce.cellar.common.util.Pair;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.converter.HttpMessageNotWritableException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * <p>ContentStreamURIsHttpMessageConverter class.</p>
 */
public class ContentStreamListHttpMessageConverter extends CellarAbstractHttpMessageConverter<Tree<Pair<String, String>>> {
    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean supports(final Class<?> clazz) {
        return Tree.class.isAssignableFrom(clazz);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void writeInternal(final Tree<Pair<String, String>> contentStreams, final HttpOutputMessage outputMessage)
            throws IOException, HttpMessageNotWritableException {
        if (contentStreams == null) {
            return;
        }

        final StringBuilder sb = new StringBuilder();
        sb.append("<html><head><title>List of content streams</title></head><body><ul>List of content streams with URIs:");
        contentStreams.asNode().getChildren().forEach(child -> renderEntries(child, sb));
        sb.append("</ul></body></html>");
        IOUtils.write(sb.toString().getBytes(StandardCharsets.UTF_8), outputMessage.getBody());
    }

    private static void renderEntries(final Tree.Node<Pair<String, String>> currNode, final StringBuilder sb) {
        final String currStreamUri = currNode.getData().getOne();
        final String currStreamLabel = currNode.getData().getTwo();

        sb.append("<li>");
        sb.append("<a href=\"").append(currStreamUri).append("\">").append("<span class=\"url\">")
                .append(currStreamLabel).append(" --> ").append(currStreamUri).append("</span>").append("</a>");
        if (currNode.getChildren().size() > 0) {
            sb.append("<ul>");
            currNode.getChildren().forEach(child -> renderEntries(child, sb));
            sb.append("</ul>");
        }
        sb.append("</li>");
    }
}
