package eu.europa.ec.opoce.cellar.virtuosobackup;

/**
 * Enum containing possible Virtuoso backup types, which can be:
 * - DIGITAL_OBJECT: case where the Model to backup is related to a digital object;
 * - ONTOLOGY: case where the Model to backup is related to an ontology ;
 * - NAL: case where the Model to backup is related to a Named Authority List.
 *
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Mar 24, 2015
 *
 * @author ARHS Developments
 * @version $Revision: 10409 $
 */
public enum BackupType {

    DIGITAL_OBJECT, ONTOLOGY, NAL, SPARQL_LOAD
}
