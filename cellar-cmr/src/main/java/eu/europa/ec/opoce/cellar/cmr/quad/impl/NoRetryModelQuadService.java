/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.quad.impl
 *             FILE : OracleLockedModelQuadService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Sep 6, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.quad.impl;

import eu.europa.ec.opoce.cellar.common.retry.IRetryAwareConnection;
import eu.europa.ec.opoce.cellar.common.retry.IRetryStrategy;
import eu.europa.ec.opoce.cellar.common.retry.impl.NoRetryStrategy;
import eu.europa.ec.opoce.cellar.jena.oracle.impl.RDFStoreRetryAwareConnection;
import oracle.spatial.rdf.client.jena.Oracle;

import java.sql.Connection;

/**
 * <class_description> An implementation of {@link eu.europa.ec.opoce.cellar.cmr.quad.ModelQuadService}
 * which attempts to access the CMR tables just once - that is, making no retry.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Sep 6, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class NoRetryModelQuadService extends BaseModelQuadService {

    /**
     * @see eu.europa.ec.opoce.cellar.cmr.quad.impl.BaseModelQuadService#newRetryStrategy(oracle.spatial.rdf.client.jena.Oracle)
     */
    @Override
    protected IRetryStrategy<Connection> newRetryStrategy(final Oracle oracle) {
        final IRetryAwareConnection<Connection> retryAwareConnection = new RDFStoreRetryAwareConnection(oracle);

        final IRetryStrategy<Connection> retryStrategy = new NoRetryStrategy<Connection>().recipientName("Oracle")
                .retryAwareConnection(retryAwareConnection);
        return retryStrategy;
    }

}
