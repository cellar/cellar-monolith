/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.cmr.ingest
 *        FILE : MetadataLoaderService.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 30-05-2013
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.ingest.impl;

import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.LanguageIsoCode;
import eu.europa.ec.opoce.cellar.ccr.service.LanguagesNalSkosLoaderService;
import eu.europa.ec.opoce.cellar.cl.configuration.PrefixConfigurationService;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.ingest.IMetadataLoaderService;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.HierarchyElement;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement;
import eu.europa.ec.opoce.cellar.cmr.model.ModelUtils;
import eu.europa.ec.opoce.cellar.cmr.model.SameAsModelUtils;
import eu.europa.ec.opoce.cellar.cmr.service.EmbargoService;
import eu.europa.ec.opoce.cellar.cmr.transformers.LanguageBean2IsoCode;
import eu.europa.ec.opoce.cellar.cmr.transformers.RdfNode2LanguageBean;
import eu.europa.ec.opoce.cellar.cmr.utils.EmbargoUtils;
import eu.europa.ec.opoce.cellar.common.util.DigitalObjectUtils;
import eu.europa.ec.opoce.cellar.common.util.Triple;
import eu.europa.ec.opoce.cellar.domain.content.mets.CellarIdentifiedObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentStream;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.domain.content.mets.Metadata;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsPackage;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.monitoring.Watch;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import eu.europa.ec.opoce.cellar.semantic.exception.CellarSemanticException;
import eu.europa.ec.opoce.cellar.semantic.helper.DateFormats;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaQueries;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarAnnotationProperty;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarType;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static eu.europa.ec.opoce.cellar.semantic.jena.transformers.Statement2Subject.statement2Subject;
import static org.apache.commons.collections15.CollectionUtils.collect;

/**
 * <class_description> Abstract class for conveniently load models.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 30-05-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class MetadataLoaderServiceImpl implements IMetadataLoaderService {

    private static final Logger LOG = LogManager.getLogger(MetadataLoaderServiceImpl.class);

    private final LanguagesNalSkosLoaderService languagesSkosService;
    private final PrefixConfigurationService prefixConfigurationService;
    private final ContentStreamService contentStreamService;
    private final IdentifierService identifierService;
    private final EmbargoService embargoHandlingService;
    private final CellarResourceDao cellarResourceDao;

    private String[] cellarPrefixUris;

    @Autowired
    public MetadataLoaderServiceImpl(LanguagesNalSkosLoaderService languagesSkosService, PrefixConfigurationService prefixConfigurationService,
                                     ContentStreamService contentStreamService, @Qualifier("pidManagerService") IdentifierService identifierService,
                                     EmbargoService embargoHandlingService, CellarResourceDao cellarResourceDao) {
        this.languagesSkosService = languagesSkosService;
        this.prefixConfigurationService = prefixConfigurationService;
        this.contentStreamService = contentStreamService;
        this.identifierService = identifierService;
        this.embargoHandlingService = embargoHandlingService;
        this.cellarResourceDao = cellarResourceDao;
    }

    @PostConstruct
    private void init() {
        this.cellarPrefixUris = this.prefixConfigurationService.getPrefixConfiguration().getPrefixUris().values().toArray(new String[0]);
    }

    @Watch(value = "Metadata Loader", arguments = {
            @Watch.WatchArgument(name = "StructMap", expression = "structMap.id")
    })
    @Override
    public Model loadMetadata(final MetsPackage metsPackage, final StructMap structMap, final boolean useOldDataIfEmpty) {
        final boolean isContentUpdate = structMap.getContentOperationType() != null;
        final Model metadataModel = ModelFactory.createDefaultModel();
        final Set<String> metadataRefs = new HashSet<>();
        final Map<String, Set<String>> urisPerDmd = new HashMap<>();
        final DigitalObject structMapDo = structMap.getDigitalObject();

        parseMetadataLoadOldData(structMapDo, useOldDataIfEmpty, metadataModel, isContentUpdate, metadataRefs, urisPerDmd);
        loadNewMetadata(metsPackage, metadataModel, urisPerDmd);
        addEmbargoDate(metadataModel, structMapDo, useOldDataIfEmpty);
        addReadOnly(structMapDo, metadataModel);

        return metadataModel;
    }

    /**
     * Adds the embargo date.
     *
     * @param metadataModel     the metadata model
     * @param work              the work
     * @param useOldDataIfEmpty the use old data if empty
     */
    private void addEmbargoDate(final Model metadataModel, final DigitalObject work, final boolean useOldDataIfEmpty) {
        final List<DigitalObject> digitalObjects = work.getAllChilds(true);

        // set embargo date in structmap
        digitalObjects.stream()
                .filter(digitalObject -> DigitalObjectType.takeEmbargoDate(digitalObject.getType()))
                .forEach(digitalObject -> setChildEmbargoDate(digitalObject, metadataModel));

        // validate embargo date
        if (!useOldDataIfEmpty) {
            embargoHandlingService.validateEmbargoDate(digitalObjects);
        }

        // spread embargo dates WORK -> ITEM (ordered list)
        digitalObjects.stream()
                .filter(digitalObject -> digitalObject.getEmbargoDate() != null)
                .forEach(this::propagateEmbargoDate);
    }

    private void setChildEmbargoDate(final DigitalObject digitalObject, final Model metadataModel) {
        // use the first identifier with a embargo date
        digitalObject.getContentids().stream()
                .map(contentIdentifier -> extractResource(contentIdentifier, metadataModel))
                .map(resource -> extractEmbargoDate(resource, digitalObject.getType()))
                .filter(Objects::nonNull)
                .findFirst()
                .ifPresent(digitalObject::setEmbargoDate);
    }

    private Resource extractResource(final ContentIdentifier contentIdentifier, final Model metadataModel) {
        final String identifier = contentIdentifier.getIdentifier();
        final String uri = this.identifierService.getUri(identifier);
        return metadataModel.getResource(uri);
    }

    private void propagateEmbargoDate(final DigitalObject digitalObject) {
        final Date embargoDate = digitalObject.getEmbargoDate();

        digitalObject.getChildObjects().stream()
                .filter(child -> child.getEmbargoDate() == null)
                .forEach(child -> child.setEmbargoDate(embargoDate));
    }

    @Override
    public void addReadOnly(final CellarIdentifiedObject cellarIdentifiedObject, final Model model) {
        final Boolean readOnly = isReadOnly(cellarIdentifiedObject, model);

        if (readOnly != null) {
            markAsReadOnly(cellarIdentifiedObject, readOnly);
        }

        if (cellarIdentifiedObject instanceof DigitalObject) {
            final DigitalObject digitalObject = (DigitalObject) cellarIdentifiedObject;
            digitalObject.getContentStreams().forEach(cs -> addReadOnly(cs, model));
            digitalObject.getChildObjects().forEach(obj -> addReadOnly(obj, model));
        }
    }

    @Override
    public void markAsReadOnly(final CellarIdentifiedObject cellarIdentifiedObject, final boolean readOnly) {
        if ((cellarIdentifiedObject.getReadOnly() != null) && (cellarIdentifiedObject.getReadOnly() != readOnly)) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCode(CmrErrors.READONLY_RESOURCE_ERROR)
                    .withMessage("Some resources of the same hierarchy have contradictory values regarding the read-only annotation (property {})")
                    .withMessageArgs(CellarAnnotationProperty.annotation_read_onlyP)
                    .build();
        }

        cellarIdentifiedObject.setReadOnly(readOnly);

        if (cellarIdentifiedObject instanceof DigitalObject) {
            final DigitalObject digitalObject = (DigitalObject) cellarIdentifiedObject;
            digitalObject.getContentStreams().forEach(cs -> markAsReadOnly(cs, readOnly));
            digitalObject.getChildObjects().forEach(obj -> markAsReadOnly(obj, readOnly));
        }
    }

    @Override
    public Boolean isReadOnly(final CellarIdentifiedObject cellarIdentifiedObject, final Model model) {
        final Set<Resource> sameAsResources = cellarIdentifiedObject.getContentids().stream()
                .map(ci -> model.getResource(this.identifierService.getUri(ci.getIdentifier())))
                .collect(Collectors.toSet());

        final List<Statement> statements = ModelUtils.getReadOnlyStatements(model, sameAsResources);

        if (!statements.isEmpty()) {
            final List<Boolean> distinctValues = statements.stream().map(s -> {
                if (!s.getObject().isLiteral()) {
                    throw ExceptionBuilder.get(CellarSemanticException.class).withCode(CmrErrors.READONLY_RESOURCE_ERROR)
                            .withMessage("Property {} is expecting a Literal as object (resource {})")
                            .withMessageArgs(CellarAnnotationProperty.annotation_read_onlyP, s.getSubject())
                            .build();
                }

                return s.getObject().asLiteral().getBoolean();
            }).distinct().collect(Collectors.toList());

            if (distinctValues.size() > 1) {
                throw ExceptionBuilder.get(CellarSemanticException.class).withCode(CmrErrors.READONLY_RESOURCE_ERROR)
                        .withMessage("Too many statements found for property {} for same resource: [{}]")
                        .withMessageArgs(CellarAnnotationProperty.annotation_read_onlyP, statements)
                        .build();
            }

            return distinctValues.get(0);
        }

        return null;
    }

    @Override
    public void parseMetadataLoadOldData(final DigitalObject digitalObject, final boolean useOldDataIfEmpty, final Model metadataModel,
                                         final boolean isContentUpdate, final Set<String> metadataRefs, final Map<String, Set<String>> urisPerDmd) {
        for (final DigitalObject childDigitalObject : digitalObject.getChildObjects()) {
            parseMetadataLoadOldData(childDigitalObject, useOldDataIfEmpty, metadataModel, isContentUpdate, metadataRefs, urisPerDmd);
        }

        //get business metadata stream
        final Metadata metadata = digitalObject.getBusinessMetadata();
        if (metadata != null) {

            final String fileRef = metadata.getFileRef();
            final Set<String> dmdUris = urisPerDmd.computeIfAbsent(fileRef, k -> new HashSet<>());

            for (final ContentIdentifier ci : digitalObject.getContentids()) {
                dmdUris.add(ci.getUri());
            }
        } else if (useOldDataIfEmpty) {
            // reuse old metadata

            final String id = digitalObject.getCellarId().getIdentifier();
            final String version = Optional.ofNullable(digitalObject.getVersions().get(ContentType.DIRECT))
                    .orElseGet(() -> retrieveDirectVersion(id));
            String direct = contentStreamService.getContentAsString(id, version, ContentType.DIRECT)
                    .orElseThrow(() -> new IllegalStateException("Direct metadata not found for " + id + " (" + version + ")"));

            //if content can be updated the contentstreams will need to be recaclulated and filled in so remove them from the metadata
            if (isContentUpdate) {
                direct = removeContentFromMetadataStream(direct);
            }

            if (metadataRefs.add(id)) {
                metadataModel.add(JenaUtils.read(direct, Lang.NT));
            }
        }
    }

    /**
     * The Digital object has been created without reading the database. It means that
     * the latest version of direct metadata is not available in the object and
     * the version is null. With a null version, S3 will return the latest version of
     * the object which can be stale data if the S3 replication is not done.
     * In order to avoid that, we need to read the database in this case to retrieve the
     * versions corresponding to the old data.
     *
     * @param cellarId the cellar id to find the record in CMR_CELLAR_RESOURCE_MD
     * @return the direct version or null (null for migrated data)
     */
    private String retrieveDirectVersion(String cellarId) {
        CellarResource resource = cellarResourceDao.findCellarId(cellarId);
        if (resource == null) {
            LOG.error("Trying to use old medatata but nothing found in the database for {}", cellarId);
        }
        final String version = resource != null ? resource.getVersion(ContentType.DIRECT).orElse(null) : null;
        if (version == null) {
            LOG.warn("Possible stale read (usage of null version) for {}. This is probably a migrated data from Fedora, " +
                    "if not there is probably something wrong here.", cellarId);
        }
        return version;
    }

    @Override
    public Model getInferredExtraData(final StructMap structMap, final Model dmd) {
        //create model to put all extra information
        final Model model = ModelFactory.createDefaultModel();
        try {
            final List<DigitalObject> allChilds = structMap.getDigitalObject().getAllChilds(true);
            //when changing the embargo date cellar must set the date bottom up so that the child nodes have the date of the parent node 
            final List<DigitalObject> sortedList = DigitalObjectUtils.sortListItemFirst(allChilds);

            for (final DigitalObject digitalObject : sortedList) {
                final Resource cellarResource = model.getResource(digitalObject.getCellarId().getUri());

                //depending on the type add other extra information
                if (DigitalObjectType.takeEmbargoDate(digitalObject.getType())) {
                    this.populateDigitalObjectWithExtractedEmbargoDate(digitalObject, dmd);
                }

                if (digitalObject.getType() == DigitalObjectType.MANIFESTATION) {
                    addManifestationType(digitalObject, dmd);
                    addManifestationHasItem(digitalObject, cellarResource, model);
                }
            }
            return model;
        } catch (final RuntimeException e) {
            JenaUtils.closeQuietly(model);
            throw e;
        }
    }

    @Override
    public void addLanguageMetadata(final DigitalObject digitalObject, final Model model, final Resource cellarResource) {
        // extract the languages from all production-uris and cellar-uri
        final Set<Resource> languages = new HashSet<Resource>();
        for (final String uri : digitalObject.getAllUris()) {
            final Resource createResource = model.createResource(uri);
            final List<Resource> resources = JenaQueries.getResources(createResource, CellarProperty.ontology_expression_uses_languageP,
                    false);
            languages.addAll(resources);
        }
        // error when no language if the notice to ingest was no prenotice
        digitalObject.checkLanguages(languages);

        // convert the languages to language-beans
        final List<LanguageBean> languageBeans = collect(languages, new RdfNode2LanguageBean(this.languagesSkosService),
                new ArrayList<LanguageBean>());
        // add all the languages to cellar-resource
        for (final LanguageBean language : languageBeans) {
            if (language.getIsoCodeTwoChar() != null) {
                final Literal isoCodeTwoCharLang = ResourceFactory.createTypedLiteral(language.getIsoCodeTwoChar(),
                        XSDDatatype.XSDlanguage);
                if (!cellarResource.hasProperty(CellarProperty.cmr_langP, isoCodeTwoCharLang)) {
                    model.add(ResourceFactory.createStatement(cellarResource, CellarProperty.cmr_langP, isoCodeTwoCharLang));
                }
            }
            final Literal isoCodeThreeCharLang = ResourceFactory.createTypedLiteral(language.getIsoCodeThreeChar(),
                    XSDDatatype.XSDlanguage);
            if (!cellarResource.hasProperty(CellarProperty.cmr_langP, isoCodeThreeCharLang)) {
                model.add(ResourceFactory.createStatement(cellarResource, CellarProperty.cmr_langP, isoCodeThreeCharLang));
            }
        }
        // set languages in digital object
        digitalObject.setLanguages(collect(languageBeans, LanguageBean2IsoCode.instance, new ArrayList<LanguageIsoCode>()));
    }

    @Watch(value = "Metadata Loader", arguments = {
            @Watch.WatchArgument(name = "Digital object business metadata", expression = "digitalObject.businessMetadata.id")
    })
    @Override
    public List<Set<String>> checkIfAllIdsExist(final DigitalObject digitalObject, final Model metadata, final List<Set<String>> faults) {
        for (final DigitalObject child : digitalObject.getChildObjects()) {
            checkIfAllIdsExist(child, metadata, faults);
        }
        for (final String uri : digitalObject.getAllUris()) {
            if (metadata.containsResource(ResourceFactory.createResource(uri))) {
                return faults;
            }
        }
        faults.add(digitalObject.getAllUris());
        return faults;
    }

    @Override
    public String removeContentFromMetadataStream(final String metadata) {
        final Model model = JenaUtils.read(metadata, Lang.NT);
        try {
            final HashSet<Resource> items = CollectionUtils.collect(model.listStatements(null, RDF.type, CellarType.cdm_itemR).toList(),
                    statement2Subject, new HashSet<>());
            for (final Resource item : items) {
                model.removeAll(item, null, null);
                model.removeAll(null, null, item);
            }
            return JenaUtils.toString(model, RDFFormat.NT);
        } finally {
            JenaUtils.closeQuietly(model);
        }
    }

    @Override
    public void removeLastModifiedInModelRecursive(final DigitalObject digitalObject, final Model model) {
        final Resource cellarResource = model.getResource(digitalObject.getCellarId().getUri());
        model.removeAll(cellarResource, CellarProperty.cmr_lastmodificationdateP, null);

        for (final ContentIdentifier ci : digitalObject.getContentids()) {
            final Resource ciResource = model.getResource(ci.getUri());
            model.removeAll(ciResource, CellarProperty.cmr_lastmodificationdateP, null);
        }

        for (final DigitalObject childDigitalObject : digitalObject.getChildObjects()) {
            removeLastModifiedInModelRecursive(childDigitalObject, model);
        }
    }

    @Watch(value = "Metadata Loader", arguments = {
            @Watch.WatchArgument(name = "Digital object business metadata", expression = "digitalObject.businessMetadata.id")
    })
    @Override
    public void addSameAsModelRecursive(final DigitalObject digitalObject, final Model model) {
        if (digitalObject.getCellarId() == null) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("there is no cellar identifier on digital object of structmap {}")
                    .withMessageArgs(digitalObject.getStructMap().getId()).build();
        }

        addSameAsModel(digitalObject, model);
        for (final ContentStream contentStream : digitalObject.getContentStreams()) {
            if (contentStream.getCellarId() == null) {
                throw ExceptionBuilder.get(CellarException.class)
                        .withMessage("there is no cellar identifier on contentstream of digital object {}")
                        .withMessageArgs(digitalObject.getCellarId().getIdentifier()).build();
            }
            addSameAsModel(contentStream, model);
        }
        for (final DigitalObject childDigitalObject : digitalObject.getChildObjects()) {
            addSameAsModelRecursive(childDigitalObject, model);
        }
    }

    /**
     * <p>addSameAsModel  adds for of a digital object a same-as relation between the cellar-resource and the production-system-resources.</p>
     *
     * @param digitalObject a {@link eu.europa.ec.opoce.cellar.domain.content.mets.CellarIdentifiedObject} object.
     * @param model         a {@link org.apache.jena.rdf.model.Model} object.
     */
    @Override
    public void addSameAsModel(final CellarIdentifiedObject digitalObject, final Model model) {
        SameAsModelUtils.addSameAs(model, digitalObject);
    }

    @Override
    public void addManifestationType(final DigitalObject digitalObject, final Model dmd) {
        // find mimetype of a manifestation
        final Set<String> mimeTypes = new HashSet<String>();
        for (final String uri : digitalObject.getAllUris()) {
            mimeTypes.addAll(JenaQueries.getLiteralValues(dmd.createResource(uri), CellarProperty.cdm_manifestation_typeP, false));
        }
        // error if there are multiple
        if (mimeTypes.size() > 1) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.MODEL_LOADING_INCOMPLETE_DATA)
                    .withMessage("the manifestation with the following uris more than 1 manifestationType defined: []")
                    .withMessageArgs(StringUtils.join(digitalObject.getAllUris(), ", ")).build();
        }

        // add the mimetypes to the manifestation digital object
        if (!mimeTypes.isEmpty()) {
            digitalObject.setManifestationMimeType(mimeTypes.iterator().next());
        }
    }

    /**
     * Adds the manifestation has item.
     *
     * @param digitalObject  the digital object
     * @param cellarResource the cellar resource
     * @param model          the model
     */
    private void addManifestationHasItem(final DigitalObject digitalObject, final Resource cellarResource, final Model model) {
        for (final ContentStream contentstream : digitalObject.getContentStreams()) {
            final Resource cellarDSResource = model.getResource(contentstream.getCellarId().getUri());
            model.add(cellarResource, CellarProperty.HierarchyProperty.cdm_manifestation_has_itemP, cellarDSResource);
        }
    }

    @Override
    public void addLastModificationInModel(final DigitalObject digitalObject, final Resource cellarResource, final Model model) {
        // add a last modification date for a digital object in its jena-model
        model.add(cellarResource, CellarProperty.cmr_lastmodificationdateP, model
                .createTypedLiteral(DateFormats.formatFullXmlDateTime(digitalObject.getLastModificationDate()), XSDDatatype.XSDdateTime));
    }

    @Override
    public void addCreationDateInModel(final Resource cellarResource, final Model model, Date creationDate) {
        // add a creation date for a digital object in its jena-model
        if (!cellarResource.hasProperty(CellarProperty.cmr_creationdateP)) {
            model.add(cellarResource, CellarProperty.cmr_creationdateP,
                    model.createTypedLiteral(DateFormats.formatFullXmlDateTime(creationDate), XSDDatatype.XSDdateTime));
        }
    }

    /**
     * Add the embargo date for a digital object in its jena-model
     *
     * @param digitalObject  the digital object
     * @param cellarResource the cellar resource
     * @param model          the model
     */
    @Override
    public void addEmbargoDateInModel(final DigitalObject digitalObject, final Resource cellarResource, final Model model) {
        final Triple<HierarchyElement<MetsElement, MetsElement>, Property, Resource> triple
                = HierarchyElement.getTriple(digitalObject.getType());
        final Date embargoDate = DigitalObjectUtils.getInheritedEmbargoDate(digitalObject);
        final String formattedEmbargoDate = DateFormats.formatFullXmlDateTime(embargoDate);
        final Literal literal = model.createTypedLiteral(formattedEmbargoDate, XSDDatatype.XSDdateTime);

        model.add(cellarResource, triple.getTwo(), literal);
    }

    @Override
    public void populateDigitalObjectWithExtractedEmbargoDate(final DigitalObject digitalObject, final Model dmd) {
        final DigitalObjectType type = digitalObject.getType();
        final Resource cellar = extractResource(digitalObject.getCellarId(), dmd);

        // extract the embargo dates from same-as resources
        final List<Date> embargoDates = JenaQueries.getResources(cellar, OWL.sameAs, false).stream()
                .map(resource -> extractEmbargoDate(resource, type))
                .collect(Collectors.toList());

        // check embargo dates
        Date embargoDate = extractEmbargoDate(cellar, type);
        for (final Date sameAsEmbargoDate : embargoDates) {
            if (embargoDate == null) {
                embargoDate = sameAsEmbargoDate;
            } else {
                assertSameEmbargoDate(digitalObject, embargoDate, sameAsEmbargoDate);
            }
        }

        // set embargo date
        if (embargoDate != null) {
            digitalObject.setEmbargoDate(embargoDate);
        }
    }

    private void assertSameEmbargoDate(final DigitalObject digitalObject, final Date embargoDate,
                                       final Date sameAsEmbargoDate) {
        if (sameAsEmbargoDate != null && !embargoDate.equals(sameAsEmbargoDate)) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withCode(CommonErrors.MODEL_LOADING_INCOMPLETE_DATA)
                    .withMessage("there are 2 different embargo dates specified for [{}], this is not allowed")
                    .withMessageArgs(StringUtils.join(digitalObject.getAllUris(), ", "))
                    .build();
        }
    }

    @Override
    public Date extractEmbargoDate(final Resource resource, final DigitalObjectType digitalObjectType) {
        // extract the embargo date
        final Property embargoProperty = EmbargoUtils.resolveEmbargoProperty(digitalObjectType);
        final String embargoValue = JenaQueries.getLiteralValue(resource, embargoProperty, false, true);
        if (embargoValue == null) {
            return null;
        }

        // parse the embargo date
        final Date date = EmbargoUtils.parseDate(embargoValue);
        if (date == null) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withCode(CommonErrors.MODEL_LOADING_INCOMPLETE_DATA)
                    .withMessage("the embargo date {} defined for {} is not valid")
                    .withMessageArgs(embargoValue, resource.getURI())
                    .build();
        }

        return date;
    }

    /**
     * Load new metadata.
     *
     * @param metsPackage   the mets package
     * @param metadataModel the metadata model
     * @param urisPerDmd    the uris per dmd
     */
    private void loadNewMetadata(final MetsPackage metsPackage, final Model metadataModel, final Map<String, Set<String>> urisPerDmd) {

        for (final Map.Entry<String, Set<String>> entry : urisPerDmd.entrySet()) {
            final String fileRef = entry.getKey();
            final Set<String> allowedCellarResourceUris = entry.getValue();

            if (StringUtils.isNotBlank(fileRef)) { // If no fileRef exists, nothing is to add to metadataModel
                ModelUtils.loadAllowedCellarResourcesOnly(metsPackage, fileRef, this.cellarPrefixUris, allowedCellarResourceUris,
                        metadataModel);
            }
        }
    }
}
