package eu.europa.ec.opoce.cellar.server.dissemination;

import eu.europa.ec.opoce.cellar.common.http.headers.CellarHttpHeaders;
import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.ExtendedMediaType;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;
import eu.europa.ec.opoce.cellar.server.service.DisseminationException;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.HttpHeaders;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Utility class for handling HTTP front-end requests to the dissemination service.
 * Implements some useful utility methods and constants.
 *
 * @Throws DisseminationException's with the appropriate HttpStatus and messages
 */
public abstract class DisseminationRequestUtils {

    /**
     * Constant <code>ACCEPT_PARAM_TYPE="type"</code>
     */
    public static final String ACCEPT_PARAM_TYPE = "type";

    /**
     * Constant <code>RDF_XML</code>
     */
    public static final MediaType RDF_XML = new MediaType("application", "rdf+xml");
    
    /**
     * Constant <code>RDF_XML_NOTICE_CONTENT</code>
     */
    public static final MediaType RDF_XML_NOTICE_CONTENT = new MediaType(RDF_XML, Collections.singletonMap(Notice.PARAM_NAME, Notice.CONTENT.getParamValue())); 
    
    public static final MediaType ZIP = new MediaType("application", "zip");

    /**
     * Constant <code>DEFAULT_META_MEDIATYPE</code>
     */
    public static final MediaType DEFAULT_META_MEDIATYPE = RDF_XML;

    /**
     * Constant <code>DEFAULT_CONTENT_MEDIATYPE</code>
     */
    public static final MediaType DEFAULT_CONTENT_MEDIATYPE = MediaType.ALL;
    
    /**
     * Constant <code>NOTICE_TYPE_CONTENT</code>
     */
    public static final String NOTICE_TYPE_CONTENT = Notice.PARAM_NAME + "=" + Notice.CONTENT.getParamValue();
    
    
    /**
     * Parse the Accept header of the given request into a list of {@link MediaType} objects.
     *
     * @param request to parse the Accept header from
     * @return the list of media types
     * @throws DisseminationException if the string cannot be parsed
     */
    public static List<MediaType> parseAcceptHeader(final HttpServletRequest request) {
        return parseAcceptHeader(request.getHeader(HttpHeaders.ACCEPT));
    }

    /**
     * Parse the given, comma-seperated string into a list of {@link MediaType} objects.
     * <p>This method can be used to parse an Accept or Content-Type header.
     *
     * @param acceptHeader the string to parse
     * @return the list of media types
     * @throws DisseminationException if the string cannot be parsed
     */
    public static List<MediaType> parseAcceptHeader(final String acceptHeader) {
        try {
            final List<MediaType> acceptTokens = MediaType.parseMediaTypes(acceptHeader);
            MediaType.sortByQualityValue(acceptTokens);

            return acceptTokens;
        } catch (final RuntimeException e) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withCause(e).withMessage("Illegal accept header: {}").withMessageArgs(e.getMessage()).build();
        }
    }

    /**
     * Parses the given, comma-separated {@String} into a {@code List} of {@link ExtendedMediaType} objects.
     *
     * @param acceptHeader the stringified Accept header to parse
     * @param extendedTypes flag specifying that extended media types must be supported
     * @return a {@code List} of {@code ExtendedMediaType}
     * @throws a {@code DisseminationException} if the string cannot be parsed
     */
    public static List<ExtendedMediaType> parseAcceptHeader(final String acceptHeader, boolean extendedTypes) {
        try {
            final List<ExtendedMediaType> acceptTokens = ExtendedMediaType.parseExtendedMediaTypes(acceptHeader);
            ExtendedMediaType.sortExtendedMediaTypeByQualityValue(acceptTokens);

            return acceptTokens;
        } catch (final RuntimeException e) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withCause(e).withMessage("Illegal accept header: {}").withMessageArgs(e.getMessage()).build();
        }
    }

    /**
     * Parse the Accept-Language header of the given request into a list of {@link AcceptLanguage} objects.
     *
     * @param request to parse the Accept-Language header from
     * @return the list of accept languages
     * @throws DisseminationException if the string cannot be parsed
     */
    public static List<AcceptLanguage> parseAcceptLanguageHeader(final HttpServletRequest request) {
        try {
            final List<AcceptLanguage> acceptLanguages = AcceptLanguage
                    .parseAcceptLanguages(request.getHeader(HttpHeaders.ACCEPT_LANGUAGE));
            AcceptLanguage.sortByQualityValue(acceptLanguages);

            return acceptLanguages;
        } catch (final Exception e) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withCause(e).withMessage("Illegal accept-language header: {}").withMessageArgs(e.getMessage()).build();
        }
    }

    /**
     * <p>getETag.</p>
     *
     * @param request a {@link javax.servlet.http.HttpServletRequest} object.
     * @return a {@link java.lang.String} object.
     */
    public static String getETag(final HttpServletRequest request) {
        try {
            return request.getHeader(HttpHeaders.IF_NONE_MATCH);
        } catch (final Exception e) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withCause(e).withMessage("Illegal eTag header: {}").withMessageArgs(e.getMessage()).build();
        }
    }

    public static String getAcceptCsMaxSize(final HttpServletRequest request) {
        try {
            return request.getHeader(CellarHttpHeaders.ACCEPT_MAX_CS_SIZE);
        } catch (final Exception e) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withCause(e).withMessage("Illegal Accept-Max-Cs-Size header: {}").withMessageArgs(e.getMessage()).build();
        }
    }

    /**
     * Validates the mime types specified in the Accept header:
     * <ol>
     * <li>Notice param can only be used with the mime type application/xml. If violation ==> 400 ERROR: </li>
     * <li>Notice param in combination with type param is not allowed. If violation ==> 400 ERROR: </li>
     * <li>The only supported values for the notice param are: tree, branch, object or identifier. If violation ==> 400 ERROR: </li>
     * <li>Mime types for metadata can not be combined with mime types for content. If violation ==> 400 ERROR: </li>
     * </ol>
     *
     * @param acceptTypes a {@link java.util.List} object.
     * @throws DisseminationException with appropriate Http Status code and message
     */
    public static void validateAcceptTypes(final List<MediaType> acceptTypes) {
        boolean metadataRequest = false;
        boolean contentRequest = false;
        for (final MediaType acceptType : acceptTypes) {
            final boolean hasNoticeParam = validateNoticeParam(acceptType);
            if ((isTypeRequest(acceptType)) && hasNoticeParam) {
                throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                        .withMessage(
                                "Illegal accept token specified in accept header. Accept token must not contain both a notice and a type parameter: '{}'")
                        .withMessageArgs(acceptType).build();
            }

            if (hasNoticeParam || isMetaDataRequest(acceptType)) {
                metadataRequest = true;
            } else {
                contentRequest = true;
            }
        }
        if (metadataRequest && contentRequest) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage(
                            "Illegal combination of accept tokens found in accept header. Accept header must not contain media types for both metadata and content: {}")
                    .withMessageArgs(StringUtils.join(acceptTypes, ", ")).build();
        }
    }

    /**
     * <p>toListOfStrings converts the list of mediaTypes to a ordered list of strings without quality.</p>
     *
     * @param acceptTypes a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    public static List<String> toListOfStrings(List<MediaType> acceptTypes) {
        acceptTypes = new ArrayList<MediaType>(acceptTypes);
        MediaType.sortByQualityValue(acceptTypes);

        return CollectionUtils.collect(acceptTypes, new Transformer<MediaType, String>() {

            @Override
            public String transform(final MediaType input) {
                // get value of mediaType with quality
                final String typeWithQualification = input.toString();
                if (typeWithQualification.contains(";q=")) {
                    // remove quality property
                    final String[] splitted = typeWithQualification.split(";");
                    final StringBuilder typeWithoutQualification = new StringBuilder(splitted[0]);
                    for (int i = 1; i < splitted.length; i++) {
                        if (!splitted[i].startsWith("q=")) {
                            typeWithoutQualification.append(";").append(splitted[i]);
                        }
                    }
                    return typeWithoutQualification.toString();
                } else {
                    return typeWithQualification;
                }
            }
        }, new ArrayList<String>(acceptTypes.size()));
    }

    /**
     * Filters the 'notice=content' parameter of the accept type when the dissemination request
     * concerns an RDF content stream (accept type "application/rdf+xml;notice=content"),
     * so that it matches the actual mime type of the RDF content stream ("application/rdf+xml").
     * 
     * @param acceptTypes the accept types.
     * @return the accept types with the 'notice=content' parameter removed.
     */
    public static List<String> filterRdfContentStreamAcceptHeader(List<String> acceptTypes) {
        return CollectionUtils.collect(acceptTypes, acceptType -> {
        	MediaType mediaType = MediaType.parseMediaType(acceptType);
        	if (mediaType.equals(RDF_XML_NOTICE_CONTENT)) {
        		return RDF_XML.toString();
        	}
        	else {
        		return acceptType;
        	}
        }, new ArrayList<String>(acceptTypes.size()));
    }  
    
    /**
     * Returns <code>true</code> if given {@link MediaType} represents a media type for resource metadata.
     * <p/>
     * Accept Mime Types for metadata are:
     * 1. application/xml;notice=tree, application/xml;notice=branch, application/xml;notice=object, application/xml;notice=identifier
     * 2. application/rdf+xml, application/rdf+xml;type=...
     * 3. MediaType.All
     *
     * @param mediaType a {@link org.springframework.http.MediaType} object.
     * @return <code>true</code> if given {@link MediaType} indicates a request for resource metadata
     * @see #isRdfRequest(org.springframework.http.MediaType)
     * @see #isNoticeRequest(org.springframework.http.MediaType)
     */
    public static boolean isMetaDataRequest(final MediaType mediaType) {
        return (isRdfRequest(mediaType) && !mediaType.toString().contains(NOTICE_TYPE_CONTENT)) || isXmlRequestWithNotice(mediaType);
    }

    /**
     * <p>validateNoticeParam.</p>
     *
     * @param mediaType a {@link org.springframework.http.MediaType} object.
     * @return a boolean.
     */
    public static boolean validateNoticeParam(final MediaType mediaType) {
        final Notice notice = Notice.mediaValueOf(mediaType);
        if (notice == Notice.NONE) {
            return false;
        }

        // check that type of notice is correct
        if (notice == Notice.IDENTIFIER) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage(
                            "Illegal accept token specified in accept header. Accept token with unsupported value '{}' as notice parameter: '{}'")
                    .withMessageArgs(notice, mediaType).build();
        }

        return true;
    }

    /**
     * Checks whether the given {@link MediaType} represents the media type <code>application/xml;notice=...</code>
     * and whether the <code>notice</code> param has a legal value.
     *
     * @param mediaType a {@link org.springframework.http.MediaType} object.
     * @return <code>true</code> if given {@link MediaType} represents the media type <code>application/xml;notice=...</code>
     * @see eu.europa.ec.opoce.cellar.server.dissemination.Notice
     */
    public static boolean isXmlRequestWithNotice(final MediaType mediaType) {
        // check that type is application/xml
        if (!mediaType.getType().equalsIgnoreCase(MediaType.APPLICATION_XML.getType())
                || !mediaType.getSubtype().equalsIgnoreCase(MediaType.APPLICATION_XML.getSubtype())) {
            return false;
        }

        // check that type of notice is correct
        final Notice notice = Notice.mediaValueOf(mediaType);
        return notice != Notice.NONE;
    }

    /**
     * Checks whether the given {@link MediaType} has a <code>type</code> parameter.
     *
     * @param mediaType a {@link org.springframework.http.MediaType} object.
     * @return <code>true</code> if given {@link MediaType} has a <code>type</code> parameter.
     */
    public static boolean isTypeRequest(final MediaType mediaType) {
        return getTypeParameter(mediaType) != null;
    }

    /**
     * <p>getTypeParameter.</p>
     *
     * @param mediaType a {@link org.springframework.http.MediaType} object.
     * @return a {@link java.lang.String} object.
     */
    public static String getTypeParameter(final MediaType mediaType) {
        return StringUtils.trim(mediaType.getParameter(ACCEPT_PARAM_TYPE));
    }

    /**
     * Checks whether the given {@link MediaType} represents the media type <code>application/rdf+xml</code>
     *
     * @param mediaType a {@link org.springframework.http.MediaType} object.
     * @return <code>true</code> if the given {@link MediaType} represents the media type <code>application/rdf+xml</code>
     */
    public static boolean isRdfRequest(final MediaType mediaType) {
        if (mediaType.equals(MediaType.ALL)) {
            return true;
        }

        return mediaType.getType().equalsIgnoreCase(RDF_XML.getType()) && mediaType.getSubtype().equalsIgnoreCase(RDF_XML.getSubtype());
    }

    /**
     * <p>getRdfRequestType starting from a mediatype.</p>
     *
     * @param cellarResource a {@link eu.europa.ec.opoce.cellar.server.service.CellarResourceBean} object.
     * @param mediaType     a {@link org.springframework.http.MediaType} object.
     * @return a {@link eu.europa.ec.opoce.cellar.server.dissemination.RequestType} object.
     */
    public static RequestType getRdfRequestType(final CellarResourceBean cellarResource, final MediaType mediaType) {
        RequestType retRequestType = null;

        final Notice noticeType = Notice.mediaValueOf(mediaType);
        switch (noticeType) {
        case TREE:
            retRequestType = RequestType.RDF_XML_TREE;
            break;
        case NON_INFERRED:
            retRequestType = RequestType.RDF_XML_NONINFERRED_OBJECT;
            break;
        case NON_INFERRED_TREE:
            retRequestType = RequestType.RDF_XML_NONINFERRED_TREE;
            break;
        case CONTENT:
            retRequestType = RequestType.RDF_CONTENT;
            break;
        case NONE:
        default:
            retRequestType = RequestType.RDF_XML_OBJECT;
        }

        return retRequestType;
    }

    /**
     * <p>getMediaType starting from a requesttype.</p>
     *
     * @param requestType a {@link eu.europa.ec.opoce.cellar.server.dissemination.RequestType} object.
     * @return a {@link org.springframework.http.MediaType} object.
     */
    public static MediaType getMediaType(final RequestType requestType) {
        return requestType.accept(new RequestTypeVisitor<Object, MediaType>() {

            @Override
            public MediaType visitTree(final Object in) {
                return new MediaType(MediaType.APPLICATION_XML, Collections.singletonMap(Notice.PARAM_NAME, Notice.TREE.getParamValue()));
            }

            @Override
            public MediaType visitBranch(final Object in) {
                return new MediaType(MediaType.APPLICATION_XML, Collections.singletonMap(Notice.PARAM_NAME, Notice.BRANCH.getParamValue()));
            }

            @Override
            public MediaType visitObject(final Object in) {
                return new MediaType(MediaType.APPLICATION_XML, Collections.singletonMap(Notice.PARAM_NAME, Notice.OBJECT.getParamValue()));
            }

            @Override
            public MediaType visitIdentifier(final Object in) {
                return new MediaType(MediaType.APPLICATION_XML,
                        Collections.singletonMap(Notice.PARAM_NAME, Notice.IDENTIFIER.getParamValue()));
            }

            @Override
            public MediaType visitContent(final Object in) {
                return new MediaType(MediaType.APPLICATION_XHTML_XML, Collections.<String, String> emptyMap());
            }

            @Override
            public MediaType visitRdfObject(final Object in) {
                return new MediaType(RDF_XML, Collections.<String, String> emptyMap());
            }

            @Override
            public MediaType visitRdfObjectNormalized(Object in) {
                return this.visitRdfObject(in);
            }

            @Override
            public MediaType visitRdfTree(final Object in) {
                return this.visitRdfObject(in);
            }

            @Override
            public MediaType visitRdfTreeNormalized(Object in) {
                return this.visitRdfObject(in);
            }

            @Override
            public MediaType visitRdfNonInferredObject(final Object in) {
                return this.visitRdfObject(in);
            }

            @Override
            public MediaType visitRdfNonInferredObjectNormalized(Object in) {
                return this.visitRdfObject(in);
            }

            @Override
            public MediaType visitRdfNonInferredTree(final Object in) {
                return this.visitRdfObject(in);
            }
            
            @Override
            public MediaType visitRdfContent(final Object in) {
                return new MediaType(RDF_XML, Collections.singletonMap(Notice.PARAM_NAME, Notice.CONTENT.getParamValue()));
            }
            

            @Override
            public MediaType visitRdfNonInferredTreeNormalized(Object in) {
                return this.visitRdfObject(in);
            }

            @Override
            public MediaType visitConceptScheme(final Object in) {
                return new MediaType(RDF_XML, Collections.<String, String> emptyMap());
            }

            @Override
            public MediaType visitZip(final Object in) {
                return new MediaType(ZIP, Collections.<String, String> emptyMap());
            }

        }, null);
    }

    /**
     * Get back the a new mediaType which is the same as the given {@link mediatype}, but without parameters.
     *
     * @param mediaType the given mediaType
     * @return mediaType the mediaType without parameters
     */
    public static MediaType getMediaTypeWithoutParams(final MediaType mediaType) {
        return new MediaType(mediaType, (Map<String, String>) null);
    }

}
