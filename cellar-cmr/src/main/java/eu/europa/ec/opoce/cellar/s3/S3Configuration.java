package eu.europa.ec.opoce.cellar.s3;

public class S3Configuration {

    private String bucket;
    private boolean unsafe;

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public boolean isUnsafe() {
        return unsafe;
    }

    public void setUnsafe(boolean unsafe) {
        this.unsafe = unsafe;
    }
}
