package eu.europa.ec.opoce.cellar.nal.repository;

import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.database.Database;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.dao.RequiredLanguageDbRecord;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static eu.europa.ec.opoce.cellar.common.util.MapUtils.addMappedSet;
import static eu.europa.ec.opoce.cellar.common.util.MapUtils.addValueToMappedList;

/**
 * @author ARHS Developments
 */
@Repository
public class RequiredLanguageDbGateway {

    private static final String SELECT_ALL = "SELECT DISTINCT URI, SORTING, ALTERNATIVE_URI FROM LANGUAGE_CONFIG";

    private final Database database;

    @Autowired
    public RequiredLanguageDbGateway(@Qualifier("databaseCellarDataCmr") Database database) {
        this.database = database;
    }

    /**
     * <p>selectAll.</p>
     *
     * @return a {@link java.util.Map} object.
     */
    public Map<String, List<RequiredLanguageDbRecord>> selectAll() {
        ResultSetExtractor<Map<String, List<RequiredLanguageDbRecord>>> extractor = new ResultSetExtractor<Map<String, List<RequiredLanguageDbRecord>>>() {

            @Override
            public Map<String, List<RequiredLanguageDbRecord>> extractData(ResultSet rs) throws SQLException, DataAccessException {
                Map<String, List<RequiredLanguageDbRecord>> recordsPerLanguage = new HashMap<String, List<RequiredLanguageDbRecord>>();
                Map<String, Set<String>> alternativesPerLanguage = new HashMap<String, Set<String>>();
                while (rs.next()) {
                    String language = rs.getString(1);
                    if (StringUtils.isBlank(language)) {
                        throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.DEFINED_LANGUAGE_IS_NOT_CORRECT).build();
                    }

                    String alternative = rs.getString(3);
                    if (StringUtils.isBlank(alternative)) {
                        addValueToMappedList(recordsPerLanguage, language, new RequiredLanguageDbRecord(language));
                    } else {
                        if (language.equals(alternative)) {
                            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.DEFINED_LANGUAGE_IS_NOT_CORRECT)
                                    .withMessage("Language found defined as its own alternative '{}'").withMessageArgs(language).build();
                        }

                        Set<String> alternatives = addMappedSet(alternativesPerLanguage, language);
                        if (alternatives.contains(alternative)) {
                            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.DEFINED_LANGUAGE_IS_NOT_CORRECT)
                                    .withMessage("Same alternative language '{}' with different order found for language '{}' ")
                                    .withMessageArgs(alternative, language).build();
                        }
                        alternatives.add(alternative);

                        int order = rs.getInt(2);
                        if (order == 0) {
                            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.DEFINED_LANGUAGE_IS_NOT_CORRECT)
                                    .withMessage("Alternative language '{}' found with invalid order '0' for language '{}'")
                                    .withMessageArgs(alternative, language).build();
                        }

                        addValueToMappedList(recordsPerLanguage, language, new RequiredLanguageDbRecord(language, order, alternative));
                    }
                }
                return recordsPerLanguage;
            }
        };

        return database.getJdbcOperations().query(SELECT_ALL, extractor);
    }

}
