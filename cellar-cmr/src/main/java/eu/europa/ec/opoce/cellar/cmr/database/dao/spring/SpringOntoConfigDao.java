package eu.europa.ec.opoce.cellar.cmr.database.dao.spring;

import eu.europa.ec.opoce.cellar.cmr.database.dao.OntoConfigDao;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.ontology.dao.OntoConfig;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

@Repository
public class SpringOntoConfigDao extends CmrSpringBaseDao<OntoConfig, Long> implements OntoConfigDao {

    private static final String URI = "URI";
    private static final String ONTO_NAME = "ONTO_NAME";
    private static final String CREATED_ON = "CREATED_ON";
    private static final String VERSION_NR = "VERSION_NR";
    private static final String VERSION_DATE = "VERSION_DATE";
    private static final String PID = "FEDORA_PID";
    private static final String VERSION = "VERSION";

    private static final String SELECT_BY_URI = "URI = :uri";
    private static final String SELECT_BY_PID = "FEDORA_PID = :pid";

    public SpringOntoConfigDao() {
        super("ONTOCONFIG", Arrays.asList(URI, ONTO_NAME, CREATED_ON, VERSION_NR, VERSION_DATE, PID, VERSION));
    }

    @Override
    public Optional<OntoConfig> findByUri(final String uri) {
        if (StringUtils.isBlank(uri)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument uri must not be blank").build();
        }

        return Optional.ofNullable(findUnique(SELECT_BY_URI, Collections.singletonMap("uri", uri)));
    }

    @Override
    public Optional<OntoConfig> findByPid(String pid) {
        Objects.requireNonNull(pid, "PID is null");
        return Optional.ofNullable(findUnique(SELECT_BY_PID, Collections.singletonMap("pid", pid)));
    }


    @Override
    public OntoConfig create(final ResultSet rs) throws SQLException {
        OntoConfig ontoConfig = new OntoConfig();
        ontoConfig.setUri(rs.getString(URI));
        ontoConfig.setName(rs.getString(ONTO_NAME));
        ontoConfig.setCreatedOn(rs.getTimestamp(CREATED_ON));
        ontoConfig.setVersionNr(rs.getString(VERSION_NR));
        ontoConfig.setVersionDate(rs.getTimestamp(VERSION_DATE));
        ontoConfig.setExternalPid(rs.getString(PID));
        ontoConfig.setVersion(rs.getString(VERSION));
        return ontoConfig;
    }

    @Override
    public void fillMap(OntoConfig ontoConfig, Map<String, Object> map) {
        map.put(URI, ontoConfig.getUri());
        map.put(ONTO_NAME, ontoConfig.getName());
        map.put(CREATED_ON, ontoConfig.getCreatedOn());
        map.put(VERSION_NR, ontoConfig.getVersionNr());
        map.put(VERSION_DATE, ontoConfig.getVersionDate());
        map.put(PID, ontoConfig.getExternalPid());
        map.put(VERSION, ontoConfig.getVersion());
    }
}
