/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.conditionalRequest
 *             FILE : ConditionalRequestVersion.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 21, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.conditionalRequest;

import eu.europa.ec.opoce.cellar.common.util.TimeUtils;

import java.util.Date;

import org.apache.commons.lang.StringUtils;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 21, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class ConditionalRequestVersions {

    private final String eTag;
    private final String newETag;
    private final String lastModified;
    private final Date newLastModified;

    public ConditionalRequestVersions(final String eTag, final String newETag, final String lastModified, final Date newLastModified) {
        this.eTag = eTag;
        this.newETag = newETag;
        this.lastModified = lastModified;
        this.newLastModified = newLastModified;
    }

    /**
     * @return the newETag
     */
    public String getNewETag() {
        return this.newETag;
    }

    /**
     * @return the newLastModified
     */
    public Date getNewLastModified() {
        return this.newLastModified;
    }

    public boolean isChanged() {
        if (StringUtils.isNotBlank(this.eTag)) { // eTag has priority
            return !this.eTag.equals(this.newETag);
        }

        if (this.lastModified != null) {
            return !this.lastModified.equals(TimeUtils.formatHTTP11Date(this.newLastModified));
        }

        return true;
    }
}
