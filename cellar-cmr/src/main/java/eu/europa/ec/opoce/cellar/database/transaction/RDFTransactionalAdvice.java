/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.database.transaction
 *             FILE : RDFTransactionalAdvice.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Mar 18, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.database.transaction;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.Order;

/**
 * <class_description> This is an advice that, at each method annotated with {@link RDFTransactional}, commits(rolls back) the transactions
 * on those RDF tables which are managed by Oracle Spatial.<br/>
 * It is necessary as Oracle Spatial does not support any transaction mechanism.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Mar 18, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Aspect
@Order(3)
public class RDFTransactionalAdvice {

    private static final RDFTransactionalAdvice instance = new RDFTransactionalAdvice();

    @Around("@annotation(rdfTransactional)")
    public Object process(final ProceedingJoinPoint pjp, final RDFTransactional rdfTransactional) throws Throwable {
        Object retObject = null;

        try {
            retObject = pjp.proceed(pjp.getArgs());
        } catch (final Throwable t) {
            // if the transaction is rollbackable, it rolls the transaction back and closes the associated connection
            if (isRollbackableException(t, rdfTransactional.rollbackFor())) {
                RDFTransactionManager.INSTANCE.rollback();
            }
            // otherwise, it just closes the connection associated to the transaction
            else {
                RDFTransactionManager.INSTANCE.close();
            }
            // in all cases, throws up
            throw t;
        }
        // commits the transaction and closes the associated connection
        RDFTransactionManager.INSTANCE.commit();

        return retObject;
    }

    private static boolean isRollbackableException(final Throwable throwable, final Class<? extends Throwable>[] rollbackableExceptions) {
        boolean isRollbackableException = false;

        for (final Class<? extends Throwable> rollbackableException : rollbackableExceptions) {
            if (throwable.getClass().equals(rollbackableException)) {
                isRollbackableException = true;
                break;
            }
        }

        return isRollbackableException;
    }

    /**
     * Needed by AspectJ
     */
    public static RDFTransactionalAdvice aspectOf() {
        return instance;
    }

}
