/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.runnable.index
 *             FILE : AutomaticReindexationRunnable.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 13 sept. 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.indexing;

import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.index.ScheduledReindexation;
import eu.europa.ec.opoce.cellar.cl.service.ScheduledReindexationService;
import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService.Priority;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cl.service.client.SparqlExecutingService;
import eu.europa.ec.opoce.cellar.cl.service.client.UriFoundDelegator;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CmrIndexRequestDao;
import eu.europa.ec.opoce.cellar.cmr.indexing.service.IIndexRequestSchedulerService;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.ExecutionStatus;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.Reason;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.RequestType;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestGenerationService;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.logging.LogContext;
import org.apache.commons.lang.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.CMR_RE_INDEXING;

/**
 * The Class AutomaticReindexationRunnable.
 * <class_description> The runnable class that should select work ids and create index requests within a configurable period of time
 * <br/><br/>
 * ON : 13 sept. 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@LogContext
public class AutomaticReindexationRunnable implements Runnable {

    private static final Logger LOG = LogManager.getLogger(AutomaticReindexationRunnable.class);

    private final ScheduledReindexationService scheduledReindexationService;

    /**
     * The cellar configuration.
     */
    private final ICellarConfiguration cellarConfiguration;

    /**
     * The cmr index request generation service.
     */
    private final CmrIndexRequestGenerationService cmrIndexRequestGenerationService;

    /**
     * The cellar resource dao.
     */
    private final CellarResourceDao cellarResourceDao;

    /**
     * The sparql executing service.
     */
    private final SparqlExecutingService sparqlExecutingService;

    /**
     * The cmr index request dao.
     */
    private final CmrIndexRequestDao cmrIndexRequestDao;

    /**
     * The identifier service.
     */
    private final IdentifierService identifierService;

    /**
     * The index request scheduler service.
     */
    private final IIndexRequestSchedulerService indexRequestSchedulerService;

    /**
     * The stop.
     */
    private final AtomicBoolean stop = new AtomicBoolean(false);

    /**
     * The pause.
     */
    private final AtomicBoolean pause = new AtomicBoolean(false);

    /**
     * The running.
     */
    private final AtomicBoolean running = new AtomicBoolean(false);

    /**
     * The work identifiers.
     */
    private HashSet<String> workIdentifiers;

    public AutomaticReindexationRunnable(ScheduledReindexationService scheduledReindexationService, ICellarConfiguration cellarConfiguration,
                                         CmrIndexRequestGenerationService cmrIndexRequestGenerationService, CellarResourceDao cellarResourceDao,
                                         SparqlExecutingService sparqlExecutingService, CmrIndexRequestDao cmrIndexRequestDao,
                                         IdentifierService identifierService, IIndexRequestSchedulerService indexRequestSchedulerService) {
        this.scheduledReindexationService = scheduledReindexationService;
        this.cellarConfiguration = cellarConfiguration;
        this.cmrIndexRequestGenerationService = cmrIndexRequestGenerationService;
        this.cellarResourceDao = cellarResourceDao;
        this.sparqlExecutingService = sparqlExecutingService;
        this.cmrIndexRequestDao = cmrIndexRequestDao;
        this.identifierService = identifierService;
        this.indexRequestSchedulerService = indexRequestSchedulerService;
    }

    /**
     * Stop.
     */
    public void stop() {
        this.stop.set(true);
    }

    /**
     * Checks if is running.
     *
     * @return true, if is running
     */
    public boolean isRunning() {
        return this.running.get();
    }

    /**
     * Pause.
     */
    public void pause() {
        this.pause.set(true);
    }

    /**
     * Resume.
     */
    public void resume() {
        this.pause.set(false);
    }

    /**
     * Checks if is paused.
     *
     * @return true, if is paused
     */
    public boolean isPaused() {
        return this.pause.get();
    }

    @Override
    @LogContext(CMR_RE_INDEXING)
    public void run() {
        running.set(true);
        try {
            //set starting date
            final Date startDate = new Date();
            // get cellar configurable properties
            final long pageSize = cellarConfiguration.getCellarServiceIndexingScheduledSelectionPageSize();
            final long batchSleepTime = cellarConfiguration.getCellarServiceIndexingScheduledBatchSleepTime();
            final String priorityString = cellarConfiguration.getCellarServiceIndexingScheduledPriority();
            final boolean doesCalcInverse = cellarConfiguration.getCellarServiceIndexingScheduledCalcInverse();
            final boolean doesCalcEmbedded = cellarConfiguration.getCellarServiceIndexingScheduledCalcEmbedded();
            final boolean doesCalcNotice = cellarConfiguration.getCellarServiceIndexingScheduledCalcNotice();
            final boolean doesCalcExpanded = cellarConfiguration.getCellarServiceIndexingScheduledCalcExpanded();
            //get scheduled configuration
            final ScheduledReindexation configuration = scheduledReindexationService.getScheduledReindexationConfiguration();
            final boolean useSparql = configuration.isUseSparql();
            final boolean allEligibleContent = configuration.isAllEligibleContent();
            final boolean allEligibleContentWithinPeriod = configuration.isAllEligibleContentWithinPeriod();
            Date lastModificationDateLowerLimit = null;
            if (allEligibleContentWithinPeriod) {
                lastModificationDateLowerLimit = AutomaticReindexationRunnable.this
                        .getLastModificationDateLowerLimit(configuration);
            }
            //get index request specific arguments
            final Collection<RequestType> status = RequestType.getStatus(doesCalcInverse, doesCalcEmbedded,
                    doesCalcNotice, doesCalcExpanded, false);
            final Priority priority = Priority.resolve(priorityString);

            execute(startDate, pageSize, batchSleepTime, configuration, useSparql,
                    allEligibleContent, allEligibleContentWithinPeriod, status, priority, lastModificationDateLowerLimit);

        } catch (final Exception e) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.INDEXING_SERVICE_ERROR)
                    .withMessage("Unexpected exception when executing the automatic reindexation").withCause(e).build();
        } finally {
            running.set(false);
        }
    }

    /**
     * Paginate through the selection result and create the indexation requests.
     *
     * @param startDate      the start date
     * @param pageSize       the page size
     * @param batchSleepTime the batch sleep time
     * @param configuration  the configuration
     * @param useSparql      the use sparql
     * @param allContent     the all eligible content
     * @param partialContent the all eligible content within period
     * @param status         the status
     * @param priority       the priority
     * @param lowerRange     the last modification date lower limit
     * @throws Exception the exception
     */
    private void execute(final Date startDate, final long pageSize, final long batchSleepTime, final ScheduledReindexation configuration,
                         final boolean useSparql, final boolean allContent, final boolean partialContent, final Collection<RequestType> status,
                         final Priority priority, final Date lowerRange) throws Exception {
        boolean canContinue = true;
        while (canContinue && !this.stop.get()) {
            if (useSparql) {
                retrieveWorkIdentifiersForSparql(configuration);
            } else {
                retrieveWorkIdentifiersForSql(startDate, pageSize, allContent, partialContent, lowerRange);
            }
            LOG.info("Creating {} indexation requests.", pageSize);
            //  when using sparql use a set to page through the workIdentifiers
            final HashSet<String> identifiersProcessedWithSparql = createRequests(pageSize, batchSleepTime, useSparql, status, priority);
            LOG.info("Finished creating {} indexation requests.", pageSize);
            //force indexation requests to be picked up by the scheduler
            this.indexRequestSchedulerService.forceScheduleIndexRequestsAsynchronously();

            //if the selected identifiers is less than the page size than this should be the last page
            if ((!useSparql && (this.workIdentifiers.size() < pageSize))
                    || (useSparql && (identifiersProcessedWithSparql.size() < pageSize))) {
                canContinue = false;
            } else {
                if (useSparql) {
                    //remove from the previous selection to avoid being processed repeatedly
                    this.workIdentifiers.removeAll(identifiersProcessedWithSparql);
                }
                //check if all requests were handled
                long runningRequests = this.cmrIndexRequestDao.countByReasonAndExecutionStatus(Reason.Scheduled, ExecutionStatus.Execution,
                        ExecutionStatus.New, ExecutionStatus.Pending);
                while (canContinue && !this.stop.get() && (runningRequests != 0)) {
                    // wait a configurable amount of time
                    Thread.sleep(batchSleepTime);
                    //check if all requests were handled
                    runningRequests = this.cmrIndexRequestDao.countByReasonAndExecutionStatus(Reason.Scheduled, ExecutionStatus.Execution,
                            ExecutionStatus.New, ExecutionStatus.Pending);
                    //if first part is already false it will not attempt to compute the second
                    canContinue = canContinue && isStillWithinConfiguredTimePeriod(startDate, configuration);
                }
            }
            //if first part is already false it will not attempt to compute the second
            canContinue = canContinue && isStillWithinConfiguredTimePeriod(startDate, configuration);
            //check if it has been paused
            if (canContinue && this.pause.get()) {
                LOG.info("The automatic reindexation runnable was paused. It will be put to sleep every 30s until its execution has been resumed");
                while (this.pause.get()) {
                    Thread.sleep(30000);
                }
                LOG.info("The automatic reindexation runnable has been resumed");
            }
        } //if can't continue exit runnable

        if (this.stop.get()) {
            LOG.info("The automatic reindexation runnable was stopped.");
            LOG.info("It should continue in its next scheduled reindexation.");
            LOG.info("To stop all scheduled reindexations disable the scheduler.");
        } else {
            LOG.info("The automatic reindexation has finished processing all selected resources.");
        }
    }

    /**
     * Creates the requests.
     * Sparql results are all put in the workidentifiers collection
     * When using a sparql the result must return a number up to pageSize of workidentifiers
     * These elements are then removed from the workidentifiers collection preventing the runnable from executing over the same element twice
     *
     * @param pageSize       the page size
     * @param batchSleepTime the batch sleep time
     * @param useSparql      the use sparql
     * @param status         the status
     * @param priority       the priority
     * @return the hash set
     * @throws InterruptedException the interrupted exception
     */
    private HashSet<String> createRequests(final long pageSize, final long batchSleepTime, final boolean useSparql,
                                           final Collection<RequestType> status, final Priority priority) throws InterruptedException {
        // create index request
        //mark the records as indexed
        final HashSet<String> result = new HashSet<String>();
        for (final String cellarId : this.workIdentifiers) {
            //check if it has been paused
            if (this.pause.get()) {
                LOG.info("The automatic reindexation runnable was paused. It will be put to sleep every 30s until its execution has been resumed");
                while (this.pause.get()) {
                    Thread.sleep(30000);
                }
                LOG.info("The automatic reindexation runnable has been resumed");
            }
            while (this.pause.get()) {
                // wait a configurable amount of time
                Thread.sleep(batchSleepTime);
            }
            this.cmrIndexRequestGenerationService.generateIndexRequest(cellarId, "administrator", status, priority, Reason.Scheduled);
            if (useSparql) {
                result.add(cellarId);
                if (result.size() == pageSize) {
                    break;
                }
            }
        }
        return result;
    }

    /**
     * Retrieve work identifiers for sql.
     * the results of the sql are put in the workIdentifiers collection
     * the query must return a pageSize number of results
     * ordered by last modification date DESC and last indexation date ASC
     * all records must be initialized with the epoch date or null
     * When an indexation request is finalized successfuly the indexation date is updated
     * and the query also filters on the records
     * that have a null lastIndexationDate or lower than
     * the initial execution date - startDate - to avoid execution twice on the same record
     *
     * @param startDate                      the start date
     * @param pageSize                       the page size
     * @param allEligibleContent             the all eligible content
     * @param allEligibleContentWithinPeriod the all eligible content within period
     * @param lastModificationDateLowerLimit the last modification date lower limit
     */
    private void retrieveWorkIdentifiersForSql(final Date startDate, final long pageSize, final boolean allEligibleContent,
                                               final boolean allEligibleContentWithinPeriod, final Date lastModificationDateLowerLimit) {
        //using SQL we always retrieve X elements of a selection ordered by lastModificationDate DESC and lastIndexationDate ASC, indexed before the starting date of this thread
        // this way the indexation is continuous
        this.workIdentifiers = new HashSet<String>();
        Collection<CellarResource> cellarResources = null;
        if (allEligibleContent) {
            cellarResources = this.cellarResourceDao.getWorks(pageSize, startDate);
        } else if (allEligibleContentWithinPeriod) {
            cellarResources = this.cellarResourceDao.getLastWorks(pageSize, lastModificationDateLowerLimit, startDate);
        }
        final List<String> cellarIds = cellarResources.stream()//
                .map(cellarResourcesMd -> cellarResourcesMd.getCellarId())//
                .filter(Objects::nonNull)//
                .collect(Collectors.toList());//
        this.workIdentifiers.addAll(cellarIds);
    }

    /**
     * Retrieve work identifiers for sparql.
     * All the results are put in the workIdentifiers collection
     *
     * @param configuration the configuration
     * @throws Exception the exception
     */
    private void retrieveWorkIdentifiersForSparql(final ScheduledReindexation configuration) throws Exception {
        if ((this.workIdentifiers == null) || this.workIdentifiers.isEmpty()) {
            //using sparql the workIdentifiers set is populated with all results
            this.workIdentifiers = new HashSet<String>();
            final String sparqlQuery = configuration.getSparqlQuery();//
            final File tempFile = this.sparqlExecutingService.executeSparqlQueryFormat(sparqlQuery, null);
            this.sparqlExecutingService.parseSparqlReponse(tempFile.getAbsolutePath(), new UriFoundDelegator() {

                @Override
                public void onUri(final String uri) {
                    final String prefixedFromUri = identifierService.getPrefixedFromUri(uri);
                    workIdentifiers.add(prefixedFromUri);
                }

                @Override
                public void onEnd() {
                    LOG.info("Parsing returns {} elements", workIdentifiers.size());
                }
            });
        }
    }

    /**
     * Checks if is still within configured time period.
     *
     * @param startDate                          the start date
     * @param scheduledReindexationConfiguration the scheduled reindexation configuration
     * @return true, if is still within configured time period
     */
    private boolean isStillWithinConfiguredTimePeriod(final Date startDate,
                                                      final ScheduledReindexation scheduledReindexationConfiguration) {
        boolean result = true;
        //  verify if current date/time is within configured duration
        final String duration = scheduledReindexationConfiguration.getDuration();
        final Date currentDate = new Date();
        //parse the duration - last character defines the type of period
        try {
            final int length = duration.length();
            final Integer amount = Integer.parseInt(duration.substring(0, length - 1));
            final String periodString = duration.substring(length - 1, length);
            if (periodString.equalsIgnoreCase("m")) {
                final Date evaluatedDate = DateUtils.addMinutes(startDate, amount);
                if (currentDate.after(evaluatedDate)) {
                    result = false;
                }
            } else if (periodString.equalsIgnoreCase("h")) {
                final Date evaluatedDate = DateUtils.addHours(startDate, amount);
                if (currentDate.after(evaluatedDate)) {
                    result = false;
                }
            } //else throw not necessary- should be validated on creation
        } catch (final Exception e) {
            LOG.error("Unexpected exception was thrown when trying to parse the configured duration.", e);
            throw e;
        }
        return result;
    }

    /**
     * Gets the last modification date lower limit.
     *
     * @param scheduledReindexationConfiguration the scheduled reindexation configuration
     * @return the last modification date lower limit
     */
    private Date getLastModificationDateLowerLimit(final ScheduledReindexation scheduledReindexationConfiguration) {
        Date result = new Date();
        //  get the lower limit for the selection based on a period
        final String period = scheduledReindexationConfiguration.getPeriod();
        //parse the period - last character defines the type of period
        try {
            final int length = period.length();
            final Integer amount = Integer.parseInt(period.substring(0, length - 1));
            final String periodString = period.substring(length - 1, length);
            if (periodString.equalsIgnoreCase("m")) {
                result = DateUtils.addMonths(result, amount * -1);
            } else if (periodString.equalsIgnoreCase("w")) {
                result = DateUtils.addWeeks(result, amount * -1);
            } else if (periodString.equalsIgnoreCase("d")) {
                result = DateUtils.addDays(result, amount * -1);
            } //else throw not necessary- should be validated on creation
        } catch (final Exception e) {
            LOG.error("Unexpected exception was thrown when trying to parse the configured period.", e);
            throw e;
        }
        return result;
    }

}
