package eu.europa.ec.opoce.cellar.nal.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.apache.commons.lang.StringUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration;
import eu.europa.ec.opoce.cellar.cmr.database.dao.NalConfigDao;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.NalOntoUtils;
import eu.europa.ec.opoce.cellar.nal.dao.NalConfig;
import eu.europa.ec.opoce.cellar.nal.domain.Concept;
import eu.europa.ec.opoce.cellar.nal.domain.ConceptScheme;
import eu.europa.ec.opoce.cellar.nal.domain.Domain;
import eu.europa.ec.opoce.cellar.nal.domain.Language;
import eu.europa.ec.opoce.cellar.nal.service.JenaGatewayNalApi;
import eu.europa.ec.opoce.cellar.nal.service.NalApi;
import eu.europa.ec.opoce.cellar.nal.service.NalConfigurationService;

@Service
public class EurovocApiService implements NalApi {

    private static final Logger LOG = LogManager.getLogger(EurovocApiService.class);

    private JenaGatewayNalApi jenaGatewayNalApi;
    private NalConfigurationService nalConfigurationService;
    private NalConfigDao nalConfigDao;

    @Autowired
    public EurovocApiService(JenaGatewayNalApi jenaGatewayNalApi, NalConfigurationService nalConfigurationService, NalConfigDao nalConfigDao) {
        this.jenaGatewayNalApi = jenaGatewayNalApi;
        this.nalConfigurationService = nalConfigurationService;
        this.nalConfigDao = nalConfigDao;
    }

    /**
     * {@inheritDoc}
     * <p/>
     * Get the translation of a given concept into a specified language.
     */
    @Override
    public Concept getConcept(final String concept, final String language) {
        if (StringUtils.isBlank(concept)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument concept cannot be blank!").build();
        }
        if (StringUtils.isBlank(language)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument language cannot be blank!").build();
        }

        String nalName = getNalNameFromUri(concept);
        Model nalModel = nalConfigurationService.getNalModel(NalOntoUtils.getBaseUri(concept), true);

        LOG.debug(IConfiguration.NAL, "Retrieving '{}' from '{}' for language '{}'.", concept, nalName, language);
        return this.jenaGatewayNalApi.getConcept(nalModel, concept, language);
    }

    /**
     * {@inheritDoc}
     * <p/>
     * Returns a list of concepts having a specific semantic relation with the given concept.
     * <p/>
     * Note: with URI %escaping skos:broader becomes http://www.w3.org/2004/02/skos/core%23broader
     */
    @Override
    public Concept[] getConceptRelatives(final String concept, final String relation, final String language) {
        if (StringUtils.isBlank(concept)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument concept cannot be blank!").build();
        }
        if (StringUtils.isBlank(relation)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument relation cannot be blank!").build();
        }
        if (StringUtils.isBlank(language)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument language cannot be blank!").build();
        }

        Model nalModel = nalConfigurationService.getNalModel(NalOntoUtils.getBaseUri(concept), true);

        LOG.debug(IConfiguration.NAL, "Retrieving concepts relations for concept '{}', relation '{}' and language '{}'.", concept, relation, language);
        return jenaGatewayNalApi.getRelatedConceptsOneQuery(nalModel, concept, relation, language);
    }

    /**
     * {@inheritDoc}
     * <p/>
     * Returns the conceptScheme. Method is used to ask for a specific skos:ConceptScheme.
     * (the EUROVOC concept scheme or one of the MicroThesaurus Concept scheme's)
     * By default the EUROVOC conceptScheme is returned.
     */
    @Override
    public ConceptScheme getConceptScheme(String conceptScheme) {
        if (StringUtils.isBlank(conceptScheme)) {
            conceptScheme = NalOntoUtils.EUROVOC_CONCEPTSCHEME_URI;
        }
        String nalName = getNalNameFromUri(conceptScheme);
        Model nalModel = nalConfigurationService.getNalModel(NalOntoUtils.getBaseUri(conceptScheme), true);
        LOG.debug(IConfiguration.NAL,"Retrieving concept scheme '{}' from NAL '{}'.", conceptScheme, nalName);
        return this.jenaGatewayNalApi.getConceptScheme(nalModel, conceptScheme);
    }

    private String getNalNameFromUri(String conceptScheme) {
        String baseUri = NalOntoUtils.getBaseUri(conceptScheme);
        NalConfig nalConfig = nalConfigDao.findByUri(baseUri)
                .orElseThrow(() -> ExceptionBuilder.get(CellarException.class)
                                    .withMessage("The concept scheme '{}' could not be found in the database")
                                    .withMessageArgs(baseUri).build());
        return nalConfig.getName();
    }

    /**
     * {@inheritDoc}
     * <p/>
     * Returns all conceptScheme's.(EUROVOC concept scheme and all MicroThesaurus Concept scheme's)
     */
    // FIXME : caching mechanism needed since we retrieve all NAL from S3
    @Override
    public ConceptScheme[] getConceptSchemes(final Date if_modified_since) {
        Model allNalModel = this.nalConfigurationService.getAllNalModel(true, NalOntoUtils.EUROVOC_ONTOLOGY_URI);
        LOG.debug(IConfiguration.NAL,"Retrieving all concept scheme from NALs configured in the system.");
        return this.jenaGatewayNalApi.getConceptSchemesOneQuery(allNalModel, if_modified_since);
    }

    /**
     * {@inheritDoc}
     * <p/>
     * Get The Domains facets of the EUROVOC thesaurus
     */
    @Override
    public Domain[] getDomains() {
        Model allNalModel = this.nalConfigurationService.getAllNalModel(true, NalOntoUtils.EUROVOC_ONTOLOGY_URI);
        LOG.debug(IConfiguration.NAL,"Retrieving all domains from NALs configured in the system.");
        return this.jenaGatewayNalApi.getDomainsOneQuery(allNalModel);
    }

    /**
     * Returns all the languages in which the EUROVOC concept scheme is currently available.
     *
     * @return an array of {@link Language} objects.
     */
    public Language[] getSupportedLanguages() {
        Model allNalModel = this.nalConfigurationService.getAllNalModel(true, NalOntoUtils.EUROVOC_ONTOLOGY_URI);
        LOG.debug(IConfiguration.NAL,"Retrieving all languages from NALs configured in the system for concept scheme '{}'.",
                NalOntoUtils.EUROVOC_CONCEPTSCHEME_URI);
        return this.jenaGatewayNalApi.getSupportedLanguages(allNalModel, NalOntoUtils.EUROVOC_CONCEPTSCHEME_URI);
    }

    /**
     * {@inheritDoc}
     * <p/>
     * Returns all the languages in which the EUROVOC concept scheme is currently available.
     */
    @Override
    public Language[] getSupportedLanguages(final String conceptScheme) {
        return this.getSupportedLanguages();
    }

    /**
     * {@inheritDoc}
     * <p/>
     * Get the top concepts of the conceptScheme
     */
    @Override
    public Concept[] getTopConcepts(final String conceptScheme, final String language) {
        if (StringUtils.isBlank(conceptScheme)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument conceptScheme cannot be blank!").build();
        }
        if (StringUtils.isBlank(language)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument language cannot be blank!").build();
        }

        Model nalModel = nalConfigurationService.getNalModel(NalOntoUtils.getBaseUri(conceptScheme), true);
        LOG.debug(IConfiguration.NAL,"Retrieving top concepts for concept scheme '{}' and language '{}'.", conceptScheme, language);
        return this.jenaGatewayNalApi.getTopConcepts(nalModel, conceptScheme, language);
    }
}
