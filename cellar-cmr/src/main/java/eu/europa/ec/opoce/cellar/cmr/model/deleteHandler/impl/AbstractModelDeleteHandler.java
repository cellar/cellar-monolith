/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.model.deleteHandler.impl
 *             FILE : AbstractModelDeleteHandler.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 15, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$$
 *          VERSION : $LastChangedRevision$$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.model.deleteHandler.impl;

import eu.europa.ec.opoce.cellar.cmr.model.deleteHandler.IModelDeleteHandler;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.SimpleSelector;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;

/**
 * <class_description> 
 *  Common abstract implementation to handle the out-dated blank nodes
 * <br/><br/>
 * <br/><br/>
 * ON : Oct 15, 2015.
 *
 * @author ARHS Developments
 * @version $Revision$$
 * @param <T> the generic type
 */
public abstract class AbstractModelDeleteHandler<T> implements IModelDeleteHandler<T> {

    /**
     * Handle blank nodes.
     * Deletes the blank nodes that contain the subjects that match the delete model statements
     *
     * @param myInputModel the my input model
     * @param deleteModel the delete model
     */
    protected void handleBlankNodes(final Model myInputModel, final Model deleteModel) {
        //list of subjects to delete
        final HashSet<Resource> subjects_to_be_deleted = new HashSet<Resource>();
        //each statement must return a subject that match all three arguments to be selected for detetion
        final StmtIterator listDeleteStatements = deleteModel.listStatements();
        try {
            while (listDeleteStatements.hasNext()) {
                final Statement deleteStatement = listDeleteStatements.next();
                final Resource subject = deleteStatement.getSubject();
                final Property predicate = deleteStatement.getPredicate();
                final RDFNode object = deleteStatement.getObject();
                if (!subject.isAnon()) {
                    final BlankNodeSelector blankNodeSelector = new BlankNodeSelector(subject, predicate, object);
                    final StmtIterator statements_aux = myInputModel.listStatements(blankNodeSelector);
                    try {
                        while (statements_aux.hasNext()) {
                            final Statement statement_aux = statements_aux.next();
                            final Resource subject_aux = statement_aux.getSubject();
                            if (subject_aux.isAnon()) {
                                subjects_to_be_deleted.add(subject_aux);
                            }
                        }
                    } finally {
                        statements_aux.close();
                    }
                }
            }
        } finally {
            listDeleteStatements.close();
        }
        //delete all for which the subject matched all 3 conditions
        for (final Resource subject_aux : subjects_to_be_deleted) {
            final Property nullProperty = null;
            final RDFNode nullRdfNode = null;
            final StmtIterator listStatements = myInputModel.listStatements(subject_aux, nullProperty, nullRdfNode);
            try {
                myInputModel.remove(listStatements);
            } finally {
                listStatements.close();
            }
        }
    }

    /**
     * <class_description> A meaningful description of the class that will be
     *                     displayed as the class summary on the JavaDoc package page.
     * <br/><br/>
     * <notes> Includes guaranteed invariants, usage instructions and/or examples,
     *         reminders about desired improvements, etc.
     * <br/><br/>
     * ON : Oct 15, 2015
     *
     * @author ARHS Developments
     * @version $Revision$$
     */
    class BlankNodeSelector extends SimpleSelector {

        /** The owl_annotated source p. */
        private final Property owl_annotatedSourceP = CellarProperty.owl_annotatedSourceP;

        /** The owl_annotated target p. */
        private final Property owl_annotatedTargetP = CellarProperty.owl_annotatedTargetP;

        /** The owl_annotated property p. */
        private final Property owl_annotatedPropertyP = CellarProperty.owl_annotatedPropertyP;

        /** The subject to match. */
        private final Resource subjectToMatch;

        /** The predicate to match. */
        private final Property predicateToMatch;

        /** The object to match. */
        private final RDFNode objectToMatch;

        /** The selection. */
        private final Map<Resource, Set<Property>> selection;

        /**
         * Instantiates a new blank node selector.
         *
         * @param subject the subject
         * @param predicate the predicate
         * @param object the object
         */
        public BlankNodeSelector(final Resource subject, final Property predicate, final RDFNode object) {
            super();
            this.subjectToMatch = subject;
            this.predicateToMatch = predicate;
            this.objectToMatch = object;
            this.selection = new HashMap<Resource, Set<Property>>();
        }

        /**
         * Populate.
         *
         * @param resource the resource
         * @param property the property
         */
        private void populate(final Resource resource, final Property property) {
            Set<Property> list = this.selection.get(resource);
            if (list == null) {
                list = new HashSet<Property>();
                this.selection.put(resource, list);
            }
            list.add(property);
        }

        /** {@inheritDoc} */
        @Override
        public boolean selects(final Statement s) {
            boolean result = false;
            final Resource subject = s.getSubject();
            if (subject.isAnon()) {
                final Property predicate = s.getPredicate();
                if (predicate.equals(this.owl_annotatedPropertyP) && s.getObject().equals(this.predicateToMatch)) {
                    this.populate(subject, this.owl_annotatedPropertyP);
                } else if (predicate.equals(this.owl_annotatedSourceP) && s.getObject().equals(this.subjectToMatch)) {
                    this.populate(subject, this.owl_annotatedSourceP);
                } else if (predicate.equals(this.owl_annotatedTargetP) && s.getObject().equals(this.objectToMatch)) {
                    this.populate(subject, this.owl_annotatedTargetP);
                }
                // the selector first purpose is to select the entire statement that obeys certain conditions 
                // here the important is to retrieve only the subject that matches all 3 conditions in different statements
                // the same subject that respects all 3 conditions 
                //so the current statement will "match" when the in memory object has 3 matches for the same subject
                final Set<Property> set = this.selection.get(subject);
                if ((set != null) && (set.size() == 3)) {
                    result = true;
                }
            }
            return result;
        }

    }

}
