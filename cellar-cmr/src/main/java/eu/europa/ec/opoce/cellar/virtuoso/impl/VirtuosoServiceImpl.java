/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.virtuoso.impl
 *             FILE : VirtuosoServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 08-06-2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.virtuoso.impl;

import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditBuilder;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventAction;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventProcess;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventType;
import eu.europa.ec.opoce.cellar.cl.domain.history.IdentifiersHistory;
import eu.europa.ec.opoce.cellar.cl.domain.history.dao.IdentifiersHistoryDao;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.SnippetExtractorService;
import eu.europa.ec.opoce.cellar.cmr.database.util.JdbcUtils;
import eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData;
import eu.europa.ec.opoce.cellar.cmr.model.ModelUtils;
import eu.europa.ec.opoce.cellar.cmr.sparql.utils.QueryUtils;
import eu.europa.ec.opoce.cellar.cmr.transformers.DigitalObject2Context;
import eu.europa.ec.opoce.cellar.common.util.Pair;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.ExceptionUtils;
import eu.europa.ec.opoce.cellar.exception.VirtuosoOperationException;
import eu.europa.ec.opoce.cellar.logging.LogContext;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.server.admin.sparql.VirtuosoInformation;
import eu.europa.ec.opoce.cellar.virtuoso.VirtuosoService;
import eu.europa.ec.opoce.cellar.virtuoso.retry.annotation.VirtuosoRetry;
import eu.europa.ec.opoce.cellar.virtuoso.util.*;
import eu.europa.ec.opoce.cellar.virtuosobackup.BackupType;
import eu.europa.ec.opoce.cellar.virtuosobackup.VirtuosoBackup.OperationType;
import eu.europa.ec.opoce.cellar.virtuosobackup.service.IVirtuosoBackupService;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;
import virtuoso.jdbc4.Driver;
import virtuoso.jena.driver.VirtDataset;
import virtuoso.jena.driver.VirtuosoUpdateFactory;
import virtuoso.jena.driver.VirtuosoUpdateRequest;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.Map.Entry;
import java.util.regex.Pattern;

import static eu.europa.ec.opoce.cellar.AppConstants.NEWLINE;
import static eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration.VIRTUOSO;
import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.CMR_VIRTUOSO;
import static org.springframework.jdbc.support.JdbcUtils.closeConnection;
import static org.springframework.jdbc.support.JdbcUtils.closeStatement;

/**
 * <class_description> This is the main implementation of the Virtuoso service.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 08-06-2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class VirtuosoServiceImpl implements VirtuosoService {

    private static final Logger LOG = LogManager.getLogger(VirtuosoServiceImpl.class);

    private static final String SERVER_VERSION_QUERY = "SPARQL SELECT ( bif:sys_stat('st_dbms_ver') AS ?version ) " +
            "WHERE  {  ?s  ?p  ?o  } LIMIT 1";

    /**
     * The virtuoso data source.
     */
    @Qualifier("virtuosoDataSource")
    @Autowired
    private DataSource virtuosoDataSource;

    @Autowired
    protected SnippetExtractorService snippetExtractorService;

    @Autowired
    @Qualifier("pidManagerService")
    protected IdentifierService identifierService;

    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    @Autowired
    private IVirtuosoBackupService virtuosoBackupService;

    @Autowired
    private IdentifiersHistoryDao identifiersHistoryDao;

    @Autowired
    @Qualifier("linkRotAlignmentTemplatesStringsMap")
    private HashMap<String, String> linkRotAlignmentTemplatesStringsMap;

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    /**
     * The Constant NEW_CELLAR_URI_REGEX.
     */
    private static final Pattern NEW_CELLAR_URI_REGEX = Pattern.compile("\\$\\{NEW_CELLAR_URI\\}", Pattern.DOTALL);
    private static final Pattern OLD_CELLAR_URI_REGEX = Pattern.compile("\\$\\{OLD_CELLAR_URI\\}", Pattern.DOTALL);
    private static final String PRIVATE_GROUP_COUNT_QUERY = "SELECT COUNT(id_to_iri(RGGM_MEMBER_IID)) "
            + "FROM DB.DBA.RDF_GRAPH_GROUP_MEMBER "
            + "WHERE RGGM_GROUP_IID = iri_to_id ('http://www.openlinksw.com/schemas/virtrdf#PrivateGraphs') "
            + "AND  RGGM_MEMBER_IID = iri_to_id ('%s')";
    private static final String EMBARGOED_OBJECT_QUERY = "SPARQL PREFIX cdm: <http://publications.europa.eu/ontology/cdm#>"
            + "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> "
            + "SELECT DISTINCT ?cellarURI ?embargo from <http://www.openlinksw.com/schemas/virtrdf#PrivateGraphs> "
            + "WHERE { ?cellarURI rdf:type cdm:work . OPTIONAL {?cellarURI cdm:work_embargo ?embargo .} FILTER(?embargo < now()) . }";

    @Override
    public Model read(final String cellarUri , VirtDataset dataset) {
        return dataset.getNamedModel(cellarUri);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @LogContext(CMR_VIRTUOSO)
    @VirtuosoRetry
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void writeNal(final Model model, String modelUri, boolean hasRetryAttempts) throws VirtuosoOperationException {
        VirtDataset dataset = null;

        try {
            dataset = new VirtDataset(virtuosoDataSource);
            VirtuosoUtils.updateModel(dataset, modelUri, model);

            if (cellarConfiguration.isVirtuosoIngestionBackupLogVerbose()) {
                LOG.info("NAL '{}' inserted.", modelUri);
            }
        } catch (final Exception e) {
        	triggerVirtuosoRetry(e, hasRetryAttempts);
        	BackupVirtuosoFailureBuilder.get(e)
        	.nalsToDrop(Collections.singleton(modelUri))
            .nalsToInsert(Collections.singletonMap(modelUri, model))
            .execute();
            throw ExceptionBuilder.get(VirtuosoOperationException.class)
                    .withCode(CmrErrors.VIRTUOSO_OPERATION_FAILED)
                    .withMessage("Unexpected exception thrown while trying to writeNal NAL {}")
                    .withCause(e)
                    .build();
        } finally {
            // we do not close the Model at this stage, as we still need it to generate the NAL snippets
            VirtuosoUtils.close(dataset);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @LogContext(CMR_VIRTUOSO)
    @VirtuosoRetry
    @Transactional(propagation = Propagation.REQUIRED)
    public void write(final Model model, String modelUri, boolean hasRetryAttempts) throws VirtuosoOperationException {
        VirtDataset dataset = null;
        try {
            dataset = new VirtDataset(virtuosoDataSource);
            //Testing purposes.Need for TC-CLLR10815 387 to work
            testRollback();
            VirtuosoUtils.updateModel(dataset, modelUri, model);

            if (cellarConfiguration.isVirtuosoIngestionBackupLogVerbose()) {
                LOG.info("Model '{}' inserted.", modelUri);
            }
        } catch (final Exception e) {
        	triggerVirtuosoRetry(e, hasRetryAttempts);
        	BackupVirtuosoFailureBuilder.get(e)
        	.sparqlLoadModelToDrop(Collections.singleton(modelUri))
            .sparqlLoadModelToInsert(Collections.singletonMap(modelUri, model))
            .execute();
            throw ExceptionBuilder.get(VirtuosoOperationException.class)
                    .withCode(CmrErrors.VIRTUOSO_OPERATION_FAILED)
                    .withMessage("Unexpected exception thrown while trying to write model {}")
                    .withCause(e)
                    .build();
        } finally {
            VirtuosoUtils.close(dataset);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @LogContext(CMR_VIRTUOSO)
    @VirtuosoRetry
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void writeOnto(final Model model, String modelUri, boolean hasRetryAttempts) throws VirtuosoOperationException {
        VirtDataset dataset = null;

        try {
            //get connection to the dataset
            dataset = new VirtDataset(virtuosoDataSource);
            VirtuosoUtils.updateModel(dataset, modelUri, model);

            if (cellarConfiguration.isVirtuosoIngestionBackupLogVerbose()) {
                LOG.info("Ontology '{}' inserted.", modelUri);
            }
        } catch (final Exception e) {
        	triggerVirtuosoRetry(e, hasRetryAttempts);
        	BackupVirtuosoFailureBuilder.get(e)
        	.ontologiesToDrop(Collections.singleton(modelUri))
            .ontologiesToInsert(Collections.singletonMap(modelUri, model))
            .execute();
            throw ExceptionBuilder.get(VirtuosoOperationException.class)
                    .withCode(CmrErrors.VIRTUOSO_OPERATION_FAILED)
                    .withMessage("Unexpected exception thrown while trying to write Ontology {}")
                    .withCause(e)
                    .build();
        } finally {
            // caller method is responsible for closing the model
            VirtuosoUtils.close(dataset);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @LogContext(CMR_VIRTUOSO)
    @VirtuosoRetry
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = VirtuosoOperationException.class)
    public Set<String> write(final CalculatedData calculatedData, boolean hasRetryAttempts) throws VirtuosoOperationException {
        LOG.debug(VIRTUOSO, "Starting to write in Virtuoso {}", calculatedData.getRootCellarId());
        eventPublisher.publishEvent(new VirtuosoWriteEvent(calculatedData));
        final long currentTimeMillis = System.currentTimeMillis();
        final Set<String> result = new HashSet<>();
        final boolean isLogVerbose = cellarConfiguration.isVirtuosoIngestionBackupLogVerbose();
        Collection<String> cellarIdsToDrop = null;
        Map<DigitalObject, Model> metadataToInsert = null;
        VirtDataset dataset = null;
        VirtuosoNormalizer virtuosoNormalizer = null;
        VirtuosoSkolemizer virtuosoSkolemizer;

        try {
            //when dropping named graphs Virtuoso does not delete possible entries on the private group, this has to be done separately
            final Collection<DigitalObject> removedDigitalObjects = calculatedData.getRemovedDigitalObjects();
            final Transformer<DigitalObject, String> instance = DigitalObject2Context.instance;
            final HashSet<String> hashSet = new HashSet<>();
            // collects data
            cellarIdsToDrop = CollectionUtils.collect(removedDigitalObjects, instance, hashSet);
            ungroup(cellarIdsToDrop);

            metadataToInsert = getMetadata(calculatedData);

            // instantiates a Virtuoso normalizer for the metadata to ingest
            if (LOG.isDebugEnabled(VIRTUOSO) || LOG.isTraceEnabled(VIRTUOSO)) {
                LOG.debug(VIRTUOSO, "Direct metadata to insert {}", toStringMetadata(metadataToInsert));
            }
            virtuosoNormalizer = new VirtuosoNormalizer(metadataToInsert);
            virtuosoSkolemizer = new VirtuosoSkolemizer();
            if (isLogVerbose) {
                LOG.info("The metadata normalization is {}.",
                        virtuosoNormalizer.isNormalizationEnabled() ? "enabled" : "NOT enabled");
                LOG.info("The skolemization is {}.", virtuosoSkolemizer.isSkolemizationEnabled() ? "enabled" : "NOT enabled");
            }

            // gets the connection to the dataset
            dataset = new VirtDataset(virtuosoDataSource);
            if (LOG.isDebugEnabled(VIRTUOSO)) {
                LOG.debug(VIRTUOSO, "Virtuoso dataset initialized (empty)");
            }

            // drops models that are supposed to be directly deleted
            internalDropDigitalObjects(cellarIdsToDrop, dataset, false);

            // write models
            final Set<String> internalWriteDigitalObjects = this
                    .internalWriteDigitalObjects(metadataToInsert, dataset, virtuosoNormalizer, virtuosoSkolemizer,calculatedData);

            result.addAll(internalWriteDigitalObjects);
            
            LOG.debug(VIRTUOSO, "Internal write done. Virtuoso dataset (triples count: {})", dataset.getCount());

            testRollback(); //testing purposes

            //Log the end of process
            AuditBuilder.get(AuditTrailEventProcess.Ingestion).withAction(AuditTrailEventAction.VirtuosoWrite)
                    .withType(AuditTrailEventType.End)
                    .withDuration(System.currentTimeMillis() - currentTimeMillis)
                    .withStructMapStatusHistory(calculatedData.getStructMapStatusHistory())
                    .withPackageHistory(calculatedData.getPackageHistory())
                    .withMessage("Finished writing in Virtuoso after {} ms.")
                    .withMessageArgs(System.currentTimeMillis() - currentTimeMillis)
                    .withLogger(LOG).withLogDatabase(false).logEvent();

            testVirtuosoRetry(); //testing purposes
        } catch (final Exception e) {
            LOG.debug(VIRTUOSO, "Failed to write in Virtuoso", e);
            triggerVirtuosoRetry(e, hasRetryAttempts);
            BackupVirtuosoFailureBuilder.get(e).digitalObjectsToDrop(cellarIdsToDrop)
            .digitalObjectsToInsert(metadataToInsert, virtuosoNormalizer).execute();
        } finally {
            ModelUtils.closeQuietly(metadataToInsert);
            LOG.debug(VIRTUOSO, "Close Virtuoso dataset (triples count={})", (dataset != null ? dataset.getCount() : 0));
            VirtuosoUtils.close(dataset);
            final long delta = System.currentTimeMillis() - currentTimeMillis;
            if (isLogVerbose) {
                LOG.info("Ingestion in Virtuoso finished after {} ms.", delta);
            }
        }

        return result;
    }

    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
    public void afterCommit(VirtuosoWriteEvent event) {
        LOG.debug(VIRTUOSO, "Transaction committed for {}", event);
    }

    private static String toStringMetadata(Map<DigitalObject, Model> metadata) {
        StringBuilder sb = new StringBuilder();
        sb.append(NEWLINE);
        for (Entry<DigitalObject, Model> e : metadata.entrySet()) {
            sb.append(e.getKey()).append(" -> triples count :").append(e.getValue().size());
            if (LOG.isTraceEnabled(VIRTUOSO)) {
                sb.append("\tModel:").append(JenaUtils.toString(e.getValue()));
            }
            sb.append(NEWLINE);
        }
        return sb.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @VirtuosoRetry
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = VirtuosoOperationException.class)
    public void drop(final Collection<String> cellarIds, boolean withVirtuosoBackup, boolean hasRetryAttempts) throws VirtuosoOperationException {
        Dataset dataset = null;
        try {
            // gets the connection to the dataset
            dataset = new VirtDataset(this.virtuosoDataSource);

            // drop the graphs
            internalDropDigitalObjects(cellarIds, dataset, !withVirtuosoBackup);

            // when dropping named graphs Virtuoso does not delete possible entries on the private group, this has to be done separately
            ungroup(cellarIds);

            // testing purposes
            testRollback();
        } catch (final Exception e) {
        	triggerVirtuosoRetry(e, hasRetryAttempts);
            if(withVirtuosoBackup) {
                BackupVirtuosoFailureBuilder.get(e).digitalObjectsToDrop(cellarIds).execute();
            } else {
                LOG.error(VIRTUOSO, "Failed to drop graphs from Virtuoso");
                // throws the exception wrapped in a VirtuosoOperationException, in order to trigger the rollback on Virtuoso
                throw ExceptionBuilder.get(VirtuosoOperationException.class).withCode(CmrErrors.VIRTUOSO_OPERATION_FAILED)
                        .withMessage("Failed to drop graphs from Virtuoso").withCause(e).build();
            }
        } finally {
            VirtuosoUtils.close(dataset);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @VirtuosoRetry
    @Transactional(propagation = Propagation.REQUIRED)
    public void hide(final String cellarId, final Date embargoDate, final Date lastModificationDate, boolean hasRetryAttempts) throws VirtuosoOperationException {
        internalHide(cellarId, embargoDate, lastModificationDate, hasRetryAttempts);
    }

    @Override
    @VirtuosoRetry
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void hideInNewTransaction(final String cellarId, final Date embargoDate, final Date lastModificationDate, boolean hasRetryAttempts)
            throws VirtuosoOperationException {
        internalHide(cellarId, embargoDate, lastModificationDate, hasRetryAttempts);
    }

    private void internalHide(final String cellarId, final Date embargoDate, final Date lastModificationDate, boolean hasRetryAttempts) {
        try {
            VirtuosoUtils.groupAction(cellarId, PrivateGroupOperation.INSERT, this.virtuosoDataSource, embargoDate, lastModificationDate);
        } catch (final Exception e) {
            triggerVirtuosoRetry(e, hasRetryAttempts);
            throw ExceptionBuilder.get(VirtuosoOperationException.class)
                    .withCode(CmrErrors.VIRTUOSO_OPERATION_FAILED)
                    .withMessage("Unexpected exception thrown while trying to hide work with cellar id {} and embargo date {}")
                    .withMessageArgs(cellarId, embargoDate)
                    .withCause(e)
                    .build();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @VirtuosoRetry
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void unhide(final String cellarId, final Date embargoDate, final Date lastModificationDate, boolean hasRetryAttempts) throws VirtuosoOperationException {
        try {
            VirtuosoUtils.groupAction(cellarId, PrivateGroupOperation.REMOVE, this.virtuosoDataSource, embargoDate, lastModificationDate);
        } catch (final Exception e) {
            triggerVirtuosoRetry(e, hasRetryAttempts);
            throw ExceptionBuilder.get(VirtuosoOperationException.class)
                    .withCode(CmrErrors.VIRTUOSO_OPERATION_FAILED)
                    .withMessage("Unexpected exception thrown while trying to unhide work with cellar id {} and embargo date {}")
                    .withMessageArgs(cellarId, embargoDate)
                    .withCause(e)
                    .build();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @VirtuosoRetry
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void unhide(final Collection<String> cellarIds, final Date embargoDate, final Date lastModificationDate, boolean hasRetryAttempts)
            throws VirtuosoOperationException {
        try {
            VirtuosoUtils.groupAction(cellarIds, PrivateGroupOperation.REMOVE, this.virtuosoDataSource, embargoDate, lastModificationDate);
        } catch (final Exception e) {
            triggerVirtuosoRetry(e, hasRetryAttempts);
            throw ExceptionBuilder.get(VirtuosoOperationException.class)
                    .withCode(CmrErrors.VIRTUOSO_OPERATION_FAILED)
                    .withMessage("Unexpected exception thrown while trying to unhide resources with cellar id {} and embargo date {}")
                    .withMessageArgs(Arrays.toString(cellarIds.toArray()), embargoDate)
                    .withCause(e)
                    .build();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @VirtuosoRetry
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void hide(final Set<String> cellarIds, boolean hasRetryAttempts) throws VirtuosoOperationException {
        try {
            LOG.debug(VIRTUOSO, "Embargoing graphs in Virtuoso: {}", cellarIds);
            VirtuosoUtils.group(cellarIds, PrivateGroupOperation.INSERT, this.virtuosoDataSource);
            LOG.debug(VIRTUOSO, "Embargoed {} graphs in Virtuoso: {}", cellarIds.size(), cellarIds);
        } catch (final Exception e) {
            triggerVirtuosoRetry(e, hasRetryAttempts);
            LOG.error("Failed to embargo graphs in Virtuoso: ", e);
            throw ExceptionBuilder.get(VirtuosoOperationException.class)
                    .withCode(CmrErrors.VIRTUOSO_OPERATION_FAILED)
                    .withMessage("Unexpected exception thrown while trying to hide works with cellar ids {}")
                    .withMessageArgs(cellarIds)
                    .withCause(e)
                    .build();
        }
    }

    /**
     * unhide.
     *
     * @param cellarIds the cellar ids
     * @throws VirtuosoOperationException the virtuoso operation exception
     * @see eu.europa.ec.opoce.cellar.virtuoso.VirtuosoService#hide(java.util.Set, boolean)
     */
    @Override
    @VirtuosoRetry
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void unhide(final Set<String> cellarIds, boolean hasRetryAttempts) throws VirtuosoOperationException {
        try {
            LOG.debug(VIRTUOSO, "Disembargoing graphs in Virtuoso: {}", cellarIds);
            VirtuosoUtils.group(cellarIds, PrivateGroupOperation.REMOVE, this.virtuosoDataSource);
            LOG.debug(VIRTUOSO, "Disembargoed {} graphs in Virtuoso: {}", cellarIds.size(), cellarIds);
        } catch (final Exception e) {
            triggerVirtuosoRetry(e, hasRetryAttempts);
            LOG.error("Failed to disembargo graphs in Virtuoso: ", e);
            throw ExceptionBuilder.get(VirtuosoOperationException.class)
                    .withCode(CmrErrors.VIRTUOSO_OPERATION_FAILED)
                    .withMessage("Unexpected exception thrown while trying to hide works with cellar ids {}")
                    .withMessageArgs(cellarIds)
                    .withCause(e)
                    .build();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
    public boolean isUnderEmbargo(final String cellarId) throws VirtuosoOperationException {
        boolean result;
        try {
            final String graphName = this.identifierService.getUri(cellarId);
            final String query = String.format(PRIVATE_GROUP_COUNT_QUERY, graphName);
            try {
                LOG.debug(VIRTUOSO, "Executing Virtuoso query {}", query);
                final int count = JdbcUtils.executeCount(this.virtuosoDataSource, query);
                result = count > 0;
            } catch (Exception e) {
                LOG.error("Error executing query {}", query);
                throw e;
            }
        } catch (final Exception e) {
            throw ExceptionBuilder.get(VirtuosoOperationException.class)
                    .withCode(CmrErrors.VIRTUOSO_OPERATION_FAILED)
                    .withMessage("Unexpected exception thrown while trying to know if {} is under embargo")
                    .withMessageArgs(cellarId)
                    .withCause(e)
                    .build();
        }

        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
    public List<Pair<String, Date>> getOutdatedEmbargoedWorks() throws VirtuosoOperationException {
        final List<Pair<String, Date>> result = new ArrayList<>();
        Connection connection = null;
        Statement preparedStatement = null;
        try {
            connection = this.virtuosoDataSource.getConnection();
            preparedStatement = connection.createStatement();
            try (final ResultSet rs = preparedStatement.executeQuery(EMBARGOED_OBJECT_QUERY)) {
                while (rs.next()) {
                    final Pair<String, Date> pair = new Pair<>(rs.getString(1), rs.getDate(2));
                    result.add(pair);
                }
            }

        } catch (final Exception e) {
            throw ExceptionBuilder.get(VirtuosoOperationException.class)
                    .withCode(CmrErrors.VIRTUOSO_OPERATION_FAILED)
                    .withMessage("Unexpected exception thrown while trying to get outdated embargoed works")
                    .withCause(e)
                    .build();
        } finally {
            closeStatement(preparedStatement);
            closeConnection(connection);
        }
        return result;
    }

    /**
     * Internal write.
     *
     * @param directMetadataToInsert the direct metadata to insert or update
     * @param dataset                the dataset
     * @param virtuosoNormalizer     the virtuoso normalizer
     * @param virtuosoSkolemizer     the virtuoso skolemizer
     * @return the sets the
     * @throws Exception the exception
     */
    private Set<String> internalWriteDigitalObjects(final Map<DigitalObject, Model> directMetadataToInsert, final VirtDataset dataset,
                                                    final VirtuosoNormalizer virtuosoNormalizer, final VirtuosoSkolemizer virtuosoSkolemizer,
                                                    final CalculatedData calculatedData) throws Exception {
        //Log the start of process
        AuditBuilder.get(AuditTrailEventProcess.Ingestion).withAction(AuditTrailEventAction.VirtuosoWrite)
                .withType(AuditTrailEventType.Start)
                .withStructMapStatusHistory(calculatedData.getStructMapStatusHistory())
                .withPackageHistory(calculatedData.getPackageHistory())
                .withMessage("")
                .withLogger(LOG).withLogDatabase(false).logEvent();

        final Set<String> result = new HashSet<>();

        // get direct model of digital objects that are supposed to be inserted
        final Set<DigitalObject> keySet = directMetadataToInsert.keySet();
        final Transformer<DigitalObject, String> instance = DigitalObject2Context.instance;
        final Collection<String> cellarIdsToInsert = CollectionUtils.collect(keySet, instance, new HashSet<>());
        LOG.debug(VIRTUOSO, "Dataset (triples count: {}), inserting models '{}'", dataset.getCount(), cellarIdsToInsert);
        final boolean isLogVerbose = this.cellarConfiguration.isVirtuosoIngestionBackupLogVerbose();
        if ((!cellarIdsToInsert.isEmpty()) && isLogVerbose) {
            LOG.info("Inserting models '{}'...", cellarIdsToInsert);
        }
        final boolean virtuosoIngestionLinkrotRealignmentEnabled = this.cellarConfiguration.isVirtuosoIngestionLinkrotRealignmentEnabled();
        final Set<Entry<DigitalObject, Model>> entrySet = directMetadataToInsert.entrySet();
        for (final Entry<DigitalObject, Model> currEntry : entrySet) {
            final DigitalObject digitalObject = currEntry.getKey();
            final ContentIdentifier contentIdentifier = digitalObject.getCellarId();
            final String cellarId = contentIdentifier.getIdentifier();
            final Model nonNormalizedNonSkolemizedModel = currEntry.getValue();

            Model normalizedSkolemizedModel = JenaUtils.create(nonNormalizedNonSkolemizedModel);
            LOG.debug(VIRTUOSO, "Non normalized skolemized model (triples count: {})", normalizedSkolemizedModel.size());
            virtuosoNormalizer.normalizeModel(normalizedSkolemizedModel, digitalObject);
            LOG.debug(VIRTUOSO, "Normalized skolemized model (triples count: {})", normalizedSkolemizedModel.size());

            final String existingCellarUri = this.identifierService.getUri(cellarId);
            normalizedSkolemizedModel = virtuosoSkolemizer.skolemizeModel(normalizedSkolemizedModel, existingCellarUri);

            //TODO this should be done in caller class
            // Virtuoso backup enabled
            final boolean virtuosoIngestionBackupEnabled = this.cellarConfiguration.isVirtuosoIngestionBackupEnabled();
            boolean alreadyBackedup = false;
            if (virtuosoIngestionBackupEnabled) {
                LOG.debug(VIRTUOSO, "Virtuoso ingestion backup id ENABLED.");
                final String nonNormalizedModelString = JenaUtils.toString(nonNormalizedNonSkolemizedModel);
                // if this is true, it means that a backup with same cellarId is already present in VIRTUOSO_BACKUP:
                // thus, it is again backed up, in order to preserve order when this cellarId will be processed to Virtuoso
                final Set<String> nonNormalizedUris = virtuosoNormalizer.getNonNormalizedUris(digitalObject);
                final Date insertionDate = new Date();
                // VIRTUOSO_BACKUP doesn't contain cellarId: ingestion on Virtuoso is possible
                alreadyBackedup = this.virtuosoBackupService.populateVirtuosoBackupIfAlreadyBackedup(cellarId, BackupType.DIGITAL_OBJECT,
                        nonNormalizedModelString, nonNormalizedUris, insertionDate, OperationType.INSERT);
            }
            if (!alreadyBackedup) {
                LOG.debug(VIRTUOSO, "Inserting model '{}', empty Virtuoso dataset", cellarId);
                VirtuosoUtils.updateModel(dataset, existingCellarUri, normalizedSkolemizedModel);
                virtuosoNormalizer.normalizeExistingMetadata(dataset, digitalObject);
                //realign link-rot
                if (virtuosoIngestionLinkrotRealignmentEnabled) {
                    LOG.debug(VIRTUOSO, "Linkrot realignment is ENABLED");
                    internalRealignment(dataset, digitalObject);
                }
                LOG.debug(VIRTUOSO, "Inserted model '{}'", cellarId);
            }
            result.add(cellarId);
        }
        LOG.debug(VIRTUOSO, "{} models inserted, Virtuoso dataset (triples count: {})", result.size(), dataset.getCount());

        if ((!cellarIdsToInsert.isEmpty()) && isLogVerbose) {
            LOG.info("Models '{}' inserted.", cellarIdsToInsert);
        }

        return result;
    }

    /**
     * Internal realignment.
     *
     * @param dataset       the dataset
     * @param digitalObject the digital object
     */
    private void internalRealignment(final Dataset dataset, final DigitalObject digitalObject) {
        final List<String> productionIds = new ArrayList<>();
        final List<ContentIdentifier> contentids = digitalObject.getContentids();
        for (final ContentIdentifier contentIdentifier : contentids) {
            productionIds.add(contentIdentifier.getIdentifier());
        }
        final ContentIdentifier doContentIdentifier = digitalObject.getCellarId();
        final String cellarId = doContentIdentifier.getIdentifier();
        realignment(dataset, cellarId, productionIds);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void realignment(final Dataset dataset, final String currentCellarID, final Collection<String> productionIds) {
        if (productionIds.isEmpty()) {
            return;
        }
        LOG.debug(VIRTUOSO, "Realignment of '{}'", currentCellarID);
        final Set<IdentifiersHistory> obsolete = new HashSet<>();
        final Set<IdentifiersHistory> obsoleteItemsTree = new HashSet<>();
        final List<IdentifiersHistory> deleted = this.identifiersHistoryDao.getDeleted(productionIds);//all deleted
        if (deleted.isEmpty()) {
            return;
        }
        for (final String productionId : productionIds) {
            final List<IdentifiersHistory> deletedForThisPid = this.identifiersHistoryDao.getDeleted(productionId);
            final IdentifiersHistory latest = this.identifiersHistoryDao.getLatest(productionId);//there might be multiple pids
            //update and mark the ones different from the TOP one as obsolete
            for (final IdentifiersHistory identifiersHistory : deletedForThisPid) {
                if (!identifiersHistory.equals(latest)) {
                    final String cellarId = identifiersHistory.getCellarId();
                    final boolean isManifestation = DigitalObjectType.isManifestation(cellarId);
                    identifiersHistory.setObsolete(Boolean.TRUE);
                    this.identifiersHistoryDao.update(identifiersHistory);
                    obsolete.add(identifiersHistory);
                    if (isManifestation) {
                        final List<IdentifiersHistory> identifierHistoryTree = this.identifiersHistoryDao
                                .getIdentifierHistoryTree(productionId, cellarId);
                        obsoleteItemsTree.addAll(identifierHistoryTree);
                        obsoleteItemsTree.remove(identifiersHistory);
                    }
                }
            }
        }

        final String currentCellarURI = this.identifierService.getCellarUriOrNull(currentCellarID);//should not be null
        final String[] queryTemplateNames = new String[]{
                "realignObsoleteObjectsTemplate", "realignObsoleteSubjectsTemplate"};
        for (final IdentifiersHistory markedAsObsolete : obsolete) {
            final String cellarIdMarkedAsObsolete = markedAsObsolete.getCellarId();
            final String cellarIdMarkedAsObsoleteURI = this.identifierService.getCellarUriOrNull(cellarIdMarkedAsObsolete);
            if (!currentCellarURI.equalsIgnoreCase(cellarIdMarkedAsObsoleteURI)) {
                final Map<Pattern, Object> placeHoldersMap = new HashMap<>();
                placeHoldersMap.put(NEW_CELLAR_URI_REGEX, currentCellarURI);
                placeHoldersMap.put(OLD_CELLAR_URI_REGEX, cellarIdMarkedAsObsoleteURI);
                for (final String queryTemplateName : queryTemplateNames) {
                    final String queryString = QueryUtils.resolveString(queryTemplateName, this.linkRotAlignmentTemplatesStringsMap,
                            placeHoldersMap);
                    final VirtuosoUpdateRequest virtuosoUpdateRequest = VirtuosoUpdateFactory.create(queryString, dataset);
                    LOG.debug(VIRTUOSO, "Executing '{}'", queryString);
                    virtuosoUpdateRequest.exec();
                }
            }
        }
        for (final IdentifiersHistory markedAsObsolete : obsoleteItemsTree) {
            final String cellarIdMarkedAsObsolete = markedAsObsolete.getCellarId();
            final String cellarIdMarkedAsObsoleteURI = this.identifierService.getCellarUriOrNull(cellarIdMarkedAsObsolete);
            final IdentifiersHistory latest = this.identifiersHistoryDao.getLatest(markedAsObsolete.getProductionId());
            final String latestItemCellarId = latest.getCellarId();
            final String latestItemCellarURI = this.identifierService.getCellarUriOrNull(latestItemCellarId);
            if (!latestItemCellarURI.equalsIgnoreCase(cellarIdMarkedAsObsoleteURI)) {
                final Map<Pattern, Object> placeHoldersMap = new HashMap<>();
                placeHoldersMap.put(NEW_CELLAR_URI_REGEX, latestItemCellarURI);
                placeHoldersMap.put(OLD_CELLAR_URI_REGEX, cellarIdMarkedAsObsoleteURI);
                final String queryString = QueryUtils.resolveString("realignObsoleteObjectsTemplate",
                        this.linkRotAlignmentTemplatesStringsMap, placeHoldersMap);
                final VirtuosoUpdateRequest virtuosoUpdateRequest = VirtuosoUpdateFactory.create(queryString, dataset);
                LOG.debug(VIRTUOSO, "Executing '{}'", queryString);
                virtuosoUpdateRequest.exec();
            }
        }
        // handle items
        for (final IdentifiersHistory toBeMarkedAsObsolete : obsoleteItemsTree) {
            toBeMarkedAsObsolete.setObsolete(Boolean.TRUE);
            this.identifiersHistoryDao.update(toBeMarkedAsObsolete);
        }

        LOG.debug(VIRTUOSO, "Realignment done");
    }

    /**
     * Internal drop.
     *
     * @param cellarIds the cellar ids
     * @param dataset   the dataset
     * @param bypassVirtuosoBackup true if Virtuoso Backup mechanism should be bypassed
     * @throws Exception the exception
     */
    private void internalDropDigitalObjects(final Collection<String> cellarIds, final Dataset dataset, final boolean bypassVirtuosoBackup) throws Exception {
        long startTime = System.currentTimeMillis();
        // Virtuoso backup not enabled
        LOG.debug(VIRTUOSO, "Dropping {} models {} ", cellarIds.size(), cellarIds);
        final boolean virtuosoIngestionBackupEnabled = this.cellarConfiguration.isVirtuosoIngestionBackupEnabled();
        if (!virtuosoIngestionBackupEnabled || bypassVirtuosoBackup) {
            VirtuosoUtils.dropModels(dataset, cellarIds);
            LOG.debug(VIRTUOSO, "{} models dropped", cellarIds.size());
        }
        // Virtuoso backup enabled
        else {
            LOG.debug(VIRTUOSO, "Virtuoso backup ENABLED");
            final boolean isLogVerbose = this.cellarConfiguration.isVirtuosoIngestionBackupLogVerbose();
            if ((!cellarIds.isEmpty()) && isLogVerbose) {
                //Log the start of process
                AuditBuilder.get(AuditTrailEventProcess.Ingestion).withAction(AuditTrailEventAction.VirtuosoDelete)
                        .withType(AuditTrailEventType.Start)
                        .withMessage("Dropping models '{}'...")
                        .withMessageArgs(cellarIds)
                        .withLogger(LOG).withLogDatabase(false).logEvent();
            }
            for (final String cellarId : cellarIds) {
                final Date insertionDate = new Date();
                LOG.debug(VIRTUOSO, "Virtuoso backup ENABLED");
                // if this is true, it means that a backup with same cellarId is already present in VIRTUOSO_BACKUP:
                // thus, it is again backed up, in order to preserve the order when this cellarId will be processed to Virtuoso
                final boolean alreadyBackedup = this.virtuosoBackupService.populateVirtuosoBackupIfAlreadyBackedup(cellarId,
                        BackupType.DIGITAL_OBJECT, null, null, insertionDate, OperationType.DROP);
                // VIRTUOSO_BACKUP doesn't contain cellarId: ingestion on Virtuoso is possible
                if (!alreadyBackedup) {
                    VirtuosoUtils.dropModels(dataset, cellarId);
                } else {
                    LOG.debug(VIRTUOSO, "Virtuoso dataset backup for {} already done", cellarId);
                }
            }
            if ((!cellarIds.isEmpty()) && isLogVerbose) {
                LOG.info("Models '{}' dropped.", cellarIds);
                //Log the end of process
                AuditBuilder.get(AuditTrailEventProcess.Ingestion).withAction(AuditTrailEventAction.VirtuosoDelete)
                        .withType(AuditTrailEventType.End).withDuration(System.currentTimeMillis() - startTime)
                        .withMessage("Models '{}' dropped.")
                        .withMessageArgs(cellarIds)
                        .withLogger(LOG).withLogDatabase(false).logEvent();
            }
        }
    }

    /**
     * Ungroup.
     * When dropping a named graph Virtuoso does not remove its reference from the private group (if existing)
     * This operation has to be done separately due to the fact that JENA and JDBC operations do not share the
     * same transaction and in the case of a roll-back CELLAR would be in error
     *
     * @param cellarIds the cellar ids
     * @throws VirtuosoOperationException the virtuoso operation exception
     */
    private void ungroup(final Collection<String> cellarIds) throws VirtuosoOperationException {
        try {
            LOG.debug(VIRTUOSO, "Removing graphs from the private group before dropping them: {}", cellarIds);
            VirtuosoUtils.group(cellarIds, PrivateGroupOperation.REMOVE, this.virtuosoDataSource);
            LOG.debug(VIRTUOSO, "Removed {} graphs from the private group", cellarIds.size());
        } catch (final Exception e) {
            LOG.error("Failed to remove graphs from the private group", e);
            throw ExceptionBuilder.get(VirtuosoOperationException.class)
                    .withCode(CmrErrors.VIRTUOSO_OPERATION_FAILED)
                    .withMessage("Unexpected exception thrown while trying to remove dropped elements from private group")
                    .withCause(e)
                    .build();
        }
    }

    /**
     * Gets the direct metadata from all digital objects except those that are excluded.
     * Explicitly for manifestation digital objects , also add the technical metadata of each of it's items
     * @param data the data
     * @return the direct metadata
     */
    public Map<DigitalObject, Model> getMetadata(final CalculatedData data) {
        final Map<DigitalObject, Model> result = new HashMap<>();
        final Collection<DigitalObject> addedDigitalObjects = data.getAddedDigitalObjects();
        final Collection<DigitalObject> changedDigitalObjects = data.getChangedDigitalObjects();
        final Collection<DigitalObject> removedDigitalObjects = data.getRemovedDigitalObjects();
        final Collection<DigitalObject> digitalObjects = new HashSet<>();
        digitalObjects.addAll(addedDigitalObjects);
        digitalObjects.addAll(changedDigitalObjects);
        digitalObjects.removeAll(removedDigitalObjects);
        for (final DigitalObject object : digitalObjects) {
            final Map<ContentType, Model> map = data.getMetadataSnippetsForS3().get(object);
            final Model dmd = map.get(ContentType.DIRECT);
            if (cellarConfiguration.isVirtuosoIngestionTechnicalEnabled() && object.getType().equals(DigitalObjectType.MANIFESTATION) && data.getTechnicalMetadata().get(object) != null) {
                //[CLLR10815-869] - Add technical metadata to manifestation graph of Virtuoso
                //Add the item technical model to the manifestation direct model
                //If the digital object is a manifestation with a non null technical model , add it to the rest of the metadata
                dmd.add(data.getTechnicalMetadata().get(object));
            }
            //Create a clone model in order to avoid closing the original model, conflicting with virtuoso retry mechanism
            result.put(object, ModelFactory.createDefaultModel().add(dmd.listStatements()));
        }
        return result;
    }



    /**
     * {@inheritDoc}
     */
    public void triggerVirtuosoRetry(Exception e, boolean hasRetryAttempts) throws VirtuosoOperationException {
        if(isMessageTransactionDeadlocked(e) && hasRetryAttempts){
            LOG.warn("Failed to write in Virtuoso, because SR172: Transaction deadlocked. Retrying...");
            throw ExceptionBuilder.get(VirtuosoOperationException.class).withCode(CmrErrors.VIRTUOSO_OPERATION_FAILED)
                    .withCause(e).build();
        }
    }

    /**
     * Is message Transaction Deadlocked
     *
     * @param e The exception
     * @return true if message is "SR172: Transaction deadlocked"
     */
    private boolean isMessageTransactionDeadlocked(Exception e) {
        return ExceptionUtils.exceptionContainsMessages(e, "SR172: Transaction deadlocked");
    }

    /**
     * For test purposes only.
     */
    private void testRollback() {
        if (this.cellarConfiguration.isTestEnabled() && this.cellarConfiguration.isTestVirtuosoRollbackEnabled()) {
            throw ExceptionBuilder.get(VirtuosoOperationException.class)
                    .withCode(CommonErrors.TEST_EXCEPTION)
                    .withMessage("FORCED EXCEPTION FOR TEST PURPOSES")
                    .build();
        }
    }

    /**
     * Test Virtuoso Retry mechanism. For test purposes only.
     *
     * @throws VirtuosoOperationException On purpose with "SR172: Transaction deadlocked" as message to trigger Virtuoso Retry mechanism
     */
    private void testVirtuosoRetry() {
        if (this.cellarConfiguration.isVirtuosoIngestionEnabled() && this.cellarConfiguration.isTestVirtuosoRetryEnabled()) {
            throw ExceptionBuilder.get(VirtuosoOperationException.class).withMessage("SR172: Transaction deadlocked").build();
        }
    }

    private Optional<String> getServerVersion() {
        try (final Connection connection = this.virtuosoDataSource.getConnection();
             final Statement statement = connection.createStatement();
             final ResultSet resultSet = statement.executeQuery(SERVER_VERSION_QUERY)) {

            if (resultSet.next()) {
                return Optional.of(resultSet.getString("version"));
            }
            return Optional.empty();
        } catch (final Exception e) {
            LOG.error("Error when retrieving Virtuoso Server's version", e);
            return Optional.empty();
        }
    }

    private Optional<String> getDriverVersion() {
        try {
            final Driver driver = new Driver();
            return Optional.of(driver.getMajorVersion() + "." + driver.getMinorVersion());
        } catch (SQLException e) {
            LOG.error("Error when retrieving Virtuoso Driver's version", e);
            return Optional.empty();
        }
    }

    @Override
    public VirtuosoInformation getVirtuosoInformation() {
        return new VirtuosoInformation(
                getServerVersion().orElse("unknown"),
                getDriverVersion().orElse("unknown"));
    }
}
