/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr
 *             FILE : MetaDataIngestionHelper.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Sep 2, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$$
 *          VERSION : $LastChangedRevision$$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr;

import com.amazonaws.services.s3.Headers;
import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.LanguageIsoCode;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditBuilder;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventAction;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventProcess;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventType;
import eu.europa.ec.opoce.cellar.cl.domain.package_history.PackageHistory;
import eu.europa.ec.opoce.cellar.cl.domain.response.IOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.ResponseOperationType;
import eu.europa.ec.opoce.cellar.cl.domain.structmap_status_history.StructMapStatusHistory;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.advice.ThrowExceptionForTestPurpose;
import eu.europa.ec.opoce.cellar.cmr.daobeans.InverseRelation;
import eu.europa.ec.opoce.cellar.cmr.database.dao.*;
import eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData;
import eu.europa.ec.opoce.cellar.cmr.notice.embedded.EmbeddedNoticeService;
import eu.europa.ec.opoce.cellar.cmr.quad.ModelQuadService;
import eu.europa.ec.opoce.cellar.cmr.service.EmbargoService;
import eu.europa.ec.opoce.cellar.cmr.technicalMd.CmrItemTechnicalMd;
import eu.europa.ec.opoce.cellar.cmr.utils.MetadataUtils;
import eu.europa.ec.opoce.cellar.common.factory.IFactory;
import eu.europa.ec.opoce.cellar.common.metadata.IBusinessMetadataBackupService;
import eu.europa.ec.opoce.cellar.common.util.DigitalObjectUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.*;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.CellarValidationException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.feed.domain.enums.FeedItemType;
import eu.europa.ec.opoce.cellar.feed.domain.enums.FeedPriority;
import eu.europa.ec.opoce.cellar.feed.domain.impl.ingestion.IngestionHistory;
import eu.europa.ec.opoce.cellar.feed.service.HistoryService;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;
import eu.europa.ec.opoce.cellar.logging.LogContext;
import eu.europa.ec.opoce.cellar.modelload.domain.ModelLoadRequest;
import eu.europa.ec.opoce.cellar.monitoring.Watch;
import eu.europa.ec.opoce.cellar.nal.dao.NalConfig;
import eu.europa.ec.opoce.cellar.nal.domain.NalObject;
import eu.europa.ec.opoce.cellar.ontology.exception.OntologyException;
import eu.europa.ec.opoce.cellar.ontology.service.OntologyService;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import eu.europa.ec.opoce.cellar.semantic.helper.MessageFormatter;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarType;
import eu.europa.ec.opoce.cellar.semantic.validation.NalValidationUtils;
import eu.europa.ec.opoce.cellar.sparql.SparqlLoadRequest;
import eu.europa.ec.opoce.cellar.virtuoso.VirtuosoService;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Predicate;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.vocabulary.RDF;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import static eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration.INGESTION;
import static eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration.VIRTUOSO;
import static eu.europa.ec.opoce.cellar.cmr.ContentType.FILE;
import static eu.europa.ec.opoce.cellar.cmr.ContentType.TECHNICAL;
import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.CMR_INGESTION;
import static eu.europa.ec.opoce.cellar.nal.NalOntoUtils.SPARQL_LOAD_PREFIX;

/**
 * <p>
 * MetaDataIngestionHelper class.
 * </p>
 *
 * @author ARHS Developments
 * @version $Revision$$
 */
@Service
public class MetaDataIngestionHelper {

    private static final Logger LOG = LogManager.getLogger(MetaDataIngestionHelper.class);

    @Autowired
    @Qualifier("modelQuadService")
    private ModelQuadService modelQuadService;

    @Autowired
    private CellarResourceDao cellarResourceDao;

    @Autowired
    private InverseRelationDao inverseRelationDao;

    @Autowired
    private ModelLoadRequestDao modelLoadRequestDao;

    @Autowired
    private SparqlLoadRequestRepository sparqlLoadRequestRepository;

    @Autowired
    private CmrItemTechnicalMdRepository cmrItemTechnicalMdRepository;

    @Autowired
    @Qualifier("pidManagerService")
    private IdentifierService identifierService;

    @Autowired
    private RelatedObjectCalculator relatedObjectCalculator;

    @Autowired
    @Qualifier("businessMetadataBackupFactory")
    private IFactory<IBusinessMetadataBackupService<StructMap, IOperation<?>>, IOperation<?>> businessMetadataBackupFactory;

    @Autowired
    private HistoryService historyService;

    @Autowired
    private EmbargoService embargoService;

    /**
     * The embedded notice service.
     */
    @Autowired
    private EmbeddedNoticeService embeddedNoticeService;

    @Autowired
    private VirtuosoService virtuosoService;

    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    @Autowired
    private OntologyService ontologyService;

    @Autowired
    private ContentStreamService contentStreamService;

    @Autowired
    private NalConfigDao nalConfigDao;

    @Autowired
    private ResponseOperationTypeResolver responseOperationTypeResolver;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    private static final String TDM_STREAM_NAME = "http://publications.europa.eu/ontology/tdm#stream_name";
    private static final String TDM_STREAM_ORDER = "http://publications.europa.eu/ontology/tdm#stream_order";
    private static final String TDM_STREAM_SIZE = "http://publications.europa.eu/ontology/tdm#stream_size";
    private static final String TDM_CHECKSUM = "http://publications.europa.eu/ontology/tdm#fixity_message_digest";
    private static final String TDM_CHECKSUM_ALGORITHM = "http://publications.europa.eu/ontology/tdm#fixity_message_digest_algorithm";

    private static final String TDM_FIXITY_STREAM="http://publications.europa.eu/ontology/tdm#stream_fixity";

    /**
     * This method updates or creates all metadata in the cmr database for a
     * given structmap.
     *
     * @param calculatedData a {@link eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData}
     *                       object.
     * @param operation      the operation
     */
    @Watch(value = "Metadata ingestion", arguments = {
            @Watch.WatchArgument(name = "cellar ID", expression = "calculatedData.rootCellarId")
    })
    public void ingest(final CalculatedData calculatedData, final IOperation<?> operation) {
        final StructMap structMap = calculatedData.getStructMap();
        if (structMap.getDigitalObject() == null) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("structmap without digital object").build();
        }

        String processName = null;
        if (LOG.isErrorEnabled()) {
            processName = MessageFormatter.arrayFormat("handle loaded metadata '{}' holding identifiers '[{}]' [metsdocument '{}']",
                    new String[]{
                            structMap.getId(), StringUtils.join(structMap.getDigitalObject().getAllUris(), ", "),
                            structMap.getMetsDocument().getDocumentId()});
        }
        LOG.info("Start {}", processName);

        try {
            long startTime = System.currentTimeMillis();
            //Log the start of process
            AuditBuilder.get(AuditTrailEventProcess.Ingestion).withAction(AuditTrailEventAction.LoadingMetadaSnippets)
                    .withType(AuditTrailEventType.Start)
                    .withStructMapStatusHistory(calculatedData.getStructMapStatusHistory())
                    .withPackageHistory(calculatedData.getPackageHistory())
                    .withMessage("Loading all metadata snippets...")
                    .withLogger(LOG).withLogDatabase(false).logEvent();

            // save the metadata snippets to CCR
            saveMetadataToCCR(calculatedData);
            saveItems(calculatedData);

            // update struct map operation for rollback purposes
            businessMetadataBackupFactory.create(operation).backupBusinessMetadata(structMap, operation);

            // save the metadata snippets to CMR
            saveMetadataToCMR(calculatedData);

            // update the CELLAR database with the new information about
            // metadata
            saveMetadataToDatabase(calculatedData, operation);
            //Log the end of process
            AuditBuilder.get(AuditTrailEventProcess.Ingestion).withAction(AuditTrailEventAction.LoadingMetadaSnippets)
                    .withType(AuditTrailEventType.End)
                    .withStructMapStatusHistory(calculatedData.getStructMapStatusHistory())
                    .withPackageHistory(calculatedData.getPackageHistory())
                    .withDuration(System.currentTimeMillis() - startTime)
                    .withMessage("Loading all metadata snippets done in {} ms.")
                    .withMessageArgs(System.currentTimeMillis() - startTime)
                    .withLogger(LOG).withLogDatabase(false).logEvent();
            LOG.info("End {}", processName);
        } catch (final RuntimeException ex) {
            LOG.error("Fail {}", processName);
            throw ex;
        }
    }

    private void saveItems(CalculatedData calculatedData) {
        for (DigitalObject o : calculatedData.getChangedDigitalObjects()) {
            if (o.getType() == DigitalObjectType.MANIFESTATION) {
                for (ContentStream item : o.getContentStreams()) {
                    if (!Strings.isBlank(item.getFileRef())) {
                        writeContent(item, calculatedData.getMetsPackage().getZipExtractionFolder());
                        o.registerUpdatedType(FILE);
                    } else {
                        LOG.debug(INGESTION, "Content {} put in the mets to avoid to remove it from S3 (fileRef == null).",
                                item.getCellarId().getIdentifier());
                    }
                }
            }
        }
    }

    private void writeContent(ContentStream content, File zipExtractionFolder) {
        Path srcFile = Paths.get(zipExtractionFolder.getAbsolutePath(), content.getFileRef());
        FileSystemResource r = new FileSystemResource(srcFile.toFile());
        if (r.exists() && !Files.isDirectory(srcFile)) {
            Map<String, String> metadata = new HashMap<>();
            metadata.put(Headers.CONTENT_TYPE, content.getMimeType());
            metadata.put(ContentStreamService.Metadata.FILENAME, dropPath(content.getFileRef()));
            String newVersion = contentStreamService.writeContent(content.getCellarId().getIdentifier(), r, FILE, metadata);
            content.setVersion(newVersion);
        } else {
            LOG.debug(INGESTION, "Skip directory {}, only regular files are accepted", srcFile);
        }
    }

    public static String dropPath(String fileRef) {
        if (fileRef.contains("/")) {
            return fileRef.substring(fileRef.lastIndexOf("/") + 1);
        }
        return fileRef;
    }

    /**
     * This method updates or creates all metadata in Virtuoso.
     *
     * @param calculatedData a {@link eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData}
     *                       object.
     */
    @Watch(value = "Virtuoso ingestion", arguments = {
            @Watch.WatchArgument(name = "cellar ID", expression = "calculatedData.rootCellarId")
    })
    public void ingestVirtuoso(final CalculatedData calculatedData) {
        if (this.cellarConfiguration.isVirtuosoIngestionEnabled()) {
            LOG.debug(VIRTUOSO, "Start ingestion  of {} in Virtuoso...", calculatedData.getRootCellarId());
            // save the metadata snippets to Virtuoso
            try {
				// save the metadata snippets to Virtuoso
				this.virtuosoService.write(calculatedData, true);
                // set under embargo if necessary
                final Set<String> embargoedCellarIds = embargoedCellarIds(calculatedData);
                if (!embargoedCellarIds.isEmpty()) {
                    LOG.debug(VIRTUOSO, "Embargo CELLAR IDs not empty -> put {} under embargo.", embargoedCellarIds);
                    this.virtuosoService.hide(embargoedCellarIds, true);
                }

                final Set<String> disEmbargoedCellarIds = disEmbargoedCellarIds(calculatedData);
                if (!disEmbargoedCellarIds.isEmpty()) {
                    LOG.debug(VIRTUOSO, "DisEmbargo CELLAR IDs not empty -> remove {} from embargo.", disEmbargoedCellarIds);
                    this.virtuosoService.unhide(disEmbargoedCellarIds, true);
                }
            } catch (final Exception e) {
            	LOG.error("Ingestion of" + calculatedData.getRootCellarId() + " in Virtuoso failed", e);
                AuditBuilder.get(AuditTrailEventProcess.Ingestion)
                        .withStructMapStatusHistory(calculatedData.getStructMapStatusHistory())
                        .withPackageHistory(calculatedData.getPackageHistory())
                        .withMessage("Unexpected exception was thrown when loading calculatedData into Virtuoso")
                        .withException(e)
                        .withLogger(LOG).logEvent();
            } finally {
                LOG.debug(VIRTUOSO, "Ingestion of {} in Virtuoso done", calculatedData.getRootCellarId());
            }
        } else {
            LOG.warn("Virtuoso ingestion is disabled");
        }
    }

    /**
     * This method delete all CellarIds Of Work Hierarchy in virtuoso.
     * 'ThrowExceptionForTestPurpose' has been added to support a special DELETE case of work without children.
     *
     * @param allCellarIdsOfWorkHierarchy all cellarIds Of Work hierarchy.
     */
    @ThrowExceptionForTestPurpose
    public void deleteVirtuoso(final List<String> allCellarIdsOfWorkHierarchy) {
        if (this.cellarConfiguration.isVirtuosoIngestionEnabled()) {
            // Delete from virtuoso
            try {
                this.virtuosoService.drop( allCellarIdsOfWorkHierarchy, true, true);

            } catch (final Exception e) {
                LOG.error("Unexpected exception was thrown while trying to drop named graphs from Virtuoso", e);
            }
        }
    }

    /**
     * <p>
     * saveMetadataToCCR.
     * </p>
     *
     * @param calculatedData a {@link eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData}
     *                       object.
     */
    private void saveMetadataToCCR(final CalculatedData calculatedData) {
        final long startTime = System.currentTimeMillis();
        //Log the start of process
        AuditBuilder.get(AuditTrailEventProcess.Ingestion).withAction(AuditTrailEventAction.LoadingMetadaSnippetsIntoS3)
                .withType(AuditTrailEventType.Start)
                .withStructMapStatusHistory(calculatedData.getStructMapStatusHistory())
                .withPackageHistory(calculatedData.getPackageHistory())
                .withMessage("Loading all metadata snippets into S3...")
                .withLogger(LOG).withLogDatabase(false).logEvent();
        updateDatastreams(calculatedData);
        //Log the end of process
        AuditBuilder.get(AuditTrailEventProcess.Ingestion).withAction(AuditTrailEventAction.LoadingMetadaSnippetsIntoS3)
                .withType(AuditTrailEventType.End)
                .withStructMapStatusHistory(calculatedData.getStructMapStatusHistory())
                .withPackageHistory(calculatedData.getPackageHistory())
                .withDuration(System.currentTimeMillis() - startTime)
                .withMessage("Loading all {} metadata snippets into S3 done in {} ms.")
                .withMessageArgs(calculatedData.getMetadataSnippetsForS3().size(), System.currentTimeMillis() - startTime)
                .withLogger(LOG).withLogDatabase(false).logEvent();
    }

    /**
     * <p>
     * saveMetadataToDatabase.
     * </p>
     *
     * @param calculatedData a {@link CalculatedData}
     *                       object.
     * @param operation
     */
    private void saveMetadataToDatabase(final CalculatedData calculatedData, IOperation<?> operation) {
        long startTime = System.currentTimeMillis();
        //Log the start of process
        AuditBuilder.get(AuditTrailEventProcess.Ingestion).withAction(AuditTrailEventAction.UpdateInverseTables)
                .withType(AuditTrailEventType.Start)
                .withStructMapStatusHistory(calculatedData.getStructMapStatusHistory())
                .withPackageHistory(calculatedData.getPackageHistory())
                .withMessage("Updating inverse tables...")
                .withLogger(LOG).withLogDatabase(false).logEvent();
        this.updateInverseTablesAndCalculateRelationChanges(calculatedData);
        //Log the end of process
        AuditBuilder.get(AuditTrailEventProcess.Ingestion).withAction(AuditTrailEventAction.UpdateInverseTables)
                .withType(AuditTrailEventType.End)
                .withStructMapStatusHistory(calculatedData.getStructMapStatusHistory())
                .withPackageHistory(calculatedData.getPackageHistory())
                .withDuration(System.currentTimeMillis() - startTime)
                .withMessage("Updating inverse tables done in {} ms.")
                .withMessageArgs(System.currentTimeMillis() - startTime)
                .withLogger(LOG).withLogDatabase(false).logEvent();

        startTime = System.currentTimeMillis();
        //Log the start of process
        AuditBuilder.get(AuditTrailEventProcess.Ingestion).withAction(AuditTrailEventAction.UpdateCellarResourceTables)
                .withType(AuditTrailEventType.Start)
                .withStructMapStatusHistory(calculatedData.getStructMapStatusHistory())
                .withPackageHistory(calculatedData.getPackageHistory())
                .withMessage("Updating Cellar Resource tables...")
                .withLogger(LOG).withLogDatabase(false).logEvent();
        this.updateCellarResourceMetadata(calculatedData, operation);
        this.updateModelLoadRequest(calculatedData);
        updateSparqlLoadRequests(calculatedData);
        this.loadOntology(calculatedData);
        //Log the end of process
        AuditBuilder.get(AuditTrailEventProcess.Ingestion).withAction(AuditTrailEventAction.UpdateCellarResourceTables)
                .withType(AuditTrailEventType.End)
                .withStructMapStatusHistory(calculatedData.getStructMapStatusHistory())
                .withPackageHistory(calculatedData.getPackageHistory())
                .withDuration(System.currentTimeMillis() - startTime)
                .withMessage("Updating Cellar Resource tables done in {} ms.")
                .withMessageArgs(System.currentTimeMillis() - startTime)
                .withLogger(LOG).withLogDatabase(false).logEvent();
    }

    /**
     * Update model load requests available in calculated data.
     *
     * @param calculatedData a {@link eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData}
     *                       object.
     */
    private void updateModelLoadRequest(final CalculatedData calculatedData) {

        for (final DigitalObject digitalObject : calculatedData.getChangedDigitalObjects()) {
            if (digitalObject.getModelLoadURL() != null) {

                AuditBuilder.get(AuditTrailEventProcess.Nal).withAction(AuditTrailEventAction.Load)
                        .withType(AuditTrailEventType.Start).withObject(digitalObject.getModelLoadURL())
                        .withStructMapStatusHistory(calculatedData.getStructMapStatusHistory())
                        .withPackageHistory(calculatedData.getPackageHistory())
                        .withMessage("Model-load request {} must be added/updated.")
                        .withMessageArgs(digitalObject.getCellarId().getIdentifier())
                        .withLogger(LOG).withLogDatabase(true).logEvent();

                Optional<ModelLoadRequest> modelLoadRequest = this.modelLoadRequestDao.findByModelURI(digitalObject.getModelLoadURL());
                String externalPid = null;
                if (modelLoadRequest.isPresent()) {
                    LOG.info("Model-load request {} already exists.", digitalObject.getModelLoadURL());
                    externalPid = deleteNalRequest(modelLoadRequest.get());
                    LOG.info("Model-load request {} has been removed.", digitalObject.getModelLoadURL());
                }

                if (externalPid == null) { // check if there is already a NALConfig
                    externalPid = nalConfigDao.findByUri(digitalObject.getModelLoadURL())
                            .map(NalConfig::getExternalPid)
                            .orElse(UUID.randomUUID().toString());
                }

                ModelLoadRequest newModelLoadRequest = new ModelLoadRequest(digitalObject.getModelLoadURL(), digitalObject.getModelLoadActivationDate(), externalPid);

                // we need to create backup of the content streams of the manifestation targeted by the model load behaviour
                // we merge all RDF content streams (if more than one) from the mets in a single RDF and back it up on S3
                Model nalModel = createModelFromContentStreams(calculatedData, digitalObject);

                //validate skos concepts during ingestion process, if invalid reject package
                validateSkosConceptsInModel(digitalObject, nalModel, calculatedData.getStructMapStatusHistory(), calculatedData.getPackageHistory());

                if (!externalPid.startsWith("nal:")) {
                    externalPid = "nal:" + externalPid;
                }
                String version = contentStreamService.writeContent(externalPid, new ByteArrayResource(JenaUtils.bytes(nalModel, RDFFormat.NT)), ContentType.DIRECT);
                newModelLoadRequest.setExternalPid(externalPid);
                newModelLoadRequest.setVersion(version);
                newModelLoadRequest.setCellarId(digitalObject.getCellarId().getIdentifier());
                try {
                    this.modelLoadRequestDao.saveObject(newModelLoadRequest);
                } catch (Exception e) {
                    LOG.warn("Cannot save model-load request identified by '{}': [Optimistic locking] the model-load  request may have been removed/updated by model-load " +
                            "request scheduler or an ingestion package.", newModelLoadRequest.getModelURI());
                    throw e;
                }

                AuditBuilder.get(AuditTrailEventProcess.Nal).withAction(AuditTrailEventAction.Load)
                        .withType(AuditTrailEventType.End).withObject(digitalObject.getModelLoadURL())
                        .withStructMapStatusHistory(calculatedData.getStructMapStatusHistory())
                        .withPackageHistory(calculatedData.getPackageHistory())
                        .withMessage("Model-load request {} ({}) has been added/modified.")
                        .withMessageArgs(digitalObject.getCellarId().getIdentifier(), digitalObject.getModelLoadURL())
                        .withLogger(LOG).withLogDatabase(true).logEvent();
            }
        }
    }

    private void validateSkosConceptsInModel(DigitalObject digitalObject, Model nalModel, StructMapStatusHistory structMapStatusHistory, PackageHistory packageHistory){
        if(!NalValidationUtils.validateSkosConcepts(nalModel)){
            AuditBuilder.get(AuditTrailEventProcess.Nal)
                    .withAction(AuditTrailEventAction.Load)
                    .withType(AuditTrailEventType.Fail)
                    .withStructMapStatusHistory(structMapStatusHistory)
                    .withPackageHistory(packageHistory)
                    .withMessage("Model-load request {} ({}) has failed.")
                    .withMessageArgs(digitalObject.getCellarId().getIdentifier(), digitalObject.getModelLoadURL())
                    .withLogDatabase(true)
                    .withObject(digitalObject.getModelLoadURL())
                    .withLogger(LOG)
                    .logEvent();

            throw ExceptionBuilder.get(CellarValidationException.class)
                    .withMessage("Invalid skos concepts found in model '{}").withMessageArgs(digitalObject.getModelLoadURL()).build();
        }
    }

    private String deleteNalRequest(ModelLoadRequest mlr) {
        try {
            if (contentStreamService.exists(mlr.getExternalPid(), ContentType.DIRECT)) {
                contentStreamService.deleteContent(mlr.getExternalPid(), ContentType.DIRECT);
            }
        } catch (Exception fce) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withCause(fce)
                    .withMessage("Removing the existing NAL '{}' backup in S3 failed")
                    .withMessageArgs(mlr.getExternalPid())
                    .build();
        }

        try {
            this.modelLoadRequestDao.deleteObject(mlr);
        } catch (RuntimeException exception) {
            LOG.warn("Cannot delete model-load request identified by '{}': [Optimistic locking] the " +
                    "model-load request may have been removed/updated by model-load request scheduler " +
                    "or an ingestion package.", mlr.getModelURI());
            throw exception;
        }
        return mlr.getNalName();
    }

    private Model createModelFromContentStreams(CalculatedData calculatedData, DigitalObject digitalObject) {
        Model nalModel = ModelFactory.createDefaultModel();
        List<ContentStream> contentStreams = digitalObject.getContentStreams();
        for (ContentStream contentStreamDo : contentStreams) {
            if (StringUtils.equalsIgnoreCase(contentStreamDo.getMimeType(), NalObject.APPLICATION_RDF_XML)) {
                String fileNameInMets = contentStreamDo.getLabel();
                try (InputStream contentStream = calculatedData.getMetsPackage().getFileStream(contentStreamDo.getFileRef())) {
                    nalModel.add(ModelFactory.createDefaultModel().read(contentStream, "", Lang.RDFXML.getName()));
                } catch (IOException ioe) {
                    String errorMessage = "Could not read file '{}' on the file system : {}";
                    LOG.error(errorMessage, fileNameInMets, ioe.toString(), ioe);
                    throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.INGESTION_SERVICE_ERROR).withCause(ioe)
                            .withMessage(errorMessage).withMessageArgs(fileNameInMets, ioe.toString())
                            .build();
                }
            }
        }
        return nalModel;
    }

    private void updateSparqlLoadRequests(CalculatedData calculatedData) {
        calculatedData.getChangedDigitalObjects().stream()
                .filter(o -> o.getSparqlLoadURL() != null)
                .peek(o -> sparqlLoadRequestRepository.findByUri(o.getSparqlLoadURL())
                        .ifPresent(this::deleteSparqlRequest))
                .map(o -> saveSparqlLoad(o, calculatedData))
                .forEach(s -> LOG.info("Sparql-load request {} (based on {} to be activated on {}) has been added.",
                        s.getExternalPid(), s.getUri(), s.getActivationDate()));
    }

    private SparqlLoadRequest saveSparqlLoad(DigitalObject digitalObject, CalculatedData calculatedData) {
        SparqlLoadRequest request = new SparqlLoadRequest(digitalObject.getSparqlLoadURL(), digitalObject.getSparqlLoadActivationDate());
        // we need to create backup of the content streams of the manifestation targeted by the sparql load behaviour
        // we merge all RDF content streams (if more than one) from the mets in a single RDF and back it up on S3
        Model model = createModelFromContentStreams(calculatedData, digitalObject);
        String externalPid = UUID.randomUUID().toString();
        String version = contentStreamService.writeContent(SPARQL_LOAD_PREFIX + externalPid,
                new ByteArrayResource(JenaUtils.bytes(model, RDFFormat.NT)), ContentType.DIRECT);
        request.setExternalPid(SPARQL_LOAD_PREFIX + externalPid);
        request.setVersion(version);
        request.setCellarId(digitalObject.getCellarId().getIdentifier());
        try {
            sparqlLoadRequestRepository.save(request);
        } catch (Exception e) {
            LOG.error("Cannot save sparql-load request identified by '{}': [Optimistic locking] the sparql-load  request may have been removed/updated by sparql-load " +
                    "request scheduler or an ingestion package.", request.getUri());
            throw e;
        }
        return request;
    }

    private String deleteSparqlRequest(SparqlLoadRequest r) {
        LOG.info("Sparql-load request {} already exists.", r.getUri());
        try {
            if (contentStreamService.exists(r.getExternalPid(), ContentType.DIRECT)) {
                contentStreamService.deleteContent(r.getExternalPid(), ContentType.DIRECT);
            }
        } catch (Exception e) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withCause(e)
                    .withMessage("Removing the existing NAL '{}' backup in S3 failed")
                    .withMessageArgs(r.getExternalPid())
                    .build();
        }

        try {
            sparqlLoadRequestRepository.deleteAllByUri(r.getUri());
        } catch (Exception e) {
            LOG.warn("Cannot delete sparql-load request identified by '{}': the sparql-load request may have been " +
                    "removed/updated by sparql-load request scheduler or an ingestion package.", r.getUri());
            throw e;
        }
        return r.getUri();
    }

    /**
     * Load ontology available in calculated data.
     *
     * @param calculatedData a {@link eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData}
     *                       object.
     */
    private void loadOntology(final CalculatedData calculatedData) {

        for (final DigitalObject digitalObject : calculatedData.getChangedDigitalObjects()) {
            if (digitalObject.isOntologyLoad()) {

                LOG.info("Ontology-load via SIP package '{}' is started.", calculatedData.getMetsPackage().getZipFile().getName());

                final List<File> itemFiles = new ArrayList<>();
                final List<ContentStream> contentStreams = digitalObject.getContentStreams();
                final MetsPackage metsPackage = calculatedData.getMetsPackage();
                final File zipExtractionFolder = metsPackage.getZipExtractionFolder();
                for (final ContentStream contentStream : contentStreams) {
                    final String fileRef = contentStream.getFileRef();
                    final File file = new File(zipExtractionFolder, fileRef);
                    itemFiles.add(file);
                }

                try {
                    ontologyService.update(itemFiles,digitalObject.getCellarId().getIdentifier());
                } catch (OntologyException e) {
                    throw ExceptionBuilder.get(CellarException.class)
                            .withCode(CmrErrors.ONTO_LOAD_INTERNAL_ERROR)
                            .withMessage("Some errors occurred during the loading of the ontology descriptor files.")
                            .build();
                }
                LOG.info("Ontology-load via SIP package '{}' is finished.", calculatedData.getMetsPackage().getZipFile().getName());
            }
        }
    }

    /**
     * <p>
     * saveMetadataToCMR.
     * </p>
     *
     * @param calculatedData a {@link eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData}
     *                       object.
     */
    private void saveMetadataToCMR(final CalculatedData calculatedData) {
        final long startTime = System.currentTimeMillis();
        //Log the start of process
        AuditBuilder.get(AuditTrailEventProcess.Ingestion).withAction(AuditTrailEventAction.LoadingMetadaSnippetsIntoOracle)
                .withType(AuditTrailEventType.Start)
                .withStructMapStatusHistory(calculatedData.getStructMapStatusHistory())
                .withPackageHistory(calculatedData.getPackageHistory())
                .withMessage("Loading all metadata snippets into oracle...")
                .withLogger(LOG).withLogDatabase(false).logEvent();
        this.updateOracleRdfTables(calculatedData);
        //Log the end of process
        AuditBuilder.get(AuditTrailEventProcess.Ingestion).withAction(AuditTrailEventAction.LoadingMetadaSnippetsIntoOracle)
                .withType(AuditTrailEventType.End).withDuration(System.currentTimeMillis() - startTime)
                .withStructMapStatusHistory(calculatedData.getStructMapStatusHistory())
                .withPackageHistory(calculatedData.getPackageHistory())
                .withMessage("Loading all {} metadata snippets into oracle done in {} ms.")
                .withMessageArgs(calculatedData.getMetadataSnippetsForS3().size() + calculatedData.getRemovedDigitalObjects().size(),System.currentTimeMillis() - startTime)
                .withLogger(LOG).withLogDatabase(false).logEvent();
    }

    private void updateDatastreams(final CalculatedData calculatedData) {

        final Date lastModificationDate = new Date();
        for (final DigitalObject digitalObject : calculatedData.getChangedDigitalObjects()) {

            final Map<ContentType, Model> map = calculatedData.getMetadataSnippetsForS3().get(digitalObject);
            final Map<ContentType, String> streams = map.entrySet().stream()
                    .collect(Collectors.toMap(
                            Entry::getKey,
                            e -> JenaUtils.toString(e.getValue(), RDFFormat.NT)));

            // add datastream from technical metadata if possible
            final Model technicalModel = calculatedData.getTechnicalMetadata().get(digitalObject);

            if (technicalModel != null) {
                streams.put(TECHNICAL, JenaUtils.toString(technicalModel, RDFFormat.NT));
            }

            final String identifier = digitalObject.getCellarId().getIdentifier();
            final Map<ContentType, String> versions = contentStreamService.writeContents(identifier, streams,
                    () -> newCellarResource(digitalObject));
            for (ContentType contentType : versions.keySet()) {
                digitalObject.registerUpdatedType(contentType);
            }

            // the versions generated by S3 will be stored in Oracle for the subsequents read of the streams
            digitalObject.setVersions(versions);

            if (DigitalObjectUtils.isUnderEmbargo(digitalObject)) {
                final Date embargoDate = DigitalObjectUtils.getInheritedEmbargoDate(digitalObject);
                embargoService.updateEmbargoDate(digitalObject.getCellarId().getIdentifier(), embargoDate,
                        lastModificationDate, digitalObject.getType());
            }
        }
    }
    @LogContext(LogContext.Context.CMR_INGESTION)
    private void addTechnicalMetadataOfItemsToDb(CellarResource resource,DigitalObject digitalObject, CalculatedData data){
        try {
            Model techMd = data.getTechnicalMetadata().get(digitalObject);
            if(techMd!=null) {
                CmrItemTechnicalMd itemTechMd = createItemMetadataEntity(techMd, resource);
                cmrItemTechnicalMdRepository.save(itemTechMd);
                logCmrItemTechnicalMd(itemTechMd, data.getStructMapStatusHistory(), data.getPackageHistory());
            }else
            {
                LOG.debug("Manifestation with id {} does not appear to have any technical metadata",digitalObject.getCellarId());
            }
        }
        catch(Exception e){
            LOG.warn("Saving technical item metadata to database failed",e);
        }

    }
    
    @LogContext(CMR_INGESTION)
    private void logCmrItemTechnicalMd(CmrItemTechnicalMd itemTechMd, StructMapStatusHistory structMapStatusHistory, PackageHistory packageHistory) {
        AuditBuilder.get(AuditTrailEventProcess.Ingestion)
                .withStructMapStatusHistory(structMapStatusHistory)
                .withPackageHistory(packageHistory)
                .withMessage(
                        "Technical-metadata [stream_name : {}, stream_size : {}, stream_order : {}, checksum : {}, checksum_algorithm : {}]")
                .withMessageArgs(itemTechMd.getStreamName(), itemTechMd.getStreamSize(), itemTechMd.getStreamOrder(),
                        itemTechMd.getChecksum(), itemTechMd.getChecksumAlgorithm())
                .withLogger(LOG).withLogDatabase(false).logEvent();
    }

    private CmrItemTechnicalMd createItemMetadataEntity(Model techMd,CellarResource resource){
        List<String> pidUris = identifierService.getIdentifier(resource.getCellarId()).getPids().stream()
                .map(o -> identifierService.getUri(o))
                .collect(Collectors.toList());
        try {
            CmrItemTechnicalMd itemTechMd = cmrItemTechnicalMdRepository.findByCellarId(resource.getCellarId());
            if (itemTechMd == null) itemTechMd = new CmrItemTechnicalMd();
            itemTechMd.setCellarId(resource.getCellarId());
            itemTechMd.setLastModificationDate(new Date());
            RDFNode blankNodeFixity = null;
            for (String id : pidUris) {
                if (techMd.containsResource(ResourceFactory.createResource(id))) {
                    Resource idResource = ResourceFactory.createResource(id);
                    StmtIterator iterator = techMd.listStatements(idResource, (Property) null, (String) null);
                    while (iterator.hasNext()) {
                        Statement stmt = iterator.nextStatement();
                        String predicate = stmt.getPredicate().toString();
                        switch (predicate) {
                            case TDM_STREAM_NAME:
                                if(stmt.getObject().isAnon()){
                                    LOG.info("Stream name value could not be retrieved as it has a blank-node value");
                                }
                                else {
                                    itemTechMd.setStreamName(StringUtils.substringBefore(stmt.getObject().toString(), "^"));
                                }
                                break;
                            case TDM_STREAM_ORDER:
                                if(stmt.getObject().isAnon()){
                                    LOG.info("Stream order value could not be retrieved as it has a blank-node value");
                                }
                                else {
                                    itemTechMd.setStreamOrder(Integer.parseInt(StringUtils.substringBefore(stmt.getObject().toString(), "^")));
                                }
                                break;
                            case TDM_STREAM_SIZE:
                                if(stmt.getObject().isAnon()){
                                    LOG.info("Stream size value could not be retrieved as it has a blank-node value");
                                }
                                else {
                                    itemTechMd.setStreamSize(Integer.parseInt(StringUtils.substringBefore(stmt.getObject().toString(), "^")));
                                }
                                break;
                            case TDM_CHECKSUM:
                                itemTechMd.setChecksum(StringUtils.substringBefore(stmt.getObject().toString(), "^"));
                                break;
                            case TDM_CHECKSUM_ALGORITHM:
                                itemTechMd.setChecksumAlgorithm(StringUtils.substringBefore(stmt.getObject().toString(), "^"));
                                break;
                            case TDM_FIXITY_STREAM:
                                blankNodeFixity = stmt.getObject();
                                break;
                            default:
                                break;
                        }
                    }
                    if (blankNodeFixity != null) {
                        StmtIterator it = techMd.listStatements(blankNodeFixity.asResource(),
                                (Property) null, (RDFNode) null);
                        while (it.hasNext()) {
                            Statement st = it.nextStatement();
                            if (st.getPredicate().equals(ResourceFactory.createProperty(TDM_CHECKSUM))
                                    &&itemTechMd.getChecksum()==null) {
                                itemTechMd.setChecksum(StringUtils.substringBefore(st.getObject().toString(), "^"));
                            } else if (st.getPredicate().equals(ResourceFactory.createProperty(TDM_CHECKSUM_ALGORITHM))
                                    &&itemTechMd.getChecksumAlgorithm()==null) {
                                itemTechMd.setChecksumAlgorithm(StringUtils.substringBefore(st.getObject().toString(), "^"));
                            }
                        }
                    }
                }
            }
            return itemTechMd;
        }catch(Exception e){
            LOG.error("Unexpected exception thrown while handling the item technical metadata.",e);
            return null;

        }

    }
    private static CellarResource newCellarResource(DigitalObject digitalObject) {
        CellarResource cellarResource = new CellarResource();
        cellarResource.setCellarId(digitalObject.getCellarId().getIdentifier());
        cellarResource.setCellarType(digitalObject.getType());
        cellarResource.setLastModificationDate(digitalObject.getLastModificationDate());
        cellarResource.setMimeType(digitalObject.getManifestationMimeType());
        return cellarResource;
    }

    private void updateCellarResourceMetadata(final CalculatedData calculatedData, IOperation<?> operation) {
        // get all cellar resources that probably will need to be updated and
        // convert to map
        final Collection<CellarResource> cellarResourcesInDb = this.cellarResourceDao
                .findStartingWith(calculatedData.getStructMap().getDigitalObject().getCellarId().getIdentifier());
        final Map<String, CellarResource> cellarResourceMapInDb = new HashMap<>();
        for (final CellarResource cellarResource : cellarResourcesInDb) {
            cellarResourceMapInDb.put(cellarResource.getCellarId(), cellarResource);
        }

        this.updateCellarResources(calculatedData, cellarResourcesInDb, cellarResourceMapInDb, operation);
        this.removeCellarResources(calculatedData, cellarResourcesInDb, cellarResourceMapInDb);
    }

    private void updateCellarResources(final CalculatedData calculatedData, final Collection<CellarResource> cellarResourcesInDb,
                                       final Map<String, CellarResource> cellarResourceMapInDb, IOperation<?> operation) {
        final Collection<DigitalObject> changedDigitalObjects = calculatedData.getChangedDigitalObjects();
        final List<DigitalObject> changedDigitalObjectsSorted = DigitalObjectUtils.sortListItemFirst(changedDigitalObjects);
        for (final DigitalObject digitalObject : changedDigitalObjectsSorted) {
            // find correct cellar resource
            final String cellarId = digitalObject.getCellarId().getIdentifier();
            final CellarResource cellarResource = cellarResourceMapInDb.containsKey(cellarId) ? cellarResourceMapInDb.get(cellarId)
                    : new CellarResource();
            // find corresponding S3 stream
            final Map<ContentType, Model> s3DataStreams = calculatedData.getMetadataSnippetsForS3().get(digitalObject);
            // update and save the cellar resource
            this.updateCellarResource(digitalObject, cellarResource, s3DataStreams);
            this.saveCellarResource(cellarResource, digitalObject, calculatedData.getMetsPackage().getSipType(), operation);
            // update and save the corresponding contentstream
            final Predicate<CellarResource> filterAllButItems = object -> object.getCellarId().startsWith(cellarId + '/');
            this.updateCellarResourceContentStreams(digitalObject, CollectionUtils.select(cellarResourcesInDb, filterAllButItems),
                    calculatedData.getMetsPackage().getSipType(), operation,calculatedData);
        }
    }

    private void removeCellarResources(final CalculatedData calculatedData, final Collection<CellarResource> cellarResources,
                                       final Map<String, CellarResource> cellarResourceMap) {
        for (final DigitalObject removedDigitalObject : calculatedData.getRemovedDigitalObjects()) {
            // find correct cellar resource
            final String cellarId = removedDigitalObject.getCellarId().getIdentifier();
            // delete the resource
            this.deleteCellarResource(cellarResourceMap.get(cellarId), removedDigitalObject,calculatedData.getMetsPackage().getSipType());

            // delete the corresponding contentstreams
            final Predicate<CellarResource> filter = object -> object.getCellarId().startsWith(cellarId + '/');
            this.deleteCellarResources(CollectionUtils.select(cellarResources, filter), removedDigitalObject, calculatedData.getMetsPackage().getSipType());
        }
    }

    private void updateCellarResource(final DigitalObject digitalObject, final CellarResource cellarResource,
                                      final Map<ContentType, Model> dataModel) {
        cellarResource.setCellarId(digitalObject.getCellarId().getIdentifier());
        cellarResource.setLastModificationDate(digitalObject.getLastModificationDate());

        // Set the versions
        cellarResource.setVersions(digitalObject.getVersions());

        this.embeddedNoticeService.refreshEmbeddedNotice(cellarResource, digitalObject, digitalObject.getAllUris(),
                dataModel.get(ContentType.DIRECT_INFERRED), Optional.ofNullable(dataModel.get(ContentType.DECODING))
                        .orElse(ModelFactory.createDefaultModel()));


        // set the correct type and according fields depending on the type
        digitalObject.getType().accept(new DefaultDigitalObjectTypeVisitor<Void, Void>() {

            @Override
            public Void visitWork(final Void in) {
                this.visitDigitalObject(DigitalObjectType.WORK);
                // sets the embargo date for works on PIDService common layer
                // (key-value)
                final Date embargoDate = digitalObject.getEmbargoDate();
                cellarResource.setEmbargoDate(embargoDate);
                return null;
            }

            @Override
            public Void visitExpression(final Void in) {
                // sets the Language
                // for expressions
                // on PIDService
                // common layer
                // (key-value)
                this.visitDigitalObject(DigitalObjectType.EXPRESSION);

                // check the consistency of digitalObject's languages
                digitalObject.checkLanguages();

                // convert languageIscoCode to 3 char code
                final Transformer<LanguageIsoCode, String> languageIsoCodeStringTransformer = new Transformer<LanguageIsoCode, String>() {

                    /** {@inheritDoc} */
                    @Override
                    public String transform(final LanguageIsoCode input) {
                        if (StringUtils.isBlank(input.getIsoCodeThreeChar())) {
                            throw ExceptionBuilder.get(CellarException.class)
                                    .withMessage("Iso 3 char code language cannot be blank.[Expr.: '{}' - '{}']")
                                    .withMessageArgs(digitalObject.getCellarId().getIdentifier(), digitalObject.getCellarId().getUri())
                                    .build();
                        }
                        return input.getIsoCodeThreeChar();
                    }
                };

                // set languages in cellar resource
                cellarResource.setLanguages(
                        CollectionUtils.collect(digitalObject.getLanguages(), languageIsoCodeStringTransformer, new ArrayList<>()));

                cellarResource.setEmbargoDate(digitalObject.getEmbargoDate());
                return null;
            }

            @Override
            public Void visitManifestation(final Void in) {
                this.visitDigitalObject(DigitalObjectType.MANIFESTATION);
                // sets the mime type for manifestations on PIDService common
                // layer (key-value)
                cellarResource.setMimeType(digitalObject.getManifestationMimeType());
                cellarResource.setEmbargoDate(digitalObject.getEmbargoDate());
                return null;
            }

            @Override
            public Void visitDossier(final Void in) {
                cellarResource.setEmbargoDate(digitalObject.getEmbargoDate());
                return this.visitDigitalObject(DigitalObjectType.DOSSIER);
            }

            @Override
            public Void visitEvent(final Void in) {
                cellarResource.setEmbargoDate(digitalObject.getEmbargoDate());
                return this.visitDigitalObject(DigitalObjectType.EVENT);
            }

            @Override
            public Void visitAgent(final Void in) {
                cellarResource.setEmbargoDate(digitalObject.getEmbargoDate());
                return this.visitDigitalObject(DigitalObjectType.AGENT);
            }

            @Override
            public Void visitTopLevelEvent(final Void in) {
                cellarResource.setEmbargoDate(digitalObject.getEmbargoDate());
                return this.visitDigitalObject(DigitalObjectType.TOPLEVELEVENT);
            }

            private Void visitDigitalObject(final DigitalObjectType doType) {
                if (cellarResource.getCellarType() == null) {
                    cellarResource.setCellarType(doType);
                } else if (cellarResource.getCellarType() != doType) {
                    throw ExceptionBuilder.get(CellarException.class).build();
                }
                return null;
            }
        }, null);
    }

    private void updateCellarResourceContentStreams(final DigitalObject digitalObject, final Collection<CellarResource> itemCellarResourcesInDb,
                                                    final TYPE sipType, IOperation<?> operation,CalculatedData calculatedData) {
        final List<ContentStream> contentStreams = digitalObject.getContentStreams();
        // put all contentstreams in a map
        final Map<String, CellarResource> cellarResourceMap = new HashMap<>();
        for (final CellarResource cellarResource : itemCellarResourcesInDb) {
            if (cellarResource.getCellarType() != DigitalObjectType.ITEM) {
                throw ExceptionBuilder.get(CellarException.class).build();
            }
            cellarResourceMap.put(cellarResource.getCellarId(), cellarResource);
        }

        // loop all contentstreams
        for (final ContentStream changedContentStream : contentStreams) {
            final String id = changedContentStream.getCellarId().getIdentifier();
            CellarResource cellarResource;
            if (cellarResourceMap.containsKey(id)) {
                // found: update
                cellarResource = cellarResourceMap.get(id);
                cellarResourceMap.remove(id);
            } else {
                // not found: create
                cellarResource = new CellarResource();
                cellarResource.setCellarId(id);
                cellarResource.setCellarType(DigitalObjectType.ITEM);
            }
            if (StringUtils.trimToNull(changedContentStream.getMimeType()) != null) {
                cellarResource.setMimeType(changedContentStream.getMimeType());
            }
            final Date now = new Date();
            cellarResource.setLastModificationDate(now);
            final Date embargoDate = digitalObject.getEmbargoDate();
            cellarResource.setEmbargoDate(embargoDate);

            // Set the versions
            cellarResource.putVersion(FILE, changedContentStream.getVersion());

            this.saveCellarResource(cellarResource, digitalObject, sipType, operation);
            this.addTechnicalMetadataOfItemsToDb(cellarResource,digitalObject,calculatedData);
        }
        // not in new set: delete
        this.deleteCellarResources(cellarResourceMap.values(), digitalObject, sipType);
    }

    private void saveCellarResource(final CellarResource cellarResource, final DigitalObject digitalObject, final TYPE sipType, IOperation<?> operation) {
        try {
            // After the saving, this check does not work!
            if (cellarResource != null) {
                final ResponseOperationType type = responseOperationTypeResolver.resolve(cellarResource, operation);
                final FeedItemType actionType = type == ResponseOperationType.CREATE ? FeedItemType.create : FeedItemType.update;
                final FeedItemType feedActionType = resolveFeedActionType(cellarResource, actionType);

                this.cellarResourceDao.saveOrUpdateObject(cellarResource);
                this.storeIngestionHistoryElement(cellarResource, digitalObject, type, actionType, feedActionType, sipType);
            }
        } catch (final RuntimeException e) {
            LOG.error(MessageFormatter.format("Unexpected exception while saving {}: {}", cellarResource, e.getMessage()), e);
            throw e;
        }
    }

    private void deleteCellarResources(final Collection<CellarResource> cellarResources, DigitalObject digitalObject, final TYPE sipType) {
        for (final CellarResource cellarResource : cellarResources) {
            this.deleteCellarResource(cellarResource, digitalObject, sipType);
        }
    }
    private void deleteItemTechnicalMetadataFromDb(String manifestationCellarId){
        cmrItemTechnicalMdRepository.deleteAllByCellarIdContaining(manifestationCellarId);
    }

    private void deleteCellarResource(final CellarResource cellarResource, final DigitalObject digitalObject, final TYPE sipType) {
        try {
            if (cellarResource != null) {
                contentStreamService.deleteContent(cellarResource);
                deleteItemTechnicalMetadataFromDb(cellarResource.getCellarId());
                storeIngestionHistoryElement(cellarResource, digitalObject, ResponseOperationType.DELETE, FeedItemType.delete, FeedItemType.delete, sipType);
            }
        } catch (final RuntimeException e) {
            LOG.error(MessageFormatter.format("Unexpected exception while deleting {}: {}", cellarResource, e.getMessage()), e);
            throw e;
        }
    }

    /**
     * Write ingestion history element.
     *
     * @param cellarResource the cellar resource
     * @param actionType     the action type
     */
    private void storeIngestionHistoryElement(final CellarResource cellarResource, final DigitalObject digitalObject, final ResponseOperationType responseOperationType,
                                              final FeedItemType actionType, final FeedItemType feedActionType, final TYPE sipType) {
        final IngestionHistory historyObject = new IngestionHistory(cellarResource.getCellarId());

        historyObject.setWemiClass(cellarResource.getCellarType());
        historyObject.setRelatives(MetadataUtils.isRelatedToChange(digitalObject, cellarResource, actionType, responseOperationType));
        historyObject.setActionType(actionType);
        historyObject.setFeedActionType(feedActionType);
        historyObject.setPriority(FeedPriority.fromSIPType(sipType));
        historyObject.setType(cellarResource.getMimeType());
        historyObject.setClasses(String.join(",", getOwlClasses(cellarResource)));
        historyObject.setIdentifiers(String.join(",", this.historyService.getProductionIdentifierNamesForCellarId(cellarResource.getCellarId())));
        this.historyService.store(historyObject);

        historyService.store(historyObject);
        applicationEventPublisher.publishEvent(historyObject);
    }

    /**
     * Resolves feed action type
     *
     * @param cellarResource the cellar resource
     * @param actionType     the action type
     * @return the desired FeedItemType
     */
    private FeedItemType resolveFeedActionType(final CellarResource cellarResource, FeedItemType actionType) {
        boolean wasUnderEmbargo = this.cellarResourceDao.isUnderEmbargo(cellarResource.getCellarId());
        boolean willBeUnderEmbargo = cellarResource.isUnderEmbargo();

        if(wasUnderEmbargo||willBeUnderEmbargo){
            ThreadContext.put("is_embargo","true");
        }

        if(actionType == FeedItemType.update && wasUnderEmbargo && !willBeUnderEmbargo) {
            return FeedItemType.create;
        }
        if(actionType == FeedItemType.update && !wasUnderEmbargo && willBeUnderEmbargo) {
            return FeedItemType.delete;
        }
        if(actionType == FeedItemType.create && willBeUnderEmbargo) {
            return FeedItemType.no_feed;
        }
        if(actionType == FeedItemType.update && wasUnderEmbargo) {
            return FeedItemType.no_feed;
        }

        return actionType;
    }

    private Collection<String> getOwlClasses(CellarResource resource) {
        String version = null;
        if (resource.getCellarType() != DigitalObjectType.ITEM) {
            version = resource.getVersion(ContentType.DIRECT_INFERRED)
                    .orElse(null);
            LOG.debug(IConfiguration.INGESTION, "Inferred version ({}) for {}", version, resource.getCellarId());
        }
        return historyService.getOwlClassesForCellarId(resource.getCellarId(), version);
    }

    private void updateInverseTablesAndCalculateRelationChanges(final CalculatedData calculatedData) {
        // get all uris out of the structmap
        final Collection<String> structMapUris = new HashSet<>();
        for (final DigitalObject digitalObject : calculatedData.getStructMap().getDigitalObject().getAllChilds(true)) {
            structMapUris.addAll(digitalObject.getAllUris());
        }

        // update all related inverse relations
        for (final Map.Entry<DigitalObject, Map<ContentType, Model>> digitalObjectMapEntry : calculatedData
                .getMetadataSnippetsForS3().entrySet()) {
            final Map<ContentType, Model> metadataMap = digitalObjectMapEntry.getValue();
            this.updateInverseRelations(digitalObjectMapEntry.getKey(), structMapUris,
                    metadataMap.get(ContentType.DIRECT_INFERRED), metadataMap.get(ContentType.INVERSE));
        }

        // remove all inverse relations of digital objects that are removed
        for (final DigitalObject removedDigitalObject : calculatedData.getRemovedDigitalObjects()) {
            final Collection<InverseRelation> relationsByTarget = this.inverseRelationDao
                    .findRelationsByTarget(removedDigitalObject.getCellarId().getIdentifier());
            for (final InverseRelation relationByTarget : relationsByTarget) {
                this.inverseRelationDao.deleteObject(relationByTarget);
            }
        }
    }

    /**
     * <p>
     * updateInverseRelations add or remove an inverse relations according to
     * the digital object.
     * </p>
     *
     * @param digitalObject a
     *                      {@link DigitalObject}
     *                      object.
     * @param structMapUris a {@link Collection} object.
     * @param directModel   a {@link Model} object.
     * @param inverseModel  a {@link Model} object.
     */
    private void updateInverseRelations(final DigitalObject digitalObject, final Collection<String> structMapUris,
                                        final Model directModel, final Model inverseModel) {
        if (inverseModel == null) {
            throw ExceptionBuilder.get(CellarException.class).build();
        }

        // find all inverse relations that where defined earlier
        final Collection<InverseRelation> relationsByTarget = this.inverseRelationDao
                .findRelationsByTarget(digitalObject.getCellarId().getIdentifier());
        final Map<String, InverseRelation> sourceAndRelation = new HashMap<>();
        for (final InverseRelation inverseRelation : relationsByTarget) {
            sourceAndRelation.put(inverseRelation.getSource(), inverseRelation);
        }
        // make collection that contains all these relations
        final Collection<InverseRelation> relationsToRemove = new HashSet<>(relationsByTarget);

        // find all new related objects
        final Set<RelatedObject> directRelations = this.relatedObjectCalculator.calculateInverseRelatedObjects(inverseModel,
                digitalObject.getCellarId().getUri());
        for (final RelatedObject directRelation : directRelations) {
            // check that the relations is one to an resource that needs to be
            // defined in another structmap
            if (inverseModel.contains(inverseModel.getResource(directRelation.getResourceUri()), RDF.type, CellarType.skos_ConceptR) ||
                    directRelation.getResourceUri().contains("resource/authority") ||
                    structMapUris.contains(directRelation.getResourceUri())) {
                continue;
            }
            final String prefixedId = this.identifierService.getPrefixed(directRelation.getResourceUri());

            // update or create the relation
            InverseRelation inverseRelation;
            if (sourceAndRelation.containsKey(prefixedId)) {
                // use the existing relation and mark as not to remove
                inverseRelation = sourceAndRelation.get(prefixedId);
                relationsToRemove.remove(inverseRelation);
            } else {
                // create new relation and mark as change
                inverseRelation = new InverseRelation();
                inverseRelation.setSource(prefixedId);
                inverseRelation.setSourceType(directRelation.getType());
                inverseRelation.setTarget(digitalObject.getCellarId().getIdentifier());
                inverseRelation.setTargetType(digitalObject.getType());

                sourceAndRelation.put(prefixedId, inverseRelation);
            }
            // update and save relation information
            this.setPropertiesInRelation(inverseRelation, digitalObject, directRelation, directModel, inverseModel);
            this.saveInverseRelation(inverseRelation);
        }
        // delete the relations that are no more, and indicate the change
        this.deleteInverseRelations(relationsToRemove);
    }

    /**
     * Gets the inverse relations.
     *
     * @param cellarId            the cellar id
     * @param digitalObjectType   the digital object type
     * @param metadataUris        the metadata uris
     * @param sameAsUris          the same as uris
     * @param directInferredModel the direct inferred model
     * @param inverseModel        the inverse model
     * @return the inverse relations
     */
    public Collection<InverseRelation> getInverseRelations(final String cellarId, final DigitalObjectType digitalObjectType,
                                                           final Set<String> metadataUris, final Set<String> sameAsUris, final Model directInferredModel, final Model inverseModel) {
        final String cellarIdUri = this.identifierService.getUri(cellarId);

        final Map<String, InverseRelation> inverseRelationsBySource = new HashMap<>();

        InverseRelation inverseRelation;
        // find all new related objects
        final Set<RelatedObject> directRelations = this.relatedObjectCalculator.calculateInverseRelatedObjects(inverseModel, cellarIdUri);
        for (final RelatedObject directRelation : directRelations) {
            // check that the relations is one to an resource that needs to be
            // defined in another structmap
            if (inverseModel.contains(inverseModel.getResource(directRelation.getResourceUri()), RDF.type, CellarType.skos_ConceptR) ||
                    directRelation.getResourceUri().contains("resource/authority") ||
                    metadataUris.contains(directRelation.getResourceUri())) {
                continue;
            }
            final String prefixedId = this.identifierService.getPrefixed(directRelation.getResourceUri());

            if (!inverseRelationsBySource.containsKey(prefixedId)) {
                inverseRelation = new InverseRelation();
                inverseRelation.setSource(prefixedId);
                inverseRelation.setSourceType(directRelation.getType());
                inverseRelation.setTarget(cellarId);
                inverseRelation.setTargetType(digitalObjectType);

                inverseRelationsBySource.put(prefixedId, inverseRelation);
            } else {
                inverseRelation = inverseRelationsBySource.get(prefixedId);
            }

            this.setPropertiesInRelation(inverseRelation, sameAsUris, directRelation, directInferredModel, inverseModel);
        }

        return inverseRelationsBySource.values();
    }

    /**
     * Sets the properties in relation.
     *
     * @param inverseRelation the inverse relation
     * @param sameAsUris      the same as uris
     * @param directRelation  the direct relation
     * @param directModel     the direct model
     * @param inverseModel    the inverse model
     */
    private void setPropertiesInRelation(final InverseRelation inverseRelation, final Collection<String> sameAsUris,
                                         final RelatedObject directRelation, final Model directModel, final Model inverseModel) {
        final Set<String> propertiesSourceTarget = new HashSet<>();
        final Set<String> propertiesTargetSource = new HashSet<>();
        final Resource directRelationR = ResourceFactory.createResource(directRelation.getResourceUri());
        for (final String resource : sameAsUris) {
            final Resource resourceR = ResourceFactory.createResource(resource);
            // get direct and inverse properties
            propertiesSourceTarget.addAll(this.getProperties(inverseModel, directRelationR, resourceR));
            propertiesTargetSource.addAll(this.getProperties(directModel, resourceR, directRelationR));
        }
        // save all properties from all different identifiers
        inverseRelation.setPropertiesSourceTarget(propertiesSourceTarget);
        inverseRelation.setPropertiesTargetSource(propertiesTargetSource);
    }

    /**
     * <p>
     * setPropertiesInRelation fills in the names of the propertyUris that
     * indicate the relation.
     * </p>
     *
     * @param inverseRelation a
     *                        {@link eu.europa.ec.opoce.cellar.cmr.daobeans.InverseRelation}
     *                        object.
     * @param digitalObject   a
     *                        {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject}
     *                        object.
     * @param directRelation  a {@link eu.europa.ec.opoce.cellar.cmr.RelatedObject} object.
     * @param directModel     a {@link Model} object.
     * @param inverseModel    a {@link Model} object.
     */
    private void setPropertiesInRelation(final InverseRelation inverseRelation, final DigitalObject digitalObject,
                                         final RelatedObject directRelation, final Model directModel, final Model inverseModel) {
        this.setPropertiesInRelation(inverseRelation, digitalObject.getAllUris(), directRelation, directModel, inverseModel);
    }

    /**
     * <p>
     * getProperties.
     * </p>
     *
     * @param model a {@link Model} object.
     * @param from  a {@link Resource} object.
     * @param to    a {@link Resource} object.
     * @return a {@link java.util.List} object.
     */
    private List<String> getProperties(final Model model, final Resource from, final Resource to) {
        return CollectionUtils.collect(model.listStatements(from, null, to).toSet(), input -> input.getPredicate().getURI(), new ArrayList<>());
    }

    /**
     * <p>
     * saveInverseRelation.
     * </p>
     *
     * @param inverseRelation a
     *                        {@link eu.europa.ec.opoce.cellar.cmr.daobeans.InverseRelation}
     *                        object.
     */
    private void saveInverseRelation(final InverseRelation inverseRelation) {
        try {
            if (inverseRelation != null) {
                this.inverseRelationDao.saveOrUpdateObject(inverseRelation);
            }
        } catch (final RuntimeException e) {
            LOG.error(MessageFormatter.format("Unexpected exception while saving {}: {}", inverseRelation, e.getMessage()), e);
            throw e;
        }
    }

    /**
     * <p>
     * deleteInverseRelations.
     * </p>
     *
     * @param inverseRelations a {@link java.util.Collection} object.
     */
    private void deleteInverseRelations(final Collection<InverseRelation> inverseRelations) {
        for (final InverseRelation inverseRelation : inverseRelations) {
            this.deleteInverseRelation(inverseRelation);
        }
    }

    /**
     * <p>
     * deleteInverseRelation.
     * </p>
     *
     * @param inverseRelation a
     *                        {@link eu.europa.ec.opoce.cellar.cmr.daobeans.InverseRelation}
     *                        object.
     */
    private void deleteInverseRelation(final InverseRelation inverseRelation) {
        try {
            if (inverseRelation != null) {
                this.inverseRelationDao.deleteObject(inverseRelation);
            }
        } catch (final RuntimeException e) {
            LOG.error(MessageFormatter.format("Unexpected exception while deleting {}: {}", inverseRelation, e.getMessage()), e);
            throw e;
        }
    }

    /**
     * <p>
     * updateOracleRdfTables.
     * </p>
     *
     * @param calculatedData a {@link eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData}
     *                       object.
     */
    private void updateOracleRdfTables(final CalculatedData calculatedData) {
        this.modelQuadService.updateDataInOracle(calculatedData);
    }

    /**
     * Embargoed cellar ids.
     *
     * @param calculatedData the calculated data
     * @return the sets the
     */
    private Set<String> embargoedCellarIds(final CalculatedData calculatedData) {
        final DigitalObject digitalObject = calculatedData.getStructMap().getDigitalObject();
        LOG.debug(VIRTUOSO, "Find embargoed cellar IDs");
        // TODO why not use child.getEmbargoDate directly?
        return digitalObject.getAllChilds(true).stream()
                .map(this::getResource)
                .filter(r -> {
                    if (r != null) {
                        boolean embargo = r.isUnderEmbargo();
                        LOG.debug(VIRTUOSO, "Resource {} is {} under embargo (embargoDate = {})",
                                r.getCellarId(), (embargo ? "" : "NOT"), r.getEmbargoDate());
                        return embargo;
                    }
                    return false;
                })
                .map(CellarResource::getCellarId)
                .collect(Collectors.toSet());
    }

    /**
     * Dis embargoed cellar ids.
     *
     * @param calculatedData the calculated data
     * @return the sets the
     */
    private Set<String> disEmbargoedCellarIds(final CalculatedData calculatedData) {
        final DigitalObject digitalObject = calculatedData.getStructMap().getDigitalObject();

        return digitalObject.getAllChilds(true).stream()
                .map(this::getResource)
                // FIXME SV
                // - wait for automatic disembargo (cron job)
                // - why only Virtuoso and not S3 or Oracle
                .filter(r -> !r.isUnderEmbargo() && virtuosoService.isUnderEmbargo(r.getCellarId()))
                .map(CellarResource::getCellarId)
                .collect(Collectors.toSet());
    }

    private CellarResource getResource(final DigitalObject digitalObject) {
        final ContentIdentifier cellarId = digitalObject.getCellarId();
        if (cellarId == null) {
            return null;
        }

        final String identifier = cellarId.getIdentifier();
        if (identifier != null) {
            return cellarResourceDao.findCellarId(identifier);
        }

        return null;
    }
}
