/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.transformers
 *             FILE : IscoCode2LanguageBean.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 25-Jan-2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.transformers;

import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.ccr.service.LanguagesNalSkosLoaderService;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;

import org.apache.commons.collections15.Transformer;

/**
 * <p>IscoCode2LanguageBean class.</p>
 *
 * @author ARHS Developments
 * @version $$Revision$$$
 */
public class IscoCode2LanguageBean implements Transformer<String, LanguageBean> {

    /** The languages nal skos loader service. */
    private final LanguagesNalSkosLoaderService languagesNalSkosLoaderService;

    /**
     * <p>Constructor for IscoCode2LanguageBean.</p>
     *
     * @param languagesNalSkosLoaderService the languages nal skos loader service
     */
    public IscoCode2LanguageBean(final LanguagesNalSkosLoaderService languagesNalSkosLoaderService) {
        this.languagesNalSkosLoaderService = languagesNalSkosLoaderService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LanguageBean transform(final String input) {
        if (input.length() == 2) {
            return this.languagesNalSkosLoaderService.getLanguageBeanByTwoCharCode(input);
        } else if (input.length() == 3) {
            return this.languagesNalSkosLoaderService.getLanguageBeanByThreeCharCode(input);
        } else {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.CMR_INTERNAL_ERROR_UNKNOWN_LANGUAGE)
                    .withMessage("Language with code '{}' not found").withMessageArgs(input).build();
        }
    }
}
