package eu.europa.ec.opoce.cellar.cmr.database.dao;

import eu.europa.ec.opoce.cellar.cl.dao.BaseDao;
import eu.europa.ec.opoce.cellar.cmr.daobeans.InverseRelation;
import eu.europa.ec.opoce.cellar.common.util.Pair;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;

import java.util.Collection;
import java.util.List;

/**
 * <p>InverseRelationDao interface.</p>
 */
public interface InverseRelationDao extends BaseDao<InverseRelation, Long> {

    /**
     * <p>findRelationsBySource.</p>
     *
     * @param sourcePsId a {@link java.lang.String} object.
     * @return a {@link java.util.Collection} object.
     */
    Collection<InverseRelation> findRelationsBySource(final String sourcePsId);

    /**
     * <p>findRelationsBySourcesAndType.</p>
     *
     * @param sourcePsIds a {@link java.util.List} object.
     * @return a {@link java.util.Collection} object.
     */
    Collection<InverseRelation> findRelationsBySources(final Collection<String> sourcePsIds);

    /**
     * <p>findRelationsBySourcesAndType.</p>
     *
     * @param sourcePsIds a {@link java.util.List} object.
     * @param doType a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType} object.
     * @param limit the max number of rows to retrieve. <code>0</code> or negative value means no limit.
     * @return               a {@link eu.europa.ec.opoce.cellar.common.util.Pair} object with the result on <code>one</code>,
     *                       and a boolean which indicates whether the result has been truncated or not on <code>two</code>.
     */
    Pair<Collection<InverseRelation>, Boolean> findRelationsBySourcesAndType(final List<String> sourcePsIds, final DigitalObjectType doType,
            final int limit);

    /**
     * <p>findRelationsByTarget.</p>
     *
     * @param targetCellarId a {@link java.lang.String} object.
     * @return a {@link java.util.Collection} object.
     */
    Collection<InverseRelation> findRelationsByTarget(final String targetCellarId);

    /**
     * <p>findRelationsByTargetsAndType.</p>
     *
     * @param targetPsIds a {@link java.util.List} object.
     * @return a {@link java.util.Collection} object.
     */
    Collection<InverseRelation> findRelationsByTargets(final Collection<String> targetPsIds);

    /**
     * <p>findRelationsByTargetsAndType.</p>
     *
     * @param targetPsIds a {@link java.util.List} object.
     * @param doType a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType} object.
     * @param limit the max number of rows to retrieve. <code>0</code> or negative value means no limit.
     * @return               a {@link eu.europa.ec.opoce.cellar.common.util.Pair} object with the result on <code>one</code>,
     *                       and a boolean which indicates whether the result has been truncated or not on <code>two</code>.
     */
    Pair<Collection<InverseRelation>, Boolean> findRelationsByTargetsAndType(final List<String> targetPsIds, final DigitalObjectType doType,
            final int limit);

}
