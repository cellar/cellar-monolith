/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.database.dao.spring
 *             FILE : SpringCmrIndexRequestBatchDao.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Nov 10, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.database.dao.spring;

import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService.Priority;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CmrIndexRequestBatchDao;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.ExecutionStatus;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestBatch;
import eu.europa.ec.opoce.cellar.semantic.helper.StringHelper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Nov 10, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Repository
public class SpringCmrIndexRequestBatchDao extends CmrSpringBaseDao<CmrIndexRequestBatch, Long>
        implements QueryAndTableCmrIndexRequest, CmrIndexRequestBatchDao {

    public static final String COLUMN_NAMES_PART = StringHelper.format("{}, max({}) as {}, min({}) as {}", Column.GROUP_BY_URI,
            Column.PRIORITY, Column.PRIORITY, Column.CREATED_ON, Column.CREATED_ON);

    public static final String GROUP_BY_PART = StringHelper.format("{}", Column.GROUP_BY_URI);

    public static final String ORDER_BY_PART = StringHelper.format("{} desc, {} asc", Column.PRIORITY, Column.CREATED_ON);

    public SpringCmrIndexRequestBatchDao() {
        super(TABLE_NAME,
                Arrays.asList( //
                        Column.GROUP_BY_URI, //
                        Column.CREATED_ON, //
                        Column.PRIORITY));
    }

    @Override
    public Collection<CmrIndexRequestBatch> findRequests(final ExecutionStatus executionStatus, final Priority minimumPriority,
                                                         final Date maximumCreatedOn, final int limit) {
        final Map<String, Object> args = new HashMap<>(3);
        args.put("executionStatus", executionStatus.getValue());
        args.put("minimumPriority", minimumPriority.getPriorityValue());
        args.put("maximumCreatedOn", maximumCreatedOn);
        return find(WHERE_PART_EXECUTION_STATUS_PRIORITY_CREATED_ON, GROUP_BY_PART, ORDER_BY_PART, args, limit);
    }

    protected final Collection<CmrIndexRequestBatch> find(final String whereQueryPart, final String groupByPart, final String orderByPart,
            final Map<String, Object> map, final int limit) {
        final String query = StringHelper.format(
                "select a.*, rownum as ID from (select {} from {} where {} group by {} order by {}) a where rownum <= {}",
                COLUMN_NAMES_PART, TABLE_NAME, whereQueryPart, groupByPart, orderByPart, limit);
        return this.getNamedParameterJdbcTemplate().query(query, map, this);
    }

    /* (non-Javadoc)
     * @see eu.europa.ec.opoce.cellar.cmr.database.dao.spring.SpringBaseDao#create(java.sql.ResultSet)
     */
    @Override
    public CmrIndexRequestBatch create(final ResultSet rs) throws SQLException {
        CmrIndexRequestBatch batch = new CmrIndexRequestBatch();
        batch.setGroupByUri(rs.getString(Column.GROUP_BY_URI));
        batch.setCreatedOn(rs.getTimestamp(Column.CREATED_ON));
        batch.setPriority(Priority.findByPriorityValue(rs.getInt(Column.PRIORITY)));
        return batch;
    }

    /* (non-Javadoc)
     * @see eu.europa.ec.opoce.cellar.cmr.database.dao.spring.SpringBaseDao#fillMap(eu.europa.ec.opoce.cellar.cmr.database.DaoObject, java.util.Map)
     */
    @Override
    public void fillMap(final CmrIndexRequestBatch daoObject, final Map<String, Object> map) {
        map.put(Column.GROUP_BY_URI, daoObject.getGroupByUri());
        map.put(Column.CREATED_ON, daoObject.getCreatedOn());
        map.put(Column.PRIORITY, daoObject.getPriority().getPriorityValue());
    }

}
