/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.admin.lastmodification
 *             FILE : LastModificationDateUpdateServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 7, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-06-07 07:49:00 +0200 (Wed, 07 Jun 2017) $
 *          VERSION : $LastChangedRevision: $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.admin.lastmodification;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditBuilder;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.model.ModelUtils;
import eu.europa.ec.opoce.cellar.cmr.quad.ModelQuadService;
import eu.europa.ec.opoce.cellar.database.CmrTableName;
import eu.europa.ec.opoce.cellar.database.IRDFStoreRelationalGateway;
import eu.europa.ec.opoce.cellar.database.transaction.RDFTransactional;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.DigitalObjectNotFoundException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import eu.europa.ec.opoce.cellar.virtuoso.util.VirtuosoUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.update.UpdateAction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static eu.europa.ec.opoce.cellar.cl.domain.audit.Auditable.LogLevel.INFO;
import static eu.europa.ec.opoce.cellar.cmr.ContentType.DIRECT;
import static eu.europa.ec.opoce.cellar.cmr.ContentType.DIRECT_INFERRED;
import static eu.europa.ec.opoce.cellar.cmr.ContentType.extract;
import static eu.europa.ec.opoce.cellar.common.util.CellarIdUtils.getParentCellarId;
import static eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType.isItem;

/**
 * @author ARHS Developments
 */
@Service
class LastModificationDateUpdateServiceImpl implements LastModificationDateUpdateService {

    private static final Logger LOG = LogManager.getLogger(LastModificationDateUpdateServiceImpl.class);

    private static final String UPDATE_QUERY = "PREFIX cmr:<http://publications.europa.eu/ontology/cdm/cmr#> " +
            "PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> " +
            "DELETE { ?date cmr:lastModificationDate ?o } " +
            "INSERT { ?date cmr:lastModificationDate \"_MODIFICATION_DATE_\"^^xsd:dateTime } " +
            "WHERE  { ?date cmr:lastModificationDate ?o } ";

    private final CellarResourceDao cellarResourceDao;
    private final IdentifierService identifierService;
    private final DataSource virtuosoDataSource;
    private final ModelQuadService modelQuadService;
    private final IRDFStoreRelationalGateway rdfStoreRelationalGateway;
    private final ContentStreamService contentStreamService;

    @Autowired
    LastModificationDateUpdateServiceImpl(CellarResourceDao cellarResourceDao,
                                          @Qualifier("pidManagerService") IdentifierService identifierService,
                                          @Qualifier("virtuosoDataSource") DataSource virtuosoDataSource,
                                          @Qualifier("modelQuadService") ModelQuadService modelQuadService,
                                          IRDFStoreRelationalGateway rdfStoreRelationalGateway, ContentStreamService contentStreamService) {
        this.cellarResourceDao = cellarResourceDao;
        this.identifierService = identifierService;
        this.virtuosoDataSource = virtuosoDataSource;
        this.modelQuadService = modelQuadService;
        this.rdfStoreRelationalGateway = rdfStoreRelationalGateway;
        this.contentStreamService = contentStreamService;
    }

    @Transactional
    @RDFTransactional
    @Override
    public void updateLastModificationDate(String cellarId, Date lastModificationDate) {
        if (Strings.isNullOrEmpty(cellarId) || lastModificationDate == null) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("Impossible to update cellarId: [{}] with value [{}]")
                    .withMessageArgs(cellarId, lastModificationDate)
                    .build();
        }
        final String id = cellarId.startsWith("cellar:") ? cellarId : "cellar:" + cellarId;

        long start = System.currentTimeMillis();
        AuditBuilder.get()
                .withMessage("Start updating [{}] with value [{}]")
                .withMessageArgs(cellarId, lastModificationDate)
                .withLogger(LOG)
                .withLogLevel(INFO)
                .logEvent();

        // Always update CMR_CELLAR_RESOURCE_MD
        CellarResource resource = updateOracle(id, lastModificationDate);

        if (!id.contains("/")) { // Update everything but ITEMS
            updateOracleRDF(id, lastModificationDate);
            updateS3(resource, lastModificationDate);
            updateVirtuoso(id, lastModificationDate);
        }

        AuditBuilder.get()
                .withMessage("End updating [{}] with value [{}] in [{}] ms")
                .withMessageArgs(cellarId, lastModificationDate, System.currentTimeMillis() - start)
                .withLogger(LOG)
                .withLogLevel(INFO)
                .logEvent();
    }

    private CellarResource updateOracle(String cellarId, Date lastModificationDate) {
        final CellarResource resource = requireNotNull(cellarResourceDao.findCellarId(cellarId), cellarId, "CMR_CELLAR_RESOURCE_MD");
        resource.setLastModificationDate(lastModificationDate);
        return cellarResourceDao.updateObject(resource);
    }

    private void updateOracleRDF(String cellarId, Date lastModificationDate) {
        // Retrieve models
        final Model model = getModelFromOracleRDF(cellarId, CmrTableName.CMR_METADATA);
        final Model modelInverse = getModelFromOracleRDF(cellarId, CmrTableName.CMR_INVERSE);

        // Update models
        updateModel(model, lastModificationDate);
        updateModel(modelInverse, lastModificationDate);

        final Map<String, Model> mapModel = Maps.newHashMap();
        mapModel.put(cellarId, model);

        final Map<String, Model> mapModelInverse = Maps.newHashMap();
        mapModelInverse.put(cellarId, model);

        // Delete + Insert in Oracle RDF
        // Warning: If this method is not enclosed in a transaction (@RDFTransactional),
        // subsequent calls to that method will produce a deadlock.
        modelQuadService.updateDataInOracle(false, Lists.newArrayList(cellarId), mapModel, mapModelInverse);
    }

    private static void updateModel(Model model, Date lastModificationDate) {
        final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
        final String query = UPDATE_QUERY.replace("_MODIFICATION_DATE_", dateFormat.format(lastModificationDate));
        // Update the model with the modification date
        UpdateAction.parseExecute(query, model);
    }

    private void updateVirtuoso(String cellarId, Date lastModificationDate) {
        VirtuosoUtils.updateLastModificationDate(cellarId, lastModificationDate, virtuosoDataSource);
    }


    private void updateS3(CellarResource resource, Date lastModificationDate) {
        Model directModel = null, directInferredModel = null;
        try {

            final String streamCellarId = isItem(resource.getCellarId()) ? getParentCellarId(resource.getCellarId()) : resource.getCellarId();
            Map<ContentType, String> models = contentStreamService.getContentsAsString(streamCellarId, extract(resource.getVersions(), DIRECT, DIRECT_INFERRED));

            directModel = JenaUtils.read(models.get(DIRECT), Lang.NT);
            directInferredModel = JenaUtils.read(models.get(DIRECT_INFERRED), Lang.NT);

            // Update lastModificationDate
            final String cellarIdUri = this.identifierService.getUri(resource.getCellarId());
            final List<String> lastModificationDateProperties = Collections.singletonList(CellarProperty.cmr_lastmodificationdate);
            ModelUtils.updateDateModel(cellarIdUri, lastModificationDate, directModel, lastModificationDateProperties);
            ModelUtils.updateDateModel(cellarIdUri, lastModificationDate, directInferredModel, lastModificationDateProperties);

            // Set updated models
            final ByteArrayResource direct = new ByteArrayResource(JenaUtils.bytes(directModel, RDFFormat.NT));
            String versionDirect = contentStreamService.writeContent(streamCellarId, direct, DIRECT);
            final ByteArrayResource inferred = new ByteArrayResource(JenaUtils.bytes(directInferredModel, RDFFormat.NT));
            String versionDirectInferred = contentStreamService.writeContent(streamCellarId, inferred, DIRECT_INFERRED);

            // Update Oracle with the new versions
            resource.putVersion(DIRECT, versionDirect);
            resource.putVersion(DIRECT_INFERRED, versionDirectInferred);
            cellarResourceDao.updateObject(resource);


        } finally {
            JenaUtils.closeQuietly(directModel, directInferredModel);
        }
    }

    private Model getModelFromOracleRDF(String cellarId, CmrTableName tableName) {
        Model model = null;
        if (rdfStoreRelationalGateway.isContextUsedInTable(tableName.getNormalTable(), cellarId)) {
            model = rdfStoreRelationalGateway.selectModel(tableName.getNormalTable(), cellarId);
        } else if (rdfStoreRelationalGateway.isContextUsedInTable(tableName.getEmbargoTable(true), cellarId)) {
            model = rdfStoreRelationalGateway.selectModel(tableName.getEmbargoTable(true), cellarId);
        }
        return requireNotNull(model, cellarId, tableName.name());
    }

    private static <T> T requireNotNull(T digitalObject, String cellarId, String repository) {
        if (digitalObject == null) {
            throw ExceptionBuilder.get(DigitalObjectNotFoundException.class)
                    .withMessage("[{}] not found in {}")
                    .withMessageArgs(cellarId, repository)
                    .build();
        }
        return digitalObject;
    }

}
