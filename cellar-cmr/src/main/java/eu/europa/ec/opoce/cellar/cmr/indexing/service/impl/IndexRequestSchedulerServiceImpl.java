/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.indexing.service.impl
 *             FILE : IndexRequestSchedulingServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Nov 7, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.indexing.service.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration;
import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService.Priority;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CmrIndexRequestBatchDao;
import eu.europa.ec.opoce.cellar.cmr.indexing.service.IIndexRequestProcessorService;
import eu.europa.ec.opoce.cellar.cmr.indexing.service.IIndexRequestSchedulerService;
import eu.europa.ec.opoce.cellar.cmr.indexing.service.IndexRequestManagerService;
import eu.europa.ec.opoce.cellar.cmr.indexing.service.RedundantIndexRequestHandler;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.ExecutionStatus;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestBatch;
import eu.europa.ec.opoce.cellar.logging.LogContext;
import org.apache.commons.lang.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.EnumMap;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration.INDEXATION;
import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.CMR_INDEXING;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Nov 7, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service("indexRequestSchedulerService")
public class IndexRequestSchedulerServiceImpl implements IIndexRequestSchedulerService {

    private static final Logger LOG = LogManager.getLogger(IndexRequestSchedulerServiceImpl.class);

    private final ICellarConfiguration cellarConfiguration;

    private final CmrIndexRequestBatchDao cmrIndexRequestBatchDao;

    private final IndexRequestManagerService indexRequestManagerService;

    private final IIndexRequestProcessorService indexRequestProcessorService;
    
    private final RedundantIndexRequestHandler redundantIndexRequestHandler;

    
    @Autowired
    public IndexRequestSchedulerServiceImpl(@Qualifier("cellarConfiguration") ICellarConfiguration cellarConfiguration,
                                            CmrIndexRequestBatchDao cmrIndexRequestBatchDao, IndexRequestManagerService indexRequestManagerService,
                                            IIndexRequestProcessorService indexRequestProcessorService, RedundantIndexRequestHandler redundantIndexRequestHandler) {
        this.cellarConfiguration = cellarConfiguration;
        this.cmrIndexRequestBatchDao = cmrIndexRequestBatchDao;
        this.indexRequestManagerService = indexRequestManagerService;
        this.indexRequestProcessorService = indexRequestProcessorService;
        this.redundantIndexRequestHandler = redundantIndexRequestHandler;
    }

    @PostConstruct
    public void init() {
        // automatic recovery: reset the index requests in Pending or Execution
        int rows = indexRequestManagerService.resetPendingExecutionCmrIndexRequests();
        LOG.info(IConfiguration.CONFIG, "{} CmrIndexRequests reset.", rows);
    }

    @Override
    public void forceScheduleIndexRequests() {
        LOG.info("Force index requests scheduling.");
        // if the indexing is disabled, the indexing is not forced!
        scheduleIndexRequests(true);
    }

    @Override
    @LogContext(CMR_INDEXING)
    @Async
    public void forceScheduleIndexRequestsAsynchronously() {
        LOG.info("Force index requests scheduling.");

        // if the indexing is disabled, the indexing is not forced!
        scheduleIndexRequests(true);
    }

    /**
     * {@inheritDoc}
     * Configured in scheduler-context.xml
     */
    @LogContext(CMR_INDEXING)
    @Override
    public void scheduleIndexRequests() {
        try {
            scheduleIndexRequests(false);
        } catch (Exception schedulingException) {
            LOG.error("Unexpected exception during index requests scheduling.", schedulingException);
        }
    }

    /**
     * Schedule index requests.
     *
     * @param force true if the method will eagerly search for other request
     *              to handle after submitting the results of the first execution.
     */
    private void scheduleIndexRequests(final boolean force) {
        if (!cellarConfiguration.isCellarServiceIndexingEnabled()) {
            indexRequestProcessorService.pause();
            return;
        } else {
            indexRequestProcessorService.resume();
        }

        synchronized (this) {
        	this.redundantIndexRequestHandler.detectAndUpdateRedundantRequests();
            StringBuilder sb = new StringBuilder("Index requests: { ");
            Collection<CmrIndexRequestBatch> batches = getNextCmrIndexRequestBatches(force);
            if (LOG.isDebugEnabled()) {
                sb.append("CmrIndexRequestBatch found:").append(summarize(batches)).append(" -> ");
            }
            while (!batches.isEmpty()) {
                try {
                    LOG.info("Start scheduling {} index notice request(s).", batches.size());
                    for (CmrIndexRequestBatch batch : batches) {
                        batch.setCmrIndexRequests(indexRequestManagerService.findIndexRequests(batch)
                                .stream()
                                .sorted(Comparator.<CmrIndexRequest>comparingInt(o -> o.getPriority().getPriorityValue()).reversed()) // HIGH priorities first
                                .collect(Collectors.toList()));

                        sb.append(detailBatch(batch));

                        indexRequestManagerService.updateBatchStatus(batch, ExecutionStatus.Pending);
                        indexRequestManagerService.updateStructMapIndxStatusAndRespectiveDatetime(batch.getCmrIndexRequests(), ExecutionStatus.Pending);
                        indexRequestProcessorService.submit(batch);
                    }
                } finally {
                    LOG.debug(INDEXATION, sb.append('}').toString());
                    LOG.info("Done scheduling {} index notice request(s).", batches.size());
                }
                if (!cellarConfiguration.isCellarServiceIndexingEnabled()) {
                    indexRequestProcessorService.pause();
                    return;
                }
                batches = getNextCmrIndexRequestBatches(force);
            }
        }
    }

    private static String detailBatch(CmrIndexRequestBatch batch) {
        return count(p -> batch.getCmrIndexRequests().forEach(b -> p.computeIfPresent(b.getPriority(), (k, v) -> ++v))).toString();
    }

    private static String summarize(Collection<CmrIndexRequestBatch> batches) {
        return count(p -> batches.forEach(b -> p.computeIfPresent(b.getPriority(), (k, v) -> ++v))).toString();
    }

    public static EnumMap<Priority, Integer> count(Consumer<EnumMap<Priority, Integer>> count) {
        final EnumMap<Priority, Integer> priorities = new EnumMap<>(Priority.class);
        priorities.put(Priority.LOWEST, 0);
        priorities.put(Priority.LOW, 0);
        priorities.put(Priority.NORMAL, 0);
        priorities.put(Priority.HIGH, 0);
        count.accept(priorities);
        return priorities;
    }

    /**
     * Gets the next cmr index request batches.
     *
     * @param force the force
     * @return the next cmr index request batches
     */
    private Collection<CmrIndexRequestBatch> getNextCmrIndexRequestBatches(final boolean force) {

        final Date maximumCreatedOn = getMaximumCreatedOn(force);
        final Priority minimumPriority = this.cellarConfiguration.getCellarServiceIndexingMinimumPriority();

        final Collection<CmrIndexRequestBatch> batches = this.cmrIndexRequestBatchDao.findRequests(ExecutionStatus.New,
                minimumPriority, maximumCreatedOn, cellarConfiguration.getCellarServiceIndexingQueryLimitResults());
        return batches.stream()
                .map(b -> {
                    b.setMinimumPriority(minimumPriority);
                    b.setMaximumCreatedOn(maximumCreatedOn);
                    return b;
                })
                .sorted(Comparator.comparing(CmrIndexRequestBatch::getPriority).reversed()) // enforce ordering
                .collect(Collectors.toList());
    }

    /**
     * Gets the maximum created on.
     *
     * @param force the force
     * @return the maximum created on
     */
    private Date getMaximumCreatedOn(final boolean force) {
        Date maximumCreatedOn = new Date();
        if (!force) {
            maximumCreatedOn = DateUtils.addMinutes(maximumCreatedOn, -cellarConfiguration.getCellarServiceIndexingMinimumAgeMinutes());
        }
        return maximumCreatedOn;
    }
}
