/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums
 *             FILE : Structure.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 20, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums;

import eu.europa.ec.opoce.cellar.common.http.headers.IURLTokenable;

import java.util.HashMap;
import java.util.Map;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 20, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public enum Structure implements IURLTokenable {
    OBJECT("object"), BRANCH("branch"), TREE("tree");

    private final String urlToken;

    private static final Map<String, Structure> STRUCTURE_PER_URL_TOKEN;

    static {
        STRUCTURE_PER_URL_TOKEN = new HashMap<String, Structure>();

        for (final Structure s : Structure.values()) {
            STRUCTURE_PER_URL_TOKEN.put(s.getURLToken(), s);
        }
    }

    private Structure(final String urlToken) {
        this.urlToken = urlToken;
    }

    public static Structure parseStructure(final String structure) {
        return STRUCTURE_PER_URL_TOKEN.get(structure);
    }

    @Override
    public String getURLToken() {
        return this.urlToken;
    }

    @Override
    public String getErrorLabel() {
        return this.toString();
    }
}
