package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.version;

import eu.europa.ec.opoce.cellar.server.dissemination.AcceptLanguage;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.DisseminationRequest;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.IDisseminationResolver;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;

import java.util.Date;

/**
 * Time map-specific resolver
 * @author ARHS Developments
 *
 */
public abstract class TimeMapRedirectResolver extends VersionRedirectResolver {

    protected TimeMapRedirectResolver(final CellarResourceBean cellarResource, final String resourceURI, final String accept,
            final String decoding, final AcceptLanguage acceptLanguage, final Date acceptDateTime, final String rel,
            final boolean provideAlternates) {
        super(cellarResource, resourceURI, null, accept, decoding, acceptLanguage, acceptDateTime, rel, provideAlternates);
    }

    public static IDisseminationResolver get(final CellarResourceBean cellarResource, final String resourceURI,
            final DisseminationRequest disseminationRequest, final String accept, final String decoding,
            final AcceptLanguage acceptLanguage, final Date acceptDateTime, final String rel, final boolean provideAlternates) {
        //Dispatches the resolution to the {@code TimeMapLinkRedirectResolver} if the accept header contains a "application/link-format" value,
        //Otherwise, dispatches to the {@code TimeMapOtherRedirectResolver}
        switch (disseminationRequest) {
        case IDENTIFIER_LINK_FORMAT:
            return TimeMapLinkRedirectResolver.get(cellarResource, resourceURI, accept, decoding, acceptLanguage, acceptDateTime, rel,
                    provideAlternates);
        default:
            return getInstanceWithoutMemento(cellarResource, decoding, disseminationRequest, accept, acceptLanguage, acceptDateTime, rel,
                    provideAlternates);
        }
    }
}
