/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.service
 *             FILE : EmbargoOperationService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 27 Jan 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.service;

import java.util.Date;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 27 Jan 2017
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface EmbargoOperationService {

    /**
     * Execute.
     *
     * @param cellarId the cellar id
     * @param embargodDate the embargod date
     */
    void execute(final String cellarId, final Date embargodDate);
}
