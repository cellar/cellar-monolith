package eu.europa.ec.opoce.cellar.cmr.utils;

import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditBuilder;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventAction;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventProcess;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventType;
import eu.europa.ec.opoce.cellar.common.util.TimeUtils;
import eu.europa.ec.opoce.cellar.semantic.helper.DateFormats;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Date;


/**
 * @author ARHS Developments
 */
public class EmbargoAudit {

    private static final Logger LOG = LogManager.getLogger(EmbargoAudit.class);

    private String cellarId;
    private Date embargoDate;
    private boolean automatic;
    private boolean disembargo;
    private String readableEmbargoDate;
    private AuditBuilder auditBuilder;

    public EmbargoAudit(final String cellarId, final Date embargoDate, final boolean automatic) {
        this.cellarId = cellarId;
        this.embargoDate = embargoDate;
        this.automatic = automatic;
        this.readableEmbargoDate = DateFormats.formatReadableFullDateTime(embargoDate);
        this.disembargo = TimeUtils.isPastDate(embargoDate);

        this.initBuilder();
    }

    private void initBuilder() {
        this.auditBuilder = AuditBuilder.get(AuditTrailEventProcess.Embargo)
                .withObject(this.cellarId)
                .withLogger(LOG)
                .withLogDatabase(true)
                .withAction(this.embargoDate == null ? AuditTrailEventAction.Pop : AuditTrailEventAction.Set);
    }

    public AuditBuilder getAuditBuilder() {
        return auditBuilder;
    }

    public void logStart() {
        if (this.disembargo) {
            this.auditBuilder
                    .withMessage(this.automatic
                            ? "The automatic disembargo scheduler will start moving '{}' out of embargo."
                            : "Start moving '{}' out of embargo.")
                    .withType(AuditTrailEventType.Start)
                    .logEvent();
        } else {
            this.auditBuilder
                    .withMessage(this.automatic
                            ? "The automatic disembargo scheduler will start changing embargo date of '{}' to '{}'."
                            : "Start changing embargo date of '{}' to '{}'.")
                    .withMessageArgs(this.cellarId, this.readableEmbargoDate)
                    .withType(AuditTrailEventType.Start)
                    .logEvent();
        }
    }

    public void logEnd() {
        if (this.disembargo) {
            this.auditBuilder
                    .withMessage("The digital objects under '{}' have been successfully moved out of embargo.")
                    .withType(AuditTrailEventType.End)
                    .logEvent();
        } else {
            this.auditBuilder
                    .withMessage("Embargo date of the digital objects under '{}' have been successfully changed to '{}'.")
                    .withMessageArgs(this.cellarId, this.readableEmbargoDate)
                    .withType(AuditTrailEventType.End)
                    .logEvent();
        }
    }

    public AuditBuilder logException(final Exception e) {
        this.auditBuilder
                .withException(e)
                .withType(AuditTrailEventType.Fail)
                .withCode(CmrErrors.CMR_UNEXPECTED_EMBARGO_ERROR);

        if (this.disembargo) {
            this.auditBuilder
                    .withMessage("An error occurred while moving '{}' out of embargo.")
                    .logEvent();
        } else {
            this.auditBuilder
                    .withMessage("An error occurred while changing embargo date of '{}' to '{}'.")
                    .withMessageArgs(this.cellarId, this.readableEmbargoDate)
                    .logEvent();
        }

        return auditBuilder;
    }
}
