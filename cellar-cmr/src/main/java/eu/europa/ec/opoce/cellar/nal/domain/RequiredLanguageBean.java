package eu.europa.ec.opoce.cellar.nal.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * <p>RequiredLanguageBean class.</p>
 */
public class RequiredLanguageBean {

    private String uri;
    private List<String> alternatives;

    /**
     * <p>Constructor for RequiredLanguageBean.</p>
     *
     * @param uri                 a {@link java.lang.String} object.
     * @param orderedAlternatives a {@link java.util.List} object.
     */
    public RequiredLanguageBean(String uri, List<String> orderedAlternatives) {
        this.uri = uri;
        if (orderedAlternatives != null)
            this.alternatives = Collections.unmodifiableList(new ArrayList<String>(orderedAlternatives));
    }

    /**
     * <p>Getter for the field <code>uri</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getUri() {
        return uri;
    }

    /**
     * <p>Getter for the field <code>alternatives</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<String> getAlternatives() {
        return alternatives != null ? alternatives : Collections.<String> emptyList();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RequiredLanguageBean)) return false;
        RequiredLanguageBean that = (RequiredLanguageBean) o;
        return Objects.equals(getUri(), that.getUri()) &&
                Objects.equals(getAlternatives(), that.getAlternatives());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUri(), getAlternatives());
    }

    @Override
    public String toString() {
        return "RequiredLanguageBean{" +
                "uri='" + uri + '\'' +
                ", alternatives=" + alternatives +
                '}';
    }
}
