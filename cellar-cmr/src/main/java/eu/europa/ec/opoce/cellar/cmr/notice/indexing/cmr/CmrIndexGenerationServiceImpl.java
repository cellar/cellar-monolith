/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr
 *             FILE : CmrIndexGenerationServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 19-Jan-2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr;

import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CmrIndexNoticeDao;
import eu.europa.ec.opoce.cellar.cmr.database.dao.IdolExpandedNoticeDao;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Expression;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Work;
import eu.europa.ec.opoce.cellar.cmr.notice.MetsElementWithLanguage;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.IndexNotice;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.IndexNoticeService;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.Action;
import eu.europa.ec.opoce.cellar.cmr.notice.xslt.tool.NoticeExpandingTransformer;
import eu.europa.ec.opoce.cellar.common.util.Triple;
import eu.europa.ec.opoce.cellar.domain.content.mets.CellarIdentifiedObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DefaultDigitalObjectTypeVisitor;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.S3QueryException;
import eu.europa.ec.opoce.cellar.nal.domain.RequiredLanguageBean;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.nal.service.LanguageService;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaQueries;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import eu.europa.ec.opoce.cellar.semantic.xml.DomUtils;
import eu.europa.ec.opoce.cellar.server.service.DisseminationDbGateway;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.w3c.dom.Document;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.lang.System.currentTimeMillis;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 19-Jan-2017
 * The Class CmrIndexGenerationServiceImpl.
 *
 * @author ARHS Developments
 * @version $$Revision$$$
 */
@Service
public class CmrIndexGenerationServiceImpl implements CmrIndexGenerationService {

    private static final Logger LOG = LogManager.getLogger(CmrIndexGenerationServiceImpl.class);
    private static final String DELETE = "D";
    private static final String INSERT = "I";
    private final IndexNoticeService noticeService;
    private final CmrIndexNoticeDao indexNoticeDao;
    private final IdolExpandedNoticeDao idolNoticeDao;
    private final LanguageService languageService;
    private final IdentifierService identifierService;
    private final DisseminationDbGateway disseminationDbGateway;
    private final NoticeExpandingTransformer noticeExpandingTransformer;
    private final ContentStreamService contentStreamService;
    private final CellarResourceDao cellarResourceDao;
    private final IndexingBestContentStrategy bestContentStrategy;
    private final PidManagerService pidManagerService;

    @Autowired
    public CmrIndexGenerationServiceImpl(IndexNoticeService noticeService, CmrIndexNoticeDao indexNoticeDao,
                                         IdolExpandedNoticeDao idolNoticeDao, LanguageService languageService,
                                         @Qualifier("pidManagerService") IdentifierService identifierService,
                                         DisseminationDbGateway disseminationDbGateway, NoticeExpandingTransformer noticeExpandingTransformer,
                                         ContentStreamService contentStreamService, CellarResourceDao cellarResourceDao,
                                         IndexingBestContentStrategy bestContentStrategy, PidManagerService pidManagerService) {
        this.noticeService = noticeService;
        this.indexNoticeDao = indexNoticeDao;
        this.idolNoticeDao = idolNoticeDao;
        this.languageService = languageService;
        this.identifierService = identifierService;
        this.disseminationDbGateway = disseminationDbGateway;
        this.noticeExpandingTransformer = noticeExpandingTransformer;
        this.contentStreamService = contentStreamService;
        this.cellarResourceDao = cellarResourceDao;
        this.bestContentStrategy = bestContentStrategy;
        this.pidManagerService = pidManagerService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<IndexNotice> compileIndexNotice(final Work work, final Collection<CmrIndexRequest> requests,
                                                      final boolean allLanguages) {
        final Map<Expression, HashMap<LanguageBean, Document>> expressionDocumentMap = new HashMap<>();
        final Map<LanguageBean, Document> languageDocumentMap = new HashMap<>();

        if (allLanguages) {
            final IndexNoticeWork indexNoticeWork = this.noticeService.createIndexNoticesWork(work);
            languageDocumentMap.putAll(indexNoticeWork.getNoticePerLanguage());
            expressionDocumentMap.putAll(indexNoticeWork.getNoticePerExpression());
        } else {
            final Collection<Expression> expressions = this.getExpressionsToIndex(work, requests);
            expressionDocumentMap.putAll(this.noticeService.createIndexNoticesWork(expressions));
        }

        final List<IndexNotice> notices = new ArrayList<>();
        notices.addAll(this.getIdolInterfaceObjectsFor(expressionDocumentMap));
        notices.addAll(this.getIdolInterfaceObjectFor(work, languageDocumentMap));

        final CmrIndexRequest indexRequest = requests.iterator().next();
        for (final IndexNotice notice : notices) {
            notice.setPriority(indexRequest.getPriority().getPriorityValue());
            final Action action = indexRequest.getAction();
            final boolean isRemove = action == CmrIndexRequest.Action.Remove;
            notice.setOperationType((isRemove || work.isDoNotIndex()) ? DELETE : INSERT);
        }
        return notices;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<IndexNotice> calculateExpandedNotices(final Work work, final Collection<CmrIndexRequest> requests,
                                                            final boolean allLanguages, final Collection<IndexNotice> notices) {
        final Set<IndexNotice> expandedNotices = new HashSet<>();

        final Map<String, IndexNotice> givenNotices = new HashMap<>();
        for (final IndexNotice notice : notices) {
            givenNotices.put(notice.getCellarUri(), notice);
        }

        final Set<String> doneLanguages = new HashSet<>();

        //loop over all expressions that need to be indexed
        for (final Expression expression : this.getExpressionsToIndex(work, requests)) {
            final Set<LanguageBean> langs = expression.getLangs();
            for (final LanguageBean lang : langs) {
                expandedNotices.add(this.getExpandedNotice(work, lang, givenNotices));
                doneLanguages.add(lang.getUri());
            }
        }
        //add all missing languages
        if (allLanguages) {
            for (final RequiredLanguageBean requiredLanguageBean : this.languageService.getRequiredLanguages()) {
                if (!doneLanguages.contains(requiredLanguageBean.getUri())) {
                    final LanguageBean lang = this.languageService.getByUri(requiredLanguageBean.getUri());
                    expandedNotices.add(this.getExpandedNotice(work, lang, givenNotices));
                }
            }
        }
        return expandedNotices;
    }

    /**
     * <p>getExpandedNotice.</p>
     *
     * @param metsElement  a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement} object.
     * @param lang         a {@link eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean} object.
     * @param givenNotices a {@link java.util.Map} object.
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.IndexNotice} object.
     */
    private IndexNotice getExpandedNotice(final MetsElement metsElement, final LanguageBean lang,
                                          final Map<String, IndexNotice> givenNotices) {
        final String idolNoticeUri = this.getIdolNoticeUri(metsElement, lang);
        final IndexNotice expandedNotice = this.getExpandedNotice(
                givenNotices.containsKey(idolNoticeUri) ? givenNotices.get(idolNoticeUri) : this.indexNoticeDao.findNotice(idolNoticeUri),
                lang);
        final IndexNotice noticeInIdol = this.idolNoticeDao.findNotice(idolNoticeUri);
        if (noticeInIdol != null) {
            expandedNotice.setId(noticeInIdol.getId());
        }
        return expandedNotice;
    }

    /**
     * <p>getExpandedNotice.</p>
     *
     * @param indexNotice a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.IndexNotice} object.
     * @param lang        a {@link eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean} object.
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.IndexNotice} object.
     */
    private IndexNotice getExpandedNotice(final IndexNotice indexNotice, final LanguageBean lang) {
        final IndexNotice newIndexNotice = new IndexNotice(indexNotice);
        if (!StringUtils.isBlank(indexNotice.getMetadata())) {
            newIndexNotice.setMetadata(
                    this.stringValueOf(this.noticeExpandingTransformer.getExpandedNotice(DomUtils.read(indexNotice.getMetadata()), lang)));
        }
        return newIndexNotice;
    }

    /**
     * <p>getExpressionsToIndex.</p>
     *
     * @param work     a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Work} object.
     * @param requests a {@link java.util.Collection} object.
     * @return a {@link java.util.Collection} object.
     */
    private Collection<Expression> getExpressionsToIndex(final Work work, final Collection<CmrIndexRequest> requests) {
        final Set<Expression> expressions = new HashSet<>();
        for (final Expression expression : languageService.filterMultiLinguisticExpressions(work)) {
            for (final CmrIndexRequest request : requests) {
                if (request.getObjectUri().equals(expression.getCellarId().getUri())) {
                    expressions.add(expression);
                    break;
                }
            }
        }
        return expressions;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<IndexNotice> compileAndSendIndexNotice(final CmrIndexRequest indexRequest) {
        if (indexRequest == null) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument indexRequest cannot be null").build();
        }
        LOG.debug("Generate and send index for {}", indexRequest);

        final List<IndexNotice> notices = new ArrayList<>();
        if (CmrIndexRequest.Action.Remove == indexRequest.getAction()) {
            this.processRemoveIndexRequest(indexRequest, notices);
        } else {
            this.processIndexRequest(indexRequest, notices);
        }

        for (final IndexNotice notice : notices) {
            notice.setPriority(indexRequest.getPriority().getPriorityValue());
            notice.setOperationType(indexRequest.getAction() == CmrIndexRequest.Action.Remove ? DELETE : INSERT);
        }
        return notices;
    }

    /**
     * <p>processIndexRequest.</p>
     *
     * @param indexRequest a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest} object.
     * @param notices      a {@link java.util.List} object.
     */
    private void processIndexRequest(final CmrIndexRequest indexRequest, final List<IndexNotice> notices) {
        final DigitalObjectType objectType = indexRequest.getObjectType();
        if (indexRequest.getObjectType() == null) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("IndexRequest without object type {}")
                    .withMessageArgs(indexRequest)
                    .build();
        }

        objectType.accept(new DefaultDigitalObjectTypeVisitor<Void, Void>() {

            @Override
            public Void visitWork(final Void in) {
                final String objectUri = indexRequest.getObjectUri();

                if ((CmrIndexRequest.Reason.DirectlyLinkedObject == indexRequest.getReason())
                        && !isResourceExists(objectUri)) {
                    LOG.warn("Unable to process {}: {}", indexRequest, "Cellar resource does not exist yet!");
                    return null;
                }

                final IndexNoticeWork indexNoticeWork = noticeService.createIndexNoticesWork(objectUri);
                if (indexNoticeWork == null) {
                    LOG.warn("Not allowed to process {}: {}", indexRequest, "Cellar resource is under embargo!");
                    return null;
                }

                notices.addAll(getIdolInterfaceObjectsFor(indexNoticeWork.getNoticePerExpression()));

                notices.addAll(getIdolInterfaceObjectFor(indexNoticeWork.getWork(),
                        indexNoticeWork.getNoticePerLanguage()));

                return null;
            }

            @Override
            public Void visitExpression(final Void in) {
                final String objectUri = indexRequest.getObjectUri();

                final List<Triple<Expression, LanguageBean, Document>> triples = noticeService
                        .createIndexNoticeExpression(objectUri);
                if ((triples == null) || triples.isEmpty()) {
                    LOG.warn("Not allowed to process {}: {}", indexRequest, "Cellar resource is under embargo!");
                    return null;
                }

                final HashMap<LanguageBean, Document> map = new HashMap<>();
                Expression expression = null;
                for (final Triple<Expression, LanguageBean, Document> triple : triples) {
                    if (expression == null) {
                        expression = triple.getOne();
                    }
                    map.put(triple.getTwo(), triple.getThree());
                }

                final List<IndexNotice> idolInterfaceObjectFor = getIdolInterfaceObjectFor(expression, map);
                notices.addAll(idolInterfaceObjectFor);
                return null;
            }

            @Override
            public Void visitManifestation(final Void in) {
                final String objectUri = indexRequest.getObjectUri();

                final List<Triple<Expression, LanguageBean, Document>> triples = noticeService
                        .createIndexNoticeManifestation(objectUri);
                if ((triples == null) || triples.isEmpty()) {
                    LOG.warn("Not allowed to process {}: {}", indexRequest, "Cellar resource is under embargo!");
                    return null;
                }
                final HashMap<LanguageBean, Document> map = new HashMap<>();
                Expression expression = null;
                for (final Triple<Expression, LanguageBean, Document> triple : triples) {
                    if (expression == null) {
                        expression = triple.getOne();
                    }
                    map.put(triple.getTwo(), triple.getThree());
                }

                final List<IndexNotice> idolInterfaceObjectFor = getIdolInterfaceObjectFor(expression, map);
                notices.addAll(idolInterfaceObjectFor);
                return null;
            }

            @Override
            public Void visitDossier(final Void in) {
                final String objectUri = indexRequest.getObjectUri();

                if ((CmrIndexRequest.Reason.DirectlyLinkedObject == indexRequest.getReason())
                        && !isResourceExists(objectUri)) {
                    LOG.warn("Unable to process {}: {}", indexRequest, "Cellar resource does not exist yet!");
                    return null;
                }

                this.processNoticePerMetsElementsWithLanguage(
                        noticeService.createIndexNoticeDossier(objectUri), notices);
                return null;
            }

            @Override
            public Void visitEvent(final Void in) {
                final String objectUri = indexRequest.getObjectUri();
                this.processNoticePerMetsElementsWithLanguage(
                        noticeService.createIndexNoticeEvent(objectUri), notices);
                return null;
            }

            @Override
            public Void visitAgent(final Void in) {
                final String objectUri = indexRequest.getObjectUri();
                this.processNoticePerMetsElementsWithLanguage(
                        noticeService.createIndexNoticeAgent(objectUri), notices);
                return null;
            }

            @Override
            public Void visitTopLevelEvent(final Void in) {
                final String objectUri = indexRequest.getObjectUri();
                this.processNoticePerMetsElementsWithLanguage(
                        noticeService.createIndexNoticeTopLevelEvent(objectUri), notices);
                return null;
            }

            private void processNoticePerMetsElementsWithLanguage(
                    final Map<MetsElementWithLanguage, Document> noticesMetsElementPerLanguage, final List<IndexNotice> notices) {
                for (final Map.Entry<MetsElementWithLanguage, Document> element : noticesMetsElementPerLanguage.entrySet()) {
                    notices.add(getIdolInterfaceObjectFor(element.getKey().getMetsElement(),
                            element.getKey().getLanguage(), element.getValue()));
                }
            }

        }, null);
    }

    /**
     * <p>isResourceExists.</p>
     *
     * @param uri a {@link java.lang.String} object.
     * @return a boolean.
     */
    private boolean isResourceExists(final String uri) {
        try {
            return this.identifierService.getIdentifier(this.identifierService.getPrefixed(uri)) != null;
        } catch (final IllegalArgumentException ignore) {
            LOG.warn("the identifier {} is not specified yet", uri);
            return false;
        }
    }

    /**
     * <p>getIdolInterfaceObjectsFor.</p>
     *
     * @param noticePerExpression a {@link java.util.Map} object.
     * @return a {@link java.util.List} object.
     */
    private List<IndexNotice> getIdolInterfaceObjectsFor(final Map<Expression, HashMap<LanguageBean, Document>> noticePerExpression) {
        final List<IndexNotice> notices = new ArrayList<>(noticePerExpression.size());
        for (final Map.Entry<Expression, HashMap<LanguageBean, Document>> expressionDocumentEntry : noticePerExpression.entrySet()) {
            final Expression expression = expressionDocumentEntry.getKey();
            final HashMap<LanguageBean, Document> documents = expressionDocumentEntry.getValue();
            final List<IndexNotice> idolInterfaceObjectFor = this.getIdolInterfaceObjectFor(expression, documents);
            notices.addAll(idolInterfaceObjectFor);
        }
        return notices;
    }

    /**
     * <p>getIdolInterfaceObjectFor.</p>
     *
     * @param expression a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Expression} object.
     * @param notices    the notices
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.IndexNotice} object.
     */
    private List<IndexNotice> getIdolInterfaceObjectFor(final Expression expression, final HashMap<LanguageBean, Document> notices) {
        final List<IndexNotice> result = new ArrayList<>();

        final Set<LanguageBean> languageBeans = expression.getLangs();

        final String manifestationsToIndex = this.getManifestationsToIndex(expression);
        if (StringUtils.isBlank(manifestationsToIndex)) {
            LOG.warn("there is no manifestation to index for {}", expression.getCellarId().getIdentifier());
        }

        for (final LanguageBean languageBean : languageBeans) {
            final IndexNotice indexNotice = this.getBaseIndexingNoticeFor(expression.getParent(), languageBean);
            indexNotice.setContentUrls(StringUtils.isBlank(manifestationsToIndex) ? "" : manifestationsToIndex);
            final Document notice = notices.get(languageBean);
            indexNotice.setMetadata(this.stringValueOf(notice));
            result.add(indexNotice);
        }

        return result;
    }

    /**
     * <p>getIdolInterfaceObjectFor.</p>
     *
     * @param metsElement       a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement} object.
     * @param noticePerLanguage a {@link java.util.Map} object.
     * @return a {@link java.util.List} object.
     */
    private List<IndexNotice> getIdolInterfaceObjectFor(final MetsElement metsElement,
                                                        final Map<LanguageBean, Document> noticePerLanguage) {
        final List<IndexNotice> objectsPerLanguage = new ArrayList<>(noticePerLanguage.size());
        for (final Map.Entry<LanguageBean, Document> languageBeanDocumentEntry : noticePerLanguage.entrySet()) {
            objectsPerLanguage.add(
                    this.getIdolInterfaceObjectFor(metsElement, languageBeanDocumentEntry.getKey(), languageBeanDocumentEntry.getValue()));
        }

        return objectsPerLanguage;
    }

    /**
     * <p>getIdolInterfaceObjectFor.</p>
     *
     * @param metsElement  a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement} object.
     * @param languageBean a {@link eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean} object.
     * @param notice       a {@link org.w3c.dom.Document} object.
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.IndexNotice} object.
     */
    private IndexNotice getIdolInterfaceObjectFor(final MetsElement metsElement, final LanguageBean languageBean, final Document notice) {
        final IndexNotice indexNotice = this.getBaseIndexingNoticeFor(metsElement, languageBean);
        indexNotice.setContentUrls(StringUtils.EMPTY);
        indexNotice.setMetadata(this.stringValueOf(notice));
        return indexNotice;
    }

    /**
     * <p>processRemoveIndexRequest.</p>
     *
     * @param indexRequest a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest} object.
     * @param notices      a {@link java.util.List} object.
     */
    private void processRemoveIndexRequest(final CmrIndexRequest indexRequest, final List<IndexNotice> notices) {
        final DigitalObjectType objectType = indexRequest.getObjectType();
        if (indexRequest.getObjectType() == null) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("IndexRequest without object type {}")
                    .withMessageArgs(indexRequest).build();
        }

        objectType.accept(new DefaultDigitalObjectTypeVisitor<Void, Void>() {

            @Override
            public Void visitWork(final Void in) {
                final MetsElement element = new MetsElement(indexRequest.getObjectType());
                element.setCellarId(identifierService.getPrefixed(indexRequest.getObjectUri()));
                this.addRemoveNoticesForRequiredLanuages(element, notices);
                return null;
            }

            @Override
            public Void visitExpression(final Void in) {
                //Uselsess, method is never called.
                return null;
            }

            @Override
            public Void visitManifestation(final Void in) {
                //Uselsess, method is never called.
                return null;
            }

            @Override
            public Void visitDossier(final Void in) {
                final MetsElement element = new MetsElement(indexRequest.getObjectType());
                element.setCellarId(identifierService.getPrefixed(indexRequest.getObjectUri()));
                this.addRemoveNoticesForRequiredLanuages(element, notices);
                return null;
            }

            @Override
            public Void visitEvent(final Void in) {
                final MetsElement element = new MetsElement(indexRequest.getObjectType());
                element.setCellarId(identifierService.getPrefixed(indexRequest.getObjectUri()));
                this.addRemoveNoticesForRequiredLanuages(element, notices);
                return null;
            }

            @Override
            public Void visitAgent(final Void in) {
                final MetsElement element = new MetsElement(indexRequest.getObjectType());
                element.setCellarId(identifierService.getPrefixed(indexRequest.getObjectUri()));
                this.addRemoveNoticesForRequiredLanuages(element, notices);
                return null;
            }

            @Override
            public Void visitTopLevelEvent(final Void in) {
                final MetsElement element = new MetsElement(indexRequest.getObjectType());
                element.setCellarId(identifierService.getPrefixed(indexRequest.getObjectUri()));
                this.addRemoveNoticesForRequiredLanuages(element, notices);
                return null;
            }

            private void addRemoveNoticesForRequiredLanuages(final MetsElement element, final List<IndexNotice> notices) {
                final Set<LanguageBean> languageBeans = CollectionUtils.collect(
                        languageService.getRequiredLanguages(),
                        input -> languageService.getByUri(input.getUri()), new HashSet<>());
                this.addRemoveNoticesForLanguages(element, languageBeans, notices);
            }

            private void addRemoveNoticesForLanguages(final MetsElement element, final Set<LanguageBean> languageBeans,
                                                      final List<IndexNotice> notices) {
                for (final LanguageBean languageBean : languageBeans) {
                    notices.add(getBaseIndexingNoticeFor(element, languageBean));
                }
            }

        }, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<IndexNotice> calculateExpandedNotices(final CmrIndexRequest indexRequest, final Collection<IndexNotice> notices) {
        if (indexRequest == null) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument indexRequest cannot be null").build();
        }
        final DigitalObjectType objectType = indexRequest.getObjectType();
        if (indexRequest.getObjectType() == null) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("IndexRequest without object type {}")
                    .withMessageArgs(indexRequest).build();
        }

        final Map<String, IndexNotice> givenNotices = new HashMap<>();
        for (final IndexNotice notice : notices) {
            givenNotices.put(notice.getCellarUri(), notice);
        }

        return objectType.accept(new DefaultDigitalObjectTypeVisitor<Void, Collection<IndexNotice>>() {

            @Override
            public Collection<IndexNotice> visitWork(final Void in) {
                final MetsElement element = new MetsElement(indexRequest.getObjectType());
                element.setCellarId(identifierService.getPrefixed(indexRequest.getObjectUri()));
                return this.getIndexNoticeFor(element);
            }

            @Override
            public Collection<IndexNotice> visitExpression(final Void in) {
                return Collections.emptyList();
            }

            @Override
            public Collection<IndexNotice> visitManifestation(final Void in) {
                return Collections.emptyList();
            }

            @Override
            public Collection<IndexNotice> visitDossier(final Void in) {
                final MetsElement element = new MetsElement(indexRequest.getObjectType());
                element.setCellarId(identifierService.getPrefixed(indexRequest.getObjectUri()));
                return this.getIndexNoticeFor(element);
            }

            @Override
            public Collection<IndexNotice> visitEvent(final Void in) {
                final MetsElement element = new MetsElement(indexRequest.getObjectType());
                element.setCellarId(identifierService.getPrefixed(indexRequest.getObjectUri()));
                return this.getIndexNoticeFor(element);
            }

            @Override
            public Collection<IndexNotice> visitAgent(final Void in) {
                final MetsElement element = new MetsElement(indexRequest.getObjectType());
                element.setCellarId(identifierService.getPrefixed(indexRequest.getObjectUri()));
                return this.getIndexNoticeFor(element);
            }

            @Override
            public Collection<IndexNotice> visitTopLevelEvent(final Void in) {
                final MetsElement element = new MetsElement(indexRequest.getObjectType());
                element.setCellarId(identifierService.getPrefixed(indexRequest.getObjectUri()));
                return this.getIndexNoticeFor(element);
            }

            private Collection<IndexNotice> getIndexNoticeFor(final MetsElement element) {
                final Set<LanguageBean> languageBeans = CollectionUtils.collect(
                        languageService.getRequiredLanguages(),
                        input -> languageService.getByUri(input.getUri()), new HashSet<>());
                return this.getIndexNoticeFor(element, languageBeans);
            }

            private Collection<IndexNotice> getIndexNoticeFor(final MetsElement element, final Set<LanguageBean> languageBeans) {
                final Set<IndexNotice> expandedNotices = new HashSet<>();
                for (final LanguageBean languageBean : languageBeans) {
                    expandedNotices.add(getExpandedNotice(element, languageBean, givenNotices));
                }
                return expandedNotices;
            }

        }, null);
    }

    /**
     * <p>stringValueOf.</p>
     *
     * @param document a {@link org.w3c.dom.Document} object.
     * @return a {@link java.lang.String} object.
     */
    private String stringValueOf(final Document document) {
        final StringWriter writer = new StringWriter();
        DomUtils.write(document, writer);
        return writer.toString();
    }

    /**
     * <p>getBaseIndexingNoticeFor.</p>
     *
     * @param metsElement  a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement} object.
     * @param languageBean a {@link eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean} object.
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.IndexNotice} object.
     */
    private IndexNotice getBaseIndexingNoticeFor(final MetsElement metsElement, final LanguageBean languageBean) {
        final IndexNotice newIndexNotice = new IndexNotice() {

            {
                this.setCellarUri(getIdolNoticeUri(metsElement, languageBean));
                this.setIsoCode(languageBean);
            }
        };

        final IndexNotice originalIndexNotice = this.indexNoticeDao.findNotice(this.getIdolNoticeUri(metsElement, languageBean));
        if (originalIndexNotice != null) {
            newIndexNotice.setId(originalIndexNotice.getId());
        }
        return newIndexNotice;
    }

    /**
     * <p>getIdolNoticeUri.</p>
     *
     * @param metsElement  a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement} object.
     * @param languageBean a {@link eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean} object.
     * @return a {@link java.lang.String} object.
     */
    private String getIdolNoticeUri(final MetsElement metsElement, final LanguageBean languageBean) {
        return languageBean.getIsoCodeThreeChar() + '_' + metsElement.getCellarId().getIdentifier();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void recalculateContentStreams(final String cellarContentStreamId) {
        final int lengthWorkId = cellarContentStreamId.indexOf('.');
        final CellarResource workResource = this.disseminationDbGateway.getCellarResource(cellarContentStreamId.substring(0, lengthWorkId));
        final Work work = this.disseminationDbGateway.createWork(workResource);
        final CellarResource expressionResource = this.disseminationDbGateway
                .getCellarResource(cellarContentStreamId.substring(0, cellarContentStreamId.indexOf('.', lengthWorkId + 1)));
        final Expression expression = this.disseminationDbGateway.createExpression(expressionResource);

        final Set<LanguageBean> langs = expression.getLangs();
        for (final LanguageBean lang : langs) {
            final String idolNoticeUri = this.getIdolNoticeUri(work, lang);
            final IndexNotice indexNotice = this.indexNoticeDao.findNotice(idolNoticeUri);
            if ((indexNotice != null) && !indexNotice.getOperationType().equals(DELETE)) {
                LOG.info("contentUris update notice for {}", this.identifierService.getUri(cellarContentStreamId));
                final String manifestationsToIndex = this.getManifestationsToIndex(expression);
                if (StringUtils.isBlank(manifestationsToIndex)) {
                    LOG.warn("there is no manifestation to index for {}", expression.getCellarId().getIdentifier());
                }
                indexNotice.setContentUrls(StringUtils.isBlank(manifestationsToIndex) ? "" : manifestationsToIndex);
                this.indexNoticeDao.saveOrUpdateObject(indexNotice);
                final IndexNotice expandedNotice = this.getExpandedNotice(indexNotice, lang);
                final IndexNotice noticeInIdol = this.idolNoticeDao.findNotice(indexNotice.getCellarUri());
                if (noticeInIdol != null) {
                    expandedNotice.setId(noticeInIdol.getId());
                }
                this.idolNoticeDao.saveOrUpdateObject(expandedNotice);
            } else {
                LOG.info("skip contentUris update notice for {}, notice didn't exist yet",
                        this.identifierService.getUri(cellarContentStreamId));
            }
        }
    }

    /**
     * get all contentstreams that need to be indexed.
     *
     * @param expression expression that need to be searched
     * @return a list of URIs that looks like this {<CONTENT_URL>http://<config-baseurl>/resource/cellar/fdda3ece-7354-43b5-83e6-c210481f9121.0001.02/DOC_1</CONTENT_URL>
     * <CONTENT_URL>http://<config-baseurl>/resource/cellar/fdda3ece-7354-43b5-83e6-c210481f9121.0001.02/DOC_2</CONTENT_URL>}, for instance
     */
    private String getManifestationsToIndex(final Expression expression) {
        if (expression == null) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument expression cannot be null").build();
        }

        final String cellarId = expression.getCellarId().getIdentifier();
        if (StringUtils.isBlank(cellarId)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument cellar id cannot be blank").build();
        }
        final long startInterval = currentTimeMillis();
        try {
            String manifestation = getBestContentForIndexation(expression);
            LOG.debug(IConfiguration.INDEXATION, "Manifestation [{}] to index for [{}]", manifestation, cellarId);
            return manifestation;
        } catch (Exception e) {
            LOG.error("Unable to retrieve best content for object with Cellar ID='{}'", cellarId);
            return "";
        } finally {
            LOG.debug("S3 get best content for {} in {} ms.", cellarId, String.valueOf(currentTimeMillis() - startInterval));
        }
    }

    public String getBestContentForIndexation(final CellarIdentifiedObject digitalObject) {
        final String id = digitalObject.getCellarId().getIdentifier();
        final DigitalObjectType type = digitalObject.getType();
        return getBestContent(id, type);
    }

    private String getBestContent(final String digitalObjectPID, DigitalObjectType type) {

        final Map<String, List<CellarResource>> contents = new LinkedMultiValueMap<>();

        final List<CellarResource> manifestations = getChildren(digitalObjectPID, type);
        // TODO filter children (embargoed)

        // iterate through all manifestations attached to the 'expression' digital object to get the
        // list of documents for each 'manifestation'
        for (CellarResource manifestation : manifestations) {
            boolean skip = false;
            final String doNotIndex = extractPropertyLiteralValue(manifestation.getCellarId(), manifestation.getVersions(), CellarProperty.cdm_do_not_indexP);
            if (StringUtils.isNotBlank(doNotIndex)) {
                skip = Boolean.parseBoolean(doNotIndex);
            }
            if (!skip) {
                List<CellarResource> items = getItems(manifestation.getCellarId());
                contents.putIfAbsent(manifestation.getCellarId(), items);
            }
        }

        // when all documents datastream of all 'manifestation' digital objects of the 'expression'
        // digital object have been registered, determine what manifestation has the best matching
        // format
        LOG.debug("Manifestation of best content : [{}]", contents);
        final List<CellarResource> items = bestContentStrategy.getBestContent(contents);
        final StringBuilder result = new StringBuilder();

        for (CellarResource item : items) {
            final String manifestation = StringUtils.substringBefore(item.getCellarId(), "/");
            final String itemName = StringUtils.substringAfterLast(item.getCellarId(), "/");
            final String uri = pidManagerService.getURI(manifestation) + "/" + itemName;
            result.append("<CONTENT_URL>").append(uri).append("</CONTENT_URL>");
        }

        // return a list of cellar URIs (e.g. cellar:101/DOC_1) that correspond to the best matching
        // format
        return result.toString();
    }

    public List<CellarResource> getChildren(final String digitalObjectPID, DigitalObjectType type) {
        if (digitalObjectPID == null) {
            throw ExceptionBuilder.get(S3QueryException.class).withCode(CoreErrors.E_100).withMessage("The Digital Object PID is null")
                    .build();
        }

        final DigitalObjectType childrenType = DigitalObjectType.getChildrenType(type);
        return getChildrenPIDsFromOracle(digitalObjectPID, childrenType);
    }

    private List<CellarResource> getChildrenPIDsFromOracle(final String digitalObjectPID, final DigitalObjectType childrenType) {
        return new ArrayList<>(cellarResourceDao.findResourceStartWith(digitalObjectPID, childrenType));
    }

    public String extractPropertyLiteralValue(final String cellarId, Map<ContentType, String> versions, final Property property) {
        String directVersion = versions.get(ContentType.DIRECT);
        final String direct = contentStreamService.getContentAsString(cellarId, directVersion, ContentType.DIRECT)
                .orElseThrow(() -> new IllegalStateException("Direct metadata not found for " + cellarId + "(" + directVersion + ")"));
        final Model directModel = JenaUtils.read(direct, Lang.NT);
        for (String pid : identifierService.getIdentifier(cellarId).getPids()) {
            final Resource resource = directModel.getResource(identifierService.getUri(pid));
            if (resource != null) {
                final String annotationLiteralValue = JenaQueries.getLiteralValue(resource, property, false, true);
                if (StringUtils.isNotBlank(annotationLiteralValue)) {
                    return annotationLiteralValue;
                }
            }
        }
        return null;
    }

    private List<CellarResource> getItems(String manifestation) {
        return cellarResourceDao.findCellarIdStartingWith(manifestation + "/")
                .stream()
                .filter(r -> r.getCellarType() == DigitalObjectType.ITEM)
                .collect(Collectors.toList());
    }

}
