/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.server.filter
 *        FILE : DisseminationLogHelper.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 01-07-2022
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2022 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.filter;

import java.util.Collection;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Work;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;

import static eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType.EXPRESSION;
import static eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType.ITEM;
import static eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType.MANIFESTATION;

/**
 * Helper class for extending the dissemination log entries corresponding to specific
 * dissemination requests with additional information regarding the related languages
 * and MIME type.
 * @author EUROPEAN DYNAMICS S.A.
 */
public class DisseminationLogHelper {
	
	/**
     * The name of the http request attribute containing the languages associated with a dissemination request, when applicable. 
     */
    public static final String LANGUAGES_REQUEST_ATTRIBUTE_NAME = "disseminationLanguages";
    /**
     * The name of the http request attribute containing the MIME type associated with a dissemination request, when applicable.
     */
    public static final String MIME_TYPE_REQUEST_ATTRIBUTE_NAME = "disseminationMimeType";
	
    
	/**
	 * Extends the dissemination log entry corresponding to a dissemination request with additional information.
	 * @param cellarResource the Cellar resource.
	 * @param work he hierarchical element representing a work. 
	 * @param mimeType the MIME type.
	 * @param cellarResourceDao the cellar resource DAO.
	 */
	public static void extendDisseminationLogEntries(CellarResourceBean cellarResource, Work work, String mimeType, CellarResourceDao cellarResourceDao) {
		if (isApplicable(cellarResource)) {
    		populateRequestWithDisseminationInfo(extractLanguages(cellarResource, work, cellarResourceDao), mimeType);
    	}
	}
	
	/**
     * Extracts and concatenates the languages associated with a digital object.
     * @param cellarResource the Cellar resource.
     * @param work the hierarchical element representing a work. 
     */
	private static String extractLanguages(CellarResourceBean cellarResource, Work work, CellarResourceDao cellarResourceDao) {
		Collection<String> languages = null;
		DigitalObjectType cellarType = cellarResource.getCellarType();
    	// If the requested notice is a branch notice or an object-expression notice,
    	// then the language can be retrieved directly from the CellarResourceBean.
    	// If the requested notice is an object-manifestation notice, then its associated language
    	// needs to be retrieved via its parent expression.
        if ((cellarType == EXPRESSION)) {
        	languages = cellarResource.getLanguages();
        }
        else if ((cellarType == MANIFESTATION || cellarType == ITEM)) {
        	// If the resource hierarchy has already been retrieved, then the parent expression
        	// can be found within the hierarchical Work object. Otherwise the hierarchy is
        	// retrieved explicitly.
        	if (work != null) {
        		languages = work.getChildren().iterator().next().getLangs().stream()
        						.map(LanguageBean::getIsoCodeThreeChar)
								.collect(Collectors.toSet());
        	}
        	else {
        		Collection<CellarResource> hierarchy = cellarResourceDao.findExpressions(cellarResource.getCellarId());
        		languages = hierarchy.stream()
				        		.map(CellarResource::getLanguages)
				        		.flatMap(Collection::stream)
				        		.collect(Collectors.toSet());
        	}
        }
        return StringUtils.join(languages, ",");
	}
	
	/**
	 * Checks whether the additional dissemination-related information should be extracted and passed
	 * to the dissemination log.
	 * @param cellarResource the Cellar resource.
	 * @return <code>true<code> if the additional dissemination-related information should be extracted and
	 * passed to the dissemination log. 
	 */
	private static boolean isApplicable(CellarResourceBean cellarResource) {
		DigitalObjectType cellarType = cellarResource.getCellarType();
		return cellarType == EXPRESSION || cellarType == MANIFESTATION || cellarType == ITEM;
	}
	
	/**
     * Stores as attributes in the http request the languages and MIME type associated to a dissemination request,
     * so that it can be retrieved and logged by the RequestLoggingFilter.
     * @param languages the languages associated to the dissemination request.
     * @param mimeType the MIME type associated to the dissemination request.
     */
	private static void populateRequestWithDisseminationInfo(String languages, String mimeType) {
		RequestAttributes reqAttributes = RequestContextHolder.getRequestAttributes();
		reqAttributes.setAttribute(LANGUAGES_REQUEST_ATTRIBUTE_NAME, languages.toUpperCase(), RequestAttributes.SCOPE_REQUEST);
		reqAttributes.setAttribute(MIME_TYPE_REQUEST_ATTRIBUTE_NAME, mimeType.toUpperCase(), RequestAttributes.SCOPE_REQUEST);
    }
	
}
