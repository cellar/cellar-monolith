/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.metsbeans
 *             FILE : HierarchyElement.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 20, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.metsbeans;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.common.filter.IFilter;
import eu.europa.ec.opoce.cellar.common.strategy.IStrategy;
import eu.europa.ec.opoce.cellar.common.util.Triple;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarType;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

/**
 * <class_description> Hiearchy element in the tree.
 * <p>
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 20, 2013
 *
 * @param <P> the generic type
 * @param <C> the generic type
 * @author ARHS Developments
 * @version $Revision$
 */
public abstract class HierarchyElement<P extends MetsElement, C extends MetsElement> extends MetsElement {

    private static final long serialVersionUID = -8768135924179756316L;

    private P parent;

    private Collection<C> children;

    private boolean doNotIndex;

    public HierarchyElement(final DigitalObjectType type) {
        this(type, Collections.emptyMap());
    }

    public HierarchyElement(final DigitalObjectType type, Map<ContentType, String> versions) {
        super(type, versions);
        this.children = new ArrayList<C>();
    }

    @Override
    public final void setType(final DigitalObjectType type) {
        if ((getType() != null) && (getType() != DigitalObjectType.UNDEFINED) && (getType() != type)) {
            Assert.isTrue(getType() == type,
                    "The type of the object is already set to '" + getType() + "' and cannot be reset to '" + type + "'.");
        }
        super.setType(type);
    }

    public Collection<C> getChildren() {
        return this.children;
    }

    public void setChildren(Collection<C> children) {
        this.children = children;
    }

    public boolean isDoNotIndex() {
        return this.doNotIndex;
    }

    public void setDoNotIndex(final boolean doNotIndex) {
        this.doNotIndex = doNotIndex;
    }

    public final void addChild(final C child) {
        if (getType().isBottomLevel()) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.CELLAR_INTERNAL_ERROR)
                    .withMessage("Cannot set children on the METS element with id {}, because it is a bottom-level element.")
                    .withMessageArgs(getCellarId().getIdentifier()).build();
        } else if ((child != null) && child.getCellarId().getIdentifier().startsWith(getCellarId().getIdentifier())) {
            getChildren().add(child);
        }
    }

    public final void addChildren(final Collection<C> children) {
        for (final C child : children) {
            this.addChild(child);
        }
    }

    public final P getParent() {
        return this.parent;
    }

    public final void setParent(final P parent) {
        if (getType().isTopLevel()) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withCode(CommonErrors.CELLAR_INTERNAL_ERROR)
                    .withMessage("Cannot set the parent of the METS element with id {}, because it is a top-level element.")
                    .withMessageArgs(getCellarId().getIdentifier())
                    .build();
        } else if ((parent != null) && getCellarId().getIdentifier().startsWith(parent.getCellarId().getIdentifier())) {
            this.parent = parent;
        }
    }

    public final boolean isUnderEmbargo() {
        final Date embargoDate = this.getEmbargoDate();
        return (embargoDate != null) && embargoDate.after(new Date());
    }

    public final Date getEmbargoDate() {
        return getEmbargoDate(this);
    }

    public void setEmbargoDate(final Date embargoDate) {
        if (!DigitalObjectType.takeEmbargoDate(getType())) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withCode(CommonErrors.CELLAR_INTERNAL_ERROR)
                    .withMessage("Cannot set the embargo date on METS element with id {}, because it is not a valid element of type {}.")
                    .withMessageArgs(getCellarId().getIdentifier(), Arrays.toString(DigitalObjectType.getEmbargoTypes().toArray()))
                    .build();
        } else {
            this.embargoDate = embargoDate == null
                    ? null
                    : new Date(embargoDate.getTime());
        }
    }

    @SuppressWarnings("unchecked")
    public TreeSet<MetsElement> getHierarchy() {
        final TreeSet<MetsElement> result = new TreeSet<>();
        result.add(this);
        for (final C child : this.getChildren()) {
            if (child instanceof HierarchyElement<?, ?>) {
                result.addAll(((HierarchyElement<P, C>) child).getHierarchy());
            }
        }
        return result;
    }

    public Collection<MetsElement> getFlatHierarchy() {
        return this.getFlatHierarchy((DigitalObjectType[]) null);
    }

    /**
     * Gets the flat hierarchy.
     *
     * @param acceptableTypes the acceptable types
     * @return the flat hierarchy
     */
    public Collection<MetsElement> getFlatHierarchy(final DigitalObjectType... acceptableTypes) {
        IFilter<MetsElement> typeFilter = null;
        if (acceptableTypes != null) {
            typeFilter = new TypeFilter(acceptableTypes);
        }
        final GetFlatHierarchyStrategy flatStrategy = new GetFlatHierarchyStrategy(typeFilter);
        this.applyStrategyOnSelfAndChildren(flatStrategy);
        return flatStrategy.getElements();
    }

    /**
     * Apply strategy on self and children.
     *
     * @param strategy the strategy
     */
    @SuppressWarnings("unchecked")
    private void applyStrategyOnSelfAndChildren(final GetFlatHierarchyStrategy strategy) {
        strategy.applyStrategy(this);
        for (final C child : this.getChildren()) {
            if (child instanceof HierarchyElement<?, ?>) {
                ((HierarchyElement<P, C>) child).applyStrategyOnSelfAndChildren(strategy);
            }
        }
    }

    /**
     * Gets the embargo date.
     *
     * @param <P>     the generic type
     * @param <C>     the generic type
     * @param element the element
     * @return the embargo date
     */
    @SuppressWarnings("unchecked")
    private static <P extends MetsElement, C extends MetsElement> Date getEmbargoDate(final HierarchyElement<P, C> element) {
        Date embargoDate = null;
        if (element != null) {
            embargoDate = element.embargoDate;
        }
        if (embargoDate == null) {
            final MetsElement parent = element.getParent();
            if ((parent != null) && (parent instanceof HierarchyElement<?, ?>)) {
                embargoDate = getEmbargoDate((HierarchyElement<P, C>) parent);
            }
        }
        return embargoDate;
    }

    private class GetFlatHierarchyStrategy implements IStrategy<MetsElement> {

        private final Collection<MetsElement> elements;

        private IFilter<MetsElement> filter;

        /**
         * Instantiates a new gets the flat hierarchy strategy.
         */
        public GetFlatHierarchyStrategy() {
            this.elements = new ArrayList<MetsElement>();
        }

        /**
         * Instantiates a new gets the flat hierarchy strategy.
         *
         * @param filter the filter
         */
        public GetFlatHierarchyStrategy(final IFilter<MetsElement> filter) {
            this();
            this.filter = filter;
        }

        @Override
        public void applyStrategy(final MetsElement element) {
            if ((this.filter == null) || this.filter.accept(element)) {
                this.elements.add(element);
            }
        }

        /**
         * Gets the elements.
         *
         * @return the elements
         */
        public Collection<MetsElement> getElements() {
            return this.elements;
        }

    }

    private class TypeFilter implements IFilter<MetsElement> {

        private final List<DigitalObjectType> acceptableTypes;

        public TypeFilter(final DigitalObjectType... acceptableTypes) {
            this.acceptableTypes = new ArrayList<DigitalObjectType>(Arrays.asList(acceptableTypes));
        }

        @Override
        public boolean accept(final MetsElement hierarchyElement) {
            return this.acceptableTypes.contains(hierarchyElement.getType());
        }

    }

    /**
     * Gets a triple out of the hierarchy element.
     *
     * @param <P>               the generic type
     * @param <C>               the generic type
     * @param digitalObjectType the digital object type
     * @return the triple
     */
    public static <P extends MetsElement, C extends MetsElement> Triple<HierarchyElement<P, C>, Property, Resource> getTriple(
            final DigitalObjectType digitalObjectType) {
        Property property = null;
        Resource resource = null;
        MetsElement metsElement = null;

        switch (digitalObjectType) {
            case WORK:
                property = CellarProperty.cdm_work_embargoP;
                resource = CellarType.cdm_workR;
                metsElement = new Work();
                break;
            case EXPRESSION:
                property = CellarProperty.cdm_expression_embargoP;
                resource = CellarType.cdm_expressionR;
                metsElement = new Expression();
                break;
            case MANIFESTATION:
                property = CellarProperty.cdm_manifestation_embargoP;
                resource = CellarType.cdm_manifestationR;
                metsElement = new Manifestation();
                break;
            case ITEM:
                property = CellarProperty.cdm_item_embargoP;
                resource = CellarType.cdm_itemR;
                metsElement = new Content();
                break;
            case DOSSIER:
                property = CellarProperty.cdm_dossier_embargoP;
                resource = CellarType.cdm_dossierR;
                metsElement = new Dossier();
                break;
            case EVENT:
                property = CellarProperty.cdm_event_embargoP;
                resource = CellarType.cdm_eventR;
                metsElement = new Event();
                break;
            case AGENT:
                property = CellarProperty.cdm_agent_embargoP;
                resource = CellarType.cdm_agentR;
                metsElement = new Agent();
                break;
            case TOPLEVELEVENT:
                property = CellarProperty.cdm_event_embargoP;
                resource = CellarType.cdm_topleveleventR;
                metsElement = new TopLevelEvent();
                break;
            // shouldn't get here!
            default:
                break;
        }
        // needs a cast here, until CELLARM-1025 is resolved
        // TODO: CELLAR-1025
        @SuppressWarnings("unchecked") final HierarchyElement<P, C> hierarchyElement = (HierarchyElement<P, C>) metsElement;

        return new Triple<HierarchyElement<P, C>, Property, Resource>(hierarchyElement, property, resource);
    }
}
