package eu.europa.ec.opoce.cellar.nal.domain;

import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

import org.apache.commons.lang.StringUtils;

/**
 * <p>Uri class.</p>
 */
public class Uri {

    private String uri;

    /**
     * <p>Constructor for Uri.</p>
     *
     * @param uri a {@link java.lang.String} object.
     */
    public Uri(String uri) {
        if (StringUtils.isBlank(uri)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Invalid uri: A uri can't be whitespace, empty or null").build();
        }
        this.uri = uri;
    }

    /**
     * <p>Getter for the field <code>uri</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getUri() {
        return uri;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Uri uri1 = (Uri) o;

        return uri != null ? uri.equals(uri1.uri) : uri1.uri == null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return uri != null ? uri.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Uri{" +
                "uri='" + uri + '\'' +
                '}';
    }
}
