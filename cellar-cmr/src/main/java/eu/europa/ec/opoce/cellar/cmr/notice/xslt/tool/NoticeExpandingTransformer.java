package eu.europa.ec.opoce.cellar.cmr.notice.xslt.tool;

import eu.europa.ec.opoce.cellar.common.util.FileSystemUtils;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.semantic.xml.DomUtils;

import java.io.InputStream;
import java.io.OutputStream;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMResult;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

@Service
/**
 * <p>NoticeExpandingTransformer class.</p>
 */
public class NoticeExpandingTransformer {

    /**
     * Constant <code>transformerFactory</code>
     */
    private static ThreadLocal<TransformerFactory> transformerFactory = new ThreadLocal<TransformerFactory>() {

        @Override
        protected TransformerFactory initialValue() {
            return TransformerFactory.newInstance();
        }
    };

    /**
     * <p>getExpandedNotice.</p>
     *
     * @param sourceDocument a {@link org.w3c.dom.Document} object.
     * @param language       a {@link eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean} object.
     * @return a {@link org.w3c.dom.Document} object.
     */
    public Document getExpandedNotice(Document sourceDocument, LanguageBean language) {
        try {
            Transformer transformer = transformerFactory.get()
                    .newTransformer(new StreamSource(FileSystemUtils.getResource("xslt/expandNotice.xsl").getInputStream()));
            if (language != null) {
                transformer.setParameter("cmd-lang", language.getIsoCodeThreeChar());
            }
            DOMSource source = new DOMSource(sourceDocument);
            Document targetDocument = DomUtils.getDocumentBuilder().newDocument();
            DOMResult result = new DOMResult(targetDocument);
            transformer.transform(source, result);
            return targetDocument;
        } catch (Exception ex) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Transform Failed").withCause(ex).build();
        }
    }

    /**
     * <p>getEmbeddedNotice.</p>
     *
     * @param input  a {@link java.io.InputStream} object.
     * @param output a {@link java.io.OutputStream} object.
     */
    public void getEmbeddedNotice(InputStream input, OutputStream output) {
        SAXSource inputSource = new SAXSource(new InputSource(input));
        StreamResult result = new StreamResult(output);
        try {
            Transformer transformer = transformerFactory.get()
                    .newTransformer(new StreamSource(FileSystemUtils.getResource("xslt/expandNotice.xsl").getInputStream()));
            transformer.setParameter("cmd-lang", "eng");
            transformer.transform(inputSource, result);
        } catch (Exception ex) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Transform Failed").withCause(ex).build();
        }
    }

}
