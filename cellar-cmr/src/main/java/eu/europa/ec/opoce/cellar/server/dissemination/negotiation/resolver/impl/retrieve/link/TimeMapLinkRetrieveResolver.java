/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.retrieve.link
 *             FILE : TimemapLinkRetrieveResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 22, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.retrieve.link;

import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.common.http.headers.HttpHeadersBuilder;
import eu.europa.ec.opoce.cellar.common.http.headers.IURLTokenable;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.ExtendedMediaType;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.Type;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.IDisseminationResolver;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.retrieve.RetrieveResolver;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.retrieve.link.builder.LinkFormatBuilder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 22, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Configurable
public class TimeMapLinkRetrieveResolver extends RetrieveResolver {

    /** The identifier service. */
    @Autowired
    @Qualifier("pidManagerService")
    private IdentifierService identifierService;

    /**
     * Instantiates a new time map link retrieve resolver.
     *
     * @param identifier the identifier
     * @param eTag the e tag
     * @param lastModified the last modified
     * @param provideResponseBody the provide response body
     */
    protected TimeMapLinkRetrieveResolver(String identifier, String eTag, String lastModified, boolean provideResponseBody) {
        super(identifier, eTag, lastModified, provideResponseBody);
    }

    /**
     * Gets the.
     *
     * @param identifier the identifier
     * @param eTag the e tag
     * @param lastModified the last modified
     * @param provideResponseBody the provide response body
     * @return the i dissemination resolver
     */
    public static IDisseminationResolver get(final String identifier, final String eTag, final String lastModified,
            final boolean provideResponseBody) {
        return new TimeMapLinkRetrieveResolver(identifier, lastModified, eTag, provideResponseBody);
    }

    /** {@inheritDoc} */
    @Override
    protected ResponseEntity<?> doHandleDisseminationRequest() {
        final String cellarUri = this.identifierService.getUri(this.cellarResource.getCellarId());

        final HttpHeadersBuilder httpHeadersBuilder = HttpHeadersBuilder.get()
                .withContentType(ExtendedMediaType.APPLICATION_LINK_FORMAT_VALUE).withDateNow();

        final LinkFormatBuilder linkFormatBuilder = LinkFormatBuilder.get(cellarUri).buildOriginalTimegates().buildMementos()
                .buildTimemaps().buildSelf();

        return new ResponseEntity<String>(linkFormatBuilder.getLinkFormat(), httpHeadersBuilder.getHttpHeaders(), HttpStatus.OK);
    }

    /** {@inheritDoc} */
    @Override
    protected IURLTokenable getTypeStructure() {
        return Type.LINK;
    }

}
