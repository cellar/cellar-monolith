/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice.core
 *             FILE : ShouldHandlePropertyVisitor.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 24 Apr 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.notice.core;

import com.google.common.annotations.VisibleForTesting;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.common.Namespace;
import eu.europa.ec.opoce.cellar.semantic.ontology.data.FallbackOntologyData;
import eu.europa.ec.opoce.cellar.semantic.ontology.data.OntologyData;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.SortedSet;

/**
 * @author ARHS Developments
 */
@Configurable
public class ShouldHandlePropertyVisitor implements NoticeTypeVisitor<Boolean> {

    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    private final Rdf2Xml rdf2Xml;
    private final OntologyData ontologyData;
    private final FallbackOntologyData fallbackOntologyData;
    private String currentPropertyUri;

    public ShouldHandlePropertyVisitor(Rdf2Xml rdf2Xml, OntologyData ontologyData, FallbackOntologyData fallbackOntologyData) {
        this.rdf2Xml = rdf2Xml;
        this.ontologyData = ontologyData;
        this.fallbackOntologyData = fallbackOntologyData;
    }

    public void setCurrentPropertyUri(String currentPropertyUri) {
        this.currentPropertyUri = currentPropertyUri;
    }

    @Override
    public Boolean visitBranch() {
        return isInNotice();
    }

    @Override
    public Boolean visitIdentifier() {
        return true;
    }

    @Override
    public Boolean visitIndexing() {
        return CellarProperty.cdm_work_has_expression.equals(this.currentPropertyUri)
                || getOntologyData().getToBeIndexedPropertyUris().contains(this.currentPropertyUri)
                || getFallbackOntologyData().getToBeIndexedPropertyUris().contains(this.currentPropertyUri);
    }

    @Override
    public Boolean visitObject() {
        return isInNotice();
    }

    @Override
    public Boolean visitTree() {
        return isInNotice();
    }

    @Override
    public Boolean visitEmbeddedNotice() {
        return getOntologyData().getEmbeddedProperties().contains(this.currentPropertyUri);
    }

    @VisibleForTesting
    Boolean isInNotice() {
        if (!cellarConfiguration.isCellarServiceDisseminationInNoticePropertiesOnlyEnabled() || !rdf2Xml.isFilter()) {
            return true;
        }

        if (this.currentPropertyUri.startsWith(Namespace.cdm)
                || this.currentPropertyUri.startsWith(Namespace.import_onto)
                || this.currentPropertyUri.startsWith(Namespace.indexation)
                || this.currentPropertyUri.startsWith(Namespace.deriv)
                || this.currentPropertyUri.startsWith(Namespace.datatype)
                || this.currentPropertyUri.startsWith(Namespace.annotation)
                || this.currentPropertyUri.startsWith(Namespace.cmr)
                || this.currentPropertyUri.startsWith(Namespace.dorie)) {
            final SortedSet ontoUris = getOntologyData().getInNoticePropertyUris();
            final SortedSet ontoFallbackUris = getFallbackOntologyData().getInNoticePropertyUris();
            return ontoUris.contains(this.currentPropertyUri) || ontoFallbackUris.contains(this.currentPropertyUri);
        } else {
            return true;
        }
    }

    public OntologyData getOntologyData() {
        return ontologyData;
    }

    public FallbackOntologyData getFallbackOntologyData() {
        return fallbackOntologyData;
    }

    @VisibleForTesting
    void setCellarConfiguration(ICellarConfiguration cellarConfiguration) {
        this.cellarConfiguration = cellarConfiguration;
    }
}
