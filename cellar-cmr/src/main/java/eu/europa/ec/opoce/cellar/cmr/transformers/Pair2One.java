package eu.europa.ec.opoce.cellar.cmr.transformers;

import eu.europa.ec.opoce.cellar.common.util.Pair;

import org.apache.commons.collections15.Transformer;
import org.springframework.stereotype.Component;

/**
 * <p>Pair2One class.</p>
 */
@Component("pair2One")
public class Pair2One<ONE, TWO> implements Transformer<Pair<ONE, TWO>, ONE> {

    /**
     * {@inheritDoc}
     */
    @Override
    public ONE transform(final Pair<ONE, TWO> pair) {
        return pair.getOne();
    }
}
