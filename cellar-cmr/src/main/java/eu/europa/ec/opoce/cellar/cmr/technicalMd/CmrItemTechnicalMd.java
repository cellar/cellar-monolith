package eu.europa.ec.opoce.cellar.cmr.technicalMd;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "CMR_ITEM_TECHNICAL_MD")
public class CmrItemTechnicalMd implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "cmr_item_tech_md_seq")
    @SequenceGenerator(name = "cmr_item_tech_md_seq", sequenceName = "CMR_ITEM_TECH_MD_SEQ",allocationSize = 1)
    private Long id;

    @Column(name = "CELLAR_ID")
    private String cellarId;

    @Column(name = "STREAM_NAME")
    private String streamName;

    @Column(name = "STREAM_ORDER")
    private int streamOrder;

    @Column(name = "STREAM_SIZE")
    private int streamSize;

    @Column(name = "CHECKSUM")
    private String checksum;

    @Column(name = "CHECKSUM_ALGORITHM")
    private String checksumAlgorithm;

    @Column(name = "LAST_MODIFICATION_DATE")
    private Date lastModificationDate;

    public CmrItemTechnicalMd() {

    }


    public CmrItemTechnicalMd(String cellarId, String streamName, int streamOrder, int streamSize, String checksum, String checksumAlgorithm, String manifestationType, String manifestationMimeType, Date lastModificationDate) {
        this.cellarId = cellarId;
        this.streamName = streamName;
        this.streamOrder = streamOrder;
        this.streamSize = streamSize;
        this.checksum = checksum;
        this.checksumAlgorithm = checksumAlgorithm;
        this.lastModificationDate = lastModificationDate;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCellarId() {
        return cellarId;
    }

    public void setCellarId(String cellarId) {
        this.cellarId = cellarId;
    }

    public String getStreamName() {
        return streamName;
    }

    public void setStreamName(String streamName) {
        this.streamName = streamName;
    }

    public int getStreamOrder() {
        return streamOrder;
    }

    public void setStreamOrder(int streamOrder) {
        this.streamOrder = streamOrder;
    }

    public int getStreamSize() {
        return streamSize;
    }

    public void setStreamSize(int streamSize) {
        this.streamSize = streamSize;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public String getChecksumAlgorithm() {
        return checksumAlgorithm;
    }

    public void setChecksumAlgorithm(String checksumAlgorithm) {
        this.checksumAlgorithm = checksumAlgorithm;
    }

    public Date getLastModificationDate() {
        return lastModificationDate;
    }

    public void setLastModificationDate(Date lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CmrItemTechnicalMd)) return false;
        CmrItemTechnicalMd that = (CmrItemTechnicalMd) o;
        return getStreamOrder() == that.getStreamOrder() && getStreamSize() == that.getStreamSize() && Objects.equals(getId(), that.getId()) && Objects.equals(getCellarId(), that.getCellarId()) && Objects.equals(getStreamName(), that.getStreamName()) && Objects.equals(getChecksum(), that.getChecksum()) && Objects.equals(getChecksumAlgorithm(), that.getChecksumAlgorithm()) && Objects.equals(getLastModificationDate(), that.getLastModificationDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getCellarId(), getStreamName(), getStreamOrder(), getStreamSize(), getChecksum(), getChecksumAlgorithm(), getLastModificationDate());
    }

    @Override
    public String toString() {
        return "CmrItemTechnicalMd{" +
                "id=" + id +
                ", cellarId='" + cellarId + '\'' +
                ", streamName='" + streamName + '\'' +
                ", streamOrder=" + streamOrder +
                ", streamSize=" + streamSize +
                ", checksum='" + checksum + '\'' +
                ", checksumAlgorithm='" + checksumAlgorithm + '\'' +
                ", lastModificationDate=" + lastModificationDate +
                '}';
    }
}



