/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums
 *             FILE : NegotiationRequest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : May 8, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums;

import java.util.LinkedList;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : May 8, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class NegotiationRequest extends LinkedList<AcceptNegotiationRequest> {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

}
