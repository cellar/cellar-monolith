package eu.europa.ec.opoce.cellar.cmr.transformers;

import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;

import org.apache.commons.collections15.Transformer;

/**
 * <p>DigitalObject2CellarUri class.</p>
 */
public class DigitalObject2CellarUri implements Transformer<DigitalObject, String> {

    /**
     * Constant <code>instance</code>
     */
    public static final Transformer<DigitalObject, String> instance = new DigitalObject2CellarUri();

    /**
     * {@inheritDoc}
     */
    @Override
    public String transform(DigitalObject input) {
        return input.getCellarId().getUri();
    }
}
