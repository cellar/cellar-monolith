/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : CellarIdentifier.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 24 mai 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.metsbeans;

import eu.europa.ec.opoce.cellar.cl.configuration.PrefixConfigurationService;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import eu.europa.ec.opoce.cellar.domain.content.mets.CellarIdentifiedObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.domain.content.mets.PrefixConfiguration;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;

/**
 * <p>MetsElement class.</p>
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class MetsElement extends CellarIdentifiedObject {

    private static final long serialVersionUID = 2981338932236234941L;

    private final transient PrefixConfiguration prefixConfiguration = ServiceLocator.getService(PrefixConfigurationService.class)
            .getPrefixConfiguration();

    private Date lastModificationDate;
    
    private Date embeddedNoticeCreationDate;

    private final Collection<String> productionUris = new HashSet<String>();

    protected Date embargoDate;

    private String mimeType;

    public MetsElement(final DigitalObjectType type) {
        this(type, Collections.emptyMap());
    }

    public MetsElement(final DigitalObjectType type, Map<ContentType, String> versions) {
        super(versions);
        setType(type);
    }

    public void setLastModificationDate(final Date lastModificationDate) {
        this.lastModificationDate = new Date(lastModificationDate.getTime());
    }

    public Date getLastModificationDate() {
        return new Date(lastModificationDate.getTime());
    }

    public Date getEmbeddedNoticeCreationDate() {
		return embeddedNoticeCreationDate;
	}

	public void setEmbeddedNoticeCreationDate(Date embeddedNoticeCreationDate) {
		this.embeddedNoticeCreationDate = new Date(embeddedNoticeCreationDate.getTime());
	}

	public void addUri(final String uri) {
        if (uri.startsWith(prefixConfiguration.getPrefixUri(ContentIdentifier.CELLAR_PREFIX))) {
            this.setCellarId(new ContentIdentifier(
                    uri.replace(prefixConfiguration.getPrefixUri(ContentIdentifier.CELLAR_PREFIX), ContentIdentifier.CELLAR_PREFIX + ":")));
        } else {
            productionUris.add(uri);
        }
    }

    public void addUris(final Collection<String> uris) {
        for (final String uri : uris) {
            addUri(uri);
        }
    }

    public Collection<String> getProductionUris() {
        return Collections.unmodifiableCollection(productionUris);
    }

    public void setCellarId(final String cellarId) {
        this.setCellarId(new ContentIdentifier(cellarId));
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }
}
