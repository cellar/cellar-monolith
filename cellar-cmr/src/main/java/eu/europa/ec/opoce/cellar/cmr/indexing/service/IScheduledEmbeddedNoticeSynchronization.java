package eu.europa.ec.opoce.cellar.cmr.indexing.service;

public interface IScheduledEmbeddedNoticeSynchronization {

    /**
     * Schedule a synchronization requests.
     */
    void scheduleSynchronizationRequests();

}
