/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.consolidation.service.impl
 *             FILE : ConsolidationServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 15, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.consolidation.service.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cmr.utils.MementoUtils;
import eu.europa.ec.opoce.cellar.cmr.sparql.jena.JenaTimeUtils;
import eu.europa.ec.opoce.cellar.common.http.headers.CellarHttpHeaders;
import eu.europa.ec.opoce.cellar.common.http.headers.HttpHeadersBuilder;
import eu.europa.ec.opoce.cellar.common.http.headers.RelType;
import eu.europa.ec.opoce.cellar.common.util.TimeUtils;
import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.server.dissemination.consolidation.enums.ConsolidationQueries;
import eu.europa.ec.opoce.cellar.server.dissemination.consolidation.service.IConsolidationService;
import eu.europa.ec.opoce.cellar.server.service.DisseminationException;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.jena.datatypes.xsd.XSDDateTime;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.sparql.engine.http.QueryEngineHTTP;

/**
 * Versioning POC. This service is based on the Python POC provided by OP.
 * ON : Dec 15, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class ConsolidationServiceImpl implements IConsolidationService {

    private static final Logger LOG = LogManager.getLogger(ConsolidationServiceImpl.class);

    private String celexBase;
    private String consolidationDataBase;
    private String consolidationMementoBase;

    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    @PostConstruct
    public void init() {
        this.celexBase = this.cellarConfiguration.getCellarUriDisseminationBase() + "resource/celex/";
        this.consolidationDataBase = this.cellarConfiguration.getCellarUriDisseminationBase() + "resource/consolidation/data/";
        this.consolidationMementoBase = this.cellarConfiguration.getCellarUriDisseminationBase() + "resource/consolidation/memento/";
    }

    @Override
    public ResponseEntity<?> getConsolidationMemento(final String identifier, final String rel, final String accept,
            final String acceptDatetime) {

        final String uri = this.celexBase + identifier;

        final String uriG = this.getUriG(uri);

        if (uriG == null) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                    .withMessage("Original resource for ['{}'] not found.").withMessageArgs(uri).build();
        }

        if (!this.isEvolutiveWork(uri)) {
            LOG.info("{} is an evolutive work.", uri);
            return this.mementoCallback(uri, uriG);
        }

        if (StringUtils.equalsIgnoreCase(rel, "timemap")) {
            LOG.info("Timemap will be returned.");
            return this.timemapCallback(uri, uriG, accept);
        } else if (StringUtils.equalsIgnoreCase(uriG, uri)) {
            LOG.info("Original timegate will be returned.");
            return this.originalTimegateCallback(uri, acceptDatetime);
        } else {
            LOG.info("Timegate will be returned.");
            return this.timegateCallback(uri, uriG, acceptDatetime);
        }
    }

    @Override
    public ResponseEntity<?> getConsolidationData(final String identifier) {

        final String uri = this.celexBase + identifier;

        if (StringUtils.endsWith(identifier, ".txt")) {
            return this.dataRepresentationCallback(uri.replace(".txt", ""), true);
        } else {
            return this.dataRepresentationCallback(uri.replace(".xml", ""), false);
        }
    }

    private ResponseEntity<?> dataRepresentationCallback(final String uri, final boolean linkFormat) {

        if (linkFormat) {
            LOG.info("Text links are requested.");
            final String timemap = this.generateLinkformatTimemap(uri);
            final HttpHeadersBuilder httpHeadersBuilder = HttpHeadersBuilder.get()
                    .withContentType("application/link-format; charset=utf-8");
            return new ResponseEntity<String>(timemap, httpHeadersBuilder.getHttpHeaders(), HttpStatus.OK);
        } else {
            LOG.info("RDF links are requested.");
            final String query = ConsolidationQueries.DESCRIBE_URI_TEMPLATE.getQuery(uri);
            final Model model = this.executeDescribeQuery(query);

            final HttpHeadersBuilder httpHeadersBuilder = HttpHeadersBuilder.get().withContentType("application/rdf+xml; charset=utf-8");

            return new ResponseEntity<String>(JenaUtils.toString(model), httpHeadersBuilder.getHttpHeaders(), HttpStatus.OK);
        }
    }

    private List<String> getRelatedEvolutiveWorks(final String uri) {

        final String query = ConsolidationQueries.SELECT_RELATED_EVOLUTIVE_WORKS_TEMPLATE.getQuery(uri);
        final ResultSet rs = this.executeSelectQuery(query);

        final List<String> relatedEvolutiveWorks = new LinkedList<String>();
        relatedEvolutiveWorks.add(uri);

        RDFNode node = null;
        while (rs.hasNext()) {
            node = rs.nextSolution().get("evolutive_work");
            if (node.isURIResource()) {
                relatedEvolutiveWorks.add(node.asResource().getURI());
            }
        }

        return relatedEvolutiveWorks;
    }

    private TimemapInfo getTimemapInfo(final String uri) {
        final String queryTminfo = ConsolidationQueries.SELECT_TIMEMAP_INFO_TEMPLATE.getQuery(uri);
        final ResultSet rsTminfo = this.executeSelectQuery(queryTminfo);

        if (rsTminfo.hasNext()) {
            final QuerySolution qs = rsTminfo.nextSolution();
            final RDFNode startdateNode = qs.get("startdate");
            final RDFNode enddateNode = qs.get("enddate");
            final RDFNode typeofdateNode = qs.get("typeofdate");

            final Date startdate = JenaTimeUtils.getDateOrNull(startdateNode);
            final Date enddate = JenaTimeUtils.getDateOrNull(enddateNode);

            if (startdate != null && enddate != null) {
                return new TimemapInfo(startdate, enddate, typeofdateNode.toString()); //TODO: toString?
            }
        }

        return null;
    }

    private void addOriginalTimegates(final StringBuilder body, final String uri) {

        final String query = ConsolidationQueries.SELECT_URI_G_TEMPLATE.getQuery(uri);
        final ResultSet rs = this.executeSelectQuery(query);

        RDFNode predecessor = null;
        while (rs.hasNext()) {
            predecessor = rs.nextSolution().get("predecessor");
            if (predecessor.isURIResource()) {
                body.append(MementoUtils.getMementoLink(this.toConsolidationMemento(predecessor.asResource().getURI()),
                        RelType.ORIGINAL_TIMEGATE));
                body.append("\n");
            }
        }
    }

    private void addMementos(final StringBuilder body, final String uri) {

        final String query = ConsolidationQueries.SELECT_RELATED_MEMENTOS_TEMPLATE.getQuery(uri);
        final ResultSet rs = this.executeSelectQuery(query);

        RDFNode memento = null, date = null;
        XSDDateTime xdtDate = null;
        QuerySolution qs = null;
        while (rs.hasNext()) {
            qs = rs.nextSolution();
            memento = qs.get("memento");
            date = qs.get("date");

            if (date != null && date instanceof XSDDateTime) {
                xdtDate = (XSDDateTime) date;
            }

            if (memento.isURIResource() && xdtDate != null) {
                body.append(MementoUtils.getMementoLink(this.toConsolidationMemento(memento.asResource().getURI()), RelType.MEMENTO,
                        xdtDate.asCalendar().getTime()));
                body.append("\n");
            }
        }
    }

    private Map<String, TimemapInfo> getTimemap(final String uri) {

        final List<String> relatedEvolutiveWorks = this.getRelatedEvolutiveWorks(uri);

        final Map<String, TimemapInfo> timemap = new HashMap<String, TimemapInfo>();
        TimemapInfo ti = null;
        for (final String t : relatedEvolutiveWorks) {
            ti = this.getTimemapInfo(t);
            if (ti != null) {
                timemap.put(this.toConsolidationMemento(t), ti);
            }
        }

        return timemap;
    }

    private static void addTimemaps(final StringBuilder body, final Map<String, TimemapInfo> timemap) {

        for (final Map.Entry<String, TimemapInfo> entry : timemap.entrySet()) {

            body.append(MementoUtils.getMementoLink(entry.getKey(), RelType.TIMEMAP, "application/link-format",
                    entry.getValue().getStartdate(), entry.getValue().getEnddate(), entry.getValue().getTypeofdate()));
            body.append("\n");
        }
    }

    private void addSelf(final StringBuilder body, final Map<String, TimemapInfo> timemap, final String uri) {
        final String consolidationUri = this.toConsolidationMemento(uri);

        body.append(MementoUtils.getMementoLink(consolidationUri, RelType.SELF, "application/link-format",
                timemap.get(consolidationUri).getStartdate(), timemap.get(consolidationUri).getEnddate(),
                timemap.get(consolidationUri).getTypeofdate()));
    }

    public String generateLinkformatTimemap(final String uri) {

        final Map<String, TimemapInfo> timemap = this.getTimemap(uri);

        final StringBuilder body = new StringBuilder();

        this.addOriginalTimegates(body, uri);
        this.addMementos(body, uri);
        addTimemaps(body, timemap);
        this.addSelf(body, timemap, uri);

        return body.toString();
    }

    private class TimemapInfo {

        private final Date startdate;
        private final Date enddate;
        private final String typeofdate;

        public TimemapInfo(final Date startdate, final Date enddate, final String typeofdate) {
            this.startdate = startdate;
            this.enddate = enddate;
            this.typeofdate = typeofdate;
        }

        /**
         * @return the startdate
         */
        public Date getStartdate() {
            return this.startdate;
        }

        /**
         * @return the enddate
         */
        public Date getEnddate() {
            return this.enddate;
        }

        /**
         * @return the typeofdate
         */
        public String getTypeofdate() {
            return this.typeofdate;
        }
    }

    private String getUriG(final String uri) {
        final String query = ConsolidationQueries.SELECT_URI_G_TEMPLATE.getQuery(uri);
        final ResultSet rs = this.executeSelectQuery(query);

        if (rs.hasNext()) {
            final RDFNode node = rs.nextSolution().get("predecessor");
            if (node.isURIResource()) {
                return node.asResource().getURI();
            }
        }

        return null;
    }

    private ResponseEntity<?> timegateCallback(final String uri, final String uriG, final String acceptDatetime) {

        Date acceptDatetimeObj = null;
        if (StringUtils.isNotBlank(acceptDatetime)) {
            acceptDatetimeObj = TimeUtils.getHTTP11DateSilently(acceptDatetime);
        }

        if (acceptDatetimeObj == null) {
            acceptDatetimeObj = new Date();
        }

        final String location = this.determineLocation(uri, acceptDatetimeObj);
        if (location == null) {
            return new ResponseEntity<String>("Bad Request. Check your query parameters", HttpStatus.NOT_ACCEPTABLE);
        }

        final String localhostUriG = this.toConsolidationMemento(uriG);
        final String localhostUriTg = this.toConsolidationMemento(uriG + "?rel=timemap");
        final String localhostUri = this.toConsolidationMemento(uri);
        final String localhostUriT = localhostUri + "?rel=timemap";

        final Date mementoDatetime = this.getMementoDatetime(uri);

        if (mementoDatetime == null) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                    .withMessage("Memento for ['{}'] not found.").withMessageArgs(uri).build();
        }

        final HttpHeadersBuilder httpHeadersBuilder = HttpHeadersBuilder.get().withLocation(this.toConsolidationMemento(location))
                .withMementoDatetime(mementoDatetime).withLink(MementoUtils.getMementoLink(localhostUriG, RelType.ORIGINAL_TIMEGATE))
                .withLink(MementoUtils.getMementoLink(localhostUriTg, RelType.TIMEMAP))
                .withLink(MementoUtils.getMementoLink(localhostUri, RelType.TIMEGATE))
                .withLink(MementoUtils.getMementoLink(localhostUriT, RelType.TIMEMAP));

        return new ResponseEntity<Void>(httpHeadersBuilder.getHttpHeaders(), HttpStatus.FOUND);
    }

    private ResponseEntity<?> originalTimegateCallback(final String uriG, final String acceptDatetime) {

        String location = null;
        if (StringUtils.isNotBlank(acceptDatetime)) {
            final Date acceptDatetimeObj = TimeUtils.getHTTP11DateSilently(acceptDatetime);

            location = this.determineLocation(uriG, acceptDatetimeObj);
        } else {
            final Date now = new Date();
            location = uriG;

            while (this.isEvolutiveWork(location)) {
                location = this.determineLocation(location, now);
                if (location == null) {
                    break;
                }
            }
        }

        if (location == null) {
            return new ResponseEntity<String>("Bad Request. Check your query parameters", HttpStatus.NOT_ACCEPTABLE);
        }

        final String localhostUriG = this.toConsolidationMemento(uriG);
        final String localhostUriT = this.toConsolidationMemento(uriG + "?rel=timemap");

        final HttpHeadersBuilder httpHeadersBuilder = HttpHeadersBuilder.get().withLocation(this.toConsolidationMemento(location))
                .withLink(MementoUtils.getMementoLink(localhostUriG, RelType.ORIGINAL_TIMEGATE))
                .withLink(MementoUtils.getMementoLink(localhostUriT, RelType.TIMEMAP)).withVary(CellarHttpHeaders.ACCEPT_DATETIME);

        return new ResponseEntity<Void>(httpHeadersBuilder.getHttpHeaders(), HttpStatus.FOUND);
    }

    private String determineLocation(final String uriG, final Date acceptDatetime) {

        final String query = ConsolidationQueries.SELECT_LOCATION_TEMPLATE.getQuery(uriG,
                TimeUtils.formatTimestampForSparqlQuery(acceptDatetime));

        final ResultSet rs = this.executeSelectQuery(query);

        if (rs.hasNext()) {
            final RDFNode node = rs.nextSolution().get("successor");

            if (node != null && node.isURIResource()) {
                final String location = node.asResource().getURI();
                return location;
            }
        }

        return null;
    }

    private ResponseEntity<?> timemapCallback(final String uri, final String uriG, final String accept) {

        final String localhostUriG = this.toConsolidationMemento(uriG);

        final HttpHeadersBuilder httpHeadersBuilder = HttpHeadersBuilder.get();
        if (StringUtils.equalsIgnoreCase(accept, "application/link-format")) {
            httpHeadersBuilder.withLocation(this.toConsolidationData(uri, ".txt"));
        } else {
            httpHeadersBuilder.withLocation(this.toConsolidationData(uri, ".xml"));
        }

        httpHeadersBuilder.withLink(MementoUtils.getMementoLink(localhostUriG, RelType.ORIGINAL_TIMEGATE));

        return new ResponseEntity<Void>(httpHeadersBuilder.getHttpHeaders(), HttpStatus.SEE_OTHER);
    }

    private ResponseEntity<?> mementoCallback(final String uri, final String uriG) {

        final String localhostUriG = this.toConsolidationMemento(uriG);
        final String localhostUriT = this.toConsolidationMemento(uriG + "?rel=timemap");

        final Date mementoDate = this.getMementoDatetime(uri);

        if (mementoDate == null) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                    .withMessage("Memento for ['{}'] not found.").withMessageArgs(uri).build();
        }

        final HttpHeadersBuilder httpHeadersBuilder = HttpHeadersBuilder.get().withLocation(this.toConsolidationData(uri, ".xml"))
                .withMementoDatetime(mementoDate).withLink(MementoUtils.getMementoLink(localhostUriG, RelType.ORIGINAL_TIMEGATE))
                .withLink(MementoUtils.getMementoLink(localhostUriT, RelType.TIMEMAP));

        return new ResponseEntity<Void>(httpHeadersBuilder.getHttpHeaders(), HttpStatus.SEE_OTHER);
    }

    private Date getMementoDatetime(final String uri) {

        final String query = ConsolidationQueries.SELECT_MEMENTO_DATETIME_TEMPLATE.getQuery(uri);

        final ResultSet rs = this.executeSelectQuery(query);
        if (rs.hasNext()) {
            final RDFNode node = rs.nextSolution().get("date");

            return JenaTimeUtils.getDateOrNull(node);
        }

        return null;
    }

    private boolean isEvolutiveWork(final String uri) {

        final String query = ConsolidationQueries.SELECT_EVOLUTIVE_WORK_TEMPLATE.getQuery(uri);

        final ResultSet rs = this.executeSelectQuery(query);

        return rs.hasNext();
    }

    private ResultSet executeSelectQuery(final String query) {
        try(final QueryEngineHTTP queryExecution = new QueryEngineHTTP(this.cellarConfiguration.getCellarServiceSparqlUri(), query)) {
            return queryExecution.execSelect();
        }
    }

    private Model executeDescribeQuery(final String query) {
        try(final QueryEngineHTTP queryExecution = new QueryEngineHTTP(this.cellarConfiguration.getCellarServiceSparqlUri(), query)) {
            return queryExecution.execDescribe();
        }
    }

    private String toConsolidationData(final String uri, final String fext) {
        return uri.replace(this.celexBase, this.consolidationDataBase) + fext;
    }

    private String toConsolidationMemento(final String uri) {
        return uri.replace(this.celexBase, this.consolidationMementoBase);
    }
}
