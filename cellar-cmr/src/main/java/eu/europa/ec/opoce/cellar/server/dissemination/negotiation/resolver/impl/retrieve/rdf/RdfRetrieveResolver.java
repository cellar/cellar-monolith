/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.resolver.impl.rdf
 *             FILE : RdfDisseminationResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 20, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.retrieve.rdf;

import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.InferenceVariance;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.Structure;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.IDisseminationResolver;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.retrieve.RetrieveResolver;
import eu.europa.ec.opoce.cellar.server.service.DisseminationException;

import java.util.Date;

import org.springframework.http.HttpStatus;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 20, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public abstract class RdfRetrieveResolver extends RetrieveResolver {

    protected final InferenceVariance inferenceMode;
    protected final boolean normalized;

    public static IDisseminationResolver get(final String identifier, final Structure noticeStructure,
            final InferenceVariance inferenceMode, final String eTag, final String lastModified, final boolean provideResponseBody,
            final Date acceptDateTime,boolean... normalized) {
        switch (noticeStructure) {
        case OBJECT:
            return RdfObjectRetrieveResolver.get(identifier, inferenceMode, eTag, lastModified, provideResponseBody, acceptDateTime,normalized.length>0?true:false);
        case TREE:
            return RdfTreeRetrieveResolver.get(identifier, inferenceMode, eTag, lastModified, provideResponseBody, acceptDateTime,normalized.length>0?true:false);
        default:
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage("Invalid structure of RDF notice: '{}'").withMessageArgs(noticeStructure).build();
        }
    }

    protected RdfRetrieveResolver(final String identifier, final InferenceVariance inferenceMode, final String eTag,
            final String lastModified, final boolean provideResponseBody, final Date acceptDateTime,boolean normalized) {
        super(identifier, eTag, lastModified, provideResponseBody, acceptDateTime);
        this.inferenceMode = inferenceMode;
        this.normalized=normalized;
    }
}
