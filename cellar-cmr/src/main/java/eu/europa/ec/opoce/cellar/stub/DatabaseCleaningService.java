package eu.europa.ec.opoce.cellar.stub;

import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CmrIndexRequestDao;
import eu.europa.ec.opoce.cellar.cmr.database.dao.InverseRelationDao;
import eu.europa.ec.opoce.cellar.database.CmrTableName;
import eu.europa.ec.opoce.cellar.database.Database;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.stereotype.Service;

import static eu.europa.ec.opoce.cellar.database.CmrTableName.CMR_INVERSE;
import static eu.europa.ec.opoce.cellar.database.CmrTableName.CMR_METADATA;

/**
 * <p>DatabaseCleaningService class.</p>
 *
 * @deprecated because not used.
 */
@Deprecated
@Service
public class DatabaseCleaningService {

    /**
     * Constant <code>LOG</code>
     */
    private static final Logger LOG = LogManager.getLogger(DatabaseCleaningService.class);

    @Autowired
    private InverseRelationDao inverseRelationDao;

    @Autowired
    private CmrIndexRequestDao indexRequestDao;

    @Autowired
    private CellarResourceDao cellarResourceDao;

    @Autowired
    @Qualifier("databaseCellarDataCmr")
    protected Database database;

    /**
     * clean all tables that cmr fills during ingest of packages this so there is an easy way to start from clean cmr-database
     */
    public void cleanDatabase() {
        LOG.info("start cleaning database");

        this.inverseRelationDao.deleteAll();
        this.cellarResourceDao.deleteAll();
        this.indexRequestDao.deleteAll();
        this.resetTable(CMR_METADATA);
        this.resetTable(CMR_INVERSE);
        this.resetTable(CMR_METADATA.getEmbargoTable());
        this.resetTable(CMR_INVERSE.getEmbargoTable());
        this.resetTable(CMR_INVERSE.getDisseminationTable());

        try {
            this.database.getJdbcOperations().execute("truncate table production_identifier");
            this.database.getJdbcOperations().execute("ALTER TABLE production_identifier DISABLE CONSTRAINT FK75A913EF52D4D9C4");
            this.database.getJdbcOperations().execute("truncate table cellar_identifier");
            this.database.getJdbcOperations().execute("ALTER TABLE production_identifier ENABLE CONSTRAINT FK75A913EF52D4D9C4");
            this.database.getJdbcOperations().execute("truncate table DO_METADATA");
        } catch (final DataAccessException e) {
            LOG.warn("the cellar and ps-ids could not be cleaned, these database tables are not accessible");
        }

        LOG.info("done cleaning database");
    }

    /**
     * <p>resetTable.</p>
     *
     * @param table a {@link java.lang.String} object.
     */
    private void resetTable(final CmrTableName table) {
        final String tableName = table.toString();
        final JdbcOperations jdbcOperations = this.database.getJdbcOperations();
        jdbcOperations.execute(new StringBuilder("TRUNCATE TABLE ").append(tableName).append(" DROP STORAGE").toString());
        jdbcOperations.execute(this.getAddTriple(tableName, "<http://publications.europa.eu/ontology/cdm#" + DigitalObjectType.AGENT + ">",
                "<http://publications.europa.eu/ontology/cdm/cmr#metsDivType>", DigitalObjectType.AGENT.toString()));
        jdbcOperations.execute(
                this.getAddTriple(tableName, "<http://publications.europa.eu/ontology/cdm#" + DigitalObjectType.TOPLEVELEVENT + ">",
                        "<http://publications.europa.eu/ontology/cdm/cmr#metsDivType>", DigitalObjectType.TOPLEVELEVENT.toString()));
        jdbcOperations
                .execute(this.getAddTriple(tableName, "<http://publications.europa.eu/ontology/cdm#" + DigitalObjectType.DOSSIER + ">",
                        "<http://publications.europa.eu/ontology/cdm/cmr#metsDivType>", DigitalObjectType.DOSSIER.toString()));
        jdbcOperations.execute(this.getAddTriple(tableName, "<http://publications.europa.eu/ontology/cdm#" + DigitalObjectType.EVENT + ">",
                "<http://publications.europa.eu/ontology/cdm/cmr#metsDivType>", DigitalObjectType.EVENT.toString()));
        jdbcOperations
                .execute(this.getAddTriple(tableName, "<http://publications.europa.eu/ontology/cdm#" + DigitalObjectType.EXPRESSION + ">",
                        "<http://publications.europa.eu/ontology/cdm/cmr#metsDivType>", DigitalObjectType.EXPRESSION.toString()));
        jdbcOperations.execute(
                this.getAddTriple(tableName, "<http://publications.europa.eu/ontology/cdm#" + DigitalObjectType.MANIFESTATION + ">",
                        "<http://publications.europa.eu/ontology/cdm/cmr#metsDivType>", DigitalObjectType.MANIFESTATION.toString()));
        jdbcOperations.execute(this.getAddTriple(tableName, "<http://publications.europa.eu/ontology/cdm#" + DigitalObjectType.WORK + ">",
                "<http://publications.europa.eu/ontology/cdm/cmr#metsDivType>", DigitalObjectType.WORK.toString()));
    }

    /**
     * <p>getAddTriple.</p>
     *
     * @param tableName a {@link java.lang.String} object.
     * @param subject   a {@link java.lang.String} object.
     * @param predicate a {@link java.lang.String} object.
     * @param object    a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    private String getAddTriple(final String tableName, final String subject, final String predicate, final String object) {
        return new StringBuilder().append("INSERT INTO ").append(tableName).append("(TRIPLE) VALUES (SDO_RDF_TRIPLE_S('").append(tableName)
                .append("','").append(subject).append("','").append(predicate).append("','\"").append(object).append("\"') )").toString();
    }

}
