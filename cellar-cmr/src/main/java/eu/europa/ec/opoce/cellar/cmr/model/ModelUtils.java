/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.cmr.model
 *        FILE : ModelUtils.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 23-05-2013
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.model;

import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.common.jena.MultipleSubjectsAwareSelector;
import eu.europa.ec.opoce.cellar.common.map.SetBasedMap;
import eu.europa.ec.opoce.cellar.common.util.IOUtils;
import eu.europa.ec.opoce.cellar.common.util.Pair;
import eu.europa.ec.opoce.cellar.core.ConvertHistoryMap;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentStream;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsPackage;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.CellarValidationException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.ontology.service.OntologyService;
import eu.europa.ec.opoce.cellar.semantic.helper.DateFormats;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaQueries;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarAnnotationProperty;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.graph.*;
import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.*;
import java.util.Map.Entry;

import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.*;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.HierarchyProperty.cdm_item_belongs_to_manifestationP;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarType.cdm_itemR;

/**
 * <class_description> Utility for models.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 23-05-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class ModelUtils extends JenaUtils {

    /**
     * The Constant LOG.
     */
    private static final Logger LOG = LogManager.getLogger(ModelUtils.class);

    /**
     * Let's consider the property <code>p</code> annotated with <code>write-once</code>. This method throws an exception if:
     * <ul>
     * <li>the METS package being inserted contains the triple <code>s1 p o1</code> and the Cellar contains a triple <code>s1 p o2</code> with <code>o1 != o2</code></li>
     * <li>the METS package being inserted does not contain a triple <code>s1 p ?o</code> and the Cellar contains a triple <code>s1 p ?o</code>.</li>
     * </ul>
     *
     * @param metsId          the ID of the METS document of reference
     * @param ontologyService the Ontology Service used to retrieve the properties annotated as write-once
     * @param newModel        the new model
     * @param oldModel        the old existing model
     * @param sameAses        the possible map of sameAses
     * @return a collection of triples - whose properties are annotated as write-once - which exist in the old model, but not in the new one
     * @throws CellarValidationException in the case described above
     */
    public static Collection<Triple> validateProtectedTriples(final String metsId, final OntologyService ontologyService,
                                                              final Model newModel, final Model oldModel, final SetBasedMap<String> sameAses) throws CellarValidationException {
        final List<Triple> protectedTriplesExistingInOldButNotInNewModel = new ArrayList<Triple>();

        // get the existing model whose predicate is annotated as write once
        Model oldProtectedModel = null;
        try {
            oldProtectedModel = getProtectedModel(ontologyService, oldModel);

            // check if offending triples are being inserted
            final StmtIterator oldProtectedModelIter = oldProtectedModel.listStatements();
            try {
                while (oldProtectedModelIter.hasNext()) {
                    final Triple oldProtectedTriple = oldProtectedModelIter.next().asTriple();

                    // selects the existing triples with write-once predicate
                    final Set<Resource> currSubjects = getSubjectsAndSameAses(oldProtectedTriple.getSubject().getURI(), sameAses);
                    final Property currPredicate = ResourceFactory.createProperty(oldProtectedTriple.getPredicate().getURI());
                    final Node currObject = oldProtectedTriple.getObject();
                    final Selector selector = new MultipleSubjectsAwareSelector(currSubjects, currPredicate, (String) null);

                    // cycle on the new triples of the new model
                    boolean newProtectedTripleExists = false;
                    final StmtIterator newProtectedTriplesIter = newModel.listStatements(selector);
                    try {
                        while (newProtectedTriplesIter.hasNext()) {
                            newProtectedTripleExists = true;
                            final Statement newProtectedTriple = newProtectedTriplesIter.next();

                            // throw an exception if there are new triples with same subject (or one of its sameAses) and write-once predicate, but different object
                            for (final Resource currSubject : currSubjects) {
                                if (currSubject.equals(newProtectedTriple.getSubject())
                                        && currPredicate.equals(newProtectedTriple.getPredicate())) {
                                    final Node statObject = newProtectedTriple.getObject().asNode();
                                    if (!currObject.sameValueAs(statObject)) {
                                        throw ExceptionBuilder.get(CellarValidationException.class)
                                                .withCode(CmrErrors.VALIDATION_EXISTING_WRITE_ONCE_TRIPLE).withMetsDocumentId(metsId)
                                                .withMessage(
                                                        "The new triple {} is being inserted, but the triple [{}] - which has same subject (or sameAs), same write-once property, but different object - already exists in the database.")
                                                .withMessageArgs(newProtectedTriple, oldProtectedTriple).build();
                                    }
                                }
                            }
                        }
                    } finally {
                        newProtectedTriplesIter.close();
                    }

                    // if there are no new triples with same subject and write-once predicate, just add to the return object
                    if (!newProtectedTripleExists) {
                        protectedTriplesExistingInOldButNotInNewModel
                                .add(new Triple(currSubjects.iterator().next().asNode(), currPredicate.asNode(), currObject));
                    }
                }
            } finally {
                oldProtectedModelIter.close();
            }
        } finally {
            ModelUtils.closeQuietly(oldProtectedModel);
        }

        return protectedTriplesExistingInOldButNotInNewModel;
    }

    /**
     * Let's consider the property <code>p</code> <code>creationDate</code>. This method throws an exception if:
     * <ul>
     * <li>the METS package being inserted contains the triple <code>s1 p o1</code> and the Cellar contains a triple <code>s1 p o2</code> with <code>o1 != o2</code></li>
     * <li>the METS package being inserted does not contain a triple <code>s1 p ?o</code> and the Cellar contains a triple <code>s1 p ?o</code>.</li>
     * </ul>
     *
     * @param metsId   the ID of the METS document of reference
     * @param newModel the new model
     * @param oldModel the old existing model
     * @param sameAses the possible map of sameAses
     * @return a collection of triples - whose properties are creationDate - which exist in the old model, but not in the new one
     * @throws CellarValidationException in the case described above
     */
    public static Collection<Triple> validateTriplesWithCreationDate(final String metsId,
                                                                     final Model newModel, final Model oldModel, final SetBasedMap<String> sameAses) throws CellarValidationException {
        final List<Triple> creationDateTriplesExistingInOldButNotInNewModel = new ArrayList<Triple>();

        // get the existing model whose properties is creationDate
        Model oldCreationDateModel = null;
        try {
            oldCreationDateModel = getModelByCreationDate(oldModel);

            // check if offending triples are being inserted
            final StmtIterator oldCreationDateModelIter = oldCreationDateModel.listStatements();
            try {
                while (oldCreationDateModelIter.hasNext()) {
                    final Triple oldCreationDateTriple = oldCreationDateModelIter.next().asTriple();

                    // selects the existing triples with creationDate property
                    final Set<Resource> currSubjects = getSubjectsAndSameAses(oldCreationDateTriple.getSubject().getURI(), sameAses);
                    final Property currPredicate = ResourceFactory.createProperty(oldCreationDateTriple.getPredicate().getURI());
                    final Node currObject = oldCreationDateTriple.getObject();
                    final Selector selector = new MultipleSubjectsAwareSelector(currSubjects, currPredicate, (String) null);

                    // cycle on the new triples of the new model
                    boolean newCreationDateTripleExists = false;
                    final StmtIterator newCreationDateTriplesIter = newModel.listStatements(selector);
                    try {
                        while (newCreationDateTriplesIter.hasNext()) {
                            newCreationDateTripleExists = true;
                            final Statement newCreationDateTriple = newCreationDateTriplesIter.next();

                            // throw an exception if there are new triples with same subject (or one of its sameAses) and creationDate property, but different object
                            for (final Resource currSubject : currSubjects) {
                                if (currSubject.equals(newCreationDateTriple.getSubject())
                                        && currPredicate.equals(newCreationDateTriple.getPredicate())) {
                                    final Node statObject = newCreationDateTriple.getObject().asNode();
                                    if (!currObject.sameValueAs(statObject)) {
                                        throw ExceptionBuilder.get(CellarValidationException.class).withMetsDocumentId(metsId)
                                                .withMessage(
                                                        "The new triple {} is being inserted, but the triple [{}] - which has same subject (or sameAs), same creationDate property, but different object - already exists in the database.")
                                                .withMessageArgs(newCreationDateTriple, oldCreationDateTriple).build();
                                    }
                                }
                            }
                        }
                    } finally {
                        newCreationDateTriplesIter.close();
                    }

                    // if there are no new triples with same subject and creationDate property, just add to the return object
                    if (!newCreationDateTripleExists) {
                        creationDateTriplesExistingInOldButNotInNewModel
                                .add(new Triple(currSubjects.iterator().next().asNode(), currPredicate.asNode(), currObject));
                    }
                }
            } finally {
                oldCreationDateModelIter.close();
            }
        } finally {
            ModelUtils.closeQuietly(oldCreationDateModel);
        }

        return creationDateTriplesExistingInOldButNotInNewModel;
    }

    /**
     * This method returns a model containing triples whose properties are annotated with <code>write-once</code>.
     *
     * @param ontologyService the ontology service
     * @param model           the model
     * @return the protected model
     */
    public static Model getProtectedModel(final OntologyService ontologyService, final Model model) {
        return getModelByProtection(ontologyService, model, true);
    }

    /**
     * This method returns a model containing triples whose properties are not annotated with <code>write-once</code>.
     *
     * @param ontologyService the ontology service
     * @param model           the model
     * @return the unprotected model
     */
    public static Model getUnprotectedModel(final OntologyService ontologyService, final Model model) {
        return getModelByProtection(ontologyService, model, false);
    }

    /**
     * This method returns a model containing triples with single max cardinality.
     *
     * @param ontologyService the ontology service
     * @param model           the model
     * @return the single max cardinality model
     */
    public static Model getSingleMaxCardinalityModel(final OntologyService ontologyService, final Model model) {
        return getModelByMaxCardinality(ontologyService, model, true);
    }

    /**
     * This method returns a model containing triples with multiple max cardinality.
     *
     * @param ontologyService the ontology service
     * @param model           the model
     * @return the multipe max cardinality model
     */
    public static Model getMultipeMaxCardinalityModel(final OntologyService ontologyService, final Model model) {
        return getModelByMaxCardinality(ontologyService, model, false);
    }

    /**
     * Serialize.
     *
     * @param model the model
     * @param lang  the lang
     * @return the string
     */
    public static String serialize(final Model model, final String lang) {
        String modelStr = null;

        final OutputStream modelOS = new ByteArrayOutputStream();
        model.write(modelOS, lang);
        modelStr = modelOS.toString();
        try {
            modelOS.close();
        } catch (final IOException e) {
            LOG.warn("A problem occurred while closing model stream.", e);
        }

        return modelStr;
    }

    /**
     * Deserialize.
     *
     * @param modelStr the model str
     * @param lang     the lang
     * @return the model
     */
    public static Model deserialize(final String modelStr, final String lang) {
        final String myModelStr = modelStr == null ? "" : modelStr;
        final InputStream modelStream = new ByteArrayInputStream(myModelStr.getBytes(Charset.forName("UTF-8")));
        return deserialize(modelStream, lang);
    }

    /**
     * Deserialize.
     *
     * @param modelStream the model stream
     * @param lang        the lang
     * @return the model
     */
    public static Model deserialize(final InputStream modelStream, final String lang) {
        final Model model = ModelFactory.createDefaultModel();
        if (modelStream == null) {
            return model;
        }

        model.read(modelStream, lang);

        IOUtils.closeQuietly(modelStream);

        return model;
    }

    /**
     * Gets the read only statements.
     *
     * @param model    the model
     * @param subjects the subjects
     * @return the read only statements
     */
    public static List<Statement> getReadOnlyStatements(final Model model, Set<Resource> subjects) {
        final Selector selector = new MultipleSubjectsAwareSelector(subjects, CellarAnnotationProperty.annotation_read_onlyP,
                (String) null);
        return model.listStatements(selector).toList();
    }

    /**
     * This method returns a model containing triples whose properties are creationDate.
     *
     * @param model the model
     * @return a model containing triples whose properties are creationDate
     */
    public static Model getModelByCreationDate(final Model model) {
        final SortedSet<String> creationDateProperties = new TreeSet<String>();
        creationDateProperties.add(CellarProperty.cmr_creationdate);

        // return the model
        return getModelBy(creationDateProperties, model, true);
    }

    /**
     * <p>getModelFromTriples convert collection of triples in jena-model.</p>
     *
     * @param triples the triples
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    public static Model getModelFromTriples(final Collection<Triple> triples) {
        final Model model = ModelFactory.createDefaultModel();
        try {
            GraphUtil.add(model.getGraph(), triples.iterator());
        } catch (final RuntimeException e) {
            ModelUtils.closeQuietly(model);
            throw e;
        }
        return model;
    }

    /**
     * Gets the model from triples per context.
     *
     * @param triplesPerContext the triples per context
     * @return the model from triples per context
     */
    public static Model getModelFromTriplesPerContext(final Map<String, Collection<Triple>> triplesPerContext) {
        final Model model = ModelFactory.createDefaultModel();

        for (final Collection<Triple> triples : triplesPerContext.values()) {
            model.add(getModelFromTriples(triples));
        }

        return model;
    }

    /**
     * Gets the model per context from triples per context.
     *
     * @param triplesPerContext the triples per context
     * @return the model per context from triples per context
     */
    public static Map<String, Model> getModelPerContextFromTriplesPerContext(final Map<String, Collection<Triple>> triplesPerContext) {
        final Map<String, Model> modelPerContext = new HashMap<String, Model>();

        for (final Entry<String, Collection<Triple>> entry : triplesPerContext.entrySet()) {
            final String context = entry.getKey();
            final Collection<Triple> triples = entry.getValue();
            final Model model = getModelFromTriples(triples);
            modelPerContext.put(context, model);
        }

        return modelPerContext;
    }

    /**
     * <p>getModelsFromPairs convert a collection of triple-context pairs into a map of context-model.</p>
     *
     * @param pairs the pairs
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    public static Map<String, Model> getModelsFromPairs(final Collection<Pair<Triple, String>> pairs) {
        final Map<String, Model> retModels = new HashMap<String, Model>();

        for (final Pair<Triple, String> pair : pairs) {
            final Triple triple = pair.getOne();
            final String context = pair.getTwo();

            Model model = retModels.get(context);
            if (retModels.get(context) == null) {
                model = ModelFactory.createDefaultModel();
                retModels.put(context, model);
            }
            model.add(model.asStatement(triple));
        }

        return retModels;
    }

    /**
     * Gets the model from model per context.
     *
     * @param modelPerContext the model per context
     * @return the model from model per context
     */
    public static Model getModelFromModelPerContext(final Map<String, Model> modelPerContext) {
        final Model model = ModelFactory.createDefaultModel();

        for (final Entry<String, Model> entry : modelPerContext.entrySet()) {
            final Model currModel = entry.getValue();
            model.add(currModel);
        }

        return model;
    }

    /**
     * Gets the triples per context from model per context.
     *
     * @param modelPerContext the model per context
     * @return the triples per context from model per context
     */
    public static Map<String, Collection<Triple>> getTriplesPerContextFromModelPerContext(final Map<String, Model> modelPerContext) {
        final Map<String, Collection<Triple>> triplesPerContext = new HashMap<String, Collection<Triple>>();

        for (final Entry<String, Model> entry : modelPerContext.entrySet()) {
            final String currContext = entry.getKey();
            final Model currModel = entry.getValue();
            triplesPerContext.put(currContext, ModelUtils.getTriplesFromModel(currModel));
        }

        return triplesPerContext;
    }

    /**
     * <p>getTriplesFromModels convert a model into a collection of triples.</p>
     *
     * @param model the model
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    public static Collection<Triple> getTriplesFromModel(final Model model) {
        final Collection<Triple> triples = new ArrayList<Triple>();

        final StmtIterator iterator = model.listStatements();
        try {
            while (iterator.hasNext()) {
                triples.add(iterator.next().asTriple());
            }
        } finally {
            iterator.close();
        }
        return triples;
    }

    /**
     * <p>getTriplesFromModels convert a map context-model into a map of context-triples.</p>
     *
     * @param models the models
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    public static Map<String, Collection<Triple>> getTriplesFromModels(final Map<String, Model> models) {
        final Map<String, Collection<Triple>> triples = new HashMap<String, Collection<Triple>>();

        for (final Entry<String, Model> model : models.entrySet()) {
            triples.put(model.getKey(), getTriplesFromModel(model.getValue()));
        }

        return triples;
    }

    /**
     * <p>getModelFromMapQuads convert map of quads in a jena-model.</p>
     *
     * @param quads the quads
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    public static Model getModelFromMapQuads(final Collection<Pair<Triple, String>> quads) {
        final Model model = ModelFactory.createDefaultModel();
        try {
            GraphUtil.add(model.getGraph(), CollectionUtils.collect(quads, Pair::getOne).iterator());
        } catch (final RuntimeException e) {
            ModelUtils.closeQuietly(model);
            throw e;
        }
        return model;
    }

    /**
     * <p>getResourceUris get the resource URIs out of the model.</p>
     *
     * @param inputModel the input model
     * @return the collection of URIs
     */
    public static Collection<String> getResourceUris(final Model inputModel) {
        final Set<String> uris = new HashSet<String>();
        final NodeIterator nodeIterator = inputModel.listObjects();
        final ResIterator resIterator = inputModel.listSubjects();
        try {
            while (nodeIterator.hasNext()) {
                final RDFNode rdfNode = nodeIterator.next();
                if (rdfNode.isURIResource()) {
                    final String uri = rdfNode.asResource().getURI();
                    if (uri.contains("/resource/") && !uri.contains("/resource/authority/")) {
                        uris.add(uri);
                    }
                }
            }
            while (resIterator.hasNext()) {
                final Resource resource = resIterator.next();
                if (resource.isURIResource()) {
                    final String uri = resource.getURI();
                    if (uri.contains("/resource/") && !uri.contains("/resource/authority/")) {
                        uris.add(uri);
                    }
                }
            }
        } finally {
            nodeIterator.close();
            resIterator.close();
        }
        return uris;
    }

    /**
     * Returns <code>true</code> if the given {@code model} is null or empty.
     *
     * @param model the model
     * @return true, if is blank
     */
    public static boolean isBlank(final Model model) {
        return (model == null) || model.isEmpty();
    }

    /**
     * <p>add</p>.
     *
     * @param destModel   the dest model
     * @param modelsToAdd the models to add
     */
    public static void add(final Model destModel, final Collection<Model> modelsToAdd) {
        if (destModel == null) {
            return;
        }

        for (final Model modelToAdd : modelsToAdd) {
            if (modelToAdd != null) {
                destModel.add(modelToAdd);
            }
        }
    }

    /**
     * <p>closeQuietly closes all models that can be found in a map.</p>
     *
     * @param <T> the generic type
     * @param map a {@link java.util.Map} object.
     */
    public static <T> void closeQuietly(final Map<T, Model> map) {
        if (map != null) {
            ModelUtils.closeQuietly(map.values());
        }
    }

    /**
     * <p>getQuads get all quads that are can be found in the given Map&lt;DigitalObject,Model&gt;.</p>
     *
     * @param metadata a {@link java.util.Map} object.
     * @return a {@link java.util.List} object.
     */
    public static List<Pair<Triple, String>> getQuadsWithDigitalObject(final Map<DigitalObject, Model> metadata) {
        return getQuads(metadata, new DigitalObjectContextExtractor());
    }

    /**
     * Gets the quads with string.
     *
     * @param metadata the metadata
     * @return the quads with string
     */
    public static List<Pair<Triple, String>> getQuadsWithString(final Map<String, Model> metadata) {
        return getQuads(metadata, new StringContextExtractor());
    }

    /**
     * <p>getQuads get all quads that are can be found in the given Model, set context to clustername.</p>
     *
     * @param cluster the cluster where the triples of given model need to be put in
     * @param model   the model that contains the triples
     * @return list of quads (triple and context)
     */
    public static List<Pair<Triple, String>> getQuads(final String cluster, final Model model) {
        final List<Pair<Triple, String>> result = new ArrayList<Pair<Triple, String>>();

        final ExtendedIterator<Triple> iterator = GraphUtil.findAll(model.getGraph());
        try {
            while (iterator.hasNext()) {
                final Triple triple = iterator.next();
                result.add(new Pair<Triple, String>(triple, cluster));
            }
        } finally {
            iterator.close();
        }

        return renameBlankNodes(result, cluster);
    }

    /**
     * <p>renameBlankNodes so that they are unique in the complete database.</p>
     *
     * @param quads   a {@link java.util.List} object.
     * @param cluster a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    public static List<Pair<Triple, String>> renameBlankNodes(final List<Pair<Triple, String>> quads, final String cluster) {
        final List<Pair<Triple, String>> newQuads = new ArrayList<Pair<Triple, String>>();
        final NodeConversionMap nodeConversionMap = new NodeConversionMap(cluster);
        for (final Pair<Triple, String> quad : quads) {
            final Triple triple = quad.getOne();
            newQuads.add(new Pair<Triple, String>(new Triple(nodeConversionMap.getConvertedValue(triple.getSubject()),
                    triple.getPredicate(), nodeConversionMap.getConvertedValue(triple.getObject())), quad.getTwo()));
        }
        return newQuads;
    }

    /**
     * Statements difference.
     *
     * @param model1 the model 1
     * @param model2 the model 2
     * @return the long
     */
    public static long statementsDifference(final Model model1, final Model model2) {
        long difference = 0;

        Model differenceModel = null;
        try {
            differenceModel = model1.difference(model2);
            difference = differenceModel.size();
        } finally {
            ModelUtils.closeQuietly(differenceModel);
        }

        return difference;
    }

    /**
     * Load allowed cellar resources only.
     *
     * @param metsPackage               the mets package
     * @param fileRef                   the file ref
     * @param allowedCellarPrefixUris   the allowed cellar prefix uris
     * @param allowedCellarResourceUris the allowed cellar resource uris
     * @param metadataModel             the metadata model
     */
    public static void loadAllowedCellarResourcesOnly(final MetsPackage metsPackage, final String fileRef,
                                                      final String[] allowedCellarPrefixUris, final Set<String> allowedCellarResourceUris, final Model metadataModel) {
         InputStream input=null;
         Model srcModel;
        try {
            input = metsPackage.getFileStream(fileRef);

            if (input == null) {
                throw ExceptionBuilder.get(CellarException.class).withMessage("Metadata inputstream is null for fileref '{}'")
                        .withMessageArgs(fileRef).build();
            }

            srcModel = ModelFactory.createDefaultModel();
            JenaUtils.read(srcModel, input, fileRef, RDFFormat.RDFXML_PLAIN);
        }finally{
            org.apache.commons.io.IOUtils.closeQuietly(input);
        }
        String uri = null;
        Resource resource = null;

        final Set<Resource> resourcesToRemove = new HashSet<Resource>();
        final ResIterator resIterator = srcModel.listSubjects();
        try {
            while (resIterator.hasNext()) {
                resource = resIterator.nextResource();
                if (resource.isURIResource()) {
                    uri = resource.getURI();
                    if (StringUtils.startsWithAny(uri, allowedCellarPrefixUris) && !allowedCellarResourceUris.contains(uri)
                            && !resourcesToRemove.contains(resource)) {
                        resourcesToRemove.add(resource);
                    }
                }
            }
        } finally {
            resIterator.close();
        }

        Statement statement = null;
        Resource subject = null;
        RDFNode object = null;

        final StmtIterator bnIterator = srcModel.listStatements((Resource) null, CellarProperty.owl_annotatedSourceP, (RDFNode) null);
        try {
            while (bnIterator.hasNext()) {
                statement = bnIterator.nextStatement();
                subject = statement.getSubject();
                object = statement.getObject();

                if (subject.isAnon() && object.isResource()) {
                    uri = object.asResource().getURI();
                    if (StringUtils.startsWithAny(uri, allowedCellarPrefixUris) && !allowedCellarResourceUris.contains(uri)
                            && !resourcesToRemove.contains(subject)) {
                        resourcesToRemove.add(subject);
                    }
                }
            }
        } finally {
            bnIterator.close();
        }

        final long size = srcModel.size();

        for (final Resource res : resourcesToRemove) {
            srcModel.removeAll(res, (Property) null, (RDFNode) null);
        }

        LOG.info("{} triples not related to the digital object(s) referenced by the DMDID ({}) have been filtered out.",
                size - srcModel.size(), fileRef);

        metadataModel.add(srcModel);
    }

    /**
     * Checks if the resource definition is equal to the expected definition {@java.lang.String expectedDefinition}.
     * If the expected definition is null, the method is accepting any resource of type URI.
     *
     * @param resource           the resource
     * @param expectedDefinition the expected definition
     * @return true, if successful
     */
    public static boolean match(final Resource resource, final String expectedDefinition) {

        if ((expectedDefinition != null) && resource.isURIResource()) {
            return resource.getURI().equals(expectedDefinition);
        }

        return true;
    }

    /**
     * Checks if the node definition is equal to the expected definition {@java.lang.String expectedDefinition}.
     *
     * @param node               the node
     * @param expectedDefinition the expected definition
     * @return true, if successful
     */
    public static boolean match(final RDFNode node, final String expectedDefinition) {

        if (expectedDefinition != null) {
            if (node.isURIResource()) {
                return node.asResource().getURI().equals(expectedDefinition);
            } else if (node.isLiteral()) {
                return node.asLiteral().getString().equals(expectedDefinition);
            }
        }

        return true;
    }

    /**
     * Explicitly begins a transaction on a model. To be used when the transaction is not managed.
     *
     * @param model the model for which starting the transaction
     */
    public static void beginTransaction(final Model model) {
        if ((model != null) && model.supportsTransactions()) {
            model.getGraph().getTransactionHandler().begin();
        }
    }

    /**
     * Explicitly commits a model. To be used when the transaction is not managed.
     *
     * @param model the model to commit
     */
    public static void commit(final Model model) {
        if ((model != null) && model.supportsTransactions()) {
            model.getGraph().getTransactionHandler().commit();
            ModelUtils.closeQuietly(model);
        }
    }

    /**
     * Explicitly rolls back a model. To be used when the transaction is not managed.
     *
     * @param model the model to roll back
     */
    public static void rollback(final Model model) {
        if ((model != null) && model.supportsTransactions()) {
            model.getGraph().getTransactionHandler().abort();
            ModelUtils.closeQuietly(model);
        }
    }

    /**
     * Merge model {@code sourceModel} into {@code destModel}.<br/><br/>
     * If {@code optimize} is true, the merge is optmized in that:
     * <br/>- new triples from {@code sourceModel} are added to {@code destModel}
     * <br/>- triples not present in {@code sourceModel} are removed from {@code destModel}
     * <br/>- triples present in both {@code sourceModel} and {@code destModel} are left untouched.
     * <br/><br/>
     * Important: <b>neither {@code sourceModel} or {@code destModel} are closed!</b>
     *
     * @param sourceModel the source model containing the triples to merge into the destination model
     * @param destModel   the destination model meant to contain the triples from the source model
     * @param optimize    if true, the merge is optimized as described above
     */
    public static void mergeModels(final Model sourceModel, final Model destModel, final boolean optimize) {
        if (!optimize) {
            destModel.removeAll();
            destModel.add(sourceModel);
            return;
        }

        Model modelToRemove = null;
        Model modelToAdd = null;
        try {
            if (!destModel.isEmpty()) {
                modelToRemove = destModel.difference(sourceModel);
                modelToAdd = sourceModel.difference(destModel);
            } else {
                modelToRemove = ModelFactory.createDefaultModel();
                modelToAdd = ModelUtils.create(sourceModel);
            }
            destModel.remove(modelToRemove.listStatements());
            destModel.add(modelToAdd);
        } finally {
            ModelUtils.closeQuietly(modelToRemove, modelToAdd);
        }
    }

    /**
     * <class_description> A meaningful description of the class that will be
     * displayed as the class summary on the JavaDoc package page.
     * <br/><br/>
     * <notes> Includes guaranteed invariants, usage instructions and/or examples,
     * reminders about desired improvements, etc.
     * <br/><br/>
     * ON : 12-Jan-2017
     * The Class NodeConversionMap.
     *
     * @author ARHS Developments
     * @version $$Revision$$$
     */
    private static class NodeConversionMap extends ConvertHistoryMap<Node, Node> {

        /**
         * The identifier.
         */
        private final String identifier;

        /**
         * The number.
         */
        private int number = 0;

        /**
         * Instantiates a new node conversion map.
         *
         * @param context the context
         */
        private NodeConversionMap(final String context) {
            this.identifier = context.startsWith(IdentifierService.PREFIX_CELLAR)
                    ? context.substring(IdentifierService.PREFIX_CELLAR.length()) : context;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public Node convert(final Node input) {
            if (input.isBlank()) {
                return NodeFactory.createBlankNode(BlankNodeId.create(this.number++ + ".id." + this.identifier));
            }
            return input;
        }
    }

    /**
     * <class_description> A meaningful description of the class that will be
     * displayed as the class summary on the JavaDoc package page.
     * <br/><br/>
     * <notes> Includes guaranteed invariants, usage instructions and/or examples,
     * reminders about desired improvements, etc.
     * <br/><br/>
     * ON : 12-Jan-2017
     * The Interface IContextExtractor.
     *
     * @param <T> the generic type
     * @author ARHS Developments
     * @version $$Revision$$$
     */
    private static interface IContextExtractor<T> {

        /**
         * Extract.
         *
         * @param t the t
         * @return the string
         */
        String extract(final T t);
    }

    /**
     * <class_description> A meaningful description of the class that will be
     * displayed as the class summary on the JavaDoc package page.
     * <br/><br/>
     * <notes> Includes guaranteed invariants, usage instructions and/or examples,
     * reminders about desired improvements, etc.
     * <br/><br/>
     * ON : 12-Jan-2017
     * The Class StringContextExtractor.
     *
     * @author ARHS Developments
     * @version $$Revision$$$
     */
    private static class StringContextExtractor implements IContextExtractor<String> {

        /**
         * {@inheritDoc}
         */
        @Override
        public String extract(final String t) {
            return t;
        }

    }

    /**
     * <class_description> A meaningful description of the class that will be
     * displayed as the class summary on the JavaDoc package page.
     * <br/><br/>
     * <notes> Includes guaranteed invariants, usage instructions and/or examples,
     * reminders about desired improvements, etc.
     * <br/><br/>
     * ON : 12-Jan-2017
     * The Class DigitalObjectContextExtractor.
     *
     * @author ARHS Developments
     * @version $$Revision$$$
     */
    private static class DigitalObjectContextExtractor implements IContextExtractor<DigitalObject> {

        /**
         * {@inheritDoc}
         */
        @Override
        public String extract(final DigitalObject t) {
            return t.getCellarId().getIdentifier();
        }

    }

    /**
     * Gets the quads.
     *
     * @param <T>       the generic type
     * @param metadata  the metadata
     * @param extractor the extractor
     * @return the quads
     */
    private static <T> List<Pair<Triple, String>> getQuads(final Map<T, Model> metadata, final IContextExtractor<T> extractor) {
        final List<Pair<Triple, String>> inverseQuads = new ArrayList<Pair<Triple, String>>(1000);
        for (final Map.Entry<T, Model> tModelEntry : metadata.entrySet()) {
            inverseQuads.addAll(getQuads(extractor.extract(tModelEntry.getKey()), tModelEntry.getValue()));
        }
        return inverseQuads;
    }

    /**
     * Gets the model by protection.
     *
     * @param ontologyService          the ontology service
     * @param model                    the model
     * @param retrieveProtectedPortion the retrieve protected portion
     * @return the model by protection
     */
    private static Model getModelByProtection(final OntologyService ontologyService, final Model model,
                                              final boolean retrieveProtectedPortion) {
        // get from the ontology the properties annotated as write once
        final SortedSet<String> writeOnceProperties = ontologyService.getWrapper().getOntologyData().getWriteOncePropertyUris();

        // return the model
        return getModelBy(writeOnceProperties, model, retrieveProtectedPortion);
    }

    /**
     * Gets the model by max cardinality.
     *
     * @param ontologyService                     the ontology service
     * @param model                               the model
     * @param retrieveSingleMaxCardinalityPortion the retrieve single max cardinality portion
     * @return the model by max cardinality
     */
    private static Model getModelByMaxCardinality(final OntologyService ontologyService, final Model model,
                                                  final boolean retrieveSingleMaxCardinalityPortion) {
        // get from the ontology the properties with single max cardinality
        final SortedSet<String> singleMaxCardinalityProperties = ontologyService.getWrapper().getOntologyData().getSingleMaxCardinalityPropertyUris();

        // return the model
        return getModelBy(singleMaxCardinalityProperties, model, retrieveSingleMaxCardinalityPortion);
    }

    /**
     * Gets the model by.
     *
     * @param properties the properties
     * @param model      the model
     * @param contained  the contained
     * @return the model by
     */
    private static Model getModelBy(final SortedSet<String> properties, final Model model, final boolean contained) {
        // filter the model
        final Model retModel = ModelFactory.createDefaultModel();
        retModel.add(model.listStatements(new SimpleSelector() {

            @Override
            public boolean selects(final Statement s) {
                final boolean isContained = properties.contains(s.getPredicate().getURI());
                return contained ? isContained : !isContained;
            }
        }));

        // return the model
        return retModel;
    }

    /**
     * Gets the subjects and same ases.
     *
     * @param subjectUri the subject uri
     * @param sameAses   the same ases
     * @return the subjects and same ases
     */
    public static Set<Resource> getSubjectsAndSameAses(final String subjectUri, final SetBasedMap<String> sameAses) {
        final Set<Resource> subjectsAndSameAses = new LinkedHashSet<Resource>();

        final Resource mainSubject = ResourceFactory.createResource(subjectUri);
        subjectsAndSameAses.add(mainSubject);
        if (sameAses != null) {
            final Set<String> subjectUris = sameAses.get(subjectUri);
            if (subjectUris != null) {
                for (final String currSubjectUri : subjectUris) {
                    subjectsAndSameAses.add(ResourceFactory.createResource(currSubjectUri));
                }
            }
        }

        return subjectsAndSameAses;
    }

    /**
     * Gets the staments from resource as model.
     *
     * @param in          the in
     * @param resourceUri the resource uri
     * @param property    the property
     * @return the staments from resource as model
     */
    public static Model getStamentsFromResourceAsModel(final Model in, final String resourceUri, final Property property) {
        final Model result = ModelFactory.createDefaultModel();
        final Resource resource = in.getResource(resourceUri);
        final StmtIterator listProperties = resource.listProperties(property);
        result.add(listProperties);
        return result;
    }

    /**
     * Close a dataset quietly.
     *
     * @param dataset the dataset to close
     */
    public static void closeQuietly(final Dataset dataset) {
        if (dataset == null) {
            return;
        }

        try {
            dataset.close();
        } catch (final Exception ignore) {
            LOG.warn("Closing dataset failed.", ignore);
        }
    }

    /**
     * Update date model.
     *
     * @param cellarUri  the cellar uri
     * @param date       the date
     * @param model      the model
     * @param properties the properties
     */
    public static void updateDateModel(final String cellarUri, final Date date, final Model model, final Collection<String> properties) {
        final Resource cellar = model.getResource(cellarUri);
        for (final String property : properties) {
            final Property createProperty = model.createProperty(property);
            cellar.removeAll(createProperty);
            if (date != null) {
                final String formatFullXmlDateTime = DateFormats.formatFullXmlDateTime(date);
                final Literal createTypedLiteral = model.createTypedLiteral(formatFullXmlDateTime, XSDDatatype.XSDdateTime);
                model.add(cellar, createProperty, createTypedLiteral);
            }
        }
        final List<Resource> resources = JenaQueries.getResources(cellar, OWL.sameAs, true);
        for (final Resource ps : resources) {
            for (final String property : properties) {
                final Property createProperty = model.createProperty(property);
                ps.removeAll(createProperty);
            }
        }
    }

    /**
     * Contains property.
     *
     * @param cellarUri the cellar uri
     * @param model     the model
     * @param property  the property
     * @return true, if successful
     */
    public static boolean containsProperty(final String cellarUri, final Model model, final String property) {
        final Resource cellar = model.getResource(cellarUri);
        final List<Resource> resources = JenaQueries.getResources(cellar, OWL.sameAs, true);
        final Property createProperty = model.createProperty(property);
        final boolean result = resources.stream().anyMatch(element -> {
            return model.listObjectsOfProperty(element, createProperty).hasNext();
        });
        return result;

    }

    /**
     * An item has a special relationship with its parent manifestation: contrary to the rest of the WEMI hierarchy,
     * where every relationship is provided in the input DMD model, such relationship is built by the CELLAR on the fly,
     * based on triples:
     * <ul>
     * <li><code>item cdm:item_belongs_to_manifestation manifestation</code></li>
     * <li><code>item cdm:item_identifier "identifier"</code></li>
     * <li><code>item cdm:cmr:manifestationMimeType mimetype</code></li>
     * <li><code>item rdf:type item_type</code></li>
     * </ul>
     * This method renews all item-related triples by removing them from the input <code>model</code>,
     * and then by re-adding them back for each manifestation.
     *
     * @param model             the input model to renew
     * @param rootDigitalObject the digital object whose hierarchy is to explore to find the items to rebuild
     */
    public static void renewItemTriples(final Model model, final DigitalObject rootDigitalObject) {
        // remove existing item-related triples
        model.removeAll(null, cdm_item_belongs_to_manifestationP, null);
        model.removeAll(null, cdm_item_identifierP, null);
        model.removeAll(null, cmr_manifestationMimeTypeP, null);
        model.removeAll(null, RDF.type, cdm_itemR);

        rootDigitalObject.getAllChilds(true).stream()
                .filter(digitalObject -> digitalObject.getType().equals(DigitalObjectType.MANIFESTATION))
                .forEach(manifestationDo -> {
                            // for each item, add back the item-related triples
                            final Resource manifestation = ResourceFactory.createResource(manifestationDo.getCellarId().getUri());
                            for (final ContentStream doContentStream : manifestationDo.getContentStreams()) {
                                final Resource contentStream = ResourceFactory.createResource(doContentStream.getCellarId().getUri());
                                model.add(contentStream, cdm_item_belongs_to_manifestationP, manifestation);
                                model.add(contentStream, cdm_item_identifierP, doContentStream.getCellarId().getIdentifier()
                                        .substring(doContentStream.getCellarId().getIdentifier().lastIndexOf("/") + 1));
                                model.add(contentStream, cmr_manifestationMimeTypeP, doContentStream.getMimeType());
                                model.add(contentStream, RDF.type, cdm_itemR);
                            }
                        }
                );
    }

    /**
     * It populates a {@link SetBasedMap} with all sameAses coming from all objects whose parent is {@code rootDigitalObject}.
     *
     * @param rootDigitalObject the root digital object
     * @return the sets the based map
     */
    public static SetBasedMap<String> getSameAses(final DigitalObject rootDigitalObject) {
        final SetBasedMap<String> sameAses = new SetBasedMap<>();

        rootDigitalObject.applyStrategyOnSelfAndChildren(digitalObject -> sameAses.add(digitalObject.getAllUris()));

        return sameAses;
    }

    /**
     * This method remove all item related triples from the model given
     *
     * @param model             the input model from whice the item-related triples will be removed
     */
    public static void removeItemTriples(Model model){
        //We consider that with CDM 4.7.7.1 and onwards that RDF.type is compulsory for models
        // or else it will fail in SHACL validation
        model.listResourcesWithProperty(RDF.type,cdm_itemR)
                .forEachRemaining(s->
                        {
                            model.removeAll(s, null, null);
                            model.removeAll(null,null,s);
                        }
                );
    }


}