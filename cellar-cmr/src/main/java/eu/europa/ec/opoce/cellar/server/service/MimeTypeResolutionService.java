/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.service
 *             FILE : MimeTypeResolutionService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 31 Jan 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.service;

import java.util.List;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;

/**
 * <class_description> This service is used to resolve the manifestation mime type  for dissemination and ingestion using the FileTypesNalSkosLoaderService.
 * <br/><br/>
 * <notes> .
 * <br/><br/>
 * ON : 31 Jan 2017
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface MimeTypeResolutionService {

    /**
     * Retrieves all manifestation-mime-type statements for all ITEMS referenced in the manifestation metadata (direct or inferred).
     * The property cdm:manifestationMimeType is set at ITEM level
     * The metadata from the ITEM are set within the metadata of the parent manifestation
     * First this service lists the items of the manifestation referenced in the direct model
     * Then it iterates this list  
     * For each ITEM this service will 
     * Recreate a statement with the ITEM as subject and the cmr:manifestationMimeType as the predicate  
     * If the  cmr:manifestationMimeType does not exist at ITEM level (direct of indirect metadata) the service will 
     * Retrieve the cdm:manifestationType at MANIFESTATION level and deduce the mime-type using the FileTypesNalSkosLoaderService
     * The FileTypesNalSkosLoaderService may have multiple mime-types for a specific manifestation type - it should return the first one
     * 
     * If it cannot find the mime type an exception is thrown
     * @param directModel the direct model of the manifestation
     * @param inferredModel the inferred model of the manifestation
     * @return the list statement holding the cdm:manifestationMimeType for each ITEM as their subject 
     */
    List<Statement> resolve(Model directModel, Model inferredModel);

    /**
     * Attempts to retrieve the cmr:manifestationMimeType from the ITEM resource
     * In case the property does not exist it should 
     * Retrieve the cdm:manifestationType at MANIFESTATION level and deduce the mime-type using the FileTypesNalSkosLoaderService
     * The cdm:manifestationType property is retrieved from any same-as resource of the MANIFESTATION
     * 
     * If it cannot find the mime type an exception is thrown.
     *
     * @param contentStreamResource the content stream resource
     * @param manifestationResource the manifestation resource
     * @param manifestationSameAsResources the manifestation same as resources
     * @return the mime type
     */
    String resolve(final Resource contentStreamResource, final Resource manifestationResource,
            final List<Resource> manifestationSameAsResources);

}
