/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice.core
 *             FILE : NodeHandler.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 20 Apr 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.notice.core;

import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.CellarIdentifierResourceService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.utils.XmlNoticeUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaQueries;
import eu.europa.ec.opoce.cellar.semantic.ontology.data.FallbackOntologyData;
import eu.europa.ec.opoce.cellar.semantic.ontology.data.OntologyData;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarType;
import eu.europa.ec.opoce.cellar.semantic.xml.XmlBuilder;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.annotation.Nullable;
import java.util.*;

/**
 * @author ARHS Developments
 */
@Configurable
public class NodeHandler implements RDFVisitor {

    private static final String EUROVOC_URI = "http://eurovoc.europa.eu/";
    private static final String THESAURUS_CONCEPT_SCHEME = EUROVOC_URI + "100141";
    private static final Resource THESAURUS_CONCEPT_SCHEME_RESOURCE = ResourceFactory.createResource(THESAURUS_CONCEPT_SCHEME);
    private static final Set<String> dateDataTypes = Collections.synchronizedSet(new HashSet<>());
    static {
        dateDataTypes.add(XSDDatatype.XSDdate.getURI());
        dateDataTypes.add(XSDDatatype.XSDdateTime.getURI());
    }

    private Property currentProperty;
    private String currentPropertyUri;
    private Property effectiveProperty;
    private XmlBuilder xmlBuilder;
    private Set<Property> skipProperties;

    @Autowired private CellarIdentifierResourceService cellarIdentifierResourceService;
    @Autowired private Rdf2XmlService rdf2XmlService;
    @Autowired private NodeHandlerService nodeHandlerService;
    @Autowired @Qualifier("pidManagerService") private IdentifierService identifierService;
    @Autowired private CellarResourceDao cellarResourceDao;

    private final Rdf2Xml rdf2Xml;
    private final ShouldHandlePropertyVisitor shouldHandlePropertyVisitor;
    private final ShouldExpandPropertyVisitor shouldExpandPropertyVisitor;
    private final OntologyData ontologyData;
    private final FallbackOntologyData fallbackOntologyData;

    public NodeHandler(Rdf2Xml rdf2Xml, ShouldHandlePropertyVisitor shouldHandlePropertyVisitor, 
                       ShouldExpandPropertyVisitor shouldExpandPropertyVisitor, OntologyData ontologyData,
                       FallbackOntologyData fallbackOntologyData) {
        this.rdf2Xml = rdf2Xml;
        this.shouldHandlePropertyVisitor = shouldHandlePropertyVisitor;
        this.shouldExpandPropertyVisitor = shouldExpandPropertyVisitor;
        this.ontologyData = ontologyData;
        this.fallbackOntologyData = fallbackOntologyData;
    }

    public Property getCurrentProperty() {
        return currentProperty;
    }

    public String getCurrentPropertyUri() {
        return currentPropertyUri;
    }

    public void setCurrent(final XmlBuilder xmlBuilder, final Property currentProperty) {
        this.setCurrent(xmlBuilder, currentProperty, currentProperty);
    }

    public void setCurrent(final XmlBuilder xmlBuilder, final Property currentProperty, final Property effectiveProperty) {
        this.setCurrent(xmlBuilder, currentProperty, effectiveProperty, Collections.singleton(OWL.sameAs));
    }

    public void setCurrent(final XmlBuilder xmlBuilder, final Property currentProperty,
                           final Property effectiveProperty, final Set<Property> skipProperties) {
        this.xmlBuilder = xmlBuilder;
        this.currentProperty = currentProperty;
        this.currentPropertyUri = currentProperty.getURI();
        this.effectiveProperty = effectiveProperty;
        this.skipProperties = skipProperties;
    }
    
    @Override
    @Nullable
    public Object visitBlank(final Resource resource, final AnonId id) {
        XmlBuilder childXmlBuilder = null;
        
        if (this.effectiveProperty.equals(CellarProperty.cdm_memberListP)) {
            this.currentPropertyUri = JenaQueries.getResourceUri(resource, CellarProperty.cdm_listedPropertyP, true);
            this.currentProperty = ResourceFactory.createProperty(currentPropertyUri);
            final NodeHandler personalNodeHandler = new NodeHandler(rdf2Xml, shouldHandlePropertyVisitor,
                    shouldExpandPropertyVisitor, ontologyData, fallbackOntologyData);
            final String tagname = XmlNoticeUtils.tagize(currentProperty);
            childXmlBuilder = this.xmlBuilder.child(tagname + ".MEMBERLIST");
            childXmlBuilder.attribute("type", "memberlist");
            nodeHandlerService.writeMemberList(personalNodeHandler, childXmlBuilder, resource, currentProperty);
        } else if (this.effectiveProperty.equals(RDF.first) || this.effectiveProperty.equals(CellarProperty.cdm_nestedListP)) {
            final NodeHandler personalNodeHandler = new NodeHandler(rdf2Xml, shouldHandlePropertyVisitor,
                    shouldExpandPropertyVisitor, ontologyData, fallbackOntologyData);
            childXmlBuilder = this.xmlBuilder.child("NESTEDLIST");
            nodeHandlerService.writeMemberList(personalNodeHandler, childXmlBuilder, resource, currentProperty);
        }
        
        return childXmlBuilder;
    }

    @Override
    @Nullable
    public Object visitURI(final Resource resource, final String uri) {
        final String tagName = XmlNoticeUtils.tagize(currentProperty);
        XmlBuilder childXmlBuilder = null;

        // Get the type of digital object when the resource is a digital object
        // This modification is due to the deletion of redundant rdf:type for parent and children in direct and inferred model
        final DigitalObjectType cellarType = getUriToDigitalObjectType(resource.getURI());

        if (this.skipProperties.contains(currentProperty)) {
            // done at an other place
        } else if (resource.equals(RDF.nil) && this.effectiveProperty.equals(RDF.rest)) {
            // nothing to do: end of list
        } else {
            if (isTopLevelConcept(resource)) {
                childXmlBuilder = this.xmlBuilder.child(tagName);
                final Map<LanguageBean, List<LanguageBean>> cache = rdf2Xml.getCache();
                final Set<LanguageBean> languages = rdf2Xml.getLanguages();
                final NoticeType noticeType = rdf2Xml.getNoticeType();
                final Model model = rdf2Xml.getModel();
                final Set<Resource> allSubjectResources = rdf2Xml.getAllSubjectResources();
                nodeHandlerService.writeConceptAllLevelsWithAnnotations(childXmlBuilder, currentProperty, resource, cache, languages,
                        noticeType, model, allSubjectResources);
            } else if (isFacet(resource)) {
                childXmlBuilder = this.xmlBuilder.child(tagName);
                final Map<LanguageBean, List<LanguageBean>> cache = rdf2Xml.getCache();
                final Set<LanguageBean> languages = rdf2Xml.getLanguages();
                final NoticeType noticeType = rdf2Xml.getNoticeType();
                final Model model = rdf2Xml.getModel();
                final Set<Resource> allSubjectResources = rdf2Xml.getAllSubjectResources();
                rdf2XmlService.writeConceptFacetWithPossibleAnnotation(childXmlBuilder, currentProperty, resource, cache, languages,
                        noticeType, model, allSubjectResources, fallbackOntologyData);
            } else if (isConcept(resource)) {
                childXmlBuilder = this.xmlBuilder.child(tagName);
                final Map<LanguageBean, List<LanguageBean>> cache = rdf2Xml.getCache();
                final Set<LanguageBean> languages = rdf2Xml.getLanguages();
                final NoticeType noticeType = rdf2Xml.getNoticeType();
                final Model model = rdf2Xml.getModel();
                final Set<Resource> allSubjectResources = rdf2Xml.getAllSubjectResources();
                nodeHandlerService.writeConcept(childXmlBuilder, resource, cache, languages, noticeType);
                nodeHandlerService.addAnnotations(childXmlBuilder, currentProperty, resource, model, allSubjectResources);
            } else if (isWEM(resource, cellarType)) {
                childXmlBuilder = this.xmlBuilder.child(tagName);
                shouldExpandPropertyVisitor.setCurrentProperty(currentPropertyUri);
                createXmlWithExpandingDataIfNeeded(childXmlBuilder, resource);
                final Model model = rdf2Xml.getModel();
                final Set<Resource> allSubjectResources = rdf2Xml.getAllSubjectResources();
                nodeHandlerService.addAnnotations(childXmlBuilder, currentProperty, resource, model, allSubjectResources);
            } else if (isAgenUnitAdministrative(resource, cellarType)) {
                childXmlBuilder = this.xmlBuilder.child(tagName);
                shouldExpandPropertyVisitor.setCurrentProperty(currentPropertyUri);
                createXmlWithExpandingDataIfNeeded(childXmlBuilder, resource);
                final NoticeType noticeType = rdf2Xml.getNoticeType();
                final Set<LanguageBean> languages = rdf2Xml.getLanguages();
                rdf2XmlService.writeAgent_UnitAdministrative(childXmlBuilder, resource, noticeType, languages);
                final Model model = rdf2Xml.getModel();
                final Set<Resource> allSubjectResources = rdf2Xml.getAllSubjectResources();
                nodeHandlerService.addAnnotations(childXmlBuilder, currentProperty, resource, model, allSubjectResources);
            } else if (isItem(resource, cellarType)) {
                childXmlBuilder = this.xmlBuilder.child(tagName);
                final Model model = rdf2Xml.getModel();
                rdf2XmlService.writeItem(childXmlBuilder, resource, this, model);
                final Set<Resource> allSubjectResources = rdf2Xml.getAllSubjectResources();
                nodeHandlerService.addAnnotations(childXmlBuilder, currentProperty, resource, model, allSubjectResources);
            } else if (currentProperty.equals(RDF.type)) {
                childXmlBuilder = this.xmlBuilder.child(tagName);
                childXmlBuilder.text(uri);
            }
        }

        return childXmlBuilder;
    }

    private boolean isTopLevelConcept(final Resource resource) {
        return resource.hasProperty(RDF.type, CellarType.skos_ConceptR)
                && resource.hasProperty(CellarProperty.skos_inSchemeP)
                && ontologyData.getAllNumberedLevelsPropertyUris().contains(currentPropertyUri);
    }

    private boolean isFacet(final Resource resource) {
        return resource.hasProperty(RDF.type, CellarType.skos_ConceptR)
                && resource.hasProperty(CellarProperty.skos_inSchemeP, THESAURUS_CONCEPT_SCHEME_RESOURCE)
                && ontologyData.getFacetPropertyUris().contains(currentPropertyUri)
                && !ontologyData.getAllNumberedLevelsPropertyUris().contains(currentPropertyUri);
    }

    private boolean isConcept(final Resource resource) {
        return resource.hasProperty(RDF.type, CellarType.skos_ConceptR)
                && resource.hasProperty(CellarProperty.skos_inSchemeP)
                && !ontologyData.getFacetPropertyUris().contains(currentPropertyUri)
                && !ontologyData.getAllNumberedLevelsPropertyUris().contains(currentPropertyUri);
    }

    private boolean isWEM(final Resource resource, final DigitalObjectType cellarType) {
        return resource.hasProperty(RDF.type, CellarType.cdm_entityTemporalR)
                || resource.hasProperty(RDF.type, CellarType.cdm_topleveleventR) // dossier & event
                || resource.hasProperty(RDF.type, CellarType.cdm_expressionR)
                || resource.hasProperty(RDF.type, CellarType.cdm_manifestationR)
                || resource.hasProperty(RDF.type, CellarType.cdm_workR)
                || DigitalObjectType.DOSSIER.equals(cellarType)
                || DigitalObjectType.EVENT.equals(cellarType)
                || DigitalObjectType.TOPLEVELEVENT.equals(cellarType)
                || DigitalObjectType.EXPRESSION.equals(cellarType)
                || DigitalObjectType.MANIFESTATION.equals(cellarType)
                || DigitalObjectType.WORK.equals(cellarType);
    }

    private boolean isItem(final Resource resource, final DigitalObjectType cellarType) {
        return resource.hasProperty(RDF.type, CellarType.cdm_itemR) || DigitalObjectType.ITEM.equals(cellarType);
    }

    private boolean isAgenUnitAdministrative(final Resource resource, final DigitalObjectType cellarType) {
        return !resource.hasProperty(RDF.type, CellarType.skos_ConceptR)
                && (resource.hasProperty(RDF.type, CellarType.cdm_agentR)
                    || DigitalObjectType.AGENT.equals(cellarType)
                    || resource.hasProperty(RDF.type, CellarType.cdm_unitAdministrativeR));
    }

    private XmlBuilder createXmlWithExpandingDataIfNeeded(final XmlBuilder xmlBuilder, final Resource resource) {
        final NoticeType noticeType = rdf2Xml.getNoticeType();
        if (noticeType.accept(shouldExpandPropertyVisitor)) {
            final String cellarPrefixedOrNull = identifierService.getCellarPrefixedOrNull(resource.getURI());
            if (cellarPrefixedOrNull != null) {
                final CellarResource cellarResource = cellarPrefixedOrNull.contains(".")
                        ? cellarResourceDao.findCellarId(cellarPrefixedOrNull.substring(0, cellarPrefixedOrNull.indexOf('.')))
                        : cellarResourceDao.findCellarId(cellarPrefixedOrNull);
                final Date embargoDate = cellarResource != null ? cellarResource.getEmbargoDate() : null;
                if ((embargoDate == null) || embargoDate.before(new Date())) {
                    xmlBuilder.attribute("embed", cellarPrefixedOrNull);

                    final Set<LanguageBean> contentLanguages = rdf2Xml.getContentLanguages();
                    if ((contentLanguages == null) || contentLanguages.isEmpty()) {
                        for (final LanguageBean languageBean : rdf2Xml.getLanguages()) {
                            final String isoCodeThreeChar = languageBean.getIsoCodeThreeChar();
                            xmlBuilder.attribute("content-lang", isoCodeThreeChar);
                        }
                    } else {
                        for (final LanguageBean languageBean : contentLanguages) {
                            final String isoCodeThreeChar = languageBean.getIsoCodeThreeChar();
                            xmlBuilder.attribute("content-lang", isoCodeThreeChar);
                        }
                    }
                }
            }
        }

        if (noticeType == NoticeType.embedded) {
            xmlBuilder.attribute("type", "embedded_link");
        } else {
            xmlBuilder.attribute("type", "link");
        }
        final Model model = rdf2Xml.getModel();
        final Resource cellarResource = XmlNoticeUtils.getResourceFromModel(resource, model);
        if (cellarResource != null) {
            final Set<Resource> sameAsResources = XmlNoticeUtils.getSameAsResources(cellarResource);
            rdf2XmlService.writeUriAndSameAs(xmlBuilder, cellarResource, sameAsResources, model);
        } else {
            rdf2XmlService.writeUriAndSameAs(xmlBuilder, resource, Collections.singleton(resource), model);
        }
        return xmlBuilder;
    }
    
    @Override
    @Nullable
    public Object visitLiteral(final Literal literal) {
        final String tagName = XmlNoticeUtils.tagize(this.currentProperty);
        final Model model = rdf2Xml.getModel();
        final Set<Resource> allSubjectResources = rdf2Xml.getAllSubjectResources();
        if (dateDataTypes.contains(ontologyData.getDatatype(this.currentPropertyUri))
                || dateDataTypes.contains(literal.getDatatypeURI())) {
            final XmlBuilder child = this.xmlBuilder.child(tagName);
            rdf2XmlService.writeDate(child, literal.getLexicalForm());
            nodeHandlerService.addAnnotations(child, this.currentProperty, literal, model, allSubjectResources);
        } else {
            final XmlBuilder child = this.xmlBuilder.child(tagName);
            child.attribute("type", "data");
            child.child("VALUE").text(literal.getLexicalForm());
            if (!StringUtils.isEmpty(literal.getLanguage())) {
                child.child("LANG").text(literal.getLanguage());
            }
            nodeHandlerService.addAnnotations(child, this.currentProperty, literal, model, allSubjectResources);
        }
        return null;
    }
    
    private DigitalObjectType getUriToDigitalObjectType(String uri) {
    	return (this.rdf2Xml.getUriToCellarIdentifierMappings() != null) ?
    			getUriToDigitalObjectTypeCached(uri) : this.cellarIdentifierResourceService.getDigitalObjectType(uri);
    }
    
    /**
     * Retrieves the {@code DigitalObjectType} of the digital object
     * corresponding to the provided URI.
     * @param uri the URI to check.
     * @return the corresponding {@code DigitalObjectType}.
     */
    private DigitalObjectType getUriToDigitalObjectTypeCached(String uri) {
        String cachedCellarId = this.rdf2Xml.getUriToCellarIdentifierMappings().get(uri);
        if (cachedCellarId != null) {
        	return this.cellarIdentifierResourceService.retrieveCellarResourceAndExtractType(cachedCellarId);
        }
        String cellarId = this.cellarIdentifierResourceService.resolveUriToCellarIdentifier(uri);
        if (cellarId != null) {
        	this.rdf2Xml.getUriToCellarIdentifierMappings().put(uri, cellarId);
        	return this.cellarIdentifierResourceService.retrieveCellarResourceAndExtractType(cellarId);
        }
        return null;
    }    
    
}
