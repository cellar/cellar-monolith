/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.nal.service
 *             FILE : OntoLoad.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 19 Apr 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ontology.domain;

import java.util.Objects;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 19 Apr 2017
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class OntoLoad {

    private String ontologyUri;

    /**
     * Instantiates a new onto load.
     *
     * @param ontologyUri the ontology uri
     */
    public OntoLoad(final String ontologyUri) {
        this.ontologyUri = ontologyUri;
    }

    public String getOntologyUri() {
        return ontologyUri;
    }

    public void setOntologyUri(String ontologyUri) {
        this.ontologyUri = ontologyUri;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OntoLoad)) return false;
        OntoLoad ontoLoad = (OntoLoad) o;
        return Objects.equals(getOntologyUri(), ontoLoad.getOntologyUri());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOntologyUri());
    }

    @Override
    public String toString() {
        return "OntoLoad{" +
                "ontologyUri='" + ontologyUri + '\'' +
                '}';
    }
}
