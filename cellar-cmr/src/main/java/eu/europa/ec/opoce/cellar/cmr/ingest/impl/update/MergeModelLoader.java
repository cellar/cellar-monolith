/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.cmr.ingest.impl.update
 *        FILE : MergeModelLoader.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 09-04-2014
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.ingest.impl.update;

import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.structmap_status_history.StructMapStatusHistory;
import eu.europa.ec.opoce.cellar.cl.domain.validation.MetadataValidationRequest;
import eu.europa.ec.opoce.cellar.cl.domain.validation.MetadataValidationResult;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData;
import eu.europa.ec.opoce.cellar.cmr.ingest.IMetadataLoaderService;
import eu.europa.ec.opoce.cellar.cmr.model.ModelUtils;
import eu.europa.ec.opoce.cellar.cmr.model.deleteHandler.IModelDeleteHandler;
import eu.europa.ec.opoce.cellar.common.map.SetBasedMap;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.CellarValidationException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.MetadataValidationResultAwareException;
import eu.europa.ec.opoce.cellar.exception.MetadataValidationResultAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.ontology.service.OntologyService;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.semantic.validation.ValidationResultCache;
import eu.europa.ec.opoce.cellar.semantic.validation.NalValidationUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.List;
import java.util.Map;

import static eu.europa.ec.opoce.cellar.CommonErrors.DIRECT_MODEL_VALIDATION_FAILED;
import static eu.europa.ec.opoce.cellar.cl.domain.validation.MetadataValidationResult.ModelType.DIRECT;

/**
 * <class_description> Class for conveniently load models for merge ingests.<br/>
 * The merge aims to preserve the triples not specified in the METS package.<br/>
 * Specifically, the following operations are performed (see also figure below):<br/>
 * <ol>
 * <li>retrieve from the CMR layer the existing direct model gi (that is non-inferred) related to the div element i of the METS document</li>
 * <li>
 * calculate sub(gi), subset of gi, containing all the triples which:
 * <ul>
 * <li>have as subject the entity referenced in the div element i</li>
 * <li>have as subject a resource with a owl:sameAs relation to the given entity</li>
 * <li>have as subject or object a blank node that is directly or indirectly linked to the given or an equivalent entity</li>
 * </ul>
 * </li>
 * <li>
 * split sub(gi) in:
 * <ul>
 * <li>sub(gi)_pro, that contains the triples whose predicate is annotated as write-once</li>
 * <li>sub(gi)_unp, that contains all other triples</li>
 * </ul>
 * </li>
 * <li>apply to sub(gi)_unp the SPARQL delete query defined in the behavior element associated to the div element i </li>
 * <li>merge sub(gi)_unp, sub(gi)_pro and the new triples from the METS document into a single document gc</li>
 * <li>calculate the inferred model inf(gc) out of gc</li>
 * <li>validate inf(gc) and gc and proceed with the usual ingestion workflow.</li>
 * </ol>
 * <br/><br/>
 * <img src="./ingest_flow_diagram.gif" />
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 09-04-2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class MergeModelLoader extends UpdateModelLoader {

    @SuppressWarnings("unused")
    private static final Logger LOG = LogManager.getLogger(MergeModelLoader.class);

    @Autowired
    private IMetadataLoaderService metadataLoaderService;

    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    @Autowired
    private OntologyService ontologyService;

    @Autowired
    @Qualifier("digitalObjectModelDeleteHandler")
    private IModelDeleteHandler<DigitalObject> digitalObjectIModelDeleteHandler;

    @Override
    protected void validateDirectModel() {
        // validates direct model against the ontology
        final Model directModel = this.getBusinessModels().get(ContentType.DIRECT);
        List<String> invalidUriList = NalValidationUtils.validateUris(directModel);
        if (!invalidUriList.isEmpty()) {
            throw ExceptionBuilder.get(CellarValidationException.class)
                    .withCode(CmrErrors.VALIDATION_RDFDATA_ERROR)
                    .withMessage("Invalid URIs found in model : {}")
                    .withMessageArgs(invalidUriList)
                    .build();
        }

        Model directWithoutItem=null;
        if(cellarConfiguration.getCellarServiceIngestionValidationItemExclusionEnabled()) {
            //create copy of model
            directWithoutItem = ModelFactory.createDefaultModel().add(directModel);
            //remove all item related triples
            ModelUtils.removeItemTriples(directWithoutItem);
        }

        final MetadataValidationResult metadataValidationResult = validationService.validate(new MetadataValidationRequest(directWithoutItem==null?directModel:directWithoutItem)
                .cellarId(safeCellarId()));
        
        String requiredShaclReport = filterShaclReport(metadataValidationResult.getValidationResult());
        if ((requiredShaclReport != null) && (data.getStructMapStatusHistory() != null)) {
            StructMapStatusHistory structMapStatusHistory = this.structMapStatusHistoryService.updateStructMapShaclReport(data.getStructMapStatusHistory(), requiredShaclReport);
            data.setStructMapStatusHistory(structMapStatusHistory);
        }
        
        metadataValidationResult.setModelType(DIRECT);

        ValidationResultCache.put(getData().getStructMap(), metadataValidationResult);

        if (!metadataValidationResult.conformModelProvided()) {
            throw MetadataValidationResultAwareExceptionBuilder.getInstance(MetadataValidationResultAwareException.class)
                    .withStructMapId(getData().getStructMap().getId())
                    .withMetadataValidationResult(metadataValidationResult)
                    .withMetsDocumentId(getData().getStructMap().getMetsDocument().getDocumentId())
                    .withMessage("Validation of the direct model failed : {}")
                    .withCode(DIRECT_MODEL_VALIDATION_FAILED)
                    .withMessageArgs(metadataValidationResult.getErrorMessage())
                    .build();
        }

        // checks the write-once triples on the direct model
        if (cellarConfiguration.isCellarServiceIngestionCheckWriteOncePropertiesEnabled()) {
            final SetBasedMap<String> sameAses = ModelUtils.getSameAses(getData().getStructMap().getDigitalObject());
            ModelUtils.validateProtectedTriples(getData().getStructMap().getMetsDocument().getDocumentId(),
                    ontologyService, directModel, getData().getExistingDirectAndInferredModel().asModel(), sameAses);
        }

        // creationDate must be checked in any case
        validateCreationDate();
    }

    /**
     * Overridden in order not to load old data, as it is done by the merge process itself.
     */
    @Override
    protected void loadMetsModel() {
        this.metsModel = metadataLoaderService.loadMetadata(this.data.getMetsPackage(), this.data.getStructMap(), false);
    }

    @Override
    protected void loadDirectModel() {
        if (getMetsModel() == null) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("Cannot load the direct model because the new model from METS is null!").build();
        }
        final Model sameAsModel = getBusinessModels().get(ContentType.SAME_AS);
        if (sameAsModel == null) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("Cannot load the direct model because the related sameAs model is null!").build();
        }

        // load the existing model eligible for merge
        final Model existingModel = calculateExistingModel(getData());

        try {
            // create the direct model (that is, non-inferred) by merging the sameAs, the METS and the existing model
            final Model directModel = ModelUtils.create(sameAsModel, getMetsModel(), existingModel);

            // renew the item-related triples
            ModelUtils.renewItemTriples(directModel, getData().getStructMap().getDigitalObject());

            getBusinessModels().put(ContentType.DIRECT, directModel);
        } finally {
            ModelUtils.closeQuietly(existingModel);
        }
    }

    /**
     * Calculate the existing model by applying the rules of merging (see requirements at https://webgate.ec.europa.eu/publications/jira/secure/attachment/36020/proposal.doc).
     */
    private Model calculateExistingModel(final CalculatedData data) {
        final Model globalExistingModel = ModelFactory.createDefaultModel();

        // retrieve the existing direct model eligible for merge
        final Map<String, Model> existingModelsPerContext = data.getExistingDirectModel().asModelPerContext();
        if (existingModelsPerContext.isEmpty()) {
            return globalExistingModel;
        }

        // apply the rules of merging
        data.getStructMap().getDigitalObject().applyStrategyOnSelfAndChildren(digitalObject -> {
            // process the existing model related to the current digital object ('gi' in javadoc schema)
            final String context = digitalObject.getCellarId().getIdentifier();
            final Model existingModel = existingModelsPerContext.get(context);
            if (existingModel == null) {
                return;
            }

            Model initialEligibleForDelete = null;
            Model notEligibleForDelete = null;
            Model eligibleForDeleteBecauseUnprotected = null;
            Model notEligibleForDeleteBecauseProtected = null;
            Model calculatedExistingModel = null;
            try {
                // calculate the model containing triples related to the root div element in the struct map ('sub(gi)')
                initialEligibleForDelete = calculateInitialModelEligibleForDelete(existingModel, digitalObject);

                // calculate the complementary model of sub_gi ('gi - sub(gi)')
                notEligibleForDelete = existingModel.difference(initialEligibleForDelete);

                // calculate the protected portion of the model where the delete query is not meant to be executed ('sub(gi)_pro')
                // then, try to run the delete query over it, and in case some triples have been deleted, raise an exception
                notEligibleForDeleteBecauseProtected = ModelUtils.getProtectedModel(ontologyService, initialEligibleForDelete);

                // calculate the unprotected portion of sub(gi), that is, the model where the delete query is meant to be executed ('sub(gi)_unp')
                // then, run the delete on it
                eligibleForDeleteBecauseUnprotected = initialEligibleForDelete.difference(notEligibleForDeleteBecauseProtected);
                digitalObjectIModelDeleteHandler.delete(eligibleForDeleteBecauseUnprotected, digitalObject, false);

                // group all models together into the new model, and set it back to the global existing model
                calculatedExistingModel = JenaUtils.create(notEligibleForDelete, notEligibleForDeleteBecauseProtected,
                        eligibleForDeleteBecauseUnprotected);
                globalExistingModel.add(calculatedExistingModel);
            } catch (final CellarException e) {
                ModelUtils.closeQuietly(globalExistingModel);
                throw e;
            } finally {
                ModelUtils.closeQuietly(existingModel, initialEligibleForDelete, notEligibleForDelete,
                        notEligibleForDeleteBecauseProtected, eligibleForDeleteBecauseUnprotected, calculatedExistingModel);
            }
        });

        return globalExistingModel;
    }

    /**
     * Calculate the initial model eligible for delete by selecting all statements of {@code model} that:</br>
     * <ol>
     * <li>have as subject the digital object {@code digitalObject}</li>
     * <li>belong to a blank node that is directly or indirectly linked to the given digital object. For example:
     * <ol>
     * <li>related annotations (linked via <code>owl:annotatedSource</code>)</li>
     * <li>related <code>rdf:List</code> (referred by <code>cdm:memberList</code>)</li>
     * </ol>
     * </ol>
     */
    private static Model calculateInitialModelEligibleForDelete(final Model model, final DigitalObject digitalObject) {
        final EligibleForDeleteSelector selector = new EligibleForDeleteSelector(digitalObject, model);
        selector.select();
        return selector.getOutModel();
    }

}
