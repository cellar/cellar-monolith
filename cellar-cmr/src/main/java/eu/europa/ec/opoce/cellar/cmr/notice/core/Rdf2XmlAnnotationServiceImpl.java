package eu.europa.ec.opoce.cellar.cmr.notice.core;

import eu.europa.ec.opoce.cellar.cmr.utils.XmlNoticeUtils;
import eu.europa.ec.opoce.cellar.semantic.jena.StatementIterable;
import eu.europa.ec.opoce.cellar.semantic.xml.XmlBuilder;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.OWL2;
import org.apache.jena.vocabulary.RDF;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class Rdf2XmlAnnotationServiceImpl implements Rdf2XmlAnnotationService {

    @Override
    public void addAnnotations(final XmlBuilder parentBuilder, final Property property,
                               final RDFNode object, Model model, final Set<Resource> allSubjectResources) {
        model.listStatements(null, OWL2.annotatedProperty, property)
                .mapWith(Statement::getSubject) // blank node
                .filterKeep(annotationNode -> isAnnotatedTarget(annotationNode, object, model))
                .filterKeep(annotationNode -> {
                    final Statement annotatedSourceStatement = annotationNode.getProperty(OWL2.annotatedSource);
                    return annotatedSourceStatement != null && allSubjectResources.contains(annotatedSourceStatement.getResource());
                })
                .forEachRemaining(annotationNode -> writeSameAsAnnotations(parentBuilder, annotationNode));
    }

    private boolean isAnnotatedTarget(final Resource annotationNode, final RDFNode object, final Model objectModel) {
        if (!object.isResource()) {
            return annotationNode.hasProperty(OWL2.annotatedTarget, object);
        }

        return collectAllSameAs(new HashSet<>(), objectModel, object.asResource())
                .stream()
                .anyMatch(sameAs -> annotationNode.hasProperty(OWL2.annotatedTarget, sameAs));
    }

    private Set<Resource> collectAllSameAs(final Set<Resource> sameAsResources, final Model model, final Resource cellarResource) {
        if (sameAsResources == null || !sameAsResources.add(cellarResource)) {
            return sameAsResources;
        }

        for (final Statement sameAs : StatementIterable.list(model, cellarResource, OWL.sameAs, null)) {
            collectAllSameAs(sameAsResources, model, sameAs.getResource());
        }

        for (final Statement sameAs : StatementIterable.list(model, null, OWL.sameAs, cellarResource)) {
            collectAllSameAs(sameAsResources, model, sameAs.getSubject());
        }

        return sameAsResources;
    }

    @Override
    public void addSameAsAnnotations(final XmlBuilder parentBuilder, final RDFNode object, final Model model) {
        model.listStatements(null, OWL2.annotatedProperty, OWL2.sameAs)
                .mapWith(Statement::getSubject) // blank node
                .filterKeep(annotationNode -> annotationNode.hasProperty(OWL2.annotatedTarget, object))
                .forEachRemaining(annotationNode -> writeSameAsAnnotations(parentBuilder, annotationNode));
    }

    private void writeSameAsAnnotations(final XmlBuilder parentBuilder, final Resource annotationNode) {
        final XmlBuilder annotationBuilder = parentBuilder.child("ANNOTATION");
        annotationNode.listProperties()
                .filterDrop(this::isAnnotationProperty)
                .forEachRemaining(annotation -> writeSameAsAnnotation(annotationBuilder, annotation));
    }

    private void writeSameAsAnnotation(final XmlBuilder annotationBuilder, final Statement annotation) {
        final RDFNode object = annotation.getObject();
        final String text;

        if (object == null) {
            text = "";
        } else {
            text = object.visitWith(StringGetterRDFVisitor.getInstance()).toString();
        }

        annotationBuilder.child(XmlNoticeUtils.tagize(annotation.getPredicate())).text(text);
    }

    private boolean isAnnotationProperty(final Statement annotation) {
        final Property annotationProperty = annotation.getPredicate();
        return annotationProperty.equals(RDF.type)
                || annotationProperty.equals(OWL2.annotatedSource)
                || annotationProperty.equals(OWL2.annotatedProperty)
                || annotationProperty.equals(OWL2.annotatedTarget);
    }
}
