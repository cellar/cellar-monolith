/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.jena.oracle.impl
 *             FILE : RDFStoreRetryAwareConnection.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 15-07-2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.jena.oracle.impl;

import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.common.closure.PClosure;
import eu.europa.ec.opoce.cellar.common.retry.IRetryAwareConnection;
import eu.europa.ec.opoce.cellar.database.transaction.RDFTransactionManager;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import oracle.spatial.rdf.client.jena.Oracle;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * <class_description> An implementation of {@link eu.europa.ec.opoce.cellar.common.retry.IRetryAwareConnection} which handles the logic
 * for the retry-aware accesses to the Oracle RDF Store.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 15-07-2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class RDFStoreRetryAwareConnection implements IRetryAwareConnection<Connection> {

    // the possible underlying messages that could denote a deadlock situation
    private static String[] RETRIABLE_MESSAGES = new String[]{ //
            "ORA-00060: deadlock detected while waiting for resource", //    
            "ORA-55303: SDO_RDF_TRIPLE_S constructor failed" //
    };

    private Oracle rdfStore;

    public RDFStoreRetryAwareConnection(final Oracle rdfStore) {
        this.rdfStore = rdfStore;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.common.retry.IRetryAwareConnection#connect(PClosure)
     */
    @Override
    public <C extends PClosure<Connection>> void connect(final C connectionCode) throws CellarException {
        Connection connection;
        try {
            // prepare connection
            connection = this.rdfStore.getConnection();
            connection.setAutoCommit(false);

            // execute connection
            connectionCode.call(connection);
        } catch (Exception exception) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.ORACLE_LOAD_FAILED).withCause(exception)
                    .withMessage("Inserting the triples in the Oracle RDF Store has failed.").build();
        } finally {
            // share connection for later commit/rollback
            RDFTransactionManager.INSTANCE.put(this.rdfStore);
        }
    }

    /**
     * @see eu.europa.ec.opoce.cellar.common.retry.IRetryAwareConnection#rollback()
     */
    @Override
    public void rollback() throws CellarException {
        try {
            this.rdfStore.rollbackTransaction();
        } catch (SQLException exception) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.ORACLE_LOAD_FAILED).withCause(exception)
                    .withMessage("Rolling back the connection in the Oracle RDF Store has failed.").build();
        }
    }

    /**
     * @see eu.europa.ec.opoce.cellar.common.retry.IRetryAwareConnection#resolveRetriableMessages()
     */
    @Override
    public String[] resolveRetriableMessages() {
        return RETRIABLE_MESSAGES;
    }

}
