/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.quad.impl
 *             FILE : OracleExponentialBackoffRetryModelQuadService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 05-03-2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.quad.impl;

import eu.europa.ec.opoce.cellar.common.retry.IRetryAwareConnection;
import eu.europa.ec.opoce.cellar.common.retry.IRetryStrategy;
import eu.europa.ec.opoce.cellar.common.retry.impl.ExponentialBackoffRetryStrategy;
import eu.europa.ec.opoce.cellar.jena.oracle.impl.RDFStoreRetryAwareConnection;
import oracle.spatial.rdf.client.jena.Oracle;

import java.sql.Connection;

/**
 * <class_description> An implementation of {@link eu.europa.ec.opoce.cellar.cmr.quad.impl.BaseModelQuadService} which manages
 * the concurrent accesses to the CMR tables through a fire-and-retry mechanism based on the <b>Exponential Backoff</b> pattern.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 05-03-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class OracleExponentialBackoffRetryModelQuadService extends BaseModelQuadService {

    /**
     * @see eu.europa.ec.opoce.cellar.cmr.quad.impl.BaseModelQuadService#newRetryStrategy(oracle.spatial.rdf.client.jena.Oracle)
     */
    @Override
    protected IRetryStrategy<Connection> newRetryStrategy(final Oracle oracle) {
        final int maxAttempts = this.cellarConfiguration.getCellarServiceIngestionCmrLockMaxRetries();
        final long ceilingDelay = this.cellarConfiguration.getCellarServiceIngestionCmrLockCeilingDelay();
        final int base = this.cellarConfiguration.getCellarServiceIngestionCmrLockBase();

        final IRetryAwareConnection<Connection> retryAwareConnection = new RDFStoreRetryAwareConnection(oracle);
        final IRetryStrategy<Connection> retryStrategy = new ExponentialBackoffRetryStrategy<Connection>()
                .maxAttempts(maxAttempts).ceilingDelay(ceilingDelay).base(base).recipientName("Oracle")
                .retryAwareConnection(retryAwareConnection);
        return retryStrategy;
    }

}
