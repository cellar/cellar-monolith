/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.version
 *             FILE : TimeMapLinkRedirectResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 28, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$$
 *          VERSION : $LastChangedRevision$$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.version;

import eu.europa.ec.opoce.cellar.common.http.headers.IURLTokenable;
import eu.europa.ec.opoce.cellar.server.dissemination.AcceptLanguage;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.Type;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.IDisseminationResolver;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;

import java.util.Date;

import org.springframework.http.ResponseEntity;

/**
 * Resolver for Time-map dissemination requests for which the expected response is a list of links.
 *
 * @author ARHS Developments
 */
public class TimeMapLinkRedirectResolver extends TimeMapRedirectResolver {

    /**
     * Instantiates a new time map link redirect resolver.
     *
     * @param cellarResource the cellar resource
     * @param resourceURI the resource uri
     * @param accept the accept
     * @param decoding the decoding
     * @param acceptLanguage the accept language
     * @param acceptDateTime the accept date time
     * @param rel the rel
     * @param provideAlternates the provide alternates
     */
    protected TimeMapLinkRedirectResolver(final CellarResourceBean cellarResource, final String resourceURI, final String accept,
            final String decoding, final AcceptLanguage acceptLanguage, final Date acceptDateTime, final String rel,
            final boolean provideAlternates) {
        super(cellarResource, resourceURI, accept, decoding, acceptLanguage, acceptDateTime, rel, provideAlternates);
    }

    /**
     * Gets the.
     *
     * @param cellarResource the cellar resource
     * @param resourceURI the resource uri
     * @param accept the accept
     * @param decoding the decoding
     * @param acceptLanguage the accept language
     * @param acceptDateTime the accept date time
     * @param rel the rel
     * @param provideAlternates the provide alternates
     * @return the i dissemination resolver
     */
    public static IDisseminationResolver get(final CellarResourceBean cellarResource, final String resourceURI, final String accept,
            final String decoding, final AcceptLanguage acceptLanguage, final Date acceptDateTime, final String rel,
            final boolean provideAlternates) {
        return new TimeMapLinkRedirectResolver(cellarResource, resourceURI, accept, decoding, acceptLanguage, acceptDateTime, rel,
                provideAlternates);
    }

    /** {@inheritDoc} */
    @Override
    protected ResponseEntity<Void> doHandleDisseminationRequest() {
        return this.seeOtherService.negotiateSeeOtherForLink(this.cellarResource);
    }

    /** {@inheritDoc} */
    @Override
    protected void doHandleWorkDisseminationRequest() {
    }

    /** {@inheritDoc} */
    @Override
    protected IURLTokenable getTypeStructure() {
        return Type.LINK;
    }
}
