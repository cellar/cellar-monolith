/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.cmr.model
 *        FILE : SameAsModelUtils.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 15-12-2015
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.model;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.OWL;
import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import eu.europa.ec.opoce.cellar.domain.content.mets.CellarIdentifiedObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;

import java.util.Collection;

/**
 * <class_description> Class utils for handling sameAses.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 15-12-2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class SameAsModelUtils {

    public static final IdentifierService IDENTIFIER_SERVICE = ServiceLocator.getService("pidManagerService", IdentifierService.class);

    /**
     * Gets sameAs model based on a list of identifiers.
     * @param identifiers the list of identifiers
     * @return the sameAs model corresponding to the identifiers
     */
    public static Model getSameAsModel(final Collection<Identifier> identifiers) {
        final Model sameAs = ModelFactory.createDefaultModel();
        try {
            for (Identifier identifier : identifiers) {
                for (String ps : identifier.getPids()) {
                    sameAs.add(sameAs.createResource(IDENTIFIER_SERVICE.getUri(identifier.getCellarId())), OWL.sameAs,
                            sameAs.createResource(IDENTIFIER_SERVICE.getUri(ps)));
                }
            }
            return sameAs;
        } catch (final RuntimeException exception) {
            JenaUtils.closeQuietly(sameAs);
            throw exception;
        }
    }

    /**
     * Adds sameAs model based on a list of identifiers.
     * Note: This method does not return sameAs triples with the same resource as subject and object (for instance: <resource:a><owl:sameAs><resource:a>).
     * @param sameAs the sameAs model to extend
     * @param digitalObject the digital object definition to be added to the sameAs model
     */
    public static void addSameAs(final Model sameAs, final CellarIdentifiedObject digitalObject) {
        final Resource cellarResource = sameAs.getResource(digitalObject.getCellarId().getUri());
        for (final ContentIdentifier contentIdentifier : digitalObject.getContentids()) {
            Resource object = sameAs.getResource(contentIdentifier.getUri());
            if (!cellarResource.equals(object)) {
                sameAs.add(cellarResource, OWL.sameAs, object);
            }
        }
    }

}
