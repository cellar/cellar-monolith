/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice.core
 *             FILE : Rdf2XmlService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 24 Apr 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.notice.core;

import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.semantic.ontology.data.FallbackOntologyData;
import eu.europa.ec.opoce.cellar.semantic.xml.XmlBuilder;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author ARHS Developments
 */
public interface Rdf2XmlService {

    void writeUriAndSameAs(XmlBuilder parentBuilder, Resource resource, Set<Resource> sameAsResources, Model model);

    void writeUri(XmlBuilder parentBuilder, String uri);

    void writeConcept(XmlBuilder conceptBuilder, Resource conceptResource,Map<LanguageBean, List<LanguageBean>> cache,
                      Set<LanguageBean> languages, NoticeType noticeType);

    void writeConceptFacetWithPossibleAnnotation(XmlBuilder xmlBuilder, Property property, Resource resource,
                                                 Map<LanguageBean, List<LanguageBean>> cache,
                                                 Set<LanguageBean> languages, NoticeType noticeType, Model model,
                                                 Set<Resource> allSubjectResources,
                                                 FallbackOntologyData fallbackOntologyData);

    void writeAgent_UnitAdministrative(XmlBuilder auaBuilder, Resource resource, NoticeType noticeType,
                                       Set<LanguageBean> languages);

    void writeItem(XmlBuilder itemBuilder, Resource resource, NodeHandler nodeHandler, Model model);

    void writeDate(XmlBuilder dateBuilder, String dateValue);


    /* annotations */
    void writeAnnotation(XmlBuilder parentBuilder, Property property, RDFNode object, Model model,
                         Set<Resource> allSubjectResources);
}
