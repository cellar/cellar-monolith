package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.param;

import java.io.Serializable;
import java.util.List;

/**
 * The Class DisseminationRequestParameterList.
 */
public class DisseminationRequestParameterList implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -7409391381968033130L;

    /** The dissemination request parameters. */
    private List<DisseminationRequestParameter> disseminationRequestParameters;

    /**
     * Gets the dissemination request parameters.
     *
     * @return the dissemination request parameters
     */
    public List<DisseminationRequestParameter> getDisseminationRequestParameters() {
        return this.disseminationRequestParameters;
    }

    /**
     * Sets the dissemination request parameters.
     *
     * @param disseminationRequestParameters the new dissemination request parameters
     */
    public void setDisseminationRequestParameters(final List<DisseminationRequestParameter> disseminationRequestParameters) {
        this.disseminationRequestParameters = disseminationRequestParameters;
    }

}
