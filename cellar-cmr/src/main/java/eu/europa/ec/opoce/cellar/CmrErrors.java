/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar
 *        FILE : CmrMessageCode.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 13-03-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar;

import eu.europa.ec.opoce.cellar.MessageCode.TYPE;

/**
 * <class_description> CMR error codes. <br/>
 * <br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc. <br/>
 * <br/>
 * ON : 13-03-2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public final class CmrErrors extends CommonErrors {

    /* 100 Codes */
    public static final MessageCode CMR_EMBARGO_ERROR = new MessageCode("E-190", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode CMR_UNEXPECTED_EMBARGO_ERROR = new MessageCode("E-191");

    /* 200 Codes */
    public static final MessageCode CMR_INTERNAL_ERROR = new MessageCode("E-201");
    public static final MessageCode CMR_INTERNAL_ERROR_UNKNOWN_LANGUAGE = new MessageCode("E-202");
    public static final MessageCode CMR_INSTALL_ERROR = new MessageCode("E-209");
    public static final MessageCode CMR_CONFIGURATION_ERROR = new MessageCode("E-210");
    public static final MessageCode BOOT_CONFIGURATION_ERROR = new MessageCode("E-211");
    public static final MessageCode NAL_CONFIGURATION_ERROR = new MessageCode("E-212");
    public static final MessageCode INGESTION_SERVICE_ERROR = new MessageCode("E-220");
    public static final MessageCode INGESTION_S3_WRITE_ERROR = new MessageCode("E-221");
    public static final MessageCode S3_NO_CONTENT_FOR_ID = new MessageCode("E-222");
    public static final MessageCode INGESTION_INVALID_UPDATE_OPERATION = new MessageCode("E-223");
    public static final MessageCode INGESTION_CONTROLLER_INCORRECT_STRUCTMAP = new MessageCode("E-224");
    public static final MessageCode MODEL_LOAD_SERVICE_ERROR = new MessageCode("E-230");
    public static final MessageCode VALIDATION_HIERARCHY = new MessageCode("E-234", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode VALIDATION_ONTOLOGY_ERROR = new MessageCode("E-235", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode VALIDATION_RDFDATA_ERROR = new MessageCode("E-236", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode VALIDATION_RDF_DATA_INCOMPLETE = new MessageCode("E-237", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode VALIDATION_DECODING_OF_CONCEPT_NOT_FOUND = new MessageCode("E-238", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode VALIDATION_EXISTING_WRITE_ONCE_TRIPLE = new MessageCode("E-239", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode INDEXING_REQUEST_SERVICE_ERROR = new MessageCode("E-240");
    public static final MessageCode INDEXING_SERVICE_ERROR = new MessageCode("E-245");
    public static final MessageCode DECODING_SNIPPET_SERVICE = new MessageCode("E-250");
    public static final MessageCode NAME_VIRTUAL_MODEL_NOT_CONFIGURED = new MessageCode("E-251");
    public static final MessageCode EMBARGO_SERVICE_ERROR = new MessageCode("E-255");
    public static final MessageCode EMBARGO_ILLEGAL_IDENTIFIER = new MessageCode("E-256", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode LANGUAGE_ERROR = new MessageCode("E-260");
    public static final MessageCode CORRUPT_LANGUAGEFILE = new MessageCode("E-261");
    public static final MessageCode LANGUAGEFILE_COULD_NOT_BE_PARSED = new MessageCode("E-262");
    public static final MessageCode DEFINED_LANGUAGE_IS_NOT_CORRECT = new MessageCode("E-263", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode DATABASE_ERROR = new MessageCode("E-270");
    public static final MessageCode UNSUPPORTED_ENCODING = new MessageCode("E-271");
    public static final MessageCode ORACLE_LOAD_FAILED = new MessageCode("E-272");
    public static final MessageCode VIRTUOSO_OPERATION_FAILED = new MessageCode("E-273");
    public static final MessageCode VIRTUOSO_BACKUP_FAILED = new MessageCode("E-274");
    public static final MessageCode CMR_FILE_ERROR = new MessageCode("E-280");
    public static final MessageCode GIVEN_PATH_IS_NO_FILEPATH = new MessageCode("E-281");
    public static final MessageCode WRITING_MODEL_TO_FILE_FAILED = new MessageCode("E-282");
    public static final MessageCode NO_FILES_IN_PATH = new MessageCode("E-283");
    public static final MessageCode NAL_API = new MessageCode("E-295");
    public static final MessageCode EUROVOC_API = new MessageCode("E-296");

    /* 300 Codes */
    public static final MessageCode NAL_API_DATA_CORRUPTION = new MessageCode("E-304");
    public static final MessageCode NAL_LOAD_REQUEST_SERVICE_ERROR = new MessageCode("E-305");
    public static final MessageCode NAL_LOAD_SERVICE_ERROR = new MessageCode("E-306");
    public static final MessageCode ONTO_LOAD_INTERNAL_ERROR = new MessageCode("E-316");
    public static final MessageCode ONTO_MISSING_CONFIG = new MessageCode("E-317");
    public static final MessageCode READONLY_RESOURCE_ERROR = new MessageCode("E-392", TYPE.FUNCTIONAL_ERROR);
    public static final MessageCode INGESTION_IN_CMR_FAILED = new MessageCode("E-397");

    /* 400 Codes */
    public static final MessageCode MERGE_FAILED = new MessageCode("E-456");

}
