package eu.europa.ec.opoce.cellar.s3.repository;

public class S3DataException extends RuntimeException {

    public S3DataException() {
    }

    public S3DataException(String s) {
        super(s);
    }

    public S3DataException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
