/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.service
 *             FILE : EmbargoOperationServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 27 Jan 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.service.impl;

import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.service.EmbargoOperation;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * <class_description> This class represents the service called when the user executes the embargo operation through the interface (RESTful API or UI)..
 * <br/><br/>
 * <notes> .
 * <br/><br/>
 * ON : 27-Jan-2017
 * The Class EmbargoOperationServiceImpl.
 *
 * @author ARHS Developments
 * @version $$Revision$$$
 */
@Service("embargoOperationService")
public class EmbargoOperationServiceImpl extends EmbargoOperationAbstractServiceImpl {

    private static final Logger LOG = LogManager.getLogger(EmbargoOperationServiceImpl.class);

    @Override
    public void loadTargetCellarResources(final EmbargoOperation embargoOperation) {
        //NOP
    }

    @Override
    public void loadIdentifiersTargetList(final EmbargoOperation embargoOperation) {
        final String cellarId = embargoOperation.getObjectId();
        final List<Identifier> identifiersTargetList = this.identifierService.getTreeIdentifiers(cellarId);
        embargoOperation.setIdentifiersTargetList(identifiersTargetList);
    }

    @Override
    public void updateIndexes(EmbargoOperation embargoOperation) {
        LOG.info("Updating notice indexes");

        final ContentIdentifier contentIdentifier = embargoOperation.getContentIdentifier();
        final boolean storedInPrivateDatabase = embargoOperation.isStoredInPrivateDatabase();
        final boolean toBeStoredInPrivateDatabase = embargoOperation.isToBeStoredInPrivateDatabase();

        updateIndexes(contentIdentifier, storedInPrivateDatabase, toBeStoredInPrivateDatabase);
    }

    @Override
    public void updateVirtuoso(final EmbargoOperation embargoOperation) {
        LOG.info("Updating virtuoso");

        final boolean toBeStoredInPrivateDatabase = embargoOperation.isToBeStoredInPrivateDatabase();
        final String cellarId = embargoOperation.getObjectId();
        final Date embargoDate = embargoOperation.getEmbargoDate();
        final Date lastModificationDate = embargoOperation.getLastModificationDate();
        final CellarResource cellarResource = embargoOperation.getCellarResource();

        if (this.cellarConfiguration.isVirtuosoIngestionEnabled()) {
            try {
                if (!toBeStoredInPrivateDatabase) {
                    this.virtuosoService.unhide(cellarId, embargoDate, lastModificationDate, true);
                } else {
                    this.virtuosoService.hideInNewTransaction(cellarId, embargoDate, lastModificationDate, true);
                }
            } catch (final Exception e) {
                LOG.error("Unexpected exception was thrown when (dis)embargoing the digital object of type {"
                        + cellarResource.getCellarType() + "} and id {" + cellarId + "} in Virtuoso", e);
            }
        }
    }

    @Override
    public void updateOracleRdfStore(final EmbargoOperation embargoOperation) {
        LOG.info("Updating Oracle RDF Store");

        final boolean saveInPrivate = embargoOperation.isToBeStoredInPrivateDatabase();
        final Date embargoDate = embargoOperation.getEmbargoDate();
        final Date lastModificationDate = embargoOperation.getLastModificationDate();
        final String cellarId = embargoOperation.getObjectId();

        this.embargoDatabaseService.moveEmbargoedData(cellarId, saveInPrivate, embargoDate, lastModificationDate);
    }

    @Override
    public void updateCellarResource(final EmbargoOperation embargoOperation) {
        LOG.info("Updating cellar resources");

        final Date embargoDate = embargoOperation.getEmbargoDate();
        final Date lastModificationDate = embargoOperation.getLastModificationDate();
        final List<Identifier> identifiersTargetList = embargoOperation.getIdentifiersTargetList();

        identifiersTargetList.forEach(identifier -> updateDateCellarResource(identifier, embargoDate, lastModificationDate));
    }

    @Override
    public void updateS3(final EmbargoOperation embargoOperation) {
        final Date embargoDate = embargoOperation.getEmbargoDate();
        final Date lastModificationDate = embargoOperation.getLastModificationDate();
        embargoOperation.getIdentifiersTargetList().forEach(id ->  {
            final CellarResource cellarResource = cellarResourceDao.findCellarId(id.getCellarId());
            embargoService.updateEmbargoDate(id.getCellarId(),
                    embargoDate, lastModificationDate, cellarResource.getCellarType());
        });
    }

    @Override
    protected EmbargoOperation createEmbargoOperation(String cellarId, Date embargoDate) {
        EmbargoOperation op = new EmbargoOperation();
        op.setObjectId(cellarId);
        op.setEmbargoDate(embargoDate);
        return op;
    }

}
