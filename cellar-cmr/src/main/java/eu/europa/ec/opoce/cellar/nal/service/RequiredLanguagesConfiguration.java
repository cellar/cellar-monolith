package eu.europa.ec.opoce.cellar.nal.service;

import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;

import java.util.List;

/**
 * <p>RequiredLanguagesConfiguration interface.</p>
 */
public interface RequiredLanguagesConfiguration {

    /**
     * Get index languages
     *
     * @return index languages
     */
    List<LanguageBean> getIndexLanguages();
}
