/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.nal.service
 *             FILE : LanguageSkosService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 03 02, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-03-02 07:04:29 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.nal.service;

import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;

/**
 * @author ARHS Developments
 */
public interface LanguageSkosService {
    LanguageBean getLanguage(String languageUri);
}
