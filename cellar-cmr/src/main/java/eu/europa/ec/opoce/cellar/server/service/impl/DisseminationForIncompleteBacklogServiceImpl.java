/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.server.service
 *        FILE : DisseminationServiceImpl.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 30-01-2012
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.service.impl;

import eu.europa.ec.opoce.cellar.domain.content.mets.CellarIdentifiedObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import eu.europa.ec.opoce.cellar.server.service.DisseminationForIncompleteBacklogService;
import eu.europa.ec.opoce.cellar.server.service.MimeTypeResolutionService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;

/**
 * <class_description> Service class for disseminating documents.
 *
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * ON : 02-08-2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class DisseminationForIncompleteBacklogServiceImpl implements DisseminationForIncompleteBacklogService {

    /** The mime type resolution service. */
    @Autowired
    private MimeTypeResolutionService mimeTypeResolutionService;

    /** {@inheritDoc} */
    @Override
    public void checkPropertiesFromBacklog(Model resultModel, final Model directModel, final Model inferredModel,
            final CellarIdentifiedObject cellarObject, final boolean inferred) {
        if (!inferred && DigitalObjectType.MANIFESTATION.equals(cellarObject.getType())) {
            final List<Statement> mimeTypeStatementsForAllItems = mimeTypeResolutionService.resolve(directModel, inferredModel);
            resultModel.add(mimeTypeStatementsForAllItems);
        }
        this.addLastModificationDate(resultModel, directModel, inferredModel, cellarObject, inferred);
        this.addLang(resultModel, directModel, inferredModel, cellarObject, inferred);
        this.removeManifestationHasItem(resultModel, cellarObject, inferred);
    }

    /**
     * Removes the manifestation has item.
     *
     * @param model the model
     * @param cellarObject the cellar object
     * @param inferred the inferred
     */
    private void removeManifestationHasItem(Model model, final CellarIdentifiedObject cellarObject, final boolean inferred) {
        if (!inferred && DigitalObjectType.MANIFESTATION.equals(cellarObject.getType())) {
            final StmtIterator manifestationHasItemStatements = model.listStatements((Resource) null,
                    CellarProperty.HierarchyProperty.cdm_manifestation_has_itemP, (RDFNode) null);
            try {
                model.remove(manifestationHasItemStatements);
            } finally {
                manifestationHasItemStatements.close();
            }
        }
    }

    /**
     * Adds the lang.
     *
     * @param model the model
     * @param directModel the direct model
     * @param inferredModel the inferred model
     * @param cellarObject the cellar object
     * @param inferred the inferred
     */
    private void addLang(Model model, final Model directModel, final Model inferredModel, final CellarIdentifiedObject cellarObject,
            final boolean inferred) {
        if (!inferred && DigitalObjectType.EXPRESSION.equals(cellarObject.getType())) {
            final Resource resource = directModel.getResource(cellarObject.getCellarId().getUri());
            // if the digital object haven't the lang in the direct model
            // then get the lang from inferred model and add it to return model
            if (!resource.hasProperty(CellarProperty.cmr_langP)) {
                final StmtIterator langs = inferredModel.listStatements(resource, CellarProperty.cmr_langP, (RDFNode) null);
                model.add(langs);
            }
        }
    }

    /**
     * Adds the last modification date.
     *
     * @param model the model
     * @param directModel the direct model
     * @param inferredModel the inferred model
     * @param cellarObject the cellar object
     * @param inferred the inferred
     */
    private void addLastModificationDate(Model model, final Model directModel, final Model inferredModel,
            final CellarIdentifiedObject cellarObject, final boolean inferred) {
        if (!inferred) {
            final Resource resource = directModel.getResource(cellarObject.getCellarId().getUri());
            // if the digital object haven't the lastmodificationdate in the direct model
            // then get the lastmodificationdate from inferred model and add it to return model
            if (!resource.hasProperty(CellarProperty.cmr_lastmodificationdateP)) {
                final Statement lastModificationDate = inferredModel.getProperty(resource, CellarProperty.cmr_lastmodificationdateP);
                model.add(lastModificationDate);
            }
        }
    }

}
