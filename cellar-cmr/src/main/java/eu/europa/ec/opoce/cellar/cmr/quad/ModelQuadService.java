/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.quad
 *             FILE : ModelQuadService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Sep 6, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.quad;

import org.apache.jena.rdf.model.Model;
import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;
import eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Sep 6, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface ModelQuadService {

    /**
     * <p>updateDataInOracle.</p>
     *
     * @param data a {@link eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData} object.
     */
    void updateDataInOracle(final CalculatedData data);

    /**
     * Update data in oracle.
     *
     * @param underEmbargo             the under embargo
     * @param metadataClustersToDelete the metadata clusters to delete
     * @param inverseClustersToDelete  the inverse clusters to delete
     * @param metadataToAdd            the metadata to add
     * @param inverseToAdd             the inverse to add
     */
    void updateDataInOracle(final boolean underEmbargo, final List<String> metadataClustersToDelete,
                            final List<String> inverseClustersToDelete, final Map<String, Model> metadataToAdd, final Map<String, Model> inverseToAdd);

    /**
     * Update data in oracle.
     *
     * @param underEmbargo     the under embargo
     * @param clustersToDelete the clusters to delete
     * @param metadataToAdd    the metadata to add
     * @param inverseToAdd     the inverse to add
     */
    void updateDataInOracle(final boolean underEmbargo, final List<String> clustersToDelete,
                            final Map<String, Model> metadataToAdd, final Map<String, Model> inverseToAdd);

    /**
     * Move embargoed data in oracle.
     *
     * @param cellarId             the cellar id
     * @param toPrivate            the to private
     * @param embargoDate          the embargo date
     * @param lastModificationDate the last modification date
     */
    void moveEmbargoedDataInOracle(final String cellarId, final boolean toPrivate, final Date embargoDate,
                                   final Date lastModificationDate);

    /**
     * Move embargoed data in oracle.
     *
     * @param cellarIds            the cellar ids
     * @param toPrivate            the to private
     * @param embargoDate          the embargo date
     * @param lastModificationDate the last modification date
     */
    void moveEmbargoedDataInOracle(final Collection<String> cellarIds, final boolean toPrivate,
                                   final Date embargoDate, final Date lastModificationDate);

    /**
     * Removes the contexts corresponding to the {@code identifiers}.
     *
     * @param identifiers the identifiers identifying the contexts to remove
     */
    void deleteContexts(final Collection<Identifier> identifiers);

}
