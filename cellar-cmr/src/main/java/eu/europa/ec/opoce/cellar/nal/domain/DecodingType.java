package eu.europa.ec.opoce.cellar.nal.domain;

/**
 * <p>DecodingType class.</p>
 *
 * @author Pieter.Fannes
 */
public enum DecodingType {
    EXCLUDE_RELATED, INCLUDE_RELATED
}
