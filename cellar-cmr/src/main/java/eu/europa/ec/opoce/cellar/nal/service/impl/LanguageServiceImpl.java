/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.nal.languageconfiguration
 *             FILE : LanguageServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 19-Jan-2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.nal.service.impl;

import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.LanguageIsoCode;
import eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Expression;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Work;
import eu.europa.ec.opoce.cellar.domain.content.mets.CellarIdentifiedObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.dao.RequiredLanguageDbRecord;
import eu.europa.ec.opoce.cellar.nal.domain.RequiredLanguageBean;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.nal.repository.RequiredLanguageDbGateway;
import eu.europa.ec.opoce.cellar.nal.service.LanguageService;
import eu.europa.ec.opoce.cellar.nal.service.LanguagesConfiguration;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author ARHS Developments
 */
@Service
public class LanguageServiceImpl implements LanguageService {

    private static final Logger LOG = LogManager.getLogger(LanguageServiceImpl.class);
    private static final Comparator<RequiredLanguageDbRecord> alternativeLanguageComparator = Comparator.comparing(o -> (o.getOrder()));
    private static final Transformer<RequiredLanguageDbRecord, String> stringTransformer = RequiredLanguageDbRecord::getAlternativeUri;

    private final Map<String, LanguageBean> byThreeChar = new HashMap<>();
    private final Map<String, LanguageBean> byTwoChar = new HashMap<>();
    private final Map<String, LanguageBean> byCode = new HashMap<>();
    private final Map<String, LanguageBean> byUri = new HashMap<>();
    private final Map<String, RequiredLanguageBean> requiredLanguages = new HashMap<>();
    private final Comparator<CellarIdentifiedObject> cellarIdComparator = (o1, o2) -> {
        // Order all multi-lingual expression by CELLAR ID
        String thisCellarId = o1.getCellarId().getIdentifier();
        String otherCellarId = o2.getCellarId().getIdentifier();
        return thisCellarId.compareTo(otherCellarId);
    };

    private LanguagesConfiguration languagesConfiguration;
    private RequiredLanguageDbGateway requiredLanguageDbGateway;

    public LanguageServiceImpl() {}

    @Autowired
    public void setLanguagesConfiguration(LanguagesConfiguration languagesConfiguration) {
        this.languagesConfiguration = languagesConfiguration;
    }

    @Autowired
    public void setRequiredLanguageDbGateway(RequiredLanguageDbGateway requiredLanguageDbGateway) {
        this.requiredLanguageDbGateway = requiredLanguageDbGateway;
    }

    @PostConstruct
    private void init() {
        List<LanguageBean> languageBeans = languagesConfiguration.getLanguages();
        for (LanguageBean languageBean : languageBeans) {
            String uri = languageBean.getUri();
            if (byUri.containsKey(uri)) {
                throw ExceptionBuilder.get(CellarException.class)
                        .withCode(CmrErrors.CMR_INTERNAL_ERROR)
                        .withMessage("Duplicate language (based on three char code): {}")
                        .withMessageArgs(uri)
                        .build();
            }
            if (byThreeChar.containsKey(languageBean.getIsoCodeThreeChar())) {
                throw ExceptionBuilder.get(CellarException.class)
                        .withCode(CmrErrors.CMR_INTERNAL_ERROR)
                        .withMessage("Duplicate language (based on three char code): {}")
                        .withMessageArgs(uri)
                        .build();
            }
            if (byTwoChar.containsKey(languageBean.getIsoCodeTwoChar())) {
                throw ExceptionBuilder.get(CellarException.class)
                        .withCode(CmrErrors.CMR_INTERNAL_ERROR)
                        .withMessage("Duplicate language (based on three char code): {}")
                        .withMessageArgs(uri)
                        .build();
            }
            if (byCode.containsKey(languageBean.getCode())) {
                throw ExceptionBuilder.get(CellarException.class)
                        .withCode(CmrErrors.CMR_INTERNAL_ERROR)
                        .withMessage("Duplicate language (based on three char code): {}")
                        .withMessageArgs(uri)
                        .build();
            }

            byUri.put(uri, languageBean);
            byThreeChar.put(languageBean.getIsoCodeThreeChar(), languageBean);
            byTwoChar.put(languageBean.getIsoCodeTwoChar(), languageBean);
            byCode.put(languageBean.getCode(), languageBean);
        }

        for (RequiredLanguageBean requiredLanguageBean : getRequiredLanguagesInternal()) {
            requiredLanguages.put(requiredLanguageBean.getUri(), requiredLanguageBean);
        }

        LOG.info(IConfiguration.CONFIG, "{} languages loaded ({}).", requiredLanguages.size(), requiredLanguages.keySet());

    }

    @Override
    public LanguageBean getByUri(String uri) {
        if (!byUri.containsKey(uri)) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withCode(CmrErrors.CMR_INTERNAL_ERROR_UNKNOWN_LANGUAGE)
                    .withMessage("Language with uri '{}' not found")
                    .withMessageArgs(uri)
                    .build();
        }
        return byUri.get(uri);
    }

    @Override
    public LanguageBean getByTwoOrThreeChar(String charCode) {
        LanguageBean languageBean = byTwoChar.get(charCode);
        return languageBean != null ? languageBean : byThreeChar.get(charCode);
    }

    @Override
    public LanguageBean getByThreeChar(String threeChar) {
        if (!byThreeChar.containsKey(threeChar)) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withCode(CmrErrors.CMR_INTERNAL_ERROR_UNKNOWN_LANGUAGE)
                    .withMessage("Language with 3 char chode '{}' not found")
                    .withMessageArgs(threeChar)
                    .build();
        }
        return byThreeChar.get(threeChar);
    }

    @Override
    public LanguageBean getByTwoChar(String twoChar) {
        if (!byTwoChar.containsKey(twoChar)) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withCode(CmrErrors.CMR_INTERNAL_ERROR_UNKNOWN_LANGUAGE)
                    .withMessage("Language with 2 char code '{}' not found")
                    .withMessageArgs(twoChar)
                    .build();
        }
        return byTwoChar.get(twoChar);
    }

    @Override
    public List<String> getFallbackLanguagesFor(String uri) {
        RequiredLanguageBean requiredLanguage = getRequiredLanguage(uri);
        return requiredLanguage != null ? requiredLanguage.getAlternatives() : Collections.emptyList();
    }

    @Override
    public RequiredLanguageBean getRequiredLanguage(String uri) {
        return requiredLanguages.get(uri);
    }

    @Override
    public List<RequiredLanguageBean> getRequiredLanguages() {
        return new ArrayList<>(requiredLanguages.values());
    }

    @Override
    public List<String> getRequiredLanguageCodes() {
        return requiredLanguages.values().stream()
                .map(RequiredLanguageBean::getUri)
                .map(LanguageServiceImpl::toLanguageCode)
                .collect(Collectors.toList());
    }

    private static String toLanguageCode(final String uri) {
        return uri.substring(uri.lastIndexOf('/') + 1, uri.length());
    }

    private List<RequiredLanguageBean> getRequiredLanguagesInternal() {
        Map<String, List<RequiredLanguageDbRecord>> recordsPerLanguage = requiredLanguageDbGateway.selectAll();

        List<RequiredLanguageBean> languageBeans = new ArrayList<>(recordsPerLanguage.size());
        for (String language : recordsPerLanguage.keySet()) {
            languageBeans.add(newRequiredLanguageBeanFrom(language, recordsPerLanguage.get(language)));
        }

        return languageBeans;
    }

    @Override
    public void filterNonEuLanguagesExpression(final Collection<Expression> inCollection) {
        CollectionUtils.filter(inCollection, arg0 -> {
            boolean result = true;
            final Set<LanguageBean> languages = arg0.getLangs();
            for (final LanguageBean language : languages) {
                final LanguageIsoCode isoCode = language.getIsoCode();
                final String isoCodeThreeChar = isoCode.getIsoCodeThreeChar();
                final LanguageBean euroLang = getByTwoOrThreeChar(isoCodeThreeChar);
                if (euroLang == null) {
                    result = false;
                    break;
                }
            }
            return result;
        });
    }

    @Override
    public void filterNonEuLanguages(final Collection<DigitalObject> inCollection) {
        CollectionUtils.filter(inCollection, arg0 -> {
            boolean result = true;
            if (DigitalObjectType.EXPRESSION.equals(arg0.getType())) {
                final List<LanguageIsoCode> isoCodes = arg0.getLanguages();
                result = isEuLanguage(isoCodes);
            } else if (DigitalObjectType.MANIFESTATION.equals(arg0.getType())) {
                final DigitalObject parentObject = arg0.getParentObject();
                if (parentObject != null) {
                    final List<LanguageIsoCode> isoCodes = parentObject.getLanguages();
                    result = isEuLanguage(isoCodes);
                }

            }
            return result;
        });
    }

    /**
     * Checks if is eu language.
     *
     * @param isoCodes the iso codes
     * @return true, if is eu language
     */
    private boolean isEuLanguage(List<LanguageIsoCode> isoCodes) {
        boolean isEu = true;
        for (final LanguageIsoCode isoCode : isoCodes) {
            final String isoCodeThreeChar = isoCode.getIsoCodeThreeChar();
            final LanguageBean language = getByTwoOrThreeChar(isoCodeThreeChar);
            if (language == null) {
                isEu = false;
                break;
            }
        }
        return isEu;
    }

    private RequiredLanguageBean newRequiredLanguageBeanFrom(String uri, List<RequiredLanguageDbRecord> records) {
        List<RequiredLanguageDbRecord> alternatives = new ArrayList<>(records.size());
        for (RequiredLanguageDbRecord alternative : records) {
            if (!StringUtils.isBlank(alternative.getAlternativeUri())) {
                alternatives.add(alternative);
            }
        }
        alternatives.sort(alternativeLanguageComparator);
        List<String> alternativesAsString = CollectionUtils.collect(alternatives, stringTransformer,
                new ArrayList<>(alternatives.size()));
        return new RequiredLanguageBean(uri, alternativesAsString);
    }

    @Override
    public List<Expression> filterMultiLinguisticExpressions(Work work) {
        //Prioritize mono-lingual expressions over multi-lingual ones 
        Collection<Expression> children = work.getChildren();
        List<Expression> result = children.stream().filter(expression -> expression.getLangs().size() == 1).collect(Collectors.toList());

        Set<LanguageBean> monoLingualExpressionLanguages = result.stream().map(Expression::getLangs)
                .flatMap(Collection::stream).collect(Collectors.toSet());

        //Discarding multi-lingual expressions that reference a language used by any mono-lingual expression
        List<Expression> multiLingualExpressions = children.stream().filter(expression -> {
            boolean isMultilingual = expression.getLangs().size() > 1;
            boolean disjoint = Collections.disjoint(monoLingualExpressionLanguages, expression.getLangs());
            return isMultilingual && disjoint;
        }).sorted(cellarIdComparator).collect(Collectors.toList());

        //keep the “first original”. Discarding any multi-lingual expression that references a language used by any predecessor
        Map<Set<LanguageBean>, Expression> multilanguagesMap = new HashMap<>();
        multiLingualExpressions.forEach(expression -> {
            if (multilanguagesMap.isEmpty()) {
                multilanguagesMap.put(expression.getLangs(), expression);
            } else {
                if (multilanguagesMap.keySet().stream().allMatch(langsSet -> Collections.disjoint(langsSet, expression.getLangs()))) {
                    multilanguagesMap.put(expression.getLangs(), expression);
                }
            }
        });
        result.addAll(multilanguagesMap.values());
        return result;
    }

    @Override
    public Collection<DigitalObject> filterMultiLinguisticExpressions(Collection<DigitalObject> dos) {
        final List<DigitalObject> result = new ArrayList<>();
        List<DigitalObject> works = filter(dos, DigitalObjectType.WORK);
        List<DigitalObject> expressions = filter(dos, DigitalObjectType.EXPRESSION);
        List<DigitalObject> manifestations = filter(dos, DigitalObjectType.MANIFESTATION);

        if (works.isEmpty() && expressions.isEmpty() && manifestations.isEmpty()) {
            result.addAll(dos);
        } else {
            //Prioritize mono-lingual expressions over multi-lingual ones 
            List<DigitalObject> monoLinguisticExpressions = expressions.stream().filter(expression -> expression.getLanguages().size() == 1)
                    .collect(Collectors.toList());
            Set<LanguageIsoCode> monoLingualExpressionLanguages = monoLinguisticExpressions.stream()
                    .map(DigitalObject::getLanguages)
                    .flatMap(Collection::stream).collect(Collectors.toSet());

            //Discarding multi-lingual expressions that reference a language used by any mono-lingual expression
            List<DigitalObject> multiLingualExpressions = expressions.stream().filter(expression -> {
                boolean isMultilingual = expression.getLanguages().size() > 1;
                boolean disjoint = Collections.disjoint(monoLingualExpressionLanguages, expression.getLanguages());
                return isMultilingual && disjoint;
            }).sorted(cellarIdComparator).collect(Collectors.toList());

            //keep the “first original”. Discarding any multi-lingual expression that references a language used by any predecessor
            Map<Set<LanguageIsoCode>, DigitalObject> multiLanguagesMap = new HashMap<>();
            multiLingualExpressions.forEach(expression -> {
                if (multiLanguagesMap.isEmpty()) {
                    Set<LanguageIsoCode> set = new HashSet<>(expression.getLanguages());
                    multiLanguagesMap.put(set, expression);
                } else {
                    if (multiLanguagesMap.keySet().stream()
                            .allMatch(langsSet -> Collections.disjoint(langsSet, expression.getLanguages()))) {
                        Set<LanguageIsoCode> set = new HashSet<>(expression.getLanguages());
                        multiLanguagesMap.put(set, expression);
                    }
                }
            });

            result.addAll(works);
            result.addAll(monoLinguisticExpressions);
            result.addAll(multiLanguagesMap.values());

            //filter the manifestations
            List<DigitalObject> filteredManifestations = manifestations.stream()
                    .filter(manifestation -> result.contains(manifestation.getParentObject())).collect(Collectors.toList());
            result.addAll(filteredManifestations);
        }
        return result;
    }


    /**
     * Filter.
     *
     * @param original          the original
     * @param digitalObjectType the digital object type
     * @return the list
     */
    private List<DigitalObject> filter(Collection<DigitalObject> original, DigitalObjectType digitalObjectType) {
        return original.stream().filter(digitalObject -> digitalObjectType.equals(digitalObject.getType()))
                .collect(Collectors.toList());
    }
}
