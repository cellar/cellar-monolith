/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination
 *             FILE : AcceptLanguages.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 1, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang.StringUtils;

/**
 * Sorted!
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Apr 1, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class SortedAcceptLanguages extends ArrayList<AcceptLanguage> {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public SortedAcceptLanguages() {
        super();
    }

    public SortedAcceptLanguages(final int size) {
        super(size);
    }

    public SortedAcceptLanguages(final Collection<AcceptLanguage> collection) {
        super(collection);
    }

    public List<String> getSortedLanguageCodes() {
        final List<String> languageCodes = new ArrayList<String>(this.size());

        for (final AcceptLanguage acceptLanguage : this) {
            languageCodes.add(StringUtils.trim(acceptLanguage.getLanguageCode()));
        }
        return languageCodes;
    }
}
