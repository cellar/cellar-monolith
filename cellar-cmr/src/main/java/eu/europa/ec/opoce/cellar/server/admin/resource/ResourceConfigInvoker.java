package eu.europa.ec.opoce.cellar.server.admin.resource;

import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.nal.service.RequiredLanguagesConfiguration;
import eu.europa.ec.opoce.cellar.nal.service.impl.LanguageSkosServiceImpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * This class handles all calls from the Resource Controller in the admin iface
 */

@Component
public class ResourceConfigInvoker {

    @Autowired(required = true)
    LanguageSkosServiceImpl languageSkosService;

    @Autowired(required = true)
    RequiredLanguagesConfiguration requiredLanguagesConfiguration;

    /**
     * This method returns a list of language beans sorted by uri
     *
     * @return a list of language beans
     */
    public List<LanguageBean> getLanguages() {
        return requiredLanguagesConfiguration.getIndexLanguages();
    }

    public LanguageBean getLanguage(final String isoCodeThreeChar) {
        final List<LanguageBean> languages = this.getLanguages();

        for (final LanguageBean language : languages) {
            if (language.getIsoCodeThreeChar().equalsIgnoreCase(isoCodeThreeChar)) {
                return language;
            }
        }

        return null;
    }

}
