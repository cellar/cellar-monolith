package eu.europa.ec.opoce.cellar.nal.service.impl;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QueryException;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.domain.NalRelatedBean;
import eu.europa.ec.opoce.cellar.nal.service.NalSparqlService;
import eu.europa.ec.opoce.cellar.semantic.jena.template.ListOfMapHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.esotericsoftware.yamlbeans.YamlReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static eu.europa.ec.opoce.cellar.nal.utils.NalSparqlQueryTemplate.broaderTransitivesQuery;
import static eu.europa.ec.opoce.cellar.nal.utils.NalSparqlQueryTemplate.conceptFacetQuery;
import static eu.europa.ec.opoce.cellar.nal.utils.NalSparqlQueryTemplate.conceptQuery;
import static eu.europa.ec.opoce.cellar.nal.utils.NalSparqlQueryTemplate.eurovocDomainQuery;
import static eu.europa.ec.opoce.cellar.nal.utils.NalSparqlQueryTemplate.eurovocMicrothesauriQuery;
import static eu.europa.ec.opoce.cellar.semantic.jena.template.JenaTemplate.select;
import static eu.europa.ec.opoce.cellar.semantic.jena.template.ListOfMapHandlers.toSetOfResourcesResolver;

@Service
public class NalSparqlServiceImpl implements NalSparqlService {

    private static final Logger LOG = LogManager.getLogger(NalSparqlServiceImpl.class);

    private static final String SKOS_INFERENCE_SPARQL_FILE_PATH = "skos-inference-sparql.yml";
    private List<Query> skosInferenceSparqlList;

    public NalSparqlServiceImpl() {
        skosInferenceSparqlList = new ArrayList<>();

        try (InputStreamReader yamlFile = new InputStreamReader(NalSparqlService.class.getResourceAsStream(SKOS_INFERENCE_SPARQL_FILE_PATH))) {
        	
        	YamlReader yamlReader = new YamlReader(yamlFile);
        	Map<String, List<String>> queriesMap = (Map<String, List<String>>) yamlReader.read();
        	List<String> queriesList = queriesMap.get("queries");
        	yamlReader.close();
        	if (queriesList != null && !queriesList.isEmpty()) {
        		for (String query : queriesList) {
        			skosInferenceSparqlList.add(QueryFactory.create(query));
        		}
        		LOG.info(IConfiguration.CONFIG, "{} sparql queries loaded from {}.", skosInferenceSparqlList.size(), SKOS_INFERENCE_SPARQL_FILE_PATH);
        	}	

        } catch (IOException | QueryException ioe) {
        	throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.NAL_LOAD_SERVICE_ERROR).withCause(ioe)
        	.withMessage("Failed to load skos sparql yml file : {}.").withMessageArgs(SKOS_INFERENCE_SPARQL_FILE_PATH)
        	.build();
        }
    }

    @Override
    public List<Query> getSkosInferenceSparqlList() {
        return skosInferenceSparqlList;
    }

    /**
     * <p>getConceptsFor.</p>
     *
     * @param model         a {@link org.apache.jena.rdf.model.Model} object.
     * @param conceptScheme a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    @Override
    public List<Resource> getConceptsFor(Model model, String conceptScheme) {
        return select(model, conceptQuery(conceptScheme), toSetOfResourcesResolver("c"));
    }

    /**
     * <p>getEurovocMicrothesauri.</p>
     *
     * @param model a {@link org.apache.jena.rdf.model.Model} object.
     * @return a {@link java.util.List} object.
     */
    @Override
    public List<Resource> getEurovocMicrothesauri(Model model) {
        return select(model, eurovocMicrothesauriQuery(), toSetOfResourcesResolver("m"));
    }

    /**
     * <p>getEurovocDomains.</p>
     *
     * @param model a {@link org.apache.jena.rdf.model.Model} object.
     * @return a {@link java.util.List} object.
     */
    @Override
    public List<Resource> getEurovocDomains(Model model) {
        return select(model, eurovocDomainQuery(), toSetOfResourcesResolver("d"));
    }

    /**
     * <p>getBroaderTransitives.</p>
     *
     * @param model         a {@link org.apache.jena.rdf.model.Model} object.
     * @param conceptScheme a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    @Override
    public List<NalRelatedBean> getBroaderTransitives(Model model, String conceptScheme) {
        //TODO JenaUtils
        return select(model, broaderTransitivesQuery(conceptScheme), (ListOfMapHandler<List<NalRelatedBean>>) resultSet -> {
            List<NalRelatedBean> results = new ArrayList<>(resultSet.size());
            for (Map<String, RDFNode> binding : resultSet) {
                String concept = ((Resource) binding.get("c")).getURI();
                String broaderTransitive = ((Resource) binding.get("b")).getURI();

                results.add(new NalRelatedBean(concept, broaderTransitive));
            }

            return results;
        });
    }

    /**
     * <p>getFacets.</p>
     *
     * @param model a {@link org.apache.jena.rdf.model.Model} object.
     * @return a {@link java.util.List} object.
     */
    @Override
    public List<NalRelatedBean> getFacets(Model model) {
        return select(model, conceptFacetQuery(), (ListOfMapHandler<List<NalRelatedBean>>) resultSet -> {
            List<NalRelatedBean> results = new ArrayList<>(resultSet.size());
            for (Map<String, RDFNode> binding : resultSet) {
                String concept = ((Resource) binding.get("c")).getURI();
                String facet = ((Resource) binding.get("f")).getURI();

                if (!concept.equals(facet)) {
                    results.add(new NalRelatedBean(concept, facet));
                }
            }

            return results;
        });
    }
}
