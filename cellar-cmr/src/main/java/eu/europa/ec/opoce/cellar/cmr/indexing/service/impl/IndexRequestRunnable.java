/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.indexing.service.impl.runnable
 *             FILE : IndexRequestRunnable.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 31, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.indexing.service.impl;

import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditableAdvice;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CmrIndexRequestDao;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestBatch;
import eu.europa.ec.opoce.cellar.common.concurrent.PriorityCallable;
import eu.europa.ec.opoce.cellar.logging.LogContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

import static eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration.INDEXATION;
import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.CMR_INDEXING;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 31, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@LogContext
class IndexRequestRunnable implements PriorityCallable<Long> {

    private static final Logger LOG = LogManager.getLogger(IndexRequestRunnable.class);

    private IndexingProcessor indexingProcessor;
    private final CmrIndexRequestBatch cmrIndexRequest;
    private final CmrIndexRequestDao cmrIndexRequestDao;
    private final long waitTime;

    IndexRequestRunnable(IndexingProcessor indexingProcessor, CmrIndexRequestBatch cmrIndexRequest,
                         CmrIndexRequestDao cmrIndexRequestDao) {
        this.indexingProcessor = indexingProcessor;
        this.cmrIndexRequest = cmrIndexRequest;
        this.cmrIndexRequestDao = cmrIndexRequestDao;
        this.waitTime = System.currentTimeMillis();
    }

    @Override
    @LogContext(CMR_INDEXING)
    public Long call() throws Exception {
        audit();
        updateStartDate();
        LOG.debug(INDEXATION, "Start {} in thread {}", cmrIndexRequest, Thread.currentThread().getName());
        long waitTime = System.currentTimeMillis() - this.waitTime;
        indexingProcessor.updateBatchStatusAndProceed(cmrIndexRequest);
        return waitTime;
    }

    private void updateStartDate() {
        cmrIndexRequest.getCmrIndexRequests().forEach(r -> {
            r.setExecutionStartDate(new Date());
            cmrIndexRequestDao.updateObject(r);
        });
    }

    private static void audit() {
        ThreadContext.put("indexing_key", UUID.randomUUID().toString());
        ThreadContext.put(AuditableAdvice.AUDIT_KEY, ThreadContext.get("indexing_key"));
    }

    @Override
    public int compareTo(PriorityCallable<Long> o) {
        IndexRequestRunnable other = (IndexRequestRunnable) o;
        return this.cmrIndexRequest.getPriority().compareTo(other.cmrIndexRequest.getPriority());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        IndexRequestRunnable callback = (IndexRequestRunnable) o;
        return compareTo(callback) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(cmrIndexRequest.getPriority());
    }

    public CmrIndexRequestBatch getCmrIndexRequest() {
        return cmrIndexRequest;
    }
}
