package eu.europa.ec.opoce.cellar.cmr;

/**
 * The Enum AlignerStatus.
 */
public enum AlignerStatus {

    /** The unknown : The status of this resource is unknown. A diagnostic or a fix can be performed.. */
    UNKNOWN,
    /** The processing : The system is running on this resource.. */
    PROCESSING,
    /** The valid : The resource is valid/aligned.. */
    VALID,
    /** The fixed : The resource has been fixed/aligned.. */
    FIXED,
    /** The corrupt : The resource has be diagnosed and misalignment(s) has(have) been identified.. */
    CORRUPT,
    /** The newly ingested : The resource has been ingested recently and it is supposed aligned.. */
    NEWLY_INGESTED, //TODO set upon ingestion
    /** The error: An error occurred during the processing. This case may occur when an unknown misalignment is found. A manual intervention is required to fix this special case. */
    ERROR;

    /**
     * Gets the cellar resource aligner status.
     *
     * @param ordinal the ordinal
     * @return the cellar resource aligner status
     */
    public static AlignerStatus getCellarResourceAlignerStatus(final byte ordinal) {
        return AlignerStatus.values()[ordinal];
    }

}
