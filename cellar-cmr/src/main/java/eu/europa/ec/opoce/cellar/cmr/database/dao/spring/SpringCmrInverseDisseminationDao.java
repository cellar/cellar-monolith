/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.cmr.database.dao.spring
 *        FILE : SpringCellarResourceDao.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 02-08-2012
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.database.dao.spring;

import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.cmr.CmrInverseDissemination;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CmrInverseDisseminationDao;
import eu.europa.ec.opoce.cellar.cmr.database.util.JdbcUtils;
import eu.europa.ec.opoce.cellar.cmr.model.ModelUtils;
import eu.europa.ec.opoce.cellar.common.util.IterableUtils;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import oracle.jdbc.OracleClob;
import oracle.jdbc.OraclePreparedStatement;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.Lang;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

import java.sql.Clob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * <class_description> Data access object implementation for {@link eu.europa.ec.opoce.cellar.cmr.database.dao.CmrInverseDisseminationDao}.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 04-02-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Repository
public class SpringCmrInverseDisseminationDao extends CmrSpringBaseDao<CmrInverseDissemination, Long> implements CmrInverseDisseminationDao {

    /**
     * The Constant LOG.
     */
    private static final Logger LOG = LogManager.getLogger(SpringCmrInverseDisseminationDao.class);

    /**
     * <p>Constructor for SpringCmrInverseDisseminationDao.</p>
     */
    public SpringCmrInverseDisseminationDao() {
        super(TABLE_NAME, Arrays.asList(Column.CONTEXT, Column.INVERSE_MODEL, Column.SAMEAS_MODEL, Column.IS_BACKLOG));
    }

    /**
     * Creates the.
     *
     * @param rs the rs
     * @return the cmr inverse dissemination
     * @throws SQLException the SQL exception
     * @see eu.europa.ec.opoce.cellar.cmr.database.dao.spring.SpringBaseDao#create(java.sql.ResultSet)
     */
    @Override
    public CmrInverseDissemination create(final ResultSet rs) throws SQLException {
        return new CmrInverseDissemination() {
            {
                final String context = rs.getString(Column.CONTEXT);
                LOG.debug("Retrieving inverse for context '{}'", context);
                final Model inverseModel = readClobAsModel(rs, Column.INVERSE_MODEL);
                final Model sameAsModel = readClobAsModel(rs, Column.SAMEAS_MODEL);
                final boolean isBacklog = rs.getByte(Column.IS_BACKLOG) == 1;

                setContext(context);
                setInverseModel(inverseModel);
                setSameAsModel(sameAsModel);
                setBacklog(isBacklog);
            }
        };
    }

    // CELLAR-2202
    private Model readClobAsModel(final ResultSet rs, final String column) throws SQLException {
        final Clob clob = rs.getClob(column);

        if (clob == null) {
            return null;
        }

        final String string = clob.getSubString(1L, (int) clob.length());
        return ModelUtils.deserialize(string, Lang.RDFXML.getName());
    }

    /**
     * Fill map.
     *
     * @param cmrInverseDissemination the cmr inverse dissemination
     * @param map                     the map
     */
    @Override
    public void fillMap(final CmrInverseDissemination cmrInverseDissemination, final Map<String, Object> map) {
        map.put(Column.CONTEXT, cmrInverseDissemination.getContext());

        if (cmrInverseDissemination.getInverseModel() != null) {
            final String modelStr = ModelUtils.serialize(cmrInverseDissemination.getInverseModel(), Lang.RDFXML.getName());
            map.put(Column.INVERSE_MODEL, modelStr);
        }
        if (cmrInverseDissemination.getSameAsModel() != null) {
            final String modelStr = ModelUtils.serialize(cmrInverseDissemination.getSameAsModel(), Lang.RDFXML.getName());
            map.put(Column.SAMEAS_MODEL, modelStr);
        }

        map.put(Column.IS_BACKLOG, Byte.valueOf(cmrInverseDissemination.isBacklog() ? "1" : "0"));
    }

    /**
     * Upsert by context.
     *
     * @param object the object
     * @return the cmr inverse dissemination
     * @see eu.europa.ec.opoce.cellar.cmr.database.dao.CmrInverseDisseminationDao#upsertByContext(eu.europa.ec.opoce.cellar.cmr.CmrInverseDissemination)
     */
    @Override
    public CmrInverseDissemination upsertByContext(final CmrInverseDissemination object) {
        return this.upsertObject(object, Column.CONTEXT);
    }

    /**
     * Upsert by context.
     *
     * @param object     the object
     * @param connection the connection
     * @return the cmr inverse dissemination
     * @see eu.europa.ec.opoce.cellar.cmr.database.dao.CmrInverseDisseminationDao#upsertByContext(eu.europa.ec.opoce.cellar.cmr.CmrInverseDissemination, java.sql.Connection)
     */
    @Override
    public CmrInverseDissemination upsertByContext(final CmrInverseDissemination object, final Connection connection) {
        final String query = this.prepareUpsertQuery(object, Column.CONTEXT);

        OraclePreparedStatement upsertStatement = null;
        OracleClob inverseModelClob = null;
        OracleClob sameAsModelClob = null;
        try {
            upsertStatement = (OraclePreparedStatement) connection.prepareStatement(query);
            upsertStatement.setStringAtName(CmrInverseDisseminationDao.Column.CONTEXT, object.getContext());
            upsertStatement.setByteAtName(CmrInverseDisseminationDao.Column.IS_BACKLOG, Byte.valueOf(object.isBacklog() ? "1" : "0"));
            inverseModelClob = setClobOnStatement(ModelUtils.serialize(object.getInverseModel(), Lang.RDFXML.getName()), connection, upsertStatement,
                    CmrInverseDisseminationDao.Column.INVERSE_MODEL);
            sameAsModelClob = setClobOnStatement(ModelUtils.serialize(object.getSameAsModel(), Lang.RDFXML.getName()), connection, upsertStatement,
                    CmrInverseDisseminationDao.Column.SAMEAS_MODEL);
            upsertStatement.execute();
        } catch (final SQLException e) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.CMR_INTERNAL_ERROR).withCause(e)
                    .withMessage("Unable to upsert dissemination table for context '{}'")
                    .withMessageArgs(object.getContext(), e.getLocalizedMessage()).build();
        } finally {
            try {
                if ((inverseModelClob != null) && inverseModelClob.isOpen()) {
                    inverseModelClob.close();
                }
                if ((sameAsModelClob != null) && sameAsModelClob.isOpen()) {
                    sameAsModelClob.close();
                }
            } catch (final SQLException e) {
                LOG.warn("Unable to close clobs while upserting dissemination table for context '{}'", object.getContext());
            } finally {
                JdbcUtils.closeStatement(upsertStatement);
            }
        }

        return object;
    }

    /**
     * Select by contexts.
     *
     * @param contexts the contexts
     * @param pageSize the page size
     * @return the collection
     * @see eu.europa.ec.opoce.cellar.cmr.database.dao.CmrInverseDisseminationDao#selectByContexts(java.util.List, int)
     */
    @Override
    public Collection<CmrInverseDissemination> selectByContexts(final List<String> contexts, final int pageSize) {
        return selectByContexts(contexts, pageSize, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<CmrInverseDissemination> selectByContexts(final List<String> contexts, final int pageSize, final String tableName) {
        if (contexts == null) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument status cannot be null").build();
        }

        final Collection<CmrInverseDissemination> cmrInverseDisseminations = new ArrayList<>();

        IterableUtils.pageUp(contexts, pageSize, pagedContexts -> {
            try {
                final HashMap<String, Object> map = new HashMap<String, Object>(1) {{
                    put("contexts", pagedContexts);
                }};
                final Collection<CmrInverseDissemination> find = find(SELECT_BY_CONTEXTS_QUERY, map, tableName);
                cmrInverseDisseminations.addAll(find);
            } catch (final DataAccessException dae) {
                throw ExceptionBuilder.get(CellarException.class)
                        .withMessage("A problem occurred while retrieving inverse models for contexts {}.")
                        .withMessageArgs(pagedContexts).withCause(dae).build();
            }
        });

        return cmrInverseDisseminations;
    }

    /**
     * Select by contexts mapped.
     *
     * @param contexts the contexts
     * @param pageSize the page size
     * @return the map
     * @see eu.europa.ec.opoce.cellar.cmr.database.dao.CmrInverseDisseminationDao#selectByContextsMapped(java.util.List, int)
     */
    @Override
    public Map<String, CmrInverseDissemination> selectByContextsMapped(final List<String> contexts, final int pageSize) {
        return selectByContextsMapped(contexts, pageSize, null);
    }

    /**
     * Select by contexts mapped.
     *
     * @param contexts  the contexts
     * @param pageSize  the page size
     * @param tableName the table name
     * @return the map
     * @see eu.europa.ec.opoce.cellar.cmr.database.dao.CmrInverseDisseminationDao#selectByContextsMapped(java.util.List, int)
     */
    @Override
    public Map<String, CmrInverseDissemination> selectByContextsMapped(final List<String> contexts, final int pageSize,
                                                                       final String tableName) {
        final Map<String, CmrInverseDissemination> cmrInverseDisseminationPerContext = new HashMap<>();

        final Collection<CmrInverseDissemination> cmrInverseDisseminations = this.selectByContexts(contexts, pageSize, tableName);
        for (final CmrInverseDissemination cmrInverseDissemination : cmrInverseDisseminations) {
            cmrInverseDisseminationPerContext.put(cmrInverseDissemination.getContext(), cmrInverseDissemination);
        }

        return cmrInverseDisseminationPerContext;
    }

    /**
     * Sets the clob on statement.
     *
     * @param str        the str
     * @param connection the connection
     * @param statement  the statement
     * @param key        the key
     * @return the clob
     * @throws SQLException the SQL exception
     */
    private static OracleClob setClobOnStatement(final String str, final Connection connection, final OraclePreparedStatement statement,
                                                 final String key) throws SQLException {
        final Clob clobObj = connection.createClob();
        clobObj.setString(1, str);
        statement.setClobAtName(key, clobObj);
        return (OracleClob) clobObj;
    }

}
