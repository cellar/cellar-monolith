/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.service
 *             FILE : EmbargoOperationAbstractServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 27-Jan-2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */

package eu.europa.ec.opoce.cellar.cmr.service.impl;

import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.EmbargoDatabaseService;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestGenerationService;
import eu.europa.ec.opoce.cellar.cmr.service.EmbargoOperation;
import eu.europa.ec.opoce.cellar.cmr.service.EmbargoOperationService;
import eu.europa.ec.opoce.cellar.cmr.service.EmbargoOperationValidationService;
import eu.europa.ec.opoce.cellar.cmr.service.EmbargoService;
import eu.europa.ec.opoce.cellar.common.util.TimeUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.logging.LogContext;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.virtuoso.VirtuosoService;
import org.apache.jena.rdf.model.Model;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Date;

import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.CMR_EMBARGO;

/**
 * <class_description>  This class holds the common execution of the (dis)embargo services .
 * <br/><br/>
 * <notes> .
 * <br/><br/>
 * ON : 27-Jan-2017
 * The Class EmbargoOperationAbstractServiceImpl.
 *
 * @author ARHS Developments
 * @version $$Revision$$$
 */
@Service
public abstract class EmbargoOperationAbstractServiceImpl implements EmbargoOperationService {

    private static final Logger LOG = LogManager.getLogger(EmbargoOperationAbstractServiceImpl.class);

    /**
     * The cellar resource dao.
     */
    @Autowired
    protected CellarResourceDao cellarResourceDao;

    /**
     * The identifier service.
     */
    @Autowired
    @Qualifier("pidManagerService")
    protected IdentifierService identifierService;

    /**
     * The embargo database service.
     */
    @Autowired
    protected EmbargoDatabaseService embargoDatabaseService;

    /**
     * The cellar configuration.
     */
    @Autowired
    @Qualifier("cellarConfiguration")
    protected ICellarConfiguration cellarConfiguration;

    /**
     * The s3 write service.
     */
    @Autowired
    protected EmbargoService embargoService;

    /**
     * The virtuoso service.
     */
    @Autowired
    protected VirtuosoService virtuosoService;

    /**
     * The cmr index request generation service.
     */
    @Autowired
    protected CmrIndexRequestGenerationService cmrIndexRequestGenerationService;

    /**
     * The embargo operation validator.
     */
    @Autowired
    protected EmbargoOperationValidationService embargoOperationValidator;

    /**
     * Creates the embargo operation.
     *
     * @param cellarId    the cellar id
     * @param embargoDate the embargo date
     * @return the embargo operation
     */
    protected abstract EmbargoOperation createEmbargoOperation(final String cellarId, final Date embargoDate);

    /**
     * Load target cellar resources.
     *
     * @param embargoOperation the embargo operation
     */
    protected abstract void loadTargetCellarResources(final EmbargoOperation embargoOperation);

    /**
     * Load identifiers target list.
     *
     * @param embargoOperation the embargo operation
     */
    protected abstract void loadIdentifiersTargetList(final EmbargoOperation embargoOperation);

    /**
     * Update indexes.
     *
     * @param embargoOperation the embargo operation
     */
    protected abstract void updateIndexes(EmbargoOperation embargoOperation);

    /**
     * Update virtuoso.
     *
     * @param embargoOperation the embargo operation
     */
    protected abstract void updateVirtuoso(final EmbargoOperation embargoOperation);

    /**
     * Update oracle rdf store.
     *
     * @param embargoOperation the embargo operation
     */
    protected abstract void updateOracleRdfStore(final EmbargoOperation embargoOperation);

    /**
     * Update cellar resource.
     *
     * @param embargoOperation the embargo operation
     */
    protected abstract void updateCellarResource(final EmbargoOperation embargoOperation);

    /**
     * Update s3.
     *
     * @param embargoOperation the embargo operation
     */
    protected abstract void updateS3(final EmbargoOperation embargoOperation);

    @Override
    @LogContext(CMR_EMBARGO)
    public void execute(final String cellarId, final Date embargoDate) {
        final EmbargoOperation embargoOperation = createEmbargoOperation(cellarId, embargoDate);

        LOG.info("Starting (dis)embargo operation");
        embargoOperationValidator.preValidation(embargoOperation);
        loadCellarResource(embargoOperation);
        embargoOperationValidator.postLoadingValidation(embargoOperation);
        loadEmbargoDirectionFlags(embargoOperation);
        embargoOperationValidator.postDirectionSetValidation(embargoOperation);
        loadLastModificationDate(embargoOperation);
        loadTargetCellarResources(embargoOperation);
        updateOracleRdfStore(embargoOperation);
        loadIdentifiersTargetList(embargoOperation);
        updateS3(embargoOperation);
        updateCellarResource(embargoOperation);
        updateVirtuoso(embargoOperation);
        loadContentIdentifier(embargoOperation);
        updateIndexes(embargoOperation);
        LOG.info("Finished (dis)embargo operation");
    }

    /**
     * Load last modification date.
     *
     * @param embargoOperation the embargo operation
     */
    private void loadLastModificationDate(final EmbargoOperation embargoOperation) {
        embargoOperation.setLastModificationDate(new Date());
    }

    /**
     * Load embargo direction flags.
     *
     * @param embargoOperation the embargo operation
     */
    private void loadEmbargoDirectionFlags(final EmbargoOperation embargoOperation) {
        final String cellarId = embargoOperation.getObjectId();
        final Date embargoDate = embargoOperation.getEmbargoDate();
        embargoOperation.setStoredInPrivateDatabase(this.embargoDatabaseService.savedOnlyPrivate(cellarId));
        embargoOperation.setToBeStoredInPrivateDatabase(TimeUtils.isFutureDate(embargoDate));
    }

    /**
     * Load cellar resource.
     *
     * @param embargoOperation the embargo operation
     */
    private void loadCellarResource(final EmbargoOperation embargoOperation) {
        final String uriOrPrefix = embargoOperation.getObjectId();
        try {
            // retrieve resource
            final String cellarId = this.identifierService.getCellarPrefixed(uriOrPrefix);
            final CellarResource cellarResource = this.cellarResourceDao.findCellarId(cellarId);
            embargoOperation.setCellarResource(cellarResource);
            embargoOperation.setObjectId(cellarId);
        } catch (final IllegalArgumentException e) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withCode(CmrErrors.EMBARGO_ILLEGAL_IDENTIFIER)
                    .withCause(e)
                    .withMessage("The given identifier '{}' is not used in the application")
                    .withMessageArgs(uriOrPrefix)
                    .root(true)
                    .build();
        }
    }

    /**
     * Update date cellar resource.
     *
     * @param cellarIdentifier     the cellar identifier
     * @param embargoDate          the embargo date
     * @param lastModificationDate the last modification date
     */
    protected void updateDateCellarResource(final Identifier cellarIdentifier, final Date embargoDate, final Date lastModificationDate) {
        final CellarResource cellarResource = this.cellarResourceDao.findResourceWithEmbeddedNotice(cellarIdentifier.getCellarId());
        cellarResource.setEmbargoDate(embargoDate);
        cellarResource.setLastModificationDate(lastModificationDate);
        this.cellarResourceDao.saveOrUpdateObject(cellarResource);
    }

    /**
     * Load content identifier.
     *
     * @param embargoOperation the embargo operation
     */
    protected void loadContentIdentifier(EmbargoOperation embargoOperation) {
        final ContentIdentifier contentIdentifier = new ContentIdentifier(embargoOperation.getObjectId());
        embargoOperation.setContentIdentifier(contentIdentifier);
    }

    /**
     * Update indexes.
     *
     * @param cellarWorkId                the cellar work id
     * @param wasStoredInPrivateDatabase  the was stored in private database
     * @param toBeStoredInPrivateDatabase the to be stored in private database
     */
    protected void updateIndexes(final ContentIdentifier cellarWorkId, final boolean wasStoredInPrivateDatabase,
                                 final boolean toBeStoredInPrivateDatabase) {
        final Model dmdIndex = null;
        try {
            CmrIndexRequest.Action action;
            if (toBeStoredInPrivateDatabase && wasStoredInPrivateDatabase) {
                return; // no update of indexes
            } else if (toBeStoredInPrivateDatabase) {
                action = CmrIndexRequest.Action.Remove;
            } else if (wasStoredInPrivateDatabase) {
                action = CmrIndexRequest.Action.Create;
            } else {
                action = CmrIndexRequest.Action.Update;
            }

            this.cmrIndexRequestGenerationService.insertIndexRequestsFromEmbargoChange(cellarWorkId, dmdIndex, action);
        } finally {
            JenaUtils.closeQuietly(dmdIndex);
        }
    }

}
