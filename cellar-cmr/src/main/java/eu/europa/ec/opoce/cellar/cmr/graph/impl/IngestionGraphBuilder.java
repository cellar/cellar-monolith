/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.graph.impl
 *             FILE : IngestionGraphBuilder.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 5, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.graph.impl;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Selector;
import org.apache.jena.rdf.model.SimpleSelector;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.PrefixConfigurationService;
import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;
import eu.europa.ec.opoce.cellar.cmr.ingest.IMetadataLoaderService;
import eu.europa.ec.opoce.cellar.common.util.CellarIdUtils;
import eu.europa.ec.opoce.cellar.common.util.DigitalObjectUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsPackage;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.exception.CellarValidationException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page. <br/>
 * <br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc. <br/>
 * <br/>
 * ON : Jun 5, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Configurable(autowire = Autowire.BY_TYPE, dependencyCheck = true)
public abstract class IngestionGraphBuilder extends GraphBuilder {

	/**
	 * Metadata loader service.
	 */
	@Autowired
	private IMetadataLoaderService metadataLoaderService;

	/**
	 * Prefix configuration service.
	 */
	@Autowired
	private PrefixConfigurationService prefixConfigurationService;

	/**
	 * Direct relations untreated.
	 */
	private List<String> directRelations;

	/**
	 * Metadata model used to retrieve the direct relations.
	 */
	private Model metadataModel;

	/**
	 * METS package used to build the graph.
	 */
	private MetsPackage metsPackage;

	/**
	 * Struct map used to build the graph.
	 */
	private StructMap structMap;

	/**
	 * Prefix uris known by Cellar.
	 */
	private String[] cellarPrefixUris;

	@PostConstruct
	private void init() {
		this.cellarPrefixUris = this.prefixConfigurationService.getPrefixConfiguration().getPrefixUris().values()
				.toArray(new String[0]);
	}

	/**
	 * Add the METS package to the builder.
	 * 
	 * @param metsPackage
	 *            the METS package used to build the graph
	 * @return the graph builder
	 */
	public IngestionGraphBuilder withMetsPackage(final MetsPackage metsPackage) {
		this.metsPackage = metsPackage;
		return this;
	}

	/**
	 * Add the struct map to the builder.
	 * 
	 * @param structMap
	 *            the struct map used to build the graph
	 * @return the graph builder
	 */
	public IngestionGraphBuilder withStructMap(final StructMap structMap) {
		this.structMap = structMap;
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IngestionGraphBuilder initialize() {
		super.initialize();
		this.directRelations = new LinkedList<String>();
		if (this.isLoadMetadataModel() && this.metsPackage != null && this.structMap != null) {
			this.metadataModel = this.metadataLoaderService.loadMetadata(this.metsPackage, this.structMap, false);
		}

		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IngestionGraphBuilder buildBase() {
		this.extractBase(this.structMap.getDigitalObject());
		return this;
	}

	/**
	 * Build the graph direct relations, i.e.: the relations given in the SIP
	 * package (where are only direct relations present)
	 * 
	 * @return the graph builder
	 */
	public IngestionGraphBuilder buildDirectRelations() {
		if (this.directRelations.isEmpty()) { // check if the direct relations
												// are already extracted
			this.extractDirectRelations(); // extract the direct relations
		}

		Identifier identifier = null;
		for (final String directRelation : this.directRelations) {
			identifier = this.constructIdentifier(directRelation);
			this.getGraph().addRelation(identifier);
		}

		return this;
	}

	public void closeQuietly() {
		JenaUtils.closeQuietly(this.metadataModel);
	}

	/**
	 * It defines if the builder is meant to load the metadata model.
	 * 
	 * @return a boolean with the result of this check
	 */
	protected boolean isLoadMetadataModel() {
		return true;
	}

	protected void checkUnknownPids(final DigitalObject digitalObject, final List<String> productionIdentifiers) {
		// check if it is needed to alter the digital object
		if (digitalObject.getBusinessMetadata() == null && digitalObject.getTechnicalMetadata() == null) {
			final Collection<String> unknownPids = this.identifierService
					.getUnknownProductionIdentifers(productionIdentifiers);

			if (!unknownPids.isEmpty() && unknownPids.size() < productionIdentifiers.size()) { // special
																								// case
																								// for
																								// create-append:
																								// the
																								// unknownPids
																								// are
																								// in
																								// line
																								// with
																								// productionIdentifiers
				throw ExceptionBuilder.get(CellarValidationException.class).withCode(CmrErrors.VALIDATION_HIERARCHY)
						.withMessage("The digital object identified by {} cannot be enriched with new PID(s) {}")
						.withMessageArgs(CollectionUtils.subtract(productionIdentifiers, unknownPids), unknownPids)
						.build();
			}
		}
	}

	private void extractBase(final DigitalObject digitalObject) {
		final List<String> productionIdentifiers = DigitalObjectUtils.extractProductionIdentifiers(digitalObject);

		this.checkUnknownPids(digitalObject, productionIdentifiers);

		final Identifier identifier = this.constructBaseIdentifier(productionIdentifiers);
		this.getGraph().addObject(identifier);

		for (final DigitalObject child : digitalObject.getChildObjects()) {
			this.extractBase(child);
		}
	}

	private void extractDirectRelations() {
		final Selector selector = new SimpleSelector() {

			@Override
			public boolean selects(final Statement s) {
				final Resource subject = s.getSubject();
				final RDFNode object = s.getObject();

				if (object.isURIResource()) { // the object must be an URI
					final String uriObject = object.asResource().getURI();
					final String uriSubject = subject.getURI();

					// the object URI and the subject URI must start by one of
					// the Cellar prefix URIs
					if (StringUtils.startsWithAny(uriSubject, cellarPrefixUris)
							&& StringUtils.startsWithAny(uriObject, cellarPrefixUris)) {
						return true;
					}
				}

				return false;
			}
		};

		String uriObject = null, prefixObject = null, prefixSubject = null;
		Statement statement = null;
		final StmtIterator it = this.metadataModel.listStatements(selector);
		try {
			while (it.hasNext()) {
				statement = it.next();

				uriObject = statement.getObject().asResource().getURI();
				prefixObject = this.identifierService.getPrefixedFromUri(uriObject); // get
				// the
				// prefix

				this.directRelations.add(prefixObject);

				prefixSubject = this.identifierService.getPrefixedFromUri(statement.getSubject().getURI()); // get
				// the
				// prefix

				this.directRelations.add(prefixSubject);
			}
		} finally {
			it.close();
		}
	}

	private Identifier constructBaseIdentifier(final List<String> pids) {
		final Set<String> cellarIds = this.identifierService.getCellarUUIDs(pids);

		// adds the cellar ids predefined in the METS package
		// those cellar ids take advantage of the CC cellar id optimization
		cellarIds.addAll(CellarIdUtils.getCellarIds(pids));

		if (cellarIds.size() > 1) {
			// ERROR: multiple cellar ids
			throw ExceptionBuilder.get(CellarValidationException.class).withCode(CmrErrors.VALIDATION_HIERARCHY)
					.withMessage("Multiple cellar ids for resource identified by {}.").withMessageArgs(pids).build();
		}

		Identifier identifier = null;
		String cellarId = null;
		final Iterator<String> iterator = cellarIds.iterator();
		if (iterator.hasNext()) { // cellar id exists

			cellarId = iterator.next();
			final String cellarIdBase = CellarIdUtils.getCellarIdBase(cellarId);

			final Identifier root = this.getGraph().getRoot();

			if (root != null && !cellarIdBase.equals(root.getCellarId())) {
				// ERROR: not match cellar id
				throw ExceptionBuilder.get(CellarValidationException.class).withCode(CmrErrors.VALIDATION_HIERARCHY)
						.withMessage(
								"The existing cellar id for resource identified by {} do not match with the root cellar id.")
						.withMessageArgs(pids).build();
			}

			if (!cellarIdBase.equals(cellarId)) {
				identifier = new Identifier(cellarIdBase, null);

				return identifier;
			}
		}

		return new Identifier(cellarId, pids);
	}
}
