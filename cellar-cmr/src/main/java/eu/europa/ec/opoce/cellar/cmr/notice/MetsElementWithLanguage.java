/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice
 *             FILE : MetsElementWithLanguage.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 24, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.notice;

import eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 24, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class MetsElementWithLanguage {

    private LanguageBean language;
    private MetsElement metsElement;

    public LanguageBean getLanguage() {
        return this.language;
    }

    public MetsElement getMetsElement() {
        return metsElement;
    }

    public MetsElementWithLanguage(MetsElement metsElement, LanguageBean language) {
        this.metsElement = metsElement;
        this.language = language;
    }
}
