/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cmr.indexing.service
 *        FILE : RedundantIndexRequestHandler.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 28-04-2022
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2022 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.indexing.service;

/**
 * Service for identifying and handling redundant indexing requests.
 * @author EUROPEAN DYNAMICS S.A.
 */
public interface RedundantIndexRequestHandler {

	/**
     * Detects and marks an indexing request having status 'New' as 'Redundant' i.e. not to be executed,
     * when a similar request (same OBJECT_URI and REQUEST_TYPE but not necessarily ACTION)
     * having status 'Pending' (submitted for execution) has already been submitted for indexing.
     * The request's PRIORITY is also part of the redundancy assessment criteria.
     */
	void detectAndUpdateRedundantRequests();
	
}
