/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice.embedded.impl
 *             FILE : EmbeddedNoticeServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Sep 22, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.notice.embedded.impl;

import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.CellarIdentifierResourceService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.IndexationCalc;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.database.dao.IndexationCalcDao;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement;
import eu.europa.ec.opoce.cellar.cmr.notice.NoticeBuilder;
import eu.europa.ec.opoce.cellar.cmr.notice.NoticeBuilderServiceFactory;
import eu.europa.ec.opoce.cellar.cmr.notice.NoticeCache;
import eu.europa.ec.opoce.cellar.cmr.notice.core.NoticeType;
import eu.europa.ec.opoce.cellar.cmr.notice.embedded.EmbeddedNoticeService;
import eu.europa.ec.opoce.cellar.common.util.CellarIdUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.CellarIdentifiedObject;
import eu.europa.ec.opoce.cellar.monitoring.Watch;
import eu.europa.ec.opoce.cellar.ontology.service.OntologyService;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.semantic.xml.DomUtils;
import eu.europa.ec.opoce.cellar.semantic.xml.OutputKeysBuilder;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.riot.Lang;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;

import java.io.StringWriter;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Sep 22, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service("embeddedNoticeService")
public class EmbeddedNoticeServiceImpl implements EmbeddedNoticeService {

    private static final Logger LOG = LogManager.getLogger(EmbeddedNoticeServiceImpl.class);

    private final NoticeBuilderServiceFactory noticeBuilderServiceFactory;
    private final OntologyService ontologyService;
    private final CellarResourceDao cellarResourceDao;
    private final IdentifierService identifierService;
    private final CellarIdentifierResourceService cellarIdentifierResourceService;
    private final ContentStreamService contentStreamService;
    private final IndexationCalcDao indexationCalcDao;

    @Autowired
    public EmbeddedNoticeServiceImpl(NoticeBuilderServiceFactory noticeBuilderServiceFactory, OntologyService ontologyService,
                                     CellarResourceDao cellarResourceDao, @Qualifier("pidManagerService") IdentifierService identifierService,
                                     CellarIdentifierResourceService cellarIdentifierResourceService,
                                     ContentStreamService contentStreamService, IndexationCalcDao indexationCalcDao) {
        this.noticeBuilderServiceFactory = noticeBuilderServiceFactory;
        this.ontologyService = ontologyService;
        this.cellarResourceDao = cellarResourceDao;
        this.identifierService = identifierService;
        this.cellarIdentifierResourceService = cellarIdentifierResourceService;
        this.contentStreamService = contentStreamService;
        this.indexationCalcDao = indexationCalcDao;
    }

    @Watch(value = "EmbeddedNotice", arguments = {
            @Watch.WatchArgument(name = "Cellar ID", expression = "cellarId")
    })
    @Override
    public void refreshTreeEmbeddedNotices(final String cellarId) {
        final List<CellarResource> cellarResources = this.cellarResourceDao.findTreeMetadataSortByCellarId(cellarId);

        for (final CellarResource cellarResource : cellarResources) {
            final MetsElement element = this.cellarIdentifierResourceService.getElement(cellarResource.getCellarId());
            Map<ContentType, String> versions = new EnumMap<>(ContentType.class);
            versions.put(ContentType.DIRECT_INFERRED, getVersion(cellarResource, ContentType.DIRECT_INFERRED));
            versions.put(ContentType.DECODING, getVersion(cellarResource, ContentType.DECODING));
            final Map<ContentType, String> metadata = contentStreamService.getContentsAsString(cellarResource.getCellarId(), versions);

            Model dmdIndex = null;
            Model decoding = null;
            try {
                dmdIndex = JenaUtils.read(metadata.get(ContentType.DIRECT_INFERRED), Lang.NT);

                String decodingModel = metadata.get(ContentType.DECODING);
                if (decodingModel != null && !decodingModel.isEmpty()) {
                    decoding = JenaUtils.read(decodingModel, Lang.NT);
                } else {
                    LOG.debug("Decoding model is empty for {}", cellarId);
                    decoding = ModelFactory.createDefaultModel();
                }

                this.refreshEmbeddedNotice(cellarResource, element, this.getAllUris(cellarResource), dmdIndex, decoding);
                
                IndexationCalc indexationCalc = new IndexationCalc(cellarResource.getCellarId(),
                		CellarIdUtils.getCellarIdBase(cellarResource.getCellarId()), cellarResource.getEmbeddedNotice());
                this.indexationCalcDao.saveObject(indexationCalc);
            } finally {
                JenaUtils.closeQuietly(dmdIndex, decoding);
            }
        }
    }

    private static String getVersion(CellarResource resource, ContentType contentType) {
        return resource.getVersion(contentType).orElse(null);
    }

    private Set<String> getAllUris(final CellarResource cellarResource) {
        final Set<String> uris = new HashSet<>();

        uris.add(this.identifierService.getUri(cellarResource.getCellarId()));
        for (final String pid : this.identifierService.getIdentifier(cellarResource.getCellarId()).getPids()) {
            uris.add(this.identifierService.getUri(pid));
        }

        return uris;
    }

    @Override
    public void refreshEmbeddedNotice(final CellarResource cellarResource, final CellarIdentifiedObject cellarIdentifiedObject,
                                      final Set<String> uris, final Model dmdIndex, final Model decoding) {
        if (this.isEmbeddedNoticeEligible(uris, dmdIndex)) {
            //calculate embedded notice
            final Document embeddedNoticeDocument = createEmbeddedNotice(cellarIdentifiedObject, dmdIndex, decoding);

            //write notice to string
            final StringWriter embeddedNotice = new StringWriter();
            DomUtils.write(embeddedNoticeDocument, embeddedNotice,
                    new OutputKeysBuilder().setOmitXmlDeclaration(OutputKeysBuilder.OmitXmlDeclaration.no));

            // put notice in S3
//            byte[] notice = embeddedNotice.toString().getBytes(StandardCharsets.UTF_8);
//            String newVersion = contentStreamService.writeContent(cellarResource.getCellarId(), new ByteArrayResource(notice), ContentType.EMBEDDED_NOTICE);
//            cellarResource.putVersion(ContentType.EMBEDDED_NOTICE, newVersion);

            //put notice in cellar resource
            cellarResource.setEmbeddedNotice(embeddedNotice.toString());
        } else {
            cellarResource.setEmbeddedNotice("");
        }

        cellarResource.setEmbeddedNoticeCreationDate(new Date());
    }

    private boolean isEmbeddedNoticeEligible(final Set<String> uris, final Model dataModel) {
        for (final String embeddedProperty : ontologyService.getWrapper().getOntologyData().getEmbeddedProperties()) {
            final Property property = ResourceFactory.createProperty(embeddedProperty);
            final ResIterator resIterator = dataModel.listResourcesWithProperty(property);
            try {
                while (resIterator.hasNext()) {
                    final Resource next = resIterator.next();
                    for (final String uri : uris) {
                        if (uri.equals(next.getURI())) {
                            return true;
                        }
                    }
                }
            } finally {
                resIterator.close();
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Document createEmbeddedNotice(final CellarIdentifiedObject object, final Model metadataModel, final Model decodingModel) {
        final NoticeBuilder builder = noticeBuilderServiceFactory.getNewNoticeBuilderInstance(NoticeType.embedded)
                .withCache(new NoticeCache()).withCloseCache(true).withLanguageIndependentModel(metadataModel)
                .withDecodingModel(decodingModel);
        try {
            builder.withObject(object).buildEmbedded();
            // the map accepts null as key, null will be the language independent key
            return builder.getDocument(null);
        } finally {
            builder.close();
        }
    }
}
