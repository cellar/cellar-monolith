package eu.europa.ec.opoce.cellar.ontology.service.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration;
import eu.europa.ec.opoce.cellar.cmr.database.dao.OntoConfigDao;
import eu.europa.ec.opoce.cellar.common.util.MapUtils;
import eu.europa.ec.opoce.cellar.ontology.dao.OntoConfig;
import eu.europa.ec.opoce.cellar.ontology.exception.OntologyException;
import eu.europa.ec.opoce.cellar.ontology.service.OntoConfigService;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OntoConfigServiceImpl implements OntoConfigService {

    private static final Logger LOG = LogManager.getLogger(OntoConfigServiceImpl.class);

    private final OntoConfigDao ontoConfigDao;

    @Autowired
    public OntoConfigServiceImpl(OntoConfigDao ontoConfigDao) {
        this.ontoConfigDao = ontoConfigDao;
    }

    @PostConstruct
    private void init() {
        Map<String, OntoConfig> settings = reload();
        for (Map.Entry<String, OntoConfig> onto : settings.entrySet()) {
            OntoConfig activeConfig = onto.getValue();
            if (activeConfig == null) {
                LOG.warn(ICellarConfiguration.CONFIG, "Ontology '{}' not configured!", onto);
            } else {
                LOG.debug(IConfiguration.ONTOLOGY, "Used configuration for ontology '{}': [{}]", onto.getKey(), activeConfig);
            }
        }
    }

    @Override
    public synchronized void reconfigure() {
        init();
    }

    private Map<String, OntoConfig> reload() {
        Collection<OntoConfig> ontologyConfigs = ontoConfigDao.findAll();

        Map<String, List<OntoConfig>> allPerOnto = new HashMap<>();
        Map<String, List<OntoConfig>> activesPerOnto = new HashMap<>();

        for (OntoConfig config : ontologyConfigs) {
            if (StringUtils.isBlank(config.getUri())) {
                throw new OntologyException("URI is not defined for ontology config: [" + config + "]");
            }

            MapUtils.addValueToMappedList(allPerOnto, config.getUri(), config);
            MapUtils.addValueToMappedList(activesPerOnto, config.getUri(), config);
        }

        final Map<String, OntoConfig> activePerOnto = new HashMap<>(allPerOnto.size());
        for (Map.Entry<String, List<OntoConfig>> onto : allPerOnto.entrySet()) {
            List<OntoConfig> actives = onto.getValue();
            if (actives.size() > 1) {
                throw new OntologyException("More then one onto configuration found for ontology " +
                        "[" + onto + "]: " + actives);
            }
            activePerOnto.put(onto.getKey(), actives.isEmpty() ? null : actives.get(0));
        }
        return activePerOnto;
    }
}