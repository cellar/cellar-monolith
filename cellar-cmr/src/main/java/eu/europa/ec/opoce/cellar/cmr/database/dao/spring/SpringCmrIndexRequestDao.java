package eu.europa.ec.opoce.cellar.cmr.database.dao.spring;

import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService.Priority;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CmrIndexRequestDao;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.ExecutionStatus;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.Reason;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.RequestType;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.semantic.helper.StringHelper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * <p>SpringCmrIndexRequestDao class.</p>
 */
@Repository
public class SpringCmrIndexRequestDao extends CmrSpringBaseDao<CmrIndexRequest, Long>
        implements QueryAndTableCmrIndexRequest, CmrIndexRequestDao {

    private static final Logger LOG = LoggerFactory.getLogger(SpringCmrIndexRequestDao.class);

    private static final String UPDATE_EXECUTION_STATUS = "UPDATE CMR_INDEX_REQUEST SET EXECUTION_STATUS = :newStatus WHERE EXECUTION_STATUS IN (:oldStatus)";
    private static final String DELETE_INDEX_REQUEST = "DELETE FROM CMR_INDEX_REQUEST WHERE EXECUTION_STATUS IN (:status) AND EXECUTION_DATE <= :maxExecutionDate";

    /**
     * <p>Constructor for SpringCmrIndexRequestDao.</p>
     */
    public SpringCmrIndexRequestDao() {
        super(TABLE_NAME,
                Arrays.asList( //
                        Column.OBJECT_URI, //
                        Column.GROUP_BY_URI, //
                        Column.CREATED_ON, //
                        Column.METS_DOC, //
                        Column.STRUCT_MAP_ID, //
                        Column.CONCEPT_SCHEME, //
                        Column.REASON, //
                        Column.REQUESTED_BY, //
                        Column.PRIORITY, //
                        Column.ACTION, //
                        Column.OBJECT_TYPE, //
                        Column.REQUEST_TYPE, //
                        Column.EXECUTION_STATUS, //
                        Column.EXECUTION_DATE, //
                        Column.EXECUTION_START_DATE,
                        Column.STATUS_CAUSE,
                        Column.STRUCTMAP_STATUS_HISTORY_ID));
    }

    @Override
    public long countByExecutionStatus(final ExecutionStatus executionStatus) {
        return this.count(WHERE_PART_EXECUTION_STATUS, new HashMap<String, Object>(1) {

            private static final long serialVersionUID = 1L;

            {
                put("executionStatus", executionStatus.getValue());
            }
        });
    }

    @Override
    public long countByReasonAndExecutionStatus(final Reason reason, final ExecutionStatus... executionStatuses) {

        final List<String> executionStatusValues = Arrays.asList(executionStatuses).stream()
                .map(executionStatus -> executionStatus.getValue()).collect(Collectors.toList());

        return this.count(WHERE_PART_REASON_EXECUTION_STATUS, new HashMap<String, Object>(1) {

            private static final long serialVersionUID = 1L;

            {
                put("executionStatuses", executionStatusValues);
                put("reason", reason.getValue());
            }
        });

    }

    @Override
    public long countByNotExecutionStatusAndNotRequestType(final RequestType requestType, final ExecutionStatus... executionStatuses) {
        
    	final List<String> executionStatusValues = Arrays.asList(executionStatuses).stream()
                .map(executionStatus -> executionStatus.getValue()).collect(Collectors.toList());
    	
    	return this.count(WHERE_EXECUTION_STATUS_NOT_IN_AND_NOT_REQUEST_TYPE, new HashMap<String, Object>(2) {

            private static final long serialVersionUID = 1L;

            {
                put("executionStatus", executionStatusValues);
                put("requestType", requestType.getValue());
            }
        });
    }

    @Override
    public CmrIndexRequest create(final ResultSet rs) throws SQLException {
        CmrIndexRequest indexRequest = new CmrIndexRequest();
        indexRequest.setObjectUri(rs.getString(Column.OBJECT_URI));
        indexRequest.setGroupByUri(rs.getString(Column.GROUP_BY_URI));
        indexRequest.setCreatedOn(rs.getTimestamp(Column.CREATED_ON));
        indexRequest.setMetsDocumentId(rs.getString(Column.METS_DOC));
        indexRequest.setStructmapId(rs.getString(Column.STRUCT_MAP_ID));
        indexRequest.setConceptScheme(rs.getString(Column.CONCEPT_SCHEME));
        indexRequest.setReason(Reason.findByValue(rs.getString(Column.REASON)));
        indexRequest.setRequestedBy(rs.getString(Column.REQUESTED_BY));
        indexRequest.setPriority(Priority.findByPriorityValue(rs.getInt(Column.PRIORITY)));
        indexRequest.setAction(Enum.valueOf(CmrIndexRequest.Action.class, rs.getString(Column.ACTION)));
        indexRequest.setObjectType(DigitalObjectType.getByValue(rs.getString(Column.OBJECT_TYPE)));
        indexRequest.setRequestType(RequestType.findByValue(rs.getString(Column.REQUEST_TYPE)));
        indexRequest.setExecutionStatus(ExecutionStatus.findByValue(rs.getString(Column.EXECUTION_STATUS)));
        indexRequest.setExecutionDate(rs.getTimestamp(Column.EXECUTION_DATE));
        indexRequest.setExecutionStartDate(rs.getTimestamp(Column.EXECUTION_START_DATE));
        indexRequest.setStatusCause(rs.getString(Column.STATUS_CAUSE));
        indexRequest.setStructMapStatusHistoryId(
                rs.getLong(Column.STRUCTMAP_STATUS_HISTORY_ID) == 0 ? null : rs.getLong(Column.STRUCTMAP_STATUS_HISTORY_ID));
        return indexRequest;
    }

    @Override
    public void fillMap(final CmrIndexRequest daoObject, final Map<String, Object> map) {
        map.put(Column.OBJECT_URI, daoObject.getObjectUri());
        map.put(Column.GROUP_BY_URI, daoObject.getGroupByUri());
        map.put(Column.CREATED_ON, daoObject.getCreatedOn());
        map.put(Column.METS_DOC, daoObject.getMetsDocumentId());
        map.put(Column.STRUCT_MAP_ID, daoObject.getStructmapId());
        map.put(Column.CONCEPT_SCHEME, daoObject.getConceptScheme());
        map.put(Column.REASON, daoObject.getReason().getValue());
        map.put(Column.REQUESTED_BY, daoObject.getRequestedBy());
        map.put(Column.PRIORITY, daoObject.getPriority().getPriorityValue());
        map.put(Column.ACTION, daoObject.getAction().name());
        map.put(Column.OBJECT_TYPE, daoObject.getObjectType().toString());
        map.put(Column.REQUEST_TYPE, daoObject.getRequestType().getValue());
        map.put(Column.EXECUTION_STATUS, daoObject.getExecutionStatus().getValue());
        map.put(Column.EXECUTION_DATE, daoObject.getExecutionDate());
        map.put(Column.EXECUTION_START_DATE, daoObject.getExecutionStartDate());
        map.put(Column.STATUS_CAUSE, daoObject.getStatusCause());
        map.put(Column.STRUCTMAP_STATUS_HISTORY_ID, daoObject.getStructMapStatusHistoryId());
    }

    @Override
    public Collection<CmrIndexRequest> findByExecutionStatus(final ExecutionStatus executionStatus1,
                                                             final ExecutionStatus executionStatus2) {
        return this.find(WHERE_PART_2_EXECUTION_STATUS, new HashMap<String, Object>(2) {

            private static final long serialVersionUID = 1L;

            {
                put("executionStatus1", executionStatus1.getValue());
                put("executionStatus2", executionStatus2.getValue());
            }
        });
    }

    @Override
    public Collection<CmrIndexRequest> findByExecutionStatus(final ExecutionStatus executionStatus, final int batchSize) {

        return this.find(WHERE_PART_EXECUTION_STATUS_LIMIT, new HashMap<String, Object>(2) {

            private static final long serialVersionUID = 1L;

            {
                put("executionStatus", executionStatus.getValue());
                put("limit", batchSize);
            }
        });
    }

    @Override
    public Collection<CmrIndexRequest> findByExecutionStatusAndGroupUriAndMinPriorityAndMaxCreatedOn(final ExecutionStatus executionStatus,
                                                                                                     final String groupByUri, final Priority minimumPriority, final Date maximumCreatedOn) {
        return this.find(WHERE_PART_EXECUTION_STATUS_AND_GROUP_BY_URI_AND_MIN_PRIORITY_AND_MAX_CREATED_ON, new HashMap<String, Object>(4) {

            private static final long serialVersionUID = 1L;

            {
                put("executionStatus", executionStatus.getValue());
                put("groupByUri", groupByUri);
                put("minimumPriority", minimumPriority.getPriorityValue());
                put("maximumCreatedOn", maximumCreatedOn);
            }
        });
    }

    @Override
    public Collection<CmrIndexRequest> findByExecutionStatusAndMaxExecutionDate(final ExecutionStatus executionStatus,
                                                                                final Date maxExecutionDate, final int batchSize) {
        return this.find(WHERE_PART_EXECUTION_STATUS_MAX_EXECUTION_DATE_LIMIT, new HashMap<String, Object>(3) {

            private static final long serialVersionUID = 1L;

            {
                put("executionStatus", executionStatus.getValue());
                put("maximumExecutionDate", maxExecutionDate);
                put("limit", batchSize);
            }
        });
    }

    @Override
    public List<CmrIndexRequest> getProcessingIndexationRequestGroupedByCellarGroup() {
        //final String query = "SELECT GROUP_BY_URI,MIN(EXECUTION_START_DATE),MAX(PRIORITY) FROM CMR_INDEX_REQUEST WHERE EXECUTION_STATUS='X' GROUP BY GROUP_BY_URI";
        final Object[] params = new Object[]{
                Column.GROUP_BY_URI, Column.EXECUTION_START_DATE, Column.PRIORITY, TABLE_NAME, Column.EXECUTION_STATUS,
                ExecutionStatus.Execution.getValue(), Column.GROUP_BY_URI};
        final String query = StringHelper.format("SELECT {},MIN({}),MAX({}) FROM {} WHERE {}='{}' GROUP BY {}", params);

        return getJdbcTemplate().query(query, (rs, rowNum) -> {
            final CmrIndexRequest result1 = new CmrIndexRequest();
            final String groupUri = rs.getString(1);
            final String cellarId = StringUtils.substringAfterLast(groupUri, "/");
            result1.setGroupByUri("cellar:" + cellarId);
            result1.setExecutionStartDate(rs.getTimestamp(2));
            result1.setPriority(rs.getInt(3));
            return result1;
        });
    }

    @Override
    public List<CmrIndexRequest> getWaitingIndexationRequestGroupedByCellarGroup() {
        //final String query = "SELECT GROUP_BY_URI,MIN(CREATED_ON),MAX(PRIORITY) FROM CMR_INDEX_REQUEST WHERE (EXECUTION_STATUS='P' or EXECUTION_STATUS='N') GROUP BY GROUP_BY_URI";
        final Object[] params = new Object[]{
                Column.GROUP_BY_URI, Column.CREATED_ON, Column.PRIORITY, TABLE_NAME, Column.EXECUTION_STATUS,
                ExecutionStatus.Pending.getValue(), Column.EXECUTION_STATUS, ExecutionStatus.New.getValue(), Column.GROUP_BY_URI};
        final String query = StringHelper.format("SELECT {},MIN({}),MAX({}) FROM {} WHERE ({}='{}' OR {}='{}') GROUP BY {}", params);

        return getJdbcTemplate().query(query, (rs, rowNum) -> {
            final CmrIndexRequest result1 = new CmrIndexRequest();
            final String groupUri = rs.getString(1);
            final String cellarId = StringUtils.substringAfterLast(groupUri, "/");
            result1.setGroupByUri("cellar:" + cellarId);
            result1.setCreatedOn(rs.getTimestamp(2));
            result1.setPriority(rs.getInt(3));
            return result1;
        });
    }

    @Override
    public int updateExecutionStatus(final ExecutionStatus newStatus, final ExecutionStatus... oldStatus) {
        final MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("newStatus", newStatus.getValue());
        params.addValue("oldStatus", Stream.of(oldStatus).map(ExecutionStatus::getValue).collect(Collectors.toList()));
        try {
            return getNamedParameterJdbcTemplate().update(UPDATE_EXECUTION_STATUS, params);
        } catch (DataAccessException e) {
            LOG.error("Impossible to update CMR_INDEX_REQUEST with the status " + newStatus, e);
            return -1;
        }
    }

    @Override
    public int deleteIndexRequest(Date maxExecutionDate, ExecutionStatus... status) {
        final MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("maxExecutionDate", maxExecutionDate);
        params.addValue("status", Stream.of(status).map(ExecutionStatus::getValue).collect(Collectors.toList()));
        try {
            return getNamedParameterJdbcTemplate().update(DELETE_INDEX_REQUEST, params);
        } catch (DataAccessException e) {
            LOG.error("Impossible to delete CMR_INDEX_REQUEST execute before " + maxExecutionDate + " with status: " +
                    Arrays.toString(status), e);
            return -1;
        }
    }
}
