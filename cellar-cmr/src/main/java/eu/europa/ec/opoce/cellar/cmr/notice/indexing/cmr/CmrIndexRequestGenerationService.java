/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr
 *             FILE : CmrIndexRequestGenerationService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 14 sept. 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr;

import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService.LinkedIndexingBehavior;
import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService.Priority;
import eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.RequestType;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;

import java.util.Collection;
import java.util.Set;

import org.apache.jena.rdf.model.Model;

/**
 * The Interface CmrIndexRequestGenerationService.
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 14 sept. 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface CmrIndexRequestGenerationService {

    /**
     * Gets the linked indexing behavior.
     *
     * @return the linked indexing behavior
     */
    LinkedIndexingBehavior getLinkedIndexingBehavior();

    /**
     * Sets the linked indexing behavior.
     *
     * @param linkedIndexingBehavior the new linked indexing behavior
     */
    void setLinkedIndexingBehavior(LinkedIndexingBehavior linkedIndexingBehavior);

    /**
     * <p>insertIndexingRequestsFromCreateIngest.</p>
     *
     * @param calculatedData a {@link eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData} object.
     */
    void insertIndexingRequestsFromCreateIngest(CalculatedData calculatedData);

    /**
     * <p>insertIndexingRequestsFromUpdateIngest.</p>
     *
     * @param calculatedData         a {@link eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData} object.
     * @param objectsWithChangedUris a {@link java.util.Set} object.
     */
    void insertIndexingRequestsFromUpdateIngest(CalculatedData calculatedData, Set<DigitalObject> objectsWithChangedUris);

    /**
     * <p>insertIndexingRequestsFromCreateIngest.</p>
     *
     * @param calculatedData a {@link eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData} object.
     * @param directAndInferredModel the direct and inferred model
     */
    void insertIndexingRequestsFromDeleteIngest(CalculatedData calculatedData, Model directAndInferredModel);

    /**
     * generate indexRequests for a change of embargo.
     *
     * @param cellarId the cellar id
     * @param directAndInferredModel corresponding model to the work that changed embargo
     * @param action                 action that needs to be executed when the indexing wil run
     */
    void insertIndexRequestsFromEmbargoChange(ContentIdentifier cellarId, Model directAndInferredModel, CmrIndexRequest.Action action);

    /**
     * Generate index request.
     *
     * @param uriOrPrefix the uri or prefix
     * @param userName the user name
     * @param types the types
     * @param priority the priority
     * @param reason the reason
     */
    void generateIndexRequest(final String uriOrPrefix, final String userName, final Collection<RequestType> types, final Priority priority,
            final CmrIndexRequest.Reason reason);

}
