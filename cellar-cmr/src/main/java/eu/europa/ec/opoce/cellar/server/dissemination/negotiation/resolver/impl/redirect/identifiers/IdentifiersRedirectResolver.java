/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.identifiers
 *             FILE : IdentifiersRetrieveResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : May 8, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.identifiers;

import eu.europa.ec.opoce.cellar.common.http.headers.IURLTokenable;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.Type;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.IDisseminationResolver;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.RedirectResolver;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;

import org.springframework.http.ResponseEntity;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : May 8, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class IdentifiersRedirectResolver extends RedirectResolver {

    public static IDisseminationResolver get(final CellarResourceBean cellarResource) {
        return new IdentifiersRedirectResolver(cellarResource);
    }

    private IdentifiersRedirectResolver(final CellarResourceBean cellarResource) {
        super(cellarResource, null, false);
    }

    @Override
    protected void initDisseminationRequest() {
        super.initDisseminationRequest();
    }

    @Override
    protected ResponseEntity<Void> doHandleDisseminationRequest() {
        return this.seeOtherService.negotiateSeeOtherForIdentifiers(this.cellarResource);
    }

    @Override
    protected void doHandleWorkDisseminationRequest() {
        //Nothing to do
    }

    @Override
    protected void doHandleExpressionDisseminationRequest() {
        //Nothing to do
    }

    @Override
    protected void doHandleManifestationDisseminationRequest() {
        //Nothing to do
    }

    @Override
    protected void doHandleItemDisseminationRequest() {
        //Nothing to do
    }

    @Override
    protected void doHandleDossierDisseminationRequest() {
        //Nothing to do
    }

    @Override
    protected void doHandleEventDisseminationRequest() {
        //Nothing to do
    }

    @Override
    protected void doHandleAgentDisseminationRequest() {
        //Nothing to do
    }

    @Override
    protected void doHandleTopLevelEventDisseminationRequest() {
        //Nothing to do
    }

    @Override
    protected IURLTokenable getTypeStructure() {
        return Type.IDENTIFIERS;
    }

}
