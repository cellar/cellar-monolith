package eu.europa.ec.opoce.cellar.cmr.notice.core;

import eu.europa.ec.opoce.cellar.semantic.xml.XmlBuilder;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;

import java.util.Set;

public interface Rdf2XmlAnnotationService {

    void addAnnotations(XmlBuilder parentBuilder, Property property, RDFNode object, Model model, Set<Resource> allSubjectResources);

    void addSameAsAnnotations(XmlBuilder parentBuilder, RDFNode object, Model model);
}
