package eu.europa.ec.opoce.cellar;

import org.apache.commons.lang.StringUtils;

public enum ExecutionStatus {

    Pending("P"),
    Success("S"),
    Failure("F");

    public static ExecutionStatus findByValue(final String value) {
        if (StringUtils.isBlank(value)) {
            return null;
        }

        for (final ExecutionStatus executionStatus : values()) {
            if (value.equals(executionStatus.value)) {
                return executionStatus;
            }
        }
        return null;
    }

    private final String value;

    ExecutionStatus(final String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
