/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.ingest
 *             FILE : IStructMapLoader.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 07-01-2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.ingest;

import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;

/**
 * <class_description> Definition of the service for loading existing data.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 07-01-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IStructMapLoader {

    /**
     * It enriches {@link data} with existing data.
     * It returns the existing {@link StructMap}.
     *
     * @param data a {@link CalculatedData} object.
     * @return a {@link StructMap} object.
     */
    public StructMap load(final CalculatedData data);

    /**
     * It enriches {@link data} with existing data: it is supposes that metadata are updated.<br>
     * It returns the existing {@link StructMap}.
     *
     * @param data a {@link CalculatedData} object.
     * @return the existing {@link StructMap} object.
     */
    public StructMap loadAndMerge(final CalculatedData data);

    /**
     * It enriches {@link data} with existing data: it is supposes that metadata are created.<br>
     * It returns the existing {@link StructMap}.
     *
     * @param data a {@link CalculatedData} object.
     * @return a {@link StructMap} object.
     */
    public StructMap loadAndCombine(final CalculatedData data);

    /**
     * It cleans {@link data}: it is supposed that metadata is deleted.<br>
     * It returns the new "virtual" {@link StructMap}.
     *
     * @param data a {@link CalculatedData} object.
     * @return a {@link StructMap} object.
     */
    public StructMap loadAndClean(final CalculatedData data);

}
