/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums
 *             FILE : DisseminationRequest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 9, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums;

import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;
import org.apache.commons.lang.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author ARHS Developments
 * @version $Revision$
 */
public enum DisseminationRequest {

    OBJECT_XML(Pattern.compile("^application/xml;notice=object;?(?:q=(?:0\\.[0-9]{1}|1\\.0))?"), Structure.OBJECT, Type.XML),
    BRANCH_XML(Pattern.compile("^application/xml;notice=branch;?(?:q=(?:0\\.[0-9]{1}|1\\.0))?"), Structure.BRANCH, Type.XML),
    TREE_XML(Pattern.compile("^application/xml;notice=tree;?(?:q=(?:0\\.[0-9]{1}|1\\.0))?"), Structure.TREE, Type.XML),
    OBJECT_RDF_FULL(Pattern.compile("^application/rdf\\+xml;?(?:q=(?:0\\.[0-9]{1}|1\\.0))?"), Structure.OBJECT, Type.RDF,
            InferenceVariance.FULL),
    OBJECT_RDF_FULL_NORMALIZED(Pattern.compile("^application/rdf\\+xml;notice=normalized?(?:q=(?:0\\.[0-9]{1}|1\\.0))?"), Structure.OBJECT, Type.RDF,
            InferenceVariance.FULL,true),
    OBJECT_RDF_NONINFERRED(Pattern.compile("^application/rdf\\+xml;notice=non-inferred;?(?:q=(?:0\\.[0-9]{1}|1\\.0))?"), Structure.OBJECT,
            Type.RDF, InferenceVariance.NONINFERRED),
    OBJECT_RDF_NONINFERRED_NORMALIZED(Pattern.compile("^application/rdf\\+xml;notice=non-inferred-normalized;?(?:q=(?:0\\.[0-9]{1}|1\\.0))?"), Structure.OBJECT,
            Type.RDF, InferenceVariance.NONINFERRED,true),
    TREE_RDF_FULL(Pattern.compile("^application/rdf\\+xml;notice=tree;?(?:q=(?:0\\.[0-9]{1}|1\\.0))?"), Structure.TREE, Type.RDF,
            InferenceVariance.FULL),
    TREE_RDF_FULL_NORMALIZED(Pattern.compile("^application/rdf\\+xml;notice=tree-normalized;?(?:q=(?:0\\.[0-9]{1}|1\\.0))?"), Structure.TREE, Type.RDF,
            InferenceVariance.FULL,true),
    TREE_RDF_NONINFERRED(Pattern.compile("^application/rdf\\+xml;notice=non-inferred-tree;?(?:q=(?:0\\.[0-9]{1}|1\\.0))?"), Structure.TREE,
            Type.RDF, InferenceVariance.NONINFERRED),
    CONTENT_STREAM_RDF(Pattern.compile("^application/rdf\\+xml;notice=content;?(?:q=(?:0\\.[0-9]{1}|1\\.0))?"), Type.CONTENT_STREAM),
    TREE_RDF_NONINFERRED_NORMALIZED(Pattern.compile("^application/rdf\\+xml;notice=non-inferred-tree-normalized;?(?:q=(?:0\\.[0-9]{1}|1\\.0))?"), Structure.TREE,
            Type.RDF, InferenceVariance.NONINFERRED,true),
    LIST(Pattern.compile("^application/list;?(?:q=(?:0\\.[0-9]{1}|1\\.0))?$|^application/list;mtype=(.+);?(?:q=(?:0\\.[0-9]{1}|1\\.0))?"),
            Type.LIST),
    ZIP(Pattern.compile("^application/zip;?(?:q=(?:0\\.[0-9]{1}|1\\.0))?$|^application/zip;mtype=(.+);?(?:q=(?:0\\.[0-9]{1}|1\\.0))?"),
            Type.ZIP),
    CONTENT_STREAM(Type.CONTENT_STREAM),
    IDENTIFIER_XML(Pattern.compile("^application/xml;notice=identifiers;?(?:q=(?:0\\.[0-9]{1}|1\\.0))?"), Type.IDENTIFIERS),
    IDENTIFIER_LINK_FORMAT(Pattern.compile("^application/link-format;?"), Type.LINK),
    ANY;

    private Type type;

    private Pattern pattern;

    private Structure structure;

    private InferenceVariance inferenceVariance;

    private boolean normalized;

    public static DisseminationRequest acceptValueOf(final String acceptHeader) {
        final String accept = acceptHeader.replaceAll("; ", ";");
        final String[] acceptHeaderSplit = accept.split(";");
        if (acceptHeaderSplit.length == 0) {
            return DisseminationRequest.ANY;
        } else {
            final String key = acceptHeaderSplit[0].trim().toLowerCase();
            if (StringUtils.isBlank(key) || key.equals("*") || key.equals("*/*")) {
                return DisseminationRequest.ANY;
            }
        }

        for (final DisseminationRequest disseminationRequest : values()) {
            final Pattern pattern = disseminationRequest.getPattern();
            if (pattern != null) {
                final Matcher matcher = pattern.matcher(accept.trim());
                if (matcher.matches()) {
                    return disseminationRequest;
                }
            }
        }

        return DisseminationRequest.CONTENT_STREAM;
    }

    /**
     * Instantiates a new dissemination request.
     */
    private DisseminationRequest() {

    }

    /**
     * Instantiates a new dissemination request.
     *
     * @param type the type
     */
    private DisseminationRequest(final Type type) {
        this.type = type;
    }

    /**
     * Instantiates a new dissemination request.
     *
     * @param mimetype the mimetype
     * @param strict   the strict
     * @param type     the type
     */
    private DisseminationRequest(final Pattern pattern, final Type type) {
        this(type);
        this.pattern = pattern;
    }

    /**
     * Instantiates a new dissemination request.
     *
     * @param mimetype  the mimetype
     * @param strict    the strict
     * @param structure the structure
     * @param type      the type
     */
    private DisseminationRequest(final Pattern pattern, final Structure structure, final Type type) {
        this(pattern, type);
        this.structure = structure;
    }

    /**
     * Instantiates a new dissemination request.
     *
     * @param mimetype          the mimetype
     * @param strict            the strict
     * @param structure         the structure
     * @param type              the type
     * @param inferenceVariance the inference variance
     */
    private DisseminationRequest(final Pattern pattern, final Structure structure, final Type type,
                                 final InferenceVariance inferenceVariance,boolean... normalized) {
        this(pattern, structure, type);
        this.inferenceVariance = inferenceVariance;
        this.normalized=normalized.length>0?true:false;
    }

    /**
     * Gets the type.
     *
     * @param cellarResource the cellar resource
     * @return the type
     */
    public Type getType(final CellarResourceBean cellarResource) {

        if (this == ANY) {
            if (cellarResource.getCellarType() == DigitalObjectType.ITEM) {
                return Type.CONTENT_STREAM; //TODO AJ ?
            } else {
                return Type.RDF;
            }
        }

        return this.type;
    }

    public Pattern getPattern() {
        return this.pattern;
    }

    /**
     * Gets the structure.
     *
     * @param cellarResource the cellar resource
     * @return the structure
     */
    public Structure getStructure(final CellarResourceBean cellarResource) {

        if (this == ANY) {
            if (cellarResource.getCellarType() != DigitalObjectType.ITEM) {
                return Structure.OBJECT;
            }
        }

        return this.structure;
    }

    /**
     * Gets the inference variance.
     *
     * @return the inferenceVariance
     */
    public InferenceVariance getInferenceVariance() {

        if (this.inferenceVariance == null) {
            return InferenceVariance.FULL;
        }

        return this.inferenceVariance;
    }

    public boolean getNormalized(){
        return this.normalized;
    }
}
