package eu.europa.ec.opoce.cellar.server.admin.sparql;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 * <p>UploadXslt class.</p>
 */
public class UploadXslt {

    private CommonsMultipartFile fileData;
    private String mimetype;
    private String folder;

    /**
     * <p>Getter for the field <code>mimetype</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getMimetype() {
        return mimetype;
    }

    /**
     * <p>Setter for the field <code>mimetype</code>.</p>
     *
     * @param mimetype a {@link java.lang.String} object.
     */
    public void setMimetype(String mimetype) {
        this.mimetype = mimetype;
    }

    /**
     * <p>Getter for the field <code>folder</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getFolder() {
        return folder;
    }

    /**
     * <p>Setter for the field <code>folder</code>.</p>
     *
     * @param folder a {@link java.lang.String} object.
     */
    public void setFolder(String folder) {
        this.folder = folder;
    }

    /**
     * <p>Getter for the field <code>fileData</code>.</p>
     *
     * @return a {@link org.springframework.web.multipart.commons.CommonsMultipartFile} object.
     */
    public CommonsMultipartFile getFileData() {
        return fileData;
    }

    /**
     * <p>Setter for the field <code>fileData</code>.</p>
     *
     * @param fileData a {@link org.springframework.web.multipart.commons.CommonsMultipartFile} object.
     */
    public void setFileData(CommonsMultipartFile fileData) {
        this.fileData = fileData;
    }
}
