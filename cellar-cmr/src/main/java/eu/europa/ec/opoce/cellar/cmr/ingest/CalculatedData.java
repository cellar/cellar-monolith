/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cmr.ingest
 *        FILE : CalculatedData.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 11-04-2013
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.ingest;

import eu.europa.ec.opoce.cellar.cl.domain.package_history.PackageHistory;
import eu.europa.ec.opoce.cellar.cl.domain.structmap_status_history.StructMapStatusHistory;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.model.IFacetedModel;
import eu.europa.ec.opoce.cellar.cmr.model.LazyFacetedModel;
import eu.europa.ec.opoce.cellar.cmr.model.ModelUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsPackage;
import eu.europa.ec.opoce.cellar.domain.content.mets.ResolvedMetsPackage;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.semantic.helper.IOHelper;
import org.apache.jena.rdf.model.Model;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 11-04-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class CalculatedData {

    @SuppressWarnings("unused")
    private static final Logger LOG = LogManager.getLogger(CalculatedData.class);

    private StructMap structMap;
    private MetsPackage metsPackage;
    private final String rootCellarId;
    private StructMapStatusHistory structMapStatusHistory;
    private final PackageHistory packageHistory;

    // unmodifiable collections
    private Map<ContentType, Model> businessMetadata;
    private Map<DigitalObject, Model> technicalMetadata;
    private List<DigitalObject> addedDigitalObjects;
    private List<DigitalObject> removedDigitalObjects;

    // modifiable collections
    private final Map<DigitalObject, Map<ContentType, Model>> metadataSnippetsForS3;
    private final Map<DigitalObject, Model> directAndInferredSnippetsForOracle;
    private final Map<DigitalObject, Model> inverseSnippetsForOracle;
    private final List<DigitalObject> objectsWithChangedEmbedded;

    // other
    private IFacetedModel existingDirectModel;
    private IFacetedModel existingDirectAndInferredModel;
    private IFacetedModel existingInverseModel;

    /**
     * <p>Constructor for CalculatedData.</p>
     *
     * @param metsPackage a {@link eu.europa.ec.opoce.cellar.domain.content.mets.MetsPackage} object.
     * @param structMap a {@link eu.europa.ec.opoce.cellar.domain.content.mets.StructMap} object.
     * @param structMapStatusHistory a {@link eu.europa.ec.opoce.cellar.cl.domain.structmap_status_history.StructMapStatusHistory} object.
     * @param packageHistory a {@link eu.europa.ec.opoce.cellar.cl.domain.package_history.PackageHistory} object.
     */
    public CalculatedData(final MetsPackage metsPackage, final StructMap structMap, final String rootCellarId,
                          final StructMapStatusHistory structMapStatusHistory, final PackageHistory packageHistory) {
        this.metsPackage = metsPackage;
        this.structMap = structMap;
        this.rootCellarId = rootCellarId;
        this.structMapStatusHistory = structMapStatusHistory;
        this.packageHistory = packageHistory;

        // unmodifiable collections
        this.businessMetadata = new HashMap<>();
        this.technicalMetadata = new HashMap<>();
        this.addedDigitalObjects = new ArrayList<>();
        this.removedDigitalObjects = new ArrayList<>();

        // modifiable collections
        this.metadataSnippetsForS3 = new HashMap<>();
        this.directAndInferredSnippetsForOracle = new HashMap<>();
        this.inverseSnippetsForOracle = new HashMap<>();
        this.objectsWithChangedEmbedded = new ArrayList<>();

        // other
        this.existingDirectModel = new LazyFacetedModel();
        this.existingDirectAndInferredModel = new LazyFacetedModel();
        this.existingInverseModel = new LazyFacetedModel();
    }

    /**
     * <p>closeQuietly closes all the models that are open. Make sure to call this after an instance of this class in not needed anymore</p>
     */
    public void closeQuietly() {
        ModelUtils.closeQuietly(this.getBusinessMetadata());
        ModelUtils.closeQuietly(this.getTechnicalMetadata());
        if (this.getMetadataSnippetsForS3() != null) {
            for (final Map<ContentType, Model> s3ContentTypeMap : this.getMetadataSnippetsForS3().values()) {
                ModelUtils.closeQuietly(s3ContentTypeMap);
            }
        }
        ModelUtils.closeQuietly(this.getDirectAndInferredSnippetsForOracle());
        ModelUtils.closeQuietly(this.getInverseSnippetsForOracle());
        this.existingDirectModel.closeQuietly();
        this.existingDirectAndInferredModel.closeQuietly();
        this.existingInverseModel.closeQuietly();
    }

    public void cloneStructMap() {
        final StructMap original = this.getStructMap();
        this.structMap = IOHelper.cloneObject(original);
    }

    public void resolveMetsPackage() {
        this.metsPackage = new ResolvedMetsPackage(this.getMetsPackage());
    }

    /**
     * <p>Getter for the field <code>structMap</code>.</p>
     *
     * @return a {@link eu.europa.ec.opoce.cellar.domain.content.mets.StructMap} object.
     */
    public StructMap getStructMap() {
        return this.structMap;
    }

    public void replaceStructMap(final StructMap structMap) {
        this.structMap = structMap;
    }

    /**
     * <p>Getter for the field <code>metsPackage</code>.</p>
     *
     * @return a {@link eu.europa.ec.opoce.cellar.domain.content.mets.MetsPackage} object.
     */
    public MetsPackage getMetsPackage() {
        return this.metsPackage;
    }

    public String getRootCellarId() {
        return this.rootCellarId;
    }

    // unmodifiable collections

    /**
     * <p>Getter for the field <code>businessMetadata</code>.</p>
     *
     * @return a {@link java.util.Map} object.
     */
    public Map<ContentType, Model> getBusinessMetadata() {
        return Collections.unmodifiableMap(this.businessMetadata);
    }

    /**
     * <p>Setter for the field <code>businessMetadata</code>.</p>
     *
     * @param businessMetadata a {@link java.util.Map} object.
     */
    public void setBusinessMetadata(final Map<ContentType, Model> businessMetadata) {
        this.businessMetadata = businessMetadata;
    }

    /**
     * <p>Getter for the field <code>technicalMetadata</code>.</p>
     *
     * @return a {@link java.util.Map} object.
     */
    public Map<DigitalObject, Model> getTechnicalMetadata() {
        return Collections.unmodifiableMap(this.technicalMetadata);
    }

    /**
     * <p>Setter for the field <code>technicalMetadata</code>.</p>
     *
     * @param technicalMetadata {@link java.util.Map} object.
     */
    public void setTechnicalMetadata(final Map<DigitalObject, Model> technicalMetadata) {
        this.technicalMetadata = technicalMetadata;
    }

    /**
     * <p>Getter for the field <code>addedDigitalObjects</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<DigitalObject> getAddedDigitalObjects() {
        return Collections.unmodifiableList(this.addedDigitalObjects);
    }

    /**
     * <p>Setter for the field <code>addedDigitalObjects</code>.</p>
     *
     * @param addedDigitalObjects {@link java.util.List} object.
     */
    public void setAddedDigitalObjects(final List<DigitalObject> addedDigitalObjects) {
        this.addedDigitalObjects = addedDigitalObjects;
    }

    /**
     * <p>Getter for the field <code>removedDigitalObjects</code>.</p>
     *
     * @return a {@link java.util.Collection} object.
     */
    public Collection<DigitalObject> getRemovedDigitalObjects() {
        return Collections.unmodifiableList(this.removedDigitalObjects);
    }

    /**
     * <p>Setter for the field <code>removedDigitalObjects</code>.</p>
     *
     * @param removedDigitalObjects {@link java.util.List} object.
     */
    public void setRemovedDigitalObjects(final List<DigitalObject> removedDigitalObjects) {
        this.removedDigitalObjects = removedDigitalObjects;
    }

    // modifiable collections

    /**
     * <p>getChangedDigitalObjects.</p>
     *
     * @return a {@link java.util.Collection} object.
     */
    public Collection<DigitalObject> getChangedDigitalObjects() {
        return this.directAndInferredSnippetsForOracle.keySet();
    }

    /**
     * <p>Getter for the field <code>metadataSnippetsForS3</code>.</p>
     *
     * @return a {@link java.util.Map} object.
     */
    public Map<DigitalObject, Map<ContentType, Model>> getMetadataSnippetsForS3() {
        return this.metadataSnippetsForS3;
    }

    /**
     * <p>Getter for the field <code>directAndInferredSnippetsForOracle</code>.</p>
     *
     * @return a {@link java.util.Map} object.
     */
    public Map<DigitalObject, Model> getDirectAndInferredSnippetsForOracle() {
        return this.directAndInferredSnippetsForOracle;
    }

    /**
     * <p>Getter for the field <code>inverseSnippetsForOracle</code>.</p>
     *
     * @return a {@link java.util.Map} object.
     */
    public Map<DigitalObject, Model> getInverseSnippetsForOracle() {
        return this.inverseSnippetsForOracle;
    }

    /**
     * <p>Getter for the field <code>objectsWithChangedEmbedded</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<DigitalObject> getObjectsWithChangedEmbedded() {
        return this.objectsWithChangedEmbedded;
    }

    public IFacetedModel getExistingDirectModel() {
        return this.existingDirectModel;
    }

    public void setExistingDirectModel(final Map<?, ?> existingDirectModels) {
        this.existingDirectModel = new LazyFacetedModel(existingDirectModels);
    }

    public IFacetedModel getExistingDirectAndInferredModel() {
        return this.existingDirectAndInferredModel;
    }

    public void setExistingDirectAndInferredModel(final Map<?, ?> existingDirectAndInferredModels) {
        this.existingDirectAndInferredModel = new LazyFacetedModel(existingDirectAndInferredModels);
    }

    public IFacetedModel getExistingInverseModel() {
        return this.existingInverseModel;
    }

    public void setExistingInverseModel(final Map<?, ?> existingInverseModels) {
        this.existingInverseModel = new LazyFacetedModel(existingInverseModels);
    }
    
    /**
     * <p>Setter for the field <code>structMapStatusHistory</code>.</p>
     *
     * @param structMapStatusHistory {@link StructMapStatusHistory} object.
     */
    public void setStructMapStatusHistory(StructMapStatusHistory structMapStatusHistory) {
        this.structMapStatusHistory = structMapStatusHistory;
    }
    
    /**
     * <p>Getter for the field <code>structMapStatusHistory</code>.</p>
     *
     * @return a {@link eu.europa.ec.opoce.cellar.cl.domain.structmap_status_history.StructMapStatusHistory} object.
     */
    public StructMapStatusHistory getStructMapStatusHistory() {
        return this.structMapStatusHistory;
    }
    
    /**
     * <p>Getter for the field <code>packageHistory</code>.</p>
     *
     * @return a {@link eu.europa.ec.opoce.cellar.cl.domain.package_history.PackageHistory} object.
     */
    public PackageHistory getPackageHistory() {
        return this.packageHistory;
    }
}
