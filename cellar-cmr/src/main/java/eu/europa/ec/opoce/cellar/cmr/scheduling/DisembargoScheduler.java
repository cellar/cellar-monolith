package eu.europa.ec.opoce.cellar.cmr.scheduling;

import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditBuilder;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventAction;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventProcess;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventType;
import eu.europa.ec.opoce.cellar.cmr.service.EmbargoService;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import eu.europa.ec.opoce.cellar.common.util.Pair;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.logging.LogContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.CMR_EMBARGO;

/**
 * This service is used to start check if something can be moved from the private to the public store at regular bases
 */
@LogContext
public class DisembargoScheduler implements Runnable {

    private static final Logger LOG = LogManager.getLogger(DisembargoScheduler.class);

    private static final EmbargoService embargoHandlingService = ServiceLocator.getService(EmbargoService.class);

    private static final Scheduler scheduler = ServiceLocator.getService(Scheduler.class);
    private static final ICellarConfiguration cellarConfiguration = ServiceLocator.getService("cellarConfiguration",
            ICellarConfiguration.class);

    private final Object running = new Object();

    /**
     * <p>run.</p>
     */
    @Override
    @LogContext(CMR_EMBARGO)
    public void run() {
        synchronized (running) {
            disembargo();
            scheduler.reschedule(true);
        }
    }

    private void disembargo() {
        // only execute if automatic disembargo is enabled
        if (!cellarConfiguration.isCellarServiceAutomaticDisembargoEnabled()) {
            return;
        }

        // find disembargoeable metadata
        final Set<Pair<String, Date>> disembargoeableDigitalObjects = embargoHandlingService.getDisembargoeableDigitalObjects();

        // try to disembargo each of the disembargoeable metadata
        final List<String> autodisembargoSuccesses = new ArrayList<>();
        final List<String> autodisembargoFailures = new ArrayList<>();
        for (final Pair<String, Date> information : disembargoeableDigitalObjects) {
            // collect info
            final String cellarId = information.getOne();
            final Date embargoDate = information.getTwo();

            // disembargo
            try {
                embargoHandlingService.automaticDisembargo(cellarId, embargoDate);
                autodisembargoSuccesses.add(cellarId);
            } catch (final CellarException exc) {
                autodisembargoFailures.add(cellarId);
            }
        }

        // audit
        if (!autodisembargoSuccesses.isEmpty()) {
            AuditBuilder.get(AuditTrailEventProcess.Embargo).withAction(AuditTrailEventAction.Pop).withType(AuditTrailEventType.End)
                    .withMessage("The objects under the following CELLAR URIs have been successfully moved out of embargo: '{}'.")
                    .withMessageArgs(autodisembargoSuccesses)
                    .withLogger(LOG)
                    .logEvent();
        }
        if (!autodisembargoFailures.isEmpty()) {
            AuditBuilder.get(AuditTrailEventProcess.Embargo).withAction(AuditTrailEventAction.Pop)
                    .withCode(CmrErrors.CMR_UNEXPECTED_EMBARGO_ERROR).withType(AuditTrailEventType.Fail)
                    .withMessage("The objects identified by the following CELLAR URIs could not be automatically moved out of embargo: '{}'.")
                    .withMessageArgs(autodisembargoFailures)
                    .withLogger(LOG)
                    .logEvent();
        }
    }

}
