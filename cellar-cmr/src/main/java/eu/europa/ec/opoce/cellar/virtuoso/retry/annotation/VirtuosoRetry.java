/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.virtuoso.annotation
 *             FILE : VirtuosoRetry.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 15-07-2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.virtuoso.retry.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <class_description> Each method annotated with {@link VirtuosoRetry} drives the write operations towards the Virtuoso store by retrying, if necessary.</br>
 * The conditions under which a retry is allowed are defined by {@link eu.europa.ec.opoce.cellar.virtuoso.retry.VirtuosoStoreRetryAwareConnection}.<br/>
 * Each annotated method should have a boolean parameter {@code hasRetryAttempts}, to operate as a flag in a try-catch clause for virtuoso backup mechanism to execute
 * after exceeding retry attempts
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 15-07-2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({
        ElementType.METHOD})
public @interface VirtuosoRetry {
}
