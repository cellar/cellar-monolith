package eu.europa.ec.opoce.cellar.cmr.catalog.entityresolving;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * <p>EntitySnippetConfiguration class.</p>
 *
 * @author Joeri.Moreno
 */
public class EntitySnippetConfiguration {

    private Map<String, File> map;

    /**
     * <p>Constructor for EntitySnippetConfiguration.</p>
     */
    public EntitySnippetConfiguration() {
    }

    /**
     * <p>getUriCatalog.</p>
     *
     * @param baseUri the baseUri that is used in the application
     * @return file that contains the catalog for the entity resolving, depending on the baseUri
     */
    public File getUriCatalog(String baseUri) {
        if (baseUri == null)
            return null;
        return map.get(baseUri);
    }

    /**
     * converter that parse the catalog with the connection between baseUri and path
     */
    public static class UriPathConverter implements Converter {

        private File fileSystemRoot;

        public UriPathConverter(File fileSystemRoot) {
            this.fileSystemRoot = fileSystemRoot;
        }

        @Override
        public void marshal(Object sourceObj, HierarchicalStreamWriter writer, MarshallingContext context) {
            EntitySnippetConfiguration uriPathSnippetConfiguration = (EntitySnippetConfiguration) sourceObj;

            for (Map.Entry<String, File> entry : uriPathSnippetConfiguration.map.entrySet()) {
                writer.startNode("entry");
                writer.addAttribute("uri", entry.getKey());
                writer.addAttribute("path", entry.getValue().getAbsolutePath());
                writer.endNode();
            }
        }

        @Override
        public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
            Map<String, File> map = new HashMap<String, File>();
            while (reader.hasMoreChildren()) {
                reader.moveDown();
                String uri = reader.getAttribute("uri");
                String path = reader.getAttribute("path");
                if (null == uri || null == path || !reader.getNodeName().equals("entry") || map.containsKey(uri)) {
                    throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.ENTITY_RESOLVING_ERROR)
                            .withMessage("Config file is incorrect").build();
                }
                File file = new File(fileSystemRoot, "cmr/resources/uri/" + path + "/catalog.xml");
                if (!file.isFile()) {
                    throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.ENTITY_RESOLVING_ERROR)
                            .withMessage("the following configured path doesn't exist {}").withMessageArgs(file.getAbsolutePath()).build();
                }
                map.put(uri, file);
                reader.moveUp();
            }
            EntitySnippetConfiguration uriPathSnippetConfiguration = new EntitySnippetConfiguration();
            uriPathSnippetConfiguration.map = Collections.unmodifiableMap(map);
            return uriPathSnippetConfiguration;
        }

        @Override
        public boolean canConvert(@SuppressWarnings("rawtypes") Class type) {
            return type.equals(EntitySnippetConfiguration.class);
        }
    }
}
