/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.sparql.jena
 *             FILE : JenaTimeUtils.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 18, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.sparql.jena;

import java.util.Date;

import org.apache.jena.datatypes.xsd.XSDDateTime;
import org.apache.jena.rdf.model.RDFNode;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 18, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class JenaTimeUtils {

    public static Date getDateOrNull(final RDFNode node) {

        if (node != null && node.isLiteral()) {
            final Object obj = node.asLiteral().getValue();
            if (obj instanceof XSDDateTime) {
                final XSDDateTime xsddt = (XSDDateTime) obj;
                return xsddt.asCalendar().getTime();
            }
        }

        return null;
    }
}
