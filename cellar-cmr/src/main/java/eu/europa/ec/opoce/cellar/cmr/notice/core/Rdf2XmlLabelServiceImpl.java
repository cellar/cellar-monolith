package eu.europa.ec.opoce.cellar.cmr.notice.core;

import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.nal.service.LanguageService;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaQueries;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import eu.europa.ec.opoce.cellar.semantic.xml.XmlBuilder;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.jena.rdf.model.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class Rdf2XmlLabelServiceImpl implements Rdf2XmlLabelService {

    private final LanguageService languageService;

    @Autowired
    public Rdf2XmlLabelServiceImpl(final LanguageService languageService) {
        this.languageService = languageService;
    }

    @Override
    public boolean writePrefLabelIfExists(final XmlBuilder prefLabelBuilder, final Resource resource,
                                          final LanguageBean useLanguage) {
        final String prefLabel = extractPrefLabel(resource, useLanguage);

        if(prefLabel == null) {
            return false;
        }

        final XmlBuilder labelBuilder = prefLabelBuilder.child("PREFLABEL");
        labelBuilder.attribute("xml:lang", useLanguage.getIsoCodeThreeChar());
        labelBuilder.text(prefLabel);
        return true;
    }

    private String extractPrefLabel(final Resource resource, final LanguageBean useLanguage) {
        String prefLabel = JenaQueries.getLiteralValue(resource, CellarProperty.skos_prefLabelP,
                useLanguage.getIsoCodeTwoChar(), false, true);

        if(prefLabel == null) {
            prefLabel = JenaQueries.getLiteralValue(resource, CellarProperty.skos_prefLabelP,
                    useLanguage.getIsoCodeThreeChar(), false, true);
        }

        return prefLabel;
    }

    @Override
    public void writeAltLabels(final XmlBuilder conceptBuilder, final Resource conceptResource,
                               final LanguageBean useLanguage) {
        for (final String altLabel : JenaQueries.getLiteralValues(conceptResource, CellarProperty.skos_altLabelP,
                useLanguage.getIsoCodeTwoChar(), false)) {
            writeAltLabel(conceptBuilder, altLabel, useLanguage.getIsoCodeThreeChar());
        }

        for (final String altLabel : JenaQueries.getLiteralValues(conceptResource, CellarProperty.skos_altLabelP,
                useLanguage.getIsoCodeThreeChar(), false)) {
            writeAltLabel(conceptBuilder, altLabel, useLanguage.getIsoCodeThreeChar());
        }
    }

    private void writeAltLabel(final XmlBuilder conceptBuilder, final String altLabel, final String isoCode) {
        final XmlBuilder labelBuilder = conceptBuilder.child("ALTLABEL");
        labelBuilder.attribute("xml:lang", isoCode);
        labelBuilder.text(altLabel);
    }

    @Override
    public void writePrefLabel(final XmlBuilder prefLabelBuilder, final Resource resource,
                               final LanguageBean useLanguage, final Map<LanguageBean, List<LanguageBean>> cache) {
        if (writePrefLabelIfExists(prefLabelBuilder, resource, useLanguage)) {
            return;
        }

        final XmlBuilder labelBuilder = prefLabelBuilder.child("PREFLABEL");
        labelBuilder.attribute("xml:lang", useLanguage.getIsoCodeThreeChar());

        for (final LanguageBean fallbackLanguage : getFallbackLanguages(useLanguage, cache)) {
            writeFallbackLanguage(prefLabelBuilder, resource, useLanguage, fallbackLanguage);
        }
    }

    private void writeFallbackLanguage(final XmlBuilder prefLabelBuilder, final Resource resource,
                                       final LanguageBean useLanguage, final LanguageBean fallbackLanguage) {
        for (final Resource fallback : JenaQueries.getResources(resource, CellarProperty.cmr_fallbackP, false)) {
            final String lang = JenaQueries.getLiteralValue(fallback, CellarProperty.cmr_langP,true, true);
            if (lang != null && !lang.equals(fallbackLanguage.getIsoCodeTwoChar()) && !lang.equals(fallbackLanguage.getIsoCodeThreeChar())) {
                continue;
            }

            final String prefLabel = JenaQueries.getLiteralValue(fallback, CellarProperty.skos_prefLabelP, false, true);
            if (null == prefLabel) {
                continue;
            }

            final XmlBuilder fallbackBuilder = prefLabelBuilder.child("FALLBACK");
            fallbackBuilder.attribute("xml:lang", useLanguage.getIsoCodeThreeChar());
            fallbackBuilder.child("LANG").text(fallbackLanguage.getIsoCodeThreeChar()); // TODO: i don't think this is ISO-639-2
            fallbackBuilder.child("PREFLABEL").text(prefLabel);
            return;
        }
    }

    private List<LanguageBean> getFallbackLanguages(final LanguageBean language, Map<LanguageBean, List<LanguageBean>> cache) {
        if (language == null) {
            return Collections.emptyList();
        }
        if (cache.containsKey(language)) {
            return cache.get(language);
        }

        final List<String> fallbackLanguages = languageService.getFallbackLanguagesFor(language.getUri());
        final ArrayList<LanguageBean> fallback = CollectionUtils.collect(fallbackLanguages, languageService::getByUri, new ArrayList<>());
        cache.put(language, fallback);
        return fallback;
    }

}

