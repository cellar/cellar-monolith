/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice
 *             FILE : NoticeBuilderFactory.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 24 Apr 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.notice;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cmr.notice.core.NoticeType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 24 Apr 2017
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class NoticeBuilderServiceFactoryImpl implements NoticeBuilderServiceFactory {

    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    /** {@inheritDoc} */
    @Override
    public NoticeBuilder getNewNoticeBuilderInstance(final NoticeType type) {
        final NoticeBuilder noticeBuilder = new NoticeBuilder(type);
        final boolean cellarServiceDisseminationNoticesSortEnabled = cellarConfiguration.isCellarServiceDisseminationNoticesSortEnabled();
        noticeBuilder.setSort(cellarServiceDisseminationNoticesSortEnabled);
        return noticeBuilder;
    }

}
