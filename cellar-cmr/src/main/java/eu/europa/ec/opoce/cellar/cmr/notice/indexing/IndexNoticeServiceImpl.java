/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice
 *             FILE : IndexNoticeServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 09 18, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-09-18 15:51:45 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.notice.indexing;

import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Agent;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Dossier;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Event;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Expression;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Manifestation;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.TopLevelEvent;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Work;
import eu.europa.ec.opoce.cellar.cmr.notice.MetsElementWithLanguage;
import eu.europa.ec.opoce.cellar.cmr.notice.NoticeBuilder;
import eu.europa.ec.opoce.cellar.cmr.notice.NoticeBuilderServiceFactory;
import eu.europa.ec.opoce.cellar.cmr.notice.NoticeCache;
import eu.europa.ec.opoce.cellar.cmr.notice.core.NoticeType;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.IndexNoticeWork;
import eu.europa.ec.opoce.cellar.common.util.Triple;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.nal.service.LanguageService;
import eu.europa.ec.opoce.cellar.nal.domain.RequiredLanguageBean;
import eu.europa.ec.opoce.cellar.server.service.DisseminationDbGateway;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * @author ARHS Developments
 */
@Service("indexNoticeService")
public class IndexNoticeServiceImpl implements IndexNoticeService {

    private final DisseminationDbGateway disseminationDbGateway;
    private final IdentifierService identifierService;
    private final LanguageService languageService;
    private final NoticeBuilderServiceFactory noticeBuilderServiceFactory;

    @Autowired
    public IndexNoticeServiceImpl(DisseminationDbGateway disseminationDbGateway,
                                  @Qualifier("pidManagerService") IdentifierService identifierService,
                                  LanguageService languageService,
                                  NoticeBuilderServiceFactory noticeBuilderServiceFactory) {
        this.disseminationDbGateway = disseminationDbGateway;
        this.identifierService = identifierService;
        this.languageService = languageService;
        this.noticeBuilderServiceFactory = noticeBuilderServiceFactory;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IndexNoticeWork createIndexNoticesWork(final String workUri) {
        final Work work = this.disseminationDbGateway.getWorkTree(this.identifierService.getCellarPrefixed(workUri));
        if (work == null) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("No tree data found for work '{}'").withMessageArgs(workUri)
                    .build();
        }

        return this.createIndexNoticesWork(work);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IndexNoticeWork createIndexNoticesWork(final Work work) {
        final NoticeCache noticeData = new NoticeCache(work);
        try {
            if (work == null) {
                throw ExceptionBuilder.get(CellarException.class).build();
            }

            if (work.isUnderEmbargo()) {
                return null;
            }

            final IndexNoticeWork indexNoticeWork = new IndexNoticeWork(work);
            final Map<Expression, HashMap<LanguageBean, Document>> noticesPerExpression = this.generateIndexNotices(noticeData, work);
            indexNoticeWork.addNoticePerExpression(noticesPerExpression);

            final Set<String> processedLanguages = new HashSet<>(noticesPerExpression.size());
            for (final Expression expression : noticesPerExpression.keySet()) {
                final Set<LanguageBean> languageBeans = expression.getLangs();
                for (final LanguageBean languageBean : languageBeans) {
                    final String uri = languageBean.getUri();
                    processedLanguages.add(uri);
                }
            }

            final List<LanguageBean> missingLanguages = new ArrayList<>();
            for (final RequiredLanguageBean requiredLanguageBean : this.languageService.getRequiredLanguages()) {
                if (!processedLanguages.contains(requiredLanguageBean.getUri())) {
                    missingLanguages.add(this.languageService.getByUri(requiredLanguageBean.getUri()));
                }
            }

            indexNoticeWork.addNoticePerLanguage(this.generateIndexNotices(noticeData, work, missingLanguages));

            return indexNoticeWork;
        } finally {
            noticeData.closeQuietly();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<Expression, HashMap<LanguageBean, Document>> createIndexNoticesWork(final Collection<Expression> expressions) {
        if (expressions == null) {
            throw ExceptionBuilder.get(CellarException.class).build();
        }

        if (expressions.isEmpty()) {
            return Collections.emptyMap();
        }
        final Work work = expressions.iterator().next().getParent();
        if (work.isUnderEmbargo()) {
            return Collections.emptyMap();
        }

        for (final Expression expression : expressions) {
            if (!expression.getParent().equals(work)) {
                throw ExceptionBuilder.get(CellarException.class).build();
            }
        }

        final NoticeCache noticeData = new NoticeCache(work);
        try {
            return this.generateIndexNotices(noticeData, expressions);
        } finally {
            noticeData.closeQuietly();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Triple<Expression, LanguageBean, Document>> createIndexNoticeExpression(final String expressionUri) {
        final List<Triple<Expression, LanguageBean, Document>> result = new ArrayList<>();
        if (StringUtils.isBlank(expressionUri)) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("Required attribute expressionUri cannot be blank")
                    .build();
        }

        final Work work = this.disseminationDbGateway.getWorkBranch(this.identifierService.getCellarPrefixed(expressionUri));
        if (work == null) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("No work found for expression '{}'")
                    .withMessageArgs(expressionUri)
                    .build();
        }

        if (work.isUnderEmbargo()) {
            return null;
        }

        final List<Expression> expressions = new ArrayList<>(languageService.filterMultiLinguisticExpressions(work));
        final int size = expressions.size();
        if (size == 0) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("Found work for expression '{}' does not contain the expression")
                    .withMessageArgs(expressionUri)
                    .build();
        }
        if (size > 1) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("Branch data for expression '{}' does contain more than one expression")
                    .withMessageArgs(expressionUri)
                    .build();
        }

        final Expression expression = expressions.get(0);

        final NoticeCache noticeData = new NoticeCache(work);
        try {
            final Set<LanguageBean> languageBeans = expression.getLangs();
            for (final LanguageBean languageBean : languageBeans) {
                final Document indexNotice = this.generateIndexNotice(noticeData, expression, languageBean);
                final Triple<Expression, LanguageBean, Document> triple = new Triple<>(expression,
                        languageBean, indexNotice);
                result.add(triple);
            }
            return result;
        } finally {
            noticeData.closeQuietly();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Triple<Expression, LanguageBean, Document>> createIndexNoticeManifestation(final String manifestUri) {
        final List<Triple<Expression, LanguageBean, Document>> result = new ArrayList<>();
        if (StringUtils.isBlank(manifestUri)) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("Required attribute manifestUri cannot be blank")
                    .build();
        }

        final Work work = this.disseminationDbGateway.getWorkBranch(this.identifierService.getCellarPrefixed(manifestUri));
        if (work == null) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("No work found for manifestation'{}'")
                    .withMessageArgs(manifestUri)
                    .build();
        }

        if (work.isUnderEmbargo()) {
            return null;
        }

        final List<Expression> expressions = new ArrayList<>(languageService.filterMultiLinguisticExpressions(work));
        int size = expressions.size();
        if (size == 0) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("Manifest data for manifestation '{}' does not contain any expression")
                    .withMessageArgs(manifestUri)
                    .build();
        }
        if (size > 1) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("Manifest data for manifestation '{}' does contain more than one expression")
                    .withMessageArgs(manifestUri)
                    .build();
        }

        final Expression expression = expressions.get(0);
        size = expression.getChildren().size();
        if (size == 0) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("Manifest data for manifestation '{}' does not contain manifestation")
                    .withMessageArgs(manifestUri)
                    .build();
        }
        if (size > 1) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("Manifest data for manifestation '{}' does contain more than one manifestation")
                    .withMessageArgs(manifestUri)
                    .build();
        }

        final NoticeCache noticeData = new NoticeCache(work);
        try {

            final Set<LanguageBean> languageBeans = expression.getLangs();
            for (final LanguageBean languageBean : languageBeans) {
                final Document indexNotice = this.generateIndexNotice(noticeData, expression, languageBean);
                final Triple<Expression, LanguageBean, Document> triple = new Triple<>(expression,
                        languageBean, indexNotice);
                result.add(triple);
            }
            return result;
        } finally {
            noticeData.closeQuietly();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<MetsElementWithLanguage, Document> createIndexNoticeDossier(final String dossierUri) {
        final Dossier dossier = this.disseminationDbGateway.getDossierTree(this.identifierService.getCellarPrefixed(dossierUri));
        if (dossier == null) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("No tree data found for dossier '{}'")
                    .withMessageArgs(dossierUri)
                    .build();
        }

        final NoticeCache noticeData = new NoticeCache(dossier);
        try {
            return this.generateIndexNotices(noticeData, dossier);
        } finally {
            noticeData.closeQuietly();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<MetsElementWithLanguage, Document> createIndexNoticeEvent(final String eventUri) {
        final Dossier dossier = this.disseminationDbGateway.getDossierBranch(this.identifierService.getCellarPrefixed(eventUri));
        if (dossier == null) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("No dossier data (branch-data) found for event '{}'")
                    .withMessageArgs(eventUri)
                    .build();
        }

        final List<Event> events = new ArrayList<>(dossier.getChildren());
        final int size = events.size();
        if (size == 0) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("Found dossier data(branch-data) for event '{}' does not contain the event")
                    .withMessageArgs(eventUri)
                    .build();
        }
        if (size > 1) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("Branch data for event '{}' does contain more than one event")
                    .withMessageArgs(eventUri)
                    .build();
        }
        final Event event = dossier.getChildren().iterator().next();

        final NoticeCache noticeData = new NoticeCache(dossier);
        try {
            return this.generateIndexNotices(noticeData, event);
        } finally {
            noticeData.closeQuietly();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<MetsElementWithLanguage, Document> createIndexNoticeAgent(final String agentUri) {
        final Agent agent = this.disseminationDbGateway.getAgent(this.identifierService.getCellarPrefixed(agentUri));
        if (agent == null) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("No tree data found for dossier '{}'")
                    .withMessageArgs(agentUri)
                    .build();
        }

        final NoticeCache noticeData = new NoticeCache(agent);
        try {
            return this.generateIndexNotices(noticeData, agent);
        } finally {
            noticeData.closeQuietly();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<MetsElementWithLanguage, Document> createIndexNoticeTopLevelEvent(final String topLevelEventUri) {
        final TopLevelEvent topLevelEvent = this.disseminationDbGateway
                .getTopLevelEvent(this.identifierService.getCellarPrefixed(topLevelEventUri));
        if (topLevelEvent == null) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("No tree data found for TopLevelEvent '{}'")
                    .withMessageArgs(topLevelEventUri)
                    .build();
        }

        final NoticeCache noticeData = new NoticeCache(topLevelEvent);
        try {
            return this.generateIndexNotices(noticeData, topLevelEvent);
        } finally {
            noticeData.closeQuietly();
        }
    }

    /**
     * <p>generateIndexNotices.</p>
     *
     * @param noticeData a {@link eu.europa.ec.opoce.cellar.cmr.notice.NoticeCache} object.
     * @param dossier    a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Dossier} object.
     * @return a {@link java.util.Map} object.
     */
    private Map<MetsElementWithLanguage, Document> generateIndexNotices(final NoticeCache noticeData, final Dossier dossier) {
        final Map<MetsElementWithLanguage, Document> noticePerEvent = new HashMap<>();

        for (final RequiredLanguageBean rlanguage : this.languageService.getRequiredLanguages()) {
            final LanguageBean language = this.languageService.getByUri(rlanguage.getUri());
            final HashSet<LanguageBean> languages = new HashSet<>(Collections.singletonList(language));

            final NoticeBuilder builder = noticeBuilderServiceFactory.getNewNoticeBuilderInstance(NoticeType.indexing).withCache(noticeData)
                    .withLanguages(languages);
            try {
                builder.withObject(dossier).buildDirect(language).buildInverse(language);
                for (final Event event : dossier.getChildren()) {
                    builder.withObject(event).buildDirect(language).buildInverse(language);
                }
                noticePerEvent.put(new MetsElementWithLanguage(dossier, language), builder.getDocument(language));
            } finally {
                builder.close();
            }
        }

        return noticePerEvent;
    }

    /**
     * <p>generateIndexNotices.</p>
     *
     * @param noticeData a {@link eu.europa.ec.opoce.cellar.cmr.notice.NoticeCache} object.
     * @param event      a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Event} object.
     * @return a {@link java.util.Map} object.
     */
    private Map<MetsElementWithLanguage, Document> generateIndexNotices(final NoticeCache noticeData, final Event event) {
        final Map<MetsElementWithLanguage, Document> noticePerEvent = new HashMap<>();

        for (final RequiredLanguageBean rlanguage : this.languageService.getRequiredLanguages()) {
            final LanguageBean language = this.languageService.getByUri(rlanguage.getUri());
            final HashSet<LanguageBean> languages = new HashSet<>(Collections.singletonList(language));
            final NoticeBuilder builder = noticeBuilderServiceFactory.getNewNoticeBuilderInstance(NoticeType.indexing).withCache(noticeData)
                    .withLanguages(languages);
            try {
                builder.withObject(event.getParent()).buildDirect(language).buildInverse(language);
                builder.withObject(event).buildDirect(language).buildInverse(language);
                noticePerEvent.put(new MetsElementWithLanguage(event, language), builder.getDocument(language));
            } finally {
                builder.close();
            }
        }
        return noticePerEvent;
    }

    /**
     * <p>generateIndexNotices.</p>
     *
     * @param noticeData a {@link eu.europa.ec.opoce.cellar.cmr.notice.NoticeCache} object.
     * @param agent      a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Agent} object.
     * @return a {@link java.util.Map} object.
     */
    private Map<MetsElementWithLanguage, Document> generateIndexNotices(final NoticeCache noticeData, final Agent agent) {
        final Map<MetsElementWithLanguage, Document> noticePerEvent = new HashMap<>();

        for (final RequiredLanguageBean rlanguage : this.languageService.getRequiredLanguages()) {
            final LanguageBean language = this.languageService.getByUri(rlanguage.getUri());
            final HashSet<LanguageBean> languages = new HashSet<>(Collections.singletonList(language));
            final NoticeBuilder builder = noticeBuilderServiceFactory.getNewNoticeBuilderInstance(NoticeType.indexing).withCache(noticeData)
                    .withLanguages(languages);
            try {
                builder.withObject(agent).buildDirect(language).buildInverse(language);
                noticePerEvent.put(new MetsElementWithLanguage(agent, language), builder.getDocument(language));
            } finally {
                builder.close();
            }
        }
        return noticePerEvent;
    }

    /**
     * <p>generateIndexNotices.</p>
     *
     * @param noticeData    a {@link eu.europa.ec.opoce.cellar.cmr.notice.NoticeCache} object.
     * @param topLevelEvent a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Event} object.
     * @return a {@link java.util.Map} object.
     */
    private Map<MetsElementWithLanguage, Document> generateIndexNotices(final NoticeCache noticeData, final TopLevelEvent topLevelEvent) {
        final Map<MetsElementWithLanguage, Document> noticePerEvent = new HashMap<>();

        for (final RequiredLanguageBean rlanguage : this.languageService.getRequiredLanguages()) {
            final LanguageBean language = this.languageService.getByUri(rlanguage.getUri());
            final HashSet<LanguageBean> languages = new HashSet<>(Collections.singletonList(language));
            final NoticeBuilder builder = noticeBuilderServiceFactory.getNewNoticeBuilderInstance(NoticeType.indexing).withCache(noticeData)
                    .withLanguages(languages);
            try {
                builder.withObject(topLevelEvent).buildDirect(language).buildInverse(language);
                noticePerEvent.put(new MetsElementWithLanguage(topLevelEvent, language), builder.getDocument(language));
            } finally {
                builder.close();
            }
        }
        return noticePerEvent;
    }

    /**
     * <p>generateIndexNotices.</p>
     *
     * @param noticeData a {@link eu.europa.ec.opoce.cellar.cmr.notice.NoticeCache} object.
     * @param work       a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Work} object.
     * @return a {@link java.util.Map} object.
     */
    private Map<Expression, HashMap<LanguageBean, Document>> generateIndexNotices(final NoticeCache noticeData, final Work work) {
        return this.generateIndexNotices(noticeData, languageService.filterMultiLinguisticExpressions(work));
    }

    /**
     * <p>generateIndexNotices.</p>
     *
     * @param noticeData  a {@link eu.europa.ec.opoce.cellar.cmr.notice.NoticeCache} object.
     * @param expressions a {@link java.util.Collection} object.
     * @return a {@link java.util.Map} object.
     */
    private Map<Expression, HashMap<LanguageBean, Document>> generateIndexNotices(final NoticeCache noticeData,
                                                                                  final Collection<Expression> expressions) {
        final Map<Expression, HashMap<LanguageBean, Document>> noticePerExpression = new HashMap<>();
        for (final Expression expression : expressions) {
            for (final LanguageBean languageBean : expression.getLangs()) {
                final Document document = this.generateIndexNotice(noticeData, expression, languageBean);
                final HashMap<LanguageBean, Document> expressionDocuments = noticePerExpression.get(expression);
                if (expressionDocuments == null) {
                    final HashMap<LanguageBean, Document> map = new HashMap<>();
                    map.put(languageBean, document);
                    noticePerExpression.put(expression, map);
                } else {
                    expressionDocuments.put(languageBean, document);
                }
            }
        }
        return noticePerExpression;
    }

    /**
     * <p>generateIndexNotice.</p>
     *
     * @param noticeData   a {@link eu.europa.ec.opoce.cellar.cmr.notice.NoticeCache} object.
     * @param expression   a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Expression} object.
     * @param languageBean the language bean
     * @return a {@link org.w3c.dom.Document} object.
     */
    private Document generateIndexNotice(final NoticeCache noticeData, final Expression expression, final LanguageBean languageBean) {
        final HashSet<LanguageBean> languages = new HashSet<>(Collections.singletonList(languageBean));
        final NoticeBuilder builder = noticeBuilderServiceFactory.getNewNoticeBuilderInstance(NoticeType.indexing).withCache(noticeData)
                .withContentLanguages(languages).withLanguages(languages);
        try {
            builder.withObject(expression.getParent()).buildDirect(languageBean).buildInverse(languageBean);
            builder.withObject(expression).buildDirect(languageBean).buildInverse(languageBean);
            for (final Manifestation manifestation : expression.getChildren()) {
                builder.withObject(manifestation).buildDirect(languageBean).buildInverse(languageBean);
            }
            return builder.getDocument(languageBean);
        } finally {
            builder.close();
        }
    }

    /**
     * <p>generateIndexNotices.</p>
     *
     * @param noticeData a {@link eu.europa.ec.opoce.cellar.cmr.notice.NoticeCache} object.
     * @param work       a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Work} object.
     * @param languages  a {@link java.util.Collection} object.
     * @return a {@link java.util.Map} object.
     */
    private Map<LanguageBean, Document> generateIndexNotices(final NoticeCache noticeData, final Work work,
                                                             final Collection<LanguageBean> languages) {
        final Map<LanguageBean, Document> noticePerLanguage = new HashMap<>(languages.size());
        for (final LanguageBean language : languages) {
            noticePerLanguage.put(language, this.generateFallBackIndexNotice(noticeData, work, language, language));
        }
        return noticePerLanguage;
    }

    /**
     * <p>generateFallBackIndexNotice.</p>
     *
     * @param noticeData      a {@link eu.europa.ec.opoce.cellar.cmr.notice.NoticeCache} object.
     * @param work            a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Work} object.
     * @param contentLanguage a {@link eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean} object.
     * @param language        a {@link eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean} object.
     * @return a {@link org.w3c.dom.Document} object.
     */
    private Document generateFallBackIndexNotice(final NoticeCache noticeData, final Work work, final LanguageBean contentLanguage,
                                                 final LanguageBean language) {
        final List<LanguageBean> inLanguage = language != null ? Collections.singletonList(language) : new ArrayList<>();
        final HashSet<LanguageBean> languages = new HashSet<>(inLanguage);
        final List<LanguageBean> inContentLanguage = contentLanguage != null ? Collections.singletonList(contentLanguage) : new ArrayList<>();
        final HashSet<LanguageBean> contentLanguages = new HashSet<>(inContentLanguage);
        final NoticeBuilder builder = noticeBuilderServiceFactory.getNewNoticeBuilderInstance(NoticeType.indexing)
                .withCache(noticeData)
                .withContentLanguages(contentLanguages)
                .withLanguages(languages);
        try {
            builder.withObject(work).buildDirect(language).buildInverse(language);
            final Expression fallbackExpression = (work != null) && (language != null) ? this.getFallBackExpression(work, language) : null;
            if (fallbackExpression != null) {
                builder.withObject(fallbackExpression).buildDirect(language).buildInverse(language);
            }
            return builder.getDocument(language);
        } finally {
            builder.close();
        }
    }

    /**
     * <p>getFallBackExpression.</p>
     *
     * @param work     a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Work} object.
     * @param language a {@link eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean} object.
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Expression} object.
     */
    private Expression getFallBackExpression(final Work work, final LanguageBean language) {
        final List<Expression> filterMultiLinguisticExpressions = languageService.filterMultiLinguisticExpressions(work);
        final List<LanguageBean> fallbackLanguages = this.getFallbackLanguages(language);
        final Optional<Expression> findFirst = filterMultiLinguisticExpressions.stream()
                .filter(expression -> !Collections.disjoint(expression.getLangs(), fallbackLanguages)).findFirst();
        return findFirst.orElse(null);
    }

    /**
     * <p>getFallbackLanguages.</p>
     *
     * @param language a {@link eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean} object.
     * @return a {@link java.util.List} object.
     */
    private List<LanguageBean> getFallbackLanguages(final LanguageBean language) {
        final List<String> fallbackLanguages = this.languageService.getFallbackLanguagesFor(language.getUri());
        return CollectionUtils.collect(fallbackLanguages, languageService::getByUri, new ArrayList<>());
    }
}
