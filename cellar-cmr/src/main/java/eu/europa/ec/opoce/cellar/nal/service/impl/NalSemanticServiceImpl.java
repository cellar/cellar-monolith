/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.nal.rework.impl
 *             FILE : NalOntologyInferenceServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 01 10, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-01-10 14:29:46 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.nal.service.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.database.dao.OntoConfigDao;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.service.NalSemanticService;
import eu.europa.ec.opoce.cellar.ontology.dao.OntoConfig;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.StringReader;

import static eu.europa.ec.opoce.cellar.CmrErrors.ONTO_MISSING_CONFIG;
import static eu.europa.ec.opoce.cellar.nal.NalOntoUtils.AUTHORITY_ONTOLOGY_URI;
import static eu.europa.ec.opoce.cellar.nal.NalOntoUtils.EUROVOC_ONTOLOGY_URI;

/**
 * @author ARHS Developments
 */
@Service
public class NalSemanticServiceImpl implements NalSemanticService {

    private static final Logger LOG = LogManager.getLogger(NalSemanticServiceImpl.class);

    private final ContentStreamService contentStreamService;
    private final OntoConfigDao ontoConfigDao;

    @Autowired
    public NalSemanticServiceImpl(ContentStreamService contentStreamService, OntoConfigDao ontoConfigDao) {
        this.contentStreamService = contentStreamService;
        this.ontoConfigDao = ontoConfigDao;
    }

    @Override
    public Model getAuthorityOntology() {
        OntoConfig cfg = getOntologyS3Pid(AUTHORITY_ONTOLOGY_URI);
        return getOntology(cfg.getExternalPid(), cfg.getVersion());
    }

    @Override
    public Model getEurovocOntology() {
        OntoConfig cfg = getOntologyS3Pid(EUROVOC_ONTOLOGY_URI);
        return getOntology(cfg.getExternalPid(), cfg.getVersion());
    }

    private Model getOntology(String ontologyS3Pid, String version) {
        return retrieveOntologyFromS3(ontologyS3Pid, version);
    }

    private Model retrieveOntologyFromS3(String ontologyS3Pid, String version) {
        Model outputModel = ModelFactory.createDefaultModel();
        // Replace ":" by "/" for backward compatibility with S3
        String pid = ontologyS3Pid.replace(":", "/");
        String ontologyAsString = contentStreamService.getContentAsString(pid, version, ContentType.DIRECT)
                .orElseThrow(() -> ExceptionBuilder.get(CellarException.class)
                        .withMessage("The ontology '{}' could not be retrieved from S3.")
                        .withMessageArgs(ontologyS3Pid)
                        .build());
        outputModel.read(new StringReader(ontologyAsString), "", Lang.NT.getName());
        LOG.debug(IConfiguration.NAL, "Successfully retrieved the ontology '{}'.", ontologyS3Pid);
        return outputModel;
    }

    private OntoConfig getOntologyS3Pid(String ontologyUri) {
        return ontoConfigDao.findByUri(ontologyUri)
                .orElseThrow(() -> ExceptionBuilder.get(CellarException.class)
                        .withCode(ONTO_MISSING_CONFIG)
                        .withMessage("The ontology with URI '{}' is missing from the ontology config table.")
                        .withMessageArgs(ontologyUri)
                        .build());

    }
}
