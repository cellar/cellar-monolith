package eu.europa.ec.opoce.cellar.cmr.daobeans;

import eu.europa.ec.opoce.cellar.cmr.database.DaoObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;

import java.util.Objects;
import java.util.Set;

/**
 * <p>InverseRelation class.</p>
 */
public class InverseRelation extends DaoObject {

    private String source;
    private String target;
    private DigitalObjectType sourceType;
    private DigitalObjectType targetType;
    private Set<String> propertiesSourceTarget;
    private Set<String> propertiesTargetSource;

    @Override
    public String toString() {
        return String.format(
                "InverseRelation{ source='%s', target='%s', sourceType='%s', targetType='%s', propertiesSourceTarget='%s', propertiesTargetSource='%s' }",
                this.source, this.target, this.sourceType, this.targetType, this.propertiesSourceTarget.toString(),
                this.propertiesTargetSource.toString());
    }

    /**
     * id of the source of the relation
     *
     * @return productionId
     */
    public String getSource() {
        return source;
    }

    /**
     * <p>Setter for the field <code>source</code>.</p>
     *
     * @param source a {@link java.lang.String} object.
     */
    public void setSource(String source) {
        this.source = source;
    }

    /**
     * <p>Getter for the field <code>sourceType</code>.</p>
     *
     * @return a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType} object.
     */
    public DigitalObjectType getSourceType() {
        return sourceType;
    }

    /**
     * <p>Setter for the field <code>sourceType</code>.</p>
     *
     * @param sourceType a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType} object.
     */
    public void setSourceType(DigitalObjectType sourceType) {
        this.sourceType = sourceType;
    }

    /**
     * id of the target of te relation
     *
     * @return cellarId
     */
    public String getTarget() {
        return target;
    }

    /**
     * <p>Setter for the field <code>target</code>.</p>
     *
     * @param target a {@link java.lang.String} object.
     */
    public void setTarget(String target) {
        this.target = target;
    }

    /**
     * <p>Getter for the field <code>targetType</code>.</p>
     *
     * @return a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType} object.
     */
    public DigitalObjectType getTargetType() {
        return targetType;
    }

    /**
     * <p>Setter for the field <code>targetType</code>.</p>
     *
     * @param targetType a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType} object.
     */
    public void setTargetType(DigitalObjectType targetType) {
        this.targetType = targetType;
    }

    /**
     * <p>Getter for the field <code>propertiesSourceTarget</code>.</p>
     *
     * @return a {@link java.util.Set} object.
     */
    public Set<String> getPropertiesSourceTarget() {
        return propertiesSourceTarget;
    }

    /**
     * <p>Setter for the field <code>propertiesSourceTarget</code>.</p>
     *
     * @param propertiesSourceTarget a {@link java.util.Set} object.
     */
    public void setPropertiesSourceTarget(Set<String> propertiesSourceTarget) {
        this.propertiesSourceTarget = propertiesSourceTarget;
    }

    /**
     * <p>Getter for the field <code>propertiesTargetSource</code>.</p>
     *
     * @return a {@link java.util.Set} object.
     */
    public Set<String> getPropertiesTargetSource() {
        return propertiesTargetSource;
    }

    /**
     * <p>Setter for the field <code>propertiesTargetSource</code>.</p>
     *
     * @param propertiesTargetSource a {@link java.util.Set} object.
     */
    public void setPropertiesTargetSource(Set<String> propertiesTargetSource) {
        this.propertiesTargetSource = propertiesTargetSource;
    }

    @Override
    public int hashCode() {
        return Objects.hash(source, target, sourceType, targetType, propertiesSourceTarget, propertiesTargetSource);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof InverseRelation))
            return false;

        final InverseRelation inverseRelation = (InverseRelation) obj;
        return Objects.equals(this.source, inverseRelation.source) &&
                Objects.equals(this.target, inverseRelation.target) &&
                inverseRelation.sourceType == this.sourceType &&
                inverseRelation.targetType == this.targetType &&
                Objects.equals(this.propertiesSourceTarget, inverseRelation.propertiesSourceTarget) &&
                Objects.equals(this.propertiesTargetSource, inverseRelation.propertiesTargetSource);
    }
}
