/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.resolver.impl.zip
 *             FILE : ZipDisseminationResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 26-03-2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.retrieve.contentPool;

import eu.europa.ec.opoce.cellar.common.http.headers.IURLTokenable;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.Type;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.IDisseminationResolver;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.retrieve.RetrieveResolver;
import eu.europa.ec.opoce.cellar.server.service.DisseminationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 26-03-2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class ContentPoolZipRetrieveResolver extends RetrieveResolver {

    public static IDisseminationResolver get(final String identifier, final String eTag, final String lastModified) {
        return new ContentPoolZipRetrieveResolver(identifier, eTag, lastModified);
    }

    protected ContentPoolZipRetrieveResolver(final String identifier, final String eTag, final String lastModified) {
        super(identifier, eTag, lastModified, true);
    }

    protected void initAndCheckDisseminationRequest() {
        super.initDisseminationRequest();

        if (this.cellarResource.getCellarType() == DigitalObjectType.ITEM) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage("Bad resource type for Zip request. [resource '{}'({})]")
                    .withMessageArgs(this.cellarResource.getCellarId(), this.cellarResource.getCellarType()).build();
        }
    }

    /**
     * @see eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.DisseminationResolver#doHandleDisseminationRequest()
     */
    @Override
    public ResponseEntity<?> doHandleDisseminationRequest() {
        return this.disseminationService.doZipContentRequest(this.eTag, this.lastModified, this.cellarResource);
    }

    @Override
    protected IURLTokenable getTypeStructure() {
        return Type.ZIP;
    }
}
