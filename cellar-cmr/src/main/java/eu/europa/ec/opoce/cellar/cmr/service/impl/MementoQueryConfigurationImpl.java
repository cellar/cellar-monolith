package eu.europa.ec.opoce.cellar.cmr.service.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import eu.europa.ec.opoce.cellar.cmr.service.IMementoQueryConfiguration;
import eu.europa.ec.opoce.cellar.cmr.sparql.utils.QueryUtils;

@Service("mementoQueryConfiguration")
public class MementoQueryConfigurationImpl implements IMementoQueryConfiguration {

    @Autowired
    @Qualifier("mementoTemplatesStringsMap")
    private HashMap<String, String> mementoTemplatesStringsMap;

    /** {@inheritDoc} */
    @Override
    public String getQueryForEvolutiveWork(final Map<Pattern, Object> placeHoldersMap) {
        return QueryUtils.resolveString("evolutiveWorkTemplate", mementoTemplatesStringsMap, placeHoldersMap);
    }

    /** {@inheritDoc} */
    @Override
    public String getQueryForEvolutiveWorkOrMemento(final Map<Pattern, Object> placeHoldersMap) {
        return QueryUtils.resolveString("evolutiveWorkOrMementoTemplate", mementoTemplatesStringsMap, placeHoldersMap);
    }

    /** {@inheritDoc} */
    @Override
    public String getQueryForFirstMemento(final Map<Pattern, Object> placeHoldersMap) {
        return QueryUtils.resolveString("firstMementoTemplate", mementoTemplatesStringsMap, placeHoldersMap);
    }

    /** {@inheritDoc} */
    @Override
    public String getQueryForLastMemento(final Map<Pattern, Object> placeHoldersMap) {
        return QueryUtils.resolveString("lastMementoTemplate", mementoTemplatesStringsMap, placeHoldersMap);
    }

    /** {@inheritDoc} */
    @Override
    public String getQueryForMementoDateTime(final Map<Pattern, Object> placeHoldersMap) {
        return QueryUtils.resolveString("mementoDateTimeTemplate", mementoTemplatesStringsMap, placeHoldersMap);
    }

    /** {@inheritDoc} */
    @Override
    public String getQueryForNearestInFuture(final Map<Pattern, Object> placeHoldersMap) {
        return QueryUtils.resolveString("nearestInFutureTemplate", mementoTemplatesStringsMap, placeHoldersMap);
    }

    /** {@inheritDoc} */
    @Override
    public String getQueryForNearestInPast(final Map<Pattern, Object> placeHoldersMap) {
        return QueryUtils.resolveString("nearestInPastTemplate", mementoTemplatesStringsMap, placeHoldersMap);
    }

    /** {@inheritDoc} */
    @Override
    public String getQueryForRelatedMementos(final Map<Pattern, Object> placeHoldersMap) {
        return QueryUtils.resolveString("relatedMementosTemplate", mementoTemplatesStringsMap, placeHoldersMap);
    }

    /** {@inheritDoc} */
    @Override
    public String getQueryForRelatedEvolutiveWorks(final Map<Pattern, Object> placeHoldersMap) {
        return QueryUtils.resolveString("relatedEvolutiveWorksTemplate", mementoTemplatesStringsMap, placeHoldersMap);
    }

    /** {@inheritDoc} */
    @Override
    public String getQueryForTimeMapInfo(final Map<Pattern, Object> placeHoldersMap) {
        return QueryUtils.resolveString("timeMapInfoTemplate", mementoTemplatesStringsMap, placeHoldersMap);
    }

    /** {@inheritDoc} */
    @Override
    public String getQueryForURIG(final Map<Pattern, Object> placeHoldersMap) {
        return QueryUtils.resolveString("uriGTemplate", mementoTemplatesStringsMap, placeHoldersMap);
    }
}
