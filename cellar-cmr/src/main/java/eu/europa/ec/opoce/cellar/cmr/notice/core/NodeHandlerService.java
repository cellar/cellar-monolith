/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice.core
 *             FILE : NodeHandlerService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 24 Apr 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.notice.core;

import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.semantic.xml.XmlBuilder;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author ARHS Developments
 */
public interface NodeHandlerService {

    void writeMemberList(NodeHandler personalNodeHandler, XmlBuilder xmlbuilder, Resource resource,
                         Property currentProperty);

    void writeConceptAllLevelsWithAnnotations(XmlBuilder xmlBuilder, Property property, Resource resource,
                                              Map<LanguageBean, List<LanguageBean>> cache, Set<LanguageBean> languages,
                                              NoticeType noticeType, Model model, Set<Resource> allSubjectResources);

    void writeConcept(XmlBuilder conceptBuilder, Resource conceptResource, Map<LanguageBean, List<LanguageBean>> cache,
                      Set<LanguageBean> languages, NoticeType noticeType);

    void addAnnotations(XmlBuilder parentBuilder, Property property, RDFNode object, Model model,
                        Set<Resource> allSubjectResources);

}
