package eu.europa.ec.opoce.cellar.cmr.indexing.service.impl;

import eu.europa.ec.opoce.cellar.cl.concurrency.Concurrent;
import eu.europa.ec.opoce.cellar.cl.concurrency.IConcurrentArgsResolver;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.IndexationCalc;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.database.dao.IndexationCalcDao;
import eu.europa.ec.opoce.cellar.domain.content.mets.OffIngestionOperationType;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class EmbeddedNoticeSynchronizationProcessor implements IConcurrentArgsResolver {

    private static final Logger LOG = LoggerFactory.getLogger(EmbeddedNoticeSynchronizationProcessor.class);

    /**
     * CMR_CELLAR_RESOURCE_MD DAO.
     */
    @Autowired
    private CellarResourceDao cellarResourceDao;
    /**
     * CMR_INDEXATION_CALC_EMBEDDED_NOTICE DAO.
     */
    @Autowired
    private IndexationCalcDao indexationCalcDao;
    
    /**
     * Updates the columns of the CMR_CELLAR_RESOURCE_MD table associated
     * with a resource's embedded notice when such requests have been generated.
     * @param indexationCalcBatch the batch of embedded notice indexing synchronization requests
     * related to a specific base Cellar-ID.
     */
    @Concurrent(locker = OffIngestionOperationType.EMBEDDED_NOTICE_SYNCHRONIZATION)
    @Transactional
    public void execute(String batchCellarBaseId, List<IndexationCalc> indexationCalcs) {
    	if (CollectionUtils.isNotEmpty(indexationCalcs)) {
			// For each entry of the batch, update the embedded-notice-related columns
			// of the corresponding resource in CMR_CELLAR_RESOURCE_MD.
    		for (IndexationCalc indexationCalc : indexationCalcs) {
    			CellarResource cellarResource = cellarResourceDao.findCellarId(indexationCalc.getCellarId());
    			if (cellarResource != null) {
    	            cellarResource.setEmbeddedNotice(indexationCalc.getEmbeddedNotice());
    	            cellarResource.setEmbeddedNoticeCreationDate(indexationCalc.getEmbeddedNoticeCreationDate());
    	            cellarResourceDao.updateObject(cellarResource);
    	            // Delete the batch entry since it is not needed anymore.
    	            indexationCalcDao.deleteObject(indexationCalc);
    	        }
        	}
    		LOG.info("{} embedded notice indexing requests related to Cellar-ID [{}] have been synchronized.",
    				indexationCalcs.size(), indexationCalcs.get(0).getCellarBaseId());
    	}
    }

    /**
     * Provides the initial ConcurrencyController locking argument(s),
     * which in this case is the Base-Cellar-Id associated to the given batch.
     */
    @Override
    public Object[] resolveArgsForConcurrency(Object[] in) {
        return new Object[]{((String) in[0])};
    }
    
}
