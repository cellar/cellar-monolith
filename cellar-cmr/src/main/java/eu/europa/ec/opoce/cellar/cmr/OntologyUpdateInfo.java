package eu.europa.ec.opoce.cellar.cmr;

import eu.europa.ec.opoce.cellar.common.CellarCatalogResolver;
import eu.europa.ec.opoce.cellar.semantic.CatalogUriResolver;
import eu.europa.ec.opoce.cellar.semantic.FallbackUriResolver;
import eu.europa.ec.opoce.cellar.semantic.UriResolver;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.springframework.core.io.Resource;

/**
 * <p>OntologyUpdateInfo class.</p>
 */
public class OntologyUpdateInfo {

    private final String updatedOntologyUri;
    private final Map<String, Resource> updates = new HashMap<String, Resource>();

    /**
     * <p>Constructor for OntologyUpdateInfo.</p>
     *
     * @param updatedOntologyUri a {@link java.lang.String} object.
     */
    public OntologyUpdateInfo(String updatedOntologyUri) {
        this.updatedOntologyUri = updatedOntologyUri;
    }

    /**
     * <p>Getter for the field <code>updatedOntologyUri</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getUpdatedOntologyUri() {
        return updatedOntologyUri;
    }

    /**
     * <p>getUpdatedUris.</p>
     *
     * @return an array of {@link java.lang.String} objects.
     */
    public String[] getUpdatedUris() {
        Set<String> updatedUris = updates.keySet();
        return updatedUris.toArray(new String[updatedUris.size()]);
    }

    /**
     * <p>addUpdatedUri.</p>
     *
     * @param updatedUri a {@link java.lang.String} object.
     * @param resource   a {@link org.springframework.core.io.Resource} object.
     */
    public void addUpdatedUri(String updatedUri, Resource resource) {
        updates.put(updatedUri, resource);
    }

    /**
     * <p>getUriResolver.</p>
     *
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.UriResolver} object.
     */
    public UriResolver getUriResolver() {
        return new UpdateUriResolver();
    }

    private class UpdateUriResolver extends FallbackUriResolver {

        private UpdateUriResolver() {
            super(new CatalogUriResolver(CellarCatalogResolver.adapt(CellarCatalogResolver.getInstance())));
        }

        @Override
        protected Resource tryResolverUri(String uri) {
            return updates.get(uri);
        }
    }

}
