/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.cmr.ingest.impl
 *        FILE : AbstractMetadataCalculator.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 11-04-2013
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.ingest.impl;

import eu.europa.ec.opoce.cellar.cmr.*;
import org.apache.jena.graph.Triple;
import org.apache.jena.query.QuerySolutionMap;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.vocabulary.RDF;
import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData;
import eu.europa.ec.opoce.cellar.cmr.ingest.IMetadataCalculator;
import eu.europa.ec.opoce.cellar.cmr.transformers.DigitalObject2Context;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.domain.content.mets.OperationSubType;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.CellarValidationException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.monitoring.Watch;
import eu.europa.ec.opoce.cellar.nal.service.DecodingSnippetService;
import eu.europa.ec.opoce.cellar.ontology.service.OntologyService;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.semantic.jena.template.JenaQueryTemplate;
import eu.europa.ec.opoce.cellar.semantic.jena.template.JenaQueryUtils;
import eu.europa.ec.opoce.cellar.semantic.jena.template.ListOfMapLoopHandlerWithoutResult;
import eu.europa.ec.opoce.cellar.semantic.jena.transformers.RdfNode2Uri;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.EnumMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page. <br/>
 * <br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc. <br/>
 * <br/>
 * ON : 11-04-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class MetadataCalculator implements IMetadataCalculator {

    private static final Logger LOG = LogManager.getLogger(MetadataCalculator.class);

    /** The snippet extractor service. */
    @Autowired
    protected SnippetExtractorService snippetExtractorService;

    /** The decoding snippet service. */
    @Autowired
    protected DecodingSnippetService decodingSnippetService;

    /** The structured query configuration. */
    @Autowired
    protected StructuredQueryConfiguration structuredQueryConfiguration;

    /** The inverse calculation service. */
    @Autowired
    protected InverseCalculationService inverseCalculationService;

    /** The ontology service. */
    @Autowired
    protected OntologyService ontologyService;

    /** The inference service. */
    @Autowired
    protected InferenceService inferenceService;

    /** The cellar configuration. */
    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    @Watch(value = "Metadata Calculator Create", arguments = {
            @Watch.WatchArgument(name = "cellar ID", expression = "data.rootCellarId")
    })
    @Override
    public void calculate(final CalculatedData data) {
        // extract from the business metadata-model the model for each digital
        // object
        this.addAllMetadataSnippets(data);

        // add the decoding and inverse models to the calculated data for each
        // digital object
        this.addDecodingAndInverseSnippets(data);

        // recalculate the embedded notices if they are changed
        this.calculateObjectsWithChangedEmbedded(data);
    }

    /**
     * <p>
     * calculateObjectsWithChangedEmbedded.
     * </p>
     *
     * @param data the data
     */
    private void calculateObjectsWithChangedEmbedded(final CalculatedData data) {
        // create a map of all triples that could be found from an earlier
        // ingest
        final Map<String, Collection<Triple>> existingTriples = data.getExistingDirectAndInferredModel().asTriplesPerContext();

        // check if there are new or removed triples in the new model which are
        // annotated to be used in the embedded notice
        final Map<DigitalObject, Model> directAndInferredSnippetsForOracle = data.getDirectAndInferredSnippetsForOracle();
        for (final Map.Entry<DigitalObject, Model> digitalObjectModelEntry : directAndInferredSnippetsForOracle.entrySet()) {
            final String context = DigitalObject2Context.instance.transform(digitalObjectModelEntry.getKey());
            if (this.isChangedEmbedded(digitalObjectModelEntry.getValue(), existingTriples.get(context))) {
                data.getObjectsWithChangedEmbedded().add(digitalObjectModelEntry.getKey());
            }
        }
    }

    /**
     * <p>
     * isChangedEmbedded indicates that there was a change or no change in the
     * triples that make up the embedded notice.
     * </p>
     *
     * @param model
     *            a {@link org.apache.jena.rdf.model.Model} object.
     * @param triples
     *            a {@link java.util.List} object.
     * @return a boolean.
     */
    private boolean isChangedEmbedded(final Model model, final Collection<Triple> triples) {
        // get properties that are annotated to be embedded
        final SortedSet<String> embeddedProperties = ontologyService.getWrapper().getOntologyData().getEmbeddedProperties();
        // filter that only let the triples trough that have a property in the
        // list above
        final Predicate<Triple> filterEmbeddedProperties = object -> embeddedProperties.contains(object.getPredicate().getURI());

        // make a Set of all embedded triples in the old data
        Set<Triple> oldTriples;
        if (triples == null) {
            oldTriples = new HashSet<>();
        } else {
            oldTriples = new HashSet<>(triples);
            CollectionUtils.filter(oldTriples, filterEmbeddedProperties);
        }

        // make a Set of all embedded triples in the new data
        final Set<Triple> newTriples = model.getGraph().find(null, null, null).toSet();
        CollectionUtils.filter(oldTriples, filterEmbeddedProperties);

        // return it these sets have a element in one that can not be found in
        // the other
        return CollectionUtils.disjunction(oldTriples, newTriples).isEmpty();
    }

    /**
     * <p>
     * addAllMetadataSnippets.
     * </p>
     * <p>
     * calculate starting from the business metadata what the different snippets
     * are for all the digital objects, this for S3 and oracle
     * </p>
     * <p>
     * remark that these models are different so that a triple in oracle for
     * sure only once is added
     * </p>
     *
     * @param data the data
     */
    private void addAllMetadataSnippets(final CalculatedData data) {

        // calculate the uris on which the extraction can stop
        final Collection<String> stopUris = this.snippetExtractorService
                .getStopUris(data.getBusinessMetadata().get(ContentType.DIRECT_INFERRED));
        for (final DigitalObject object : data.getStructMap().getDigitalObject().getAllChilds(true)) {
            final boolean ignoreNull = (object.getStructMap().getContentOperationType() != null)
                    && (object.getType() == DigitalObjectType.MANIFESTATION);

            final boolean isStructMapDeleteOperation = data.getStructMap().resolveOperationSubType() == OperationSubType.Delete;

            // do the calculation if
            // - the content changed
            // - the business metadata is filled in
            // - a parent or child had new metadata
            // - we are on a delete operation
            if (ignoreNull || (null != object.getBusinessMetadata()) || couldHaveBeenChanged(object) || isStructMapDeleteOperation) {
                final Set<String> uris = object.getAllUris();

                // Allows to prevent the adding of triples containing RDF:type
                // property for parent and children
                final Set<String> urisParentAndChildren = new HashSet<>();
                for (final DigitalObject obj : object.getChildObjects()) {
                    urisParentAndChildren.addAll(obj.getAllUris());
                }
                final DigitalObject parentObject = object.getParentObject();
                if (parentObject != null) {
                    urisParentAndChildren.addAll(parentObject.getAllUris());
                }
                final boolean isManifestation = DigitalObjectType.MANIFESTATION.equals(object.getType());

                // calculate the snippet for all the S3 metadata streams,
                // starting from the big model, and put them into the S3 map
                final Map<ContentType, Model> s3Snippets = new EnumMap<>(
                        ContentType.class);
                for (final Map.Entry<ContentType, Model> contentTypeModelEntry : data.getBusinessMetadata().entrySet()) {
                    s3Snippets.put(contentTypeModelEntry.getKey(), this.snippetExtractorService.getSnippet(
                            contentTypeModelEntry.getValue(), uris, stopUris, true, urisParentAndChildren, isManifestation));
                }
                data.getMetadataSnippetsForS3().put(object, s3Snippets);

                // put the direct snippets for RDF store in the map after
                // removing extra information like same_as of targets
                final Model rdfStoreDirectAndInferredSnippets = this.snippetExtractorService.getSnippet(
                        data.getBusinessMetadata().get(ContentType.DIRECT_INFERRED), uris, stopUris, false, urisParentAndChildren,
                        isManifestation);
                data.getDirectAndInferredSnippetsForOracle().put(object, rdfStoreDirectAndInferredSnippets);
            }
        }
    }

    /**
     * <p>
     * getConceptSnippet.
     * </p>
     *
     * @param digitalObject
     *            a
     *            {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject}
     *            object.
     * @param metadataSnippet
     *            a {@link org.apache.jena.rdf.model.Model} object.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    private Model getConceptSnippet(final DigitalObject digitalObject, final Model metadataSnippet) {
        Model conceptModel = null;
        try {
            final QuerySolutionMap initialBinding = new QuerySolutionMap();
            conceptModel = JenaQueryTemplate.construct(metadataSnippet,
                    this.structuredQueryConfiguration.resolveConstructConceptUrisQuery(), initialBinding);
            if ((conceptModel == null) || conceptModel.isEmpty()) {
                LOG.debug("No concepts found in metadata of resource '{}'", digitalObject.getCellarId().getUri());
                conceptModel = ModelFactory.createDefaultModel();
            }
            final boolean cellarServiceNalLoadDecodingValidationEnabled = cellarConfiguration
                    .isCellarServiceNalLoadDecodingValidationEnabled();
            if (cellarServiceNalLoadDecodingValidationEnabled) {
                this.checkAllConceptsDoExist(digitalObject, conceptModel);
            }

            return conceptModel;
        } catch (final RuntimeException e) {
            JenaQueryUtils.closeQuietly(conceptModel);
            throw e;
        }
    }

    /**
     * Integrity check.
     *
     * @param digitalObject            a
     *            {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject}
     *            object.
     * @param conceptModel            a {@link org.apache.jena.rdf.model.Model} object.
     */
    private void checkAllConceptsDoExist(final DigitalObject digitalObject, final Model conceptModel) {
        final Set<String> concepts = new HashSet<>();
        JenaQueryTemplate.select(conceptModel, this.structuredQueryConfiguration.resolveListConceptUrisQuery(),
                new ListOfMapLoopHandlerWithoutResult() {

                    @Override
                    protected void process(final Map<String, RDFNode> result) {
                        final RDFNode rdfNode = result.get("concept");
                        final String transform = RdfNode2Uri.instance.transform(rdfNode);
                        concepts.add(transform);
                    }
                });

        LOG.info("{} concepts found in metadata of resource '{}': {}", String.valueOf(concepts.size()),
                digitalObject.getCellarId().getUri(), StringUtils.join(concepts, ","));

        final Collection<String> conceptsWithoutDecoding = this.decodingSnippetService.getConceptsWithoutDecoding(concepts);
        if (!conceptsWithoutDecoding.isEmpty()) {
            throw ExceptionBuilder.get(CellarValidationException.class).withCode(CmrErrors.VALIDATION_DECODING_OF_CONCEPT_NOT_FOUND)
                    .withMessage("Integrity check failed! Concepts without decoding for resource '{}': {}")
                    .withMessageArgs(digitalObject.getCellarId().getUri(), StringUtils.join(conceptsWithoutDecoding, ",")).build();
        }
    }

    /**
     * <p>
     * addDecodingAndInverseSnippets.
     * </p>
     * <p>
     * add for each digital object the decoding and inverse streams for oracle
     * and S3.
     * </p>
     *
     * @param data the data
     */
    private void addDecodingAndInverseSnippets(final CalculatedData data) {
        for (final Map.Entry<DigitalObject, Map<ContentType, Model>> digitalObjectMapEntry : data.getMetadataSnippetsForS3()
                .entrySet()) {
            // add the decoding model to the digital object, on the basis of the
            // DMD_INDEX model of the digital object
            final DigitalObject digitalObject = digitalObjectMapEntry.getKey();
            final Map<ContentType, Model> map = digitalObjectMapEntry.getValue();
            final Model inferredModel = map.get(ContentType.DIRECT_INFERRED);
            if (inferredModel == null) {
                throw ExceptionBuilder.get(CellarException.class).build();
            }
            map.put(ContentType.DECODING, this.getConceptSnippet(digitalObject, inferredModel));

            // add the inverse model to the digital object, on the basis of the
            // DMD_INDEX model of the digital object
            final Model originalModel = map.get(ContentType.DIRECT);
            if (originalModel == null) {
                throw ExceptionBuilder.get(CellarException.class).build();
            }
            Model inverseModel = null;
            Model inverseWithSuperProperties = null;
            try {
                // calculate the basic inverse model
                inverseModel = this.inverseCalculationService.getInverseModel(originalModel);
                // add types in the inverse model
                addTypes(inverseModel, digitalObject, inferredModel);
                // infer properties so all triples with the super properties are
                // also in the model
                inverseWithSuperProperties = this.inferenceService.calculateInferredSuperpropertyModel(inverseModel);
                // put the so-calculated inverse model onto the map
                final Model model = map.get(ContentType.SAME_AS);
                final Model create = JenaUtils.create(inverseWithSuperProperties, model);
                map.put(ContentType.INVERSE, create);
                data.getInverseSnippetsForOracle().put(digitalObject, create);
            } finally {
                JenaUtils.closeQuietly(inverseModel, inverseWithSuperProperties);
            }
        }
    }

    /**
     * <p>
     * addTypes add all the RDF-types associated with the given digital object
     * to the inverse model.
     * </p>
     *
     * @param inverseModel
     *            a {@link org.apache.jena.rdf.model.Model} object.
     * @param digitalObject
     *            a
     *            {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject}
     *            object.
     * @param inferredModel
     *            a {@link org.apache.jena.rdf.model.Model} object.
     */
    private static void addTypes(final Model inverseModel, final DigitalObject digitalObject, final Model inferredModel) {
        final Resource resourceCellar = ResourceFactory.createResource(digitalObject.getCellarId().getUri());
        for (final RDFNode rdfNode : inferredModel.listObjectsOfProperty(resourceCellar, RDF.type).toSet()) {
            inverseModel.add(resourceCellar, RDF.type, rdfNode);
        }
        for (final ContentIdentifier contentIdentifier : digitalObject.getContentids()) {
            final Resource resourcePs = ResourceFactory.createResource(contentIdentifier.getUri());
            for (final RDFNode rdfNode : inferredModel.listObjectsOfProperty(resourcePs, RDF.type).toSet()) {
                inverseModel.add(resourcePs, RDF.type, rdfNode);
            }
        }
    }

    /**
     * <p>
     * couldHaveBeenChanged indicates if a parent or child of this object has
     * new metadata given from the input.
     * </p>
     *
     * @param object
     *            a
     *            {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject}
     *            object.
     * @return a boolean.
     */
    private static boolean couldHaveBeenChanged(final DigitalObject object) {

        if ((object.getParentObject() != null) && (object.getParentObject().getBusinessMetadata() != null)) {
            return true;
        }
        for (final DigitalObject digitalObject : object.getChildObjects()) {
            if (digitalObject.getBusinessMetadata() != null) {
                return true;
            }
        }
        return false;

        /*boolean result = false;
        
        DigitalObject parentObject = object.getParentObject();
        while (parentObject != null) {
            if ((parentObject.getBusinessMetadata() != null)) {
                result = true;
                break;
            } else {
                parentObject = parentObject.getParentObject();
            }
        }
        if (!result) {
            result = object.getAllChilds(false).stream().anyMatch(element -> element.getBusinessMetadata() != null);
        }
        return result;*/
    }
}
