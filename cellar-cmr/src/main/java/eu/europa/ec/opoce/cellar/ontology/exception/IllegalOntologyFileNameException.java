/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ontology
 *             FILE : IllegalOntologyFileNameException.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 01 11, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-01-11 15:10:54 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ontology.exception;

/**
 * @author ARHS Developments
 */
public class IllegalOntologyFileNameException extends OntologyException {

    public IllegalOntologyFileNameException(String message) {
        super(message);
    }
}
