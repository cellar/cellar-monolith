/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.xml
 *             FILE : XmlObjectRedirectResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 4, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.xml;

import eu.europa.ec.opoce.cellar.common.http.headers.IURLTokenable;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.server.dissemination.AcceptLanguage;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.Structure;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.IDisseminationResolver;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;

import java.util.List;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Apr 4, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class XmlObjectRedirectResolver extends XmlRedirectResolver {

    /**
     * Instantiates a new xml object redirect resolver.
     *
     * @param cellarResource the cellar resource
     * @param decoding the decoding
     * @param acceptLanguage the accept language
     * @param provideAlternates the provide alternates
     */
    protected XmlObjectRedirectResolver(final CellarResourceBean cellarResource, final String decoding, final AcceptLanguage acceptLanguage,
            final boolean provideAlternates) {
        super(cellarResource, decoding, acceptLanguage, provideAlternates);
    }

    /**
     * Gets the.
     *
     * @param cellarResource the cellar resource
     * @param decoding the decoding
     * @param acceptLanguage the accept language
     * @param provideAlternates the provide alternates
     * @return the i dissemination resolver
     */
    public static IDisseminationResolver get(final CellarResourceBean cellarResource, final String decoding,
            final AcceptLanguage acceptLanguage, final boolean provideAlternates) {
        return new XmlObjectRedirectResolver(cellarResource, decoding, acceptLanguage, provideAlternates);
    }

    /** {@inheritDoc} */
    @Override
    protected void initDisseminationRequest() {
        super.initDisseminationRequest();
    }

    /** {@inheritDoc} */
    @Override
    protected void doHandleWorkDisseminationRequest() {

        if (this.hasAcceptLanguageBean()) {
            this.retrieveTargetExpressions();
        }

        this.setDecodingLanguage();
    }

    /** {@inheritDoc} */
    @Override
    protected void doHandleDossierDisseminationRequest() {
        this.setDecodingLanguage();
    }

    /** {@inheritDoc} */
    @Override
    protected void doHandleAgentDisseminationRequest() {
        this.setDecodingLanguage();
    }

    /** {@inheritDoc} */
    @Override
    protected void doHandleTopLevelEventDisseminationRequest() {
        this.setDecodingLanguage();
    }

    /** {@inheritDoc} */
    @Override
    protected void doHandleExpressionDisseminationRequest() {
        @SuppressWarnings("unused")
        final List<LanguageBean> languageBeans = this.getLanguageBeanOfExpression(this.cellarResource);

        this.setDecodingLanguage();
    }

    /** {@inheritDoc} */
    @Override
    protected void doHandleEventDisseminationRequest() {
        this.setDecodingLanguage();
    }

    /** {@inheritDoc} */
    @Override
    protected void doHandleManifestationDisseminationRequest() {
        final CellarResourceBean expression = this.disseminationService.checkForParentExpression(this.cellarResource);
        @SuppressWarnings("unused")
        final List<LanguageBean> languageBeans = this.getLanguageBeanOfExpression(expression);

        this.setDecodingLanguage();
    }

    /** {@inheritDoc} */
    @Override
    protected IURLTokenable getTypeStructure() {
        return Structure.OBJECT;
    }

}
