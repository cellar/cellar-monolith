package eu.europa.ec.opoce.cellar.nal.service;

import eu.europa.ec.opoce.cellar.nal.domain.Concept;
import eu.europa.ec.opoce.cellar.nal.domain.ConceptScheme;
import eu.europa.ec.opoce.cellar.nal.domain.Domain;
import eu.europa.ec.opoce.cellar.nal.domain.Language;

import java.util.Date;

/**
 * <p>NalApi interface.</p>
 *
 * @author Pieter.Fannes
 */
// TODO implement modified since logic
// TODO add lastModified attribute and the optional version and date attributes to ConceptScheme
// TODO conceptSchemes and lastModified date logic
// TODO related logic supportedLanguages (for domains and conceptschemes do only return labels of supported languages,...)
// TODO fallback labels for domains and conceptSchemes
// TODO EUROVOC API getTopConcepts: What if speicified conceptScheme argument is the EUROVOC concept scheme
public interface NalApi {

    /**
     * Returns all the languages in which the concept scheme is currently available.
     * The result list is defaulted to the required languages:
     * - when the concept schema URI is not provided or
     * - when the concept schema does not supply itself the list of supported languages.
     *
     * @param conceptScheme OPTIONAL
     * @return an array of {@link Language} objects.
     */
    Language[] getSupportedLanguages(String conceptScheme);

    /**
     * Returns the conceptScheme. Method is used to ask for a specific skos:ConceptScheme
     *
     * @param conceptScheme uri argument is mandatory
     * @return a {@link ConceptScheme} object.
     */
    ConceptScheme getConceptScheme(String conceptScheme);

    /**
     * Returns all conceptScheme's
     * Note: For EUROVOC this service returns the EUROVOC concept scheme and all MicroThesaurus Concept scheme
     *
     * @param if_modified_since Optional. Filters concept scheme that where modified in CELLAR after the specified date-time.
     *                          See ConceptScheme field lastModified
     * @return an array of {@link ConceptScheme} objects.
     */
    ConceptScheme[] getConceptSchemes(Date if_modified_since);

    /**
     * Get the top concepts of the conceptScheme
     *
     * @param conceptScheme uri argument is mandatory
     * @param language      argument is mandatory
     * @return an array of {@link Concept} objects.
     */
    Concept[] getTopConcepts(String conceptScheme, String language);

    /**
     * Returns a list of concepts having a specific semantic relation with the given concept.
     * <p/>
     * Note: with URI %escaping skos:broader becomes http://www.w3.org/2004/02/skos/core%23broader
     *
     * @param concept  uri argument is mandatory
     * @param relation uri argument is mandatory. Typical values are:
     *                 - http://www.w3.org/2004/02/skos/core#broader
     *                 - http://www.w3.org/2004/02/skos/core#narrower
     *                 - http://www.w3.org/2004/02/skos/core#related
     * @param language argument is mandatory
     * @return an array of {@link Concept} objects.
     */
    Concept[] getConceptRelatives(String concept, String relation, String language);

    /**
     * Get the translation of a given concept into a specified language.
     *
     * @param concept  uri argument is mandatory
     * @param language argument is mandatory
     * @return a {@link Concept} object.
     */
    Concept getConcept(String concept, String language);

    /**
     * Get The Domains facets of the EUROVOC thesaurus
     *
     * @return an array of {@link Domain} objects.
     */
    Domain[] getDomains();
}
