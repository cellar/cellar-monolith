/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.metsbeans
 *             FILE : Content.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 20, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.metsbeans;

import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;

import java.util.Map;

/**
 * <class_description> Content stream mets bean.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 20, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class Content extends HierarchyElement<Manifestation, MetsElement> {

    private static final long serialVersionUID = 5690290535012197946L;

    protected boolean isVisible;

    public Content() {
        super(DigitalObjectType.ITEM);
    }

    public Content(Map<ContentType, String> versions) {
        super(DigitalObjectType.ITEM, versions);
    }

    /**
     * Indicate if the content stream is visible.
     */
    public boolean isVisible() {
        return this.isVisible;
    }

    /**
     * Set the visibility of the content stream.
     * @param visible the visibility to set
     */
    public void setVisible(final boolean visible) {
        this.isVisible = visible;
    }

}
