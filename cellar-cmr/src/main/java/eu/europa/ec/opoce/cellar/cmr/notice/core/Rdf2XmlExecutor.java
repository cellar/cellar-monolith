/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice.core
 *             FILE : Rdf2XmlExecutor.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 24 Apr 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.notice.core;

import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.utils.XmlNoticeUtils;
import eu.europa.ec.opoce.cellar.common.util.MapUtils;
import eu.europa.ec.opoce.cellar.ontology.service.OntologyService;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaQueries;
import eu.europa.ec.opoce.cellar.semantic.ontology.data.FallbackOntologyData;
import eu.europa.ec.opoce.cellar.semantic.ontology.data.OntologyData;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import eu.europa.ec.opoce.cellar.semantic.pattern.Command;
import eu.europa.ec.opoce.cellar.semantic.xml.XmlBuilder;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.OWL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author ARHS Developments
 */
@Configurable
public class Rdf2XmlExecutor implements Command<XmlBuilder> {

    @Autowired
    @Qualifier("pidManagerService")
    private IdentifierService identifierService;

    @Autowired
    private OntologyService ontologyService;

    @Autowired
    private Rdf2XmlService rdf2XmlService;

    private final Rdf2Xml rdf2Xml;

    public Rdf2XmlExecutor(Rdf2Xml rdf2Xml) {
        this.rdf2Xml = rdf2Xml;
    }

    @Override
    public XmlBuilder execute() {
        final OntologyData ontologyData = ontologyService.getWrapper().getOntologyData();
        final FallbackOntologyData fallbackOntologyData = ontologyService.getWrapper().getFallbackOntologyData();
        final ShouldHandlePropertyVisitor shouldHandlePropertyVisitor
                = new ShouldHandlePropertyVisitor(rdf2Xml, ontologyData, fallbackOntologyData);
        final ShouldExpandPropertyVisitor shouldExpandPropertyVisitor = new ShouldExpandPropertyVisitor(ontologyData);
        final String subjectUri = rdf2Xml.getSubjectUri();
        final Model model = rdf2Xml.getModel();
        final Resource subject = model.getResource(subjectUri);
        rdf2Xml.setAllSubjectResources(XmlNoticeUtils.getSameAsResources(subject));
        final Set<Resource> allSubjectResources = rdf2Xml.getAllSubjectResources();
        final Set<Statement> nfSubjectStatements = XmlNoticeUtils.getAllStatements(subject, allSubjectResources);
        final Set<Statement> subjectStatements = filterStatements(nfSubjectStatements, model);
        final XmlBuilder rootXmlBuilder = rdf2Xml.getRootXmlBuilder();
        if (rdf2Xml.isWriteSubjectInfo()) {
            rdf2XmlService.writeUriAndSameAs(rootXmlBuilder, subject, allSubjectResources, model);
        }

        allSubjectResources.add(subject);
        final NodeHandler nodeHandler = new NodeHandler(rdf2Xml, shouldHandlePropertyVisitor, shouldExpandPropertyVisitor,
                ontologyData, fallbackOntologyData);
        for (final Statement statement : subjectStatements) {

            nodeHandler.setCurrent(rootXmlBuilder, statement.getPredicate());

            shouldHandlePropertyVisitor.setCurrentPropertyUri(nodeHandler.getCurrentPropertyUri());
            final NoticeType noticeType = rdf2Xml.getNoticeType();
            if (noticeType.accept(shouldHandlePropertyVisitor)) {
                statement.getObject().visitWith(nodeHandler);
            } else if (nodeHandler.getCurrentProperty().equals(CellarProperty.cdm_memberListP)) {
                final String resourceUri = JenaQueries.getResourceUri(statement.getResource(), CellarProperty.cdm_listedPropertyP, true);
                shouldHandlePropertyVisitor.setCurrentPropertyUri(resourceUri);
                if (noticeType.accept(shouldHandlePropertyVisitor)) {
                    statement.getObject().visitWith(nodeHandler);
                }
            }

        }

        return rootXmlBuilder;
    }

    public Set<Statement> filterStatements(final Set<Statement> allStatements, final Model model) {
        final Map<Property, Map<RDFNode, Statement>> filteredStatements = new HashMap<>();
        for (final Statement statement : allStatements) {
            final Map<RDFNode, Statement> rdfNodeStatementMap = MapUtils.addMappedMap(filteredStatements, statement.getPredicate());
            final Set<Resource> resources = model.listSubjectsWithProperty(OWL.sameAs, statement.getObject()).toSet();

            if (resources.isEmpty()) {
                rdfNodeStatementMap.put(statement.getObject(), statement);
            } else {
                final Resource resource = resources.iterator().next();
                final String prefixedOrNull = identifierService.getPrefixedOrNull(resource.getURI());
                if (StringUtils.isEmpty(prefixedOrNull) || !prefixedOrNull.startsWith("cellar:")) {
                    rdfNodeStatementMap.put(statement.getObject(), statement);
                } else {
                    rdfNodeStatementMap.put(resource, statement);
                }
            }
        }

        final Set<Statement> statements = new HashSet<>();
        for (final Map<RDFNode, Statement> rdfNodeStatementMap : filteredStatements.values()) {
            statements.addAll(rdfNodeStatementMap.values());
        }
        return statements;
    }
}
