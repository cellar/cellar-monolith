/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.service
 *             FILE : IRetrieveOJListService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Aug 8, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.service;

import eu.europa.ec.opoce.cellar.cmr.oj.OfficialJournals;

/**
 * <class_description> This class contains the method requests to retrieve data from the daily oj.
 * <br/><br/>
 * <notes> .
 * <br/><br/>
 * ON : Aug 8, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IRetrieveOJListService {

    /**
     * Gets the the date of the most recent oj.
     *
     * @param year the year
     * @return the the date of the most recent oj
     */
    OfficialJournals getTheDateOfTheMostRecentOj(final int year);

    /**
     * Gets the the list of oj series.
     *
     * @param year the year
     * @return the the list of oj series
     */
    OfficialJournals getTheListOfOjSeries(final int year);

    /**
     * Gets the the list of oj sub series.
     *
     * @param year the year
     * @return the the list of oj sub series
     */
    OfficialJournals getTheListOfOjSubSeries(final int year);

    /**
     * List the daily oj per date.
     *
     * @param year the year
     * @param month the month
     * @param day the day
     * @return the official journals
     */
    OfficialJournals list(final int year, final int month, final int day);

    /**
     * List the daily oj per series,sub-series and number for a specific year.
     *
     * @param year the year
     * @param series the series
     * @param subseries the subseries
     * @param number the number
     * @return the official journals
     */
    OfficialJournals list(final int year, final String series, final String subseries, final int number);
}
