/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ontology
 *             FILE : OntologyChangedEvent.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 01 11, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-01-11 16:28:11 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ontology.domain;

import java.util.Objects;

/**
 * @author ARHS Developments
 */
public class OntologyChangedEvent {

    private final String uri;

    public OntologyChangedEvent(String uri) {
        this.uri = uri;
    }

    public String getUri() {
        return uri;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OntologyChangedEvent)) return false;
        OntologyChangedEvent that = (OntologyChangedEvent) o;
        return Objects.equals(getUri(), that.getUri());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUri());
    }

    @Override
    public String toString() {
        return "OntologyChangedEvent{" +
                "uri='" + uri + '\'' +
                '}';
    }
}
