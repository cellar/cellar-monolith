package eu.europa.ec.opoce.cellar.nal.domain;

/**
 * <p>Language class.</p>
 */
public class Language {

    private String code;

    /**
     * <p>Constructor for Language.</p>
     *
     * @param code a {@link java.lang.String} object.
     */
    public Language(String code) {
        this.code = code;
    }

    /**
     * <p>Getter for the field <code>code</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCode() {
        return code;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Language language = (Language) o;

        if (code != null ? !code.equals(language.code) : language.code != null)
            return false;

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return code != null ? code.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "Language{" +
                "code='" + code + '\'' +
                '}';
    }
}
