package eu.europa.ec.opoce.cellar.cmr.metsbeans;

import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;

import java.util.Collections;
import java.util.Map;

public class Manifestation extends HierarchyElement<Expression, Content> {

    private static final long serialVersionUID = -8550015487957127571L;

    public Manifestation() {
        super(DigitalObjectType.MANIFESTATION, Collections.emptyMap());
    }

    public Manifestation(Map<ContentType, String> versions) {
        super(DigitalObjectType.MANIFESTATION, versions);
    }

}
