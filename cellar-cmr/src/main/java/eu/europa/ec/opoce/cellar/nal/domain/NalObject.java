package eu.europa.ec.opoce.cellar.nal.domain;

/**
 * <p>NalObject class.</p>
 */
public class NalObject {
    public static final String APPLICATION_RDF_XML = "application/rdf+xml";
}
