package eu.europa.ec.opoce.cellar.core.dao;

import eu.europa.ec.opoce.cellar.core.exception.UncompressException;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * A <tt>FileDao</tt> is a DAO responsible to storing file. The store method is invoked to store the
 * file.
 *
 * @author dsavares
 *
 */
public interface FileDao {

    /**
     * Get all the archives from the authenticOJ reception folder.
     *
     * @return an array of {@link File} holding all the archives available if any, or an empty
     *         array.
     * @throws IOException
     *             if an I/O error occurs.
     */
    File[] getAllSipArchivesFromAuthenticOJReceptionFolder() throws IOException;

    /**
     * Get all the archives from the daily reception folder.
     *
     * @return an array of {@link File} holding all the archives available if any, or an empty
     *         array.
     * @throws IOException
     *             if an I/O error occurs.
     */
    File[] getAllSipArchivesFromDailyReceptionFolder() throws IOException;

    /**
     * Get all the archives from the bulk reception folder.
     *
     * @param maxSIPToReturn maximum sip to return in the list.
     * @return an array of {@link File} holding all the archives available if any, or an empty
     *         array.
     * @throws IOException
     *             if an I/O error occurs.
     */
    File[] getAllSipArchivesFromBulkReceptionFolder(int maxSIPToReturn) throws IOException;

    /**
     * Get all the archives from the bulkLowPriority reception folder.
     *
     * @param maxSIPToReturn maximum sip to return in the list.
     * @return an array of {@link File} holding all the archives available if any, or an empty
     *         array.
     * @throws IOException
     *             if an I/O error occurs.
     */
    File[] getAllSipArchivesFromBulkLowPriorityReceptionFolder(final int maxSIPToReturn) throws IOException;
    
    /**
     * Get a Mets {@link File} from the <i>temporary work folder</i>.
     *
     * @param folderName
     *            the name of the folder where the mets file is located.
     * @return the Mets {@link File} to retrieve.
     * @throws IOException
     *             if an I/O error occurs.
     */
    File getMetsFileFromTemporaryWorkFolder(String folderName) throws IOException;

    /**
     * Delete the working folder associated to a specific archive.
     *
     * @param folderName
     *            the name of the temporary folder to be deleted
     * @throws IOException
     *             if an I/O error occurs.
     */
    void deleteFromTemporaryWorkFolder(final String folderName) throws IOException;

    /**
     * Delete the archive from the <b>authenticOJ</b> <i>reception folder</i>.
     *
     * @param fileName
     *            the name of the archive to be deleted
     * @throws IOException
     *             if an I/O error occurs.
     */
    void deleteFromAuthenticOJReceptionFolder(final String fileName) throws IOException;

    /**
     * Delete the archive from the <b>daily</b> <i>reception folder</i>.
     *
     * @param fileName
     *            the name of the archive to be deleted
     * @throws IOException
     *             if an I/O error occurs.
     */
    void deleteFromDailyReceptionFolder(final String fileName) throws IOException;

    /**
     * Delete the archive from the <b>bulk</b> <i>reception folder</i>.
     *
     * @param fileName
     *            the name of the archive to be deleted
     * @throws IOException
     *             if an I/O error occurs.
     */
    void deleteFromBulkReceptionFolder(final String fileName) throws IOException;

    /**
     * Delete the archive from the <b>bulkLowPriority</b> <i>reception folder</i>.
     *
     * @param fileName
     *            the name of the archive to be deleted
     * @throws IOException
     *             if an I/O error occurs.
     */
    void deleteFromBulkLowPriorityReceptionFolder(final String fileName) throws IOException;
    
    /**
     * Delete the foxml from the <i>foxml folder</i>.
     *
     * @param foxmlFolderName
     *            the folder name of the foxml to be deleted
     * @throws IOException
     *             if an I/O error occurs.
     */
    void deleteFromFoxmlFolder(final String foxmlFolderName) throws IOException;

    /**
     * Uncompress a compressed archive. The archive is uncompressed in a folder with the same name
     * as the archive removing the file extension.
     *
     * @param archive
     *            the archive to uncompress,
     * @return the path of the location where the archive is uncompressed. The path is relative to
     *         the <i>temporary work folder</i>
     * @throws UncompressException if an occurs during uncompression.
     */
    String uncompress(final File archive) throws UncompressException;

    /**
     * Uncompress a compressed archive. The archive is uncompressed in a folder with the same name
     * as the archive removing the file extension. Only the metadata files (.mets.xml and .rdf) are uncompressed.
     * 
     * @param archive the archive to uncompress
     * @return the path of the location where the archive is uncompressed. The path is relative to
     *         the <i>temporary work folder</i>
     * @throws UncompressException if an occurs during uncompression.
     */
    String uncompressMetadataOnly(final File archive) throws UncompressException;
    
    /**
     * Uncompress a compressed archive without logging the possible exceptions. The archive is uncompressed in a folder with the same name
     * as the archive removing the file extension.
     *
     * @param archive
     *            the archive to uncompress,
     * @return the path of the location where the archive is uncompressed. The path is relative to
     *         the <i>temporary work folder</i>
     * @throws UncompressException if an occurs during uncompression.
     */
    String uncompressQuietly(final File archive) throws UncompressException;
    
    /**
     * Uncompress a compressed archive without logging the possible exceptions. The archive is uncompressed in a folder with the same name
     * as the archive removing the file extension. Only the metadata files (.mets.xml and .rdf) are uncompressed.
     * @param archive the archive to uncompress
     * @return the path of the location where the archive is uncompressed. The path is relative to
     *         the <i>temporary work folder</i>
     * @throws UncompressException if an occurs during uncompression.
     */
    String uncompressQuietlyMetadataOnly(final File archive) throws UncompressException;

    /**
     * Store the stream as a file in the authenticOJ reception folder.
     *
     * @param sipObjectStream
     *            the daat to store
     * @return the name of the file used to store the sipObjectStream. The name is relative to the
     *         <i>reception folder</i>.
     * @throws IOException
     *             if an I/O error occurs.
     */
    String storeArchiveInAuthenticOJReceptionFolder(byte[] sipObjectStream) throws IOException;

    /**
     * Move a file to the reception folder of type and rename-it.
     * The sip filename must be like "metsId.zip".
     *
     * @param sipFile
     *            the sip file to move
     * @param metsId
     *            the corresponding metsId of this SIP
     * @param priority
     *            the type of reception folder
     * @return the new sip filename. (should be like 'metsId.zip')
     * @throws IOException
     *             if an I/O error occurs.
     */
    File moveSIPToReceptionFolderByPriority(File sipFile,
                                              String metsId,
                                              TYPE priority) throws IOException;

    /**
     * Store the stream as a file in the daily reception folder.
     *
     * @param sipObjectStream
     *            the daat to store
     * @return the name of the file used to store the sipObjectStream. The name is relative to the
     *         <i>reception folder</i>.
     * @throws IOException
     *             if an I/O error occurs.
     */
    String storeArchiveInDailyReceptionFolder(byte[] sipObjectStream) throws IOException;

    /**
     * Gets all files from a specific location inside the <i>temporary work folder</i>.
     *
     * @param location
     *            the location where to retrieve the files. The path is relative to <i>temporary
     *            work folder</i>
     * @return the list of {@link File} contained in the location.
     * @throws IOException
     *             if an I/O error occurs.
     */
    File[] getAllFilesFromUncompressedLocation(String location) throws IOException;

    /**
     * Move the archive(SIP) to the error folder.
     *
     * @param sipFile
     *            the sip file to be moved.
     * @throws IOException
     *             if an I/O error occurs.
     */
    void moveSIPToErrorFolder(final File sipFile) throws IOException;

    /**
     * Copy the archive(SIP) to the error folder.
     *
     * @param sipFile
     *            the sip file to be copied.
     * @throws IOException
     *             if an I/O error occurs.
     */
    void copySIPToErrorFolder(final File sipFile) throws IOException;

    /**
     * Move the archive(SIP) to the backup folder.
     *
     * @param sipFile
     *            the sip file to be moved.
     * @throws IOException
     *             if an I/O error occurs.
     */
    void moveSIPToBackupFolder(final File sipFile) throws IOException;

    /**
     * Move the mets (exploded sip) to the error folder.
     *
     * @param metsFolderPath
     *            the mets folder to be moved
     * @throws IOException
     *             if an I/O error occurs.
     */
    void moveMetsToErrorFolder(final String metsFolderPath) throws IOException;

    /**
     * Move the mets (exploded sip) to the backup folder.
     *
     * @param metsFolderPath
     *            the mets folder to be moved
     * @throws IOException
     *             if an I/O error occurs.
     */
    void moveMetsToBackupFolder(final String metsFolderPath) throws IOException;

    /**
     * Move the generated foxml (for a mets) to the backup folder.
     *
     * @param foxmlFolderPath
     *            the foxml folder to be moved
     * @throws IOException
     *             if an I/O error occurs.
     */
    void moveFoxmlToBackupFolder(final String foxmlFolderPath) throws IOException;

    /**
     * Move a file to the authenticOJ reception folder and rename-it.
     * The sip filename must be like "metsId.zip".
     *
     * @param sipFile
     *            the sip file to move
     * @param metsId
     *            the corresponding metsId of this SIP
     * @return the new sip filename. (should be like 'metsId.zip')
     * @throws IOException
     *             if an I/O error occurs.
     */
    String moveSIPToAuthenticOJReceptionFolder(File sipFile, String metsId) throws IOException;

    /**
     * Move a file to the daily reception folder and rename-it.
     * The sip filename must be like "metsId.zip".
     *
     * @param sipFile
     *            the sip file to move
     * @param metsId
     *            the corresponding metsId of this SIP
     * @return the new sip filename. (should be like 'metsId.zip')
     * @throws IOException
     *             if an I/O error occurs.
     */
    String moveSIPToDailyReceptionFolder(File sipFile, String metsId) throws IOException;

    /**
     * Move a file to the bulk reception folder and rename-it.
     * The sip filename must be like "metsId.zip".
     *
     * @param sipFile
     *            the sip file to move
     * @param metsId
     *            the corresponding metsId of this SIP
     * @return the new sip filename. (should be like 'metsId.zip')
     * @throws IOException
     *             if an I/O error occurs.
     */
    String moveSIPToBulkReceptionFolder(final File sipFile, final String metsId) throws IOException;
    
    /**
     * Folder where authenticOJ mets response are written.
     * @return the response folder.
     */
    File getResponseAuthenticOJFolder();

    /**
     * Folder where daily mets response are written.
     * @return the response folder.
     */
    File getResponseDailyFolder();

    /**
     * Folder where bulk mets response are written.
     * @return the response folder.
     */
    File getResponseBulkFolder();

    /**
     * Folder where low priority bulk mets response are written.
     * @return the response folder.
     */
    File getResponseBulkLowPriorityFolder();
    
    /**
     * Folder where mets packages are exported.
     * @return the archive export folder.
     */
    File getArchiveFolderBatchJobExport();

    /**
     * Temporary folder where mets packages are exported during the batch job execution.
     * @return the temporary export folder.
     */
    File getArchiveTemporaryFolderBatchJobExport();

    /**
     * Folder where mets packages are exported (REST api).
     * @return the archive REST export folder.
     */
    File getArchiveFolderExportRest();

    /**
     * Folder where update mets packages are exported.
     * @return the archive update export folder.
     */
    File getArchiveFolderExportUpdateResponse();

    /**
     * Returns the SIP file with the provided name from the specified
     * reception folder.
     * @param sipFileName the SIP file name to retrieve.
     * @param receptionFolderType the reception folder type where the
     * SIP file is located in.
     * @return the SIP file.
     */
    File getSipFromReceptionFolder(String sipFileName, TYPE receptionFolderType);
    
    /**
     * Moves the list of provided files and folders to the response
     * folder corresponding to the supplied type.
     * @param files the list of files to move.
     * @param folderType the folder type.
     */
    void moveToResponseErrorFolder(List<File> files, TYPE folderType);
    
}
