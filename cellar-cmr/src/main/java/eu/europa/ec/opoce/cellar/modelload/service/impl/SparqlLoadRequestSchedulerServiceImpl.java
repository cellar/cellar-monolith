package eu.europa.ec.opoce.cellar.modelload.service.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration;
import eu.europa.ec.opoce.cellar.cmr.database.dao.SparqlLoadRequestRepository;
import eu.europa.ec.opoce.cellar.modelload.service.SparqlLoadRequestSchedulerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

import static eu.europa.ec.opoce.cellar.ExecutionStatus.Failure;
import static eu.europa.ec.opoce.cellar.ExecutionStatus.Pending;

@Service("sparqlLoadRequestSchedulerService")
public class SparqlLoadRequestSchedulerServiceImpl implements SparqlLoadRequestSchedulerService {

    private static final Logger LOG = LogManager.getLogger(SparqlLoadRequestSchedulerServiceImpl.class);

    private final ICellarConfiguration cellarConfiguration;
    private final SparqlLoadRequestRepository sparqlLoadRequestRepository;
    private final SparqlLoadRequestProcessor processor;

    @Autowired
    public SparqlLoadRequestSchedulerServiceImpl(ICellarConfiguration cellarConfiguration, SparqlLoadRequestRepository sparqlLoadRequestRepository,
                                                 SparqlLoadRequestProcessor processor) {
        this.cellarConfiguration = cellarConfiguration;
        this.sparqlLoadRequestRepository = sparqlLoadRequestRepository;
        this.processor = processor;
    }


    @Override
    public void scheduleSparqlLoadRequests() {
        if (cellarConfiguration.isCellarServiceSparqlLoadEnabled()) {
            sparqlLoadRequestRepository.findByActivationDateBeforeAndExecutionStatusIn(new Date(), Pending).stream()
                    .peek(r -> LOG.debug(IConfiguration.SPARQL_LOAD, "SPARQL load : {}", r))
                    .forEach(r -> {
                        try {
                            processor.process(r);
                        } catch (Exception e) {
                            LOG.error("Activation failed for " + r.getUri(), e);
                            r.setExecutionStatus(Failure);
                        }
                        sparqlLoadRequestRepository.save(r);
                    });
        }
    }

    @Override
    @Transactional
    public void restart(Long id, Date newActivationDate) {
        sparqlLoadRequestRepository.findById(id)
                .map(r -> {
                    r.setActivationDate(newActivationDate);
                    r.setExecutionStatus(Pending);
                    return r;
                })
                .ifPresent(sparqlLoadRequestRepository::save);
    }
}
