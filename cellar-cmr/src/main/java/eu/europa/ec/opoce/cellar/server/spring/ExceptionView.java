package eu.europa.ec.opoce.cellar.server.spring;

import java.io.ObjectOutputStream;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.view.AbstractView;

/**
 * <p>ExceptionView class.</p>
 */
public class ExceptionView extends AbstractView {

    private final RuntimeException exception;

    /**
     * <p>Constructor for ExceptionView.</p>
     *
     * @param exception a {@link java.lang.RuntimeException} object.
     */
    public ExceptionView(RuntimeException exception) {
        this.exception = exception;
        setContentType("application/octet-stream");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        response.setContentType(getContentType());
        response.setHeader("exception", "true");
        ServletOutputStream responseOutputStream = response.getOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(responseOutputStream);
        objectOutputStream.writeObject(exception);
        objectOutputStream.flush();
        responseOutputStream.flush();
    }
}
