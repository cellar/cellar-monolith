package eu.europa.ec.opoce.cellar.nal.domain;

import java.util.Arrays;
import java.util.Objects;

/**
 * <p>Concept class.</p>
 */
public class Concept {

    private Uri uri;

    private String language;
    private String identifier;
    private LanguageString prefLabel;
    private String[] notations;
    private String[] altLabels;
    private String[] hiddenLabels;

    /**
     * <p>Constructor for Concept.</p>
     *
     * @param uri a {@link Uri} object.
     */
    public Concept(Uri uri) {
        this.uri = uri;
    }

    /**
     * <p>Setter for the field <code>language</code>.</p>
     *
     * @param language a {@link java.lang.String} object.
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * <p>Setter for the field <code>identifier</code>.</p>
     *
     * @param identifier a {@link java.lang.String} object.
     */
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    /**
     * <p>Setter for the field <code>prefLabel</code>.</p>
     *
     * @param prefLabel a {@link LanguageString} object.
     */
    public void setPrefLabel(LanguageString prefLabel) {
        this.prefLabel = prefLabel;
    }

    /**
     * <p>Setter for the field <code>notations</code>.</p>
     *
     * @param notations an array of {@link java.lang.String} objects.
     */
    public void setNotations(String[] notations) {
        this.notations = notations;
    }

    /**
     * <p>Setter for the field <code>altLabels</code>.</p>
     *
     * @param altLabels an array of {@link java.lang.String} objects.
     */
    public void setAltLabels(String[] altLabels) {
        this.altLabels = altLabels;
    }

    /**
     * <p>Setter for the field <code>hiddenLabels</code>.</p>
     *
     * @param hiddenLabels an array of {@link java.lang.String} objects.
     */
    public void setHiddenLabels(String[] hiddenLabels) {
        this.hiddenLabels = hiddenLabels;
    }

    /**
     * <p>Getter for the field <code>uri</code>.</p>
     *
     * @return a {@link Uri} object.
     */
    public Uri getUri() {
        return uri;
    }

    /**
     * <p>Getter for the field <code>language</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLanguage() {
        return language;
    }

    /**
     * <p>Getter for the field <code>identifier</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * <p>Getter for the field <code>prefLabel</code>.</p>
     *
     * @return a {@link LanguageString} object.
     */
    public LanguageString getPrefLabel() {
        return prefLabel;
    }

    /**
     * <p>Getter for the field <code>notations</code>.</p>
     *
     * @return an array of {@link java.lang.String} objects.
     */
    public String[] getNotations() {
        return notations;
    }

    /**
     * <p>Getter for the field <code>altLabels</code>.</p>
     *
     * @return an array of {@link java.lang.String} objects.
     */
    public String[] getAltLabels() {
        return altLabels;
    }

    /**
     * <p>Getter for the field <code>hiddenLabels</code>.</p>
     *
     * @return an array of {@link java.lang.String} objects.
     */
    public String[] getHiddenLabels() {
        return hiddenLabels;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Concept)) return false;
        Concept concept = (Concept) o;
        return Objects.equals(getUri(), concept.getUri()) &&
                Objects.equals(getLanguage(), concept.getLanguage()) &&
                Objects.equals(getIdentifier(), concept.getIdentifier()) &&
                Objects.equals(getPrefLabel(), concept.getPrefLabel()) &&
                Arrays.equals(getNotations(), concept.getNotations()) &&
                Arrays.equals(getAltLabels(), concept.getAltLabels()) &&
                Arrays.equals(getHiddenLabels(), concept.getHiddenLabels());
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(getUri(), getLanguage(), getIdentifier(), getPrefLabel());
        result = 31 * result + Arrays.hashCode(getNotations());
        result = 31 * result + Arrays.hashCode(getAltLabels());
        result = 31 * result + Arrays.hashCode(getHiddenLabels());
        return result;
    }

    @Override
    public String toString() {
        return "Concept{" +
                "uri=" + uri +
                ", language='" + language + '\'' +
                ", identifier='" + identifier + '\'' +
                ", prefLabel=" + prefLabel +
                ", notations=" + Arrays.toString(notations) +
                ", altLabels=" + Arrays.toString(altLabels) +
                ", hiddenLabels=" + Arrays.toString(hiddenLabels) +
                '}';
    }
}
