/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ontology.service.impl
 *             FILE : OntologyLoader.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 03 07, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-03-07 11:59:22 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ontology.service.impl;

import eu.europa.ec.opoce.cellar.ontology.exception.OntologyException;
import eu.europa.ec.opoce.cellar.ontology.service.OntologyService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class is responsible to load the ontologies in the
 * database if none exist.
 *
 * @author ARHS Developments
 * @since 7.7
 */
@Component
class OntologyLoader {

    private static final Logger LOG = LogManager.getLogger(OntologyLoader.class);

    private final OntologyService ontologyService;
    private final List<Resource> resources;

    @Autowired
    OntologyLoader(OntologyService ontologyService, @Value("classpath:ontologies/*.*") Resource[] resources) {
        this.ontologyService = ontologyService;
        this.resources = Arrays.asList(resources);
    }

    @PostConstruct
    public void load() {
        // check if exist then load
        try {
            List<File> files = resources.stream()
                    .map(r -> new ResourceFileAdapter(r.getFilename(), r))
                    .collect(Collectors.toList());

            LOG.debug("Load files from classpath {}", files);

            // Only update if the ontologies don't exist.
            ontologyService.update(files,null ,false);
        } catch (OntologyException oe) {
            LOG.error("Failed to load ontologies", oe);
        }
    }

    static class ResourceFileAdapter extends File {

        private final Resource resource;

        ResourceFileAdapter(String pathname, Resource resource) {
            super(pathname);
            this.resource = resource;
        }

        @Override
        public boolean exists() {
            return true;
        }

        public InputStream getInputStream() throws IOException {
            return resource.getInputStream();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            if (!super.equals(o)) {
                return false;
            }
            ResourceFileAdapter that = (ResourceFileAdapter) o;
            return resource != null ? resource.equals(that.resource) : that.resource == null;
        }

        @Override
        public int hashCode() {
            int result = super.hashCode();
            result = 31 * result + (resource != null ? resource.hashCode() : 0);
            return result;
        }
    }
}
