/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr
 *             FILE : EmbargoDatabaseService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jul 21, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$$
 *          VERSION : $LastChangedRevision$$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr;

import org.apache.jena.rdf.model.Model;
import eu.europa.ec.opoce.cellar.cmr.quad.ModelQuadService;
import eu.europa.ec.opoce.cellar.database.IRDFStoreRelationalGateway;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.monitoring.Watch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Date;

import static eu.europa.ec.opoce.cellar.database.CmrTableName.CMR_METADATA;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jul 21, 2015
 *
 * @author ARHS Developments
 * @version $Revision$$
 */
@Service
public class EmbargoDatabaseServiceImpl implements EmbargoDatabaseService {

    @Autowired
    @Qualifier("modelQuadService")
    private ModelQuadService metaDataLoadService;

    @Autowired
    @Qualifier("cmrMetadataRelationalGateway")
    private IRDFStoreRelationalGateway cmrMetadataRelationalGateway;

    @Override
    public boolean isMetadataSavedPublic(final DigitalObject digitalObject) {
        return this.isMetadataSavedPublic(digitalObject.getCellarId().getIdentifier());
    }

    @Override
    public boolean isMetadataSavedPublic(final String cellarId) {
        return this.cmrMetadataRelationalGateway.isContextUsedInTable(CMR_METADATA, cellarId);
    }

    @Override
    public boolean isMetadataSavedPrivate(final DigitalObject digitalObject) {
        return this.isMetadataSavedPrivate(digitalObject.getCellarId().getIdentifier());
    }

    @Watch(value = "Is metadata private", arguments = {
            @Watch.WatchArgument(name = "cellarId", expression = "cellarId")
    })
    @Override
    public boolean isMetadataSavedPrivate(final String cellarId) {
        return this.cmrMetadataRelationalGateway.isContextUsedInTable(CMR_METADATA.getEmbargoTable(), cellarId);
    }

    @Override
    public Model getMetadata(final String cellarId, final boolean savedPrivate) {
        Model model;

        if (savedPrivate) {
            model = this.cmrMetadataRelationalGateway.selectModel(CMR_METADATA.getEmbargoTable(), cellarId);
        } else {
            model = this.cmrMetadataRelationalGateway.selectModel(CMR_METADATA, cellarId);
        }

        return model;
    }

    @Override
    public boolean savedOnlyPrivate(final String cellarId) {
        return this.isMetadataSavedPrivate(cellarId);
    }

    @Override
    public void moveEmbargoedData(final String cellarId, final boolean toPrivate, final Date embargoDate,
                                  final Date lastModificationDate) {
        this.metaDataLoadService.moveEmbargoedDataInOracle(cellarId, toPrivate, embargoDate, lastModificationDate);
    }

    @Override
    public void moveEmbargoedData(final Collection<String> cellarIds, boolean toPrivate, Date embargoDate,
                                  Date lastModificationDate) {
        this.metaDataLoadService.moveEmbargoedDataInOracle(cellarIds, toPrivate, embargoDate, lastModificationDate);
    }

}
