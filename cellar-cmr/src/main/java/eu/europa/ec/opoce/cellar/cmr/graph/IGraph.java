/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.graph
 *             FILE : IGraph.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : May 2, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.graph;

import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;

import java.util.List;

/**
 * <class_description> Interface of the graph to be protected by the concurrency controller
 * <br/><br/>
 * ON : May 2, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IGraph {

    /**
     * Return the base of the graph.
     * @return the base of the graph
     */
    List<Identifier> getObjects();

    /**
     * Return the root of the base graph.
     * @return the root of the base graph
     */
    Identifier getRoot();

    /**
     * Return the children of the base graph.
     * @return the children of the base graph
     */
    List<Identifier> getChildren();

    /**
     * Return the relations of the graph.
     * @return the relation of the graph
     */
    List<Identifier> getRelations();

    /**
     * Add a relation to an identifier within the graph.
     * @param the identifier
     * @return <code>true</code> if the add was successful, <code>false</code> otherwise
     */
    boolean addRelation(final Identifier identifier);

    /**
     * Add an identifier.
     * @param the identifier
     * @return <code>true</code> if the add was successful, <code>false</code> otherwise
     */
    boolean addObject(final Identifier identifier);

}
