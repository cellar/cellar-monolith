/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums
 *             FILE : DisseminationMethod.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 3, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 3, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public enum DisseminationMethod {
    GET(true), HEAD(false);

    private final boolean responseBodyNeeded;

    private DisseminationMethod(final boolean responseBodyNeeded) {
        this.responseBodyNeeded = responseBodyNeeded;
    }

    /**
     * @return the responseBodyNeeded
     */
    public boolean isResponseBodyNeeded() {
        return this.responseBodyNeeded;
    }
}
