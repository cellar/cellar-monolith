/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr
 *             FILE : EmbargoDatabaseService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 6 Jan 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr;

import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import org.apache.jena.rdf.model.Model;

import java.util.Collection;
import java.util.Date;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 6 Jan 2017
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface EmbargoDatabaseService {

    /**
     * <p>isMetadataSavedPublic.</p>
     *
     * @param digitalObject digital object to check
     * @return indication if metadata is saved public
     */
    boolean isMetadataSavedPublic(DigitalObject digitalObject);

    /**
     * <p>isMetadataSavedPublic.</p>
     *
     * @param cellarId cellarId object to check
     * @return indication if metadata is saved public
     */
    boolean isMetadataSavedPublic(String cellarId);

    /**
     * <p>isMetadataSavedPrivate.</p>
     *
     * @param digitalObject digital object to check
     * @return indication if metadata is saved private
     */
    boolean isMetadataSavedPrivate(DigitalObject digitalObject);

    /**
     * <p>isMetadataSavedPrivate.</p>
     *
     * @param cellarId cellarId to check
     * @return indication if metadata is saved private
     */
    boolean isMetadataSavedPrivate(String cellarId);

    /**
     * Get metadata model from Oracle RDF Store.
     * @param cellarId context cellar id
     * @param savedPrivate indicates if the cluster is under embargo
     * @return the metadata model
     */
    Model getMetadata(String cellarId, boolean savedPrivate);

    /**
     * <p>savedOnlyPrivate.</p>
     *
     * @param cellarId the cellar id
     * @return a boolean.
     */
    boolean savedOnlyPrivate(final String cellarId);

    /**
     * Move embargoed data.
     *
     * @param cellarId the cellar id
     * @param toPrivate the to private
     * @param embargoDate the embargo date
     * @param lastModificationDate the last modification date
     */
    void moveEmbargoedData(String cellarId, boolean toPrivate, Date embargoDate, Date lastModificationDate);

    /**
     * Move embargoed data sequentially.
     * The difference from the moveEmbargoedData is that this method updated a defined set of cellar ids
     *
     * @param cellarIds the cellar ids
     * @param toPrivate the to private
     * @param embargoDate the embargo date
     * @param lastModificationDate the last modification date
     */
    void moveEmbargoedData(final Collection<String> cellarIds, boolean toPrivate, Date embargoDate,
            Date lastModificationDate);

}
