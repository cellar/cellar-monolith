/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.modelload.domain
 *             FILE : ModelLoadRequest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Mar 17, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.virtuosobackup;

import eu.europa.ec.opoce.cellar.cmr.database.DaoObject;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Mar 17, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class VirtuosoBackup extends DaoObject implements Serializable {

    private static final long serialVersionUID = 8575239801995163364L;

    /** The objectId property is used to identify the object to backup:
     * - In the case of NALs or ontologies, this identifier is set to the NAL/ontology's URI;
     * - In the case of digital objects, this identifier is set to the digital object's cellar ID.
     */
    private String objectId;

    private Date insertionDate;

    private OperationType operationType;

    private boolean isResyncable;

    private String reason;

    private String model;

    private String skosModel;

    private boolean underEmbargo;

    private Date embargoDate;

    private Collection<String> productionIds;

    private BackupType backupType;

    private String ruleSetQuery;

    public VirtuosoBackup() {
    }

    /**
     * Constructor for digital objects backup
     * @param objectId cellar Id of the digital object to backup
     * @param backupType the backup type
     * @param model digital object model to backup
     * @param productionIds
     * @param insertionDate
     * @param operationType
     * @param isResyncable
     * @param reason
     */
    public VirtuosoBackup(final String objectId, final BackupType backupType, final String model, final Collection<String> productionIds,
            final Date insertionDate, final OperationType operationType, final boolean isResyncable, final String reason) {
        super();
        this.objectId = objectId;
        this.backupType = backupType;
        this.insertionDate = insertionDate;
        this.operationType = operationType;
        this.isResyncable = isResyncable;
        this.reason = reason;
        this.model = model;
        this.skosModel = null;
        this.productionIds = productionIds;
        this.ruleSetQuery = null;
    }

    /**
     * Constructor for ontologies or NALs backup
     * @param objectId
     * @param backupType
     * @param model
     * @param insertionDate
     * @param operationType
     * @param isResyncable
     * @param reason
     */
    public VirtuosoBackup(final String objectId, final BackupType backupType, final String model, final String skosModel,
            final String ruleSetQuery, final Date insertionDate, final OperationType operationType, final boolean isResyncable,
            final String reason) {
        super();
        this.objectId = objectId;
        this.backupType = backupType;
        this.model = model;
        this.skosModel = skosModel;
        this.insertionDate = insertionDate;
        this.operationType = operationType;
        this.isResyncable = isResyncable;
        this.reason = reason;
        this.ruleSetQuery = ruleSetQuery;
    }

    /**
     * @return the objectId
     */
    public String getObjectId() {
        return objectId;
    }

    /**
     * @param objectId the objectId to set
     */
    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    /**
     * @return the model
     */
    public String getModel() {
        return model;
    }

    /**
     * @return the skos model that may be associated with an ontology model
     */
    public String getSkosModel() {
        return skosModel;
    }

    /**
     * @param model the model to set
     */
    public void setModel(String model) {
        this.model = model;
    }

    /**
     * @param skosModel the SKOS model to set
     */
    public void setSkosModel(final String skosModel) {
        this.skosModel = skosModel;
    }

    /**
     * @return the insertionDate
     */
    public Date getInsertionDate() {
        return insertionDate;
    }

    /**
     * @param insertionDate the insertionDate to set
     */
    public void setInsertionDate(Date insertionDate) {
        this.insertionDate = insertionDate;
    }

    /**
     * @return the operationType
     */
    public OperationType getOperationType() {
        return operationType;
    }

    /**
     * @param operationType the operationType to set
     */
    public void setOperationType(OperationType operationType) {
        this.operationType = operationType;
    }

    public boolean isResyncable() {
        return isResyncable;
    }

    public void setResyncable(boolean isResyncable) {
        this.isResyncable = isResyncable;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * @return the underEmbargo
     */
    public boolean isUnderEmbargo() {
        return underEmbargo;
    }

    /**
     * @param underEmbargo the underEmbargo to set
     */
    public void setUnderEmbargo(boolean underEmbargo) {
        this.underEmbargo = underEmbargo;
    }

    /**
     * @return the embargoDate
     */
    public Date getEmbargoDate() {
        return embargoDate;
    }

    /**
     * @param embargoDate the embargoDate to set
     */
    public void setEmbargoDate(final Date embargoDate) {
        this.embargoDate = embargoDate;
    }

    /**
     * @return the productionIds
     */
    public Collection<String> getProductionIds() {
        return productionIds;
    }

    /**
     * @param productionIds the productionIds to set
     */
    public void setProductionIds(final Collection<String> productionIds) {
        this.productionIds = productionIds;
    }

    /**
     * @return the backup type
     */
    public BackupType getBackupType() {
        return backupType;
    }

    /**
     * @param backupType the backup type to set
     */
    public void setBackupType(final BackupType backupType) {
        this.backupType = backupType;
    }

    /**
     * @param ruleSetQuery the rule set query to set
     */
    public void setRuleSetQuery(final String ruleSetQuery) {
        this.ruleSetQuery = ruleSetQuery;
    }

    /**
     * @return the rule set query
     */
    public String getRuleSetQuery() {
        return ruleSetQuery;
    }

    public enum OperationType {

        DROP("D"), INSERT("I");

        /**
         * Find by value.
         *
         * @param value the value
         * @return the execution status
         */
        public static OperationType findByValue(final String value) {
            if (StringUtils.isBlank(value)) {
                return null;
            }

            for (final OperationType operationType : values()) {
                if (value.equals(operationType.value)) {
                    return operationType;
                }
            }

            return null;
        }

        /** The value. */
        private final String value;

        /**
         * Instantiates a new execution status.
         *
         * @param value the value
         */
        private OperationType(final String value) {
            this.value = value;
        }

        /**
         * Gets the value.
         *
         * @return the value
         */
        public String getValue() {
            return this.value;
        }
    }

}
