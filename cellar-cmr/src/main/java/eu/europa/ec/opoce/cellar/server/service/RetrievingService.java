package eu.europa.ec.opoce.cellar.server.service;

import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;

import java.util.List;
import java.util.Map;

/**
 * Service for all the methods used to retrieve path of the digital objects
 * 
 * @author dbacquel
 * 
 */
public interface RetrievingService {


    /**
     * <p>
     * Returns the list of URI of datastream
     * 
     * @param digitalObjectPID
     *            the <i>digital object</i> that contains the datastream to extract the URI from.
     * @param versions
     *            The languages order by requested priority
     * @param type
     * @param manifestationTypes
     *            The manifestationTypes order by requested priority
     * @return a string that contains the response in XML format.
     */
    List<CellarResource> getUriFromDatastream(String digitalObjectPID, Map<ContentType, String> versions, DigitalObjectType type, List<String> manifestationTypes);
}
