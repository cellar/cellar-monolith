package eu.europa.ec.opoce.cellar.cmr.notice.core;

/**
 * @author ARHS Developments
 */
public interface NoticeTypeVisitor<T> {

    T visitBranch();

    T visitIdentifier();

    T visitIndexing();

    T visitObject();

    T visitTree();

    T visitEmbeddedNotice();
}
