package eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr;

import eu.europa.ec.opoce.cellar.cmr.metsbeans.Work;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.IndexNotice;

import java.util.Collection;

public interface CmrIndexGenerationService {

    /**
     * create indexnotices in the idol autonomy table
     *
     * @param work         work that needs to be indexed. All expression, manifestations and items need to be defined in this structure
     * @param requests     the requests that need to be handled
     * @param allLanguages indication if missing languages need to be added as indexes
     * @return a {@link java.util.Collection} object.
     */
    Collection<IndexNotice> compileIndexNotice(Work work, Collection<CmrIndexRequest> requests, boolean allLanguages);

    /**
     * <p>calculateExpandedNotices.</p>
     *
     * @param work         a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Work} object.
     * @param requests     a {@link java.util.Collection} object.
     * @param allLanguages a boolean.
     * @param notices      a {@link java.util.Collection} object.
     * @return a {@link java.util.Collection} object.
     */
    Collection<IndexNotice> calculateExpandedNotices(Work work, Collection<CmrIndexRequest> requests, boolean allLanguages,
            Collection<IndexNotice> notices);

    /**
     * create indexnotices in the idol autonomy table
     *
     * @param indexRequest request to define what need to be indexed
     * @return a {@link java.util.Collection} object.
     */
    Collection<IndexNotice> compileAndSendIndexNotice(CmrIndexRequest indexRequest);

    /**
     * <p>calculateExpandedNotices.</p>
     *
     * @param indexRequest a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest} object.
     * @param notices      a {@link java.util.Collection} object.
     * @return a {@link java.util.Collection} object.
     */
    Collection<IndexNotice> calculateExpandedNotices(CmrIndexRequest indexRequest, Collection<IndexNotice> notices);

    /**
     * <p>recalculateContentStreams.</p>
     *
     * @param cellarContentStreamId a {@link java.lang.String} object.
     */
    void recalculateContentStreams(String cellarContentStreamId);

}
