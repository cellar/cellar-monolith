package eu.europa.ec.opoce.cellar.database;

import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cmr.database.util.JdbcUtils;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

import java.sql.SQLException;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import oracle.spatial.rdf.client.jena.ConnectionSetupException;
import oracle.spatial.rdf.client.jena.Oracle;
import oracle.spatial.rdf.client.jena.OraclePool;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Factory class to retrieve <code>Oracle</code> instances. This class implements the
 * {@OracleFactory} interface by retrieving <code>oracle.spatial.rdf.client.jena.Oracle</code> instances from a
 * given <code>oracle.spatial.rdf.client.jena.OraclePool</code> instance.
 *
 * @see OracleFactory
 */
@Service
public class PooledOracleFactoryService implements OracleFactory {

    @Autowired(required = true)
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    @Autowired(required = true)
    @Qualifier("cmrDataSource")
    private DataSource cmrDataSource;

    private OraclePool oracleRdfPool;

    @PostConstruct
    private void init() throws SQLException {
        this.oracleRdfPool = new OraclePool( //
                JdbcUtils.getDatabaseUrl(this.cmrDataSource), //
                JdbcUtils.getDatabaseUser(this.cmrDataSource), //
                this.cellarConfiguration.getCellarDatabaseRdfPassword() //
        );
    }

    /**
     * @see eu.europa.ec.opoce.cellar.database.OracleFactory#getOracle()
     */
    @Override
    public Oracle getOracle() {
        try {
            return this.oracleRdfPool.getOracle();
        } catch (ConnectionSetupException e) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.CMR_INTERNAL_ERROR).withCause(e).build();
        }
    }

}
