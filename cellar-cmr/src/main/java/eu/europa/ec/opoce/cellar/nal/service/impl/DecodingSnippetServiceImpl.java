package eu.europa.ec.opoce.cellar.nal.service.impl;

import com.google.common.collect.Sets;
import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.common.util.Pair;
import eu.europa.ec.opoce.cellar.database.Database;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.domain.DecodingType;
import eu.europa.ec.opoce.cellar.nal.domain.NalSnippetBean;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.nal.service.DecodingSnippetService;
import eu.europa.ec.opoce.cellar.nal.service.NalSnippetDbGateway;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.semantic.jena.template.JenaTemplate;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.riot.RDFFormat;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.support.lob.LobHandler;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.cmr_fallback;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.cmr_langP;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.cmr_prefLabelP;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.skos_altLabel;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.skos_prefLabel;

@Service
public class DecodingSnippetServiceImpl implements DecodingSnippetService {

    private static final Logger LOG = LogManager.getLogger(DecodingSnippetServiceImpl.class);

    private final static Set<String> ALWAYS_INCLUDE = Sets.newHashSet(
            CellarProperty.dc_identifier,
            CellarProperty.at_code,
            CellarProperty.cmr_level,
            CellarProperty.skos_broaderTransitive,
            CellarProperty.cmr_facet_M,
            CellarProperty.cmr_facet_D,
            CellarProperty.cmr_facet_T
    );

    private NalSnippetDbGateway nalSnippetGateway;

    @Autowired
    public DecodingSnippetServiceImpl(LobHandler lobHandler, @Qualifier("databaseCellarDataCmr") Database database) {
        nalSnippetGateway = new NalSnippetDbGatewayImpl(database, lobHandler);
    }

    /**
     * <p>Getter for the field <code>nalSnippetGateway</code>.</p>
     *
     * @return a {@link NalSnippetDbGatewayImpl} object.
     */
    private NalSnippetDbGateway getNalSnippetGateway() {
        return nalSnippetGateway;
    }

    /**
     * <p>getConceptsWithoutDecoding.</p>
     *
     * @param conceptsToDecode a {@link java.util.Collection} object.
     * @return a {@link java.util.Collection} object.
     */
    @Override
    public Collection<String> getConceptsWithoutDecoding(Collection<String> conceptsToDecode) {
        conceptsToDecode = new HashSet<>(conceptsToDecode);
        return CollectionUtils.subtract(conceptsToDecode, getNalSnippetGateway().selectExistingConcepts(conceptsToDecode));
    }

    /**
     * <p>getDecodingFor.</p>
     *
     * @param concept a {@link java.lang.String} object.
     * @param type    a {@link eu.europa.ec.opoce.cellar.nal.decoding.DecodingType} object.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    public Model getDecodingFor(String concept, DecodingType type) {
        if (StringUtils.isBlank(concept)) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.DECODING_SNIPPET_SERVICE)
                    .withMessage("Required argument concept cannot be blank!").build();
        }
        if (type == null) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.DECODING_SNIPPET_SERVICE)
                    .withMessage("Required argument type cannot be null!").build();
        }

        return getDecodingFor(Collections.singletonList(concept), type).getTwo();
    }

    /**
     * <p>getDecodingFor.</p>
     *
     * @param concepts a {@link java.util.Collection} object.
     * @param type     a {@link DecodingType} object.
     * @return a {@link eu.europa.ec.opoce.cellar.common.util.Pair} object.
     */
    @Override
    @SuppressWarnings("serial")
    public Pair<Collection<String>, Model> getDecodingFor(final Collection<String> concepts, final DecodingType type) {
        if (concepts == null) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.DECODING_SNIPPET_SERVICE)
                    .withMessage("Required argument concepts cannot be null!").build();
        }
        if (type == null) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.DECODING_SNIPPET_SERVICE)
                    .withMessage("Required argument type cannot be null!").build();
        }
        return getDecodingFor(Collections.singletonMap(type, concepts));
    }

    /**
     * <p>getDecodingFor.</p>
     *
     * @param conceptsPerDecodingType a {@link java.util.Map} object.
     * @return a {@link eu.europa.ec.opoce.cellar.common.util.Pair} object.
     */
    @Override
    public Pair<Collection<String>, Model> getDecodingFor(Map<DecodingType, Collection<String>> conceptsPerDecodingType) {
        if (conceptsPerDecodingType == null) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.DECODING_SNIPPET_SERVICE)
                    .withMessage("Required argument concepts cannot be null!").build();
        }

        Model result = null;
        try {
            final Collection<String> decodedConcepts = new HashSet<>();

            result = ModelFactory.createDefaultModel();

            final Map<DecodingType, Map<String, NalSnippetBean>> nalSnippetsPerDecodingType = getSnippetsFor(conceptsPerDecodingType);

            Map<String, NalSnippetBean> nalSnippetsPerConcept = nalSnippetsPerDecodingType.remove(DecodingType.INCLUDE_RELATED);
            if (nalSnippetsPerConcept != null) {
                loadDecoding(DecodingType.INCLUDE_RELATED, nalSnippetsPerConcept, result, decodedConcepts);
            }

            for (final Map.Entry<DecodingType, Map<String, NalSnippetBean>> snippet : nalSnippetsPerDecodingType.entrySet()) {
                nalSnippetsPerConcept = snippet.getValue();
                loadDecoding(snippet.getKey(), nalSnippetsPerConcept, result, decodedConcepts);
            }

            return new Pair<>(decodedConcepts, result);
        } catch (final RuntimeException e) {
            JenaUtils.closeQuietly(result);

            throw e;
        }
    }

    /**
     * <p>loadDecoding.</p>
     *
     * @param type                 a {@link DecodingType} object.
     * @param nalSnippetPerConcept a {@link java.util.Map} object.
     * @param result               a {@link org.apache.jena.rdf.model.Model} object.
     * @param decodedConcepts      a {@link java.util.Set} object.
     */
    private void loadDecoding(DecodingType type, Map<String, NalSnippetBean> nalSnippetPerConcept, Model result,
                              Collection<String> decodedConcepts) {
        Model model = null;
        try {
            for (final Map.Entry<String, NalSnippetBean> snippet : nalSnippetPerConcept.entrySet()) {
                decodedConcepts.add(snippet.getKey());

                final NalSnippetBean nalSnippet = snippet.getValue();
                if (model == null) {
                    model = JenaUtils.read(nalSnippet.getRdfSnippet(), "decoding_snippet", RDFFormat.RDFXML_PLAIN);
                } else {
                    JenaUtils.read(model, nalSnippet.getRdfSnippet(), "decoding_snippet", RDFFormat.RDFXML_PLAIN);
                }

                if (DecodingType.INCLUDE_RELATED == type) {
                    for (final NalSnippetBean related : nalSnippet.getRelatedSnippets()) {
                        JenaUtils.read(model, related.getRdfSnippet(), "related_decoding_snippet", RDFFormat.RDFXML_PLAIN);
                    }
                }
            }

            filter(model, type);

            if (model != null) {
                result.add(model);
            }
        } finally {
            JenaUtils.closeQuietly(model);
        }
    }

    /**
     * <p>getDecodingFor.</p>
     *
     * @param concept  a {@link java.lang.String} object.
     * @param language a {@link eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean} object.
     * @param type     a {@link eu.europa.ec.opoce.cellar.nal.decoding.DecodingType} object.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    public Model getDecodingFor(String concept, LanguageBean language, DecodingType type) {
        if (StringUtils.isBlank(concept)) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.DECODING_SNIPPET_SERVICE)
                    .withMessage("Required argument concept cannot be blank!").build();
        }
        if (type == null) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.DECODING_SNIPPET_SERVICE)
                    .withMessage("Required argument type cannot be null!").build();
        }
        if (language == null) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.DECODING_SNIPPET_SERVICE)
                    .withMessage("Required argument language cannot be null!").build();
        }

        return getDecodingFor(Collections.singletonList(concept), language, type).getTwo();
    }

    /**
     * <p>getDecodingFor.</p>
     *
     * @param concepts a {@link java.util.Collection} object.
     * @param language a {@link eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean} object.
     * @param type     a {@link DecodingType} object.
     * @return a {@link eu.europa.ec.opoce.cellar.common.util.Pair} object.
     */
    @Override
    @SuppressWarnings("serial")
    public Pair<Collection<String>, Model> getDecodingFor(final Collection<String> concepts, final LanguageBean language,
                                                          final DecodingType type) {
        if (concepts == null) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.DECODING_SNIPPET_SERVICE)
                    .withMessage("Required argument concepts cannot be null!").build();
        }
        if (type == null) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.DECODING_SNIPPET_SERVICE)
                    .withMessage("Required argument type cannot be null!").build();
        }
        if (language == null) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.DECODING_SNIPPET_SERVICE)
                    .withMessage("Required argument language cannot be null!").build();
        }

        return getDecodingFor(new HashMap<DecodingType, Collection<String>>() {

            {
                put(type, concepts);
            }
        }, language);
    }

    /**
     * <p>getDecodingFor.</p>
     *
     * @param conceptsPerDecodingType a {@link java.util.Map} object.
     * @param language                a {@link eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean} object.
     * @return a {@link eu.europa.ec.opoce.cellar.common.util.Pair} object.
     */
    @Override
    public Pair<Collection<String>, Model> getDecodingFor(Map<DecodingType, Collection<String>> conceptsPerDecodingType,
                                                          final LanguageBean language) {
        if (conceptsPerDecodingType == null) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.DECODING_SNIPPET_SERVICE)
                    .withMessage("Required argument conceptsPerDecodingType cannot be null!").build();
        }
        if (language == null) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.DECODING_SNIPPET_SERVICE)
                    .withMessage("Required argument language cannot be null!").build();
        }

        final Pair<Collection<String>, Model> result = getDecodingFor(conceptsPerDecodingType);

        return JenaTemplate.execute(result.getTwo(), model -> new Pair<>(result.getOne(), extractLangDecodingInternal(model, language)), true);
    }

    /**
     * <p>extractLangDecoding.</p>
     *
     * @param model    a {@link org.apache.jena.rdf.model.Model} object.
     * @param language a {@link eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean} object.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    @Override
    public Model extractLangDecoding(Model model, LanguageBean language) {
        if (model == null) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.DECODING_SNIPPET_SERVICE)
                    .withMessage("Required argument model cannot be null!").build();
        }
        if (language == null) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.DECODING_SNIPPET_SERVICE)
                    .withMessage("Required argument language cannot be null!").build();
        }

        return extractLangDecodingInternal(model, language);
    }

    /**
     * <p>getSnippetsFor.</p>
     *
     * @param concepts a {@link java.util.Map} object.
     * @return a {@link java.util.Map} object.
     */
    @Override
    public Map<DecodingType, Map<String, NalSnippetBean>> getSnippetsFor(Map<DecodingType, Collection<String>> concepts) {
        final Map<DecodingType, Map<String, NalSnippetBean>> snippetsPerDecodingType = new HashMap<DecodingType, Map<String, NalSnippetBean>>(
                concepts.size());
        for (final Map.Entry<DecodingType, Collection<String>> concept : concepts.entrySet()) {
            snippetsPerDecodingType.put(concept.getKey(), getSnippetsFor(concept.getValue(), concept.getKey()));
        }

        return snippetsPerDecodingType;
    }

    /**
     * <p>getSnippetsFor.</p>
     *
     * @param concepts a {@link java.util.Collection} object.
     * @param type     a {@link DecodingType} object.
     * @return a {@link java.util.Map} object.
     */
    @Override
    public Map<String, NalSnippetBean> getSnippetsFor(Collection<String> concepts, DecodingType type) {
        if (type == null) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.DECODING_SNIPPET_SERVICE)
                    .withMessage("Required argument type cannot be null!").build();
        }

        return DecodingType.INCLUDE_RELATED == type ? getNalSnippetGateway().selectSnippetsInclRelated(concepts)
                : getNalSnippetGateway().selectSnippets(concepts);
    }

    /**
     * <p>filter.</p>
     *
     * @param model        a {@link org.apache.jena.rdf.model.Model} object.
     * @param decodingType a {@link eu.europa.ec.opoce.cellar.nal.decoding.DecodingType} object.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    private Model filter(Model model, DecodingType decodingType) {
        if ((model == null) || (DecodingType.INCLUDE_RELATED == decodingType)) {
            return model;
        }

        model.removeAll(null, CellarProperty.skos_broaderTransitiveP, null);
        model.removeAll(null, CellarProperty.cmr_levelP, null);
        model.removeAll(null, CellarProperty.cmr_facet_TP, null);
        model.removeAll(null, CellarProperty.cmr_facet_MP, null);
        model.removeAll(null, CellarProperty.cmr_facet_DP, null);

        return model;
    }

    /**
     * <p>extractLangDecodingInternal.</p>
     *
     * @param model    a {@link org.apache.jena.rdf.model.Model} object.
     * @param language a {@link eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean} object.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    private Model extractLangDecodingInternal(Model model, LanguageBean language) {
        if (model == null) {
            return null;
        }

        String code2Char = language.getIsoCodeTwoChar();
        final String code3Char = language.getIsoCodeThreeChar();

        if (StringUtils.isBlank(code3Char)) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.DECODING_SNIPPET_SERVICE)
                    .withMessage("Language with missing 3-char iso code: '{}'").withMessageArgs(language.getUri()).build();
        }
        if (StringUtils.isBlank(code2Char)) {
            code2Char = code3Char;
        }

        final List<Statement> statementsToInclude = new ArrayList<Statement>();
        for (final Statement statement : model.listStatements().toList()) {
            final String property = statement.getPredicate().getURI();
            if (ALWAYS_INCLUDE.contains(property)) {
                statementsToInclude.add(statement);
            } else if (StringUtils.equals(property, skos_prefLabel) || StringUtils.equals(property, skos_altLabel)) {
                if (includeLabelStatement(statement, code2Char, code3Char)) {
                    statementsToInclude.add(statement);
                }
            } else if (StringUtils.equals(property, cmr_fallback)) {
                final Statement preflabelStatement = statement.getProperty(cmr_prefLabelP);
                if ((preflabelStatement != null) && includeLabelStatement(preflabelStatement, code2Char, code3Char)) {
                    final Statement cmrLangStatement = statement.getProperty(cmr_langP);
                    statementsToInclude.add(statement);
                    statementsToInclude.add(preflabelStatement);
                    if (cmrLangStatement != null) {
                        statementsToInclude.add(cmrLangStatement);
                    }
                }
            }
        }

        Model decodedModel = null;
        try {
            decodedModel = ModelFactory.createDefaultModel();
            decodedModel.add(statementsToInclude);

            return decodedModel;
        } catch (final Exception e) {
            JenaUtils.closeQuietly(decodedModel);

            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.CMR_INTERNAL_ERROR).withCause(e).build();
        }
    }

    /**
     * <p>includeLabelStatement.</p>
     *
     * @param labelStatement a {@link org.apache.jena.rdf.model.Statement} object.
     * @param code2Char      a {@link java.lang.String} object.
     * @param code3Char      a {@link java.lang.String} object.
     * @return a boolean.
     */
    private boolean includeLabelStatement(Statement labelStatement, String code2Char, String code3Char) {
        final Literal literal = labelStatement.getLiteral();
        final String lang = literal != null ? literal.getLanguage() : null;

        return StringUtils.equals(lang, code2Char) || StringUtils.equals(lang, code3Char);
    }

}
