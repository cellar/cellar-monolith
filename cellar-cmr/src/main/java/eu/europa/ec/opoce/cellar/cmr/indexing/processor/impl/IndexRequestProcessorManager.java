/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.indexing.processor.impl
 *             FILE : IndexRequestProcessorManager.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Nov 22, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.indexing.processor.impl;

import eu.europa.ec.opoce.cellar.cmr.indexing.IndexationMonitoringService;
import eu.europa.ec.opoce.cellar.cmr.indexing.processor.IndexRequestProcessor;
import eu.europa.ec.opoce.cellar.cmr.indexing.service.IndexRequestManagerService;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestBatch;

import org.apache.logging.log4j.ThreadContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.ExecutionStatus.Error;

import java.util.List;
import java.util.UUID;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Nov 22, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class IndexRequestProcessorManager {

	private static final Logger LOG = LoggerFactory.getLogger(IndexRequestProcessorManager.class);
	
    /**
     * {@link org.springframework.core.annotation.Order} to make sure
     * that the {@link IndexRequestProcessor} are executed in the correct
     * order.
     */
    private final List<IndexRequestProcessor> processors;
    
    private final IndexRequestManagerService indexRequestManagerService;
    private final IndexationMonitoringService indexationMonitoringService;

    @Autowired
    public IndexRequestProcessorManager(@Qualifier("group") List<IndexRequestProcessor> processors, IndexRequestManagerService indexRequestManagerService,
    		IndexationMonitoringService indexationMonitoringService) {
        this.processors = processors;
        this.indexRequestManagerService = indexRequestManagerService;
        this.indexationMonitoringService = indexationMonitoringService;
    }

    /**
     * 1. remove useless index entries
     * 2. generate index requests corresponding inverses
     * 3. generate embedded notices
     *
     * @param cmrIndexRequest
     */
    @Transactional
    public void execute(final CmrIndexRequestBatch cmrIndexRequest) {
    	try {
        	processors.forEach(p -> p.execute(cmrIndexRequest));
    	} catch (Exception e) {
    		String cause = UUID.randomUUID().toString();
            LOG.error("[" + cause + "] Unexpected exception during indexation.", e);
            indexRequestManagerService.updateBatchStatus(cmrIndexRequest, Error, cause);
    	}
	    finally {
	        indexationMonitoringService.unregister(cmrIndexRequest);
	        ThreadContext.remove("indexing_key");
	    }
    }
}
