/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.cmr.ingest.impl.update
 *        FILE : MergeModelLoader.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 09-04-2014
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.ingest.impl.delete;

import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData;
import eu.europa.ec.opoce.cellar.cmr.ingest.impl.AbstractModelLoader;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.HierarchyProperty.cdm_item_belongs_to_manifestationP;
import static org.apache.jena.rdf.model.ResourceFactory.createResource;

/**
 * <class_description> Class for conveniently load models for delete ingests.<br/>
 * <p>
 * <p>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 09-04-2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class DeleteModelLoader extends AbstractModelLoader {
    private static final Logger LOG = LogManager.getLogger(DeleteModelLoader.class);

    /**
     * It is a deletion: the METS package is not supposed to link any RDF file.
     *
     * @see eu.europa.ec.opoce.cellar.cmr.ingest.impl.create.CreateModelLoader#loadMetsModel()
     */
    @Override
    protected void loadMetsModel() {
    }

    @Override
    protected void loadDirectModel() {
        // Load the direct model
        final Model directModel = calculateCleanedExistingModel(this.getData());

        // Set the direct model into the map of business models
        this.getBusinessModels().put(ContentType.DIRECT, directModel);
    }

    /**
     * Calculate the new model by loading the existing model and cleaning it from the triples containing an element to delete.
     */
    private static Model calculateCleanedExistingModel(final CalculatedData data) {
        final Model existingModel = data.getExistingDirectModel().asModel();

        final Map<String, Set<String>> urisToRemove = new HashMap<>();
        for (final DigitalObject r : data.getRemovedDigitalObjects()) {
            final ContentIdentifier id = r.getCellarId();
            urisToRemove.put(id.getIdentifier(), urisToRemove(existingModel, id, r.getAllUris()));
        }

        // Remove the triples containing an element to delete
        final Set<Statement> statementsToRemove = new HashSet<>();
        final StmtIterator iter = existingModel.listStatements();
        try {
            while (iter.hasNext()) {
                final Statement stmt = iter.nextStatement();

                final String subjectUri = stmt.getSubject().getURI();
                final String propertyUri = stmt.getPredicate().getURI();
                final RDFNode objectNode = stmt.getObject();
                final String objectUri = (objectNode.isResource() ? objectNode.asResource().getURI() : null);

                for (final DigitalObject remove : data.getRemovedDigitalObjects()) {
                    final Set<String> uris = urisToRemove.get(remove.getCellarId().getIdentifier());
                    for (final String toBeRemovedUri : uris) {
                        if (StringUtils.equals(subjectUri, toBeRemovedUri)
                                || (CellarProperty.HierarchyProperty.cdm_hierarchy_properties_set.contains(propertyUri) && StringUtils.equals(objectUri, toBeRemovedUri))) {
                            statementsToRemove.add(stmt);
                        }
                    }
                }
            }
            existingModel.remove(new ArrayList<>(statementsToRemove));

            return existingModel;
        } finally {
            iter.close();
        }
    }

    private static Set<String> urisToRemove(Model existingModel, ContentIdentifier cellarId, Set<String> uris) {
        final Set<String> toRemove = new HashSet<>(uris);
        for (String uri : uris) {
            if (uri.contains("resource/cellar")) {
                if (DigitalObjectType.isManifestation(cellarId.getIdentifier())) {
                    toRemove.addAll(existingModel.listStatements(null, cdm_item_belongs_to_manifestationP, createResource(uri))
                            .toList()
                            .stream()
                            .map(s -> s.getSubject().getURI())
                            .collect(Collectors.toList()));
                }
            }
        }
        return toRemove;
    }
}
