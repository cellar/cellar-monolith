/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.virtuosobackup.service.impl
 *             FILE : VirtuosoBackupSchedulerServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Mar 18, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.virtuosobackup.service.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.common.util.CellarIdUtils;
import eu.europa.ec.opoce.cellar.logging.LogContext;
import eu.europa.ec.opoce.cellar.virtuoso.util.VirtuosoUtils;
import eu.europa.ec.opoce.cellar.virtuosobackup.VirtuosoBackup;
import eu.europa.ec.opoce.cellar.virtuosobackup.service.IVirtuosoBackupSchedulerService;
import eu.europa.ec.opoce.cellar.virtuosobackup.service.IVirtuosoBackupService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.CMR_VIRTUOSO;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Mar 18, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service("virtuosoBackupSchedulerService")
public class VirtuosoBackupSchedulerServiceImpl implements IVirtuosoBackupSchedulerService {

    /**
     * Logger instance.
     */
    private static final Logger LOGGER = LogManager.getLogger(VirtuosoBackupSchedulerServiceImpl.class);

    /**
     * The cellar configuration.
     */
    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    /**
     * The virtuoso backup service.
     */
    @Autowired
    private IVirtuosoBackupService virtuosoBackupService;

    /**
     * The cellar resource dao.
     */
    @Autowired
    private CellarResourceDao cellarResourceDao;

    @Override
    @LogContext(CMR_VIRTUOSO)
    public void scheduleVirtuosoBackup() {
        final boolean isLogVerbose = cellarConfiguration.isVirtuosoIngestionBackupLogVerbose();
        if (!cellarConfiguration.isVirtuosoIngestionBackupEnabled()) {
            return;
        }

        final int limit = cellarConfiguration.getVirtuosoIngestionBackupMaxResults();
        final Collection<VirtuosoBackup> allVirtuosoBackup = virtuosoBackupService.findResyncablesSortedByInsertionDateAsc(limit);

        //Group Digital object by cellarId base
        final Map<String, List<VirtuosoBackup>> allVirtuosoBackupWork = new HashMap<>();
        for (final VirtuosoBackup virtuosoBackup : allVirtuosoBackup) {
            final String cellarIdBase = CellarIdUtils.getCellarIdBase(virtuosoBackup.getObjectId());
            List<VirtuosoBackup> listDigitalObject;
            if (allVirtuosoBackupWork.containsKey(cellarIdBase)) {
                listDigitalObject = allVirtuosoBackupWork.get(cellarIdBase);
            } else {
                listDigitalObject = new ArrayList<>();

            }
            listDigitalObject.add(virtuosoBackup);
            allVirtuosoBackupWork.put(cellarIdBase, listDigitalObject);
        }

        for (final Entry<String, List<VirtuosoBackup>> virtuosoBackupWork : allVirtuosoBackupWork.entrySet()) {
            final CellarResource cellarResource = cellarResourceDao.findCellarId(virtuosoBackupWork.getKey());
            if (cellarResource != null) {
                final boolean underEmbargo = cellarResource.isUnderEmbargo();
                for (final VirtuosoBackup virtuosoBackup : virtuosoBackupWork.getValue()) {
                    virtuosoBackup.setUnderEmbargo(underEmbargo);
                    virtuosoBackup.setEmbargoDate(cellarResource.getEmbargoDate());
                }
            }
        }

        /*
         * Temporary workaround for CELLAR-2092:
         * TODO: remove when fixed by OpenLink
         */
        final Collection<VirtuosoBackup> virtuosoBackups = allVirtuosoBackup.stream()
                .filter(backup -> {
                    final String reason = backup.getReason();
                    return reason == null || !reason.contains("Character data are not allowed here by XML structure rules");
                })
                .collect(Collectors.toList());

        if (isLogVerbose) {
            LOGGER.info("Start processing virtuoso backup ...");
            LOGGER.info("Number of remaining entries : {}", virtuosoBackupService.countAllResyncable() - allVirtuosoBackup.size());
            LOGGER.info("Number of being processed entries : {}", virtuosoBackups.size());
        }
        int backupProcessed = 0;
        boolean synchroOk = true;
        for (final VirtuosoBackup virtuosoBackup : virtuosoBackups) {
            try {
                // process the backup from VIRTUOSO_BACKUP table to Virtuoso
                virtuosoBackupService.processVirtuosoBackup(virtuosoBackup, true);

                // delete data from VIRTUOSO_BACKUP table
                virtuosoBackupService.delete(virtuosoBackup);
                backupProcessed++;
            } catch (final Exception exception) {
                synchroOk = false;
                if (VirtuosoUtils.isVirtuosoNotAvailable(exception)) {
                    LOGGER.warn("Virtuoso backup identified by Id '{}' and cellarId '{}' has not been processed as Virtuoso is not available",
                            virtuosoBackup.getId(), virtuosoBackup.getObjectId());
                } else {
                    LOGGER.error("Virtuoso backup identified by Id '" + virtuosoBackup.getId() + "' and cellarId '" +
                            virtuosoBackup.getObjectId() + "' has not been processed because of an unexpected error", exception);
                }
            }
        }
        if (isLogVerbose) {
            LOGGER.info("{} out of {} Virtuoso backups have been correctly processed.", backupProcessed, allVirtuosoBackup.size());
            if (allVirtuosoBackup.size() > 0) {
                LOGGER.info("Synchronization status : {}", synchroOk ? "OK" : "NOK");
            }
        }
    }

}
