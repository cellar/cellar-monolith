package eu.europa.ec.opoce.cellar.nal.service.impl;

import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.database.dao.NalConfigDao;
import eu.europa.ec.opoce.cellar.common.util.Wrapper;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.NalOntoUtils;
import eu.europa.ec.opoce.cellar.nal.dao.NalConfig;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.nal.service.LanguageSkosService;
import eu.europa.ec.opoce.cellar.nal.utils.NalSparqlQueryTemplate;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import eu.europa.ec.opoce.cellar.semantic.jena.template.JenaTemplate;
import eu.europa.ec.opoce.cellar.semantic.jena.template.ListOfMapLoopHandlerWithoutResult;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.riot.Lang;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.StringReader;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static eu.europa.ec.opoce.cellar.nal.NalOntoUtils.NAL_PREFIX;

/**
 * Service that implements an api for internal purposes on the language nal concept scheme
 */
@Service
public class LanguageSkosServiceImpl implements LanguageSkosService {

    private ContentStreamService contentStreamService;
    private NalConfigDao nalConfigDao;

    @Autowired
    public LanguageSkosServiceImpl(ContentStreamService contentStreamService, NalConfigDao nalConfigDao) {
        this.contentStreamService = contentStreamService;
        this.nalConfigDao = nalConfigDao;
    }


    /**
     * Performs a sparql guery on the ora model of the language concept scheme to retrieve the data of the given language uri.
     * A {@link eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean} object is returned for the given language uri.
     * Returns null if the language could not be found!
     *
     * @param languageUri the uri of the language resource
     * @return a language bean
     */
    @Override
    public LanguageBean getLanguage(final String languageUri) {
        if (StringUtils.isBlank(languageUri)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument uri must not be blank!").build();
        }

        final Wrapper<Boolean> exist = new Wrapper<>(false);

        final Set<String> codes = new HashSet<>();
        final Set<String> iso2Codes = new HashSet<>();
        final Set<String> iso3Codes = new HashSet<>();

        final ListOfMapLoopHandlerWithoutResult handler = new ListOfMapLoopHandlerWithoutResult() {

            @Override
            protected void process(Map<String, RDFNode> result) {
                exist.setValue(true);
                addStringValueIfNotBlanc((Literal) result.get("code"), codes);
                addStringValueIfNotBlanc((Literal) result.get("iso2Code"), iso2Codes);
                addStringValueIfNotBlanc((Literal) result.get("iso3Code"), iso3Codes);
            }
        };

        NalConfig nalConfig = nalConfigDao.findByUri(NalOntoUtils.NAL_LANGUAGE_URI)
                .orElseThrow(() -> ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.NAL_MISSING_URI)
                        .withMessage("The NAL {} could not be found in the NALCONFIG table.")
                        .withMessageArgs(NalOntoUtils.NAL_LANGUAGE_URI)
                        .build()
                );

        String nalName = nalConfig.getName();
        Model nalModel = getNalModelFromS3(nalName);
        JenaTemplate.select(nalModel, NalSparqlQueryTemplate.selectLanguageQuery(languageUri), handler);

        if (!exist.getValue()) {
            return null;
        }

        if (codes.size() > 1) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.NAL_API_DATA_CORRUPTION)
                    .withMessage("More then one op-code found for language '{}': {}")
                    .withMessageArgs(languageUri, StringUtils.join(codes, ", ")).build();
        }
        if (iso2Codes.size() > 1) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.NAL_API_DATA_CORRUPTION)
                    .withMessage("More then one iso 2-char code found for language '{}': {}")
                    .withMessageArgs(languageUri, StringUtils.join(iso2Codes, ", ")).build();
        }
        if (iso3Codes.size() > 1) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.NAL_API_DATA_CORRUPTION)
                    .withMessage("More then one iso 3-char code found for language '{}': {}")
                    .withMessageArgs(languageUri, StringUtils.join(iso3Codes, ", ")).build();
        }

        return new LanguageBean() {
            {
                setUri(languageUri);
                setCode(!codes.isEmpty() ? codes.iterator().next() : null);
                if (!iso2Codes.isEmpty()) {
                    setIsoCodeTwoChar(iso2Codes.iterator().next());
                }
                if (!iso3Codes.isEmpty()) {
                    setIsoCodeThreeChar(iso3Codes.iterator().next());
                }
            }
        };
    }

    private Model getNalModelFromS3(String conceptScheme) {
        String nalContent = contentStreamService.getContentAsString(NAL_PREFIX + conceptScheme, ContentType.DIRECT_INFERRED)
                .orElseThrow(() -> ExceptionBuilder.get(CellarException.class)
                        .withMessage("The NAL for the concept scheme '{}' (inferred) has been not found in the repository")
                        .withMessageArgs(conceptScheme)
                        .build());
        return ModelFactory.createDefaultModel().read(new StringReader(nalContent), "", Lang.RDFXML.getName());
    }

    private void addStringValueIfNotBlanc(Literal literal, Set<String> labels) {
        final String stringValue = stringValueOf(literal);
        if (!StringUtils.isBlank(stringValue)) {
            labels.add(stringValue);
        }
    }

    private String stringValueOf(Literal literal) {
        return literal != null ? literal.getString() : null;
    }
}
