/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.server.service
 *        FILE : DisseminationException.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 30-01-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.service;

import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareException;
import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;

/**
 * This is a {@link DisseminationException} buildable by an {@link HttpStatusAwareExceptionBuilder}.</br>
 * </br> 
 * ON : 30-01-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public class DisseminationException extends HttpStatusAwareException {

    private static final long serialVersionUID = -6658123758579830268L;

    /**
     * Constructs a new exception with its associated builder
     * 
     * @param builder the builder to use for building the exception
     */
    public DisseminationException(final HttpStatusAwareExceptionBuilder<? extends HttpStatusAwareException> builder) {
        super(builder);
    }
}
