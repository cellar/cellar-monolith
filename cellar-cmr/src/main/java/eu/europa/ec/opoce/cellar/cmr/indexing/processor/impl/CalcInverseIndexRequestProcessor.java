/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.indexing.processor.impl
 *             FILE : CalcInverseIndexRequestProcessorImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Nov 17, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.indexing.processor.impl;

import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;
import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.RelatedObject;
import eu.europa.ec.opoce.cellar.cmr.daobeans.InverseRelation;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CmrIndexRequestDao;
import eu.europa.ec.opoce.cellar.cmr.database.dao.InverseRelationDao;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.ExecutionStatus;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.RequestType;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestBatch;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestGenerationService;
import eu.europa.ec.opoce.cellar.common.util.CellarIdUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.semantic.helper.MessageFormatter;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Nov 17, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component
@Qualifier("group")
@Order(20)
public class CalcInverseIndexRequestProcessor extends AbstractIndexRequestProcessor {

    private static final Logger LOG = LogManager.getLogger(CalcInverseIndexRequestProcessor.class);

    private final IdentifierService identifierService;

    private final CmrIndexRequestDao indexRequestDao;

    private final InverseRelationDao inverseRelationDao;

    private final CmrIndexRequestGenerationService cmrIndexRequestGenerationService;

    @Autowired
    public CalcInverseIndexRequestProcessor(@Qualifier("pidManagerService") IdentifierService identifierService, CmrIndexRequestDao indexRequestDao,
                                            InverseRelationDao inverseRelationDao, CmrIndexRequestGenerationService cmrIndexRequestGenerationService) {
        this.identifierService = identifierService;
        this.indexRequestDao = indexRequestDao;
        this.inverseRelationDao = inverseRelationDao;
        this.cmrIndexRequestGenerationService = cmrIndexRequestGenerationService;
    }

    @Override
    protected void execute(CmrIndexRequestBatch group, List<CmrIndexRequest> requests) {
        final Identifier identifier = this.resolveIdentifier(group.getGroupByUri());
        if (identifier == null) {
            return;
        }

        final Set<RelatedObject> relatedObjects = this.retrieveRelatedObjects(identifier);
        final Collection<CmrIndexRequest> generatedIndexRequests = this.generateIndexRequests(group, relatedObjects);
        this.saveQuietly(generatedIndexRequests);

        LOG.info("{} index requests have been generated for the inverses of '{}'.",
                generatedIndexRequests.size(), identifier.getCellarId());
    }

    @Override
    protected List<CmrIndexRequest> filter(CmrIndexRequestBatch request) {
        return request.getCmrIndexRequests().stream()
                .filter(r -> r.getExecutionStatus() == ExecutionStatus.Execution && r.getRequestType() == RequestType.CalcInverse)
                .collect(Collectors.toList());
    }

    private Identifier resolveIdentifier(final String uri) {
        final String cellarId = this.identifierService.getCellarPrefixedOrNull(uri);
        if (StringUtils.isBlank(cellarId)) {
            LOG.warn("The identifier {} that was added is already removed", uri);
            return null;
        }

        Identifier identifier = null;
        try {
            identifier = identifierService.getIdentifier(cellarId);
        } catch (IllegalArgumentException cause) {
            LOG.warn("The identifier " + uri + " that was added is already removed", cause);
        }
        return identifier;
    }

    private void saveQuietly(final Collection<CmrIndexRequest> indexRequests) {
        for (CmrIndexRequest indexRequest : indexRequests) {
            try {
                this.indexRequestDao.saveOrUpdateObject(indexRequest);
            } catch (Exception e) {
                LOG.error(MessageFormatter.format("Unexpected exception while saving {}: {}", indexRequest, e.getMessage()), e);
            }
        }
    }

    private Collection<CmrIndexRequest> generateIndexRequests(final CmrIndexRequestBatch cmrIndexRequest, final Set<RelatedObject> relations) {
        final Set<CmrIndexRequest> indexRequests = new HashSet<>();
        for (RelatedObject inverseRelation : relations) {

            final String cellarId = this.identifierService.getCellarPrefixedOrNull(inverseRelation.getResourceUri());
            if (StringUtils.isBlank(cellarId)) {
                LOG.warn("Unable to create direct linked index request for {}: Cellar resource does not exist yet!", inverseRelation.getResourceUri());
            } else {
                final DigitalObjectType directType = inverseRelation.getType();
                if (directType != null) {
                    CmrIndexRequest indexRequest = new CmrIndexRequest();
                    indexRequest.setObjectUri(identifierService.getUri(cellarId));
                    indexRequest.setGroupByUri(identifierService.getUri(CellarIdUtils.getRootCellarId(cellarId)));
                    indexRequest.setCreatedOn(cmrIndexRequest.getCreatedOn());
                    indexRequest.setReason(CmrIndexRequest.Reason.InverseLinkedObject);
                    indexRequest.setRequestType(RequestType.CalcNotice);
                    indexRequest.setRequestedBy("indexingService");
                    IIndexingService.Priority priority = cmrIndexRequestGenerationService.getLinkedIndexingBehavior()
                            .transformToPriority(cmrIndexRequest.getPriority().getPriorityValue());
                    indexRequest.setPriority(priority);
                    indexRequest.setAction(CmrIndexRequest.Action.Update);
                    indexRequest.setObjectType(directType);
                    indexRequests.add(indexRequest);
                }
            }
        }
        return indexRequests;
    }

    private Set<RelatedObject> retrieveRelatedObjects(final Identifier identifier) {
        final Set<InverseRelation> inverseRelations = new HashSet<>();
        inverseRelations.addAll(this.inverseRelationDao.findRelationsBySource(identifier.getCellarId()));
        inverseRelations.addAll(this.inverseRelationDao.findRelationsBySources(identifier.getPids()));
        return inverseRelations.stream()
                .map(ir -> new RelatedObject(identifierService.getUri(ir.getTarget()), ir.getTargetType(), ""))
                .collect(Collectors.toSet());
    }

}
