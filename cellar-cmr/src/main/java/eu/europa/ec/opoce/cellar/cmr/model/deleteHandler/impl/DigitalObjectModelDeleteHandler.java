/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cmr.model.deleteHandler
 *        FILE : IModelDeleteHandler.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 14-04-2014
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.model.deleteHandler.impl;

import eu.europa.ec.opoce.cellar.cmr.model.deleteHandler.IModelDeleteHandler;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import org.apache.jena.rdf.model.Model;

/**
 * <class_description> Convenience class for deleting from a model the triples defined in the <code>deleteModelString</code> or <code>deleteSparqlQuery</code> fields of the given <code>digitalObject</code>.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 14-04-2014
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
@Service("digitalObjectModelDeleteHandler")
public class DigitalObjectModelDeleteHandler implements IModelDeleteHandler<DigitalObject> {

    @Autowired
    @Qualifier("rdfModelDeleteHandler")
    private IModelDeleteHandler<String> rdfModelDeleteHandler;

    @Autowired
    @Qualifier("sparqlModelDeleteHandler")
    private IModelDeleteHandler<String> sparqlModelDeleteHandler;

    /**
     * This method removes from the given {@link model} the triples defined in the <code>deleteModelString</code> or <code>deleteSparqlQuery</code> fields of the given {@link digitalObject}.
     * 
     * @param inputModel the model from which to delete data
     * @param digitalObject the digital object
     * @param preserveInputModel if <code>true</code>, the {@link inputModel} is kept unmodified, and only the number of deleted entries will be returned
     * @return the number of triples deleted
     */
    @Override
    public long delete(final Model inputModel, final DigitalObject digitalObject, final boolean preserveInputModel) {
        long deleted = 0;

        final String deleteModelString = digitalObject.getDeleteModelString();
        deleted += this.rdfModelDeleteHandler.delete(inputModel, deleteModelString, preserveInputModel);
        final String deleteQuery = digitalObject.getDeleteSparqlQuery();
        deleted += this.sparqlModelDeleteHandler.delete(inputModel, deleteQuery, preserveInputModel);

        return deleted;
    }

}
