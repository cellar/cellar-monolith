/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.database.dao
 *             FILE : CmrInverseDisseminationDao.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 3 août 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.database.dao;

import eu.europa.ec.opoce.cellar.cl.dao.BaseDao;
import eu.europa.ec.opoce.cellar.cmr.CmrInverseDissemination;
import eu.europa.ec.opoce.cellar.database.CmrTableName;
import eu.europa.ec.opoce.cellar.semantic.helper.StringHelper;

import java.sql.Connection;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * <p>CmrInverseDisseminationDao interface.</p>
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface CmrInverseDisseminationDao extends BaseDao<CmrInverseDissemination, Long> {

    /** The Constant TABLE_NAME. */
    String TABLE_NAME = CmrTableName.CMR_INVERSE.getDisseminationTable().toString();

    /**
     * The Interface Column.
     * <class_description> A meaningful description of the class that will be
     *                     displayed as the class summary on the JavaDoc package page.
     * <br/><br/>
     * <notes> Includes guaranteed invariants, usage instructions and/or examples,
     *         reminders about desired improvements, etc.
     * <br/><br/>
     * ON : 3 août 2016
     *
     * @author ARHS Developments
     * @version $Revision$
     */
    interface Column {

        /** The context. */
        String CONTEXT = "CONTEXT";

        /** The inverse model. */
        String INVERSE_MODEL = "INVERSE_MODEL";

        /** The sameas model. */
        String SAMEAS_MODEL = "SAMEAS_MODEL";

        /** The is backlog. */
        String IS_BACKLOG = "IS_BACKLOG";
    }

    /** The Constant SELECT_BY_CONTEXTS_QUERY. */
    String SELECT_BY_CONTEXTS_QUERY = StringHelper.format("{} in (:contexts)", Column.CONTEXT);

    /**
     * <p>Upsert the given entry by context</p>.
     *
     * @param daoObject the object to upsert by context
     * @return the upserted {@link eu.europa.ec.opoce.cellar.cmr.CmrInverseDissemination} object
     */
    CmrInverseDissemination upsertByContext(final CmrInverseDissemination daoObject);

    /**
     * <p>Upsert the given entry by context using the given connection</p>.
     *
     * @param daoObject the {@link java.sql.Connection} to upsert by context
     * @param connection the connection
     * @return the upserted {@link eu.europa.ec.opoce.cellar.cmr.CmrInverseDissemination} object
     */
    CmrInverseDissemination upsertByContext(final CmrInverseDissemination daoObject, final Connection connection);

    /**
     * <p>Selects the entries matching the given contexts. Pages-up the selection by {@code pageSize}.</p>
     *
     * @param contexts contexts to match
     * @param pageSize the size of the paging
     * @return a {@link java.util.Collection} object containing the selected {@link eu.europa.ec.opoce.cellar.cmr.CmrInverseDissemination}
     */
    Collection<CmrInverseDissemination> selectByContexts(final List<String> contexts, final int pageSize);

    /**
     * Select by contexts.
     *
     * @param contexts the contexts
     * @param pageSize the page size
     * @param tableName the table name
     * @return the collection
     */
    Collection<CmrInverseDissemination> selectByContexts(final List<String> contexts, final int pageSize, String tableName);

    /**
     * <p>Selects the entries matching the given contexts. Pages-up the selection by {@code pageSize}.</p>
     *
     * @param contexts contexts to match
     * @param pageSize the size of the paging
     * @return a {@link java.util.Map} context-cmrInverseDissemination
     */
    Map<String, CmrInverseDissemination> selectByContextsMapped(final List<String> contexts, final int pageSize);

    /**
     * Select by contexts mapped.
     *
     * @param contexts the contexts
     * @param pageSize the page size
     * @param tableName the table name
     * @return the map
     */
    Map<String, CmrInverseDissemination> selectByContextsMapped(final List<String> contexts, final int pageSize, String tableName);
}
