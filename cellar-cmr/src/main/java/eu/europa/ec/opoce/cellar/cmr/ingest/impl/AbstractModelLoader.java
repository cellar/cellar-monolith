/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.ingest.impl
 *             FILE : AbstractModelLoader.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 12 08, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-12-08 11:48:51 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.ingest.impl;

import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.enums.CellarStatusServiceShaclReportLevel;
import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditBuilder;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventAction;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventProcess;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventType;
import eu.europa.ec.opoce.cellar.cl.domain.structmap_status_history.StructMapStatusHistory;
import eu.europa.ec.opoce.cellar.cl.domain.validation.MetadataValidationRequest;
import eu.europa.ec.opoce.cellar.cl.domain.validation.MetadataValidationResult;
import eu.europa.ec.opoce.cellar.cl.service.client.CellarIdentifierService;
import eu.europa.ec.opoce.cellar.cl.service.client.StructMapStatusHistoryService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.InferenceService;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData;
import eu.europa.ec.opoce.cellar.cmr.ingest.IMetadataLoaderService;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.HierarchyElement;
import eu.europa.ec.opoce.cellar.cmr.model.ModelUtils;
import eu.europa.ec.opoce.cellar.common.metadata.IModelLoader;
import eu.europa.ec.opoce.cellar.common.util.DigitalObjectUtils;
import eu.europa.ec.opoce.cellar.common.util.StringUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentStream;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.domain.content.mets.Metadata;
import eu.europa.ec.opoce.cellar.domain.content.mets.OperationType;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.CellarValidationException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.MetadataValidationResultAwareException;
import eu.europa.ec.opoce.cellar.exception.MetadataValidationResultAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.logging.LogContext;
import eu.europa.ec.opoce.cellar.modelload.utils.ModelLoadUtils;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.semantic.validation.ValidationResultCache;
import eu.europa.ec.opoce.cellar.semantic.validation.ValidationService;
import eu.europa.ec.opoce.cellar.semantic.validation.NalValidationUtils;
import org.apache.commons.io.IOUtils;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;

import java.io.*;
import java.util.*;

import static eu.europa.ec.opoce.cellar.CommonErrors.DIRECT_MODEL_VALIDATION_FAILED;
import static eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventAction.ValidateDirectMetadata;
import static eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventAction.ValidateTechnicalMetadata;
import static eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventType.End;
import static eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventType.Fail;
import static eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventType.Start;
import static eu.europa.ec.opoce.cellar.cl.domain.validation.MetadataValidationResult.ModelType.DIRECT;
import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.CMR_TMD_VALIDATION;

/**
 * @author ARHS Developments
 */
@Configurable
@LogContext
public class AbstractModelLoader implements IModelLoader<CalculatedData> {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractModelLoader.class);

    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    @Autowired
    private ContentStreamService contentStreamService;

    @Autowired
    private CellarResourceDao cellarResourceDao;

    @Autowired
    private IMetadataLoaderService metadataLoaderService;

    @Autowired
    private InferenceService inferenceService;

    @Autowired
    private CellarIdentifierService cellarIdentifierService;

    @Autowired
    @Qualifier("MetadataValidationService")
    protected ValidationService validationService;
    
    @Autowired
    protected StructMapStatusHistoryService structMapStatusHistoryService;

    /**
     * The loader's calculated data.
     */
    protected CalculatedData data;

    /**
     * The loader's new metadata from METS.
     */
    protected Model metsModel;

    /**
     * The loader's business metadata.
     */
    private final Map<ContentType, Model> businessModels;

    /**
     * The loader's technical metadata.
     */
    private final Map<DigitalObject, Model> technicalModels;

    /**
     * Constructs a new model loader.
     */
    public AbstractModelLoader() {
        this.businessModels = new HashMap<>();
        this.technicalModels = new HashMap<>();
    }

    /**
     * Load.
     *
     * @param data the data
     * @see IModelLoader#load(Object)
     */
    @Override
    public void load(CalculatedData data) {
        this.data = data;

        // validates the loader
        validatePackage(this.data);

        // loads the business and technical models
        try {
            loadBusinessModels();
            data.setStructMapStatusHistory(this.data.getStructMapStatusHistory());
            loadTechnicalModels();
        } catch (final CellarValidationException | MetadataValidationResultAwareException e) {
            throw e;
        } catch (final RuntimeException e) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.MODEL_LOAD_SERVICE_ERROR).withCause(e).build();
        }
    }

    /**
     * Close quietly.
     *
     * @see IModelLoader#closeQuietly()
     */
    @Override
    public void closeQuietly() {
        // close all models
        ModelUtils.closeQuietly(this.metsModel);
        ModelUtils.closeQuietly(this.businessModels.values());
        ModelUtils.closeQuietly(this.technicalModels.values());
        this.data.closeQuietly();
    }

    /**
     * It validates the direct model.
     */
    protected void validateDirectModel() {
        // validates direct model against the ontology
        final Model directModel = this.businessModels.get(ContentType.DIRECT);

        final List<String> invalidUriList = NalValidationUtils.validateUris(directModel);
        if (!invalidUriList.isEmpty()) {
            throw ExceptionBuilder.get(CellarValidationException.class)
                    .withCode(CmrErrors.VALIDATION_RDFDATA_ERROR)
                    .withMessage("Invalid URIs found in model : {}")
                    .withMessageArgs(invalidUriList)
                    .build();
        }
         Model directWithoutItem=null;
        if(cellarConfiguration.getCellarServiceIngestionValidationItemExclusionEnabled()) {
            //create copy of model
             directWithoutItem = ModelFactory.createDefaultModel().add(directModel);
            //remove all item related triples
            ModelUtils.removeItemTriples(directWithoutItem);
        }

        final MetadataValidationResult result = validationService.validate(new MetadataValidationRequest(directWithoutItem==null?directModel:directWithoutItem)
                .cellarId(safeCellarId()));
        
        String requiredShaclReport = filterShaclReport(result.getValidationResult());
        if ((requiredShaclReport != null) && (data.getStructMapStatusHistory() != null)){
            StructMapStatusHistory structMapStatusHistory = this.structMapStatusHistoryService.updateStructMapShaclReport(data.getStructMapStatusHistory(), requiredShaclReport);
            data.setStructMapStatusHistory(structMapStatusHistory);
        }
        
        result.setModelType(DIRECT);

        ValidationResultCache.put(getData().getStructMap(), result);

        if (!result.conformModelProvided()) {
            throw MetadataValidationResultAwareExceptionBuilder.getInstance(MetadataValidationResultAwareException.class)
                    .withStructMapId(getData().getStructMap().getId())
                    .withMetadataValidationResult(result)
                    .withMetsDocumentId(getData().getStructMap().getMetsDocument().getDocumentId())
                    .withMessage("Validation of the direct model failed : {}")
                    .withCode(DIRECT_MODEL_VALIDATION_FAILED)
                    .withMessageArgs(result.getErrorMessage())
                    .build();
        }
    }
    
    /**
     * Extract the nodes of required SHACL report level from the provided String JSON-formatted report
     * and produce a separate JSON-formatted String report with just them. The output may include '#Violation' only
     * or '#Violation' and '#Warning' node information.
     * @param report JSON-formatted report produced by SHACL validation
     * @return the JSON-formatted String report with just the appropriate nodes included
     */
    protected String filterShaclReport(String report){
        CellarStatusServiceShaclReportLevel reportLevel = cellarConfiguration.getCellarStatusServiceShaclReportLevel();
        
        // In case report is BLANK ... or report level is NOTHING
        if (StringUtils.isBlank(report) ||
            reportLevel.equals(CellarStatusServiceShaclReportLevel.NO)) {
            LOGGER.debug("Nothing to filter from the provided SHACL report.");
            return null;
        }
        
        Model model = null;
        Model reportModel = JenaUtils.create();
        List<Statement> reportResourceStatements = new ArrayList<>();
        Writer stringWriter = new StringWriter();
        Resource resource;
        try {
            model = JenaUtils.read(report, Lang.RDFJSON);
            
            // Extract Resources with ERRORS
            ResIterator resIterator = model.listSubjectsWithProperty(
                    ResourceFactory.createProperty("http://www.w3.org/ns/shacl#resultSeverity"),
                    ResourceFactory.createProperty("http://www.w3.org/ns/shacl#Violation"));
            while(resIterator.hasNext()){
                resource = resIterator.next();
                resource.listProperties().forEachRemaining(reportResourceStatements::add);
            }
            
            // In case the WARNINGs are required as well ...
            if(reportLevel.equals(CellarStatusServiceShaclReportLevel.WARNING)){
                resIterator = model.listSubjectsWithProperty(
                        ResourceFactory.createProperty("http://www.w3.org/ns/shacl#resultSeverity"),
                        ResourceFactory.createProperty("http://www.w3.org/ns/shacl#Warning"));
                while(resIterator.hasNext()){
                    resource = resIterator.next();
                    resource.listProperties().forEachRemaining(reportResourceStatements::add);
                }
            }
            // Add corresponding statements to report model
            reportModel.add(reportResourceStatements);
            resIterator.close();
            
            // Convert model to JSON-formatted String, provided that it is not empty
            String reportedContent = null;
            if (!reportModel.isEmpty()){
                // Convert reportModel to a JSON-formatted String
                reportModel.getWriter(Lang.RDFJSON.getLabel()).write(reportModel, stringWriter, "");
                reportedContent = stringWriter.toString().replace("\n", "");
                LOGGER.debug("The FILTERED SHACL report is {}",reportedContent);
            }
            return reportedContent;
        }
        finally {
            try {
                stringWriter.close();
            } catch (IOException e) {
                LOGGER.error(e.getMessage());
            }
            JenaUtils.closeQuietly(reportModel);
            JenaUtils.closeQuietly(model);
        }
    }

    /**
     * Validate tmd.
     */
    @LogContext(CMR_TMD_VALIDATION)
    private void validateTmd() {
        for (Model tmdModel : technicalModels.values()) {

            List<String> invalidUriList = NalValidationUtils.validateUris(tmdModel);
            if (!invalidUriList.isEmpty()) {
                throw ExceptionBuilder.get(CellarValidationException.class)
                        .withCode(CmrErrors.VALIDATION_RDFDATA_ERROR)
                        .withMessage("Invalid URIs found in model : {}")
                        .withMessageArgs(invalidUriList)
                        .build();
            }
            
        }
    }

    /**
     * It creates a model with all the sameAs relations of all digital objects in {@code structMap}.<br/>
     * Then, sets it into {@code businessModels[ContentType.SAME_AS]}.
     */
    private void loadSameAsModel() {
        final Model sameAsModel = ModelFactory.createDefaultModel();
        metadataLoaderService.addSameAsModelRecursive(this.data.getStructMap().getDigitalObject(), sameAsModel);
        this.businessModels.put(ContentType.SAME_AS, sameAsModel);
    }

    /**
     * It creates a model with all the relations of all digital objects in {@code structMap}.<br/>
     * Then, sets it into the {@code metsModel} field of the loader.
     */
    protected void loadMetsModel() {
        // read all business metadata into a jena-model
        this.metsModel = metadataLoaderService.loadMetadata(this.data.getMetsPackage(), this.data.getStructMap(), true);
    }

    /**
     * It creates a model with all the direct relations of all digital objects in {@code structMap}
     * and sets it into the {@code directModel} field of the builder.
     */
    protected void loadDirectModel() {
        if (this.metsModel == null) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("Cannot load the direct model because the new model from METS is null!").build();
        }
        final Model sameAsModel = this.businessModels.get(ContentType.SAME_AS);
        if (sameAsModel == null) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("Cannot load the direct model because the related sameAs model is null!").build();
        }

        // create the direct model (that is, non-inferred) by merging the sameAs and the METS model
        final Model directModel = ModelUtils.create(sameAsModel, this.metsModel);

        // renew the item-related triples
        ModelUtils.renewItemTriples(directModel, this.data.getStructMap().getDigitalObject());

        this.businessModels.put(ContentType.DIRECT, directModel);
    }

    /**
     * It creates a model that contains the inferred relations of the {@code directModel} plus the {@code directModel} itself.<br/>
     * Then, sets it into {@code businessModels[ContentType.DIRECT_INFERRED]}.
     */
    private void loadDirectAndInferredModel() {
        final Model directModel = getBusinessModels().get(ContentType.DIRECT);
        if (directModel == null) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("Cannot load the inferred model because the related direct model is null!").build();
        }

        // execute the merge between the direct model, the extra info for the inferred model and the existing model
        // then calculate the inferred model on it
        Model inferredExtraData = null;
        Model mergedModel = null;
        try {
            // calculate the extra information needed for the inferred model
            inferredExtraData = metadataLoaderService.getInferredExtraData(getData().getStructMap(), directModel);

            // create temporary model by merging the direct model with the existing model, and adding the extra info for the inferred model
            mergedModel = ModelUtils.create(directModel, inferredExtraData);

            // do inference on the temp model to get the full inferred model
            final Model inferredModel = inferenceService.calculateInferredModel(mergedModel);

            // set the resulting model into the map of business models
            getBusinessModels().put(ContentType.DIRECT_INFERRED, inferredModel);
        } finally {
            ModelUtils.closeQuietly(mergedModel, inferredExtraData);
        }
    }

    /**
     * It gets the business models out of the loader's models, and sets the {@code businessModels} map
     * whose key is the digital object of reference, and value the related technical model.
     */
    private void loadBusinessModels() {
        // load and validate the mets model
        loadMetsModel();

        // load the sameAs model
        loadSameAsModel();

        // load and validate the direct model
        loadDirectModel();
        setLastModificationDate();
        setEmbargoDate();
        setCreationDate();
        setLang();

        if(cellarConfiguration.isCellarServiceIngestionValidationMetadataEnabled()) {
            try {
                logMetadataValidation(ValidateDirectMetadata, Start, null);
                validateDirectModel();
                logMetadataValidation(ValidateDirectMetadata, End, "Successful validation");
            } catch (CellarValidationException | MetadataValidationResultAwareException e) {
                logMetadataValidation(ValidateDirectMetadata, Fail, e.getMessage());
                throw e;
            }
        }

        // load the direct and inferred model
        loadDirectAndInferredModel();

        // set the loaded business models into the calculated data
        this.data.setBusinessMetadata(this.businessModels);
    }

    private void logMetadataValidation(final AuditTrailEventAction action, final AuditTrailEventType type, final String message) {
        AuditBuilder.get(AuditTrailEventProcess.Ingestion)
                .withAction(action)
                .withType(type)
                .withObject(firstIdentifier())
                .withStructMapStatusHistory(data.getStructMapStatusHistory())
                .withPackageHistory(data.getPackageHistory())
                .withMessage(message)
                .withLogDatabase(true)
                .logEvent();
    }

    private String firstIdentifier() {
        return getData().getStructMap().getDigitalObject()
                .getContentids()
                .stream()
                .findFirst()
                .map(ContentIdentifier::toString)
                .orElse("");
    }

    /**
     * It gets the technical models out of the loader's models, and sets the {@code technicalModels} map
     * whose key is the digital object of reference, and value the related technical model.
     */
    private void loadTechnicalModels() {
        // load the technical models
        for (final DigitalObject digitalObject : this.data.getStructMap().getDigitalObject().getAllChilds(true)) {
            // search technical metadata stream
            Model model = null;
            final Metadata technicalMetadata = digitalObject.getTechnicalMetadata();
            final OperationType operationType = this.data.getStructMap().getMetsDocument().getType();
            if (technicalMetadata != null) {
                try (InputStream is = this.data.getMetsPackage().getFileStream(technicalMetadata.getMetadataStream().getFileRef())) {
                    model = JenaUtils.read(is, "TECHNICAL", RDFFormat.RDFXML_PLAIN); // from mets package, format is XML
                } catch (IOException e) {
                    throw new UncheckedIOException("Unreadable technical metadata :" + technicalMetadata.getMetadataStream().getFileRef(), e);
                }
            } else if ((operationType == OperationType.Update) || (operationType == OperationType.Delete)
                    && cellarResourceDao.exists(digitalObject.getCellarId().getIdentifier())) {
                final String identifier = digitalObject.getCellarId().getIdentifier();
                final CellarResource findCellarId = cellarResourceDao.findCellarId(identifier);
                if (findCellarId != null) {
                    // get technical metadata from s3 if not given and allowed
                    model = contentStreamService.getContent(identifier, findCellarId.getVersion(ContentType.TECHNICAL).orElse(null), ContentType.TECHNICAL)
                            .map(i -> {
                                try {
                                    return JenaUtils.read(i, Lang.NT);
                                } finally {
                                    IOUtils.closeQuietly(i);
                                }
                            })
                            .orElse(null);
                }
            }

            // if metadata found put it in a model
            if (model != null) {
                final List<ContentStream> contentStreams = digitalObject.getContentStreams();
                for (ContentStream contentStream : contentStreams) {
                    final Boolean readOnly = metadataLoaderService.isReadOnly(contentStream, model);
                    if (readOnly != null) {
                        metadataLoaderService.markAsReadOnly(contentStream, readOnly);
                        final CellarIdentifier cellarIdentifier = cellarIdentifierService.getCellarIdentifier(contentStream.getCellarId().getIdentifier());
                        cellarIdentifier.setReadOnly(readOnly);
                        cellarIdentifierService.updateCellarIdentifier(cellarIdentifier);
                    }
                }
                this.technicalModels.put(digitalObject, model);
            }
        }

        // set the loaded technical models into the calculated data
        data.setTechnicalMetadata(technicalModels);

        try {
            logMetadataValidation(ValidateTechnicalMetadata, Start, null);
            validateTmd();
            logMetadataValidation(ValidateTechnicalMetadata, End, "Successful validation");
        } catch (CellarValidationException | MetadataValidationResultAwareException e) {
            logMetadataValidation(ValidateTechnicalMetadata, Fail, e.getMessage());
            throw e;
        }
    }

    /**
     * It validates the mets package against some static rules.
     *
     * @param data the data
     */
    private void validatePackage(final CalculatedData data) {
        if (data == null) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Calculated data cannot be null.").build();
        }
        if (data.getMetsPackage() == null) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Calculated data without METS package.").build();
        }
        if (data.getStructMap() == null) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Calculated data without structMap.").build();
        }
        if (data.getStructMap().getDigitalObject() == null) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("StructMap without digitalObject.").build();
        }

        ModelLoadUtils.validateModels(data);
    }

    /**
     * Gets the data.
     *
     * @return the data
     */
    protected CalculatedData getData() {
        return this.data;
    }

    /**
     * Gets the mets model.
     *
     * @return the mets model
     */
    protected Model getMetsModel() {
        return this.metsModel;
    }

    /**
     * Gets the business models.
     *
     * @return the business models
     */
    protected Map<ContentType, Model> getBusinessModels() {
        return this.businessModels;
    }

    /**
     * Sets the last modification date.
     */
    private void setLastModificationDate() {
        final Model directModel = getBusinessModels().get(ContentType.DIRECT);
        if (directModel == null) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("Cannot set the last modification date because the related direct model is null!").build();
        }

        // remove each occurrence of lastModificationDate
        metadataLoaderService.removeLastModifiedInModelRecursive(getData().getStructMap().getDigitalObject(), directModel);
        // add lastModificationDate for each digital object
        getData().getStructMap().getDigitalObject().applyStrategyOnSelfAndChildren(digitalObject -> {
            final Resource cellarResource = directModel.getResource(digitalObject.getCellarId().getUri());
            metadataLoaderService.addLastModificationInModel(digitalObject, cellarResource, directModel);
        });

    }

    /**
     * Sets the embargo date.
     */
    private void setEmbargoDate() {
        final Model directModel = getBusinessModels().get(ContentType.DIRECT);
        if (directModel == null) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("Cannot set the embargo date because the related direct model is null!").build();
        }
        final StructMap structMap = getData().getStructMap();
        structMap.getDigitalObject().applyStrategyOnSelfAndChildren(digitalObject -> {
            final boolean underEmbargo = DigitalObjectUtils.isUnderEmbargo(digitalObject);

            //add embargo property if it does not exist
            final DigitalObjectType type = digitalObject.getType();
            final String embargoPropertyUri = HierarchyElement.getTriple(type).getTwo().getURI();
            final boolean containsEmbargoProperty = ModelUtils.containsProperty(digitalObject.getCellarId().getUri(), directModel,
                    embargoPropertyUri);
            if (underEmbargo && !containsEmbargoProperty) {

                final Resource cellarResource = directModel.getResource(digitalObject.getCellarId().getUri());
                metadataLoaderService.addEmbargoDateInModel(digitalObject, cellarResource, directModel);
            }

        });
    }

    /**
     * Sets the creation date.
     */
    private void setCreationDate() {
        final Model directModel = getBusinessModels().get(ContentType.DIRECT);
        if (directModel == null) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("Cannot set the creation date because the related direct model is null!").build();
        }

        // add creationDate for each digital object
        for (final DigitalObject digitalObject : getData().getAddedDigitalObjects()) {
            final Resource cellarResource = directModel.getResource(digitalObject.getCellarId().getUri());
            metadataLoaderService.addCreationDateInModel(cellarResource, directModel, digitalObject.getLastModificationDate());
        }
    }

    /**
     * Sets the lang.
     */
    private void setLang() {
        final Model directModel = getBusinessModels().get(ContentType.DIRECT);
        if (directModel == null) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Cannot set the lang because the related direct model is null!")
                    .build();
        }

        // add lang for each expression
        getData().getStructMap().getDigitalObject().applyStrategyOnSelfAndChildren(digitalObject -> {
            if (digitalObject.getType() == DigitalObjectType.EXPRESSION) {
                final Resource cellarResource = directModel.getResource(digitalObject.getCellarId().getUri());
                metadataLoaderService.addLanguageMetadata(digitalObject, directModel, cellarResource);
            }
        });

    }

    /**
     * This method retrieve the CELLAR id from the CalculatedData if exist but
     * <strong>NEVER</strong> throw an exception
     *
     * @return
     */
    protected String safeCellarId() {
        return Optional.ofNullable(getData())
                .map(CalculatedData::getStructMap)
                .map(StructMap::getDigitalObject)
                .map(DigitalObject::getCellarId)
                .map(ContentIdentifier::getIdentifier)
                .orElse("");
    }

}
