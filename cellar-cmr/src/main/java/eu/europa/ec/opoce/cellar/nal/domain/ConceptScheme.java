package eu.europa.ec.opoce.cellar.nal.domain;

import java.util.Arrays;
import java.util.Objects;

/**
 * <p>ConceptScheme class.</p>
 */
public class ConceptScheme {

    private Uri uri;
    private String version;
    private String date;
    private String lastModified;
    private LanguageString[] labels;

    /**
     * <p>Constructor for ConceptScheme.</p>
     *
     * @param uri a {@link Uri} object.
     */
    public ConceptScheme(Uri uri) {
        this.uri = uri;
    }

    /**
     * <p>Setter for the field <code>version</code>.</p>
     *
     * @param version a {@link java.lang.String} object.
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * <p>Setter for the field <code>date</code>.</p>
     *
     * @param date a {@link java.lang.String} object.
     */
    public void setDate(String date) {
        this.date = date;
    }

    /**
     * <p>Setter for the field <code>lastModified</code>.</p>
     *
     * @param lastModified a {@link java.lang.String} object.
     */
    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    /**
     * <p>Setter for the field <code>labels</code>.</p>
     *
     * @param labels an array of {@link LanguageString} objects.
     */
    public void setLabels(LanguageString[] labels) {
        this.labels = labels;
    }

    /**
     * <p>Getter for the field <code>uri</code>.</p>
     *
     * @return a {@link Uri} object.
     */
    public Uri getUri() {
        return uri;
    }

    /**
     * <p>Getter for the field <code>version</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getVersion() {
        return version;
    }

    /**
     * <p>Getter for the field <code>date</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDate() {
        return date;
    }

    /**
     * <p>Getter for the field <code>lastModified</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLastModified() {
        return lastModified;
    }

    /**
     * <p>Getter for the field <code>labels</code>.</p>
     *
     * @return an array of {@link LanguageString} objects.
     */
    public LanguageString[] getLabels() {
        return labels;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ConceptScheme)) return false;
        ConceptScheme that = (ConceptScheme) o;
        return Objects.equals(getUri(), that.getUri()) &&
                Objects.equals(getVersion(), that.getVersion()) &&
                Objects.equals(getDate(), that.getDate()) &&
                Objects.equals(getLastModified(), that.getLastModified()) &&
                Arrays.equals(getLabels(), that.getLabels());
    }

    @Override
    public int hashCode() {

        int result = Objects.hash(getUri(), getVersion(), getDate(), getLastModified());
        result = 31 * result + Arrays.hashCode(getLabels());
        return result;
    }

    @Override
    public String toString() {
        return "ConceptScheme{" +
                "uri=" + uri +
                ", version='" + version + '\'' +
                ", date='" + date + '\'' +
                ", lastModified='" + lastModified + '\'' +
                ", labels=" + Arrays.toString(labels) +
                '}';
    }
}
