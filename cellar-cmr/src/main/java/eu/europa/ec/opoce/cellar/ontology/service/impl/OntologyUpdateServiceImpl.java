/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ontology
 *             FILE : OntologyUpdateServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 01 16, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-01-16 13:45:11 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ontology.service.impl;

import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.database.dao.OntoConfigDao;
import eu.europa.ec.opoce.cellar.ontology.dao.OntoConfig;
import eu.europa.ec.opoce.cellar.ontology.domain.OntologyUpdate;
import eu.europa.ec.opoce.cellar.ontology.exception.OntologyException;
import eu.europa.ec.opoce.cellar.ontology.service.OntologyUpdateResult;
import eu.europa.ec.opoce.cellar.ontology.service.OntologyUpdateService;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.virtuoso.VirtuosoService;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.RDFFormat;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.Charset;
import java.util.Date;
import java.util.Map;

import static eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration.ONTOLOGY;
import static eu.europa.ec.opoce.cellar.nal.NalOntoUtils.ONTO_PREFIX_LEGACY;

/**
 * @author ARHS Developments
 * @since 7.7
 */
@Service
public class OntologyUpdateServiceImpl implements OntologyUpdateService {

    private static final Logger LOG = LogManager.getLogger(OntologyUpdateServiceImpl.class);

    private static final String VERSION_SPARQL = "" +
            "PREFIX owl:<http://www.w3.org/2002/07/owl#>" +
            " SELECT ?versionInfo" +
            " WHERE { ?s owl:versionInfo ?versionInfo . }";

    private final OntoConfigDao ontoConfigDao;
    private final ContentStreamService contentStreamService;
    private final ApplicationEventPublisher eventPublisher;
    private final VirtuosoService virtuosoService;

    @Autowired
    public OntologyUpdateServiceImpl(OntoConfigDao ontoConfigDao, ContentStreamService contentStreamService, ApplicationEventPublisher eventPublisher, VirtuosoService virtuosoService) {
        this.ontoConfigDao = ontoConfigDao;
        this.contentStreamService = contentStreamService;
        this.eventPublisher = eventPublisher;
        this.virtuosoService = virtuosoService;
    }

    @Override
    @Transactional
    public OntologyUpdateResult update(OntologyUpdate ontologyUpdate, boolean override) {
        final OntologyUpdateResult result = new OntologyUpdateResult();
        for (Map.Entry<String, Model> values : ontologyUpdate.getUpdates().entrySet()) {
            final String uri = uri(values.getKey());
            OntoConfig ontoConfig = ontoConfigDao.findByUri(uri).orElseGet(() -> newOntoConfig(uri));
            Date now = new Date();
            ontoConfig.setVersionDate(now);
            ontoConfig.setCreatedOn(now);

            final Model model = values.getValue();

            if (ontoConfig.getExternalPid() == null) {
                String name = uri.substring(uri.lastIndexOf('/') + 1);
                ontoConfig.setExternalPid(ONTO_PREFIX_LEGACY + "ontology-" + name);
                LOG.warn("Missing PID for the existing Ontology {} -> Assign the new value: {}", uri, ontoConfig.getExternalPid());
            } else if (ontoConfig.getExternalPid() != null && ontoConfig.getVersion() != null && !override) {
                LOG.info("Ontology [{}] already exists", ontoConfig.getName());
                result.addModel(model, false);
                continue;
            }

            try {
                // Retrieve version from model if exist
                ontoConfig.setVersionNr(readVersionNr(model));

                // Persist in Oracle
                ontoConfigDao.saveOrUpdateObject(ontoConfig);

                // Persist in S3
                final String version = contentStreamService.writeContent(ontoConfig.getExternalPid(), toResource(model), ContentType.DIRECT);
                LOG.debug(ONTOLOGY, "Ontology {} persisted, repository: {} ({})", uri, ontoConfig.getExternalPid(), version);

                result.addModel(model, true);

                // Persist in Virtuoso
                virtuosoService.writeOnto(model, uri, true);

                // Cache entry will be evict from the cache after COMMIT
                eventPublisher.publishEvent(new ClearOntologyCacheEvent(ontoConfig.getExternalPid()));

            } catch (Exception e) {
                throw new OntologyException("Error when updating ontology [" + ontologyUpdate.getOntologyUri() + "]", e);
            }
        }
        return result;
    }

    private static String readVersionNr(Model ontology) {
        return JenaUtils.select(VERSION_SPARQL, ontology).stream()
                .map(sln -> sln.get("versionInfo"))
                .map(n -> n.asLiteral().getLexicalForm())
                .findFirst()
                .orElse("");
    }

    private static OntoConfig newOntoConfig(String uri) {
        LOG.error("Missing Ontology [{}], fallback to default. Please check the version and upgrade it if needed.", uri);
        String name;
        if (uri.endsWith("/")) {
            name = uri(uri.substring(0, uri.length() - 1));
        } else {
            name = uri(uri);
        }
        name = name.substring(name.lastIndexOf('/') + 1, name.length());

        OntoConfig ontoConfig = new OntoConfig();
        ontoConfig.setExternalPid(null);
        ontoConfig.setName(name.toUpperCase());
        ontoConfig.setUri(uri(uri));
        return ontoConfig;
    }

    /**
     * The declaration of the URI into the RDF files seems not
     * really reliable. In order to prevent some misbehaviour,
     * we always delete the '#' at the end of the URI if there
     * is one.
     *
     * @param uri the URI to transform
     * @return the transformed URI
     */
    private static String uri(String uri) {
        String value = uri.replaceAll("#", "");
        if (value.endsWith("/")) {
            value = uri.substring(0, uri.length() - 1);
        }
        return value;
    }

    private static Resource toResource(Model model) {
        return new ByteArrayResource(JenaUtils.toString(model, RDFFormat.NT).getBytes(Charset.forName("UTF-8")));
    }
}
