/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.database
 *             FILE : IRDFStoreRelationalGateway.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 25-07-2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.database;

import org.apache.jena.graph.Triple;
import org.apache.jena.rdf.model.Model;
import eu.europa.ec.opoce.cellar.cmr.notice.cache.ISameAsCache;
import eu.europa.ec.opoce.cellar.common.util.Pair;

import java.sql.Connection;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * <class_description> Gateway to be used to access the RDF store in a relational way.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 25-07-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IRDFStoreRelationalGateway {

    /**
     * Insert quads in Oracle RDF store.
     *
     * @param connection    connection to use to perform the operation on the RDF store
     * @param quads         all triples together with the context where to put them in
     * @param syncDissTable if true, synchronize dissemination table
     * @param table         table where the data is
     */
    void insert(final Connection connection, final List<Pair<Triple, String>> quads, final CmrTableName table, final boolean syncDissTable);

    default void insert(final Connection connection, final List<Pair<Triple, String>> quads, final CmrTableName table) {
        insert(connection, quads, table, true);
    }

    /**
     * Remove quads from Oracle RDF store.
     *
     * @param connection    connection to use to perform the operation on the RDF store
     * @param quads         all triples together with the context where to put them in
     * @param oldQuads      a {@link java.util.Map} object
     * @param table         of the table where the data is
     * @param syncDissTable if true, synchronize dissemination table
     */
    void remove(final Connection connection, final List<Pair<Triple, String>> quads,
                final Map<Pair<Triple, String>, String> oldQuads, final CmrTableName table, final boolean syncDissTable);

    /**
     * Delete the given context.
     *
     * @param connection connection to use to perform the operation on the RDF store
     * @param contexts   the contexts to delete
     * @param tables     tables where the data is
     */
    void deleteContexts(final Connection connection, final List<String> contexts, final CmrTableName... tables);

    /**
     * Move contexts.
     *
     * @param connection the connection
     * @param contexts   the contexts
     * @param fromTable  the from table
     * @param toTable    the to table
     */
    void moveContexts(final Connection connection, final List<String> contexts, final CmrTableName fromTable, final CmrTableName toTable);

    /**
     * Synchronize the given dissemination table relatively to the contexts present in the given list of quads.
     *
     * @param connection the connection to use
     * @param quads      the quads whose contexts are to synchronize
     * @param table      the dissemination table to synchronize
     */
    void syncDissTable(final Connection connection, final List<Pair<Triple, String>> quads, final CmrTableName table);

    /**
     * Synchronize the given dissemination table relatively to the contexts present in the given list of quads.
     *
     * @param connection  the connection to use
     * @param context     the context to synchronize
     * @param sameAsCache the cache for sameAses, if any
     * @param table       the dissemination table to synchronize
     */
    void syncDissTable(final Connection connection, final String context, final ISameAsCache sameAsCache, final CmrTableName table);

    /**
     * Select the model of the given context using the given connection.
     *
     * @param table      table where the data is
     * @param context    the context to select
     * @param connection the connection to use
     * @return a model with all the data about the context in a given table
     */
    Model selectModel(CmrTableName table, String context, Connection connection);

    /**
     * Select the model of the given context.
     *
     * @param table   table where the data is
     * @param context the context to select
     * @return a model with all the data about the context in a given table
     */
    Model selectModel(final CmrTableName table, final String context);

    /**
     * Select the models of the given contexts from the dissemination table.
     *
     * @param table    table where the data is
     * @param contexts the list of contexts to select
     * @return a collection of models with all the data about the contexts in a given table
     */
    Collection<Model> selectModels(final CmrTableName table, final List<String> contexts);

    /**
     * Select the models of the given contexts from the dissemination table.
     *
     * @param table    table where the data is
     * @param contexts the list of contexts to select
     * @return a map of context-model with all the data about the contexts in a given table
     */
    Map<String, Model> selectModelsPerContext(final CmrTableName table, final List<String> contexts);

    /**
     * Select the model of the given context together with all the child-contexts below this context.
     *
     * @param table   table where the data is
     * @param context the context to select
     * @return a model with all the data about the selected context in a given table
     */
    Map<Pair<Triple, String>, String> selectCompleteContext(final CmrTableName table, final String context);

    /**
     * Select complete context names.
     *
     * @param table   the table
     * @param context the context
     * @return the list
     */
    List<String> selectCompleteContextNames(final CmrTableName table, final String context);

    /**
     * Check if the given context is used in the given table.
     *
     * @param table   table where the data is
     * @param context the context to check
     * @return a boolean.
     */
    boolean isContextUsedInTable(final CmrTableName table, final String context);

    //CELLARM-1121
    //TODO place the queries in an application context to wire it and share through the different services
    // replace the following 4 methods by one that executes a given query

    /**
     * Drop virtual model.
     *
     * @param virtualModelName the virtual model name
     */
    void dropVirtualModel(final String virtualModelName);

    /**
     * Drop sem model.
     *
     * @param semModelName the sem model name
     */
    void dropSemModel(final String semModelName);

    /**
     * Drop entailment.
     *
     * @param entailmentName the entailment name
     */
    void dropEntailment(final String entailmentName);

    /**
     * Drop table.
     *
     * @param tableName the table name
     */
    void dropTable(final String tableName);

}
