package eu.europa.ec.opoce.cellar.server.filter;

import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

/**
 * <p>Abstract RequestLogUtils class.</p>
 */
public abstract class RequestLogUtils {

    public static enum OPTIONS {
        INCL_QUERYSTRING, INCL_CLIENTINFO, INCL_HEADERINFO
    }

    /**
     * <p>newLogMessageFor.</p>
     *
     * @param request a {@link javax.servlet.http.HttpServletRequest} object.
     * @param options a {@link eu.europa.ec.opoce.cellar.server.filter.RequestLogUtils.OPTIONS} object.
     * @return a {@link java.lang.String} object.
     */
    public static String newLogMessageFor(HttpServletRequest request, OPTIONS... options) {
        Set<OPTIONS> optionSet = new HashSet<OPTIONS>();
        if (options != null) {
            optionSet.addAll(Arrays.asList(options));
        }

        StringBuilder msg = new StringBuilder();
        msg.append("uri=").append(request.getRequestURI());
        if (optionSet.contains(OPTIONS.INCL_QUERYSTRING)) {
            String queryString = request.getQueryString();
            if (StringUtils.isNotBlank(queryString)) {
                msg.append('?').append(queryString);
            }
        }
        if (optionSet.contains(OPTIONS.INCL_CLIENTINFO)) {
            String client = request.getRemoteAddr();
            if (StringUtils.isNotBlank(client)) {
                msg.append(";client=").append(client);
            }
            HttpSession session = request.getSession(false);
            if (session != null) {
                msg.append(";session=").append(session.getId());
            }
            String user = request.getRemoteUser();
            if (user != null) {
                msg.append(";user=").append(user);
            }
        }
        if (optionSet.contains(OPTIONS.INCL_HEADERINFO)) {
            Enumeration<?> headerNames = request.getHeaderNames();
            boolean hasMoreElements;
            if (hasMoreElements = headerNames.hasMoreElements()) {
                msg.append(";headers=[");
                while (hasMoreElements) {
                    String name = (String) headerNames.nextElement();
                    msg.append(name).append("=").append(request.getHeader(name));
                    if (hasMoreElements = headerNames.hasMoreElements()) {
                        msg.append(", ");
                    }
                }
                msg.append("]");
            }
        }

        return msg.toString();
    }

}
