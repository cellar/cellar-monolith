/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.graph
 *             FILE : IGraphBuilder.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : May 2, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.graph;

import eu.europa.ec.opoce.cellar.cmr.graph.impl.GraphBuilder;

/**
 * <class_description> Graph builder interface.
 * <br/><br/>
 * ON : May 2, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IGraphBuilder {

    /**
     * Initialize the graph builder.
     * @return the graph builder
     */
    GraphBuilder initialize();

    /**
     * Build the graph base.
     * @return the graph builder
     */
    GraphBuilder buildBase();

    /**
     * Build the graph relations.
     * @return the graph builder
     */
    GraphBuilder buildRelations();

    /**
     * Return the builded graph.
     * @return the builded graph
     */
    IGraph getGraph();
}
