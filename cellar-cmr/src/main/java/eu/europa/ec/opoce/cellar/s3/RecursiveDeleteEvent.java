package eu.europa.ec.opoce.cellar.s3;

import java.util.Map;

public class RecursiveDeleteEvent {

    private final String baseCellarId;
    private final Map<String, String> deleteMarkers;

    public RecursiveDeleteEvent(String baseCellarId, Map<String, String> deleteMarkers) {
        this.baseCellarId = baseCellarId;
        this.deleteMarkers = deleteMarkers;
    }

    public String getBaseCellarId() {
        return baseCellarId;
    }

    public Map<String, String> getDeleteMarkers() {
        return deleteMarkers;
    }
}
