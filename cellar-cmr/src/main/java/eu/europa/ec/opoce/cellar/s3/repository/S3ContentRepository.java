package eu.europa.ec.opoce.cellar.s3.repository;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.event.ProgressEvent;
import com.amazonaws.event.ProgressEventType;
import com.amazonaws.event.ProgressListener;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.Headers;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.services.s3.model.DeleteObjectsRequest.KeyVersion;
import com.amazonaws.services.s3.model.Tag;
import com.amazonaws.services.s3.model.DeleteObjectsResult.DeletedObject;
import com.amazonaws.services.s3.transfer.*;
import com.amazonaws.services.s3.transfer.internal.S3ProgressListener;
import com.amazonaws.services.s3.transfer.model.UploadResult;
import com.google.common.annotations.Beta;
import com.google.common.base.Stopwatch;
import com.google.common.base.Strings;
import com.google.common.base.Throwables;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.s3.S3Configuration;
import eu.europa.ec.opoce.cellar.s3.domain.ContentVersion;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Timer;
import io.micrometer.core.instrument.*;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.MarkerManager;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.retry.support.RetrySynchronizationManager;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.NumberFormat;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.amazonaws.event.ProgressEventType.TRANSFER_COMPLETED_EVENT;
import static eu.europa.ec.opoce.cellar.s3.AWSConfiguration.DEFAULT_REGION;
import static eu.europa.ec.opoce.cellar.s3.ContentStreamService.Metadata.FILENAME;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.MINUTES;

@Service
public class S3ContentRepository implements ContentRepository {

    private static final Logger LOG = LogManager.getLogger(S3ContentRepository.class);
    private static final Marker S3 = MarkerManager.getMarker("S3");
    private static final Predicate<String> ALL = s -> true;
    private static final NumberFormat PERCENT_FORMAT = NumberFormat.getPercentInstance();
    private static final long TIMEOUT_UPLOAD_MINUTES = 10;

    private final AmazonS3 s3Client;
    private final TransferManager transferManager;
    private final S3Configuration s3Conf;

    private ICellarConfiguration cellarConfiguration;

    // Metrics
    private final Counter s3ReadCounter = Metrics.counter("S3.read.count");
    private final Counter s3WriteCounter = Metrics.counter("S3.write.count");
    private final Timer s3ReadTimer = Metrics.timer("S3.read.timer");
    private final Timer s3WriteTimer = Metrics.timer("S3.write.timer");
    private final DistributionSummary s3ReadDistribution;
    private final DistributionSummary s3WriteDistribution;

    @Autowired
    public S3ContentRepository(AmazonS3 amazonS3, S3Configuration s3Configuration, ICellarConfiguration cellarConfiguration,
                               MeterRegistry meterRegistry) {
        this.s3Client = amazonS3;
        this.s3Conf = s3Configuration;
        this.transferManager = TransferManagerBuilder.standard()
                .withS3Client(amazonS3)
                .build();
        this.cellarConfiguration = cellarConfiguration;

        this.s3ReadDistribution = DistributionSummary.builder("S3.read.distribution")
                .scale(100)
                .baseUnit("bytes")
                .register(meterRegistry);

        this.s3WriteDistribution = DistributionSummary.builder("S3.write.distribution")
                .scale(100)
                .baseUnit("bytes")
                .register(meterRegistry);

        PERCENT_FORMAT.setMinimumFractionDigits(2);
    }

    @PostConstruct
    public void check() {
        isAccessible();
        BucketVersioningConfiguration cfg = s3Client.getBucketVersioningConfiguration(s3Conf.getBucket());
        Assert.assertEquals("The versioning for bucket [" + s3Conf.getBucket() + "] is not enabled.",
                BucketVersioningConfiguration.ENABLED, cfg.getStatus());
    }

    @PreDestroy
    public void shutdown() {
        transferManager.shutdownNow(true);
    }

    @Override
    public void isAccessible() {
        try {
            s3Client.headBucket(new HeadBucketRequest(s3Conf.getBucket()));
        } catch (AmazonServiceException e) {
            switch (e.getStatusCode()) {
                case 301:
                    LOG.error("The bucket is not in the region: " + DEFAULT_REGION, e);
                    break;
                case 403:
                    LOG.error("Credentials for AWS are not valid", e);
                    break;
                case 404:
                    LOG.error("The bucket " + s3Conf.getBucket() + " does not exist", e);
                    break;
                default:
                    LOG.error("Unexpected error", e);
            }
            throw e;
        }
    }

    @Override
    public Optional<InputStream> getContent(String cellarID, String version, ContentType contentType) {
        s3ReadCounter.increment();
        Stopwatch watch = Stopwatch.createStarted();
        try {
            Optional<InputStream> content = Optional.ofNullable(s3Client.getObject(new GetObjectRequest(s3Conf.getBucket(),
                    asS3Identifier(cellarID, contentType), version)))
                    .map(this::reportSize)
                    .map(S3Object::getObjectContent);
            watch.stop();
            s3ReadTimer.record(watch.elapsed(MILLISECONDS), MILLISECONDS);
            LOG.debug(S3, "{} {} ({}) retrieved in {} ms", contentType, cellarID, version, watch.elapsed(MILLISECONDS));
            return content;
        } catch (SdkClientException e) {
            LOG.debug(e);
            return Optional.empty();
        }
    }

    @Override
    public Optional<String> getLargeContent(String cellarID, String version, ContentType contentType) {
        s3ReadCounter.increment();
        String v = Strings.isNullOrEmpty(version) ? null : version; // use null because S3 API does not accept empty version
        try {
            Stopwatch watch = Stopwatch.createStarted();
            Path tmpFile = Files.createTempFile(cellarID + contentType, ".nt");
            Download download = transferManager.download(new GetObjectRequest(s3Conf.getBucket(), asS3Identifier(cellarID, contentType), v), tmpFile.toFile());
            download.addProgressListener(new VerboseDownloadListener(cellarID, contentType));
            download.waitForCompletion();
            watch.stop();
            s3ReadTimer.record(watch.elapsed(MILLISECONDS), MILLISECONDS);
            byte[] data = Files.readAllBytes(tmpFile);
            if (data.length > 0) {
                s3ReadDistribution.record(data.length);
            }
            return Optional.of(new String(data, StandardCharsets.UTF_8));

        } catch (AmazonS3Exception | IOException | InterruptedException e) {
            if (e instanceof InterruptedException) {
                Thread.currentThread().interrupt();
            }
            LOG.error("Error retrieving " + contentType + " with ID " + cellarID + " (" + version + ")", e);
            return Optional.empty();
        }
    }

    @Override
    public List<ContentVersion> getVersions(String cellarID, ContentType contentType) {
        return getVersions(cellarID, contentType, Integer.MAX_VALUE);
    }

    @Override
    public List<ContentVersion> getVersions(String cellarID, ContentType contentType, int maxResults) {
        s3ReadCounter.increment();
        try {
            VersionListing versions = s3Client.listVersions(s3Conf.getBucket(), asS3Identifier(cellarID) + "/" + contentType.file(),
                    null, null, null, maxResults);
            return asContentVersion(versions);
        } catch (SdkClientException e) {
            LOG.debug(e);
            return Collections.emptyList();
        }
    }

    private static List<ContentVersion> asContentVersion(VersionListing listing) {
        return listing.getVersionSummaries().stream()
                .map(S3ContentRepository::asContentVersion)
                .collect(Collectors.toList());
    }

    /**
     * Restore strategies;
     * A.
     * 1. retrieve the previous version
     * 2. put that version of the object as the latest version
     * B.
     * 1. delete all the version between the latest and the version to reach.
     *
     * @param cellarID
     * @param version
     * @param contentType
     */
    @Override
    @Retryable(
            exceptionExpression = "@cellarStaticConfiguration.isS3RetryEnabled()", maxAttemptsExpression = "@cellarStaticConfiguration.getS3RetryMaxAttempts()",
            value = S3DataException.class,
            backoff = @Backoff(delayExpression = "@cellarStaticConfiguration.getS3RetryDelay()", randomExpression = "@cellarStaticConfiguration.isS3RetryRandom()",
                    multiplierExpression = "@cellarStaticConfiguration.getS3RetryMultiplier()", maxDelayExpression = "@cellarStaticConfiguration.getS3RetryMaxDelay()")
    )
    public String restore(String cellarID, String version, ContentType contentType) {
        try (InputStream toRestore = getContent(cellarID, version, contentType).orElseThrow(() ->
                new S3DataException("Version [" + version + "] not found for object [" + asS3Identifier(cellarID, contentType) + "]"))) {
            return putContent(cellarID, new InputStreamResource(toRestore), contentType);
        } catch (IOException e) {
            LOG.error("Cannot restore {} {} ({})", cellarID, contentType, version, Throwables.getStackTraceAsString(e));
        }
        return version;
    }

    @Override
    @Retryable(
            exceptionExpression = "@cellarStaticConfiguration.isS3RetryEnabled()", maxAttemptsExpression = "@cellarStaticConfiguration.getS3RetryMaxAttempts()",
            value = S3DataException.class,
            backoff = @Backoff(delayExpression = "@cellarStaticConfiguration.getS3RetryDelay()", randomExpression = "@cellarStaticConfiguration.isS3RetryRandom()",
                    multiplierExpression = "@cellarStaticConfiguration.getS3RetryMultiplier()", maxDelayExpression = "@cellarStaticConfiguration.getS3RetryMaxDelay()")
    )
    public Map<String, Object> getContentMetadata(String cellarID, String version, ContentType contentType) {
        logS3RetryAttempt();
        //testing purposes
        testS3Retry("getContentMetadata");

        return asMap(getRawMetadata(cellarID, version, contentType), contentType);
    }
    private ObjectMetadata getRawMetadata(String cellarID, String version, ContentType contentType) {
        s3ReadCounter.increment();
        try {
            return s3Client.getObjectMetadata(new GetObjectMetadataRequest(s3Conf.getBucket(),
                    asS3Identifier(cellarID) + "/" + contentType.file(), version));
        } catch (SdkClientException e) {
            throw new S3DataException("Cannot retrieve " + contentType + " metadata for " + cellarID + " (" + version + ")", e);
        }
    }

    /**
     * Restore by deleting all the intermediary versions
     *
     * @param cellarID
     * @param version
     * @param contentType
     */
    @Retryable(
            exceptionExpression = "@cellarStaticConfiguration.isS3RetryEnabled()", maxAttemptsExpression = "@cellarStaticConfiguration.getS3RetryMaxAttempts()",
            value = S3DataException.class,
            backoff = @Backoff(delayExpression = "@cellarStaticConfiguration.getS3RetryDelay()", randomExpression = "@cellarStaticConfiguration.isS3RetryRandom()",
                    multiplierExpression = "@cellarStaticConfiguration.getS3RetryMultiplier()", maxDelayExpression = "@cellarStaticConfiguration.getS3RetryMaxDelay()")
    )
    public void restoreDelete(String cellarID, String version, ContentType contentType) {
        List<ContentVersion> versions = getVersions(cellarID, contentType);
        for (ContentVersion v : versions) {
            if (v.getVersion().equals(version)) {
                break;
            }
            deleteContent(cellarID, v.getVersion(), contentType);
        }
    }

    @Override
    @Retryable(
            exceptionExpression = "@cellarStaticConfiguration.isS3RetryEnabled()", maxAttemptsExpression = "@cellarStaticConfiguration.getS3RetryMaxAttempts()",
            value = S3DataException.class,
            backoff = @Backoff(delayExpression = "@cellarStaticConfiguration.getS3RetryDelay()", randomExpression = "@cellarStaticConfiguration.isS3RetryRandom()",
                    multiplierExpression = "@cellarStaticConfiguration.getS3RetryMultiplier()", maxDelayExpression = "@cellarStaticConfiguration.getS3RetryMaxDelay()")
    )
    public String putContent(String cellarID, Resource resource, ContentType contentType) {
        logS3RetryAttempt();
        //testing purposes
        testS3Retry("putContent-newObjectMetadata");

        return putContent(cellarID, resource, contentType, new ObjectMetadata());
    }


    @Override
    @Retryable(
            exceptionExpression = "@cellarStaticConfiguration.isS3RetryEnabled()", maxAttemptsExpression = "@cellarStaticConfiguration.getS3RetryMaxAttempts()",
            value = S3DataException.class,
            backoff = @Backoff(delayExpression = "@cellarStaticConfiguration.getS3RetryDelay()", randomExpression = "@cellarStaticConfiguration.isS3RetryRandom()",
                    multiplierExpression = "@cellarStaticConfiguration.getS3RetryMultiplier()", maxDelayExpression = "@cellarStaticConfiguration.getS3RetryMaxDelay()")
    )
    public EnumMap<ContentType, String> putContents(String cellarID, Map<ContentType, String> contents) {
        logS3RetryAttempt();
        //testing purposes
        testS3Retry("putContents");

        return putContents(cellarID, contents, new ObjectMetadata());
    }

    @Override
    @Retryable(
            exceptionExpression = "@cellarStaticConfiguration.isS3RetryEnabled()", maxAttemptsExpression = "@cellarStaticConfiguration.getS3RetryMaxAttempts()",
            value = S3DataException.class,
            backoff = @Backoff(delayExpression = "@cellarStaticConfiguration.getS3RetryDelay()", randomExpression = "@cellarStaticConfiguration.isS3RetryRandom()",
                    multiplierExpression = "@cellarStaticConfiguration.getS3RetryMultiplier()", maxDelayExpression = "@cellarStaticConfiguration.getS3RetryMaxDelay()")
    )
    public String putContent(String cellarID, Resource resource, ContentType contentType, Map<String, String> metadata) {
        logS3RetryAttempt();
        //testing purposes
        testS3Retry("putContent-asObjectMetadata");

        return putContent(cellarID, resource, contentType, asObjectMetadata(metadata));
    }

    private String putContent(String cellarID, Resource resource, ContentType contentType, ObjectMetadata metadata) {
        s3WriteCounter.increment();
        metadata.addUserMetadata("cellarId", cellarID);
        Stopwatch watch = Stopwatch.createStarted();

        if (contentType == ContentType.FILE) {
            return putContentFromFile(cellarID, resource, contentType, metadata, watch);
        } else {
            return putContentFromNonFile(cellarID, resource, contentType, metadata, watch);
        }
    }

    private String putContentFromFile(String cellarID, Resource resource, ContentType contentType, ObjectMetadata metadata, Stopwatch watch) {
        try (FileInputStream fis = new FileInputStream(resource.getFile())) {
            Upload upload = transferManager.upload(s3Conf.getBucket(), asS3Identifier(cellarID, contentType), fis, metadata);
            upload.addProgressListener(new VerboseUploadListener(cellarID, contentType, s3WriteDistribution));
            UploadResult result = upload.waitForUploadResult();
            watch.stop();
            s3WriteTimer.record(watch.elapsed(MILLISECONDS), MILLISECONDS);
            return result.getVersionId();
        } catch (Exception e) {
            throw new S3DataException("S3 update failed for [" + cellarID + "]", e);
        }
    }

    private String putContentFromNonFile(String cellarID, Resource resource, ContentType contentType, ObjectMetadata metadata, Stopwatch watch) {
        try (InputStream is = resource.getInputStream()) {
            PutObjectRequest putRequest = new PutObjectRequest(s3Conf.getBucket(), asS3Identifier(cellarID, contentType), is, metadata);
            putRequest.getRequestClientOptions().setReadLimit((int) resource.contentLength() + 1);
            Upload upload = transferManager.upload(putRequest);
            upload.addProgressListener(new VerboseUploadListener(cellarID, contentType, s3WriteDistribution));
            UploadResult result = upload.waitForUploadResult();
            watch.stop();
            s3WriteTimer.record(watch.elapsed(MILLISECONDS), MILLISECONDS);
            return result.getVersionId();
        } catch (Exception e) {
            throw new S3DataException("S3 update failed for [" + cellarID + "]", e);
        }
    }

    @Beta
    private EnumMap<ContentType, String> putContents(String cellarID, Map<ContentType, String> contents,
                                                     ObjectMetadata metadata) {

        s3WriteCounter.increment(contents.size());
        metadata.addUserMetadata("cellarId", cellarID);
        Stopwatch watch = Stopwatch.createStarted();

        final EnumMap<ContentType, String> versions = new EnumMap<>(ContentType.class);
        final CountDownLatch parts = new CountDownLatch(contents.size());
        try {
            final List<ContentUpload> uploads = new ArrayList<>(contents.size());
            for (Map.Entry<ContentType, String> content : contents.entrySet()) {
                LOG.debug(S3, "Start upload {} ({}) (non-blocking)", cellarID, content.getKey());
                try (InputStream is = new ByteArrayInputStream(content.getValue().getBytes(StandardCharsets.UTF_8))) {
                    PutObjectRequest putObject = new PutObjectRequest(s3Conf.getBucket(), asS3Identifier(cellarID, content.getKey()),
                            is, metadata.clone());
                    uploads.add(new ContentUpload(transferManager.upload(putObject, new CountDownLatchListener(parts, cellarID,
                            content.getKey(), contents.size())), content.getKey()));
                }
            }
            // Wait for all uploads
            boolean done = parts.await(TIMEOUT_UPLOAD_MINUTES, MINUTES);
            if (!done) {
                LOG.warn("Upload timeout reached ({} minutes) => blocking execution", TIMEOUT_UPLOAD_MINUTES);
            }

            for (ContentUpload upload : uploads) {
                if (!upload.upload.isDone()) { // ensure that the transfer is done.
                    upload.upload.waitForCompletion();
                }
                UploadResult result = upload.upload.waitForUploadResult();
                versions.put(upload.type, result.getVersionId());
            }

            watch.stop();
            s3WriteTimer.record(watch.elapsed(MILLISECONDS), MILLISECONDS);

            return versions;

        } catch (InterruptedException interrupted) {
            Thread.currentThread().interrupt();
            throw new S3DataException("S3 update failed (broken barrier) for [" + cellarID + "]", interrupted);
        } catch (Exception e) {
            throw new S3DataException("S3 update failed for [" + cellarID + "]", e);
        }
    }

    static class CountDownLatchListener implements S3ProgressListener {

        private final CountDownLatch latch;
        private final String cellarId;
        private final ContentType contentType;
        private final int parts;

        CountDownLatchListener(CountDownLatch latch, String cellarId, ContentType contentType, int parts) {
            this.latch = latch;
            this.cellarId = cellarId;
            this.contentType = contentType;
            this.parts = parts;
        }

        @Override
        public void onPersistableTransfer(PersistableTransfer persistableTransfer) {
        }

        @Override
        public void progressChanged(ProgressEvent progressEvent) {
            if (progressEvent.getEventType() == TRANSFER_COMPLETED_EVENT) {
                LOG.debug(S3, "Transfer done for {} ({}). Count down {}/{}", cellarId, contentType,
                        parts - latch.getCount(), parts);
                latch.countDown();
            }
        }
    }

    static class ContentUpload {
        private Upload upload;
        private ContentType type;

        ContentUpload(Upload upload, ContentType type) {
            this.upload = upload;
            this.type = type;
        }
    }

    /**
     * {@inheritDoc}
     * In S3, the deletion of an object in a bucket with versioning enabled can be done
     * by specifying the version of the object to delete. It is not possible to restore
     * a deleted file. However, if the version is not specified, the file will be not
     * effectively removed from S3 but a marker will be added to the object. The S3
     * documentation suggest that a new identifier will be generated for the marker but
     * the API does not return that identifier so to find it we need to retrieve all the
     * versions of the objects and identify manually the deleted marker.
     * If an object is mark as "deleted", the object will be inaccessible and the only
     * operation permitted on that marker will be HTTP DELETE.
     * <p>
     * Delete Operation and Cross-Region Replication
     * If you delete an object from the source bucket, the cross-region replication behavior
     * is as follows:
     * <ul>
     * <li>
     * If a DELETE request is made without specifying an object version ID, Amazon S3 adds
     * a delete marker, which cross-region replication replicates to the destination bucket.
     * </li>
     * <li>
     * If a DELETE request specifies a particular object version ID to delete, Amazon S3
     * deletes that object version in the source bucket, but it does not replicate the
     * deletion in the destination bucket (in other words, it does not delete the same
     * object version from the destination bucket).
     * </li>
     * </ul>
     *
     * @param cellarID the cellar identifier used to find the object
     * @param type     the type of the content (i.e. the file in Amazon S3)
     */
    @Override
    @Retryable(
            exceptionExpression = "@cellarStaticConfiguration.isS3RetryEnabled()", maxAttemptsExpression = "@cellarStaticConfiguration.getS3RetryMaxAttempts()",
            value = S3DataException.class,
            backoff = @Backoff(delayExpression = "@cellarStaticConfiguration.getS3RetryDelay()", randomExpression = "@cellarStaticConfiguration.isS3RetryRandom()",
                    multiplierExpression = "@cellarStaticConfiguration.getS3RetryMultiplier()", maxDelayExpression = "@cellarStaticConfiguration.getS3RetryMaxDelay()")
    )
    public void delete(String cellarID, ContentType type) {
        logS3RetryAttempt();
        //testing purposes
        testS3Retry("delete");

        s3WriteCounter.increment();
        s3Client.deleteObject(s3Conf.getBucket(), asS3Identifier(cellarID, type));
    }

    @Override
    public void deleteContent(String cellarID, String version, ContentType contentType) {
        s3WriteCounter.increment();
        try {
            if (version != null) {
                s3Client.deleteVersion(s3Conf.getBucket(), asS3Identifier(cellarID, contentType), version);
            } else {
                s3Client.deleteObject(s3Conf.getBucket(), asS3Identifier(cellarID, contentType));
            }
        } catch (AmazonClientException e) {
            throw new S3DataException("Cannot delete version [" + version + "] of object [" + asS3Identifier(cellarID) + "]", e);
        }
    }

    @Override
    @Retryable(
            exceptionExpression = "@cellarStaticConfiguration.isS3RetryEnabled()", maxAttemptsExpression = "@cellarStaticConfiguration.getS3RetryMaxAttempts()",
            value = S3DataException.class,
            backoff = @Backoff(delayExpression = "@cellarStaticConfiguration.getS3RetryDelay()", randomExpression = "@cellarStaticConfiguration.isS3RetryRandom()",
                    multiplierExpression = "@cellarStaticConfiguration.getS3RetryMultiplier()", maxDelayExpression = "@cellarStaticConfiguration.getS3RetryMaxDelay()")
    )
    public Map<String, String> deleteRecursively(String baseCellarID) {
        logS3RetryAttempt();
        //testing purposes
        testS3Retry("deleteRecursively");

        s3ReadCounter.increment();
        final ObjectListing objects = s3Client.listObjects(s3Conf.getBucket(), asS3Identifier(baseCellarID));
        final List<KeyVersion> keys = objects.getObjectSummaries().stream()
                .map(o -> new KeyVersion(o.getKey()))
                .collect(Collectors.toList());
        LOG.debug(S3, "Delete all objects starting with [{}] ({})", baseCellarID, keys.size());
        return deleteKeyVersions(keys);
    }

    @Override
    public Map<String, String> deleteVersions(Map<String, String> keyVersions) {
        List<KeyVersion> keys = keyVersions.entrySet().stream()
                .map(e -> new KeyVersion(e.getKey(), e.getValue()))
                .collect(Collectors.toList());
        return deleteKeyVersions(keys);
    }

    private Map<String, String> deleteKeyVersions(List<KeyVersion> keyVersions) {
        s3WriteCounter.increment();
        if (!keyVersions.isEmpty()) {
            final DeleteObjectsRequest req = new DeleteObjectsRequest(s3Conf.getBucket());
            req.setKeys(keyVersions);
            final DeleteObjectsResult result = s3Client.deleteObjects(req);
            if (LOG.isDebugEnabled(S3)) {
                result.getDeletedObjects().forEach(o -> LOG.debug(S3, "Delete: {} ({})", o.getKey(), o.getVersionId()));
            }
            return result.getDeletedObjects().stream()
                    .collect(Collectors.toMap(
                            DeletedObject::getKey,
                            e -> e.isDeleteMarker() ? e.getDeleteMarkerVersionId() : e.getVersionId(),
                            (o1, o2) -> {
                                LOG.debug(S3, "Object with multiple versions, drop duplicate ()/() => ", o1, o2, o1);
                                return o1;
                            }));
        }
        return Collections.emptyMap();

    }

    @Override
    public boolean exists(String cellarID, ContentType type) {
        s3ReadCounter.increment();
        return s3Client.doesObjectExist(s3Conf.getBucket(), asS3Identifier(cellarID, type));
    }

    /**
     * The update of the metadata will necessarily create a new version of
     * the object.
     * "Each Amazon S3 object has data, a key, and metadata. Object key (or key name)
     * uniquely identifies the object in a bucket. Object metadata is a set of name-value
     * pairs. You can set object metadata at the time you upload it. After you upload the
     * object, you cannot modify object metadata. The only way to modify object metadata
     * is to make a copy of the object and set the metadata."
     *
     * @param id           the S3 key of the object to modify
     * @param version      the version of that object
     * @param userMetadata the new metadata to insert
     * @see <a href="https://docs.aws.amazon.com/AmazonS3/latest/dev/UsingMetadata.html">
     * Object Key and Metadata</a>
     */
    @Override
    @Retryable(
            exceptionExpression = "@cellarStaticConfiguration.isS3RetryEnabled()", maxAttemptsExpression = "@cellarStaticConfiguration.getS3RetryMaxAttempts()",
            value = S3DataException.class,
            backoff = @Backoff(delayExpression = "@cellarStaticConfiguration.getS3RetryDelay()", randomExpression = "@cellarStaticConfiguration.isS3RetryRandom()",
                    multiplierExpression = "@cellarStaticConfiguration.getS3RetryMultiplier()", maxDelayExpression = "@cellarStaticConfiguration.getS3RetryMaxDelay()")
    )
    public String updateMetadata(String id, String version, ContentType contentType, Map<String, String> userMetadata) {
        logS3RetryAttempt();
        //testing purposes
        testS3Retry("updateMetadata");

        s3WriteCounter.increment();
        final ObjectMetadata existing = getRawMetadata(id, version, contentType);
        existing.getUserMetadata().putAll(userMetadata);

        final String s3Key = asS3Identifier(id, contentType);
        final CopyObjectRequest request = new CopyObjectRequest(s3Conf.getBucket(), s3Key, s3Conf.getBucket(), s3Key)
                .withSourceVersionId(version)
                .withNewObjectMetadata(existing);
        final CopyObjectResult result = s3Client.copyObject(request);
        return result.getVersionId();
    }

    @Override
    public List<String> listStartWith(String prefix) {
        s3ReadCounter.increment();
        Stopwatch watch = Stopwatch.createStarted();
        LOG.debug(S3, "List startsWith {} (limit 500)", prefix);
        final ListObjectsRequest request = new ListObjectsRequest();
        request.setBucketName(s3Conf.getBucket());
        request.setPrefix(prefix);
        request.setMaxKeys(500);
        ObjectListing listing = s3Client.listObjects(request);
        final List<String> results = listing.getObjectSummaries().stream()
                .map(S3ObjectSummary::getKey)
                .collect(Collectors.toList());
        while (listing.isTruncated()) {
            LOG.debug(S3, "Listing truncate (startsWith {}) current size={}, fetch next 1000 objects...", prefix, results.size());
            listing = s3Client.listNextBatchOfObjects(listing);
            results.addAll(listing.getObjectSummaries().stream()
                    .map(S3ObjectSummary::getKey)
                    .collect(Collectors.toList()));

        }

        LOG.debug(S3, "Return {} results for {}", results.size(), prefix);
        s3ReadDistribution.record(watch.elapsed(MILLISECONDS));
        return results;
    }

    @Override
    public void deleteBucket() {
        deleteBucket(ALL);
    }

    @Override
    public void deleteBucket(Predicate<String> filter) {
        deleteBucket(filter, s3Conf.getBucket());
    }

    @Override
    public void deleteBucket(Predicate<String> filter, String bucketName) {
        if (s3Conf.isUnsafe()) { // Very dangerous operation, safeguard needed
            deleteAllVersions(filter, bucketName);
            if (filter == ALL || filter == null) {
                // After all objects and object versions are deleted, delete the bucket.
                s3Client.deleteBucket(bucketName);
            } else {
                LOG.warn("Bucket has not been deleted, filtering is not null or equals to ALL.");
            }
        } else {
            LOG.warn("Delete bucket ignored, safeguard is active.");
        }
    }

    @Override
    public String getDefaultBucketName() {
        return s3Conf.getBucket();
    }

    @Override
    public String tag(String key, String version, ContentType contentType, Map<String, String> tags) {
        final ObjectTagging tagging = new ObjectTagging(tags.entrySet().stream()
                .map(s -> new Tag(s.getKey(), s.getValue()))
                .collect(Collectors.toList()));
        final String s3Key = asS3Identifier(key, contentType);
        final SetObjectTaggingRequest request = Optional.ofNullable(version)
                .map(v -> new SetObjectTaggingRequest(s3Conf.getBucket(), s3Key, v, tagging))
                .orElseGet(() -> new SetObjectTaggingRequest(s3Conf.getBucket(), s3Key, tagging));
        final SetObjectTaggingResult result = s3Client.setObjectTagging(request);
        return result.getVersionId();
    }

    private void deleteAllVersions(Predicate<String> filter, String bucketName) {
        VersionListing listing = s3Client.listVersions(new ListVersionsRequest().withBucketName(bucketName));
        final Predicate<S3VersionSummary> s3Filter = s3VersionSummary -> filter.test(s3VersionSummary.getKey());
        List<KeyVersion> keys;
        do {
            keys = listing.getVersionSummaries().stream()
                    .filter(s3Filter)
                    .peek(v -> LOG.debug(S3, "Prepare delete {} ({})", v.getKey(), v.getVersionId()))
                    .map(v -> new KeyVersion(v.getKey(), v.getVersionId()))
                    .collect(Collectors.toList());

            final Map<String, String> deleted = deleteKeyVersions(keys);
            LOG.info("{} objects deleted", deleted.size());
            listing = s3Client.listNextBatchOfVersions(listing);

        } while (!keys.isEmpty());
    }

    private static String asS3Identifier(String cellarID, ContentType contentType) {
        return asS3Identifier(cellarID) + "/" + contentType.file();
    }

    private static String asS3Identifier(String id) {
        final String sid = sanitized(id);
        if (sid.contains("/") && !(sid.startsWith("nal") || sid.startsWith("onto"))) { // item
            int idx = sid.lastIndexOf("/");
            String item = sid.substring(idx + 1);
            return sid.substring(0, idx).replaceAll("\\.", "/") + "/" + item;
        }
        return sid.replaceAll("\\.", "/");
    }

    private static String sanitized(String id) {
        // For backward compatibility reasons, the key was previously referenced in s3
        // by using ":" which is not allowed in S3.
        if (id.startsWith("cellar:")) {
            return id.substring("cellar:".length());
        }
        // NAL, Ontology
        return id.replaceFirst(":", "/");
    }

    private static ContentVersion asContentVersion(S3VersionSummary versionSummary) {
        ContentVersion version = new ContentVersion();
        version.setLatest(versionSummary.isLatest());
        version.setLastModified(versionSummary.getLastModified());
        version.setVersion(versionSummary.getVersionId());
        version.setDeleted(versionSummary.isDeleteMarker());
        version.setETag(versionSummary.getETag());
        return version;
    }

    private static ObjectMetadata asObjectMetadata(Map<String, String> metadata) {
        ObjectMetadata om = new ObjectMetadata();
        if (metadata.containsKey(Headers.CONTENT_TYPE)) {
            om.setContentType(metadata.get(Headers.CONTENT_TYPE));
            metadata.remove(Headers.CONTENT_TYPE);
        }
        for (Map.Entry<String, String> e : metadata.entrySet()) {
            om.addUserMetadata(e.getKey(), e.getValue());
        }
        return om;
    }

    private static Map<String, Object> asMap(ObjectMetadata metadata, ContentType type) {
        Map<String, Object> map = metadata.getRawMetadata().entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        map.putAll(metadata.getUserMetadata());
        if (type == ContentType.FILE && map.get(FILENAME) == null) {
            // The migration tool store the filename in the field ContentDisposition with
            // the form "attachment; filename=\"" + filename + "\"".
            if (!Strings.isNullOrEmpty(metadata.getContentDisposition())) {
                map.put(FILENAME, filename(metadata.getContentDisposition()));
            }
        }
        return map;
    }

    private static String filename(String contentDisposition) {
        return contentDisposition.replace("attachment; filename=\"", "")
                .replace("\"", "");
    }

    private S3Object reportSize(S3Object s3Object) {
        if (s3Object.getObjectMetadata() != null && s3Object.getObjectMetadata().getContentLength() > 0) {
            s3ReadDistribution.record(s3Object.getObjectMetadata().getContentLength());
        }
        return s3Object;
    }

    /**
     * Log the current attempt to retry after the first failure
     */
    private void logS3RetryAttempt() {
        if(RetrySynchronizationManager.getContext().getRetryCount() != 0) {
            LOG.warn("S3DataException on attempt No. {}. Next attempt No. {}, Retrying ...",
                    RetrySynchronizationManager.getContext().getRetryCount() - 1, RetrySynchronizationManager.getContext().getRetryCount());
        }
    }

    /**
     * Test S3 Retry mechanism. For test purposes only
     * @param methodName the caller method name
     *
     * @throws S3DataException on purpose until last retry.
     */
    private void testS3Retry(String methodName) {
        if (this.cellarConfiguration.isTestS3RetryEnabled() && this.cellarConfiguration.isS3RetryEnabled() &&
                RetrySynchronizationManager.getContext().getRetryCount() != cellarConfiguration.getS3RetryMaxAttempts() - 1) {
            LOG.warn("FORCED EXCEPTION FOR TEST PURPOSES, method: {}, on attempt: {}", methodName, RetrySynchronizationManager.getContext().getRetryCount());
            throw new S3DataException("FORCED EXCEPTION FOR TEST PURPOSES");
        }
    }


    static class VerboseDownloadListener implements ProgressListener {

        private final Stopwatch timer = Stopwatch.createUnstarted();
        private final String cellarID;
        private final ContentType contentType;

        private long total = 0L;
        private long progress = 0L;

        VerboseDownloadListener(String cellarID, ContentType contentType) {
            this.cellarID = cellarID;
            this.contentType = contentType;
        }

        @Override
        public void progressChanged(ProgressEvent progressEvent) {
            final ProgressEventType type = progressEvent.getEventType();
            switch (type) {
                case TRANSFER_STARTED_EVENT:
                    LOG.debug(S3, "Download started {} ({})", cellarID, contentType);
                    timer.start();
                    break;
                case RESPONSE_CONTENT_LENGTH_EVENT:
                    total = progressEvent.getBytes();
                    break;
                case RESPONSE_BYTE_TRANSFER_EVENT:
                    progress += progressEvent.getBytes();
                    LOG.debug(S3, "Downloading {}: {}", cellarID, displayProgress(progress, total));
                    break;
                case TRANSFER_COMPLETED_EVENT:
                    timer.stop();
                    LOG.debug(S3, "Downloaded {} ({}) in {}", cellarID, contentType, timer.stop());
            }
        }
    }

    static class VerboseUploadListener implements ProgressListener {

        private final Stopwatch timer = Stopwatch.createUnstarted();
        private final String cellarID;
        private final ContentType contentType;
        private final DistributionSummary distributionSummary;

        private long progress = 0L;

        VerboseUploadListener(String cellarID, ContentType contentType,
                              DistributionSummary distributionSummary) {
            this.cellarID = cellarID;
            this.contentType = contentType;
            this.distributionSummary = distributionSummary;
        }

        @Override
        public void progressChanged(ProgressEvent progressEvent) {
            final ProgressEventType type = progressEvent.getEventType();
            switch (type) {
                case TRANSFER_STARTED_EVENT:
                    timer.start();
                    LOG.debug(S3, "Upload started {} ({})", cellarID, contentType);
                    break;
                case REQUEST_BYTE_TRANSFER_EVENT:
                    progress += progressEvent.getBytes();
                    distributionSummary.record(progressEvent.getBytes());
                    LOG.debug(S3, "Uploading {}: {}", cellarID, FileUtils.byteCountToDisplaySize(progress));
                    break;
                case TRANSFER_COMPLETED_EVENT:
                    LOG.debug(S3, "Uploaded {} ({}) in {}", cellarID, contentType, timer.stop());
            }
        }
    }

    private static String displayProgress(final long progress, final long total) {
        final String p = FileUtils.byteCountToDisplaySize(progress);
        final String t = FileUtils.byteCountToDisplaySize(total);
        final String percent = PERCENT_FORMAT.format((float) progress / total);
        return p + "/" + t + " (" + percent + ")";
    }
}
