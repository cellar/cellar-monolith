package eu.europa.ec.opoce.cellar.s3;

import com.google.common.annotations.Beta;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.s3.domain.ContentVersion;
import org.springframework.core.io.Resource;

import java.io.InputStream;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

/**
 * @author ARHS Developments
 * @since 8.1.0
 */
public interface ContentStreamService {

    Optional<InputStream> getContent(String cellarID, ContentType contentType);

    /**
     * @param cellarID
     * @param version
     * @param contentType
     * @return
     */
    Optional<InputStream> getContent(String cellarID, String version, ContentType contentType);

    /**
     * = transaction begin ?
     * 1. retrieve version in relational database
     * 2. check if content is under embargo
     * 2.1. if embargoed is accessible => 3.
     * 2.2. else throw exception
     * 3. get content stream based on the version
     * = transaction end
     *
     * @param cellarID
     * @return
     */
    Optional<String> getContentAsString(String cellarID, ContentType contentType);

    /**
     * @param cellarID
     * @param version
     * @param contentType
     * @return
     */
    Optional<String> getContentAsString(String cellarID, String version, ContentType contentType);

    /**
     * @param cellarID
     * @param version
     * @param contentType
     * @return
     */
    Optional<String> getLargeContentAsString(String cellarID, String version, ContentType contentType);

    /**
     * @param cellarID
     * @param contentTypes
     * @return
     */
    EnumMap<ContentType, String> getContentsAsString(String cellarID, Map<ContentType, String> contentTypes);

    /**
     * Write the content of the {@link InputStream} into the data repository (or
     * multiple data repositories).
     *
     * @param id      the cellar ID used to find the resource
     * @param content the type of resource (DIRECT, DIRECT+INFERRED, TECHNICAL...)
     */
    String writeContent(String id, Resource content, ContentType contentType);

    /**
     * @param id
     * @param content
     * @param contentType
     * @param resourceSupplier
     * @return
     */
    String writeContent(String id, Resource content, ContentType contentType, Supplier<CellarResource> resourceSupplier);

    /**
     * @param cellarID
     * @param contentType
     * @param content
     * @param metadata
     * @return
     */
    String writeContent(String cellarID, Resource content, ContentType contentType, Map<String, String> metadata);

    /**
     * @param cellarID
     * @param content
     * @param contentType
     * @param metadata
     * @param resourceSupplier
     * @return
     */
    String writeContent(String cellarID, Resource content, ContentType contentType, Map<String, String> metadata,
                        Supplier<CellarResource> resourceSupplier);

    /**
     *
     * @param cellarID
     * @param contents
     * @param resourceSupplier
     * @return
     */
    @Beta
    EnumMap<ContentType, String> writeContents(String cellarID, Map<ContentType, String> contents,
                                               Supplier<CellarResource> resourceSupplier);

    /**
     * @param cellarID
     */
    void deleteContent(String cellarID);

    /**
     * @param resource
     */
    void deleteContent(CellarResource resource);

    /**
     * @param cellarID
     * @param contentType
     */
    void deleteContent(String cellarID, ContentType contentType);

    /**
     * @param baseCellarID
     */
    void deleteRecursively(String baseCellarID);

    /**
     * @param cellarID
     * @param contentType
     * @return
     */
    List<ContentVersion> getVersions(String cellarID, ContentType contentType);

    /**
     * @param cellarID
     * @param contentType
     * @param maxResults
     * @return
     */
    List<ContentVersion> getVersions(String cellarID, ContentType contentType, int maxResults);

    /**
     * @param cellarID
     * @param contentType
     * @return
     */
    boolean exists(String cellarID, ContentType contentType);

    /**
     * @param resourceID
     * @param contentType
     * @return
     */
    boolean exists(String resourceID, ContentType contentType, String version);

    /**
     * @param id
     * @param version
     * @param contentType
     * @return
     */
    Map<String, Object> getMetadata(String id, String version, ContentType contentType);

    /**
     * @param id
     * @param version
     * @param userMetadata
     */
    String updateMetadata(String id, String version, ContentType contentType, Map<String, String> userMetadata);

    /**
     * @return
     */
    Date getLastModificationDate(String id, String version, ContentType contentType);

    /**
     * @param prefix
     * @return
     */
    List<String> listKeyStartWith(String prefix);


    class Metadata {

        public static final String MANIFESTATION_LAST_INDEX = "manifestation-last-index";
        public static final String FILENAME = "filename";

    }
}
