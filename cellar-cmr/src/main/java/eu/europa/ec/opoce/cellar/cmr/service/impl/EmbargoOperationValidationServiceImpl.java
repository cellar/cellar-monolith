/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.service
 *             FILE : EmbargoOperationValidationServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 27 Jan 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.service.impl;

import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.EmbargoDatabaseService;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.service.EmbargoOperation;
import eu.europa.ec.opoce.cellar.cmr.service.EmbargoOperationValidationService;
import eu.europa.ec.opoce.cellar.common.util.CellarIdUtils;
import eu.europa.ec.opoce.cellar.common.util.TimeUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;

/**
 * <class_description> This service holds the validation methods used during the (dis)embargo operation.
 * <br/><br/>
 * <notes> .
 * <br/><br/>
 * ON : 27 Jan 2017
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class EmbargoOperationValidationServiceImpl implements EmbargoOperationValidationService {

    private static final Logger LOG = LogManager.getLogger(EmbargoOperationValidationServiceImpl.class);

    @Autowired
    private EmbargoDatabaseService embargoDatabaseService;

    @Autowired
    private CellarResourceDao cellarResourceDao;

    @Override
    public void preValidation(final EmbargoOperation embargoOperation) {
        LOG.info("Validating cellarId");

        if (embargoOperation.getObjectId() == null) {
            throw ExceptionBuilder.get(CellarException.class).build();
        }

        if (embargoOperation.isAutomaticDisembargo()) {
            final Date embargoDate = embargoOperation.getEmbargoDate();

            if (TimeUtils.isFutureDate(embargoDate)) {
                ExceptionBuilder.get(CellarException.class)
                        .withCode(CmrErrors.EMBARGO_SERVICE_ERROR)
                        .withMessage("Disembargo must not take a future date - {}")
                        .withMessageArgs(embargoDate)
                        .build();
            }
        }
    }

    @Override
    public void postLoadingValidation(final EmbargoOperation embargoOperation) {
        LOG.info("Validating the cellar resource");
        final CellarResource cellarResource = embargoOperation.getCellarResource();

        if (cellarResource == null) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withCode(CmrErrors.EMBARGO_ILLEGAL_IDENTIFIER)
                    .withMessage("The given identifier doesn't exist")
                    .build();
        }

        if (!DigitalObjectType.takeEmbargoDate(cellarResource.getCellarType())) {
            final String cellarId = embargoOperation.getObjectId();
            ExceptionBuilder.get(CellarException.class)
                    .withCode(CmrErrors.EMBARGO_ILLEGAL_IDENTIFIER)
                    .withMessage("Only elements of type [{}] can have an embargo date: '{}' is not a valid node.")
                    .withMessageArgs(Arrays.toString(DigitalObjectType.getEmbargoTypes().toArray()), cellarId)
                    .build();
        }

        final Date embargoDate = embargoOperation.getEmbargoDate();
        if (!TimeUtils.isFutureDate(embargoDate)) {
            return;
        }

        String cellarId = cellarResource.getCellarId();
        while (cellarId != null && cellarId.contains(CellarIdUtils.WEM_SEPARATOR)) {
            cellarId = CellarIdUtils.getParentCellarId(cellarId);

            final CellarResource parentCellarResource = cellarResourceDao.findCellarId(cellarId);
            final Date parentEmbargoDate = parentCellarResource.getEmbargoDate();

            if ((parentEmbargoDate != null) && TimeUtils.isFutureDate(parentEmbargoDate) && parentEmbargoDate.after(embargoDate)) {
                throw ExceptionBuilder.get(CellarException.class)
                        .withMessage("CELLAR can't change the embargo date of {} to {} while a parent node has an earlier embargo date.")
                        .withMessageArgs(cellarId, embargoDate)
                        .build();
            }
        }
    }

    @Override
    public void postDirectionSetValidation(final EmbargoOperation embargoOperation) {
        LOG.info("Validating embargo direction");

        final boolean storedInPrivateDatabase = embargoOperation.isStoredInPrivateDatabase();
        final boolean toBeStoredInPrivateDatabase = embargoOperation.isToBeStoredInPrivateDatabase();
        final String cellarId = embargoOperation.getObjectId();

        if (storedInPrivateDatabase != toBeStoredInPrivateDatabase) {
            String parentCellarId = CellarIdUtils.getParentCellarId(cellarId);

            while (parentCellarId != null) {
                final boolean metadataSavedPrivate = this.embargoDatabaseService.isMetadataSavedPrivate(parentCellarId);

                if (metadataSavedPrivate) {
                    throw ExceptionBuilder.get(CellarException.class)
                            .withCode(CmrErrors.EMBARGO_ILLEGAL_IDENTIFIER)
                            .withMessage("The operation cannot take place while the parent node [{}] is under embargo.")
                            .withMessageArgs(parentCellarId)
                            .build();
                }

                parentCellarId = CellarIdUtils.getParentCellarId(parentCellarId);
            }
        }
    }
}
