package eu.europa.ec.opoce.cellar.nal.service;

import eu.europa.ec.opoce.cellar.cmr.metsbeans.Expression;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Work;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.nal.domain.RequiredLanguageBean;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;

import java.util.Collection;
import java.util.List;

/**
 * @author ARHS Developments
 */
public interface LanguageService {

    LanguageBean getByUri(String uri);

    LanguageBean getByTwoOrThreeChar(String charCode);

    LanguageBean getByThreeChar(String threeChar);

    LanguageBean getByTwoChar(String twoChar);

    List<String> getFallbackLanguagesFor(String uri);

    RequiredLanguageBean getRequiredLanguage(String uri);

    List<RequiredLanguageBean> getRequiredLanguages();

    List<String> getRequiredLanguageCodes();

    void filterNonEuLanguagesExpression(Collection<Expression> inCollection);

    void filterNonEuLanguages(Collection<DigitalObject> inCollection);

    /**
     * Extract valid expressions.
     * <ul>
     *     <li>Prioritize mono-lingual expressions over multi-lingual ones<ul>
     *     <li>Discarding multi-lingual expressions that reference a language used by any mono-lingual expression</li>
     *     <li>Order all multi-lingual expression by CELLAR ID and keep the “first original”</li>
     *     <li>Discarding any multi-lingual expression that references a language used by any predecessor</li>
     * </ul>
     * @param work the work
     * @return the list of expressions valid for indexation
     */
    List<Expression> filterMultiLinguisticExpressions(final Work work);

    /**
     * Extract valid expressions.
     * <ul>
     *     <li>Prioritize mono-lingual expressions over multi-lingual ones<ul>
     *     <li>Discarding multi-lingual expressions that reference a language used by any mono-lingual expression</li>
     *     <li>Order all multi-lingual expression by CELLAR ID and keep the “first original” </li>
     *     <li>Discarding any multi-lingual expression that references a language used by any predecessor</li>
     * </ul>
     * @param dos the digital objects
     * @return the digital objects valid for indexation
     */
    Collection<DigitalObject> filterMultiLinguisticExpressions(final Collection<DigitalObject> dos);

}
