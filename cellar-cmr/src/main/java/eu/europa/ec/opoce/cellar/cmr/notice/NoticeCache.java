/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice
 *             FILE : NoticeCache.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 29, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.notice;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;
import eu.europa.ec.opoce.cellar.cmr.CmrInverseDissemination;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.StructuredQueryConfiguration;
import eu.europa.ec.opoce.cellar.cmr.daobeans.InverseRelation;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CmrInverseDisseminationDao;
import eu.europa.ec.opoce.cellar.cmr.database.dao.InverseRelationDao;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.HierarchyElement;
import eu.europa.ec.opoce.cellar.cmr.model.SameAsModelUtils;
import eu.europa.ec.opoce.cellar.cmr.notice.cache.ISameAsCache;
import eu.europa.ec.opoce.cellar.cmr.notice.cache.impl.ServiceBasedSameAsCache;
import eu.europa.ec.opoce.cellar.common.cacheable.IMultipleCacheableObject;
import eu.europa.ec.opoce.cellar.common.cacheable.impl.CacheableObject;
import eu.europa.ec.opoce.cellar.common.cacheable.impl.MultipleMappedCacheableObject;
import eu.europa.ec.opoce.cellar.common.util.DigitalObjectUtils;
import eu.europa.ec.opoce.cellar.common.util.Pair;
import eu.europa.ec.opoce.cellar.database.IRDFStoreRelationalGateway;
import eu.europa.ec.opoce.cellar.domain.content.mets.CellarIdentifiedObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.nal.domain.DecodingType;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.nal.service.DecodingSnippetService;
import eu.europa.ec.opoce.cellar.ontology.service.OntologyService;
import eu.europa.ec.opoce.cellar.s3.ContentStreamExceptions;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.semantic.jena.template.JenaQueryTemplate;
import eu.europa.ec.opoce.cellar.semantic.jena.template.ListOfMapLoopHandlerWithoutResult;
import eu.europa.ec.opoce.cellar.semantic.jena.transformers.RdfNode2Uri;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.riot.Lang;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static eu.europa.ec.opoce.cellar.cmr.ContentType.DECODING;
import static eu.europa.ec.opoce.cellar.cmr.ContentType.DIRECT;
import static eu.europa.ec.opoce.cellar.cmr.ContentType.DIRECT_INFERRED;
import static eu.europa.ec.opoce.cellar.cmr.ContentType.TECHNICAL;
import static eu.europa.ec.opoce.cellar.database.CmrTableName.CMR_INVERSE;
import static org.apache.commons.collections15.CollectionUtils.subtract;

/**
 * <class_description> Cache for notices.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 29, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Configurable
public class NoticeCache {

    /**
     * The Constant LOG.
     */
    private static final Logger LOG = LogManager.getLogger(NoticeCache.class);

    /**
     * The decoding snippet service.
     */
    @Autowired
    private DecodingSnippetService decodingSnippetService;

    @Autowired
    private ContentStreamService contentStreamService;

    @Autowired
    private StructuredQueryConfiguration structuredQueryConfiguration;

    @Autowired
    private OntologyService ontologyService;

    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    @Autowired
    @Qualifier("cmrMetadataRelationalGateway")
    private IRDFStoreRelationalGateway cmrMetadataRelationalGateway;

    @Autowired
    private InverseRelationDao inverseRelationDao;

    @Autowired
    private CmrInverseDisseminationDao cmrInverseDisseminationDao;

    private ISameAsCache sameAsCache;

    private Collection<Model> languageDependentMetadata;

    private CacheableObject<NoticeCacheKey, Map<ContentType, String>> dataStreams;

    private CacheableObject<NoticeCacheKey, Model> decodedModel;

    private CacheableObject<NoticeCacheKey, Model> languageIndependentModel;

    private CacheableObject<CellarIdentifiedObject, Pair<Model, Boolean>> inverseModel;

    private IMultipleCacheableObject<String, Model> inverseTripleMetadataNonOptimized;

    private IMultipleCacheableObject<String, CmrInverseDissemination> inverseTripleMetadata;

    public NoticeCache() {
        this(null);
    }

    /**
     * Instantiates a new notice cache.
     *
     * @param object the object
     */
    @SuppressWarnings("serial")
    public NoticeCache(final CellarIdentifiedObject object) {
        this.sameAsCache = new ServiceBasedSameAsCache();
        this.sameAsCache.init(object);

        this.languageDependentMetadata = new ArrayList<>();

        this.dataStreams = new CacheableObject<NoticeCacheKey, Map<ContentType, String>>() {

            @Override
            protected Map<ContentType, String> retrieve(NoticeCacheKey key) {
                return retrieveModel(key);
            }
        };
        this.decodedModel = new CacheableObject<NoticeCacheKey, Model>() {

            @Override
            protected Model retrieve(NoticeCacheKey key) {
                return retrieveDecodedMetadata(key);
            }
        };
        this.languageIndependentModel = new CacheableObject<NoticeCacheKey, Model>() {

            @Override
            protected Model retrieve(final NoticeCacheKey key) {
                return retrieveLanguageIndependentMetadata(key);
            }
        };
        this.inverseModel = new CacheableObject<CellarIdentifiedObject, Pair<Model, Boolean>>() {

            @Override
            protected Pair<Model, Boolean> retrieve(final CellarIdentifiedObject metsElement) {
                return cellarConfiguration.isCellarServiceDisseminationDatabaseOptimizationEnabled() ? retrieveInverseMetadata(metsElement)
                        : retrieveInverseMetadataNonOptimized(metsElement);
            }
        };
        this.inverseTripleMetadataNonOptimized = new MultipleMappedCacheableObject<String, Model>() {

            @Override
            protected Map<String, Model> retrieveMultiple(final Collection<String> inverseTargets) {
                boolean underEmbargo = isUnderEmbargo(object);
                return retrieveInverseTripleMetadataNonOptimized((List<String>) inverseTargets, underEmbargo);
            }
        };
        this.inverseTripleMetadata = new MultipleMappedCacheableObject<String, CmrInverseDissemination>() {

            @Override
            protected Map<String, CmrInverseDissemination> retrieveMultiple(final Collection<String> inverseTargets) {
                boolean underEmbargo = isUnderEmbargo(object);
                return retrieveInverseTripleMetadata((List<String>) inverseTargets, underEmbargo);
            }
        };
    }

    private static boolean isUnderEmbargo(Object object) {
        if (object instanceof DigitalObject) {
            return DigitalObjectUtils.isUnderEmbargo((DigitalObject) object);
        } else if (object instanceof HierarchyElement) {
            return ((HierarchyElement<?, ?>) object).isUnderEmbargo();
        }

        return false;
    }

    /**
     * Gets the language independent.
     *
     * @param cellarId the cellar id
     * @return the language independent
     */
    public Model getLanguageIndependent(final ContentIdentifier cellarId, Map<ContentType, String> versions) {
        return this.languageIndependentModel.get(new NoticeCacheKey(cellarId.getIdentifier(), versions));
    }

    /**
     * Gets the language dependent.
     *
     * @param cellarId the cellar id
     * @param language the language
     * @return the language dependent
     */
    public Model getLanguageDependent(final ContentIdentifier cellarId, Map<ContentType, String> versions, final LanguageBean language) {
        final Model languageDependent = loadLanguageDependent(new NoticeCacheKey(cellarId.getIdentifier(), versions), language);
        this.languageDependentMetadata.add(languageDependent);
        return languageDependent;
    }

    /**
     * Gets the inverse.
     *
     * @param metsElement the mets element
     * @return the inverse
     */
    public Pair<Model, Boolean> getInverse(final CellarIdentifiedObject metsElement) {
        return this.inverseModel.get(metsElement);
    }

    /**
     * Gets the decoded metadata not cached.
     *
     * @param cellarIdentifier the cellar identifier
     * @param conceptSnippet   the concept snippet
     * @return the decoded metadata not cached
     */
    public Model getDecodedMetadataNotCached(final String cellarIdentifier, final Model conceptSnippet) {
        Pair<Collection<String>, Model> decodingFor = null;
        try {
            final Map<DecodingType, Collection<String>> map = extractConceptsToDecode(conceptSnippet);
            decodingFor = decodingSnippetService.getDecodingFor(map);
            checkForUnDecodedConcepts(cellarIdentifier, map.values(), decodingFor.getOne());

            return decodingFor.getTwo();
        } catch (final RuntimeException e) {
            if (decodingFor != null) {
                JenaUtils.closeQuietly(decodingFor.getTwo());
            }
            throw e;
        }
    }

    /**
     * Close quietly.
     */
    public void closeQuietly() {
        JenaUtils.closeQuietly(this.languageDependentMetadata);
        JenaUtils.closeQuietly(this.decodedModel.values());
        JenaUtils.closeQuietly(this.languageIndependentModel.values());
        for (final Pair<Model, Boolean> pair : this.inverseModel.values()) {
            JenaUtils.closeQuietly(pair.getOne());
        }
        JenaUtils.closeQuietly(this.inverseTripleMetadataNonOptimized.values());
        for (final CmrInverseDissemination cmrInverseDissemination : this.inverseTripleMetadata.values()) {
            JenaUtils.closeQuietly(cmrInverseDissemination.getInverseModel());
            JenaUtils.closeQuietly(cmrInverseDissemination.getSameAsModel());
        }
    }


    private Map<ContentType, String> retrieveModel(final NoticeCacheKey key) {
        return contentStreamService.getContentsAsString(key.getCellarId(), key.getVersions());
    }

    private Model retrieveDecodedMetadata(final NoticeCacheKey key) {
        Model conceptSnippet = null;
        try {
            Map<ContentType, String> models = dataStreams.get(key);
            String model = models.get(ContentType.DECODING);
            if (model != null && !model.isEmpty()) {
                conceptSnippet = JenaUtils.read(model, Lang.NT);
            } else {
                conceptSnippet = ModelFactory.createDefaultModel();
            }
            return getDecodedMetadataNotCached(key.getCellarId(), conceptSnippet);
        } finally {
            JenaUtils.closeQuietly(conceptSnippet);
        }
    }

    /**
     * Retrieve language independent metadata.
     *
     * @param cellarIdentifier the cellar identifier
     * @return the model
     */
    private Model retrieveLanguageIndependentMetadata(final NoticeCacheKey key) {
        key.addContentTypes(DIRECT, DIRECT_INFERRED, TECHNICAL, DECODING);
        final Map<ContentType, String> models = dataStreams.get(key);
        Model businessMetaData = null;
        Model technicalMetaData = null;
        Model sameAsRel = null;
        try {
            String directModel = models.get(DIRECT_INFERRED);
            if (directModel == null) {
                throw ContentStreamExceptions.notFound(key.getCellarId(), key.getVersion(DIRECT_INFERRED), DIRECT_INFERRED).get();
            }
            businessMetaData = JenaUtils.read(directModel, Lang.NT);
            sameAsRel = this.sameAsCache.extractSameAses(businessMetaData);
            if (models.containsKey(TECHNICAL)) {
                String tm = models.get(ContentType.TECHNICAL);
                if (tm != null) {
                    technicalMetaData = JenaUtils.read(tm, Lang.NT);
                }
            }
            if (technicalMetaData == null) {
                return JenaUtils.create(businessMetaData, sameAsRel);
            } else {
                return JenaUtils.create(businessMetaData, technicalMetaData, sameAsRel);
            }
        } finally {
            JenaUtils.closeQuietly(businessMetaData, technicalMetaData, sameAsRel);
        }
    }

    /**
     * Load language dependent.
     *
     * @param contentId the content id
     * @param language  the language
     * @return the model
     */
    private Model loadLanguageDependent(final NoticeCacheKey key, final LanguageBean language) {
        key.addContentTypes(DECODING);
        final Model conceptDecodedMetaData = this.decodedModel.get(key);
        return conceptDecodedMetaData != null ? decodingSnippetService.extractLangDecoding(conceptDecodedMetaData, language)
                : ModelFactory.createDefaultModel();
    }

    /**
     * Extract concepts to decode.
     *
     * @param conceptSnippet the concept snippet
     * @return the map
     */
    @SuppressWarnings("serial")
    private Map<DecodingType, Collection<String>> extractConceptsToDecode(final Model conceptSnippet) {
        if (conceptSnippet.isEmpty()) {
            return Collections.emptyMap();
        }

        final Set<String> unannotatedConcepts = new HashSet<>();
        final Set<String> annotatedConcepts = new HashSet<>();

        JenaQueryTemplate.select(conceptSnippet, structuredQueryConfiguration.resolveListConceptUrisQuery(),
                new ListOfMapLoopHandlerWithoutResult() {

                    @Override
                    protected void process(final Map<String, RDFNode> map) {
                        final String property = RdfNode2Uri.instance.transform(map.get("property"));
                        final String concept = RdfNode2Uri.instance.transform(map.get("concept"));
                        if (ontologyService.getWrapper().getOntologyData().getFacetPropertyUris().contains(property)
                                || ontologyService.getWrapper().getOntologyData().getAllNumberedLevelsPropertyUris().contains(property)) {
                            annotatedConcepts.add(concept);
                        } else {
                            unannotatedConcepts.add(concept);
                        }
                    }
                });

        unannotatedConcepts.removeAll(annotatedConcepts);

        Map<DecodingType, Collection<String>> concepts = new HashMap<>();
        concepts.put(DecodingType.INCLUDE_RELATED, annotatedConcepts);
        concepts.put(DecodingType.EXCLUDE_RELATED, unannotatedConcepts);
        return concepts;
    }

    /**
     * Check for un decoded concepts.
     *
     * @param cellarIdentifier the cellar identifier
     * @param conceptsToDecode the concepts to decode
     * @param decodedConcepts  the decoded concepts
     */
    private void checkForUnDecodedConcepts(final String cellarIdentifier, final Collection<Collection<String>> conceptsToDecode,
                                           final Collection<String> decodedConcepts) {
        final Collection<String> allConcepts = new HashSet<>();
        for (final Collection<String> concepts : conceptsToDecode) {
            allConcepts.addAll(concepts);
        }
        final Collection<String> notFound = subtract(allConcepts, decodedConcepts);
        if (!notFound.isEmpty()) {
            LOG.warn("Concepts without decoding found for resource '{}': {}", cellarIdentifier, StringUtils.join(notFound, ","));
        }
    }

    /**
     * Retrieve inverse metadata non optimized.
     *
     * @param metsElement the mets element
     * @return the pair
     */
    private Pair<Model, Boolean> retrieveInverseMetadataNonOptimized(final CellarIdentifiedObject metsElement) {
        final Pair<Collection<String>, Boolean> inverseTargetsPair = retrieveInverseTargets(metsElement);
        final Collection<String> inverseTargets = inverseTargetsPair.getOne();
        final boolean isTruncated = inverseTargetsPair.getTwo();

        final Model inverseModel = ModelFactory.createDefaultModel();
        Model sameAsModel = null;
        try {
            final Collection<Model> inverseRelationModels = this.inverseTripleMetadataNonOptimized.getMultiple(inverseTargets);
            for (final Model inverseRelationModel : inverseRelationModels) {
                inverseModel.add(inverseRelationModel);
            }

            sameAsModel = this.sameAsCache.extractSameAses(inverseModel);
            return new Pair<>(JenaUtils.create(inverseModel, sameAsModel), isTruncated);
        } finally {
            JenaUtils.closeQuietly(inverseModel, sameAsModel);
        }
    }

    /**
     * Retrieve inverse metadata.
     *
     * @param metsElement the mets element
     * @return the pair
     */
    private Pair<Model, Boolean> retrieveInverseMetadata(final CellarIdentifiedObject metsElement) {
        final Pair<Collection<String>, Boolean> inverseTargetsPair = retrieveInverseTargets(metsElement);
        final Collection<String> inverseTargets = inverseTargetsPair.getOne();
        final boolean isTruncated = inverseTargetsPair.getTwo();

        final Model inverseModel = ModelFactory.createDefaultModel();
        final Model sameAsModel = ModelFactory.createDefaultModel();
        try {
            final Collection<CmrInverseDissemination> cmrInverseDisseminations = this.inverseTripleMetadata.getMultiple(inverseTargets);
            for (final CmrInverseDissemination cmrInverseDissemination : cmrInverseDisseminations) {
                if (cmrInverseDissemination.getInverseModel() != null) {
                    inverseModel.add(cmrInverseDissemination.getInverseModel());
                }
                if (cmrInverseDissemination.getSameAsModel() != null) {
                    sameAsModel.add(cmrInverseDissemination.getSameAsModel());
                }
            }

            final Identifier sameAsIdentifier = this.sameAsCache.getSameAses().get(metsElement.getCellarId().getIdentifier());
            sameAsModel.add(SameAsModelUtils.getSameAsModel(Collections.singletonList(sameAsIdentifier)));

            return new Pair<>(JenaUtils.create(inverseModel, sameAsModel), isTruncated);
        } finally {
            JenaUtils.closeQuietly(inverseModel, sameAsModel);
        }
    }

    /**
     * Retrieve inverse targets.
     *
     * @param metsElement the mets element
     * @return the pair
     */
    private Pair<Collection<String>, Boolean> retrieveInverseTargets(final CellarIdentifiedObject metsElement) {
        final Pair<Collection<InverseRelation>, Boolean> inverseRelationsResult = getInverseRelations(metsElement);
        final Collection<InverseRelation> inverseRelations = inverseRelationsResult.getOne();
        final Boolean inverseRelationsTruncated = inverseRelationsResult.getTwo();

        // we need a set here, as there may be documents that reference this document multiple times via its sameAses
        final Collection<String> inverseTargets = new HashSet<>();
        CollectionUtils.collect(inverseRelations, InverseRelation::getTarget, inverseTargets);

        return new Pair<>(inverseTargets, inverseRelationsTruncated);
    }

    /**
     * Retrieve inverse triple metadata non optimized.
     *
     * @param inverseTargets the inverse targets
     * @param underEmbargo   the under embargo
     * @return the map
     */
    private Map<String, Model> retrieveInverseTripleMetadataNonOptimized(final List<String> inverseTargets, final Boolean underEmbargo) {
        if (underEmbargo) {
            return cmrMetadataRelationalGateway.selectModelsPerContext(CMR_INVERSE.getEmbargoTable(), inverseTargets);
        }
        return cmrMetadataRelationalGateway.selectModelsPerContext(CMR_INVERSE.getNormalTable(), inverseTargets);

    }

    /**
     * Retrieve inverse triple metadata.
     *
     * @param inverseTargets the inverse targets
     * @param underEmbargo   the under embargo
     * @return the map
     */
    private Map<String, CmrInverseDissemination> retrieveInverseTripleMetadata(final List<String> inverseTargets,
                                                                               final boolean underEmbargo) {
        if (underEmbargo) {
            return cmrMetadataRelationalGateway.selectModelsPerContext(CMR_INVERSE.getEmbargoTable(), inverseTargets)
                    .entrySet()
                    .stream()
                    .collect(Collectors.toMap(Map.Entry::getKey, e -> adapt(e.getKey(), e.getValue())));

        } else {
            return cmrInverseDisseminationDao.selectByContextsMapped(inverseTargets, cellarConfiguration.getCellarDatabaseInClauseParams(),
                    CMR_INVERSE.getDisseminationTable().toString());
        }
    }

    /**
     * Gets the inverse relations.
     *
     * @param metsElement the mets element
     * @return the inverse relations
     */
    private Pair<Collection<InverseRelation>, Boolean> getInverseRelations(final CellarIdentifiedObject metsElement) {
        final String metsIdentifier = metsElement.getCellarId().getIdentifier();
        final Identifier identifier = this.sameAsCache.getIdentifier(metsIdentifier);
        if (identifier == null) {
            LOG.warn("the identifier '{}' doesn't exist anymore", metsIdentifier);
            return new Pair<>(Collections.<InverseRelation>emptySet(), false);
        }

        final List<String> ids = new ArrayList<>();
        ids.add(identifier.getCellarId());
        ids.addAll(identifier.getPids());

        return inverseRelationDao.findRelationsBySourcesAndType(ids, metsElement.getType(), cellarConfiguration.getCellarServiceInverseNoticesLimit());
    }

    private static CmrInverseDissemination adapt(String context, Model model) {
        CmrInverseDissemination cid = new CmrInverseDissemination();
        cid.setContext(context);
        cid.setBacklog(false);
        cid.setInverseModel(model);
        return cid;
    }

}
