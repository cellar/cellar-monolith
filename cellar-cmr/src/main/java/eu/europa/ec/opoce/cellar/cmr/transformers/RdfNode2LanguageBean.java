/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.transformers
 *             FILE : RdfNode2LanguageBean.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 25-Jan-2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.transformers;

import eu.europa.ec.opoce.cellar.ccr.service.LanguagesNalSkosLoaderService;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;

import org.apache.commons.collections15.Transformer;

import org.apache.jena.rdf.model.RDFNode;

/**
 * <p>RdfNode2LanguageBean class.</p>
 *
 * @author ARHS Developments
 * @version $$Revision$$$
 */
public class RdfNode2LanguageBean implements Transformer<RDFNode, LanguageBean> {

    /** The languages skos service. */
    private final LanguagesNalSkosLoaderService languagesSkosService;

    /**
     * <p>Constructor for RdfNode2LanguageBean.</p>
     *
     * @param languagesSkosService the languages skos service
     */
    public RdfNode2LanguageBean(final LanguagesNalSkosLoaderService languagesSkosService) {
        this.languagesSkosService = languagesSkosService;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LanguageBean transform(final RDFNode input) {
        if (!input.isURIResource()) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("the rdfnode '{}' is no resource")
                    .withMessageArgs(input.toString()).build();
        }
        return this.languagesSkosService.getLanguageBeanByUri(input.asResource().getURI());
    }
}
