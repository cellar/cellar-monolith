package eu.europa.ec.opoce.cellar.cmr.database.id;

import eu.europa.ec.opoce.cellar.cmr.database.QueryService;
import eu.europa.ec.opoce.cellar.cmr.database.dao.id.DatabaseIdService;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
/**
 * <p>IdolOracleSequence class.</p>
 */
public class IdolOracleSequence implements DatabaseIdService {

    @Autowired(required = true)
    @Qualifier(value = "idolQueryService")
    private QueryService queryService;

    @SuppressWarnings("serial")
    private Map<String, String> sequences = new HashMap<String, String>() {

        {
            put("TB_CELLAR_MODIFICATION", "CRM_IDOL_TABLE_SEQ");
        }
    };

    /**
     * {@inheritDoc}
     */
    public long getId(String tableName) {
        String sequence = sequences.get(tableName);
        if (StringUtils.isBlank(sequence)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("No sequence defined for table '{}'").withMessage(tableName)
                    .build();
        }

        return queryService.queryForLong("select " + sequence + ".nextval from dual");
    }
}
