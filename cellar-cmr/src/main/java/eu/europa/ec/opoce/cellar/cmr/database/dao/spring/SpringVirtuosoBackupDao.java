/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.database.dao.spring
 *             FILE : SpringModelLoadRequestDao.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Mar 17, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.database.dao.spring;

import eu.europa.ec.opoce.cellar.cmr.database.dao.VirtuosoBackupDao;
import eu.europa.ec.opoce.cellar.virtuosobackup.BackupType;
import eu.europa.ec.opoce.cellar.virtuosobackup.VirtuosoBackup;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Mar 17, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Repository
public class SpringVirtuosoBackupDao extends CmrSpringBaseDao<VirtuosoBackup, Long> implements VirtuosoBackupDao {

    /**
     * Instantiates a new spring virtuoso backup dao.
     */
    public SpringVirtuosoBackupDao() {
        super(TABLE_NAME, Arrays.asList(Column.OBJECT_ID, Column.INSERTION_DATE, Column.RULE_SET_QUERY, Column.OPERATION_TYPE,
                Column.IS_RESYNCABLE, Column.REASON, Column.PRODUCTION_ID, Column.BACKUP_TYPE, Column.MODEL, Column.SKOS_MODEL));
    }

    /** {@inheritDoc} */
    @SuppressWarnings("serial")
    @Override
    public VirtuosoBackup create(final ResultSet rs) throws SQLException {
        return new VirtuosoBackup() {

            {
                this.setObjectId(rs.getString(Column.OBJECT_ID));
                this.setInsertionDate(rs.getTimestamp(Column.INSERTION_DATE));
                this.setOperationType(OperationType.findByValue(rs.getString(Column.OPERATION_TYPE)));
                this.setResyncable(rs.getByte(Column.IS_RESYNCABLE) == 1);
                this.setReason(rs.getString(Column.REASON));
                this.setModel(rs.getString(Column.MODEL));
                this.setSkosModel(rs.getString(Column.SKOS_MODEL));
                this.setBackupType(BackupType.valueOf(rs.getString(Column.BACKUP_TYPE)));
                this.setRuleSetQuery(rs.getString(Column.RULE_SET_QUERY));

                final String productionId = rs.getString(Column.PRODUCTION_ID);
                if (StringUtils.isNotBlank(productionId)) {
                    this.setProductionIds(Arrays.asList(StringUtils.split(productionId, ",")));
                }
            }
        };
    }

    /** {@inheritDoc} */
    @Override
    public void fillMap(final VirtuosoBackup daoObject, final Map<String, Object> map) {
        map.put(Column.OBJECT_ID, daoObject.getObjectId());
        map.put(Column.INSERTION_DATE, daoObject.getInsertionDate());
        map.put(Column.OPERATION_TYPE, daoObject.getOperationType().getValue());
        map.put(Column.IS_RESYNCABLE, Byte.valueOf(daoObject.isResyncable() ? "1" : "0"));
        map.put(Column.REASON, daoObject.getReason());
        map.put(Column.BACKUP_TYPE, daoObject.getBackupType().toString());
        map.put(Column.RULE_SET_QUERY, daoObject.getRuleSetQuery());
        if ((daoObject.getProductionIds() != null) && !daoObject.getProductionIds().isEmpty()) {
            map.put(Column.PRODUCTION_ID, StringUtils.join(daoObject.getProductionIds(), ","));
        }
        // the order of iteration should obey the order or insertion
        //LOB columns should be listed last in the insert or dml command.
        map.put(Column.MODEL, daoObject.getModel());
        map.put(Column.SKOS_MODEL, daoObject.getSkosModel());

    }

    /** {@inheritDoc} */
    @Override
    public Collection<VirtuosoBackup> findByObjectId(final String objectId) {
        return this.find(WHERE_PART_OBJECT_ID, new HashMap<String, Object>(1) {

            private static final long serialVersionUID = 1L;

            {
                this.put("objectId", objectId);
            }
        });
    }

    /** {@inheritDoc} */
    @Override
    public Collection<VirtuosoBackup> findAllSortedByInsertionDateAsc(final int limit, final boolean isResyncable) {
        return this.findSortBy(WHERE_PART_IS_RESYNCABLE, new HashMap<String, Object>(1) {

            private static final long serialVersionUID = 1L;

            {
                this.put("isResyncable", isResyncable);
            }
        }, Column.INSERTION_DATE, limit).getOne();
    }

    /** {@inheritDoc} */
    @Override
    public long countAllResyncable() {
        return this.count(WHERE_PART_IS_RESYNCABLE, new HashMap<String, Object>(1) {

            private static final long serialVersionUID = 1L;

            {
                this.put("isResyncable", true);
            }
        });
    }

    /** {@inheritDoc} */
    @Override
    public VirtuosoBackup findLastBackup(final String objectId) {
        // gets the collection of backups with the given cellarId, in insertion date ascending order
        final Collection<VirtuosoBackup> backups = findSortBy(WHERE_PART_OBJECT_ID, new HashMap<String, Object>(1) {

            private static final long serialVersionUID = 1L;

            {
                this.put("objectId", objectId);
            }
        }, Column.INSERTION_DATE);

        // select the last backup
        VirtuosoBackup lastBackup = null;
        for (final Iterator<VirtuosoBackup> iterator = backups.iterator(); iterator.hasNext();) {
            lastBackup = iterator.next();
        }
        return lastBackup;
    }

}
