package eu.europa.ec.opoce.cellar.cmr.database;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
/**
 * <p>CmrQueryService class.</p>
 */
public class CmrQueryService extends QueryService {

    /**
     * <p>setCellarDataSource.</p>
     *
     * @param dataSource a {@link javax.sql.DataSource} object.
     */
    @Autowired(required = true)
    public void setCellarDataSource(@Qualifier("cmrDataSource") DataSource dataSource) {
        setDataSource(dataSource);
    }
}
