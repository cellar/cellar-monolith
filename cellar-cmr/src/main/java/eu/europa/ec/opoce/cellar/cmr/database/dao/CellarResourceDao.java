/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : CellarResourceDao.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 6 juin 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.database.dao;

import eu.europa.ec.opoce.cellar.cl.dao.BaseDao;
import eu.europa.ec.opoce.cellar.cmr.AlignerStatus;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.CellarResource.CleanerStatus;
import eu.europa.ec.opoce.cellar.cmr.notice.xslt.function.EmbeddedNoticeRetrievalMethodType;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.exception.CellarException;

import java.util.*;

/**
 * <p>CellarResourceDao interface.</p>
 *
 * @author Pieter.Fannes
 */
public interface CellarResourceDao extends BaseDao<CellarResource, Long> {

     Date selectMinEmbargoDate(Date currentTimestamp);
    /**
     * <p>findCellarId.</p>
     *
     * @param identifier a {@link java.lang.String} object.
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.CellarResource} object.
     */
    CellarResource findCellarId(final String identifier);
    
    
    /**
     * <p>findCellarIds.</p>
     *
     * @param identifiers the Cellar identifiers to look for.
     * @return the matching collection of Cellar resources.
     */
    Collection<CellarResource> findCellarIds(final Collection<String> identifiers);

    /**
     * By default (for performance reasons) the field EMBEDDED_NOTICE
     * is not retrieved. This method give the access to that column.
     * If you don't need to retrieve the EMBEDDED_NOTICE use {@link #findCellarId(String)}
     * instead.
     *
     * @param identifier the cellar identifier
     * @return the {@link CellarResource} which match the identifier, or null
     * if nothing has been found
     * @throws CellarException if the identifier is null
     */
    CellarResource findResourceWithEmbeddedNotice(String identifier);

    /**
     * Check if the digital object designated by the identifier in
     * parameter exists in the database.
     *
     * @param identifier the identifier
     * @return true if the digital object exists in the database
     */
    boolean exists(String identifier);

    /**
     * Check if a given resource is under embargo
     *
     * @param cellarId
     * @return true if under embargo, false otherwise
     */
    boolean isUnderEmbargo(String cellarId);

    /**
     * Find cellar id starting with.
     *
     * @param identifier the identifier
     * @return the collection
     */
    List<CellarResource> findCellarIdStartingWith(final String identifier);

    /**
     * <p>findStartingWith.</p>
     *
     * @param identifier a {@link java.lang.String} object.
     * @return a {@link java.util.Collection} object.
     */
    Collection<CellarResource> findStartingWith(String identifier);

    /**
     * By default (for performance reasons) the field EMBEDDED_NOTICE
     * is not retrieved. This method give the access to that column.
     * If you don't need to retrieve the EMBEDDED_NOTICE use {@link
     * #findStartingWith(String)}
     * instead.
     *
     * @param identifier the cellar identifier
     * @return the collection of {@link CellarResource}s which start with
     * the identifier, or an empty collection if nothing has been found
     * @throws CellarException if the identifier is null
     */
    Collection<CellarResource> findWithEmbeddedNoticeStartingWith(String identifier);

    /**
     * @param cellarId the beginning of the cellarId
     * @param type     the type of digital object to find
     * @return the list of the {@link CellarResource} which start
     */
    Collection<CellarResource> findResourceStartWith(String cellarId, DigitalObjectType type);

    /**
     * <p>findStartingWithSortByCellarId.</p>
     *
     * @param identifier a {@link java.lang.String} object.
     * @return a {@link java.util.Collection} object, sorted by the given identifier.
     */
    Collection<CellarResource> findStartingWithSortByCellarId(final String identifier);

    /**
     * <p>findHierarchy.</p>
     *
     * @param identifier a {@link java.lang.String} object.
     * @return a {@link java.util.Collection} object.
     */
    Collection<CellarResource> findHierarchy(String identifier);

    /**
     * By default (for performance reasons) the field EMBEDDED_NOTICE
     * is not retrieved. This method give the access to that column.
     * If you don't need to retrieve the EMBEDDED_NOTICE use {@link
     * #findHierarchy(String)}
     * instead.
     *
     * @param identifier the cellar identifier
     * @return the collection of {@link CellarResource}s which start with
     * the identifier, or an empty collection if nothing has been found
     * @throws CellarException if the identifier is null
     */
    Collection<CellarResource> findSpecificWemHierarchyWithEmbeddedNotice(String identifier);

    /**
     * <p>findExpression.</p>
     *
     * @param identifier a {@link java.lang.String} object.
     * @param language   a {@link java.lang.String} object.
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.CellarResource} object.
     */
    CellarResource findExpression(String identifier, String language);

    /**
     * Find expressions.
     *
     * @param identifier the identifier
     * @param language   the language
     * @return the collection
     */
    List<CellarResource> findExpressions(String identifier, String language);

    /**
     * <p>findExpressions.</p>
     *
     * @param identifier a {@link java.lang.String} object.
     * @return a {@link java.util.Collection} object.
     */
    Collection<CellarResource> findExpressions(String identifier);

    /**
     * 
     * @param identifier the Cellar identifier.
     * @param type the type of digital object to find.
     * @param language the language to be matched.
     * @return the rows satisfying the combined criteria of base-CellarId, type, and language matches. 
     */
    Collection<CellarResource> findResourcesWithTypeAndLanguage(String identifier, DigitalObjectType type, String language);

    /**
     * <p>findManifestations.</p>
     *
     * @param identifier a {@link java.lang.String} object.
     * @return a {@link java.util.Collection} object.
     */
    Collection<CellarResource> findManifestations(final String identifier);

    /**
     * <p>findTree.</p>
     *
     * @param identifier a {@link java.lang.String} object.
     * @return a {@link java.util.Collection} object.
     */
    Collection<CellarResource> findTree(String identifier);

    /**
     * Return the Cellar resource corresponding to the complete tree of the identifier.
     *
     * @param identifier the identifier to search
     * @return the Cellar resource corresponding to the complete tree
     */
    List<CellarResource> findTreeSortByCellarId(final String identifier);

    /**
     * Find tree metadata sort by cellar id.
     *
     * @param identifier the identifier
     * @return the list
     */
    List<CellarResource> findTreeMetadataSortByCellarId(final String identifier);
    
    /**
     * Wrapper method for retrieving Cellar resources with their embedded notices,
     * that delegates to the appropriate DAO method based on the type of the resource to retrieve.
     * 
     * @param methodType the type of the DAO method to use.
     * @param identifier the identifier.
     * @return the retrieved Cellar resources.
     */
    Collection<CellarResource> retrieveWithEmbeddedNoticeUsingCache(EmbeddedNoticeRetrievalMethodType methodType, String identifier);

    /**
     * Gets the number of metadata resources.
     *
     * @return the number of metadata resources
     */
    long getNumberOfMetadataResources();

    /**
     * Gets the number of metadata resources diagnosable.
     *
     * @return the number of metadata resources diagnosable
     */
    long getNumberOfMetadataResourcesDiagnosable();

    /**
     * Gets the number of metadata resources fixable.
     *
     * @return the number of metadata resources fixable
     */
    long getNumberOfMetadataResourcesFixable();

    /**
     * Gets the number of metadata resources error.
     *
     * @return the number of metadata resources error
     */
    long getNumberOfMetadataResourcesError();

    /**
     * Gets the total number of works.
     *
     * @return the total number of works
     */
    long getTotalNumberOfWorks();

    /**
     * Gets the works order by last modified desc and last index asc.
     *
     * @param limit          the limit
     * @param lastIndexation the last indexation
     * @return the works order by last modified desc and last index asc
     */
    Collection<CellarResource> getWorks(long limit, final Date lastIndexation);

    /**
     * Gets the last works.
     *
     * @param limit                the limit
     * @param lastModificationDate the last modification date
     * @param lastIndexation       the last indexation
     * @return the last works
     */
    Collection<CellarResource> getLastWorks(long limit, final Date lastModificationDate, final Date lastIndexation);

    /**
     * Gets the total number of dossiers.
     *
     * @return the total number of dossiers
     */
    long getTotalNumberOfDossiers();

    /**
     * Gets the total number of events.
     *
     * @return the total number of events
     */
    long getTotalNumberOfEvents();

    /**
     * Gets the total number of agents.
     *
     * @return the total number of agents
     */
    long getTotalNumberOfAgents();

    /**
     * Gets the total number of top level events.
     *
     * @return the total number of top level events
     */
    long getTotalNumberOfTopLevelEvents();

    /**
     * Find metadata resources per cleaner status.
     *
     * @param cleanerStatus the cleaner status
     * @param batchSize     the batch size
     * @return the collection
     */
    Collection<CellarResource> findMetadataResourcesPerCleanerStatus(final CleanerStatus cleanerStatus, final int batchSize);

    /**
     * Find roots per cleaner status.
     *
     * @param cleanerStatus the cleaner status
     * @param batchSize     the batch size
     * @return the collection
     */
    Collection<CellarResource> findRootsPerCleanerStatus(final CleanerStatus cleanerStatus, final int batchSize);

    /**
     * Find roots per aligner status.
     *
     * @param alignerStatus the aligner status
     * @param batchSize     the batch size
     * @return the collection
     */
    Collection<CellarResource> findRootsPerAlignerStatus(final AlignerStatus alignerStatus, final int batchSize);

    /**
     * <p>getExpressionsPerLanguage.</p>
     *
     * @return a {@link java.util.Map} object.
     */
    Map<String, Integer> getExpressionsPerLanguage();

    /**
     * <p>getManifestationsPerType.</p>
     *
     * @return a {@link java.util.Map} object.
     */
    Map<String, Integer> getManifestationsPerType();

    /**
     * <p>getManifestationsPerTypePerLanguage.</p>
     *
     * @return a {@link java.util.Map} object.
     */
    Map<String, Map<String, Integer>> getManifestationsPerTypePerLanguage();

    /**
     * Retrieves the root object of the object with cellarId = {@link cellarId}.
     *
     * @param cellarId the cellarId of the object of which retrieving the root
     * @return the corresponding root object, or null if no object is found
     */
    CellarResource getRoot(String cellarId);

    /**
     * Gets the aligner resources in error count.
     *
     * @return the aligner resources in error count
     */
    long getAlignerResourcesInErrorCount();

    /**
     * Gets the aligner resources diagnosable count.
     *
     * @return the aligner resources diagnosable count
     */
    long getAlignerResourcesDiagnosableCount();

    /**
     * Gets the all resources with lower embargo date.
     *
     * @param identifier  the identifier
     * @param embargoDate the embargo date
     * @return the all resources with lower embargo date
     */
    List<CellarResource> getAllResourcesWithLowerEmbargoDate(final String identifier, final Date embargoDate);

    /**
     * Update the indexation date of the digital object
     * designated by its cellar identifier.
     *
     * @param cellarId the digital object to update
     * @param date     the new indexation date
     * @return the count of rows modified by the query
     * or -1 if something wrong happened.
     */
    int updateIndexationDate(String cellarId, Date date);

    /**
     * Delete all the {@link CellarResource} where the
     * Cellar ID starts with the value in parameter.
     *
     * @param baseCellarId the beginning of the cellar IDs to find
     * @return the count of rows modified by the query
     * or -1 if something wrong happened.
     */
    int deleteResourceStartsWith(String baseCellarId);

    /**
     * Count the number of manifestations based on the
     * baseCellarId (BASE_CELLAR_ID column)
     *
     * @param baseCellarId the base cellar ID (work id)
     * @return the number of manifestations
     */
    int countManifestations(String baseCellarId);

}
