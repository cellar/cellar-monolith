/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.service
 *             FILE : CellarResourceListHttpMessageElement.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 17-Jan-2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2016 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.converter;

import java.io.Serializable;
import java.util.SortedSet;

/**
 * The Class CellarResourceListHttpMessageElement.
 * <class_description> this object represents the cellar resource object in the HTTP-300 response.
 * <br/><br/>
 * <notes> .
 * <br/><br/>
 * ON : 17-Jan-2017
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class CellarResourceListHttpMessageElement implements Serializable {

    private static final long serialVersionUID = -7228432347323919626L;

    /** The cellar id. */
    private String cellarId;

    /** The uri. */
    private String uri;

    /** The used languages. */
    private SortedSet<String> usedLanguages;

    /**
     * Gets the cellar id.
     *
     * @return the cellar id
     */
    public String getCellarId() {
        return this.cellarId;
    }

    /**
     * Sets the cellar id.
     *
     * @param cellarId the new cellar id
     */
    public void setCellarId(final String cellarId) {
        this.cellarId = cellarId;
    }

    /**
     * Gets the uri.
     *
     * @return the uri
     */
    public String getUri() {
        return this.uri;
    }

    /**
     * Sets the uri.
     *
     * @param uri the new uri
     */
    public void setUri(final String uri) {
        this.uri = uri;
    }

    /**
     * Gets the used languages.
     *
     * @return the used languages
     */
    public SortedSet<String> getUsedLanguages() {
        return this.usedLanguages;
    }

    /**
     * Sets the used languages.
     *
     * @param usedLanguages the new used languages
     */
    public void setUsedLanguages(final SortedSet<String> usedLanguages) {
        this.usedLanguages = usedLanguages;
    }
}
