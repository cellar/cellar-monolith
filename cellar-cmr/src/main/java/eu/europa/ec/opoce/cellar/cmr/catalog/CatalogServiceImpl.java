package eu.europa.ec.opoce.cellar.cmr.catalog;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.cmr.catalog.entityresolving.CatalogConfigurationService;
import eu.europa.ec.opoce.cellar.common.CatalogService;
import eu.europa.ec.opoce.cellar.configuration.BaseUriConfiguration;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import org.apache.commons.lang.StringUtils;
import org.apache.xerces.util.XMLCatalogResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;

@Service
public class CatalogServiceImpl implements CatalogService {

    @Autowired
    private BaseUriConfiguration baseUriConfiguration;

    @Autowired
    private CatalogConfigurationService entityConfigurationService;

    private XMLCatalogResolver catalog;

    @PostConstruct
    public void init() {
        String baseUri = baseUriConfiguration.getBaseUri();
        if (StringUtils.isBlank(baseUri)) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.ENTITY_RESOLVING_ERROR)
                    .withMessage("the baseUri of the application is not set").build();
        }
        File uriCatalog = entityConfigurationService.getUriCatalog(baseUri);
        if (uriCatalog == null || !uriCatalog.exists()) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.ENTITY_RESOLVING_ERROR)
                    .withMessage("there is no catalog found for baseUri {}").withMessageArgs(baseUri).build();
        }

        catalog = new XMLCatalogResolver(new String[]{uriCatalog.getAbsolutePath()});
        catalog.setPreferPublic(true);
        catalog.setUseLiteralSystemId(false);
    }

    public XMLCatalogResolver getCatalog() {
        return catalog;
    }

}
