package eu.europa.ec.opoce.cellar.cmr.service;

import java.util.Map;
import java.util.regex.Pattern;

import org.apache.jena.query.Query;

/**
 * The Interface StructuredDynamicQueryConfiguration.
 */
public interface StructuredDynamicQueryConfiguration {

    /**
     * Gets the query for oj of series.
     *
     * @param placeHoldersMap the place holders map
     * @return the query for oj of series
     */
    Query getQueryForOjOfSeries(Map<Pattern, Object> placeHoldersMap);

    /**
     * Gets the query for oj of the day.
     *
     * @param placeHoldersMap the place holders map
     * @return the query for oj of the day
     */
    Query getQueryForOjOfTheDay(final Map<Pattern, Object> placeHoldersMap);

    /**
     * Gets the query for the most recent work date of oj.
     *
     * @param placeHoldersMap the place holders map
     * @return the query for the most recent work date of oj
     */
    Query getQueryForTheMostRecentWorkDateOfOj(final Map<Pattern, Object> placeHoldersMap);

    /**
     * Gets the query to list all series of oj.
     *
     * @param placeHoldersMap the place holders map
     * @return the query to list all series of oj
     */
    Query getQueryToListAllSeriesOfOj(final Map<Pattern, Object> placeHoldersMap);

    /**
     * Gets the query to list all sub series of oj.
     *
     * @param placeHoldersMap the place holders map
     * @return the query to list all sub series of oj
     */
    Query getQueryToListAllSubSeriesOfOj(final Map<Pattern, Object> placeHoldersMap);
}
