package eu.europa.ec.opoce.cellar.core;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>Abstract ConvertHistoryMap class.</p>
 */
public abstract class ConvertHistoryMap<T, S> {

    private Map<T, S> map;

    /**
     * <p>Constructor for ConvertHistoryMap.</p>
     */
    public ConvertHistoryMap() {
        this.map = new HashMap<T, S>();
    }

    /**
     * <p>getConvertedValue.</p>
     *
     * @param input a T object.
     * @return a S object.
     */
    public S getConvertedValue(T input) {
        if (map.containsKey(input)) {
            return map.get(input);
        } else {
            S value = convert(input);
            map.put(input, value);
            return value;
        }
    }

    /**
     * <p>convert.</p>
     *
     * @param input a T object.
     * @return a S object.
     */
    public abstract S convert(T input);
}
