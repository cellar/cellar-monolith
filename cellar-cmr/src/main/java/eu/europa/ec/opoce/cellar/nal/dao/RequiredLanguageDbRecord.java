package eu.europa.ec.opoce.cellar.nal.dao;

import java.util.Objects;

/**
 * <p>RequiredLanguageDbRecord class.</p>
 */
public class RequiredLanguageDbRecord {

    private String uri;
    private int order;
    private String alternativeUri;

    /**
     * <p>Constructor for RequiredLanguageDbRecord.</p>
     *
     * @param uri a {@link java.lang.String} object.
     */
    public RequiredLanguageDbRecord(String uri) {
        this.uri = uri;
    }

    /**
     * <p>Constructor for RequiredLanguageDbRecord.</p>
     *
     * @param uri            a {@link java.lang.String} object.
     * @param sorting        a int.
     * @param alternativeUri a {@link java.lang.String} object.
     */
    public RequiredLanguageDbRecord(String uri, int sorting, String alternativeUri) {
        this.uri = uri;
        this.order = sorting;
        this.alternativeUri = alternativeUri;
    }

    /**
     * <p>Getter for the field <code>uri</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getUri() {
        return uri;
    }

    /**
     * <p>Getter for the field <code>order</code>.</p>
     *
     * @return a int.
     */
    public int getOrder() {
        return order;
    }

    /**
     * <p>Getter for the field <code>alternativeUri</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getAlternativeUri() {
        return alternativeUri;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RequiredLanguageDbRecord)) return false;
        RequiredLanguageDbRecord that = (RequiredLanguageDbRecord) o;
        return getOrder() == that.getOrder() &&
                Objects.equals(getUri(), that.getUri()) &&
                Objects.equals(getAlternativeUri(), that.getAlternativeUri());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUri(), getOrder(), getAlternativeUri());
    }

    @Override
    public String toString() {
        return "RequiredLanguageDbRecord{" +
                "uri='" + uri + '\'' +
                ", order=" + order +
                ", alternativeUri='" + alternativeUri + '\'' +
                '}';
    }
}
