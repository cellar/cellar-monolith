package eu.europa.ec.opoce.cellar.cmr.utils;

import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.OWL;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Set;

public final class XmlNoticeUtils {

    private XmlNoticeUtils() {}

    public static String tagize(final Property property) {
        String uri = property.getURI();
        int lastPartIndex = uri.lastIndexOf('#');
        if (-1 == lastPartIndex) {
            lastPartIndex = uri.lastIndexOf('/');
        }
        uri = uri.substring(lastPartIndex + 1);

        final int queryIndex = uri.indexOf('?');
        return (queryIndex == -1 ? uri : uri.substring(0, queryIndex)).toUpperCase();
    }

    public static String decodeUri(final String uri) {
        try {
            return URLDecoder.decode(uri, "UTF-8");
        } catch (final UnsupportedEncodingException e) {
            throw ExceptionBuilder.get(CellarException.class).withCause(e).build();
        }
    }

    public static Set<Resource> getSameAsResources(final Resource cellarResource) {
        return cellarResource.listProperties(OWL.sameAs)
                .mapWith(Statement::getResource)
                .toSet();
    }

    public static Set<Statement> getAllStatements(final Resource cellarResource, final Set<Resource> sameAsResources) {
        final Set<Statement> statements = cellarResource.listProperties().toSet();

        for (final Resource sameAsResource : sameAsResources) {
            statements.addAll(sameAsResource.listProperties().toSet());
        }

        return statements;
    }

    public static Resource getResourceFromModel(final Resource resource, final Model model) {
        final StmtIterator stmtIterator = model.listStatements(null, OWL.sameAs, resource);
        try {
            return stmtIterator.hasNext() ? stmtIterator.next().getSubject() : resource;
        } finally {
            stmtIterator.close();
        }
    }
}
