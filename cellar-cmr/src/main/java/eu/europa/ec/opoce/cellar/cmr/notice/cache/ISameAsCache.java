/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice
 *             FILE : NoticeCache.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 29, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.notice.cache;

import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.CellarIdentifiedObject;

import java.util.Collection;
import java.util.Map;

import org.apache.jena.rdf.model.Model;

/**
 * <class_description> Cache for sameAs-es.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 29, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface ISameAsCache {

    void init(final CellarIdentifiedObject object);

    Model extractSameAses(final Model inputModel);

    Collection<Identifier> getIdentifiers(final Collection<String> uris);

    Identifier getIdentifier(final String uri);

    Map<String, Identifier> getSameAses();

}
