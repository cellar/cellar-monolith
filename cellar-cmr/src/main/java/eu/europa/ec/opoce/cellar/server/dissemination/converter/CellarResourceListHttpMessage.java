/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.service
 *             FILE : CellarResourceListHttpMessage.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 17-Jan-2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2016 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.converter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The Class CellarResourceListHttpMessage.
 * <class_description> this class object represents the wrapper of CellarResourceList elements that represent the cellar resource object in the HTTP-300 response.
 * <br/><br/>
 * <notes> .
 * <br/><br/>
 * ON : 17-Jan-2017
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class CellarResourceListHttpMessage implements Serializable {

    private static final long serialVersionUID = -7674815564129805693L;

    /** The expressions. */
    private List<CellarResourceListHttpMessageElement> expressions = new ArrayList<CellarResourceListHttpMessageElement>();

    /**
     * Gets the expressions.
     *
     * @return the expressions
     */
    public List<CellarResourceListHttpMessageElement> getExpressions() {
        return this.expressions;
    }

    /**
     * Sets the expressions.
     *
     * @param expressions the new expressions
     */
    public void setExpressions(final List<CellarResourceListHttpMessageElement> expressions) {
        this.expressions = expressions;
    }

}
