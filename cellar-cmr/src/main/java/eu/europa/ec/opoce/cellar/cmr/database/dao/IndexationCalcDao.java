package eu.europa.ec.opoce.cellar.cmr.database.dao;

import eu.europa.ec.opoce.cellar.cl.dao.BaseDao;
import eu.europa.ec.opoce.cellar.cmr.IndexationCalc;

public interface IndexationCalcDao extends BaseDao<IndexationCalc, Long> {

    IndexationCalc findCellarId(final String identifier);
}
