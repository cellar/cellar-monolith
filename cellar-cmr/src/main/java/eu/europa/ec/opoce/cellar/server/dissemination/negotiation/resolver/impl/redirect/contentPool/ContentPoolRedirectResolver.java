/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.content
 *             FILE : ContentRedirectResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 14, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.contentPool;

import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.server.dissemination.AcceptLanguage;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.DisseminationRequest;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.RedirectResolver;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;
import eu.europa.ec.opoce.cellar.server.service.DisseminationException;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.http.HttpStatus;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Apr 14, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Configurable
public abstract class ContentPoolRedirectResolver extends RedirectResolver {

    /** The dissemination request. */
    private final DisseminationRequest disseminationRequest;

    /** The accept. */
    private final String accept;

    /**
     * Instantiates a new content pool redirect resolver.
     *
     * @param cellarResource the cellar resource
     * @param disseminationRequest the dissemination request
     * @param accept the accept
     * @param acceptLanguage the accept language
     * @param provideAlternates the provide alternates
     */
    protected ContentPoolRedirectResolver(final CellarResourceBean cellarResource, final DisseminationRequest disseminationRequest,
            final String accept, final AcceptLanguage acceptLanguage, final boolean provideAlternates) {
        super(cellarResource, acceptLanguage, provideAlternates);
        this.disseminationRequest = disseminationRequest;
        this.accept = accept;
    }

    /** {@inheritDoc} */
    @Override
    protected void initDisseminationRequest() {
        super.initDisseminationRequest();
        this.setMimeTypeMappings(this.disseminationRequest, this.accept);
    }

    /** {@inheritDoc} */
    @Override
    protected void doHandleWorkDisseminationRequest() {
        if ((this.acceptLanguageBeans == null) || this.acceptLanguageBeans.isEmpty()) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage("Missing/Invalid accept language: '{}'").withMessageArgs(this.acceptLanguageBeans).build();
        }

        this.retrieveTargetExpressions();
        this.retrieveTargetManifestaion();
    }

    /** {@inheritDoc} */
    @Override
    protected void doHandleExpressionDisseminationRequest() {
        this.getLanguageBeanOfExpression(this.cellarResource);
        this.retrieveTargetManifestaion();
    }

    /** {@inheritDoc} */
    @Override
    protected void doHandleManifestationDisseminationRequest() {
        this.disseminationService.checkForParentExpression(this.cellarResource);
        this.checkManifestationTypeOfManifestation(this.cellarResource);
    }

}
