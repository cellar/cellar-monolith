package eu.europa.ec.opoce.cellar.server.admin.sparql;

public class VirtuosoInformation {

    private final String serverVersion;
    private final String driverVersion;

    public VirtuosoInformation(String serverVersion, String driverVersion) {
        this.serverVersion = serverVersion;
        this.driverVersion = driverVersion;
    }

    public String getServerVersion() {
        return serverVersion;
    }

    public String getDriverVersion() {
        return driverVersion;
    }
}
