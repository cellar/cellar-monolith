/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.jena.oracle.result.resolver
 *             FILE : IResultSetResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Aug 8, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.jena.oracle.result.resolver;

import org.apache.jena.query.ResultSet;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Aug 8, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 * @param <T> the generic type
 */
public interface IJenaResultSetResolver<T> {

    /**
     * Resolve.
     *
     * @param resultSet the result set
     * @return the t
     */
    T resolve(final ResultSet resultSet);
}
