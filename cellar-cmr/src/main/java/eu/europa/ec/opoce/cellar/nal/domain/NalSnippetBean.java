package eu.europa.ec.opoce.cellar.nal.domain;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

/**
 * <p>NalSnippetBean class.</p>
 */
public class NalSnippetBean {

    private String concept;
    private String conceptScheme;
    private final byte[] rdfSnippet;

    private Collection<NalSnippetBean> relatedSnippets;

    /**
     * <p>Constructor for NalSnippetBean.</p>
     *
     * @param concept       a {@link java.lang.String} object.
     * @param conceptScheme a {@link java.lang.String} object.
     * @param rdfSnippet    an array of byte.
     */
    public NalSnippetBean(String concept, String conceptScheme, byte[] rdfSnippet) {
        this.concept = concept;
        this.conceptScheme = conceptScheme;
        this.rdfSnippet = rdfSnippet;
    }

    /**
     * <p>Getter for the field <code>concept</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getConcept() {
        return concept;
    }

    /**
     * <p>Getter for the field <code>conceptScheme</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getConceptScheme() {
        return conceptScheme;
    }

    /**
     * <p>Getter for the field <code>rdfSnippet</code>.</p>
     *
     * @return a {@link java.io.InputStream} object.
     */
    public InputStream getRdfSnippet() {
        return new ByteArrayInputStream(rdfSnippet);
    }

    /**
     * <p>Setter for the field <code>relatedSnippets</code>.</p>
     *
     * @param relatedSnippets a {@link java.util.Collection} object.
     */
    public void setRelatedSnippets(Collection<NalSnippetBean> relatedSnippets) {
        this.relatedSnippets = relatedSnippets != null ? new ArrayList<NalSnippetBean>(relatedSnippets) : null;
    }

    /**
     * <p>Getter for the field <code>relatedSnippets</code>.</p>
     *
     * @return a {@link java.util.Collection} object.
     */
    public Collection<NalSnippetBean> getRelatedSnippets() {
        return relatedSnippets != null ? Collections.unmodifiableCollection(relatedSnippets) : Collections.<NalSnippetBean> emptyList();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof NalSnippetBean)) return false;
        NalSnippetBean that = (NalSnippetBean) o;
        return Objects.equals(getConcept(), that.getConcept()) &&
                Objects.equals(getConceptScheme(), that.getConceptScheme()) &&
                Objects.equals(getRelatedSnippets(), that.getRelatedSnippets());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getConcept(), getConceptScheme(), getRelatedSnippets());
    }

    @Override
    public String toString() {
        return "NalSnippetBean{" +
                "concept='" + concept + '\'' +
                ", conceptScheme='" + conceptScheme + '\'' +
                ", relatedSnippets=" + relatedSnippets +
                '}';
    }
}
