/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.service.impl
 *             FILE : InverseRelationServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 24, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.service.impl;

import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;
import eu.europa.ec.opoce.cellar.cmr.daobeans.InverseRelation;
import eu.europa.ec.opoce.cellar.cmr.database.dao.InverseRelationDao;
import eu.europa.ec.opoce.cellar.cmr.service.IInverseRelationService;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <class_description> Inverse relations service implementation.
 * <br/><br/>
 * ON : Apr 24, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class InverseRelationServiceImpl implements IInverseRelationService {

    /**
     * The inverse relations dao.
     */
    @Autowired
    private InverseRelationDao inverseRelationDao;

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<InverseRelation> getInverseRelationsOnTarget(final Collection<Identifier> productionIdentifiers) {
        final Set<String> targets = new HashSet<String>();
        for (final Identifier productionIdentifier : productionIdentifiers) {
            if (productionIdentifier.cellarIdExists()) {
                targets.add(productionIdentifier.getCellarId());
            }
        }

        if (targets.isEmpty()) {
            return Collections.emptyList();
        } else {
            return this.inverseRelationDao.findRelationsByTargets(targets);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<InverseRelation> getInverseRelationsOnSource(final Collection<Identifier> productionIdentifiers) {
        final Set<String> sources = new HashSet<String>();
        for (final Identifier productionIdentifier : productionIdentifiers) {
            if (productionIdentifier.getPids() != null) {
                sources.addAll(productionIdentifier.getPids());
            }

            if (productionIdentifier.cellarIdExists()) {
                sources.add(productionIdentifier.getCellarId());
            }
        }

        if (sources.isEmpty()) {
            return Collections.emptyList();
        } else {
            return this.inverseRelationDao.findRelationsBySources(sources);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateInverseRelations(final Collection<InverseRelation> inverseRelationsToDelete,
            final Collection<InverseRelation> inverseRelationsToAdd) {
        for (final InverseRelation ir : inverseRelationsToDelete) {
            this.inverseRelationDao.deleteObject(ir);
        }

        for (final InverseRelation ir : inverseRelationsToAdd) {
            this.inverseRelationDao.saveObject(ir);
        }
    }
}
