/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.indexing.processor.impl
 *             FILE : RemoveIndexRequestProcessorImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Nov 17, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.indexing.processor.impl;

import com.google.common.base.Joiner;
import eu.europa.ec.opoce.cellar.cl.domain.structmap_status_history.StructMapStatusHistory;
import eu.europa.ec.opoce.cellar.cl.service.client.StructMapStatusHistoryService;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.IndexNotice;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.Action;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.ExecutionStatus;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.RequestType;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestBatch;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.NoticeIndexRequestHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Nov 17, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component
@Qualifier("group")
@Order(10)
public class RemoveIndexRequestProcessor extends AbstractIndexRequestProcessor {

    private final NoticeIndexRequestHandler noticeIndexRequestHandler;
    
    private final StructMapStatusHistoryService structMapStatusHistoryService;

    @Autowired
    public RemoveIndexRequestProcessor(NoticeIndexRequestHandler noticeIndexRequestHandler,
                                       StructMapStatusHistoryService structMapStatusHistoryService) {
        this.noticeIndexRequestHandler = noticeIndexRequestHandler;
        this.structMapStatusHistoryService = structMapStatusHistoryService;
    }

    @Override
    protected void execute(CmrIndexRequestBatch batch, List<CmrIndexRequest> requests) {
        for (CmrIndexRequest r : requests) {
            if (isRoot(r)) {
                Collection<IndexNotice> indexNotices = noticeIndexRequestHandler.calculateNotice(r);
                Collection<IndexNotice> expandedNotices = noticeIndexRequestHandler.calculateExpanded(r, indexNotices);
                // Update LANGUAGES field of corresponding STRUCTMAP_STATUS_HISTORY entry
                updateStructMapLanguages(requests, expandedNotices);
                break;
            }
        }
    }

    private static boolean isRoot(CmrIndexRequest request) {
        return request.getObjectUri().equals(request.getGroupByUri());
    }

    @Override
    protected List<CmrIndexRequest> filter(CmrIndexRequestBatch request) {
        return request.getCmrIndexRequests().stream()
                .filter(r -> r.getExecutionStatus() == ExecutionStatus.Execution &&
                        r.getRequestType() == RequestType.CalcNotice &&
                        r.getAction() == Action.Remove)
                .collect(Collectors.toList());
    }
    
    /**
     * Update the LANGUAGES field of the corresponding STRUCTMAP_STATUS_HISTORY entry
     * @param requests the current list of CmrIndexRequests
     * @param expandedNotices the totality of the expanded notices generated
     */
    private void updateStructMapLanguages(List<CmrIndexRequest> requests, Collection<IndexNotice> expandedNotices){
        // Filter the REMOVE index requests of interest (i.e. the ones that have a non-null StructMapStatusHistoryId value)
        List<CmrIndexRequest> calcNoticeRequests = requests.stream()
                .filter(indx -> (indx.getStructMapStatusHistoryId() != null && indx.getAction().equals(Action.Remove)))
                .collect(Collectors.toList());
        
        // Update the corresponding STRUCTMAP_STATUS_HISTORY entries
        StructMapStatusHistory structMapStatusHistory;
        for (CmrIndexRequest cmrIndexRequest : calcNoticeRequests){
            structMapStatusHistory = this.structMapStatusHistoryService.getEntry(cmrIndexRequest.getStructMapStatusHistoryId());
            if(structMapStatusHistory != null){
                // Extract the LANGUAGES of the expanded notices into a comma-separated string
                List<String> languagesCodes = expandedNotices.stream().map(indx -> indx.getIsoCode().toString().toUpperCase())
                        .sorted().collect(Collectors.toList());
                String languageString = Joiner.on(", ").join(languagesCodes);
                
                // Update the LANGUAGES field of the corresponding StructMapStatusHistory
                this.structMapStatusHistoryService.updateStructMapLanguages(structMapStatusHistory, languageString);
            }
        }
    }

}
