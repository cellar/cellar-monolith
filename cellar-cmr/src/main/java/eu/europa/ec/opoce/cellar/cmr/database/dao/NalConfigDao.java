/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.database.dao
 *             FILE : NalConfigDao.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 09-Jan-2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.database.dao;

import eu.europa.ec.opoce.cellar.cl.dao.BaseDao;
import eu.europa.ec.opoce.cellar.nal.dao.NalConfig;

import java.util.Collection;
import java.util.Optional;

/**
 * <p>NalConfigDao interface.</p>
 *
 * @author ARHS Developments
 * @version $$Revision$$$
 */
public interface NalConfigDao extends BaseDao<NalConfig, Long> {

    /**
     * <p>findByUri.</p>
     *
     * @param uri a {@link String} object.
     * @return a {@link java.util.Collection} object.
     */
    Optional<NalConfig> findByUri(String uri);

    Optional<NalConfig> findByPid(String pid);

    /**
     * Find by name.
     *
     * @param name the name
     * @return the collection
     */
    Optional<NalConfig> findByName(String name);

    /**
     * <p>findByOntology.</p>
     *
     * @param ontologyUri a {@link java.lang.String} object.
     * @return a {@link java.util.Collection} object.
     */
    Collection<NalConfig> findByOntology(String ontologyUri);

    Collection<NalConfig> findByOntologyLike(String ontologyUri);
}
