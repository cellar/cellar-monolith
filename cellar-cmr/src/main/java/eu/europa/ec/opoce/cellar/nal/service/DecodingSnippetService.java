/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.nal.decoding
 *             FILE : DecodingSnippetService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 03 02, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-03-02 07:31:26 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.nal.service;

import org.apache.jena.rdf.model.Model;
import eu.europa.ec.opoce.cellar.common.util.Pair;
import eu.europa.ec.opoce.cellar.nal.domain.DecodingType;
import eu.europa.ec.opoce.cellar.nal.domain.NalSnippetBean;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;

import java.util.Collection;
import java.util.Map;

/**
 * @author ARHS Developments
 */
public interface DecodingSnippetService {
    Collection<String> getConceptsWithoutDecoding(Collection<String> conceptsToDecode);

    @SuppressWarnings("serial")
    Pair<Collection<String>, Model> getDecodingFor(Collection<String> concepts, DecodingType type);

    Pair<Collection<String>, Model> getDecodingFor(Map<DecodingType, Collection<String>> conceptsPerDecodingType);

    @SuppressWarnings("serial")
    Pair<Collection<String>, Model> getDecodingFor(Collection<String> concepts, LanguageBean language,
                                                   DecodingType type);

    Pair<Collection<String>, Model> getDecodingFor(Map<DecodingType, Collection<String>> conceptsPerDecodingType,
                                                   LanguageBean language);

    Model extractLangDecoding(Model model, LanguageBean language);

    Map<DecodingType, Map<String, NalSnippetBean>> getSnippetsFor(Map<DecodingType, Collection<String>> concepts);

    Map<String, NalSnippetBean> getSnippetsFor(Collection<String> concepts, DecodingType type);
}
