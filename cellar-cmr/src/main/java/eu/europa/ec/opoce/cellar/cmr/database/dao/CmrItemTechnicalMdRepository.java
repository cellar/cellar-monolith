package eu.europa.ec.opoce.cellar.cmr.database.dao;

import eu.europa.ec.opoce.cellar.cmr.technicalMd.CmrItemTechnicalMd;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Repository
@Transactional(transactionManager = "transactionManager")
public interface CmrItemTechnicalMdRepository extends CrudRepository<CmrItemTechnicalMd,Long> {

    Optional<CmrItemTechnicalMd> findById(Long id);


    void deleteAllByCellarIdContaining(String cellarId);

    CmrItemTechnicalMd findByCellarId(String cellarId);
}
