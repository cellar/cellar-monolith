/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.oj
 *             FILE : OfficialJournals.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jul 8, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.oj;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jul 8, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlType(name = "officialJournalsType")
@XmlRootElement(name = "officialJournals")
public class OfficialJournals {

    /** The official journals. */
    private List<OfficialJournal> officialJournals;

    /**
     * Instantiates a new official journals.
     */
    public OfficialJournals() {

    }

    /**
     * Instantiates a new official journals.
     *
     * @param officialJournals the official journals
     */
    public OfficialJournals(final List<OfficialJournal> officialJournals) {
        this.officialJournals = officialJournals;
    }

    /**
     * Gets the official journals.
     *
     * @return the officialJournals
     */
    @XmlElement(name = "officialJournal")
    public List<OfficialJournal> getOfficialJournals() {
        return officialJournals;
    }
}
