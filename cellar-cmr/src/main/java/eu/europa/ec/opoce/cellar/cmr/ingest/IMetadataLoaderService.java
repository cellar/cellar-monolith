/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.ingest
 *             FILE : IMetadataLoaderService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Sep 16, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - ations Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.ingest;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import eu.europa.ec.opoce.cellar.domain.content.mets.CellarIdentifiedObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsPackage;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Sep 16, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IMetadataLoaderService {

    /**
     * Load metadata.
     *
     * @param metsPackage the mets package
     * @param structMap the struct map
     * @param useOldDataIfEmpty the use old data if empty
     * @return the model
     */
    Model loadMetadata(final MetsPackage metsPackage, final StructMap structMap, final boolean useOldDataIfEmpty);

    /**
     * Adds the read only.
     *
     * @param cellarIdentifiedObject the cellar identified object
     * @param model the model
     */
    void addReadOnly(final CellarIdentifiedObject cellarIdentifiedObject, final Model model);

    /**
     * Mark as read only.
     *
     * @param cellarIdentifiedObject the cellar identified object
     * @param readOnly the read only
     */
    void markAsReadOnly(CellarIdentifiedObject cellarIdentifiedObject, boolean readOnly);

    /**
     * Checks if is read only.
     *
     * @param cellarIdentifiedObject the cellar identified object
     * @param model the model
     * @return the boolean
     */
    Boolean isReadOnly(CellarIdentifiedObject cellarIdentifiedObject, final Model model);

    /**
     * Parses the metadata load old data.
     *
     * @param digitalObject the digital object
     * @param useOldDataIfEmpty the use old data if empty
     * @param metadataModel the metadata model
     * @param isContentUpdate the is content update
     * @param metadataRefs the metadata refs
     * @param urisPerDmd the uris per dmd
     */
    void parseMetadataLoadOldData(final DigitalObject digitalObject, final boolean useOldDataIfEmpty, final Model metadataModel,
            final boolean isContentUpdate, final Set<String> metadataRefs, final Map<String, Set<String>> urisPerDmd);

    /**
     * <p>getAllDmdIndexDataToAdd.</p>
     *
     * @param structMap a {@link eu.europa.ec.opoce.cellar.domain.content.mets.StructMap} object.
     * @param dmd       a {@link org.apache.jena.rdf.model.Model} object.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    Model getInferredExtraData(final StructMap structMap, final Model dmd);

    /**
     * <p>addLanguageMetadata.</p>
     *
     * @param digitalObject  a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject} object.
     * @param model          a {@link org.apache.jena.rdf.model.Model} object.
     * @param cellarResource a {@link org.apache.jena.rdf.model.Resource} object.
     */
    void addLanguageMetadata(final DigitalObject digitalObject, final Model model, final Resource cellarResource);

    /**
     * <p>checkIfAllIdsExist check that all defined identifiers have a corresponding resource in a model if not.</p>
     *
     * @param digitalObject a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject} object.
     * @param metadata      a {@link org.apache.jena.rdf.model.Model} object.
     * @param faults        a {@link java.util.List} object, contains all identifiers that don't have metadata attached.
     * @return a {@link java.util.List} object, contains all identifiers that don't have metadata.
     */
    List<Set<String>> checkIfAllIdsExist(final DigitalObject digitalObject, final Model metadata, final List<Set<String>> faults);

    /**
     * <p>removeContentFromMetadataStream removes the content streams of a string with contains the metadata.</p>
     *
     * @param metadata a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    String removeContentFromMetadataStream(final String metadata);

    /**
     * <p>removeLastModifiedInModelRecursive removes the last modification date of of a digital object.</p>
     *
     * @param digitalObject a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject} object.
     * @param model         a {@link org.apache.jena.rdf.model.Model} object.
     */
    void removeLastModifiedInModelRecursive(final DigitalObject digitalObject, final Model model);

    /**
     * <p>addSameAsModelRecursive  adds for of a digital object and its children a same-as relation between the cellar-resource and the production-system-resources.</p>
     *
     * @param digitalObject a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject} object.
     * @param model         a {@link org.apache.jena.rdf.model.Model} object.
     */
    void addSameAsModelRecursive(final DigitalObject digitalObject, final Model model);

    /**
     * <p>addSameAsModel  adds for of a digital object a same-as relation between the cellar-resource and the production-system-resources.</p>
     *
     * @param digitalObject a {@link eu.europa.ec.opoce.cellar.domain.content.mets.CellarIdentifiedObject} object.
     * @param model         a {@link org.apache.jena.rdf.model.Model} object.
     */
    void addSameAsModel(final CellarIdentifiedObject digitalObject, final Model model);

    /**
     * <p>addManifestationType.</p>
     *
     * @param digitalObject a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject} object.
     * @param dmd           a {@link org.apache.jena.rdf.model.Model} object.
     */
    void addManifestationType(final DigitalObject digitalObject, final Model dmd);

    /**
     * <p>addLastModificationInModel.</p>
     *
     * @param digitalObject  a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject} object.
     * @param cellarResource a {@link org.apache.jena.rdf.model.Resource} object.
     * @param model          a {@link org.apache.jena.rdf.model.Model} object.
     */
    void addLastModificationInModel(final DigitalObject digitalObject, final Resource cellarResource, final Model model);

    /**
     * Adds the embargo date in model.
     *
     * @param digitalObject the digital object
     * @param cellarResource the cellar resource
     * @param model the model
     */
    void addEmbargoDateInModel(final DigitalObject digitalObject, final Resource cellarResource, final Model model);

    /**
     * <p>addCreationDateInModel.</p>
     *
     * @param cellarResource a {@link org.apache.jena.rdf.model.Resource} object.
     * @param model          a {@link org.apache.jena.rdf.model.Model} object.
     */
    void addCreationDateInModel(final Resource cellarResource, final Model model, Date creationDate);

    /**
     * <p>addEmbargoDate.</p>
     *
     * @param digitalObject a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject} object.
     * @param dmd the dmd
     */
    void populateDigitalObjectWithExtractedEmbargoDate(final DigitalObject digitalObject, final Model dmd);

    /**
     * Extract embargo date.
     *
     * @param resource the resource
     * @param digitalObjectType the digital object type
     * @return the date
     */
    Date extractEmbargoDate(final Resource resource, final DigitalObjectType digitalObjectType);

}
