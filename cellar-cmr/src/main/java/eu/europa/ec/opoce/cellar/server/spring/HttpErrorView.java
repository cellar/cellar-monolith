package eu.europa.ec.opoce.cellar.server.spring;

import eu.europa.ec.opoce.cellar.semantic.helper.StringHelper;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.View;

/**
 * <p>HttpErrorView class.</p>
 */
public class HttpErrorView implements View {

    private final String msg;
    private final int errorCode;

    /**
     * <p>Constructor for HttpErrorView.</p>
     *
     * @param errorCode a int.
     * @param msg       a {@link java.lang.String} object.
     * @param args      a {@link java.lang.Object} object.
     */
    public HttpErrorView(int errorCode, String msg, Object... args) {
        this.errorCode = errorCode;
        this.msg = StringHelper.format(msg, args);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getContentType() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void render(Map<String, ?> model, HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.sendError(errorCode, msg);
    }
}
