package eu.europa.ec.opoce.cellar.cmr.database.dao;

import eu.europa.ec.opoce.cellar.cl.dao.BaseDao;
import eu.europa.ec.opoce.cellar.ontology.dao.OntoConfig;

import java.util.Optional;

/**
 * @author ARHS Developments
 */
public interface OntoConfigDao extends BaseDao<OntoConfig, Long> {

    /**
     * Find the ontology configuration based on the URI
     *
     * @param uri the uri of the ontology to find
     * @return the ontology configuration which match to the URI
     */
    Optional<OntoConfig> findByUri(String uri);

    /**
     * Find the ontology configuration based on the PID.
     *
     * @param pid the PID
     * @return the ontology configuration which match to the URI
     */
    Optional<OntoConfig> findByPid(String pid);
}
