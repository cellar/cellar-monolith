/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect
 *             FILE : RedirectDisseminationResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Mar 31, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.mimeType.MimeTypeMapping;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cl.service.client.MimeTypeCacheService;
import eu.europa.ec.opoce.cellar.cmr.service.IMementoService;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import eu.europa.ec.opoce.cellar.common.http.headers.HttpHeadersBuilder;
import eu.europa.ec.opoce.cellar.common.http.headers.directives.CacheControl;
import eu.europa.ec.opoce.cellar.common.http.headers.directives.Tcn;
import eu.europa.ec.opoce.cellar.common.util.CellarIdUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.server.dissemination.AcceptLanguage;
import eu.europa.ec.opoce.cellar.server.dissemination.converter.CellarResourceListHttpMessage;
import eu.europa.ec.opoce.cellar.server.dissemination.converter.CellarResourceListHttpMessageElement;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.DisseminationRequest;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.redirect.service.ISeeOtherService;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.IDisseminationResolver;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.ResourceRedirectAbstractResolver;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.content.ContentStreamRedirectResolver;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.contentPool.ContentPoolListRedirectResolver;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.contentPool.ContentPoolZipRedirectResolver;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.identifiers.IdentifiersRedirectResolver;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.rdf.RdfRedirectResolver;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.version.VersionRedirectResolver;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.xml.XmlRedirectResolver;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;
import eu.europa.ec.opoce.cellar.server.service.DisseminationException;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page. <br/>
 * <br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc. <br/>
 * <br/>
 * ON : Mar 31, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Configurable
public abstract class RedirectResolver extends ResourceRedirectAbstractResolver {

    protected static final Logger LOG = LogManager.getLogger(RedirectResolver.class);

    /**
     * The see other service.
     */
    @Autowired
    protected ISeeOtherService seeOtherService;

    /**
     * The mime type cache service.
     */
    @Autowired
    private MimeTypeCacheService mimeTypeCacheService;

    /**
     * The identifier service.
     */
    protected static IdentifierService identifierService = ServiceLocator.getService("pidManagerService", IdentifierService.class);

    /**
     * The cellar configuration.
     */
    protected static ICellarConfiguration cellarConfiguration = ServiceLocator.getService("cellarConfiguration",
            ICellarConfiguration.class);

    /**
     * The memento service.
     */
    protected static IMementoService mementoService = ServiceLocator.getService("mementoService", IMementoService.class);

    /**
     * The accept language.
     */
    protected final AcceptLanguage acceptLanguage;

    /**
     * The mime type mapping list
     */
    private List<MimeTypeMapping> mimeTypeMappingList;

    /**
     * The accept date time.
     */
    // Accept-DateTime header passed in the request (optional)
    protected final Date acceptDateTime;

    /**
     * The rel.
     */
    // Rel request parameter (optional)
    protected final String rel;

    /**
     * The link.
     */
    // Link header passed in the request (optional)
    protected String link;

    /**
     * The provide alternates.
     */
    // Indication on whether or not alternates can be provided
    protected final boolean provideAlternates;

    /**
     * Partial constructor, without Accept-DateTime support.
     *
     * @param cellarResource    the cellar resource
     * @param acceptLanguage    the accept language
     * @param provideAlternates the provide alternates
     */
    protected RedirectResolver(final CellarResourceBean cellarResource, final AcceptLanguage acceptLanguage,
                               final boolean provideAlternates) {
        this(cellarResource, acceptLanguage, null, null, null, provideAlternates);
    }

    /**
     * Partial constructor, with Accept-DateTime support.
     *
     * @param cellarResource    the cellar resource
     * @param acceptLanguage    the accept language
     * @param acceptDateTime    the accept date time
     * @param rel               the rel
     * @param link              the link
     * @param provideAlternates the provide alternates
     */
    protected RedirectResolver(final CellarResourceBean cellarResource, final AcceptLanguage acceptLanguage, final Date acceptDateTime,
                               final String rel, final String link, final boolean provideAlternates) {
        this.cellarResource = cellarResource;
        this.acceptLanguage = acceptLanguage;
        this.acceptDateTime = acceptDateTime;
        this.rel = rel;
        this.link = link;
        this.provideAlternates = provideAlternates;
    }

    /**
     * Retrieves an appropriate dissemination resolver for the requested
     * resource type.
     *
     * @param cellarResource       the requested resource
     * @param decoding             the specified decoding
     * @param disseminationRequest the provided dissemination request
     * @param accept               the value parsed from the accept header
     * @param acceptLanguage       the accept language
     * @param acceptDateTime       the value of the accept date-time header field
     * @param rel                  the rel request parameter
     * @param provideAlternates    whether or not alternates should be provided
     * @return a {@code IDisseminationResolver} matching the {@code Type} of the
     * requested resource
     */
    public static IDisseminationResolver get(final CellarResourceBean cellarResource, final String decoding,
                                             final DisseminationRequest disseminationRequest, final String accept, final AcceptLanguage acceptLanguage,
                                             final Date acceptDateTime, final String rel, final boolean provideAlternates) {
        IDisseminationResolver result = null;
        if (cellarConfiguration.isCellarServiceDisseminationMementoEnabled()) {
            // Memento resolution is only triggered if the related property has been
            // set in Cellar's configuration
            // If the resource is capable of date-time negotiation AND is
            // Memento-related, the Memento-specific resolver is instantiated

            final String uri = identifierService.getUri(cellarResource.getCellarId());
            if (null != uri) {
                if (mementoService.isEvolutiveWork(uri)) {
                    result = VersionRedirectResolver.get(cellarResource, uri, disseminationRequest, accept, decoding, acceptLanguage,
                            acceptDateTime, rel, provideAlternates);
                }
            }
        }
        if (result == null) {
            result = getInstanceWithoutMemento(cellarResource, decoding, disseminationRequest, accept, acceptLanguage, acceptDateTime, rel,
                    provideAlternates);
        }

        return result;
    }

    /**
     * Gets the instance without memento.
     *
     * @param cellarResource       the cellar resource
     * @param decoding             the decoding
     * @param disseminationRequest the dissemination request
     * @param accept               the accept
     * @param acceptLanguage       the accept language
     * @param acceptDateTime       the accept date time
     * @param rel                  the rel
     * @param provideAlternates    the provide alternates
     * @return the instance without memento
     */
    protected static IDisseminationResolver getInstanceWithoutMemento(final CellarResourceBean cellarResource, final String decoding,
                                                                      final DisseminationRequest disseminationRequest, final String accept, final AcceptLanguage acceptLanguage,
                                                                      final Date acceptDateTime, final String rel, final boolean provideAlternates) {

        // Otherwise, proceed normally
        switch (disseminationRequest.getType(cellarResource)) {
            case XML:
                return XmlRedirectResolver.get(cellarResource, decoding, disseminationRequest, acceptLanguage, provideAlternates);
            case RDF:
                return RdfRedirectResolver.get(cellarResource, disseminationRequest, acceptLanguage, acceptDateTime, rel, provideAlternates);
            case LIST:
                return ContentPoolListRedirectResolver.get(cellarResource, disseminationRequest, accept, acceptLanguage, provideAlternates);
            case ZIP:
                return ContentPoolZipRedirectResolver.get(cellarResource, disseminationRequest, accept, acceptLanguage, provideAlternates);
            case IDENTIFIERS:
                return IdentifiersRedirectResolver.get(cellarResource);
            default:
            case CONTENT_STREAM:
                return ContentStreamRedirectResolver.get(cellarResource, accept, acceptLanguage, provideAlternates);
        }
    }

    /**
     * Checks for accept language bean.
     *
     * @return true, if successful
     */
    protected boolean hasAcceptLanguageBean() {
        return (this.acceptLanguageBeans != null) && !this.acceptLanguageBeans.isEmpty();
    }

    /**
     * Checks for no accept language bean.
     *
     * @return true, if successful
     */
    private boolean hasNoAcceptLanguageBean() {
        return (this.acceptLanguageBeans == null) || this.acceptLanguageBeans.isEmpty();
    }

    /**
     * Throw error if has no accept language bean.
     */
    protected void throwErrorIfHasNoAcceptLanguageBean() {
        if (this.hasNoAcceptLanguageBean()) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_ACCEPTABLE)
                    .withMessage("Invalid content type {} for {} ['{}'] without language")
                    .withMessageArgs(this.getTypeStructure().getErrorLabel(), cellarResource.getCellarType(), cellarResource.getCellarId())
                    .build();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void initDisseminationRequest() {
        this.acceptLanguageBeans = this.buildAcceptLanguageBeans();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResponseEntity<?> handleDisseminationRequest() {
        ResponseEntity<?> result;
        this.initDisseminationRequest();
        switch (cellarResource.getCellarType()) {
            case WORK:
                this.doHandleWorkDisseminationRequest();
                break;
            case AGENT:
                this.doHandleAgentDisseminationRequest();
                break;
            case TOPLEVELEVENT:
                this.doHandleTopLevelEventDisseminationRequest();
                break;
            case DOSSIER:
                this.doHandleDossierDisseminationRequest();
                break;
            case EXPRESSION:
                this.doHandleExpressionDisseminationRequest();
                break;
            case EVENT:
                this.doHandleEventDisseminationRequest();
                break;
            case MANIFESTATION:
                this.doHandleManifestationDisseminationRequest();
                break;
            case ITEM:
                this.doHandleItemDisseminationRequest();
                break;
            // should never get here!
            case UNDEFINED:
            default:
        }
        if (shouldReturnMultipleChoiceForExpression()) {
            result = getMultipleChoiceResponse();
        } else {
            result = this.doHandleDisseminationRequest();
        }
        return result;
    }

    /**
     * Do handle work dissemination request.
     */
    protected void doHandleWorkDisseminationRequest() {
        this.throwDefaultNotAcceptable();
    }

    /**
     * Do handle agent dissemination request.
     */
    protected void doHandleAgentDisseminationRequest() {
        this.throwDefaultNotAcceptable();
    }

    /**
     * Do handle top level event dissemination request.
     */
    protected void doHandleTopLevelEventDisseminationRequest() {
        this.throwDefaultNotAcceptable();
    }

    /**
     * Do handle dossier dissemination request.
     */
    protected void doHandleDossierDisseminationRequest() {
        this.throwDefaultNotAcceptable();
    }

    /**
     * Do handle expression dissemination request.
     */
    protected void doHandleExpressionDisseminationRequest() {
        this.throwDefaultNotAcceptable();
    }

    /**
     * Do handle event dissemination request.
     */
    protected void doHandleEventDisseminationRequest() {
        this.throwDefaultNotAcceptable();
    }

    /**
     * Do handle manifestation dissemination request.
     */
    protected void doHandleManifestationDisseminationRequest() {
        this.throwDefaultNotAcceptable();
    }

    /**
     * Do handle item dissemination request.
     */
    protected void doHandleItemDisseminationRequest() {
        this.throwDefaultNotAcceptable();
    }

    /**
     * Throw default not acceptable.
     */
    private void throwDefaultNotAcceptable() {
        throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_ACCEPTABLE)
                .withMessage("Invalid content type {} for {} ['{}'].")
                .withMessageArgs(this.getTypeStructure().getErrorLabel(), cellarResource.getCellarType(), cellarResource.getCellarId())
                .build();
    }

    // the dissemination requests are split into multiple requests
    // [# number of requests] = Accept[#headers] x Accept-Language[#headers]
    // but for dissemination requests involving expression and manifestations

    /**
     * Builds the accept language beans.
     *
     * @return the list
     */
    // this list can be enlarged
    private List<LanguageBean> buildAcceptLanguageBeans() {
        List<LanguageBean> result = null;
        if (this.acceptLanguage != null) {
            result = new ArrayList<>();
            final String languageCode = this.acceptLanguage.getLanguageCode();
            final LanguageBean language = this.disseminationService.parseLanguageBean(languageCode);
            if (language != null) {
                result.add(language);
            }
        }
        return result;
    }

    /**
     * Sets the manifestation type.
     *
     * @param disseminationRequest the dissemination request
     * @param accept               the accept
     */
    protected void setMimeTypeMappings(final DisseminationRequest disseminationRequest, final String accept) {
        final Matcher matcher = disseminationRequest.getPattern().matcher(accept);

        if (matcher.matches()) {
            if (matcher.groupCount() > 0) {
                final String manifestationType = matcher.group(1);
                final Map<String, List<MimeTypeMapping>> manifestationTypesMap = mimeTypeCacheService.getManifestationTypesMap();

                if (manifestationTypesMap.containsKey(manifestationType)) {
                    this.mimeTypeMappingList = manifestationTypesMap.get(manifestationType);
                } else {
                    this.mimeTypeMappingList = new ArrayList<>();
                }
            }
        }

        final DigitalObjectType sourceResourceType = cellarResource.getCellarType();
        if ((this.mimeTypeMappingList.isEmpty()) && (sourceResourceType != DigitalObjectType.MANIFESTATION)) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage("Missing mtype token for {} resource ['{}']")
                    .withMessageArgs(sourceResourceType, cellarResource.getCellarId()).build();
        }
    }

    /**
     * If a matching manifestation type is found for a manifestation, the manifestation is assigned to the field cellarResource
     * otherwise a DisseminationException with HTTP 404 is thrown.
     */
    protected void retrieveTargetManifestaion() {
        final List<String> manifestationTypes = mimeTypeMappingList.stream().map(MimeTypeMapping::getManifestationType)
                .collect(Collectors.toList());
        for (final MimeTypeMapping mimeTypeMapping : mimeTypeMappingList) {
            final String manifestationType = mimeTypeMapping.getManifestationType();
            for (final CellarResourceBean manifestation : this.disseminationService.checkForChildManifestations(cellarResource)) {
                if (StringUtils.equals(manifestation.getMimeType(), manifestationType)) {
                    cellarResource = manifestation;
                    return;
                }
            }
        }

        throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                .withMessage("Not found manifestation ['{}'] matching manifestation type [{}]")
                .withMessageArgs(cellarResource.getCellarId(), manifestationTypes).build();
    }

    /**
     * Checks the manifestation type of manifestation.
     *
     * @param manifestation the manifestation
     */
    protected void checkManifestationTypeOfManifestation(final CellarResourceBean manifestation) {
        // CellarResourceBean has:
        // - a manifestation type if it is a manifestation
        // - a mime type if it is an item (CMR_CELLAR_RESOURCE_MD)
        final String manifestationType = manifestation.getMimeType();
        String requestedManifestationType = null;
        for (final MimeTypeMapping mimeTypeMapping : this.mimeTypeMappingList) {
            requestedManifestationType = mimeTypeMapping.getManifestationType();
            if (StringUtils.isNotBlank(requestedManifestationType)) {
                break;
            }
        }

        if (requestedManifestationType != null && !StringUtils.equals(manifestationType, requestedManifestationType)) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage("Cellar resource of type Manifestation '{}' with invalid mtype token: '{}'")
                    .withMessageArgs(manifestation.getCellarId(), requestedManifestationType).build();
        }
    }

    /**
     * Builds the expression list http message.
     *
     * @param expressionListHttpMessageElements the expression list http message elements
     * @return the expression list http message
     */
    private CellarResourceListHttpMessage buildExpressionListHttpMessage(
            final List<CellarResourceListHttpMessageElement> expressionListHttpMessageElements) {
        CellarResourceListHttpMessage message = new CellarResourceListHttpMessage();
        message.setExpressions(expressionListHttpMessageElements);
        return message;
    }

    /**
     * Builds the expression list http message element.
     *
     * @param expression the expression
     * @return the expression list http message element
     */
    private CellarResourceListHttpMessageElement buildExpressionListHttpMessageElement(final CellarResourceBean expression) {
        CellarResourceListHttpMessageElement message = new CellarResourceListHttpMessageElement();
        message.setCellarId(expression.getCellarId());
        message.setUri(this.disseminationDbGateway.getCellarIdentifiedObject(expression.getCellarId()).getCellarId().getUri());
        message.setUsedLanguages(getLanguageUris(expression));
        return message;
    }

    private ResponseEntity<CellarResourceListHttpMessage> getMultipleChoiceResponse() {
        final HttpStatus httpStatus = HttpStatus.MULTIPLE_CHOICES;
        final HttpHeadersBuilder httpHeadersBuilder = HttpHeadersBuilder.get().withCacheControl(CacheControl.NO_STORE).withDateNow();
        httpHeadersBuilder.withTcn(Tcn.CHOICE).withAlternates(getAlternates()).withContentType(MediaType.TEXT_HTML);

        final Comparator<CellarResourceBean> comparator = (o1, o2) -> {
            //first the mono-linguistic or the expression with the less languages
            int result = Integer.compare(o1.getLanguages().size(), o2.getLanguages().size());
            if (result == 0) {
                //if both have the same number of languages sort by expression index
                final Integer thisCellarIdIndex = Integer
                        .parseInt(StringUtils.substringAfterLast(o1.getCellarId(), CellarIdUtils.WEM_SEPARATOR));
                final Integer otherCellarIdIndex = Integer
                        .parseInt(StringUtils.substringAfterLast(o2.getCellarId(), CellarIdUtils.WEM_SEPARATOR));
                result = thisCellarIdIndex.compareTo(otherCellarIdIndex);
            }
            return result;
        };

        final List<CellarResourceListHttpMessageElement> expressionListHttpMessageElements = targetedExpressionsLinkedToAcceptLanguage
                .stream().sorted(comparator).map(this::buildExpressionListHttpMessageElement).collect(Collectors.toList());
        final CellarResourceListHttpMessage buildExpressionListHttpMessage = buildExpressionListHttpMessage(
                expressionListHttpMessageElements);

        return new ResponseEntity<>(buildExpressionListHttpMessage, httpHeadersBuilder.getHttpHeaders(), httpStatus);
    }
}
