/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver
 *             FILE : ResourceRetrieveAbstractResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 26-Jan-2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 26-Jan-2017
 * The Class ResourceRetrieveAbstractResolver.
 *
 * @author ARHS Developments
 * @version $$Revision$$$
 * @param <T> the generic type
 */
public abstract class ResourceRetrieveAbstractResolver extends AbstractResolver {

}
