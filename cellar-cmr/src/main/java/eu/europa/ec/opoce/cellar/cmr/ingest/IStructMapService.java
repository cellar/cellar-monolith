/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.ingest
 *             FILE : IStructMapService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 11, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.ingest;

import eu.europa.ec.opoce.cellar.cl.domain.response.IOperation;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Apr 11, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IStructMapService {

    void execute(final CalculatedData calculatedData, final IOperation<?> operation);

}
