package eu.europa.ec.opoce.cellar.s3;

import com.amazonaws.services.s3.Headers;
import com.google.common.base.Charsets;
import com.google.common.base.Strings;
import com.google.common.collect.ImmutableMap;
import com.google.common.io.CharStreams;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CmrItemTechnicalMdRepository;
import eu.europa.ec.opoce.cellar.cmr.database.dao.NalConfigDao;
import eu.europa.ec.opoce.cellar.cmr.database.dao.OntoConfigDao;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.feed.util.ContentStreamServiceBridge;
import eu.europa.ec.opoce.cellar.nal.dao.NalConfig;
import eu.europa.ec.opoce.cellar.ontology.dao.OntoConfig;
import eu.europa.ec.opoce.cellar.s3.domain.ContentVersion;
import eu.europa.ec.opoce.cellar.s3.repository.ContentRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UncheckedIOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Supplier;

import static eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration.INGESTION;
import static eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration.INGESTION_MD_UPDATE;
import static eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService.PREFIX_CELLAR;
import static eu.europa.ec.opoce.cellar.nal.NalOntoUtils.NAL_PREFIX;
import static eu.europa.ec.opoce.cellar.nal.NalOntoUtils.NAL_PREFIX_LEGACY;
import static eu.europa.ec.opoce.cellar.nal.NalOntoUtils.ONTO_PREFIX;
import static eu.europa.ec.opoce.cellar.nal.NalOntoUtils.ONTO_PREFIX_LEGACY;
import static eu.europa.ec.opoce.cellar.nal.NalOntoUtils.SPARQL_LOAD_PREFIX;
import static java.util.Objects.requireNonNull;

@Service
public class S3ContentStreamService implements ContentStreamService, ContentStreamServiceBridge {

    private static final Logger LOG = LogManager.getLogger(S3ContentStreamService.class);

    public static final String CONTENT_STREAM_CACHE = "content-stream-cache";

    private final ContentRepository contentRepository;
    private final CellarResourceDao cellarResourceDao;
    private final NalConfigDao nalConfigDao;
    private final OntoConfigDao ontoConfigDao;

    private final CmrItemTechnicalMdRepository cmrItemTechnicalMdRepository;

    private final ApplicationEventPublisher eventPublisher;
    private final ICellarConfiguration cfg;
    private final CacheManager cacheManager;
    private final ImmutableMap<String, String> rollbackTags;

    @Autowired
    public S3ContentStreamService(ContentRepository contentRepository, CellarResourceDao cellarResourceDao,
                                  NalConfigDao nalConfigDao, OntoConfigDao ontoConfigDao, ApplicationEventPublisher eventPublisher,
                                  @Qualifier("cellarConfiguration") ICellarConfiguration configuration, CacheManager cacheManager,
                                  CmrItemTechnicalMdRepository cmrItemTechnicalMdRepository) {
        this.contentRepository = contentRepository;
        this.cellarResourceDao = cellarResourceDao;
        this.nalConfigDao = nalConfigDao;
        this.ontoConfigDao = ontoConfigDao;
        this.eventPublisher = eventPublisher;
        this.cfg = configuration;
        this.cacheManager = cacheManager;
        this.rollbackTags = cfg.getS3RollbackTag();
        this.cmrItemTechnicalMdRepository=cmrItemTechnicalMdRepository;
    }

    @Override
    @Transactional
    public Optional<InputStream> getContent(String cellarID, ContentType contentType) {
        return getContent(cellarID, null, contentType);
    }

    @Override
    @Transactional
    public Optional<InputStream> getContent(String cellarID, String version, ContentType contentType) {
        return contentRepository.getContent(cellarID, version, contentType);
    }

    @Override
    @Transactional
    @Cacheable(value = CONTENT_STREAM_CACHE, unless = "#result == null", key = "{ #cellarID, #contentType }")
    public Optional<String> getContentAsString(String cellarID, ContentType contentType) {
        return getContentAsString(cellarID, null, contentType);
    }

    @Override
    @Transactional
    @Cacheable(value = CONTENT_STREAM_CACHE, unless = "#result == null", key = "{ #cellarID, #version, #contentType }")
    public Optional<String> getContentAsString(String cellarID, String version, ContentType contentType) {
        return contentRepository.getContent(cellarID, version, checkType(contentType))
                .map(S3ContentStreamService::asString);
    }

    @Override
    public Optional<String> getLargeContentAsString(String cellarID, String version, ContentType contentType) {
        return contentRepository.getLargeContent(cellarID, version, checkType(contentType));
    }

    @Override
    public EnumMap<ContentType, String> getContentsAsString(String cellarID, Map<ContentType, String> types) {
        EnumMap<ContentType, String> results = new EnumMap<>(ContentType.class);
        for (Map.Entry<ContentType, String> type : types.entrySet()) {
            results.put(type.getKey(), contentRepository.getContent(cellarID, type.getValue(), type.getKey())
                    .map(S3ContentStreamService::asString)
                    .orElse(null));
        }
        return results;
    }

    /**
     * {@inheritDoc}
     * Update the {@link CellarResource} in the relational database
     * which contains the ID of the latest version of the object stored
     * in S3. If for some reasons an exception is thrown by the method,
     * the transaction in the relational database is rollback and the
     * method {@link #rollBackUpdate(ContentStreamUpdatedEvent)}
     * is executed to delete the object in S3.
     * 1. Retrieve from Oracle the CellarResource based on the cellar ID
     * or created it if none exist.
     * 2. Update S3
     * 3. Update Oracle with the version generated by S3.
     *
     * @param id          the ID of the object to update
     * @param content     the content to store in S3
     * @param contentType the type of document to update ({@link ContentType})
     */
    @Override
    @Transactional
    @CacheEvict(value = CONTENT_STREAM_CACHE, key = "{ #id, #contentType }")
    public String writeContent(String id, Resource content, ContentType contentType) {
        return writeContent(id, content, contentType, Collections.emptyMap(), null);
    }

    @Override
    @Transactional
    @CacheEvict(value = CONTENT_STREAM_CACHE, key = "{ #id, #contentType }")
    public String writeContent(String id, Resource content, ContentType contentType, Supplier<CellarResource> resourceSupplier) {
        return writeContent(id, content, contentType, Collections.emptyMap(), resourceSupplier);
    }

    @Override
    @Transactional
    @CacheEvict(value = CONTENT_STREAM_CACHE, key = "{ #id, #contentType }")
    public String writeContent(String id, Resource content, ContentType contentType, Map<String, String> metadata) {
        return writeContent(id, content, contentType, metadata, null);
    }

    @Override
    @Transactional
    @CacheEvict(value = CONTENT_STREAM_CACHE, key = "{ #id, #contentType }")
    public String writeContent(String id, Resource content, ContentType contentType, Map<String, String> metadata,
                               Supplier<CellarResource> resourceSupplier) {
        if (id.startsWith(PREFIX_CELLAR)) {
            return writeResourceContent(id, content, contentType, metadata, resourceSupplier);
        } else if (id.startsWith(NAL_PREFIX) || id.startsWith(NAL_PREFIX_LEGACY)) {
            return writeNalContent(id, content, contentType);
        } else if (id.startsWith(ONTO_PREFIX) || id.startsWith(ONTO_PREFIX_LEGACY)) {
            return writeOntologyContent(id, content, contentType);
        } else if (id.startsWith(SPARQL_LOAD_PREFIX)) {
            return writeSparqlLoadContent(id, content, contentType);
        } else {
            throw new RuntimeException("Unsupported id " + id);
        }
    }

    @Override
    @Transactional
    public EnumMap<ContentType, String> writeContents(String cellarID, Map<ContentType, String> contents, Supplier<CellarResource> resourceSupplier) {
        EnumMap<ContentType, String> versions = new EnumMap<>(ContentType.class);
        if (cellarID.startsWith(PREFIX_CELLAR)) {
            CellarResource resource = Optional.ofNullable(cellarResourceDao.findCellarId(cellarID))
                    .orElseGet(resourceSupplier != null ? resourceSupplier : () -> newCellarResource(cellarID));
            if (LOG.isInfoEnabled(INGESTION_MD_UPDATE)) {
                LOG.info(INGESTION_MD_UPDATE, "Ingestion {} ({}) -> read  (db): {}", cellarID, contents.keySet(), resource.getVersions());
            } else if (LOG.isDebugEnabled(INGESTION_MD_UPDATE)) {
                LOG.debug(INGESTION_MD_UPDATE, "Ingestion {} ({}) -> read  (db): {}", cellarID, contents, resource.getVersions());
            }
            if (LOG.isInfoEnabled(INGESTION_MD_UPDATE)) {
                List<ContentVersion> currentVersions = contentRepository.getVersions(cellarID, ContentType.DIRECT);
                ContentVersion latest = null;
                for (ContentVersion v : currentVersions) {
                    if (v.isLatest()) {
                        latest = v;
                        break;
                    }
                }
                String dbVersion = resource.getVersion(ContentType.DIRECT).orElse(null);
                String latestS3 = latest != null ? latest.getVersion() : null;
                String versionExisting = ThreadContext.get("INGESTION_MD_UPDATE_EXISTING_READ_DIRECT" + cellarID);
                if (dbVersion != null && !dbVersion.equals(latestS3)) {
                    LOG.error(INGESTION_MD_UPDATE, "Ingestion {}, try to update direct.nt by reading {} (db) but latest is {} (S3)",
                            cellarID, dbVersion, latestS3);
                }
                if (dbVersion != null && !dbVersion.equals(versionExisting)) {
                    LOG.warn(INGESTION_MD_UPDATE, "Ingestion {}, try to update direct.nt by reading {} (db) but existing read is {} (db)" +
                            ", S3 latest is {}", cellarID, dbVersion, versionExisting, latestS3);
                }
                LOG.info(INGESTION_MD_UPDATE, "Ingestion {} (latest direct: {}) (read db: {}) (existing db: {})", cellarID,
                        latestS3, dbVersion, versionExisting);
                ThreadContext.remove("INGESTION_MD_UPDATE_EXISTING_READ_DIRECT" + cellarID);
            }
            versions.putAll(contentRepository.putContents(cellarID, contents));
            for (Map.Entry<ContentType, String> version : versions.entrySet()) {
                resource.putVersion(version.getKey(), version.getValue());
                eventPublisher.publishEvent(new ContentStreamUpdatedEvent(cellarID, version.getValue(), version.getKey()));
            }
            cellarResourceDao.saveOrUpdateObject(resource);
        } else {
            throw new UnsupportedOperationException("Bulk update not supported for NALs and Ontologies");
        }

        // Key must be resolved dynamically, not possible by annotation
        // remove from cache AFTER the update of S3
        final Cache cache = cacheManager.getCache(CONTENT_STREAM_CACHE);
        if (cache != null) {
            for (Map.Entry<ContentType, String> content : contents.entrySet()) {
                String key = content.getValue() + " " + content.getKey();
                cache.evict(key);
                LOG.debug(INGESTION, "Remove from cache {} ", key);
            }
        }
        return versions;
    }

    private String writeResourceContent(String id, Resource content, ContentType contentType, Map<String, String> metadata,
                                        Supplier<CellarResource> resourceSupplier) {
        CellarResource resource = Optional.ofNullable(cellarResourceDao.findCellarId(id))
                .orElseGet(resourceSupplier != null ? resourceSupplier : () -> newCellarResource(id));
        String newVersion = updateRepository(id.substring(PREFIX_CELLAR.length()), content, checkType(contentType), metadata);
        resource.putVersion(contentType, newVersion);
        cellarResourceDao.saveOrUpdateObject(resource);
        return newVersion;
    }

    private String writeOntologyContent(String id, Resource content, ContentType contentType) {
        OntoConfig resource = ontoConfigDao.findByPid(id).orElseThrow(() -> new IllegalStateException("Ontology not found in ONTOCONFIG: " + id));
        // replace ":" by "/" for backward compatibility, the key for Fedora was onto:ontology-name
        final String pid = id.replace(":", "/");
        String newVersion = updateRepository(pid, content, checkType(contentType), Collections.emptyMap());
        resource.setVersion(newVersion);
        ontoConfigDao.updateObject(resource);
        return newVersion;
    }

    private String writeSparqlLoadContent(String id, Resource content, ContentType contentType) {
        return updateRepository(id, content, checkType(contentType), Collections.emptyMap());
    }

    private String writeNalContent(String id, Resource content, ContentType contentType) {
        Optional<NalConfig> resource = nalConfigDao.findByPid(id);
        // replace ":" by "/" for backward compatibility, the key for Fedora was nal:nal-name
        final String pid = id.replace(":", "/");
        String newVersion = updateRepository(pid, content, checkType(contentType), Collections.emptyMap());
        resource.ifPresent(r -> {
            r.setVersion(newVersion);
            nalConfigDao.updateObject(r);
        });
        return newVersion;
    }

    /**
     * @param id
     * @param content
     * @param contentType
     * @return the new version of the object (generated by the repository)
     */
    private String updateRepository(String id, Resource content, ContentType contentType, Map<String, String> metadata) {
        final String newVersion = contentRepository.putContent(id, content, checkType(contentType), metadata);
        eventPublisher.publishEvent(new ContentStreamUpdatedEvent(id, newVersion, contentType));
        return newVersion;
    }

    @Override
    @Transactional
    @CacheEvict(value = CONTENT_STREAM_CACHE, key = "{ #cellarID, #contentType }")
    public void deleteContent(String cellarID, ContentType contentType) {
        if (cellarID.startsWith("nal") || cellarID.startsWith("onto") || cellarID.startsWith("sparql-load")) {
            contentRepository.delete(cellarID, checkType(contentType));
        } else {
            CellarResource resource = Optional.ofNullable(cellarResourceDao.findCellarId(cellarID))
                    .orElseThrow(() -> new IllegalStateException("Cellar ID [" + cellarID + "] not found"));
            contentRepository.delete(cellarID, checkType(contentType));
            Optional<String> version = resource.getVersion(contentType);
            version.ifPresent(v -> {
                eventPublisher.publishEvent(new ContentStreamDeletedEvent(cellarID, v, contentType));
            });
        }
    }

    @Override
    @Transactional
    public void deleteContent(String cellarID) {
        CellarResource resource = Optional.ofNullable(cellarResourceDao.findCellarId(cellarID))
                .orElseThrow(() -> new IllegalStateException("Cellar ID [" + cellarID + "] not found"));

        deleteContent(resource);
    }

    @Override
    @Transactional
    public void deleteContent(CellarResource resource) {
        for (Map.Entry<ContentType, String> type : resource.getVersions().entrySet()) {
            contentRepository.delete(resource.getCellarId(), type.getKey()); // TODO AJ Test if null version is enough.
            if (!Strings.isNullOrEmpty(type.getValue())) {
                eventPublisher.publishEvent(new ContentStreamDeletedEvent(resource.getCellarId(), type.getValue(), type.getKey()));
                resource.putVersion(type.getKey(), null);
            }
        }
        cellarResourceDao.deleteObject(resource);
    }

    @Override
    @Transactional
    public void deleteRecursively(String baseCellarID) {
        if (baseCellarID != null && !baseCellarID.isEmpty()) {
            Map<String, String> deleteMarkers = contentRepository.deleteRecursively(baseCellarID);
            eventPublisher.publishEvent(new RecursiveDeleteEvent(baseCellarID, deleteMarkers));
            int count = cellarResourceDao.deleteResourceStartsWith(baseCellarID);
            cmrItemTechnicalMdRepository.deleteAllByCellarIdContaining(baseCellarID);

            if (count != -1) {
                LOG.info("{} deleted from CMR_CELLAR_RESOURCE_MD ({}*)", count, baseCellarID);
            } else {
                // trigger rollback in S3
                throw new IllegalStateException("Cannot delete resources from CMR_CELLAR_RESOURCE_MD (" + baseCellarID + "*)");
            }
        }
        LOG.warn("BaseCellarID is empty, delete cannot be performed.");
    }

    @Override
    public List<ContentVersion> getVersions(String cellarID, ContentType contentType) {
        return contentRepository.getVersions(cellarID, checkType(contentType));
    }

    @Override
    public List<ContentVersion> getVersions(String cellarID, ContentType contentType, int maxResults) {
        return contentRepository.getVersions(cellarID, checkType(contentType), maxResults);
    }

    @Override
    public boolean exists(String cellarID, ContentType contentType) {
        return contentRepository.exists(requireNonNull(cellarID, "Cellar ID is null"), checkType(contentType));
    }

    @Override
    public boolean exists(String resourceID, ContentType contentType, String version) {
        return false;
    }

    @Override
    public Map<String, Object> getMetadata(String id, String version, ContentType contentType) {
        return contentRepository.getContentMetadata(requireNonNull(id, "Cellar ID is null"), version, checkType(contentType));
    }

    @Override
    @Transactional
    public String updateMetadata(String id, String version, ContentType contentType, Map<String, String> userMetadata) {
        String newVersion = contentRepository.updateMetadata(id, version, contentType, userMetadata);
        CellarResource resource = Optional.ofNullable(cellarResourceDao.findCellarId(id))
                .orElseThrow(() -> new IllegalStateException("Cannot update resource metadata " + id + " (" + version + ")"));

        resource.putVersion(contentType, newVersion);
        cellarResourceDao.updateObject(resource);
        return newVersion;
    }

    @Override
    public Date getLastModificationDate(String id, String version, ContentType contentType) {
        return (Date) contentRepository.getContentMetadata(id, version, contentType).get(Headers.LAST_MODIFIED);
    }

    @Override
    public List<String> listKeyStartWith(String prefix) {
        String start = prefix;
        if (prefix.startsWith("cellar:")) {
            start = prefix.replace("cellar:", "");
        }
        return contentRepository.listStartWith(start);
    }

    @Transactional
    public void restore(String cellarID, String version, ContentType contentType) {
        String restoredVersion = contentRepository.restore(cellarID, version, contentType);
        eventPublisher.publishEvent(new ContentStreamUpdatedEvent(cellarID, restoredVersion, contentType));
        CellarResource resource = Optional.ofNullable(cellarResourceDao.findCellarId(cellarID))
                .orElseThrow(() -> new IllegalStateException("Cannot restore a newly created resource (" + cellarID + ")"));

        resource.putVersion(contentType, restoredVersion);
        cellarResourceDao.updateObject(resource);
    }

    /**
     * This method is executed if a rollback of the current transaction occurs.
     * S3 does not support transaction so we need to manually perform operations
     * in S3 and the database to compensate the rollback process in order to prevent
     * inconsistent state of the S3 bucket.
     * This method create a new transaction to update the relational database.
     * <ul>
     *     <li>Create: Insert a delete marker and mark the object with TX_ROLLBACK</li>
     *     <li>Update: Rollback to the previous version the object and mark the object
     *     with TX_ROLLBACK</li>
     * </ul>
     *
     * @param event the event which contains the key and the version of the
     *              object to delete in S3.
     */
    @TransactionalEventListener(phase = TransactionPhase.AFTER_ROLLBACK)
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void rollBackUpdate(ContentStreamUpdatedEvent event) {
        try {
            String newVersion = contentRepository.tag(event.getCellarId(), event.getVersion(),
                    event.getContentType(), rollbackTags);
            LOG.debug("Object {}({}) tagged with {} -> new version: {}", event.getCellarId(),
                    event.getVersion(), rollbackTags, newVersion);
        } catch (Exception e) {
            LOG.warn("Failed tagging {}({})/{} with {}", event.getCellarId(), event.getVersion(),
                    event.getContentType().file(), rollbackTags);
            return;
        }

        final String id = event.getCellarId().startsWith("cellar:") ? event.getCellarId() : "cellar:" + event.getCellarId();
        final CellarResource resource = cellarResourceDao.findCellarId(id);
        if (resource == null) {
            // Relational database has been rollback to a previous version (i.e. record has been deleted)
            contentRepository.delete(event.getCellarId(), event.getContentType());
        } else {
            // UPDATE operation rollback
            LOG.debug("Transaction rollback, read previous version for {}", event.getCellarId());

            Optional<String> version = resource.getVersion(event.getContentType());
            if (!version.isPresent()) { // nothing to do if the version is not null
                String oldVersion = getPreviousVersion(event.getCellarId(), event.getContentType(), event.getVersion());
                resource.putVersion(event.getContentType(), oldVersion);
                cellarResourceDao.saveOrUpdateObject(resource);
            }
        }

    }

    private String getPreviousVersion(String cellarId, ContentType contentType, String currentVersion) {
        List<ContentVersion> versions = contentRepository.getVersions(cellarId, checkType(contentType));
        return versions.stream()
                .filter(c -> !c.isDeleted())
                .filter(c -> !c.getVersion().equals(cellarId))
                .min(Comparator.comparing(ContentVersion::getLastModified))
                .map(c -> {
                    LOG.debug("Previous version found for {} ({}) - {}: {}, ", cellarId, currentVersion, contentType,
                            c.getVersion());
                    return c.getVersion();
                })
                .orElseThrow(() -> {
                    LOG.debug("Current versions [{}]", versions);
                    return new IllegalStateException("No alive version found!");
                });
    }

    private static CellarResource newCellarResource(String cellarID) {
        CellarResource cellarResource = new CellarResource();
        cellarResource.setCellarId(cellarID);
        cellarResource.setCellarType(DigitalObjectType.findType(cellarID));
        cellarResource.setLastModificationDate(new Date());
        return cellarResource;
    }

    private static ContentType checkType(ContentType contentType) {
        return requireNonNull(contentType, "ContentType is null. Must be one of the following value: "
                + Arrays.toString(ContentType.values()));
    }

    private static String asString(InputStream is) {
        try {
            return CharStreams.toString(new InputStreamReader(is, Charsets.UTF_8));
        } catch (IOException e) {
            LOG.error("Cannot read input stream", e);
            throw new UncheckedIOException(e);
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                LOG.warn("Cannot close input stream", e);
            }
        }
    }

}
