package eu.europa.ec.opoce.cellar.cmr.sparql.utils;

import eu.europa.ec.opoce.cellar.cmr.service.impl.RetrieveOJListServiceImpl;
import eu.europa.ec.opoce.cellar.common.Placeholder;
import eu.europa.ec.opoce.cellar.common.util.PlaceHolderUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.datatypes.xsd.XSDDateTime;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.sparql.engine.http.QueryEngineHTTP;

import java.util.*;
import java.util.regex.Pattern;

/**
 * Class containing utility methods for SPARQL queries
 * @author ARHS Developments
 *
 */
public class QueryUtils {

    //URI regular expression
    public static final String URI_PLACEHOLDER_REGEX = "\\$\\{uri\\}";
    //URI pattern, built from the URI regex
    public static final Pattern URI_PLACEHOLDER_PATTERN = Pattern.compile(URI_PLACEHOLDER_REGEX, Pattern.DOTALL);
    //Accept-DateTime regular expression
    public static final String ACCEPT_DATETIME_PLACEHOLDER_REGEX = "\\$\\{accept_datetime.*?\\}";
    //Accept-Datetime pattern, built from the Accept-DateTime regex
    public static final Pattern ACCEPT_DATETIME_PLACEHOLDER_PATTERN = Pattern.compile(ACCEPT_DATETIME_PLACEHOLDER_REGEX, Pattern.DOTALL);
    //Post-fix for Cellar resource IDs
    public static final String CELLAR_RESOURCE_POSTFIX = "resource/cellar/";

    /**
     * Resolves a query.
     * This method will dynamically create a SPARQL query based on a provided template
     * This service is based on a defined beans context that holds the query templates that should be "dynamic" through the use of place-holder values.
     * The following place-holders are accepted:
     * - Strings and Dates, for the direct translation;
     * - A Map, which will reference sub conditions (useful when a condition is not mandatory, it can be replaced by ""). This Map must also contain the 
     *   name of the sub-query (key = "sub_query_name"). When using a map as a place-holder value the name of the parameter in the main query must match
     *   the name of the condition String bean.
     *   
     * @param name the name
     * @param queryString the query template
     * @param placeHoldersMap the place holders map
     * @return the query
     */
    public static String resolveString(final String templateName, final Map<String, String> queryTemplateStringsMap,
            final Map<Pattern, Object> placeHoldersMap) {

        String queryString = queryTemplateStringsMap.get(templateName);
        for (final Map.Entry<Pattern, Object> entry : placeHoldersMap.entrySet()) {
            final Pattern patternToReplace = entry.getKey();
            final Object valueToReplace = placeHoldersMap.get(patternToReplace);
            final List<Placeholder> placeholders = PlaceHolderUtils.getPlaceholders(queryString, patternToReplace);

            if (valueToReplace instanceof Date) {
                // a special format is used to pass formatter patterns e.g.: ${end_date "yyyy-MM-dd"}
                queryString = PlaceHolderUtils.replacePlaceholdersByDate(queryString, placeholders, (Date) valueToReplace);
            } else if (valueToReplace instanceof Map) {
                //a query may have sub-queries
                @SuppressWarnings({
                        "unchecked"})
                final Map<Pattern, Object> conditions = (Map<Pattern, Object>) valueToReplace;
                final String sub_query_name = (String) conditions.get(RetrieveOJListServiceImpl.SUB_QUERY_NAME_PATTERN);
                conditions.remove(RetrieveOJListServiceImpl.SUB_QUERY_NAME_PATTERN);
                String subQueryString = queryTemplateStringsMap.get(sub_query_name);
                for (final Map.Entry<Pattern, Object> subQueriesEntry : conditions.entrySet()) {
                    final Pattern patternToReplaceInSubQuery = subQueriesEntry.getKey();
                    final Object valueToReplaceInSubQuery = conditions.get(patternToReplaceInSubQuery);
                    final List<Placeholder> subQueryPlaceholders = PlaceHolderUtils.getPlaceholders(subQueryString,
                            patternToReplaceInSubQuery);
                    if (valueToReplaceInSubQuery instanceof Date) {
                        // a special format is used to pass formatter patterns e.g.: ${end_date "yyyy-MM-dd"}
                        subQueryString = PlaceHolderUtils.replacePlaceholdersByDate(subQueryString, subQueryPlaceholders,
                                (Date) valueToReplaceInSubQuery);
                    } else {
                        final String valueToReplaceInSubQueryString = (String) valueToReplaceInSubQuery;
                        if (StringUtils.isNotBlank(valueToReplaceInSubQueryString)) {//if the condition is not mandatory it can be replaced by ""
                            subQueryString = PlaceHolderUtils.replacePlaceholders(subQueryString, subQueryPlaceholders,
                                    valueToReplaceInSubQueryString);
                        } else {
                            subQueryString = "";
                        }
                    }
                }
                queryString = PlaceHolderUtils.replacePlaceholders(queryString, placeholders, subQueryString);
            } else {
                queryString = PlaceHolderUtils.replacePlaceholders(queryString, placeholders, (String) valueToReplace);
            }
        }

        return queryString;
    }

    /**
     * Resolves a query.
     * This method will dynamically create a SPARQL query based on a provided template
     * This service is based on a defined beans context that holds the query templates that should be "dynamic" through the use of place-holder values.
     * The following place-holders are accepted:
     * - Strings and Dates, for the direct translation;
     * - A Map, which will reference sub conditions (useful when a condition is not mandatory, it can be replaced by ""). This Map must also contain the
     *   name of the sub-query (key = "sub_query_name"). When using a map as a place-holder value the name of the parameter in the main query must match
     *   the name of the condition String bean.
     *
     * @param name the name
     * @param queryString the query template
     * @param placeHoldersMap the place holders map
     * @return the query
     */
    public static Query resolveQuery(final String templateName, final Map<String, String> queryTemplateStringsMap,
            final Map<Pattern, Object> placeHoldersMap) {
        final Query result = QueryFactory.create(resolveString(templateName, queryTemplateStringsMap, placeHoldersMap), Syntax.syntaxARQ);
        return result;
    }

    /**
     * Executes a provided {@code Query} against a SPARQL end-point
     * @param cellarUriSparqlService the URI of the SPARQL end-point
     * @param query the {@code Query} to execute
     * @return the results of the query execution, as a {@code ResultSet}
     */
    public static ResultSet executeSelectQuery(String cellarUriSparqlService, Query query) {
        try(final QueryEngineHTTP queryEngine = new QueryEngineHTTP(cellarUriSparqlService, query)) {
            return queryEngine.execSelect();
        }
    }

    /**
     * Executes a provided {@code Query} against a SPARQL end-point
     * @param cellarUriSparqlService the URI of the SPARQL end-point
     * @param query the Query to execute, passed as a String
     * @return the results of the query execution, as a {@code ResultSet}
     */
    public static List<QuerySolution> executeSelectQuery(String cellarUriSparqlService, String query) {
        try(final QueryEngineHTTP queryEngine = new QueryEngineHTTP(cellarUriSparqlService, query)) {
            final ArrayList<QuerySolution> querySolutions = new ArrayList<>();
            queryEngine.execSelect().forEachRemaining(querySolutions::add);
            return querySolutions;
        }
    }

    /**
     * Retrieves the date contained within a given RDFNode, as a {@code Date}
     * @param node the RDFNode
     * @return the date contained within the RDFNode, or null if no date was found
     */
    public static Date getDateOrNull(final RDFNode node) {

        if (node != null && node.isLiteral()) {
            final Object obj = node.asLiteral().getValue();
            if (obj instanceof XSDDateTime) {
                final XSDDateTime xsddt = (XSDDateTime) obj;
                return xsddt.asCalendar().getTime();
            }
        }

        return null;
    }

    /**
     * Retrieves the date contained within a given RDFNode, as a {@code XSDDateTime}
     * @param node the RDFNode
     * @return the date contained within the RDFNode, or null if no date was found
     */
    public static XSDDateTime getXSDDateTimeOrNull(final RDFNode node) {

        if (node != null && node.isLiteral()) {
            final Object obj = node.asLiteral().getValue();
            if (obj instanceof XSDDateTime) {
                return (XSDDateTime) obj;
            }
        }
        return null;
    }

    /**
     * Builds a placeholders map (associates a pattern to a given object)
     * @param object the object to associate to a pattern
     * @return the placeholders map
     */
    public static Map<Pattern, Object> preparePlaceHoldersMap(final Object object) {

        final Map<Pattern, Object> placeHoldersMap = new HashMap<Pattern, Object>();
        //Only 2 cases: either a URI is provided as a String, or an Accept-DateTime is provided as a Date
        if (object instanceof String) {
            placeHoldersMap.put(QueryUtils.URI_PLACEHOLDER_PATTERN, object);
        } else {
            placeHoldersMap.put(QueryUtils.ACCEPT_DATETIME_PLACEHOLDER_PATTERN, object);
        }

        return placeHoldersMap;
    }

    /**
     * Builds a placeholders map (associates a pattern to a given object)
     * @param uri the URI to put in the map
     * @param acceptDateTime the accept-dateTime to put in the map
     * @return the placeholders map
     */
    public static Map<Pattern, Object> preparePlaceHoldersMap(final String uri, final Date acceptDateTime) {

        final Map<Pattern, Object> placeHoldersMap = new HashMap<Pattern, Object>();
        placeHoldersMap.put(QueryUtils.URI_PLACEHOLDER_PATTERN, uri);
        placeHoldersMap.put(QueryUtils.ACCEPT_DATETIME_PLACEHOLDER_PATTERN, acceptDateTime);

        return placeHoldersMap;
    }
}
