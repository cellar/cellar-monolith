/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums
 *             FILE : AcceptNegotiationRequest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : May 8, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums;

import eu.europa.ec.opoce.cellar.server.dissemination.AcceptLanguage;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.param.DisseminationRequestParameter;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : May 8, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class AcceptNegotiationRequest {

    private final AcceptLanguage acceptLanguage;

    private final DisseminationRequestParameter disseminationRequestParameter;

    public AcceptNegotiationRequest(final AcceptLanguage acceptLanguage,
            final DisseminationRequestParameter disseminationRequestParameter) {
        this.acceptLanguage = acceptLanguage;
        this.disseminationRequestParameter = disseminationRequestParameter;
    }

    /**
     * @return the acceptLanguage
     */
    public AcceptLanguage getAcceptLanguage() {
        return acceptLanguage;
    }

    /**
     * @return the disseminationRequestParameter
     */
    public DisseminationRequestParameter getDisseminationRequestParameter() {
        return disseminationRequestParameter;
    }

}
