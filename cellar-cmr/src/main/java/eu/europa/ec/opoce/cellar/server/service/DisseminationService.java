/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.service
 *             FILE : DisseminationService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 27, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$$
 *          VERSION : $LastChangedRevision$$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.service;

import com.github.rutledgepaulv.prune.Tree;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import org.apache.jena.rdf.model.Model;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement;
import eu.europa.ec.opoce.cellar.common.util.Pair;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.server.dissemination.converter.DisseminationContentStreams;
import org.springframework.http.ResponseEntity;
import org.w3c.dom.Document;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * <p>DisseminationService interface.</p>
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface DisseminationService {

    /**
     * <p>identifierNotice.</p>
     *
     * @param uris a {@link java.lang.String} object.
     * @return a {@link org.springframework.http.ResponseEntity} object.
     */
    ResponseEntity<Document> identifierNotice(final String uris);

    /**
     * <p>isUnderEmbargo.</p>
     *
     * @param cellarResourceBean a {@link eu.europa.ec.opoce.cellar.server.service.CellarResourceBean} object.
     * @return a boolean.
     */
    boolean isUnderEmbargo(final CellarResourceBean cellarResourceBean);

    /**
     * <p>checkForCellarResource.</p>
     *
     * @param ps   a {@link java.lang.String} object.
     * @param id   a {@link java.lang.String} object.
     * @return a {@link eu.europa.ec.opoce.cellar.server.service.CellarResourceBean} object.
     */
    CellarResourceBean checkForCellarResource(final String ps, final String id);

    /**
     * <p>checkForCellarResource.</p>
     *
     * @param psId   a {@link java.lang.String} object.
     * @return a {@link eu.europa.ec.opoce.cellar.server.service.CellarResourceBean} object.
     */
    CellarResourceBean checkForCellarResource(final String psId);

    /**
     * <p>checkForChildExpression.</p>
     *
     * @param cellarResourceBean a {@link eu.europa.ec.opoce.cellar.server.service.CellarResourceBean} object.
     * @param language           a {@link eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean} object.
     * @return a {@link eu.europa.ec.opoce.cellar.server.service.CellarResourceBean} object.
     */
    List<CellarResourceBean> checkForChildExpression(final CellarResourceBean cellarResourceBean, final LanguageBean language);

    /**
     * <p>checkForParentExpression.</p>
     *
     * @param cellarResourceBean a {@link eu.europa.ec.opoce.cellar.server.service.CellarResourceBean} object.
     * @return a {@link eu.europa.ec.opoce.cellar.server.service.CellarResourceBean} object.
     */
    CellarResourceBean checkForParentExpression(final CellarResourceBean cellarResourceBean);

    /**
     * <p>checkForChildManifestations.</p>
     *
     * @param cellarResourceBean a {@link eu.europa.ec.opoce.cellar.server.service.CellarResourceBean} object.
     * @return a {@link java.util.List} object.
     */
    List<CellarResourceBean> checkForChildManifestations(final CellarResourceBean cellarResourceBean);

    /**
     * <p>parseLanguageBean.</p>
     *
     * @param language a {@link java.lang.String} object.
     * @return a {@link eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean} object.
     */
    LanguageBean parseLanguageBean(final String language);

    /**
     * <p>doXmlTreeRequest.</p>
     *
     * @param eTag                  a {@link java.lang.String} object.
     * @param lastModified the last modified
     * @param cellarResourceBean    a {@link eu.europa.ec.opoce.cellar.server.service.CellarResourceBean} object.
     * @param decodingLanguage      a {@link eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean} object.
     * @param filter                a {@link boolean} value.
     * @param provideResponseBody   a {@link boolean} value.
     * @return a {@link org.springframework.http.ResponseEntity} object.
     */
    ResponseEntity<Document> doXmlTreeRequest(final String eTag, final String lastModified, final CellarResourceBean cellarResourceBean,
            final LanguageBean decodingLanguage, final boolean filter, final boolean provideResponseBody);

    /**
     * <p>doXmlBranchRequest.</p>
     *
     * @param eTag                  a {@link java.lang.String} object.
     * @param lastModified the last modified
     * @param cellarResourceBean    a {@link eu.europa.ec.opoce.cellar.server.service.CellarResourceBean} object.
     * @param decodingLanguage      a {@link eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean} object.
     * @param contentLanguages the content languages
     * @param filter                a {@link boolean} value.
     * @param provideResponseBody   a {@link boolean} value.
     * @return a {@link org.springframework.http.ResponseEntity} object.
     */
    ResponseEntity<Document> doXmlBranchRequest(final String eTag, final String lastModified, final CellarResourceBean cellarResourceBean,
            final LanguageBean decodingLanguage, final List<LanguageBean> contentLanguages, final boolean filter,
            final boolean provideResponseBody);

    /**
     * <p>doXmlObjectRequest.</p>
     *
     * @param eTag                  a {@link java.lang.String} object.
     * @param lastModified the last modified
     * @param cellarResourceBean    a {@link eu.europa.ec.opoce.cellar.server.service.CellarResourceBean} object.
     * @param decodingLanguage      a {@link eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean} object.
     * @param filter                a {@link boolean} value.
     * @param provideResponseBody   a {@link boolean} value.
     * @return a {@link org.springframework.http.ResponseEntity} object.
     */
    ResponseEntity<Document> doXmlObjectRequest(final String eTag, final String lastModified, final CellarResourceBean cellarResourceBean,
            final LanguageBean decodingLanguage, final boolean filter, final boolean provideResponseBody);

    /**
     * <p>doTypeRequest.</p>
     *
     * @param cellarResourceBean a {@link eu.europa.ec.opoce.cellar.server.service.CellarResourceBean} object.
     * @param acceptTypes        a {@link java.util.List} object.
     * @param contentLanguages   a {@link java.util.List} object.
     * @param provideAlternates the provide alternates
     * @return a {@link org.springframework.http.ResponseEntity} object.
     */
    ResponseEntity<DisseminationContentStreams> doTypeRequest(final CellarResourceBean cellarResourceBean, final List<String> acceptTypes,
            final List<LanguageBean> contentLanguages, final boolean provideAlternates);

    /**
     * <p>doRdfObjectRequest.</p>
     *
     * @param eTag               a {@link java.lang.String} object.
     * @param lastModified the last modified
     * @param cellarResourceBean a {@link eu.europa.ec.opoce.cellar.server.service.CellarResourceBean} object.
     * @param normalized if the notice should be normalized
     * @return a {@link org.springframework.http.ResponseEntity} object.
     */
    ResponseEntity<Model> doRdfObjectRequest(final String eTag, final String lastModified, final CellarResourceBean cellarResourceBean,boolean normalized);

    /**
     * <p>doRdfTreeRequest.</p>
     *
     * @param eTag                  a {@link java.lang.String} object.
     * @param lastModified the last modified
     * @param cellarResourceBean    a {@link eu.europa.ec.opoce.cellar.server.service.CellarResourceBean} object.
     * @param provideResponseBody   a {@link boolean} value
     * @param normalized if the notice should be normalized
     * @return a {@link org.springframework.http.ResponseEntity} object.
     */
    ResponseEntity<Model> doRdfTreeRequest(final String eTag, final String lastModified, final CellarResourceBean cellarResourceBean,
            final boolean provideResponseBody,final boolean normalized);

    /**
     * <p>doRdfNonInferredObjectRequest.</p>
     *
     * @param eTag               a {@link java.lang.String} object.
     * @param lastModified the last modified
     * @param cellarResourceBean a {@link eu.europa.ec.opoce.cellar.server.service.CellarResourceBean} object.
     * @param normalized if the notice should be normalized
     * @return a {@link org.springframework.http.ResponseEntity} object.
     */
    ResponseEntity<Model> doRdfNonInferredObjectRequest(final String eTag, final String lastModified,
            final CellarResourceBean cellarResourceBean,boolean normalized);

    /**
     * <p>doRdfNonInferredTreeRequest.</p>
     *
     * @param eTag                  a {@link java.lang.String} object.
     * @param lastModified the last modified
     * @param cellarResourceBean    a {@link eu.europa.ec.opoce.cellar.server.service.CellarResourceBean} object.
     * @param provideResponseBody   a {@link boolean} value
     * @param normalized if the notice should be normalized
     * @return a {@link org.springframework.http.ResponseEntity} object.
     */
    ResponseEntity<Model> doRdfNonInferredTreeRequest(final String eTag, final String lastModified,
            final CellarResourceBean cellarResourceBean, final boolean provideResponseBody,final boolean normalized);

    /**
     * <p>doListContentRequest.</p>
     *
     * @param eTag               a {@link java.lang.String} object.
     * @param lastModified the last modified
     * @param cellarResourceBean a {@link eu.europa.ec.opoce.cellar.server.service.CellarResourceBean} object.
     * @return a tree of content stream labels along with their URIs.
     */
    ResponseEntity<Tree<Pair<String, String>>> doListContentRequest(final String eTag, final String lastModified,
                                                                    final CellarResourceBean cellarResourceBean);

    /**
     * <p>doZipContentRequest.</p>
     *
     * @param eTag               a {@link java.lang.String} object.
     * @param lastModified the last modified
     * @param cellarResourceBean a {@link eu.europa.ec.opoce.cellar.server.service.CellarResourceBean} object.
     * @return a {@link org.springframework.http.ResponseEntity} object.
     */
    ResponseEntity<File> doZipContentRequest(final String eTag, final String lastModified, final CellarResourceBean cellarResourceBean);

    /**
     * <p>doIdentifiersRequest.</p>
     *
     * @param eTag               a {@link java.lang.String} object.
     * @param lastModified the last modified
     * @param cellarResourceBean a {@link eu.europa.ec.opoce.cellar.server.service.CellarResourceBean} object.
     * @return a {@link org.springframework.http.ResponseEntity} object.
     */
    ResponseEntity<Document> doIdentifiersRequest(final String eTag, final String lastModified,
            final CellarResourceBean cellarResourceBean);

    /**
     * <p>getManifestationContents.</p>
     *
     * @param cellarResourceBean    a {@link eu.europa.ec.opoce.cellar.server.service.CellarResourceBean} object.
     * @return a {@link java.util.Collection} of {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement}s
     */
    Collection<MetsElement> getManifestationContents(final CellarResourceBean cellarResourceBean);

    /**
     * Do get Sparql.
     *
     * @param id the id
     * @param uri the uri
     * @return the response entity
     */
    ResponseEntity<?> doGetSparql(final String id, final String uri);

    /**
     * Retrieve complete technical metadata (TMD + SAMEAS).
     * @param cellarId cellar identifier
     * @param versions the versions of each file in S3 (retrieved from Oracle)
     * @return complete technical metadata model
     */
    Model getCompleteTechnicalMetaData(String cellarId, Map<ContentType, String> versions);

    /**
     * Resolve cellar resource bean.
     *
     * @param prefix the prefix
     * @param identifier the identifier
     * @return the cellar resource bean
     */
    CellarResourceBean resolveCellarResourceBean(final String prefix, final String identifier);
}
