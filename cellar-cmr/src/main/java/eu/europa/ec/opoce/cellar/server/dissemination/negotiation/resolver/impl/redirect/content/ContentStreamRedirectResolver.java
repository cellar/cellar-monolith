/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.content
 *             FILE : ContentStreamRedirectResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 10, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.content;

import eu.europa.ec.opoce.cellar.common.http.headers.IURLTokenable;
import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.server.dissemination.AcceptLanguage;
import eu.europa.ec.opoce.cellar.server.dissemination.DisseminationRequestUtils;
import eu.europa.ec.opoce.cellar.server.dissemination.converter.DisseminationContentStreams;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.Type;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.IDisseminationResolver;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.RedirectResolver;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;
import eu.europa.ec.opoce.cellar.server.service.DisseminationException;

import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Apr 10, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class ContentStreamRedirectResolver extends RedirectResolver {

    /** The accept. */
    protected final String accept;

    /** The mime types. */
    protected List<String> mimeTypes;

    /**
     * Instantiates a new content stream redirect resolver.
     *
     * @param cellarResource the cellar resource
     * @param accept the accept
     * @param acceptLanguage the accept language
     * @param provideAlternates the provide alternates
     */
    protected ContentStreamRedirectResolver(final CellarResourceBean cellarResource, final String accept,
            final AcceptLanguage acceptLanguage, final boolean provideAlternates) {
        super(cellarResource, acceptLanguage, provideAlternates);
        this.accept = accept;
    }

    /**
     * Gets the.
     *
     * @param cellarResource the cellar resource
     * @param accept the accept
     * @param acceptLanguage the accept language
     * @param provideAlternates the provide alternates
     * @return the i dissemination resolver
     */
    public static IDisseminationResolver get(final CellarResourceBean cellarResource, final String accept,
            final AcceptLanguage acceptLanguage, final boolean provideAlternates) {
        return new ContentStreamRedirectResolver(cellarResource, accept, acceptLanguage, provideAlternates);
    }

    /** {@inheritDoc} */
    @Override
    protected void initDisseminationRequest() {
        super.initDisseminationRequest();
        List<MediaType> acceptTypes = DisseminationRequestUtils.parseAcceptHeader(this.accept);
        if (acceptTypes.isEmpty()) {
            acceptTypes = Collections.singletonList(DisseminationRequestUtils.DEFAULT_CONTENT_MEDIATYPE);
        }

        this.mimeTypes = DisseminationRequestUtils.filterRdfContentStreamAcceptHeader(DisseminationRequestUtils.toListOfStrings(acceptTypes));
    }

    /*
     * (non-Javadoc)
     * 
     * @see eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.AbstractResolver#doHandleDisseminationRequest()
     */
    /** {@inheritDoc} */
    @Override
    protected ResponseEntity<?> doHandleDisseminationRequest() {
        final ResponseEntity<DisseminationContentStreams> datastreams = this.disseminationService.doTypeRequest(this.cellarResource,
                this.mimeTypes, this.acceptLanguageBeans, this.provideAlternates);

        // throw an exception if no datastream has been found
        if (datastreams.getBody().getDisseminationContentStreamDataList().size() == 0) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                    .withMessage("No contentStreamData to content found. [resource '{}'({}) - content types '{}' - content languages '{}']")
                    .withMessageArgs(this.cellarResource.getCellarId(), this.cellarResource.getCellarType(), this.accept,
                            StringUtils.join(this.acceptLanguageBeans, ", "))
                    .build();
        }

        // returns
        return datastreams;
    }

    /** {@inheritDoc} */
    @Override
    protected void doHandleWorkDisseminationRequest() {
        this.throwErrorIfHasNoAcceptLanguageBean();
    }

    /** {@inheritDoc} */
    @Override
    protected void doHandleExpressionDisseminationRequest() {
        final List<LanguageBean> languageBeans = this.getLanguageBeanOfExpression(this.cellarResource);
        this.addAcceptLanguageBeanIfEmpty(languageBeans);
    }

    /** {@inheritDoc} */
    @Override
    protected void doHandleManifestationDisseminationRequest() {
        final CellarResourceBean expression = this.disseminationService.checkForParentExpression(this.cellarResource);
        final List<LanguageBean> languageBeans = this.getLanguageBeanOfExpression(expression);
        this.addAcceptLanguageBeanIfEmpty(languageBeans);
    }

    /** {@inheritDoc} */
    @Override
    protected void doHandleItemDisseminationRequest() {
        // nothing to do (special case)
    }

    /**
     * Adds the accept language bean if empty.
     *
     * @param languageBeans the language beans
     */
    private void addAcceptLanguageBeanIfEmpty(final List<LanguageBean> languageBeans) {
        if (this.acceptLanguageBeans == null) {
            this.acceptLanguageBeans = languageBeans;
        } else if (this.acceptLanguageBeans.isEmpty()) {
            this.acceptLanguageBeans.addAll(languageBeans);
        }
    }

    /** {@inheritDoc} */
    @Override
    protected IURLTokenable getTypeStructure() {
        return Type.CONTENT_STREAM;
    }

}
