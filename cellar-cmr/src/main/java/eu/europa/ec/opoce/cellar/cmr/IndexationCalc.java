package eu.europa.ec.opoce.cellar.cmr;

import eu.europa.ec.opoce.cellar.cmr.database.DaoObject;

import java.io.Serializable;
import java.util.Date;

public class IndexationCalc extends DaoObject implements Serializable {

	private static final long serialVersionUID = 4853935908334019956L;

	private String cellarId;

    private String cellarBaseId;
    
    private String embeddedNotice;

    private Date embeddedNoticeCreationDate;

    
    public IndexationCalc() {}

    public IndexationCalc(String cellarId, String cellarBaseId, String embeddedNotice) {
        this.cellarId = cellarId;
        this.cellarBaseId = cellarBaseId;
        this.embeddedNotice = embeddedNotice;
        this.embeddedNoticeCreationDate = new Date();
    }
    
    
    public String getCellarId() {
        return cellarId;
    }

    public void setCellarId(String cellarId) {
        this.cellarId = cellarId;
    }
    
    public String getCellarBaseId() {
        return cellarBaseId;
    }

    public void setCellarBaseId(String cellarBaseId) {
        this.cellarBaseId = cellarBaseId;
    }

    public String getEmbeddedNotice() {
        return embeddedNotice;
    }

    public void setEmbeddedNotice(String embeddedNotice) {
        this.embeddedNotice = embeddedNotice;
    }

    public Date getEmbeddedNoticeCreationDate() {
        return embeddedNoticeCreationDate;
    }

    public void setEmbeddedNoticeCreationDate(Date embeddedNoticeCreationDate) {
        this.embeddedNoticeCreationDate = embeddedNoticeCreationDate;
    }

    @Override
    public String toString() {
        return "IndexationCalc{" +
                "cellarId='" + cellarId + '\'' +
                ", baseCellarId,='" + cellarBaseId + '\'' +
                ", embeddedNotice='" + embeddedNotice + '\'' +
                ", embeddedNoticeCreationDate=" + embeddedNoticeCreationDate +
                '}';
    }

}
