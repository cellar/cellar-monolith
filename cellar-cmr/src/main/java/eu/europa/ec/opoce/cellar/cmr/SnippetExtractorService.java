/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr
 *             FILE : SnippetExtractorService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 19-Apr-2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr;

import eu.europa.ec.opoce.cellar.semantic.jena.transformers.Statement2Object;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarType;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.owl_annotatedPropertyP;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.owl_annotatedSourceP;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarType.owl_axiomR;
import static org.apache.commons.collections15.CollectionUtils.collect;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 19-Apr-2017
 * The Class SnippetExtractorService.
 *
 * @author ARHS Developments
 * @version $$Revision$$$
 */
@Service
/**
 * <p>SnippetExtractorService class.</p>
 */
public class SnippetExtractorService {

    private static final Logger LOG = LogManager.getLogger(SnippetExtractorService.class);

    /** The stop classes. */
    private Collection<Resource> stopClasses;

    /**
     * <p>init.</p>
     */
    @PostConstruct
    public void init() {

        this.stopClasses = new HashSet<Resource>();
        this.stopClasses.add(CellarType.cdm_workR);
        this.stopClasses.add(CellarType.cdm_expressionR);
        this.stopClasses.add(CellarType.cdm_manifestationR);
        this.stopClasses.add(CellarType.cdm_dossierR);
        this.stopClasses.add(CellarType.cdm_eventR);
        this.stopClasses.add(CellarType.cdm_agentR);
        this.stopClasses.add(CellarType.cdm_topleveleventR);
    }

    /**
     * list all uri's that need to be used a stopUri out of a model.
     *
     * @param dmdIndex jenaModel to search for resources that match the condition to stop
     * @return set of uri's to stop on
     */
    public Collection<String> getStopUris(final Model dmdIndex) {
        final Set<Resource> resources = new HashSet<Resource>();
        for (final Resource stopClass : this.stopClasses) {
            resources.addAll(dmdIndex.listResourcesWithProperty(RDF.type, stopClass).toList());
        }

        final Set<String> stopUris = new HashSet<String>();
        for (final Resource resource : resources) {
            addSameAs(stopUris, resource);
        }
        return stopUris;
    }

    // add all uris that are equivalent to a certain uri

    /**
     * <p>addSameAs.</p>
     *
     * @param stopUris a {@link java.util.Set} object.
     * @param resource a {@link org.apache.jena.rdf.model.Resource} object.
     * @return a {@link java.util.Set} object.
     */
    private static Set<String> addSameAs(final Set<String> stopUris, final Resource resource) {
        if (stopUris.contains(resource.getURI())) {
            return stopUris;
        }
        stopUris.add(resource.getURI());
        final ResIterator resIterator = resource.getModel().listResourcesWithProperty(OWL.sameAs, resource);
        try {

            for (final Resource sameAs : resIterator.toList()) {
                addSameAs(stopUris, sameAs);
            }
            for (final RDFNode sameAs : resource.getModel().listObjectsOfProperty(resource, OWL.sameAs).toList()) {
                addSameAs(stopUris, sameAs.asResource());
            }
        } finally {
            resIterator.close();
        }
        return stopUris;
    }

    /**
     * extract a partial model out of the complete model only handling about certain uris.
     *
     * @param model                     the original model
     * @param uris                      the uri's of which the data is got to put in the partial model
     * @param stopUris                  uri's to stop the recursive action of adding
     * @param addAdditionalInfoOfTarget indication if extra information like sameAs, type and inScheme needs to be added for classes that are stopped on
     * @param urisParentAndChildren     uris of parent and children
     * @param isManifestation           indication if the digital object is a mnifestation or not
     * @return partial model with only requested information
     */
    public Model getSnippet(final Model model, final Collection<String> uris, final Collection<String> stopUris,
            final boolean addAdditionalInfoOfTarget, final Set<String> urisParentAndChildren, final boolean isManifestation) {
        LOG.debug("there are '{}' triples that can be extracted for [{}]", model.size(), StringUtils.join(uris, ", "));
        final Model snippet = ModelFactory.createDefaultModel();
        final Set<Resource> doneResources = new HashSet<Resource>();
        final Set<Resource> todoResources = new HashSet<Resource>();
        final SnippetConvertedMap convertedMap = new SnippetConvertedMap(model);

        for (final String uri : uris) {
            todoResources.add(model.getResource(uri));

            // Allows to retrieve items since the move of manifestation_has_item property in inferred model
            if (isManifestation) {
                final List<String> itemsUri = this.addItemsUri(model, uri);
                for (final String itemUri : itemsUri) {
                    todoResources.add(model.getResource(itemUri));
                }
            }
        }

        while (!todoResources.isEmpty()) {
            final Resource activeResource = todoResources.iterator().next();
            todoResources.remove(activeResource);

            this.addAllTriplesWhereSubject(activeResource, snippet, convertedMap);
            this.addAnnotationsOf(activeResource, snippet, convertedMap);

            doneResources.add(activeResource);

            final Collection<RDFNode> recursiveNodes = CollectionUtils.collect(activeResource.listProperties(), Statement2Object.instance,
                    new HashSet<RDFNode>());
            for (final RDFNode recursiveNode : recursiveNodes) {
                if (recursiveNode.isLiteral()) {
                    continue;
                    //if(recursiveNode.isAnon()){ throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrorCode.INGESTION_SERVICE_ERROR).withMessage("Blank nodes are not supported in the snippet extraction").build();}
                }

                final Resource recursiveResource = recursiveNode.asResource();
                if (!doneResources.contains(recursiveResource) && !todoResources.contains(recursiveResource)) {
                    if (recursiveResource.isURIResource() && stopUris.contains(recursiveResource.getURI())) {
                        if (addAdditionalInfoOfTarget) {
                            if (!urisParentAndChildren.contains(recursiveResource.getURI())) {
                                this.addSameAsAndTypes(snippet, recursiveResource, convertedMap);
                            }
                        }
                    } else if (addAdditionalInfoOfTarget || !recursiveResource.hasProperty(RDF.type, CellarType.skos_ConceptR)) {
                        todoResources.add(recursiveResource);
                    }
                }
            }
        }
        LOG.debug("the extracted snippet contains '{}' triples", snippet.size());
        return snippet;
    }

    /**
     * Gets the snippet without recursivity.
     *
     * @param model the model
     * @param uris the uris
     * @return the snippet without recursivity
     */
    public Model getSnippetWithoutRecursivity(final Model model, final Collection<String> uris) {
        LOG.debug("there are '{}' triples that can be extracted for [{}]", model.size(), StringUtils.join(uris, ", "));
        final Model snippet = ModelFactory.createDefaultModel();
        final Set<Resource> todoResources = new HashSet<Resource>();
        final SnippetConvertedMap convertedMap = new SnippetConvertedMap(model);

        for (final String uri : uris) {
            todoResources.add(model.getResource(uri));
        }

        while (!todoResources.isEmpty()) {
            final Resource activeResource = todoResources.iterator().next();
            todoResources.remove(activeResource);
            this.addAllTriplesWhereSubject(activeResource, snippet, convertedMap);
            this.addAnnotationsOf(activeResource, snippet, convertedMap);
        }
        LOG.debug("the extracted snippet contains '{}' triples", snippet.size());
        return snippet;
    }

    /**
     * Adds the items uri.
     *
     * @param model the model
     * @param uri the uri
     * @return the list
     */
    private List<String> addItemsUri(final Model model, final String uri) {
        final List<String> itemsUri = new ArrayList<String>();
        final RDFNode manifestationNode = ResourceFactory.createResource(uri);
        final ResIterator items = model.listSubjectsWithProperty(CellarProperty.HierarchyProperty.cdm_item_belongs_to_manifestationP,
                manifestationNode);
        try {
            while (items.hasNext()) {
                final Resource res = items.nextResource();
                final String itemUri = res.getURI();
                itemsUri.add(itemUri);
            }
        } finally {
            items.close();
        }
        return itemsUri;
    }

    /**
     * <p>addSameAsAndTypes.</p>
     *
     * @param snippet      a {@link org.apache.jena.rdf.model.Model} object.
     * @param resource     a {@link org.apache.jena.rdf.model.Resource} object.
     * @param convertedMap a {@link eu.europa.ec.opoce.cellar.cmr.SnippetExtractorService.SnippetConvertedMap} object.
     */
    private void addSameAsAndTypes(final Model snippet, final Resource resource, final SnippetConvertedMap convertedMap) {
        final Set<Resource> done = new HashSet<Resource>();
        this.handleResource(snippet, resource, done, convertedMap);
        this.addSameAsAndTypes(snippet, done, resource, convertedMap);
    }

    /**
     * <p>addSameAsAndTypes.</p>
     *
     * @param snippet      a {@link org.apache.jena.rdf.model.Model} object.
     * @param done         a {@link java.util.Set} object.
     * @param resource     a {@link org.apache.jena.rdf.model.Resource} object.
     * @param convertedMap a {@link eu.europa.ec.opoce.cellar.cmr.SnippetExtractorService.SnippetConvertedMap} object.
     */
    private void addSameAsAndTypes(final Model snippet, final Set<Resource> done, final Resource resource,
            final SnippetConvertedMap convertedMap) {
        final ResIterator resIterator = resource.getModel().listResourcesWithProperty(OWL.sameAs, resource);
        try {
            for (final Resource sameAs : resIterator.toList()) {
                if (!done.contains(sameAs)) {
                    snippet.add(convertedMap.getConvertedValue(sameAs), OWL.sameAs, convertedMap.getConvertedValue(resource));
                    this.handleResource(snippet, sameAs, done, convertedMap);
                    this.addSameAsAndTypes(snippet, done, resource, convertedMap);
                }
            }
            for (final RDFNode sameAs : resource.getModel().listObjectsOfProperty(resource, OWL.sameAs).toList()) {
                if (!done.contains(sameAs.asResource())) {
                    snippet.add(convertedMap.getConvertedValue(resource), OWL.sameAs, convertedMap.getConvertedValue(sameAs.asResource()));
                    this.handleResource(snippet, sameAs.asResource(), done, convertedMap);
                    this.addSameAsAndTypes(snippet, done, resource, convertedMap);
                }
            }
        } finally {
            resIterator.close();
        }
    }

    /**
     * <p>handleResource.</p>
     *
     * @param snippet      a {@link org.apache.jena.rdf.model.Model} object.
     * @param resource     a {@link org.apache.jena.rdf.model.Resource} object.
     * @param done         a {@link java.util.Set} object.
     * @param convertedMap a {@link eu.europa.ec.opoce.cellar.cmr.SnippetExtractorService.SnippetConvertedMap} object.
     */
    private void handleResource(final Model snippet, final Resource resource, final Set<Resource> done,
            final SnippetConvertedMap convertedMap) {
        this.addProperty(snippet, RDF.type, resource, convertedMap);
        this.addProperty(snippet, CellarProperty.skos_inSchemeP, resource, convertedMap);
        done.add(resource);
    }

    /**
     * <p>addProperty.</p>
     *
     * @param snippet           a {@link org.apache.jena.rdf.model.Model} object.
     * @param property          a {@link org.apache.jena.rdf.model.Property} object.
     * @param recursiveResource a {@link org.apache.jena.rdf.model.Resource} object.
     * @param convertedMap      a {@link eu.europa.ec.opoce.cellar.cmr.SnippetExtractorService.SnippetConvertedMap} object.
     */
    private void addProperty(final Model snippet, final Property property, final Resource recursiveResource,
            final SnippetConvertedMap convertedMap) {
        final Collection<RDFNode> objects = collect(recursiveResource.asResource().listProperties(property), Statement2Object.instance);
        for (final RDFNode object : objects) {
            final Resource subject = convertedMap.getConvertedValue(recursiveResource);
            snippet.add(subject, property, object.isResource() ? convertedMap.getConvertedValue(object.asResource()) : object);
        }
    }

    /**
     * <p>addAllTriplesWhereSubject.</p>
     *
     * @param originalResource a {@link org.apache.jena.rdf.model.Resource} object.
     * @param snippet          a {@link org.apache.jena.rdf.model.Model} object.
     * @param convertedMap     a {@link eu.europa.ec.opoce.cellar.cmr.SnippetExtractorService.SnippetConvertedMap} object.
     */
    private void addAllTriplesWhereSubject(final Resource originalResource, final Model snippet, final SnippetConvertedMap convertedMap) {
        final Resource snippetResource = convertedMap.getConvertedValue(originalResource);
        final StmtIterator stmtIterator = originalResource.listProperties();
        try {
            while (stmtIterator.hasNext()) {
                final Statement statement = stmtIterator.next();
                final RDFNode object = statement.getObject();
                snippet.add(snippetResource, statement.getPredicate(),
                        object.isResource() ? convertedMap.getConvertedValue(object.asResource()) : object);
            }
        } finally {
            stmtIterator.close();
        }
    }

    /**
     * <p>addAnnotationsOf.</p>
     *
     * @param activeResource a {@link org.apache.jena.rdf.model.Resource} object.
     * @param snippet        a {@link org.apache.jena.rdf.model.Model} object.
     * @param convertedMap   a {@link eu.europa.ec.opoce.cellar.cmr.SnippetExtractorService.SnippetConvertedMap} object.
     */
    private void addAnnotationsOf(final Resource activeResource, final Model snippet, final SnippetConvertedMap convertedMap) {
        final ResIterator annotationsIterator = activeResource.getModel().listSubjectsWithProperty(owl_annotatedSourceP, activeResource);
        try {
            for (final Resource annotation : annotationsIterator.toSet()) {
                if (annotation.hasProperty(RDF.type, owl_axiomR) && annotation.hasProperty(owl_annotatedPropertyP)) {
                    this.addAllTriplesWhereSubject(annotation, snippet, convertedMap);
                }
            }
        } finally {
            annotationsIterator.close();
        }
    }

}
