package eu.europa.ec.opoce.cellar.server.dissemination;

import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.Structure;
import eu.europa.ec.opoce.cellar.server.service.DisseminationException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

/**
 * Enumeration of supported notice parameters.
 * <p/>
 * <ol>
 * <li><code>notice=tree</code></li>
 * <li><code>notice=branch</code></li>
 * <li><code>notice=object</code></li>
 * <li><code>notice=identifier</code></li>
 * <li><code>notice=non-inferred</code></li>
 * <li><code>notice=non-inferred-tree</code></li>
 *  <li><code>notice=content</code></li>
 *  <li><code>notice=normalized</code></li>
 *  <li><code>notice=tree-normalized</code></li>
 *  <li><code>notice=non-inferred-normalized</code></li>
 *  <li><code>notice=non-inferred-tree-normalized</code></li>
 * </ol>
 */
@SuppressWarnings("serial")
public enum Notice {

    // TODO: CELLAR-683, CELLARM-405: Merge this into the Structure enum, if possible 
    TREE(Structure.TREE.getURLToken()), //
    BRANCH(Structure.BRANCH.getURLToken()), //
    OBJECT(Structure.OBJECT.getURLToken()), //
    IDENTIFIER("identifier"), //
    NON_INFERRED("non-inferred"), //
    NON_INFERRED_TREE("non-inferred-tree"), //
    CONTENT("content"),
    NORMALIZED_OBJECT("normalized"),//
    NORMALIZED_TREE("tree-normalized"),//
    NORMALIZED_NON_INFERRED_OBJECT("non-inferred-normalized"),//
    NORMALIZED_NON_INFERRED_TREE("non-inferred-tree-normalized"),//
    NONE("none");

    public static String PARAM_NAME = "notice";

    private static final Map<MediaType, List<String>> VALID_NOTICES_PER_MEDIATYPE = new HashMap<MediaType, List<String>>();

    static {
        List<String> notices = new ArrayList<String>() {

            {
                add(TREE.getParamValue());
                add(BRANCH.getParamValue());
                add(OBJECT.getParamValue());
            }
        };
        VALID_NOTICES_PER_MEDIATYPE.put(MediaType.APPLICATION_XML, notices);
        notices = new ArrayList<String>() {

            {
                add(TREE.getParamValue());
                add(NON_INFERRED.getParamValue());
                add(NON_INFERRED_TREE.getParamValue());
                add(CONTENT.getParamValue());
                add(NORMALIZED_TREE.getParamValue());
                add(NORMALIZED_OBJECT.getParamValue());
                add(NORMALIZED_NON_INFERRED_TREE.getParamValue());
                add(NORMALIZED_NON_INFERRED_OBJECT.getParamValue());
            }
        };
        VALID_NOTICES_PER_MEDIATYPE.put(DisseminationRequestUtils.RDF_XML, notices);
    }

    private String paramValue;

    /**
     * <p>Constructor for Notice.</p>
     *
     * @param paramValue a {@link java.lang.String} object.
     */
    Notice(String paramValue) {
        this.paramValue = paramValue;
    }

    /**
     * <p>Getter for the field <code>paramValue</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getParamValue() {
        return paramValue;
    }

    /**
     * <p>mediaValueOf.</p>
     *
     * @param mediaType a {@link org.springframework.http.MediaType} object.
     * @return a {@link eu.europa.ec.opoce.cellar.server.dissemination.Notice} object.
     */
    public static Notice mediaValueOf(final MediaType mediaType) {
        Notice retNotice = NONE;
        if (mediaType == null) {
            return retNotice;
        }

        final String noticeStr = StringUtils.trim(mediaType.getParameter(PARAM_NAME));
        if (!StringUtils.isEmpty(noticeStr)) {
            retNotice = Notice.paramValueOf(noticeStr);
            final MediaType mediaTypeWithoutParams = DisseminationRequestUtils.getMediaTypeWithoutParams(mediaType);

            if (retNotice == null) {
                throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                        .withMessage("Unknown notice parameter '{}' for media type '{}'").withMessageArgs(noticeStr, mediaTypeWithoutParams)
                        .build();
            }

            if (VALID_NOTICES_PER_MEDIATYPE.get(mediaTypeWithoutParams) == null
                    || !VALID_NOTICES_PER_MEDIATYPE.get(mediaTypeWithoutParams).contains(noticeStr)) {
                throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                        .withMessage(
                                "Notice parameter '{}' is not supported for media type '{}'. Here follows how media types support notice parameters: '{}'")
                        .withMessageArgs(noticeStr, mediaTypeWithoutParams, VALID_NOTICES_PER_MEDIATYPE).build();
            }
        }

        return retNotice;
    }

    /**
     * <p>paramValueOf.</p>
     *
     * @param paramValue a {@link java.lang.String} object.
     * @return a {@link eu.europa.ec.opoce.cellar.server.dissemination.Notice} object.
     */
    private static Notice paramValueOf(final String paramValue) {
        Notice retNotice = null;

        for (Notice currNotice : values()) {
            if (currNotice.paramValue.equalsIgnoreCase(paramValue)) {
                retNotice = currNotice;
                break;
            }
        }

        return retNotice;
    }

}
