/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cmr.service
 *        FILE : MementoQueryConfiguration.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 08-09-2015
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */

package eu.europa.ec.opoce.cellar.cmr.service;

import java.util.Map;
import java.util.regex.Pattern;

/**
 * Interface for Memento query getters
 * 
 * @author ARHS Developments
 * @version
 */

public interface IMementoQueryConfiguration {

    /**
     * Gets the query used to determine whether a resource is Memento-related 
     * @param placeHoldersMap a place holders map for query arguments
     * @return the query for Memento verification
     */
    String getQueryForEvolutiveWork(final Map<Pattern, Object> placeHoldersMap);

    String getQueryForEvolutiveWorkOrMemento(final Map<Pattern, Object> placeHoldersMap);

    /**
     * Gets the query for first memento related to an evolutive work
     * @param placeHoldersMap a place holders map for query arguments
     * @return the query for first memento
     */
    String getQueryForFirstMemento(final Map<Pattern, Object> placeHoldersMap);

    /**
     * Gets the query for last memento related to an evolutive work
     * @param placeHoldersMap a place holders map for query arguments
     * @return the query for last memento
     */
    String getQueryForLastMemento(final Map<Pattern, Object> placeHoldersMap);

    /**
     * Gets the query for memento date-time
     * @param placeHoldersMap a place holders map for query arguments
     * @return the query for memento date-time
     */
    String getQueryForMementoDateTime(final Map<Pattern, Object> placeHoldersMap);

    /**
     * Gets the query for retrieving the nearest memento in future
     * @param placeHoldersMap a place holders map for query arguments
     * @return the query for nearest in future
     */
    String getQueryForNearestInFuture(final Map<Pattern, Object> placeHoldersMap);

    /**
     * Gets the query for retrieving the nearest memento in past
     * @param placeHoldersMap a place holders map for query arguments
     * @return the query for nearest in past, as a String
     */
    String getQueryForNearestInPast(final Map<Pattern, Object> placeHoldersMap);

    /**
     * Gets the query for retrieving the related memento's
     * @param placeHoldersMap a place holders map for query arguments
     * @return the query for related memento's
     */
    String getQueryForRelatedMementos(final Map<Pattern, Object> placeHoldersMap);

    /**
     * Gets the query for retrieving the related evolutive works
     * @param placeHoldersMap a place holders map for query arguments
     * @return the query for related memento's
     */
    String getQueryForRelatedEvolutiveWorks(final Map<Pattern, Object> placeHoldersMap);

    /**
     * Gets the query for retrieving time map info
     * @param placeHoldersMap a place holders map for query arguments
     * @return the query for time map info
     */
    String getQueryForTimeMapInfo(final Map<Pattern, Object> placeHoldersMap);

    /**
     * Gets the query for computing the original resource in a hierarchy
     * @param placeHoldersMap a place holders map for query arguments
     * @return the query for computing the original resource in a hierarchy
     */
    String getQueryForURIG(final Map<Pattern, Object> placeHoldersMap);

}
