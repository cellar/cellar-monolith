package eu.europa.ec.opoce.cellar.server.filter;

import eu.europa.ec.opoce.cellar.logging.CellarLogLevelService;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>LoggerContextFilter class.</p>
 */
public class LoggerContextFilter extends OncePerRequestFilter {

    private static final String CELLAR_CONTEXT = "CELLAR_CONTEXT";

    private String loggerName;

    private ApplicationContext context;
    private CellarLogLevelService cellarLogLevelService;

    /**
     * <p>Constructor for LoggerContextFilter.</p>
     */
    public LoggerContextFilter() {
        super();

        addRequiredProperty("loggerName");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {
        ThreadContext.put(CELLAR_CONTEXT, loggerName);
        try {
            filterChain.doFilter(request, response);
        } finally {
            ThreadContext.remove(CELLAR_CONTEXT);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void initFilterBean() throws ServletException {
        super.initFilterBean();

        if (StringUtils.isBlank(loggerName)) {
            throw new ServletException("Initialization from FilterConfig for filter '" + getFilterName()
                    + "' failed; the following required property cannot be blank: 'loggerName'");
        }

        this.context = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());
        this.cellarLogLevelService = context.getBean(CellarLogLevelService.class);

        if (this.cellarLogLevelService == null) {
            throw new ServletException("Initialization from FilterConfig for filter '" + getFilterName()
                    + "' failed; no spring bean of type '" + CellarLogLevelService.class.getName() + "' found!");
        }
    }

    public void setLoggerName(String loggerName) {
        this.loggerName = loggerName;
    }
}
