package eu.europa.ec.opoce.cellar.server.filter;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Enumeration;

import static eu.europa.ec.opoce.cellar.server.filter.DisseminationLogHelper.LANGUAGES_REQUEST_ATTRIBUTE_NAME;
import static eu.europa.ec.opoce.cellar.server.filter.DisseminationLogHelper.MIME_TYPE_REQUEST_ATTRIBUTE_NAME;

/**
 * <class_description> Filter for logging requests.<br />
 * Ingestion must be enabled to have it on.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 02-08-2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class RequestLoggingFilter extends OncePerRequestFilter {

    /**
     * Constant <code>INCOMING_REQUEST="Incoming Request: "</code>
     */
    private static final String INCOMING_REQUEST = "Incoming %s-Request: ";

    /**
     * Constant <code>USED="Used "</code>
     */
    private static final String USED = "|Used ";

    private static final String STATUS = "HTTP status=";
    /**
     * Constant <code>MS_TO_PROCESS_REQUEST=" ms. to process Request: "</code>
     */
    private static final String MS_TO_PROCESS_REQUEST = " ms. to process %s-Request: ";

    /**
     * Constant <code>EXCEPTION_WHILE_PROCESSING_REQUEST="Exception while processing request: "</code>
     */
    private static final String EXCEPTION_WHILE_PROCESSING_REQUEST = "Exception while processing request: ";

    private Logger loggerName;

    private boolean includeClientInfo = false;
    private boolean includeQueryString = false;
    private boolean includeHeaderInfo = false;

    /**
     * {@inheritDoc}
     */
    @Override
    protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response, final FilterChain filterChain)
            throws ServletException, IOException {

        final long startTime = System.currentTimeMillis();
        beforeRequest(request, createMessage(request, String.format(INCOMING_REQUEST, request.getMethod()), null));
        try {
            filterChain.doFilter(request, response);

            final String prefix = new StringBuilder(STATUS).append(response.getStatus()).append(USED)
                    .append(System.currentTimeMillis() - startTime).append(String.format(MS_TO_PROCESS_REQUEST, request.getMethod()))
                    .toString();
            afterRequest(request, createMessage(request, prefix, null));
        } catch (final ServletException e) {
            final String suffix = new StringBuilder(": ").append(e.getMessage()).toString();
            afterException(request, createMessage(request, EXCEPTION_WHILE_PROCESSING_REQUEST, suffix), e);

            throw e;
        } catch (final Exception e) {
            final String suffix = new StringBuilder(": ").append(e.getMessage()).toString();
            afterException(request, createMessage(request, EXCEPTION_WHILE_PROCESSING_REQUEST, suffix), e);

            throw new ServletException(e);
        }
    }

    /**
     * Write the log message <i>before</i> the request is processed.
     *
     * @param request current HTTP request
     * @param message the message to log
     */
    protected void beforeRequest(final HttpServletRequest request, final String message) {
        if (logger() != null) {
            logger().info(message);
        } else {
            getServletContext().log(message);
        }
    }

    /**
     * Write the log message <i>after</i> the request is processed.
     *
     * @param request current HTTP request
     * @param message the message to log
     */
    protected void afterRequest(final HttpServletRequest request, final String message) {
        if (logger() != null) {
            logger().info(message);
        } else {
            getServletContext().log(message);
        }
    }

    /**
     * Write the log message <i>after</i> an exception is thrown.
     *
     * @param request current HTTP request
     * @param message the message to log
     * @param e       the Exception to log
     */
    protected void afterException(final HttpServletRequest request, final String message, final Exception e) {
        if (logger() != null) {
            logger().error(message, e);
        } else {
            getServletContext().log(message, e);
        }
    }

    /**
     * Create a log message for the given request, prefix and suffix. <p>If <code>includeQueryString</code> is
     * <code>true</code> then the inner part of the log message will take the form <code>request_uri?query_string</code>
     * otherwise the message will simply be of the form <code>request_uri</code>. <p>The final message is composed of the
     * inner part as described and the supplied prefix and suffix.
     *
     * @param request a {@link javax.servlet.http.HttpServletRequest} object.
     * @param prefix  a {@link java.lang.String} object.
     * @param suffix  a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    protected String createMessage(final HttpServletRequest request, final String prefix, final String suffix) {
        final StringBuilder msg = new StringBuilder();
        if (StringUtils.isNotBlank(prefix)) {
            msg.append(prefix);
        }
        msg.append("uri=").append(request.getRequestURI());
        if (isIncludeQueryString()) {
            final String queryString = request.getQueryString();
            if (StringUtils.isNotBlank(queryString)) {
                msg.append('?').append(queryString);
            }
        }
        if (isIncludeClientInfo()) {
            final String client = request.getRemoteAddr();
            if (StringUtils.isNotBlank(client)) {
                msg.append(";client=").append(client);
            }
            final HttpSession session = request.getSession(false);
            if (session != null) {
                msg.append(";session=").append(session.getId());
            }
            final String user = request.getRemoteUser();
            if (user != null) {
                msg.append(";user=").append(user);
            }
        }
        if (isIncludeHeaderInfo()) {
            final Enumeration<?> headerNames = request.getHeaderNames();
            boolean hasMoreElements;
            if (hasMoreElements = headerNames.hasMoreElements()) {
                msg.append(";headers=[");
                while (hasMoreElements) {
                    final String name = (String) headerNames.nextElement();
                    msg.append(name).append("=").append(request.getHeader(name));
                    if (hasMoreElements = headerNames.hasMoreElements()) {
                        msg.append(", ");
                    }
                }
                appendDisseminationInfoToHeaderInfo(request, msg);
                msg.append("]");
            }
        }
        if (StringUtils.isNotBlank(suffix)) {
            msg.append(suffix);
        }

        return msg.toString();
    }

    /**
     * Set whether or not the client address and session id should be included in the log message. <p>Should be configured
     * using an <code>&lt;init-param&gt;</code> for parameter name "includeClientInfo" in the filter definition in
     * <code>web.xml</code>.
     *
     * @param includeClientInfo a boolean.
     */
    public void setIncludeClientInfo(final boolean includeClientInfo) {
        this.includeClientInfo = includeClientInfo;
    }

    /**
     * Return whether or not the client address and session id should be included in the log message.
     *
     * @return a boolean.
     */
    protected boolean isIncludeClientInfo() {
        return this.includeClientInfo;
    }

    /**
     * Set whether or not the query string should be included in the log message. <p>Should be configured using an
     * <code>&lt;init-param&gt;</code> for parameter name "includeQueryString" in the filter definition in
     * <code>web.xml</code>.
     *
     * @param includeQueryString a boolean.
     */
    public void setIncludeQueryString(final boolean includeQueryString) {
        this.includeQueryString = includeQueryString;
    }

    /**
     * Return whether or not the query string should be included in the log message.
     *
     * @return a boolean.
     */
    protected boolean isIncludeQueryString() {
        return this.includeQueryString;
    }

    /**
     * Set whether or not the request headers should be included in the log message. <p>Should be configured using an
     * <code>&lt;init-param&gt;</code> for parameter name "includeHeaderInfo" in the filter definition in
     * <code>web.xml</code>.
     *
     * @param includeHeaderInfo a boolean.
     */
    public void setIncludeHeaderInfo(final boolean includeHeaderInfo) {
        this.includeHeaderInfo = includeHeaderInfo;
    }

    /**
     * Return whether or not the request headers should be included in the log message.
     *
     * @return a boolean.
     */
    protected boolean isIncludeHeaderInfo() {
        return this.includeHeaderInfo;
    }

    /**
     * <p>loggerName.</p>
     *
     * @return a {@link org.slf4j.Logger} object.
     */
    private Logger logger() {
        if (this.loggerName == null) {
            this.loggerName = LogManager.getLogger(RequestLoggingFilter.class);
        }
        return this.loggerName;
    }
    
    /**
     * Appends additional dissemination-related information to selected dissemination log entries.
     * @param request the http request.
     * @param msg the message to log.
     */
	private void appendDisseminationInfoToHeaderInfo(final HttpServletRequest request, StringBuilder msg) {
    	if (request.getAttribute(LANGUAGES_REQUEST_ATTRIBUTE_NAME) != null) {
    		String languages = (String) request.getAttribute(LANGUAGES_REQUEST_ATTRIBUTE_NAME);
    		msg.append(", ").append(languages);
    	}
		if (request.getAttribute(MIME_TYPE_REQUEST_ATTRIBUTE_NAME) != null) {
			String mimeType = (String) request.getAttribute(MIME_TYPE_REQUEST_ATTRIBUTE_NAME);
			msg.append(", ").append(mimeType);
		}
    } 

}
