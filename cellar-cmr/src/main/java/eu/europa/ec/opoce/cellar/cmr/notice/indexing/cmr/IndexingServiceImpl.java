package eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr;

import eu.europa.ec.opoce.cellar.cl.configuration.ConfigurationPropertyService.EXISTINGPROPS_MODE;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CmrIndexRequestDao;
import eu.europa.ec.opoce.cellar.cmr.indexing.service.IIndexRequestProcessorService;
import eu.europa.ec.opoce.cellar.cmr.indexing.service.IndexRequestManagerService;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.ExecutionStatus;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.RequestType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * <p>CmrIndexRequestService class.</p>
 */
@Service
public class IndexingServiceImpl implements IIndexingService {

    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    /** The index request dao. */
    @Autowired(required = true)
    private CmrIndexRequestDao indexRequestDao;

    @Autowired
    private IIndexRequestProcessorService indexRequestProcessorService;

    @Autowired
    private IndexRequestManagerService indexRequestManagerService;

    /**
     * Returns the minimal level of priority handled by the indexing service.
     * @return the minimal level of priority handled by the indexing service
     */
    @Override
    public Priority getMinimalLevelOfPriorityHandled() {
        return this.cellarConfiguration.getCellarServiceIndexingMinimumPriority(); // managed by the CELLAR configuration
    }

    /**
     * Sets the minimal level of priority handled by the indexing service.
     * @param minimalLevelOfPriorityToBeHandled the new minimal level of priority
     */
    @Override
    public void setMinimalLevelOfPriorityHandled(final Priority minimalLevelOfPriorityToBeHandled) {
        this.cellarConfiguration.setCellarServiceIndexingMinimumPriority(minimalLevelOfPriorityToBeHandled, EXISTINGPROPS_MODE.TO_DB); // managed by the CELLAR configuration
    }

    /**
     * Returns true if the indexing service is enabled. Otherwise, false.
     * @return true if the indexing service is enabled. Otherwise, false.
     */
    @Override
    public boolean isIndexingEnabled() {
        return this.cellarConfiguration.isCellarServiceIndexingEnabled();
    }

    /**
     * Sets the indexing service.
     * @param indexing indicates if the service is enabled or not
     */
    @Override
    public void setIndexingEnabled(final boolean indexing) {
        this.cellarConfiguration.setCellarServiceIndexingEnabled(indexing, EXISTINGPROPS_MODE.TO_DB);
    }

    /**
     * This method returns the status of the indexing service.
     *
     * @return a {@link INDEXING_SERVICE_STATUS} object.
     */
    @Override
    public INDEXING_SERVICE_STATUS getIndexingStatus() {
        return this.isIndexingRunning() ? (this.isIndexingEnabled() ? INDEXING_SERVICE_STATUS.INDEXING : INDEXING_SERVICE_STATUS.STOPPING)
                : INDEXING_SERVICE_STATUS.NOT_INDEXING;
    }

    /**
     * <p>isRunning.</p>
     *
     * @return a boolean.
     */
    @Override
    public boolean isIndexingRunning() {
        return this.indexRequestProcessorService.isIndexingRunning();
    }

    @Override
    public void restartFailedCmrIndexRequests() {
        this.indexRequestManagerService.restartFailedCmrIndexRequests();
    }

    /**
     * <p>getCountIndexRequestsToHandle.</p>
     *
     * @return a long.
     */
    @Override
    public long getCountIndexRequestsToHandle() {
        return this.indexRequestDao.countByNotExecutionStatusAndNotRequestType(RequestType.Skip,
        		ExecutionStatus.Done, ExecutionStatus.Error, ExecutionStatus.Redundant);
    }

    /**
     * <p>getCountFailedIndexRequests.</p>
     *
     * @return a long.
     */
    @Override
    public long getCountFailedIndexRequests() {
        return this.indexRequestDao.countByExecutionStatus(ExecutionStatus.Error);
    }
}
