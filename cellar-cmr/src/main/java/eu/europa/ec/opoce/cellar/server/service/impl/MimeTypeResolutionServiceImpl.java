/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.service.impl
 *             FILE : MimeTypeResolutionServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 31 Jan 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.service.impl;

import eu.europa.ec.opoce.cellar.ccr.service.FileTypesNalSkosLoaderService;
import eu.europa.ec.opoce.cellar.cl.domain.mimeType.MimeTypeMapping;
import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaQueries;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import eu.europa.ec.opoce.cellar.server.service.DisseminationException;
import eu.europa.ec.opoce.cellar.server.service.MimeTypeResolutionService;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.rdf.model.SimpleSelector;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import java.util.ArrayList;
import java.util.List;

/**
 * <class_description>  This service is used to resolve the manifestation mime type  for dissemination and ingestion using the FileTypesNalSkosLoaderService.
 * <br/><br/>
 * <notes> .
 * <br/><br/>
 * ON : 31 Jan 2017
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class MimeTypeResolutionServiceImpl implements MimeTypeResolutionService {

    private static final Logger LOG = LogManager.getLogger(MimeTypeResolutionServiceImpl.class);

    /** The Constant NS_TYPE. */
    private static final String NS_TYPE = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";

    /** The file types nal skos loader service. */
    @Autowired
    private FileTypesNalSkosLoaderService fileTypesNalSkosLoaderService;

    /** {@inheritDoc} */
    @Override
    public String resolve(final Resource contentStreamResource, final Resource manifestationResource,
            final List<Resource> manifestationSameAsResources) {
        //get mimetype from the ITEM metadata
        String result = JenaQueries.getLiteralValue(contentStreamResource, CellarProperty.cmr_manifestationMimeTypeP, false, true);
        if (StringUtils.isBlank(result)) {
            LOG.warn("The CONTENT-STREAM metadata for the resource [" + contentStreamResource.toString()
                    + "] does not have the manifestation-mime-type property. CELLAR will deduce based on the parent MANIFESTATION and the NAL file-type SKOS service");
            final List<Resource> manifestationResources = new ArrayList<Resource>();
            manifestationResources.add(manifestationResource);
            manifestationResources.addAll(manifestationSameAsResources);

            final String manifestationType = manifestationResources.stream()
                    .filter(resource -> resource.hasProperty(CellarProperty.cdm_manifestation_typeP)).findFirst()
                    .map(resource -> JenaQueries.getLiteralValue(resource, CellarProperty.cdm_manifestation_typeP, false, true)).orElse("");

            if (!StringUtils.isBlank(manifestationType)) {
                final List<MimeTypeMapping> mimeTypeMappings = fileTypesNalSkosLoaderService
                        .getMimeTypeMappingByManifestationType(manifestationType);
                if ((mimeTypeMappings != null) && !mimeTypeMappings.isEmpty()) {
                    for (final MimeTypeMapping mimeTypeMapping : mimeTypeMappings) {
                        final String mimeType = mimeTypeMapping.getMimeType();
                        if (StringUtils.isNotBlank(mimeType)) {
                            result = mimeType;
                            break;
                        }
                    }
                    if (StringUtils.isBlank(result)) {
                        LOG.warn("CELLAR was not able to find a matching mime-type for the manifestation-type [" + manifestationType + "]");
                    }
                }
            } else {
                LOG.warn("CELLAR was not able to retrive the manifestation-type for the parent MANIFESTATION of the resource ["
                        + contentStreamResource.toString() + "]");
            }
        }
        if (StringUtils.isBlank(result)) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class)
                    .withMessage("Unable to resolve the mime type for: [resource '{}']").withMessageArgs(contentStreamResource.getURI())
                    .build();
        }
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<Statement> resolve(final Model manifestationDirectModel, final Model manifestationInferredModel) {
        final List<Statement> result = new ArrayList<Statement>();
        final Property type = ResourceFactory.createProperty(NS_TYPE);
        final RDFNode itemNode = ResourceFactory.createResource(CellarProperty.cdm_item);
        //Get all items linked to this manifestation
        final ResIterator items = manifestationDirectModel.listSubjectsWithProperty(type, itemNode);
        try {
            while (items.hasNext()) {
                final Resource res = items.nextResource();
                final String itemUri = res.getURI();
                final Resource resource = manifestationDirectModel.getResource(itemUri);
                Statement resultItem = resource.getProperty(CellarProperty.cmr_manifestationMimeTypeP);
                // if the ITEM hasn't the manifestationMimeType in the direct model
                // then get the manifestationMimeType from inferred model and add it to return model
                if (resultItem == null) {
                    resultItem = manifestationInferredModel.getProperty(res, CellarProperty.cmr_manifestationMimeTypeP);
                }
                //if the property is not on the direct or inferred metadata
                //retrieve the manifestation-type from the MANIFESTATION and use the fileTypesNalSkosLoaderService to get the mime-type
                if (resultItem == null) {
                    final String defaultMimeType = loadDefaultManifestationTypes(manifestationDirectModel);
                    if (StringUtils.isNotBlank(defaultMimeType)) {
                        resultItem = manifestationDirectModel.createLiteralStatement(res, CellarProperty.cmr_manifestationMimeTypeP,
                                defaultMimeType);
                    }
                }
                if (resultItem != null) {
                    result.add(resultItem);
                }
                else {
                    throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class)
                            .withMessage("Unable to resolve the mime type for: [resource '{}']").withMessageArgs(res.getURI()).build();
                }
            }
        } finally {
            items.close();
        }
        return result;
    }

    /**
     * Get the manifestation type from the manifestation and use the FileTypesNalSkosLoaderService to get the mime-types.
     *
     * @param manifestationDirectModel the manifestation direct model
     * @return the string[]
     */
    private String loadDefaultManifestationTypes(final Model manifestationDirectModel) {
        final SimpleSelector selector = new SimpleSelector() {

            /** {@inheritDoc} */
            @Override
            public boolean test(final Statement s) {
                boolean test = false;
                this.predicate = CellarProperty.cdm_manifestation_typeP;
                test = super.test(s);
                return test;
            }
        };

        final StmtIterator manifestationTypeStatements = manifestationDirectModel.listStatements(selector);
        try {
            while (manifestationTypeStatements.hasNext()) {
                final Statement next = manifestationTypeStatements.next();
                final String manifestationType = next.getString();
                final List<MimeTypeMapping> mimeTypeMappings = fileTypesNalSkosLoaderService
                        .getMimeTypeMappingByManifestationType(manifestationType);
                if ((mimeTypeMappings != null) && !mimeTypeMappings.isEmpty()) {
                    for (final MimeTypeMapping mimeTypeMapping : mimeTypeMappings) {
                        final String mimeType = mimeTypeMapping.getMimeType();
                        if (StringUtils.isNotBlank(mimeType)) {
                            return mimeType;
                        }
                    }
                }
            }
        } finally {
            manifestationTypeStatements.close();
        }
        return null;
    }
}
