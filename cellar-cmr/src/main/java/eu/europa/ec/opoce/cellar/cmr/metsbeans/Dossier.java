package eu.europa.ec.opoce.cellar.cmr.metsbeans;

import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;

import java.util.Map;

public class Dossier extends HierarchyElement<MetsElement, Event> {

    private static final long serialVersionUID = -1918492966672954190L;

    public Dossier() {
        super(DigitalObjectType.DOSSIER);
    }

    public Dossier(Map<ContentType, String> versions) {
        super(DigitalObjectType.DOSSIER, versions);
    }
}
