/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cmr.database.util
 *        FILE : JdbcUtils.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 08-06-2015
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.database.util;

import eu.europa.ec.opoce.cellar.exception.CellarConfigurationException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 * <class_description> This is an utility component for JDBC access.<br/>
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 08-06-2015
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public class JdbcUtils extends org.springframework.jdbc.support.JdbcUtils {

    /**
     * Execute a query.
     * 
     * @param dataSource the datasource
     * @param query the query to execute
     * @return the boolean result
     * @throws Exception thrown in case of any error
     */
    public static boolean execute(final DataSource dataSource, final String query) throws Exception {
        boolean success = false;

        Connection connection = null;
        Statement preparedStatement = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.createStatement();
            success = preparedStatement.execute(query);
        } finally {
            closeStatement(preparedStatement);
            closeConnection(connection);
        }

        return success;
    }

    /**
     * Execute a query assuming it is a count query.
     * 
     * @param dataSource the datasource
     * @param query the query to execute
     * @return the count
     * @throws Exception thrown in case of any error
     */
    public static int executeCount(final DataSource dataSource, final String query) throws Exception {
        int count = 0;

        Connection connection = null;
        Statement preparedStatement = null;
        ResultSet resultSet = null;
        try {
            connection = dataSource.getConnection();
            preparedStatement = connection.createStatement();
            resultSet = preparedStatement.executeQuery(query);
            if (resultSet.next()) {
                count = resultSet.getInt(1);
            }
        } finally {
            closeResultSet(resultSet);
            closeStatement(preparedStatement);
            closeConnection(connection);
        }

        return count;
    }

    /**
     * Gets from the given {@link datasource} the URL of the database connection.
     * 
     * @param dataSource the datasource
     * @return the URL of the connection to the database
     * @throws CellarConfigurationException the cellar configuration exception
     * @throws SQLException a SQM excpetion thrown in case a problem occurs while checking the database connection URL
     */
    public static String getDatabaseUrl(final DataSource dataSource) throws SQLException {
        String databaseUrl = null;

        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            databaseUrl = conn.getMetaData().getURL();
        } finally {
            JdbcUtils.closeConnection(conn);
        }

        return databaseUrl;
    }

    /**
     * Gets from the given {@link datasource} the user of the database connection.
     * 
     * @param dataSource the datasource
     * @return the user of the connection to the database
     * @throws CellarConfigurationException the cellar configuration exception
     * @throws SQLException a SQM excpetion thrown in case a problem occurs while checking the database connection URL
     */
    public static String getDatabaseUser(final DataSource dataSource) throws SQLException {
        String databaseUser = null;

        Connection conn = null;
        try {
            conn = dataSource.getConnection();
            databaseUser = conn.getMetaData().getUserName();
        } finally {
            JdbcUtils.closeConnection(conn);
        }

        return databaseUser;
    }

    /**
     * Resolve the datasource out of the given {@link dataSourceName}.
     *
     * @param dataSourceName the data source name
     * @throws NamingException the naming exception
     * @return the javax.sql.DataSource object
     */
    public static DataSource resolveDataSource(final String dataSourceName) throws NamingException {
        final DataSource dataSource = (DataSource) new InitialContext().lookup("java:comp/env/" + dataSourceName);
        return dataSource;
    }

}
