package eu.europa.ec.opoce.cellar.nal.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * <p>Abstract InClauseUtils class.</p>
 */
public abstract class InClauseUtils {

    /**
     * Constant <code>INCLAUSE_BLOCKS={
     * 1,
     * 2,
     * 3,
     * 4,
     * 5,
     * 10,
     * 15,
     * 20,
     * 25,
     * 30,
     * 35,
     * 40
     * }</code>
     */
    private static final int INCLAUSE_BLOCKS[] = {
            1, 2, 3, 4, 5, 10, 15, 20, 25, 30, 35, 40};

    /**
     * <p>getInClauseLimit.</p>
     *
     * @param ids a {@link java.util.Collection} object.
     * @return a int.
     */
    public static int getInClauseLimit(Collection<String> ids) {
        return getInClauseLimit(ids.size());
    }

    /**
     * <p>getInClauseLimit.</p>
     *
     * @param numberOfIds a int.
     * @return a int.
     */
    public static int getInClauseLimit(int numberOfIds) {
        int limit = 0;
        for (int block : INCLAUSE_BLOCKS) {
            if (numberOfIds <= block) {
                limit = block;
                break;
            }
        }

        if (limit == 0) {
            // list size is more than the defined block size, so return max block defined
            limit = INCLAUSE_BLOCKS[INCLAUSE_BLOCKS.length - 1];
        }

        return limit;
    }

    /**
     * <p>getIdsPerInClause.</p>
     *
     * @param ids a {@link java.util.Collection} object.
     * @return a {@link java.util.List} object.
     */
    public static List<List<String>> getIdsPerInClause(Collection<String> ids) {
        List<List<String>> idBlocks = new ArrayList<List<String>>();
        if (ids == null || ids.isEmpty()) {
            return idBlocks;
        }

        int inClauseLimit = getInClauseLimit(ids);
        ids = new ArrayList<String>(ids);

        int size = ids.size();
        int rest = size % inClauseLimit;

        if (size < inClauseLimit) {
            for (int i = rest; i != 0 && i < inClauseLimit; i++) {
                ids.add(((List<String>) ids).get(size - 1));
            }
        } else if (rest != 0) {
            int newLimit = getInClauseLimit(rest);
            for (int i = rest % newLimit; i != 0 && i < newLimit; i++) {
                ids.add(((List<String>) ids).get(size - 1));
            }
        }

        int from = 0;
        int to = from + inClauseLimit;
        while (to <= ids.size()) {
            idBlocks.add(((List<String>) ids).subList(from, to));
            from = to;
            if (from == ids.size()) {
                break;
            }

            to = from + inClauseLimit;
            if (to > ids.size()) {
                to = ids.size();
            }
        }

        return idBlocks;
    }

}
