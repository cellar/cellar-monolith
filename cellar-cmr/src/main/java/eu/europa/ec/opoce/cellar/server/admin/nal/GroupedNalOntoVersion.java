package eu.europa.ec.opoce.cellar.server.admin.nal;

public class GroupedNalOntoVersion {

    private final String name;
    private final String uri;
    private final NalOntoVersion nalOntoVersion;

    public GroupedNalOntoVersion(String name, String uri, NalOntoVersion nalOntoVersion) {
        this.name = name;
        this.uri = uri;
        this.nalOntoVersion = nalOntoVersion;
    }

    public NalOntoVersion getNalOntoVersion() {
        return nalOntoVersion;
    }

    public String getName() {
        return name;
    }

    public String getUri() {
        return uri;
    }
}
