package eu.europa.ec.opoce.cellar.cmr.service.impl;

import eu.europa.ec.opoce.cellar.cmr.service.StructuredDynamicQueryConfiguration;
import eu.europa.ec.opoce.cellar.common.Placeholder;
import eu.europa.ec.opoce.cellar.common.util.PlaceHolderUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.Syntax;

/**
 * The Class StructuredDynamicQueryConfigurationImpl.
 */
@Service
public class StructuredDynamicQueryConfigurationImpl implements StructuredDynamicQueryConfiguration {

    /** The dynamic queries. */
    @Autowired
    @Qualifier("dynamicQueryStringsMap")
    private HashMap<String, String> dynamicQueryStringsMap;

    /** {@inheritDoc} */
    @Override
    public Query getQueryForOjOfSeries(final Map<Pattern, Object> placeHoldersMap) {
        return this.resolveQuery("ojOfSeries", placeHoldersMap);
    }

    /** {@inheritDoc} */
    @Override
    public Query getQueryForOjOfTheDay(final Map<Pattern, Object> placeHoldersMap) {
        return this.resolveQuery("ojOfTheDay", placeHoldersMap);
    }

    /** {@inheritDoc} */
    @Override
    public Query getQueryForTheMostRecentWorkDateOfOj(final Map<Pattern, Object> placeHoldersMap) {
        return this.resolveQuery("lastOjDateOfTheYear", placeHoldersMap);
    }

    /** {@inheritDoc} */
    @Override
    public Query getQueryToListAllSeriesOfOj(final Map<Pattern, Object> placeHoldersMap) {
        return this.resolveQuery("allOjSeriesForTheYear", placeHoldersMap);
    }

    /** {@inheritDoc} */
    @Override
    public Query getQueryToListAllSubSeriesOfOj(final Map<Pattern, Object> placeHoldersMap) {
        return this.resolveQuery("allOjSubSeriesForTheYear", placeHoldersMap);
    }

    /**
     * Resolve query.
     * This method will dynamically create the query replacing its values
     * This service uses the dynamicQueries.xml context that holds the queries that should be "dynamic" by the use of place-holders values
     * when passing the place-holders to the service StructuredDynamicQueryConfiguration one can pass :
     * Strings and Dates for the direct translation or a Map
     * This Map will reference sub conditions (useful when a condition is not mandatory, it can be replaced by "")
     * This Map has also to contain the name of the sub-query (key = "sub_query_name")
     * When using a map as a place-holder value the name of the parameter in the main query must match the name of the condition string bean
     * @param name the name
     * @param placeHoldersMap the place holders map
     * @return the query
     */
    private Query resolveQuery(final String name, final Map<Pattern, Object> placeHoldersMap) {
        String queryString = this.dynamicQueryStringsMap.get(name);
        for (final Map.Entry<Pattern, Object> entry : placeHoldersMap.entrySet()) {
            final Pattern patternToReplace = entry.getKey();
            final Object valueToReplace = placeHoldersMap.get(patternToReplace);
            final List<Placeholder> placeholders = PlaceHolderUtils.getPlaceholders(queryString, patternToReplace);

            if (valueToReplace instanceof Date) {
                // a special format is used to pass formatter patterns e.g.: ${end_date "yyyy-MM-dd"}
                queryString = PlaceHolderUtils.replacePlaceholdersByDate(queryString, placeholders, (Date) valueToReplace);
            } else if (valueToReplace instanceof Map) {
                //a query may have sub-queries
                @SuppressWarnings({
                        "unchecked"})
                final Map<Pattern, Object> conditions = (Map<Pattern, Object>) valueToReplace;
                final String sub_query_name = (String) conditions.get(RetrieveOJListServiceImpl.SUB_QUERY_NAME_PATTERN);
                conditions.remove(RetrieveOJListServiceImpl.SUB_QUERY_NAME_PATTERN);
                String subQueryString = this.dynamicQueryStringsMap.get(sub_query_name);
                for (final Map.Entry<Pattern, Object> subQueriesEntry : conditions.entrySet()) {
                    final Pattern patternToReplaceInSubQuery = subQueriesEntry.getKey();
                    final Object valueToReplaceInSubQuery = conditions.get(patternToReplaceInSubQuery);
                    final List<Placeholder> subQueryPlaceholders = PlaceHolderUtils.getPlaceholders(subQueryString,
                            patternToReplaceInSubQuery);
                    if (valueToReplaceInSubQuery instanceof Date) {
                        // a special format is used to pass formatter patterns e.g.: ${end_date "yyyy-MM-dd"}
                        subQueryString = PlaceHolderUtils.replacePlaceholdersByDate(subQueryString, subQueryPlaceholders,
                                (Date) valueToReplaceInSubQuery);
                    } else {
                        final String valueToReplaceInSubQueryString = (String) valueToReplaceInSubQuery;
                        if (StringUtils.isNotBlank(valueToReplaceInSubQueryString)) {//if the condition is not mandatory it can be replaced by ""
                            subQueryString = PlaceHolderUtils.replacePlaceholders(subQueryString, subQueryPlaceholders,
                                    valueToReplaceInSubQueryString);
                        } else {
                            subQueryString = "";
                        }
                    }
                }
                queryString = PlaceHolderUtils.replacePlaceholders(queryString, placeholders, subQueryString);
            } else {
                queryString = PlaceHolderUtils.replacePlaceholders(queryString, placeholders, (String) valueToReplace);
            }
        }
        final Query result = QueryFactory.create(queryString, Syntax.syntaxARQ);
        return result;
    }

}
