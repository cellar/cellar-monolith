package eu.europa.ec.opoce.cellar.ontology.service.impl;

import com.google.common.base.MoreObjects;

public class ClearOntologyCacheEvent {

    private final String pid;

    public ClearOntologyCacheEvent(String pid) {
        this.pid = pid;
    }

    public String getPid() {
        return pid;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("pid", pid)
                .toString();
    }
}
