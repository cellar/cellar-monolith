/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.modelload.service.impl
 *             FILE : IModelLoadRequestScheduler.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Mar 18, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.virtuosobackup.service;

import eu.europa.ec.opoce.cellar.exception.VirtuosoBackupException;
import eu.europa.ec.opoce.cellar.exception.VirtuosoOperationException;
import eu.europa.ec.opoce.cellar.virtuosobackup.BackupType;
import eu.europa.ec.opoce.cellar.virtuosobackup.VirtuosoBackup;
import eu.europa.ec.opoce.cellar.virtuosobackup.VirtuosoBackup.OperationType;

import java.util.Collection;
import java.util.Date;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Mar 18, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IVirtuosoBackupService {

    /**
     * Find resyncables sorted by insertion date asc.
     *
     * @param limit the limit
     * @return the collection
     */
    Collection<VirtuosoBackup> findResyncablesSortedByInsertionDateAsc(final int limit);

    /**
     * Count all resyncable.
     *
     * @return the long
     */
    long countAllResyncable();

    /**
     * Find last backup.
     *
     * @param cellarId the cellar id
     * @return the virtuoso backup
     */
    VirtuosoBackup findLastBackup(final String cellarId);

    /**
     * Delete.
     *
     * @param virtuosoBackup the virtuoso backup
     * @throws VirtuosoBackupException the virtuoso backup exception
     */
    void delete(final VirtuosoBackup virtuosoBackup) throws VirtuosoBackupException;

    /**
     * Populate virtuoso backup.
     *
     * @param cellarId the cellar id
     * @param model the model
     * @param productionIds the production ids
     * @param insertionDate the insertion date
     * @param operationType the operation type
     * @param isResyncable the is resyncable
     * @param reason the reason
     * @throws VirtuosoBackupException the virtuoso backup exception
     */
    void populateVirtuosoBackup(final String cellarId, final BackupType backupType, final String model,
            final Collection<String> productionIds, final Date insertionDate, final OperationType operationType, final boolean isResyncable,
            final String reason) throws VirtuosoBackupException;

    /**
     * Populates the Virtuoso backup table
     * @param uri the Ontology or NAL's URI
     * @param backupType the backup type (digital object, NAL, or ontology)
     * @param model the model to backup
     * @param insertionDate the date-time at which this entry is being created within the backup table
     * @param operationType the operation type
     * @param isResyncable indicates whether Virtuoso is available or not
     * @param reason the initial reason/cause why Virtuoso failed on insertion
     * @throws VirtuosoBackupException
     */
    void populateVirtuosoBackup(final String uri, final BackupType backupType, final String model, final String skosModel,
            final String ruleSetQuery, final Date insertionDate, final OperationType operationType, final boolean isResyncable,
            final String reason) throws VirtuosoBackupException;

    /**
     * Populate virtuoso backup if already backed up.
     *
     * @param cellarId the cellar id
     * @param backupType the type of backed up object
     * @param model the model
     * @param productionIds the production ids
     * @param insertionDate the insertion date
     * @param operationType the operation type
     * @return true, if successful
     * @throws VirtuosoBackupException the virtuoso backup exception
     */
    boolean populateVirtuosoBackupIfAlreadyBackedup(final String cellarId, final BackupType backupType, final String model,
            final Collection<String> productionIds, final Date insertionDate, final OperationType operationType)
                    throws VirtuosoBackupException;

    /**
     * Processes the virtuoso backup, i.e. attempts to store a VirtuosoBackup object into the Virtuoso instance
     *
     * @param virtuosoBackup the virtuoso backup to process
     * @param hasRetryAttempts true if retry attempts are available, false when retry attempts are exceeded
     * @throws VirtuosoOperationException the virtuoso operation exception
     */
    void processVirtuosoBackup(final VirtuosoBackup virtuosoBackup, boolean hasRetryAttempts) throws VirtuosoOperationException;
}
