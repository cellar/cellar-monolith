/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.support.dissemination
 *             FILE : EncodedIdentifier.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 16, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination;

import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Apr 16, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class EncodedIdentifier {

    private final String prefix;
    private final String identifier;

    private final CellarResourceBean cellarResource;

    public EncodedIdentifier(final String prefix, final String identifier, final CellarResourceBean cellarResource) {
        this.prefix = prefix;
        this.identifier = identifier;
        this.cellarResource = cellarResource;
    }

    /**
     * @return the prefix
     */
    public String getPrefix() {
        return this.prefix;
    }

    /**
     * @return the identifier
     */
    public String getIdentifier() {
        return this.identifier;
    }

    /**
     * @return the cellarResource
     */
    public CellarResourceBean getCellarResource() {
        return this.cellarResource;
    }
}
