/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice.core
 *             FILE : ShouldExpandPropertyVisitor.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 24 Apr 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.notice.core;

import eu.europa.ec.opoce.cellar.semantic.ontology.data.OntologyData;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;

/**
 * @author ARHS Developments
 */
public class ShouldExpandPropertyVisitor implements NoticeTypeVisitor<Boolean> {

    private String currentProperty;

    private final OntologyData ontologyData;

    public ShouldExpandPropertyVisitor(OntologyData ontologyData) {
        this.ontologyData = ontologyData;
    }

    public void setCurrentProperty(String currentProperty) {
        this.currentProperty = currentProperty;
    }

    @Override
    public Boolean visitBranch() {
        return CellarProperty.cdm_work_has_expression.equals(this.currentProperty)
                || getOntologyData().getEmbeddingNotices().contains(this.currentProperty);
    }

    @Override
    public Boolean visitIdentifier() {
        return getOntologyData().getEmbeddingNotices().contains(this.currentProperty);
    }

    @Override
    public Boolean visitIndexing() {
        return CellarProperty.cdm_work_has_expression.equals(this.currentProperty)
                || getOntologyData().getEmbeddingNotices().contains(this.currentProperty);
    }

    @Override
    public Boolean visitObject() {
        return getOntologyData().getEmbeddingNotices().contains(this.currentProperty);
    }

    @Override
    public Boolean visitTree() {
        return getOntologyData().getEmbeddingNotices().contains(this.currentProperty);
    }

    @Override
    public Boolean visitEmbeddedNotice() {
        return false;
    }

    public OntologyData getOntologyData() {
        return ontologyData;
    }
}
