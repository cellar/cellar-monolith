/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.service
 *             FILE : EmbargoOperationValidationService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 30 Jan 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.service;

/**
 * <class_description> This service holds the validation methods used during the (dis)embargo operation.
 * <br/><br/>
 * <notes> 
 * <br/><br/>
 * ON : 30 Jan 2017
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface EmbargoOperationValidationService {

    /**
     * Pre validation.
     *
     * @param embargoOperation the embargo operation
     */
    void preValidation(EmbargoOperation embargoOperation);

    /**
     * Post loading validation.
     *
     * @param embargoOperation the embargo operation
     */
    void postLoadingValidation(EmbargoOperation embargoOperation);

    /**
     * Post direction set validation.
     *
     * @param embargoOperation the embargo operation
     */
    void postDirectionSetValidation(EmbargoOperation embargoOperation);

}
