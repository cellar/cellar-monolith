/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.ingest.impl
 *             FILE : StructMapLoader.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 07-01-2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.ingest.impl;

import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;
import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.PrefixConfigurationService;
import eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData;
import eu.europa.ec.opoce.cellar.cmr.ingest.IStructMapLoader;
import eu.europa.ec.opoce.cellar.cmr.model.IFacetedModel;
import eu.europa.ec.opoce.cellar.cmr.service.EmbargoService;
import eu.europa.ec.opoce.cellar.cmr.transformers.DigitalObject2CellarUri;
import eu.europa.ec.opoce.cellar.domain.content.mets.CellarIdentifiedObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentStream;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.monitoring.Watch;
import eu.europa.ec.opoce.cellar.semantic.helper.DateFormats;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaQueries;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarAnnotationProperty;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarType;
import eu.europa.ec.opoce.cellar.server.service.MimeTypeResolutionService;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.collections15.ListUtils;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * <class_description> Base implementation of the service for loading existing data.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 07-01-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class StructMapLoader implements IStructMapLoader {

    /** The prefix configuration service. */
    @Autowired(required = true)
    private PrefixConfigurationService prefixConfigurationService;

    /** The embargo handling service. */
    @Autowired
    private EmbargoService embargoHandlingService;

    /** The mime type resolution service. */
    @Autowired
    private MimeTypeResolutionService mimeTypeResolutionService;

    /**
     * Load.
     *
     * @param data the data
     * @return the struct map
     * @see eu.europa.ec.opoce.cellar.cmr.ingest.IStructMapLoader#load(eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData)
     */
    @Override
    public StructMap load(final CalculatedData data) {
        final StructMap structMap = data.getStructMap();
        final ContentIdentifier cellarId = structMap.getDigitalObject().getCellarId();

        // build and return a struct map on the basis of the loaded data
        final IFacetedModel existingFullModel = data.getExistingDirectAndInferredModel();
        final String cellarUri = cellarId.getUri();
        final Resource cellarResource = ResourceFactory.createResource(cellarUri);
        if (!existingFullModel.asModel().containsResource(cellarResource)) {
            return null;
        }

        // get cellar resources in a map
        final SortedMap<String, Resource> uris = new TreeMap<String, Resource>();
        for (final Statement statement : existingFullModel.asModel().listStatements().toList()) {
            final Resource subject = statement.getSubject();
            if (!subject.isURIResource() || !(subject.getURI().equals(cellarUri) || subject.getURI().startsWith(cellarUri + '.'))) {
                continue;
            }
            uris.put(subject.getURI(), subject);
        }

        // create a new structmap with the original loaded data
        final StructMap result = new StructMap(structMap.getMetsDocument());
        result.setDigitalObject(this.create(result, cellarUri, uris));
        return result;
    }

    /**
     * Load and merge.
     *
     * @param data the data
     * @return the struct map
     * @see eu.europa.ec.opoce.cellar.cmr.ingest.IStructMapLoader#loadAndMerge(eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData)
     */
    @Override
    public StructMap loadAndMerge(final CalculatedData data) {
        final StructMap ingestedStructMap = data.getStructMap();
        final List<DigitalObject> addedDigitalObject = new ArrayList<DigitalObject>();

        // load the old structure from the database
        final StructMap existingStructMap = this.load(data);
        if (null == existingStructMap) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withCode(CmrErrors.INGESTION_SERVICE_ERROR)
                    .withMessage("Update for structMap id '{}' requested but not found db")
                    .withMessageArgs(ingestedStructMap.getId())
                    .build();
        }

        final DigitalObject originalWork = existingStructMap.getDigitalObject();
        embargoHandlingService.validateEmbargoDate(ingestedStructMap);

        final DigitalObject ingestedWork = ingestedStructMap.getDigitalObject();
        final boolean isContentUpdate = isContentUpdate(ingestedStructMap);
        // merge the old and new data so that missing parts are not forgotten in inner links
        merge(originalWork, ingestedWork, false, isContentUpdate);

        // fill in a list of newly added digital objects
        final Collection<DigitalObject> newDigitalObjects = getNewDigitalObjects(ingestedWork, originalWork);
        addedDigitalObject.addAll(newDigitalObjects);
        data.setAddedDigitalObjects(addedDigitalObject);
        return existingStructMap;
    }

    /**
     * Load and combine.
     *
     * @param data the data
     * @return the struct map
     * @see eu.europa.ec.opoce.cellar.cmr.ingest.IStructMapLoader#loadAndCombine(eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData)
     */
    @Watch(value = "StructMap Loader")
    @Override
    public StructMap loadAndCombine(final CalculatedData data) {
        final StructMap structMap = data.getStructMap();
        final List<DigitalObject> addedDigitalObject = new ArrayList<DigitalObject>();

        //load the old structure from the database
        final StructMap existingStructMap = this.load(data);

        if (existingStructMap == null) {
            // fill in a list of all newly added digital objects
            final List<DigitalObject> allChilds = structMap.getDigitalObject().getAllChilds(true);
            addedDigitalObject.addAll(allChilds);
        } else {
            // confirm there are only new digital objects, fill in missing parts and merge the production system uris so that this list is compete
            merge(existingStructMap.getDigitalObject(), structMap.getDigitalObject(), true, isContentUpdate(structMap));
            // fill in a list of all newly added digital objects
            addedDigitalObject.addAll(getNewDigitalObjects(structMap.getDigitalObject(), existingStructMap.getDigitalObject()));
        }
        data.setAddedDigitalObjects(addedDigitalObject);

        return existingStructMap;
    }

    /**
     * Load and clean.
     *
     * @param data the data
     * @return the struct map
     * @see eu.europa.ec.opoce.cellar.cmr.ingest.IStructMapLoader#loadAndClean(eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData)
     */
    @Override
    public StructMap loadAndClean(final CalculatedData data) {
        final StructMap structMap = data.getStructMap();
        final ContentIdentifier cellarId = structMap.getDigitalObject().getCellarId();
        final Collection<DigitalObject> removedDigitalObjects = data.getRemovedDigitalObjects();

        // Build and return a structMap on the basis of the loaded data
        final IFacetedModel existingFullModel = data.getExistingDirectAndInferredModel();
        final String cellarUri = cellarId.getUri();
        final Resource cellarResource = ResourceFactory.createResource(cellarUri);
        if (!existingFullModel.asModel().containsResource(cellarResource)) {
            return null;
        }

        // Get CellarResources in a map, filter out the resources to delete
        final SortedMap<String, Resource> uris = new TreeMap<String, Resource>();
        for (final Statement statement : existingFullModel.asModel().listStatements().toList()) {
            final Resource subject = statement.getSubject();
            if (isIncludeUri(subject, cellarUri, removedDigitalObjects)) {
                uris.put(subject.getURI(), subject);
            }
        }

        // Create a new structMap with the cleaned original data
        final StructMap cleanedStructMap = new StructMap(structMap.getMetsDocument());
        cleanedStructMap.setDigitalObject(this.create(cleanedStructMap, cellarUri, uris));
        return cleanedStructMap;
    }

    /**
     * Checks if is include uri.
     *
     * @param subject the subject
     * @param cellarUri the cellar uri
     * @param digitalObjectsToRemove the digital objects to remove
     * @return true, if is include uri
     */
    private static boolean isIncludeUri(final Resource subject, final String cellarUri,
            final Collection<DigitalObject> digitalObjectsToRemove) {
        final String subjectUri = subject.getURI();
        if (!subject.isURIResource() || !(subject.getURI().equals(cellarUri) || subject.getURI().startsWith(cellarUri + '.'))) {
            return false;
        } else {
            for (final DigitalObject doToRemove : digitalObjectsToRemove) {
                final String cellarUriToRemove = doToRemove.getCellarId().getUri();
                if (StringUtils.containsIgnoreCase(subjectUri, cellarUriToRemove)) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * <p>getNewDigitalObjects gives a collection of all digital objects that are added with this new vesion.</p>
     *
     * @param newTopDigitalObject a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject} object.
     * @param oldTopDigitalObject a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject} object.
     * @return a {@link java.util.Collection} object.
     */
    private static Collection<DigitalObject> getNewDigitalObjects(final DigitalObject newTopDigitalObject,
            final DigitalObject oldTopDigitalObject) {
        final Collection<DigitalObject> addedDigitalObjects = new ArrayList<DigitalObject>();
        final List<DigitalObject> oldChilds = oldTopDigitalObject.getAllChilds(true);
        final Transformer<DigitalObject, String> instance = DigitalObject2CellarUri.instance;
        final Set<String> oldCellarUris = CollectionUtils.collect(oldChilds, instance, new HashSet<String>());
        final List<DigitalObject> newChilds = newTopDigitalObject.getAllChilds(true);
        for (final DigitalObject newDigitalObject : newChilds) {
            final ContentIdentifier cellarId = newDigitalObject.getCellarId();
            final String uri = cellarId.getUri();
            if (oldCellarUris.contains(uri)) {
                continue;
            }
            addedDigitalObjects.add(newDigitalObject);
        }
        return addedDigitalObjects;
    }

    /**
     * <p>isContentUpdate.</p>
     *
     * @param structMap a {@link eu.europa.ec.opoce.cellar.domain.content.mets.StructMap} object.
     * @return a boolean.
     */
    private static boolean isContentUpdate(final StructMap structMap) {
        return structMap.getContentOperationType() != null;
    }

    /**
     * <p>merge makes a combination of the new and old digital objects if allowed, otherwise an error will be thrown.</p>
     * <p>the merge makes sure that the cmr has a complet picture of the existing and new digital objects, so that inner relations can be updated accordingly</p>
     *
     * @param source          a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject} object.
     * @param destination     a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject} object.
     * @param onlyStructure   a boolean.
     * @param isContentUpdate a boolean.
     */
    private static void merge(final DigitalObject source, final DigitalObject destination, final boolean onlyStructure,
            final boolean isContentUpdate) {
        // if only structure checks that there is nothing new define for the new destination digital object
        if (onlyStructure) {
            if (destination.getBusinessMetadata() != null) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.MERGE_FAILED)
                        .withMessage("there is data that needs to be merged between [{}] and [{}], this is not allowed")
                        .withMessageArgs(StringUtils.join(destination.getAllUris(), ", "), StringUtils.join(source.getAllUris(), ", "))
                        .build();
            }
            //exception when a new uri for this digital object is defined
            if (!CollectionUtils.isSubCollection(destination.getAllUris(), source.getAllUris())) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.MERGE_FAILED)
                        .withMessage("it is not allowed a new sameAs URIs at work level in a package create-append:[{}] given:[{}]")
                        .withMessageArgs(source.getAllUris(), destination.getAllUris()).build();
            }
        }
        //merge uris so that all possible uris are available
        destination.setContentids(ListUtils.union(destination.getContentids(), source.getContentids()));
        // merge old and new contentstream ids if there is no change allowed on that level
        if (!isContentUpdate) {
            final List<ContentStream> destinationContentStreams = destination.getContentStreams();
            final List<ContentStream> sourceContentStreams = source.getContentStreams();
            if (destinationContentStreams.isEmpty() && !sourceContentStreams.isEmpty()) {
                destinationContentStreams.addAll(sourceContentStreams);
            } else {
                final List<ContentStream> streamsNotPresentInDestination = new ArrayList<ContentStream>();
                for (final ContentStream sourceContentStream : sourceContentStreams) {
                    final ContentIdentifier sourceCellarId = sourceContentStream.getCellarId();
                    final String sourceIdentifier = sourceCellarId.getIdentifier();
                    boolean found = false;
                    for (final ContentStream destinationContentStream : destinationContentStreams) {
                        final ContentIdentifier destinaitonCellarId = destinationContentStream.getCellarId();
                        final String destinationIdentifier = destinaitonCellarId.getIdentifier();
                        //when the same content stream is found we merge the content ids
                        if (sourceIdentifier.equals(destinationIdentifier)) {
                            final List<ContentIdentifier> destinationContentids = destinationContentStream.getContentids();
                            final List<ContentIdentifier> sourceContentids = sourceContentStream.getContentids();
                            final List<ContentIdentifier> union = ListUtils.union(destinationContentids, sourceContentids);
                            destinationContentStream.setContentids(union);
                            found = true;
                            continue;
                        }
                    }
                    //if source has a content stream not present in the destination
                    // add it later
                    if (!found) {
                        streamsNotPresentInDestination.add(sourceContentStream);
                    }
                }
                // add content streams that were present in the past
                for (final ContentStream contentStream : streamsNotPresentInDestination) {
                    source.addContentStream(contentStream);
                }

            }
        }
        // make sure all child digital objects are found, recursively merged, or added, from earlier ingest, in the destination structure
        final List<DigitalObject> childObjects = source.getChildObjects();
        for (final DigitalObject sourceChild : childObjects) {
            final ContentIdentifier sourceCellarId = sourceChild.getCellarId();
            final String sourceChildUri = sourceCellarId.getUri();
            boolean foundChild = false;
            final List<DigitalObject> childObjects2 = destination.getChildObjects();
            for (final DigitalObject destinationChild : childObjects2) {
                final ContentIdentifier destinationCellarId = destinationChild.getCellarId();
                final String uri = destinationCellarId.getUri();
                if (!sourceChildUri.equals(uri)) {
                    continue;
                }
                merge(sourceChild, destinationChild, onlyStructure, isContentUpdate);
                foundChild = true;
                break;
            }
            if (foundChild) {
                continue;
            }
            destination.addChildObject(sourceChild);
        }
    }

    /**
     * <p>create fill in the complete structmap starting from the top uri and a map of uri and resources.</p>
     *
     * @param structMap a {@link eu.europa.ec.opoce.cellar.domain.content.mets.StructMap} object.
     * @param manifestationUri       a {@link java.lang.String} object.
     * @param uris      a {@link java.util.SortedMap} object that contains uris with the corresponding jena-resource.
     * @return a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject} object.
     */
    private DigitalObject create(final StructMap structMap, final String manifestationUri, final SortedMap<String, Resource> uris) {
        //create digital object in the structmap
        final DigitalObject digitalObject = new DigitalObject(structMap);
        // fill in top digital object
        this.setContentIdentifiers(manifestationUri, uris.get(manifestationUri), digitalObject);
        final Resource resource = uris.get(manifestationUri);
        final List<Resource> sameAsUris = JenaQueries.getResources(resource, OWL.sameAs, false);
        final DigitalObjectType digitalObjectType = findType(resource, sameAsUris);
        if (null != digitalObjectType) {
            digitalObject.setType(digitalObjectType);
        }

        final String lastModificationString = JenaQueries.getLiteralValue(resource, CellarProperty.cmr_lastmodificationdateP, true, false);
        final Date lastModificationDate = DateFormats.parse(lastModificationString, DateFormats.Format.FULLXMLDATETIME);
        digitalObject.setLastModificationDate(lastModificationDate);

        final Boolean readOnly = JenaQueries.getBooleanValue(resource, CellarAnnotationProperty.annotation_read_onlyP, false, false, true);
        digitalObject.setReadOnly(readOnly);

        //add content streams to the created digital object

        uris.entrySet().forEach(entry -> {
            final String contentStreamUri = entry.getKey();
            final Resource contentStreamResource = entry.getValue();
            //is an ITEM "...cellar/xxx/DOC_1" and does not have another slash after the "DOC_1"
            if (contentStreamUri.startsWith(manifestationUri + '/')
                    && (contentStreamUri.indexOf('/', manifestationUri.length() + 1) == -1)) {
                final ContentStream createContentStream = this.createContentStream(contentStreamUri, contentStreamResource, resource,
                        sameAsUris);
                digitalObject.addContentStream(createContentStream);
            }
        });

        // add child-digital objects by running this method recursively
        uris.entrySet().forEach(entry -> {
            final String contentStreamUri = entry.getKey();
            if (contentStreamUri.startsWith(manifestationUri + '.') && (contentStreamUri.indexOf('.', manifestationUri.length() + 1) == -1)
                    && (contentStreamUri.indexOf('/', manifestationUri.length() + 1) == -1)) {
                digitalObject.addChildObject(this.create(structMap, contentStreamUri, uris));
            }
        });
        return digitalObject;
    }

    /**
     * <p>createContentStream create a contentstream instance for a contentstream with a given uri.</p>
     *
     * @param contentStreamUri the content stream uri
     * @param contentStreamResource the content stream resource
     * @param manifestationResource the manifestation resource
     * @param sameAsUris the same as uris
     * @return a {@link eu.europa.ec.opoce.cellar.domain.content.mets.ContentStream} object.
     */
    private ContentStream createContentStream(final String contentStreamUri, final Resource contentStreamResource,
            final Resource manifestationResource, final List<Resource> sameAsUris) {
        //get mimetype
        final String mimeType = mimeTypeResolutionService.resolve(contentStreamResource, manifestationResource, sameAsUris);

        // create contentstream
        final ContentStream contentStream = new ContentStream(mimeType, null);
        //set identifiers
        this.setContentIdentifiers(contentStreamUri, contentStreamResource, contentStream);

        final Boolean readOnly = JenaQueries.getBooleanValue(contentStreamResource, CellarAnnotationProperty.annotation_read_onlyP, false,
                false, true);
        contentStream.setReadOnly(readOnly);

        return contentStream;
    }

    /**
     * <p>setContentIdentifiers fills in the Cellar identifier object wit the cellarid and the corresponding same_as_ids.</p>
     *
     * @param uri                    a {@link java.lang.String} object.
     * @param resource the resource
     * @param cellarIdentifiedObject a {@link eu.europa.ec.opoce.cellar.domain.content.mets.CellarIdentifiedObject} object.
     */
    private void setContentIdentifiers(final String uri, final Resource resource, final CellarIdentifiedObject cellarIdentifiedObject) {
        //set cellar id
        cellarIdentifiedObject.setCellarId(new ContentIdentifier(this.prefixize(uri)));
        //list same_as resources
        final List<Resource> sameAsUris = JenaQueries.getResources(resource, OWL.sameAs, false);

        // convert all resource in contentIdentifiers
        final List<ContentIdentifier> contentIdentifiers = new ArrayList<ContentIdentifier>();
        for (final Resource sameAsUri : sameAsUris) {
            contentIdentifiers.add(new ContentIdentifier(this.prefixize(sameAsUri.getURI())));
        }
        // set all content identifiers
        cellarIdentifiedObject.setContentids(contentIdentifiers);
    }

    /**
     * <p>findType finds the type of a resource or the same_as resources.</p>
     *
     * @param resource   a {@link org.apache.jena.rdf.model.Resource} object.
     * @param sameAsUris a {@link java.util.List} object.
     * @return a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType} object.
     */
    private static DigitalObjectType findType(final Resource resource, final List<Resource> sameAsUris) {
        //try to find the digital objecttype from the cellar resource
        DigitalObjectType digitalObjectType = findDigitalObjectType(JenaQueries.getResourceUris(resource, RDF.type));
        if (null != digitalObjectType) {
            return digitalObjectType;
        }

        // try to find the digital objecttype from the same_as resources
        for (final Resource sameAsUri : sameAsUris) {
            digitalObjectType = findDigitalObjectType(JenaQueries.getResourceUris(sameAsUri, RDF.type));
            if (null != digitalObjectType) {
                return digitalObjectType;
            }
        }
        return null;
    }

    /**
     * <p>findDigitalObjectType converts the uri of the type in an digital object type.</p>
     *
     * @param types a {@link java.util.Collection} object.
     * @return a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType} object.
     */
    private static DigitalObjectType findDigitalObjectType(Collection<String> types) {
        if (!(types instanceof Set)) {
            types = new HashSet<String>(types);
        }
        if (types.contains(CellarType.cdm_workR.getURI())) {
            return DigitalObjectType.WORK;
        }
        if (types.contains(CellarType.cdm_expressionR.getURI())) {
            return DigitalObjectType.EXPRESSION;
        }
        if (types.contains(CellarType.cdm_manifestationR.getURI())) {
            return DigitalObjectType.MANIFESTATION;
        }
        if (types.contains(CellarType.cdm_dossierR.getURI())) {
            return DigitalObjectType.DOSSIER;
        }
        if (types.contains(CellarType.cdm_eventR.getURI())) {
            return DigitalObjectType.EVENT;
        }
        if (types.contains(CellarType.cdm_agentR.getURI())) {
            return DigitalObjectType.AGENT;
        }
        if (types.contains(CellarType.cdm_topleveleventR.getURI())) {
            return DigitalObjectType.TOPLEVELEVENT;
        }
        return null;
    }

    /**
     * <p>prefixize converts the full uri of a digital object in the prefixed version.</p>
     *
     * @param uri a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    private String prefixize(final String uri) {
        for (final Map.Entry<String, String> entry : this.prefixConfigurationService.getPrefixUris().entrySet()) {
            final String namespace = entry.getValue();
            if (!uri.startsWith(namespace)) {
                continue;
            }
            return entry.getKey() + ':' + uri.substring(namespace.length());
        }
        throw ExceptionBuilder.get(CellarException.class).withMessage("No prefix found for URI in db: {}").withMessageArgs(uri).build();
    }
}
