package eu.europa.ec.opoce.cellar.database;

/**
 * Abstract base class to implement strategy pattern for
 * the database specific operation of well formatting database values
 */
public abstract class DatabaseStrategy {

    /**
     * Formats the given value to the database specific format
     *
     * @param value a {@link java.lang.Object} object.
     * @return the formatted value for a particular database
     */
    public abstract String format(Object value);
}
