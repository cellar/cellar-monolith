/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver
 *             FILE : ResourceRedirectAbstractResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 20-Jan-2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver;

import eu.europa.ec.opoce.cellar.ccr.service.LanguagesNalSkosLoaderService;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Expression;
import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;
import eu.europa.ec.opoce.cellar.server.service.DisseminationDbGateway;
import eu.europa.ec.opoce.cellar.server.service.DisseminationException;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.http.HttpStatus;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Collectors;

/**
 * <class_description> due to the fact that CELLAR should accept the ingestion of multi linguistic crossreferenced expressions the response can now return a Htttp-300 status listing the expressions.
 * <br/><br/>
 * <notes> .
 * <br/><br/>
 * ON : 20-Jan-2017
 * The Class ResourceRedirectAbstractResolver.
 *
 * @author ARHS Developments
 * @version $$Revision$$$
 */
@Configurable
public abstract class ResourceRedirectAbstractResolver extends AbstractResolver {

    /**
     * The targeted expressions linked to accept language.
     */
    protected List<CellarResourceBean> targetedExpressionsLinkedToAcceptLanguage;
    /**
     * The dissemination db gateway.
     */
    @Autowired
    protected DisseminationDbGateway disseminationDbGateway;
    /**
     * The accept language beans.
     */
    protected List<LanguageBean> acceptLanguageBeans;
    /**
     * The languages nal skos loader service.
     */
    @Autowired
    private LanguagesNalSkosLoaderService languagesNalSkosLoaderService;

    /**
     * Instantiates a new abstract resolver.
     */
    protected ResourceRedirectAbstractResolver() {
    }

    /**
     * Gets the alternates.
     *
     * @return the alternates
     */
    protected Set<String> getAlternates() {
        return targetedExpressionsLinkedToAcceptLanguage.stream()
                .map(expression -> this.disseminationDbGateway.getCellarIdentifiedObject(expression.getCellarId()).getCellarId().getUri())
                .collect(Collectors.toSet());
    }

    /**
     * Gets the language uris.
     *
     * @param expression the expression
     * @return the language uris
     */
    protected TreeSet<String> getLanguageUris(final CellarResourceBean expression) {
        return expression.getLanguages().stream()
                .map(lang -> this.languagesNalSkosLoaderService.getLanguageBeanByThreeCharCode(lang).getUri())
                .collect(Collectors.toCollection(TreeSet::new));
    }

    /**
     * Check if CELLAR should return multiple choice for expression.
     *
     * @return true, if there are multiple target expressions
     */
    protected boolean shouldReturnMultipleChoiceForExpression() {
        return !((targetedExpressionsLinkedToAcceptLanguage == null) ||
                targetedExpressionsLinkedToAcceptLanguage.isEmpty()
                || (targetedExpressionsLinkedToAcceptLanguage.size() == 1));
    }

    /**
     * Retrieve target expressions.
     *
     * @return the list
     */
    protected List<Expression> retrieveTargetExpressions() {
        List<CellarResourceBean> expressions;
        // accept languages must exist
        for (final LanguageBean language : this.acceptLanguageBeans) {
            //return the first expression that matches one of the accept languages header
            expressions = this.disseminationService.checkForChildExpression(this.cellarResource, language);
            if ((expressions != null) && !expressions.isEmpty()) {
                targetedExpressionsLinkedToAcceptLanguage = expressions;
                //the cellarResource should point to the requested resource - in this case the unique matching expression
                if (targetedExpressionsLinkedToAcceptLanguage.size() == 1) {
                    this.cellarResource = targetedExpressionsLinkedToAcceptLanguage.get(0);
                }
                return expressions.stream().map(expression -> {
                    Expression e = new Expression();
                    e.setLangs(new HashSet<>(Collections.singletonList(language)));
                    return e;
                }).collect(Collectors.toList());
            }
        }

        throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_ACCEPTABLE)
                .withMessage("Not found work ['{}'] + language(s) [{}]")
                .withMessageArgs(this.cellarResource.getCellarId(), StringUtils.join(this.acceptLanguageBeans, ", ")).build();
    }

}
