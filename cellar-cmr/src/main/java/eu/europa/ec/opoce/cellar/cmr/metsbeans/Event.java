package eu.europa.ec.opoce.cellar.cmr.metsbeans;

import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;

import java.util.Map;

public class Event extends HierarchyElement<Dossier, MetsElement> {

    private static final long serialVersionUID = -2136900045286926832L;

    public Event() {
        super(DigitalObjectType.EVENT);
    }

    public Event(Map<ContentType, String> versions) {
        super(DigitalObjectType.EVENT, versions);
    }

}
