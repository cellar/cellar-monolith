package eu.europa.ec.opoce.cellar.cmr.database.dao.spring;

import eu.europa.ec.opoce.cellar.cmr.daobeans.InverseRelation;
import eu.europa.ec.opoce.cellar.cmr.database.dao.InverseRelationDao;
import eu.europa.ec.opoce.cellar.common.util.Pair;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.helper.StringHelper;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

@Repository
public class SpringInverseRelationDao extends CmrSpringBaseDao<InverseRelation, Long> implements InverseRelationDao {

    /**
     * Constant <code>table="CMR_INVERSE_RELATIONS"</code>
     */
    private static final String table = "CMR_INVERSE_RELATIONS";

    /**
     * Constant <code>getBySourceId</code>
     */
    private static final String getBySourceId = StringHelper.format("{} = :sourceId ", Column.SOURCE_ID);

    /**
     * Constant <code>getBySourceIds</code>
     */
    private static final String getBySourceIds = StringHelper.format("{} in (:sourceIds)", Column.SOURCE_ID);

    /**
     * Constant <code>getBySourceIdsAndType</code>
     */
    private static final String getBySourceIdsAndType = StringHelper.format("{} = :sourceType and {} in (:sourceIds)", Column.SOURCE_TYPE,
            Column.SOURCE_ID);

    /**
     * Constant <code>getByTargetId</code>
     */
    private static final String getByTargetId = StringHelper.format("{} = :targetId ", Column.TARGET_ID);

    /**
     * Constant <code>getByTargetIds</code>
     */
    private static final String getByTargetIds = StringHelper.format("{} in (:targetIds)", Column.TARGET_ID);

    /**
     * Constant <code>getByTargetIdsAndType</code>
     */
    private static final String getByTargetIdsAndType = StringHelper.format("{} = :targetType and {} in (:targetIds)", Column.TARGET_TYPE,
            Column.TARGET_ID);

    private interface Column {

        String SOURCE_ID = "SOURCE_ID";
        String TARGET_ID = "TARGET_ID";
        String SOURCE_TYPE = "SOURCE_TYPE";
        String TARGET_TYPE = "TARGET_TYPE";
        String PROPERTIES_SOURCE_TARGET = "PROPERTIES_SOURCE_TARGET";
        String PROPERTIES_TARGET_SOURCE = "PROPERTIES_TARGET_SOURCE";
    }

    /**
     * <p>Constructor for SpringInverseRelationDao.</p>
     */
    public SpringInverseRelationDao() {
        super(table, Arrays.asList(Column.SOURCE_ID, Column.TARGET_ID, Column.SOURCE_TYPE, Column.TARGET_TYPE,
                Column.PROPERTIES_SOURCE_TARGET, Column.PROPERTIES_TARGET_SOURCE));
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("serial")
    @Override
    public Collection<InverseRelation> findRelationsBySource(final String sourcePsId) {
        if (StringUtils.isBlank(sourcePsId)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument status cannot be null").build();
        }

        return this.find(getBySourceId, new HashMap<String, Object>(1) {

            {
                this.put("sourceId", sourcePsId);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("serial")
    @Override
    public Collection<InverseRelation> findRelationsBySources(final Collection<String> sourcePsIds) {
        if (sourcePsIds == null || sourcePsIds.isEmpty()) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument status cannot be empty").build();
        }

        return this.find(getBySourceIds, new HashMap<String, Object>(1) {

            {
                this.put("sourceIds", sourcePsIds);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Pair<Collection<InverseRelation>, Boolean> findRelationsBySourcesAndType(final List<String> sourcePsIds,
            final DigitalObjectType doType, final int limit) {
        if (sourcePsIds == null || sourcePsIds.isEmpty()) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument status cannot be empty").build();
        }
        if (doType == null) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument status cannot be null").build();
        }

        Pair<Collection<InverseRelation>, Boolean> result = null;
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("sourceType", doType.toString());
        params.put("sourceIds", sourcePsIds);
        if (limit <= 0) {
            result = new Pair<Collection<InverseRelation>, Boolean>(this.find(getBySourceIdsAndType, params), false);
        } else {
            result = this.find(getBySourceIdsAndType, params, limit);
        }
        if (result.getOne() == null) {
            result = new Pair<Collection<InverseRelation>, Boolean>(Collections.<InverseRelation> emptyList(), false);
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("serial")
    @Override
    public Collection<InverseRelation> findRelationsByTarget(final String targetCellarId) {
        if (StringUtils.isBlank(targetCellarId)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument status cannot be null").build();
        }

        return this.find(getByTargetId, new HashMap<String, Object>(1) {

            {
                this.put("targetId", targetCellarId);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("serial")
    @Override
    public Collection<InverseRelation> findRelationsByTargets(final Collection<String> targetPsIds) {
        if (targetPsIds == null || targetPsIds.isEmpty()) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument status cannot be empty").build();
        }

        return this.find(getByTargetIds, new HashMap<String, Object>(1) {

            {
                this.put("targetIds", targetPsIds);
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Pair<Collection<InverseRelation>, Boolean> findRelationsByTargetsAndType(final List<String> targetPsIds,
            final DigitalObjectType doType, final int limit) {
        if (targetPsIds == null || targetPsIds.isEmpty()) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument status cannot be empty").build();
        }
        if (doType == null) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument status cannot be null").build();
        }

        Pair<Collection<InverseRelation>, Boolean> result = null;
        final Map<String, Object> params = new HashMap<String, Object>();
        params.put("targetType", doType.toString());
        params.put("targetIds", targetPsIds);
        if (limit <= 0) {
            result = new Pair<Collection<InverseRelation>, Boolean>(this.find(getByTargetIdsAndType, params), false);
        } else {
            result = this.find(getByTargetIdsAndType, params, limit);
        }
        if (result.getOne() == null) {
            result = new Pair<Collection<InverseRelation>, Boolean>(Collections.<InverseRelation> emptyList(), false);
        }
        return result;
    }

    @Override
    public InverseRelation create(final ResultSet rs) throws SQLException {
        return new InverseRelation() {

            {
                this.setSource(rs.getString(Column.SOURCE_ID));
                this.setTarget(rs.getString(Column.TARGET_ID));
                this.setSourceType(DigitalObjectType.getByValue(rs.getString(Column.SOURCE_TYPE)));
                this.setTargetType(DigitalObjectType.getByValue(rs.getString(Column.TARGET_TYPE)));
                this.setPropertiesSourceTarget(
                        new HashSet<String>(Arrays.asList(StringUtils.split(rs.getString(Column.PROPERTIES_SOURCE_TARGET)))));
                this.setPropertiesTargetSource(
                        new HashSet<String>(Arrays.asList(StringUtils.split(rs.getString(Column.PROPERTIES_TARGET_SOURCE)))));
            }
        };
    }

    @Override
    public void fillMap(final InverseRelation inverseRelation, final Map<String, Object> map) {
        map.put(Column.SOURCE_ID, inverseRelation.getSource());
        map.put(Column.TARGET_ID, inverseRelation.getTarget());
        map.put(Column.SOURCE_TYPE, inverseRelation.getSourceType().toString());
        map.put(Column.TARGET_TYPE, inverseRelation.getTargetType().toString());
        map.put(Column.PROPERTIES_SOURCE_TARGET, StringUtils.join(inverseRelation.getPropertiesSourceTarget(), " "));
        map.put(Column.PROPERTIES_TARGET_SOURCE, StringUtils.join(inverseRelation.getPropertiesTargetSource(), " "));
    }
}
