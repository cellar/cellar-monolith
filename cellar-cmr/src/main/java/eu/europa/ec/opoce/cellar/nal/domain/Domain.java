package eu.europa.ec.opoce.cellar.nal.domain;

import java.util.Arrays;
import java.util.Objects;

/**
 * <p>Domain class.</p>
 */
public class Domain {

    private Uri uri;
    private String identifier;
    private LanguageString[] labels;
    private Uri[] conceptSchemes;

    /**
     * <p>Constructor for Domain.</p>
     *
     * @param uri a {@link Uri} object.
     */
    public Domain(Uri uri) {
        this.uri = uri;
    }

    /**
     * <p>Setter for the field <code>identifier</code>.</p>
     *
     * @param identifier a {@link java.lang.String} object.
     */
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    /**
     * <p>Setter for the field <code>labels</code>.</p>
     *
     * @param labels an array of {@link LanguageString} objects.
     */
    public void setLabels(LanguageString[] labels) {
        this.labels = labels;
    }

    /**
     * <p>Setter for the field <code>conceptSchemes</code>.</p>
     *
     * @param conceptSchemes an array of {@link Uri} objects.
     */
    public void setConceptSchemes(Uri[] conceptSchemes) {
        this.conceptSchemes = conceptSchemes;
    }

    /**
     * <p>Getter for the field <code>uri</code>.</p>
     *
     * @return a {@link Uri} object.
     */
    public Uri getUri() {
        return uri;
    }

    /**
     * <p>Getter for the field <code>identifier</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getIdentifier() {
        return identifier;
    }

    /**
     * <p>Getter for the field <code>labels</code>.</p>
     *
     * @return an array of {@link LanguageString} objects.
     */
    public LanguageString[] getLabels() {
        return labels;
    }

    /**
     * <p>Getter for the field <code>conceptSchemes</code>.</p>
     *
     * @return an array of {@link Uri} objects.
     */
    public Uri[] getConceptSchemes() {
        return conceptSchemes;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Domain)) return false;
        Domain domain = (Domain) o;
        return Objects.equals(getUri(), domain.getUri()) &&
                Objects.equals(getIdentifier(), domain.getIdentifier()) &&
                Arrays.equals(getLabels(), domain.getLabels()) &&
                Arrays.equals(getConceptSchemes(), domain.getConceptSchemes());
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(getUri(), getIdentifier());
        result = 31 * result + Arrays.hashCode(getLabels());
        result = 31 * result + Arrays.hashCode(getConceptSchemes());
        return result;
    }

    @Override
    public String toString() {
        return "Domain{" +
                "uri=" + uri +
                ", identifier='" + identifier + '\'' +
                ", labels=" + Arrays.toString(labels) +
                ", conceptSchemes=" + Arrays.toString(conceptSchemes) +
                '}';
    }
}
