/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ontology
 *             FILE : DatabaseCatalogResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 01 12, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-01-12 13:22:25 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ontology.service.impl;

import eu.europa.ec.opoce.cellar.cmr.database.dao.OntoConfigDao;
import eu.europa.ec.opoce.cellar.common.CatalogResolver;
import eu.europa.ec.opoce.cellar.ontology.dao.OntoConfig;
import eu.europa.ec.opoce.cellar.ontology.exception.OntologyNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author ARHS Developments
 * @since 7.7
 */
@Component
public class DatabaseCatalogResolver implements CatalogResolver {

    private final OntoConfigDao ontoConfigDao;

    @Autowired
    public DatabaseCatalogResolver(OntoConfigDao ontoConfigDao) {
        this.ontoConfigDao = ontoConfigDao;
    }

    @Override
    public String resolve(String key) {
        final String uri = key.replaceAll("#", "");
        return ontoConfigDao.findByUri(uri)
                .map(OntoConfig::getExternalPid)
                .orElseThrow(() -> new OntologyNotFoundException("No PID found for " + uri));
    }
}
