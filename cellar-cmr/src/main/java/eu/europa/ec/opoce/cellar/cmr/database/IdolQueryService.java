package eu.europa.ec.opoce.cellar.cmr.database;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
/**
 * <p>IdolQueryService class.</p>
 */
public class IdolQueryService extends QueryService {

    /**
     * <p>setCellarDataSource.</p>
     *
     * @param dataSource a {@link javax.sql.DataSource} object.
     */
    @Autowired(required = true)
    public void setCellarDataSource(@Qualifier("dataSourceOracleCellarIdol") DataSource dataSource) {
        setDataSource(dataSource);
    }
}
