/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.database.dao.spring
 *             FILE : SpringModelLoadRequestDao.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Mar 17, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.database.dao.spring;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import org.apache.commons.lang.StringUtils;

import eu.europa.ec.opoce.cellar.cmr.database.dao.ModelLoadRequestDao;
import eu.europa.ec.opoce.cellar.modelload.domain.ModelLoadRequest;
import eu.europa.ec.opoce.cellar.ExecutionStatus;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Mar 17, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Repository
public class SpringModelLoadRequestDao extends CmrSpringBaseDao<ModelLoadRequest, Long> implements ModelLoadRequestDao {

    public SpringModelLoadRequestDao() {
        super(TABLE_NAME, Arrays.asList(Column.MODEL_URI, Column.EXECUTION_STATUS, Column.ACTIVATION_DATE,
                Column.PID, Column.EXECUTION_DATE, Column.NAL_NAME, Column.VERSION,Column.CELLAR_ID));
    }

    @Override
    public ModelLoadRequest create(final ResultSet rs) throws SQLException {
        ModelLoadRequest modelLoadRequest = new ModelLoadRequest();
        modelLoadRequest.setModelURI(rs.getString(Column.MODEL_URI));
        modelLoadRequest.setExecutionStatus(ExecutionStatus.findByValue(rs.getString(Column.EXECUTION_STATUS)));
        modelLoadRequest.setActivationDate(rs.getTimestamp(Column.ACTIVATION_DATE));
        modelLoadRequest.setExternalPid(rs.getString(Column.PID));
        modelLoadRequest.setExecutionDate(rs.getTimestamp(Column.EXECUTION_DATE));
        modelLoadRequest.setNalName(rs.getString(Column.NAL_NAME));
        modelLoadRequest.setVersion(rs.getString(Column.VERSION));
        modelLoadRequest.setCellarId(rs.getString(Column.CELLAR_ID));
        return modelLoadRequest;
    }

    @Override
    public void fillMap(ModelLoadRequest daoObject, Map<String, Object> map) {
        map.put(Column.MODEL_URI, daoObject.getModelURI());
        map.put(Column.EXECUTION_STATUS, daoObject.getExecutionStatus().getValue());
        map.put(Column.ACTIVATION_DATE, daoObject.getActivationDate());
        map.put(Column.PID, daoObject.getExternalPid());
        map.put(Column.EXECUTION_DATE, daoObject.getExecutionDate());
        map.put(Column.NAL_NAME, daoObject.getNalName());
        map.put(Column.VERSION, daoObject.getVersion());
        map.put(Column.CELLAR_ID,daoObject.getCellarId());
    }

    @Override
    public Optional<ModelLoadRequest> findByModelURI(final String modelURI) {
        return Optional.ofNullable(this.findUnique(WHERE_PART_MODEL_URI, Collections.singletonMap("modelURI", modelURI)));
    }

    @Override
    public Collection<ModelLoadRequest> findByCriteria(final String modelURI, final String executionStatus) {
        String where;
        Map<String, Object> column = new HashMap<>();
        ExecutionStatus execStatus = null;
        if (!StringUtils.isEmpty(executionStatus)) {
            execStatus = ExecutionStatus.valueOf(executionStatus);
        }
        if (!StringUtils.isEmpty(modelURI) && execStatus != null) {
            where = WHERE_PART_MODEL_URI_EXECUTION_STATUS;
            column.put("modelURI", modelURI);
            column.put("executionStatus", execStatus.getValue());
        } else if (!StringUtils.isEmpty(modelURI)) {
            where = WHERE_PART_MODEL_URI;
            column.put("modelURI", modelURI);
        } else if (execStatus != null) {
            where = WHERE_PART_EXECUTION_STATUS;
            column.put("executionStatus", execStatus.getValue());
        } else {
            return this.findAll();
        }
        return this.find(where, column);
    }

    @Override
    public Collection<ModelLoadRequest> findByExecutionStatusAndMaxActivationDate(final ExecutionStatus executionStatus,
            final Date maximumActivationDate) {
        Map<String, Object> args = new HashMap<>();
        args.put("executionStatus", executionStatus.getValue());
        args.put("maximumActivationDate", maximumActivationDate);
        return this.find(WHERE_PART_EXECUTION_STATUS_MAX_ACTIVATION_DATE, args);
    }

}
