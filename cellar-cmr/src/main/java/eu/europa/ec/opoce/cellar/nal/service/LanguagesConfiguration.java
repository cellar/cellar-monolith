package eu.europa.ec.opoce.cellar.nal.service;

import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;

import java.util.List;

/**
 * <p>LanguagesConfiguration interface.</p>
 */
public interface LanguagesConfiguration {

    /**
     * <p>getLanguages.</p>
     *
     * @return a {@link java.util.List} object.
     */
    List<LanguageBean> getLanguages();
}
