/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.cmr.ingest.impl.create
 *        FILE : StructMapCreateService.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 11-04-2013
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.ingest.impl.create;

import eu.europa.ec.opoce.cellar.cl.domain.response.IOperation;
import eu.europa.ec.opoce.cellar.cmr.DataStreamWrapper;
import eu.europa.ec.opoce.cellar.cmr.EmbargoDatabaseService;
import eu.europa.ec.opoce.cellar.cmr.MetaDataIngestionHelper;
import eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData;
import eu.europa.ec.opoce.cellar.cmr.ingest.IStructMapLoader;
import eu.europa.ec.opoce.cellar.cmr.ingest.impl.AbstractStructMapService;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestGenerationService;
import eu.europa.ec.opoce.cellar.common.metadata.IExistingMetadataLoader;
import eu.europa.ec.opoce.cellar.common.metadata.IModelLoader;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentStream;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.monitoring.Watch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class StructMapCreateService extends AbstractStructMapService {

    private static final Logger LOG = LogManager.getLogger(StructMapCreateService.class);

    @Autowired
    private MetaDataIngestionHelper metaDataIngestionHelper;

    @Autowired
    private IStructMapLoader structMapLoader;

    @Autowired
    private CmrIndexRequestGenerationService cmrIndexRequestGenerationService;

    @Autowired
    private EmbargoDatabaseService embargoDatabaseService;

    @Resource(name = "existingMetadataLoader")
    private IExistingMetadataLoader<String, Map<?, ?>, DataStreamWrapper> existingMetadataLoader;

    @Watch(value = "StructMap Create", arguments = {
            @Watch.WatchArgument(name = "cellar ID", expression = "calculatedData.rootCellarId")
    })
    @Override
    public void execute(final CalculatedData calculatedData, final IOperation<?> operation) {
        if (calculatedData.getMetsPackage() == null || calculatedData.getStructMap() == null
                || calculatedData.getStructMap().getDigitalObject() == null) {
            throw ExceptionBuilder.get(CellarException.class).build();
        }
        createTimed(calculatedData, operation);
    }

    void createTimed(final CalculatedData calculatedData, final IOperation<?> operation) {
        final StructMap structMap = calculatedData.getStructMap();

        // check that needed parts are filled in.
        checkCellarIdFilled(structMap.getDigitalObject());
        checkFileRefNotNull(structMap.getDigitalObject());

        // complete the structmap with data that was loaded at an earlier moment
        this.structMapLoader.loadAndCombine(calculatedData);
        this.log(LOG, structMap, "combine structmap '{}' with previous data", structMap.getId());

        // load data with the proper model loader
        final IModelLoader<CalculatedData> modelLoader = this.createModelLoader(structMap);
        modelLoader.load(calculatedData);

        try {
            // calculate data for database and S3
            this.metadataCalculator.calculate(calculatedData);
            this.log(LOG, structMap, "loaded all metadata in memory for '{}'", structMap.getId());

            // ingest the calculated data in S3 and the database
            this.metaDataIngestionHelper.ingest(calculatedData, operation);
            this.log(LOG, structMap, "loaded all metadata in cellar for '{}'", structMap.getId());

            // when saved private indicate in S3 that the content is under embargo
            if (embargoDatabaseService.isMetadataSavedPrivate(structMap.getDigitalObject())) {
                existingMetadataLoader.evictCache(structMap.getDigitalObject().getCellarId().getIdentifier());
            } else {
                // create requests for indexation
                cmrIndexRequestGenerationService.insertIndexingRequestsFromCreateIngest(calculatedData);
                this.log(LOG, structMap, "created requests for indexing for '{}'", structMap.getId());
            }
            //TODO this code is located here temporary. It will be move when Virtuoso will be in the same transaction that S3
            this.metaDataIngestionHelper.ingestVirtuoso(calculatedData);
        } finally {
            modelLoader.closeQuietly();
        }
    }

    /**
     * <p>checkFileRefNotNull checks that all contentstreams reference to a file in the fileref section.</p>
     *
     * @param digitalObject a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject} object.
     */
    private void checkFileRefNotNull(final DigitalObject digitalObject) {
        for (final ContentStream contentStream : digitalObject.getContentStreams()) {
            if (null == contentStream.getFileRef()) {
                throw ExceptionBuilder.get(CellarException.class).withMessage("FileRef in ContentStream cannot be null for a create")
                        .build();
            }
        }
        for (final DigitalObject child : digitalObject.getChildObjects()) {
            checkFileRefNotNull(child);
        }
    }

}
