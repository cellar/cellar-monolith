/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.nal.rework.impl
 *             FILE : NalConfigurationServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 02 02, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-02-02 09:17:50 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.nal.service.impl;

import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.database.dao.NalConfigDao;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.dao.NalConfig;
import eu.europa.ec.opoce.cellar.nal.service.NalConfigurationService;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.StringReader;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author ARHS Developments
 */
@Service
public class NalConfigurationServiceImpl implements NalConfigurationService {
    private final NalConfigDao nalConfigDao;
    private final ContentStreamService contentStreamService;

    @Autowired
    public NalConfigurationServiceImpl(NalConfigDao nalConfigDao, ContentStreamService contentStreamService) {
        this.nalConfigDao = nalConfigDao;
        this.contentStreamService = contentStreamService;
    }

    @Override
    public List<String> getAllNalUriList() {
        return nalConfigDao.findAll().stream()
                .map(NalConfig::getUri)
                .collect(Collectors.toList());
    }

    @Override
    public String getNal(String nalUri, boolean inferredNal) {
        return getNal(nalUri, inferredNal, RDFFormat.RDFXML_PLAIN);
    }

    @Override
    public String getNal(String nalUri, boolean inferredNal, RDFFormat format) {
        NalConfig nalConfig = nalConfigDao.findByUri(nalUri)
                .orElseThrow(() ->
                        ExceptionBuilder.get(CellarException.class)
                                .withMessage("The NAL for the URI '{}' could not be found in the database.")
                                .withMessageArgs(nalUri)
                                .build());

        ContentType type = inferredNal ? ContentType.DIRECT_INFERRED : ContentType.DIRECT;
        String version = inferredNal ? nalConfig.getVersionInferred() : nalConfig.getVersion();

        return contentStreamService.getContentAsString(nalConfig.getExternalPid(), version, type)
                .map(s -> JenaUtils.translate(s, RDFFormat.NTRIPLES_UTF8, format))
                .orElseThrow(() -> ExceptionBuilder.get(CellarException.class)
                        .withMessage("The NAL for the URI '{}' ({}/{}) could not be retrieved")
                        .withMessageArgs(nalUri, nalConfig.getExternalPid(), type)
                        .build());
    }

    @Override
    public Model getNalModel(String nalUri, boolean inferredNal) {
        return ModelFactory.createDefaultModel().read(new StringReader(getNal(nalUri, inferredNal, RDFFormat.RDFXML_PLAIN)),
                Lang.RDFXML.getName());
    }

    @Override
    public Model getAllNalModel(boolean inferred, String ontology) {
        Collection<NalConfig> nalConfigs = nalConfigDao.findByOntologyLike(ontology);
        Model allNalModel = ModelFactory.createDefaultModel();

        for (NalConfig nalConfig : nalConfigs) {
            allNalModel.add(getNalModel(nalConfig.getUri(), inferred));
        }
        return allNalModel;
    }
}
