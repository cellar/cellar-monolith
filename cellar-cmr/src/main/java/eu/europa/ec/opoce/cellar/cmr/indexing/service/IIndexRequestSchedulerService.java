/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.indexing.service
 *             FILE : IIndexRequestSchedulingService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Nov 7, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.indexing.service;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Nov 7, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IIndexRequestSchedulerService {

    /**
     * Schedule index requests.
     */
    void scheduleIndexRequests();

    /**
     * Force schedule index requests.
     */
    void forceScheduleIndexRequests();

    /**
     * Force schedule index requests asynchronously.
     */
    void forceScheduleIndexRequestsAsynchronously();
}
