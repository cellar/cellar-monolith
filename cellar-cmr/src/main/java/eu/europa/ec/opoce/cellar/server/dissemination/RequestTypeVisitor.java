package eu.europa.ec.opoce.cellar.server.dissemination;

/**
 * <p>RequestTypeVisitor interface.</p>
 */
public interface RequestTypeVisitor<IN, OUT> {

    /**
     * <p>visitTree.</p>
     *
     * @param in a IN object.
     * @return a OUT object.
     */
    OUT visitTree(IN in);

    /**
     * <p>visitBranch.</p>
     *
     * @param in a IN object.
     * @return a OUT object.
     */
    OUT visitBranch(IN in);

    /**
     * <p>visitObject.</p>
     *
     * @param in a IN object.
     * @return a OUT object.
     */
    OUT visitObject(IN in);

    /**
     * <p>visitIdentifier.</p>
     *
     * @param in a IN object.
     * @return a OUT object.
     */
    OUT visitIdentifier(IN in);

    /**
     * <p>visitContent.</p>
     *
     * @param in a IN object.
     * @return a OUT object.
     */
    OUT visitContent(IN in);

    /**
     * <p>visitRdfObject.</p>
     *
     * @param in a IN object.
     * @return a OUT object.
     */
    OUT visitRdfObject(IN in);

    /**
     * <p>visitRdfObjectNormalized.</p>
     * @param in a IN object
     * @return a OUT object
     */
    OUT visitRdfObjectNormalized(IN in);

    /**
     * <p>visitRdfTree.</p>
     *
     * @param in a IN object.
     * @return a OUT object.
     */
    OUT visitRdfTree(IN in);

    /**
     * <p>visitRdfTreeNormalized.</p>
     * @param in a IN object
     * @return a OUT object
     */
    OUT visitRdfTreeNormalized(IN in);

    /**
     * <p>visitRdfNonInferredObject.</p>
     *
     * @param in a IN object.
     * @return a OUT object.
     */
    OUT visitRdfNonInferredObject(IN in);

    /**
     * <p>visitRdfNonInferredObjectNormalized.</p>
     * @param in a IN object
     * @return a OUT object
     */
    OUT visitRdfNonInferredObjectNormalized(IN in);

    /**
     * <p>visitRdfNonInferredTree.</p>
     *
     * @param in a IN object.
     * @return a OUT object.
     */
    OUT visitRdfNonInferredTree(IN in);

    /**
     * <p>visitRdfContent.</p>
     *
     * @param in a IN object.
     * @return a OUT object.
     */
    OUT visitRdfContent(IN in);

    /**
     * <p>visitRdfNonInferredTreeNormalized.</p>
     * @param in a IN object
     * @return a OUT object
     */
    OUT visitRdfNonInferredTreeNormalized(IN in);

    OUT visitConceptScheme(IN in);

    OUT visitZip(IN in);
}
