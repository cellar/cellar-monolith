package eu.europa.ec.opoce.cellar.cmr.transformers;

import eu.europa.ec.opoce.cellar.common.util.Pair;

import org.apache.commons.collections15.Transformer;
import org.springframework.stereotype.Component;

/**
 * <p>Pair2Two class.</p>
 */
@Component("pair2Two")
public class Pair2Two<ONE, TWO> implements Transformer<Pair<ONE, TWO>, TWO> {

    /**
     * {@inheritDoc}
     */
    @Override
    public TWO transform(final Pair<ONE, TWO> pair) {
        return pair.getTwo();
    }
}
