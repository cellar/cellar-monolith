/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums
 *             FILE : FilterVariance.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 20, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums;

import eu.europa.ec.opoce.cellar.common.http.headers.IURLTokenable;

import java.util.HashMap;
import java.util.Map;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 20, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public enum FilterVariance implements IURLTokenable {
    FILTERED("filtered", true), UNFILTERED("unfiltered", false);

    private final String urlToken;
    private final boolean isFilterEnabled;

    private static final Map<String, FilterVariance> FILTER_PER_URL_TOKEN;

    static {
        FILTER_PER_URL_TOKEN = new HashMap<String, FilterVariance>();

        for (final FilterVariance f : FilterVariance.values()) {
            FILTER_PER_URL_TOKEN.put(f.getURLToken(), f);
        }
    }

    private FilterVariance(final String urlToken, final boolean isFilterEnabled) {
        this.urlToken = urlToken;
        this.isFilterEnabled = isFilterEnabled;
    }

    public boolean isFilterEnabled() {
        return this.isFilterEnabled;
    }

    public static FilterVariance parseInferenceVariance(final String filterVariance) {
        return FILTER_PER_URL_TOKEN.get(filterVariance);
    }

    public static FilterVariance resolve(final boolean filter) {
        if (filter) {
            return FILTERED;
        } else {
            return UNFILTERED;
        }
    }

    @Override
    public String getURLToken() {
        return this.urlToken;
    }

    @Override
    public String getErrorLabel() {
        return this.toString();
    }
}
