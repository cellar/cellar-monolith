package eu.europa.ec.opoce.cellar.cmr.notice.core;

import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.semantic.xml.XmlBuilder;
import org.apache.jena.rdf.model.Resource;

import java.util.List;
import java.util.Map;

public interface Rdf2XmlLabelService {

    boolean writePrefLabelIfExists(XmlBuilder prefLabelBuilder, Resource resource, LanguageBean useLanguage);

    void writeAltLabels(XmlBuilder conceptBuilder, Resource conceptResource, LanguageBean useLanguage);

    void writePrefLabel(XmlBuilder prefLabelBuilder, Resource resource, LanguageBean useLanguage, Map<LanguageBean, List<LanguageBean>> cache);

}
