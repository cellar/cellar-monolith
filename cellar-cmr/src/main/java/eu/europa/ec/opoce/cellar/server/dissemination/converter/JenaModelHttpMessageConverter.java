package eu.europa.ec.opoce.cellar.server.dissemination.converter;

import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;

import java.io.IOException;

import org.springframework.http.HttpOutputMessage;
import org.springframework.http.converter.HttpMessageNotWritableException;

import org.apache.jena.rdf.model.Model;

/**
 * <p>JenaModelHttpMessageConvertor class.</p>
 */
public class JenaModelHttpMessageConverter extends CellarAbstractHttpMessageConverter<Model> {


    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean supports(Class<?> clazz) {
        return Model.class.isAssignableFrom(clazz);
    }



    /**
     * {@inheritDoc}
     */
    @Override
    protected void writeInternal(Model model, HttpOutputMessage outputMessage) throws IOException, HttpMessageNotWritableException {
        if (model != null) {
            try {
                JenaUtils.write(model, outputMessage.getBody());
            } finally {
                JenaUtils.closeQuietly(model);
            }
        }
    }
}
