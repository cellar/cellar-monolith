package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.param;

import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.DisseminationRequest;

import java.io.Serializable;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

/**
 * The Class DisseminationRequestParameter.
 */
public class DisseminationRequestParameter implements Comparable<DisseminationRequestParameter>, Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 8859339465294167061L;

    /** The Constant QUALITY_PARAMETER_PATERN. */
    private static final Pattern QUALITY_PARAMETER_PATERN = Pattern.compile(".*(q=0\\.[0-9]{1}|q=1\\.0)$");

    /** The dissemination request. */
    private DisseminationRequest disseminationRequest;

    /** The quality value. */
    private Double qualityValue;

    /** The accept value string. */
    private String acceptValueString;

    /**
     * Gets the dissemination request.
     *
     * @return the dissemination request
     */
    public DisseminationRequest getDisseminationRequest() {
        return this.disseminationRequest;
    }

    /**
     * Sets the dissemination request.
     *
     * @param disseminationRequest the new dissemination request
     */
    public void setDisseminationRequest(final DisseminationRequest disseminationRequest) {
        this.disseminationRequest = disseminationRequest;
    }

    /**
     * Gets the quality value.
     *
     * @return the quality value
     */
    public Double getQualityValue() {
        return this.qualityValue;
    }

    /**
     * Sets the quality value.
     *
     * @param qualityValue the new quality value
     */
    public void setQualityValue(final Double qualityValue) {
        this.qualityValue = qualityValue;
    }

    /**
     * Gets the accept value string.
     *
     * @return the accept value string
     */
    public String getAcceptValueString() {
        return this.acceptValueString;
    }

    /**
     * Sets the accept value string.
     *
     * @param acceptValueString the new accept value string
     */
    public void setAcceptValueString(final String acceptValueString) {
        this.acceptValueString = acceptValueString;
    }

    /**
     * Parses the accept header.
     *
     * @param accept the accept
     * @return the dissemination request parameter object
     */
    public static DisseminationRequestParameter parseAcceptHeader(final String accept) {
        final DisseminationRequestParameter result = new DisseminationRequestParameter();
        //set the quality parameter value
        final Matcher matcher = QUALITY_PARAMETER_PATERN.matcher(accept);
        if (matcher.matches()) {
            final String qualityParameter = matcher.group(1);//q=0.8
            final String qualityParameterValue = StringUtils.substringAfter(qualityParameter, "=");
            result.setQualityValue(Double.parseDouble(qualityParameterValue));
        } else {
            result.setQualityValue(new Double(1.0));
        }
        final DisseminationRequest disseminationRequest = DisseminationRequest.acceptValueOf(accept);
        result.setDisseminationRequest(disseminationRequest);
        result.setAcceptValueString(accept);
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public int compareTo(final DisseminationRequestParameter o) {
        final Double otherQualityValue = o.getQualityValue();
        return this.qualityValue.compareTo(otherQualityValue);
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (!(obj instanceof DisseminationRequestParameter))
            return false;
        final DisseminationRequestParameter o = (DisseminationRequestParameter) obj;

        return Objects.equals(this.getDisseminationRequest(), o.getDisseminationRequest()) &&
                Objects.equals(this.getAcceptValueString(), o.getAcceptValueString());
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return Objects.hash(disseminationRequest, acceptValueString);
    }

}
