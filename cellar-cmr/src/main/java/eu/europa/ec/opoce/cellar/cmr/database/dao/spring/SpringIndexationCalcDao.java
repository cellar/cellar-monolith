package eu.europa.ec.opoce.cellar.cmr.database.dao.spring;

import eu.europa.ec.opoce.cellar.cmr.IndexationCalc;
import eu.europa.ec.opoce.cellar.cmr.database.dao.IndexationCalcDao;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.helper.StringHelper;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Repository
public class SpringIndexationCalcDao extends CmrSpringBaseDao<IndexationCalc, Long> implements IndexationCalcDao {

    private static final String TABLE = "CMR_INDEXATION_CALC_EMBEDDED_NOTICE";

    private static final String ID = "ID";
    private static final String CELLAR_ID = "CELLAR_ID";
    private static final String CELLAR_BASE_ID = "CELLAR_BASE_ID";
    private static final String EMBEDDED_NOTICE = "EMBEDDED_NOTICE";
    private static final String EMBEDDED_NOTICE_CREATION_DATE = "EMBEDDED_NOTICE_CREATION_DATE";

    private static final String getCellarId = StringHelper.format("{} = :cellarId ", CELLAR_ID);

    private static final Collection<String> ALL_FIELDS = Arrays.asList(CELLAR_ID, CELLAR_BASE_ID, EMBEDDED_NOTICE, EMBEDDED_NOTICE_CREATION_DATE);

    @Autowired
    public SpringIndexationCalcDao() {
        super(TABLE, ALL_FIELDS);
    }

    
    @Override
    @Cacheable(value = "findCellarId", unless = "#result == null")
    public IndexationCalc findCellarId(final String identifier) {
        checkIdentifier(identifier);
        return findUnique(getCellarId, Collections.singletonMap("cellarId", identifier));
    }
    

    @Override
    protected void fillMap(IndexationCalc indexationCalc, Map<String, Object> map) {
        map.put(CELLAR_ID, indexationCalc.getCellarId());
        map.put(CELLAR_BASE_ID, indexationCalc.getCellarBaseId());
        map.put(EMBEDDED_NOTICE, indexationCalc.getEmbeddedNotice());
        map.put(EMBEDDED_NOTICE_CREATION_DATE, indexationCalc.getEmbeddedNoticeCreationDate());
    }

    @Override
    @CacheEvict(value = "findCellarId", allEntries = true)
    public IndexationCalc saveOrUpdateObject(final IndexationCalc indexationCalc) {
        return super.saveOrUpdateObject(indexationCalc);
    }

    @Override
    @CacheEvict(value = "findCellarId", allEntries = true)
    public IndexationCalc saveObject(final IndexationCalc indexationCalc) {
        return super.saveObject(indexationCalc);
    }

    @Override
    @CacheEvict(value = "findCellarId", allEntries = true)
    public IndexationCalc updateObject(final IndexationCalc indexationCalc) {
        return super.updateObject(indexationCalc);
    }

    @Override
    @CacheEvict(value = "findCellarId", allEntries = true)
    public void deleteObject(final IndexationCalc indexationCalc) {
        super.deleteObject(indexationCalc);
    }

    
    private static void checkIdentifier(String identifier) {
        if (StringUtils.isBlank(identifier)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument status cannot be null").build();
        }
    }
    
    
    //static final class IndexationCalcRowMapper implements RowMapper<IndexationCalc> {

        @Override
        public IndexationCalc create(ResultSet rs) throws SQLException {
            final IndexationCalc calc = new IndexationCalc();
            calc.setId(rs.getLong(ID));
            calc.setCellarId(rs.getString(CELLAR_ID));
            calc.setCellarBaseId(rs.getString(CELLAR_BASE_ID));
            calc.setEmbeddedNotice(rs.getString(EMBEDDED_NOTICE));
            calc.setEmbeddedNoticeCreationDate(rs.getDate(EMBEDDED_NOTICE_CREATION_DATE));

            return calc;
        }
    //}
}
