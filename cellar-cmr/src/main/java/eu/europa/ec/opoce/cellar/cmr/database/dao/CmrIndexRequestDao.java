/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.database.dao
 *             FILE : CmrIndexRequestDao.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 19 sept. 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2016 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.database.dao;

import eu.europa.ec.opoce.cellar.cl.dao.BaseDao;
import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService.Priority;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.ExecutionStatus;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.Reason;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.RequestType;

import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * <p>CmrIndexRequestDao interface.</p>
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface CmrIndexRequestDao extends BaseDao<CmrIndexRequest, Long> {

    /**
     * Count by execution status.
     *
     * @param executionStatus the execution status
     * @return the int
     */
    long countByExecutionStatus(final ExecutionStatus executionStatus);

    /**
     * Count by reason and execution status.
     *
     * @param reason the reason
     * @param executionStatuses the execution statuses
     * @return the long
     */
    long countByReasonAndExecutionStatus(final Reason reason, final ExecutionStatus... executionStatuses);

    /**
     * Count by not execution status and not request type.
     *
     * @param requestType the request type
     * @param executionStatuses the execution statuses
     * @return the int
     */
    long countByNotExecutionStatusAndNotRequestType(final RequestType requestType, final ExecutionStatus... executionStatuses);

    /**
     * Find by execution status.
     *
     * @param executionStatus1 the execution status1
     * @param executionStatus2 the execution status2
     * @return the collection
     */
    Collection<CmrIndexRequest> findByExecutionStatus(final ExecutionStatus executionStatus1, final ExecutionStatus executionStatus2);

    /**
     * Find by execution status.
     * @param executionStatus the execution status
     * @param batchSize the limit
     * @return the collection
     */
    Collection<CmrIndexRequest> findByExecutionStatus(final ExecutionStatus executionStatus, final int batchSize);

    /**
     * Find by execution status and group uri and max created on.
     *
     * @param executionStatus the execution status
     * @param groupByUri the group by uri
     * @param minimumPriority the minimum priority
     * @param maximumCreatedOn the maximum created on
     * @return the collection
     */
    Collection<CmrIndexRequest> findByExecutionStatusAndGroupUriAndMinPriorityAndMaxCreatedOn(final ExecutionStatus executionStatus,
            final String groupByUri, final Priority minimumPriority, final Date maximumCreatedOn);

    /**
     * Find by execution status and maximum execution date.
     *
     * @param executionStatus the execution status
     * @param maxExecutionDate the maximum execution date
     * @param batchSize the limit
     * @return the collection
     */
    Collection<CmrIndexRequest> findByExecutionStatusAndMaxExecutionDate(final ExecutionStatus executionStatus, final Date maxExecutionDate,
            final int batchSize);

    /**
     * Gets the processing indexation request grouped by cellar group.
     *
     * @return the processing indexation request grouped by cellar group
     */
    List<CmrIndexRequest> getProcessingIndexationRequestGroupedByCellarGroup();

    /**
     * Gets the waiting indexation request grouped by cellar group.
     *
     * @return the waiting indexation request grouped by cellar group
     */
    List<CmrIndexRequest> getWaitingIndexationRequestGroupedByCellarGroup();

    /**
     * Update the execution status of all the {@link CmrIndexRequest} which
     * have a status equals to the status in parameters.
     *
     * @param newStatus the status to applied to the {@link CmrIndexRequest}
     * @param oldStatus the status which will be replaced
     * @return the count of rows modified
     */
    int updateExecutionStatus(ExecutionStatus newStatus, ExecutionStatus... oldStatus);

    /**
     * Delete from the database the {@link CmrIndexRequest} with an
     * EXECUTION_DATE inferior or equals to the maxExecutionDate parameter
     * and a status equals to one of the statuses in parameter.
     *
     * @param maxExecutionDate the max execution date
     * @param status           the statuses of {@link CmrIndexRequest}
     * @return the count of rows deleted
     */
    int deleteIndexRequest(Date maxExecutionDate, ExecutionStatus... status);
}
