package eu.europa.ec.opoce.cellar.cmr;

import eu.europa.ec.opoce.cellar.ontology.service.OntologyService;
import eu.europa.ec.opoce.cellar.semantic.SparqlUpdateService;
import eu.europa.ec.opoce.cellar.semantic.inferredmodel.InferredModelConfiguration;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.update.UpdateRequest;

/**
 * <p>InferenceService class.</p>
 */
@Service
public class InferenceService {

    private final OntologyService ontologyService;

    private final SparqlUpdateService sparqlUpdateService;

    private final InferredModelConfiguration configuration;

    @Autowired
    public InferenceService(OntologyService ontologyService, SparqlUpdateService sparqlUpdateService, InferredModelConfiguration configuration) {
        this.ontologyService = ontologyService;
        this.sparqlUpdateService = sparqlUpdateService;
        this.configuration = configuration;
    }

    /**
     * <p>calculateInferredModel.</p>
     *
     * @param inModel model that need to be inferred
     * @return new model with original data and inferred data
     */
    public Model calculateInferredModel(final Model inModel) {
        final Model outModel = JenaUtils.create(inModel);
        for (UpdateRequest query : this.configuration.getDefaultQueries()) {
            sparqlUpdateService.runUpdate(outModel, query, false, ontologyService.getWrapper().getOntology().getInferredOntology());
        }
        return outModel;
    }

    /**
     * <p>calculateInferredSuperclassModel.</p>
     *
     * @param inModel model that need to be inferred
     * @return new model with original data and inferred data for the superclasses
     */
    public Model calculateInferredSuperclassModel(final Model inModel) {
        return this.calculateSpecificInference(inModel, this.configuration.getInsertSuperclass());
    }

    /**
     * <p>calculateInferredSuperpropertyModel.</p>
     *
     * @param inModel a {@link org.apache.jena.rdf.model.Model} object.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    public Model calculateInferredSuperpropertyModel(final Model inModel) {
        return this.calculateSpecificInference(inModel, this.configuration.getInsertSuperproperty());
    }

    /**
     * <p>calculateInferredDomainModel.</p>
     *
     * @param inModel a {@link org.apache.jena.rdf.model.Model} object.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    public Model calculateInferredDomainModel(final Model inModel) {
        return this.calculateSpecificInference(inModel, this.configuration.getInsertDomain());
    }

    /**
     * <p>calculateInferredRangeModel.</p>
     *
     * @param inModel a {@link org.apache.jena.rdf.model.Model} object.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    public Model calculateInferredRangeModel(final Model inModel) {
        return this.calculateSpecificInference(inModel, this.configuration.getInsertRange());
    }

    /**
     * <p>calculateInferredRestrictionModel.</p>
     *
     * @param inModel a {@link org.apache.jena.rdf.model.Model} object.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    public Model calculateInferredRestrictionModel(final Model inModel) {
        return this.calculateSpecificInference(inModel, this.configuration.getInsertRestriction());
    }

    private Model calculateSpecificInference(final Model inModel, final UpdateRequest updateRequest) {
        final Model outModel = JenaUtils.create(inModel);
        sparqlUpdateService.runUpdate(outModel, updateRequest, false, ontologyService.getWrapper().getOntology().getInferredOntology());
        return outModel;
    }

}
