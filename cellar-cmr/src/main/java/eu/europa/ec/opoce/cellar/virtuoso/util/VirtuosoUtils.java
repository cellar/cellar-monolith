/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.virtuoso.util
 *        FILE : VirtuosoUtils.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 5-05-2015
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.virtuoso.util;

import eu.europa.ec.opoce.cellar.cl.domain.virtuoso.service.VirtuosoExceptionMessagesLoaderService;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.database.util.JdbcUtils;
import eu.europa.ec.opoce.cellar.cmr.model.ModelUtils;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import eu.europa.ec.opoce.cellar.common.util.CellarIdUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.exception.ExceptionUtils;
import eu.europa.ec.opoce.cellar.ontology.service.OntologyService;
import eu.europa.ec.opoce.cellar.semantic.helper.DateFormats;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaQueries;
import eu.europa.ec.opoce.cellar.semantic.jena.transformers.RdfNode2Uri;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.RDFS;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import virtuoso.jena.driver.VirtDataset;
import virtuoso.jena.driver.VirtGraph;

import javax.sql.DataSource;
import java.util.*;

import static eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration.VIRTUOSO;

/**
 * <class_description> This is an utility component for Virtuoso.<br/>
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 15-05-2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class VirtuosoUtils {
    private static final Logger LOG = LogManager.getLogger(VirtuosoUtils.class);
    private static final IdentifierService identifierService = ServiceLocator.getService("pidManagerService", IdentifierService.class);
    private static final OntologyService ontologyService = ServiceLocator.getService(OntologyService.class);
    private static final CellarResourceDao CELLAR_RESOURCE_DAO = ServiceLocator.getService(CellarResourceDao.class);
    private static final VirtuosoExceptionMessagesLoaderService VIRTUOSO_EXCEPTION_MESSAGES_LOADER_SERVICE
            = ServiceLocator.getService(VirtuosoExceptionMessagesLoaderService.class);
    private static final String GROUP_ACTION_QUERY = "%s('http://www.openlinksw.com/schemas/virtrdf#PrivateGraphs','%s')";

    /**
     * Update the model with cellarURI {@link existingCellarUri} by replacing it with the new model {@link newModel}.
     *
     * @param dataset the dataset of reference
     * @param existingCellarUri the cellarURI of the existing model
     * @param newModel the new model
     */
    public static void updateModel(final VirtDataset dataset, final String existingCellarUri, final Model newModel) {
        LOG.debug(VIRTUOSO, "Virtuoso dataset (triples count: {}), cellarURI={}, newModel size={}",
                dataset.getCount(), existingCellarUri, newModel.size());
        final long currentTimeMillis = System.currentTimeMillis();
        Model existingModel = null;
        Model existingLocal = null;
        try {
            // load existing model
            existingModel = dataset.getNamedModel(existingCellarUri);
        	// clone virtuoso model to a local model and execute isomorphism check, in order to avoid to many open statements on virtuoso side
        	existingLocal= ModelFactory.createDefaultModel().add(existingModel);
            LOG.debug(VIRTUOSO, "Retrieve model reference (VIRTUOSO_MODEL_REF) from VirtDataset (size={})",
                    existingModel != null ? existingModel.size() : "null");
            // Isomorphic check in order to identify if an update on model is necessary
            if(existingLocal.isIsomorphicWith(newModel)) {
            	LOG.info(VIRTUOSO, "The existing model of Cellar URI {} is isomorphic with the new model. No update is executed.", existingCellarUri);
            	return;
            	}
            ModelUtils.closeQuietly(existingLocal);
            if (existingModel == null) {
                existingModel = ModelFactory.createDefaultModel();
                dataset.addNamedModel(existingCellarUri, existingModel);
                LOG.debug(VIRTUOSO, "Virtuoso dataset (triples count: {}). Model '{}' does not exist: creating it.",
                        dataset.getCount(), existingCellarUri);
            } else {
                LOG.debug(VIRTUOSO, "Virtuoso dataset (triples count: {}). Existing model '{}' loaded.",
                        dataset.getCount(), existingCellarUri);
            }

            // update existing model with new model
            // Strangely, the update of existingModel produce a side effect in VirtDataset (data will persisted in
            // Virtuoso) but this change is not visible with dataset.getCount(). Great design.
            ModelUtils.mergeModels(newModel, existingModel, false);

            final boolean modelsHaveDifferentSizes = existingModel.size() != newModel.size();
            final Level logLevel = modelsHaveDifferentSizes ? Level.ERROR : Level.DEBUG;

            LOG.log(logLevel, VIRTUOSO, "Virtuoso dataset after merge (triples count: {}), reference Model VIRTUOSO_MODEL_REF size={} " +
                    "(for virtuoso insertion) '{}' updated.", newModel.size(), existingModel.size(), existingCellarUri);

            if(modelsHaveDifferentSizes) {
                LOG.error("The merge model has a different size (size={}) than the new model (size={})", existingModel.size(), newModel.size());
            }
            final long delta = System.currentTimeMillis() - currentTimeMillis;
            LOG.info("CELLAR's request to update {} statements took {} ms to be processed.", newModel.size(), delta);
            
        } finally {
            ModelUtils.closeQuietly(existingModel, existingLocal);
        }
    }

    /**
     * Drop the models named by the given cellarIds.
     *
     * @param dataset   the dataset of reference
     * @param cellarIds the cellarIds, which represent the names of the models to drop
     */
    public static void dropModels(final Dataset dataset, final String... cellarIds) {
        for (final String cellarId : cellarIds) {
            final String existingCellarUri = identifierService.getUri(cellarId);
            ((VirtDataset)dataset).removeNamedModel(existingCellarUri);
            LOG.debug(VIRTUOSO, "Model '{}' dropped.", existingCellarUri);
        }
        LOG.debug(VIRTUOSO, "Virtuoso dataset after drop (triples count {})",
                (dataset instanceof VirtDataset) ? ((VirtDataset) dataset).getCount() : "not found");
    }

    /**
     * Drop the models named by the given cellaIds.
     *
     * @param dataset   the dataset of reference
     * @param cellarIds the cellarIds, which represent the names of the models to drop
     */
    public static void dropModels(final Dataset dataset, final Collection<String> cellarIds) {
        dropModels(dataset, cellarIds.toArray(new String[cellarIds.size()]));
    }

    /**
     * Close the given dataset.
     *
     * @param dataset the dataset to close
     */
    public static void close(final Dataset dataset) {
        try {
            if (dataset != null) {
                dataset.close();
            }
        } catch (final Exception exc) {
            LOG.error("Cannot close connection to Virtuoso!", exc);
        }
    }

    public static void close(final VirtDataset dataset) {
        close((Dataset) dataset);
    }

    /**
     * Close the given virtuoso graph.
     *
     * @param graph the graph to close
     */
    public static void close(final VirtGraph graph) {
        try {
            if ((graph != null) && !graph.isClosed()) {
                graph.close();
            }
        } catch (final Exception exc) {
            LOG.error("Cannot close Virtuoso graph '" + graph.getGraphUrl() + "'!", exc);
        }
    }

    /**
     * Check from the given {@link exception} if Virtuoso is not available.
     *
     * @param exception the exception caused on the Virtuoso layer
     * @return true if Virtuoso is not available
     */
    public static boolean isVirtuosoNotAvailable(final Exception exception) {
        final String[] messages = VIRTUOSO_EXCEPTION_MESSAGES_LOADER_SERVICE.getAllMessages().toArray(new String[]{});
        return ExceptionUtils.exceptionContainsMessages(exception, messages);
    }

    /**
     * Group action.
     *
     * @param workCellarId the work cellar id
     * @param privateGroupOperation the private group operation
     * @param virtuosoDataSource the virtuoso data source
     * @param embargoDate the embargo date
     * @param lastModificationDate the lastModification date
     * @throws Exception the exception
     */
    public static void groupAction(final String cellarId, final PrivateGroupOperation privateGroupOperation,
            final DataSource virtuosoDataSource, final Date embargoDate, final Date lastModificationDate) throws Exception {
        final Set<String> cellarIds = new HashSet<>();
        cellarIds.add(cellarId);
        final List<CellarResource> cellarResources = CELLAR_RESOURCE_DAO.findCellarIdStartingWith(cellarId);
        for (final CellarResource cellarResource : cellarResources) {
            final String identifier = cellarResource.getCellarId();
            if (!DigitalObjectType.ITEM.equals(cellarResource.getCellarType())) {
                cellarIds.add(identifier);
            }
        }
        groupAction(cellarIds, privateGroupOperation, virtuosoDataSource, embargoDate, lastModificationDate);

    }

    /**
     * Group action.
     *
     * @param cellarIds the cellar ids
     * @param privateGroupOperation the private group operation
     * @param virtuosoDataSource the virtuoso data source
     * @param embargoDate the embargo date
     * @param lastModificationDate the last modification date
     * @throws Exception the exception
     */
    public static void groupAction(final Collection<String> cellarIds, final PrivateGroupOperation privateGroupOperation,
            final DataSource virtuosoDataSource, final Date embargoDate, final Date lastModificationDate) throws Exception {
        group(cellarIds, privateGroupOperation, virtuosoDataSource);
        for (final String foundCellarId : cellarIds) {
            updateEmbargoDate(foundCellarId, embargoDate, virtuosoDataSource);
            updateLastModificationDate(foundCellarId, lastModificationDate, virtuosoDataSource);
        }
    }

    /**
     * Group.
     *
     * @param cellarIds the cellar ids
     * @param privateGroupOperation the private group operation
     * @param virtuosoDataSource the virtuoso data source
     * @throws Exception the exception
     */
    public static void group(final Collection<String> cellarIds, final PrivateGroupOperation privateGroupOperation,
            final DataSource virtuosoDataSource) throws Exception {
        for (final String cellarId : cellarIds) {
            LOG.debug(VIRTUOSO, "Moving '{}' to {}", cellarId, privateGroupOperation.getOperation());
            if (!DigitalObjectType.isItem(cellarId)) {
                final String graphName = identifierService.getUri(cellarId);
                final String query = String.format(GROUP_ACTION_QUERY, privateGroupOperation.getOperation(), graphName);
                try {
                    LOG.debug(VIRTUOSO, "Executing Virtuoso query: {}", query);
                    final boolean execute = JdbcUtils.execute(virtuosoDataSource, query);
                    LOG.debug(VIRTUOSO, "{} to perform a {} group-graph operation for the graph <{}>", (execute ? "Succeeded" : "Failed"),
                            privateGroupOperation, graphName);
                } catch(Exception e) {
                    LOG.error("Error executing query {}", query);
                    throw e;
                }
            }
        }
    }

    /**
     * Update embargo date.
     * Removes existing statement and adds new with new value
     *
     * @param cellarId the cellar id
     * @param embargoDate the embargo date
     * @param virtuosoDataSource the virtuoso data source
     */
    private static void updateEmbargoDate(final String cellarId, final Date embargoDate, final DataSource virtuosoDataSource) {
        Dataset dataset = null;
        try {
            dataset = new VirtDataset(virtuosoDataSource);
            final Model inferredOntologyModel = ontologyService.getWrapper().getOntology().getInferredOntology();
            updateEmbargoDate(embargoDate, dataset, cellarId, inferredOntologyModel);
        } finally {
            VirtuosoUtils.close(dataset);
        }
    }

    /**
     * Update embargo date.
     *
     * @param embargoDate the embargo date
     * @param virtDataset the virt dataset
     * @param cellarId the cellar id
     * @param inferrenceOntologyModel the inferrence ontology model
     */
    private static void updateEmbargoDate(final Date embargoDate, final Dataset virtDataset, final String cellarId,
            final Model inferrenceOntologyModel) {
        Model existingModel = null;
        try {
            final CellarResource cellarResource = CELLAR_RESOURCE_DAO.findCellarId(cellarId);
            final DigitalObjectType cellarType = cellarResource.getCellarType();
            if (!DigitalObjectType.takeEmbargoDate(cellarType)) {
                VirtuosoUtils.LOG.warn("Attempted to update embargo date on cellar resource of type {}", cellarType);
                return;
            }
            String cellarProperty = null;
            switch (cellarType) {
            case WORK:
                cellarProperty = CellarProperty.cdm_work_embargo;
                break;
            case EXPRESSION:
                cellarProperty = CellarProperty.cdm_expression_embargo;
                break;
            case MANIFESTATION:
                cellarProperty = CellarProperty.cdm_manifestation_embargo;
                break;
            case ITEM:
                cellarProperty = CellarProperty.cdm_item_embargo;
                break;
            case DOSSIER:
                cellarProperty = CellarProperty.cdm_dossier_embargo;
                break;
            case EVENT:
            case TOPLEVELEVENT:
                cellarProperty = CellarProperty.cdm_event_embargo;
                break;
            case AGENT:
                cellarProperty = CellarProperty.cdm_agent_embargo;
                break;
            default:
                break;
            }
            final Resource embargoR = inferrenceOntologyModel.getResource(cellarProperty);
            final List<Resource> resourceList = JenaQueries.getResources(embargoR, RDFS.subPropertyOf, false);
            final Collection<String> propertyCollection = CollectionUtils.collect(resourceList, RdfNode2Uri.instance);
            propertyCollection.add(cellarProperty);

            final String currentCellarUri = identifierService.getUri(cellarId);
            String cellarUri = currentCellarUri;
            if (DigitalObjectType.isItem(cellarId)) {
                final String parentCellarId = CellarIdUtils.getParentCellarId(cellarId);
                cellarUri = identifierService.getUri(parentCellarId);
            }
            existingModel = virtDataset.getNamedModel(cellarUri);

            // Remove the existing triples
            final Set<Statement> statementsToRemove = new HashSet<>();
            final StmtIterator iter = existingModel.listStatements();
            try {
                while (iter.hasNext()) {
                    final Statement stmt = iter.nextStatement();
                    final Property predicate = stmt.getPredicate();
                    if (predicate.isURIResource()) {
                        final String uri = predicate.getURI();
                        if (propertyCollection.contains(uri)) {
                            statementsToRemove.add(stmt);
                        }
                    }
                }
            } finally {
                iter.close();
            }
            //add new
            if (embargoDate != null) {
                existingModel.remove(new ArrayList<>(statementsToRemove));
                final String formatFullXmlDateTime = DateFormats.formatFullXmlDateTime(embargoDate);
                for (final String property : propertyCollection) {
                    final Literal createTypedLiteral = existingModel.createTypedLiteral(formatFullXmlDateTime, XSDDatatype.XSDdateTime);
                    final Resource createResource = existingModel.createResource(currentCellarUri);
                    final Property createProperty = existingModel.createProperty(property);
                    final Statement createStatement = existingModel.createStatement(createResource, createProperty, createTypedLiteral);
                    existingModel.add(createStatement);
                }
            }
        } finally {
            ModelUtils.closeQuietly(existingModel);
        }
    }

    /**
     * Update lastModificationDate.
     * Removes existing statement and adds new with new value
     *
     * @param cellarId the cellar id
     * @param lastModificationDate the lastModificationDate
     * @param virtuosoDataSource the virtuoso data source
     */
    public static void updateLastModificationDate(final String cellarId, final Date lastModificationDate,
            final DataSource virtuosoDataSource) {
        Dataset dataset = null;
        try {
            dataset = new VirtDataset(virtuosoDataSource);
            final Model inferredOntologyModel = ontologyService.getWrapper().getOntology().getInferredOntology();
            updateLastModificationDate(lastModificationDate, dataset, cellarId, inferredOntologyModel);
        } finally {
            VirtuosoUtils.close(dataset);
        }
    }

    /**
     * Update lastModificationDate.
     *
     * @param lastModificationDate the lastModificationDate
     * @param virtDataset the virt dataset
     * @param cellarId the cellar id
     * @param inferrenceOntologyModel the inferrence ontology model
     */
    private static void updateLastModificationDate(final Date lastModificationDate, final Dataset virtDataset, final String cellarId,
            final Model inferrenceOntologyModel) {
        Model existingModel = null;
        try {
            final String currentCellarUri = identifierService.getUri(cellarId);
            String cellarUri = currentCellarUri;
            if (DigitalObjectType.isItem(cellarId)) {
                final String parentCellarId = CellarIdUtils.getParentCellarId(currentCellarUri);
                cellarUri = identifierService.getUri(parentCellarId);
            }
            existingModel = virtDataset.getNamedModel(cellarUri);
            // Remove the existing triples
            final Set<Statement> statementsToRemove = new HashSet<>();
            final StmtIterator iter = existingModel.listStatements();
            try {
                while (iter.hasNext()) {
                    final Statement stmt = iter.nextStatement();
                    final Property predicate = stmt.getPredicate();
                    if (predicate.isURIResource()) {
                        final String uri = predicate.getURI();
                        if (CellarProperty.cmr_lastmodificationdate.equals(uri)) {
                            statementsToRemove.add(stmt);
                        }
                    }
                }
            } finally {
                iter.close();
            }
            //add new
            if (lastModificationDate != null) {
                existingModel.remove(new ArrayList<>(statementsToRemove));
                final String formatFullXmlDateTime = DateFormats.formatFullXmlDateTime(lastModificationDate);
                final Literal createTypedLiteral = existingModel.createTypedLiteral(formatFullXmlDateTime, XSDDatatype.XSDdateTime);
                final Resource createResource = existingModel.createResource(currentCellarUri);
                final Property createProperty = existingModel.createProperty(CellarProperty.cmr_lastmodificationdate);
                final Statement createStatement = existingModel.createStatement(createResource, createProperty, createTypedLiteral);
                existingModel.add(createStatement);
            }
        } finally {
            ModelUtils.closeQuietly(existingModel);
        }
    }

}
