/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.oj
 *             FILE : OfficialJournal.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jul 8, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.oj;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jul 8, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlType(name = "officialJournalType")
@XmlRootElement(name = "officialJournal")
public class OfficialJournal {

    /** The uri. */
    private String uri;

    /** The oj class. */
    private String ojClass;

    /** The oj number. */
    private Integer ojNumber;

    /** The oj collection. */
    private String ojCollection;

    /** The oj year. */
    private Integer ojYear;

    /** The work date document. */
    private String workDateDocument;

    /**
     * Instantiates a new official journal.
     */
    public OfficialJournal() {

    }

    /**
     * Instantiates a new official journal.
     *
     * @param uri the uri
     * @param ojClass the oj class
     * @param ojNumber the oj number
     * @param ojCollection the oj collection
     * @param ojYear the oj year
     * @param workDateDocument the work date document
     */
    public OfficialJournal(final String uri, final String ojClass, final Integer ojNumber, final String ojCollection, final Integer ojYear,
            final String workDateDocument) {
        this.uri = uri;
        this.ojClass = ojClass;
        this.ojNumber = ojNumber;
        this.ojCollection = ojCollection;
        this.ojYear = ojYear;
        this.workDateDocument = workDateDocument;
    }

    /**
     * Gets the oj class.
     *
     * @return the ojClass
     */
    @XmlElement(name = "ojClass")
    public String getOjClass() {
        return ojClass;
    }

    /**
     * Gets the oj collection.
     *
     * @return the ojCollection
     */
    @XmlElement(name = "ojCollection")
    public String getOjCollection() {
        return ojCollection;
    }

    /**
     * Gets the oj number.
     *
     * @return the ojNumber
     */
    @XmlElement(name = "ojNumber")
    public Integer getOjNumber() {
        return ojNumber;
    }

    /**
     * Gets the oj year.
     *
     * @return the ojYear
     */
    @XmlElement(name = "ojYear")
    public Integer getOjYear() {
        return ojYear;
    }

    /**
     * Gets the uri.
     *
     * @return the uri
     */
    @XmlElement(name = "uri")
    public String getUri() {
        return uri;
    }

    /**
     * Gets the work date document.
     *
     * @return the workDateDocument
     */
    @XmlElement(name = "workDateDocument")
    public String getWorkDateDocument() {
        return workDateDocument;
    }

}
