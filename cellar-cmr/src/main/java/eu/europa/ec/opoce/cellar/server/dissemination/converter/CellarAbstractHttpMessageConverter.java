/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.service
 *             FILE : CellarAbstractHttpMessageConverter.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 20-Jan-2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.converter;

import java.io.IOException;

import org.springframework.http.HttpInputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;


/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 20-Jan-2017
 * The Class CellarAbstractHttpMessageConverter.
 *
 * @author ARHS Developments
 * @version $$Revision$$$
 * @param <T> the generic type
 */
public abstract class CellarAbstractHttpMessageConverter<T> extends AbstractHttpMessageConverter<T> {

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canRead(final Class<?> clazz, final MediaType mediaType) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean canWrite(final Class<?> clazz, final MediaType mediaType) {
        return this.supports(clazz);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected T readInternal(final Class<? extends T> clazz, final HttpInputMessage inputMessage)
            throws IOException, HttpMessageNotReadableException {
        return null;
    }

}
