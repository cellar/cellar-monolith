package eu.europa.ec.opoce.cellar.jena.oracle;

import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.common.closure.PClosure;
import eu.europa.ec.opoce.cellar.common.closure.RPClosure;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.jena.template.JenaQueryUtils;
import oracle.spatial.rdf.client.jena.Attachment;
import oracle.spatial.rdf.client.jena.GraphOracleSem;
import oracle.spatial.rdf.client.jena.InferenceMaintenanceMode;
import oracle.spatial.rdf.client.jena.ModelOracleSem;
import oracle.spatial.rdf.client.jena.Oracle;
import oracle.spatial.rdf.client.jena.QueryOptions;
import org.apache.jena.graph.Graph;
import org.apache.jena.rdf.model.Model;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;

/**
 * <p>Abstract OraTemplate class.</p>
 */
@SuppressWarnings("Duplicates")
public abstract class OraTemplate {

    private static final Logger LOG = LogManager.getLogger(OraTemplate.class);

    /**
     * Constant <code>ATTACHMENT_ALLOW_QUERY_INVALID</code>
     */
    public static final Attachment ATTACHMENT_ALLOW_QUERY_INVALID = Attachment.createInstance(new String[]{}, new String[]{},
            InferenceMaintenanceMode.NO_UPDATE, QueryOptions.ALLOW_QUERY_INVALID);

    /**
     * <p>execute.</p>
     *
     * @param oracle    a {@link oracle.spatial.rdf.client.jena.Oracle} object.
     * @param modelName a {@link java.lang.String} object.
     * @param actions   a {@link RPClosure} object.
     * @return a R object.
     */
    public static <R> R execute(final Oracle oracle, final String modelName, final RPClosure<R, Model> actions) {
        ModelOracleSem model = null;
        try {
            model = ModelOracleSem.createOracleSemModel(oracle, modelName);
            return actions.call(model);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.CMR_INTERNAL_ERROR).withCause(e)
                    .withMessage(e.getMessage()).build();
        } finally {
            closeQuietly(null, model, null);
        }
    }

    /**
     * <p>execute.</p>
     *
     * @param oracle    a {@link oracle.spatial.rdf.client.jena.Oracle} object.
     * @param modelName a {@link java.lang.String} object.
     * @param actions   a {@link PClosure} object.
     */
    public static void execute(final Oracle oracle, final String modelName, final PClosure<Model> actions) {
        ModelOracleSem model = null;
        try {
            model = ModelOracleSem.createOracleSemModel(oracle, modelName);
            actions.call(model);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.CMR_INTERNAL_ERROR).withCause(e)
                    .withMessage(e.getMessage()).build();
        } finally {
            closeQuietly(null, model, null);
        }
    }

    /**
     * <p>execute.</p>
     *
     * @param oracle     a {@link oracle.spatial.rdf.client.jena.Oracle} object.
     * @param modelName  a {@link java.lang.String} object.
     * @param attachment a {@link oracle.spatial.rdf.client.jena.Attachment} object.
     * @param actions    a {@link RPClosure} object.
     * @return a R object.
     */
    public static <R> R execute(final Oracle oracle, final String modelName, final Attachment attachment,
                                final RPClosure<R, Model> actions) {
        GraphOracleSem graphOracleSem = null;
        ModelOracleSem model = null;
        try {
            graphOracleSem = new GraphOracleSem(oracle, modelName, attachment);
            model = new ModelOracleSem(graphOracleSem);
            return actions.call(model);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.CMR_INTERNAL_ERROR).withCause(e)
                    .withMessage(e.getMessage()).build();
        } finally {
            closeQuietly(null, model, graphOracleSem);
        }
    }

    /**
     * <p>execute.</p>
     *
     * @param oracle     a {@link oracle.spatial.rdf.client.jena.Oracle} object.
     * @param modelName  a {@link java.lang.String} object.
     * @param attachment a {@link oracle.spatial.rdf.client.jena.Attachment} object.
     * @param actions    a {@link PClosure} object.
     */
    public static void execute(final Oracle oracle, final String modelName, final Attachment attachment,
                               final PClosure<Model> actions) {
        GraphOracleSem graphOracleSem = null;
        ModelOracleSem model = null;
        try {
            graphOracleSem = new GraphOracleSem(oracle, modelName, attachment);
            model = new ModelOracleSem(graphOracleSem);
            actions.call(model);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.CMR_INTERNAL_ERROR).withCause(e)
                    .withMessage(e.getMessage()).build();
        } finally {
            closeQuietly(null, model, graphOracleSem);
        }
    }

    /**
     * <p>execute.</p>
     *
     * @param oracle  a {@link oracle.spatial.rdf.client.jena.Oracle} object.
     * @param actions a {@link RPClosure} object.
     * @param close   a boolean.
     * @return a R object.
     */
    public static <R> R execute(final Oracle oracle, final RPClosure<R, Oracle> actions, final boolean close) {
        try {
            return actions.call(oracle);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.CMR_INTERNAL_ERROR).withCause(e)
                    .withMessage(e.getMessage()).build();
        } finally {
            if (close) {
                closeQuietly(oracle);
            }
        }
    }

    /**
     * <p>execute.</p>
     *
     * @param oracle  a {@link oracle.spatial.rdf.client.jena.Oracle} object.
     * @param actions a {@link PClosure} object.
     * @param close   a boolean.
     */
    public static void execute(final Oracle oracle, final PClosure<Oracle> actions, final boolean close) {
        try {
            actions.call(oracle);
        } catch (RuntimeException e) {
            throw e;
        } catch (Exception e) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.CMR_INTERNAL_ERROR).withCause(e)
                    .withMessage(e.getMessage()).build();
        } finally {
            if (close) {
                closeQuietly(oracle);
            }
        }
    }

    /**
     * <p>execute.</p>
     *
     * @param oracle  a {@link oracle.spatial.rdf.client.jena.Oracle} object.
     * @param actions a {@link RPClosure} object.
     * @return a R object.
     */
    public static <R> R execute(final Oracle oracle, final RPClosure<R, Oracle> actions) {
        return execute(oracle, actions, true);
    }

    /**
     * <p>execute.</p>
     *
     * @param oracle  a {@link oracle.spatial.rdf.client.jena.Oracle} object.
     * @param actions a {@link PClosure} object.
     */
    public static void execute(final Oracle oracle, final PClosure<Oracle> actions) {
        execute(oracle, actions, true);
    }

    /**
     * <p>executeAndKeepOpen.</p>
     *
     * @param oracle  a {@link oracle.spatial.rdf.client.jena.Oracle} object.
     * @param actions a {@link RPClosure} object.
     * @return a R object.
     */
    public static <R> R executeAndKeepOpen(final Oracle oracle, final RPClosure<R, Oracle> actions) {
        return execute(oracle, actions, false);
    }

    /**
     * <p>executeAndKeepOpen.</p>
     *
     * @param oracle  a {@link oracle.spatial.rdf.client.jena.Oracle} object.
     * @param actions a {@link PClosure} object.
     */
    public static void executeAndKeepOpen(final Oracle oracle, final PClosure<Oracle> actions) {
        execute(oracle, actions, false);
    }

    /**
     * <p>doInTransaction.</p>
     *
     * @param oracle    a {@link oracle.spatial.rdf.client.jena.Oracle} object.
     * @param modelName a {@link java.lang.String} object.
     * @param actions   a {@link RPClosure} object.
     * @return a R object.
     */
    public static <R> R doInTransaction(final Oracle oracle, final String modelName,
                                        final RPClosure<R, Model> actions) {
        ModelOracleSem model = null;
        try {
            model = ModelOracleSem.createOracleSemModel(oracle, modelName);

            model.begin();
            R result = actions.call(model);
            model.commit();

            return result;
        } catch (RuntimeException e) {
            if (model != null) {
                rollbackQuietly(model.getGraph());
            }
            throw e;
        } catch (Exception e) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.CMR_INTERNAL_ERROR).withCause(e)
                    .withMessage(e.getMessage()).build();
        } finally {
            closeQuietly(null, model, null);
        }
    }

    /**
     * <p>doInTransaction.</p>
     *
     * @param oracle    a {@link oracle.spatial.rdf.client.jena.Oracle} object.
     * @param modelName a {@link java.lang.String} object.
     * @param actions   a {@link PClosure} object.
     */
    public static void doInTransaction(final Oracle oracle, final String modelName,
                                       final PClosure<Model> actions) {
        ModelOracleSem model = null;
        try {
            model = ModelOracleSem.createOracleSemModel(oracle, modelName);

            model.begin();
            actions.call(model);
            model.commit();
        } catch (RuntimeException e) {
            if (model != null) {
                rollbackQuietly(model.getGraph());
            }
            throw e;
        } catch (Exception e) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.CMR_INTERNAL_ERROR).withCause(e)
                    .withMessage(e.getMessage()).build();
        } finally {
            closeQuietly(null, model, null);
        }
    }

    /**
     * <p>doInTransaction.</p>
     *
     * @param oracle     a {@link oracle.spatial.rdf.client.jena.Oracle} object.
     * @param modelName  a {@link java.lang.String} object.
     * @param attachment a {@link oracle.spatial.rdf.client.jena.Attachment} object.
     * @param actions    a {@link RPClosure} object.
     * @return a R object.
     */
    public static <R> R doInTransaction(final Oracle oracle, final String modelName, final Attachment attachment,
                                        final RPClosure<R, Model> actions) {
        GraphOracleSem graphOracleSem = null;
        ModelOracleSem model = null;
        try {
            graphOracleSem = new GraphOracleSem(oracle, modelName, attachment);
            model = new ModelOracleSem(graphOracleSem);

            model.begin();
            R result = actions.call(model);
            model.commit();

            return result;
        } catch (RuntimeException e) {
            if (model != null) {
                rollbackQuietly(model.getGraph());
            }
            throw e;
        } catch (Exception e) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.CMR_INTERNAL_ERROR).withCause(e)
                    .withMessage(e.getMessage()).build();
        } finally {
            closeQuietly(null, model, graphOracleSem);
        }
    }

    /**
     * <p>doInTransaction.</p>
     *
     * @param oracle     a {@link oracle.spatial.rdf.client.jena.Oracle} object.
     * @param modelName  a {@link java.lang.String} object.
     * @param attachment a {@link oracle.spatial.rdf.client.jena.Attachment} object.
     * @param actions    a {@link PClosure} object.
     */
    public static void doInTransaction(final Oracle oracle, final String modelName, final Attachment attachment,
                                       final PClosure<Model> actions) {
        GraphOracleSem graphOracleSem = null;
        ModelOracleSem model = null;
        try {
            graphOracleSem = new GraphOracleSem(oracle, modelName, attachment);
            model = new ModelOracleSem(graphOracleSem);

            model.begin();
            actions.call(model);
            model.commit();
        } catch (RuntimeException e) {
            if (model != null) {
                rollbackQuietly(model.getGraph());
            }
            throw e;
        } catch (Exception e) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.CMR_INTERNAL_ERROR).withCause(e)
                    .withMessage(e.getMessage()).build();
        } finally {
            closeQuietly(null, model, graphOracleSem);
        }
    }

    /**
     * <p>closeQuietly.</p>
     *
     * @param oracle a {@link oracle.spatial.rdf.client.jena.Oracle} object.
     * @param model  a {@link org.apache.jena.rdf.model.Model} object.
     * @param graph  a {@link org.apache.jena.graph.Graph} object.
     */
    public static void closeQuietly(final Oracle oracle, final Model model, final Graph graph) {
        JenaQueryUtils.closeQuietly(model, graph);
        try {
            if (oracle != null) {
                oracle.dispose();
            }
        } catch (Exception ignore) {
            LOG.warn("Closing Oracle has failed with following error: {}", ignore.getMessage());
        }
    }

    /**
     * <p>closeQuietly.</p>
     *
     * @param oracle a {@link oracle.spatial.rdf.client.jena.Oracle} object.
     */
    public static void closeQuietly(final Oracle oracle) {
        closeQuietly(oracle, null, null);
    }

    private static void rollbackQuietly(final GraphOracleSem graph) {
        try {
            if (graph != null) {
                graph.rollbackTransaction();
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);
        }
    }
}
