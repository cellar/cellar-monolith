package eu.europa.ec.opoce.cellar.ontology.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

import static eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration.ONTOLOGY;

@Component
public class OntologyCacheListener {

    private static final Logger LOG = LogManager.getLogger(OntologyCacheListener.class);

    private static final String ONTOLOGY_CACHE_NAME = "ontology-cache";
    private final CacheManager cacheManager;

    @Autowired
    public OntologyCacheListener(CacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
    @CacheEvict(value = ONTOLOGY_CACHE_NAME, key = "#event.pid", beforeInvocation = true)
    public void clearCache(ClearOntologyCacheEvent event) {
        LOG.debug(ONTOLOGY, "{} evicted from {}", event, ONTOLOGY_CACHE_NAME);
    }
}
