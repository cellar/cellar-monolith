package eu.europa.ec.opoce.cellar.nal.domain;

import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;

import java.util.List;
import java.util.Objects;

/**
 * <p>LanguagesSettings class.</p>
 */
public class LanguagesSettings {

    private List<LanguageBean> languages;

    /**
     * <p>Constructor for LanguagesSettings.</p>
     *
     * @param languages a {@link java.util.List} object.
     */
    public LanguagesSettings(List<LanguageBean> languages) {
        this.languages = languages;
    }

    /**
     * <p>Getter for the field <code>languages</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<LanguageBean> getLanguages() {
        return languages;
    }

    /**
     * <p>Setter for the field <code>languages</code>.</p>
     *
     * @param languages a {@link java.util.List} object.
     */
    public void setLanguages(List<LanguageBean> languages) {
        this.languages = languages;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LanguagesSettings)) return false;
        LanguagesSettings that = (LanguagesSettings) o;
        return Objects.equals(getLanguages(), that.getLanguages());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getLanguages());
    }

    @Override
    public String toString() {
        return "LanguagesSettings{" +
                "languages=" + languages +
                '}';
    }
}
