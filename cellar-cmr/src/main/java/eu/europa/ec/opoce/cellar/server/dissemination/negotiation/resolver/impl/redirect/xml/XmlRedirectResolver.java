/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.xml
 *             FILE : XmlRedirectResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 1, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.xml;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.server.dissemination.AcceptLanguage;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.DisseminationRequest;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.Structure;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.IDisseminationResolver;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.RedirectResolver;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;
import eu.europa.ec.opoce.cellar.server.service.DisseminationException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Apr 1, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Configurable
public abstract class XmlRedirectResolver extends RedirectResolver {

    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    protected final String decoding;
    protected LanguageBean decodingLanguageBean;

    protected XmlRedirectResolver(final CellarResourceBean cellarResource, final String decoding, final AcceptLanguage acceptLanguage,
            final boolean provideAlternates) {
        super(cellarResource, acceptLanguage, provideAlternates);
        this.decoding = decoding;
    }

    public static IDisseminationResolver get(final CellarResourceBean cellarResource, final String decoding,
            final DisseminationRequest disseminationRequest, final AcceptLanguage acceptLanguage, final boolean provideAlternates) {

        switch (disseminationRequest.getStructure(cellarResource)) {
        case BRANCH: // xml
            return XmlBranchRedirectResolver.get(cellarResource, decoding, acceptLanguage, provideAlternates);
        case TREE: // xml
            return XmlTreeRedirectResolver.get(cellarResource, decoding, acceptLanguage, provideAlternates);
        default:
        case OBJECT: // xml
            return XmlObjectRedirectResolver.get(cellarResource, decoding, acceptLanguage, provideAlternates);
        }
    }

    @Override
    protected void initDisseminationRequest() {
        super.initDisseminationRequest();
        this.decodingLanguageBean = this.disseminationService.parseLanguageBean(this.decoding);
    }

    protected boolean hasDecodingLanguage() {
        return this.decodingLanguageBean != null;
    }

    protected boolean hasNoDecodingLanguage() {
        return this.decodingLanguageBean == null;
    }

    protected void throwErrorIfHasNoDecodingLanguage() {
        if (this.hasNoDecodingLanguage()) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage("Missing/invalid decoding language param: {}.").withMessageArgs(this.decoding).build();
        }
    }

    protected LanguageBean getDefaultDecodingLanguageBean() {
        return this.disseminationService.parseLanguageBean(this.cellarConfiguration.getCellarServiceDisseminationDecodingDefault());
    }

    @Override
    public ResponseEntity<Void> doHandleDisseminationRequest() {
        return this.seeOtherService.negotiateSeeOtherForXml(this.cellarResource, (Structure) this.getTypeStructure(),
                this.decodingLanguageBean, this.provideAlternates);
    }

    protected void setDecodingLanguage() {
        if (this.hasNoDecodingLanguage()) {
            if (this.acceptLanguage != null) {
                final String languageCode = this.acceptLanguage.getLanguageCode();
                final LanguageBean language = this.disseminationService.parseLanguageBean(languageCode);
                this.decodingLanguageBean = language;
            } else {
                this.decodingLanguageBean = this.getDefaultDecodingLanguageBean();
            }
        }
    }

}
