package eu.europa.ec.opoce.cellar.cmr.notice;

import com.google.common.base.MoreObjects;
import eu.europa.ec.opoce.cellar.cmr.ContentType;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class NoticeCacheKey {

    private String cellarId;
    private Map<ContentType, String> versions;

    public NoticeCacheKey() {
    }

    public NoticeCacheKey(String cellarId, Map<ContentType, String> versions) {
        this.cellarId = cellarId;
        this.versions = new HashMap<>(versions);
    }

    public String getCellarId() {
        return cellarId;
    }

    public Map<ContentType, String> getVersions() {
        return versions;
    }

    public String getVersion(ContentType contentType) {
        return versions.get(contentType);
    }

    /**
     * Workaround to retrieve a file from S3 when the versions
     * are not present (versions for legacy data are not set in
     * Oracle)
     */
    public void addContentTypes(ContentType... types) {
        for (ContentType type : types) {
            if (!versions.containsKey(type)) {
                versions.put(type, null);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NoticeCacheKey that = (NoticeCacheKey) o;
        return Objects.equals(cellarId, that.cellarId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cellarId);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("cellarId", cellarId)
                .add("versions", versions)
                .toString();
    }
}
