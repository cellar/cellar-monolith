/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice
 *             FILE : IndexNoticeService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 09 18, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-09-18 15:50:23 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.notice.indexing;

import eu.europa.ec.opoce.cellar.cmr.metsbeans.Expression;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Work;
import eu.europa.ec.opoce.cellar.cmr.notice.MetsElementWithLanguage;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.IndexNoticeWork;
import eu.europa.ec.opoce.cellar.common.util.Triple;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import org.w3c.dom.Document;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ARHS Developments
 */
public interface IndexNoticeService {
    /**
     * generate indexnotice for a work.
     *
     * @param workUri uri of the work that needs indexing
     * @return indexnotice for each of the required languages
     */
    IndexNoticeWork createIndexNoticesWork(String workUri);

    /**
     * generate indexnotice for a work.
     *
     * @param work work that needs to be indexed, this element also needs the children to be filled in, otherwise only the work will be indexed not childs
     * @return indexnotice for each of the required languages
     */
    IndexNoticeWork createIndexNoticesWork(Work work);

    /**
     * generate indexnotice for expressions.
     *
     * @param expressions expressions that need to be indexed, make sure the hierarchy elements are filled in otherwise this will not work
     * @return indexnotice for each of the expressions
     */
    Map<Expression, HashMap<LanguageBean, Document>> createIndexNoticesWork(Collection<Expression> expressions);

    /**
     * generate indexnotice for a expression.
     *
     * @param expressionUri uri of the expression that needs to be indexed
     * @return indexnotice for the expression
     */
    List<Triple<Expression, LanguageBean, Document>> createIndexNoticeExpression(String expressionUri);

    /**
     * generate indexnotice for a manifestation.
     *
     * @param manifestUri uri of the manifestation that needs to be indexed
     * @return indexnotice for the manifestation
     */
    List<Triple<Expression, LanguageBean, Document>> createIndexNoticeManifestation(String manifestUri);

    /**
     * generate indexnotices for a dossier.
     *
     * @param dossierUri uri of the dossier that needs to be indexed
     * @return indexnotice for the dossier in each required language
     */
    Map<MetsElementWithLanguage, Document> createIndexNoticeDossier(String dossierUri);

    /**
     * generate indexnotices for a event.
     *
     * @param eventUri uri of the event that needs to be indexed
     * @return indexnotice for the event in each required language
     */
    Map<MetsElementWithLanguage, Document> createIndexNoticeEvent(String eventUri);

    /**
     * generate indexnotices for a agent.
     *
     * @param agentUri uri of the agent that needs to be indexed
     * @return indexnotice for the agent in each required language
     */
    Map<MetsElementWithLanguage, Document> createIndexNoticeAgent(String agentUri);

    /**
     * generate indexnotices for a topLevelEvent.
     *
     * @param topLevelEventUri uri of the topLevelEvent that needs to be indexed
     * @return indexnotice for the topLevelEvent in each required language
     */
    Map<MetsElementWithLanguage, Document> createIndexNoticeTopLevelEvent(String topLevelEventUri);
}
