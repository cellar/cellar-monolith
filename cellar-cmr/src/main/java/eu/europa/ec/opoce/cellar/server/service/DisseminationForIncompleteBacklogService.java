/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.service
 *             FILE : DisseminationService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 27, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$$
 *          VERSION : $LastChangedRevision$$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.service;

import org.apache.jena.rdf.model.Model;

import eu.europa.ec.opoce.cellar.domain.content.mets.CellarIdentifiedObject;

/**
 * <p>DisseminationForIncompleteBacklogService interface.</p>
 */
public interface DisseminationForIncompleteBacklogService {

    /**
     * Improve model to support backlog by adding missing properties 
     * 
     * @param model the return model
     * @param directModel the direct model
     * @param inferredModel the inferred model
     * @param cellarObject the cellar object
     * @param inferred true if the return model must contain the inferred model
     */
    void checkPropertiesFromBacklog(Model model, final Model directModel, final Model inferredModel,
            final CellarIdentifiedObject cellarObject, final boolean inferred);

}
