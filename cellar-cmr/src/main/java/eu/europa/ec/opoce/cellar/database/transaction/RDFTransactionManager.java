/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.jena.oracle
 *             FILE : RDFTransactionManager.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Mar 14, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.database.transaction;

import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.jena.oracle.OraTemplate;

import java.sql.SQLException;
import java.util.concurrent.ConcurrentHashMap;

import oracle.spatial.rdf.client.jena.Oracle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <class_description> This is a singleton that manages the transactions on those RDF tables which are managed by Oracle Spatial.<br/>
 * It is necessary as Oracle Spatial does not support any transaction mechanism.<br/><br/>
 * This singleton offers such support by sharing a table where the key is the current thread's id, and the value the current open connection to the RDF Store.
 * The connection is tipically opened and put onto the shared table by implementations of {@link eu.europa.ec.opoce.cellar.jena.oracle.LockedConnection}.<br/>
 * When asked for commit (rollback), the singleton searches the table for the entry with the current thread's id, gets the connection,
 * commits (rolls back) it, and closes it.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Mar 14, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public enum RDFTransactionManager {

    INSTANCE;

    private static final Logger LOG = LoggerFactory.getLogger(RDFTransactionManager.class);

    private ConcurrentHashMap<Long, Oracle> threadConnectionMap;

    private RDFTransactionManager() {
        this.threadConnectionMap = new ConcurrentHashMap<>();
    }

    /**
     * Puts a new {@link connection} onto the table.
     * 
     * @param oracle the Oracle Spatial connection to put onto the table
     */
    public void put(final Oracle oracle) {
        this.threadConnectionMap.putIfAbsent(Thread.currentThread().getId(), oracle);
    }

    /**
     * Commits the transaction of the connection whose id is the current thread's id.<br />
     * Then, closes the connection.
     * 
     * @throws CellarException should a problem occur during the commit
     */
    public void commit() throws CellarException {
        // resolves connection from current thread
        final Oracle connection = resolveCurrentThreadConnection();
        if (connection == null) {
            return;
        }

        // tries to commit
        try {
            connection.commitTransaction();
        } catch (SQLException e) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage(
                            "A problem occurred while committing the transaction associated to the Oracle Spatial connection created by thread '{}'.")
                    .withMessageArgs(Thread.currentThread().getId()).withCause(e).build();
        } finally {
            OraTemplate.closeQuietly(connection);
        }
    }

    /**
     * Rolls back the transaction of the connection whose id is the current thread's id.<br />
     * Then, closes the connection.
     * 
     * @throws CellarException should a problem occur during the rollback
     */
    public void rollback() throws CellarException {
        // resolves connection from current thread
        final Oracle connection = resolveCurrentThreadConnection();
        if (connection == null) {
            return;
        }

        // tries to rollback
        try {
            connection.rollbackTransaction();
        } catch (SQLException e) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage(
                            "A problem occurred while rolling back the transaction associated to the Oracle Spatial connection created by thread '{}'.")
                    .withMessageArgs(Thread.currentThread().getId()).withCause(e).build();
        } finally {
            OraTemplate.closeQuietly(connection);
        }
    }

    /**
     * Closes the connection whose id is the current thread's id.
     */
    public void close() throws CellarException {
        // resolves connection from current thread
        final Oracle connection = resolveCurrentThreadConnection();
        if (connection == null) {
            return;
        }

        // close it
        OraTemplate.closeQuietly(connection);
    }

    private Oracle resolveCurrentThreadConnection() {
        // retrieve the connection associated to the current thread
        final long threadId = Thread.currentThread().getId();
        final Oracle connection = this.threadConnectionMap.remove(threadId);
        if (connection == null) {
            LOG.info("There is no Oracle Spatial connection associated to thread with id '{}'.", threadId);
        }
        return connection;
    }

}
