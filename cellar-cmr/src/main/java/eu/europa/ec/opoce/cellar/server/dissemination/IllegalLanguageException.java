/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination
 *        FILE : IllegalLanguageException.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 30-01-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination;

import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareException;
import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;

/**
 * This is a {@link IllegalLanguageException} buildable by an {@link HttpStatusAwareExceptionBuilder}.</br>
 * </br> 
 * ON : 30-01-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public class IllegalLanguageException extends HttpStatusAwareException {

    private static final long serialVersionUID = -3411780520458765151L;

    private ErrorMessage errorMessage;

    /**
     * Constructs a new exception with its associated builder
     * 
     * @param builder the builder to use for building the exception
     */
    public IllegalLanguageException(final HttpStatusAwareExceptionBuilder<? extends HttpStatusAwareException> builder) {
        super(builder);
        this.errorMessage = new ErrorMessage(this.getHttpStatus(), this.getMessage());
    }

    public ErrorMessage getErrorMessage() {
        return errorMessage;
    }

}
