/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.indexing.processor.impl
 *             FILE : AbstractIndexRequestProcessor.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Nov 17, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.indexing.processor.impl;

import eu.europa.ec.opoce.cellar.cmr.indexing.processor.IndexRequestProcessor;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestBatch;
import eu.europa.ec.opoce.cellar.monitoring.Watch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.UUID;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Nov 17, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public abstract class AbstractIndexRequestProcessor implements IndexRequestProcessor {

    private static final Logger LOG = LogManager.getLogger(AbstractIndexRequestProcessor.class);

    @Watch(value = "AbstractIndexRequestProcessor", arguments = {
            @Watch.WatchArgument(name = "objectUri", expression = "cmrIndexRequest.objectUri")
    })
    @Override
    public void execute(CmrIndexRequestBatch request) {
        final List<CmrIndexRequest> requests = filter(request);
        if (!requests.isEmpty()) {
            LOG.debug("Execute [{}] for [{}]", getClass(), request);
            try {
                execute(request, requests);
                requests.forEach(r -> r.setExecutionStatus(CmrIndexRequest.ExecutionStatus.Done));
            } catch (Exception e) {
                final String cause = UUID.randomUUID().toString();
                requests.forEach(r -> {
                    r.setExecutionStatus(CmrIndexRequest.ExecutionStatus.Error);
                    r.setStatusCause(cause);
                });
                LOG.error("[" + cause + "] Unexcepted error.", e);
            }
        }
    }

    protected abstract void execute(CmrIndexRequestBatch batch, List<CmrIndexRequest> requests);

    protected abstract List<CmrIndexRequest> filter(CmrIndexRequestBatch request);
}
