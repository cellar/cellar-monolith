/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.ingest.impl
 *             FILE : CMRBasedStructMapLoader.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 07-01-2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.ingest.impl;

import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.common.metadata.IExistingMetadataLoader;
import eu.europa.ec.opoce.cellar.common.util.Pair;
import eu.europa.ec.opoce.cellar.database.CmrTableName;
import eu.europa.ec.opoce.cellar.database.IRDFStoreRelationalGateway;
import org.apache.jena.graph.Triple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * <class_description> Service for loading existing data from the CMR layer.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 07-01-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service("cmrBasedExistingMetadataLoader")
public class CMRBasedExistingMetadataLoader implements IExistingMetadataLoader<String, Map<Pair<Triple, String>, String>, Map<ContentType, String>> {

    /**
     * The cmr metadata relational gateway.
     */
    @Autowired
    @Qualifier("cmrMetadataRelationalGateway")
    private IRDFStoreRelationalGateway cmrMetadataRelationalGateway;

    /**
     * Load direct and inferred.
     *
     * @param cellarId the cellar id
     * @param options  the options
     * @return the map
     * @see eu.europa.ec.opoce.cellar.common.metadata.IExistingMetadataLoader#loadDirectAndInferred(java.lang.Object, java.lang.Object[])
     */
    @Override
    public Map<Pair<Triple, String>, String> loadDirectAndInferred(final String cellarId, final Object... options) {
        return this.load(cellarId, CmrTableName.CMR_METADATA);
    }

    /**
     * Load inverse.
     *
     * @param cellarId the cellar id
     * @return the map
     * @see eu.europa.ec.opoce.cellar.common.metadata.IExistingMetadataLoader#loadInverse(java.lang.Object)
     */
    @Override
    public Map<Pair<Triple, String>, String> loadInverse(final String cellarId) {
        return this.load(cellarId, CmrTableName.CMR_INVERSE);
    }

    /**
     * Load the metadata from CMR_* and the matching embargo table CMR_*_EMBARGO.
     * The data can be spread between tables because the embargo can be done at any DO level
     * The method selectCompleteContext does a LIKE on the WORK CELLAR ID
     *
     * @param cellarId the cellar id
     * @param table    the table
     * @return the map
     */
    private Map<Pair<Triple, String>, String> load(final String cellarId, final CmrTableName table) {
        final Map<Pair<Triple, String>, String> retrievedPairs = new HashMap<>();
        retrievedPairs.putAll(this.cmrMetadataRelationalGateway.selectCompleteContext(table, cellarId));
        retrievedPairs.putAll(this.cmrMetadataRelationalGateway.selectCompleteContext(table.getEmbargoTable(), cellarId));
        return retrievedPairs;
    }
}
