/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.service.impl
 *             FILE : EmbargoHandlingServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jul 15, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$$
 *          VERSION : $LastChangedRevision$$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.service.impl;

import eu.europa.ec.opoce.cellar.ccr.service.database.DefaultTransactionService;
import eu.europa.ec.opoce.cellar.cl.concurrency.Concurrent;
import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.StructuredQueryService;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.HierarchyElement;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement;
import eu.europa.ec.opoce.cellar.cmr.model.ModelUtils;
import eu.europa.ec.opoce.cellar.cmr.scheduling.Scheduler;
import eu.europa.ec.opoce.cellar.cmr.service.EmbargoOperationService;
import eu.europa.ec.opoce.cellar.cmr.service.EmbargoService;
import eu.europa.ec.opoce.cellar.cmr.utils.EmbargoAudit;
import eu.europa.ec.opoce.cellar.cmr.utils.EmbargoUtils;
import eu.europa.ec.opoce.cellar.common.util.DigitalObjectUtils;
import eu.europa.ec.opoce.cellar.common.util.Pair;
import eu.europa.ec.opoce.cellar.common.util.TimeUtils;
import eu.europa.ec.opoce.cellar.common.util.Triple;
import eu.europa.ec.opoce.cellar.database.transaction.RDFTransactional;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.domain.content.mets.OffIngestionOperationType;
import eu.europa.ec.opoce.cellar.domain.content.mets.OperationSubType;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMapType;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.VirtuosoOperationException;
import eu.europa.ec.opoce.cellar.logging.LogContext;
import eu.europa.ec.opoce.cellar.ontology.service.OntologyService;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaQueries;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import org.apache.commons.collections.CollectionUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static eu.europa.ec.opoce.cellar.CommonErrors.INVALID_EMBARGO_DATE_IN_OBJECT_HIERARCHY;
import static eu.europa.ec.opoce.cellar.CommonErrors.MISSING_EMBARGO_DATE_IN_OBJECT_HIERARCHY;
import static eu.europa.ec.opoce.cellar.cmr.ContentType.DIRECT;
import static eu.europa.ec.opoce.cellar.cmr.ContentType.DIRECT_INFERRED;
import static eu.europa.ec.opoce.cellar.common.util.CellarIdUtils.getParentCellarId;
import static eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType.ITEM;
import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.CMR_EMBARGO;


/**
 * <p>
 * EmbargoHandlingService class.
 * </p>
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
@LogContext
public class EmbargoServiceImpl extends DefaultTransactionService implements EmbargoService {

    private static final Logger LOG = LogManager.getLogger(EmbargoServiceImpl.class);

    @Autowired
    private StructuredQueryService structuredQueryService;

    @Autowired
    private CellarResourceDao cellarResourceDao;

    @Autowired
    @Qualifier("pidManagerService")
    private IdentifierService identifierService;

    @Autowired
    @Qualifier("embargoOperationService")
    private EmbargoOperationService embargoOperationService;

    @Autowired
    @Qualifier("automaticDisembargoOperationService")
    private EmbargoOperationService automaticDisembargoOperationService;

    @Autowired
    private ContentStreamService contentStreamService;

    @Autowired
    private OntologyService ontologyService;

    @Autowired
    private Scheduler scheduler;

    /**
     * Gets the disembargoeable works
     *
     * @return the disembargoeable works
     */
    @Override
    public Set<Pair<String, Date>> getDisembargoeableDigitalObjects() {
        return this.structuredQueryService.getEmbargoChanged().stream()
                .filter(pair -> TimeUtils.isPastDate(pair.getTwo()))
                .map(pair -> {
                    final String identifier = this.identifierService.getPrefixed(pair.getOne());
                    final Date embargoDate = pair.getTwo();
                    return new Pair<>(identifier, embargoDate);
                })
                .collect(Collectors.toSet());
    }

    @Override
    @LogContext(CMR_EMBARGO)
    @Concurrent(locker = OffIngestionOperationType.EMBARGO)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = CellarException.class, noRollbackFor = VirtuosoOperationException.class)
    @RDFTransactional(rollbackFor = CellarException.class)
    public void changeEmbargo(final String cellarId, final Date embargoDate) {
        this.executeEmbargo(cellarId, embargoDate, false);
    }

    @Override
    @LogContext(CMR_EMBARGO)
    @Concurrent(locker = OffIngestionOperationType.EMBARGO)
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = CellarException.class, noRollbackFor = VirtuosoOperationException.class)
    @RDFTransactional(rollbackFor = CellarException.class)
    public void automaticDisembargo(final String cellarId, final Date embargoDate) {
        this.executeEmbargo(cellarId, embargoDate, true);
    }

    private void executeEmbargo(final String cellarId, final Date embargoDate, final boolean automatic) {
        final EmbargoAudit embargoAudit = new EmbargoAudit(cellarId, embargoDate, automatic);

        try {
            embargoAudit.logStart();

            if(automatic) {
                this.automaticDisembargoOperationService.execute(cellarId, embargoDate);

            } else {
                this.embargoOperationService.execute(cellarId, embargoDate);
            }
            embargoAudit.logEnd();
        } catch (final RuntimeException e) {
            embargoAudit.logException(e);
            throw ExceptionBuilder.get(CellarException.class)
                    .withAuditBuilder(embargoAudit.getAuditBuilder())
                    .build();
        }
    }

    public void reschedule(){
        scheduler.reschedule(false);
    }


    @Override
    public HierarchyElement<MetsElement, MetsElement> getPublicHierarchyElement(final String psi) {
        final Identifier identifier = this.identifierService.getIdentifier(psi);
        final String cellarId = identifier.getCellarId();

        if (!this.cellarResourceDao.isUnderEmbargo(cellarId)) {
            return null;
        }

        return getHierarchyElement(identifier);
    }

    @Override
    public List<HierarchyElement<MetsElement, MetsElement>> getEmbargoHierarchyElements() {
        final Model digitalObjects = this.structuredQueryService.getEmbargoDigitalObjects();

        try {
            return convertModelInHierarchyElements(digitalObjects);
        } finally {
            JenaUtils.closeQuietly(digitalObjects);
        }
    }

    private HierarchyElement<MetsElement, MetsElement> getHierarchyElement(final Identifier identifier) {
        final String cellarId = identifier.getCellarId();
        final Collection<String> productionUris = new ArrayList<>();

        for (final String pid : identifier.getPids()) {
            final String uri = this.identifierService.getUri(pid);
            productionUris.add(uri);
        }

        final CellarResource cellarResource = this.cellarResourceDao.findCellarId(cellarId);
        final DigitalObjectType type = cellarResource.getCellarType();
        final Date embargoDate = cellarResource.getEmbargoDate();
        final Date lastModificationDate = cellarResource.getLastModificationDate();

        final HierarchyElement<MetsElement, MetsElement> result = HierarchyElement.getTriple(type).getOne();
        result.setCellarId(cellarId);
        result.addUris(productionUris);
        result.setEmbargoDate(embargoDate);
        result.setLastModificationDate(lastModificationDate);

        return result;
    }

    private List<HierarchyElement<MetsElement, MetsElement>> convertModelInHierarchyElements(final Model model) {
        return DigitalObjectType.getEmbargoTypes().stream()
                .map(type -> extractHierarchyElementFromModel(model, HierarchyElement.getTriple(type)))
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    private List<HierarchyElement<MetsElement, MetsElement>> extractHierarchyElementFromModel(final Model model,
                                                                                              final Triple<HierarchyElement<MetsElement, MetsElement>, Property, Resource> triple) {
        final List<HierarchyElement<MetsElement, MetsElement>> result = new ArrayList<>();
        final ResIterator resIterator = model.listResourcesWithProperty(RDF.type, triple.getTwo());
        try {
            while (resIterator.hasNext()) {
                final Resource resource = resIterator.next();
                final HierarchyElement<MetsElement, MetsElement> create = create(triple, resource);
                result.add(create);
            }
        } finally {
            resIterator.close();
        }
        return result;
    }

    private HierarchyElement<MetsElement, MetsElement> create(final Triple<HierarchyElement<MetsElement, MetsElement>, Property, Resource> triple,
                                                              final Resource resource) {
        final HierarchyElement<MetsElement, MetsElement> result = triple.getOne();
        setUris(result, resource);
        final String embargoDate = JenaQueries.getLiteralValue(resource, triple.getTwo(), false, true);

        if (embargoDate != null) {
            setEmbargoDate(result, embargoDate);
        }

        result.setLastModificationDate(new Date());
        return result;
    }

    private void setUris(final MetsElement metsElement, final Resource resource) {
        metsElement.addUri(resource.getURI());
        final List<String> resourceUris = JenaQueries.getResourceUris(resource, OWL.sameAs);
        for (final String sameAs : resourceUris) {
            metsElement.addUri(sameAs);
        }
    }

    private void setEmbargoDate(final HierarchyElement<MetsElement, MetsElement> hierarchyElement, final String embargoDate) {
        final Date date = EmbargoUtils.parseDate(embargoDate);
        hierarchyElement.setEmbargoDate(date);
    }

    @Override
    public void validateEmbargoDate(final List<DigitalObject> elements) {
        LOG.debug("Validating embargo date");

        for (final DigitalObject child : elements) {
            final Date childEmbargoDate = child.getEmbargoDate();

            if (childEmbargoDate == null) {
                continue;
            }

            final List<DigitalObject> parents = DigitalObjectUtils.getParents(child);
            for (final DigitalObject parent : parents) {
                Date parentEmbargoDate = parent.getEmbargoDate();
                if (parentEmbargoDate == null) {
                    /* The parent embargo date is not specified in the UPDATE package,
                       but it may exist in the database. */
                    parentEmbargoDate = getEmbargoDateOnUpdate(parent);
                }

                if (parentEmbargoDate == null || TimeUtils.isPastDate(parentEmbargoDate)) {
                    continue;
                }

                if (childEmbargoDate.before(parentEmbargoDate)) {
                    throw ExceptionBuilder.get(CellarException.class)
                            .withMessage("A lower level digital object cannot have an embargo date inferior to the embargo date of a superior digital object.")
                            .withCode(INVALID_EMBARGO_DATE_IN_OBJECT_HIERARCHY)
                            .build();
                }
            }
        }
    }

    private Date getEmbargoDateOnUpdate(final DigitalObject element) {
        final Date embargoDate = element.getEmbargoDate();
        if (embargoDate != null) {
            return embargoDate;
        }

        final String cellarId = resolveCellarId(element);
        if (cellarId != null) {
            final CellarResource parentCellarResource = cellarResourceDao.findCellarId(cellarId);
            if (parentCellarResource != null) {
                return parentCellarResource.getEmbargoDate();
            }
        }

        return null;
    }

    private String resolveCellarId(final DigitalObject element) {
        final ContentIdentifier cellarId = element.getCellarId();
        if (cellarId != null) {
            return cellarId.toString();
        }

        final List<ContentIdentifier> ids = element.getContentids();
        if (!ids.isEmpty()) {
            return identifierService.getCellarUUIDIfExists(ids.get(0).getIdentifier());
        }

        return null;
    }

    @Override
    public void validateEmbargoDate(final StructMap ingestedStructMap) {
        final DigitalObject ingestedWork = ingestedStructMap.getDigitalObject();
        final OperationSubType resolveOperationSubType = ingestedStructMap.resolveOperationSubType();
        final StructMapType metadataOperationType = ingestedStructMap.getMetadataOperationType();

        if (requireEmbargoDateValidation(resolveOperationSubType, metadataOperationType)) {
            final List<DigitalObject> ingestedDigitalObjects = ingestedWork.getAllChilds(true);

            if (isEmbargoPropertySet(ingestedDigitalObjects)) {
                // get existing resources
                final List<String> storedCellarIds = getStoredCellarIds(ingestedWork.getCellarId());
                final List<String> declaredCellarIds = getDeclaredCellarIds(ingestedDigitalObjects);
                // check for missing resources
                final Collection missingCellarIds = CollectionUtils.disjunction(storedCellarIds, declaredCellarIds);

                if (!missingCellarIds.isEmpty()) {
                    throw ExceptionBuilder.get(CellarException.class)
                            .withMessage("When trying to perform an embargo during the ingestion through a MERGE or " +
                                    "UPDATE node.metadata package the embargo date property must be set to " +
                                    "every digital object. The METS is not declaring the following digital " +
                                    "objects :{}")
                            .withCode(MISSING_EMBARGO_DATE_IN_OBJECT_HIERARCHY)
                            .withMessageArgs(Arrays.toString(missingCellarIds.toArray()))
                            .build();
                }
            }
        }
    }

    private static boolean requireEmbargoDateValidation(final OperationSubType operationType, StructMapType structMapType) {
        //TODO what about APPEND
        return OperationSubType.Merge.equals(operationType)
                || (OperationSubType.Update.equals(operationType) && StructMapType.Node.equals(structMapType));
    }

    private static boolean isEmbargoPropertySet(final List<DigitalObject> ingestedDigitalObjects) {
        return ingestedDigitalObjects.stream()
                .map(DigitalObject::getEmbargoDate)
                .anyMatch(Objects::nonNull);
    }

    private List<String> getStoredCellarIds(ContentIdentifier identifier) {
        return this.cellarResourceDao
                .findStartingWithSortByCellarId(identifier.getIdentifier())
                .stream()
                .map(CellarResource::getCellarId)
                .filter(cellarId -> !DigitalObjectType.isItem(cellarId))
                .collect(Collectors.toList());
    }

    private static List<String> getDeclaredCellarIds(final List<DigitalObject> ingestedDigitalObjects) {
        return ingestedDigitalObjects.stream()
                .map(element -> element.getCellarId().getIdentifier())
                .collect(Collectors.toList());
    }

    @Override
    public boolean isUnderEmbargo(String cellarId) {
        return this.cellarResourceDao.isUnderEmbargo(cellarId);
    }

    @Override
    public boolean isUnderEmbargo(CellarResource resource) {
        final Date embargoDate = resource.getEmbargoDate();
        return embargoDate != null && embargoDate.after(new Date());
    }




    @Override
    @Transactional
    public void updateEmbargoDate(String cellarId, Date embargoDate, Date lastModificationDate, DigitalObjectType digitalObjectType) {
        Model direct = null;
        Model inferred = null;

        try {
            // MD of item is in manifestation
            final String streamCellarId = digitalObjectType == ITEM ? getParentCellarId(cellarId) : cellarId;

            CellarResource resource = cellarResourceDao.findCellarId(streamCellarId);
            Map<ContentType, String> v = new EnumMap<>(ContentType.class);
            v.put(DIRECT, resource.getVersion(DIRECT).orElse(null));
            v.put(DIRECT_INFERRED, resource.getVersion(DIRECT_INFERRED).orElse(null));

            // Get MD stream from S3
            Map<ContentType, String> streams = contentStreamService.getContentsAsString(streamCellarId, v);

            direct = JenaUtils.read(streams.get(DIRECT), Lang.NT);
            inferred = JenaUtils.read(streams.get(DIRECT_INFERRED), Lang.NT);

            final String cellarIdUri = identifierService.getUri(cellarId);
            ModelUtils.updateDateModel(cellarIdUri, embargoDate, direct, ontologyService.getEmbargoDateProperties(digitalObjectType, false));
            ModelUtils.updateDateModel(cellarIdUri, embargoDate, inferred, ontologyService.getEmbargoDateProperties(digitalObjectType, true));

            // Update lastModificationDate

            final List<String> lastModificationDatePropertyList = Collections.singletonList(CellarProperty.cmr_lastmodificationdate);
            ModelUtils.updateDateModel(cellarIdUri, lastModificationDate, direct, lastModificationDatePropertyList);
            ModelUtils.updateDateModel(cellarIdUri, lastModificationDate, inferred, lastModificationDatePropertyList);

            // Set updated models
            String directModel = JenaUtils.toString(direct, RDFFormat.NT);
            String inferredModel = JenaUtils.toString(inferred, RDFFormat.NT);

            // Update S3
            String directVersion = contentStreamService.writeContent(streamCellarId, new ByteArrayResource(directModel.getBytes(StandardCharsets.UTF_8)), DIRECT);
            String inferredVersion = contentStreamService.writeContent(streamCellarId, new ByteArrayResource(inferredModel.getBytes(StandardCharsets.UTF_8)), DIRECT_INFERRED);
            // Update CellarResource with the new versions
            resource.putVersion(DIRECT, directVersion);
            resource.putVersion(DIRECT_INFERRED, inferredVersion);

            cellarResourceDao.updateObject(resource);

        } finally {
            JenaUtils.closeQuietly(direct, inferred);
        }
    }

}
