package eu.europa.ec.opoce.cellar.cmr;

import io.micrometer.core.instrument.Clock;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.jmx.JmxConfig;
import io.micrometer.jmx.JmxMeterRegistry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

@Configuration
public class MetricsConfiguration {

    @Bean
    public MeterRegistry metricRegistry() {
        JmxConfig jmxConfig = new JmxConfig() {

            @Override
            public String get(String s) {
                return null;
            }

            @Override
            public Duration step() {
                return Duration.ofSeconds(10);
            }
        };

        JmxMeterRegistry jmxMeterRegistry = new JmxMeterRegistry(jmxConfig, Clock.SYSTEM);
        Metrics.addRegistry(jmxMeterRegistry);
        return jmxMeterRegistry;
    }
}
