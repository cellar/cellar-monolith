/*   Copyright (C) <2018>  <Publications Office of the European Union>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    contact: <https://publications.europa.eu/en/web/about-us/contact>
 */
package eu.europa.ec.opoce.cellar.cmr.advice;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.StructMapProcessorException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Order(3)
public class ThrowExceptionAdvice {

    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    @Around("@annotation(throwExceptionForTestPurpose)")
    public Object Process(final ProceedingJoinPoint proceedingJoinPoint, final ThrowExceptionForTestPurpose throwExceptionForTestPurpose) throws Throwable {

        final boolean throwExceptionEnabled = this.cellarConfiguration.isTestEnabled() && this.cellarConfiguration.isTestRollbackEnabled();

        Object proceed = proceedingJoinPoint.proceed(proceedingJoinPoint.getArgs());

        if (throwExceptionEnabled) {

            final CellarException exception = ExceptionBuilder.get(StructMapProcessorException.class)
                    .withCode(CommonErrors.TEST_EXCEPTION)
                    .withMessage("FORCED EXCEPTION FOR TEST PURPOSES").build();
            throw exception;
        }
        return proceed;
    }
}
