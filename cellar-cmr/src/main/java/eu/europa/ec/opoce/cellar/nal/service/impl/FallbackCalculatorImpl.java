package eu.europa.ec.opoce.cellar.nal.service.impl;

import eu.europa.ec.opoce.cellar.common.util.MapUtils;
import eu.europa.ec.opoce.cellar.common.util.Pair;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.domain.FallBackNode;
import eu.europa.ec.opoce.cellar.nal.domain.RequiredLanguageBean;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.nal.service.FallbackCalculator;
import eu.europa.ec.opoce.cellar.nal.service.LanguageService;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaQueries;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarType;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDF;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class FallbackCalculatorImpl implements FallbackCalculator {
    private LanguageService languageService;
    private Model model;
    private List<RequiredLanguageBean> requiredLanguages;
    private Map<Resource, List<FallBackNode>> fallBacksPerResource = new HashMap<>();
    private Map<Resource, Set<String>> missingLanguagesPerResource = new HashMap<>();

    /**
     * <p>Constructor for FallbackCalculator.</p>
     *
     * @param model             a {@link org.apache.jena.rdf.model.Model} object.
     * @param requiredLanguages a {@link java.util.List} object.
     * @param languageService
     * @param model             a {@link Model} object.
     * @param requiredLanguages a {@link List} object.
     */
    FallbackCalculatorImpl(LanguageService languageService, Model model, List<RequiredLanguageBean> requiredLanguages) {
        if (model == null) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument model cannot be null").build();
        }
        if (requiredLanguages == null) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument requiredLanguages cannot be null").build();
        }
        if (languageService == null) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument languageService cannot be null").build();
        }
        this.languageService = languageService;
        this.model = model;
        this.requiredLanguages = new ArrayList<>(requiredLanguages);
    }

    /**
     * <p>reset.</p>
     */
    @Override
    public void reset() {
        fallBacksPerResource.clear();
        missingLanguagesPerResource.clear();
    }

    /**
     * <p>calculate.</p>
     */
    @Override
    public void calculate() {
        for (Resource concept : listConcepts()) {
            calculateFallbackNodes(concept);
        }
    }

    /**
     * <p>calculateFallbackNodes.</p>
     *
     * @param resource a {@link org.apache.jena.rdf.model.Resource} object.
     * @return a {@link eu.europa.ec.opoce.cellar.common.util.Pair} object.
     */
    @Override
    public Pair<List<FallBackNode>, Set<String>> calculateFallbackNodes(Resource resource) {
        if (fallBacksPerResource.containsKey(resource)) {
            return new Pair<>(Collections.unmodifiableList(fallBacksPerResource.get(resource)),
                    getMissingLanguages(resource));
        }

        Map<String, String> labelsPerLanguage = getlabelsPerLanguage(resource);

        List<FallBackNode> fallBackNodes = MapUtils.addMappedList(fallBacksPerResource, resource);
        for (RequiredLanguageBean requiredLanguageBean : requiredLanguages) {
            LanguageBean langBean = languageService.getByUri(requiredLanguageBean.getUri());
            if (langBean == null) {
                throw ExceptionBuilder.get(CellarException.class).withMessage("Missing language configuration for required language '{}'")
                        .withMessageArgs(requiredLanguageBean.getUri()).build();
            }

            String twoCharCodeRequiredLanguage = langBean.getIsoCodeTwoChar();
            String threeCharCodeRequiredLanguage = langBean.getIsoCodeThreeChar();

            if (StringUtils.isBlank(twoCharCodeRequiredLanguage) && StringUtils.isBlank(threeCharCodeRequiredLanguage)) {
                throw ExceptionBuilder.get(CellarException.class).withMessage("Missing two and three char code for required language '{}'")
                        .withMessageArgs(requiredLanguageBean.getUri()).build();
            }
            if (labelsPerLanguage.containsKey(twoCharCodeRequiredLanguage)
                    || labelsPerLanguage.containsKey(threeCharCodeRequiredLanguage)) {
                continue;
            }

            boolean found = false;
            for (String alternative : requiredLanguageBean.getAlternatives()) {
                LanguageBean alterBean = languageService.getByUri(alternative);
                if (alterBean == null) {
                    throw ExceptionBuilder.get(CellarException.class).withMessage("Missing language configuration for language '{}'")
                            .withMessageArgs(alternative).build();
                }

                String twoCharCodeAltLanguage = alterBean.getIsoCodeTwoChar();
                String threeCharCodeAltLanguage = alterBean.getIsoCodeThreeChar();

                if (StringUtils.isBlank(twoCharCodeAltLanguage) && StringUtils.isBlank(threeCharCodeAltLanguage)) {
                    throw ExceptionBuilder.get(CellarException.class).withMessage("Missing two and three char code for language '{}'")
                            .withMessageArgs(alternative).build();
                }

                String fallbackCode = twoCharCodeAltLanguage;
                String fallbackLabel = labelsPerLanguage.get(fallbackCode);
                if (StringUtils.isBlank(fallbackLabel)) {
                    fallbackCode = threeCharCodeAltLanguage;
                    fallbackLabel = labelsPerLanguage.get(fallbackCode);
                }

                if (!StringUtils.isBlank(fallbackLabel)) {
                    fallBackNodes.add(new FallBackNode(fallbackLabel, fallbackCode, !StringUtils.isBlank(twoCharCodeRequiredLanguage)
                            ? twoCharCodeRequiredLanguage : threeCharCodeRequiredLanguage));

                    found = true;
                    break;
                }
            }

            if (!found) {
                MapUtils.addValueToMappedSet(missingLanguagesPerResource, resource, requiredLanguageBean.getUri());
            }
        }

        return new Pair<>(Collections.unmodifiableList(fallBackNodes), getMissingLanguages(resource));
    }

    /**
     * <p>getFallbackNodesPerResource.</p>
     *
     * @return a {@link java.util.Map} object.
     */
    @Override
    public Map<Resource, List<FallBackNode>> getFallbackNodesPerResource() {
        return Collections.unmodifiableMap(fallBacksPerResource);
    }

    /**
     * <p>Getter for the field <code>missingLanguagesPerResource</code>.</p>
     *
     * @return a {@link java.util.Map} object.
     */
    @Override
    public Map<Resource, Set<String>> getMissingLanguagesPerResource() {
        return Collections.unmodifiableMap(missingLanguagesPerResource);
    }

    /**
     * <p>getMissingLanguages.</p>
     *
     * @param concept a {@link org.apache.jena.rdf.model.Resource} object.
     * @return a {@link java.util.Set} object.
     */
    private Set<String> getMissingLanguages(Resource concept) {
        Set<String> missingLanguages = missingLanguagesPerResource.get(concept);
        return missingLanguages != null ? Collections.unmodifiableSet(missingLanguages) : Collections.<String> emptySet();
    }

    /**
     * <p>listConcepts.</p>
     *
     * @return a {@link java.util.List} object.
     */
    private List<Resource> listConcepts() {
        return model.listResourcesWithProperty(RDF.type, CellarType.skos_ConceptR).toList();
    }

    /**
     * <p>getlabelsPerLanguage.</p>
     *
     * @param concept a {@link org.apache.jena.rdf.model.Resource} object.
     * @return a {@link java.util.Map} object.
     */
    private Map<String, String> getlabelsPerLanguage(Resource concept) {
        List<Literal> literals = JenaQueries.getLiterals(concept, CellarProperty.skos_prefLabelP, false);
        Map<String, String> labelsPerLanguage = new HashMap<String, String>(literals.size());
        for (Literal literal : literals) {
            if (!StringUtils.isBlank(literal.getString())) {
                String lang = StringUtils.trimToEmpty(literal.getLanguage());
                if (!lang.isEmpty())
                    labelsPerLanguage.put(lang, literal.getString());
            }
        }
        return labelsPerLanguage;
    }

}
