/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.service
 *             FILE : DisseminationContentStreamData.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 29, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$$
 *          VERSION : $LastChangedRevision$$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.converter;

import java.util.Set;

/**
 * <p>DisseminationContentStreamData class.</p>
 *
 * @author ARHS Developments
 * @version $Revision$$
 */
public class DisseminationContentStreamData {

    /** The url. */
    private final String url;

    /** The order. */
    private int order = -1;

    /** The doc index. 
     * The order may sometimes not represent a natural order
     *  e.g.: all content streams may have the same order value 
     *  So we store the docIndex to use when displaying the results
     *  */
    private int docIndex = -1;

    /** The first page. */
    private int firstPage = -1;

    /** The last page. */
    private int lastPage = -1;

    /** The stream name. */
    private String streamName;

    /** The related work ids. */
    private Set<String> workIds;

    /** The stream label. */
    private String streamLabel;

    /** The owner id. */
    private String ownerId;

    /** The media type. */
    private String mediaType;

    /**
       * <p>Constructor for DisseminationContentStreamData.</p>
       *
       * @param url a {@link java.lang.String} object.
       */
    public DisseminationContentStreamData(final String url) {
        this.url = url;
    }

    /**
     * <p>Setter for the field <code>firstPage</code>.</p>
     *
     * @param firstPage a int.
     */
    public void setFirstPage(final int firstPage) {
        this.firstPage = firstPage;
    }

    /**
     * <p>Setter for the field <code>lastPage</code>.</p>
     *
     * @param lastPage a int.
     */
    public void setLastPage(final int lastPage) {
        this.lastPage = lastPage;
    }

    /**
     * <p>Setter for the field <code>order</code>.</p>
     *
     * @param order a int.
     */
    public void setOrder(final int order) {
        this.order = order;
    }

    /**
     * <p>Getter for the field <code>firstPage</code>.</p>
     *
     * @return a int.
     */
    public int getFirstPage() {
        return this.firstPage;
    }

    /**
     * <p>Getter for the field <code>lastPage</code>.</p>
     *
     * @return a int.
     */
    public int getLastPage() {
        return this.lastPage;
    }

    /**
     * <p>Getter for the field <code>order</code>.</p>
     *
     * @return a int.
     */
    public int getOrder() {
        return this.order;
    }

    /**
     * <p>Getter for the field <code>url</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getUrl() {
        return this.url;
    }

    /**
     * Gets the stream name.
     *
     * @return the stream name
     */
    public String getStreamName() {
        return this.streamName;
    }

    /**
     * Sets the stream name.
     *
     * @param streamName the new stream name
     */
    public void setStreamName(final String streamName) {
        this.streamName = streamName;
    }

    /**
     * Gets the work ids.
     *
     * @return the work ids
     */
    public Set<String> getWorkIds() {
        return this.workIds;
    }

    /**
     * Sets the work ids.
     *
     * @param workIds the new work ids
     */
    public void setWorkIds(final Set<String> workIds) {
        this.workIds = workIds;
    }

    /**
     * Gets the stream label.
     *
     * @return the stream label
     */
    public String getStreamLabel() {
        return this.streamLabel;
    }

    /**
     * Sets the stream label.
     *
     * @param streamLabel the new stream label
     */
    public void setStreamLabel(final String streamLabel) {
        this.streamLabel = streamLabel;
    }

    /**
     * Gets the owner id.
     *
     * @return the owner id
     */
    public String getOwnerId() {
        return this.ownerId;
    }

    /**
     * Sets the owner id.
     *
     * @param ownerId the new owner id
     */
    public void setOwnerId(final String ownerId) {
        this.ownerId = ownerId;
    }

    /**
     * Gets the doc index.
     *
     * @return the doc index
     */
    public int getDocIndex() {
        return this.docIndex;
    }

    /**
     * Sets the doc index.
     *
     * @param docIndex the new doc index
     */
    public void setDocIndex(final int docIndex) {
        this.docIndex = docIndex;
    }

    /**
     * Gets the media type.
     * 
     * @return the media type
     */
    public String getMediaType() {
        return mediaType;
    }

    /**
     * Sets the media type.
     * 
     * @param mediaType the media type to set
     */
    public void setMediaType(final String mediaType) {
        this.mediaType = mediaType;
    }
}
