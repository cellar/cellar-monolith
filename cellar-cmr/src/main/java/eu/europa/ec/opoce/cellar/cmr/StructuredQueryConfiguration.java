/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : CellarIdentifier.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 3 juin 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;

import eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.Syntax;

/**
 * The Class StructuredQueryConfiguration.
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 3 juin 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
/**
 * <p>StructuredQueryConfiguration class.</p>
 */
public class StructuredQueryConfiguration {

    private static final Logger LOG = LogManager.getLogger(StructuredQueryConfiguration.class);

    /** The queries. */
    private Map<String, Query> queries;

    /**
     * <p>init.</p>
     */
    @PostConstruct
    private void init() {
        final ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("spring_configs/queries.xml");
        context.start();

        final String prefixes = context.getBean("prefixes", String.class);

        queries = new HashMap<>();
        for (final Map.Entry<String, String> entry : context.getBeansOfType(String.class).entrySet()) {
            if (entry.getKey().equals("prefixes")) {
                continue;
            }
            final String queryString = prefixes + entry.getValue();
            queries.put(entry.getKey(), QueryFactory.create(queryString, Syntax.syntaxARQ));
        }
        context.close();
        LOG.info(IConfiguration.CONFIG, "{} queries configured", queries.size());
    }

    /**
     * <p>resolveListConceptUrisQuery.</p>
     *
     * @return a {@link org.apache.jena.query.Query} object.
     */
    public Query resolveListConceptUrisQuery() {
        return resolveQuery("listConceptUris");
    }

    /**
     * <p>resolveConstructConceptUrisQuery.</p>
     *
     * @return a {@link org.apache.jena.query.Query} object.
     */
    public Query resolveConstructConceptUrisQuery() {
        return resolveQuery("constructConceptUris");
    }

    /**
     * <p>resolveRelatedObjectsWithTypeQuery.</p>
     *
     * @return a {@link org.apache.jena.query.Query} object.
     */
    public Query resolveRelatedObjectsWithTypeQuery() {
        return resolveQuery("relatedObjectsWithType");
    }

    /**
     * <p>resolveInverseObjectsWithTypeQuery.</p>
     *
     * @return a {@link org.apache.jena.query.Query} object.
     */
    public Query resolveInverseObjectsWithTypeQuery() {
        return resolveQuery("inverseObjectsWithType2");
    }

    /**
     * <p>resolveEmbargoDateQuery.</p>
     *
     * @return a {@link org.apache.jena.query.Query} object.
     */
    public Query resolveEmbargoDateQuery() {
        return resolveQuery("embargoDate");
    }

    /**
     * Resolve embargo digital object query.
     *
     * @return the query
     */
    public Query resolveEmbargoDigitalObjectQuery() {
        return resolveQuery("embargoDigitalObject");
    }

    /**
     * <p>resolveQuery.</p>
     *
     * @param name a {@link java.lang.String} object.
     * @return a {@link org.apache.jena.query.Query} object.
     */
    private Query resolveQuery(final String name) {
        return this.queries.get(name);
    }

}
