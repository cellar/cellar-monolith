/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.exception
 *        FILE : UncompressException.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 30-01-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.core.exception;

import eu.europa.ec.opoce.cellar.exception.BuildableException;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

/**
 * This is a {@link UncompressException} buildable by an {@link ExceptionBuilder}.</br>
 * It signals that an uncompress exception of some kind has occurred. This class is the general class
 * of exceptions produced by failed or interrupted uncompress operations.</br>
 * </br>
 * ON : 30-01-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public class UncompressException extends CellarException {

    private static final long serialVersionUID = 1L;

    /**
     * Constructs a new exception with its associated builder.
     * 
     * @param builder the builder to use for building the exception
     */
    public UncompressException(final ExceptionBuilder<? extends BuildableException> builder) {
        super(builder);
    }
}
