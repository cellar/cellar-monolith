/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.server.admin.log
 *        FILE : LogServiceImpl.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 30-01-2012
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.admin.log;

import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.admin.log.LogService;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.logging.CellarLogLevelService;
import eu.europa.ec.opoce.cellar.logging.LoggerContext;
import eu.europa.ec.opoce.cellar.semantic.helper.DateFormats;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileInputStream;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * <class_description> Service class to interact with the cmr log files.
 *
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * ON : 02-08-2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class LogServiceImpl implements LogService {

    private static final Logger LOG = LogManager.getLogger(LogServiceImpl.class);

    private static final String CMR_PREFIX = "cmr_";
    private static final String LOG_EXTENSION = ".log";

    @Autowired(required = true)
    private CellarLogLevelService cellarLogLevelService;

    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    private File cellarFolderLog;

    @PostConstruct
    private void checkResources() {
        this.cellarFolderLog = new File(cellarConfiguration.getCellarFolderCmrLog());
    }

    /**
     * This method return a list of strings. Every string is a representation of a log file.
     * The list is ordered by date of the logs.
     *
     * @return list of string's containing log files
     */
    @Override
    public List<String> getLogs() {
        final List<String> wrappingLogs = new ArrayList<String>();
        wrappingLogs.addAll(Arrays.asList(this.cellarFolderLog.list(getLogFilter())));
        final List<String> dateLogs = new ArrayList<String>();
        dateLogs.addAll(Arrays.asList(this.cellarFolderLog.list(getClearLogFilter())));

        Collections.sort(dateLogs, dateComparator());
        dateLogs.addAll(wrappingLogs);
        return dateLogs;
    }

    /**
     * This method returns a list of all the log files from today
     *
     * @return a list of string's containing log files
     */
    @Override
    public List<String> getLogsToday() {
        final List<String> wrappingLogs = new ArrayList<String>();
        wrappingLogs.addAll(Arrays.asList(this.cellarFolderLog.list(getLogFilter())));
        final List<String> todayLogs = new ArrayList<String>();
        todayLogs.addAll(Arrays.asList(this.cellarFolderLog.list(getTodayLogFilter())));

        todayLogs.addAll(wrappingLogs);
        return todayLogs;
    }

    /**
     * This method returns a list of all the log files from yesterday
     *
     * @return a list of string's containing log files
     */
    @Override
    public List<String> getLogsYesterday() {
        final List<String> wrappingLogs = new ArrayList<String>();
        wrappingLogs.addAll(Arrays.asList(this.cellarFolderLog.list(getLogFilter())));
        final List<String> yesterdayLogs = new ArrayList<String>();
        yesterdayLogs.addAll(Arrays.asList(this.cellarFolderLog.list(getYesterdayLogFilter())));

        yesterdayLogs.addAll(wrappingLogs);
        return yesterdayLogs;
    }

    /**
     * This method returns a list of all the log files from this week, ordered by date
     *
     * @return a list of string's containing log files
     */
    @Override
    public List<String> getLogsThisWeek() {
        final List<String> wrappingLogs = new ArrayList<String>();
        wrappingLogs.addAll(Arrays.asList(this.cellarFolderLog.list(getLogFilter())));
        final List<String> thisWeekLogs = new ArrayList<String>();
        thisWeekLogs.addAll(Arrays.asList(this.cellarFolderLog.list(getThisWeekLogFilter())));

        Collections.sort(thisWeekLogs, dateComparator());
        thisWeekLogs.addAll(wrappingLogs);
        return thisWeekLogs;
    }

    /**
     * This method returns a log file
     *
     * @param log: string filename of the logfile
     * @return a string containing the log, or an empty string if the log is not found
     */
    @Override
    public InputStream getLog(String log) {
        InputStream inputStream = null;
        if (StringUtils.isNotBlank(log)) {
            final File file = new File(this.cellarFolderLog, log);
            try {
                inputStream = new FileInputStream(file);
            } catch (final Exception e) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.GIVEN_PATH_IS_NO_FILEPATH)
                        .withMessage("Error while reading file '{}'").withMessageArgs(log).withCause(e).build();
            }
        }
        return inputStream;
    }

    /**
     * <p>clearLogs.</p>
     */
    @Override
    public void clearLogs() {
        LOG.info("Clearing log files");
        final File[] files = this.cellarFolderLog.listFiles(getClearLogFilter());
        final List<String> failed = new ArrayList<String>();
        for (final File file : files) {
            final boolean success = file.delete();
            if (!success) {
                failed.add(file.getAbsolutePath());
            }
        }
        if (!failed.isEmpty()) {
            LOG.warn("Unable to delete file(s) : '{}'", StringUtils.join(failed.toArray(), ", "));
        }
    }

    /**
     * This method removes all the log files prior to the specified date
     *
     * @param date: a string containing the date (yyyy-MM-dd)
     */
    @Override
    public void clearLogs(String date) {
        if (StringUtils.isNotBlank(date)) {
            LOG.info("Clearing log files");
            final File[] files = this.cellarFolderLog.listFiles(getClearLogFilter(date));
            final List<String> failed = new ArrayList<String>();
            for (final File file : files) {
                final boolean success = file.delete();
                if (!success) {
                    failed.add(file.getAbsolutePath());
                }
            }
            if (!failed.isEmpty()) {
                LOG.warn("Unable to delete file(s) : '{}'", StringUtils.join(failed.toArray(), ", "));
            }
        }
    }

    /**
     * This method set's the default log level of the application
     *
     * @param logLevel: a string containing the log level, this string can only be 'debug' or 'info'
     */
    @Override
    public void setLogLevel(String logLevel) {
        if (StringUtils.isNotBlank(logLevel)) {
            LoggerContext.CellarLevel level;
            if (logLevel.equalsIgnoreCase("debug")) {
                level = LoggerContext.CellarLevel.DEBUG;
            } else if (logLevel.equalsIgnoreCase("info")) {
                level = LoggerContext.CellarLevel.INFO;
            } else {
                LOG.warn("Invalid log level specified: '{}'", logLevel);
                return;
            }
            LOG.info("Setting log level to '{}'", logLevel);
            cellarLogLevelService.setLevel(level);
        }
    }

    /**
     * This method returns the string value of the application's log level
     *
     * @return a string containing the log level
     */
    @Override
    public String getLogLevel() {
        return cellarLogLevelService.getLevel().toString();
    }

    /**
     * <p>dateComparator.</p>
     *
     * @return a {@link java.util.Comparator} object.
     */
    private Comparator<String> dateComparator() {
        return new Comparator<String>() {

            @Override
            public int compare(String s1, String s2) {
                final Date date1 = DateFormats.parse(s1.substring(s1.length() - 10), DateFormats.Format.DATE);
                final Date date2 = DateFormats.parse(s2.substring(s2.length() - 10), DateFormats.Format.DATE);
                return ((null != date1) && (null != date2)) ? date2.compareTo(date1) : 0;
            }
        };
    }

    /**
     * <p>getLogFilter.</p>
     *
     * @return a {@link java.io.FilenameFilter} object.
     */
    private FilenameFilter getLogFilter() {
        return new FilenameFilter() {

            @Override
            public boolean accept(File folder, String filename) {
                return StringUtils.startsWithIgnoreCase(filename, CMR_PREFIX) && StringUtils.endsWithIgnoreCase(filename, LOG_EXTENSION);
            }
        };
    }

    /**
     * <p>getClearLogFilter.</p>
     *
     * @return a {@link java.io.FilenameFilter} object.
     */
    private FilenameFilter getClearLogFilter() {
        return new FilenameFilter() {

            @Override
            public boolean accept(File folder, String filename) {
                return !StringUtils.endsWithIgnoreCase(filename, LOG_EXTENSION) && StringUtils.startsWithIgnoreCase(filename, CMR_PREFIX);
            }
        };
    }

    /**
     * <p>getClearLogFilter.</p>
     *
     * @param date a {@link java.lang.String} object.
     * @return a {@link java.io.FilenameFilter} object.
     */
    private FilenameFilter getClearLogFilter(final String date) {
        return new FilenameFilter() {

            @Override
            public boolean accept(File folder, String filename) {
                final Date deleteDate = DateFormats.parse(date, DateFormats.Format.DATE);
                if (!StringUtils.endsWithIgnoreCase(filename, LOG_EXTENSION) && StringUtils.startsWithIgnoreCase(filename, CMR_PREFIX)) {
                    final Date logDate = DateFormats.parse(filename.substring(filename.length() - 10), DateFormats.Format.DATE);
                    return logDate.before(deleteDate);
                }
                return false;
            }
        };
    }

    /**
     * <p>getTodayLogFilter.</p>
     *
     * @return a {@link java.io.FilenameFilter} object.
     */
    private FilenameFilter getTodayLogFilter() {
        return new FilenameFilter() {

            @Override
            public boolean accept(File folder, String filename) {
                return !StringUtils.endsWithIgnoreCase(filename, LOG_EXTENSION) && StringUtils.startsWithIgnoreCase(filename, CMR_PREFIX)
                        && StringUtils.endsWithIgnoreCase(filename, DateFormats.formatDate(Calendar.getInstance().getTime()));
            }
        };
    }

    /**
     * <p>getYesterdayLogFilter.</p>
     *
     * @return a {@link java.io.FilenameFilter} object.
     */
    private FilenameFilter getYesterdayLogFilter() {
        return new FilenameFilter() {

            @Override
            public boolean accept(File folder, String filename) {
                final Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DATE, -1); //yesterday
                return !StringUtils.endsWithIgnoreCase(filename, LOG_EXTENSION) && StringUtils.startsWithIgnoreCase(filename, CMR_PREFIX)
                        && StringUtils.endsWithIgnoreCase(filename, DateFormats.formatDate(calendar.getTime()));
            }
        };
    }

    /**
     * <p>getThisWeekLogFilter.</p>
     *
     * @return a {@link java.io.FilenameFilter} object.
     */
    private FilenameFilter getThisWeekLogFilter() {
        return new FilenameFilter() {

            @Override
            public boolean accept(File folder, String filename) {
                final Calendar calendar = Calendar.getInstance();
                calendar.add(Calendar.DATE, -7);
                if (!StringUtils.endsWithIgnoreCase(filename, LOG_EXTENSION) && StringUtils.startsWithIgnoreCase(filename, CMR_PREFIX)) {
                    final Date date = DateFormats.parse(filename.substring(filename.length() - 10), DateFormats.Format.DATE);
                    return (null != date) && date.after(calendar.getTime());
                }
                return false;
            }
        };
    }
}
