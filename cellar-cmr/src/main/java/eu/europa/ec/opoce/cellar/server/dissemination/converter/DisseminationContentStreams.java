package eu.europa.ec.opoce.cellar.server.dissemination.converter;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>DisseminationContentStreams class.</p>
 */
public class DisseminationContentStreams {

    private final List<DisseminationContentStreamData> disseminationContentStreamDataList;

    /**
     * <p>Constructor for DisseminationContentStreams.</p>
     *
     * @param disseminationContentStreamDataList
     *         a {@link java.util.List} object.
     */
    public DisseminationContentStreams(List<DisseminationContentStreamData> disseminationContentStreamDataList) {
        this.disseminationContentStreamDataList = disseminationContentStreamDataList;
    }

    /**
     * <p>Constructor for DisseminationContentStreams.</p>
     */
    public DisseminationContentStreams() {
        disseminationContentStreamDataList = new ArrayList<DisseminationContentStreamData>();
    }

    /**
     * <p>Getter for the field <code>disseminationContentStreamDataList</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<DisseminationContentStreamData> getDisseminationContentStreamDataList() {
        return disseminationContentStreamDataList;
    }

}
