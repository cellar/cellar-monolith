/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.nal.decoding
 *             FILE : NalSnippetDbGateway.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 03 02, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-03-02 07:35:06 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.nal.service;

import eu.europa.ec.opoce.cellar.nal.domain.NalRelatedBean;
import eu.europa.ec.opoce.cellar.nal.domain.NalSnippetBean;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author ARHS Developments
 */
public interface NalSnippetDbGateway {
    void setBatchSize(int batchSize);

    Map<String, NalSnippetBean> selectSnippets(Collection<String> concepts);

    Map<String, NalSnippetBean> selectSnippetsInclRelated(Collection<String> concepts);

    Set<String> selectExistingConcepts(Collection<String> concepts);

    void insertSnippets(List<NalSnippetBean> nalSnippetDOs);

    void insertRelations(List<NalRelatedBean> relatedSnippetDOs);

    int deleteNalSnippetOfConceptScheme(String conceptSchemeUri);

    int deleteAllNalSnippet();

    int deleteRelatedSnippetOfRelatedResourceUri(String conceptSchemeUri);

    int deleteAllRelatedSnippet();
}
