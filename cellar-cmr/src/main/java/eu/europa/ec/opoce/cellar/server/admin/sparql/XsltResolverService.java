package eu.europa.ec.opoce.cellar.server.admin.sparql;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * <p>XsltResolverService interface.</p>
 */
public interface XsltResolverService {

    /**
     * <p>getQueries.</p>
     *
     * @return an array of {@link eu.europa.ec.opoce.cellar.server.admin.sparql.Query} objects.
     */
    Query[] getQueries();

    /**
     * <p>getQuery.</p>
     *
     * @param query a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     * @throws java.io.IOException if any.
     */
    String getQuery(String query) throws IOException;

    /**
     * <p>getStylesheetOptions.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    String getStylesheetOptions();

    /**
     * <p>getStylesheetsWithMimetype.</p>
     *
     * @return a {@link java.util.Map} object.
     */
    Map<String, String> getStylesheetsWithMimetype();

    /**
     * <p>getDefinitions.</p>
     *
     * @return a {@link java.util.List} object.
     */
    List<String> getDefinitions();

    /**
     * <p>deleteDefintions.</p>
     *
     * @param definitionList a {@link java.util.List} object.
     */
    void deleteDefintions(List<String> definitionList);

    /**
     * <p>deleteStylesheets.</p>
     *
     * @param stylesheetList a {@link java.util.List} object.
     */
    void deleteStylesheets(List<String> stylesheetList);

    /**
     * <p>getStylesheet.</p>
     *
     * @param stylesheet a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    String getStylesheet(String stylesheet);

    /**
     * <p>getDefinition.</p>
     *
     * @param definition a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    String getDefinition(String definition);

    /**
     * <p>saveDefinition.</p>
     *
     * @param uploadItem a {@link eu.europa.ec.opoce.cellar.server.admin.sparql.UploadDefinition} object.
     */
    void saveDefinition(UploadDefinition uploadItem);

    /**
     * <p>saveStylesheet.</p>
     *
     * @param uploadItem a {@link eu.europa.ec.opoce.cellar.server.admin.sparql.UploadXslt} object.
     */
    void saveStylesheet(UploadXslt uploadItem);

    /**
     * <p>saveStylesheet.</p>
     *
     * @param uploadItem a {@link eu.europa.ec.opoce.cellar.server.admin.sparql.UploadXslt} object.
     * @param outputFolder the output folder.
     * 
     * @return true if copy is ok.
     */
    boolean saveStylesheet(UploadXslt uploadItem, File outputFolder);
}
