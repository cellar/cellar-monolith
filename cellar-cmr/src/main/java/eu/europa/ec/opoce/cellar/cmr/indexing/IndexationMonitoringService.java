/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.indexing
 *             FILE : IndexationMonitoringService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 08 03, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-08-03 11:32:07 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.indexing;

import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestBatch;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestsCount;

import java.util.List;

/**
 * @author ARHS Developments
 */
public interface IndexationMonitoringService {

    /**
     * @param cmrIndexRequest the {@link CmrIndexRequest} to register
     */
    void register(CmrIndexRequestBatch cmrIndexRequest);

    /**
     * @param cmrIndexRequest the {@link CmrIndexRequest} to unregister
     */
    void unregister(CmrIndexRequestBatch cmrIndexRequest);

    /**
     * Retrieve the CmrIndexRequest which are currently executed
     * by the indexation service.
     *
     * @param limit the maximum number of results which will be
     *              retrieved (could be less).
     * @return a list of {@link CmrIndexRequestsCount} which contains
     * the priority, the uri and the timestamp.
     */
    List<CmrIndexRequestsCount> getInProgressRequests(int limit);

    /**
     * Retrieve the CmrIndexRequest which are currently waiting in
     * the queue of the the indexation service.
     *
     * @param limit the maximum number of results which will be
     *              retrieved (could be less).
     * @return a list of {@link CmrIndexRequestsCount} which contains
     * the priority, the uri and the timestamp.
     */
    List<CmrIndexRequestsCount> getWaitingGroup(int limit);

    /**
     * Retrieve the CmrIndexRequest which are done. The implementation
     * should consider to limit the number of {@link CmrIndexRequest}
     * kept in memory by using an appropriate eviction mechanism based
     * on the time or the size in memory.
     *
     * @param limit the maximum number of results which will be
     *              retrieved (could be less).
     * @return a list of {@link CmrIndexRequestsCount} which contains
     * the priority, the uri and the timestamp.
     */
    List<CmrIndexRequestsCount> getHistory(int limit);
}
