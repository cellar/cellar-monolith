/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice
 *             FILE : NoticeCache.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 29, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.notice.cache.impl;

import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.model.ModelUtils;
import eu.europa.ec.opoce.cellar.cmr.model.SameAsModelUtils;
import eu.europa.ec.opoce.cellar.cmr.notice.cache.ISameAsCache;
import eu.europa.ec.opoce.cellar.domain.content.mets.CellarIdentifiedObject;
import org.apache.jena.rdf.model.Model;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * <class_description> Cache for sameAs-es.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 29, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public abstract class AbstractSameAsCache implements ISameAsCache {

    private static final Logger LOG = LogManager.getLogger(AbstractSameAsCache.class);

    protected Map<String, Identifier> sameAses;

    public AbstractSameAsCache() {
        this.sameAses = new HashMap<String, Identifier>();
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cmr.notice.cache.ISameAsCache#init(eu.europa.ec.opoce.cellar.domain.content.mets.CellarIdentifiedObject)
     */
    @Override
    public void init(final CellarIdentifiedObject object) {
        if (object != null) {
            final Collection<Identifier> identifiers = this.resolveIdentifierService()
                    .getTreeIdentifiers(object.getCellarId().getIdentifier());
            for (final Identifier identifier : identifiers) {
                this.sameAses.put(identifier.getCellarId(), identifier);
                for (final String psId : identifier.getPids()) {
                    this.sameAses.put(psId, identifier);
                }
            }
        }
    }

    protected abstract IdentifierService resolveIdentifierService();

    /**
     * @see eu.europa.ec.opoce.cellar.cmr.notice.cache.ISameAsCache#extractSameAses(org.apache.jena.rdf.model.Model)
     */
    @Override
    public Model extractSameAses(final Model inputModel) {
        final Collection<String> uris = ModelUtils.getResourceUris(inputModel);
        final Collection<Identifier> identifiers = this.getIdentifiers(uris);
        return SameAsModelUtils.getSameAsModel(identifiers);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cmr.notice.cache.ISameAsCache#getIdentifier(java.lang.String)
     */
    @Override
    public Identifier getIdentifier(final String uri) {
        final String id = this.resolveIdentifierService().getPrefixed(uri);
        if (this.sameAses.containsKey(id)) {
            return this.sameAses.get(id);
        }
        try {
            final Identifier identifier = this.resolveIdentifierService().getIdentifier(id);
            this.sameAses.put(identifier.getCellarId(), identifier);
            for (final String psId : identifier.getPids()) {
                this.sameAses.put(psId, identifier);
            }
            return identifier;
        } catch (final IllegalArgumentException e) {
            LOG.warn("the identifier {} is not specified yet", id);
            return null;
        }
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cmr.notice.cache.ISameAsCache#getIdentifiers(java.util.Collection)
     */
    @Override
    public Collection<Identifier> getIdentifiers(final Collection<String> uris) {
        final Set<Identifier> identifiers = new HashSet<Identifier>();
        final Collection<String> idsNotInCache = new ArrayList<String>();

        final IdentifierService resolveIdentifierService = this.resolveIdentifierService();
        for (final String uri : uris) {
            final String id = resolveIdentifierService.getPrefixed(uri);
            if (this.sameAses.containsKey(id)) {
                final Identifier e = this.sameAses.get(id);
                identifiers.add(e);
            } else {
                idsNotInCache.add(id);
            }
        }

        if (!idsNotInCache.isEmpty()) {
            Collection<Identifier> identifiersNotInCache = Collections.<Identifier> emptyList();
            try {
                identifiersNotInCache = resolveIdentifierService.getIdentifiers(idsNotInCache);
            } catch (final IllegalArgumentException e) {
                // nothing to log here
            }
            identifiers.addAll(identifiersNotInCache);
            for (final Identifier identifierNotInCache : identifiersNotInCache) {
                this.sameAses.put(identifierNotInCache.getCellarId(), identifierNotInCache);
                for (final String psId : identifierNotInCache.getPids()) {
                    this.sameAses.put(psId, identifierNotInCache);
                }
            }
        }

        return identifiers;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cmr.notice.cache.ISameAsCache#getSameAses()
     */
    @Override
    public Map<String, Identifier> getSameAses() {
        return this.sameAses;
    }

}
