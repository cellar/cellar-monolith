package eu.europa.ec.opoce.cellar.sparql;

import eu.europa.ec.opoce.cellar.ExecutionStatus;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * @author ARHS Developments
 */
@Entity
@Table(name = "SPARQL_LOAD_REQUEST")
public class SparqlLoadRequest implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "sparql_load_gen")
    @SequenceGenerator(name = "sparql_load_gen", sequenceName = "SPARQL_LOAD_SEQ", allocationSize = 1)
    private Long id;

    @Column(name = "URI")
    private String uri;

    @Column(name = "EXECUTION_STATUS")
    @Enumerated(EnumType.STRING)
    private ExecutionStatus executionStatus;

    @Column(name = "ACTIVATION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date activationDate;

    @Column(name = "EXECUTION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date executionDate;

    @Column(name = "PID")
    private String externalPid;

    @Column(name = "version")
    private String version;

    @Column(name="CELLAR_ID")
    private String cellarId;

    public SparqlLoadRequest() {
    }

    public SparqlLoadRequest(String uri, Date activationDate) {
        this.uri = uri;
        this.activationDate = activationDate;
        this.executionStatus = ExecutionStatus.Pending;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public ExecutionStatus getExecutionStatus() {
        return executionStatus;
    }

    public void setExecutionStatus(ExecutionStatus executionStatus) {
        this.executionStatus = executionStatus;
    }

    public Date getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(Date activationDate) {
        this.activationDate = activationDate;
    }

    public Date getExecutionDate() {
        return executionDate;
    }

    public void setExecutionDate(Date executionDate) {
        this.executionDate = executionDate;
    }

    public String getExternalPid() {
        return externalPid;
    }

    public void setExternalPid(String externalPid) {
        this.externalPid = externalPid;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getCellarId() {
        return cellarId;
    }

    public void setCellarId(String cellarId) {
        this.cellarId = cellarId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SparqlLoadRequest)) return false;
        SparqlLoadRequest that = (SparqlLoadRequest) o;
        return id.equals(that.id) && uri.equals(that.uri) && executionStatus == that.executionStatus && activationDate.equals(that.activationDate) && executionDate.equals(that.executionDate) && externalPid.equals(that.externalPid) && version.equals(that.version) && cellarId.equals(that.cellarId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, uri, executionStatus, activationDate, executionDate, externalPid, version, cellarId);
    }

    @Override
    public String toString() {
        return "SparqlLoadRequest{" +
                "id=" + id +
                ", uri='" + uri + '\'' +
                ", executionStatus=" + executionStatus +
                ", activationDate=" + activationDate +
                ", executionDate=" + executionDate +
                ", externalPid='" + externalPid + '\'' +
                ", version='" + version + '\'' +
                ", cellarId='" + cellarId + '\'' +
                '}';
    }
}
