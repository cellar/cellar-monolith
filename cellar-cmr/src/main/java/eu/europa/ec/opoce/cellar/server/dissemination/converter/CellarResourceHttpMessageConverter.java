/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.service
 *             FILE : ExpressionsHttpMessageConverter.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 17-Nov-2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2016 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.converter;

import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.converter.HttpMessageNotWritableException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * The Class CellarResourceHttpMessageConverter.
 * <class_description> This class object iterates through the wrapper class CellarResourceListHttpMessage 
 * and displays the CellarResourceListHttpMessageElement items as specified .
 * <br/><br/>
 * <notes>.
 * <br/><br/>
 * ON : 17-Nov-2016
 *
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class CellarResourceHttpMessageConverter extends CellarAbstractHttpMessageConverter<CellarResourceListHttpMessage> {


    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean supports(final Class<?> clazz) {
        return CellarResourceListHttpMessage.class.isAssignableFrom(clazz);
    }

    /**
    
     <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
    <title>300 Multiple-Choice Response</title>
    </head>
    <body>
    <p>List of expressions:</p>
    <ul>
    <li class="expression">
    cellar:cc04ed40-2651-11e6-86d0-01aa75ed71a1.0010
    <ul>
    <li>
    <a href="http://publications.europa.eu/resource/cellar/cc04ed40-2651-11e6-86d0-01aa75ed71a1.0010">
    <span class="url">(http://publications.europa.eu/resource/cellar/cc04ed40-2651-11e6-86d0-01aa75ed71a1.0010)</span>
    <span class="language">http://publications.europa.eu/resource/authority/language/ENG</span>
    </a>
    </li>
    </ul>
    </li>
    <li class="expression">
    cellar:cc04ed40-2651-11e6-86d0-01aa75ed71a1.0011
    <ul>
    <li>
    <a href="http://publications.europa.eu/resource/cellar/cc04ed40-2651-11e6-86d0-01aa75ed71a1.0011">
    <span class="url">(http://publications.europa.eu/resource/cellar/cc04ed40-2651-11e6-86d0-01aa75ed71a1.0011)</span>
    <span class="language">http://publications.europa.eu/resource/authority/language/ENG</span>
    <span class="language">http://publications.europa.eu/resource/authority/language/GLE</span>
    </a>
    </li>
    </ul>
    </li>
    </ul>
    </body>
    </html>
    
     * {@inheritDoc}
     */
    @Override
    protected void writeInternal(final CellarResourceListHttpMessage expressionListHttpMessage, final HttpOutputMessage outputMessage)
            throws IOException, HttpMessageNotWritableException {
        final StringBuilder sb = new StringBuilder();
        sb.append("<html><head><title>300 Multiple-Choice Response</title></head><body><p>List of expressions:</p><ul>");
        expressionListHttpMessage.getExpressions().stream().forEach(expression -> {
            sb.append("<li class=\"expression\">").append(expression.getCellarId());
            sb.append("<ul><li>");
            sb.append("<a href=\"").append(expression.getUri()).append("\">");
            sb.append("<span class=\"url\">(" + expression.getUri() + ")</span>");
            expression.getUsedLanguages().forEach(language -> {
                sb.append("<span class=\"language\">(" + language + ")</span>");
            });
            sb.append("</a></li></ul></li>");
        });
        sb.append("</ul></body></html>");
        IOUtils.write(sb.toString().getBytes(StandardCharsets.UTF_8), outputMessage.getBody());
    }
}
