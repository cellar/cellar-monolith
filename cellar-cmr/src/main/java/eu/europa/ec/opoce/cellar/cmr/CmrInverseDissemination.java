package eu.europa.ec.opoce.cellar.cmr;

import eu.europa.ec.opoce.cellar.cmr.database.DaoObject;

import org.apache.jena.rdf.model.Model;

/**
 * <p>CmrInverseDissemination class.</p>
 */
public class CmrInverseDissemination extends DaoObject {

    private String context;
    private Model inverseModel;
    private Model sameAsModel;
    private boolean isBacklog;

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public Model getInverseModel() {
        return inverseModel;
    }

    public void setInverseModel(Model inverseModel) {
        this.inverseModel = inverseModel;
    }

    public Model getSameAsModel() {
        return sameAsModel;
    }

    public void setSameAsModel(Model sameAsModel) {
        this.sameAsModel = sameAsModel;
    }

    public boolean isBacklog() {
        return isBacklog;
    }

    public void setBacklog(boolean isBacklog) {
        this.isBacklog = isBacklog;
    }

}
