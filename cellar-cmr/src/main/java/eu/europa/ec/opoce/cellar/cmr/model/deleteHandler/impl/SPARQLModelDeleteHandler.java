/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cmr.model.deleteHandler
 *        FILE : IModelDeleteHandler.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 14-04-2014
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.model.deleteHandler.impl;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.cmr.model.ModelUtils;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import org.springframework.stereotype.Service;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.sparql.modify.request.UpdateModify;
import org.apache.jena.update.Update;
import org.apache.jena.update.UpdateAction;
import org.apache.jena.update.UpdateFactory;
import org.apache.jena.update.UpdateRequest;

import static eu.europa.ec.opoce.cellar.AppConstants.NEWLINE;

/**
 * <class_description> Class for deleting from a model the triples defined by
 * the given SPARQL delete query. <br/>
 * <br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc. <br/>
 * <br/>
 * ON : 14-04-2014
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
@Service("sparqlModelDeleteHandler")
public class SPARQLModelDeleteHandler extends AbstractModelDeleteHandler<String> {

    /**
     * This method removes from the given {@link model} the triples defined by
     * the given {@link deleteSparqlQuery}.
     * 
     * @param inputModel
     *            the model from which to delete data
     * @param deleteSparqlQuery
     *            the string representing the SPARQL delete query
     * @param preserveInputModel
     *            if <code>true</code>, the {@link inputModel} is kept
     *            unmodified, and only the number of deleted entries will be
     *            returned
     * @return the number of triples deleted
     */
    @Override
    public long delete(final Model inputModel, final String deleteSparqlQuery, final boolean preserveInputModel) {
        long deleted = 0;

        final String myDeleteSparqlQuery = deleteSparqlQuery;
        if (myDeleteSparqlQuery != null) {
            // build the delete request and check that it is valid
            final UpdateRequest deleteRequest = buildDeleteRequest(myDeleteSparqlQuery);

            // executes the query on the model
            final Model myInputModel = preserveInputModel ? ModelUtils.create(inputModel) : inputModel;
            final Model modelBackup_1 = ModelUtils.create(myInputModel);
            final Model modelBackup_2 = ModelUtils.create(myInputModel);
            try {
                //a blank node to be eligible for deletion needs to match each deleted statement with 3 statements  
                //the SPARQL query can affect blank nodes and delete one of these 3 statements
                //In this case the blank is no longer eligible (with the current algorithm) 
                //for this reason we need to execute the deletion on a backup model
                // retrieve the difference and use it to delete the blank nodes on the original model
                UpdateAction.execute(deleteRequest, modelBackup_1);
                final Model difference = modelBackup_2.difference(modelBackup_1);
                this.handleBlankNodes(myInputModel, difference);

                UpdateAction.execute(deleteRequest, myInputModel);
                deleted = ModelUtils.statementsDifference(modelBackup_2, myInputModel);
            } catch (final Exception e) {
                throw ExceptionBuilder.get(CellarException.class).withMessage("An error occurred while executing the SPARQL query '{}'")
                        .withMessageArgs(myDeleteSparqlQuery).withCause(e).build();
            } finally {
                ModelUtils.closeQuietly(modelBackup_1);
                ModelUtils.closeQuietly(modelBackup_2);
                if (preserveInputModel) {
                    ModelUtils.closeQuietly(myInputModel);
                }
            }
        }

        return deleted;
    }

    private static UpdateRequest buildDeleteRequest(final String deleteSparqlQueryStr) {
        UpdateRequest deleteRequest = null;

        // decode the query
        String myDeleteSparqlQueryStr = deleteSparqlQueryStr;
        try {
            myDeleteSparqlQueryStr = URLDecoder.decode(myDeleteSparqlQueryStr, "UTF-8");
        } catch (final UnsupportedEncodingException exc) {
            throw ExceptionBuilder.get(CellarException.class) //
                    .withCode(CommonErrors.ERROR_WHILE_PARSING_THE_METS_DOCUMENT) //        
                    .withMessage("An error occurred while deconding the SPARQL query '{}'") //
                    .withMessageArgs(myDeleteSparqlQueryStr) //
                    .withCause(exc) //
                    .root(true) //
                    .build();
        }

        // create the delete request as an update request, and check it is valid
        try {
            deleteRequest = UpdateFactory.create(myDeleteSparqlQueryStr);
        } catch (final Exception exc) {
            throw ExceptionBuilder.get(CellarException.class) //
                    .withCode(CommonErrors.ERROR_WHILE_PARSING_THE_METS_DOCUMENT) //
                    .withMessage("The SPARQL query '{}' is not a valid delete query") //
                    .withMessageArgs(myDeleteSparqlQueryStr.replaceAll("\n", NEWLINE)) //
                    .withCause(exc) //
                    .root(true) //
                    .build();
        }

        // check it is a valid delete request with a where clause
        for (final Update delete : deleteRequest.getOperations()) {
            if (delete instanceof UpdateModify) {
                final UpdateModify deleteUM = (UpdateModify) delete;
                if (deleteUM.hasDeleteClause() && !deleteUM.hasInsertClause() && (deleteUM.getWherePattern() != null)) {
                    continue;
                }
            }
            throw ExceptionBuilder.get(CellarException.class) //
                    .withCode(CommonErrors.ERROR_WHILE_PARSING_THE_METS_DOCUMENT) //        
                    .withMessage("The SPARQL query '{}' is not a valid delete query with a where clause") //
                    .withMessageArgs(myDeleteSparqlQueryStr.replaceAll("\n", NEWLINE)) //
                    .build();
        }

        return deleteRequest;
    }
}
