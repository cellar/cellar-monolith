/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.quad
 *             FILE : ModelQuadServiceFactory.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Sep 6, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.quad;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cmr.quad.impl.NoRetryModelQuadService;
import eu.europa.ec.opoce.cellar.cmr.quad.impl.OracleExponentialBackoffRetryModelQuadService;
import eu.europa.ec.opoce.cellar.cmr.quad.impl.OracleExponentialRetryModelQuadService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <class_description> Factory class that chooses between implementations of {@link eu.europa.ec.opoce.cellar.cmr.quad.ModelQuadService}
 * on the basis of configuration property {@code cellar.service.ingestion.cmr.lock.mode}.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Sep 6, 2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
@Configuration
public class ModelQuadServiceFactory {

    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    @Bean(name = "modelQuadService")
    public ModelQuadService create() {
        ModelQuadService modelQuadService = null;

        switch (this.cellarConfiguration.getCellarServiceIngestionCmrLockMode()) {
        case oracleNoRetry:
            modelQuadService = new NoRetryModelQuadService();
            break;
        case oracleExponentialRetry:
            modelQuadService = new OracleExponentialRetryModelQuadService();
            break;
        case oracleExponentialBackoffRetry:
        default:
            modelQuadService = new OracleExponentialBackoffRetryModelQuadService();
        }

        return modelQuadService;
    }
}
