package eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr;

import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService.Priority;
import org.springframework.util.Assert;

import java.io.Serializable;
import java.util.Date;

/**
 * The Class CmrIndexRequestsCount.
 * This bean will be used to aggregate information for the indexation dashboard tables
 */
public class CmrIndexRequestsCount implements Serializable {

    private static final long serialVersionUID = 6496530214623534165L;

    private Priority priority;

    private String groupURI;

    private Date createdAt;

    private Date doneAt;

    public CmrIndexRequestsCount() {
    }

    /**
     * Gets the priority.
     *
     * @return the priority
     */
    public Priority getPriority() {
        return this.priority;
    }

    /**
     * Sets the priority.
     *
     * @param priority the new priority
     */
    public void setPriority(final int priority) {
        final Priority byPriorityValue = Priority.findByPriorityValue(priority);
        Assert.isTrue(byPriorityValue != null);
        this.priority = byPriorityValue;
    }

    /**
     * Gets the group uri.
     *
     * @return the group uri
     */
    public String getGroupURI() {
        return this.groupURI;
    }

    /**
     * Sets the group uri.
     *
     * @param groupURI the new group uri
     */
    public void setGroupURI(final String groupURI) {
        this.groupURI = groupURI;
    }

    public Date getDoneAt() {
        return doneAt;
    }

    public void setDoneAt(Date doneAt) {
        this.doneAt = doneAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

}
