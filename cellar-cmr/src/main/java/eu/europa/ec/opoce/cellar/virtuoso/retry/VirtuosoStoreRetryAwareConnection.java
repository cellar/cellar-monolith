/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.virtuoso.retry
 *             FILE : VirtuosoStoreRetryAwareConnection.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 20-07-2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.virtuoso.retry;

import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.common.closure.PClosure;
import eu.europa.ec.opoce.cellar.common.retry.IRetryAwareConnection;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

/**
 * <class_description> An implementation of {@link eu.europa.ec.opoce.cellar.common.retry.IRetryAwareConnection} which handles the logic
 * for the retry-aware accesses to the Virtuoso store.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 06-03-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class VirtuosoStoreRetryAwareConnection implements IRetryAwareConnection<Void> {

    // the possible underlying messages that could denote a deadlock situation
    private static String[] RETRIABLE_MESSAGES = new String[]{ //
            "SR172: Transaction deadlocked" //
    };

    /**
     * @see eu.europa.ec.opoce.cellar.common.retry.IRetryAwareConnection#connect(PClosure)
     */
    @Override
    public <C extends PClosure<Void>> void connect(final C connectionCode) throws CellarException {
        try {
            // execute connection
            connectionCode.call(null);
        } catch (Exception exception) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.VIRTUOSO_OPERATION_FAILED).withCause(exception)
                    .withMessage("Inserting the triples in the Virtuoso Store has failed.").build();
        }
    }

    /**
     * @see eu.europa.ec.opoce.cellar.common.retry.IRetryAwareConnection#rollback()
     */
    @Override
    public void rollback() throws CellarException {
    }

    /**
     * @see eu.europa.ec.opoce.cellar.common.retry.IRetryAwareConnection#resolveRetriableMessages()
     */
    @Override
    public String[] resolveRetriableMessages() {
        return RETRIABLE_MESSAGES;
    }

}
