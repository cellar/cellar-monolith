/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice
 *             FILE : NoticeBuilder.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 29, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.notice;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.enums.CellarConfigurationPropertyKey;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.notice.core.NoticeType;
import eu.europa.ec.opoce.cellar.cmr.notice.core.Rdf2Xml;
import eu.europa.ec.opoce.cellar.cmr.notice.core.Rdf2XmlExecutor;
import eu.europa.ec.opoce.cellar.cmr.notice.dissemination.DisseminationNoticeServiceImpl;
import eu.europa.ec.opoce.cellar.common.util.Pair;
import eu.europa.ec.opoce.cellar.domain.content.mets.CellarIdentifiedObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import eu.europa.ec.opoce.cellar.semantic.xml.XmlBuilder;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;
import org.w3c.dom.Document;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * <class_description> Builder for notices.<br/>
 * This class is friendly as it is only used by in-package class {@link DisseminationNoticeServiceImpl}.
 * It could have been created as inner class of {@link DisseminationNoticeServiceImpl}, but for clarity purposes it has been extracted here.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 29, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Configurable
public class NoticeBuilder {

    /**
     * The Constant NOTICE.
     */
    public static final String NOTICE = "NOTICE";
    /**
     * The Constant OBJECT.
     */
    public static final String OBJECT = "OBJECT";
    /**
     * The Constant Type.
     */
    public static final String TYPE = "type";

    private static final Logger LOG = LogManager.getLogger(NoticeCache.class);
    /**
     * The Constant DECODING.
     */
    private static final String DECODING = "decoding";

    /**
     * The Constant INVERSE_SECTION.
     */
    private static final String INVERSE_SECTION = "INVERSE";

    /**
     * The Constant MANIFESTATION_TYPE.
     */
    private static final String MANIFESTATION_TYPE = "manifestation-type";
    /**
     * The xml builder map.
     */
    private final HashMap<LanguageBean, XmlBuilder> xmlBuilderMap = new HashMap<>();
    /**
     * The noticeType.
     */
    private final NoticeType noticeType;
    /**
     * The cellar resource dao.
     */
    @Autowired
    private CellarResourceDao cellarResourceDao;
    /**
     * The cellar configuration.
     */
    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;
    /**
     * The cellarIdentifiedObject.
     */
    private CellarIdentifiedObject cellarIdentifiedObject;

    /**
     * The cache.
     */
    private NoticeCache cache;

    /**
     * The content languages.
     */
    private Set<LanguageBean> contentLanguages;

    /**
     * The languages.
     */
    private Set<LanguageBean> languages;

    /**
     * The decoding model.
     */
    private Model decodingModel;

    /**
     * The language dependent model map.
     */
    private HashMap<LanguageBean, Model> languageDependentModelMap = new HashMap<>();

    /**
     * The mappings from URI to Cellar identifier.
     */
    private Map<String, String> uriToCellarIdentifierMappings = null;
    
    /**
     * The language independent model.
     */
    private Model languageIndependentModel;

    /**
     * The filter.
     */
    private boolean filter;

    /**
     * The sort.
     */
    private boolean sort;

    /**
     * The close cache.
     */
    private boolean closeCache;

    /**
     * Instantiates a new notice builder.
     *
     * @param noticeType the noticeType
     */
    public NoticeBuilder(final NoticeType noticeType) {
        this.noticeType = noticeType;
        this.filter = true;
    }

    /**
     * With cellarIdentifiedObject.
     *
     * @param object the cellarIdentifiedObject
     * @return the notice builder
     */
    public NoticeBuilder withObject(final CellarIdentifiedObject object) {
        this.cellarIdentifiedObject = object;
        return this;
    }

    /**
     * With cache.
     *
     * @param cache the cache
     * @return the notice builder
     */
    public NoticeBuilder withCache(final NoticeCache cache) {
        this.cache = cache;
        return this;
    }

    /**
     * With content languages.
     *
     * @param contentLanguages the content languages
     * @return the notice builder
     */
    public NoticeBuilder withContentLanguages(final Set<LanguageBean> contentLanguages) {
        this.contentLanguages = contentLanguages;
        return this;
    }

    /**
     * With languages.
     *
     * @param languages the languages
     * @return the notice builder
     */
    public NoticeBuilder withLanguages(final Set<LanguageBean> languages) {
        this.languages = languages;
        return this;
    }

    /**
     * With decoding model.
     *
     * @param decodingModel the decoding model
     * @return the notice builder
     */
    public NoticeBuilder withDecodingModel(final Model decodingModel) {
        this.decodingModel = decodingModel;
        return this;
    }

    /**
     * With language dependent model.
     *
     * @param languageDependentModelMap the language dependent model map
     * @return the notice builder
     */
    public NoticeBuilder withLanguageDependentModel(final HashMap<LanguageBean, Model> languageDependentModelMap) {
        this.languageDependentModelMap = languageDependentModelMap;
        return this;
    }

    /**
     * With language independent model.
     *
     * @param languageIndependentModel the language independent model
     * @return the notice builder
     */
    public NoticeBuilder withLanguageIndependentModel(final Model languageIndependentModel) {
        this.languageIndependentModel = languageIndependentModel;
        return this;
    }

    /**
     * With URI to Cellar identifier mappings.
     * @param isUseCaching {@code true} to use URI to Cellar identifier caching.
     * {@code false} otherwise.
     * @return the notice builder.
     */
    public NoticeBuilder withUseUriToCellarIdentifierCaching(final boolean isUseCaching) {
    	this.uriToCellarIdentifierMappings = isUseCaching ? new HashMap<>() : null;
    	return this;
    }
    
    /**
     * With filter.
     *
     * @param filter the filter
     * @return the notice builder
     */
    public NoticeBuilder withFilter(final boolean filter) {
        this.filter = filter;
        return this;
    }

    /**
     * With sort.
     *
     * @param sort the sort
     * @return the notice builder
     */
    public NoticeBuilder withSort(final boolean sort) {
        this.sort = sort;
        return this;
    }
    
    /**
     * With close cache.
     *
     * @param closeCache the close cache
     * @return the notice builder
     */
    public NoticeBuilder withCloseCache(final boolean closeCache) {
        this.closeCache = closeCache;
        return this;
    }

    /**
     * Gets the document.
     *
     * @param languageBean the language bean
     * @return the document
     */
    public Document getDocument(final LanguageBean languageBean) {
        final XmlBuilder xmlBuilder = this.xmlBuilderMap.get(languageBean);
        final Document document = xmlBuilder.getDocument();
        if (this.sort) {
            eu.europa.ec.opoce.cellar.common.util.XMLUtils.sortChildNodes(document, false, -1, null);
        }
        return document;
    }

    /**
     * Builds the direct.
     *
     * @param languageBean the language bean
     * @param decoding     the decoding
     * @return the notice builder
     */
    public NoticeBuilder buildDirect(final LanguageBean languageBean, final LanguageBean decoding) {
        // fallbacks mandatory properties
        final LanguageBean inDecoding = decoding != null ? decoding : languageBean;
        XmlBuilder xmlBuilder = this.fallbackDirect(languageBean, inDecoding);

        // fills direct notice part
        final DigitalObjectType digitalObjectType = this.cellarIdentifiedObject.getType();
        final String label = digitalObjectType.getLabel();
        final XmlBuilder directNoticeBuilder = xmlBuilder.child(label);
        this.addMimeTypeAttribute(directNoticeBuilder);

        final Model languageDependentModel = this.languageDependentModelMap.get(languageBean);
        final Model model = JenaUtils.create(this.languageIndependentModel, languageDependentModel);

        //Add the creationDate when the date doesn't exist in the backlog
        this.addCreationDate(model);

        try {

            final Model directAndInverse = ModelFactory.createDefaultModel();
            directAndInverse.add(model);
            if (isInverseEnabled()) {
                final Pair<Model, Boolean> inverseResult = this.cache.getInverse(this.cellarIdentifiedObject);
                final Model inverseModel = inverseResult.getOne();
                directAndInverse.add(inverseModel);
            }

            final ContentIdentifier cellarId = this.cellarIdentifiedObject.getCellarId();
            final String uri = cellarId.getUri();
            final Rdf2Xml rdf2Xml = new Rdf2Xml(directAndInverse, uri, directNoticeBuilder, this.contentLanguages, this.languages,
                    this.filter, this.noticeType, this.uriToCellarIdentifierMappings, true);
            final Rdf2XmlExecutor rdf2XmlExecutor = new Rdf2XmlExecutor(rdf2Xml);
            rdf2XmlExecutor.execute();
        } finally {
            JenaUtils.closeQuietly(model);
        }

        return this;
    }

    /**
     * Builds the direct.
     *
     * @param languageBean the language bean
     * @return the notice builder
     */
    public NoticeBuilder buildDirect(final LanguageBean languageBean) {
        return this.buildDirect(languageBean, null);
    }

    /**
     * Builds the inverse.
     *
     * @param languageBean the language bean
     * @param decoding     the decoding
     * @return the notice builder
     */
    public NoticeBuilder buildInverse(final LanguageBean languageBean, final LanguageBean decoding) {
        final XmlBuilder xmlBuilder = this.xmlBuilderMap.get(languageBean);
        // fallbacks mandatory properties
        final LanguageBean inDecoding = decoding != null ? decoding : languageBean;
        this.fallbackInverse(languageBean, inDecoding);

        // checks if the inverse mode is enabled for this kind of object
        if (!this.isInverseEnabled()) {
            return this;
        }
        // checks if the inverse node is supposed to be removed
        if (cellarConfiguration.isCellarServiceInverseNoticesRemoveNodeEnabled()) {
            return this;
        }

        // fills inverse notice part
        final XmlBuilder inverseNoticeBuilder = xmlBuilder.child(INVERSE_SECTION);
        this.addMimeTypeAttribute(inverseNoticeBuilder);

        final Pair<Model, Boolean> inverseResult = this.cache.getInverse(this.cellarIdentifiedObject);
        final Model model = inverseResult.getOne();
        final Boolean truncated = inverseResult.getTwo();
        inverseNoticeBuilder.attribute("complete", "" + !truncated);

        final ContentIdentifier cellarId = this.cellarIdentifiedObject.getCellarId();
        final String uri = cellarId.getUri();
        final Rdf2Xml rdf2Xml = new Rdf2Xml(model, uri, inverseNoticeBuilder, this.contentLanguages, this.languages, this.filter, this.noticeType,
        		this.uriToCellarIdentifierMappings, false);
        final Rdf2XmlExecutor rdf2XmlExecutor = new Rdf2XmlExecutor(rdf2Xml);
        rdf2XmlExecutor.execute();

        return this;
    }

    /**
     * Builds the inverse.
     *
     * @param languageBean the language bean
     * @return the notice builder
     */
    public NoticeBuilder buildInverse(final LanguageBean languageBean) {
        return this.buildInverse(languageBean, null);
    }

    /**
     * Builds the embedded.
     *
     * @return the notice builder
     */
    public NoticeBuilder buildEmbedded() {
        final XmlBuilder xmlBuilder = this.fallbackEmbedded();

        // fills embedded notice part
        this.addMimeTypeAttribute(xmlBuilder);
        final Model languageDependentModel = this.languageDependentModelMap.get(null);
        final Model model = JenaUtils.create(this.languageIndependentModel, languageDependentModel);

        //Add the creationDate when the date doesn't exist in the backlog
        this.addCreationDate(model);

        try {
            final Rdf2Xml rdf2Xml = new Rdf2Xml(model, this.cellarIdentifiedObject.getCellarId().getUri(), xmlBuilder, null, null, false, this.noticeType,
            		null, false);
            final Rdf2XmlExecutor rdf2XmlExecutor = new Rdf2XmlExecutor(rdf2Xml);
            rdf2XmlExecutor.execute();

        } finally {
            JenaUtils.closeQuietly(model);
        }

        return this;
    }

    /**
     * Close.
     */
    public void close() {
        if (this.closeCache) {
            this.cache.closeQuietly();
        }
    }

    /**
     * Add a default creation date (1001-01-01) in the notice if the creation date doesn't exist.
     *
     * @param model the model
     */
    private void addCreationDate(final Model model) {
        final Resource cellarResource = model.getResource(this.cellarIdentifiedObject.getCellarId().getUri());
        if (!cellarResource.hasProperty(CellarProperty.cmr_creationdateP)) {
            final String defaultDate = "1001-01-01T00:00:00.000+00:00";
            model.add(cellarResource, CellarProperty.cmr_creationdateP, model.createTypedLiteral(defaultDate, XSDDatatype.XSDdateTime));
        }
    }

    /**
     * Fallback.
     */
    private void fallback() {
        if (this.noticeType == null) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.INPUT_MUST_BE_DEFINED)
                    .withMessage("Property 'noticeType' cannot be null.").build();
        }
        if (this.cellarIdentifiedObject == null) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.INPUT_MUST_BE_DEFINED)
                    .withMessage("Property 'cellarIdentifiedObject' cannot be null.").build();
        }

        if (this.cache == null) {
            this.cache = new NoticeCache(this.cellarIdentifiedObject);
            // considering that the cache has been created by the builder itself, it is its responsibility to close it!
            this.closeCache = true;
        }
    }

    /**
     * Fallback direct.
     *
     * @param languageBean the language bean
     * @param decoding     the decoding
     * @return the xml builder
     */
    private XmlBuilder fallbackDirect(final LanguageBean languageBean, final LanguageBean decoding) {
        XmlBuilder xmlBuilder = this.xmlBuilderMap.get(languageBean);
        this.fallback();
        final boolean test = languageBean == null;
        if (test) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.INPUT_MUST_BE_DEFINED)
                    .withMessage("Property 'language' cannot be null.").build();
        }

        if (xmlBuilder == null) {
            xmlBuilder = new XmlBuilder(NOTICE).attribute(TYPE, this.noticeType.toString());
            xmlBuilder.attribute(DECODING, decoding.getIsoCodeThreeChar());
            this.xmlBuilderMap.put(languageBean, xmlBuilder);
        }
        final ContentIdentifier cellarId = this.cellarIdentifiedObject.getCellarId();
        this.languageIndependentModel = this.cache.getLanguageIndependent(cellarId, cellarIdentifiedObject.getVersions());
        //at this point maybe retrieving the content-language map could be enough
        final Model contentLanguageDependentModel = this.cache.getLanguageDependent(cellarId, cellarIdentifiedObject.getVersions(), languageBean);
        final Model decodingLanguageDependentModel = this.cache.getLanguageDependent(cellarId, cellarIdentifiedObject.getVersions(), decoding);
        final Model languageDependentModel = ModelFactory.createUnion(contentLanguageDependentModel, decodingLanguageDependentModel);

        this.languageDependentModelMap.put(languageBean, languageDependentModel);
        return xmlBuilder;
    }

    /**
     * Fallback inverse.
     *
     * @param languageBean the language bean
     * @param decoding     the decoding
     */
    private void fallbackInverse(final LanguageBean languageBean, final LanguageBean decoding) {
        XmlBuilder xmlBuilder = this.xmlBuilderMap.get(languageBean);
        this.fallback();
        final boolean test = languageBean == null;
        if (test) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.INPUT_MUST_BE_DEFINED)
                    .withMessage("Property 'language' cannot be null.").build();
        }

        if (xmlBuilder == null) {
            xmlBuilder = new XmlBuilder(NOTICE).attribute(TYPE, this.noticeType.toString());
            xmlBuilder.attribute(DECODING, decoding.getIsoCodeThreeChar());
            this.xmlBuilderMap.put(languageBean, xmlBuilder);
        }
    }

    /**
     * Fallback embedded.
     *
     * @return the xml builder
     */
    private XmlBuilder fallbackEmbedded() {
        XmlBuilder xmlBuilder = this.xmlBuilderMap.get(null);
        this.fallback();
        if (this.decodingModel == null) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.INPUT_MUST_BE_DEFINED)
                    .withMessage("Property 'decodingModel' cannot be null.").build();
        }

        if (xmlBuilder == null) {
            xmlBuilder = new XmlBuilder(this.cellarIdentifiedObject.getType().toString());
            this.xmlBuilderMap.put(null, xmlBuilder);
        }
        final Model languageDependentModel = this.cache.getDecodedMetadataNotCached(this.cellarIdentifiedObject.getCellarId().getIdentifier(),
                this.decodingModel);
        this.languageDependentModelMap.put(null, languageDependentModel);
        return xmlBuilder;
    }

    /**
     * Checks if is inverse enabled.
     *
     * @return true, if is inverse enabled
     */
    private boolean isInverseEnabled() {
        final String key = "cellar.service.inverseNotices." + this.cellarIdentifiedObject.getType().toString().toLowerCase() + ".enabled";
        final CellarConfigurationPropertyKey confKey = CellarConfigurationPropertyKey.get(key);
        if (confKey == null) {
                LOG.warn("The configuration property key '{}' does not exist: inverse notices for object of type '{}' " +
                        "will not be generated.", key, cellarIdentifiedObject);
            return false;
        }

        return Boolean.valueOf(cellarConfiguration.getProperty(confKey));
    }

    /**
     * Adds the mime type attribute.
     *
     * @param xmlBuilder the xml builder
     */
    private void addMimeTypeAttribute(final XmlBuilder xmlBuilder) {
        if (!DigitalObjectType.MANIFESTATION.equals(this.cellarIdentifiedObject.getType())) {
            return;
        }

        String mimeType;
        if (this.cellarIdentifiedObject instanceof DigitalObject) {
            mimeType = ((DigitalObject) this.cellarIdentifiedObject).getManifestationMimeType();
        } else {
            final CellarResource cellarResource = cellarResourceDao.findCellarId(this.cellarIdentifiedObject.getCellarId().getIdentifier());
            mimeType = cellarResource.getMimeType();
        }
        if (StringUtils.isNotBlank(mimeType)) {
            xmlBuilder.attribute(MANIFESTATION_TYPE, mimeType);
        }
    }

    /**
     * Checks if is sort.
     *
     * @return the sort
     */
    public boolean isSort() {
        return sort;
    }

    /**
     * Sets the sort.
     *
     * @param sort the sort to set
     */
    public void setSort(boolean sort) {
        this.sort = sort;
    }

}
