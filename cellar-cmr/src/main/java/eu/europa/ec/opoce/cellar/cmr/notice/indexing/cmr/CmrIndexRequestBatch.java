/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr
 *             FILE : IndexRequestBatch.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 31, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr;

import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService.Priority;
import eu.europa.ec.opoce.cellar.cmr.database.DaoObject;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 31, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class CmrIndexRequestBatch extends DaoObject implements Comparable<CmrIndexRequestBatch>, Serializable {

    /*
     * select group_by_uri, priority, min(created_on) as mindate from cmr_index_request group by group_by_uri, priority order by priority desc, mindate asc;
     * 
    private Date firstRequestDate;
    private final String groupByUri;
    private final DigitalObjectType digitalObjectType;
    private Priority priority;
     */

    private String groupByUri;
    private Priority priority;
    private Date createdOn;

    private Date maximumCreatedOn;
    private Priority minimumPriority;

    private Collection<CmrIndexRequest> cmrIndexRequests;

    public CmrIndexRequestBatch() {

    }

    public CmrIndexRequestBatch(final String groupByUri, final Priority priority, final Date createdOn,
            final List<CmrIndexRequest> cmrIndexRequests) {
        this.groupByUri = groupByUri;
        this.priority = priority;
        this.createdOn = createdOn;
        this.cmrIndexRequests = cmrIndexRequests;
    }

    /**
     * @return the groupByUri
     */
    public String getGroupByUri() {
        return this.groupByUri;
    }

    /**
     * @param groupByUri the groupByUri to set
     */
    public void setGroupByUri(final String groupByUri) {
        this.groupByUri = groupByUri;
    }

    /**
     * @return the priority
     */
    public Priority getPriority() {
        return this.priority;
    }

    /**
     * @param priority the priority to set
     */
    public void setPriority(final Priority priority) {
        this.priority = priority;
    }

    /**
     * @return the createdOn
     */
    public Date getCreatedOn() {
        return this.createdOn;
    }

    /**
     * @param createdOn the createdOn to set
     */
    public void setCreatedOn(final Date createdOn) {
        this.createdOn = createdOn;
    }

    /**
     * @return the maximumCreatedOn
     */
    public Date getMaximumCreatedOn() {
        return this.maximumCreatedOn;
    }

    /**
     * @param maximumCreatedOn the maximumCreatedOn to set
     */
    public void setMaximumCreatedOn(Date maximumCreatedOn) {
        this.maximumCreatedOn = maximumCreatedOn;
    }

    /**
     * @return the minimumPriority
     */
    public Priority getMinimumPriority() {
        return this.minimumPriority;
    }

    /**
     * @param minimumPriority the minimumPriority to set
     */
    public void setMinimumPriority(Priority minimumPriority) {
        this.minimumPriority = minimumPriority;
    }

    /**
     * @return the cmrIndexRequests
     */
    public Collection<CmrIndexRequest> getCmrIndexRequests() {
        return this.cmrIndexRequests;
    }

    public void setCmrIndexRequests(final Collection<CmrIndexRequest> cmrIndexRequests) {
        this.cmrIndexRequests = cmrIndexRequests;
    }

    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(final CmrIndexRequestBatch o) {
        return this.getPriority().getPriorityValue() - o.getPriority().getPriorityValue();
    }

    @Override
    public String toString() {
        return new StringBuilder("CmrIndexRequestBatch{ ").append(this.getId()).append(" groupByUri='").append(this.groupByUri).append('\'')
                .append(", createdOn=").append(this.createdOn).append(", priority=").append(this.priority).append('}').toString();
    }
}
