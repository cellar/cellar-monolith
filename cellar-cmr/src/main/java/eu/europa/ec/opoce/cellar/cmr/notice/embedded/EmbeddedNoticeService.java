/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice.embedded
 *             FILE : EmbeddedNoticeService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Sep 22, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.notice.embedded;

import org.apache.jena.rdf.model.Model;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.domain.content.mets.CellarIdentifiedObject;
import org.w3c.dom.Document;

import java.util.Set;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Sep 22, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface EmbeddedNoticeService {

    void refreshTreeEmbeddedNotices(final String cellarId);

    void refreshEmbeddedNotice(final CellarResource cellarResource, final CellarIdentifiedObject cellarIdentifiedObject,
                               final Set<String> uris, final Model dmdIndex, final Model decoding);

    /**
     * Creates the embedded notice.
     *
     * @param object        the object
     * @param metadataModel the metadata model
     * @param decodingModel the decoding model
     * @return the document
     */
    Document createEmbeddedNotice(CellarIdentifiedObject object, Model metadataModel, Model decodingModel);
}
