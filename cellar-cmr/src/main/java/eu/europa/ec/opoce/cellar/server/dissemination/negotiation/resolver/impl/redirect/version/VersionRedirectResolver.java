/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.version
 *             FILE : VersionRedirectResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 26, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.version;

import eu.europa.ec.opoce.cellar.common.http.headers.IURLTokenable;
import eu.europa.ec.opoce.cellar.common.http.headers.RelType;
import eu.europa.ec.opoce.cellar.server.dissemination.AcceptLanguage;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.DisseminationRequest;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.Type;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.IDisseminationResolver;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.RedirectResolver;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;
import org.apache.commons.lang.StringUtils;

import java.util.Date;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 26, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public abstract class VersionRedirectResolver extends RedirectResolver {

    /** The resource uri. */
    protected final String resourceURI;

    /** The original resource uri. */
    protected final String originalResourceURI;

    /**
     * Instantiates a new version redirect resolver.
     *
     * @param cellarResource the cellar resource
     * @param resourceURI the resource uri
     * @param originalResourceURI the original resource uri
     * @param accept the accept
     * @param decoding the decoding
     * @param acceptLanguage the accept language
     * @param acceptDateTime the accept date time
     * @param rel the rel
     * @param provideAlternates the provide alternates
     */
    protected VersionRedirectResolver(final CellarResourceBean cellarResource, final String resourceURI, final String originalResourceURI,
            final String accept, final String decoding, final AcceptLanguage acceptLanguage, final Date acceptDateTime, final String rel,
            final boolean provideAlternates) {
        super(cellarResource, acceptLanguage, acceptDateTime, rel, null, provideAlternates);
        this.resourceURI = resourceURI;
        this.originalResourceURI = originalResourceURI;
    }

    /**
     * Gets the.
     *
     * @param cellarResource the cellar resource
     * @param resourceURI the resource uri
     * @param disseminationRequest the dissemination request
     * @param accept the accept
     * @param decoding the decoding
     * @param acceptLanguage the accept language
     * @param acceptDateTime the accept date time
     * @param rel the rel
     * @param provideAlternates the provide alternates
     * @return the i dissemination resolver
     */
    public static IDisseminationResolver get(final CellarResourceBean cellarResource, final String resourceURI,
            final DisseminationRequest disseminationRequest, final String accept, final String decoding,
            final AcceptLanguage acceptLanguage, final Date acceptDateTime, final String rel, final boolean provideAlternates) {
        final String originalResourceURI = identifierService.getCellarUriOrNull(mementoService.getOriginalCellarUri(resourceURI));

        IDisseminationResolver retDisseminationResolver;
        // case timemap: rel parameter is set to "timemap"
        if (StringUtils.isNotBlank(rel) && StringUtils.equalsIgnoreCase(RelType.TIMEMAP.getValue(), rel)) {
            retDisseminationResolver = TimeMapRedirectResolver.get(cellarResource, resourceURI, disseminationRequest, accept, decoding,
                    acceptLanguage, acceptDateTime, rel, provideAlternates);
        }
        // case timegate: the resource's URI is the original resource's URI
        else if (StringUtils.equalsIgnoreCase(resourceURI, originalResourceURI)) {
            retDisseminationResolver = TimeGateRedirectResolver.get(cellarResource, resourceURI, disseminationRequest, accept, decoding,
                    acceptLanguage, acceptDateTime, rel, provideAlternates);
        }
        // case intermediate resource: the resource's URI is not the original resource's URI
        else {
            retDisseminationResolver = IntermediateResourceRedirectResolver.get(cellarResource, resourceURI, originalResourceURI,
                    disseminationRequest, accept, decoding, acceptLanguage, acceptDateTime, rel, provideAlternates);
        }

        return retDisseminationResolver;
    }

    /** {@inheritDoc} */
    @Override
    protected IURLTokenable getTypeStructure() {
        return Type.MEMENTO;
    }

}
