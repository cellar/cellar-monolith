/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.rdf
 *             FILE : RdfTreeRedirectResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 10, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.rdf;

import eu.europa.ec.opoce.cellar.common.http.headers.IURLTokenable;
import eu.europa.ec.opoce.cellar.server.dissemination.AcceptLanguage;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.InferenceVariance;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.Structure;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.IDisseminationResolver;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Apr 10, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class RdfTreeRedirectResolver extends RdfRedirectResolver {

    /**
     * Instantiates a new rdf tree redirect resolver.
     *
     * @param cellarResource the cellar resource
     * @param inferenceVariance the inference variance
     * @param acceptLanguage the accept language
     * @param provideAlternates the provide alternates
     * @param normalized if the notice should be normalized
     */
    protected RdfTreeRedirectResolver(final CellarResourceBean cellarResource, final InferenceVariance inferenceVariance,
            final AcceptLanguage acceptLanguage, final boolean provideAlternates,boolean normalized) {
        super(cellarResource, inferenceVariance, acceptLanguage, provideAlternates,normalized);
    }

    /**
     * Gets the.
     *
     * @param cellarResource the cellar resource
     * @param inferenceVariance the inference variance
     * @param acceptLanguage the accept language
     * @param provideAlternates the provide alternates
     * @param normalized if the notice should be normalized
     * @return the i dissemination resolver
     */
    public static IDisseminationResolver get(final CellarResourceBean cellarResource, final InferenceVariance inferenceVariance,
            final AcceptLanguage acceptLanguage, final boolean provideAlternates,boolean normalized) {
        return new RdfTreeRedirectResolver(cellarResource, inferenceVariance, acceptLanguage, provideAlternates,normalized);
    }

    /** {@inheritDoc} */
    @Override
    protected void doHandleWorkDisseminationRequest() {
        // nothing to do
    }

    /** {@inheritDoc} */
    @Override
    protected void doHandleDossierDisseminationRequest() {
        // nothing to do
    }

    /** {@inheritDoc} */
    @Override
    protected void doHandleAgentDisseminationRequest() {
        // nothing to do
    }

    /** {@inheritDoc} */
    @Override
    protected void doHandleTopLevelEventDisseminationRequest() {
        // nothing to do
    }

    /** {@inheritDoc} */
    @Override
    protected IURLTokenable getTypeStructure() {
        return Structure.TREE;
    }

}
