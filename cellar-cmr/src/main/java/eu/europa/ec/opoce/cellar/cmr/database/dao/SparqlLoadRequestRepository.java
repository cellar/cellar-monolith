package eu.europa.ec.opoce.cellar.cmr.database.dao;

import eu.europa.ec.opoce.cellar.ExecutionStatus;
import eu.europa.ec.opoce.cellar.sparql.SparqlLoadRequest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author ARHS Developments
 */
@Repository
@Transactional(transactionManager = "transactionManager")
public interface SparqlLoadRequestRepository extends CrudRepository<SparqlLoadRequest, Long> {

    Optional<SparqlLoadRequest> findById(Long id);

    Optional<SparqlLoadRequest> findByUri(String uri);

    List<SparqlLoadRequest> findByExecutionStatus(ExecutionStatus status);

    List<SparqlLoadRequest> findByActivationDateBeforeAndExecutionStatusIn(Date activationDate, ExecutionStatus... status);

    List<SparqlLoadRequest> findByUriAndExecutionStatus(String uri, ExecutionStatus status);

    void deleteAllByUri(String uri);
}
