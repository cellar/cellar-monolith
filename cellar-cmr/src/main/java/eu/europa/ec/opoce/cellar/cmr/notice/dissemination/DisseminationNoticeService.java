package eu.europa.ec.opoce.cellar.cmr.notice.dissemination;

import org.apache.jena.rdf.model.Model;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Agent;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Dossier;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.TopLevelEvent;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Work;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import org.w3c.dom.Document;

import java.util.Collection;
import java.util.List;

public interface DisseminationNoticeService {
    /**
     * generate a branch notice.
     *
     * @param work             the notice has to be generated of this work, with only 1 expression selected
     * @param decodingLanguage the decoding language that needs to be used in de notice
     * @param filter           indicates if the branch notice must be filtered
     * @return notice  about the work, expression and manifestation and decoded in the specified language
     */
    Document createBranchNoticeForWork(Work work, LanguageBean decodingLanguage, List<LanguageBean> contentLanguages, boolean filter);

    /**
     * generate a branch notice.
     *
     * @param dossier  the notice has to be generated of this dossier, with only 1 event
     * @param language the decoding language that needs to be used in de notice
     * @param filter   indicates if the branch notice must be filtered
     * @return notice  about the dossier and expression, decoded in the specified language
     */
    Document createBranchNoticeForDossier(Dossier dossier, LanguageBean language, boolean filter);

    /**
     * generate a branch notice.
     *
     * @param agent    the notice has to be generated of this agent
     * @param language the decoding language that needs to be used in de notice
     * @param filter   the filter
     * @return notice  about the agent and decoded in the specified language
     */
    Document createBranchNoticeForAgent(Agent agent, LanguageBean language, boolean filter);

    /**
     * generate a branch notice.
     *
     * @param topLevelEvent the notice has to be generated of this topLevelEvent
     * @param language      the decoding language that needs to be used in de notice
     * @param filter        the filter
     * @return notice  about the topLevelEvent and decoded in the specified language
     */
    Document createBranchNoticeForTopLevelEvent(TopLevelEvent topLevelEvent, LanguageBean language, boolean filter);

    /**
     * generate a tree notice.
     *
     * @param dossier  the notice has to be generated of this dossier
     * @param language the decoding language that needs to be used in de notice
     * @param filter   the filter
     * @return notice  about the dossier and expressions and decoded in the specified language
     */
    Document createTreeNoticeForDossier(Dossier dossier, LanguageBean language, boolean filter);

    /**
     * generate a tree notice.
     *
     * @param work     the notice has to be generated of this work
     * @param language the decoding language that needs to be used in de notice
     * @param filter   the filter
     * @return notice  about the object and decoded in the specified language
     */
    Document createTreeNoticeForWork(Work work, LanguageBean language, boolean filter);

    /**
     * generate a object notice.
     *
     * @param object   object the notice has to be generated of
     * @param language the decoding language that needs to be used in de notice
     * @param filter   the filter
     * @return notice  about the object and decoded in the specified language
     */
    Document createObjectNotice(MetsElement object, LanguageBean language, boolean filter);

    /**
     * generate identifier notice.
     *
     * @param input  collection of uris that need to be shown in the identifier notice
     * @param result model with all the information about the input to build the identifier notice
     * @return document containing a xml tag for each uri of the input and it's related information
     */
    Document createIdentifierNotice(Collection<String> input, Model result);

    /**
     * generate identifier notice.
     *
     * @param input  collection of uris that need to be shown in the identifier notice
     * @param result model with all the information about the input to build the identifier notice
     * @return document containing a xml tag for each uri of the input and it's related information
     */
    Document createIdentifierNoticeWithoutObject(Collection<String> input, Model result);

}
