/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.indexing.service
 *             FILE : IIndexRequestManagingService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Nov 10, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.indexing.service;

import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.ExecutionStatus;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestBatch;

import java.util.Collection;

/**
 * This class is responsible to handle the {@link CmrIndexRequest}.
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IndexRequestManagerService {

    /**
     * Reset pending execution cmr index requests.
     */
    int resetPendingExecutionCmrIndexRequests();

    /**
     * Change the {@link CmrIndexRequest}s from status Pending
     * to New
     */
    void resetPendingRequests();

    Collection<CmrIndexRequest> findIndexRequests(CmrIndexRequestBatch batch);

    /**
     * Update the status of all the {@link CmrIndexRequest} in the
     * {@link CmrIndexRequestBatch}.
     *
     * @param batch     the current set of {@link CmrIndexRequest}
     * @param newStatus the new status of the {@link CmrIndexRequest}s
     */
    void updateBatchStatus(CmrIndexRequestBatch batch, ExecutionStatus newStatus);

    /**
     * Update the status of all the {@link CmrIndexRequest} in the
     * {@link CmrIndexRequestBatch}.
     *
     * @param batch     the current set of {@link CmrIndexRequest}
     * @param newStatus the new status of the {@link CmrIndexRequest}s
     * @param statusCause the cause of the status' change
     */
    void updateBatchStatus(CmrIndexRequestBatch batch, ExecutionStatus newStatus, String statusCause);

    /**
     * Update the status of the {@link CmrIndexRequest}
     *
     * @param request   the {@link CmrIndexRequest} to update
     * @param newStatus the new status of the {@link CmrIndexRequest}
     */
    void updateRequestStatus(CmrIndexRequest request, ExecutionStatus newStatus);

    /**
     * Update the status of the {@link CmrIndexRequest}. This method
     * accept a cause which is used to make easier the debugging. The cause
     * can be a unique identifier which match a log event (generally wrote
     * in a log file). This feature is useful if the new status is
     * {@link ExecutionStatus#Error} because the stacktrace can be linked to
     * the status. The {@link CmrIndexRequest} is potentially handled by more
     * than one thread during its lifecycle so the debugging and the tracing
     * of the changes done on the {@link ExecutionStatus} can be difficult
     * to follow. Assigning a unique identifier to the error makes the debugging
     * easier.
     *
     * @param request the {@link CmrIndexRequest} to update
     * @param status the new status of the {@link CmrIndexRequest}
     * @param statusCause the cause of the status' change
     */
    void updateRequestStatus(CmrIndexRequest request, ExecutionStatus status, String statusCause);

    /**
     * Remove the old {@link CmrIndexRequest}s
     *
     * That method is called by a {@link org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler}
     * created by Spring Framework. Do not remove it, even if that method seems unused.
     * See cellar-core/src/main/resources/scheduler-context.xml for cron declaration.
     */
    void removeOldCmrIndexRequests();

    /**
     * Finalize cmr index request batch.
     *
     * @param batch the batch
     */
    void finalizeCmrIndexRequestBatch(final CmrIndexRequestBatch batch);

    /**
     * Finalize cmr index request batch.
     *
     * @param batch           the batch
     * @param executionStatus the execution status
     */
    void finalizeCmrIndexRequestBatch(final CmrIndexRequestBatch batch, final ExecutionStatus executionStatus, String statusCause);

    /**
     * Restart failed cmr index requests.
     */
    void restartFailedCmrIndexRequests();
    
    /**
     * Update the INDX_EXECUTION_STATUS (and the corresponding datetime) of the respective STRUCTMAP_STATUS_HISTORY entries.
     * @param cmrIndexRequests the current list of index requests
     * @param newStatus the new status for the current list of index requests
     */
    void updateStructMapIndxStatusAndRespectiveDatetime(Collection<CmrIndexRequest> cmrIndexRequests, CmrIndexRequest.ExecutionStatus newStatus);
    
}
