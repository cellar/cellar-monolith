/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.virtuoso
 *             FILE : VirtuosoService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 08-06-2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.virtuoso;

import eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData;
import eu.europa.ec.opoce.cellar.common.util.Pair;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.exception.VirtuosoOperationException;
import eu.europa.ec.opoce.cellar.server.admin.sparql.VirtuosoInformation;
import eu.europa.ec.opoce.cellar.virtuoso.retry.annotation.VirtuosoRetry;
import virtuoso.jena.driver.VirtDataset;

import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.Model;

import java.util.*;

/**
 * <class_description> This interface declares the methods for accessing the Virtuoso platform.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 08-06-2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface VirtuosoService {

    Model read(final String cellarUri, VirtDataset dataset);

    /**
     * Write a model into virtuoso.
     *
     * @param model the model to write
     * @param modelUri the URI associate to the model
     * @param hasRetryAttempts true if retry attempts are available, false when retry attempts are exceeded
     * @throws VirtuosoOperationException if something goes wrong
     */
    void write(final Model model, String modelUri, boolean hasRetryAttempts) throws VirtuosoOperationException;

    /**
     * Writes a NAL named graph in Virtuoso
     *
     * @param model the NAL as a Jena Model
     * @param modelUri the URI of the NAL
     * @param hasRetryAttempts true if retry attempts are available, false when retry attempts are exceeded
     * @throws VirtuosoOperationException thrown if a problem occurs while writing
     */
    void writeNal(final Model model, String modelUri, boolean hasRetryAttempts) throws VirtuosoOperationException;

    /**
     * Writes a Ontology named graph in Virtuoso
     *
     * @param model the Ontology as a Jena Model
     * @param modelUri the URI of the ontology
     * @param hasRetryAttempts true if retry attempts are available, false when retry attempts are exceeded
     * @throws VirtuosoOperationException thrown if a problem occurs while writing
     */
    void writeOnto(final Model model, String modelUri, boolean hasRetryAttempts) throws VirtuosoOperationException;

    /**
     * Write the calculated data in Virtuoso.
     *
     * @param calculatedData the calculated data to write
     * @param hasRetryAttempts true if retry attempts are available, false when retry attempts are exceeded
     * @return the set
     * @throws VirtuosoOperationException thrown in case a problem occurs while writing
     */
    Set<String> write(final CalculatedData calculatedData, boolean hasRetryAttempts) throws VirtuosoOperationException;

    /**
     * Drops the named graphs with the given {@cellarIDs}.
     *
     * @param cellarIDs the cellar ids representing the named graphs to drop
     * @param hasRetryAttempts true if retry attempts are available, false when retry attempts are exceeded
     * @param withVirtuosoBackup true if Virtuoso Backup mechanism should be used
     * @throws VirtuosoOperationException thrown in case a problem occurs while dropping
     */
    void drop(final Collection<String> cellarIDs, boolean withVirtuosoBackup, boolean hasRetryAttempts) throws VirtuosoOperationException;

    /**
     * Hide existing cellar object.
     *
     * @param cellarId the cellar ids
     * @param embargoDate the embargo date
     * @param lastModificationDate the lastModification date
     * @param hasRetryAttempts true if retry attempts are available, false when retry attempts are exceeded
     * @throws VirtuosoOperationException the virtuoso operation exception
     * @see eu.europa.ec.opoce.cellar.virtuoso.VirtuosoService#drop(java.util.Collection, boolean, boolean)
     */
    void hide(String cellarId, final Date embargoDate, final Date lastModificationDate, boolean hasRetryAttempts) throws VirtuosoOperationException;

    /**
     * Hide for embargo. The difference is the transaction propagation
     *
     * @param cellarId the cellar id
     * @param embargoDate the embargo date
     * @param lastModificationDate the last modification date
     * @param hasRetryAttempts true if retry attempts are available, false when retry attempts are exceeded
     * @throws VirtuosoOperationException the virtuoso operation exception
     */
    void hideInNewTransaction(final String cellarId, final Date embargoDate, final Date lastModificationDate, boolean hasRetryAttempts)
            throws VirtuosoOperationException;


    /**
     * Unhide existing cellar object.
     *
     * @param cellarId the cellar id
     * @param embargoDate the embargo date
     * @param lastModificationDate the lastModification date
     * @param hasRetryAttempts true if retry attempts are available, false when retry attempts are exceeded
     * @throws VirtuosoOperationException the exception
     */
    void unhide(String cellarId, final Date embargoDate, final Date lastModificationDate, boolean hasRetryAttempts) throws VirtuosoOperationException;

    /**
     * Unhide.
     *
     * @param cellarIds the cellar ids
     * @param embargoDate the embargo date
     * @param lastModificationDate the last modification date
     * @param hasRetryAttempts true if retry attempts are available, false when retry attempts are exceeded
     * @throws VirtuosoOperationException the virtuoso operation exception
     */
    void unhide(final Collection<String> cellarIds, final Date embargoDate, final Date lastModificationDate, boolean hasRetryAttempts)
            throws VirtuosoOperationException;

    /**
     * Hide.
     *
     * @param cellarIds the cellar ids
     * @param hasRetryAttempts true if retry attempts are available, false when retry attempts are exceeded
     * @throws VirtuosoOperationException the exception
     */
    void hide(Set<String> cellarIds, boolean hasRetryAttempts) throws VirtuosoOperationException;

    /**
     * Unhide.
     *
     * @param cellarIds the cellar ids
     * @param hasRetryAttempts true if retry attempts are available, false when retry attempts are exceeded
     * @throws VirtuosoOperationException the virtuoso operation exception
     */
    void unhide(Set<String> cellarIds, boolean hasRetryAttempts) throws VirtuosoOperationException;

    /**
     * Checks if is under embargo.
     *
     * @param cellarId the cellar id
     * @return true, if is under embargo
     * @throws VirtuosoOperationException the virtuoso operation exception
     * @see eu.europa.ec.opoce.cellar.virtuoso.VirtuosoService#isUnderEmbargo(java.lang.String)
     */
    boolean isUnderEmbargo(final String cellarId) throws VirtuosoOperationException;

    /**
     * Gets the outdated embargoed works.
     *
     * @return the outdated embargoed works
     * @throws VirtuosoOperationException the virtuoso operation exception
     */
    List<Pair<String, Date>> getOutdatedEmbargoedWorks() throws VirtuosoOperationException;

    /**
     * realignment - realigns the models of inverse relations with the pid history table.
     * get content ids
     * get obsolete records - keep cellar ids
     * get inverse relations by target () - using the current cellar id
     * apply script
     *
     * @param dataset the dataset
     * @param currentCellarID the current cellar id
     * @param productionIds the production ids
     */
    void realignment(final Dataset dataset, final String currentCellarID, final Collection<String> productionIds);

    /**
     * Returns Virtuoso's driver and server versions
     * @return the virtuoso environment information
     */
    VirtuosoInformation getVirtuosoInformation();

    /**
     * Throws {@link VirtuosoOperationException} exception with message "SR172: Transaction deadlocked" to
     * trigger Virtuoso Retry mechanism in case of a deadlocked transaction.
     * <br/><br/>
     *To be combined with {@link VirtuosoRetry} annotation and a try-catch clause in the corresponding method
     *
     * @param e The exception
     * @param hasRetryAttempts true if retry attempts are available, false when retry attempts are exceeded
     *
     * @throws VirtuosoOperationException on purpose if "SR172: Transaction deadlocked" and has available retry attempts
     */
    void triggerVirtuosoRetry(Exception e, boolean hasRetryAttempts) throws VirtuosoOperationException;

    Map<DigitalObject, Model> getMetadata(final CalculatedData data);
}
