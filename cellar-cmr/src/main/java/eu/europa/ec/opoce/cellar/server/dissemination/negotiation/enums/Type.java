/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums
 *             FILE : Type.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 20, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums;

import eu.europa.ec.opoce.cellar.common.http.headers.IURLTokenable;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 20, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public enum Type implements IURLTokenable {
    LIST("list"),
    RDF("rdf"),
    XML("xml"),
    ZIP("zip"),
    CONTENT_STREAM("CONTENT_STREAM"),
    IDENTIFIERS("identifiers"),
    MEMENTO("memento"),
    LINK("link-format");

    private final String urlToken;

    private Type(final String urlToken) {
        this.urlToken = urlToken;
    }

    @Override
    public String getURLToken() {
        return this.urlToken;
    }

    @Override
    public String getErrorLabel() {
        return this.toString();
    }
}
