package eu.europa.ec.opoce.cellar.versioning.impl;

import eu.europa.ec.opoce.cellar.versioning.ITimeMapInfo;

import java.util.Date;

/**
 * Implements the {@code ITimeMapInfo} interface
 * Objects of this class represent basic time information related to a given TimeMap 
 * @author ARHS-Developments
 *
 */

public class TimeMapInfoImpl implements ITimeMapInfo {

    /**
     * Start date of the TimeMap
     */
    private final Date startdate;

    /**
     * End date of the TimeMap
     */
    private final Date enddate;

    /**
     * Type of date
     */
    private final String typeofdate;

    public TimeMapInfoImpl(final Date startdate, final Date enddate, final String typeofdate) {
        this.startdate = startdate;
        this.enddate = enddate;
        this.typeofdate = typeofdate;
    }

    @Override
    public Date getStartDate() {
        return this.startdate;
    }

    @Override
    public Date getEndDate() {
        return this.enddate;
    }

    @Override
    public String getTypeofDate() {
        return this.typeofdate;
    }
}
