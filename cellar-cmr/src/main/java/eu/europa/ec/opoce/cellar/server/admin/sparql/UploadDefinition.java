package eu.europa.ec.opoce.cellar.server.admin.sparql;

import org.springframework.web.multipart.commons.CommonsMultipartFile;

/**
 * <p>UploadDefinition class.</p>
 */
public class UploadDefinition {

    private CommonsMultipartFile fileData;

    /**
     * <p>Getter for the field <code>fileData</code>.</p>
     *
     * @return a {@link org.springframework.web.multipart.commons.CommonsMultipartFile} object.
     */
    public CommonsMultipartFile getFileData() {
        return fileData;
    }

    /**
     * <p>Setter for the field <code>fileData</code>.</p>
     *
     * @param fileData a {@link org.springframework.web.multipart.commons.CommonsMultipartFile} object.
     */
    public void setFileData(CommonsMultipartFile fileData) {
        this.fileData = fileData;
    }
}
