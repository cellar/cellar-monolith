/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ontology
 *             FILE : OntologyUpdateService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 01 16, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-01-16 13:44:03 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ontology.service;

import eu.europa.ec.opoce.cellar.ontology.domain.OntologyUpdate;
import eu.europa.ec.opoce.cellar.ontology.exception.OntologyException;

/**
 * @author ARHS Developments
 * @since 7.7
 */
public interface OntologyUpdateService {

    OntologyUpdateResult update(OntologyUpdate ontologyUpdate, boolean override) throws OntologyException;
}
