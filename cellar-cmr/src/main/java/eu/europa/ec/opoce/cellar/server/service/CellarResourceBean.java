package eu.europa.ec.opoce.cellar.server.service;

import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * <p>CellarResourceBean class.</p>
 */
public class CellarResourceBean {

    private CellarResource cellarResource;

    /**
     * <p>Constructor for CellarResourceBean.</p>
     *
     * @param cellarResource a {@link eu.europa.ec.opoce.cellar.cmr.CellarResource} object.
     */
    public CellarResourceBean(CellarResource cellarResource) {
        this.cellarResource = cellarResource;
    }

    /**
     * <p>getCellarId.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCellarId() {
        return cellarResource.getCellarId();
    }

    /**
     * <p>getCellarType.</p>
     *
     * @return a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType} object.
     */
    public DigitalObjectType getCellarType() {
        return cellarResource.getCellarType();
    }

    /**
     * <p>getEmbargoDate.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getEmbargoDate() {
        return cellarResource.getEmbargoDate();
    }

    public boolean isUnderEmbargo() {
        return this.cellarResource.isUnderEmbargo();
    }

    /**
     * <p>getLanguages.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<String> getLanguages() {
        return cellarResource.getLanguages();
    }

    /**
     * <p>getLastModificationDate.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getLastModificationDate() {
        return cellarResource.getLastModificationDate();
    }

    /**
     * <p>getMimeType.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getMimeType() {
        return cellarResource.getMimeType();
    }

    public Map<ContentType, String> getVersions() {
        return cellarResource.getVersions();
    }
    
    public Date getEmbeddedNoticeCreationDate() {
    	return this.cellarResource.getEmbeddedNoticeCreationDate();
    }
    
}
