/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.version
 *             FILE : IntermediateResourceRedirectResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 28, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$$
 *          VERSION : $LastChangedRevision$$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.version;

import eu.europa.ec.opoce.cellar.cmr.utils.MementoUtils;
import eu.europa.ec.opoce.cellar.common.http.headers.*;
import eu.europa.ec.opoce.cellar.server.dissemination.AcceptLanguage;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.DisseminationRequest;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.IDisseminationResolver;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 28, 2015
 *
 * @author ARHS Developments
 * @version $Revision$$
 */
@Configurable
public class IntermediateResourceRedirectResolver extends VersionRedirectResolver {

    /**
     * Instantiates a new intermediate resource redirect resolver.
     *
     * @param cellarResource the cellar resource
     * @param resourceURI the resource uri
     * @param originalResourceURI the original resource uri
     * @param accept the accept
     * @param decoding the decoding
     * @param acceptLanguage the accept language
     * @param acceptDateTime the accept date time
     * @param rel the rel
     * @param provideAlternates the provide alternates
     */
    protected IntermediateResourceRedirectResolver(final CellarResourceBean cellarResource, final String resourceURI,
            final String originalResourceURI, final String accept, final String decoding, final AcceptLanguage acceptLanguage,
            final Date acceptDateTime, final String rel, final boolean provideAlternates) {
        super(cellarResource, resourceURI, originalResourceURI, accept, decoding, acceptLanguage, acceptDateTime, rel, provideAlternates); //TODO: all these params are useful?
    }

    /**
     * Gets the.
     *
     * @param cellarResource the cellar resource
     * @param resourceURI the resource uri
     * @param originalResourceURI the original resource uri
     * @param disseminationRequest the dissemination request
     * @param accept the accept
     * @param decoding the decoding
     * @param acceptLanguage the accept language
     * @param acceptDateTime the accept date time
     * @param rel the rel
     * @param provideAlternates the provide alternates
     * @return the i dissemination resolver
     */
    public static IDisseminationResolver get(final CellarResourceBean cellarResource, final String resourceURI,
            final String originalResourceURI, final DisseminationRequest disseminationRequest, final String accept, final String decoding,
            final AcceptLanguage acceptLanguage, final Date acceptDateTime, final String rel, final boolean provideAlternates) {

        return new IntermediateResourceRedirectResolver(cellarResource, resourceURI, originalResourceURI, accept, decoding, acceptLanguage,
                acceptDateTime, rel, provideAlternates); // returns itself
    }

    /**
     * Response returned when requesting an intermediate resource.
     *
     * @return the response entity
     */
    @Override
    protected ResponseEntity<Void> doHandleDisseminationRequest() {
        // retrieve nearest location
        final String nearestMementoURI = MementoUtils.getNearestMemento(this.resourceURI, this.acceptDateTime);
        if (null == nearestMementoURI) {
            // nothing found: return 406
            return new ResponseEntity<Void>(HttpStatus.NOT_ACCEPTABLE);
        }

        // otherwise, build links and return
        final Collection<ILink> links = new LinkedList<ILink>();
        links.add(new LinkImpl(this.originalResourceURI, RelType.ORIGINAL_TIMEGATE));
        links.add(new LinkImpl(this.originalResourceURI + "?rel=timemap", RelType.TIMEMAP));
        links.add(new LinkImpl(this.resourceURI + "?rel=timemap", RelType.TIMEMAP));

        final HttpHeadersBuilder builder = HttpHeadersBuilder.get() //
                .withContentType("text/plan; charset=utf-8") //
                .withLocation(nearestMementoURI) //
                .withLink(StringUtils.join(links, ","));

        return new ResponseEntity<Void>(builder.getHttpHeaders(), HttpStatus.FOUND);
    }

    /**
     * Do handle work dissemination request.
     *
     * @see eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.RedirectResolver#doHandleWorkDisseminationRequest()
     */
    @Override
    protected void doHandleWorkDisseminationRequest() {
    }

    /**
     * Gets the type structure.
     *
     * @return the type structure
     * @see eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.version.VersionRedirectResolver#getTypeStructure()
     */
    @Override
    protected IURLTokenable getTypeStructure() {
        return RelType.MEMENTO;
    }

}
