/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice
 *             FILE : NoticeService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 23-1-2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.notice.dissemination;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Agent;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Dossier;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Event;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.TopLevelEvent;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Work;
import eu.europa.ec.opoce.cellar.cmr.notice.NoticeBuilder;
import eu.europa.ec.opoce.cellar.cmr.notice.NoticeBuilderServiceFactory;
import eu.europa.ec.opoce.cellar.cmr.notice.NoticeCache;
import eu.europa.ec.opoce.cellar.cmr.notice.core.NoticeType;
import eu.europa.ec.opoce.cellar.cmr.notice.core.Rdf2Xml;
import eu.europa.ec.opoce.cellar.cmr.notice.core.Rdf2XmlExecutor;
import eu.europa.ec.opoce.cellar.domain.content.mets.CellarIdentifiedObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DefaultDigitalObjectTypeVisitor;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.nal.service.LanguageService;
import eu.europa.ec.opoce.cellar.semantic.helper.DateFormats;
import eu.europa.ec.opoce.cellar.semantic.xml.XmlBuilder;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.OWL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

/**
 * <class_description> Service for building notices.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 23-01-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service("disseminationNoticeService")
public class DisseminationNoticeServiceImpl implements DisseminationNoticeService {

    private final LanguageService languageService;
    private final IdentifierService identifierService;
    private final CellarResourceDao cellarResourceDao;
    private final NoticeBuilderServiceFactory noticeBuilderServiceFactory;
    private final ICellarConfiguration cellarConfiguration;

    @Autowired
    public DisseminationNoticeServiceImpl(LanguageService languageService,
                                          @Qualifier("pidManagerService") IdentifierService identifierService,
                                          CellarResourceDao cellarResourceDao,
                                          NoticeBuilderServiceFactory noticeBuilderServiceFactory,
                                          ICellarConfiguration cellarConfiguration) {
        this.languageService = languageService;
        this.identifierService = identifierService;
        this.cellarResourceDao = cellarResourceDao;
        this.noticeBuilderServiceFactory = noticeBuilderServiceFactory;
        this.cellarConfiguration = cellarConfiguration;
    }

    @Override
    public Document createBranchNoticeForWork(final Work work, final LanguageBean decodingLanguage, final List<LanguageBean> contentLanguages,
                                              final boolean filter) {
        return this.createNotice(work, contentLanguages, decodingLanguage, filter, NoticeType.branch);
    }

    @Override
    public Document createBranchNoticeForDossier(final Dossier dossier, final LanguageBean language, final boolean filter) {
        if (dossier.getChildren().size() != 1) {
            throw ExceptionBuilder
                    .get(CellarException.class)
                    .withMessage("if a branch of a dossier is asked there has to be exactly 1 event defined")
                    .build();
        }
        return this.createBranchNoticeForDossier(dossier.getChildren().iterator().next(), language, filter);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Document createBranchNoticeForAgent(final Agent agent, final LanguageBean language, final boolean filter) {
        final List<LanguageBean> inLanguage = language != null ? Collections.singletonList(language) : new ArrayList<>();
        final HashSet<LanguageBean> languages = new HashSet<>(inLanguage);
        final HashSet<LanguageBean> contentLanguages = new HashSet<>();
        final NoticeBuilder builder = noticeBuilderServiceFactory
                .getNewNoticeBuilderInstance(NoticeType.branch)
                .withContentLanguages(contentLanguages)
                .withLanguages(languages)
                .withUseUriToCellarIdentifierCaching(true);
        try {
            return builder
                    .withObject(agent)
                    .withFilter(filter)
                    .buildDirect(language)
                    .buildInverse(language)
                    .getDocument(language);
        } finally {
            builder.close();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Document createBranchNoticeForTopLevelEvent(final TopLevelEvent topLevelEvent, final LanguageBean language, final boolean filter) {
        final HashSet<LanguageBean> contentLanguages = new HashSet<>();
        final List<LanguageBean> inLanguage = language != null ? Collections.singletonList(language) : new ArrayList<>();
        final HashSet<LanguageBean> languages = new HashSet<>(inLanguage);

        final NoticeBuilder builder = noticeBuilderServiceFactory
                .getNewNoticeBuilderInstance(NoticeType.branch)
                .withContentLanguages(contentLanguages)
                .withLanguages(languages)
                .withUseUriToCellarIdentifierCaching(true);
        try {
            return builder
                    .withObject(topLevelEvent)
                    .withFilter(filter)
                    .buildDirect(language)
                    .buildInverse(language)
                    .getDocument(language);
        } finally {
            builder.close();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Document createTreeNoticeForDossier(final Dossier dossier, final LanguageBean language, final boolean filter) {
        final List<LanguageBean> inLanguage = language != null ? Collections.singletonList(language) : new ArrayList<>();
        final HashSet<LanguageBean> languages = new HashSet<>(inLanguage);
        final HashSet<LanguageBean> contentLanguages = new HashSet<>();
        final NoticeBuilder builder = noticeBuilderServiceFactory
                .getNewNoticeBuilderInstance(NoticeType.tree)
                .withContentLanguages(contentLanguages)
                .withLanguages(languages)
                .withUseUriToCellarIdentifierCaching(true);
        try {
            builder
                .withObject(dossier)
                .withFilter(filter)
                .buildDirect(language)
                .buildInverse(language);
            for (final Event event : dossier.getChildren()) {
                builder.withObject(event).buildDirect(language).buildInverse(language);
            }
            return builder.getDocument(language);
        } finally {
            builder.close();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Document createTreeNoticeForWork(final Work work, final LanguageBean language, final boolean filter) {
        final LanguageBean[] arrayOfContentLanguages = new LanguageBean[]{
                language};
        final List<LanguageBean> contentLanguages = Arrays.asList(arrayOfContentLanguages);
        return this.createNotice(work, contentLanguages, language, filter, NoticeType.tree);
    }

    /**
     * Creates the notice.
     *
     * @param work             the work
     * @param contentLanguages the content languages
     * @param decodingLanguage the decoding language
     * @param filter           the filter
     * @param type             the type
     * @return the document
     */
    private Document createNotice(final Work work, final List<LanguageBean> contentLanguages, final LanguageBean decodingLanguage,
                                  final boolean filter, final NoticeType type) {
        final List<LanguageBean> inLanguage = decodingLanguage != null
                ? Collections.singletonList(decodingLanguage)
                : new ArrayList<>();
        final HashSet<LanguageBean> decodingLanguages = new HashSet<>(inLanguage);
        final HashSet<LanguageBean> inContentLanguages = contentLanguages != null
                ? new HashSet<>(contentLanguages)
                : new HashSet<>();

        final NoticeBuilder builder = noticeBuilderServiceFactory
                .getNewNoticeBuilderInstance(type).withLanguages(decodingLanguages)
                .withContentLanguages(inContentLanguages)
                .withUseUriToCellarIdentifierCaching(true);
        try {
            //contentLanguages can have 1 or more elements in the case of multilingual expressions
            //we can choose the first one because all elements should be linked to the same object
            final LanguageBean contentLanguage = contentLanguages != null ? contentLanguages.get(0) : null;
            final boolean showEmbargo = cellarConfiguration.isCellarServiceDisseminationRetrieveEmbargoedResourceEnabled();
            builder.withObject(work)
                    .withFilter(filter)
                    .buildDirect(contentLanguage, decodingLanguage)
                    .buildInverse(contentLanguage, decodingLanguage);

            work.getChildren().stream()
                    .filter(expression -> !expression.isUnderEmbargo() || showEmbargo)
                    .map(expression -> {
                        builder.withObject(expression)
                                .buildDirect(contentLanguage)
                                .buildInverse(contentLanguage);
                        return expression.getChildren();
                    })
                    .flatMap(Collection::stream)
                    .filter(manifestation -> !manifestation.isUnderEmbargo() || showEmbargo)
                    .forEach(manifestation -> builder.withObject(manifestation)
                            .buildDirect(contentLanguage)
                            .buildInverse(contentLanguage));

            return builder.getDocument(contentLanguage);
        } finally {
            builder.close();
        }
    }

    /**
     * <p>createNotice.</p>
     *
     * @param event    a {@link Event} object.
     * @param language a {@link LanguageBean} object.
     * @param filter   the filter
     * @return a {@link org.w3c.dom.Document} object.
     */
    private Document createBranchNoticeForDossier(final Event event, final LanguageBean language, final boolean filter) {
        final List<LanguageBean> inLanguage = language != null
                ? Collections.singletonList(language)
                : new ArrayList<>();
        final HashSet<LanguageBean> languages = new HashSet<>(inLanguage);
        final HashSet<LanguageBean> contentLanguages = new HashSet<>();
        final NoticeBuilder builder = noticeBuilderServiceFactory
                .getNewNoticeBuilderInstance(NoticeType.branch)
                .withContentLanguages(contentLanguages)
                .withLanguages(languages)
                .withUseUriToCellarIdentifierCaching(true);
        try {
            return builder
                    .withObject(event.getParent())
                    .withFilter(filter)
                    .buildDirect(language)
                    .buildInverse(language)
                    .withObject(event)
                    .buildDirect(language)
                    .buildInverse(language)
                    .getDocument(language);
        } finally {
            builder.close();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Document createObjectNotice(final MetsElement object, final LanguageBean language, final boolean filter) {
        final LanguageBean contentLanguage = this.getContentLanguage(object);
        final List<LanguageBean> inlanguage = language != null
                ? Collections.singletonList(language)
                : new ArrayList<>();
        final HashSet<LanguageBean> languages = new HashSet<>(inlanguage);
        final List<LanguageBean> inContentLanguage = contentLanguage != null
                ? Collections.singletonList(contentLanguage)
                : new ArrayList<>();
        final HashSet<LanguageBean> contentLanguages = new HashSet<>(inContentLanguage);
        final NoticeBuilder builder = noticeBuilderServiceFactory
                .getNewNoticeBuilderInstance(NoticeType.object)
                .withCache(new NoticeCache())
                .withCloseCache(true)
                .withFilter(filter)
                .withContentLanguages(contentLanguages)
                .withLanguages(languages)
                .withUseUriToCellarIdentifierCaching(true);
        try {
            return builder
                    .withObject(object)
                    .buildDirect(language)
                    .buildInverse(language)
                    .getDocument(language);
        } finally {
            builder.close();
        }
    }

    /**
     * <p>getContentLanguage.</p>
     *
     * @param object a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement} object.
     * @return a {@link eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean} object.
     */
    private LanguageBean getContentLanguage(final CellarIdentifiedObject object) {
        return object.getType().accept(new DefaultDigitalObjectTypeVisitor<Object, LanguageBean>() {

            @Override
            public LanguageBean visitWork(final Object o) {
                return null;
            }

            @Override
            public LanguageBean visitExpression(final Object o) {
                return languageService.getByTwoOrThreeChar(
                        cellarResourceDao.findCellarId(object.getCellarId().getIdentifier()).getLanguages().get(0));
            }

            @Override
            public LanguageBean visitManifestation(final Object o) {
                final String manifestationId = object.getCellarId().getIdentifier();
                return languageService.getByTwoOrThreeChar(cellarResourceDao
                        .findCellarId(manifestationId.substring(0, manifestationId.indexOf(".", manifestationId.indexOf(".") + 1)))
                        .getLanguages().get(0));
            }

            @Override
            public LanguageBean visitDossier(final Object o) {
                return null;
            }

            @Override
            public LanguageBean visitEvent(final Object o) {
                return null;
            }

            @Override
            public LanguageBean visitAgent(final Object o) {
                return null;
            }

            @Override
            public LanguageBean visitTopLevelEvent(final Object o) {
                return null;
            }

            @Override
            public LanguageBean visitEuronal(final Object o) {
                return null;
            }
        }, null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Document createIdentifierNotice(final Collection<String> input, final Model result) {
        final NoticeType type = NoticeType.identifier;
        final XmlBuilder root = new XmlBuilder(NoticeBuilder.NOTICE).attribute(NoticeBuilder.TYPE, type.name());

        for (final String uri : input) {
            final XmlBuilder objectElement = root.child(NoticeBuilder.OBJECT).attribute("in", uri);
            final String fullUri = this.identifierService.getUri(uri);
            addEmbargoDateToSameAs(result, objectElement, fullUri);
        }
        return root.getDocument();
    }

    private void addEmbargoDateToSameAs(Model result, XmlBuilder objectElement, String fullUri) {
        if (fullUri.contains("resource/cellar/")) {
            final Resource cellar = result.getResource(fullUri);
            if (cellar.hasProperty(OWL.sameAs)) {
                this.addEmbargoDate(objectElement, fullUri);
                final Rdf2Xml rdf2Xml = new Rdf2Xml(result, fullUri, objectElement, null, null, false, NoticeType.identifier);
                final Rdf2XmlExecutor rdf2XmlExecutor = new Rdf2XmlExecutor(rdf2Xml);
                rdf2XmlExecutor.execute();
            }
        } else {
            final Resource ps = result.getResource(fullUri);
            final ResIterator resIterator = result.listResourcesWithProperty(OWL.sameAs, ps);
            try {
                if (resIterator.hasNext()) {
                    final Resource cellar = resIterator.next();
                    if (resIterator.hasNext()) {
                        throw ExceptionBuilder.get(CellarException.class).withMessage("more than 1 cellarUri for psUri: '{}'")
                                .withMessageArgs(fullUri).build();
                    }
                    this.addEmbargoDate(objectElement, cellar.getURI());
                    final Rdf2Xml rdf2Xml = new Rdf2Xml(result, cellar.getURI(), objectElement, null, null, false, NoticeType.identifier);
                    final Rdf2XmlExecutor rdf2XmlExecutor = new Rdf2XmlExecutor(rdf2Xml);
                    rdf2XmlExecutor.execute();
                } else {
                    final Rdf2Xml rdf2Xml = new Rdf2Xml(result, fullUri, objectElement, null, null, false, NoticeType.identifier);
                    final Rdf2XmlExecutor rdf2XmlExecutor = new Rdf2XmlExecutor(rdf2Xml);
                    rdf2XmlExecutor.execute();
                }
            } finally {
                resIterator.close();
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Document createIdentifierNoticeWithoutObject(final Collection<String> input, final Model result) {
        final NoticeType type = NoticeType.identifier;
        final XmlBuilder root = new XmlBuilder(NoticeBuilder.NOTICE).attribute(NoticeBuilder.TYPE, type.name());

        for (final String uri : input) {
            final String fullUri = this.identifierService.getUri(uri);
            addEmbargoDateToSameAs(result, root, fullUri);
        }
        return root.getDocument();
    }

    /**
     * <p>addEmbargoDate.</p>
     *
     * @param objectElement a {@link eu.europa.ec.opoce.cellar.semantic.xml.XmlBuilder} object.
     * @param uri           a {@link java.lang.String} object.
     */
    private void addEmbargoDate(final XmlBuilder objectElement, final String uri) {
        final String cellarPrefixed = this.identifierService.getCellarPrefixed(uri);
        final String topCellarPrefixed = StringUtils.contains(cellarPrefixed, ".")
                ? StringUtils.substring(cellarPrefixed, 0, StringUtils.indexOf(cellarPrefixed, "."))
                : cellarPrefixed;
        final CellarResource cellarResource = this.cellarResourceDao.findCellarId(topCellarPrefixed);
        if (cellarResource.getEmbargoDate() != null) {
            objectElement.attribute("embargo-date", DateFormats.formatFullXmlDateTime(cellarResource.getEmbargoDate()));
        }
    }
}
