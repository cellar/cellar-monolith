package eu.europa.ec.opoce.cellar.server.dissemination;

import org.springframework.http.HttpStatus;

/**
 * overview of an error that occurred and its corresponding statuscode
 */
public class ErrorMessage {

    private HttpStatus httpStatus;
    private String message;

    public ErrorMessage(HttpStatus httpStatus, String message) {
        this.httpStatus = httpStatus;
        this.message = message;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public String getMessage() {
        return message;
    }

}
