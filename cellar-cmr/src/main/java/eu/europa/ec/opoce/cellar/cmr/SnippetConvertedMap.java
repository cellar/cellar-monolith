/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr
 *             FILE : SnippetConvertedMap.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 19 Apr 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr;

import eu.europa.ec.opoce.cellar.core.ConvertHistoryMap;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 19 Apr 2017
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class SnippetConvertedMap extends ConvertHistoryMap<Resource, Resource> {

    /** The model. */
    private final Model model;

    /**
     * Instantiates a new snippet converted map.
     *
     * @param model the model
     */
    public SnippetConvertedMap(final Model model) {
        this.model = model;
    }

    /** {@inheritDoc} */
    @Override
    public Resource convert(final Resource input) {
        return input.isURIResource() ? input : this.model.getResource(input.getURI());
    }
}
