/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.utils
 *             FILE : MetadataUtils.java
 *
 *       CREATED BY : EUROPEAN DYNAMICS S.A.
 *               ON : 02-Sep-2022
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.utils;

import eu.europa.ec.opoce.cellar.cl.domain.response.ResponseOperationType;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetadataType;
import eu.europa.ec.opoce.cellar.feed.domain.enums.FeedItemType;

/**
 * @author EUROPEAN DYNAMICS S.A.
 */
public final class MetadataUtils {

    private MetadataUtils() {

    }

    /**
     * Object is related to change, meaning it is not directly changed but its neighbors are.
     *
     * @param digitalObject the digital object
     * @param cellarResource the cellar resource
     * @param feedItemType the feed item type
     * @param responseOperationType the response operation type
     * @return true if object is related to changed object, false if not
     */
    public static boolean isRelatedToChange(final DigitalObject digitalObject, CellarResource cellarResource,
                                            final FeedItemType feedItemType, final ResponseOperationType responseOperationType) {
        if(!isDelete(feedItemType) && isDelete(responseOperationType)) {
            return true;
        } else {
            return isUpdate(feedItemType) && !isItem(cellarResource) && !containsBusinessOrTechnicalMetadata(digitalObject);
        }
    }

    /**
     * Object is directly changed.
     *
     * @param digitalObject the digital object
     * @param cellarResource the cellar resource
     * @param feedItemType the feed item type
     * @param responseOperationType the response operation type
     * @return true if object is directly changed, false if not
     */
    public static boolean isDirectlyChanged(final DigitalObject digitalObject, CellarResource cellarResource,
                                          final FeedItemType feedItemType, final ResponseOperationType responseOperationType) {
        return !isRelatedToChange(digitalObject, cellarResource, feedItemType, responseOperationType);
    }

    /**
     * Contains Business or Technical metadata.
     *
     * @param digitalObject the digital object
     * @return true if digital object contains business or technical metadata, false if not
     */
    public static boolean containsBusinessOrTechnicalMetadata(final DigitalObject digitalObject) {
        return containsBusinessMetadata(digitalObject) || containsTechnicalMetadata(digitalObject);
    }

    /**
     * Contains Business metadata.
     *
     * @param digitalObject the digital object
     * @return true if digital object business metadata, false if not
     */
    public static boolean containsBusinessMetadata(final DigitalObject digitalObject) {
        if(digitalObject.getBusinessMetadata() == null) {
            return false;
        }
        return digitalObject.getBusinessMetadata().getType() == MetadataType.DMD;
    }

    /**
     * Contains Technical metadata.
     *
     * @param digitalObject the digital object
     * @return true if digital object contains technical metadata, false if not
     */
    public static boolean containsTechnicalMetadata(final DigitalObject digitalObject) {
        if(digitalObject.getTechnicalMetadata() == null) {
            return false;
        }
        return digitalObject.getTechnicalMetadata().getType() == MetadataType.TECHNICAL;
    }

    /**
     * Is Work.
     *
     * @param cellarResource the cellar resource
     * @return true if cellar resource is Work, false if not
     */
    public static boolean isWork(final CellarResource cellarResource) {
        if(cellarResource.getCellarType() == null) {
            return false;
        }
        return cellarResource.getCellarType() == DigitalObjectType.WORK;
    }

    /**
     * Is Expression.
     *
     * @param cellarResource the cellar resource
     * @return true if cellar resource is Expression, false if not
     */
    public static boolean isExpression(final CellarResource cellarResource) {
        if(cellarResource.getCellarType() == null) {
            return false;
        }
        return cellarResource.getCellarType() == DigitalObjectType.EXPRESSION;
    }

    /**
     * Is Manifestation.
     *
     * @param cellarResource the cellar resource
     * @return true if cellar resource is Manifestation, false if not
     */
    public static boolean isManifestation(final CellarResource cellarResource) {
        if(cellarResource.getCellarType() == null) {
            return false;
        }
        return cellarResource.getCellarType() == DigitalObjectType.MANIFESTATION;
    }

    /**
     * Is item.
     *
     * @param cellarResource the cellar resource
     * @return true if cellar resource is Item, false if not
     */
    public static boolean isItem(final CellarResource cellarResource) {
        if(cellarResource.getCellarType() == null) {
            return false;
        }
        return cellarResource.getCellarType() == DigitalObjectType.ITEM;
    }

    /**
     * Is Create.
     *
     * @param feedItemType the feed item type
     * @return true if feed item type is create, false if not
     */
    public static boolean isCreate(final FeedItemType feedItemType) {
        if(feedItemType == null) {
            return false;
        }
        return feedItemType == FeedItemType.create;
    }

    /**
     * Is Update.
     *
     * @param feedItemType the feed item type
     * @return true if feed item type is create, false if not
     */
    public static boolean isUpdate(final FeedItemType feedItemType) {
        if(feedItemType == null) {
            return false;
        }
        return feedItemType == FeedItemType.update;
    }

    /**
     * Is Delete.
     *
     * @param feedItemType the feed item type
     * @return true if feed item type is create, false if not
     */
    public static boolean isDelete(final FeedItemType feedItemType) {
        if(feedItemType == null) {
            return false;
        }
        return feedItemType == FeedItemType.delete;
    }

    /**
     * Is Create.
     *
     * @param responseOperationType the response operation type
     * @return true if response operation type is Create, false if not
     */
    public static boolean isCreate(final ResponseOperationType responseOperationType) {
        if(responseOperationType == null) {
            return false;
        }
        return responseOperationType == ResponseOperationType.CREATE;
    }

    /**
     * Is Update.
     *
     * @param responseOperationType the response operation type
     * @return true if response operation type is Update, false if not
     */
    public static boolean isUpdate(final ResponseOperationType responseOperationType) {
        if(responseOperationType == null) {
            return false;
        }
        return responseOperationType == ResponseOperationType.UPDATE;
    }

    /**
     * Is Delete.
     *
     * @param responseOperationType the response operation type
     * @return true if response operation type is Delete, false if not
     */
    public static boolean isDelete(final ResponseOperationType responseOperationType) {
        if(responseOperationType == null) {
            return false;
        }
        return responseOperationType == ResponseOperationType.DELETE;
    }
}
