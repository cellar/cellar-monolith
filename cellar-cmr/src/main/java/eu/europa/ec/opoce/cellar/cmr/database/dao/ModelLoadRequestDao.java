/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.database.dao
 *             FILE : ModelLoadRequestDao.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Mar 17, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.database.dao;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;

import eu.europa.ec.opoce.cellar.cl.dao.BaseDao;
import eu.europa.ec.opoce.cellar.modelload.domain.ModelLoadRequest;
import eu.europa.ec.opoce.cellar.ExecutionStatus;
import eu.europa.ec.opoce.cellar.semantic.helper.StringHelper;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Mar 17, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface ModelLoadRequestDao extends BaseDao<ModelLoadRequest, Long> {

    String TABLE_NAME = "MODEL_LOAD_REQUEST";

    interface Column {

        String MODEL_URI = "MODEL_URI";
        String EXECUTION_STATUS = "EXECUTION_STATUS";
        String ACTIVATION_DATE = "ACTIVATION_DATE";
        String NAL_NAME = "NAL_NAME";
        String PID = "FEDORA_PID";
        String EXECUTION_DATE = "EXECUTION_DATE";
        String VERSION = "VERSION";
        String CELLAR_ID="CELLAR_ID";
    }

    String WHERE_PART_MODEL_URI = StringHelper.format("{} = :modelURI", Column.MODEL_URI);
    String WHERE_PART_EXECUTION_STATUS = StringHelper.format("{} = :executionStatus", Column.EXECUTION_STATUS);
    String WHERE_PART_MODEL_URI_EXECUTION_STATUS = StringHelper.format("{} = :modelURI AND {} = :executionStatus", Column.MODEL_URI,
            Column.EXECUTION_STATUS);
    String WHERE_PART_EXECUTION_STATUS_MAX_ACTIVATION_DATE = StringHelper.format("{} = :executionStatus AND {} <= :maximumActivationDate",
            Column.EXECUTION_STATUS, Column.ACTIVATION_DATE);

    Optional<ModelLoadRequest> findByModelURI(String modelURI);

    Collection<ModelLoadRequest> findByCriteria(String modelURI, String executionStatus);

    Collection<ModelLoadRequest> findByExecutionStatusAndMaxActivationDate(ExecutionStatus executionStatus,
            Date maximumActivationDate);
}
