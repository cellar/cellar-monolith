/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.graph.impl
 *             FILE : ReadGraphBuilder.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 27-01-2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.graph.impl;

/**
 * <class_description> Graph builder implementation for read.
 * <br/><br/>
 * ON : 27-01-2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class ReadGraphBuilder extends IngestionGraphBuilder {

    /**
     * @see eu.europa.ec.opoce.cellar.cmr.graph.IGraphBuilder#buildRelations()
     */
    @Override
    public GraphBuilder buildRelations() {
        return null;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cmr.graph.impl.IngestionGraphBuilder#isLoadMetadataModel()
     */
    @Override
    protected boolean isLoadMetadataModel() {
        return false;
    }

}
