package eu.europa.ec.opoce.cellar.cmr.database.dao.spring;

import eu.europa.ec.opoce.cellar.semantic.helper.StringHelper;
import org.springframework.stereotype.Repository;

/**
 * The Interface QueryAndTableCmrIndexRequest.
 */
@Repository
public interface QueryAndTableCmrIndexRequest {

    /**
     * ALTER TABLE CMR_INDEX_REQUEST MODIFY (CREATED_ON TIMESTAMP(0));.
     */
    interface Column {

        /** The object uri. */
        String OBJECT_URI = "OBJECT_URI";

        /** The group by uri. */
        String GROUP_BY_URI = "GROUP_BY_URI";

        /** The created on. */
        String CREATED_ON = "CREATED_ON";

        /** The mets doc. */
        String METS_DOC = "METS_DOC";

        /** The struct map id. */
        String STRUCT_MAP_ID = "STRUCTMAP_ID";

        /** The concept scheme. */
        String CONCEPT_SCHEME = "CONCEPT_SCHEME";

        /** The reason. */
        String REASON = "REASON";

        /** The requested by. */
        String REQUESTED_BY = "REQUESTED_BY";

        /** The priority. */
        String PRIORITY = "PRIORITY";

        /** The action. */
        String ACTION = "ACTION";

        /** The object type. */
        String OBJECT_TYPE = "OBJECT_TYPE";

        /** The request type. */
        String REQUEST_TYPE = "REQUEST_TYPE";

        /** The execution status. */
        String EXECUTION_STATUS = "EXECUTION_STATUS";

        /** The execution date. */
        String EXECUTION_DATE = "EXECUTION_DATE";

        /** The execution start date. */
        String EXECUTION_START_DATE = "EXECUTION_START_DATE";

        String STATUS_CAUSE = "STATUS_CAUSE";
        
        /** The STRUCTMAP_STATUS_HISTORY identifier (in case of 'Ingested Object' requests of 'CalcNotice')*/
        String STRUCTMAP_STATUS_HISTORY_ID = "STRUCTMAP_STATUS_HISTORY_ID";
    }

    /** Constant <code>TABLE_NAME="CMR_INDEX_REQUEST"</code>. */
    String TABLE_NAME = "CMR_INDEX_REQUEST";

    /** The where part execution status. */
    String WHERE_PART_EXECUTION_STATUS = StringHelper.format("{} = :executionStatus", Column.EXECUTION_STATUS);

    String WHERE_PART_REASON_EXECUTION_STATUS = StringHelper.format("{} IN (:executionStatuses) AND {} = :reason", Column.EXECUTION_STATUS,
            Column.REASON);

    /** The where part execution status and limit. */
    String WHERE_PART_EXECUTION_STATUS_LIMIT = StringHelper.format("{} = :executionStatus AND ROWNUM <= :limit", Column.EXECUTION_STATUS);

    /** The WHER e_ par t_2_ executio n_ status. */
    String WHERE_PART_2_EXECUTION_STATUS = StringHelper.format("{} = :executionStatus1 OR {} = :executionStatus2", Column.EXECUTION_STATUS,
            Column.EXECUTION_STATUS);

    /** The where part execution status created on. */
    String WHERE_PART_EXECUTION_STATUS_PRIORITY_CREATED_ON = StringHelper.format(
            "{} = :executionStatus AND {} >= :minimumPriority AND {} <= :maximumCreatedOn", Column.EXECUTION_STATUS, Column.PRIORITY,
            Column.CREATED_ON);

    /** The where part execution status and group by uri and max created on. */
    String WHERE_PART_EXECUTION_STATUS_AND_GROUP_BY_URI_AND_MIN_PRIORITY_AND_MAX_CREATED_ON = StringHelper.format(
            "{} = :executionStatus AND {} = :groupByUri AND {} >= :minimumPriority AND {} <= :maximumCreatedOn", Column.EXECUTION_STATUS,
            Column.GROUP_BY_URI, Column.PRIORITY, Column.CREATED_ON);

    /** The WHER e_ par t_ no t_2_ executio n_ statu s_ an d_ no t_ reques t_ type. */
    String WHERE_EXECUTION_STATUS_NOT_IN_AND_NOT_REQUEST_TYPE = StringHelper.format(
            "{} NOT IN (:executionStatus) AND {} != :requestType", Column.EXECUTION_STATUS, Column.REQUEST_TYPE);

    /** The where part execution status, maximum execution date and limit. */
    String WHERE_PART_EXECUTION_STATUS_MAX_EXECUTION_DATE_LIMIT = StringHelper.format(
            "{} = :executionStatus AND {} <= :maximumExecutionDate AND ROWNUM <= :limit", Column.EXECUTION_STATUS, Column.EXECUTION_DATE);

}
