package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.version;

import eu.europa.ec.opoce.cellar.cmr.utils.MementoUtils;
import eu.europa.ec.opoce.cellar.common.http.headers.*;
import eu.europa.ec.opoce.cellar.server.dissemination.AcceptLanguage;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.DisseminationRequest;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.IDisseminationResolver;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;

import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;

import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class TimeGateRedirectResolver extends VersionRedirectResolver {

    protected TimeGateRedirectResolver(final CellarResourceBean cellarResource, final String resourceURI, final String accept,
            final String decoding, final AcceptLanguage acceptLanguage, final Date acceptDateTime, final String rel,
            final boolean provideAlternates) {

        super(cellarResource, resourceURI, null, accept, decoding, acceptLanguage, acceptDateTime, rel, provideAlternates);
    }

    public static IDisseminationResolver get(final CellarResourceBean cellarResource, final String resourceURI,
            final DisseminationRequest disseminationRequest, final String accept, final String decoding,
            final AcceptLanguage acceptLanguage, final Date acceptDateTime, final String rel, final boolean provideAlternates) {
        return new TimeGateRedirectResolver(cellarResource, resourceURI, accept, decoding, acceptLanguage, acceptDateTime, rel,
                provideAlternates);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.AbstractResolver#doHandleDisseminationRequest()
     */
    @Override
    protected ResponseEntity<Void> doHandleDisseminationRequest() {
        // retrieve nearest location
        final String nearestMementoURI = MementoUtils.getNearestMemento(this.resourceURI, this.acceptDateTime);
        if (null == nearestMementoURI) {
            // nothing found: return 406
            return new ResponseEntity<Void>(HttpStatus.NOT_ACCEPTABLE);
        }

        // otherwise, build links and return
        final Collection<ILink> links = new LinkedList<ILink>();
        links.add(new LinkImpl(this.resourceURI, RelType.ORIGINAL_TIMEGATE));
        links.add(new LinkImpl(this.resourceURI + "?rel=timemap", RelType.TIMEMAP));

        final HttpHeadersBuilder builder = HttpHeadersBuilder.get() //
                .withContentType("text/plan; charset=utf-8") //
                .withVary(CellarHttpHeaders.ACCEPT_DATETIME) //
                .withLocation(nearestMementoURI) //
                .withLink(StringUtils.join(links, ","));

        return new ResponseEntity<Void>(builder.getHttpHeaders(), HttpStatus.FOUND);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.RedirectResolver#doHandleWorkDisseminationRequest()
     */
    @Override
    protected void doHandleWorkDisseminationRequest() {
    }

    /**
     * @see eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.version.VersionRedirectResolver#getTypeStructure()
     */
    @Override
    protected IURLTokenable getTypeStructure() {
        return RelType.ORIGINAL_TIMEGATE;
    }
}
