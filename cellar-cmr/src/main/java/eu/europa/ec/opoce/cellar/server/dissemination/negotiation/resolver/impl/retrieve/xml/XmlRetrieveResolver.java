/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.resolver.impl.xml
 *             FILE : XmlDisseminationResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 20, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.retrieve.xml;

import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.FilterVariance;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.Structure;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.IDisseminationResolver;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.retrieve.RetrieveResolver;
import eu.europa.ec.opoce.cellar.server.service.DisseminationException;

import org.springframework.http.HttpStatus;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 20, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public abstract class XmlRetrieveResolver extends RetrieveResolver {

    protected final FilterVariance filterMode;

    private final String decoding;
    protected LanguageBean decodingLanguageBean;

    public static IDisseminationResolver get(final String identifier, final Structure noticeStructure, final FilterVariance filterMode,
            final String decoding, final String eTag, final String lastModified, final boolean provideResponseBody) {
        switch (noticeStructure) {
        case BRANCH:
            return XmlBranchRetrieveResolver.get(identifier, filterMode, decoding, eTag, lastModified, provideResponseBody);
        case TREE:
            return XmlTreeRetrieveResolver.get(identifier, filterMode, decoding, eTag, lastModified, provideResponseBody);
        default:
        case OBJECT:
            return XmlObjectRetrieveResolver.get(identifier, filterMode, decoding, eTag, lastModified, provideResponseBody);
        }
    }

    protected XmlRetrieveResolver(final String identifier, final FilterVariance filterMode, final String decoding, final String eTag,
            final String lastModified, final boolean provideResponseBody) {
        super(identifier, eTag, lastModified, provideResponseBody);
        this.filterMode = filterMode;
        this.decoding = decoding;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.retrieve.RetrieveResolver#initDisseminationRequest()
     */
    @Override
    protected void initDisseminationRequest() {
        super.initDisseminationRequest();

        this.decodingLanguageBean = this.disseminationService.parseLanguageBean(this.decoding);

        if (this.decodingLanguageBean == null) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage("Invalid decoding language: '{}'").withMessageArgs(this.decoding).build();
        }
    }
}
