package eu.europa.ec.opoce.cellar.s3;

/**
 * @author ARHS Developments
 */
public class ContentStreamDataException extends RuntimeException {

    public ContentStreamDataException() {
    }

    public ContentStreamDataException(String s) {
        super(s);
    }

    public ContentStreamDataException(String s, Throwable throwable) {
        super(s, throwable);
    }
}
