/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.admin.nal
 *             FILE : NalOntoVersion.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Sep 16, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$$
 *          VERSION : $LastChangedRevision$$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.admin.nal;

import eu.europa.ec.opoce.cellar.semantic.helper.DateFormats;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.commons.lang.builder.EqualsBuilder;

import java.util.Date;

/**
 * <p>NalOntoVersion class.</p>
 *
 * @author ARHS Developments
 * @version $Revision$$
 */
public class NalOntoVersion implements Comparable<NalOntoVersion> {

    private String name;

    private String uri;

    private String versionNumber;

    private Date versionDate;

    private String externalPid;

    public String getName() {
        return this.name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getUri() {
        return this.uri;
    }

    public void setUri(final String uri) {
        this.uri = uri;
    }

    public String getFormattedVersionDate() {
        if (this.versionDate == null) {
            return "date not set";
        }

        return DateFormats.formatFullDateTime(this.versionDate);
    }

    public Date getVersionDate() {
        return this.versionDate;
    }

    public void setVersionDate(final Date versionDate) {
        this.versionDate = versionDate;
    }

    public String getVersionNumber() {
        return this.versionNumber;
    }

    public String getFormattedVersionNumber() {
        if (this.versionNumber == null) {
            return "version not set";
        }

        return this.versionNumber;
    }

    public void setVersionNumber(final String versionNumber) {
        this.versionNumber = versionNumber;
    }

    public String getExternalPid() {
        return externalPid;
    }

    public void setExternalPid(String externalPid) {
        this.externalPid = externalPid;
    }

    @Override
    public boolean equals(final Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public int compareTo(final NalOntoVersion o) {
        final CompareToBuilder compareToBuilder = new CompareToBuilder();
        compareToBuilder.append(this.name, o.getName());
        compareToBuilder.append(this.uri, o.getUri());
        compareToBuilder.append(this.versionNumber, o.getVersionNumber());
        compareToBuilder.append(this.versionDate, o.getVersionDate());
        return compareToBuilder.toComparison();
    }
}
