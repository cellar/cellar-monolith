/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.nal.decoding
 *             FILE : FallbackCalculator.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 03 02, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-03-02 07:43:26 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.nal.service;

import org.apache.jena.rdf.model.Resource;
import eu.europa.ec.opoce.cellar.common.util.Pair;
import eu.europa.ec.opoce.cellar.nal.domain.FallBackNode;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author ARHS Developments
 */
public interface FallbackCalculator {
    void reset();

    void calculate();

    Pair<List<FallBackNode>, Set<String>> calculateFallbackNodes(Resource resource);

    Map<Resource, List<FallBackNode>> getFallbackNodesPerResource();

    Map<Resource, Set<String>> getMissingLanguagesPerResource();
}
