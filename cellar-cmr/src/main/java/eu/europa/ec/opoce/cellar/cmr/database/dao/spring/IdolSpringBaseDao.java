package eu.europa.ec.opoce.cellar.cmr.database.dao.spring;

import eu.europa.ec.opoce.cellar.cmr.database.DaoObject;
import eu.europa.ec.opoce.cellar.cmr.database.dao.id.DatabaseIdService;

import java.io.Serializable;
import java.util.Collection;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * <p>Abstract IdolSpringBaseDao class.</p>
 */
public abstract class IdolSpringBaseDao<OBJ extends DaoObject, ID extends Serializable> extends SpringBaseDao<OBJ, ID> {

    /**
     * <p>setSpringDatabaseIdService.</p>
     *
     * @param databaseIdService a {@link eu.europa.ec.opoce.cellar.cmr.database.dao.id.DatabaseIdService} object.
     */
    @Autowired(required = true)
    @Qualifier(value = "idolOracleSequence")
    public void setSpringDatabaseIdService(DatabaseIdService databaseIdService) {
        super.setDatabaseIdService(databaseIdService);
    }

    /**
     * <p>setSpringDataSource.</p>
     *
     * @param dataSource a {@link javax.sql.DataSource} object.
     */
    @Autowired(required = true)
    @Qualifier("dataSourceOracleCellarIdol")
    public void setSpringDataSource(DataSource dataSource) {
        super.setDataSource(dataSource);
    }

    /**
     * <p>Constructor for IdolSpringBaseDao.</p>
     *
     * @param tablename   a {@link java.lang.String} object.
     * @param columnNames a {@link java.util.Collection} object.
     */
    protected IdolSpringBaseDao(String tablename, Collection<String> columnNames) {
        super(tablename, columnNames);
    }

    /**
     * <p>Constructor for IdolSpringBaseDao.</p>
     *
     * @param tablename    a {@link java.lang.String} object.
     * @param idColumnName a {@link java.lang.String} object.
     * @param columnNames  a {@link java.util.Collection} object.
     */
    protected IdolSpringBaseDao(String tablename, String idColumnName, Collection<String> columnNames) {
        super(tablename, idColumnName, columnNames);
    }
}
