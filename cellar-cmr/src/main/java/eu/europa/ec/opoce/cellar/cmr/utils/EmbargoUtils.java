package eu.europa.ec.opoce.cellar.cmr.utils;

import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.semantic.helper.DateFormats;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import org.apache.jena.rdf.model.Property;

import java.util.Date;

public final class EmbargoUtils {
    private EmbargoUtils() {}

    public static Date parseDate(final String dateString) {
        final Date date = DateFormats.parse(dateString, DateFormats.Format.FULLXMLDATETIME);

        if (date == null) {
            return DateFormats.parseXmlDateTime(dateString);
        }

        return date;
    }

    public static Property resolveEmbargoProperty(final DigitalObjectType type) {
        switch (type) {
            case WORK:
                return CellarProperty.cdm_work_embargoP;
            case EXPRESSION:
                return CellarProperty.cdm_expression_embargoP;
            case MANIFESTATION:
                return CellarProperty.cdm_manifestation_embargoP;
            case ITEM:
                return CellarProperty.cdm_item_embargoP;
            case DOSSIER:
                return CellarProperty.cdm_dossier_embargoP;
            case EVENT:
            case TOPLEVELEVENT:
                return CellarProperty.cdm_event_embargoP;
            case AGENT:
                return CellarProperty.cdm_agent_embargoP;
            default:
                return null;
        }
    }
}
