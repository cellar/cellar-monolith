package eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr;

import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CmrIndexNoticeDao;
import eu.europa.ec.opoce.cellar.cmr.database.dao.IdolExpandedNoticeDao;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Expression;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Manifestation;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Work;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.IndexNotice;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.Action;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.comparators.IndexRequestComparator;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.transformers.Expression2CmrIndexRequestTransformer;
import eu.europa.ec.opoce.cellar.common.util.MapUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.monitoring.Watch;
import eu.europa.ec.opoce.cellar.nal.service.LanguageService;
import eu.europa.ec.opoce.cellar.semantic.helper.MessageFormatter;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
public class NoticeIndexRequestHandler {

    private static final Logger LOG = LogManager.getLogger(NoticeIndexRequestHandler.class);

    @Autowired(required = true)
    private CmrIndexGenerationService cmrIndexGenerationService;

    @Autowired
    private CmrIndexNoticeDao cmrIndexNoticeDao;

    @Autowired
    private IdolExpandedNoticeDao idolExpandedNoticeDao;

    @Autowired
    private LanguageService languageService;

    /**
     * calculate notice for indexation, not expanded
     *
     * @param toRemoveIndexRequest a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest} object.
     * @return a {@link java.util.Collection} object.
     */
    public Collection<IndexNotice> calculateNotice(final CmrIndexRequest toRemoveIndexRequest) {
        final Collection<IndexNotice> indexNotices = this.cmrIndexGenerationService.compileAndSendIndexNotice(toRemoveIndexRequest);
        this.saveQuietlyInToCmrCache(indexNotices);
        return indexNotices;
    }

    /**
     * calculate expanded notice for indexation, not from cached parts
     *
     * @param toRemoveIndexRequest a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest} object.
     * @param indexNotices         a {@link java.util.Collection} object.
     */
    public Collection<IndexNotice> calculateExpanded(final CmrIndexRequest toRemoveIndexRequest, final Collection<IndexNotice> indexNotices) {
        final Collection<IndexNotice> expandedNotices = this.cmrIndexGenerationService.calculateExpandedNotices(toRemoveIndexRequest,
                indexNotices);
        this.saveQuietlyInToIdol(expandedNotices);
        return expandedNotices;
    }

    /**
     * calculate notice for indexation, not expanded
     *
     * @param calcNoticeCmrIndexRequests the list of {@link CmrIndexRequest} used to calculate
     *                                   the notice.
     * @return a {@link java.util.Collection} object.
     */
    @Watch(value = "Calculate Notice for group")
    public Collection<IndexNotice> calculateNoticeForGroup(final Collection<CmrIndexRequest> calcNoticeCmrIndexRequests) {
        return this.calculateNotice(this.optimize(calcNoticeCmrIndexRequests));
    }

    /**
     * calculate expanded notice for indexation, not from cached parts
     *
     * @param calcExpandedCmrIndexRequets a {@link java.util.Collection} object.
     * @param indexNotices        a {@link java.util.Collection} object.
     */
    public Collection<IndexNotice> calculateExpandedForGroup(final Collection<CmrIndexRequest> calcExpandedCmrIndexRequets,
                                          final Collection<IndexNotice> indexNotices) {
        return this.calculateExpanded(this.optimize(calcExpandedCmrIndexRequets), indexNotices);
    }

    /**
     * calculate notice for indexation, not expanded
     *
     * @param calcNoticeCmrIndexRequets a {@link java.util.Collection} object.
     * @param work                a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Work} object.
     * @return a {@link java.util.Collection} object.
     */
    @Watch(value = "Calculate Notice for group")
    public Collection<IndexNotice> calculateNoticeForGroup(final Collection<CmrIndexRequest> calcNoticeCmrIndexRequets, final Work work) {
        final Set<CmrIndexRequest> optimizedRequests = this.optimizeRequests(work, calcNoticeCmrIndexRequets);
        if (calcNoticeCmrIndexRequets.isEmpty() && optimizedRequests.isEmpty()) {
            return Collections.emptySet();
        } else {
            final Collection<CmrIndexRequest> requests = optimizedRequests.isEmpty() ? calcNoticeCmrIndexRequets : optimizedRequests;
            final boolean addMissingLanguages = this.isAddMissingLanguages(calcNoticeCmrIndexRequets);
            final Collection<IndexNotice> indexNotices = this.cmrIndexGenerationService.compileIndexNotice(work, requests,
                    addMissingLanguages);
            this.saveQuietlyInToCmrCache(indexNotices);
            return indexNotices;
        }
    }

    /**
     * calculate expanded notice for indexation, not from cached parts
     *
     * @param calcExpandedCmrIndexRequets a {@link java.util.Collection} object.
     * @param work                a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Work} object.
     * @param indexNotices        a {@link java.util.Collection} object.
     *
     * @return the collection of the expanded notices
     */
    public Collection<IndexNotice> calculateExpandedForGroup(final Collection<CmrIndexRequest> calcExpandedCmrIndexRequets, final Work work,
                                          final Collection<IndexNotice> indexNotices) {
        final Set<CmrIndexRequest> optimizedRequests = this.optimizeRequests(work, calcExpandedCmrIndexRequets);
        final Collection<CmrIndexRequest> requests = optimizedRequests.isEmpty() ? calcExpandedCmrIndexRequets : optimizedRequests;
        final boolean addMissingLanguages = this.isAddMissingLanguages(calcExpandedCmrIndexRequets);
        final Collection<IndexNotice> expandedNotices = this.cmrIndexGenerationService.calculateExpandedNotices(work, requests,
                addMissingLanguages, indexNotices);
        this.saveQuietlyInToIdol(expandedNotices);
        return expandedNotices;
    }

    /**
     * indication if the missing languages also need to be indexed
     *
     * @param requests a {@link java.util.Collection} object.
     * @return a boolean.
     */
    private boolean isAddMissingLanguages(final Collection<CmrIndexRequest> requests) {
        for (final CmrIndexRequest indexRequest : requests) {
            if (indexRequest.getObjectType() == DigitalObjectType.WORK) {
                return true;
            }
        }
        return false;
    }

    /**
     * calculate set of requests that needs to be handled
     *
     * @param work                a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Work} object.
     * @param indexRequests1      a {@link java.util.Collection} object.
     * @return a {@link java.util.Set} object.
     */
    private Set<CmrIndexRequest> optimizeRequests(final Work work, final Collection<CmrIndexRequest> indexRequests1) {
        final Map<String, Set<CmrIndexRequest>> smallGroupRequest = this.calculateExpressionsToIndex(work, indexRequests1);
        final Set<CmrIndexRequest> optimizedRequests = new HashSet<CmrIndexRequest>();
        final Collection<Set<CmrIndexRequest>> values = smallGroupRequest.values();
        for (final Set<CmrIndexRequest> indexRequests : values) {
            final CmrIndexRequest optimize = this.optimize(indexRequests);
            optimizedRequests.add(optimize);
        }
        return optimizedRequests;
    }

    /**
     * calculate the expressions that need to be (re-)indexed
     *
     * @param work          a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Work} object.
     * @param indexRequests a {@link java.util.Collection} object.
     * @return a {@link java.util.Map} object.
     */
    private Map<String, Set<CmrIndexRequest>> calculateExpressionsToIndex(final Work work,
                                                                          final Collection<CmrIndexRequest> indexRequests) {
        final Map<String, Set<CmrIndexRequest>> smallGroupRequest = new HashMap<String, Set<CmrIndexRequest>>();
        for (final CmrIndexRequest request : indexRequests) {
            final DigitalObjectType objectType = request.getObjectType();
            if (objectType == DigitalObjectType.WORK) {
                final Collection<Expression> expressions = work.getChildren();
                languageService.filterNonEuLanguagesExpression(expressions);
                final Expression2CmrIndexRequestTransformer transformer = new Expression2CmrIndexRequestTransformer(request);
                final Collection<CmrIndexRequest> collect = CollectionUtils.collect(expressions, transformer);
                for (final CmrIndexRequest indexRequest : collect) {
                    final String objectUri = indexRequest.getObjectUri();
                    MapUtils.addValueToMappedSet(smallGroupRequest, objectUri, indexRequest);
                }
            } else if (objectType == DigitalObjectType.MANIFESTATION) {
                final CmrIndexRequest indexRequest = this.getExpressionIndexRequest(work, request);
                final String objectUri = indexRequest.getObjectUri();
                MapUtils.addValueToMappedSet(smallGroupRequest, objectUri, indexRequest);
            } else {
                final String objectUri = request.getObjectUri();
                MapUtils.addValueToMappedSet(smallGroupRequest, objectUri, request);
            }
        }
        return smallGroupRequest;
    }

    /**
     * get CmrIndexRequest starting from the first expression defined in the given work
     *
     * @param work    a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Work} object.
     * @param request a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest} object.
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest} object.
     */
    private CmrIndexRequest getExpressionIndexRequest(final Work work, final CmrIndexRequest request) {
        final Collection<Expression> expressions = work.getChildren();
        languageService.filterNonEuLanguagesExpression(expressions);
        for (final Expression expression : expressions) {
            final Collection<Manifestation> manifestations = expression.getChildren();
            for (final Manifestation manifestation : manifestations) {
                final String objectUri = request.getObjectUri();
                final ContentIdentifier cellarId = manifestation.getCellarId();
                final String uri = cellarId.getUri();
                if (uri.equals(objectUri)) {
                    return new Expression2CmrIndexRequestTransformer(request).transform(expression);
                }
            }
        }
        throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.INDEXING_REQUEST_SERVICE_ERROR).build();
    }

    /**
     * merge multiple indexrequests to one indexrequest that needs to be handled
     *
     * @param indexRequests a {@link java.util.Collection} object.
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest} object.
     */
    @Nullable
    private CmrIndexRequest optimize(final Collection<CmrIndexRequest> indexRequests) {
        //we receive as input a collection of requests for the same digital object
        //in this method we try to return only one request
        //the requests are sorted by creation date ASC
        CmrIndexRequest result = null;
        final List<CmrIndexRequest> sortIndexRequests = this.sortIndexRequests(indexRequests);
        for (final CmrIndexRequest indexRequest : sortIndexRequests) {
            if (result == null) {
                result = indexRequest;
            } else {
                final Action savedAction = result.getAction();
                final Action currentAction = indexRequest.getAction();
                final boolean savedActionCreate = savedAction == CmrIndexRequest.Action.Create;
                final boolean currentActionIsRemove = currentAction == CmrIndexRequest.Action.Remove;
                final boolean savedActionIsUpdate = savedAction == CmrIndexRequest.Action.Update;
                final boolean savedActionIsUpdateId = savedAction == CmrIndexRequest.Action.UpdateId;
                final boolean currentActionIsUpdate = currentAction != CmrIndexRequest.Action.Update;
                final boolean savedActionIsRemove = savedAction == CmrIndexRequest.Action.Remove;
                //remove > create
                //if we have a remove the following request (if any) can be an create at most
                //if we have an update the following request (if any) can be an delete at most
                // update > updateId
                if ((savedActionCreate && currentActionIsRemove) || savedActionIsUpdate || (savedActionIsUpdateId && currentActionIsUpdate)
                        || savedActionIsRemove) {
                    result = indexRequest;
                }
            }
        }
        return result;
    }

    /**
     * <p>sortIndexRequests.</p>
     *
     * @param requests a {@link java.util.Collection} object.
     * @return a {@link java.util.List} object.
     */
    private List<CmrIndexRequest> sortIndexRequests(final Collection<CmrIndexRequest> requests) {
        final List<CmrIndexRequest> listedRequests = new ArrayList<CmrIndexRequest>(requests);
        Collections.sort(listedRequests, IndexRequestComparator.instance);
        return listedRequests;
    }

    /**
     * save expanded idol-indexing notices in idol
     *
     * @param expandedNotices a {@link java.lang.Iterable} object.
     */
    private void saveQuietlyInToIdol(final Iterable<IndexNotice> expandedNotices) {
        for (final IndexNotice indexNotice : expandedNotices) {
            try {
                this.idolExpandedNoticeDao.saveOrUpdateObject(indexNotice);
            } catch (final RuntimeException e) {
                LOG.error(MessageFormatter.format("Unexpected exception while saving {} in idol: {}", indexNotice, e.getMessage()), e);
                throw e;
            }
        }
    }

    /**
     * save idol-indexing notices in cache
     *
     * @param indexNotices a {@link java.lang.Iterable} object.
     */
    private void saveQuietlyInToCmrCache(final Iterable<IndexNotice> indexNotices) {
        for (final IndexNotice indexNotice : indexNotices) {
            try {
                this.cmrIndexNoticeDao.saveOrUpdateObject(indexNotice);
            } catch (final RuntimeException e) {
                LOG.error(MessageFormatter.format("Unexpected exception while saving {} in index-notice-cache: {}", indexNotice,
                        e.getMessage()), e);
                throw e;
            }
        }
    }
}
