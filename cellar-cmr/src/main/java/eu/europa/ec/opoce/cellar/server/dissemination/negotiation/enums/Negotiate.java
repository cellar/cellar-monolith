/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums
 *             FILE : Negotiate.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 22, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Apr 22, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public enum Negotiate {
    VLIST("vlist", true), //
    OTHER(false);

    private final String value;
    private final boolean alternatesEnabled;

    private static final Map<String, Negotiate> NEGOTIATE_PER_VALUE;

    static {
        NEGOTIATE_PER_VALUE = new HashMap<String, Negotiate>();

        for (final Negotiate n : Negotiate.values()) {
            if (StringUtils.isNotEmpty(n.value)) {
                NEGOTIATE_PER_VALUE.put(n.value, n);
            }
        }
    }

    private Negotiate(final boolean alternatesEnabled) {
        this(null, alternatesEnabled);
    }

    private Negotiate(final String value, final boolean alternatesEnabled) {
        this.value = value;
        this.alternatesEnabled = alternatesEnabled;
    }

    public static Negotiate negotiateValueOf(final String negotiate) {
        final Negotiate n = NEGOTIATE_PER_VALUE.get(negotiate.toLowerCase());

        if (n != null) {
            return n;
        }

        return Negotiate.OTHER;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return this.value;
    }

    /**
     * @return the alternatesEnabled
     */
    public boolean isAlternatesEnabled() {
        return this.alternatesEnabled;
    }
}
