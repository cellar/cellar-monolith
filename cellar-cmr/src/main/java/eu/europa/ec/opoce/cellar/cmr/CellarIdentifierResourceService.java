package eu.europa.ec.opoce.cellar.cmr;

import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement;
import eu.europa.ec.opoce.cellar.cmr.model.SameAsModelUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;
import org.apache.jena.rdf.model.Model;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

@Service
/**
 * <p>CellarIdentifierResourceService class.</p>
 */
public class CellarIdentifierResourceService {

    /**
     * Constant <code>log</code>
     */
    private static final Logger LOG = LogManager.getLogger(CellarIdentifierResourceService.class);

    @Autowired
    private CellarResourceDao cellarResourceDao;

    @Autowired
    @Qualifier("pidManagerService")
    private IdentifierService identifierService;

    /**
     * <p>getSameAsModel.</p>
     *
     * @param urisOrIds list of uris and id's that need to be put in the model
     * @return model with all given uri's and id's and all there sameAs-relations
     */
    public Model getSameAsModel(final Collection<String> urisOrIds) {
        final HashSet<Identifier> identifiers = new HashSet<Identifier>();
        for (final String uri : urisOrIds) {
            try {
                identifiers.add(this.identifierService.getIdentifier(this.identifierService.getPrefixed(uri)));
            } catch (final IllegalArgumentException ignore) {
                //the identifier is not yet used in cellar, this is no problem
                LOG.warn("the identifier {} is not specified yet", uri);
            }
        }

        return SameAsModelUtils.getSameAsModel(identifiers);
    }

    /**
     * <p>getElement.</p>
     *
     * @param resource cellarResourceBean that needs to be converted into a metsElement
     * @return metsElement with data of the cellarResourceBean
     */
    public MetsElement getElement(final CellarResourceBean resource) {
        final MetsElement metsElement = new MetsElement(resource.getCellarType());
        metsElement.setCellarId(resource.getCellarId());
        metsElement.setLastModificationDate(resource.getLastModificationDate());
        metsElement.setVersions(resource.getVersions());
        metsElement.setEmbeddedNoticeCreationDate(resource.getEmbeddedNoticeCreationDate());
        return metsElement;
    }

    /**
     * <p>getElement.</p>
     *
     * @param urisOrId uri or id that data is needed about
     * @return metsElement with data about the id or uri
     */
    public MetsElement getElement(final String urisOrId) {
        final CellarResource resource = this.cellarResourceDao.findCellarId(this.identifierService.getCellarPrefixed(urisOrId));
        if (resource == null) {
            return null;
        }

        final MetsElement metsElement = new MetsElement(resource.getCellarType());
        metsElement.setCellarId(resource.getCellarId());
        metsElement.setLastModificationDate(resource.getLastModificationDate());
        return metsElement;
    }

    /**
     * <p>getDigitalObjectType.</p>
     *
     * @param urisOrId uri or id that data is needed about
     * @return the digitalObjectType
     */
    public DigitalObjectType getDigitalObjectType(final String urisOrId) {
        final String cellarPrefix = resolveUriToCellarIdentifier(urisOrId);
        if (cellarPrefix == null) {
            return null;
        }
        return retrieveCellarResourceAndExtractType(cellarPrefix);
    }
    
    /**
     * Resolves the provided URI to its corresponding Cellar identifier,
     * if applicable.
     * @param uri the URI to resolve.
     * @return the corresponding Cellar identifier if applicable,
     * {@code null} otherwise.
     */
    public String resolveUriToCellarIdentifier(final String uri) {
    	return this.identifierService.getCellarPrefixedOrNull(uri);
    }
    
    /**
     * Retrieves the Cellar resource corresponding to the provided Cellar identifier
     * and extracts its {@code DigitalObjectType}.
     * @param cellarId the Cellar identifier to look for.
     * @return the corresponding {@code DigitalObjectType}.
     */
    public DigitalObjectType retrieveCellarResourceAndExtractType(final String cellarId) {
    	CellarResource resource = this.cellarResourceDao.findCellarId(cellarId);
    	return (resource != null) ? resource.getCellarType() : null;
    }

    /**
     * This method returns the total number of works in cellar
     *
     * @return long containing the total number of works
     */
    public long getTotalNumberOfWorks() {
        return this.cellarResourceDao.getTotalNumberOfWorks();
    }

    /**
     * This method returns the total number of dossiers in cellar
     *
     * @return long containing the total number of dossiers
     */
    public long getTotalNumberOfDossiers() {
        return this.cellarResourceDao.getTotalNumberOfDossiers();
    }

    /**
     * This method returns the total number of events in cellar
     *
     * @return long containing the total number of events
     */
    public long getTotalNumberOfEvents() {
        return this.cellarResourceDao.getTotalNumberOfEvents();
    }

    /**
     * This method returns the total number of agents in cellar
     *
     * @return long containing the total number of agents
     */
    public long getTotalNumberOfAgents() {
        return this.cellarResourceDao.getTotalNumberOfAgents();
    }

    /**
     * This method returns the total number of top level events in cellar
     *
     * @return long containing the total number of top level events
     */
    public long getTotalNumberOfTopLEvelEvents() {
        return this.cellarResourceDao.getTotalNumberOfTopLevelEvents();
    }

    /**
     * This method returns a map containing the total number of expressions per language.
     * The key is the language, the value is the total number of expressions for that language
     * eg. "eng" => 20
     *
     * @return a map containing the total number of expressions per language
     */
    public Map<String, Integer> getExpressionsPerLanguage() {
        return this.cellarResourceDao.getExpressionsPerLanguage();
    }

    /**
     * This method returns a map containing the total number of manifestations per type.
     * The key is the type, the value is the total number of manifestations for that type.
     * eg. "pdf1x" => 20
     *
     * @return a map containing the total number of manifestations per type
     */
    public Map<String, Integer> getManifestationsPerType() {
        return this.cellarResourceDao.getManifestationsPerType();
    }

    /**
     * This method returns a Map containing manifestation types.
     * Each manifestation type has a map holding the total number of languages,
     * for that specific manifestation type
     * <p/>
     * eg. 'pdf1x' => ('eng' => 20)
     *
     * @return a map containing the manifestations per type per language
     */
    public Map<String, Map<String, Integer>> getManifestationsPerTypePerLanguage() {
        return this.cellarResourceDao.getManifestationsPerTypePerLanguage();
    }

    /**
     * This method returns a list of all expression languages
     *
     * @return a list of languages
     */
    public List<String> getLanguagesForTable() {
        final Map<String, Map<String, Integer>> results = this.getManifestationsPerTypePerLanguage();

        int size = 0;
        String languageType = "";

        for (final Entry<String, Map<String, Integer>> entry : results.entrySet()) {
            final String type = entry.getKey();
            final int tmpSize = entry.getValue().size();
            if (tmpSize > size) {
                size = tmpSize;
                languageType = type;
            }
        }

        final List<String> languages = new ArrayList<String>();
        if (null != results.get(languageType)) {
            languages.addAll(results.get(languageType).keySet());
        }
        return languages;
    }

}
