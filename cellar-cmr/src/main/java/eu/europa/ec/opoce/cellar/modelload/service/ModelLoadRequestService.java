/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.modelload.service.impl
 *             FILE : IModelLoadRequestScheduler.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Mar 18, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.modelload.service;

import eu.europa.ec.opoce.cellar.modelload.domain.ModelLoadRequest;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Mar 18, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface ModelLoadRequestService {

    Collection<ModelLoadRequest> findAll();

    Optional<ModelLoadRequest> findById(final String id);

    Collection<ModelLoadRequest> find(final String modelURI, final String executionStatus);

    void restart(final String id, final Date activationDate);

    void delete(final String id);

    void update(final String id, final String modelURI, final String pid, final String executionStatus,
                       final Date activationDate);
}
