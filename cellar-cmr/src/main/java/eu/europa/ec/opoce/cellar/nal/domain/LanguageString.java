package eu.europa.ec.opoce.cellar.nal.domain;

import java.util.Objects;

/**
 * <p>LanguageString class.</p>
 */
public class LanguageString {

    private String language;
    private String string;

    /**
     * <p>Constructor for LanguageString.</p>
     *
     * @param language a {@link java.lang.String} object.
     * @param string   a {@link java.lang.String} object.
     */
    public LanguageString(String language, String string) {
        this.language = language;
        this.string = string;
    }

    /**
     * <p>Getter for the field <code>language</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getLanguage() {
        return language;
    }

    /**
     * <p>Getter for the field <code>string</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getString() {
        return string;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LanguageString)) return false;
        LanguageString that = (LanguageString) o;
        return Objects.equals(getLanguage(), that.getLanguage()) &&
                Objects.equals(getString(), that.getString());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getLanguage(), getString());
    }

    @Override
    public String toString() {
        return "LanguageString{" +
                "language='" + language + '\'' +
                ", string='" + string + '\'' +
                '}';
    }
}
