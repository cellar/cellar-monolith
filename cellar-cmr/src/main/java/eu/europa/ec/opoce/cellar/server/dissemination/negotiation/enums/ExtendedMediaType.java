package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.springframework.http.MediaType;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

/**
 * An extension of the {@code MediaType} class, adding Memento-specific values
 * Variable names refer to http://mementoweb.org/guide/rfc 
 * @author ARHS Developments
 *
 */
public class ExtendedMediaType extends MediaType {

    private static final long serialVersionUID = 7192915845106591433L;

    /**
     * Public constant media type that includes all media ranges (i.e. <code>&#42;/&#42;</code>).
     */
    public static final MediaType APPLICATION_LINK_FORMAT;

    /**
     * A String equivalent of {@link MediaType#ALL}.
     */
    public static final String APPLICATION_LINK_FORMAT_VALUE = "application/link-format";

    public ExtendedMediaType(MediaType other, Map<String, String> parameters) {
        super(other, parameters);
    }

    static {
        APPLICATION_LINK_FORMAT = MediaType.valueOf(APPLICATION_LINK_FORMAT_VALUE);
    }

    /**
     * Parse the given, comma-separated string into a list of {@code ExtendedMediaType} objects.
     * <p>This method can be used to parse an Accept or Content-Type header.
     * @param mediaTypes the string to parse
     * @return the list of media types
     * @throws IllegalArgumentException if the string cannot be parsed
     */
    public static List<ExtendedMediaType> parseExtendedMediaTypes(String extendedMediaTypes) {
        if (!StringUtils.hasLength(extendedMediaTypes)) {
            return Collections.emptyList();
        }
        String[] tokens = extendedMediaTypes.split(",\\s*");
        List<ExtendedMediaType> result = new ArrayList<ExtendedMediaType>(tokens.length);
        for (String token : tokens) {
            result.add((ExtendedMediaType) parseMediaType(token));
        }
        return result;
    }

    public static void sortExtendedMediaTypeByQualityValue(List<ExtendedMediaType> extendedMediaTypes) {
        Assert.notNull(extendedMediaTypes, "'extendedMediaTypes' must not be null");
        if (extendedMediaTypes.size() > 1) {
            Collections.sort(extendedMediaTypes, QUALITY_VALUE_COMPARATOR);
        }
    }

    /**
     * Comparator used by {@link #sortByQualityValue(List)}.
     */
    public static final Comparator<ExtendedMediaType> QUALITY_VALUE_COMPARATOR = new Comparator<ExtendedMediaType>() {

        public int compare(ExtendedMediaType mediaType1, ExtendedMediaType mediaType2) {
            double quality1 = mediaType1.getQualityValue();
            double quality2 = mediaType2.getQualityValue();
            int qualityComparison = Double.compare(quality2, quality1);
            if (qualityComparison != 0) {
                return qualityComparison; // audio/*;q=0.7 < audio/*;q=0.3
            } else if (mediaType1.isWildcardType() && !mediaType2.isWildcardType()) { // */* < audio/*
                return 1;
            } else if (mediaType2.isWildcardType() && !mediaType1.isWildcardType()) { // audio/* > */*
                return -1;
            } else if (!mediaType1.getType().equals(mediaType2.getType())) { // audio/basic == text/html
                return 0;
            } else { // mediaType1.getType().equals(mediaType2.getType())
                if (mediaType1.isWildcardSubtype() && !mediaType2.isWildcardSubtype()) { // audio/* < audio/basic
                    return 1;
                } else if (mediaType2.isWildcardSubtype() && !mediaType1.isWildcardSubtype()) { // audio/basic > audio/*
                    return -1;
                } else if (!mediaType1.getSubtype().equals(mediaType2.getSubtype())) { // audio/basic == audio/wave
                    return 0;
                } else {
                    int paramsSize1 = mediaType1.getParameters().size();
                    int paramsSize2 = mediaType2.getParameters().size();
                    return (paramsSize2 < paramsSize1 ? -1 : (paramsSize2 == paramsSize1 ? 0 : 1)); // audio/basic;level=1 < audio/basic
                }
            }
        }
    };

}
