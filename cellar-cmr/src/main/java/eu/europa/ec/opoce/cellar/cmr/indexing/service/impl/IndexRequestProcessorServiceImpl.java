/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.indexing.service.impl
 *             FILE : IndexRequestProcessingServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 31, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.indexing.service.impl;

import eu.europa.ec.opoce.cellar.ccr.service.database.DefaultTransactionService;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CmrIndexRequestDao;
import eu.europa.ec.opoce.cellar.cmr.indexing.service.IIndexRequestProcessorService;
import eu.europa.ec.opoce.cellar.cmr.indexing.service.IndexRequestManagerService;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestBatch;
import eu.europa.ec.opoce.cellar.common.concurrent.FutureCallback;
import eu.europa.ec.opoce.cellar.common.concurrent.ListenableFuture;
import eu.europa.ec.opoce.cellar.common.concurrent.ListeningExecutorService;
import eu.europa.ec.opoce.cellar.common.concurrent.ListeningPriorityThreadPoolExecutor;
import eu.europa.ec.opoce.cellar.common.concurrent.NamedThreadFactory;
import eu.europa.ec.opoce.cellar.logging.LogContext;
import eu.europa.ec.opoce.cellar.monitoring.Watch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Phaser;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import static eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration.INDEXATION;
import static eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.ExecutionStatus.Done;
import static eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.ExecutionStatus.Error;
import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.CMR_INDEXING;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 31, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service("indexRequestProcessingService")
public class IndexRequestProcessorServiceImpl extends DefaultTransactionService implements IIndexRequestProcessorService {

    private static final Logger LOG = LogManager.getLogger(IndexRequestProcessorServiceImpl.class);

    private final ICellarConfiguration cellarConfiguration;

    private final IndexRequestManagerService indexRequestManagerService;

    private final IndexingProcessor indexingProcessor;

    private final CmrIndexRequestDao cmrIndexRequestDao;

    private final ListeningPriorityThreadPoolExecutor indexingExecutor;

    private final AtomicBoolean running = new AtomicBoolean(true);


    @Autowired
    public IndexRequestProcessorServiceImpl(@Qualifier("cellarConfiguration") ICellarConfiguration cellarConfiguration,
                                            IndexRequestManagerService indexRequestManagerService, IndexingProcessor indexingProcessor,
                                            CmrIndexRequestDao cmrIndexRequestDao){

        this.indexingExecutor = new ListeningPriorityThreadPoolExecutor(cellarConfiguration.getCellarServiceIndexingPoolThreads(),
                cellarConfiguration.getCellarServiceIndexingPoolThreads(), 0L, TimeUnit.MILLISECONDS, new PriorityBlockingQueue<>(),
                new NamedThreadFactory("indexing-request"));
        this.cellarConfiguration = cellarConfiguration;
        this.indexRequestManagerService = indexRequestManagerService;
        this.indexingProcessor = indexingProcessor;
        this.cmrIndexRequestDao = cmrIndexRequestDao;
    }

    @Override
    public boolean isIndexingRunning() {
        return running.get() && indexingExecutor.isRunningTasks();
    }

    private void setPoolSize(int newSize) {
        int oldSize = indexingExecutor.getMaximumPoolSize();
        try {
            indexingExecutor.setCorePoolSize(newSize);
            indexingExecutor.setMaximumPoolSize(newSize);
            LOG.info("Update indexing-request thread pool size: {} -> {}", oldSize, newSize);
        } catch (IllegalArgumentException e) {
            LOG.error("Impossible to change the size of the thread pool", e);
        }
    }

    /**
     * {@inheritDoc}
     * <p>
     * This process will execute the following steps:
     * <ul>
     * <li>Remove useless index entries</li>
     * <li>Generate index requests corresponding inverses</li>
     * <li>Generate embedded index requests</li>
     * <li>Generate the index notice</li>
     * </ul>
     * he entire indexation process is dominated by the performance of the notice generation.
     * <p>
     * Task completion: A callback is registered when the tasks previously described are
     * submitted to the thread pool. Waiting for all the parts of the computation
     * have done their work is generally achieved by using a {@link CountDownLatch} or
     * a {@link Phaser} but in that case, we don't want to block the caller thread by
     * using {@link CountDownLatch#await()} or {@link Phaser#arriveAndAwaitAdvance()}.
     * The callback is executed when the task is completed.
     *
     * @param indexRequestBatch the index request batch
     * @see FutureCallback
     * @see ListeningExecutorService
     */
    @Watch("indexing")
    @Override
    public void submit(final CmrIndexRequestBatch indexRequestBatch) {
        if (running.get()) {
            LOG.info("Index request batch {}", indexRequestBatch);
            final long start = System.currentTimeMillis();
            final ListenableFuture<Long> future = indexingExecutor.submit(createIndexingTask(indexRequestBatch));
            LOG.debug(INDEXATION, "Register callback group [{}] ({})", indexRequestBatch.getGroupByUri(), indexRequestBatch.getPriority());
            future.addCallback(createCallback(indexRequestBatch, start));
        } else {
            LOG.info("The indexing process is currently in pause.");
        }
    }

    @Override
    public void pause() {
        if (running.compareAndSet(true, false)) {
            LOG.info("Pause requested!");
            int delayedRequests = indexingExecutor.getQueue().size();
            indexRequestManagerService.resetPendingRequests();
            indexingExecutor.getQueue().clear();
            LOG.info("IndexingService pause -> delayed {} index request(s)", delayedRequests);
        }
    }

    @Override
    public void resume() {
        if (running.compareAndSet(false, true)) {
            LOG.info("IndexingService resumed.");
        }
    }

    /**
     * @param request the current index request
     * @return the {@link IndexRequestRunnable}
     */
    private IndexRequestRunnable createIndexingTask(final CmrIndexRequestBatch request) {
        return new IndexRequestRunnable(indexingProcessor, request, cmrIndexRequestDao);
    }

    private FutureCallback<Long> createCallback(final CmrIndexRequestBatch batch, final long start) {
        return new IndexingCallback(batch, start);
    }

    @LogContext
    class IndexingCallback implements FutureCallback<Long> {

        private final CmrIndexRequestBatch batch;
        private final long start;

        IndexingCallback(final CmrIndexRequestBatch batch, final long start) {
            this.batch = batch;
            this.start = start;
        }

        @Override
        @LogContext(CMR_INDEXING)
        public void onSuccess(Long waitTime) {
            try {
                LOG.debug("Callback [{}], {} requests", batch.getGroupByUri(), batch.getCmrIndexRequests().size());
                indexRequestManagerService.finalizeCmrIndexRequestBatch(batch);
                indexRequestManagerService.updateStructMapIndxStatusAndRespectiveDatetime(batch.getCmrIndexRequests(), Done);
            } finally {
                complete(true, waitTime);
            }

        }

        @Override
        @LogContext(CMR_INDEXING)
        public void onFailure(Throwable error) {
            try {
                final String cause = UUID.randomUUID().toString();
                LOG.error("[" + cause + "] Failed to process " + batch.getGroupByUri(), error);
                indexRequestManagerService.finalizeCmrIndexRequestBatch(batch, Error, cause);
                indexRequestManagerService.updateStructMapIndxStatusAndRespectiveDatetime(batch.getCmrIndexRequests(), Error);
            } finally {
                complete(false, -1);
            }
        }

        private void complete(boolean success, long waitTime) {
            LOG.info("{} ({}) done with status [{}] in {} ms ({} ms waiting in PriorityQueue)", batch.getGroupByUri(), batch.getPriority(),
                    success ? "SUCCESS" : "FAILURE",
                    System.currentTimeMillis() - start,
                    waitTime);
        }
    }

    @Override
    public void refreshPoolSize() {
        setPoolSize(cellarConfiguration.getCellarServiceIndexingPoolThreads());
    }

    @PreDestroy
    public void preDestroy() {
        LOG.info("Shutting down the indexation thread pool executor");
        this.indexingExecutor.shutdown();
    }

}
