/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.database.impl
 *             FILE : CMRMetadataJenaGateway.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Aug 6, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.database.impl;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.ResultSet;
import eu.europa.ec.opoce.cellar.database.CmrTableName;
import eu.europa.ec.opoce.cellar.database.IRDFStoreJenaGateway;
import eu.europa.ec.opoce.cellar.database.OracleFactory;
import eu.europa.ec.opoce.cellar.jena.oracle.OraCallbacks;
import eu.europa.ec.opoce.cellar.jena.oracle.result.resolver.IJenaResultSetResolver;
import eu.europa.ec.opoce.cellar.semantic.jena.template.JenaQueryUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static eu.europa.ec.opoce.cellar.jena.oracle.OraTemplate.execute;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Aug 6, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service("cmrMetadataJenaGateway")
public class CMRMetadataJenaGateway implements IRDFStoreJenaGateway {

    @Autowired
    private OracleFactory oracleFactory;

    public <T> T executeSelectQuery(final Query query, final CmrTableName table, final IJenaResultSetResolver<T> resolver) {
        return execute(this.oracleFactory.getOracle(), OraCallbacks.modelCallback(table.name(), model -> {
            QueryExecution queryExecution = null;
            try {
                queryExecution = QueryExecutionFactory.create(query, model);

                final ResultSet resultSet = queryExecution.execSelect();
                return resolver.resolve(resultSet);
            } finally {
                JenaQueryUtils.closeQuietly(queryExecution, model);
            }
        }));
    }
}
