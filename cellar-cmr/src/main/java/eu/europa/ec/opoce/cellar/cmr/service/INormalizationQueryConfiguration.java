/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.service
 *             FILE : INormalizationQueryConfiguration.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Sep 30, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.service;

import java.util.Collection;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Sep 30, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface INormalizationQueryConfiguration {

    String getQueryForObjectsWithGraphNormalization(final String normalizedUri, final Collection<String> nonNormalizedUris);

    String getQueryForSubjectsWithGraphNormalization(final String normalizedUri, final Collection<String> nonNormalizedUris);

    String getQueryForObjectsWithoutGraphNormalization(final String normalizedUri, final Collection<String> nonNormalizedUris);

    String getQueryForSubjectsWithoutGraphNormalization(final String normalizedUri, final Collection<String> nonNormalizedUris);
}
