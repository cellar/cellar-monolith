package eu.europa.ec.opoce.cellar.cmr.transformers;

import eu.europa.ec.opoce.cellar.LanguageIsoCode;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;

import org.apache.commons.collections15.Transformer;

/**
 * <p>LanguageBean2IsoCode class.</p>
 */
public class LanguageBean2IsoCode implements Transformer<LanguageBean, LanguageIsoCode> {

    /**
     * Constant <code>instance</code>
     */
    public static Transformer<LanguageBean, LanguageIsoCode> instance = new LanguageBean2IsoCode();

    /**
     * {@inheritDoc}
     */
    @Override
    public LanguageIsoCode transform(LanguageBean input) {
        return input.getIsoCode();
    }
}
