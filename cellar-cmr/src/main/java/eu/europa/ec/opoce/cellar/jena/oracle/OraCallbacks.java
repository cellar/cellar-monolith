package eu.europa.ec.opoce.cellar.jena.oracle;

import org.apache.jena.rdf.model.Model;
import eu.europa.ec.opoce.cellar.common.closure.PClosure;
import eu.europa.ec.opoce.cellar.common.closure.RPClosure;
import oracle.spatial.rdf.client.jena.Attachment;
import oracle.spatial.rdf.client.jena.Oracle;

/**
 * <p>Abstract OraCallbacks class.</p>
 */
public abstract class OraCallbacks {

    /**
     * <p>modelCallback.</p>
     *
     * @param model   a {@link java.lang.String} object.
     * @param actions a {@link RPClosure} object.
     * @param <R>     a R object.
     * @return a {@link RPClosure} object.
     */
    public static <R> RPClosure<R, Oracle> modelCallback(final String model, final RPClosure<R, Model> actions) {
        return modelCallback(model, actions, false);
    }

    /**
     * <p>modelCallback.</p>
     *
     * @param model      a {@link java.lang.String} object.
     * @param attachment a {@link oracle.spatial.rdf.client.jena.Attachment} object.
     * @param actions    a {@link RPClosure} object.
     * @return a {@link RPClosure} object.
     */
    public static <R> RPClosure<R, Oracle> modelCallback(final String model, final Attachment attachment,
                                                         final RPClosure<R, Model> actions) {
        return modelCallback(model, attachment, actions, false);
    }

    /**
     * <p>modelCallback.</p>
     *
     * @param model         a {@link java.lang.String} object.
     * @param actions       a {@link RPClosure} object.
     * @param transactional a boolean.
     * @return a {@link RPClosure} object.
     */
    public static <R> RPClosure<R, Oracle> modelCallback(final String model, final RPClosure<R, Model> actions,
                                                         boolean transactional) {
        return transactional ?
                oracle -> OraTemplate.doInTransaction(oracle, model, actions) :
                oracle -> OraTemplate.execute(oracle, model, actions);
    }

    /**
     * <p>modelCallback.</p>
     *
     * @param model         a {@link java.lang.String} object.
     * @param attachment    a {@link oracle.spatial.rdf.client.jena.Attachment} object.
     * @param actions       a {@link RPClosure} object.
     * @param transactional a boolean.
     * @return a {@link RPClosure} object.
     */
    public static <R> RPClosure<R, Oracle> modelCallback(final String model, final Attachment attachment,
                                                         final RPClosure<R, Model> actions, boolean transactional) {
        return transactional ?
                oracle -> OraTemplate.doInTransaction(oracle, model, attachment, actions) :
                oracle -> OraTemplate.execute(oracle, model, attachment, actions);
    }

    /**
     * <p>modelCallback.</p>
     *
     * @param model   a {@link java.lang.String} object.
     * @param actions a {@link RPClosure} object.
     * @return a {@link PClosure} object.
     */
    public static PClosure<Oracle> modelCallback(final String model, final PClosure<Model> actions) {
        return modelCallback(model, actions, false);
    }

    /**
     * <p>modelCallback.</p>
     *
     * @param model         a {@link java.lang.String} object.
     * @param actions       a {@link PClosure} object.
     * @param transactional a boolean.
     * @return a {@link PClosure} object.
     */
    public static PClosure<Oracle> modelCallback(final String model, final PClosure<Model> actions,
                                                 boolean transactional) {
        return transactional ?
                oracle -> OraTemplate.doInTransaction(oracle, model, actions) :
                oracle -> OraTemplate.execute(oracle, model, actions);
    }

    /**
     * <p>modelCallback.</p>
     *
     * @param model      a {@link java.lang.String} object.
     * @param attachment a {@link oracle.spatial.rdf.client.jena.Attachment} object.
     * @param actions    a {@link PClosure} object.
     * @return a {@link PClosure} object.
     */
    public static PClosure<Oracle> modelCallback(final String model, final Attachment attachment,
                                                 final PClosure<Model> actions) {
        return modelCallback(model, attachment, actions, false);
    }

    /**
     * <p>modelCallback.</p>
     *
     * @param model         a {@link java.lang.String} object.
     * @param attachment    a {@link oracle.spatial.rdf.client.jena.Attachment} object.
     * @param actions       a {@link PClosure} object.
     * @param transactional a boolean.
     * @return a {@link PClosure} object.
     */
    public static PClosure<Oracle> modelCallback(final String model, final Attachment attachment,
                                                 final PClosure<Model> actions, boolean transactional) {
        return transactional ?
                oracle -> OraTemplate.doInTransaction(oracle, model, attachment, actions) :
                oracle -> OraTemplate.execute(oracle, model, attachment, actions);
    }

}
