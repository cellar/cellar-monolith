package eu.europa.ec.opoce.cellar.s3;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.google.common.base.Strings;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.annotation.EnableRetry;

import java.util.stream.Stream;

@EnableRetry
@Configuration
public class AWSConfiguration {

    private static final Logger LOG = LogManager.getLogger(AWSConfiguration.class);
    public static final Regions DEFAULT_REGION = Regions.EU_WEST_1;

    @Bean
    public S3Configuration s3Configuration(@Qualifier("cellarStaticConfiguration") ICellarConfiguration configuration) {
        S3Configuration s3Configuration = new S3Configuration();
        s3Configuration.setBucket(configuration.getS3Bucket());
        s3Configuration.setUnsafe(configuration.isS3Unsafe());
        if (s3Configuration.isUnsafe()) {
            LOG.warn("Unsafe mode for S3, bucket permanent deletion is enable, this option must be used in test environment only");
        }
        return s3Configuration;
    }

    @Bean
    public AmazonS3 amazonS3(@Qualifier("cellarStaticConfiguration") ICellarConfiguration configuration) {
        String region = configuration.getS3Region();
        if (Strings.isNullOrEmpty(region)) {
            LOG.error("Region is empty (property 's3.region'), fallback to " + DEFAULT_REGION);
            region = DEFAULT_REGION.getName();
        }

        final AWSCredentials credentials = new BasicAWSCredentials(configuration.getS3CredentialsAccessKey(),
                configuration.getS3CredentialsSecretKey());
        AmazonS3ClientBuilder builder = AmazonS3ClientBuilder.standard()
                .withCredentials(new CellarAWSCredentialsProviderChain(credentials))
                .withClientConfiguration(newClientConfiguration(configuration));
        if (!Strings.isNullOrEmpty(configuration.getS3Url()) && configuration.getS3Url().startsWith("http://")) { // localstack
            LOG.info(ICellarConfiguration.CONFIG, "S3 URL set to {}", configuration.getS3Url());
            builder = builder.withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(configuration.getS3Url(), region))
                    .withPathStyleAccessEnabled(true);
        } else {
            builder = builder.withRegion(findRegion(region));
        }
        return builder.build();
    }

    private static ClientConfiguration newClientConfiguration(ICellarConfiguration configuration) {
        ClientConfiguration cc = new ClientConfiguration()
                .withConnectionTimeout(configuration.getS3ClientConnectionTimeout())
                .withSocketTimeout(configuration.getS3ClientSocketTimeout());
        if (configuration.isProxyEnabled()) {
            cc.withProxyHost(configuration.getProxyHost())
                    .withProxyPort(configuration.getProxyPort())
                    .withProxyPassword(configuration.getProxyPassword())
                    .withProxyUsername(configuration.getProxyUser());
        }
        return cc;
    }

    private static Regions findRegion(String name) {
        return Stream.of(Regions.values())
                .filter(r -> r.getName().equalsIgnoreCase(name))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Region name " + name + " is not valid"));
    }
}
