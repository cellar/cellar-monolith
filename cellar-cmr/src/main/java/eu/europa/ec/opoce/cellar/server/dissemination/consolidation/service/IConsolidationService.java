/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.consolidation.service
 *             FILE : IConsolidationDataService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 15, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.consolidation.service;

import org.springframework.http.ResponseEntity;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 15, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IConsolidationService {

    ResponseEntity<?> getConsolidationMemento(final String identifier, final String rel, final String accept, final String acceptDatetime);

    ResponseEntity<?> getConsolidationData(final String identifier);

}
