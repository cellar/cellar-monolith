/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ontology
 *             FILE : OntologyNotFoundException.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 01 12, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-01-12 09:53:00 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ontology.exception;

/**
 * @author ARHS Developments
 */
public class OntologyNotFoundException extends OntologyException {

    public OntologyNotFoundException(String message) {
        super(message);
    }
}
