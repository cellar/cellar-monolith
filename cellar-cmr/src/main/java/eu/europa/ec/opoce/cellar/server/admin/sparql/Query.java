package eu.europa.ec.opoce.cellar.server.admin.sparql;

/**
 * <p>Query class.</p>
 */
public class Query {

    private String name;
    private String id;

    /**
     * <p>Constructor for Query.</p>
     *
     * @param name a {@link java.lang.String} object.
     * @param id   a {@link java.lang.String} object.
     */
    public Query(String name, String id) {
        this.name = name;
        this.id = id;
    }

    /**
     * <p>Getter for the field <code>name</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getName() {
        return name;
    }

    /**
     * <p>Setter for the field <code>name</code>.</p>
     *
     * @param name a {@link java.lang.String} object.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getId() {
        return id;
    }

    /**
     * <p>Setter for the field <code>id</code>.</p>
     *
     * @param id a {@link java.lang.String} object.
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "query";
    }
}
