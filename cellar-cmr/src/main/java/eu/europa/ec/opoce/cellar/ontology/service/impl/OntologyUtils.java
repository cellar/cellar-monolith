/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ontology.service.impl
 *             FILE : OntologyUtils.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 03 07, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-03-07 14:24:47 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ontology.service.impl;

import com.google.common.io.Files;
import eu.europa.ec.opoce.cellar.ontology.service.impl.OntologyLoader.ResourceFileAdapter;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author ARHS Developments
 */
final class OntologyUtils {

    private OntologyUtils() {
    }

    static InputStream toInputStream(File file) throws IOException {
        if (file instanceof ResourceFileAdapter) {
            return ((ResourceFileAdapter) file).getInputStream();
        } else {
            return Files.asByteSource(file).openStream();
        }

    }
}
