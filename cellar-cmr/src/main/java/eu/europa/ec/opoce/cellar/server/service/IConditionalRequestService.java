/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.service
 *             FILE : ConditionalRequestService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 20, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.service;

import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.HierarchyElement;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement;
import eu.europa.ec.opoce.cellar.server.conditionalRequest.ConditionalRequestVersions;
import eu.europa.ec.opoce.cellar.server.conditionalRequest.ETagType;

import java.util.Collection;

/**
 * <class_description>  .
 * <br/><br/>
 * <notes>  .
 * <br/><br/>
 * ON : Oct 20, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IConditionalRequestService {

    /**
     * Calculate version with embedding.
     *
     * @param element the element
     * @param etagType the etag type
     * @param eTag the e tag
     * @param lastModified the last modified
     * @return the conditional request versions
     */
    ConditionalRequestVersions calculateVersionWithEmbedding(final HierarchyElement<? extends MetsElement, ? extends MetsElement> element,
            final ETagType etagType, final String eTag, final String lastModified);

    /**
     * Calculate version without embedding.
     *
     * @param element the element
     * @param etagType the etag type
     * @param eTag the e tag
     * @param lastModified the last modified
     * @return the conditional request versions
     */
    ConditionalRequestVersions calculateVersionWithoutEmbedding(final CellarResourceBean element, final ETagType etagType,
            final String eTag, final String lastModified);

    /**
     * Calculate version with embedding.
     *
     * @param metsElement the mets element
     * @param etagType the etag type
     * @param eTag the e tag
     * @param lastModified the last modified
     * @return the conditional request versions
     */
    ConditionalRequestVersions calculateVersionWithEmbedding(final MetsElement metsElement, final ETagType etagType, final String eTag,
            final String lastModified);

    /**
     * Calculate version without embedding.
     *
     * @param metsElements the mets elements
     * @param etagType the etag type
     * @param eTag the e tag
     * @param lastModified the last modified
     * @return the conditional request versions
     */
    ConditionalRequestVersions calculateVersionWithoutEmbedding(final Collection<MetsElement> metsElements, final ETagType etagType,
            final String eTag, final String lastModified);

    /**
     * Calculate version with embedding.
     *
     * @param metsElements the mets elements
     * @param etagType the etag type
     * @param eTag the e tag
     * @param lastModified the last modified
     * @return the conditional request versions
     */
    ConditionalRequestVersions calculateVersionWithEmbedding(final Collection<MetsElement> metsElements, final ETagType etagType,
            final String eTag, final String lastModified);

    /**
     * Calculate version without embedding.
     *
     * @param resource the current Cellar resource (here an ITEM)
     * @param etagType the etag type
     * @param eTag the e tag
     * @param lastModified the last modified
     * @return the conditional request versions
     */
    ConditionalRequestVersions calculateVersionWithoutEmbedding(CellarResource resource, ETagType etagType, String eTag, String lastModified);

    /**
     * Calculate version.
     *
     * @param etagType the etag type
     * @param eTag the e tag
     * @param lastModified the last modified
     * @return the conditional request versions
     */
    ConditionalRequestVersions calculateVersionNow(final ETagType etagType, final String eTag, final String lastModified);
}
