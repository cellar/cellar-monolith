/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.resolver.impl.rdf
 *             FILE : RdfObjectDisseminationResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 20, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.retrieve.rdf;

import eu.europa.ec.opoce.cellar.common.http.headers.IURLTokenable;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.InferenceVariance;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.Structure;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.IDisseminationResolver;
import eu.europa.ec.opoce.cellar.server.service.DisseminationException;

import java.util.Date;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 20, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class RdfObjectRetrieveResolver extends RdfRetrieveResolver {

    public static IDisseminationResolver get(final String identifier, final InferenceVariance inferenceMode, final String eTag,
            final String lastModified, final boolean provideResponseBody, final Date acceptDateTime,boolean normalized) {
        return new RdfObjectRetrieveResolver(identifier, inferenceMode, eTag, lastModified, provideResponseBody, acceptDateTime,normalized);
    }

    private RdfObjectRetrieveResolver(final String identifier, final InferenceVariance inferenceMode, final String eTag,
            final String lastModified, final boolean provideResponseBody, final Date acceptDateTime,boolean normalized) {
        super(identifier, inferenceMode, eTag, lastModified, provideResponseBody, acceptDateTime,normalized);
    }

    @Override
    protected void initDisseminationRequest() {
        super.initDisseminationRequest();

        if (this.cellarResource.getCellarType() == DigitalObjectType.ITEM) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage("Bad resource type for RDF object notice. [resource '{}'({})]")
                    .withMessageArgs(this.cellarResource.getCellarId(), this.cellarResource.getCellarType()).build();
        }
    }

    /**
     * @see eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.retrieve.RetrieveResolver#doHandleDisseminationRequest()
     */
    @Override
    public ResponseEntity<?> doHandleDisseminationRequest() {
        switch (this.inferenceMode) {
        case FULL:
            return this.disseminationService.doRdfObjectRequest(this.eTag, this.lastModified, this.cellarResource,this.normalized);
        default:
        case NONINFERRED:
            return this.disseminationService.doRdfNonInferredObjectRequest(this.eTag, this.lastModified, this.cellarResource,this.normalized);
        }
    }

    @Override
    protected IURLTokenable getTypeStructure() {
        return Structure.OBJECT;
    }
}
