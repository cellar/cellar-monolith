/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.indexing.processor.impl
 *             FILE : CalcNoticeIndexRequestProcessorImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Nov 17, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.indexing.processor.impl;

import com.google.common.base.Joiner;
import eu.europa.ec.opoce.cellar.cl.domain.structmap_status_history.StructMapStatusHistory;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cl.service.client.StructMapStatusHistoryService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.EmbargoDatabaseService;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Work;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.IndexNotice;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.ExecutionStatus;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.RequestType;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestBatch;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.NoticeIndexRequestHandler;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.monitoring.Watch;
import eu.europa.ec.opoce.cellar.server.service.DisseminationDbGateway;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Nov 17, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component
@Qualifier("group")
@Order(40)
public class CalcNoticeIndexRequestProcessor extends AbstractIndexRequestProcessor {

    private static final Logger LOG = LogManager.getLogger(CalcNoticeIndexRequestProcessor.class);

    private final EmbargoDatabaseService embargoDatabaseService;

    private final IdentifierService identifierService;

    private final DisseminationDbGateway disseminationDbGateway;

    private final NoticeIndexRequestHandler noticeIndexRequestHandler;
    
    private final StructMapStatusHistoryService structMapStatusHistoryService;

    @Autowired
    public CalcNoticeIndexRequestProcessor(EmbargoDatabaseService embargoDatabaseService, @Qualifier("pidManagerService") IdentifierService identifierService,
                                           DisseminationDbGateway disseminationDbGateway, NoticeIndexRequestHandler noticeIndexRequestHandler,
                                           StructMapStatusHistoryService structMapStatusHistoryService) {
        this.embargoDatabaseService = embargoDatabaseService;
        this.identifierService = identifierService;
        this.disseminationDbGateway = disseminationDbGateway;
        this.noticeIndexRequestHandler = noticeIndexRequestHandler;
        this.structMapStatusHistoryService = structMapStatusHistoryService;
    }


    @Watch(value = "Calculate Notice Index", arguments = {
            @Watch.WatchArgument(name = "priority", expression = "batch.priority"),
            @Watch.WatchArgument(name = "groupByUri", expression = "batch.groupByUri"),
    })
    @Override
    public void execute(CmrIndexRequestBatch group, List<CmrIndexRequest> requests) {
        generateIndexNotices(group, requests);
    }

    private void generateIndexNotices(CmrIndexRequestBatch group, List<CmrIndexRequest> requests) {
        final String groupCellarId = this.identifierService.getCellarPrefixed(group.getGroupByUri()); // the uri must be correct!
        final CellarResource groupCellarResource = this.disseminationDbGateway.checkForResourceWithId(groupCellarId);

        if (groupCellarResource != null && !this.embargoDatabaseService.isMetadataSavedPrivate(groupCellarId)) {
            Collection<IndexNotice> expandedNotices = new HashSet<>();
            if (groupCellarResource.getCellarType() == DigitalObjectType.WORK) {
                expandedNotices = handlePublicWorkRequestGroup(groupCellarId, requests);
            } else {
                expandedNotices = handlePublicOtherRequestGroup(requests);
            }
            LOG.info("The index notices of '{}' have been generated.", groupCellarId);
            // Update LANGUAGES field of corresponding STRUCTMAP_STATUS_HISTORY entry
            updateStructMapLanguages(requests, expandedNotices);
        } else {
            LOG.warn("'{}' is not available. The index notices have not been generated.", groupCellarId);
        }
    }

    @Override
    protected List<CmrIndexRequest> filter(CmrIndexRequestBatch request) {
        return request.getCmrIndexRequests().stream()
                .filter(r -> r.getExecutionStatus() == ExecutionStatus.Execution &&
                        (r.getRequestType() == RequestType.CalcNotice || r.getRequestType() == RequestType.CalcExpanded))
                .collect(Collectors.toList());
    }

    /**
     * index a branch of expression or work with required-language, that was not specified in the work
     */
    private Collection<IndexNotice> handlePublicWorkRequestGroup(final String groupCellarId, final Collection<CmrIndexRequest> cmrIndexRequests) {
        final Work work = this.disseminationDbGateway.getWorkTreeForIndexation(groupCellarId);
        if (work == null) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("The following id {} could not be indexed as a work")
                    .withMessageArgs(groupCellarId)
                    .build();
        }

        final Collection<CmrIndexRequest> calcNoticeRequests = CollectionUtils.select(cmrIndexRequests, o -> o.getRequestType() == RequestType.CalcNotice);

        final Collection<IndexNotice> indexNotices = noticeIndexRequestHandler.calculateNoticeForGroup(calcNoticeRequests, work);
        if (!indexNotices.isEmpty()) {
            for (CmrIndexRequest indexRequest : calcNoticeRequests) {
                indexRequest.setRequestType(RequestType.CalcExpanded);
            }
        }

        final Collection<CmrIndexRequest> calcExpandedCmrIndexRequests = cmrIndexRequests.stream()
                .filter(r -> r.getRequestType() == RequestType.CalcExpanded)
                .collect(Collectors.toList());
        
        return noticeIndexRequestHandler.calculateExpandedForGroup(calcExpandedCmrIndexRequests, work, indexNotices);
    }

    /**
     * index a tree of Dossier or Agent
     */
    private Collection<IndexNotice> handlePublicOtherRequestGroup(final Collection<CmrIndexRequest> requests) {
        final Collection<CmrIndexRequest> calcNoticeCmrIndexRequests = CollectionUtils.select(requests, o -> o.getRequestType() == RequestType.CalcNotice);


        final Collection<IndexNotice> indexNotices = this.noticeIndexRequestHandler.calculateNoticeForGroup(calcNoticeCmrIndexRequests);
        for (CmrIndexRequest indexRequest : calcNoticeCmrIndexRequests) {
            indexRequest.setRequestType(RequestType.CalcExpanded);
        }
        return noticeIndexRequestHandler.calculateExpandedForGroup(calcNoticeCmrIndexRequests, indexNotices);
    }
    
    /**
     * Update the LANGUAGES field of the corresponding STRUCTMAP_STATUS_HISTORY entry
     * @param requests the current list of CmrIndexRequests
     * @param expandedNotices the totality of the expanded notices generated
     */
    private void updateStructMapLanguages(List<CmrIndexRequest> requests, Collection<IndexNotice> expandedNotices){
        // Filter the NON-REMOVE index requests of interest (i.e. the ones that have a non-null StructMapStatusHistoryId value)
        List<CmrIndexRequest> calcNoticeRequests = requests.stream()
                .filter(indx -> (indx.getStructMapStatusHistoryId() != null && !indx.getAction().equals(CmrIndexRequest.Action.Remove)))
                .collect(Collectors.toList());
        
        // Update the corresponding STRUCTMAP_STATUS_HISTORY entries
        StructMapStatusHistory structMapStatusHistory;
        for (CmrIndexRequest cmrIndexRequest : calcNoticeRequests){
            structMapStatusHistory = this.structMapStatusHistoryService.getEntry(cmrIndexRequest.getStructMapStatusHistoryId());
            if (structMapStatusHistory != null){
                // Extract the LANGUAGES of the expanded notices into a comma-separated string
                List<String> languagesCodes = expandedNotices.stream().map(indx -> indx.getIsoCode().toString().toUpperCase())
                        .sorted().collect(Collectors.toList());
                String languageString = Joiner.on(", ").join(languagesCodes);
                
                LOG.debug("The LANGUAGES of {} are {}",structMapStatusHistory, languageString);
                
                // Update the LANGUAGES field of the corresponding StructMapStatusHistory
                this.structMapStatusHistoryService.updateStructMapLanguages(structMapStatusHistory, languageString);
            }
        }
    }
}
