/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.common.http.memento
 *             FILE : MementoUtils.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 26-10-2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.utils;

import eu.europa.ec.opoce.cellar.cmr.service.IMementoService;
import eu.europa.ec.opoce.cellar.common.MessageFormatter;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import eu.europa.ec.opoce.cellar.common.http.headers.RelType;
import eu.europa.ec.opoce.cellar.common.util.StringUtils;
import eu.europa.ec.opoce.cellar.common.util.TimeUtils;

import java.util.Date;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 26-10-2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class MementoUtils {

    protected static IMementoService mementoService = ServiceLocator.getService("mementoService", IMementoService.class);

    private static final String URI_REL_FORMAT = "<{}>;rel=\"{}\"";
    private static final String URI_REL_DATETIME_FORMAT = URI_REL_FORMAT + ";datetime=\"{}\"";
    private static final String URI_REL_TYPE_FROM_UNTIL_DTYPE_FORMAT = URI_REL_FORMAT
            + ";type=\"{}\";from=\"{}\";until=\"{}\";dtype=\"{}\"";

    public static String getMementoLink(final String uri, final RelType rel) {
        return MessageFormatter.format(URI_REL_FORMAT, uri, rel.getValue());
    }

    public static String getMementoLink(final String uri, final RelType rel, final Date datetime) {
        return MessageFormatter.format(URI_REL_DATETIME_FORMAT, uri, rel.getValue(), TimeUtils.formatHTTP11Date(datetime));
    }

    public static String getMementoLink(final String uri, final RelType rel, final String type, final Date from, final Date until,
            final String dtype) {
        return MessageFormatter.format(URI_REL_TYPE_FROM_UNTIL_DTYPE_FORMAT, uri, rel.getValue(), type, TimeUtils.formatHTTP11Date(from),
                TimeUtils.formatHTTP11Date(until), dtype);
    }

    /**
     * Return the URI of the nearest memento, or {@link null} if none is found.
     * 
     * @param resourceURI the URI of the resource of which searching for the nearest memento
     * @param acceptDateTime the date to use as acceptDateTime. IT may be {@link null}, in which case the current date is used 
     * @return the URI of the nearest memento
     */
    public static String getNearestMemento(final String resourceURI, final Date acceptDateTime) {
        String mementoURI = null;

        // if acceptDateTime is present, just try to navigate the nearest in past
        if (acceptDateTime != null) {
            mementoURI = mementoService.getNearestInPastCellarUri(resourceURI, acceptDateTime);
        }
        // otherwise, try to navigate firstly the nearest in past, then the nearest in future
        else {
            mementoURI = resourceURI;
            final Date now = new Date();
            while (mementoService.isEvolutiveWork(mementoURI)) {
                final String pastMementoURI = mementoService.getNearestInPastCellarUri(mementoURI, now);

                if (StringUtils.isBlank(pastMementoURI)) {
                    mementoURI = mementoService.getNearestInFutureCellarUri(mementoURI, now);
                } else {
                    mementoURI = pastMementoURI;
                }
            }
            if (resourceURI.equals(mementoURI)) {
                mementoURI = null;
            }
        }

        // return
        return mementoURI;
    }
}
