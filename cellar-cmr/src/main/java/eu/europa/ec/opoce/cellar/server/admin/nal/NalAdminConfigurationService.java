/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.admin.nal
 *             FILE : NalAdminConfigurationService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 10 Jan 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.admin.nal;

import eu.europa.ec.opoce.cellar.common.util.Triple;

import java.util.List;
import java.util.Set;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 10 Jan 2017
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface NalAdminConfigurationService {
    /**
     * <p>
     * getNalVersions.
     * </p>
     *
     * @return a {@link java.util.Set} object.
     */
    Set<NalOntoVersion> getNalVersions();

    /**
     * Delete NAL.
     *
     * @param nalName the nal name
     * @return the list
     */
    List<Triple<String, String, String>> deleteNAL(String nalName);

}
