package eu.europa.ec.opoce.cellar.server.spring;

import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Map;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.view.AbstractView;

/**
 * <p>ObjectView class.</p>
 */
public class ObjectView extends AbstractView {

    private final Serializable object;

    /**
     * <p>Constructor for ObjectView.</p>
     *
     * @param object a {@link java.io.Serializable} object.
     */
    public ObjectView(Serializable object) {
        this.object = object;
        setContentType("application/octet-stream");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void renderMergedOutputModel(Map<String, Object> model, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        response.setContentType("application/octet-stream");
        ServletOutputStream responseOutputStream = response.getOutputStream();
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(responseOutputStream);
        objectOutputStream.writeObject(object);
        objectOutputStream.flush();
        responseOutputStream.flush();
    }
}
