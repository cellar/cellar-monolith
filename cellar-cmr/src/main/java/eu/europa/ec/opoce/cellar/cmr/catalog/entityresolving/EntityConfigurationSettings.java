package eu.europa.ec.opoce.cellar.cmr.catalog.entityresolving;

/**
 * <p>EntityConfigurationSettings class.</p>
 */
public class EntityConfigurationSettings {

    private EntitySnippetConfiguration configuration;

    /**
     * <p>Getter for the field <code>configuration</code>.</p>
     *
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.catalog.entityresolving.EntitySnippetConfiguration} object.
     */
    public EntitySnippetConfiguration getConfiguration() {
        return configuration;
    }
}
