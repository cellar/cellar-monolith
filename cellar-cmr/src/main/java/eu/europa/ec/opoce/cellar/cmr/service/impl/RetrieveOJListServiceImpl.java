/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.service.impl
 *             FILE : RetrieveOJListServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Aug 8, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.service.impl;

import eu.europa.ec.opoce.cellar.cmr.oj.OfficialJournals;
import eu.europa.ec.opoce.cellar.cmr.service.IRetrieveOJListService;
import eu.europa.ec.opoce.cellar.cmr.service.StructuredDynamicQueryConfiguration;
import eu.europa.ec.opoce.cellar.database.CmrTableName;
import eu.europa.ec.opoce.cellar.database.IRDFStoreJenaGateway;
import eu.europa.ec.opoce.cellar.jena.oracle.result.resolver.impl.OfficialJournalsResolver;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import org.apache.jena.query.Query;

/**
 * <class_description>  This class is dedicated to the listing of the daily oj by date and series set <br/>
 * <br/>
 * <notes>  <br/>
 * <br/>
 * ON : Aug 8, 2014.
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class RetrieveOJListServiceImpl implements IRetrieveOJListService {

    /** The Constant BLANK. */
    private static final String BLANK = "";

    /** The Constant START_DATE_PLACEHOLDER_REGEX. */
    private static final String START_DATE_PLACEHOLDER_REGEX = "\\$\\{start_date.*?\\}";

    /** The Constant START_DATE_PLACEHOLDER_PATTERN. */
    private static final Pattern START_DATE_PLACEHOLDER_PATTERN = Pattern.compile(START_DATE_PLACEHOLDER_REGEX, Pattern.DOTALL);

    /** The Constant END_DATE_PLACEHOLDER_REGEX. */
    private static final String END_DATE_PLACEHOLDER_REGEX = "\\$\\{end_date.*?\\}";

    /** The Constant END_DATE_PLACEHOLDER_PATTERN. */
    private static final Pattern END_DATE_PLACEHOLDER_PATTERN = Pattern.compile(END_DATE_PLACEHOLDER_REGEX, Pattern.DOTALL);

    /** The Constant OJ_YEAR_PLACEHOLDER_REGEX. */
    private static final String OJ_YEAR_PLACEHOLDER_REGEX = "\\$\\{oj_year\\}";

    /** The Constant OJ_YEAR_PLACEHOLDER_PATTERN. */
    private static final Pattern OJ_YEAR_PLACEHOLDER_PATTERN = Pattern.compile(OJ_YEAR_PLACEHOLDER_REGEX, Pattern.DOTALL);

    /** The Constant OJ_SERIES_CONDITION_PLACEHOLDER_REGEX. */
    private static final String OJ_SERIES_CONDITION_PLACEHOLDER_REGEX = "\\$\\{oj_series_condition\\}";

    /** The Constant OJ_SERIES_CONDITION_PLACEHOLDER_PATTERN. */
    private static final Pattern OJ_SERIES_CONDITION_PLACEHOLDER_PATTERN = Pattern.compile(OJ_SERIES_CONDITION_PLACEHOLDER_REGEX,
            Pattern.DOTALL);

    /** The Constant SERIES_URI_PLACEHOLDER_REGEX. */
    private static final String SERIES_URI_PLACEHOLDER_REGEX = "\\$\\{series_uri\\}";

    /** The Constant SERIES_URI_PLACEHOLDER_PATTERN. */
    private static final Pattern SERIES_URI_PLACEHOLDER_PATTERN = Pattern.compile(SERIES_URI_PLACEHOLDER_REGEX, Pattern.DOTALL);

    /** The Constant OJ_SUBSERIES_CONDITION_PLACEHOLDER_REGEX. */
    private static final String OJ_SUBSERIES_CONDITION_PLACEHOLDER_REGEX = "\\$\\{oj_subseries_condition\\}";

    /** The Constant OJ_SUBSERIES_CONDITION_PLACEHOLDER_PATTERN. */
    private static final Pattern OJ_SUBSERIES_CONDITION_PLACEHOLDER_PATTERN = Pattern.compile(OJ_SUBSERIES_CONDITION_PLACEHOLDER_REGEX,
            Pattern.DOTALL);

    /** The Constant SUB_SERIES_PLACEHOLDER_REGEX. */
    private static final String SUB_SERIES_PLACEHOLDER_REGEX = "\\$\\{sub_series\\}";

    /** The Constant SUB_SERIES_PLACEHOLDER_PATTERN. */
    private static final Pattern SUB_SERIES_PLACEHOLDER_PATTERN = Pattern.compile(SUB_SERIES_PLACEHOLDER_REGEX, Pattern.DOTALL);

    /** The Constant OJ_NUMBER_CONDITION_PLACEHOLDER_REGEX. */
    private static final String OJ_NUMBER_CONDITION_PLACEHOLDER_REGEX = "\\$\\{oj_number_condition\\}";

    /** The Constant OJ_NUMBER_CONDITION_PLACEHOLDER_PATTERN. */
    private static final Pattern OJ_NUMBER_CONDITION_PLACEHOLDER_PATTERN = Pattern.compile(OJ_NUMBER_CONDITION_PLACEHOLDER_REGEX,
            Pattern.DOTALL);

    /** The Constant OJ_NUMBER_PLACEHOLDER_REGEX. */
    private static final String OJ_NUMBER_PLACEHOLDER_REGEX = "\\$\\{oj_number\\}";

    /** The Constant OJ_NUMBER_PLACEHOLDER_PATTERN. */
    private static final Pattern OJ_NUMBER_PLACEHOLDER_PATTERN = Pattern.compile(OJ_NUMBER_PLACEHOLDER_REGEX, Pattern.DOTALL);

    /** The Constant SUB_QUERY_NAME. */
    private static final String SUB_QUERY_NAME = "sub_query_name";

    /** The Constant SUB_QUERY_NAME_PATTERN. */
    public static final Pattern SUB_QUERY_NAME_PATTERN = Pattern.compile(SUB_QUERY_NAME, Pattern.DOTALL);

    /** The rdf store jena gateway. */
    @Autowired
    private IRDFStoreJenaGateway rdfStoreJenaGateway;

    /** The structured dynamic query configuration. */
    @Autowired
    private StructuredDynamicQueryConfiguration structuredDynamicQueryConfiguration;

    /** The oj series map. */
    @Autowired
    @Qualifier("ojSeriesMap")
    private HashMap<String, String> ojSeriesMap;

    /** {@inheritDoc} */
    @Override
    public OfficialJournals getTheDateOfTheMostRecentOj(final int year) {
        final Map<Pattern, Object> placeHoldersMap = new HashMap<>();
        placeHoldersMap.put(OJ_YEAR_PLACEHOLDER_PATTERN, Integer.toString(year));
        //build the sparql query
        final Query sparqlQueryObj = this.structuredDynamicQueryConfiguration.getQueryForTheMostRecentWorkDateOfOj(placeHoldersMap);
        //execute and extract output
        final OfficialJournals result = this.rdfStoreJenaGateway.executeSelectQuery(sparqlQueryObj, CmrTableName.CMR_METADATA,
                new OfficialJournalsResolver());
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public OfficialJournals getTheListOfOjSeries(final int year) {
        final Map<Pattern, Object> placeHoldersMap = new HashMap<>();
        placeHoldersMap.put(OJ_YEAR_PLACEHOLDER_PATTERN, Integer.toString(year));
        //build the sparql query
        final Query sparqlQueryObj = this.structuredDynamicQueryConfiguration.getQueryToListAllSeriesOfOj(placeHoldersMap);
        //execute and extract output
        final OfficialJournals result = this.rdfStoreJenaGateway.executeSelectQuery(sparqlQueryObj, CmrTableName.CMR_METADATA,
                new OfficialJournalsResolver());
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public OfficialJournals getTheListOfOjSubSeries(final int year) {
        final Map<Pattern, Object> placeHoldersMap = new HashMap<>();
        placeHoldersMap.put(OJ_YEAR_PLACEHOLDER_PATTERN, Integer.toString(year));
        //build the sparql query
        final Query sparqlQueryObj = this.structuredDynamicQueryConfiguration.getQueryToListAllSubSeriesOfOj(placeHoldersMap);
        //execute and extract output
        final OfficialJournals result = this.rdfStoreJenaGateway.executeSelectQuery(sparqlQueryObj, CmrTableName.CMR_METADATA,
                new OfficialJournalsResolver());
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public OfficialJournals list(final int year, final int month, final int day) {
        final Calendar calendar = Calendar.getInstance();
        // Calendar's month is 0-11 OP's month is 1-12
        calendar.set(year, (month == 0 ? month : (month - 1)), (day == 0 ? 1 : day));
        final Date startDate = calendar.getTime();
        // sparql purposes
        if ((month == 0) && (day == 0)) {
            calendar.add(Calendar.YEAR, 1);
        } else if (day == 0) {
            calendar.add(Calendar.MONTH, 1);
        } else {
            calendar.add(Calendar.DATE, 1);
        }
        final Date endDate = calendar.getTime();

        //populate the place-holders map
        final Map<Pattern, Object> placeHoldersMap = new HashMap<>();
        placeHoldersMap.put(START_DATE_PLACEHOLDER_PATTERN, startDate);
        placeHoldersMap.put(END_DATE_PLACEHOLDER_PATTERN, endDate);
        placeHoldersMap.put(OJ_YEAR_PLACEHOLDER_PATTERN, Integer.toString(year));

        //build the query
        final Query sparqlQueryObj = this.structuredDynamicQueryConfiguration.getQueryForOjOfTheDay(placeHoldersMap);
        //execute and extract output
        final OfficialJournals result = this.rdfStoreJenaGateway.executeSelectQuery(sparqlQueryObj, CmrTableName.CMR_METADATA,
                new OfficialJournalsResolver());
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public OfficialJournals list(final int year, final String series, final String subseries, final int number) {

        //the controller that calls this service may do it in several combinations (having all 4 parameters, only 3,only 2...etc)
        //the query involved contains conditions that are only supposed to be added  OJ_SERIES_CONDITION_PLACEHOLDER_REGEX|OJ_SUBSERIES_CONDITION_PLACEHOLDER_REGEX|OJ_NUMBER_CONDITION_PLACEHOLDER_REGEX
        //when populating the place-holders map we need to check if the passed parameter is not blank, in that case we replace the condition by a blank space
        final Map<Pattern, Object> placeHoldersMap = new HashMap<>();
        // the year is added to the list of conditions to limit the number of results and to improve the performance of the search
        placeHoldersMap.put(OJ_YEAR_PLACEHOLDER_PATTERN, Integer.toString(year));//the year

        //handle the series parameter
        Map<Pattern, Object> subQueryPlaceHoldersMap = new HashMap<>();//we build a place-holders map for the condition
        if (StringUtils.isNotBlank(series)) {//if the series parameter is not blank we add it by putting its value in the map
            final String fullOjSeriesURI = this.ojSeriesMap.get(series);//get the full URI from the ojSeries map
            subQueryPlaceHoldersMap.put(SERIES_URI_PLACEHOLDER_PATTERN, fullOjSeriesURI);
        } else {
            subQueryPlaceHoldersMap.put(SERIES_URI_PLACEHOLDER_PATTERN, BLANK);//otherwise we mark the condition to be replace by a blank space
        }
        subQueryPlaceHoldersMap.put(SUB_QUERY_NAME_PATTERN, "oj_series_condition");//we have to add the name of the condition to load
        placeHoldersMap.put(OJ_SERIES_CONDITION_PLACEHOLDER_PATTERN, subQueryPlaceHoldersMap);//we add the condition to the main place-holders map

        //  handle the sub-series parameter
        subQueryPlaceHoldersMap = new HashMap<>();
        if (StringUtils.isNotBlank(subseries)) {
            subQueryPlaceHoldersMap.put(SUB_SERIES_PLACEHOLDER_PATTERN, subseries);
        } else {
            subQueryPlaceHoldersMap.put(SUB_SERIES_PLACEHOLDER_PATTERN, BLANK);
        }
        subQueryPlaceHoldersMap.put(SUB_QUERY_NAME_PATTERN, "oj_subseries_condition");
        placeHoldersMap.put(OJ_SUBSERIES_CONDITION_PLACEHOLDER_PATTERN, subQueryPlaceHoldersMap);

        // handle the oj number parameter
        subQueryPlaceHoldersMap = new HashMap<>();
        if (number > 0) {//the oj number condition is only supposed to added if the number passed as a parameter through the controller is bigger than 0
            subQueryPlaceHoldersMap.put(OJ_NUMBER_PLACEHOLDER_PATTERN, Integer.toString(number));
        } else {
            subQueryPlaceHoldersMap.put(OJ_NUMBER_PLACEHOLDER_PATTERN, BLANK);
        }
        subQueryPlaceHoldersMap.put(SUB_QUERY_NAME_PATTERN, "oj_number_condition");
        placeHoldersMap.put(OJ_NUMBER_CONDITION_PLACEHOLDER_PATTERN, subQueryPlaceHoldersMap);

        //build the sparql query
        final Query sparqlQueryObj = this.structuredDynamicQueryConfiguration.getQueryForOjOfSeries(placeHoldersMap);
        //execute and extract output
        final OfficialJournals result = this.rdfStoreJenaGateway.executeSelectQuery(sparqlQueryObj, CmrTableName.CMR_METADATA,
                new OfficialJournalsResolver());
        return result;
    }

}
