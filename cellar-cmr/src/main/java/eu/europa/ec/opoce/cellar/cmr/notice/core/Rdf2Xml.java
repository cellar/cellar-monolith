/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice.core
 *             FILE : Rdf2Xml.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 24-Apr-2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.notice.core;

import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.semantic.xml.XmlBuilder;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author ARHS Developments
 */
public class Rdf2Xml {

    private final Map<LanguageBean, List<LanguageBean>> cache;
    private final Model model;
    private final XmlBuilder rootXmlBuilder;
    private final Set<LanguageBean> contentLanguages;
    private final Set<LanguageBean> languages;
    private final boolean filter;
    private final NoticeType noticeType;
    private Set<Resource> allSubjectResources;
    private final boolean writeSubjectInfo;
    private final String subjectUri;
    private final Map<String, String> uriToCellarIdentifierMappings;

    /**
     * Create a new object to convert a given Jena model to an XML notice
     *
     * @param model             the input model
     * @param subjectUri        subject of which the data is needed from
     * @param rootXmlBuilder    xmlBuilder element that needs to contain all data about the given subject after executing the command
     * @param contentLanguages  the content languages
     * @param languages         the languages
     * @param filter            the filter
     * @param noticeType        the type of notice that needs to be generated
     */
    public Rdf2Xml(final Model model, final String subjectUri, final XmlBuilder rootXmlBuilder,
                   final Set<LanguageBean> contentLanguages, final Set<LanguageBean> languages,
                   final boolean filter, final NoticeType noticeType) {
        this(model, subjectUri, rootXmlBuilder, contentLanguages, languages, filter, noticeType, null, true);
    }

    /**
     * Create a new object to convert a given Jena model to an XML notice
     *
     * @param model            the input model
     * @param subjectUri       subject of which the data is needed from
     * @param rootXmlBuilder   xmlBuilder element that needs to contain all data about the given subject after executing the command
     * @param contentLanguages the content languages
     * @param languages        the languages
     * @param filter           the filter
     * @param noticeType       the type of notice that needs to be generated
     * @param writeSubjectInfo indicate if the data about the subject need to be added in the xml-builder
     */
    public Rdf2Xml(final Model model, final String subjectUri, final XmlBuilder rootXmlBuilder,
                   final Set<LanguageBean> contentLanguages, final Set<LanguageBean> languages,
                   final boolean filter, final NoticeType noticeType, Map<String, String> uriToCellarIdentifierMappings,
                   final boolean writeSubjectInfo) {
        this.model = model;
        this.noticeType = noticeType;
        this.subjectUri = subjectUri;
        this.rootXmlBuilder = rootXmlBuilder;
        this.contentLanguages = contentLanguages;
        this.languages = languages;
        this.filter = filter;
        this.uriToCellarIdentifierMappings = uriToCellarIdentifierMappings;
        this.writeSubjectInfo = writeSubjectInfo;
        this.cache = new HashMap<>();
    }

    protected Set<LanguageBean> getContentLanguages() {
        return this.contentLanguages;
    }

    protected Set<LanguageBean> getLanguages() {
        return this.languages;
    }

    public Model getModel() {
        return this.model;
    }

    public XmlBuilder getRootXmlBuilder() {
        return this.rootXmlBuilder;
    }

    public boolean isFilter() {
        return this.filter;
    }

    public NoticeType getNoticeType() {
        return this.noticeType;
    }

    public boolean isWriteSubjectInfo() {
        return this.writeSubjectInfo;
    }

    public Set<Resource> getAllSubjectResources() {
        return allSubjectResources;
    }

    public void setAllSubjectResources(Set<Resource> allSubjectResources) {
        this.allSubjectResources = allSubjectResources;
    }

    public Map<LanguageBean, List<LanguageBean>> getCache() {
        return cache;
    }

    public String getSubjectUri() {
        return subjectUri;
    }

	public Map<String, String> getUriToCellarIdentifierMappings() {
		return uriToCellarIdentifierMappings;
	}

}
