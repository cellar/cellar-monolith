/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.rdf
 *             FILE : RdfRedirectResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 9, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.rdf;

import eu.europa.ec.opoce.cellar.server.dissemination.AcceptLanguage;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.DisseminationRequest;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.InferenceVariance;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.Structure;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.IDisseminationResolver;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.RedirectResolver;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;

import java.util.Date;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.http.ResponseEntity;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Apr 9, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Configurable
public abstract class RdfRedirectResolver extends RedirectResolver {

    protected final InferenceVariance inferenceVariance;
    protected final boolean normalized;

    protected RdfRedirectResolver(final CellarResourceBean cellarResource, final InferenceVariance inferenceVariance,
            final AcceptLanguage acceptLanguage, final boolean provideAlternates,final boolean normalized) {
        super(cellarResource, acceptLanguage, provideAlternates);
        this.inferenceVariance = inferenceVariance;
        this.normalized=normalized;
    }

    public static IDisseminationResolver get(final CellarResourceBean cellarResource, final DisseminationRequest disseminationRequest,
            final AcceptLanguage acceptLanguage, final Date acceptDateTime, final String rel, final boolean provideAlternates) {
        switch (disseminationRequest.getStructure(cellarResource)) {
        case TREE: // rdf
            return RdfTreeRedirectResolver.get(cellarResource, disseminationRequest.getInferenceVariance(), acceptLanguage,
                    provideAlternates,disseminationRequest.getNormalized());
        default:
        case OBJECT: // rdf
            return RdfObjectRedirectResolver.get(cellarResource, disseminationRequest.getInferenceVariance(), acceptLanguage,
                    provideAlternates,disseminationRequest.getNormalized());
        }
    }

    @Override
    protected ResponseEntity<Void> doHandleDisseminationRequest() {
        return this.seeOtherService.negotiateSeeOtherForRdf(this.cellarResource, (Structure) this.getTypeStructure(),
                this.inferenceVariance, this.provideAlternates,this.normalized);
    }

}
