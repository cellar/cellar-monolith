package eu.europa.ec.opoce.cellar.s3;

import eu.europa.ec.opoce.cellar.cmr.ContentType;

import java.util.function.Supplier;

public final class ContentStreamExceptions {

    private ContentStreamExceptions() {
    }

    public static Supplier<IllegalStateException> notFound(String id, String version, ContentType contentType) {
        return () -> new IllegalStateException(contentType + " not found for " + id + " (" + version + ")");
    }
}
