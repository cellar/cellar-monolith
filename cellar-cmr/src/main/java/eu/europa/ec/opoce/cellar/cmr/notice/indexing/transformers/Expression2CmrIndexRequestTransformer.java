package eu.europa.ec.opoce.cellar.cmr.notice.indexing.transformers;

import eu.europa.ec.opoce.cellar.cmr.metsbeans.Expression;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest;

import org.apache.commons.collections15.Transformer;

/**
 * <p>Expression2CmrIndexRequestTransformer class.</p>
 */
public class Expression2CmrIndexRequestTransformer implements Transformer<Expression, CmrIndexRequest> {

    private final CmrIndexRequest request;

    /**
     * <p>Constructor for Expression2CmrIndexRequestTransformer.</p>
     *
     * @param request a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest} object.
     */
    public Expression2CmrIndexRequestTransformer(CmrIndexRequest request) {
        this.request = request;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("serial")
    @Override
    public CmrIndexRequest transform(final Expression expression) {
        return new CmrIndexRequest(request) {

            {
                setObjectUri(expression.getCellarId().getUri());
                setObjectType(expression.getType());
            }
        };
    }

}
