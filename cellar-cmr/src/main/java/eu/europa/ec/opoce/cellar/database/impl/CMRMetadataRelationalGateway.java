/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.database.impl
 *             FILE : CMRMetadataRelationalGateway.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 25-07-2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.database.impl;

import com.google.common.collect.Lists;
import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cmr.CmrInverseDissemination;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CmrInverseDisseminationDao;
import eu.europa.ec.opoce.cellar.cmr.model.ModelUtils;
import eu.europa.ec.opoce.cellar.cmr.notice.cache.ISameAsCache;
import eu.europa.ec.opoce.cellar.cmr.notice.cache.impl.NonTransactionalServiceBasedSameAsCache;
import eu.europa.ec.opoce.cellar.common.util.IterableUtils;
import eu.europa.ec.opoce.cellar.common.util.Pair;
import eu.europa.ec.opoce.cellar.database.CmrTableName;
import eu.europa.ec.opoce.cellar.database.CmrTableName.CmrTableMode;
import eu.europa.ec.opoce.cellar.database.Database;
import eu.europa.ec.opoce.cellar.database.IRDFStoreRelationalGateway;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.jena.comparator.TripleComparator;
import oracle.jdbc.OracleClob;
import oracle.jdbc.OraclePreparedStatement;
import oracle.spatial.rdf.client.jena.OracleTranslator;
import oracle.spatial.rdf.client.jena.OracleUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections15.Transformer;
import org.apache.commons.io.FileUtils;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.Triple;
import org.apache.jena.rdf.model.Model;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import static eu.europa.ec.opoce.cellar.database.CmrTableName.CMR_BLANK_NODE;
import static org.apache.commons.collections15.CollectionUtils.collect;

/**
 * <class_description> Gateway to be used to access the CMR metadata onto the RDF store store in a relational way.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 25-07-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service("cmrMetadataRelationalGateway")
public class CMRMetadataRelationalGateway implements IRDFStoreRelationalGateway {

    private static final Logger LOG = LogManager.getLogger(CMRMetadataRelationalGateway.class);

    /**
     * The cmr inverse dissemination dao.
     */
    @Autowired
    private CmrInverseDisseminationDao cmrInverseDisseminationDao;

    /**
     * The database.
     */
    @Autowired(required = true)
    @Qualifier("databaseCellarDataCmr")
    private Database database;

    /**
     * The triple 2 context transformer.
     */
    @Autowired
    @Qualifier("pair2Two")
    private Transformer<Pair<Triple, String>, String> triple2ContextTransformer;

    /**
     * The cellar configuration.
     */
    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    /**
     * The Constant SELECT.
     */
    private static final String SELECT = "" + //
            "select CASE WHEN s.VALUE_TYPE='UR' THEN 'URI' WHEN s.VALUE_TYPE='BN' THEN 'BLN' ELSE 'LIT' END s$RDFVTYP,"/*   */ + " \n" + //
            "       decode(s.value_type, 'BN', ('_:'||substr(s.value_name,instr(s.value_name,'m',4)+1)), s.value_name) s,"/**/ + " \n" + //
            "       CASE WHEN p.VALUE_TYPE='UR' THEN 'URI' WHEN p.VALUE_TYPE='BN' THEN 'BLN' ELSE 'LIT' END p$RDFVTYP,"/*   */ + " \n" + //
            "       p.value_name    as p,"/*                                                                                */ + " \n" + //
            "       CASE WHEN o.VALUE_TYPE='UR' THEN 'URI' WHEN o.VALUE_TYPE='BN' THEN 'BLN' ELSE 'LIT' END o$RDFVTYP,"/*   */ + " \n" + //
            "       o.literal_type  as o$RDFLTYP,"/*                                                                        */ + " \n" + //
            "       o.language_type as o$RDFLANG,"/*                                                                        */ + " \n" + //
            "       o.long_value    as o$RDFCLOB,"/*                                                                        */ + " \n" + //
            "       decode(o.value_type, 'BN', ('_:'||substr(o.value_name,instr(o.value_name,'m',4)+1)), o.value_name) o,"/**/ + " \n" + //
            "       inner.rowid,"/*                                                                                         */ + " \n" + //
            "       inner.context"/*                                                                                        */ + " \n" + //
            "from ("/*                                                                                                      */ + " \n" + //
            "    select t.triple.rdf_s_id sid,"/*                                                                           */ + " \n" + //
            "           t.triple.rdf_p_id pid,"/*                                                                           */ + " \n" + //
            "           t.triple.rdf_o_id cid,"/*                                                                           */ + " \n" + //
            "           rowid,"/*                                                                                           */ + " \n" + //
            "           context"/*                                                                                          */ + " \n" + //
            "    from "/*                                                                                                   */ + " \n";

    /**
     * The Constant WHERE_CONTEXT_EQUALS.
     */
    private static final String WHERE_CONTEXT_EQUALS = "" + //
            " t  where context = ?"/*                                                                                       */ + " \n";

    /**
     * The Constant WHERE_CONTEXT_LIKE.
     */
    private static final String WHERE_CONTEXT_LIKE = "" + //
            " t  where context like ?"/*                                                                                    */ + " \n";

    /**
     * The Constant WHERE_CONTEXT_IN.
     */
    private static final String WHERE_CONTEXT_IN = "" + //
            " t  where context in ("/*                                                                                      */ + " \n";

    /**
     * The Constant WHERE_SUFFIX.
     */
    private static final String WHERE_SUFFIX = "" + //
            ") inner, mdsys.rdf_value$ s, mdsys.rdf_value$ p, mdsys.rdf_value$ o"/*                                         */ + " \n" + //
            "where"/*                                                                                                       */ + " \n" + //
            "    inner.sid=s.value_id and"/*                                                                                */ + " \n" + //
            "    inner.pid=p.value_id and"/*                                                                                */ + " \n" + //
            "    inner.cid=o.value_id"/*                                                                                    */ + " \n";

    /**
     * The quad file number.
     */
    private static int quadFileNumber = 1;

    /**
     * The oracle translator.
     */
    private static OracleTranslator oracleTranslator = new OracleTranslator();

    /**
     * Check blank node model.
     */
    @PostConstruct
    private void checkBlankNodeModel() {
        final List<Integer> modelNumbers = new ArrayList<Integer>();
        this.database.getJdbcOperations().query("select SDO_RDF.GET_MODEL_ID('" + CMR_BLANK_NODE.toString() + "') from dual",
                rs -> {
                    modelNumbers.add(rs.getInt(1));
                });
        if (modelNumbers.isEmpty()) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.CELLAR_INTERNAL_ERROR)
                    .withMessage(
                            "the rdf-model for blank node is not defined, if there was already data loaded this could corrupt all of it.")
                    .build();
        }
    }

    /**
     * @see eu.europa.ec.opoce.cellar.database.IRDFStoreRelationalGateway#insert(Connection, List, CmrTableName, boolean)
     */
    @Override
    public void insert(final Connection connection, final List<Pair<Triple, String>> quads, final CmrTableName table,
                       final boolean syncDissTable) {
        if (CollectionUtils.isEmpty(quads)) {
            return;
        }

        // insert the model into cmr_* table
        final String sql = "INSERT INTO " + table.toString() + " (CONTEXT,TRIPLE) VALUES(?, SDO_RDF_TRIPLE_S( ?, ?, ?, ?, 0))";
        try (OraclePreparedStatement insertStatement = (OraclePreparedStatement) connection.prepareStatement(sql)) {
            Collections.sort(quads, getQuadComparator());

            for (final Pair<Triple, String> quad : quads) {
                addTripleToInsert(insertStatement, table.toString(), quad.getTwo(), quad.getOne(), connection);
            }

            insertStatement.executeBatch();
        } catch (final SQLException e) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.CMR_INTERNAL_ERROR).withCause(e)
                    .withMessage("Unable to load clusters (see {} for triples that failed): {}")
                    .withMessageArgs(this.writeQuadsToFile(quads), e.getLocalizedMessage()).build();
        }

        // if needed, sync diss table
        if (syncDissTable) {
            this.syncDissTable(connection, quads, table);
        }
    }

    /**
     * @see eu.europa.ec.opoce.cellar.database.IRDFStoreRelationalGateway#remove(Connection, List, Map, CmrTableName, boolean)
     */
    @Override
    public void remove(final Connection connection, final List<Pair<Triple, String>> quads,
                       final Map<Pair<Triple, String>, String> oldQuads, final CmrTableName table, final boolean syncDissTable) {
        if (CollectionUtils.isEmpty(quads)) {
            return;
        }

        // delete the due entries from cmr_* table
        final String deleteQuery = "DELETE FROM " + table.toString() + " WHERE ROWID = ?";
        try (OraclePreparedStatement deleteStatement = (OraclePreparedStatement) connection.prepareStatement(deleteQuery)) {
            for (final Pair<Triple, String> quad : quads) {
                deleteStatement.clearParameters();
                deleteStatement.setString(1, oldQuads.get(quad));
                deleteStatement.addBatch();
            }
            deleteStatement.executeBatch();
        } catch (final SQLException e) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.CMR_INTERNAL_ERROR).withCause(e)
                    .withMessage("Unable to delete clusters from table '" + table + "' (see {} for triples that failed): {}")
                    .withMessageArgs(this.writeQuadsToFile(quads), e.getLocalizedMessage()).build();
        }

        // if needed, sync diss table
        if (syncDissTable) {
            this.syncDissTable(connection, quads, table);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteContexts(final Connection connection, final List<String> contexts, final CmrTableName... tables) {
        if (CollectionUtils.isEmpty(contexts)) {
            return;
        }

        final List<CmrTableName> syncedDissTables = Lists.newArrayList();
        Arrays.stream(tables).forEach(table -> {
                    this.internalDeleteContexts(table.toString(), connection, contexts);

                    // if needed, sync diss table
                    if (dissTableCanBeSynced(table) && !syncedDissTables.contains(table.getDisseminationTable())) {
                        this.internalDeleteContexts(table.getDisseminationTable().toString(), connection, contexts);
                        syncedDissTables.add(table.getDisseminationTable());
                    }
                }
        );
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moveContexts(final Connection connection, final List<String> contexts,
                             final CmrTableName sourceTable, final CmrTableName destTable) {
        if (CollectionUtils.isEmpty(contexts)) {
            return;
        }

        final String contextsInString = "'" + String.join("','", contexts) + "'";

        LOG.info("Moving the contexts : {} from {} to {}", contextsInString, sourceTable, destTable);

        // add the contexts to the destination table
        try (CallableStatement call = connection.prepareCall("INSERT INTO " + destTable + " (triple, context) "
                + "SELECT sdo_rdf_triple_s(t.triple.rdf_c_id, model.model_id, t.triple.rdf_s_id, t.triple.rdf_p_id, t.triple.rdf_o_id), context "
                + "FROM " + sourceTable + " t, (select model_id from  mdsys.rdf_model$ where model_name = '" + destTable + "') model "
                + "WHERE context IN (" + contextsInString + ")")) {
            call.execute();
        } catch (SQLException e) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.CMR_INTERNAL_ERROR).withCause(e)
                    .withMessage("Unable to add contexts '{}' to table '{}'.")
                    .withMessageArgs(contextsInString, destTable).build();
        }

        // if the destination table is a non-dissemination table - and has a dissemination table - add the contexts also to the dissemination table
        if (destTable.getMode() == CmrTableMode.NORMAL && destTable.getDisseminationTable() != null) {
            // firstly get the involved contexts out of "context%"
            final Set<String> movedContexts = new HashSet<>();
            try (final ResultSet rs = connection
                    .prepareCall("SELECT DISTINCT CONTEXT FROM " + destTable + " WHERE context IN (" + contextsInString + ")")
                    .executeQuery()) {
                while (rs.next()) {
                    movedContexts.add(rs.getString(1));
                }
            } catch (final SQLException e) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.CMR_INTERNAL_ERROR).withCause(e)
                        .withMessage("Unable to select model from table '{}' for contexts ({})")
                        .withMessageArgs(destTable, contextsInString).build();
            }

            // then add to the dissemination table the entries related to these contexts
            final ISameAsCache sameAsCache = new NonTransactionalServiceBasedSameAsCache();
            for (final String movedContext : movedContexts) {
                this.syncDissTable(connection, movedContext, sameAsCache, destTable);
            }
        }

        // remove the contexts from the source table
        try (CallableStatement call = connection.prepareCall("DELETE FROM  " + sourceTable + " WHERE CONTEXT IN (" + contextsInString + ")")) {
            call.execute();
        } catch (SQLException e) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.CMR_INTERNAL_ERROR).withCause(e)
                    .withMessage("Unable to remove contexts '{}' from table '{}'.")
                    .withMessageArgs(contextsInString, sourceTable).build();
        }

        // if the source table is a non-dissemination table - and has a dissemination table - remove the contexts also from the dissemination table
        if (sourceTable.getMode() == CmrTableMode.NORMAL && destTable.getDisseminationTable() != null) {
            try (CallableStatement call = connection.prepareCall("DELETE FROM  " + destTable.getDisseminationTable() + " WHERE CONTEXT IN (" + contextsInString + ")")) {
                call.execute();
            } catch (SQLException e) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.CMR_INTERNAL_ERROR).withCause(e)
                        .withMessage("Unable to remove contexts '{}' from table '{}'.")
                        .withMessageArgs(contextsInString, destTable.getDisseminationTable()).build();
            }
        }
    }

    /**
     * @see eu.europa.ec.opoce.cellar.database.IRDFStoreRelationalGateway#syncDissTable(Connection, List, CmrTableName)
     */
    @Override
    public void syncDissTable(final Connection connection, final List<Pair<Triple, String>> quads, final CmrTableName table) {
        if (CollectionUtils.isEmpty(quads) || !dissTableCanBeSynced(table)) {
            return;
        }

        final ISameAsCache sameAsCache = new NonTransactionalServiceBasedSameAsCache();
        final Set<String> contexts = collect(quads, this.triple2ContextTransformer, new HashSet<>());
        for (final String context : contexts) {
            this.syncDissTable(connection, context, sameAsCache, table);
        }
    }

    /**
     * @see eu.europa.ec.opoce.cellar.database.IRDFStoreRelationalGateway#syncDissTable(Connection, String, ISameAsCache, CmrTableName)
     */
    @Override
    public void syncDissTable(final Connection connection, final String context, final ISameAsCache sameAsCache, final CmrTableName table) {
        if (context == null || !dissTableCanBeSynced(table)) {
            return;
        }

        final ISameAsCache mySameAsCache = sameAsCache == null ? new NonTransactionalServiceBasedSameAsCache() : sameAsCache;
        final CmrInverseDissemination inverseObject = new CmrInverseDissemination();
        inverseObject.setContext(context);
        final Model inverseModel = this.selectModel(table, context, connection);
        inverseObject.setInverseModel(inverseModel);
        final Model sameAsModel = mySameAsCache.extractSameAses(inverseModel);
        inverseObject.setSameAsModel(sameAsModel);
        inverseObject.setBacklog(false);
        this.cmrInverseDisseminationDao.upsertByContext(inverseObject, connection);
    }

    /**
     * Select model.
     *
     * @param table   the table
     * @param context the context
     * @return the model
     * @see eu.europa.ec.opoce.cellar.database.IRDFStoreRelationalGateway#selectModel(eu.europa.ec.opoce.cellar.database.CmrTableName, java.lang.String)
     */
    @Override
    public Model selectModel(final CmrTableName table, final String context) {
        final List<Triple> triples = new ArrayList<>();

        this.database.getJdbcOperations().query(SELECT + table.toString() + WHERE_CONTEXT_EQUALS + WHERE_SUFFIX,
                setStringParameters(context), getRowToTripleHandler(triples));

        return ModelUtils.getModelFromTriples(triples);
    }

    /**
     * Select model.
     *
     * @param table      the table
     * @param context    the context
     * @param connection the connection
     * @return the model
     * @see eu.europa.ec.opoce.cellar.database.IRDFStoreRelationalGateway#selectModel(eu.europa.ec.opoce.cellar.database.CmrTableName, java.lang.String, java.sql.Connection)
     */
    @Override
    public Model selectModel(final CmrTableName table, final String context, final Connection connection) {
        return staticSelectModel(table, context, connection);
    }

    /**
     * Select models.
     *
     * @param table    the table
     * @param contexts the contexts
     * @return the collection
     * @see eu.europa.ec.opoce.cellar.database.IRDFStoreRelationalGateway#selectModels(eu.europa.ec.opoce.cellar.database.CmrTableName, java.util.List)
     */
    @Override
    public Collection<Model> selectModels(final CmrTableName table, final List<String> contexts) {
        final Collection<Model> retModels = new ArrayList<>();

        final Map<String, List<Triple>> triplesPerContext = this.getTriplesPerContext(table, contexts);
        collect(triplesPerContext.values(), triples -> ModelUtils.getModelFromTriples(triples), retModels);

        return retModels;
    }

    /**
     * Select models per context.
     *
     * @param table    the table
     * @param contexts the contexts
     * @return the map
     * @see eu.europa.ec.opoce.cellar.database.IRDFStoreRelationalGateway#selectModelsPerContext(eu.europa.ec.opoce.cellar.database.CmrTableName, java.util.List)
     */
    @Override
    public Map<String, Model> selectModelsPerContext(final CmrTableName table, final List<String> contexts) {
        final Map<String, Model> retModels = new HashMap<>();

        final Map<String, List<Triple>> triplesPerContext = this.getTriplesPerContext(table, contexts);
        for (final Entry<String, List<Triple>> triplesPerContextEntry : triplesPerContext.entrySet()) {
            retModels.put(triplesPerContextEntry.getKey(), ModelUtils.getModelFromTriples(triplesPerContextEntry.getValue()));
        }

        return retModels;
    }

    /**
     * Select complete context.
     *
     * @param table   the table
     * @param context the context
     * @return the map
     * @see eu.europa.ec.opoce.cellar.database.IRDFStoreRelationalGateway#selectCompleteContext(eu.europa.ec.opoce.cellar.database.CmrTableName, java.lang.String)
     */
    @Override
    public Map<Pair<Triple, String>, String> selectCompleteContext(final CmrTableName table, final String context) {
        final Map<Pair<Triple, String>, String> results = new HashMap<>();

        final String query = SELECT + table.toString() + WHERE_CONTEXT_LIKE + WHERE_SUFFIX;
        this.database.getJdbcOperations().query(query, setStringParameters(context + '%'), getRowToQuintupleHandler(results));

        return results;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> selectCompleteContextNames(final CmrTableName table, final String context) {
        final String clusterin = context + ".%";
        final String query = "select distinct CONTEXT from " + table.toString() + " where context = '" + context + "' OR context like '"
                + clusterin + "'";
        return this.database.getJdbcOperations().queryForList(query, String.class);
    }

    /**
     * Checks if is context used in table.
     *
     * @param table   the table
     * @param context the context
     * @return true, if is context used in table
     * @see eu.europa.ec.opoce.cellar.database.IRDFStoreRelationalGateway#isContextUsedInTable(eu.europa.ec.opoce.cellar.database.CmrTableName, java.lang.String)
     */
    @Override
    public boolean isContextUsedInTable(final CmrTableName table, final String context) {
        final String query = "SELECT count(*) FROM " + table.toString() + " WHERE context = '" + context + "'";
        return this.database.getJdbcOperations().queryForObject(query, Integer.class) != 0;
    }

    // TODO: CELLARM-1121
    // place the queries in an application context to wire it and share through the different services
    // replace the following 4 methods by one that executes a given query

    /**
     * {@inheritDoc}
     */
    @Override
    public void dropVirtualModel(final String virtualModelName) {
        database.getJdbcOperations().execute(String.format("BEGIN SEM_APIS.DROP_VIRTUAL_MODEL('%s'); END;", virtualModelName));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dropSemModel(final String semModelName) {
        database.getJdbcOperations().execute(String.format("BEGIN SEM_APIS.DROP_SEM_MODEL('%s'); END;", semModelName));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dropEntailment(final String entailmentName) {
        database.getJdbcOperations().execute(String.format("BEGIN SEM_APIS.DROP_ENTAILMENT('%s'); END;", entailmentName));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dropTable(final String tableName) {
        database.getJdbcOperations().execute(String.format("DROP table %s ", tableName));
    }

    /**
     * Statically selects the model from table {@code table} corresponding to the given {@code context}.
     *
     * @param table      the table to select the model from
     * @param context    the context of the model to select
     * @param connection the connection to use
     * @return the retrieved model
     */
    public static Model staticSelectModel(final CmrTableName table, final String context, final Connection connection) {
        final List<Triple> triples = new ArrayList<>();

        try (PreparedStatement selectStatement = connection.prepareStatement(SELECT + table.toString() + WHERE_CONTEXT_EQUALS + WHERE_SUFFIX)) {
            selectStatement.setString(1, context);
            try (ResultSet rs = selectStatement.executeQuery()) {
                while (rs.next()) {
                    triples.add(extractTripleFromResultSet(rs));
                }
            }
        } catch (final SQLException e) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.CMR_INTERNAL_ERROR).withCause(e)
                    .withMessage("Unable to select model from table '{}' for context '{}'").withMessageArgs(table, context).build();
        }

        return ModelUtils.getModelFromTriples(triples);
    }

    // ***************
    // private methods
    // ***************

    private Map<String, List<Triple>> getTriplesPerContext(final CmrTableName table, final List<String> contexts) {
        final Map<String, List<Triple>> triplesPerContext = new HashMap<>();

        IterableUtils.pageUp(contexts, this.cellarConfiguration.getCellarDatabaseInClauseParams(),
                pagedContexts -> {
                    final String sql = SELECT + table.toString() + WHERE_CONTEXT_IN + IterableUtils.toInClauseParams(pagedContexts)
                            + ") " + WHERE_SUFFIX;

                    try {
                        database.getJdbcOperations().query(sql,
                                getRowToQuadrupleHandler(triplesPerContext));
                    } catch (final DataAccessException dae) {
                        throw ExceptionBuilder.get(CellarException.class)
                                .withMessage("A problem occurred while retrieving inverse models for contexts {}.")
                                .withMessageArgs(pagedContexts).withCause(dae).build();
                    }
                }
        );

        return triplesPerContext;
    }

    private void internalDeleteContexts(final String tableName, final Connection connection, final List<String> contexts) {
        IterableUtils.pageUp(contexts, this.cellarConfiguration.getCellarDatabaseInClauseParams(),
                pagedContexts -> {
                    final String sql = "DELETE FROM  " + tableName + " WHERE CONTEXT in ( "
                            + IterableUtils.toInClauseParams(pagedContexts.size()) + ")";
                    try (OraclePreparedStatement removeStatement = (OraclePreparedStatement) connection.prepareStatement(sql)) {
                        int i = 1;
                        Collections.sort((List<String>) pagedContexts);
                        for (final String context : pagedContexts) {
                            removeStatement.setString(i++, context);
                        }
                        removeStatement.executeUpdate();
                    } catch (final SQLException e) {
                        throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.CMR_INTERNAL_ERROR).withCause(e)
                                .withMessage("Unable to remove contexts.").build();
                    }
                }
        );
    }

    private String writeQuadsToFile(final List<Pair<Triple, String>> quads) {
        final ArrayList<String> lines = new ArrayList<>();
        lines.add(getLine("context", "subject", "predicate", "object"));

        collect(quads,
                input -> getLine(input.getTwo(),
                        input.getOne().getSubject().toString(),
                        input.getOne().getPredicate().toString(),
                        input.getOne().getObject().toString()),
                lines);
        final File file = this.getNextQuadFile();
        try {
            FileUtils.writeLines(file, lines);
            return file.getAbsolutePath();
        } catch (final IOException e) {
            LOG.warn("writing to the file {} failed", file.getAbsolutePath());
            return null;
        }
    }

    private synchronized File getNextQuadFile() {
        File file = new File(this.cellarConfiguration.getCellarFolderRoot(), "quads/" + quadFileNumber++ + ".txt");
        while (file.exists()) {
            file = new File(this.cellarConfiguration.getCellarFolderRoot(), "quads/" + quadFileNumber++ + ".txt");
        }
        return file;
    }

    private static Comparator<Pair<Triple, String>> getQuadComparator() {
        return new Comparator<Pair<Triple, String>>() {

            private final TripleComparator tripleComparator = new TripleComparator();

            @Override
            public int compare(final Pair<Triple, String> thisPair, final Pair<Triple, String> otherPair) {
                final Triple thisPairTriple = thisPair.getOne();
                final Triple otherPairTriple = otherPair.getOne();
                int result = this.tripleComparator.compare(thisPairTriple, otherPairTriple);
                if (result != 0) {
                    return result;
                }
                final String otherPairString = otherPair.getTwo();
                final String thisPairString = thisPair.getTwo();
                result = thisPairString.compareTo(otherPairString);
                return result;
            }
        };
    }

    private static RowCallbackHandler getRowToTripleHandler(final Collection<Triple> triples) {
        return rs -> addResultSetToTriples(rs, triples);
    }

    private static RowCallbackHandler getRowToQuadrupleHandler(final Map<String, List<Triple>> triplesPerContext) {
        return rs -> addResultSetToQuadruples(rs, triplesPerContext);
    }

    private static RowCallbackHandler getRowToQuintupleHandler(final Map<Pair<Triple, String>, String> quintuples) {
        return rs -> addResultSetToQuintuples(rs, quintuples);
    }

    private static void addResultSetToTriples(final ResultSet rs, final Collection<Triple> triples) throws SQLException {
        triples.add(extractTripleFromResultSet(rs));
    }

    private static void addResultSetToQuadruples(final ResultSet rs, final Map<String, List<Triple>> quadruples) throws SQLException {
        final Triple triple = extractTripleFromResultSet(rs);
        final String context = rs.getString(11);

        final List<Triple> currTriples = quadruples.computeIfAbsent(context, k -> new ArrayList<>());
        currTriples.add(triple);
    }

    private static void addResultSetToQuintuples(final ResultSet rs, final Map<Pair<Triple, String>, String> quintuples)
            throws SQLException {
        final Triple triple = extractTripleFromResultSet(rs);
        final String rowId = rs.getString(10);
        final String context = rs.getString(11);

        quintuples.put(new Pair<>(triple, context), rowId);
    }

    private static Triple extractTripleFromResultSet(final ResultSet rs) throws SQLException {
        final String subjecttype = rs.getString(1);
        final String subject = rs.getString(2);
        final String propertytype = rs.getString(3);
        final String property = rs.getString(4);
        final String objecttype = rs.getString(5);
        final String objectliteral = rs.getString(6);
        final String objectlanguage = rs.getString(7);
        final Clob objectlong = rs.getClob(8);
        final String object = rs.getString(9);

        final Node subjectnode = createNodeFromQueryResult(subjecttype, null, null, null, subject);
        final Node propertynode = createNodeFromQueryResult(propertytype, null, null, null, property);
        final Node objectnode = createNodeFromQueryResult(objecttype, objectliteral, objectlanguage, objectlong, object);
        return new Triple(subjectnode, propertynode, objectnode);
    }

    private static void addTripleToInsert(final OraclePreparedStatement insertStatement, final String tableName, final String context,
                                          final Triple triple, final Connection connection) throws SQLException {
        insertStatement.clearParameters();
        insertStatement.setString(1, context);
        insertStatement.setString(2, tableName);
        insertStatement.setString(3, encode(triple.getSubject()));
        insertStatement.setString(4, encode(triple.getPredicate()));

        final Node object = triple.getObject();
        final String encoded = encode(object);

        Clob clobObj = null;
        try {
            if (object.isLiteral() && (encoded.getBytes().length > 4000)) {
                clobObj = connection.createClob();
                clobObj.setString(1, encoded);
                insertStatement.setClob(5, clobObj);
            } else {
                insertStatement.setString(5, encoded);
            }
            insertStatement.addBatch();
        } finally {
            if (clobObj != null) {
                OracleClob clob = (OracleClob) clobObj;
                if (clob.isOpen())
                    clob.close();
            }
        }
    }

    private static PreparedStatementSetter setStringParameters(final String... ins) {
        return ps -> {
            int i = 1;
            for (final String in : ins) {
                ps.setString(i++, in);
            }
        };
    }

    private static Node createNodeFromQueryResult(final String vtype, final String ltyp, final String lang, final java.sql.Clob clob,
                                                  final String val) throws SQLException {
        if ("URI".equals(vtype)) {
            return OracleUtils.createUriNode(val, oracleTranslator);
        } else if ("BLN".equals(vtype)) {
            return OracleUtils.createBNode(val, oracleTranslator);
        } else if ("LIT".equals(vtype)) {
            try {
                return OracleUtils.createLiteralNode(val, ltyp, lang, clob, oracleTranslator);
            } finally {
                if ((clob != null) && (clob instanceof OracleClob)) {
                    final OracleClob oclob = (OracleClob) clob;
                    if (oclob.isOpen()) {
                        oclob.close();
                    }
                }
            }
        } else {
            return null;
        }
    }

    private static String getLine(final String... data) {
        final StringBuilder builder = new StringBuilder();
        for (final String s : data) {
            builder.append(s).append("\t");
        }
        return builder.toString();
    }

    private static String encode(final Node node) {
        return oracleTranslator.encode(node, OracleTranslator.ADD_REMOVE);
    }

    private static boolean dissTableCanBeSynced(final CmrTableName table) {
        return table != null &&
                table.getMode() != CmrTableMode.DISS &&
                table.getDisseminationTable() != null;
    }

}
