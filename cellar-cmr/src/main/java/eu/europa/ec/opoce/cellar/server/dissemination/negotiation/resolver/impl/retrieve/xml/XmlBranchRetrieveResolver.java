/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.resolver.impl.xml
 *             FILE : XmlBranchDisseminationResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 20, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.retrieve.xml;

import eu.europa.ec.opoce.cellar.common.http.headers.IURLTokenable;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.FilterVariance;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.Structure;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.IDisseminationResolver;
import eu.europa.ec.opoce.cellar.server.service.DisseminationException;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 20, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class XmlBranchRetrieveResolver extends XmlRetrieveResolver {

    /**
     * Gets the.
     *
     * @param identifier the identifier
     * @param filterMode the filter mode
     * @param decoding the decoding
     * @param eTag the e tag
     * @param lastModified the last modified
     * @param provideResponseBody the provide response body
     * @return the i dissemination resolver
     */
    public static IDisseminationResolver get(final String identifier, final FilterVariance filterMode, final String decoding,
            final String eTag, final String lastModified, final boolean provideResponseBody) {
        return new XmlBranchRetrieveResolver(identifier, filterMode, decoding, eTag, lastModified, provideResponseBody);
    }

    /**
     * Instantiates a new xml branch retrieve resolver.
     *
     * @param identifier the identifier
     * @param filterMode the filter mode
     * @param decoding the decoding
     * @param eTag the e tag
     * @param lastModified the last modified
     * @param provideResponseBody the provide response body
     */
    private XmlBranchRetrieveResolver(final String identifier, final FilterVariance filterMode, final String decoding, final String eTag,
            final String lastModified, final boolean provideResponseBody) {
        super(identifier, filterMode, decoding, eTag, lastModified, provideResponseBody);
    }

    /** {@inheritDoc} */
    @Override
    protected void initDisseminationRequest() {
        super.initDisseminationRequest();

        if ((this.cellarResource.getCellarType() != DigitalObjectType.EXPRESSION)
                && (this.cellarResource.getCellarType() != DigitalObjectType.EVENT)) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage("Bad resource type for branch notice. [resource '{}'({})]")
                    .withMessageArgs(this.cellarResource.getCellarId(), this.cellarResource.getCellarType()).build();
        }
    }

    /**
     * Do handle dissemination request.
     *
     * @return the response entity
     * @see eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.retrieve.RetrieveResolver#doHandleDisseminationRequest()
     */
    @Override
    public ResponseEntity<?> doHandleDisseminationRequest() {
        final boolean isExpression = this.cellarResource.getCellarType() == DigitalObjectType.EXPRESSION;
        final List<LanguageBean> languageBeans = isExpression ? this.getLanguageBeanOfExpression(this.cellarResource)
                : new ArrayList<LanguageBean>();

        return this.disseminationService.doXmlBranchRequest(this.eTag, this.lastModified, this.cellarResource, this.decodingLanguageBean,
                languageBeans, this.filterMode.isFilterEnabled(), this.provideResponseBody);

    }

    /** {@inheritDoc} */
    @Override
    protected IURLTokenable getTypeStructure() {
        return Structure.BRANCH;
    }
}
