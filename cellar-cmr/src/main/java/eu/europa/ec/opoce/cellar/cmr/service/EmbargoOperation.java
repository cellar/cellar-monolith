/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.service
 *             FILE : EmbargoOperation.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 27 Jan 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.service;

import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;

import java.util.Date;
import java.util.List;

/**
 * <class_description> This class represents the object that holds the data necessary for the embargo operation.
 * <br/><br/>
 * <notes> 
 * <br/><br/>
 * ON : 27 Jan 2017
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class EmbargoOperation {

    /** The automatic disembargo. */
    private boolean automaticDisembargo;

    /** The object id can be cellar id or a production id. */
    private String objectId;

    /** The embargo date. */
    private Date embargoDate;

    /** The last modification date. */
    private Date lastModificationDate;

    /** The cellar resource. */
    private CellarResource cellarResource;

    /** The stored in private database. */
    private boolean storedInPrivateDatabase;

    /** The to be stored in private database. */
    private boolean toBeStoredInPrivateDatabase;

    /** The identifiers target list. */
    private List<Identifier> identifiersTargetList;

    /** The content identifier. */
    private ContentIdentifier contentIdentifier;

    /** The targeted resources. */
    private List<CellarResource> targetedResources;

    /** The targeted resources identifiers. */
    private List<String> targetedResourcesIdentifiers;

    /**
     * Checks if is automatic disembargo.
     *
     * @return the automaticDisembargo
     */
    public boolean isAutomaticDisembargo() {
        return automaticDisembargo;
    }

    /**
     * Sets the automatic disembargo.
     *
     * @param automaticDisembargo the automaticDisembargo to set
     */
    public void setAutomaticDisembargo(boolean automaticDisembargo) {
        this.automaticDisembargo = automaticDisembargo;
    }

    /**
     * Gets the object id.
     *
     * @return the object id
     */
    public String getObjectId() {
        return objectId;
    }

    /**
     * Sets the object id.
     *
     * @param objectId the new object id
     */
    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    /**
     * Gets the embargo date.
     *
     * @return the embargoDate
     */
    public Date getEmbargoDate() {
        return embargoDate;
    }

    /**
     * Sets the embargo date.
     *
     * @param embargoDate the embargoDate to set
     */
    public void setEmbargoDate(Date embargoDate) {
        this.embargoDate = embargoDate;
    }

    /**
     * Gets the last modification date.
     *
     * @return the lastModificationDate
     */
    public Date getLastModificationDate() {
        return lastModificationDate;
    }

    /**
     * Sets the last modification date.
     *
     * @param lastModificationDate the lastModificationDate to set
     */
    public void setLastModificationDate(Date lastModificationDate) {
        this.lastModificationDate = lastModificationDate;
    }

    /**
     * Gets the cellar resource.
     *
     * @return the cellarResource
     */
    public CellarResource getCellarResource() {
        return cellarResource;
    }

    /**
     * Sets the cellar resource.
     *
     * @param cellarResource the cellarResource to set
     */
    public void setCellarResource(CellarResource cellarResource) {
        this.cellarResource = cellarResource;
    }

    /**
     * Checks if is stored in private database.
     *
     * @return the storedInPrivateDatabase
     */
    public boolean isStoredInPrivateDatabase() {
        return storedInPrivateDatabase;
    }

    /**
     * Sets the stored in private database.
     *
     * @param storedInPrivateDatabase the storedInPrivateDatabase to set
     */
    public void setStoredInPrivateDatabase(boolean storedInPrivateDatabase) {
        this.storedInPrivateDatabase = storedInPrivateDatabase;
    }

    /**
     * Checks if is to be stored in private database.
     *
     * @return the toBeStoredInPrivateDatabase
     */
    public boolean isToBeStoredInPrivateDatabase() {
        return toBeStoredInPrivateDatabase;
    }

    /**
     * Sets the to be stored in private database.
     *
     * @param toBeStoredInPrivateDatabase the toBeStoredInPrivateDatabase to set
     */
    public void setToBeStoredInPrivateDatabase(boolean toBeStoredInPrivateDatabase) {
        this.toBeStoredInPrivateDatabase = toBeStoredInPrivateDatabase;
    }

    /**
     * Gets the identifiers target list.
     *
     * @return the identifiersTargetList
     */
    public List<Identifier> getIdentifiersTargetList() {
        return identifiersTargetList;
    }

    /**
     * Sets the identifiers target list.
     *
     * @param identifiersTargetList the identifiersTargetList to set
     */
    public void setIdentifiersTargetList(List<Identifier> identifiersTargetList) {
        this.identifiersTargetList = identifiersTargetList;
    }

    /**
     * Gets the content identifier.
     *
     * @return the contentIdentifier
     */
    public ContentIdentifier getContentIdentifier() {
        return contentIdentifier;
    }

    /**
     * Sets the content identifier.
     *
     * @param contentIdentifier the contentIdentifier to set
     */
    public void setContentIdentifier(ContentIdentifier contentIdentifier) {
        this.contentIdentifier = contentIdentifier;
    }

    /**
     * Gets the targeted resources identifiers.
     *
     * @return the targetedResourcesIdentifiers
     */
    public List<String> getTargetedResourcesIdentifiers() {
        return targetedResourcesIdentifiers;
    }

    /**
     * Sets the targeted resources identifiers.
     *
     * @param targetedResourcesIdentifiers the targetedResourcesIdentifiers to set
     */
    public void setTargetedResourcesIdentifiers(List<String> targetedResourcesIdentifiers) {
        this.targetedResourcesIdentifiers = targetedResourcesIdentifiers;
    }

    /**
     * Gets the targeted resources.
     *
     * @return the targetedResources
     */
    public List<CellarResource> getTargetedResources() {
        return targetedResources;
    }

    /**
     * Sets the targeted resources.
     *
     * @param targetedResources the targetedResources to set
     */
    public void setTargetedResources(List<CellarResource> targetedResources) {
        this.targetedResources = targetedResources;
    }

}
