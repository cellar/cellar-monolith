/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.redirect.service
 *             FILE : ISeeOtherService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 21, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.redirect.service;

import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.DisseminationRequest;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.InferenceVariance;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.Structure;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;

import org.springframework.http.ResponseEntity;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 21, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface ISeeOtherService {

    /**
     * Negotiate see other for link (link-format).
     * @param cellarResource the cellar resource
     * @return the response entity
     */
    ResponseEntity<Void> negotiateSeeOtherForLink(final CellarResourceBean cellarResource);

    /**
     * Negotiate see other for xml.
     *
     * @param cellarResource the cellar resource
     * @param structure the structure
     * @param decodingLanguage the decoding language
     * @param provideAlternates the provide alternates
     * @return the response entity
     */
    ResponseEntity<Void> negotiateSeeOtherForXml(CellarResourceBean cellarResource, Structure structure, LanguageBean decodingLanguage,
            boolean provideAlternates);

    /**
     * Negotiate see other for rdf.
     *
     * @param cellarResource the cellar resource
     * @param structure the structure
     * @param inferenceMode the inference mode
     * @param provideAlternates the provide alternates
     * @return the response entity
     */
    ResponseEntity<Void> negotiateSeeOtherForRdf(CellarResourceBean cellarResource, Structure structure, InferenceVariance inferenceMode,
            boolean provideAlternates,boolean normalized);

    /**
     * Negotiate see other for list.
     *
     * @param cellarResource the cellar resource
     * @param provideAlternates the provide alternates
     * @return the response entity
     */
    ResponseEntity<Void> negotiateSeeOtherForList(CellarResourceBean cellarResource, boolean provideAlternates);

    /**
     * Negotiate see other for zip.
     *
     * @param cellarResource the cellar resource
     * @param provideAlternates the provide alternates
     * @return the response entity
     */
    ResponseEntity<Void> negotiateSeeOtherForZip(CellarResourceBean cellarResource, boolean provideAlternates);

    /**
     * Negotiate see other for identifiers.
     *
     * @param cellarResource the cellar resource
     * @return the response entity
     */
    ResponseEntity<Void> negotiateSeeOtherForIdentifiers(CellarResourceBean cellarResource);

    /**
     * Negotiate see other for concept scheme.
     *
     * @param conceptSchemeId the concept scheme id
     * @return the response entity
     */
    ResponseEntity<Void> negotiateSeeOtherForConceptScheme(final String conceptSchemeId, final String conceptId,
            final DisseminationRequest disseminationRequest);

}
