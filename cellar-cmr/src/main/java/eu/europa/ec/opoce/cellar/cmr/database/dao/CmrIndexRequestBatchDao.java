/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.database.dao
 *             FILE : CmrIndexRequestBatchDao.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Nov 7, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.database.dao;

import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService.Priority;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.ExecutionStatus;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestBatch;

import java.util.Collection;
import java.util.Date;

/**
 * @author ARHS Developments
 * @version $Revision$
 */
public interface CmrIndexRequestBatchDao {

    /**
     * Find a list of {@link CmrIndexRequestBatch} based on its status, priority and the
     * date of creation.
     *
     * @param executionStatus  the {@link ExecutionStatus} of the {@link CmrIndexRequestBatch}
     * @param minimumPriority  the minimum priority to find. All entries in the database with an
     *                         higher priority than that value are retrieved
     * @param maximumCreatedOn the date used to find the {@link CmrIndexRequestBatch}. All the
     *                         entries created <strong>after</strong> this date are ignored.
     * @param limit            the maximum number of results
     * @return the {@link CmrIndexRequestBatch} found or empty if nothing has been found.
     */
    Collection<CmrIndexRequestBatch> findRequests(ExecutionStatus executionStatus, Priority minimumPriority, Date maximumCreatedOn, int limit);
}
