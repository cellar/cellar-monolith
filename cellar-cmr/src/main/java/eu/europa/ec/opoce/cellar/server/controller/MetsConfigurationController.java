package eu.europa.ec.opoce.cellar.server.controller;

import eu.europa.ec.opoce.cellar.cl.configuration.PrefixConfigurationService;
import eu.europa.ec.opoce.cellar.server.spring.XmlView;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.View;

import com.thoughtworks.xstream.XStream;

//@Controller

/**
 * <p>MetsConfigurationController class.</p>
 */
public class MetsConfigurationController {

    @Autowired(required = true)
    private PrefixConfigurationService prefixConfigurationService;

    /**
     * <p>getPrefixConfig.</p>
     *
     * @return a {@link org.springframework.web.servlet.View} object.
     */
    @RequestMapping(value = "/mets/prefixconfig", method = RequestMethod.GET)
    public View getPrefixConfig() {
        XStream xStream=new XStream();
        xStream.setMode(XStream.NO_REFERENCES);
        String structMapXml = xStream.toXML(prefixConfigurationService.getPrefixUris());
        return new XmlView(structMapXml);
    }
}
