/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver
 *             FILE : AbstractResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 1, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver;

import eu.europa.ec.opoce.cellar.common.http.headers.IURLTokenable;
import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;
import eu.europa.ec.opoce.cellar.server.service.DisseminationException;
import eu.europa.ec.opoce.cellar.server.service.DisseminationService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Apr 1, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Configurable
public abstract class AbstractResolver implements IDisseminationResolver {

    /** The cellar resource. */
    protected CellarResourceBean cellarResource;

    /** The dissemination service. */
    @Autowired
    protected DisseminationService disseminationService;

    /**
     * Instantiates a new abstract resolver.
     */
    protected AbstractResolver() {
    }

    /**
     * BASE.
     */
    protected abstract void initDisseminationRequest();

    /** {@inheritDoc} */
    @Override
    public ResponseEntity<?> handleDisseminationRequest() {
        this.initDisseminationRequest();
        return this.doHandleDisseminationRequest();
    }

    /**
     * Do handle dissemination request.
     *
     * @return the response entity
     */
    protected abstract ResponseEntity<?> doHandleDisseminationRequest();

    /**
     * COMMON.
     *
     * @return the type structure
     */

    protected abstract IURLTokenable getTypeStructure();

    /**
     * Gets the language bean of expression.
     *
     * @param expression the expression
     * @return the language bean of expression
     */
    protected List<LanguageBean> getLanguageBeanOfExpression(final CellarResourceBean expression) {
        final List<LanguageBean> result = new ArrayList<LanguageBean>();
        final List<String> resourceLanguages = expression.getLanguages();
        for (final String resourceLanguage : resourceLanguages) {
            final LanguageBean languageBean = this.disseminationService.parseLanguageBean(resourceLanguage);
            if (languageBean != null) {
                result.add(languageBean);
            }
        }
        if (result.isEmpty()) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage("Cellar resource of type Expression '{}' with invalid languages: {}")
                    .withMessageArgs(expression.getCellarId(), StringUtils.join(resourceLanguages, ", ")).build();
        }
        return result;
    }
}
