/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.database.dao
 *             FILE : ModelLoadRequestDao.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Mar 17, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.database.dao;

import eu.europa.ec.opoce.cellar.cl.dao.BaseDao;
import eu.europa.ec.opoce.cellar.semantic.helper.StringHelper;
import eu.europa.ec.opoce.cellar.virtuosobackup.VirtuosoBackup;

import java.util.Collection;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Mar 17, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface VirtuosoBackupDao extends BaseDao<VirtuosoBackup, Long> {

    static final String TABLE_NAME = "VIRTUOSO_BACKUP";

    interface Column {

        String OBJECT_ID = "OBJECT_ID";
        String OPERATION_TYPE = "OPERATION_TYPE";
        String INSERTION_DATE = "INSERTION_DATE";
        String IS_RESYNCABLE = "IS_RESYNCABLE";
        String REASON = "REASON";
        String MODEL = "MODEL";
        String SKOS_MODEL = "SKOS_MODEL";
        String PRODUCTION_ID = "PRODUCTION_ID";
        String URI = "URI";
        String BACKUP_TYPE = "BACKUP_TYPE";
        String RULE_SET_QUERY = "RULE_SET_QUERY";
    }

    String WHERE_PART_OBJECT_ID = StringHelper.format("{} = :objectId", Column.OBJECT_ID);

    String WHERE_PART_IS_RESYNCABLE = StringHelper.format("{} = :isResyncable", Column.IS_RESYNCABLE);

    Collection<VirtuosoBackup> findByObjectId(final String objectId);

    Collection<VirtuosoBackup> findAllSortedByInsertionDateAsc(final int limit, final boolean isResyncable);

    long countAllResyncable();

    VirtuosoBackup findLastBackup(final String objectId);

}
