/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.indexing.processor.impl
 *             FILE : CalcEmbeddedIndexRequestProcessorImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Nov 17, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.indexing.processor.impl;

import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.notice.embedded.EmbeddedNoticeService;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.ExecutionStatus;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.RequestType;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestBatch;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Nov 17, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component
@Qualifier("group")
@Order(30)
public class CalcEmbeddedIndexRequestProcessor extends AbstractIndexRequestProcessor {

    private static final Logger LOG = LogManager.getLogger(CalcEmbeddedIndexRequestProcessor.class);

    private final IdentifierService identifierService;

    private final EmbeddedNoticeService embeddedNoticeService;

    @Autowired
    public CalcEmbeddedIndexRequestProcessor(@Qualifier("pidManagerService") IdentifierService identifierService,
                                             EmbeddedNoticeService embeddedNoticeService) {
        this.identifierService = identifierService;
        this.embeddedNoticeService = embeddedNoticeService;
    }

    @Override
    protected void execute(CmrIndexRequestBatch group, List<CmrIndexRequest> requests) {
        final String cellarId = this.identifierService.getCellarPrefixedOrNull(group.getGroupByUri());
        if (StringUtils.isBlank(cellarId)) {
            LOG.warn("The identifier '{}' that was added is already removed.", group.getGroupByUri());
        } else {
            this.embeddedNoticeService.refreshTreeEmbeddedNotices(cellarId);
            LOG.info("The embedded notice of '{}' has been refreshed.", cellarId);
        }
    }

    @Override
    protected List<CmrIndexRequest> filter(CmrIndexRequestBatch request) {
        return request.getCmrIndexRequests().stream()
                .filter(r -> r.getExecutionStatus() == ExecutionStatus.Execution && r.getRequestType() == RequestType.CalcEmbedded)
                .collect(Collectors.toList());
    }

}
