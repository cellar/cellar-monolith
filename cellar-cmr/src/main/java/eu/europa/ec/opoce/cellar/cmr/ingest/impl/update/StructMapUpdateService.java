/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.cmr.ingest.impl.update
 *        FILE : StructMapUpdateService.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 11-04-2013
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.ingest.impl.update;

import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.response.IOperation;
import eu.europa.ec.opoce.cellar.cl.service.client.CellarIdentifierService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.EmbargoDatabaseService;
import eu.europa.ec.opoce.cellar.cmr.MetaDataIngestionHelper;
import eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData;
import eu.europa.ec.opoce.cellar.cmr.ingest.IStructMapLoader;
import eu.europa.ec.opoce.cellar.cmr.ingest.impl.AbstractStructMapService;
import eu.europa.ec.opoce.cellar.cmr.model.ModelUtils;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestGenerationService;
import eu.europa.ec.opoce.cellar.cmr.transformers.DigitalObject2CellarUri;
import eu.europa.ec.opoce.cellar.common.metadata.IModelLoader;
import eu.europa.ec.opoce.cellar.domain.content.mets.*;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.S3OperationException;
import eu.europa.ec.opoce.cellar.s3.ContentStreamExceptions;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.semantic.jena.StatementIterable;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.Lang;
import org.apache.jena.vocabulary.OWL;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

import static eu.europa.ec.opoce.cellar.cmr.ContentType.DIRECT_INFERRED;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 11-04-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class StructMapUpdateService extends AbstractStructMapService {

    /**
     * Constant <code>log</code>
     */
    private static final Logger LOG = LogManager.getLogger(StructMapUpdateService.class);

    @Autowired
    private MetaDataIngestionHelper metaDataIngestionHelper;

    @Autowired
    private IStructMapLoader structMapLoader;

    @Autowired
    private CmrIndexRequestGenerationService cmrIndexRequestGenerationService;

    @Autowired
    private EmbargoDatabaseService embargoDatabaseService;

    @Autowired
    private CellarIdentifierService cellarIdentifierService;

    /**
     * this method updates the indicated metadata in the cmr database for a given structmap and mets-package
     *
     * @param metsPackage metadata that needs to be updated or created
     * @param structMap   structmap that indicates how the update needs to be executed
     */
    @Override
    public void execute(final CalculatedData calculatedData, final IOperation<?> operation) {
        updateTimed(calculatedData, operation);
    }

    private void updateTimed(final CalculatedData calculatedData, final IOperation<?> operation) {
        final StructMap structMap = calculatedData.getStructMap();

        // check that needed parts are filled in.
        this.checkCellarIdFilled(structMap.getDigitalObject());
        final DigitalObject digitalObject = structMap.getDigitalObject();

        this.fill(digitalObject);
        // add contentstreams from the digital object information

        // tree update so items can be removed, added or replaced
        if (StructMapTypeHelper.isTreeType(structMap.getMetadataOperationType())) {
            final List<DigitalObject> removedDigitalObjects = new ArrayList<>();

            // load the old struct map
            final Set<String> newCellarUris = CollectionUtils.collect(digitalObject.getAllChilds(true), DigitalObject2CellarUri.instance,
                    new HashSet<>());
            final StructMap oldStructMap = this.structMapLoader.load(calculatedData);

            // make sure an update is possible
            if (null == oldStructMap) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.INGESTION_SERVICE_ERROR)
                        .withMessage("Update for structMap id '{}' requested but not found").withMessageArgs(structMap.getId()).build();
            }
            // if no change in contentstreams is allowed, check and put them back from the old data
            if (StructMapTypeHelper.isTreeMetadataType(structMap)) {
                this.setTreeMetadataContentStreams(structMap, oldStructMap);
            }

            // find removed objects
            for (final DigitalObject oldDigitalObject : oldStructMap.getDigitalObject().getAllChilds(true)) {
                if (newCellarUris.contains(oldDigitalObject.getCellarId().getUri())) {
                    continue;
                }
                removedDigitalObjects.add(oldDigitalObject);
            }
            calculatedData.setRemovedDigitalObjects(removedDigitalObjects);
            this.log(LOG, structMap, "compared structmap '{}' with previous data", structMap.getId());
        }
        // merge the old and new data so there is a complete structmap, so that the inner links are correct
        else {
            this.structMapLoader.loadAndMerge(calculatedData);
            this.log(LOG, structMap, "merged structmap '{}' with previous data", structMap.getId());
        }

        // Init versions for all objects
        if (digitalObject.getVersions().isEmpty()) {
            digitalObject.applyStrategyOnSelfAndChildren(o -> {
                CellarResource resource = cellarResourceDao.findResourceWithEmbeddedNotice(o.getCellarId().getIdentifier());
                if (resource != null) {
                    o.setVersions(resource.getVersions());
                }
            });
        }

        // load data with the proper model loader
        final IModelLoader<CalculatedData> modelLoader = this.createModelLoader(structMap);
        modelLoader.load(calculatedData);

        try {
            // calculate data for database and S3
            this.metadataCalculator.calculate(calculatedData);
            this.log(LOG, structMap, "loaded all metadata in memory for structmap'{}'", structMap.getId());

            // calculate list of digital object with a change in production identifiers
            final Set<DigitalObject> updateId = new HashSet<>();
            this.calculateIdChanges(digitalObject, updateId);

            // ingest the calculated data in S3 and the database
            this.metaDataIngestionHelper.ingest(calculatedData, operation);

            if (!this.embargoDatabaseService.isMetadataSavedPrivate(structMap.getDigitalObject())) {
                // create requests for indexation
                this.cmrIndexRequestGenerationService.insertIndexingRequestsFromUpdateIngest(calculatedData, updateId);
                // not needed no delete of index possible there is always a replace, only remove is embargo and handled at different location
                this.log(LOG, structMap, "created requests for indexing for structmap '{}'", structMap.getId());
            }
            //TODO this code is located here temporary. It will be move when Virtuoso will be in the same transaction that S3
            this.metaDataIngestionHelper.ingestVirtuoso(calculatedData);
        } finally {
            modelLoader.closeQuietly();
        }
    }

    /**
     * <p>setTreeMetadataContentStreams.</p>
     *
     * @param toSet           a {@link eu.europa.ec.opoce.cellar.domain.content.mets.StructMap} object.
     * @param loadedStructMap a {@link eu.europa.ec.opoce.cellar.domain.content.mets.StructMap} object.
     */
    private void setTreeMetadataContentStreams(final StructMap toSet, final StructMap loadedStructMap) {
        //get all digital objects from old and new structure
        final Map<String, DigitalObject> toSetMap = this.mapize(toSet.getDigitalObject(), new HashMap<>());
        final Map<String, DigitalObject> sourceMap = this.mapize(loadedStructMap.getDigitalObject(), new HashMap<>());

        for (final Map.Entry<String, DigitalObject> entry : toSetMap.entrySet()) {
            //check that content streams are not defined
            final List<ContentStream> toSetContentStreams = entry.getValue().getContentStreams();
            //TODO: set correct errorcode
            if (!toSetContentStreams.isEmpty()) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.INGESTION_INVALID_UPDATE_OPERATION)
                        .withMessage("Metadata.tree update should not contain content").build();
            }

            //add original content streams
            final DigitalObject sourceDigitalObject = sourceMap.remove(entry.getKey());
            if (null == sourceDigitalObject) {
                continue;
            }
            final List<ContentStream> contentStreams = sourceDigitalObject.getContentStreams();
            if (null == contentStreams) {
                continue;
            }
            toSetContentStreams.addAll(contentStreams);
        }

        for (final DigitalObject digitalObject : sourceMap.values()) {
            //error when content would be removed underneed a digital object that is removed, not allowed if no tree content request
            if (!digitalObject.getContentStreams().isEmpty()) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.INGESTION_INVALID_UPDATE_OPERATION)
                        .withMessage("Metadata.tree tried to remove data containing content: {}")
                        .withMessageArgs(digitalObject.getCellarId().getUri()).build();
            }
        }
    }

    /**
     * <p>mapize gives all cellar-uris and according digital objects starting from a digital object.</p>
     *
     * @param digitalObject a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject} object.
     * @param result        a {@link java.util.Map} object.
     * @return a {@link java.util.Map} object.
     */
    private Map<String, DigitalObject> mapize(final DigitalObject digitalObject, final Map<String, DigitalObject> result) {
        result.put(digitalObject.getCellarId().getUri(), digitalObject);
        for (final DigitalObject object : digitalObject.getChildObjects()) {
            this.mapize(object, result);
        }
        return result;
    }

    /**
     * <p>fill complete the complete structure with items from the metadata.</p>
     *
     * @param digitalObject a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject} object.
     */
    private void fill(final DigitalObject digitalObject) {
        for (final DigitalObject child : digitalObject.getChildObjects()) {
            this.fill(child);
        }

        final String identifier = digitalObject.getCellarId().getIdentifier();
//        if (!digitalObject.getContentStreams().isEmpty()) {
//            CellarResource resource = cellarResourceDao.findCellarId(digitalObject.getCellarId().getIdentifier());
//            if (resource != null) {
//                digitalObject.setVersions(resource.getVersions());
//            }
//        }

        for (final ContentStream contentStream : digitalObject.getContentStreams()) {
            if (contentStream.getFileRef() != null) {
                continue;
            }
            final String version = digitalObject.getVersions().get(DIRECT_INFERRED);
            final String directInferred = getContentStreamService().getContentAsString(identifier, version, DIRECT_INFERRED)
                    .orElseThrow(ContentStreamExceptions.notFound(identifier, version, DIRECT_INFERRED));

            final Model model = JenaUtils.read(directInferred, Lang.NT);
            try {
                final ContentIdentifier contentStreamCellarId = contentStream.getCellarId();
                //put value of mimetypeproperty in item
                final String uri = contentStreamCellarId.getUri();
                final Resource createResource = model.createResource(uri);
                final Statement requiredProperty = model.getRequiredProperty(createResource, CellarProperty.cmr_manifestationMimeTypeP);
                final RDFNode object = requiredProperty.getObject();
                final String mimeType = JenaUtils.stringize(object);
                contentStream.setMimeType(mimeType);
            } finally {
                JenaUtils.closeQuietly(model);
            }
        }
        if (!Boolean.TRUE.equals(digitalObject.getReadOnly())) {
            final CellarIdentifier cellarIdentifier = cellarIdentifierService.getCellarIdentifier(identifier);
            digitalObject.setReadOnly(cellarIdentifier.getReadOnly());
        }
    }

    /**
     * <p>calculateIdChanges looks for all digital objects that have change in identifiers.</p>
     *
     * @param digitalObject         a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject} object.
     * @param ObjectsWhereIdChanged a {@link java.util.Set} object.
     */
    private void calculateIdChanges(final DigitalObject digitalObject, final Set<DigitalObject> ObjectsWhereIdChanged) {
        for (final DigitalObject childDigitalObject : digitalObject.getChildObjects()) {
            this.calculateIdChanges(childDigitalObject, ObjectsWhereIdChanged);
        }

        if ((null != digitalObject.getBusinessMetadata()) && this.isUriChanged(digitalObject)) {
            ObjectsWhereIdChanged.add(digitalObject);
        }
    }

    /**
     * <p>isUriChanged gives indication if there are uris defined that where not defined before.</p>
     *
     * @param digitalObject a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject} object.
     * @return a boolean.
     */
    private boolean isUriChanged(final DigitalObject digitalObject) {
        final Set<String> currentUris = this.getCurrentUris(digitalObject);
        final Set<String> newUris = digitalObject.getAllUris();

        if (currentUris.size() != newUris.size()) {
            return true;
        }
        currentUris.removeAll(newUris);
        return !currentUris.isEmpty();
    }

    /**
     * <p>getCurrentUris gives a list of all current identifiers stored in s3.</p>
     *
     * @param digitalObject a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject} object.
     * @return a {@link java.util.Set} object.
     */
    private Set<String> getCurrentUris(final DigitalObject digitalObject) {
        final Model currentSameAsModel = getSameAsModelFromDmd(digitalObject);
        try {
            final Set<String> result = new HashSet<>();
            for (final Statement statement : StatementIterable.list(currentSameAsModel, null, OWL.sameAs, null)) {
                result.add(statement.getResource().getURI());
                result.add(statement.getSubject().getURI());
            }
            return result;
        } finally {
            JenaUtils.closeQuietly(currentSameAsModel);
        }
    }

    private Model getSameAsModelFromDmd(final DigitalObject digitalObject) {
        Model result = ModelFactory.createDefaultModel();
        final ContentIdentifier cellarId = digitalObject.getCellarId();
        final Model directModel = getContentStreamService().getContentAsString(cellarId.getIdentifier(), digitalObject.getVersions().get(ContentType.DIRECT), ContentType.DIRECT)
                .map(s -> JenaUtils.read(s, Lang.NT))
                .orElse(null);
        if (directModel != null) {
            final String uri = cellarId.getUri();
            result = ModelUtils.getStamentsFromResourceAsModel(directModel, uri, OWL.sameAs);

            if (result.isEmpty()) {
                throw ExceptionBuilder.get(S3OperationException.class)
                        .withCode(CmrErrors.S3_NO_CONTENT_FOR_ID)
                        .withMessage("Unable to retrieve the same_as model from the DMD model for the digital object: {}")
                        .withMessageArgs(cellarId.getIdentifier())
                        .build();
            }
        }
        return result;

    }
}
