/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.content
 *             FILE : ZipRedirectResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 14, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.contentPool;

import eu.europa.ec.opoce.cellar.common.http.headers.IURLTokenable;
import eu.europa.ec.opoce.cellar.server.dissemination.AcceptLanguage;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.DisseminationRequest;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.Type;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.IDisseminationResolver;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;

import org.springframework.http.ResponseEntity;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Apr 14, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class ContentPoolZipRedirectResolver extends ContentPoolRedirectResolver {

    protected ContentPoolZipRedirectResolver(final CellarResourceBean cellarResource, final DisseminationRequest disseminationRequest,
            final String accept, final AcceptLanguage acceptLanguage, final boolean provideAlternates) {
        super(cellarResource, disseminationRequest, accept, acceptLanguage, provideAlternates);
    }

    public static IDisseminationResolver get(final CellarResourceBean cellarResource, final DisseminationRequest disseminationRequest,
            final String accept, final AcceptLanguage acceptLanguage, final boolean provideAlternates) {
        return new ContentPoolZipRedirectResolver(cellarResource, disseminationRequest, accept, acceptLanguage, provideAlternates);
    }

    @Override
    public ResponseEntity<Void> doHandleDisseminationRequest() {
        return this.seeOtherService.negotiateSeeOtherForZip(this.cellarResource, this.provideAlternates);
    }

    @Override
    protected IURLTokenable getTypeStructure() {
        return Type.ZIP;
    }

}
