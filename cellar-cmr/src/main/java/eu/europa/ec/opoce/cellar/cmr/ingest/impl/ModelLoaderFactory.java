/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.ingest.impl
 *             FILE : ModelLoaderFactory.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 07-04-2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.ingest.impl;

import eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData;
import eu.europa.ec.opoce.cellar.cmr.ingest.impl.create.CreateModelLoader;
import eu.europa.ec.opoce.cellar.cmr.ingest.impl.delete.DeleteModelLoader;
import eu.europa.ec.opoce.cellar.cmr.ingest.impl.update.MergeModelLoader;
import eu.europa.ec.opoce.cellar.cmr.ingest.impl.update.UpdateModelLoader;
import eu.europa.ec.opoce.cellar.common.factory.IFactory;
import eu.europa.ec.opoce.cellar.common.metadata.IModelLoader;
import eu.europa.ec.opoce.cellar.domain.content.mets.OperationSubType;
import eu.europa.ec.opoce.cellar.exception.NotImplementedExceptionBuilder;

import org.springframework.stereotype.Component;

/**
 * <class_description> Factory class that creates new instances of {@link eu.europa.ec.opoce.cellar.common.metadata.IModelLoader}
 * on the basis of the current operation's subtype.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 07-04-2014
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
@Component("modelLoaderFactory")
public class ModelLoaderFactory implements IFactory<IModelLoader<CalculatedData>, OperationSubType> {

    /**
     * @see eu.europa.ec.opoce.cellar.common.factory.IFactory#create(java.lang.Object)
     */
    @Override
    public IModelLoader<CalculatedData> create(final OperationSubType opSubType) {
        IModelLoader<CalculatedData> modelLoader = null;

        switch (opSubType) {
        case Create:
        case Append:
            modelLoader = new CreateModelLoader();
            break;
        case Update:
            modelLoader = new UpdateModelLoader();
            break;
        case Merge:
            modelLoader = new MergeModelLoader();
            break;
        case Delete:
            modelLoader = new DeleteModelLoader();
            break;
        default:
            throw NotImplementedExceptionBuilder.get().withMessage("Model loader is not implemented for opearion's sub-type '{}'.")
                    .withMessageArgs(opSubType).build();
        }

        return modelLoader;
    }
}
