package eu.europa.ec.opoce.cellar.cmr;

import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditBuilder;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventAction;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventProcess;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventType;
import eu.europa.ec.opoce.cellar.cl.domain.response.IOperation;
import eu.europa.ec.opoce.cellar.cmr.advice.ThrowExceptionForTestPurpose;
import eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData;
import eu.europa.ec.opoce.cellar.cmr.ingest.IStructMapService;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsPackage;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.domain.content.mets.impl.NonImplementedOperationTypeVisitor;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.MetadataValidationResultAwareException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * MetaDataIngestionServiceServer class.
 * </p>
 */
@Service
public class MetaDataIngestionServiceServer implements MetaDataIngestionService<CalculatedData> {

    private static final Logger LOG = LogManager.getLogger(MetaDataIngestionServiceServer.class);

    @Autowired
    @Qualifier("structMapCreateService")
    private IStructMapService structMapCreateService;

    @Autowired
    @Qualifier("structMapUpdateService")
    private IStructMapService structMapUpdateService;

    @Autowired
    @Qualifier("structMapDeleteService")
    private IStructMapService structMapDeleteService;

    /**
     * This method updates or creates all metadata in the cmr database for a
     * given structmap and mets-package, and log the operations performed into
     * {@code operation}.
     *
     * @param calculatedData the data with: - the metsPackage that needs to be updated or
     *                       created - the structMap that indicates how the update needs to
     *                       be executed
     */
    @Override
    @ThrowExceptionForTestPurpose
    public void ingest(final CalculatedData calculatedData, final IOperation<?> operation) {
        final MetsPackage metsPackage = calculatedData.getMetsPackage();
        final StructMap structMap = calculatedData.getStructMap();
        if (structMap == null) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument structmap cannot be null!")
                    .build();
        }

        //Log the start of process
        AuditBuilder.get(AuditTrailEventProcess.Ingestion).withAction(AuditTrailEventAction.Insert)
                .withStructMapStatusHistory(calculatedData.getStructMapStatusHistory())
                .withPackageHistory(calculatedData.getPackageHistory())
                .withType(AuditTrailEventType.Start)
                .withMessage("Begin insert of {} in the CCR")
                .withMessageArgs(structMap.getDigitalObject().getCellarId())
                .withLogger(LOG).withLogDatabase(false).logEvent();

        if (metsPackage == null) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("Required argument metsPackage cannot be null!").build();
        }

        final Map<String, String> contextMap = new HashMap<>();
        contextMap.put("metsdocument", metsPackage.getZipFile().getName());
        contextMap.put("structmap", structMap.getId());

        try {
            // make sure the logging happens in the ingest logfile
            final long startTime = System.currentTimeMillis();
            //Log the start of process
            AuditBuilder.get(AuditTrailEventProcess.Ingestion).withAction(AuditTrailEventAction.IngestionStructMap)
                    .withStructMapStatusHistory(calculatedData.getStructMapStatusHistory())
                    .withPackageHistory(calculatedData.getPackageHistory())
                    .withType(AuditTrailEventType.Start)
                    .withMessage("Start ingestion [structmap '{}' - metsdocument '{}']")
                    .withMessageArgs(structMap.getId(), structMap.getMetsDocument().getDocumentId())
                    .withLogger(LOG).withLogDatabase(false).logEvent();

            // ingest a clone in order that changing this structure doesn't
            // have an impact after returning
            calculatedData.cloneStructMap();
            calculatedData.resolveMetsPackage();
            calculatedData.getStructMap().getMetsDocument().getType()
                    .accept(new StructMapOperationTypeVisitor(calculatedData, operation), null);
            synchronize(structMap.getDigitalObject().getAllChilds(true),
                    calculatedData.getStructMap().getDigitalObject().getAllChilds(true));

            //Log the end of process
            AuditBuilder.get(AuditTrailEventProcess.Ingestion).withAction(AuditTrailEventAction.IngestionStructMap)
                    .withType(AuditTrailEventType.End)
                    .withStructMapStatusHistory(calculatedData.getStructMapStatusHistory())
                    .withPackageHistory(calculatedData.getPackageHistory())
                    .withDuration(System.currentTimeMillis() - startTime)
                    .withMessage("Start ingestion [structmap '{}' - metsdocument '{}']")
                    .withMessageArgs(structMap.getId(), structMap.getMetsDocument().getDocumentId())
                    .withLogger(LOG).withLogDatabase(false).logEvent();
        } catch (CellarException ce) {
            // there is a special handling of the MetadataValidationResultAwareException as an extra file
            // has to be added to the response package so we cannot hide it in the CellarException process
            if (ce.getCause() instanceof MetadataValidationResultAwareException) {
                throw (MetadataValidationResultAwareException)ce.getCause();
            } else {
                throw ce;
            }
        }

    }

    /**
     * <p>
     * synchronize so the original structure is updated only with these
     * properties.
     * </p>
     *
     * @param originals a {@link java.util.List} object.
     * @param clones    a {@link java.util.List} object.
     */
    private void synchronize(final List<DigitalObject> originals, final List<DigitalObject> clones) {
        for (final DigitalObject original : originals) {
            for (final DigitalObject clone : clones) {
                if (original.getCellarId().getIdentifier().equals(clone.getCellarId().getIdentifier())) {
                    original.setLanguages(clone.getLanguages());
                    original.setManifestationMimeType(clone.getManifestationMimeType());
                    original.setEmbargoDate(clone.getEmbargoDate());
                }
            }
        }
    }

    private class StructMapOperationTypeVisitor extends NonImplementedOperationTypeVisitor {

        private final CalculatedData calculatedData;
        private final IOperation<?> operation;

        StructMapOperationTypeVisitor(final CalculatedData calculatedData, final IOperation<?> operation) {
            this.calculatedData = calculatedData;
            this.operation = operation;
        }

        /**
         * @see eu.europa.ec.opoce.cellar.domain.content.mets.impl.NonImplementedOperationTypeVisitor#visitCreate(java.lang.Void)
         */
        @Override
        public Void visitCreate(final Void in) {
            structMapCreateService.execute(this.calculatedData, this.operation);
            return null;
        }

        /**
         * @see eu.europa.ec.opoce.cellar.domain.content.mets.impl.NonImplementedOperationTypeVisitor#visitCreateOrIgnore(java.lang.Void)
         */
        @Override
        public Void visitCreateOrIgnore(final Void in) {
            return this.visitCreate(in);
        }

        /**
         * @see eu.europa.ec.opoce.cellar.domain.content.mets.impl.NonImplementedOperationTypeVisitor#visitUpdate(java.lang.Void)
         */
        @Override
        public Void visitUpdate(final Void in) {
            structMapUpdateService.execute(this.calculatedData, this.operation);
            return null;
        }

        /**
         * @see eu.europa.ec.opoce.cellar.domain.content.mets.impl.NonImplementedOperationTypeVisitor#visitDelete(java.lang.Void)
         */
        @Override
        public Void visitDelete(final Void in) {
            structMapDeleteService.execute(this.calculatedData, this.operation);
            return null;
        }
    }

}
