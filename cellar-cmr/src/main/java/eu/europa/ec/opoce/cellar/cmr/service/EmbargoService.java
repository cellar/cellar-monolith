/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr
 *             FILE : IEmbargoHandlingService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 15, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.service;

import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.HierarchyElement;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement;
import eu.europa.ec.opoce.cellar.common.util.Pair;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;

import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 15, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface EmbargoService {

    /**
     * Gets the works eligible for disembargo.
     *
     * @return a pair where the first entry is the CELLAR identifier, and the second is the embargo date.
     */
    Set<Pair<String, Date>> getDisembargoeableDigitalObjects();

    /**
     * Gets the embargo hierarchyElement.
     *
     * @return the embargo hierarchyElement
     */
    List<HierarchyElement<MetsElement, MetsElement>> getEmbargoHierarchyElements();

    /**
     * Change embargo.
     *
     * @param cellarId the cellar id
     * @param embargoDate the embargo date
     */
    void changeEmbargo(String cellarId, Date embargoDate);

    /**
     * Gets the public hierarchy element.
     *
     * @param psi the psi
     * @return the public hierarchy element
     */
    HierarchyElement<MetsElement, MetsElement> getPublicHierarchyElement(final String psi);


    /**
     * Dis embargo.
     *
     * @param cellarId the cellar id
     * @param embargoDate the embargo date
     */
    void automaticDisembargo(final String cellarId, final Date embargoDate);

    /**
     * Validate embargo date on the ingested structmap.
     * Checks specific scenarios for operations MERGE and UPDATE node.metadata
     * If an embargo date is set it checks that that the entire WEMI hierarchy is declared
     * otherwise 
     *
     * @param ingestedStructMap the ingested struct map
     */
    void validateEmbargoDate(final StructMap ingestedStructMap);

    /**
     * Checks for all elements if their parents and grandparents are ok with their embargo date.
     * If a parent or grandparent imposes a higher embargo date then the child must not have a lower date
     * throws exception whenever there is an error
     * @param elements the elements
     */
    void validateEmbargoDate(final List<DigitalObject> elements);

    /**
     * Check if a digital object is under embargo
     * @param cellarId the cellar ID
     * @return true if the digital object is under embargo, false otherwise
     */
    boolean isUnderEmbargo(final String cellarId);

    /**
     * Check if a digital object is under embargo
     * @param resource the {@link CellarResource}
     * @return true if the digital object is under embargo, false otherwise
     */
    boolean isUnderEmbargo(CellarResource resource);

    void reschedule();

    /**
     *
     * @param cellarId
     * @param embargoDate
     * @param lastModificationDate
     * @param digitalObjectType
     */
    void updateEmbargoDate(String cellarId, Date embargoDate, Date lastModificationDate, DigitalObjectType digitalObjectType);
}
