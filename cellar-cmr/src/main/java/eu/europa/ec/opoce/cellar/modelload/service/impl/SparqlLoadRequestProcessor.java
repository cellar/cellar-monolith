package eu.europa.ec.opoce.cellar.modelload.service.impl;

import eu.europa.ec.opoce.cellar.feed.domain.impl.sparql.SparqlLoadHistory;
import eu.europa.ec.opoce.cellar.feed.service.HistoryService;
import eu.europa.ec.opoce.cellar.logging.LogContext;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.sparql.SparqlLoadRequest;
import eu.europa.ec.opoce.cellar.virtuoso.VirtuosoService;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.Lang;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import static eu.europa.ec.opoce.cellar.ExecutionStatus.Success;
import static eu.europa.ec.opoce.cellar.cmr.ContentType.DIRECT;
import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.CMR_NAL_LOAD;
import static eu.europa.ec.opoce.cellar.s3.ContentStreamExceptions.notFound;

/**
 * @author ARHS Developments
 */
@Component
public class SparqlLoadRequestProcessor {

    private final ContentStreamService contentStreamService;
    private final VirtuosoService virtuosoService;
    private final HistoryService historyService;

    @Autowired
    public SparqlLoadRequestProcessor(ContentStreamService contentStreamService, VirtuosoService virtuosoService,
                                      HistoryService historyService) {
        this.contentStreamService = contentStreamService;
        this.virtuosoService = virtuosoService;
        this.historyService = historyService;
    }

    @LogContext(CMR_NAL_LOAD)
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void process(SparqlLoadRequest request) {
        Model model = contentStreamService.getContentAsString(request.getExternalPid(), request.getVersion(), DIRECT)
                .map(s -> JenaUtils.read(s, Lang.NT))
                .orElseThrow(notFound(request.getExternalPid(), request.getVersion(), DIRECT));

        // Because of current AWS policy we cannot delete the data by using the version
        // but only use the delete marker
        contentStreamService.deleteContent(request.getExternalPid(), DIRECT);
		virtuosoService.write(model, request.getUri(), true);
        String pIds= String.join(",", this.historyService.getProductionIdentifierNamesForCellarId(request.getCellarId()));
        historyService.store(new SparqlLoadHistory(request.getUri(),request.getCellarId(),pIds,request.getVersion()));
        request.setExecutionStatus(Success);
    }

}
