package eu.europa.ec.opoce.cellar.cmr.database.dao.spring;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import org.apache.commons.lang.StringUtils;

import eu.europa.ec.opoce.cellar.cmr.database.dao.NalConfigDao;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.dao.NalConfig;
import eu.europa.ec.opoce.cellar.semantic.helper.StringHelper;

@Repository
public class SpringNalConfigDao extends CmrSpringBaseDao<NalConfig, Long> implements NalConfigDao {

    private final String getWhereClauseNalQuery = StringHelper.format("{} = :uri", Column.URI);
    private final String getWhereClausePidQuery = StringHelper.format("{} = :pid", Column.PID);
    private final String getWhereClauseNameQuery = StringHelper.format("{} = :name", Column.NAME);
    private final String getWhereClauseOntoQuery = StringHelper.format("{} = :ontology_uri", Column.ONTOLOGY_URI);
    private final String getWhereClauseOntoLikeQuery = StringHelper.format("{} like :ontology_uri ", Column.ONTOLOGY_URI);

    private interface Column {

        String URI = "URI";
        String NAME = "NAL_NAME";
        String ONTOLOGY_URI = "ONTOLOGY_URI";
        String CREATED_ON = "CREATED_ON";
        String VERSION_NR = "VERSION_NR";
        String VERSION_DATE = "VERSION_DATE";
        String STATUS = "STATUS";
        String PID = "FEDORA_PID";
        String VERSION = "VERSION";
        String VERSION_INFERRED = "VERSION_INFERRED";
    }

    public SpringNalConfigDao() {
        super("NALCONFIG",
                Arrays.asList(Column.URI, Column.ONTOLOGY_URI, Column.CREATED_ON, Column.VERSION_NR, Column.VERSION_DATE,
                        Column.PID, Column.NAME, Column.VERSION, Column.VERSION_INFERRED));
    }


    @Override
    public Optional<NalConfig> findByUri(final String uri) {
        if (StringUtils.isBlank(uri)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument uri must not be blank").build();
        }
        return Optional.ofNullable(findUnique(getWhereClauseNalQuery, Collections.singletonMap("uri", uri)));
    }

    @Override
    public Optional<NalConfig> findByPid(final String pid) {
        if (StringUtils.isBlank(pid)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument uri must not be blank").build();
        }
        return Optional.ofNullable(findUnique(getWhereClausePidQuery, Collections.singletonMap("pid", pid)));
    }

    @Override
    public Optional<NalConfig> findByName(final String name) {
        if (StringUtils.isBlank(name)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument name must not be blank").build();
        }
        return Optional.ofNullable(findUnique(getWhereClauseNameQuery, Collections.singletonMap("name", name)));
    }

    @Override
    public Collection<NalConfig> findByOntology(final String ontologyUri) {
        if (StringUtils.isBlank(ontologyUri)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument ontologyUri must not be blank").build();
        }
        return find(getWhereClauseOntoQuery, Collections.singletonMap("ontology_uri", ontologyUri));
    }

    @Override
    public Collection<NalConfig> findByOntologyLike(final String ontologyUri) {
        if (StringUtils.isBlank(ontologyUri)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument ontologyUri must not be blank").build();
        }
        return find(getWhereClauseOntoLikeQuery, Collections.singletonMap("ontology_uri", ontologyUri));
    }

    @Override
    public NalConfig create(final ResultSet rs) throws SQLException {
        NalConfig nalConfig = new NalConfig();
        nalConfig.setUri(rs.getString(Column.URI));
        nalConfig.setName(rs.getString(Column.NAME));
        nalConfig.setOntologyUri(rs.getString(Column.ONTOLOGY_URI));
        nalConfig.setCreatedOn(rs.getTimestamp(Column.CREATED_ON));
        nalConfig.setVersionNr(rs.getString(Column.VERSION_NR));
        nalConfig.setVersionDate(rs.getTimestamp(Column.VERSION_DATE));
        nalConfig.setExternalPid(rs.getString(Column.PID));
        nalConfig.setVersion(rs.getString(Column.VERSION));
        nalConfig.setVersionInferred(rs.getString(Column.VERSION_INFERRED));
        return nalConfig;
    }

    @Override
    public void fillMap(NalConfig nalConfig, Map<String, Object> map) {
        map.put(Column.URI, nalConfig.getUri());
        map.put(Column.NAME, nalConfig.getName());
        map.put(Column.ONTOLOGY_URI, nalConfig.getOntologyUri());
        map.put(Column.CREATED_ON, nalConfig.getCreatedOn());
        map.put(Column.VERSION_NR, nalConfig.getVersionNr());
        map.put(Column.VERSION_DATE, nalConfig.getVersionDate());
        map.put(Column.PID, nalConfig.getExternalPid());
        map.put(Column.VERSION, nalConfig.getVersion());
        map.put(Column.VERSION_INFERRED, nalConfig.getVersionInferred());
    }
}
