/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.nalinference.service
 *             FILE : SnippetGeneratorService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 12 28, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-12-28 14:18:57 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.nal.service.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration;
import eu.europa.ec.opoce.cellar.common.Namespace;
import eu.europa.ec.opoce.cellar.nal.NalOntoUtils;
import eu.europa.ec.opoce.cellar.nal.domain.NalRelatedBean;
import eu.europa.ec.opoce.cellar.nal.domain.NalSnippetBean;
import eu.europa.ec.opoce.cellar.nal.service.NalSnippetCreatorService;
import eu.europa.ec.opoce.cellar.nal.service.NalSnippetDbGateway;
import eu.europa.ec.opoce.cellar.nal.service.NalSparqlService;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.semantic.jena.template.JenaTemplate;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static eu.europa.ec.opoce.cellar.nal.utils.NalSparqlQueryTemplate.constructDecodingSnippetWithoutFallback;
import static eu.europa.ec.opoce.cellar.nal.utils.NalSparqlQueryTemplate.constructFallbackSnippet;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.at_code;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.cmr_facet_D;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.cmr_facet_M;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.cmr_facet_T;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.cmr_level;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.dc_identifier;
import static eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty.skos_broaderTransitive;

/**
 * @author ARHS Developments
 */
@Service
public class NalSnippetCreatorServiceImpl implements NalSnippetCreatorService {

    private static final Logger LOG = LogManager.getLogger(NalSnippetCreatorServiceImpl.class);

    private static final SnippetRetrieverClosure NAL_SNIPPET_RETRIEVER =
            new SnippetRetrieverClosure(dc_identifier, cmr_level, skos_broaderTransitive, at_code);

    private static final SnippetRetrieverClosure EUROVOC_SNIPPET_RETRIEVER =
            new SnippetRetrieverClosure(dc_identifier, cmr_facet_T, cmr_facet_D, cmr_facet_M, at_code);

    private static final SnippetRetrieverClosure EUROVOC_MTDOMAIN_SNIPPET_RETRIEVER =
            new SnippetRetrieverClosure(dc_identifier, at_code);

    private NalSnippetDbGateway nalSnippetGateway;
    private NalSparqlService nalSparqlService;


    @Autowired
    public NalSnippetCreatorServiceImpl(NalSnippetDbGateway nalSnippetGateway, NalSparqlService nalSparqlService) {
        this.nalSnippetGateway = nalSnippetGateway;
        this.nalSparqlService = nalSparqlService;
    }

    public void generate(Model inputModel, String conceptSchemeUri) {
        final boolean eurovoc = NalOntoUtils.EUROVOC_CONCEPTSCHEME_URI.equalsIgnoreCase(conceptSchemeUri);
        deleteFromRepository(conceptSchemeUri);

        // extract the list of concepts from the model
        List<Resource> resources = getConcepts(inputModel, conceptSchemeUri);
        final List<NalRelatedBean> relatedDOs = new ArrayList<>();
        if (eurovoc) {
            insertEurovocConcept(inputModel, conceptSchemeUri, resources);
            insertEurovocMicroThesauriAndDomain(inputModel, conceptSchemeUri);
            relatedDOs.addAll(getEurovocRelatedSnippet(inputModel));
        } else {
            insertAuthorityNalConcept(inputModel, conceptSchemeUri, resources);
            relatedDOs.addAll(getAuthorityRelatedSnippet(inputModel, conceptSchemeUri));
        }
        persistRelatedSnippets(relatedDOs);
    }

    private List<Resource> getConcepts(Model inputModel, String conceptSchemeUri) {
        long startTime = System.currentTimeMillis();
        LOG.debug(IConfiguration.NAL, "Started retrieving concepts to decode for concept scheme URI '{}'.", conceptSchemeUri);
        List<Resource> resources = nalSparqlService.getConceptsFor(inputModel, conceptSchemeUri);
        LOG.debug(IConfiguration.NAL,"Finished retrieving {} concepts to decode for concept scheme URI '{}' in {} ms.",
                resources.size(), conceptSchemeUri, (System.currentTimeMillis() - startTime));
        return resources;
    }

    private List<NalRelatedBean> getAuthorityRelatedSnippet(Model inputModel, String conceptSchemeUri) {
        long startTime = System.currentTimeMillis();
        LOG.debug(IConfiguration.NAL,"Calculating related snippets...");
        List<NalRelatedBean> relatedSnippetList = nalSparqlService.getBroaderTransitives(inputModel, conceptSchemeUri);
        LOG.debug(IConfiguration.NAL,"Finished calculating '{}' related snippets in {} ms.", relatedSnippetList.size(), (System.currentTimeMillis() - startTime));
        return relatedSnippetList;
    }

    private void insertAuthorityNalConcept(Model inputModel, String conceptSchemeUri, List<Resource> resources) {
        long startTime = System.currentTimeMillis();
        LOG.debug(IConfiguration.NAL,"Starting decoding concepts.");
        calculateAndInsertTemplate(conceptSchemeUri, inputModel, resources, NAL_SNIPPET_RETRIEVER);
        LOG.debug(IConfiguration.NAL,"'{}' concepts decoding finished in {} ms", resources.size(), (System.currentTimeMillis() - startTime));
    }

    private List<NalRelatedBean> getEurovocRelatedSnippet(Model inputModel) {
        long startTime = System.currentTimeMillis();
        LOG.debug(IConfiguration.NAL,"Started calculating related snippets for eurovoc...");
        List<NalRelatedBean> eurovocFacets = nalSparqlService.getFacets(inputModel);
        LOG.debug(IConfiguration.NAL,"Finished calculating related snippets for Eurovoc in {} ms.", (System.currentTimeMillis() - startTime));
        return eurovocFacets;
    }

    private void insertEurovocMicroThesauriAndDomain(Model inputModel, String conceptSchemeUri) {
        LOG.debug(IConfiguration.NAL,"Started querying for all domains and microthesauri...");
        long startTime = System.currentTimeMillis();
        List<Resource> resources = nalSparqlService.getEurovocMicrothesauri(inputModel);
        final int microThesauriSize = resources.size();
        resources.addAll(nalSparqlService.getEurovocDomains(inputModel));
        calculateAndInsertTemplate(conceptSchemeUri, inputModel, resources, EUROVOC_MTDOMAIN_SNIPPET_RETRIEVER);
        LOG.debug(IConfiguration.NAL,"Finished querying {} microthesauri and {} domain decoding finished in {} ms.",
                microThesauriSize, (resources.size() - microThesauriSize), (System.currentTimeMillis() - startTime));
    }

    private void insertEurovocConcept(Model inputModel, String conceptSchemeUri, List<Resource> resources) {
        long startTime = System.currentTimeMillis();
        LOG.debug(IConfiguration.NAL,"Started processing of Eurovoc concept decoding...");
        calculateAndInsertTemplate(conceptSchemeUri, inputModel, resources, EUROVOC_SNIPPET_RETRIEVER);
        LOG.debug(IConfiguration.NAL,"Finished processing of '{}' Eurovoc concept decoding finished in {} ms.",
                resources.size(), (System.currentTimeMillis() - startTime));
    }

    private void persistRelatedSnippets(final List<NalRelatedBean> relatedDOs) {
        long startTime = System.currentTimeMillis();
        LOG.debug(IConfiguration.NAL,"Related snippets to be inserted : {}", relatedDOs);
        LOG.debug(IConfiguration.NAL,"Started inserting {} related snippets...", relatedDOs.size());
        nalSnippetGateway.insertRelations(relatedDOs);
        LOG.debug(IConfiguration.NAL,"Finished inserting related snippets done in {} ms.", (System.currentTimeMillis() - startTime));
    }

    private void deleteFromRepository(String conceptSchemeUri) {
        long startTime = System.currentTimeMillis();
        LOG.debug(IConfiguration.NAL,"Starting deleting snippets for concept scheme '{}'.", conceptSchemeUri);

        int relatedSnippetsDeletedCount = nalSnippetGateway.deleteRelatedSnippetOfRelatedResourceUri(conceptSchemeUri);
        LOG.debug(IConfiguration.NAL,"'{}' related snippets were deleted for concept scheme '{}' in {} ms.",
                relatedSnippetsDeletedCount, conceptSchemeUri, (System.currentTimeMillis() - startTime));

        startTime = System.currentTimeMillis();
        int nalSnippetDeletedCount = nalSnippetGateway.deleteNalSnippetOfConceptScheme(conceptSchemeUri);
        LOG.debug(IConfiguration.NAL,"'{}' nal snippets were deleted for concept scheme '{}' in {} ms.",
                nalSnippetDeletedCount, conceptSchemeUri, (System.currentTimeMillis() - startTime));
    }

    private void calculateAndInsertTemplate(final String conceptScheme, final Model model, final Collection<Resource> resources,
                                            final SnippetRetrieverClosure closure) {
        LOG.debug(IConfiguration.NAL,"Started calculating snippets for concept scheme '{}'...", conceptScheme);
        final List<NalSnippetBean> nalSnippetDOs = new LinkedList<NalSnippetBean>();
        long startTime = System.currentTimeMillis();

        for (final Resource resource : resources) {
            nalSnippetDOs.add(this.createNalSnippet(conceptScheme, model, resource, closure));
        }
        LOG.debug(IConfiguration.NAL,"Finished calculating {} snippets for concept scheme '{}' in {} ms.",
                nalSnippetDOs.size(), conceptScheme, (System.currentTimeMillis() - startTime));

        this.persistNalSnippets(nalSnippetDOs);
    }

    private void persistNalSnippets(final List<NalSnippetBean> nalSnippetDOs) {
        long startTime = System.currentTimeMillis();
        LOG.debug(IConfiguration.NAL,"Started inserting {} decoding snippets...", nalSnippetDOs.size());
        nalSnippetGateway.insertSnippets(nalSnippetDOs);
        nalSnippetDOs.clear();
        LOG.debug(IConfiguration.NAL,"Finished inserting snippets done in {} ms.", (System.currentTimeMillis() - startTime));
    }

    private NalSnippetBean createNalSnippet(final String conceptScheme, final Model model, final Resource resource,
                                            final SnippetRetrieverClosure closure) {
        Model result = null;
        try {
            result = closure.calculateSnippet(model, resource.getURI());
            this.addPrefixesTo(result);
            StringWriter stringWriter = new StringWriter();
            RDFDataMgr.write(stringWriter, result, RDFFormat.RDFXML_PLAIN);
            return new NalSnippetBean(resource.getURI(), conceptScheme, stringWriter.toString().getBytes(StandardCharsets.UTF_8));
        } finally {
            JenaUtils.closeQuietly(result);
        }
    }

    private void addPrefixesTo(final Model model) {
        Map<String, String> prefixes = new HashMap<>();
        prefixes.put("rdf", Namespace.rdf);
        prefixes.put("rdfs", Namespace.rdfs);
        prefixes.put("owl", Namespace.owl);
        prefixes.put("skos", Namespace.skos);
        prefixes.put("dc", Namespace.dc);
        prefixes.put("cmr", Namespace.cmr);
        prefixes.put("xsd", Namespace.xsd);
        model.setNsPrefixes(prefixes);
    }

    private static class SnippetRetrieverClosure {
        private final String[] properties;

        private SnippetRetrieverClosure(final String... properties) {
            this.properties = properties;
        }

        private Model calculateSnippet(final Model model, final String resource) {
            Model snippet1 = null;
            Model snippet2 = null;
            try {
                snippet1 = JenaTemplate.construct(model, constructDecodingSnippetWithoutFallback(resource, this.properties));
                snippet2 = JenaTemplate.construct(model, constructFallbackSnippet(resource));
                return JenaUtils.create(snippet1, snippet2);
            } finally {
                JenaUtils.closeQuietly(snippet1, snippet2);
            }
        }
    }
}
