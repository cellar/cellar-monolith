package eu.europa.ec.opoce.cellar.database;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

/**
 * Helper class to perform JDBC based database operations
 * Implements Strategy pattern to perform database specific operations.
 *
 * @see DatabaseStrategy
 * @see org.springframework.jdbc.core.JdbcOperations
 */
public class Database {

    private final DatabaseStrategy databaseStrategy;
    private final NamedParameterJdbcOperations namedParameterJdbcOperations;

    /**
     * Create a new Database for the given Spring NamedParameterJdbcOperations.
     *
     * @param namedParameterJdbcOperations the Spring NamedParameterJdbcOperations to wrap
     * @param databaseStrategy             a {@link eu.europa.ec.opoce.cellar.database.DatabaseStrategy} object.
     */
    public Database(DatabaseStrategy databaseStrategy, NamedParameterJdbcOperations namedParameterJdbcOperations) {
        this.databaseStrategy = databaseStrategy;
        this.namedParameterJdbcOperations = namedParameterJdbcOperations;
    }

    /**
     * Create a new Database for the given DataSource.
     * <p>Creates a classic Spring JdbcTemplate and wraps it.
     *
     * @param dataSource       the JDBC DataSource to access
     * @param databaseStrategy a {@link eu.europa.ec.opoce.cellar.database.DatabaseStrategy} object.
     */
    public Database(DatabaseStrategy databaseStrategy, DataSource dataSource) {
        this.databaseStrategy = databaseStrategy;
        this.namedParameterJdbcOperations = new NamedParameterJdbcTemplate(dataSource);
    }

    /**
     * Create a new NamedParameterJdbcOperations for the given classic Spring JdbcTemplate.
     *
     * @param classicJdbcTemplate the classic Spring JdbcTemplate to wrap
     * @param databaseStrategy    a {@link eu.europa.ec.opoce.cellar.database.DatabaseStrategy} object.
     */
    public Database(DatabaseStrategy databaseStrategy, JdbcOperations classicJdbcTemplate) {
        this.databaseStrategy = databaseStrategy;
        this.namedParameterJdbcOperations = new NamedParameterJdbcTemplate(classicJdbcTemplate);
    }

    /**
     * <p>getJdbcOperations.</p>
     *
     * @return a {@link org.springframework.jdbc.core.JdbcOperations} object.
     */
    public JdbcOperations getJdbcOperations() {
        return namedParameterJdbcOperations.getJdbcOperations();
    }

    /**
     * <p>Getter for the field <code>namedParameterJdbcOperations</code>.</p>
     *
     * @return a {@link org.springframework.jdbc.core.namedparam.NamedParameterJdbcOperations} object.
     */
    public NamedParameterJdbcOperations getNamedParameterJdbcOperations() {
        return namedParameterJdbcOperations;
    }

    /**
     * <p>format.</p>
     *
     * @param value a boolean.
     * @return a {@link java.lang.String} object.
     */
    public String format(boolean value) {
        return format(Boolean.valueOf(value));
    }

    /**
     * <p>format.</p>
     *
     * @param value a int.
     * @return a {@link java.lang.String} object.
     */
    public String format(int value) {
        return format(Integer.valueOf(value));
    }

    /**
     * <p>format.</p>
     *
     * @param value a {@link java.lang.Object} object.
     * @return a {@link java.lang.String} object.
     */
    public String format(Object value) {
        return databaseStrategy.format(value);
    }
}
