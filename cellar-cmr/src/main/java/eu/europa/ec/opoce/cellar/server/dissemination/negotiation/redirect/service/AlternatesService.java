/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.redirect.service.impl
 *             FILE : RedirectAlternatesCalculator.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 15, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.redirect.service;

import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.InferenceVariance;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.Structure;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.Type;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;

import java.util.Set;
import java.util.SortedSet;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Apr 15, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface AlternatesService {

    Set<String> getXmlAlternatesWithBaseUriPlaceHolder(DigitalObjectType resourceType, String extensionPattern, Structure structure,
            LanguageBean decodingLanguage);

    Set<String> getRdfAlternatesWithBaseUriPlaceHolder(DigitalObjectType resourceType, String extensionPattern, Structure structure,
            InferenceVariance inferenceVariance,boolean normalized);

    Set<String> getContentPoolAlternatesWithBaseUriPlaceHolder(CellarResourceBean cellarResource, String extensionPattern, Type type);

    SortedSet<String> getContentStreamAlternates(final String uri);

    SortedSet<String> applyBaseUriToAlternatesWithPlaceHolder(String baseUri, Set<String> alternateEntries);

}
