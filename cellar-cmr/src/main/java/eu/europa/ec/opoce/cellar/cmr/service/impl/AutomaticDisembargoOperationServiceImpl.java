/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.service
 *             FILE : AutomaticDisembargoOperationServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 27-Jan-2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */

package eu.europa.ec.opoce.cellar.cmr.service.impl;

import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.service.EmbargoOperation;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <class_description> This class represents the service called when the automatic disembargo operation is triggered.
 * <br/><br/>
 * <notes>
 * <br/><br/>
 * ON : 27-Jan-2017
 * The Class AutomaticDisembargoOperationServiceImpl.
 *
 * @author ARHS Developments
 * @version $$Revision$$$
 */
@Service("automaticDisembargoOperationService")
public class AutomaticDisembargoOperationServiceImpl extends EmbargoOperationAbstractServiceImpl {

    private static final Logger LOG = LogManager.getLogger(AutomaticDisembargoOperationServiceImpl.class);

    @Override
    public void loadTargetCellarResources(final EmbargoOperation embargoOperation) {
        final String cellarId = embargoOperation.getObjectId();
        final Date lastModificationDate = embargoOperation.getLastModificationDate();

        LOG.info("Selecting the resources eligible to be disembargoed under [{}] with the embargo date lower than [{}].",
                cellarId, lastModificationDate);
        final List<CellarResource> allResourcesWithLowerEmbargoDate = cellarResourceDao
                .getAllResourcesWithLowerEmbargoDate(cellarId, lastModificationDate);

        LOG.info("Gathering the CELLAR ids from the selected resources {}", allResourcesWithLowerEmbargoDate);
        final List<String> cellarIds = allResourcesWithLowerEmbargoDate.stream()
                .map(CellarResource::getCellarId)
                .collect(Collectors.toList());

        embargoOperation.setTargetedResources(allResourcesWithLowerEmbargoDate);
        embargoOperation.setTargetedResourcesIdentifiers(cellarIds);
    }

    @Override
    public void loadIdentifiersTargetList(final EmbargoOperation embargoOperation) {
        //NOP
    }

    @Override
    public void updateIndexes(EmbargoOperation embargoOperation) {
        LOG.info("Updating notice indexes");
        final ContentIdentifier contentIdentifier = embargoOperation.getContentIdentifier();
        updateIndexes(contentIdentifier, true, false);
    }

    @Override
    public void updateVirtuoso(final EmbargoOperation embargoOperation) {
        LOG.info("Updating Virtuoso");

        if (this.cellarConfiguration.isVirtuosoIngestionEnabled()) {
            final String cellarId = embargoOperation.getObjectId();
            final Date lastModificationDate = embargoOperation.getLastModificationDate();
            final CellarResource cellarResource = embargoOperation.getCellarResource();
            final List<String> targetedResourcesIdentifiers = embargoOperation.getTargetedResourcesIdentifiers();

            try {
                this.virtuosoService.unhide(targetedResourcesIdentifiers, lastModificationDate, lastModificationDate, true);
            } catch (final Exception e) {
                LOG.error("Unexpected exception was thrown when (dis)embargoing the digital object of type {"
                        + cellarResource.getCellarType() + "} and id {" + cellarId + "} in Virtuoso", e);
            }
        }
    }

    @Override
    public void updateOracleRdfStore(final EmbargoOperation embargoOperation) {
        LOG.info("Updating Oracle RDF Store");

        final Date lastModificationDate = embargoOperation.getLastModificationDate();
        final List<String> targetedResourcesIdentifiers = embargoOperation.getTargetedResourcesIdentifiers();

        LOG.info("Moving the following resources: {}", Arrays.toString(targetedResourcesIdentifiers.toArray()));
        this.embargoDatabaseService.moveEmbargoedData(
                targetedResourcesIdentifiers,
                false,
                lastModificationDate,
                lastModificationDate
        );
    }

    @Override
    public void updateCellarResource(final EmbargoOperation embargoOperation) {
        LOG.info("Updating cellar resources");

        final Date lastModificationDate = embargoOperation.getLastModificationDate();
        final List<String> targetedResourcesIdentifiers = embargoOperation.getTargetedResourcesIdentifiers();
        final List<CellarResource> targetedResources = embargoOperation.getTargetedResources();

        LOG.info("Updating the following resources: {}", Arrays.toString(targetedResourcesIdentifiers.toArray()));
        targetedResources.forEach(element -> {
            element.setEmbargoDate(lastModificationDate);
            element.setLastModificationDate(lastModificationDate);
            this.cellarResourceDao.saveOrUpdateObject(element);
        });
    }

    @Override
    public void updateS3(final EmbargoOperation embargoOperation) {
        LOG.info("Updating S3");
        final Date lastModificationDate = embargoOperation.getLastModificationDate();
        final List<CellarResource> targetedResources = embargoOperation.getTargetedResources();
        targetedResources.forEach(element -> {
            final String id = element.getCellarId();
            embargoService.updateEmbargoDate(id, lastModificationDate, lastModificationDate, element.getCellarType());
        });
    }

    @Override
    protected EmbargoOperation createEmbargoOperation(String cellarId, Date embargoDate) {
        EmbargoOperation op = new EmbargoOperation();
        op.setObjectId(cellarId);
        op.setEmbargoDate(embargoDate);
        op.setAutomaticDisembargo(true);
        return op;
    }

}
