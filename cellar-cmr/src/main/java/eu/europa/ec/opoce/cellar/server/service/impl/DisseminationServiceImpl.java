/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.server.service
 *        FILE : DisseminationServiceImpl.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 30-01-2012
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.service.impl;

import com.amazonaws.services.s3.Headers;
import com.github.rutledgepaulv.prune.Tree;
import com.google.common.base.Stopwatch;
import com.google.common.io.ByteStreams;
import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.ccr.service.DocumentAndOriginalCellarId;
import eu.europa.ec.opoce.cellar.ccr.service.DocumentAndOriginalCellarId.Type;
import eu.europa.ec.opoce.cellar.ccr.service.LanguagesNalSkosLoaderService;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.controller.util.ControllerUtil;
import eu.europa.ec.opoce.cellar.cl.dao.CellarIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.sparql.UriTemplates;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService;
import eu.europa.ec.opoce.cellar.cl.service.client.SparqlExecutingService;
import eu.europa.ec.opoce.cellar.cl.service.client.UriTemplatesService;
import eu.europa.ec.opoce.cellar.cl.service.client.XslTransformerService;
import eu.europa.ec.opoce.cellar.cmr.CellarIdentifierResourceService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Agent;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Content;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Dossier;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Event;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Expression;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Manifestation;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.TopLevelEvent;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Work;
import eu.europa.ec.opoce.cellar.cmr.notice.NoticeCache;
import eu.europa.ec.opoce.cellar.cmr.notice.dissemination.DisseminationNoticeService;
import eu.europa.ec.opoce.cellar.cmr.notice.xslt.tool.NoticeExpandingTransformer;
import eu.europa.ec.opoce.cellar.cmr.service.EmbargoService;
import eu.europa.ec.opoce.cellar.cmr.transformers.IscoCode2LanguageBean;
import eu.europa.ec.opoce.cellar.common.http.headers.HttpHeadersBuilder;
import eu.europa.ec.opoce.cellar.common.http.headers.directives.CacheControl;
import eu.europa.ec.opoce.cellar.common.http.headers.directives.Tcn;
import eu.europa.ec.opoce.cellar.common.util.CellarIdUtils;
import eu.europa.ec.opoce.cellar.common.util.Pair;
import eu.europa.ec.opoce.cellar.domain.content.mets.CellarIdentifiedObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaQueries;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import eu.europa.ec.opoce.cellar.server.conditionalRequest.ConditionalRequestVersions;
import eu.europa.ec.opoce.cellar.server.conditionalRequest.ETagType;
import eu.europa.ec.opoce.cellar.server.dissemination.DisseminationRequestUtils;
import eu.europa.ec.opoce.cellar.server.dissemination.RequestType;
import eu.europa.ec.opoce.cellar.server.dissemination.converter.DisseminationContentStreamData;
import eu.europa.ec.opoce.cellar.server.dissemination.converter.DisseminationContentStreams;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.redirect.service.AlternatesService;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;
import eu.europa.ec.opoce.cellar.server.service.DisseminationDbGateway;
import eu.europa.ec.opoce.cellar.server.service.DisseminationException;
import eu.europa.ec.opoce.cellar.server.service.DisseminationForIncompleteBacklogService;
import eu.europa.ec.opoce.cellar.server.service.DisseminationService;
import eu.europa.ec.opoce.cellar.server.service.IConditionalRequestService;
import eu.europa.ec.opoce.cellar.server.service.RetrievingService;
import eu.europa.ec.opoce.cellar.server.filter.DisseminationLogHelper;
import eu.europa.ec.opoce.cellar.virtuoso.util.VirtuosoNormalizer;
import eu.europa.ec.opoce.cellar.virtuoso.util.VirtuosoSkolemizer;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Timer;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.NodeIterator;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.riot.Lang;
import org.apache.jena.vocabulary.OWL;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.util.UriTemplate;
import org.w3c.dom.Document;

import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import static eu.europa.ec.opoce.cellar.cmr.ContentType.DIRECT;
import static eu.europa.ec.opoce.cellar.cmr.ContentType.DIRECT_INFERRED;
import static eu.europa.ec.opoce.cellar.cmr.ContentType.FILE;
import static java.util.Collections.singletonList;

/**
 * <class_description> Service class for disseminating documents.
 * <p>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc. ON : 02-08-2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class DisseminationServiceImpl implements DisseminationService {

    private static final Logger LOG = LogManager.getLogger(DisseminationServiceImpl.class);
    private static final String APPLICATION_SPARQL_RESULTS_XML = "application/sparql-results+xml";
    private static final String PREFIX_TEMP_FILE = "sparqlQuery";
    private static final String SUFFIX_TEMP_FILE = ".xml";
    private static final String CONTENT_STREAM_PREFIX = "contentStreams";
    private static final String MISSING_PARAM_DECODING_LANGUAGE ="missing param: decoding language";

    private static final String INVALID_TYPE_SPECIFIED="Invalid type specified";
    @Autowired
    @Qualifier("disseminationNoticeService")
    private DisseminationNoticeService disseminationNoticeService;

    @Autowired
    private LanguagesNalSkosLoaderService languagesNalSkosLoaderService;

    @Autowired
    private DisseminationDbGateway disseminationDbGateway;

    @Autowired
    @Qualifier("pidManagerService")
    private IdentifierService identifierService;

    @Autowired
    private CellarIdentifierResourceService cellarIdentifierResourceService;

    @Autowired
    private NoticeExpandingTransformer noticeExpandingTransformer;

    private IscoCode2LanguageBean iscoCode2LanguageBean;

    @Autowired
    private ContentStreamService contentStreamService;

    @Autowired
    private ICellarConfiguration cellarConfiguration;

    @Autowired
    private AlternatesService alternatesService;

    @Autowired
    private IConditionalRequestService conditionalRequestService;

    @Autowired
    private CellarIdentifierDao cellarIdentifierDao;

    @Autowired
    private SparqlExecutingService sparqlExecutingService;

    @Autowired
    private UriTemplatesService uriTemplateService;

    @Autowired
    private XslTransformerService xslTransformerService;

    @Autowired
    private DisseminationForIncompleteBacklogService disseminationImproveService;

    @Autowired
    private EmbargoService embargoService;

    @Autowired
    private RetrievingService retrievingService;

    @Autowired
    @Qualifier("pidManagerService")
    private PidManagerService pidManagerService;

    @Autowired
    private CellarResourceDao cellarResourceDao;

    private final Timer xsltTimer = Metrics.timer("dissemination.sparql.xslt.timer");

    private static DisseminationException createDisseminationException(Throwable cause, String message, Object...
            args) {
        return HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class)
                .withMessage(message)
                .withMessageArgs(args)
                .withCause(cause)
                .build();
    }

    private static boolean isZipContentStream(String mimeType) {
        return mimeType != null && mimeType.startsWith(DisseminationRequestUtils.ZIP.toString());
    }

    /**
     * [0]: manifestationPID
     * [1]: datastreamID
     *
     * @param element the {@link MetsElement}
     * @return an array with the manifestationPID and datastreamID
     */

    private static boolean isVisible(MetsElement element) {
        return element instanceof Content && ((Content) element).isVisible();
    }

    /**
     * <p>
     * init.
     * </p>
     */
    @PostConstruct
    private void init() {
        this.iscoCode2LanguageBean = new IscoCode2LanguageBean(this.languagesNalSkosLoaderService);
        deleteOldZipFiles();
    }

    /**
     * Delete old zip files.
     */
    private void deleteOldZipFiles() {
        if (Files.exists(Paths.get(cellarConfiguration.getCellarFolderTemporaryDissemination()))) {
            final Collection<File> oldAndNoMoreUsedContentStreamFiles = FileUtils.listFiles(
                    new File(this.cellarConfiguration.getCellarFolderTemporaryDissemination()),
                    FileFilterUtils.prefixFileFilter(CONTENT_STREAM_PREFIX), null);
            for (final File file : oldAndNoMoreUsedContentStreamFiles) {
                FileUtils.deleteQuietly(file);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResponseEntity<Document> identifierNotice(final String uris) {
        final String[] splittedUris = StringUtils.split(uris);
        final List<String> normalizedUris = new ArrayList<>();
        for (final String splittedUri : splittedUris) {
            if (this.identifierService.isUri(splittedUri)) {
                normalizedUris.add(splittedUri);
            } else {
                final String normalizedUri = splittedUri.replace('/', ':');
                normalizedUris.add(normalizedUri);
            }
        }

        if (LOG.isDebugEnabled()) {
            LOG.debug("# uris '{}', uris: ['{}']", normalizedUris.size(), StringUtils.join(normalizedUris, "','"));
        }

        final Model results = this.cellarIdentifierResourceService.getSameAsModel(normalizedUris);
        if (!results.isEmpty()) {
            final Document document = this.disseminationNoticeService.createIdentifierNotice(normalizedUris, results);
            return new ResponseEntity<>(document, HttpHeadersBuilder.get().withDateNow().getHttpHeaders(), HttpStatus.OK);
        } else {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                    .withMessage("Resource '{}' does not have non-empty sameas model.").withMessageArgs(uris).build();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isUnderEmbargo(final CellarResourceBean cellarResourceBean) {
        final String cellarId = StringUtils.split(cellarResourceBean.getCellarId(), ".")[0];
        return this.embargoService.isUnderEmbargo(cellarId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Nullable
    public CellarResourceBean checkForCellarResource(final String ps, final String id) {
        return this.checkForCellarResource(makePrefixUri(ps, id));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Nullable
    public CellarResourceBean checkForCellarResource(final String psId) {
        String cellarId;
        try {
            cellarId = this.identifierService.getCellarPrefixed(psId);
        } catch (final IllegalArgumentException e) {
            return null;
        }
        final CellarResource cellarResource = this.disseminationDbGateway.checkForResourceWithId(cellarId);

        return cellarResource == null ? null : new CellarResourceBean(cellarResource);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Nullable
    public LanguageBean parseLanguageBean(final String language) {
        if (StringUtils.isBlank(language)) {
            return null;
        }

        try {
            return this.iscoCode2LanguageBean.transform(language);
        } catch (final CellarException e) {
            LOG.warn("Unable to parse language bean for code '{}': '{}'", language, e.getMessage());
            return null;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Nullable
    public List<CellarResourceBean> checkForChildExpression(final CellarResourceBean cellarResourceBean, final LanguageBean language) {
        if (cellarResourceBean.getCellarType() != DigitalObjectType.WORK) {
            return null;
        }

        final List<CellarResource> expressions = this.disseminationDbGateway.checkForChildExpressions(cellarResourceBean.getCellarId(),
                language);
        List<CellarResourceBean> result = null;
        if ((expressions != null) && !expressions.isEmpty()) {
            result = expressions.stream().map(CellarResourceBean::new).collect(Collectors.toList());
        }

        return result;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Nullable
    public CellarResourceBean checkForParentExpression(final CellarResourceBean cellarResourceBean) {
        if ((cellarResourceBean.getCellarType() != DigitalObjectType.MANIFESTATION)
                && (cellarResourceBean.getCellarType() != DigitalObjectType.ITEM)) {
            return null;
        }

        final List<CellarResource> expressions = this.disseminationDbGateway.getExpressionsInHierarchy(cellarResourceBean.getCellarId());

        return (expressions != null) && expressions.isEmpty() ? null : new CellarResourceBean(expressions.iterator().next());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<CellarResourceBean> checkForChildManifestations(final CellarResourceBean cellarResourceBean) {
        if ((cellarResourceBean.getCellarType() != DigitalObjectType.WORK)
                && (cellarResourceBean.getCellarType() != DigitalObjectType.EXPRESSION)) {
            return Collections.emptyList();
        }

        final List<CellarResource> manifestations = this.disseminationDbGateway
                .getManifestationsInHierarchy(cellarResourceBean.getCellarId());
        return CollectionUtils.collect(manifestations, CellarResourceBean::new, new ArrayList<>(manifestations.size()));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResponseEntity<Document> doXmlTreeRequest(final String eTag, final String lastModified,
                                                     final CellarResourceBean cellarResourceBean, final LanguageBean decodingLanguage, final boolean filter,
                                                     final boolean provideResponseBody) {

        final String cellarId = cellarResourceBean.getCellarId();
        final DigitalObjectType type = cellarResourceBean.getCellarType();

        if ((type != DigitalObjectType.WORK) && (type != DigitalObjectType.DOSSIER) && (type != DigitalObjectType.AGENT)
                && (type != DigitalObjectType.TOPLEVELEVENT)) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage("Invalid content type tree for {}").withMessageArgs(type.toString()).build();
        }
        if (decodingLanguage == null) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage(MISSING_PARAM_DECODING_LANGUAGE).build();
        }

        Document document = null;
        ConditionalRequestVersions conditionalRequestVersions;

        if (type == DigitalObjectType.WORK) {
            final Work work = this.disseminationDbGateway.getWorkTree(cellarId);

            conditionalRequestVersions = this.conditionalRequestService.calculateVersionWithEmbedding(work, ETagType.TREE_WORK, eTag,
                    lastModified);

            if (conditionalRequestVersions.isChanged() && provideResponseBody) {
                document = this.disseminationNoticeService.createTreeNoticeForWork(work, decodingLanguage, filter);
            }
        } else if (type == DigitalObjectType.DOSSIER) {
            final Dossier dossier = this.disseminationDbGateway.getDossierTree(cellarId);

            conditionalRequestVersions = this.conditionalRequestService.calculateVersionWithEmbedding(dossier, ETagType.TREE_DOSSIER, eTag,
                    lastModified);

            if (conditionalRequestVersions.isChanged() && provideResponseBody) {
                document = this.disseminationNoticeService.createTreeNoticeForDossier(dossier, decodingLanguage, filter);
            }
        } else if (type == DigitalObjectType.TOPLEVELEVENT) {
            final TopLevelEvent topLevelEvent = this.disseminationDbGateway.getTopLevelEvent(cellarId);

            conditionalRequestVersions = this.conditionalRequestService.calculateVersionWithEmbedding(topLevelEvent,
                    ETagType.TREE_TOPLEVELEVENT, eTag, lastModified);

            if (conditionalRequestVersions.isChanged() && provideResponseBody) {
                document = this.disseminationNoticeService.createBranchNoticeForTopLevelEvent(topLevelEvent, decodingLanguage, filter);
            }
        } else {
            final Agent agent = this.disseminationDbGateway.getAgent(cellarId);

            conditionalRequestVersions = this.conditionalRequestService.calculateVersionWithEmbedding(agent, ETagType.TREE_AGENT, eTag,
                    lastModified);

            if (conditionalRequestVersions.isChanged() && provideResponseBody) {
                document = this.disseminationNoticeService.createBranchNoticeForAgent(agent, decodingLanguage, filter);
            }
        }

        final Document expandedDocument = document != null ? this.noticeExpandingTransformer.getExpandedNotice(document, null) : null;

        final HttpStatus httpStatus = conditionalRequestVersions.isChanged() ? HttpStatus.OK : HttpStatus.NOT_MODIFIED;

        final HttpHeadersBuilder httpHeadersBuilder = HttpHeadersBuilder.get().withDefaultNoticeHeaders(
                conditionalRequestVersions.getNewETag(), conditionalRequestVersions.getNewLastModified(),
                DisseminationRequestUtils.getMediaType(RequestType.TREE_NOTICE));

        return new ResponseEntity<>(expandedDocument, httpHeadersBuilder.getHttpHeaders(), httpStatus);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResponseEntity<Document> doXmlBranchRequest(final String eTag, final String lastModified,
                                                       final CellarResourceBean cellarResourceBean, final LanguageBean decodingLanguage, final List<LanguageBean> contentLanguages,
                                                       final boolean filter, final boolean provideResponseBody) {

        final String cellarId = cellarResourceBean.getCellarId();
        final DigitalObjectType type = cellarResourceBean.getCellarType();

        if ((type != DigitalObjectType.EXPRESSION) && (type != DigitalObjectType.EVENT)) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage("Invalid content type branch for {}").withMessageArgs(type.toString()).build();
        }
        if (decodingLanguage == null) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage(MISSING_PARAM_DECODING_LANGUAGE).build();
        }

        Document document = null;
        Set<LanguageBean> languages = new HashSet<>();
        ConditionalRequestVersions conditionalRequestVersions;

        if (type == DigitalObjectType.EVENT) {
            final Dossier dossier = this.disseminationDbGateway.getDossierBranch(cellarId);
            if ((dossier == null) || dossier.getChildren().isEmpty()) {
                throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                        .withMessage("Not found [event '{}' with unknown dossier]").withMessageArgs(cellarId).build();
            }

            conditionalRequestVersions = this.conditionalRequestService.calculateVersionWithEmbedding(dossier, ETagType.BRANCH_EVENT, eTag,
                    lastModified);

            if (conditionalRequestVersions.isChanged() && provideResponseBody) {
                document = this.disseminationNoticeService.createBranchNoticeForDossier(dossier, decodingLanguage, filter);

            }
        } else  {
            final Work work = this.disseminationDbGateway.getWorkBranch(cellarId);

            conditionalRequestVersions = this.conditionalRequestService.calculateVersionWithEmbedding(work, ETagType.BRANCH_EXPRESSION,
                    eTag, lastModified);

            if (conditionalRequestVersions.isChanged() && provideResponseBody) {
                document = this.disseminationNoticeService.createBranchNoticeForWork(work, decodingLanguage, contentLanguages, filter);
                final Expression expression = work.getChildren().iterator().next();
                languages = expression.getLangs();
            }
            
            DisseminationLogHelper.extendDisseminationLogEntries(cellarResourceBean, work, MediaType.APPLICATION_XML_VALUE, cellarResourceDao);
        }

        for (final LanguageBean language : languages) {
            document = this.noticeExpandingTransformer.getExpandedNotice(document, language);
        }

        final HttpStatus httpStatus = conditionalRequestVersions.isChanged() ? HttpStatus.OK : HttpStatus.NOT_MODIFIED;
        final HttpHeadersBuilder httpHeadersBuilder = HttpHeadersBuilder.get().withDefaultNoticeHeaders(
                conditionalRequestVersions.getNewETag(), conditionalRequestVersions.getNewLastModified(),
                DisseminationRequestUtils.getMediaType(RequestType.BRANCH_NOTICE));

        return new ResponseEntity<>(document, httpHeadersBuilder.getHttpHeaders(), httpStatus);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResponseEntity<Document> doXmlObjectRequest(final String eTag, final String lastModified,
                                                       final CellarResourceBean cellarResourceBean, final LanguageBean decodingLanguage, final boolean filter,
                                                       final boolean provideResponseBody) {
        if (null == decodingLanguage) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage(MISSING_PARAM_DECODING_LANGUAGE).build();
        }

        final MetsElement object = this.cellarIdentifierResourceService.getElement(cellarResourceBean);
        if (DigitalObjectType.TOPLEVELEVENT.equals(object.getType())) {
            object.setType(DigitalObjectType.EVENT);
        }

        final ConditionalRequestVersions conditionalRequestVersions = this.conditionalRequestService.calculateVersionWithEmbedding(object,
                ETagType.OBJECT, eTag, lastModified);
        Document document = null;
        Work workBranch = null;
        if (conditionalRequestVersions.isChanged() && provideResponseBody) {
            document = this.disseminationNoticeService.createObjectNotice(object, decodingLanguage, filter);

            Set<LanguageBean> languages = null;
            if ((object.getType() == DigitalObjectType.EXPRESSION) || (object.getType() == DigitalObjectType.MANIFESTATION)) {
                workBranch = this.disseminationDbGateway.getWorkBranch(cellarResourceBean.getCellarId());
                final Expression expression = workBranch.getChildren().iterator().next();
                languages = expression.getLangs();
                if (languages != null) {
                    for (final LanguageBean language : languages) {
                        document = this.noticeExpandingTransformer.getExpandedNotice(document, language);
                    }
                }
            }
            if (languages == null) {
                document = this.noticeExpandingTransformer.getExpandedNotice(document, null);
            }
        }
        
        DisseminationLogHelper.extendDisseminationLogEntries(cellarResourceBean, workBranch, MediaType.APPLICATION_XML_VALUE, cellarResourceDao);
        
        final HttpStatus httpStatus = conditionalRequestVersions.isChanged() ? HttpStatus.OK : HttpStatus.NOT_MODIFIED;

        final HttpHeadersBuilder httpHeadersBuilder = HttpHeadersBuilder.get().withDefaultNoticeHeaders(
                conditionalRequestVersions.getNewETag(), conditionalRequestVersions.getNewLastModified(),
                DisseminationRequestUtils.getMediaType(RequestType.OBJECT_NOTICE));

        return new ResponseEntity<>(document, httpHeadersBuilder.getHttpHeaders(), httpStatus);
    }

    /**
     * Do rdf object request.
     *
     * @param eTag               the e tag
     * @param lastModified       the last modified
     * @param cellarResourceBean the cellar resource bean
     * @param normalized the normalized
     * @return the response entity
     * @see eu.europa.ec.opoce.cellar.server.service.DisseminationService#doRdfObjectRequest(java.lang.String,
     * java.lang.String, eu.europa.ec.opoce.cellar.server.service.CellarResourceBean,boolean)
     */
    @Override
    public ResponseEntity<Model> doRdfObjectRequest(final String eTag, final String lastModified,
                                                    final CellarResourceBean cellarResourceBean,boolean normalized) {
        return internalRdfObjectRequest(eTag, lastModified, cellarResourceBean, true,normalized);
    }

    /**
     * Do rdf non inferred object request.
     *
     * @param eTag               the e tag
     * @param lastModified       the last modified
     * @param cellarResourceBean the cellar resource bean
     * @param normalized the normalized
     * @return the response entity
     * @see eu.europa.ec.opoce.cellar.server.service.DisseminationService#doRdfNonInferredObjectRequest(java.lang.String,
     * java.lang.String, eu.europa.ec.opoce.cellar.server.service.CellarResourceBean,boolean)
     */
    @Override
    public ResponseEntity<Model> doRdfNonInferredObjectRequest(final String eTag, final String lastModified,
                                                               final CellarResourceBean cellarResourceBean,boolean normalized) {
        return internalRdfObjectRequest(eTag, lastModified, cellarResourceBean, false,normalized);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResponseEntity<Model> doRdfTreeRequest(final String eTag, final String lastModified, final CellarResourceBean cellarResourceBean,
                                                  final boolean provideResponseBody,final boolean normalized) {
        return internalRdfTreeRequest(eTag, lastModified, cellarResourceBean, provideResponseBody, true,normalized);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResponseEntity<Model> doRdfNonInferredTreeRequest(final String eTag, final String lastModified,
                                                             final CellarResourceBean cellarResourceBean, final boolean provideResponseBody,final boolean normalized) {
        return internalRdfTreeRequest(eTag, lastModified, cellarResourceBean, provideResponseBody, false,normalized);
    }

    /**
     * Do list content request.
     *
     * @param eTag               the e tag
     * @param lastModified       the last modified
     * @param cellarResourceBean the cellar resource bean
     * @return the response entity
     * @see eu.europa.ec.opoce.cellar.server.service.DisseminationService#doListContentRequest(java.lang.String,
     * java.lang.String, eu.europa.ec.opoce.cellar.server.service.CellarResourceBean)
     */
    @Override
    public ResponseEntity<Tree<Pair<String, String>>> doListContentRequest(final String eTag, final String lastModified,
                                                                           final CellarResourceBean cellarResourceBean) {
        return internalListContentRequest(eTag, lastModified, cellarResourceBean);
    }

    /**
     * Do zip content request.
     *
     * @param eTag               the e tag
     * @param lastModified       the last modified
     * @param cellarResourceBean the cellar resource bean
     * @return the response entity
     * @see eu.europa.ec.opoce.cellar.server.service.DisseminationService#doZipContentRequest(java.lang.String,
     * java.lang.String, eu.europa.ec.opoce.cellar.server.service.CellarResourceBean)
     */
    @Override
    public ResponseEntity<File> doZipContentRequest(final String eTag, final String lastModified,
                                                    final CellarResourceBean cellarResourceBean) {
        return internalZipContentRequest(eTag, lastModified, cellarResourceBean);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResponseEntity<Document> doIdentifiersRequest(final String eTag, final String lastModified,
                                                         final CellarResourceBean cellarResourceBean) {
        return internalIdentifiersRequest(eTag, lastModified, cellarResourceBean);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResponseEntity<DisseminationContentStreams> doTypeRequest(final CellarResourceBean cellarResource,
                                                                     final List<String> acceptTypes, final List<LanguageBean> contentLanguages, final boolean provideAlternates) {
        final String cellarId = cellarResource.getCellarId();
        final DigitalObjectType type = cellarResource.getCellarType();

        boolean found = false;
        List<DocumentAndOriginalCellarId> cellarIds = new ArrayList<>();
        if (type == DigitalObjectType.WORK) {
            if ((contentLanguages == null) || contentLanguages.isEmpty()) {
                throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                        .withMessage(INVALID_TYPE_SPECIFIED).build();
            }

            // try to retrieve the original content by cycling on preferred
            // languages
            for (final LanguageBean language : contentLanguages) {
                final List<Expression> expressions = this.disseminationDbGateway.getExpressionsFromWork(cellarId, language);
                if (expressions != null) {
                    final Set<DocumentAndOriginalCellarId> documentAndOriginalCellarIdSet = expressions.stream()
                            .map(expression -> findDocuments(expression, acceptTypes))
                            .flatMap(Collection::stream)
                            .collect(Collectors.toSet());
                    cellarIds.addAll(documentAndOriginalCellarIdSet);
                    found = isOriginalContentFound(cellarIds);
                    if (found) {
                        break;
                    }
                }
            }

        } else if (type == DigitalObjectType.EXPRESSION || type == DigitalObjectType.MANIFESTATION) {
            cellarIds = findDocuments(cellarId, cellarResource.getVersions(), type, acceptTypes);
            found = isOriginalContentFound(cellarIds);
        } else if (type == DigitalObjectType.ITEM) {
            final DocumentAndOriginalCellarId documentAndOriginalCellarId = new DocumentAndOriginalCellarId(cellarId, cellarId,
                    cellarResource.getVersions(), Type.ORIGINAL_CONTENT);
            final CellarIdentifier cellarIdentifier = this.cellarIdentifierDao.getCellarIdentifier(cellarId);
            documentAndOriginalCellarId.setDocIndex(cellarIdentifier.getDocIndex());
            cellarIds = singletonList(documentAndOriginalCellarId);
            found = isOriginalContentFound(cellarIds);
        } else {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage(INVALID_TYPE_SPECIFIED).build();
        }

        // Handle case not found
        // CELLARM-614
        if (!found) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_ACCEPTABLE)
                    .withCode(CmrErrors.S3_NO_CONTENT_FOR_ID)
                    .withMessage("cellar identifier {} does not hold a content datastream of the requested type").withMessageArgs(cellarId)
                    .build();
        }

        HttpStatus httpStatus;
        final HttpHeadersBuilder httpHeadersBuilder = HttpHeadersBuilder.get().withCacheControl(CacheControl.NO_STORE).withDateNow();

        // FIXME JRM: POC of CELLAR-685: Remove DisseminationContentStreams and
        // use a List<DisseminationContentStreamData>
        // and define and apply a comparator to compare on the order of
        // DisseminationContentStreamData
        final DisseminationContentStreams disseminationContentStreams = getDisseminationContentStreams(cellarIds);
        final int numberOfDatastreams = cellarIds.size();

        // Add alternatesHeader
        // FIXME JRM: Re-engineer together with POC of CELLAR-685's
        // implementation
        if (provideAlternates && (numberOfDatastreams >= 1)) {
            final String manifestationId = StringUtils.split(cellarIds.get(0).getOriginalCellarId(), '/')[0];
            this.checkForCellarResource(manifestationId);
            final String manifestationUri = this.disseminationDbGateway.getCellarIdentifiedObject(manifestationId).getCellarId().getUri();
            final SortedSet<String> alternates = this.alternatesService.getContentStreamAlternates(manifestationUri);
            httpHeadersBuilder.withTcn(Tcn.CHOICE).withAlternates(alternates);
        }

        if (numberOfDatastreams == 1) {
            final DisseminationContentStreamData contentStreamData = disseminationContentStreams.getDisseminationContentStreamDataList().get(0);
            httpStatus = HttpStatus.SEE_OTHER;
            httpHeadersBuilder.withLocation(contentStreamData.getUrl());
            httpHeadersBuilder.withContentType(contentStreamData.getMediaType());
        } else {
            httpStatus = HttpStatus.MULTIPLE_CHOICES;
            httpHeadersBuilder.withContentType(DisseminationRequestUtils.getMediaType(RequestType.CONTENT));
        }
        return new ResponseEntity<>(disseminationContentStreams, httpHeadersBuilder.getHttpHeaders(),
                httpStatus);
    }

    private List<DocumentAndOriginalCellarId> findDocuments(String cellarId, Map<ContentType, String> versions, DigitalObjectType type, List<String> manifestations) {
        final DocumentAndOriginalCellarId documentAndOriginalCellarId = new DocumentAndOriginalCellarId("", cellarId,
                versions, Type.MANIFESTATION);
        if (manifestations != null && !cellarId.isEmpty()) {
            for (final String manifestationType : manifestations) {
                List<CellarResource> matchItems = retrievingService.getUriFromDatastream(cellarId, versions, type, singletonList(manifestationType));
                if (!matchItems.isEmpty()) {
                    return newDocuments(matchItems);
                }
            }
            return Collections.emptyList();
        }
        return Collections.singletonList(documentAndOriginalCellarId);
    }

    private List<DocumentAndOriginalCellarId> findDocuments(Expression expression, List<String> manifestations) {
        String cellarId = expression.getCellarId().getIdentifier();
        List<DocumentAndOriginalCellarId> docs = findDocuments(cellarId, expression.getVersions(),
                expression.getType(), manifestations);
        if (!docs.isEmpty()) {
            return docs;
        } else {
            return Collections.singletonList(new DocumentAndOriginalCellarId("", cellarId,
                    expression.getVersions(), Type.MANIFESTATION));
        }
    }

    private List<DocumentAndOriginalCellarId> newDocuments(final List<CellarResource> cellarIdList) {
        final List<DocumentAndOriginalCellarId> results = new ArrayList<>(cellarIdList.size());
        for (CellarResource resource : cellarIdList) {
            DocumentAndOriginalCellarId doc = new DocumentAndOriginalCellarId(resource.getCellarId(), resource.getCellarId(),
                    resource.getVersions(), Type.ORIGINAL_CONTENT);
            final CellarIdentifier identifier = pidManagerService.getCellarIdentifierByUuid(resource.getCellarId());
            if (identifier != null) {
                doc.setDocIndex(identifier.getDocIndex());
            }
            results.add(doc);
        }
        return results;
    }

    /**
     * Internal rdf object request.
     *
     * @param eTag               the e tag
     * @param lastModified       the last modified
     * @param cellarResourceBean the cellar resource bean
     * @param inferred           the inferred
     * @param normalized the normalized
     * @return the response entity
     */
    private ResponseEntity<Model> internalRdfObjectRequest(final String eTag, final String lastModified,
                                                           final CellarResourceBean cellarResourceBean, final boolean inferred,final boolean normalized) {
        final DigitalObjectType cellarType = cellarResourceBean.getCellarType();
        String cellarId=cellarResourceBean.getCellarId();
        if (cellarType == DigitalObjectType.ITEM) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage(INVALID_TYPE_SPECIFIED).build();
        }

        final ConditionalRequestVersions conditionalRequestVersions = this.conditionalRequestService
                .calculateVersionWithoutEmbedding(cellarResourceBean, ETagType.RDF_OBJECT, eTag, lastModified);

        Model model = null;
        if (conditionalRequestVersions.isChanged()) {
            model = this.createRdfModelForObject(cellarResourceBean.getCellarId(), inferred,normalized);
        }
        Model normalizedSkolemizedModel=null;
        if(normalized){
            LOG.debug("Normalizing model for {}",cellarId);
            VirtuosoNormalizer normalizer=new VirtuosoNormalizer();
            normalizedSkolemizedModel=ModelFactory.createDefaultModel().add(model);
            normalizer.normalizeModel(normalizedSkolemizedModel,cellarId);
        }

        DisseminationLogHelper.extendDisseminationLogEntries(cellarResourceBean, null, DisseminationRequestUtils.RDF_XML.toString(), cellarResourceDao);

        RequestType type;
        if(normalized){
            type=inferred? RequestType.RDF_XML_OBJECT_NORMALIZED : RequestType.RDF_XML_NONINFERRED_OBJECT_NORMALIZED;
        }
        else{
           type= inferred?RequestType.RDF_XML_OBJECT:RequestType.RDF_XML_NONINFERRED_OBJECT;
        }

        final HttpStatus httpStatus = conditionalRequestVersions.isChanged() ? HttpStatus.OK : HttpStatus.NOT_MODIFIED;
        final HttpHeadersBuilder httpHeadersBuilder = HttpHeadersBuilder.get().withDefaultNoticeHeaders(
                conditionalRequestVersions.getNewETag(), conditionalRequestVersions.getNewLastModified(),
                DisseminationRequestUtils.getMediaType(type));

        return new ResponseEntity<>(normalized?normalizedSkolemizedModel:model, httpHeadersBuilder.getHttpHeaders(), httpStatus);
    }

    /**
     * Internal rdf tree request.
     *
     * @param eTag                the e tag
     * @param lastModified        the last modified
     * @param cellarResourceBean  the cellar resource bean
     * @param provideResponseBody the provide response body
     * @param inferred            the inferred
     * @param normalized the normalized
     * @return the response entity
     */
    private ResponseEntity<Model> internalRdfTreeRequest(final String eTag, final String lastModified,
                                                         final CellarResourceBean cellarResourceBean, final boolean provideResponseBody, final boolean inferred,final boolean normalized) {
        final DigitalObjectType cellarType = cellarResourceBean.getCellarType();

        final String cellarId = cellarResourceBean.getCellarId();

        Model model = null;
        ConditionalRequestVersions conditionalRequestVersions;
        switch (cellarType) {
            case WORK:
                final Work work = this.disseminationDbGateway.getWorkTree(cellarId);
                conditionalRequestVersions = this.conditionalRequestService.calculateVersionWithoutEmbedding(
                        work.getFlatHierarchy(ETagType.RDF_TREE_WORK.getDigitalObjectTypes()), ETagType.RDF_TREE_WORK, eTag, lastModified);
                if (conditionalRequestVersions.isChanged() && provideResponseBody) {
                    model = createRdfModelForWorkTree(work, inferred,normalized);
                }
                break;
            case DOSSIER:
                final Dossier dossier = this.disseminationDbGateway.getDossierTree(cellarId);
                conditionalRequestVersions = this.conditionalRequestService.calculateVersionWithoutEmbedding(
                        dossier.getFlatHierarchy(ETagType.RDF_TREE_DOSSIER.getDigitalObjectTypes()), ETagType.RDF_TREE_DOSSIER, eTag,
                        lastModified);
                if (conditionalRequestVersions.isChanged() && provideResponseBody) {
                    model = createRdfModelForDossierTree(dossier, inferred,normalized);
                }
                break;
            case AGENT:
                final Agent agent = this.disseminationDbGateway.getAgent(cellarId);
                conditionalRequestVersions = this.conditionalRequestService.calculateVersionWithoutEmbedding(
                        agent.getFlatHierarchy(ETagType.RDF_TREE_AGENT.getDigitalObjectTypes()), ETagType.RDF_TREE_AGENT, eTag, lastModified);
                if (conditionalRequestVersions.isChanged() && provideResponseBody) {
                    model = this.createRdfModelForObject(cellarId, inferred,normalized);
                }

                break;
            case TOPLEVELEVENT:
                final TopLevelEvent topLevelEvent = this.disseminationDbGateway.getTopLevelEvent(cellarId);
                conditionalRequestVersions = this.conditionalRequestService.calculateVersionWithoutEmbedding(
                        topLevelEvent.getFlatHierarchy(ETagType.RDF_TREE_TOPLEVELEVENT.getDigitalObjectTypes()),
                        ETagType.RDF_TREE_TOPLEVELEVENT, eTag, lastModified);
                if (conditionalRequestVersions.isChanged() && provideResponseBody) {
                    model = this.createRdfModelForObject(cellarId, inferred,normalized);
                }

                break;
            default:
                throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                        .withMessage("Invalid type '{}' specified").withMessageArgs(cellarType).build();
        }
        Model normalizedSkolemizedModel=null;
        if(normalized){
            LOG.debug("Normalizing model for {}",cellarId);
            VirtuosoNormalizer normalizer=new VirtuosoNormalizer();
            normalizedSkolemizedModel=ModelFactory.createDefaultModel().add(model);
            normalizer.normalizeModel(normalizedSkolemizedModel,cellarId);
        }
        
        RequestType type;
        if(normalized){
            type=inferred? RequestType.RDF_XML_TREE_NORMALIZED : RequestType.RDF_XML_NONINFERRED_TREE_NORMALIZED;
        }
        else{
           type= inferred?RequestType.RDF_XML_TREE:RequestType.RDF_XML_NONINFERRED_TREE;
        }
        
        final HttpStatus httpStatus = conditionalRequestVersions.isChanged() ? HttpStatus.OK : HttpStatus.NOT_MODIFIED;
        final HttpHeadersBuilder httpHeadersBuilder = HttpHeadersBuilder.get().withDefaultNoticeHeaders(
                conditionalRequestVersions.getNewETag(), conditionalRequestVersions.getNewLastModified(),
                DisseminationRequestUtils.getMediaType(type));

        return new ResponseEntity<>(normalized?normalizedSkolemizedModel:model, httpHeadersBuilder.getHttpHeaders(), httpStatus);
    }

    /**
     * Internal list content request.
     *
     * @param eTag               the e tag
     * @param lastModified       the last modified
     * @param cellarResourceBean the cellar resource bean
     * @return the response entity
     */
    private ResponseEntity<Tree<Pair<String, String>>> internalListContentRequest(final String eTag, final String lastModified,
                                                                                  final CellarResourceBean cellarResourceBean) {
        final Collection<MetsElement> contents = getManifestationContents(cellarResourceBean);

        final HttpHeadersBuilder httpHeadersBuilder = HttpHeadersBuilder.get()
                .withCacheControl(CacheControl.NO_CACHE)
                .withContentType(DisseminationRequestUtils.getMediaType(RequestType.CONTENT))
                .withDateNow();
        HttpStatus httpStatus = HttpStatus.OK;

        Tree<Pair<String, String>> contentStreamLabels = null;
        if (!contents.isEmpty()) {
            final ConditionalRequestVersions conditionalRequestVersions = this.conditionalRequestService
                    .calculateVersionWithoutEmbedding(contents, ETagType.LIST_CONTENT, eTag, lastModified);

            httpHeadersBuilder.withETag(conditionalRequestVersions.getNewETag())
                    .withLastModified(conditionalRequestVersions.getNewLastModified());

            if (conditionalRequestVersions.isChanged()) {
                contentStreamLabels = list(contents);
            } else {
                httpStatus = HttpStatus.NOT_MODIFIED;
            }
        }

        return new ResponseEntity<>(contentStreamLabels, httpHeadersBuilder.getHttpHeaders(), httpStatus);
    }

    private Tree<Pair<String, String>> list(Collection<MetsElement> contents) {
        final Tree.Node<Pair<String, String>> root = Tree.node(null);

        for (final MetsElement content : contents) {
            if (!(content instanceof Content) || !((Content) content).isVisible()) {
                continue;
            }

            final String cellarId = content.getCellarId().getIdentifier();
            final String version = content.getVersions().get(FILE);
            Map<String, Object> metadata = contentStreamService.getMetadata(cellarId, version, FILE);
            String contentType = (String) metadata.get(Headers.CONTENT_TYPE);
            String label = (String) metadata.get(ContentStreamService.Metadata.FILENAME);
            final Tree.Node firstLevelNode = Tree.node(new Pair<>(content.getCellarId().getUri(), label));
            root.addChildNode(firstLevelNode);
            if (isZipContentStream(contentType)) {
                try (final InputStream contentStream = this.contentStreamService.getContent(cellarId, version, FILE)
                        .orElseThrow(() -> new IllegalStateException("Cannot find FILE " + cellarId + "(" + version + ")"));
                     final ZipInputStream zis = new ZipInputStream(contentStream)) {
                    ZipEntry entry;
                    while ((entry = zis.getNextEntry()) != null) {
                        firstLevelNode.addChild(new Pair<>(content.getCellarId().getUri() + '/' + entry.getName(), entry.getName()));
                    }
                } catch (Exception e) {
                    throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class)
                            .withHttpStatus(HttpStatus.UNPROCESSABLE_ENTITY)
                            .withMessage("A problem occurred while accessing datastream '{}' from S3.")
                            .withCause(e)
                            .withMessageArgs(content.getCellarId().getUri())
                            .build();
                }
            }
        }

        return root.asTree();
    }

    /**
     * Internal zip content request.
     *
     * @param eTag               the e tag
     * @param lastModified       the last modified
     * @param cellarResourceBean the cellar resource bean
     * @return the response entity
     */
    private ResponseEntity<File> internalZipContentRequest(final String eTag, final String lastModified,
                                                           final CellarResourceBean cellarResourceBean) {
        final Collection<MetsElement> contents = getManifestationContents(cellarResourceBean);
        if (contents.isEmpty()) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                    .withMessage("Resource '{}' does not have content stream.").withMessageArgs(cellarResourceBean.getCellarId()).build();
        }
        final ConditionalRequestVersions conditionalRequestVersions = this.conditionalRequestService
                .calculateVersionWithoutEmbedding(cellarResourceBean, ETagType.ZIP_CONTENT, eTag, lastModified);

        final File zipFile = getTemporaryZipFile(cellarResourceBean.getCellarId());

        if (conditionalRequestVersions.isChanged() && !contents.isEmpty()) {
            zip(cellarResourceBean, contents, zipFile);
        }

        final HttpStatus httpStatus = conditionalRequestVersions.isChanged() ? HttpStatus.OK : HttpStatus.NOT_MODIFIED;
        final HttpHeadersBuilder httpHeadersBuilder = HttpHeadersBuilder.get().withDefaultNoticeHeaders(
                conditionalRequestVersions.getNewETag(), conditionalRequestVersions.getNewLastModified(),
                DisseminationRequestUtils.getMediaType(RequestType.ZIP));

        return new ResponseEntity<>(zipFile, httpHeadersBuilder.getHttpHeaders(), httpStatus);
    }

    private void zip(CellarResourceBean resource, Collection<MetsElement> contents, File dest) {
        try {
            if (isSingleZip(contents)) {
                MetsElement element = contents.iterator().next();
                String cellarId = element.getCellarId().getIdentifier();
                String version = element.getVersions().get(FILE);
                try (FileOutputStream out = new FileOutputStream(dest);
                     InputStream is = contentStreamService.getContent(cellarId, version, FILE)
                             .orElseThrow(() -> new IllegalStateException("File not found for " + cellarId + "(" + version + ")"))) {
                    ByteStreams.copy(is, out);
                }
            } else {
                packageElements(contents, dest);
            }
        } catch (IOException ioe) {
            throw createDisseminationException(ioe, "An unexpected error occurred while retrieving content streams of cellar resource with id '{}'",
                    resource.getCellarId());
        }
    }

    private void packageElements(Collection<MetsElement> elements, File dest) throws
            IOException {
        try (FileOutputStream fos = new FileOutputStream(dest); ZipOutputStream zos = new ZipOutputStream(fos)) {
            Collection<MetsElement> filtered = elements.stream()
                    .filter(DisseminationServiceImpl::isVisible)
                    .collect(Collectors.toList());
            for (MetsElement element : filtered) {
                String cellarId = element.getCellarId().getIdentifier();
                String version = element.getVersions().get(FILE);
                Map<String, Object> metadata = contentStreamService.getMetadata(cellarId, version, FILE);
                String filename = (String) metadata.get(ContentStreamService.Metadata.FILENAME);
                try (InputStream is = contentStreamService.getContent(cellarId, version, FILE)
                        .orElseThrow(() -> new IllegalStateException(filename + " not found for " + cellarId + "(" + version + ")"))) {
                    zos.putNextEntry(new ZipEntry(filename));
                    long bytes = ByteStreams.copy(is, zos);
                    LOG.debug("[{}] file added to the archive ({} bytes)", filename, bytes);
                }
            }
        }
    }

    private boolean isSingleZip(final Collection<MetsElement> elements) {
        if (elements.size() == 1) {
            final MetsElement single = elements.iterator().next();
            if (isVisible(single)) {
                Map<String, Object> metadata = contentStreamService.getMetadata(single.getCellarId().getIdentifier(), single.getVersions().get(FILE), FILE);
                String contentType = (String) metadata.get(Headers.CONTENT_TYPE);
                return isZipContentStream(contentType);
            }
        }
        return false;
    }

    /**
     * Gets the temporary zip file.
     *
     * @param identifier the identifier
     * @return the temporary zip file
     */
    private File getTemporaryZipFile(final String identifier) {
        return new File(this.cellarConfiguration.getCellarFolderTemporaryDissemination() + "/" + CONTENT_STREAM_PREFIX + "_" + identifier
                + "_" + Thread.currentThread().getId() + ".zip");
    }


    /**
     * Internal identifiers request.
     *
     * @param eTag               the e tag
     * @param lastModified       the last modified
     * @param cellarResourceBean the cellar resource bean
     * @return the response entity
     */
    private ResponseEntity<Document> internalIdentifiersRequest(final String eTag, final String lastModified,
                                                                final CellarResourceBean cellarResourceBean) {
        final List<String> normalizedUris = singletonList(cellarResourceBean.getCellarId());

        LOG.debug("# uri: ['{}']", normalizedUris.get(0));

        Document document = null;
        HttpStatus httpStatus;

        final ConditionalRequestVersions conditionalRequestVersions = this.conditionalRequestService
                .calculateVersionWithoutEmbedding(cellarResourceBean, ETagType.IDENTIFIERS, eTag, lastModified);

        if (conditionalRequestVersions.isChanged()) {
            final Model model = this.cellarIdentifierResourceService.getSameAsModel(normalizedUris);
            if (!model.isEmpty()) {
                document = this.disseminationNoticeService.createIdentifierNoticeWithoutObject(normalizedUris, model);
                httpStatus = HttpStatus.OK;
            } else {
                throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                        .withMessage("Resource '{}' does not have non-empty model.").withMessageArgs(cellarResourceBean.getCellarId())
                        .build();
            }
        } else {
            httpStatus = HttpStatus.NOT_MODIFIED;
        }

        final HttpHeadersBuilder httpHeadersBuilder = HttpHeadersBuilder.get().withDefaultNoticeHeaders(
                conditionalRequestVersions.getNewETag(), conditionalRequestVersions.getNewLastModified(),
                DisseminationRequestUtils.getMediaType(RequestType.IDENTIFIER_NOTICE));

        return new ResponseEntity<>(document, httpHeadersBuilder.getHttpHeaders(), httpStatus);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Collection<MetsElement> getManifestationContents(final CellarResourceBean cellarResourceBean) {
        final DigitalObjectType cellarType = cellarResourceBean.getCellarType();

        if (cellarType == DigitalObjectType.MANIFESTATION) {
            final String cellarId = cellarResourceBean.getCellarId();
            final Manifestation manifestation = this.disseminationDbGateway.getManifestationBranch(cellarId);
            return manifestation.getFlatHierarchy(DigitalObjectType.ITEM);
        } else {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class)
                    .withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage("Invalid type '{}' specified")
                    .withMessageArgs(cellarType)
                    .build();
        }
    }

    /**
     * The original content is found only if <code>cellarIds</code> is not empty
     * and there is more than one, or, if there is only one, it is not of type
     * {@link Type#MANIFESTATION}
     * .
     *
     * @param cellarIds the Cellar's ids to check
     * @return the result of the boolean check
     */
    private boolean isOriginalContentFound(final List<DocumentAndOriginalCellarId> cellarIds) {
        if (cellarIds == null || cellarIds.isEmpty()) {
            return false;
        }

        for (DocumentAndOriginalCellarId cellarId : cellarIds) {
            if (cellarId.getType() == Type.ORIGINAL_CONTENT) {
                return true;
            }
        }

        return false;
    }

    /**
     * <p>
     * getDisseminationContentStreams.
     * </p>
     *
     * @param ids a {@link java.util.Collection} object.
     * @return a
     * {@link eu.europa.ec.opoce.cellar.server.dissemination.converter.DisseminationContentStreams}
     * object.
     */
    private DisseminationContentStreams getDisseminationContentStreams(final Collection<DocumentAndOriginalCellarId> ids) {
        if (ids.isEmpty()) {
            return new DisseminationContentStreams();
        }

        final HashMap<String, Set<String>> workIdentifiers = getWorkIdentifiers(ids);

        final ArrayList<DisseminationContentStreamData> contentStreams = CollectionUtils.collect(ids,
                input -> {
                    final Model technicalMetaData = getTechnicalMetaData(input);
                    final String documentId = input.getDocumentId();
                    final String urlToContent = getURLToContent(documentId);
                    final String mediaType = getContentType(documentId);
                    final DisseminationContentStreamData streamData = new DisseminationContentStreamData(urlToContent);
                    streamData.setOwnerId(StringUtils.substringBefore(documentId, "/"));
                    streamData.setMediaType(mediaType);

                    try {
                        final String originalCellarId = input.getOriginalCellarId();
                        final String uri = identifierService.getUri(originalCellarId);
                        final Resource cellarResource = technicalMetaData.getResource(uri);
                        if (cellarResource != null) {
                            final List<Resource> resources = JenaQueries.getResources(cellarResource, OWL.sameAs, false);
                            for (final Resource psResource : resources) {
                                final Statement order = psResource.getProperty(CellarProperty.tdm_stream_orderP);
                                if (order != null) {
                                    streamData.setOrder(order.getInt());
                                }
                                final Statement first = psResource.getProperty(CellarProperty.tdm_stream_page_physical_firstP);
                                if (first != null) {
                                    streamData.setFirstPage(first.getInt());
                                }
                                final Statement last = psResource.getProperty(CellarProperty.tdm_stream_page_physical_lastP);
                                if (last != null) {
                                    streamData.setLastPage(last.getInt());
                                }
                                final Statement streamName = psResource.getProperty(CellarProperty.tdm_stream_nameP);
                                if (streamName != null) {
                                    streamData.setStreamName(streamName.getString());
                                }

                                final Statement streamLabel = psResource.getProperty(CellarProperty.tdm_stream_labelP);
                                if (streamLabel != null) {
                                    streamData.setStreamLabel(streamLabel.getString());
                                }

                                if (workIdentifiers != null) {
                                    streamData.setWorkIds(workIdentifiers.get(originalCellarId));
                                }

                            }
                            final Integer docIndex = input.getDocIndex();
                            streamData.setDocIndex(docIndex);
                            // FIXME JRM: POC of CELLAR-685
                            // if no order could be obtained from the sameAs
                            // resources above, order after name
                            if (streamData.getOrder() < 0) {
                                streamData.setOrder(docIndex);
                            }

                        }
                    } finally {
                        JenaUtils.closeQuietly(technicalMetaData);
                    }
                    return streamData;
                }, new ArrayList<>());
        return new DisseminationContentStreams(contentStreams);

    }

    /**
     * Gets the content type.
     * 
     * @param documentId the id of the document to retrieve
     * @return the content type of the document matching the provided id
     */
    private String getContentType(final String documentId) {
        final CellarResource resource = this.cellarResourceDao.findCellarId(documentId);

        if(resource == null) {
            return null;
        }

        return resource.getMimeType();
    }

    /**
     * Gets the work identifier.
     *
     * @param ids the ids
     * @return the work identifier
     */
    private HashMap<String, Set<String>> getWorkIdentifiers(final Collection<DocumentAndOriginalCellarId> ids) {
        final HashMap<String, Set<String>> result = new HashMap<>();

        Model manifestationModel = null;
        Model relatedWorkMetadataModel = null;
        try {
            for (final DocumentAndOriginalCellarId documentAndOriginalCellarId : ids) {
                // cellar:df2ca785-8de9-11e5-a129-01aa75ed71a1.0009.02/DOC_1
                final String dataStreamCellarId = documentAndOriginalCellarId.getOriginalCellarId();
                // cellar:df2ca785-8de9-11e5-a129-01aa75ed71a1.0009.02
                final String manifestationCellarId = CellarIdUtils.getParentCellarId(dataStreamCellarId);
                final Set<String> workIds = new HashSet<>();
                result.put(dataStreamCellarId, workIds);

                // get the manifestation DIRECT metadata
                manifestationModel = getDirectModel(manifestationCellarId);
                // search for
                // <j.0:manifestation_part_of_manifestation
                // rdf:resource="http://cellar-dev.publications.europa.eu/resource/oj/DD_1981_11_027_EL.ELL.pdfa1b"/>
                final NodeIterator propertiesFromManifestation = manifestationModel
                        .listObjectsOfProperty(CellarProperty.cdm_manifestation_part_of_manifestationP);
                try {
                    while (propertiesFromManifestation.hasNext()) {
                        // http://cellar-dev.publications.europa.eu/resource/oj/DD_1981_11_027_EL.ELL.pdfa1b
                        final RDFNode manifestationNode = propertiesFromManifestation.next();
                        final String manifestationPartOfManifestationUri = manifestationNode.toString();
                        // oj:DD_1981_11_027_EL.ELL.pdfa1b
                        final String pidString = this.identifierService.getPrefixed(manifestationPartOfManifestationUri);
                        // get manifestation cellar identifier
                        final String cellarUUID = this.identifierService.getCellarUUIDIfExists(pidString);
                        if (StringUtils.isNotBlank(cellarUUID)) {
                            // get work cellar identifier
                            final String workId = CellarIdUtils.getCellarIdBase(cellarUUID);
                            // get the related Work DMD model
                            relatedWorkMetadataModel = getDirectModel(workId);
                            // search for
                            // <j.0:work_id_document
                            // rdf:datatype="http://www.w3.org/2001/XMLSchema#string">oj:DD_1981_11_027_EL</j.0:work_id_document>
                            final NodeIterator propertiesFromRelatedWork = relatedWorkMetadataModel
                                    .listObjectsOfProperty(CellarProperty.cdm_workIdDocumentP);
                            try {
                                while (propertiesFromRelatedWork.hasNext()) {
                                    // oj:DD_1981_11_027_EL
                                    final RDFNode relatedWorkNode = propertiesFromRelatedWork.next();
                                    workIds.add(relatedWorkNode.asLiteral().getString());
                                }
                            } finally {
                                propertiesFromRelatedWork.close();
                            }
                        }
                    }
                } finally {
                    propertiesFromManifestation.close();
                }
            }
        } catch (final Exception e) {
            LOG.warn("Unable to retrieve the related work id identifier for [{}]", ids);
        } finally {
            JenaUtils.closeQuietly(manifestationModel);
            JenaUtils.closeQuietly(relatedWorkMetadataModel);
        }
        return result;
    }

    private Model getDirectModel(String cellarID) throws IOException {
        CellarResource manifestation = cellarResourceDao.findCellarId(cellarID);
        String version = manifestation.getVersion(DIRECT).orElse(null);
        try (InputStream is = contentStreamService.getContent(manifestation.getCellarId(), version, DIRECT)
                .orElseThrow(() -> new IllegalStateException("Direct metadata not found for " + manifestation.getCellarId() + "(" + version + ")"))) {
            return JenaUtils.read(is, Lang.NT);
        }
    }

    private String getURLToContent(String cellarId) {
        final int positionDataStream = cellarId.lastIndexOf('/');
        if (positionDataStream > -1) {
            final String dataStreamName = cellarId.substring(positionDataStream + 1);
            final String objectId = cellarId.substring(0, positionDataStream);
            return getPublicURLToContent(objectId, dataStreamName);
        } else {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("cellar identifier {} does not hold a content datastream")
                    .withMessageArgs(cellarId)
                    .build();
        }
    }

    private String getPublicURLToContent(final String digitalObjectPID, final String datastreamName) {
        Assert.notNull(digitalObjectPID, "[E-100] The Digital Object PID is null");

        String urlToContent;
        final String namespace = StringUtils.substringBefore(digitalObjectPID, ":");
        if (StringUtils.equalsIgnoreCase(namespace, "transformed")) {
            urlToContent = this.cellarConfiguration.getCellarUriTranformationResourceBase() + "/";
        } else {
            Assert.notNull(datastreamName, "[E-100] The datastream ID is null");
            urlToContent = this.cellarConfiguration.getCellarUriResourceBase() + "/";
        }

        final String pid = StringUtils.substringAfter(digitalObjectPID, ":");
        urlToContent += pid + (StringUtils.isNotBlank(datastreamName) ? "/" + datastreamName : "");

        return urlToContent;
    }

    private Model getTechnicalMetaData(final DocumentAndOriginalCellarId id) {
        if (id != null) {
            return getCompleteTechnicalMetaData(id.getOriginalCellarId(), id.getVersions());
        }
        return JenaUtils.create();
    }

    @Override
    public Model getCompleteTechnicalMetaData(final String cellarId, Map<ContentType, String> versions) {
        Model sameAsModel = null;
        Model tmdModel = null;
        Model result = ModelFactory.createDefaultModel();
        try {
            // For legacy reasons, the metadata of the items are store under in the manifestation so
            // we need to retrieve the versions of the manifestation before accessing it in S3
            final String manifestationId = cellarId.substring(0, cellarId.lastIndexOf('/'));
            CellarResource manifestation = cellarResourceDao.findCellarId(manifestationId);
            String technicalModel = contentStreamService.getContentAsString(manifestationId, manifestation.getVersion(ContentType.TECHNICAL).orElse(null),
                    ContentType.TECHNICAL)
                    .orElse(null);
            if (technicalModel != null) {
                tmdModel = JenaUtils.read(technicalModel, Lang.NT);
                final Collection<String> cellarIds = singletonList(cellarId);
                sameAsModel = this.cellarIdentifierResourceService.getSameAsModel(cellarIds);
                result = JenaUtils.create(sameAsModel, tmdModel);
            }
            return result;
        } finally {
            JenaUtils.closeQuietly(sameAsModel, tmdModel);
        }
    }

    /**
     * Creates the rdf model for object.
     *
     * @param cellarId the cellar id
     * @param inferred the inferred
     * @param normalized the normalized
     * @return the model
     */
    private Model createRdfModelForObject(final String cellarId, final boolean inferred,final boolean normalized) {
        final CellarIdentifiedObject cellarObject = this.disseminationDbGateway.getCellarIdentifiedObject(cellarId);
        return this.createRdfModelForObject(cellarObject, inferred,normalized);
    }

    /**
     * Creates the rdf model for object.
     *
     * @param cellarObject the cellar object
     * @param inferred     the inferred
     * @param normalized the normalized
     * @return the model
     */
    private Model createRdfModelForObject(final CellarIdentifiedObject cellarObject, final boolean inferred,final boolean normalized) {
        Model retModel;

        Map<ContentType, String> versions = ContentType.extract(cellarObject.getVersions(), DIRECT, DIRECT_INFERRED);
        Map<ContentType, String> models = contentStreamService.getContentsAsString(cellarObject.getCellarId().getIdentifier(), versions);
        Model mainModel = null;
        Model inverseModel = null;
        try {
            mainModel = JenaUtils.read(models.get(inferred ? ContentType.DIRECT_INFERRED : ContentType.DIRECT), Lang.NT);
            if (inferred) {
                inverseModel = new NoticeCache().getInverse(cellarObject).getOne();
            }
            retModel = JenaUtils.create(mainModel, inverseModel);

            final Model directModel = JenaUtils.read(models.get(ContentType.DIRECT), Lang.NT);
            final Model inferredModel = JenaUtils.read(models.get(ContentType.DIRECT_INFERRED), Lang.NT);
            this.disseminationImproveService.checkPropertiesFromBacklog(retModel, directModel, inferredModel, cellarObject, inferred);
        } finally {
            JenaUtils.closeQuietly(mainModel, inverseModel);
        }
        //Skolemization based on the uri of the "targeted" object of the blank node
        if(normalized){
            VirtuosoSkolemizer skolemizer=new VirtuosoSkolemizer(true);
            retModel=skolemizer.skolemizeModel(retModel,cellarObject.getCellarId().getUri());
        }
        return retModel;
    }

    /**
     * Creates the rdf model for work tree.
     *
     * @param cellarId the cellar id
     * @param inferred the inferred
     * @param normalized the normalized
     * @return the model
     */
    private Model createRdfModelForWorkTree(final Work work, final boolean inferred,final boolean normalized) {
        final Model treeModel = ModelFactory.createDefaultModel();
        boolean embargoAllowed = cellarConfiguration.isCellarServiceDisseminationRetrieveEmbargoedResourceEnabled();

        Model workModel = null;
        try {
            workModel = this.createRdfModelForObject(work, inferred,normalized);

            treeModel.add(workModel);
            for (final Expression expression : work.getChildren()) {
                if (expression.isUnderEmbargo() && !embargoAllowed) {
                    continue;
                }

                Model expressionModel = null;
                try {
                    Model filteredExpressionModel = null;
                    try {
                        expressionModel = this.createRdfModelForObject(expression, inferred,normalized);
                        filteredExpressionModel = expressionModel.difference(workModel);
                        treeModel.add(filteredExpressionModel);
                    } finally {
                        JenaUtils.closeQuietly(filteredExpressionModel);
                    }
                    for (final Manifestation manifestation : expression.getChildren()) {
                        if (manifestation.isUnderEmbargo() && !embargoAllowed) {
                            continue;
                        }
                        Model manifestationModel = null;
                        Model filteredManifestationModel = null;
                        try {
                            manifestationModel = this.createRdfModelForObject(manifestation, inferred,normalized);
                            filteredManifestationModel = manifestationModel.difference(expressionModel);
                            treeModel.add(filteredManifestationModel);
                        } finally {
                            JenaUtils.closeQuietly(manifestationModel, filteredManifestationModel);
                        }
                    }
                } finally {
                    JenaUtils.closeQuietly(expressionModel);
                }
            }
        } finally {
            JenaUtils.closeQuietly(workModel);
        }

        return treeModel;
    }

    /**
     * Creates the rdf model for dossier tree.
     *
     * @param cellarId the cellar id
     * @param inferred the inferred
     * @param normalized the normalized
     * @return the model
     */
    private Model createRdfModelForDossierTree(final Dossier dossier, final boolean inferred,final boolean normalized) {
        final Model treeModel = ModelFactory.createDefaultModel();
        Model dossierModel = null;
        try {
            dossierModel = this.createRdfModelForObject(dossier, inferred,normalized);
            treeModel.add(dossierModel);
            for (final Event event : dossier.getChildren()) {
                Model eventModel = null;
                Model filteredEventModel = null;
                try {
                    eventModel = this.createRdfModelForObject(event, inferred,normalized);
                    filteredEventModel = eventModel.difference(dossierModel);
                    treeModel.add(filteredEventModel);
                } finally {
                    JenaUtils.closeQuietly(eventModel, filteredEventModel);
                }
            }
        } finally {
            JenaUtils.closeQuietly(dossierModel);
        }

        return treeModel;
    }

    /**
     * <p>
     * makePrefixUri.
     * </p>
     *
     * @param ps   a {@link java.lang.String} object.
     * @param psid a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    private String makePrefixUri(final String ps, final String psid) {
        if (StringUtils.isBlank(ps) || StringUtils.isBlank(psid)) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("Production system or production system id is not set: '{}' : '{}'").withMessageArgs(ps, psid).build();
        }
        return ps + ':' + psid;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ResponseEntity<?> doGetSparql(final String id, final String uri) {
        File tempFile = null;
        File transformFile = null;
        InputStream inputStream = null;
        try {
            final UriTemplates uriTemplatesById = this.uriTemplateService.getUriTemplatesById(Long.valueOf(id));
            final UriTemplate template = new UriTemplate(uriTemplatesById.getUriPattern());

            final String base = this.cellarConfiguration.getCellarUriDisseminationBase() + "resource";
            final Map<String, String> uriVariables = template.match(base + uri);
            String sparqlQuery = uriTemplatesById.getSparql();
            for (final Entry<String, String> entry : uriVariables.entrySet()) {
                sparqlQuery = StringUtils.replace(sparqlQuery, "{" + entry.getKey() + "}", entry.getValue());
            }
            tempFile = this.sparqlExecutingService.executeSparqlQueryFormat(sparqlQuery, APPLICATION_SPARQL_RESULTS_XML);

            if ((uriTemplatesById.getXslt() != null) && !uriTemplatesById.getXslt().isEmpty()) {
                final Stopwatch elapsed = Stopwatch.createStarted();
                final File xslt = new File(
                        this.cellarConfiguration.getCellarFolderSparqlResponseTemplates() + File.separator + uriTemplatesById.getXslt());
                transformFile = File.createTempFile(PREFIX_TEMP_FILE, SUFFIX_TEMP_FILE);
                this.xslTransformerService.transform(tempFile, xslt, transformFile);
                FileUtils.copyFile(transformFile, tempFile);

                xsltTimer.record(elapsed.elapsed());
            }

            inputStream = new FileInputStream(tempFile);
            final HttpHeadersBuilder httpHeadersBuilder = HttpHeadersBuilder.get().withCacheControl(CacheControl.NO_STORE).withDateNow()
                    .withContentType(MediaType.TEXT_XML);
            byte[] byteArray = IOUtils.toByteArray(inputStream);
            return new ResponseEntity<>(byteArray, httpHeadersBuilder.getHttpHeaders(), HttpStatus.OK);
        } catch (final Exception e) {
            return ControllerUtil.makeErrorResponse(e, HttpStatus.SERVICE_UNAVAILABLE);
        } finally {
            if (transformFile != null) {
                try {
                    FileUtils.deleteQuietly(transformFile);
                } catch (final Exception e) {
                    LOG.error(e.getMessage(), e);
                }
            }
            IOUtils.closeQuietly(inputStream);
            FileUtils.deleteQuietly(tempFile);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CellarResourceBean resolveCellarResourceBean(final String prefix, final String identifier) {
        final CellarResourceBean cellarResource = checkForCellarResource(prefix, identifier);
        boolean error = false;
        if (cellarResource == null) {
            error = true;
        }
        if (!error) {
            final boolean underEmbargo = cellarResource.isUnderEmbargo();
            final boolean cellarServiceDisseminationRetrieveEmbargoedResourceEnabled = this.cellarConfiguration
                    .isCellarServiceDisseminationRetrieveEmbargoedResourceEnabled();
            if (!cellarServiceDisseminationRetrieveEmbargoedResourceEnabled && underEmbargo) {
                error = true;
            }
            if (cellarServiceDisseminationRetrieveEmbargoedResourceEnabled && underEmbargo) {
                LOG.info("Retrieving embargoed content for dissemination");
            }
        }
        if (error) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                    .withMessage("Resource [system '{}' - id '{}'] not found.").withMessageArgs(prefix, identifier).build();
        }
        return cellarResource;
    }

}
