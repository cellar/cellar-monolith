/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.graph.impl
 *             FILE : AppendGraphBuilder.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 27-01-2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.graph.impl;

/**
 * <class_description> Graph builder implementation for append ingestion.
 * <br/><br/>
 * ON : May 2, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class AppendGraphBuilder extends CreateGraphBuilder {

    // TODO: do nothing, for now
    // to be implemented once OP will have created CELLAR ticket for the problem described at:
    // https://webgate.ec.europa.eu/publications/jira/browse/MAAS-803

}
