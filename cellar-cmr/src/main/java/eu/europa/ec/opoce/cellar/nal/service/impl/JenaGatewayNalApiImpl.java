/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.nal.service
 *             FILE : JenaGatewayNalApi.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 19-Apr-2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.nal.service.impl;

import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.common.util.MapUtils;
import eu.europa.ec.opoce.cellar.common.util.Pair;
import eu.europa.ec.opoce.cellar.common.util.Wrapper;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.domain.Concept;
import eu.europa.ec.opoce.cellar.nal.domain.ConceptScheme;
import eu.europa.ec.opoce.cellar.nal.domain.Domain;
import eu.europa.ec.opoce.cellar.nal.domain.Language;
import eu.europa.ec.opoce.cellar.nal.domain.LanguageString;
import eu.europa.ec.opoce.cellar.nal.domain.Uri;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.nal.service.JenaGatewayNalApi;
import eu.europa.ec.opoce.cellar.nal.service.LanguageService;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.semantic.jena.template.JenaTemplate;
import eu.europa.ec.opoce.cellar.semantic.jena.template.ListOfMapLoopHandlerWithoutResult;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import static eu.europa.ec.opoce.cellar.nal.utils.NalSparqlQueryTemplate.askForTypeConcept;
import static eu.europa.ec.opoce.cellar.nal.utils.NalSparqlQueryTemplate.askForTypeConceptScheme;
import static eu.europa.ec.opoce.cellar.nal.utils.NalSparqlQueryTemplate.askForTypeDomain;
import static eu.europa.ec.opoce.cellar.nal.utils.NalSparqlQueryTemplate.selectConcept;
import static eu.europa.ec.opoce.cellar.nal.utils.NalSparqlQueryTemplate.selectConceptScheme;
import static eu.europa.ec.opoce.cellar.nal.utils.NalSparqlQueryTemplate.selectConceptSchemes;
import static eu.europa.ec.opoce.cellar.nal.utils.NalSparqlQueryTemplate.selectConceptSchemesOneQuery;
import static eu.europa.ec.opoce.cellar.nal.utils.NalSparqlQueryTemplate.selectDomain;
import static eu.europa.ec.opoce.cellar.nal.utils.NalSparqlQueryTemplate.selectDomains;
import static eu.europa.ec.opoce.cellar.nal.utils.NalSparqlQueryTemplate.selectDomainsOneQuery;
import static eu.europa.ec.opoce.cellar.nal.utils.NalSparqlQueryTemplate.selectFallbackLabelFor;
import static eu.europa.ec.opoce.cellar.nal.utils.NalSparqlQueryTemplate.selectFallbackLabelsFor;
import static eu.europa.ec.opoce.cellar.nal.utils.NalSparqlQueryTemplate.selectRelatedConcepts;
import static eu.europa.ec.opoce.cellar.nal.utils.NalSparqlQueryTemplate.selectRelatedConceptsOneQuery;
import static eu.europa.ec.opoce.cellar.nal.utils.NalSparqlQueryTemplate.selectSupportedLanguages;

/**
 * <p>JenaGatewayNalApi class.</p>
 *
 * @author ARHS Developments
 * @version $$Revision$$$
 */
@Service
public class JenaGatewayNalApiImpl implements JenaGatewayNalApi {
    /**
     * The language service.
     */
    private final LanguageService languageService;

    /**
     * <p>Constructor for JenaGatewayNalApi.</p>
     *
     * @param languageService a {@link LanguageServiceImpl} object.
     */
    @Autowired
    public JenaGatewayNalApiImpl(LanguageService languageService) {
        this.languageService = languageService;
    }

    /**
     * <p>getSupportedLanguages.</p>
     *
     * @param model         a {@link org.apache.jena.rdf.model.Model} object.
     * @param conceptScheme a {@link java.lang.String} object.
     * @return an array of {@link Language} objects.
     */
    @Override
    public Language[] getSupportedLanguages(Model model, String conceptScheme) {
        return JenaUtils.select(selectSupportedLanguages(conceptScheme), model).stream()
                .map(s -> stringValueOf((Literal) s.get("code")))
                .filter(StringUtils::isNotBlank)
                .map(c -> new Language(getCode(c)))
                .toArray(Language[]::new);
    }

    /**
     * <p>getDomainsOneQuery.</p>
     *
     * @param model a {@link org.apache.jena.rdf.model.Model} object.
     * @return an array of {@link Domain} objects.
     */
    @Override
    public Domain[] getDomainsOneQuery(Model model) {
        if (model == null) {
            throw ExceptionBuilder.get(CellarException.class).build();
        }

        final Set<String> domains = new HashSet<>();
        final Map<String, Set<String>> identifiersPerDomain = new HashMap<>();
        final Map<String, Set<Uri>> conceptSchemesPerDomain = new HashMap<>();
        final Map<String, Map<String, Set<String>>> labelsPerLangPerDomain = new HashMap<>();

        final ListOfMapLoopHandlerWithoutResult handler = new ListOfMapLoopHandlerWithoutResult() {
            @Override
            protected void process(Map<String, RDFNode> result) {
                final Resource domain = (Resource) result.get("domain");
                domains.add(domain.getURI());

                final Set<String> identifiers = MapUtils.addMappedSet(identifiersPerDomain, domain.getURI());
                addStringValueIfNotBlanc((Literal) result.get("identifier"), identifiers);

                final Set<Uri> conceptSchemes = MapUtils.addMappedSet(conceptSchemesPerDomain, domain.getURI());
                final Resource cs = (Resource) result.get("cs");
                if (cs != null) {
                    conceptSchemes.add(new Uri(cs.getURI()));
                }

                final Map<String, Set<String>> labelsPerLang = MapUtils.addMappedMap(labelsPerLangPerDomain, domain.getURI());
                final Literal label = (Literal) result.get("label");
                if (label != null) { // TODO fallback labels?
                    MapUtils.addValueToMappedSet(labelsPerLang, label.getLanguage(), label.getString());
                }
            }
        };

        JenaTemplate.select(model, selectDomainsOneQuery(), handler);

        int domainIndex = 0;
        final Domain[] domainBeans = new Domain[domains.size()];
        for (final String domain : domains) {
            final Domain domainBean = new Domain(new Uri(domain));
            domainBeans[domainIndex++] = domainBean;

            final Map<String, Set<String>> labelsPerLang = labelsPerLangPerDomain.get(domain);
            final List<LanguageString> langLabels = new ArrayList<LanguageString>(labelsPerLang.size());
            for (final Entry<String, Set<String>> entry : labelsPerLang.entrySet()) {

                final String lang = entry.getKey();
                final Set<String> labels = entry.getValue();

                final int count = labels.size();
                if (count > 1) {
                    throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.NAL_API_DATA_CORRUPTION)
                            .withMessage("More than one skos-label found for domain '{}' - language '{}': {}")
                            .withMessageArgs(domain, lang, StringUtils.join(labels, ", ")).build();
                }

                if (count == 1) {
                    langLabels.add(new LanguageString(getCode(lang), labels.iterator().next()));
                }
            }

            final Set<String> identifiers = identifiersPerDomain.get(domain);
            final int count = identifiers.size();
            if (count > 1) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.NAL_API_DATA_CORRUPTION)
                        .withMessage("More than one dc-identifier found for domain '{}': {}")
                        .withMessageArgs(domain, StringUtils.join(identifiers, ", ")).build();
            }

            final String identifier = count == 1 ? identifiers.iterator().next() : null;

            final Set<Uri> conceptSchemes = conceptSchemesPerDomain.get(domain);
            domainBean.setConceptSchemes(conceptSchemes.toArray(new Uri[conceptSchemes.size()]));
            domainBean.setLabels(langLabels.toArray(new LanguageString[langLabels.size()]));
            domainBean.setIdentifier(identifier);
        }

        return domainBeans;
    }

    /**
     * <p>getDomains.</p>
     *
     * @param model a {@link org.apache.jena.rdf.model.Model} object.
     * @return an array of {@link Domain} objects.
     */
    @Override
    public Domain[] getDomains(Model model) {
        if (model == null) {
            throw ExceptionBuilder.get(CellarException.class).build();
        }

        final Set<String> domains = new HashSet<String>();

        final ListOfMapLoopHandlerWithoutResult handler = new ListOfMapLoopHandlerWithoutResult() {

            @Override
            protected void process(Map<String, RDFNode> result) {
                domains.add(((Resource) result.get("domain")).getURI());
            }
        };

        JenaTemplate.select(model, selectDomains(), handler);

        int domainIndex = 0;
        final Domain[] domainBeans = new Domain[domains.size()];
        for (final String domain : domains) {
            final Domain domainBean = getDomain(model, domain);
            if (domainBean == null) {
                throw ExceptionBuilder.get(CellarException.class).build();
            }

            domainBeans[domainIndex++] = domainBean;
        }

        return domainBeans;
    }

    /**
     * <p>getDomain.</p>
     *
     * @param model  a {@link org.apache.jena.rdf.model.Model} object.
     * @param domain a {@link java.lang.String} object.
     * @return a {@link Domain} object.
     */
    @Override
    public Domain getDomain(Model model, String domain) {
        if ((model == null) || StringUtils.isBlank(domain)) {
            throw ExceptionBuilder.get(CellarException.class).build();
        }

        if (!isDomain(model, domain)) {
            return null;
        }

        final Set<String> identifiers = new HashSet<String>();
        final Map<String, Set<String>> labelsPerLang = new HashMap<String, Set<String>>();
        final Set<Uri> conceptSchemes = new HashSet<Uri>();

        final ListOfMapLoopHandlerWithoutResult handler = new ListOfMapLoopHandlerWithoutResult() {

            @Override
            protected void process(Map<String, RDFNode> result) {
                addStringValueIfNotBlanc((Literal) result.get("identifier"), identifiers);

                final Resource cs = (Resource) result.get("cs");
                if (cs != null) {
                    conceptSchemes.add(new Uri(cs.getURI()));
                }

                final Literal label = (Literal) result.get("label");
                if (label != null) { // TODO fallback labels?
                    MapUtils.addValueToMappedSet(labelsPerLang, label.getLanguage(), label.getString());
                }
            }
        };

        JenaTemplate.select(model, selectDomain(domain), handler);

        int count = identifiers.size();
        if (count > 1) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.NAL_API_DATA_CORRUPTION)
                    .withMessage("More than one dc-identifier found for domain '{}': {}")
                    .withMessageArgs(domain, StringUtils.join(identifiers, ", ")).build();
        }
        final String identifier = count == 1 ? identifiers.iterator().next() : null;

        final List<LanguageString> langLabels = new ArrayList<LanguageString>(labelsPerLang.size());

        for (final Entry<String, Set<String>> entry : labelsPerLang.entrySet()) {
            final String lang = entry.getKey();
            final Set<String> labels = entry.getValue();

            count = labels.size();
            if (count > 1) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.NAL_API_DATA_CORRUPTION)
                        .withMessage("More than one skos-label found for domain '{}' - language '{}': {}")
                        .withMessageArgs(domain, lang, StringUtils.join(labels, ", ")).build();
            }

            if (count == 1) {
                langLabels.add(new LanguageString(getCode(lang), labels.iterator().next()));
            }
        }

        return new Domain(new Uri(domain)) {

            {
                setIdentifier(identifier);
                setLabels(langLabels.toArray(new LanguageString[langLabels.size()]));
                setConceptSchemes(conceptSchemes.toArray(new Uri[conceptSchemes.size()]));
            }
        };
    }

    /**
     * <p>getConceptSchemes.</p>
     *
     * @param model        a {@link org.apache.jena.rdf.model.Model} object.
     * @param lastModified a {@link java.util.Date} object.
     * @return an array of {@link ConceptScheme} objects.
     */
    @Override
    public ConceptScheme[] getConceptSchemes(Model model, Date lastModified) {
        if (model == null) {
            throw ExceptionBuilder.get(CellarException.class).build();
        }

        final Set<String> concepSchemes = new HashSet<String>();

        final ListOfMapLoopHandlerWithoutResult handler = new ListOfMapLoopHandlerWithoutResult() {

            @Override
            protected void process(Map<String, RDFNode> result) {
                concepSchemes.add(((Resource) result.get("cs")).getURI());
            }
        };

        JenaTemplate.select(model, selectConceptSchemes(), handler);

        int csIndex = 0;
        final ConceptScheme[] csBeans = new ConceptScheme[concepSchemes.size()];
        for (final String cs : concepSchemes) {
            final ConceptScheme csBean = getConceptScheme(model, cs);
            if (csBean == null) {
                throw ExceptionBuilder.get(CellarException.class).build();
            }

            csBeans[csIndex++] = csBean;
        }

        return csBeans;
    }

    // TODO use fallback labels if needed, add lastModified attribute, add optional version and date attributes

    /**
     * <p>getConceptSchemesOneQuery.</p>
     *
     * @param model        a {@link org.apache.jena.rdf.model.Model} object.
     * @param lastModified a {@link java.util.Date} object.
     * @return an array of {@link ConceptScheme} objects.
     */
    @Override
    public ConceptScheme[] getConceptSchemesOneQuery(Model model, Date lastModified) {
        if (model == null) {
            throw ExceptionBuilder.get(CellarException.class).build();
        }

        final Set<String> concepSchemes = new HashSet<String>();
        final Map<String, Map<String, Set<String>>> labelsPerLangPerCs = new HashMap<String, Map<String, Set<String>>>();

        final ListOfMapLoopHandlerWithoutResult handler = new ListOfMapLoopHandlerWithoutResult() {

            @Override
            protected void process(Map<String, RDFNode> result) {
                final Resource cs = (Resource) result.get("cs");
                concepSchemes.add(cs.getURI());

                final Map<String, Set<String>> labelsPerLang = MapUtils.addMappedMap(labelsPerLangPerCs, cs.getURI());
                final Literal label = (Literal) result.get("label");
                if (label != null) {
                    MapUtils.addValueToMappedSet(labelsPerLang, label.getLanguage(), label.getString());
                }
            }
        };

        JenaTemplate.select(model, selectConceptSchemesOneQuery(), handler);

        int csIndex = 0;
        final ConceptScheme[] csBeans = new ConceptScheme[concepSchemes.size()];
        for (final String cs : concepSchemes) {
            final ConceptScheme csBean = new ConceptScheme(new Uri(cs));
            csBeans[csIndex++] = csBean;

            final Map<String, Set<String>> labelsPerLang = labelsPerLangPerCs.get(cs);
            final List<LanguageString> langLabels = new ArrayList<LanguageString>(labelsPerLang.size());
            for (final Entry<String, Set<String>> entry : labelsPerLang.entrySet()) {
                final String lang = entry.getKey();
                final Set<String> labels = entry.getValue();

                final int count = labels.size();
                if (count > 1) {
                    throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.NAL_API_DATA_CORRUPTION)
                            .withMessage("More than one skos-label found for conceptScheme '{}' - language '{}': {}")
                            .withMessageArgs(cs, lang, StringUtils.join(labels, ", ")).build();
                }

                if (count == 1) {
                    langLabels.add(new LanguageString(getCode(lang), labels.iterator().next()));
                }
            }

            csBean.setLabels(langLabels.toArray(new LanguageString[langLabels.size()]));
        }

        return csBeans;
    }

    // TODO use fallback labels if needed, add lastModified attribute, optional version and date attributes

    /**
     * <p>getConceptScheme.</p>
     *
     * @param model         a {@link org.apache.jena.rdf.model.Model} object.
     * @param conceptScheme a {@link java.lang.String} object.
     * @return a {@link ConceptScheme} object.
     */
    @Override
    public ConceptScheme getConceptScheme(Model model, final String conceptScheme) {
        if ((model == null) || StringUtils.isBlank(conceptScheme)) {
            throw ExceptionBuilder.get(CellarException.class).build();
        }

        if (!isConceptScheme(model, conceptScheme)) {
            return null;
        }

        final Map<String, Set<String>> labelsPerLang = new HashMap<String, Set<String>>();

        final ListOfMapLoopHandlerWithoutResult handler = new ListOfMapLoopHandlerWithoutResult() {

            @Override
            protected void process(Map<String, RDFNode> result) {
                final Literal label = (Literal) result.get("label");
                if (label != null) {
                    MapUtils.addValueToMappedSet(labelsPerLang, label.getLanguage(), label.getString());
                }
            }
        };

        JenaTemplate.select(model, selectConceptScheme(conceptScheme), handler);

        final List<LanguageString> langLabels = new ArrayList<LanguageString>(labelsPerLang.size());
        for (final Entry<String, Set<String>> entry : labelsPerLang.entrySet()) {
            final String lang = entry.getKey();
            final Set<String> labels = entry.getValue();

            final int count = labels.size();
            if (count > 1) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.NAL_API_DATA_CORRUPTION)
                        .withMessage("More than one skos-label found for conceptScheme '{}' - language '{}': {}")
                        .withMessageArgs(conceptScheme, lang, StringUtils.join(labels, ", ")).build();
            }

            if (count == 1) {
                langLabels.add(new LanguageString(getCode(lang), labels.iterator().next()));
            }
        }

        return new ConceptScheme(new Uri(conceptScheme)) {

            {
                setLabels(langLabels.toArray(new LanguageString[langLabels.size()]));
            }
        };
    }

    /**
     * <p>getTopConcepts.</p>
     *
     * @param model         a {@link org.apache.jena.rdf.model.Model} object.
     * @param conceptScheme a {@link java.lang.String} object.
     * @param language      a {@link java.lang.String} object.
     * @return an array of {@link Concept} objects.
     */
    @Override
    public Concept[] getTopConcepts(Model model, String conceptScheme, String language) {
        if ((model == null) || StringUtils.isBlank(conceptScheme) || StringUtils.isBlank(language)) {
            throw ExceptionBuilder.get(CellarException.class).build();
        }

        return getRelatedConceptsOneQuery(model, conceptScheme, CellarProperty.skos_hasTopConcept, language);
    }

    /**
     * <p>getRelatedConcepts.</p>
     *
     * @param model    a {@link org.apache.jena.rdf.model.Model} object.
     * @param concept  a {@link java.lang.String} object.
     * @param relation a {@link java.lang.String} object.
     * @param language a {@link java.lang.String} object.
     * @return an array of {@link Concept} objects.
     */
    @Override
    public Concept[] getRelatedConcepts(Model model, String concept, String relation, String language) {
        if ((model == null) || StringUtils.isBlank(concept) || StringUtils.isBlank(relation) || StringUtils.isBlank(language)) {
            throw ExceptionBuilder.get(CellarException.class).build();
        }

        final Set<String> relatedConcepts = new HashSet<String>();

        final ListOfMapLoopHandlerWithoutResult handler = new ListOfMapLoopHandlerWithoutResult() {

            @Override
            protected void process(Map<String, RDFNode> result) {
                relatedConcepts.add(((Resource) result.get("concept")).getURI());
            }
        };

        JenaTemplate.select(model, selectRelatedConcepts(concept, relation), handler);

        int conceptIndex = 0;
        final Concept[] conceptBeans = new Concept[relatedConcepts.size()];
        for (final String relatedConcept : relatedConcepts) {
            final Concept conceptBean = getConcept(model, relatedConcept, language);
            if (conceptBean == null) {
                throw ExceptionBuilder.get(CellarException.class).build();
            }

            conceptBeans[conceptIndex++] = conceptBean;
        }

        return conceptBeans;
    }

    /**
     * <p>getRelatedConceptsOneQuery.</p>
     *
     * @param model    a {@link org.apache.jena.rdf.model.Model} object.
     * @param concept  a {@link java.lang.String} object.
     * @param relation a {@link java.lang.String} object.
     * @param language a {@link java.lang.String} object.
     * @return an array of {@link Concept} objects.
     */
    @Override
    public Concept[] getRelatedConceptsOneQuery(Model model, String concept, String relation, String language) {
        if ((model == null) || StringUtils.isBlank(concept) || StringUtils.isBlank(relation) || StringUtils.isBlank(language)) {
            throw ExceptionBuilder.get(CellarException.class).build();
        }

        final Set<String> concepts = new HashSet<String>();

        final Map<String, Set<String>> prefLabelsPerConcept = new HashMap<String, Set<String>>();
        final Map<String, Set<String>> altLabelsPerConcept = new HashMap<String, Set<String>>();
        final Map<String, Set<String>> hiddenLabelsPerConcept = new HashMap<String, Set<String>>();
        final Map<String, Set<String>> identifiersPerConcept = new HashMap<String, Set<String>>();
        final Map<String, Set<String>> notationsPerConcept = new HashMap<String, Set<String>>();

        final ListOfMapLoopHandlerWithoutResult handler = new ListOfMapLoopHandlerWithoutResult() {

            @Override
            protected void process(Map<String, RDFNode> result) {
                final Resource concept = (Resource) result.get("concept");
                concepts.add(concept.getURI());

                addStringValueIfNotBlanc((Literal) result.get("preflabel"), MapUtils.addMappedSet(prefLabelsPerConcept, concept.getURI()));
                addStringValueIfNotBlanc((Literal) result.get("altlabel"), MapUtils.addMappedSet(altLabelsPerConcept, concept.getURI()));
                addStringValueIfNotBlanc((Literal) result.get("hiddenlabel"),
                        MapUtils.addMappedSet(hiddenLabelsPerConcept, concept.getURI()));
                addStringValueIfNotBlanc((Literal) result.get("identifier"),
                        MapUtils.addMappedSet(identifiersPerConcept, concept.getURI()));

                final Set<String> notations = MapUtils.addMappedSet(notationsPerConcept, concept.getURI());
                final RDFNode notation = result.get("notation");
                if ((notation != null) && notation.isLiteral()) {
                    addStringValueIfNotBlanc((Literal) notation, notations);
                }
            }
        };

        final Pair<String, String> languageTags = isoCharCodes(language);
        JenaTemplate.select(model, selectRelatedConceptsOneQuery(concept, relation, languageTags.getOne(), languageTags.getTwo()), handler);

        int conceptIndex = 0;
        final Concept[] conceptBeans = new Concept[concepts.size()];

        final Map<String, Concept> conceptsWithMissingLabel = new HashMap<String, Concept>();
        for (final String relatedConcept : concepts) {
            final Concept conceptBean = new Concept(new Uri(relatedConcept));
            conceptBeans[conceptIndex++] = conceptBean;

            final Set<String> prefLabels = prefLabelsPerConcept.get(relatedConcept);
            final int prefLabelCount = prefLabels.size();

            final Set<String> identifiers = identifiersPerConcept.get(relatedConcept);
            final int idCount = identifiers.size();

            if (prefLabels.size() > 1) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.NAL_API_DATA_CORRUPTION)
                        .withMessage("More the one skos-preflabel found for concept '{}' - language '{}': {}")
                        .withMessageArgs(relatedConcept, language, StringUtils.join(prefLabels, ", ")).build();
            }
            if (idCount > 1) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.NAL_API_DATA_CORRUPTION)
                        .withMessage("More the one dc-identifier found for concept '{}': {}")
                        .withMessageArgs(relatedConcept, StringUtils.join(identifiers, ", ")).build();
            }

            final LanguageString preflabel = prefLabelCount == 1 ? new LanguageString(languageTags.getTwo(), prefLabels.iterator().next())
                    : null;
            if ((preflabel == null) || StringUtils.isBlank(preflabel.getString())) {
                conceptsWithMissingLabel.put(relatedConcept, conceptBean);
            }

            final String identifier = idCount == 1 ? identifiers.iterator().next() : null;

            conceptBean.setLanguage(language);
            conceptBean.setIdentifier(identifier);
            conceptBean.setPrefLabel(preflabel);

            final Set<String> altLabels = altLabelsPerConcept.get(relatedConcept);
            conceptBean.setAltLabels(altLabels.toArray(new String[altLabels.size()]));

            final Set<String> hiddenLabels = hiddenLabelsPerConcept.get(relatedConcept);
            conceptBean.setHiddenLabels(hiddenLabels.toArray(new String[hiddenLabels.size()]));

            final Set<String> notations = notationsPerConcept.get(relatedConcept);
            conceptBean.setNotations(notations.toArray(new String[notations.size()]));
        }

        if (conceptsWithMissingLabel.isEmpty()) {
            return conceptBeans;
        }

        final ListOfMapLoopHandlerWithoutResult fallbackHandler = new ListOfMapLoopHandlerWithoutResult() {

            @Override
            protected void process(Map<String, RDFNode> result) {
                final String concept = ((Resource) result.get("concept")).getURI();

                final Literal literal = (Literal) result.get("flabel");
                final Literal lang = (Literal) result.get("flanguage");

                conceptsWithMissingLabel.get(concept).setPrefLabel(new LanguageString(getCode(lang.getString()), literal.getString()));
            }
        };

        JenaTemplate.select(model, selectFallbackLabelsFor(conceptsWithMissingLabel.keySet(), languageTags.getOne(), languageTags.getTwo()),
                fallbackHandler);

        return conceptBeans;
    }

    /**
     * <p>getConcept.</p>
     *
     * @param model    a {@link org.apache.jena.rdf.model.Model} object.
     * @param concept  a {@link java.lang.String} object.
     * @param language a {@link java.lang.String} object.
     * @return a {@link Concept} object.
     */
    @Override
    public Concept getConcept(Model model, String concept, final String language) {
        if (!isConcept(model, concept)) {
            return null;
        }

        final Set<String> prefLabels = new HashSet<String>();
        final Set<String> altLabels = new HashSet<String>();
        final Set<String> hiddenLabels = new HashSet<String>();
        final Set<String> notations = new HashSet<String>();
        final Set<String> identifiers = new HashSet<String>();

        final ListOfMapLoopHandlerWithoutResult handler = new ListOfMapLoopHandlerWithoutResult() {

            @Override
            protected void process(Map<String, RDFNode> result) {
                addStringValueIfNotBlanc((Literal) result.get("preflabel"), prefLabels);
                addStringValueIfNotBlanc((Literal) result.get("altlabel"), altLabels);
                addStringValueIfNotBlanc((Literal) result.get("hiddenlabel"), hiddenLabels);
                addStringValueIfNotBlanc((Literal) result.get("identifier"), identifiers);

                final RDFNode notation = result.get("notation");
                if ((notation != null) && notation.isLiteral()) {
                    addStringValueIfNotBlanc((Literal) notation, notations);
                }
            }
        };

        final Pair<String, String> languageTags = isoCharCodes(language);
        JenaTemplate.select(model, selectConcept(concept, languageTags.getOne(), languageTags.getTwo()), handler);

        final int prefLabelCount = prefLabels.size();
        final int idCount = identifiers.size();
        if (prefLabels.size() > 1) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.NAL_API_DATA_CORRUPTION)
                    .withMessage("More the one skos-preflabel found for concept '{}' - language '{}': {}")
                    .withMessageArgs(concept, language, StringUtils.join(prefLabels, ", ")).build();
        }
        if (idCount > 1) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.NAL_API_DATA_CORRUPTION)
                    .withMessage("More the one dc-identifier found for concept '{}': {}")
                    .withMessageArgs(concept, StringUtils.join(identifiers, ", ")).build();
        }

        final String identifier = idCount == 1 ? identifiers.iterator().next() : null;

        final Wrapper<LanguageString> preflabel = new Wrapper<LanguageString>();
        if (prefLabelCount == 1) {
            preflabel.setValue(new LanguageString(languageTags.getTwo(), prefLabels.iterator().next()));
        }

        if ((preflabel.getValue() == null) || StringUtils.isBlank(preflabel.getValue().getString())) {
            final ListOfMapLoopHandlerWithoutResult fallbackHandler = new ListOfMapLoopHandlerWithoutResult() {

                @Override
                protected void process(Map<String, RDFNode> result) {
                    final Literal literal = (Literal) result.get("flabel");
                    final Literal lang = (Literal) result.get("flanguage");

                    preflabel.setValue(new LanguageString(getCode(lang.getString()), literal.getString()));
                }
            };

            JenaTemplate.select(model, selectFallbackLabelFor(concept, languageTags.getOne(), languageTags.getTwo()), fallbackHandler);
        }

        return new Concept(new Uri(concept)) {

            {
                setLanguage(language);
                setIdentifier(identifier);
                setPrefLabel(preflabel.getValue());
                setAltLabels(altLabels.toArray(new String[altLabels.size()]));
                setHiddenLabels(hiddenLabels.toArray(new String[hiddenLabels.size()]));
                setNotations(notations.toArray(new String[notations.size()]));
            }
        };
    }

    /**
     * <p>addStringValueIfNotBlanc.</p>
     *
     * @param literal a {@link org.apache.jena.rdf.model.Literal} object.
     * @param labels  a {@link java.util.Set} object.
     */
    private void addStringValueIfNotBlanc(Literal literal, Set<String> labels) {
        final String stringValue = stringValueOf(literal);
        if (!StringUtils.isBlank(stringValue)) {
            labels.add(stringValue);
        }
    }

    /**
     * <p>stringValueOf.</p>
     *
     * @param literal a {@link org.apache.jena.rdf.model.Literal} object.
     * @return a {@link java.lang.String} object.
     */
    private static String stringValueOf(Literal literal) {
        return literal != null ? literal.getString() : null;
    }

    /**
     * <p>isConceptScheme.</p>
     *
     * @param model       a {@link org.apache.jena.rdf.model.Model} object.
     * @param resourceUri a {@link java.lang.String} object.
     * @return a boolean.
     */
    private boolean isConceptScheme(Model model, String resourceUri) {
        return JenaTemplate.ask(model, askForTypeConceptScheme(resourceUri));
    }

    /**
     * <p>isDomain.</p>
     *
     * @param model       a {@link org.apache.jena.rdf.model.Model} object.
     * @param resourceUri a {@link java.lang.String} object.
     * @return a boolean.
     */
    private boolean isDomain(Model model, String resourceUri) {
        return JenaTemplate.ask(model, askForTypeDomain(resourceUri));
    }

    /**
     * <p>isConcept.</p>
     *
     * @param model       a {@link org.apache.jena.rdf.model.Model} object.
     * @param resourceUri a {@link java.lang.String} object.
     * @return a boolean.
     */
    private boolean isConcept(Model model, String resourceUri) {
        return JenaTemplate.ask(model, askForTypeConcept(resourceUri));
    }

    /**
     * <p>isoCharCodes.</p>
     *
     * @param language a {@link java.lang.String} object.
     * @return a {@link eu.europa.ec.opoce.cellar.common.util.Pair} object.
     */
    private Pair<String, String> isoCharCodes(String language) {
        final LanguageBean langBean = languageService.getByTwoOrThreeChar(language);
        if (langBean != null) {
            return new Pair<>(langBean.getIsoCodeTwoChar(), langBean.getIsoCodeThreeChar());
        } else {
            return new Pair<>(language, language);
        }
    }

    /**
     * <p>getCode.</p>
     *
     * @param lang a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    private String getCode(String lang) {
        return isoCharCodes(lang).getTwo();
    }
}
