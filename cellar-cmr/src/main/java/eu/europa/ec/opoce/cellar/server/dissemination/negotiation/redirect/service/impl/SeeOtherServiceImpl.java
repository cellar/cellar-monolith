/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.redirect.service.impl
 *             FILE : SeeOtherServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 21, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.redirect.service.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.service.IMementoService;
import eu.europa.ec.opoce.cellar.common.http.headers.HttpHeadersBuilder;
import eu.europa.ec.opoce.cellar.common.http.headers.ILink;
import eu.europa.ec.opoce.cellar.common.http.headers.LinkImpl;
import eu.europa.ec.opoce.cellar.common.http.headers.RelType;
import eu.europa.ec.opoce.cellar.common.http.headers.directives.CacheControl;
import eu.europa.ec.opoce.cellar.common.http.headers.directives.Tcn;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.DisseminationRequest;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.InferenceVariance;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.Structure;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.Type;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.redirect.service.AlternatesService;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.redirect.service.ISeeOtherService;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.LinkedList;
import java.util.Set;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 21, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class SeeOtherServiceImpl implements ISeeOtherService {

    private static final Logger LOG = LogManager.getLogger(SeeOtherServiceImpl.class);

    /** The identifier service. */
    @Autowired
    @Qualifier("pidManagerService")
    private IdentifierService identifierService;

    /** The alternates service. */
    @Autowired
    private AlternatesService alternatesService;

    @Autowired
    private IMementoService mementoService;

    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    /** {@inheritDoc} */
    @Override
    public ResponseEntity<Void> negotiateSeeOtherForLink(final CellarResourceBean cellarResource) {
        return this.constructResponse(cellarResource, Type.LINK.getURLToken(), null);
    }

    /** {@inheritDoc} */
    @Override
    public ResponseEntity<Void> negotiateSeeOtherForXml(final CellarResourceBean cellarResource, final Structure structure,
            final LanguageBean decodingLanguage, final boolean provideAlternates) {

        LOG.debug("Negotiate {} notice request [resource '{}'({}) - decoding language '{}']", structure, cellarResource.getCellarId(),
                cellarResource.getCellarType(), decodingLanguage);

        String language = StringUtils.isNotBlank(decodingLanguage.getIsoCodeTwoChar()) ?
                decodingLanguage.getIsoCodeTwoChar() : decodingLanguage.getIsoCodeThreeChar();
        final String extensionPattern = "%s/%s?decoding=%s";
        final String extension = String.format(extensionPattern, Type.XML.getURLToken(), structure.getURLToken(),
                language);

        Set<String> alternatesWithPlaceHolder = null;
        if (provideAlternates) {
            alternatesWithPlaceHolder = this.alternatesService.getXmlAlternatesWithBaseUriPlaceHolder(cellarResource.getCellarType(),
                    extensionPattern, structure, decodingLanguage);
        }

        return this.constructResponse(cellarResource, extension, alternatesWithPlaceHolder);
    }

    /** {@inheritDoc} */
    @Override
    public ResponseEntity<Void> negotiateSeeOtherForRdf(final CellarResourceBean cellarResource, final Structure structure,
            final InferenceVariance inferenceMode, final boolean provideAlternates,final boolean normalized) {

        LOG.debug("Negotiate {} RDF/XML notice request [resource '{}'({}) - inference mode '{}'-normalized '{}']", structure.getErrorLabel(),
                cellarResource.getCellarId(), cellarResource.getCellarType(), inferenceMode,normalized);

        final String extensionPattern = normalized?"%s/%s/%s/%s":"%s/%s/%s";
        final String extension = String.format(extensionPattern, Type.RDF.getURLToken(), structure.getURLToken(),
                inferenceMode.getURLToken(),normalized?"normalized":null);

        Set<String> alternatesWithPlaceHolder = null;
        if (provideAlternates) {
            alternatesWithPlaceHolder = this.alternatesService.getRdfAlternatesWithBaseUriPlaceHolder(cellarResource.getCellarType(),
                    extensionPattern, structure, inferenceMode,normalized);
        }

        return this.constructResponse(cellarResource, extension, alternatesWithPlaceHolder);
    }

    /** {@inheritDoc} */
    @Override
    public ResponseEntity<Void> negotiateSeeOtherForList(final CellarResourceBean cellarResource, final boolean provideAlternates) {
        return this.negotiateSeeOtherForType(Type.LIST, cellarResource, provideAlternates);
    }

    /** {@inheritDoc} */
    @Override
    public ResponseEntity<Void> negotiateSeeOtherForZip(final CellarResourceBean cellarResource, final boolean provideAlternates) {
        return this.negotiateSeeOtherForType(Type.ZIP, cellarResource, provideAlternates);
    }

    /** {@inheritDoc} */
    @Override
    public ResponseEntity<Void> negotiateSeeOtherForIdentifiers(final CellarResourceBean cellarResource) {
        return this.negotiateSeeOtherForType(Type.IDENTIFIERS, cellarResource, false);
    }

    /** {@inheritDoc} */
    @Override
    public ResponseEntity<Void> negotiateSeeOtherForConceptScheme(final String conceptSchemeId, final String conceptId,
            final DisseminationRequest disseminationRequest) {
        //get cellar dissemination base
        final String base = this.cellarConfiguration.getCellarUriDisseminationBase() + "resource";
        String location = null;
        final String type = disseminationRequest.getType(null).getURLToken();
        if (StringUtils.isNotBlank(conceptId)) {
            //build redirect url e.g.:http://publications.europa.eu/resource/authority/fd_091.CADUC/rdf
            location = base + "/authority/" + conceptSchemeId + "." + conceptId + "/" + type;
        } else {
            //build redirect url e.g.:http://publications.europa.eu/resource/authority/fd_091/rdf
            location = base + "/authority/" + conceptSchemeId + "/" + type;
        }
        // Standard Headers
        final HttpHeadersBuilder httpHeadersBuilder = HttpHeadersBuilder.get();
        httpHeadersBuilder.withCacheControl(CacheControl.NO_STORE);
        httpHeadersBuilder.withDateNow();
        httpHeadersBuilder.withLocation(location);
        httpHeadersBuilder.withTcn(Tcn.CHOICE);
        final HttpHeaders httpHeaders = httpHeadersBuilder.getHttpHeaders();
        final ResponseEntity<Void> result = new ResponseEntity<Void>(httpHeaders, HttpStatus.SEE_OTHER);
        return result;
    }

    /**
     * Negotiate see other for type.
     *
     * @param type the type
     * @param cellarResource the cellar resource
     * @param provideAlternates the provide alternates
     * @return the response entity
     */
    private ResponseEntity<Void> negotiateSeeOtherForType(final Type type, final CellarResourceBean cellarResource,
            final boolean provideAlternates) {

        LOG.debug("Negotiate {} request for resource '{}'", type, cellarResource.getCellarId());

        final String extensionPattern = "%s";
        final String extension = String.format(extensionPattern, type.getURLToken());

        Set<String> alternatesWithPlaceHolder = null;
        if (provideAlternates) {
            alternatesWithPlaceHolder = this.alternatesService.getContentPoolAlternatesWithBaseUriPlaceHolder(cellarResource,
                    extensionPattern, type);
        }

        return this.constructResponse(cellarResource, extension, alternatesWithPlaceHolder);
    }

    /**
     * Construct response.
     *
     * @param cellarResource the cellar resource
     * @param extension the extension
     * @param alternatesWithPlaceHolder the alternates with place holder
     * @return the response entity
     */
    private ResponseEntity<Void> constructResponse(final CellarResourceBean cellarResource, final String extension,
            final Set<String> alternatesWithPlaceHolder) {

        final String base = this.identifierService.getUri(cellarResource.getCellarId());
        final String location = base + "/" + extension;
        final Collection<ILink> links = new LinkedList<ILink>();

        if (cellarConfiguration.isCellarServiceDisseminationMementoEnabled() && mementoService.isEvolutiveWorkOrMemento(base)) {
            final String originalURI = mementoService.getOriginalCellarUri(base);
            links.add(new LinkImpl(originalURI, RelType.ORIGINAL_TIMEGATE));
            links.add(new LinkImpl(originalURI, RelType.TIMEMAP));
        }

        // Standard Headers
        HttpHeadersBuilder httpHeadersBuilder = HttpHeadersBuilder.get().withCacheControl(CacheControl.NO_STORE).withDateNow()
                .withLocation(location) //
                .withTcn(Tcn.CHOICE); // All resources are transparently negotiated

        if (links.size() > 0) {
            httpHeadersBuilder = httpHeadersBuilder.withLink(StringUtils.join(links, ","));
        }

        if ((alternatesWithPlaceHolder != null)) {
            // If no alternates exist (= set is empty), the response header is empty. It must be present (see spec 'draft_17_04_14_v6.doc') in the 303 response so we cannot omit it.
            final Set<String> alternates = this.alternatesService.applyBaseUriToAlternatesWithPlaceHolder(base, alternatesWithPlaceHolder);
            httpHeadersBuilder = httpHeadersBuilder.withAlternates(alternates);
        }

        final HttpHeaders httpHeaders = httpHeadersBuilder.getHttpHeaders();

        return new ResponseEntity<Void>(httpHeaders, HttpStatus.SEE_OTHER);
    }

}
