/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice.xslt.function
 *        FILE : EmbeddedNoticeRetrievalMethodType.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 02-01-2023
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2023 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.notice.xslt.function;

/**
 * Contains the embedded notice retrieval strategy to be followed
 * while performing the embedded notice calculation during dissemination.
 * @author EUROPEAN DYNAMICS S.A
 */
public enum EmbeddedNoticeRetrievalMethodType {

	/**
	 * 
	 */
	SINGLE_RESOURCE,
	/**
	 * 
	 */
	SPECIFIC_HIERARCHY,
	/**
	 * 
	 */
	STARTING_WITH
	
}
