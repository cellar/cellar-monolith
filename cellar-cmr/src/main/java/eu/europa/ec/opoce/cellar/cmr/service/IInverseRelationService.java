/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.service
 *             FILE : IInverseRelationService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 24, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.service;

import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;
import eu.europa.ec.opoce.cellar.cmr.daobeans.InverseRelation;

import java.util.Collection;

/**
 * <class_description> Inverse relations service interface.
 * <br/><br/>
 * ON : Apr 24, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IInverseRelationService {

    /**
     * Find the inverse relations on target id.
     * @param productionIdentifiers production ids to use on target id
     * @return the inverse relations
     */
    Collection<InverseRelation> getInverseRelationsOnTarget(final Collection<Identifier> productionIdentifiers);

    /**
     * Find the inverse relations on source id.
     * @param productionIdentifiers production ids to use on target id
     * @return the inverse relations
     */
    Collection<InverseRelation> getInverseRelationsOnSource(final Collection<Identifier> productionIdentifiers);

    void updateInverseRelations(final Collection<InverseRelation> inverseRelationsToDelete,
            final Collection<InverseRelation> inverseRelationsToAdd);
}
