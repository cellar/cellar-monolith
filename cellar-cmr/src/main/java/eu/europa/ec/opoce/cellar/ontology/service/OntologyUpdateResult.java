package eu.europa.ec.opoce.cellar.ontology.service;

import org.apache.jena.rdf.model.Model;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ARHS Developments
 */
public class OntologyUpdateResult {

    private final Map<Model, Boolean> models = new HashMap<>();

    public OntologyUpdateResult() {
    }

    public Map<Model, Boolean> getModels() {
        return models;
    }

    public void addModel(Model model, boolean updated) {
        this.models.put(model, updated);
    }
}
