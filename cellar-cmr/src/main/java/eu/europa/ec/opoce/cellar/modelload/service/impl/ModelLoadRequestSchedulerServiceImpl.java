/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.modelload.service.impl
 *             FILE : ModelLoadRequestSchedulerImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Mar 18, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.modelload.service.impl;

import eu.europa.ec.opoce.cellar.ExecutionStatus;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditBuilder;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventAction;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventProcess;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventType;
import eu.europa.ec.opoce.cellar.cmr.database.dao.ModelLoadRequestDao;
import eu.europa.ec.opoce.cellar.modelload.domain.ModelLoadRequest;
import eu.europa.ec.opoce.cellar.modelload.service.ModelLoadRequestProcessingService;
import eu.europa.ec.opoce.cellar.modelload.service.ModelLoadRequestSchedulerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Date;

import static eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventType.End;
import static eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventType.Fail;
import static eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventType.Start;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Mar 18, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service("modelLoadRequestSchedulerService")
public class ModelLoadRequestSchedulerServiceImpl implements ModelLoadRequestSchedulerService {

    private static final Logger LOG = LogManager.getLogger(ModelLoadRequestSchedulerServiceImpl.class);

    private ICellarConfiguration cellarConfiguration;
    private ModelLoadRequestDao modelLoadRequestDao;
    private ModelLoadRequestProcessingService modelLoadRequestProcessingService;

    @Autowired
    public ModelLoadRequestSchedulerServiceImpl(@Qualifier("cellarConfiguration") ICellarConfiguration cellarConfiguration,
                                                ModelLoadRequestDao modelLoadRequestDao,
                                                ModelLoadRequestProcessingService modelLoadRequestProcessingService) {
        this.cellarConfiguration = cellarConfiguration;
        this.modelLoadRequestDao = modelLoadRequestDao;
        this.modelLoadRequestProcessingService = modelLoadRequestProcessingService;
    }

    @Override
    public void scheduleModelLoadRequests() {

        if (!this.cellarConfiguration.isCellarServiceNalLoadEnabled()) {
            return;
        }

        Date maximumActivationDate = new Date();
        Collection<ModelLoadRequest> modelLoadRequests = this.modelLoadRequestDao
                .findByExecutionStatusAndMaxActivationDate(ExecutionStatus.Pending, maximumActivationDate);

        if (modelLoadRequests.isEmpty()) {
            LOG.debug(IConfiguration.NAL, "No model-load requests found in status 'Pending' and activation date lower than '{}'.", maximumActivationDate);
            return;
        }

        for (final ModelLoadRequest mlr : modelLoadRequests) {
            long startTime = System.currentTimeMillis();
            LOG.info("Start processing model-load requests '{}'", mlr);
            try {
                audit(Start, mlr.getModelURI(), mlr.getExternalPid());
                modelLoadRequestProcessingService.process(mlr);
                mlr.setExecutionStatus(ExecutionStatus.Success);
                LOG.debug(IConfiguration.NAL, "Finished processing model-load requests '{}' in {} ms", mlr, (System.currentTimeMillis() - startTime));
                audit(End, mlr.getModelURI(), mlr.getExternalPid());
            } catch (Exception e) {
                mlr.setExecutionStatus(ExecutionStatus.Failure);
                LOG.error("Exception during the processing of the model-load request '{}'.", mlr.getModelURI(), e);
                audit(Fail, mlr.getModelURI(), mlr.getExternalPid());
            } finally {
                try {
                    mlr.setExecutionDate(new Date());
                    this.modelLoadRequestDao.updateObject(mlr);
                } catch (RuntimeException exception) {
                    LOG.warn("Cannot update model-load request identified by '{}': [Optimistic locking] the model-load " +
                            "request may have been removed/updated by an ingestion package.", mlr.getModelURI());
                }
            }
        }
        LOG.info("Done processing '{}' model-load request(s).", modelLoadRequests.size());
    }

    private static void audit(AuditTrailEventType type, String uri, String pid) {
        AuditBuilder.get(AuditTrailEventProcess.Nal)
                .withAction(AuditTrailEventAction.Decode)
                .withType(type)
                .withMessage("Model-load request '{}' ({})")
                .withMessageArgs(uri, pid)
                .withObject(uri)
                .withLogDatabase(true)
                .logEvent();
    }
}
