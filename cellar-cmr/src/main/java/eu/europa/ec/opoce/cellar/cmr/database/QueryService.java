package eu.europa.ec.opoce.cellar.cmr.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.util.Assert;

/**
 * <p>
 * Abstract QueryService class.
 * </p>
 */
public abstract class QueryService extends JdbcDaoSupport {

	/**
	 * <p>
	 * queryForLong.
	 * </p>
	 *
	 * @param query
	 *            a {@link java.lang.String} object.
	 * @return a {@link java.lang.Long} object.
	 */
	public Long queryForLong(String query) {
		List<Long> result = queryForLongList(query);

		Assert.isTrue(result.size() <= 1);
		return result.isEmpty() ? null : result.get(0);
	}

	/**
	 * <p>
	 * queryForLongList.
	 * </p>
	 *
	 * @param query
	 *            a {@link java.lang.String} object.
	 * @return a {@link java.util.List} object.
	 */
	public List<Long> queryForLongList(String query) {
		return getJdbcTemplate().query(query, new RowMapper<Long>() {

			@Override
			public Long mapRow(ResultSet rs, int rowNum) throws SQLException {
				return rs.getLong(1);
			}
		});
	}
}
