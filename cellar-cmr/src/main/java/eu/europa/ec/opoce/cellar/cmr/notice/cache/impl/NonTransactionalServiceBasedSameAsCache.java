/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice
 *             FILE : NoticeCache.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 29, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.notice.cache.impl;

import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;

/**
 * <class_description> Non transactional, read-only cache for sameAs-es.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 29, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class NonTransactionalServiceBasedSameAsCache extends AbstractSameAsCache {

    private static final IdentifierService nonTransactionalReadOnlyIdentifierService = ServiceLocator
            .getService("nonTransactionalReadOnlyPidManagerService", IdentifierService.class);

    /**
     * @see eu.europa.ec.opoce.cellar.cmr.notice.cache.impl.AbstractSameAsCache#resolveIdentifierService()
     */
    @Override
    protected IdentifierService resolveIdentifierService() {
        return nonTransactionalReadOnlyIdentifierService;
    }

}
