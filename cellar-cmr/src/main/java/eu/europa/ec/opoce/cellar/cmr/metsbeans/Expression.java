package eu.europa.ec.opoce.cellar.cmr.metsbeans;

import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * <p>Expression class.</p>
 */
public class Expression extends HierarchyElement<Work, Manifestation> {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 6241056120388360506L;

    /** The langs. */
    private Set<LanguageBean> langs = new HashSet<LanguageBean>();

    public Expression() {
        super(DigitalObjectType.EXPRESSION, Collections.emptyMap());
    }

    public Expression(Map<ContentType, String> versions) {
        super(DigitalObjectType.EXPRESSION, versions);
    }

    /**
     * Gets the langs.
     *
     * @return the langs
     */
    public Set<LanguageBean> getLangs() {
        return this.langs;
    }

    /**
     * Sets the langs.
     *
     * @param langs the new langs
     */
    public void setLangs(final Set<LanguageBean> langs) {
        this.langs = langs;
    }

    /**
     * Adds the lang.
     *
     * @param lang the lang
     */
    public void addLang(final LanguageBean lang) {
        this.langs.add(lang);
    }

}
