/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.cmr.database.dao.spring
 *        FILE : SpringCellarResourceDao.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 02-08-2012
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.database.dao.spring;

import eu.europa.ec.opoce.cellar.cmr.AlignerStatus;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.CellarResource.CleanerStatus;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.notice.xslt.function.EmbeddedNoticeRetrievalMethodType;
import eu.europa.ec.opoce.cellar.common.util.CellarIdUtils;
import eu.europa.ec.opoce.cellar.database.Database;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.helper.StringHelper;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;

import static eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType.AGENT;
import static eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType.DOSSIER;
import static eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType.EVENT;
import static eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType.EXPRESSION;
import static eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType.MANIFESTATION;
import static eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType.TOPLEVELEVENT;
import static eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType.WORK;

/**
 * <class_description> Data access object implementation for querying the
 * S3 APIs. <br/>
 * <br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc. <br/>
 * <br/>
 * ON : 04-02-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Repository
public class SpringCellarResourceDao extends CmrSpringBaseDao<CellarResource, Long> implements CellarResourceDao {

    private static final Logger LOG = LoggerFactory.getLogger(SpringCellarResourceDao.class);

    private static final String TABLE = "CMR_CELLAR_RESOURCE_MD";

    private static final String ID = "ID";
    private static final String CELLAR_ID = "CELLAR_ID";
    private static final String CELLAR_BASE_ID = "CELLAR_BASE_ID";
    private static final String CELLAR_TYPE = "CELLAR_TYPE";
    private static final String EMBARGO_DATE = "EMBARGO_DATE";
    private static final String MIME_TYPE = "MIME_TYPE";
    private static final String LANGUAGE = "LANGUAGE";
    private static final String LAST_MODIFICATION_DATE = "LAST_MODIFICATION_DATE";
    private static final String EMBEDDED_NOTICE_CREATION_DATE = "EMBEDDED_NOTICE_CREATION_DATE";
    private static final String EMBEDDED_NOTICE = "EMBEDDED_NOTICE";
    private static final String CLEANER_STATUS = "CLEANER_STATUS";
    private static final String ALIGNER_STATUS = "ALIGNER_STATUS";
    private static final String LAST_INDEXATION = "LAST_INDEXATION";

    private static final String VERSION_PREFIX = "VERSION_";

    private static final String VERSION_DIRECT = VERSION_PREFIX + "DIRECT";
    private static final String VERSION_DIRECT_INFERRED = VERSION_PREFIX + "DIRECT_INFERRED";
    private static final String VERSION_TECHNICAL = VERSION_PREFIX + "TECHNICAL";
    private static final String VERSION_SAME_AS = VERSION_PREFIX + "SAME_AS";
    private static final String VERSION_INVERSE = VERSION_PREFIX + "INVERSE";
    private static final String VERSION_EMBEDDED_NOTICE = VERSION_PREFIX + "EMBEDDED_NOTICE";
    private static final String VERSION_DECODING = VERSION_PREFIX + "DECODING";
    private static final String VERSION_FILE = VERSION_PREFIX + "FILE";

    private static final String getCellarId = StringHelper.format("{} = :cellarId ", CELLAR_ID);
    
    private static final String getCellarIds = StringHelper.format("{} IN (:cellarIds) ", CELLAR_ID);

    private static final String getLikeCellarId = StringHelper.format("{} like :cellarId ", CELLAR_ID);

    private static final String cellarTypeIsWork = StringHelper.format("{} = '" + WORK + "'", CELLAR_TYPE);

    private static final String cellarTypeIsWorkAndIndexedNotAfter = StringHelper.format(
            "{} = '" + WORK + "' AND ( {} < :lastIndexation OR {} IS NULL )", CELLAR_TYPE, LAST_INDEXATION,
            LAST_INDEXATION);

    private static final String cellarTypeIsWorkAndLastModificationDateAfter = StringHelper.format(
            "{} = '" + WORK + "' AND {} > :lastModificationDate AND ( {} < :lastIndexation OR {} IS NULL )", CELLAR_TYPE,
            LAST_MODIFICATION_DATE, LAST_INDEXATION, LAST_INDEXATION);

    private static final String getTotalNumberOfDossiers = StringHelper.format("{} = '" + DOSSIER + "'", CELLAR_TYPE);

    private static final String getTotalNumberOfEvents = StringHelper.format("{} = '" + EVENT + "'", CELLAR_TYPE);

    private static final String getTotalNumberOfAgents = StringHelper.format("{} = '" + AGENT + "'", CELLAR_TYPE);

    private static final String getTotalNumberOfTopLEvelEvents = StringHelper.format("{} = '" + TOPLEVELEVENT + "'", CELLAR_TYPE);

    private static final String getExpressionsPerLanguage = StringHelper.format(
            "select {}, count(1) as total from {} where {} = '" + EXPRESSION + "' group by {}", LANGUAGE, TABLE, CELLAR_TYPE,
            LANGUAGE);


    private static final String getClosestEmbargoTo= "select min(embargo_date) a from cmrowner.cmr_cellar_resource_md where embargo_date > :currentTimestamp";
    /**
     * The Constant getManifestationsPerType.
     */
    private static final String getManifestationsPerType = StringHelper.format(
            "select {}, count(1) as total from {} where {} = '" + MANIFESTATION + "' group by {}", MIME_TYPE, TABLE,
            CELLAR_TYPE, MIME_TYPE);

    /**
     * The Constant getManifestationsPerTypePerLanguage.
     */
    private static final String getManifestationsPerTypePerLanguage = StringHelper.format(
            "select a.{}, b.{}, count(1) from {} a, {} b where b.{} = a.{} AND( b.{} = a.{} OR substr(b.{},1,48) =a.{} )  and b.{} = '"
                    + MANIFESTATION + "' and a.{} is not null group by b.{}, a.{}",
            LANGUAGE, MIME_TYPE, TABLE, TABLE, CELLAR_BASE_ID, CELLAR_BASE_ID, CELLAR_ID,
            CELLAR_ID, CELLAR_ID, CELLAR_ID, CELLAR_TYPE, LANGUAGE, MIME_TYPE, LANGUAGE);

    /**
     * The Constant getWemHierarchyNoItems.
     */
    private static final String getWemHierarchyNoItems = StringHelper.format("{} = :cellarBaseId AND "
    		+ "("
    			+ "{} = '{}' "
    			+ "OR {} = :cellarExpressionId "
    			+ "OR ({} IN ('{}', '{}') AND {} LIKE :cellarId)"
    		+ ")",
    		CELLAR_BASE_ID,
    		CELLAR_TYPE, WORK,
    		CELLAR_ID,
    		CELLAR_TYPE, EXPRESSION, MANIFESTATION, CELLAR_ID);
    
    /**
     * The Constant getBase.
     */
    private static final String getBase = StringHelper.format("{} = :cellarBaseId", CELLAR_BASE_ID);

    /**
     * The Constant getBaseMetadata.
     */
    private static final String getBaseMetadata = StringHelper.format(
            "{} = :cellarBaseId AND {} IN ('{}', '{}', '{}', '{}', '{}', '{}', '{}')", CELLAR_BASE_ID, CELLAR_TYPE, WORK,
            EXPRESSION, MANIFESTATION, DOSSIER, EVENT, AGENT, TOPLEVELEVENT);

    /**
     * The Constant getTypeAndBase.
     */
    private static final String getTypeAndBase = StringHelper.format("{} = :type AND {} = :cellarBaseId", CELLAR_TYPE,
            CELLAR_BASE_ID);
    
    /**
     * The Constant getTypeAndBaseAndLikeLanguage.
     */
    private static final String getTypeAndBaseAndLikeLanguage = StringHelper.format("{} = :type AND {} = :cellarBaseId AND {} LIKE :language",
    		CELLAR_TYPE, CELLAR_BASE_ID, LANGUAGE);
    
    /**
     * The Constant getRootsPerCleanerStatus.
     */
    private static final String getRootsPerCleanerStatus = StringHelper.format(
            "{} IN ('{}', '{}', '{}', '{}') AND {} = :cleanerStatus AND rownum <= :batchSize", CELLAR_TYPE, WORK, DOSSIER, AGENT,
            TOPLEVELEVENT, CLEANER_STATUS);

    /**
     * The Constant getRootsPerAlignerStatus.
     */
    private static final String getHigherEmbargo="embargo_date>:current_timestamp";

    private static final String getRootsPerAlignerStatus = StringHelper.format(
            "{} IN ('{}', '{}', '{}', '{}') AND {} = :alignerStatus AND rownum <= :batchSize", CELLAR_TYPE, WORK, DOSSIER, AGENT,
            TOPLEVELEVENT, ALIGNER_STATUS);

    /**
     * The Constant getMetadataResources.
     */
    private static final String getMetadataResources = StringHelper.format("{} IN ('{}', '{}', '{}', '{}', '{}', '{}', '{}')",
            CELLAR_TYPE, WORK, EXPRESSION, MANIFESTATION, DOSSIER, EVENT, AGENT, TOPLEVELEVENT);

    /**
     * The Constant getMetadataResourcesDiagnosable.
     */
    private static final String getMetadataResourcesDiagnosable = StringHelper.format(
            "{} IN ('{}', '{}', '{}', '{}', '{}', '{}', '{}') AND {} IN ({}, {})", CELLAR_TYPE, WORK, EXPRESSION, MANIFESTATION,
            DOSSIER, EVENT, AGENT, TOPLEVELEVENT, CLEANER_STATUS, CleanerStatus.PROCESSING.ordinal(),
            CleanerStatus.UNKNOWN.ordinal());

    /**
     * The Constant getMetadataResourcesFixable.
     */
    private static final String getMetadataResourcesFixable = StringHelper.format(
            "{} IN ('{}', '{}', '{}', '{}', '{}', '{}', '{}') AND {} IN ({}, {}, {})", CELLAR_TYPE, WORK, EXPRESSION, MANIFESTATION,
            DOSSIER, EVENT, AGENT, TOPLEVELEVENT, CLEANER_STATUS, CleanerStatus.PROCESSING.ordinal(),
            CleanerStatus.UNKNOWN.ordinal(), CleanerStatus.CORRUPT.ordinal());

    /**
     * The Constant getMetadataResourcesError.
     */
    private static final String getMetadataResourcesError = StringHelper.format("{} = {}", CLEANER_STATUS,
            CleanerStatus.ERROR.ordinal());

    /**
     * The Constant getMetadataResourcesPerCleanerStatus.
     */
    private static final String getMetadataResourcesPerCleanerStatus = StringHelper.format(
            "{} IN ('{}', '{}', '{}', '{}', '{}', '{}', '{}') AND {} = :cleanerStatus AND rownum <= :batchSize", CELLAR_TYPE, WORK,
            EXPRESSION, MANIFESTATION, DOSSIER, EVENT, AGENT, TOPLEVELEVENT, CLEANER_STATUS);

    /**
     * The Constant getRoot.
     */
    private static final String getRoot = StringHelper.format( //
            "{} = '" + ContentIdentifier.CELLAR_PREFIX + ":' || ( " + //
                    "   select {} " + //
                    "   from " + TABLE + " " + //
                    "   where {} = :cellarId " + //
                    ")",
            CELLAR_ID, CELLAR_BASE_ID, CELLAR_ID);

    /**
     * The Constant getResourcesWithLowerEmbargoDate.
     */
    private static final String getResourcesWithLowerEmbargoDate = StringHelper.format(
            "{} like :cellarId  AND ( {} <= :embargoDate OR {} IS NULL)", CELLAR_ID, EMBARGO_DATE, EMBARGO_DATE);

    /**
     * The Constant getAlignerResourcesInError.
     */
    private static final String getAlignerResourcesInError = StringHelper.format(
            "{} IN ('{}', '{}', '{}', '{}','{}', '{}', '{}') AND {} = {}", CELLAR_TYPE, WORK, EXPRESSION, MANIFESTATION, DOSSIER,
            EVENT, AGENT, TOPLEVELEVENT, ALIGNER_STATUS, AlignerStatus.ERROR.ordinal());

    /**
     * The Constant getAlignerResourcesDiagnosables.
     */
    private static final String getAlignerResourcesDiagnosables = StringHelper.format(
            "{} IN ('{}', '{}', '{}', '{}', '{}', '{}', '{}') AND {} = {}", CELLAR_TYPE, WORK, EXPRESSION, MANIFESTATION, DOSSIER,
            EVENT, AGENT, TOPLEVELEVENT, ALIGNER_STATUS, AlignerStatus.UNKNOWN.ordinal());

    private static final String CELLAR_ID_AND_TYPE = "CELLAR_ID like :cellarId AND CELLAR_TYPE = :type";
    private static final String UPDATE_INDEXATION_DATE = "UPDATE CMR_CELLAR_RESOURCE_MD SET LAST_INDEXATION = :newDate WHERE CELLAR_ID = :cellarId";
    private static final String EXISTS = "SELECT id FROM CMR_CELLAR_RESOURCE_MD WHERE CELLAR_ID = :cellarId and ROWNUM = 1";
    private static final String DELETE_STARTS_WITH = "DELETE FROM CMR_CELLAR_RESOURCE_MD WHERE CELLAR_ID LIKE :cellarId";
    private static final String COUNT_MANIFESTATIONS = "SELECT COUNT(*) AS manifestation FROM CMR_CELLAR_RESOURCE_MD WHERE CELLAR_BASE_ID = :cellarBaseId AND CELLAR_TYPE = 'MANIFESTATION'";

    private static final Collection<String> ALL_FIELDS = Arrays.asList(CELLAR_ID, CELLAR_BASE_ID, CELLAR_TYPE, EMBARGO_DATE, MIME_TYPE,
            LANGUAGE, LAST_MODIFICATION_DATE, EMBEDDED_NOTICE_CREATION_DATE, EMBEDDED_NOTICE, CLEANER_STATUS, ALIGNER_STATUS, LAST_INDEXATION,
            VERSION_DIRECT, VERSION_DIRECT_INFERRED, VERSION_TECHNICAL, VERSION_SAME_AS, VERSION_INVERSE, VERSION_EMBEDDED_NOTICE, VERSION_FILE,
            VERSION_DECODING);

    private static final Collection<String> FIELDS_NO_CLOB = Arrays.asList(CELLAR_ID, CELLAR_BASE_ID, CELLAR_TYPE, EMBARGO_DATE, MIME_TYPE,
            LANGUAGE, LAST_MODIFICATION_DATE, EMBEDDED_NOTICE_CREATION_DATE, CLEANER_STATUS, ALIGNER_STATUS, LAST_INDEXATION,
            VERSION_DIRECT, VERSION_DIRECT_INFERRED, VERSION_TECHNICAL, VERSION_SAME_AS, VERSION_INVERSE, VERSION_EMBEDDED_NOTICE, VERSION_FILE,
            VERSION_DECODING);

    private static final String IS_UNDER_EMBARGO = "SELECT embargo_date FROM CMR_CELLAR_RESOURCE_MD WHERE CELLAR_ID = :cellarId";

    private final Database database;

    @Autowired
    public SpringCellarResourceDao(@Qualifier("databaseCellarDataCmr") Database database) {
        super(TABLE, FIELDS_NO_CLOB);
        this.database = database;
    }


    @Override
    @Cacheable(value = "findCellarId", unless = "#result == null")
    public CellarResource findCellarId(final String identifier) {
        checkIdentifier(identifier);
        return findUnique(getCellarId, Collections.singletonMap("cellarId", identifier));
    }
    
    @Override
    public Collection<CellarResource> findCellarIds(final Collection<String> identifiers) {
        return find(getCellarIds, Collections.singletonMap("cellarIds", identifiers));
    }

    @Override
    public CellarResource findResourceWithEmbeddedNotice(String identifier) {
        checkIdentifier(identifier);
        return findUnique(getCellarId, Collections.singletonMap("cellarId", identifier), ALL_FIELDS, new CellarResourceRowMapper(ALL_FIELDS));
    }

    @Override
    public boolean exists(String cellarId) {
        try {
            return getNamedParameterJdbcTemplate().query(EXISTS, Collections.singletonMap("cellarId", cellarId), rs -> {
                rs.next();
                return rs.getInt("id") > 0;
            });
        } catch (DataAccessException e) {
            LOG.error("Impossible to select " + cellarId, e);
            return false;
        }
    }

    @Override
    public boolean isUnderEmbargo(String cellarId) {
        try {
            final Timestamp embargoDate = getNamedParameterJdbcTemplate().query(
                    IS_UNDER_EMBARGO,
                    Collections.singletonMap("cellarId", cellarId), rs -> {
                        rs.next();
                        return rs.getTimestamp(EMBARGO_DATE);
                    }
            );

            return embargoDate != null && embargoDate.after(new Date());
        } catch (DataAccessException e) {
            LOG.error("Impossible to select " + cellarId, e);
            return false;
        }
    }

    @Override
    public Collection<CellarResource> findStartingWith(final String identifier) {
        checkIdentifier(identifier);

        final Collection<CellarResource> cellarResources = findWithBase(identifier);
        CollectionUtils.filter(cellarResources, o -> o.getCellarId().startsWith(identifier));
        return cellarResources;
    }

    @Override
    public Collection<CellarResource> findWithEmbeddedNoticeStartingWith(String identifier) {
        checkIdentifier(identifier);
        return find(getBase, Collections.singletonMap("cellarBaseId", getBaseId(identifier)), TABLE, ALL_FIELDS,
                new CellarResourceRowMapper(ALL_FIELDS)).stream()
                .filter(r -> r.getCellarId().startsWith(identifier))
                .collect(Collectors.toList());
    }





    @Override
    public Collection<CellarResource> findResourceStartWith(String cellarId, DigitalObjectType type) {
        final Map<String, Object> args = new HashMap<>(2);
        args.put("cellarId", cellarId + "%");
        args.put("type", type.toString());
        return find(CELLAR_ID_AND_TYPE, args);
    }

    @Override
    public List<CellarResource> findCellarIdStartingWith(final String identifier) {
        checkIdentifier(identifier);
        return new ArrayList<>(find(getLikeCellarId, Collections.singletonMap("cellarId", identifier + "%")));
    }

    @Override
    public Collection<CellarResource> findStartingWithSortByCellarId(final String identifier) {
        checkIdentifier(identifier);

        final Collection<CellarResource> cellarResources = findWithBaseSortByCellarId(identifier);
        CollectionUtils.filter(cellarResources, o -> o.getCellarId().startsWith(identifier));
        return cellarResources;
    }

    @Override
    public Collection<CellarResource> findHierarchy(final String identifier) {
        checkIdentifier(identifier);

        final Collection<CellarResource> cellarResources = findTree(identifier);
        CollectionUtils.filter(cellarResources, o -> o.getCellarId().startsWith(identifier) || identifier.startsWith(o.getCellarId()));
        return cellarResources;
    }

    @Override
    public Collection<CellarResource> findSpecificWemHierarchyWithEmbeddedNotice(String identifier) {
    	checkIdentifier(identifier);
    	
    	DigitalObjectType cellarType = DigitalObjectType.findType(identifier);
    	if (cellarType != EXPRESSION && cellarType != MANIFESTATION) {
    		throw ExceptionBuilder.get(CellarException.class).withMessage("The provided CellarID should be either an Expression or a Manifestation.").build();
    	}
    	
        String cellarExpressionId = (cellarType == EXPRESSION) ?
        		identifier : StringUtils.substring(identifier, 0, StringUtils.ordinalIndexOf(identifier, CellarIdUtils.WEM_SEPARATOR, 2));
        
        Map<String, Object> args = new HashMap<>();
        args.put("cellarId", addLikeSuffix(identifier));
        args.put("cellarBaseId", getBaseId(identifier));
        args.put("cellarExpressionId", cellarExpressionId);
       
        return find(getWemHierarchyNoItems, args, TABLE, ALL_FIELDS, new CellarResourceRowMapper(ALL_FIELDS)).stream()
        		.collect(Collectors.toList());
    }

    @Override
    public CellarResource findExpression(String identifier, String language) {
        checkIdentifier(identifier);

        final Collection<CellarResource> cellarResources = findWithTypeAndBase(DigitalObjectType.EXPRESSION, identifier);
        CollectionUtils.filter(cellarResources, o -> (o.getCellarId().startsWith(identifier) || identifier.startsWith(o.getCellarId()))
                && o.getLanguages().contains(language));
        if (cellarResources.size() > 1) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("Found more than one result for search of expressions with id {} and language {} )")
                    .withMessageArgs(identifier, language).build();
        }
        return cellarResources.isEmpty() ? null : cellarResources.iterator().next();
    }

    @Override
    public List<CellarResource> findExpressions(final String identifier, final String language) {
        checkIdentifier(identifier);

        final ArrayList<CellarResource> cellarResources = new ArrayList<>(findWithTypeAndBase(DigitalObjectType.EXPRESSION, identifier));
        CollectionUtils.filter(cellarResources, o -> (o.getCellarId().startsWith(identifier) || identifier.startsWith(o.getCellarId()))
                && o.getLanguages().contains(language));
        return cellarResources;
    }

    @Override
    public Collection<CellarResource> findExpressions(final String identifier) {
        checkIdentifier(identifier);

        final Collection<CellarResource> cellarResources = findWithTypeAndBase(DigitalObjectType.EXPRESSION, identifier);
        CollectionUtils.filter(cellarResources, o -> o.getCellarId().startsWith(identifier) || identifier.startsWith(o.getCellarId()));
        return cellarResources;
    }

    @Override
    public Collection<CellarResource> findResourcesWithTypeAndLanguage(String identifier, DigitalObjectType type, String language) {
        checkIdentifier(identifier);
        Map<String, Object> args = new HashMap<>();
        args.put("type", type.toString());
        args.put("cellarBaseId", getBaseId(identifier));
        args.put("language", addLikePrefixAndSuffix(language));
        
        return find(getTypeAndBaseAndLikeLanguage, args);
    }
    
    @Override
    public Collection<CellarResource> findManifestations(final String identifier) {
        checkIdentifier(identifier);

        final Collection<CellarResource> cellarResources = findWithTypeAndBase(DigitalObjectType.MANIFESTATION, identifier);
        CollectionUtils.filter(cellarResources, o -> o.getCellarId().startsWith(identifier) || identifier.startsWith(o.getCellarId()));
        return cellarResources;
    }

    @Override
    public Collection<CellarResource> findTree(final String identifier) {
        checkIdentifier(identifier);
        return findWithBase(identifier);
    }

    @Override
    public List<CellarResource> findTreeSortByCellarId(final String identifier) {
        checkIdentifier(identifier);
        return findWithBaseSortByCellarId(identifier);
    }

    @Override
    public List<CellarResource> findTreeMetadataSortByCellarId(final String identifier) {
        checkIdentifier(identifier);
        return findWithBaseMetadataSortByCellarId(identifier);
    }
    
    @Override
    @Cacheable(value = "embeddedNoticeCache", unless = "#result == null")
    public Collection<CellarResource> retrieveWithEmbeddedNoticeUsingCache(EmbeddedNoticeRetrievalMethodType methodType, String identifier) {
    	switch (methodType) {
	    	case SINGLE_RESOURCE:
	    		return Collections.singletonList(findResourceWithEmbeddedNotice(identifier));
	    	case SPECIFIC_HIERARCHY:
	    		return findSpecificWemHierarchyWithEmbeddedNotice(identifier);
	    	case STARTING_WITH:
	    		return findWithEmbeddedNoticeStartingWith(identifier);
    		default:
    			return Collections.emptyList();
    	}
    }
    
    @Override
    public long getNumberOfMetadataResources() {
        return count(getMetadataResources, new HashMap<>());
    }

    @Override
    public long getNumberOfMetadataResourcesDiagnosable() {
        return count(getMetadataResourcesDiagnosable, new HashMap<>());
    }

    @Override
    public long getNumberOfMetadataResourcesFixable() {
        return count(getMetadataResourcesFixable, new HashMap<>());
    }

    @Override
    public long getNumberOfMetadataResourcesError() {
        return count(getMetadataResourcesError, new HashMap<>());
    }

    @Override
    public long getTotalNumberOfWorks() {
        return count(cellarTypeIsWork, new HashMap<>());
    }

    @Override
    public Collection<CellarResource> getWorks(final long limit, final Date lastIndexation) {
        Map<String, String> args = new HashMap<>();
        args.put(LAST_MODIFICATION_DATE, "DESC");
        args.put(LAST_INDEXATION, "ASC");
        return new ArrayList<>(findSortBy(cellarTypeIsWorkAndIndexedNotAfter, Collections.singletonMap("lastIndexation", lastIndexation), args, limit));
    }

    @Override
    public Collection<CellarResource> getLastWorks(final long limit, final Date lastModificationDate, final Date lastIndexation) {
        Map<String, Object> args = new HashMap<>();
        args.put("lastModificationDate", lastModificationDate);
        args.put("lastIndexation", lastIndexation);

        Map<String, String> args2 = new HashMap<>();
        args2.put(LAST_MODIFICATION_DATE, "DESC");
        args2.put(LAST_INDEXATION, "ASC");

        return new ArrayList<>(findSortBy(cellarTypeIsWorkAndLastModificationDateAfter, args, args2, limit));
    }

    @Override
    public long getTotalNumberOfDossiers() {
        return count(getTotalNumberOfDossiers, new HashMap<>());
    }

    @Override
    public long getTotalNumberOfEvents() {
        return count(getTotalNumberOfEvents, new HashMap<>());
    }

    @Override
    public long getTotalNumberOfAgents() {
        return count(getTotalNumberOfAgents, new HashMap<>());
    }

    @Override
    public long getTotalNumberOfTopLevelEvents() {
        return count(getTotalNumberOfTopLEvelEvents, new HashMap<>());
    }

    @Override
    public Collection<CellarResource> findMetadataResourcesPerCleanerStatus(final CleanerStatus cleanerStatus, final int batchSize) {
        Map<String, Object> args = new HashMap<>();
        args.put("cleanerStatus", cleanerStatus.ordinal());
        args.put("batchSize", batchSize);
        return find(getMetadataResourcesPerCleanerStatus, args);
    }

    @Override
    public Collection<CellarResource> findRootsPerCleanerStatus(final CleanerStatus cleanerStatus, final int batchSize) {
        Map<String, Object> args = new HashMap<>();
        args.put("cleanerStatus", cleanerStatus.ordinal());
        args.put("batchSize", batchSize);
        return find(getRootsPerCleanerStatus, args);
    }

    @Override
    public Collection<CellarResource> findRootsPerAlignerStatus(final AlignerStatus alignerStatus, final int batchSize) {
        Map<String, Object> args = new HashMap<>();
        args.put("alignerStatus", alignerStatus.ordinal());
        args.put("batchSize", batchSize);
        return find(getRootsPerAlignerStatus, args);
    }

    @Override
    public Map<String, Integer> getExpressionsPerLanguage() {
        final ResultSetExtractor<Map<String, Integer>> extractor = rs -> {
            final Map<String, Integer> results = new HashMap<>();
            while (rs.next()) {
                final String language = rs.getString(1);
                final Integer total = rs.getInt(2);
                results.put(language, total);
            }
            return results;
        };
        return database.getJdbcOperations().query(getExpressionsPerLanguage, extractor);
    }

    @Override
    public Map<String, Integer> getManifestationsPerType() {
        final ResultSetExtractor<Map<String, Integer>> extractor = rs -> {
            final Map<String, Integer> results = new HashMap<>();
            while (rs.next()) {
                String type = rs.getString(1);
                if (type == null) {
                    type = "No type";
                }
                final Integer total = rs.getInt(2);
                results.put(type, total);
            }
            return results;
        };
        return database.getJdbcOperations().query(getManifestationsPerType, extractor);
    }

    @Override
    public Map<String, Map<String, Integer>> getManifestationsPerTypePerLanguage() {
        final ResultSetExtractor<Map<String, Map<String, Integer>>> extractor = rs -> {
            final Map<String, Map<String, Integer>> results = new HashMap<>();
            while (rs.next()) {
                final String language = rs.getString(1);
                String type = rs.getString(2);
                if (type == null) {
                    type = "No type";
                }
                final Integer count = rs.getInt(3);
                if (results.get(type) == null) {
                    final Map<String, Integer> languages = new HashMap<>();
                    languages.put(language, count);
                    results.put(type, languages);
                } else {
                    results.get(type).put(language, count);
                }
            }
            return results;
        };
        return database.getJdbcOperations().query(getManifestationsPerTypePerLanguage, extractor);
    }

    /**
     * Gets the root.
     *
     * @param cellarId the cellar id
     * @return the root
     * @see eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao#getRoot(java.lang.String)
     */
    @Override
    public CellarResource getRoot(final String cellarId) {
        return findUnique(getRoot, Collections.singletonMap("cellarId", cellarId));
    }

    @Override
    public CellarResource create(final ResultSet rs) throws SQLException {
        return new CellarResourceRowMapper(columnNames).mapRow(rs, 0);
    }

    @Override
    public void fillMap(final CellarResource cellarResource, final Map<String, Object> map) {
        map.put(CELLAR_ID, cellarResource.getCellarId());
        map.put(CELLAR_TYPE, cellarResource.getCellarType().toString());
        map.put(EMBARGO_DATE, cellarResource.getEmbargoDate());
        map.put(MIME_TYPE, cellarResource.getMimeType());
        map.put(LANGUAGE, StringUtils.join(cellarResource.getLanguages(), " "));
        map.put(LAST_MODIFICATION_DATE, cellarResource.getLastModificationDate());
        map.put(EMBEDDED_NOTICE_CREATION_DATE, cellarResource.getEmbeddedNoticeCreationDate());
        if(cellarResource.getEmbeddedNotice() != null) {
            // Hack. We're always keep the embedded notice. S3 migration
            // modify the version of the metadata (columns VERSION_*) and
            // we don't retrieve the EMBEDDED_NOTICE at each request to
            // avoid to kill performances by reading CLOBs in Oracle when
            // it's not necessary. So in any case, we'll only perform update
            // of EMBEDDED_NOTICE when the value has been changed.
            map.put(EMBEDDED_NOTICE, cellarResource.getEmbeddedNotice());
        }
        map.put(CELLAR_BASE_ID, getBaseId(cellarResource.getCellarId()));
        map.put(CLEANER_STATUS,
                cellarResource.getCleanerStatus() == null ? CleanerStatus.UNKNOWN.ordinal() : cellarResource.getCleanerStatus().ordinal());

        int alignerStatus = AlignerStatus.UNKNOWN.ordinal();
        if (cellarResource.getAlignerStatus() != null) {
            alignerStatus = cellarResource.getAlignerStatus().ordinal();
        }
        map.put(ALIGNER_STATUS, alignerStatus);
        map.put(LAST_INDEXATION, cellarResource.getLastIndexation());

        cellarResource.getVersions().forEach((k, v) -> map.put(VERSION_PREFIX + k.name(), v));
    }

    /**
     * <p>
     * findWithTypeAndBase.
     * </p>
     *
     * @param objectType a
     *                   {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType}
     *                   object.
     * @param identifier a {@link java.lang.String} object.
     * @return a {@link java.util.Collection} object.
     */
    private Collection<CellarResource> findWithTypeAndBase(final DigitalObjectType objectType, final String identifier) {
        checkIdentifier(identifier);

        Map<String, Object> args = new HashMap<>();
        args.put("type", objectType.toString());
        args.put("cellarBaseId", getBaseId(identifier));
        return new ArrayList<>(find(getTypeAndBase, args));
    }

    /**
     * <p>
     * findWithBase.
     * </p>
     *
     * @param identifier a {@link java.lang.String} object.
     * @return a {@link java.util.Collection} object.
     */
    private Collection<CellarResource> findWithBase(final String identifier) {
        checkIdentifier(identifier);
        return new ArrayList<>(find(getBase, Collections.singletonMap("cellarBaseId", getBaseId(identifier))));
    }

    /**
     * Find with base sort by cellar id.
     *
     * @param identifier the identifier
     * @return the list
     */
    private List<CellarResource> findWithBaseSortByCellarId(final String identifier) {
        checkIdentifier(identifier);
        return new ArrayList<>(findSortBy(getBase, Collections.singletonMap("cellarBaseId", getBaseId(identifier)), CELLAR_ID));
    }

    /**
     * Find with base metadata sort by cellar id.
     *
     * @param identifier the identifier
     * @return the list
     */
    private List<CellarResource> findWithBaseMetadataSortByCellarId(final String identifier) {
        checkIdentifier(identifier);
        return new ArrayList<>(findSortBy(getBaseMetadata, Collections.singletonMap("cellarBaseId", getBaseId(identifier)), CELLAR_ID));
    }

    /**
     * <p>
     * getBaseId.
     * </p>
     *
     * @param cellarId a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    private static String getBaseId(final String cellarId) {
        return cellarId.indexOf('.') == -1 ? cellarId.substring(cellarId.indexOf(':') + 1)
                : cellarId.substring(cellarId.indexOf(':') + 1, cellarId.indexOf('.'));
    }

    /**
     * Appends the '%' wildcard character in the end of the provided string.
     * @param string the string to prepend/append to.
     * @return the input string with the wildcard character appended.
     */
    private static String addLikeSuffix(final String input) {
    	return input + "%";
    }
    
    /**
     * Appends the '%' wildcard character in the beginning end of the provided string.
     * @param string the string to append to.
     * @return the input string with the wildcard character prepended and appended.
     */
    private static String addLikePrefixAndSuffix(final String input) {
    	return "%" + input + "%";
    }
    
    @Override
    public long getAlignerResourcesInErrorCount() {
        return count(getAlignerResourcesInError, new HashMap<>());
    }

    @Override
    public long getAlignerResourcesDiagnosableCount() {
        return count(getAlignerResourcesDiagnosables, new HashMap<>());
    }

    @Override
    @CacheEvict(value = "findCellarId", allEntries = true)
    public CellarResource saveOrUpdateObject(final CellarResource cellarResource) {
        return super.saveOrUpdateObject(cellarResource);
    }

    @Override
    @CacheEvict(value = "findCellarId", allEntries = true)
    public CellarResource saveObject(final CellarResource cellarResource) {
        return super.saveObject(cellarResource);
    }

    @Override
    @CacheEvict(value = "findCellarId", allEntries = true)
    public CellarResource updateObject(final CellarResource cellarResource) {
        return super.updateObject(cellarResource);
    }

    @Override
    @CacheEvict(value = "findCellarId", allEntries = true)
    public void deleteObject(final CellarResource cellarResource) {
        super.deleteObject(cellarResource);
    }

    @Override
    public List<CellarResource> getAllResourcesWithLowerEmbargoDate(final String identifier, final Date embargoDate) {
        Map<String, Object> args = new HashMap<>();
        args.put("cellarId", identifier + "%");
        args.put("embargoDate", embargoDate);
        return new ArrayList<>(find(getResourcesWithLowerEmbargoDate, args));
    }

    @Override
    public int updateIndexationDate(String cellarId, Date date) {
        final MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("cellarId", cellarId);
        params.addValue("newDate", date);
        try {
            return getNamedParameterJdbcTemplate().update(UPDATE_INDEXATION_DATE, params);
        } catch (DataAccessException e) {
            LOG.error("Impossible to update " + cellarId + " with indexation date " + date, e);
            return -1;
        }
    }

    @Override
    public int deleteResourceStartsWith(String baseCellarId) {
        final MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("cellarId", baseCellarId + "%");
        try {
            return getNamedParameterJdbcTemplate().update(DELETE_STARTS_WITH, params);
        } catch (DataAccessException e) {
            LOG.error("Impossible to delete the objects which start with" + baseCellarId, e);
            return -1;
        }
    }



    @Override
    public Date selectMinEmbargoDate(Date currentTimestamp){
        final MapSqlParameterSource params=new MapSqlParameterSource();
        params.addValue("currentTimestamp",currentTimestamp);
        try{
            return getNamedParameterJdbcTemplate().
                    query(getClosestEmbargoTo,params
                    ,resultSet -> {
                        boolean hasNext=resultSet.next();
                        return hasNext?resultSet.getTimestamp("a"):null;
                            });

        }
        catch(DataAccessException e){
            LOG.error("Error while executing select query for next closest embargo date",e);
            return DateTime.now().plusSeconds(20).toDate();
        }
    }

    @Override
    public int countManifestations(String baseCellarId) {
        final MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("cellarBaseId", baseCellarId);
        try {
            return getNamedParameterJdbcTemplate().query(COUNT_MANIFESTATIONS, params, resultSet -> {
                boolean hasNext = resultSet.next();
                return hasNext ? resultSet.getInt("manifestation") : 0;
            });
        } catch (DataAccessException e) {
            LOG.error("Impossible to count the manifestations for" + baseCellarId, e);
            return -1;
        }
    }

    private static void checkIdentifier(String identifier) {
        if (StringUtils.isBlank(identifier)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument status cannot be null").build();
        }
    }

    static final class CellarResourceRowMapper implements RowMapper<CellarResource> {

        private final Set<String> columnNames;

        CellarResourceRowMapper(Collection<String> columnNames) {
            this.columnNames = new HashSet<>(columnNames);
        }

        @Override
        public CellarResource mapRow(ResultSet rs, int rowNum) throws SQLException {
            final CellarResource resource = new CellarResource();
            resource.setId(rs.getLong(ID));
            resource.setCellarId(rs.getString(CELLAR_ID));
            resource.setCellarType(DigitalObjectType.getByValue(rs.getString(CELLAR_TYPE)));
            resource.setEmbargoDate(rs.getTimestamp(EMBARGO_DATE));
            resource.setMimeType(rs.getString(MIME_TYPE));
            resource.setLanguages(Arrays.asList(StringUtils.split(StringUtils.trimToEmpty(rs.getString(LANGUAGE)), " ")));
            resource.setLastModificationDate(rs.getTimestamp(LAST_MODIFICATION_DATE));
            resource.setEmbeddedNoticeCreationDate(rs.getTimestamp(EMBEDDED_NOTICE_CREATION_DATE));
            if (columnNames.contains(EMBEDDED_NOTICE)) {
                resource.setEmbeddedNotice(rs.getString(EMBEDDED_NOTICE));
            }
            resource.setCleanerStatus(CleanerStatus.getStatus(rs.getByte(CLEANER_STATUS)));
            resource.setAlignerStatus(AlignerStatus.getCellarResourceAlignerStatus(rs.getByte(ALIGNER_STATUS)));
            resource.setLastIndexation(rs.getTimestamp(LAST_INDEXATION));

            Map<ContentType, String> versions = new EnumMap<>(ContentType.class);
            for (ContentType contentType : ContentType.values()) {
                String version = rs.getString(VERSION_PREFIX + contentType.name());
                if (version != null) {
                    versions.put(contentType, version);
                }
            }
            resource.setVersions(versions);

            return resource;
        }
    }
}
