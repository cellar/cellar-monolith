/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.cmr.ingest.impl.create
 *        FILE : CreateModelLoader.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 21-05-2013
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.ingest.impl.create;

import eu.europa.ec.opoce.cellar.cmr.ingest.impl.AbstractModelLoader;

/**
 * <class_description> Class for conveniently load models for creation ingests.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 22-05-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class CreateModelLoader extends AbstractModelLoader {
    public CreateModelLoader() {
        super();
    }
}
