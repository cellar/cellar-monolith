/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cmr.model.deleteHandler
 *        FILE : IModelDeleteHandler.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 14-04-2014
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.model.deleteHandler.impl;

import eu.europa.ec.opoce.cellar.cmr.model.ModelUtils;

import org.springframework.stereotype.Service;

import org.apache.jena.rdf.model.Model;

/**
 * <class_description> Class for deleting from a model the triples contained in the given string representing an RDF model.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 14-04-2014
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
@Service("rdfModelDeleteHandler")
public class RDFModelDeleteHandler extends AbstractModelDeleteHandler<String> {

    /**
     * This method removes from the given {@link model} the triples included in the model represented by the given {@link modelString}.
     * 
     * @param inputModel the model from which to delete data
     * @param modelString the string representing the RDF model to delete
     * @param preserveInputModel if <code>true</code>, the {@link inputModel} is kept unmodified, and only the number of deleted entries will be returned
     * @return the number of triples deleted
     */
    @Override
    public long delete(final Model inputModel, final String modelString, final boolean preserveInputModel) {
        long deleted = 0;

        if (modelString != null) {
            final Model myInputModel = preserveInputModel ? ModelUtils.create(inputModel) : inputModel;
            Model deleteModel = null;
            final Model modelBackup = ModelUtils.create(myInputModel);
            try {
                deleteModel = ModelUtils.deserialize(modelString, null);
                //first we handle the blank nodes
                this.handleBlankNodes(myInputModel, deleteModel);
                //then we execute the deletion on the model
                myInputModel.remove(deleteModel);
                deleted = ModelUtils.statementsDifference(modelBackup, myInputModel);
            } finally {
                ModelUtils.closeQuietly(modelBackup, deleteModel);
                if (preserveInputModel) {
                    ModelUtils.closeQuietly(myInputModel);
                }
            }
        }
        return deleted;
    }

}
