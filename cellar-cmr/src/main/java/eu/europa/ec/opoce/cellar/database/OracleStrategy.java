package eu.europa.ec.opoce.cellar.database;

import java.text.SimpleDateFormat;

/**
 * This class implements the {@DatabaseStrategy} interface for a Oracle database.
 *
 * @see DatabaseStrategy
 */
public class OracleStrategy extends DatabaseStrategy {

    // TODO CORRECT DATE FORMAT
    private SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");

    /**
     * <p>Constructor for OracleStrategy.</p>
     */
    public OracleStrategy() {
    }

    /** {@inheritDoc} */
    @Override
    public String format(Object value) {
        if (value == null) {
            return null;
        }

        Class<? extends Object> clazz = value.getClass();
        if (Character.class.equals(clazz) || char.class.equals(clazz)) {
            value = value.toString();
        }

        if (value instanceof String) {
            StringBuffer stringVal = new StringBuffer((String) value);
            for (int i = 0; i < stringVal.length(); i++) {
                if (stringVal.charAt(i) == '\'') {
                    stringVal.insert(i, '\'');
                    i++;
                }
            }
            return "\'" + stringVal + "\'";
        }

        if (value instanceof java.util.Date) {
            return dateFormat.format(value);
        }
        return value.toString();
    }
}
