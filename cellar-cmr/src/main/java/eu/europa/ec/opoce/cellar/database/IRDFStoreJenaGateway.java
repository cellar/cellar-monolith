/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.database
 *             FILE : IRDFStoreJenaGateway.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Aug 8, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.database;

import eu.europa.ec.opoce.cellar.jena.oracle.result.resolver.IJenaResultSetResolver;

import org.apache.jena.query.Query;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Aug 8, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IRDFStoreJenaGateway {

    <T> T executeSelectQuery(final Query query, final CmrTableName table, final IJenaResultSetResolver<T> resolver);
}
