package eu.europa.ec.opoce.cellar.server.controller;

import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

//@Controller

/**
 * <p>IndexingAdminController class.</p>
 */
public class IndexingAdminController {

    private static final Logger LOG = LogManager.getLogger(IndexingAdminController.class);

    @Autowired
    private IIndexingService indexRequestService;

    /**
     * <p>enable.</p>
     *
     * @param httpServletResponse a {@link javax.servlet.http.HttpServletResponse} object.
     * @throws java.io.IOException if any.
     */
    @RequestMapping(value = "/indexing/enable", method = RequestMethod.GET)
    public void enable(final HttpServletResponse httpServletResponse) throws IOException {
        this.indexRequestService.setIndexingEnabled(true);
        this.sendHtmlResponse(httpServletResponse, "indexing is enabled");
    }

    /**
     * <p>disable.</p>
     *
     * @param httpServletResponse a {@link javax.servlet.http.HttpServletResponse} object.
     * @throws java.io.IOException if any.
     */
    @RequestMapping(value = "/indexing/disable", method = RequestMethod.GET)
    public void disable(final HttpServletResponse httpServletResponse) throws IOException {
        this.indexRequestService.setIndexingEnabled(false);
        this.sendHtmlResponse(httpServletResponse, "indexing is disabled");
    }

    /**
     * <p>line.</p>
     *
     * @param httpServletResponse a {@link javax.servlet.http.HttpServletResponse} object.
     * @throws java.io.IOException if any.
     */
    @RequestMapping(value = "/indexing/line", method = RequestMethod.GET)
    public void line(final HttpServletResponse httpServletResponse) throws IOException {
        LOG.warn(
                "=======================================================================================================================================");
        this.sendHtmlResponse(httpServletResponse, "line is set in log");
    }

    /**
     * <p>sendHtmlResponse.</p>
     *
     * @param httpServletResponse a {@link javax.servlet.http.HttpServletResponse} object.
     * @param html                a {@link java.lang.String} object.
     * @throws java.io.IOException if any.
     */
    private void sendHtmlResponse(final HttpServletResponse httpServletResponse, final String html) throws IOException {
        httpServletResponse.setContentType("text/html");
        final ServletOutputStream outputStream = httpServletResponse.getOutputStream();
        outputStream.write(html.getBytes(StandardCharsets.UTF_8));
        outputStream.flush();
    }
}
