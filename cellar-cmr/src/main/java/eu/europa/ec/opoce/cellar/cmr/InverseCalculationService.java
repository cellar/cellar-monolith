package eu.europa.ec.opoce.cellar.cmr;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;
import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.ontology.service.OntologyService;
import eu.europa.ec.opoce.cellar.semantic.SparqlUpdateService;
import eu.europa.ec.opoce.cellar.semantic.inferredmodel.InferredModelConfiguration;
import eu.europa.ec.opoce.cellar.semantic.jena.StatementIterable;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarType;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static eu.europa.ec.opoce.cellar.semantic.jena.transformers.Statement2ObjectResource.statement2ObjectResource;

@Service
public class InverseCalculationService {

    /**
     * Constant <code>log</code>
     */
    private static final Logger LOG = LogManager.getLogger(InverseCalculationService.class);

    @Autowired(required = true)
    private OntologyService ontologyService;

    @Autowired(required = true)
    private SparqlUpdateService sparqlUpdateService;

    @Autowired(required = true)
    private InferredModelConfiguration configuration;

    /*
     * TODO: Check this very bad handling of shared resource
     */
    private final Map<Property, Property> inverses = new HashMap<Property, Property>();

    /**
     * calculate the inverse model from an input model
     *
     * @param inputModel original model
     * @return inverse model
     */
    public Model getInverseModel(final Model inputModel) {
        final Model inverseRelationsModel = this.getInverseRelations(inputModel);

        for (final Statement axiomResourceStatement : StatementIterable.list(inputModel, null, RDF.type, CellarType.owl_axiomR)) {
            final Resource axiom = axiomResourceStatement.getSubject();

            final RDFNode targetNode = this.getAnnotatedTargetAsNode(axiom);
            if (targetNode == null) {
                continue;
            }

            final Resource annotatedSource = (Resource) axiom.listProperties(CellarProperty.owl_annotatedSourceP).next().getObject();
            final Property annotatedProperty = axiom.listProperties(CellarProperty.owl_annotatedPropertyP).next().getObject()
                    .as(Property.class);
            final Resource annotatedTarget = (Resource) targetNode;

            final boolean tripleExists = this.doesTripleExist(inputModel, annotatedSource, annotatedProperty, annotatedTarget);
            if (!tripleExists) {
                LOG.warn("Axiom found without triple [{} {} {}].", annotatedSource, annotatedProperty, annotatedTarget);
                continue;
            }

            final Resource newAxiom = inputModel.createResource();
            this.createInverseAxiom(inverseRelationsModel, newAxiom, annotatedSource, annotatedProperty, annotatedTarget);
            this.copyStatementsToNewAxiom(inputModel, axiom, inverseRelationsModel, newAxiom);
        }

        return inverseRelationsModel;
    }

    /**
     * <p>doesTripleExist.</p>
     *
     * @param model    a {@link org.apache.jena.rdf.model.Model} object.
     * @param subject  a {@link org.apache.jena.rdf.model.Resource} object.
     * @param property a {@link org.apache.jena.rdf.model.Property} object.
     * @param object   a {@link org.apache.jena.rdf.model.Resource} object.
     * @return a boolean.
     */
    private boolean doesTripleExist(final Model model, final Resource subject, final Property property, final Resource object) {
        final Set<Resource> sameAsResources = this.getSameAsResources(model, subject);

        for (final Resource resource : sameAsResources) {
            final boolean exists = model.contains(resource, property, object);
            if (exists) {
                return true;
            }
        }

        return false;
    }

    /**
     * <p>getSameAsResources.</p>
     *
     * @param model    a {@link org.apache.jena.rdf.model.Model} object.
     * @param resource a {@link org.apache.jena.rdf.model.Resource} object.
     * @return a {@link java.util.Set} object.
     */
    private Set<Resource> getSameAsResources(final Model model, final Resource resource) {
        //check if resource is value of an owl:sameAs -> if so, we need to take the subject which is the cellarResource, else it already is the cellarResource
        final StmtIterator stmtIterator = model.listStatements(null, OWL.sameAs, resource);
        try {
            final Resource cellarResource = stmtIterator.hasNext() ? stmtIterator.next().getSubject() : resource;
            //all owl:sameAs statements have cellarResource as the subject...
            final List<Statement> sameAsStatements = model.listStatements(cellarResource, OWL.sameAs, (RDFNode) null).toList();
            final Set<Resource> sameAsResources = CollectionUtils.collect(sameAsStatements, statement2ObjectResource, new HashSet<Resource>());
            sameAsResources.add(resource);
            return sameAsResources;
        } finally {
            stmtIterator.close();
        }
    }

    /**
     * <p>getInverseRelations.</p>
     *
     * @param inputModel a {@link org.apache.jena.rdf.model.Model} object.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    private Model getInverseRelations(final Model inputModel) {
        final Model inferences = ModelFactory.createDefaultModel();
        sparqlUpdateService.runUpdate(inputModel, inferences, configuration.getInsertInverse(), false,
                ontologyService.getWrapper().getOntology().getInferredOntology());
        return inferences;
    }

    /**
     * <p>getAnnotatedTargetAsNode.</p>
     *
     * @param axiom a {@link org.apache.jena.rdf.model.Resource} object.
     * @return a {@link org.apache.jena.rdf.model.RDFNode} object.
     */
    private RDFNode getAnnotatedTargetAsNode(final Resource axiom) {
        final StmtIterator targetIterator = axiom.listProperties(CellarProperty.owl_annotatedTargetP);
        try {
            final RDFNode targetNode = targetIterator.hasNext() ? targetIterator.next().getObject() : null;
            if (targetNode == null) {
                final ExceptionBuilder<CellarException> exceptionBuilder = ExceptionBuilder.get(CellarException.class);
                exceptionBuilder.withCode(CmrErrors.MODEL_LOAD_SERVICE_ERROR);
                exceptionBuilder.withMessage("There are axioms/blank nodes that do not have the owl:annotatdTarget statement as expected.");
                throw exceptionBuilder.build();
            }
            return !targetNode.isResource() ? null : targetNode;
        } finally {
            targetIterator.close();
        }
    }

    /**
     * <p>createInverseAxiom.</p>
     *
     * @param inferences        a {@link org.apache.jena.rdf.model.Model} object.
     * @param newAxiom          a {@link org.apache.jena.rdf.model.Resource} object.
     * @param annotatedSource   a {@link org.apache.jena.rdf.model.Resource} object.
     * @param annotatedProperty a {@link org.apache.jena.rdf.model.Property} object.
     * @param annotatedTarget   a {@link org.apache.jena.rdf.model.Resource} object.
     */
    private void createInverseAxiom(final Model inferences, final Resource newAxiom, final Resource annotatedSource,
                                    final Property annotatedProperty, final Resource annotatedTarget) {
        final Property inverseProperty = this.getInverseProperty(annotatedProperty);
        if (inverseProperty == null) {
            return;
        }

        inferences.add(this.createStatement(newAxiom, RDF.type, CellarType.owl_axiomR));
        inferences.add(this.createStatement(newAxiom, CellarProperty.owl_annotatedSourceP, annotatedTarget));
        inferences.add(this.createStatement(newAxiom, CellarProperty.owl_annotatedPropertyP, inverseProperty));
        inferences.add(this.createStatement(newAxiom, CellarProperty.owl_annotatedTargetP, annotatedSource));
    }

    /**
     * <p>getInverseProperty.</p>
     *
     * @param property a {@link org.apache.jena.rdf.model.Property} object.
     * @return a {@link org.apache.jena.rdf.model.Property} object.
     */
    private Property getInverseProperty(final Property property) {
        Property inverse = this.inverses.get(property);
        if (inverse != null) {
            return inverse;
        }

        final Model ontologyModel = ontologyService.getWrapper().getOntology().getInferredOntology();

        final Set<Property> result = new HashSet<Property>();
        for (final Statement statement : ontologyModel.listStatements(null, OWL.inverseOf, property).toList()) {
            result.add(statement.getSubject().as(Property.class));
        }
        for (final Statement statement : ontologyModel.listStatements(property, OWL.inverseOf, (RDFNode) null).toList()) {
            result.add(statement.getObject().as(Property.class));
        }

        Assert.isTrue(result.size() <= 1);
        inverse = result.size() == 0 ? null : result.iterator().next();

        if (inverse != null) {
            this.inverses.put(property, inverse);
            this.inverses.put(inverse, property);
        }

        return inverse;
    }

    /**
     * <p>copyStatementsToNewAxiom.</p>
     *
     * @param inputModel a {@link org.apache.jena.rdf.model.Model} object.
     * @param inputAxiom a {@link org.apache.jena.rdf.model.Resource} object.
     * @param newModel   a {@link org.apache.jena.rdf.model.Model} object.
     * @param newAxiom   a {@link org.apache.jena.rdf.model.Resource} object.
     */
    private void copyStatementsToNewAxiom(final Model inputModel, final Resource inputAxiom, final Model newModel,
                                          final Resource newAxiom) {
        for (final Statement axiomStatement : StatementIterable.list(inputModel, inputAxiom, null, null)) {
            final Property property = axiomStatement.getPredicate();

            if (CellarProperty.owl_annotatedPropertyP.equals(property) || CellarProperty.owl_annotatedSourceP.equals(property)
                    || CellarProperty.owl_annotatedTargetP.equals(property)) {
                continue;
            }

            newModel.add(this.createStatement(newAxiom, axiomStatement.getPredicate(), axiomStatement.getObject()));
        }
    }

    /**
     * <p>createStatement.</p>
     *
     * @param subject   a {@link org.apache.jena.rdf.model.Resource} object.
     * @param predicate a {@link org.apache.jena.rdf.model.Property} object.
     * @param object    a {@link org.apache.jena.rdf.model.RDFNode} object.
     * @return a {@link org.apache.jena.rdf.model.Statement} object.
     */
    private Statement createStatement(final Resource subject, final Property predicate, final RDFNode object) {
        try {
            if (subject == null) {
                LOG.warn("Creation of statement problem. Subject is null.");
            }
            if (predicate == null) {
                LOG.warn("Creation of statement problem. Predicate is null.");
            }
            if (object == null) {
                LOG.warn("Creation of statement problem. Object is null.");
            }

            return ResourceFactory.createStatement(subject, predicate, object);
        } catch (final RuntimeException e) {
            LOG.error("Creation of statement ['{}', '{}', '{}'] failed.", subject, predicate, object);
            throw e;
        }
    }

}
