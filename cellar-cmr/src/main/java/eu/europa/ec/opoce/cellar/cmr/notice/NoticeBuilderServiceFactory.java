/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice
 *             FILE : NoticeBuilderServiceFactory.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 24 Apr 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.notice;

import eu.europa.ec.opoce.cellar.cmr.notice.core.NoticeType;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 24 Apr 2017
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface NoticeBuilderServiceFactory {

    /**
     * Gets a new builder of type {@link type} for conveniently building a notice.
     *
     * @param type the notice's type
     * @return the builder newly created
     */
    NoticeBuilder getNewNoticeBuilderInstance(NoticeType type);

}
