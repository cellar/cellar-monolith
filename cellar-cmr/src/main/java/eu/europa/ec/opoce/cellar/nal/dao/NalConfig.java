package eu.europa.ec.opoce.cellar.nal.dao;

import com.google.common.base.MoreObjects;
import eu.europa.ec.opoce.cellar.cmr.database.DaoObject;

import java.util.Date;
import java.util.Objects;

/**
 * <p>NalConfig class.</p>
 */
public class NalConfig extends DaoObject implements Comparable<NalConfig> {

    private String uri;
    private String name;
    private String ontologyUri;
    private Date createdOn;
    private String versionNr;
    private Date versionDate;

    /**
     * External PID is a reference to the non-inferred NAL stored in
     * another repository. Previously the PID was a reference
     * to a s3 object but it is now a key for an AWS S3
     * object.
     */
    private String externalPid;
    private String version;
    private String versionInferred;


    public String getExternalPid() {
        return externalPid;
    }

    public void setExternalPid(String externalPid) {
        this.externalPid = externalPid;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getOntologyUri() {
        return ontologyUri;
    }

    public void setOntologyUri(String ontology_uri) {
        this.ontologyUri = ontology_uri;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public String getVersionNr() {
        return versionNr;
    }

    public void setVersionNr(String versionNr) {
        this.versionNr = versionNr;
    }

    public Date getVersionDate() {
        return versionDate;
    }

    public void setVersionDate(Date versionDate) {
        this.versionDate = versionDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getVersion() {
        return version;
    }

    public void setVersion(String newVersion) {
        this.version = newVersion;
    }

    public String getVersionInferred() {
        return versionInferred;
    }

    public void setVersionInferred(String versionInferred) {
        this.versionInferred = versionInferred;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("uri", uri)
                .add("name", name)
                .add("ontologyUri", ontologyUri)
                .add("createdOn", createdOn)
                .add("versionNr", versionNr)
                .add("versionDate", versionDate)
                .add("externalPid", externalPid)
                .add("version", version)
                .add("versionInferred", versionInferred)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NalConfig nalConfig = (NalConfig) o;
        return Objects.equals(uri, nalConfig.uri) &&
                Objects.equals(name, nalConfig.name) &&
                Objects.equals(ontologyUri, nalConfig.ontologyUri) &&
                Objects.equals(createdOn, nalConfig.createdOn) &&
                Objects.equals(versionNr, nalConfig.versionNr) &&
                Objects.equals(versionDate, nalConfig.versionDate) &&
                Objects.equals(externalPid, nalConfig.externalPid) &&
                Objects.equals(version, nalConfig.version) &&
                Objects.equals(versionInferred, nalConfig.versionInferred);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uri, name, ontologyUri, createdOn, versionNr, versionDate, externalPid, version, versionInferred);
    }

    @Override
    public int compareTo(NalConfig o) {
        return this.getUri().compareTo(o.getUri());
    }
}
