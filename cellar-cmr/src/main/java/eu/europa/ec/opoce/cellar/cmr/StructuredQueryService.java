/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr
 *             FILE : StructuredQueryService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 17 août 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr;

import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.utils.EmbargoUtils;
import eu.europa.ec.opoce.cellar.common.util.Pair;
import eu.europa.ec.opoce.cellar.database.OracleFactory;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.jena.oracle.OraCallbacks;
import eu.europa.ec.opoce.cellar.jena.oracle.OraTemplate;
import eu.europa.ec.opoce.cellar.semantic.jena.template.JenaQueryTemplate;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.query.Query;
import org.apache.jena.query.QuerySolutionMap;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.*;

import static eu.europa.ec.opoce.cellar.database.CmrTableName.CMR_METADATA;

/**
 * <p>StructuredQueryService class.</p>
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class StructuredQueryService {

    /**
     * The structured query configuration.
     */
    @Autowired
    private StructuredQueryConfiguration structuredQueryConfiguration;

    /**
     * The oracle factory.
     */
    @Autowired
    private OracleFactory oracleFactory;

    /**
     * The identifier service.
     */
    @Autowired
    @Qualifier("pidManagerService")
    private IdentifierService identifierService;

    /**
     * get a list of works that can be moved from private to public store.
     *
     * @return set of workUris and corresponding dates
     */
    public Set<Pair<String, Date>> getEmbargoChanged() {
        final List<Map<String, RDFNode>> bindings = executeSelect(CMR_METADATA.getEmbargoTable().toString(),
                structuredQueryConfiguration.resolveEmbargoDateQuery(), null);

        final HashSet<String> uris = new HashSet<>();
        final HashSet<Pair<String, Date>> queryResult = CollectionUtils.collect(bindings,
                input -> {
                    final String cellarUri = input.get("cellarURI").asResource().getURI();
                    final RDFNode rdfNode = input.get("embargo");
                    if (rdfNode == null) {
                        return new Pair<String, Date>(cellarUri, null);
                    }
                    final Date embargoDate = EmbargoUtils.parseDate(rdfNode.asLiteral().getLexicalForm());
                    uris.add(cellarUri);
                    return new Pair<>(cellarUri, embargoDate);
                }, new HashSet<>());

        return aggregate(uris, queryResult);
    }

    /**
     * Aggregate branches to reduce the number of operations.
     *
     * @param uris the uris
     * @param all  the all
     * @return the hash set
     */
    private HashSet<Pair<String, Date>> aggregate(final HashSet<String> uris, final HashSet<Pair<String, Date>> all) {
        //everything under a parent node can be excluded
        //e.g:
        //http://cellar-dev.publications.europa.eu/resource/cellar/47f59d7a-63b8-11e6-80a5-01aa75ed71a1.0020
        //http://cellar-dev.publications.europa.eu/resource/cellar/47f59d7a-63b8-11e6-80a5-01aa75ed71a1
        // the second one should exclude the first one
        //identify works and expressions and manifestations
        final HashSet<String> works = new HashSet<>();
        final HashSet<String> expressions = new HashSet<>();
        final HashSet<String> manifestations = new HashSet<>();
        final HashSet<String> items = new HashSet<>();
        for (final String uri : uris) {
            final String cellarPrefixed = identifierService.getCellarPrefixed(uri);
            final boolean isWorkDossierTopLevelEventOrAgent = DigitalObjectType.isWorkDossierTopLevelEventOrAgent(cellarPrefixed);
            if (isWorkDossierTopLevelEventOrAgent) {
                works.add(uri);
            }
            final boolean isExpressionOrEvent = DigitalObjectType.isExpressionOrEvent(cellarPrefixed);
            if (isExpressionOrEvent) {
                expressions.add(uri);
            }
            final boolean isManifestation = DigitalObjectType.isManifestation(cellarPrefixed);
            if (isManifestation) {
                manifestations.add(uri);
            }
            final boolean isItem = DigitalObjectType.isItem(cellarPrefixed);
            if (isItem) {
                items.add(uri);
            }
        }
        final HashSet<String> exclusions = new HashSet<String>();
        if (!works.isEmpty()) {
            for (final String work : works) {
                for (final String expression : expressions) {
                    if (expression.startsWith(work)) {
                        exclusions.add(expression);
                    }
                }
                for (final String manifestation : manifestations) {
                    if (manifestation.startsWith(work)) {
                        exclusions.add(manifestation);
                    }
                }
                for (final String item : items) {
                    if (item.startsWith(work)) {
                        exclusions.add(item);
                    }
                }
            }
        }

        if (!expressions.isEmpty()) {
            for (final String expression : expressions) {
                if (!exclusions.contains(expression)) {
                    for (final String manifestation : manifestations) {
                        if (manifestation.startsWith(expression)) {
                            exclusions.add(manifestation);
                        }
                    }
                    for (final String item : items) {
                        if (item.startsWith(expression)) {
                            exclusions.add(item);
                        }
                    }
                }
            }
        }

        if (!manifestations.isEmpty()) {
            for (final String manifestation : manifestations) {
                if (!exclusions.contains(manifestation)) {
                    for (final String item : items) {
                        if (item.startsWith(manifestation)) {
                            exclusions.add(item);
                        }
                    }
                }
            }
        }
        final HashSet<Pair<String, Date>> result = new HashSet<Pair<String, Date>>();
        for (final Pair<String, Date> pair : all) {
            final String uri = pair.getOne();
            if (!exclusions.contains(uri)) {
                result.add(pair);
            }
        }
        return result;
    }

    /**
     * get all DOs that are in the embargo-metadata-table.
     *
     * @return jenaModel with DOs Uris and the corresponding embargodate
     */
    public Model getEmbargoDigitalObjects() {
        // TODO: CELLARM-244, CELLAR-465
        // check for CMR table to use
        return executeConstruct(CMR_METADATA.getEmbargoTable().toString(), structuredQueryConfiguration.resolveEmbargoDigitalObjectQuery(),
                null);
    }

    /**
     * <p>executeSelect.</p>
     *
     * @param tableName      a {@link java.lang.String} object.
     * @param query          a {@link org.apache.jena.query.Query} object.
     * @param initialBinding a {@link org.apache.jena.query.QuerySolutionMap} object.
     * @return a {@link java.util.List} object.
     */
    private List<Map<String, RDFNode>> executeSelect(final String tableName, final Query query, final QuerySolutionMap initialBinding) {
        if (query == null) {
            throw ExceptionBuilder.get(CellarException.class).build();
        }
        if (StringUtils.isEmpty(tableName)) {
            throw ExceptionBuilder.get(CellarException.class).build();
        }

        return OraTemplate.execute(oracleFactory.getOracle(),
                OraCallbacks.modelCallback(tableName, model -> {
                    return JenaQueryTemplate.select(model, query, initialBinding);
                }));
    }

    /**
     * <p>executeConstruct.</p>
     *
     * @param tableName      a {@link java.lang.String} object.
     * @param query          a {@link org.apache.jena.query.Query} object.
     * @param initialBinding a {@link org.apache.jena.query.QuerySolutionMap} object.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    private Model executeConstruct(final String tableName, final Query query, final QuerySolutionMap initialBinding) {
        if (query == null) {
            throw ExceptionBuilder.get(CellarException.class).build();
        }
        if (StringUtils.isEmpty(tableName)) {
            throw ExceptionBuilder.get(CellarException.class).build();
        }

        return OraTemplate.execute(oracleFactory.getOracle(),
                OraCallbacks.modelCallback(tableName, model -> {
                    return JenaQueryTemplate.construct(model, query, initialBinding);
                }));
    }
}
