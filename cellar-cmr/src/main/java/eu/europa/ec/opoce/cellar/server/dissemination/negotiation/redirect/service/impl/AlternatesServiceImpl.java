/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.redirect.service
 *             FILE : AlternatesCalculator.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 2, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.redirect.service.impl;

import eu.europa.ec.opoce.cellar.cl.dao.CellarIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.InferenceVariance;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.Structure;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.Type;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.redirect.service.AlternatesService;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;
import eu.europa.ec.opoce.cellar.server.service.DisseminationService;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Apr 2, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component
public class AlternatesServiceImpl implements AlternatesService {

    private static final Logger LOG = LogManager.getLogger(AlternatesServiceImpl.class);

    /** The dissemination service. */
    @Autowired
    private DisseminationService disseminationService;

    /** The cellar resource dao. */
    @Autowired
    private CellarResourceDao cellarResourceDao;

    /** The cellar identifier dao. */
    @Autowired
    private CellarIdentifierDao cellarIdentifierDao;

    /** The Constant BASE_URI_PLACE_HOLDER. */
    private static final String BASE_URI_PLACE_HOLDER = "§BASE_URI§";

    /** The Constant SOURCE_QUALITY. */
    private static final String SOURCE_QUALITY = "1.0"; // yet, a constant and not mutable

    /** {@inheritDoc} */
    @Override
    public Set<String> getXmlAlternatesWithBaseUriPlaceHolder(final DigitalObjectType resourceType, final String extensionPattern,
            final Structure structure, final LanguageBean decodingLanguage) {
        switch (resourceType) {
        case WORK:
        case DOSSIER:
        case AGENT:
        case TOPLEVELEVENT:
            return this.getXmlAlternatives(extensionPattern, structure, decodingLanguage, true);
        case EXPRESSION:
        case EVENT:
            return this.getXmlAlternatives(extensionPattern, structure, decodingLanguage, false);
        case MANIFESTATION:
            // XML manifestations have no alternates --> return an empty set
            return this.constructAlternateEntries(new HashSet<String>(), "application/xml");
        default:
            this.logNoPossibleAlternatesMessage(resourceType);
            return null;
        }
    }

    /**
     * Gets the xml alternatives.
     *
     * @param extensionPattern the extension pattern
     * @param structure the structure
     * @param decodingLanguage the decoding language
     * @param alternativesForWorkLevel the alternatives for work level
     * @return the xml alternatives
     */
    private Set<String> getXmlAlternatives(final String extensionPattern, final Structure structure, final LanguageBean decodingLanguage,
            final boolean alternativesForWorkLevel) {
        final String responseLink = String.format(extensionPattern, Type.XML.getURLToken(), structure.getURLToken(),
                decodingLanguage.getIsoCodeTwoChar());

        final String[] allAlternatives = (alternativesForWorkLevel ? getAllXmlAlternativesForWorkLevels(extensionPattern, decodingLanguage)
                : this.getAllXmlAlternativesForExpressionLevels(extensionPattern, decodingLanguage));

        final Set<String> alternateLinks = new HashSet<String>();
        alternateLinks.addAll(Arrays.asList(allAlternatives));
        alternateLinks.remove(responseLink);

        return this.constructAlternateEntries(alternateLinks, "application/xml");
    }

    /**
     * Gets the all xml alternatives for work levels.
     *
     * @param extensionPattern the extension pattern
     * @param decodingLanguage the decoding language
     * @return the all xml alternatives for work levels
     */
    private static String[] getAllXmlAlternativesForWorkLevels(final String extensionPattern, final LanguageBean decodingLanguage) {
        return new String[] {
                String.format(extensionPattern, Type.XML.getURLToken(), Structure.TREE.getURLToken(), decodingLanguage.getIsoCodeTwoChar()),
                String.format(extensionPattern, Type.XML.getURLToken(), Structure.OBJECT.getURLToken(),
                        decodingLanguage.getIsoCodeTwoChar())};
    }

    /**
     * Gets the all xml alternatives for expression levels.
     *
     * @param extensionPattern the extension pattern
     * @param decodingLanguage the decoding language
     * @return the all xml alternatives for expression levels
     */
    private String[] getAllXmlAlternativesForExpressionLevels(final String extensionPattern, final LanguageBean decodingLanguage) {
        return new String[] {
                String.format(extensionPattern, Type.XML.getURLToken(), Structure.BRANCH.getURLToken(),
                        decodingLanguage.getIsoCodeTwoChar()),
                String.format(extensionPattern, Type.XML.getURLToken(), Structure.OBJECT.getURLToken(),
                        decodingLanguage.getIsoCodeTwoChar())};
    }

    /** {@inheritDoc} */
    @Override
    public Set<String> getRdfAlternatesWithBaseUriPlaceHolder(final DigitalObjectType resourceType, final String extensionPattern,
            final Structure structure, final InferenceVariance inferenceVariance,boolean normalized) {
        switch (resourceType) {
        case WORK:
        case DOSSIER:
        case AGENT:
        case TOPLEVELEVENT:
            return this.getRdfAlternatives(extensionPattern, structure, inferenceVariance, true,normalized);
        case EXPRESSION:
        case MANIFESTATION:
        case EVENT:
            return this.getRdfAlternatives(extensionPattern, structure, inferenceVariance, false,normalized);
        default:
            this.logNoPossibleAlternatesMessage(resourceType);
            return null;
        }
    }

    /**
     * Gets the rdf alternatives.
     *
     * @param extensionPattern the extension pattern
     * @param structure the structure
     * @param inferenceVariance the inference variance
     * @param alternativesForWorkLevel the alternatives for work level
     * @param normalized if the notice should be normalized
     * @return the rdf alternatives
     */
    private Set<String> getRdfAlternatives(final String extensionPattern, final Structure structure,
            final InferenceVariance inferenceVariance, final boolean alternativesForWorkLevel,final boolean normalized) {
        String normalizedToken=normalized?"normalized":"";
        final String responseLink = String.format(extensionPattern, Type.RDF.getURLToken(), structure.getURLToken(),
                inferenceVariance.getURLToken(),normalizedToken);

        final String[] allAlternatives = (alternativesForWorkLevel ? getAllRdfAlternativesForWorkLevels(extensionPattern)
                : getAllRdfAlternativesForNonWorkLevels(extensionPattern));

        final Set<String> alternateLinks = new HashSet<String>();
        alternateLinks.addAll(Arrays.asList(allAlternatives));
        alternateLinks.remove(responseLink);

        return this.constructAlternateEntries(alternateLinks, "application/rdf+xml");
    }

    /**
     * Gets the all rdf alternatives for work levels.
     *
     * @param extensionPattern the extension pattern
     * @return the all rdf alternatives for work levels
     */
    private static String[] getAllRdfAlternativesForWorkLevels(final String extensionPattern) {
        return new String[] {
                String.format(extensionPattern, Type.RDF.getURLToken(), Structure.TREE.getURLToken(), InferenceVariance.FULL.getURLToken(),"normalized"),
                String.format(extensionPattern, Type.RDF.getURLToken(), Structure.TREE.getURLToken(), InferenceVariance.FULL.getURLToken(),"non-normalized"),
                String.format(extensionPattern, Type.RDF.getURLToken(), Structure.TREE.getURLToken(),
                        InferenceVariance.NONINFERRED.getURLToken(),"normalized"),
                String.format(extensionPattern, Type.RDF.getURLToken(), Structure.TREE.getURLToken(),
                        InferenceVariance.NONINFERRED.getURLToken()),
                String.format(extensionPattern, Type.RDF.getURLToken(), Structure.OBJECT.getURLToken(),
                        InferenceVariance.FULL.getURLToken(),"normalized"),
                String.format(extensionPattern, Type.RDF.getURLToken(), Structure.OBJECT.getURLToken(),
                        InferenceVariance.FULL.getURLToken()),
                String.format(extensionPattern, Type.RDF.getURLToken(), Structure.OBJECT.getURLToken(),
                        InferenceVariance.NONINFERRED.getURLToken(),"normalized"),
                String.format(extensionPattern, Type.RDF.getURLToken(), Structure.OBJECT.getURLToken(),
                        InferenceVariance.NONINFERRED.getURLToken())};
    }

    /**
     * Gets the all rdf alternatives for non work levels.
     *
     * @param extensionPattern the extension pattern
     * @return the all rdf alternatives for non work levels
     */
    private static String[] getAllRdfAlternativesForNonWorkLevels(final String extensionPattern) {
        return new String[] {
                String.format(extensionPattern, Type.RDF.getURLToken(), Structure.OBJECT.getURLToken(),
                        InferenceVariance.FULL.getURLToken(),"normalized"),
                String.format(extensionPattern, Type.RDF.getURLToken(), Structure.OBJECT.getURLToken(),
                        InferenceVariance.FULL.getURLToken()),
                String.format(extensionPattern, Type.RDF.getURLToken(), Structure.OBJECT.getURLToken(),
                        InferenceVariance.NONINFERRED.getURLToken(),"normalized"),
                String.format(extensionPattern, Type.RDF.getURLToken(), Structure.OBJECT.getURLToken(),
                        InferenceVariance.NONINFERRED.getURLToken())};
    }

    /**
     * Log no possible alternates message.
     *
     * @param resourceType the resource type
     */
    private void logNoPossibleAlternatesMessage(final DigitalObjectType resourceType) {
        LOG.debug("The ResourceType '{}' has no possible alternates! No alternates are given back even if they were requested.", resourceType);
    }

    /** {@inheritDoc} */
    @Override
    public Set<String> getContentPoolAlternatesWithBaseUriPlaceHolder(final CellarResourceBean cellarResource,
            final String extensionPattern, final Type type) {

        final Type alternateType = (type == Type.ZIP ? Type.LIST : Type.ZIP);

        final Set<String> sortedAlternateEntries = new TreeSet<String>();

        sortedAlternateEntries.add(this.constructAlternateEntry(String.format(extensionPattern, alternateType.getURLToken()),
                "application/" + alternateType.getURLToken()));

        final String contentStreamWithLowestNumber = this.getContentStreamWithLowestNumber(cellarResource);
        final String contentStreamCellarId = cellarResource.getCellarId() + "/" + contentStreamWithLowestNumber;
        if (StringUtils.isNotBlank(contentStreamWithLowestNumber)) {
            sortedAlternateEntries.add(this.constructAlternateEntry(contentStreamWithLowestNumber,
                    this.cellarResourceDao.findCellarId(contentStreamCellarId).getMimeType()));
        }

        return sortedAlternateEntries;
    }

    /**
     * Gets the content stream with lowest number.
     *
     * @param cellarResource the cellar resource
     * @return the content stream with lowest number
     */
    private String getContentStreamWithLowestNumber(final CellarResourceBean cellarResource) {
        final Map<Integer, String> sortedContentStreams = new TreeMap<Integer, String>();
        final Collection<MetsElement> manifestationContents = this.disseminationService.getManifestationContents(cellarResource);
        for (final MetsElement element : manifestationContents) {
            String contentStreamName;
            Integer docIndex;
            final ContentIdentifier contentIdentifier = element.getCellarId();
            final String identifier = contentIdentifier.getIdentifier();
            final String cellarId = cellarResource.getCellarId();
            final CellarIdentifier cellarIdentifier = this.cellarIdentifierDao.getCellarIdentifier(identifier);
            final String searchString = cellarId + "/";
            contentStreamName = StringUtils.replace(identifier, searchString, "");
            docIndex = cellarIdentifier.getDocIndex();
            sortedContentStreams.put(docIndex, contentStreamName);
        }
        final Iterator<Entry<Integer, String>> iter = sortedContentStreams.entrySet().iterator();
        if (iter.hasNext()) {
            return iter.next().getValue();
        } else {
            return null;
        }
    }

    /**
     * Construct alternate entries.
     *
     * @param links the links
     * @param mimeType the mime type
     * @return the sets the
     */
    private Set<String> constructAlternateEntries(final Set<String> links, final String mimeType) {
        final Set<String> alternateEntries = new HashSet<String>();
        for (final String link : links) {
            alternateEntries.add(this.constructAlternateEntry(link, mimeType));
        }
        return alternateEntries;
    }

    /**
     * Construct alternate entry.
     *
     * @param link the link
     * @param mimeType the mime type
     * @return the string
     */
    private String constructAlternateEntry(final String link, final String mimeType) {
        return "{\"" + BASE_URI_PLACE_HOLDER + "/" + link + "\" " + SOURCE_QUALITY + " {type " + mimeType + "}}";
    }

    /** {@inheritDoc} */
    @Override
    public SortedSet<String> getContentStreamAlternates(final String itemUri) {

        final Set<String> sortedAlternateEntries = new TreeSet<String>();

        sortedAlternateEntries
                .add(this.constructAlternateEntry(String.format("%s", Type.ZIP.getURLToken()), "application/" + Type.ZIP.getURLToken()));
        sortedAlternateEntries
                .add(this.constructAlternateEntry(String.format("%s", Type.LIST.getURLToken()), "application/" + Type.LIST.getURLToken()));

        return this.applyBaseUriToAlternatesWithPlaceHolder(itemUri, sortedAlternateEntries);
    }

    /** {@inheritDoc} */
    @Override
    public SortedSet<String> applyBaseUriToAlternatesWithPlaceHolder(final String baseUri, final Set<String> alternateEntries) {
        final SortedSet<String> completeAlternateEntries = new TreeSet<String>();
        for (final String entry : alternateEntries) {
            completeAlternateEntries.add(StringUtils.replace(entry, BASE_URI_PLACE_HOLDER, baseUri));
        }
        return completeAlternateEntries;
    }
}
