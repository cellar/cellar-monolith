/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.ontology
 *        FILE : ModelUtils.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 23-05-2013
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.model;

import eu.europa.ec.opoce.cellar.common.util.Pair;
import org.apache.jena.graph.Triple;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * <class_description> Lazy implementation of a model that can be retrieved in several formats ("facets"). <br />
 * It is lazy in the sense that its facets are initialized only when requested via methods <code>as*</code>,
 * and if empty, they can be retrieved from other initialized facets, if any. <br /><br />
 * 
 * The dependencies between facets are as follows:<br /><br />
 * 
 * Model can be initialized from:<ul>
 *   <li>MPC (ModelPerContext)</li>
 *   <li>TPC (TriplesPerContext)</li>
 *   <li>QPR (QuadruplePerRowid)</li></ul>
 * MPC can be initialized from:<ul>
 *   <li>TPC</li>
 *   <li>QPR</li></ul>
 * TPC can be initialized from:<ul>
 *   <li>MPC</li>
 *   <li>QPR</li></ul>
 * QPR cannot be initialized from anything else. <br /><br />
 * 
 *      --- Model ---
 *      |     |     |
 *      |     |     |
 *      V     |     V
 *     MPC <--+--> TPC
 *      |     |     |
 *      |     |     |
 *      |     V     |
 *      ---> QPR <---
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 23-05-2013
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public class LazyFacetedModel implements IFacetedModel {

    @SuppressWarnings("unused")
    private static final Logger LOG = LogManager.getLogger(LazyFacetedModel.class);

    private Model model;
    private Map<String, Model> modelPerContext;
    private Map<String, Collection<Triple>> triplesPerContext;
    private Map<Pair<Triple, String>, String> quadruplePerRowid;

    public LazyFacetedModel() {
    }

    @SuppressWarnings("unchecked")
    public LazyFacetedModel(final Map<?, ?> data) {
        if (data != null && data.entrySet().size() > 0) {
            final Entry<?, ?> firstEntry = data.entrySet().iterator().next();
            if (firstEntry.getKey() instanceof String && firstEntry.getValue() instanceof Model) {
                this.modelPerContext = (Map<String, Model>) data;
            } else if (firstEntry.getKey() instanceof String && firstEntry.getValue() instanceof Map<?, ?>) {
                this.triplesPerContext = (Map<String, Collection<Triple>>) data;
            } else if (firstEntry.getKey() instanceof Pair<?, ?> && firstEntry.getValue() instanceof String) {
                this.quadruplePerRowid = (Map<Pair<Triple, String>, String>) data;
            }
        }
    }

    /**
     * Model can be retrieved from:<br />
     *   - ModelPerContext <br />
     *   - TriplesPerContext <br />
     *   - QuadruplePerRowid. <br />
     */
    @Override
    public Model asModel() {
        if (this.model == null) {
            // try to populate from model per context
            if (this.modelPerContext != null) {
                this.model = ModelUtils.getModelFromModelPerContext(this.modelPerContext);
            }
            // try to populate from triples per context
            else if (this.triplesPerContext != null) {
                this.model = ModelUtils.getModelFromTriplesPerContext(this.triplesPerContext);
            }
            // try to populate from quadruple per rowid
            else if (this.quadruplePerRowid != null) {
                final Map<String, Model> myModelPerContext = ModelUtils.getModelsFromPairs(this.quadruplePerRowid.keySet());
                this.model = ModelUtils.getModelFromModelPerContext(myModelPerContext);
            }
            // default to empty
            else {
                this.model = ModelFactory.createDefaultModel();
            }
        }

        return this.model;
    }

    /**
     * ModelPerContext can be retrieved from:<br />
     *   - TriplesPerContext <br />
     *   - QuadruplePerRowid. <br />
     */
    @Override
    public Map<String, Model> asModelPerContext() {
        if (this.modelPerContext == null) {
            // try to populate from triples per context
            if (this.triplesPerContext != null) {
                this.modelPerContext = ModelUtils.getModelPerContextFromTriplesPerContext(this.triplesPerContext);
            }
            // try to populate from quadruple per rowid
            else if (this.quadruplePerRowid != null) {
                this.modelPerContext = ModelUtils.getModelsFromPairs(this.quadruplePerRowid.keySet());
            }
            // default to empty
            else {
                this.modelPerContext = new HashMap<String, Model>();
            }
        }

        return this.modelPerContext;
    }

    /**
     * TriplesPerContext can be retrieved from:<br />
     *   - ModelPerContext <br />
     *   - QuadruplePerRowid. <br />
     */
    @Override
    public Map<String, Collection<Triple>> asTriplesPerContext() {
        if (this.triplesPerContext == null) {
            // try to populate from model per context
            if (this.modelPerContext != null) {
                this.triplesPerContext = ModelUtils.getTriplesPerContextFromModelPerContext(this.modelPerContext);
            }
            // try to populate from quadruple per rowid
            else if (this.quadruplePerRowid != null) {
                final Map<String, Model> myModelPerContext = ModelUtils.getModelsFromPairs(this.quadruplePerRowid.keySet());
                this.triplesPerContext = ModelUtils.getTriplesPerContextFromModelPerContext(myModelPerContext);
            }
            // default to empty
            else {
                this.triplesPerContext = new HashMap<String, Collection<Triple>>();
            }
        }

        return this.triplesPerContext;
    }

    /**
     * QuadruplePerRowid cannot be retrieved from anything, and - if null - must defaults to empty.
     */
    @Override
    public Map<Pair<Triple, String>, String> asQuadruplePerRowid() {
        if (this.quadruplePerRowid == null) {
            // default to empty
            this.quadruplePerRowid = new HashMap<Pair<Triple, String>, String>();
        }

        return this.quadruplePerRowid;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cmr.model.IFacetedModel#closeQuietly()
     */
    @Override
    public void closeQuietly() {
        ModelUtils.closeQuietly(this.model);
        ModelUtils.closeQuietly(this.modelPerContext);
    }

}
