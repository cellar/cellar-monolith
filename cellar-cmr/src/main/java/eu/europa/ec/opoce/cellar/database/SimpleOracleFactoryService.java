package eu.europa.ec.opoce.cellar.database;

import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import oracle.jdbc.OracleConnection;
import oracle.spatial.rdf.client.jena.Oracle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Factory class to retrieve <code>Oracle</code> instances.
 * This class implements the {@OracleFactory} interface by creating new <code>Oracle</code> instances
 * from <code>OracleConnection</code> instances retrieved from a given <code>DataSource</code>.
 *
 * @see OracleFactory
 * 
 * @deprecated because not used.
 */
@Deprecated
//@Service
public class SimpleOracleFactoryService implements OracleFactory {

    @Autowired(required = true)
    @Qualifier("cmrDataSource")
    private DataSource datasource;

    /** {@inheritDoc} */
    @Override
    public Oracle getOracle() {
        try(Connection connection = datasource.getConnection()) {
            if (connection instanceof OracleConnection) {
                return new Oracle(connection.unwrap(OracleConnection.class));
            }
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.CMR_INTERNAL_ERROR)
                    .withMessage("Unable to extract Oracle Connection from datasource").build();
        } catch (SQLException e) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.CMR_INTERNAL_ERROR).withCause(e).build();
        }
    }

}
