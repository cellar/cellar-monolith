/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.service.impl
 *             FILE : NormalizationQueryConfiguration.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Sep 30, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.service.impl;

import eu.europa.ec.opoce.cellar.cmr.service.INormalizationQueryConfiguration;
import eu.europa.ec.opoce.cellar.cmr.sparql.utils.QueryUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Sep 30, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component("normalizationQueryConfiguration")
public class NormalizationQueryConfiguration implements INormalizationQueryConfiguration {

    private static final String UNION_CLAUSE = " UNION ";

    private static final String WHERE_CLAUSE_REGEX = "\\$\\{where_clause\\}";
    private static final Pattern WHERE_CLAUSE_PATTERN = Pattern.compile(WHERE_CLAUSE_REGEX, Pattern.DOTALL);

    private static final String NORMALIZED_URI_REGEX = "\\$\\{normalized_uri\\}";
    private static final Pattern NORMALIZED_URI_PATTERN = Pattern.compile(NORMALIZED_URI_REGEX, Pattern.DOTALL);

    private static final String NON_NORMALIZED_URI_REGEX = "\\$\\{non_normalized_uri\\}";
    private static final Pattern NON_NORMALIZED_URI_PATTERN = Pattern.compile(NON_NORMALIZED_URI_REGEX, Pattern.DOTALL);

    @Autowired
    @Qualifier("normalizationTemplatesStringsMap")
    private HashMap<String, String> normalizationTemplatesStringsMap;

    @Override
    public String getQueryForObjectsWithGraphNormalization(final String normalizedUri, final Collection<String> nonNormalizedUris) {
        return this.getQueryForNormalization(normalizedUri, nonNormalizedUris, "updateNormalizeObjectsWithGraphTemplate",
                "whereClauseNormalizeObjectsWithGraphTemplate");
    }

    @Override
    public String getQueryForSubjectsWithGraphNormalization(String normalizedUri, Collection<String> nonNormalizedUris) {
        return this.getQueryForNormalization(normalizedUri, nonNormalizedUris, "updateNormalizeSubjectsWithGraphTemplate",
                "whereClauseNormalizeSubjectsWithGraphTemplate");
    }

    @Override
    public String getQueryForObjectsWithoutGraphNormalization(final String normalizedUri, final Collection<String> nonNormalizedUris) {
        return this.getQueryForNormalization(normalizedUri, nonNormalizedUris, "updateNormalizeObjectsWithoutGraphTemplate",
                "whereClauseNormalizeObjectsWithoutGraphTemplate");
    }

    @Override
    public String getQueryForSubjectsWithoutGraphNormalization(String normalizedUri, Collection<String> nonNormalizedUris) {
        return this.getQueryForNormalization(normalizedUri, nonNormalizedUris, "updateNormalizeSubjectsWithoutGraphTemplate",
                "whereClauseNormalizeSubjectsWithoutGraphTemplate");
    }

    /** 
     * Query object cannot be returned: Query does not support DELETE query.
     */
    private String getQueryForNormalization(final String normalizedUri, final Collection<String> nonNormalizedUris,
            final String queryTemplateName, final String whereClauseTemplateName) {
        final Map<Pattern, Object> placeHoldersMap = new HashMap<Pattern, Object>();
        placeHoldersMap.put(WHERE_CLAUSE_PATTERN, this.getWhereClauseForObjectsNormalization(nonNormalizedUris, whereClauseTemplateName));
        placeHoldersMap.put(NORMALIZED_URI_PATTERN, normalizedUri);

        return QueryUtils.resolveString(queryTemplateName, this.normalizationTemplatesStringsMap, placeHoldersMap);
    }

    private String getWhereClauseForObjectsNormalization(final Collection<String> nonNormalizedUris, final String whereClauseTemplateName) {
        final List<String> whereClauses = new LinkedList<String>();

        for (final String nonNormalizedUri : nonNormalizedUris) {
            whereClauses.add(QueryUtils.resolveString(whereClauseTemplateName, this.normalizationTemplatesStringsMap,
                    Collections.singletonMap(NON_NORMALIZED_URI_PATTERN, (Object) nonNormalizedUri)));
        }

        return StringUtils.join(whereClauses, UNION_CLAUSE);
    }

}
