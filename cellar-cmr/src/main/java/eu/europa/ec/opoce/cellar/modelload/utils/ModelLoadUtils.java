/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.modelload.domain
 *             FILE : ModelLoadRequestUtils.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Mar 17, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.modelload.utils;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentStream;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsPackage;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.domain.NalObject;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.riot.RDFFormat;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Mar 17, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class ModelLoadUtils {

    private static final Logger LOG = LogManager.getLogger(ModelLoadUtils.class);

    public static void validateModels(final CalculatedData data) {
        validateModels(data.getStructMap().getDigitalObject(), data.getMetsPackage(), new HashSet<String>());
    }

    public static void validateModels(final DigitalObject digitalObject, final MetsPackage metsPackage, final Set<String> modelLoadURLs) {

        if (digitalObject.getModelLoadURL() != null) { // this object is a manifestation (validated by METS parser)

            if (!modelLoadURLs.add(digitalObject.getModelLoadURL())) {
                throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.ERROR_WHILE_PARSING_THE_METS_DOCUMENT)
                        .withMessage("Model-load URL '{}' cannot be specified several times in the same package.")
                        .withMessageArgs(digitalObject.getModelLoadURL()).build();
            }

            final List<String> fileRefs = getRdfContentStreamsFileRefs(digitalObject);

            if (fileRefs.size() <= 0) { // at least one model is required
                throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.ERROR_WHILE_PARSING_THE_METS_DOCUMENT)
                        .withMessage("Manifestation '{}' does not contain '{}' stream(s).")
                        .withMessageArgs(digitalObject.getCellarId().getIdentifier(), NalObject.APPLICATION_RDF_XML).build();
            }

            LOG.info("Validating model-load RDF files '{}'...", fileRefs);

            for (final String fileRef : fileRefs) {
                try {
                    JenaUtils.read(metsPackage.getFileStream(fileRef), fileRef, RDFFormat.RDFXML_PLAIN);
                } catch (Exception exception) {
                    throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.FAILED_PARSING_CONTENT_STREAM)
                            .withMessage("Model-load RDF file '{}' cannot be read.").withMessageArgs(fileRef).withCause(exception).build();
                }
            }

            LOG.info("Validation of model-load RDF files '{}' has been performed successfully.", fileRefs);
        }

        for (final DigitalObject o : digitalObject.getChildObjects()) {
            validateModels(o, metsPackage, modelLoadURLs);
        }
    }

    public static List<String> getRdfContentStreamsFileRefs(final DigitalObject digitalObject) {
        final List<String> fileRefs = new LinkedList<>();

        for (final ContentStream cs : digitalObject.getContentStreams()) {
            if (StringUtils.equalsIgnoreCase(cs.getMimeType(), NalObject.APPLICATION_RDF_XML)) {
                fileRefs.add(cs.getFileRef());
            }
        }

        return fileRefs;
    }
}
