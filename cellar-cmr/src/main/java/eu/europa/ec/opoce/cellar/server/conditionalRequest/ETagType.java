/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.etag
 *             FILE : ETagType.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 21, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.conditionalRequest;

import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.semantic.helper.DateFormats;

import java.util.Date;

/**
 * <class_description> This class defines the groups for the etag calculation
 * <br/><br/>
 * ON : Oct 21, 2014.
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public enum ETagType {

    /** The tree dossier. */
    TREE_DOSSIER("TDo"),

    /** The tree work. */
    TREE_WORK("TWo", DigitalObjectType.WORK, DigitalObjectType.EXPRESSION, DigitalObjectType.MANIFESTATION),

    /** The tree agent. */
    TREE_AGENT("TAg"),

    /** The tree top level event. */
    TREE_TOPLEVELEVENT("TTe"),

    /** The branch expression. */
    BRANCH_EXPRESSION("BEx", DigitalObjectType.WORK, DigitalObjectType.EXPRESSION, DigitalObjectType.MANIFESTATION),

    /** The branch event. */
    BRANCH_EVENT("BEv"),

    /** The object. */
    OBJECT("Obj"),

    /** The rdf tree work. */
    RDF_TREE_WORK("RTW", DigitalObjectType.WORK, DigitalObjectType.EXPRESSION, DigitalObjectType.MANIFESTATION),

    /** The rdf tree dossier. */
    RDF_TREE_DOSSIER("RTD"),

    /** The rdf tree agent. */
    RDF_TREE_AGENT("RTA"),

    /** The rdf tree top level event. */
    RDF_TREE_TOPLEVELEVENT("RTT"),

    /** The rdf object. */
    RDF_OBJECT("ROb"),

    /** The list content. */
    LIST_CONTENT("Cls"),

    /** The zip content. */
    ZIP_CONTENT("Czp"),

    /** The content stream. */
    CONTENT_STREAM("Con"),

    /** The identifiers. */
    IDENTIFIERS("Ide"),

    /** The oj list. */
    OJ_LIST("OJl"),

    /** The concept scheme. */
    CONCEPT_SCHEME("Cos");

    /** The Constant START. */
    private static final String START = "\"";

    /** The Constant PREFIX_SEPARATOR. */
    private static final String PREFIX_SEPARATOR = "-";

    /** The Constant END. */
    private static final String END = "\"";

    /** The value. */
    private final String value;

    /** The do types. */
    private final DigitalObjectType[] doTypes;

    /**
     * Instantiates a new e tag type.
     *
     * @param value the value
     */
    private ETagType(final String value) {
        this.value = value;
        this.doTypes = null;
    }

    /**
     * Instantiates a new e tag type.
     *
     * @param value the value
     * @param doTypes the do types
     */
    private ETagType(final String value, final DigitalObjectType... doTypes) {
        this.value = value;
        this.doTypes = doTypes;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    public String getValue() {
        return this.value;
    }

    /**
     * Gets the digital object types.
     *
     * @return the digital object types
     */
    public DigitalObjectType[] getDigitalObjectTypes() {
        return this.doTypes;
    }

    /**
     * Builds the e tag.
     *
     * @param date the date
     * @return the string
     */
    public String buildETag(final Date date) {
        return START +
                this.getValue() +
                PREFIX_SEPARATOR +
                DateFormats.formatUnreadableFullDateTime(date) +
                END;
    }
}
