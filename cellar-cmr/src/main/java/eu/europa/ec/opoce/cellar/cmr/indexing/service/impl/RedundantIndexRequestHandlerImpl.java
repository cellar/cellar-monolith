/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cmr.indexing.service.impl
 *        FILE : RedundantIndexRequestHandlerImpl.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 28-04-2022
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2022 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.indexing.service.impl;

import java.util.*;
import java.util.stream.Collectors;

import eu.europa.ec.opoce.cellar.cmr.indexing.service.IndexRequestManagerService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eu.europa.ec.opoce.cellar.cmr.database.dao.CmrIndexRequestDao;
import eu.europa.ec.opoce.cellar.cmr.indexing.service.RedundantIndexRequestHandler;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.Action;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.ExecutionStatus;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.RequestType;

/**
 * Service for identifying and handling redundant indexing requests.
 * @author EUROPEAN DYNAMICS S.A.
 */
@Service("redundantIndexRequestHandler")
public class RedundantIndexRequestHandlerImpl implements RedundantIndexRequestHandler {

	/**
	 * 
	 */
    private final CmrIndexRequestDao cmrIndexRequestDao;
	/**
	 * 
	 */
    private static final String REQUEST_LOG_PLACEHOLDER = "[ID: %s, OBJECT_URI: %s, REQUEST_TYPE: %s, ACTION: %s, PRIORITY: %s, EXECUTION_STATUS: %s]";
    /**
	 * 
	 */
    private static final Logger LOG = LogManager.getLogger(RedundantIndexRequestHandlerImpl.class);
	
	/**
	 *
	 */
	private final IndexRequestManagerService indexRequestManagerService;
    
    
    @Autowired
	public RedundantIndexRequestHandlerImpl(CmrIndexRequestDao cmrIndexRequestDao,
											IndexRequestManagerService indexRequestManagerService) {
		this.cmrIndexRequestDao = cmrIndexRequestDao;
		this.indexRequestManagerService = indexRequestManagerService;
	}
	
    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void detectAndUpdateRedundantRequests() {
    	// Retrieve all indexing requests having status 'New' or 'Pending'.
    	final Collection<CmrIndexRequest> indexRequests = this.cmrIndexRequestDao.findByExecutionStatus(ExecutionStatus.New, ExecutionStatus.Pending);
    	// Group the indexing requests by OBJECT_URI, REQUEST_TYPE, EXECUTION_STATUS.
    	final Map<String, Map<RequestType, Map<ExecutionStatus, List<CmrIndexRequest>>>> groupedIndexRequests = indexRequests.stream()
	    		.collect(Collectors.groupingBy(CmrIndexRequest::getObjectUri,
	    					Collectors.groupingBy(CmrIndexRequest::getRequestType,
	    							Collectors.groupingBy(CmrIndexRequest::getExecutionStatus))));
    	
    	// Iterate over the retrieved requests and detect those that should become Redundant.
    	final List<CmrIndexRequest> redundantRequests = detectRedundantRequests(groupedIndexRequests);
    	// Update the status of the previously identified indexing requests.
    	updateRequests(redundantRequests);
		// Update the INDX_EXECUTION_STATUS of the corresponding STRUCTMAP_STATUS_HISTORY entries
		this.indexRequestManagerService.updateStructMapIndxStatusAndRespectiveDatetime(redundantRequests, ExecutionStatus.Redundant);
    	// Log the number of updated/modified rows (requests) in CMR_INDEX_REQUEST.
    	logNumberOfNewRequestsSetAsRedundant(redundantRequests.size());
    }
    
    /**
     * Performs the detection of redundant indexing requests.
     * @param groupedIndexRequests the 'New' and 'Pending' indexing requests grouped appropriately. 
     * @return the indexing requests to be skipped, i.e. those that will not be executed.
     */
    private List<CmrIndexRequest> detectRedundantRequests(Map<String, Map<RequestType, Map<ExecutionStatus, List<CmrIndexRequest>>>> groupedIndexRequests) {
    	List<CmrIndexRequest> overallRedundantRequests = new ArrayList<>();
    	// The optimization will be attempted for a given OBJECT_URI and REQUEST_TYPE combination.
    	for (Map.Entry<String, Map<RequestType, Map<ExecutionStatus, List<CmrIndexRequest>>>> uriGroup : groupedIndexRequests.entrySet()) {
    		for (Map.Entry<RequestType, Map<ExecutionStatus, List<CmrIndexRequest>>> reqTypeGroup : uriGroup.getValue().entrySet()) {
    			// The optimization is applied if the following two cases hold:
    			//  1. When for a given OBJECT_URI and REQUEST_TYPE combination both 'New' and 'Pending' indexing requests exist.
    			//  2. There is no request with ACTION 'Remove' among them.
    			if (isProceedWithDetection(reqTypeGroup)) {
    				List<CmrIndexRequest> pendingRequests = reqTypeGroup.getValue().get(ExecutionStatus.Pending);
    				List<CmrIndexRequest> newRequests = reqTypeGroup.getValue().get(ExecutionStatus.New);
        			
    				// Find the minimum and maximum priorities for the Pending' requests.
    				IntSummaryStatistics pendingPrioritiesStats = computePrioritiesStatistics(pendingRequests);
    				// Iterates over the 'New' requests and conditionally sets them as 'Redundant'.
    				List<CmrIndexRequest> redundantRequests = markNewIndexRequestsAsRedundant(newRequests, pendingPrioritiesStats);
    				overallRedundantRequests.addAll(redundantRequests);
    				logRedundantAndPendingRequests(redundantRequests, pendingRequests);
    			}
    		}
    	}
    	return overallRedundantRequests;
    }
	
    /**
     * Iterates over the requests having status NEW and changes it to REDUNDANT,
     * depending on their priority.
     * @param newRequests the requests having status NEW.
     * @param pendingPrioritiesStats the min/max priorities of the PENDING requests.
     * @return redundantRequests the subset of the NEW requests that were marked as REDUNDANT.
     */
    private List<CmrIndexRequest> markNewIndexRequestsAsRedundant(List<CmrIndexRequest> newRequests, IntSummaryStatistics pendingPrioritiesStats) {
    	List<CmrIndexRequest> redundantRequests = new ArrayList<>();
    	for (CmrIndexRequest newReq : newRequests) {
			if (newReq.getPriority().getPriorityValue() <= pendingPrioritiesStats.getMax()) {
				newReq.setExecutionStatus(ExecutionStatus.Redundant);
				redundantRequests.add(newReq);
			}
		}
    	return redundantRequests;
    }
    
    /**
     * Checks whether all preconditions required for proceeding with the detection
     * of redundant requests are met.
     * @param reqTypeGroup the requests having a given OBJECT_URI and REQUEST_TYPE combination.
     * @return true if all preconditions required for proceeding with the detection of redundant
     * requests for the given OBJECT_URI and REQUEST_TYPE combination are met.
     */
    private boolean isProceedWithDetection(Map.Entry<RequestType, Map<ExecutionStatus, List<CmrIndexRequest>>> reqTypeGroup) {
    	Set<ExecutionStatus> availableExecutionStatus = reqTypeGroup.getValue().keySet();
    	return availableExecutionStatus.contains(ExecutionStatus.New)
    			&& availableExecutionStatus.contains(ExecutionStatus.Pending)
    			&& noRemoveActionInRequests(reqTypeGroup.getValue().values());
    }
    
    /**
     * Computes the min/max values of the priorities of the supplied index requests.
     * @param requests the index requests to process.
     * @return the min/max priority of the supplied index requests.
     */
    private IntSummaryStatistics computePrioritiesStatistics(List<CmrIndexRequest> requests) {
    	return requests
    			.stream()
				.collect(Collectors.summarizingInt(req -> req.getPriority().getPriorityValue()));
    }
    
    /**
     * Checks whether an ACTION of type REMOVE exists among the provided index requests.
     * @param requests the index requests to be processed.
     * @return true if no REMOVE ACTION is detected among the provided index requests.
     */
    private boolean noRemoveActionInRequests(Collection<List<CmrIndexRequest>> requests) {
    	return requests
		    	.stream()
				.flatMap(Collection::stream)
				.noneMatch(req -> req.getAction() == Action.Remove);
    }

    /**
     * Performs a database update for the provided index requests.
     * @param redundantRequests the REDUNDANT index requests.
     */
    private void updateRequests(List<CmrIndexRequest> redundantRequests) {    	
    	redundantRequests.forEach(this.cmrIndexRequestDao::updateObject);
    }
    
    /**
     * Logs the total number of indexing requests whose status changed from NEW
     * to REDUNDANT.
     * @param totalNumber the total number of indexing requests whose status
     * changed from NEW to REDUNDANT.
     */
    private void logNumberOfNewRequestsSetAsRedundant(int totalNumber) {
    	if (totalNumber > 0) {
    		LOG.info("CMR_INDEX_REQUEST: {} 'New' index requests were set to 'Redundant'.", totalNumber);
    	}
    	else {
    		LOG.info("No redundant index requests detected.");
    	}
    }
    
    /**
     * Logs the Redundant index requests of a given OBJECT_URI and REQUEST_TYPE combination,
     * along with their corresponding Pending requests.
     * @param redundantRequests the Redundant index requests.
     * @param pendingRequests the Pending index requests.
     */
    private void logRedundantAndPendingRequests(List<CmrIndexRequest> redundantRequests, List<CmrIndexRequest> pendingRequests) {
    	if (!redundantRequests.isEmpty()) {
    		StringBuilder sb = new StringBuilder();
    		sb.append("The following New index requests were changed to Redundant:\t");
    		redundantRequests.forEach(r -> appendRequestLog(sb, r));
    		sb.append("Caused by the following Pending requests:\t");
    		pendingRequests.forEach(r -> appendRequestLog(sb, r));
    		LOG.info(sb);
    	}
    }
    
    /**
     * Appends the provided index request info to the supplied string builder.
     * @param sb the string builder.
     * @param r the index request.
     */
    private void appendRequestLog(StringBuilder sb, CmrIndexRequest r) {
    	sb.append("\t")
    		.append(String.format(REQUEST_LOG_PLACEHOLDER,
				r.getId(), r.getObjectUri(), r.getRequestType().getValue(), r.getAction(),
				r.getPriority().getPriorityValue(), r.getExecutionStatus().getValue()))
			.append("\t");
    }
    
}
