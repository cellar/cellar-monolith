package eu.europa.ec.opoce.cellar.server.admin.nal;

import eu.europa.ec.opoce.cellar.cmr.database.dao.OntoConfigDao;
import eu.europa.ec.opoce.cellar.ontology.dao.OntoConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Date;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class OntoAdminConfigurationService {

    private final Function<OntoConfig, NalOntoVersion> ontoConfig2ActiveVersion = ontoConfig -> {
        NalOntoVersion ontology = new NalOntoVersion();
        ontology.setName(ontoConfig.getName());
        ontology.setUri(ontoConfig.getUri());
        ontology.setVersionNumber(ontoConfig.getVersionNr());
        ontology.setVersionDate(ontoConfig.getVersionDate() != null ? new Date(ontoConfig.getVersionDate().getTime()) : null);
        ontology.setExternalPid(ontoConfig.getExternalPid());
        return ontology;
    };

    private final OntoConfigDao ontoConfigDao;

    @Autowired
    public OntoAdminConfigurationService(OntoConfigDao ontoConfigDao) {
        this.ontoConfigDao = ontoConfigDao;
    }

    public Set<NalOntoVersion> getOntologyVersions() {
        return ontoConfigDao.findAll().stream()
                .map(ontoConfig2ActiveVersion)
                .collect(Collectors.toSet());
    }

    public Collection<GroupedNalOntoVersion> getGroupedOntologyVersions() {
        return getOntologyVersions().stream()
                .map(n -> new GroupedNalOntoVersion(n.getName(), n.getUri(), n))
                .collect(Collectors.toList());
    }
}
