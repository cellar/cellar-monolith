package eu.europa.ec.opoce.cellar.server.dissemination;

/**
 * <p>RequestType class.</p>
 */
public enum RequestType {

    TREE_NOTICE() {

        @Override
        public <IN, OUT> OUT accept(final RequestTypeVisitor<IN, OUT> visitor, final IN in) {
            return visitor.visitTree(in);
        }
    },

    BRANCH_NOTICE() {

        @Override
        public <IN, OUT> OUT accept(final RequestTypeVisitor<IN, OUT> visitor, final IN in) {
            return visitor.visitBranch(in);
        }
    },

    OBJECT_NOTICE() {

        @Override
        public <IN, OUT> OUT accept(final RequestTypeVisitor<IN, OUT> visitor, final IN in) {
            return visitor.visitObject(in);
        }
    },

    IDENTIFIER_NOTICE() {

        @Override
        public <IN, OUT> OUT accept(final RequestTypeVisitor<IN, OUT> visitor, final IN in) {
            return visitor.visitIdentifier(in);
        }
    },

    RDF_XML_OBJECT() {

        @Override
        public <IN, OUT> OUT accept(final RequestTypeVisitor<IN, OUT> visitor, final IN in) {
            return visitor.visitRdfObject(in);
        }
    },
    RDF_XML_OBJECT_NORMALIZED() {

        @Override
        public <IN, OUT> OUT accept(final RequestTypeVisitor<IN, OUT> visitor, final IN in) {
            return visitor.visitRdfObjectNormalized(in);
        }
    },

    RDF_XML_TREE() {

        @Override
        public <IN, OUT> OUT accept(final RequestTypeVisitor<IN, OUT> visitor, final IN in) {
            return visitor.visitRdfTree(in);
        }
    },
    RDF_XML_TREE_NORMALIZED() {

        @Override
        public <IN, OUT> OUT accept(final RequestTypeVisitor<IN, OUT> visitor, final IN in) {
            return visitor.visitRdfTreeNormalized(in);
        }
    },

    RDF_XML_NONINFERRED_OBJECT() {

        @Override
        public <IN, OUT> OUT accept(final RequestTypeVisitor<IN, OUT> visitor, final IN in) {
            return visitor.visitRdfNonInferredObject(in);
        }
    },
    RDF_XML_NONINFERRED_OBJECT_NORMALIZED() {

        @Override
        public <IN, OUT> OUT accept(final RequestTypeVisitor<IN, OUT> visitor, final IN in) {
            return visitor.visitRdfNonInferredObjectNormalized(in);
        }
    },

    RDF_XML_NONINFERRED_TREE() {

        @Override
        public <IN, OUT> OUT accept(final RequestTypeVisitor<IN, OUT> visitor, final IN in) {
            return visitor.visitRdfNonInferredTree(in);
        }
    },

    RDF_XML_NONINFERRED_TREE_NORMALIZED() {

        @Override
        public <IN, OUT> OUT accept(final RequestTypeVisitor<IN, OUT> visitor, final IN in) {
            return visitor.visitRdfNonInferredTreeNormalized(in);
        }
    },

    RDF_CONTENT() {

        @Override
        public <IN, OUT> OUT accept(final RequestTypeVisitor<IN, OUT> visitor, final IN in) {
            return visitor.visitRdfContent(in);
        }

    },

    CONTENT() {

        @Override
        public <IN, OUT> OUT accept(final RequestTypeVisitor<IN, OUT> visitor, final IN in) {
            return visitor.visitContent(in);
        }
    },

    CONCEPT_SCHEME() {

        @Override
        public <IN, OUT> OUT accept(final RequestTypeVisitor<IN, OUT> visitor, final IN in) {
            return visitor.visitConceptScheme(in);
        }
    },

    ZIP() {

        @Override
        public <IN, OUT> OUT accept(final RequestTypeVisitor<IN, OUT> visitor, final IN in) {
            return visitor.visitZip(in);
        }
    };

    /**
     * <p>accept.</p>
     *
     * @param visitor a {@link eu.europa.ec.opoce.cellar.server.dissemination.RequestTypeVisitor} object.
     * @param in      a IN object.
     * @param <IN>    a IN object.
     * @param <OUT>   a OUT object.
     * @return a OUT object.
     */
    public abstract <IN, OUT> OUT accept(RequestTypeVisitor<IN, OUT> visitor, IN in);
}
