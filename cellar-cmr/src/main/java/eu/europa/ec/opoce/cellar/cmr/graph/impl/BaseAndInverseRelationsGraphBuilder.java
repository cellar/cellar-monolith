/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.graph.impl
 *             FILE : IndexingGraphBuilder.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 5, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.graph.impl;

import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;

import java.util.List;

import org.springframework.beans.factory.annotation.Configurable;

/**
 * <class_description> Graph builder implementation for indexing and embedding.
 * <br/><br/>
 * ON : Jun 5, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Configurable
public class BaseAndInverseRelationsGraphBuilder extends GraphBuilder {

    /**
     * The cellar identifier of the root element to index.
     */
    private String rootCellarId;

    /**
     * Add the root cellar id.
     * @param rootCellarId the cellar id of the root element concerned by indexing/embedding
     * @return the graph builder
     */
    public GraphBuilder withRootCellarId(final String rootCellarId) {
        this.rootCellarId = rootCellarId;
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GraphBuilder buildBase() {
        final Identifier identifier = new Identifier(this.rootCellarId);
        this.getGraph().addObject(identifier);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GraphBuilder buildRelations() {
        final Identifier root = this.getGraph().getRoot();

        final List<String> pids = this.identifierService.getAllPIDIfCellarIdExists(root);
        root.setPids(pids);

        return this.buildInverseRelationsOnSource().buildInverseRelationsOnTarget();
    }

}
