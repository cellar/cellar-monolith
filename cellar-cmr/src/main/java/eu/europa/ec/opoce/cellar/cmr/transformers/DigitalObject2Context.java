package eu.europa.ec.opoce.cellar.cmr.transformers;

import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;

import org.apache.commons.collections15.Transformer;

/**
 * <p>DigitalObject2Context class.</p>
 */
public class DigitalObject2Context implements Transformer<DigitalObject, String> {

    /**
     * Constant <code>instance</code>
     */
    public static Transformer<DigitalObject, String> instance = new DigitalObject2Context();

    /**
     * {@inheritDoc}
     */
    @Override
    public String transform(DigitalObject input) {
        return input.getCellarId().getIdentifier();
    }
}
