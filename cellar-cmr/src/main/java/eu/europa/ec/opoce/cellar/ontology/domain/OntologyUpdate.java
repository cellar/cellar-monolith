/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ontology
 *             FILE : OntologyUpdate.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 01 12, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-01-12 11:00:07 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ontology.domain;

import com.google.common.base.MoreObjects;
import org.apache.jena.rdf.model.Model;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author ARHS Developments
 */
public class OntologyUpdate {

    private final String ontologyUri;
    private final Map<String, Model> updates = new HashMap<>();

    public OntologyUpdate(String ontologyUri) {
        this.ontologyUri = ontologyUri;
    }

    public String getOntologyUri() {
        return ontologyUri;
    }

    public void add(String uri, Model content) {
        updates.put(uri, content);
    }

    public Map<String, Model> getUpdates() {
        return updates;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OntologyUpdate)) return false;
        OntologyUpdate that = (OntologyUpdate) o;
        return Objects.equals(getOntologyUri(), that.getOntologyUri()) &&
                Objects.equals(getUpdates(), that.getUpdates());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOntologyUri(), getUpdates());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("ontologyUri", ontologyUri)
                // Avoid toString() model -> already close issue !
                .add("updates", updates.keySet())
                .toString();
    }
}
