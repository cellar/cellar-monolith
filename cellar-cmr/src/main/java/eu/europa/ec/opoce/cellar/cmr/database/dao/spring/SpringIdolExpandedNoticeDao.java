package eu.europa.ec.opoce.cellar.cmr.database.dao.spring;

import eu.europa.ec.opoce.cellar.cmr.database.dao.IdolExpandedNoticeDao;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.IndexNotice;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.service.LanguageService;
import eu.europa.ec.opoce.cellar.semantic.helper.StringHelper;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

@Repository
public class SpringIdolExpandedNoticeDao extends IdolSpringBaseDao<IndexNotice, Long> implements IdolExpandedNoticeDao {

    /**
     * Constant <code>table="TB_CELLAR_MODIFICATION"</code>
     */
    private static final String table = "TB_CELLAR_MODIFICATION";
    /**
     * Constant <code>getNoticeId="StringHelper.format({} = :cellarNoticeI"{trunked}</code>
     */
    private static final String getNoticeId = StringHelper.format("{} = :cellarNoticeId ", Column.CRM_CELLAR_URI_NM);
    @Autowired
    private LanguageService languageService;

    private interface Column {

        String CRM_ID = "CRM_ID";
        String CRM_OPERATION_CD = "CRM_OPERATION_CD";
        String CRM_CELLAR_URI_NM = "CRM_CELLAR_URI_NM";
        String CRM_METADATA_LOB = "CRM_METADATA_LOB";
        String CRM_CONTENT_URLS_LOB = "CRM_CONTENT_URLS_LOB";
        String CRM_PRIORITY_NO = "CRM_PRIORITY_NO";
        String CRM_LANGUAGE_ISO_CODE_NM = "CRM_LANGUAGE_ISO_CODE_NM";
        String CRM_CREATED_ON = "CRM_CREATED_ON";
    }

    /**
     * <p>Constructor for SpringIdolExpandedNoticeDao.</p>
     */
    public SpringIdolExpandedNoticeDao() {
        super(table, Column.CRM_ID, Arrays.asList(Column.CRM_OPERATION_CD, Column.CRM_CELLAR_URI_NM, Column.CRM_METADATA_LOB,
                Column.CRM_CONTENT_URLS_LOB, Column.CRM_PRIORITY_NO, Column.CRM_LANGUAGE_ISO_CODE_NM, Column.CRM_CREATED_ON));
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("serial")
    @Override
    public IndexNotice findNotice(final String noticeId) {
        if (StringUtils.isBlank(noticeId)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument status cannot be null").build();
        }

        return findUnique(getNoticeId, new HashMap<String, Object>(1) {

            {
                put("cellarNoticeId", noticeId);
            }
        });
    }

    @Override
    public IndexNotice create(final ResultSet rs) throws SQLException {
        return new IndexNotice() {

            {

                setOperationType(rs.getString(Column.CRM_OPERATION_CD));
                setCellarUri(rs.getString(Column.CRM_CELLAR_URI_NM));
                setMetadata(rs.getString(Column.CRM_METADATA_LOB));
                setContentUrls(rs.getString(Column.CRM_CONTENT_URLS_LOB));
                setLastModificationDate(rs.getTimestamp(Column.CRM_CREATED_ON));
                setPriority(rs.getInt(Column.CRM_PRIORITY_NO));
                setIsoCode(languageService.getByTwoChar(rs.getString(Column.CRM_LANGUAGE_ISO_CODE_NM)));
            }
        };
    }

    @Override
    public void fillMap(IndexNotice indexNotice, Map<String, Object> map) {
        map.put(Column.CRM_OPERATION_CD, indexNotice.getOperationType());
        map.put(Column.CRM_CELLAR_URI_NM, indexNotice.getCellarUri());
        map.put(Column.CRM_METADATA_LOB, indexNotice.getMetadata());
        map.put(Column.CRM_CONTENT_URLS_LOB, indexNotice.getContentUrls());
        map.put(Column.CRM_CREATED_ON, indexNotice.getLastModificationDate());
        map.put(Column.CRM_PRIORITY_NO, indexNotice.getPriority());
        map.put(Column.CRM_LANGUAGE_ISO_CODE_NM, indexNotice.getIsoCode().getIsoCodeTwoChar());
    }
}
