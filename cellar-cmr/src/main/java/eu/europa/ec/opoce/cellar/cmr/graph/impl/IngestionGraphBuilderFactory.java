/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.graph.impl
 *             FILE : IngestionGraphBuilderFactory.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 27-01-2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.graph.impl;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.common.factory.IFactory;
import eu.europa.ec.opoce.cellar.domain.content.mets.OperationSubType;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

import org.springframework.stereotype.Component;

/**
 * <class_description> Factory class that creates new instances of {@link eu.europa.ec.opoce.cellar.cmr.graph.impl.IngestionGraphBuilder}
 * on the basis of the current operation's subtype.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 27-01-2015
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
@Component("ingestionGraphBuilderFactory")
public class IngestionGraphBuilderFactory implements IFactory<IngestionGraphBuilder, OperationSubType> {

    /**
     * @see eu.europa.ec.opoce.cellar.common.factory.IFactory#create(java.lang.Object)
     */
    @Override
    public IngestionGraphBuilder create(final OperationSubType opSubType) {
        IngestionGraphBuilder graphBuilder = null;

        switch (opSubType) {
        case Read:
            graphBuilder = new ReadGraphBuilder();
            break;
        case Create:
            graphBuilder = new CreateGraphBuilder();
            break;
        case Append:
            graphBuilder = new AppendGraphBuilder();
            break;
        case Update:
        case Merge:
            graphBuilder = new UpdateGraphBuilder();
            break;
        case Delete:
            graphBuilder = new DeleteGraphBuilder();
            break;
        default:
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.UNKNOWN_METS_OPERATION)
                    .withMessage("Cannot resolve the strategy for locking the StructMap with OperationSubType '{}'.")
                    .withMessageArgs(opSubType).build();
        }

        return graphBuilder;
    }
}
