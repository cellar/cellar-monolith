package eu.europa.ec.opoce.cellar.nal.service.impl;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration;
import eu.europa.ec.opoce.cellar.common.closure.RPClosure;
import eu.europa.ec.opoce.cellar.common.util.MapUtils;
import eu.europa.ec.opoce.cellar.database.Database;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.NalOntoUtils;
import eu.europa.ec.opoce.cellar.nal.domain.NalRelatedBean;
import eu.europa.ec.opoce.cellar.nal.domain.NalSnippetBean;
import eu.europa.ec.opoce.cellar.nal.service.NalSnippetDbGateway;
import eu.europa.ec.opoce.cellar.nal.utils.InClauseUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.RowCallbackHandler;
import org.springframework.jdbc.support.lob.LobCreator;
import org.springframework.jdbc.support.lob.LobHandler;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <p>NalSnippetDbGateway class.</p>
 */
@Service
public class NalSnippetDbGatewayImpl implements NalSnippetDbGateway {

    private static final Logger LOG = LogManager.getLogger(NalSnippetDbGatewayImpl.class);

    private static final String EUROVOC_KEYWORD = "eurovoc";
    private static final String EUROVOC_CONCEPT_SCHEME_URI = "http://eurovoc.europa.eu";

    private static final String CLOSING_BRACKET = ")";
    private static final String INSERT = "INSERT INTO NAL_SNIPPET (RESOURCE_URI, CONCEPTSCHEME_URI, RDFSNIPPET) VALUES(?,?,?)";
    private static final String DELETE_NAL_SNIPPET = "DELETE FROM NAL_SNIPPET WHERE CONCEPTSCHEME_URI = ?";
    private static final String DELETE_ALL_NAL_SNIPPET = "DELETE FROM NAL_SNIPPET";
    private static final String INSERT_RELATIONS = "INSERT INTO RELATED_SNIPPET (RESOURCE_URI, RELATED_RESOURCE_URI) VALUES(?,?)";
    private static final String DELETE_RELATED_SNIPPET = "DELETE FROM RELATED_SNIPPET WHERE RELATED_RESOURCE_URI like ?";
    private static final String DELETE_ALL_RELATED_SNIPPET = "DELETE FROM RELATED_SNIPPET";
    private static final String SELECT_SNIPPETS = "SELECT RESOURCE_URI, CONCEPTSCHEME_URI, RDFSNIPPET FROM NAL_SNIPPET WHERE RESOURCE_URI IN (";
    private static final String SELECT_EXISTING_CONCEPTS = "SELECT RESOURCE_URI FROM NAL_SNIPPET WHERE RESOURCE_URI IN (";
    private static final String SELECT_SNIPPETS_INCL_RELATED = "SELECT RESOURCE_URI, CONCEPTSCHEME_URI, RDFSNIPPET, RELATED_RESOURCE_URI, RELATED_CONCEPTSCHEME_URI, RELATED_RDFSNIPPET "
            + "FROM RELATED_NAL_SNIPPET WHERE RESOURCE_URI IN (";

    // only used to know if we have the minimal setup in NAL_SNIPPET to ingest a package
    // if there is no record, we need to execute the init NAL script that adds dummy records in the DB
    // afterwards, the real NAL packages MUST be ingested
    // we have this scenario when all NAL have been deleted from the system and we need to allow CELLAR to boot
    // with a minimal config that allows the loading of NAL to fully set it up
    private static final String SELECT_NAL_SNIPPET_OF_CONCEPTSCHEME_LANGUAGE =
            "SELECT RESOURCE_URI FROM NAL_SNIPPET WHERE CONCEPTSCHEME_URI = '" + NalOntoUtils.NAL_LANGUAGE_URI + "'";
    private static final String NAL_SNIPPET_INIT_SCRIPT = "init_nal_snippet.csv";

    private Database database;
    private LobHandler lobHandler;
    private int batchSize = 200;

    @Autowired
    public NalSnippetDbGatewayImpl(@Qualifier("databaseCellarDataCmr") Database database, LobHandler lobHandler) {
        this.database = database;
        this.lobHandler = lobHandler;
    }

    @PostConstruct
    public void initNalSnippet() {
        List<String> resourceUriForNalLanguage = new LinkedList<>();
        RowCallbackHandler callbackHandler = rs -> resourceUriForNalLanguage.add(rs.getString(1));
        getJdbcOperations().query(SELECT_NAL_SNIPPET_OF_CONCEPTSCHEME_LANGUAGE, callbackHandler);
        if (resourceUriForNalLanguage.isEmpty()) {
            LOG.error("" +
                    "The NAL language '{}' was not previously loaded. CELLAR will start booting with dummy records " +
                    "in NAL_SNIPPET table for the missing NAL. The next action must be to ingest the missing NAL language to have CELLAR in " +
                    "a valid state. Do not ingest other METS package until NAL language has been successfully ingested.", NalOntoUtils.NAL_LANGUAGE_URI);
            try {
                URL url = NalSnippetDbGatewayImpl.class.getResource(NAL_SNIPPET_INIT_SCRIPT);
                String content = Resources.toString(url, Charsets.UTF_8);
                String[] lines = content.split("\n");
                if (lines.length == 0) {
                    LOG.error("The file '{}' containing the dummy records for the missing NAL language cannot be empty.",
                            NAL_SNIPPET_INIT_SCRIPT);
                } else {
                    List<NalSnippetBean> nalSnippetBeans = new ArrayList<>(lines.length);
                    for (String line : lines) {
                        String[] csvLine = line.split(",");
                        String resourceUri = csvLine[0];
                        String conceptSchemeUri = csvLine[1];
                        String rdfSnippet = csvLine[2];

                        nalSnippetBeans.add(new NalSnippetBean(resourceUri, conceptSchemeUri, rdfSnippet.getBytes(StandardCharsets.UTF_8)));
                    }
                    LOG.warn("Inserting '{}' dummy records due to missing NAL language!", nalSnippetBeans.size());
                    insertSnippetsAsBatch(nalSnippetBeans);
                    LOG.warn("Finished inserting '{}' dummy records due to missing NAL language!", nalSnippetBeans.size());
                }
            } catch (IOException ioe) {
                LOG.error("Cannot read '{}' to retrieve the dummy records for the missing NAL language : {}",
                        NAL_SNIPPET_INIT_SCRIPT, ioe.getMessage(), ioe);
            }
        }
    }


    /**
     * <p>Setter for the field <code>batchSize</code>.</p>
     *
     * @param batchSize a int.
     */
    @Override
    public void setBatchSize(int batchSize) {
        if (batchSize > 0)
            this.batchSize = batchSize;
    }

    /**
     * <p>selectSnippets.</p>
     *
     * @param concepts a {@link java.util.Collection} object.
     * @return a {@link java.util.Map} object.
     */
    @Override
    public Map<String, NalSnippetBean> selectSnippets(Collection<String> concepts) {
        concepts = new HashSet<>(concepts);

        final Map<String, NalSnippetBean> nalSnippetPerConcept = new HashMap<>(concepts.size());
        if (concepts.isEmpty()) {
            return nalSnippetPerConcept;
        }

        RowCallbackHandler callbackHandler = rs -> {
            String concept = rs.getString(1);
            String conceptScheme = rs.getString(2);
            String rdfSnippet = lobHandler.getClobAsString(rs, 3);

            if (nalSnippetPerConcept.containsKey(concept)) {
                throw ExceptionBuilder.get(CellarException.class)
                        .withMessage("More than one record found in NAL_SNIPPET table for concept '{}'").withMessageArgs(concept)
                        .build();
            }

            nalSnippetPerConcept.put(concept, new NalSnippetBean(concept, conceptScheme, getStringBytes(rdfSnippet)));
        };

        List<List<String>> conceptBlocks = InClauseUtils.getIdsPerInClause(concepts);
        for (List<String> conceptBlock : conceptBlocks) {
            getJdbcOperations().query(generateSelect(conceptBlock.size()), prepareInClauseStatement(conceptBlock), callbackHandler);
        }

        return nalSnippetPerConcept;
    }

    /**
     * <p>selectSnippetsInclRelated.</p>
     *
     * @param concepts a {@link java.util.Collection} object.
     * @return a {@link java.util.Map} object.
     */
    @Override
    public Map<String, NalSnippetBean> selectSnippetsInclRelated(Collection<String> concepts) {
        concepts = new HashSet<>(concepts);

        final Map<String, NalSnippetBean> nalSnippetPerConcept = new HashMap<>(concepts.size());
        if (concepts.isEmpty()) {
            return nalSnippetPerConcept;
        }

        final Map<String, NalSnippetBean> nalSnippetPerRelatedConcept = new HashMap<>();
        final Map<String, Set<String>> relatedConceptsPerConcept = new HashMap<>();

        RowCallbackHandler callbackHandler = rs -> {
            String concept = rs.getString(1);
            String conceptScheme = rs.getString(2);

            if (nalSnippetPerConcept.get(concept) == null) {
                String rdfSnippet = lobHandler.getClobAsString(rs, 3);
                nalSnippetPerConcept.put(concept, new NalSnippetBean(concept, conceptScheme, getStringBytes(rdfSnippet)));
            }

            String relatedConcept = rs.getString(4);
            if (StringUtils.isBlank(relatedConcept)) {
                return;
            }

            MapUtils.addValueToMappedSet(relatedConceptsPerConcept, concept, relatedConcept);
            if (!nalSnippetPerRelatedConcept.containsKey(relatedConcept)) {
                String relatedSnippet = lobHandler.getClobAsString(rs, 6);
                nalSnippetPerRelatedConcept.put(relatedConcept,
                        new NalSnippetBean(relatedConcept, rs.getString(5), getStringBytes(relatedSnippet)));
            }
        };

        List<List<String>> conceptBlocks = InClauseUtils.getIdsPerInClause(concepts);
        for (List<String> conceptBlock : conceptBlocks) {
            getJdbcOperations().query(generateSelectInclRelated(conceptBlock.size()), prepareInClauseStatement(conceptBlock),
                    callbackHandler);
        }

        for (Map.Entry<String, NalSnippetBean> snippet : nalSnippetPerConcept.entrySet()) {
            List<NalSnippetBean> related = new ArrayList<>();
            Set<String> relatedConcepts = relatedConceptsPerConcept.get(snippet.getKey());
            if (relatedConcepts != null) {
                for (String relatedConcept : relatedConcepts) {
                    related.add(nalSnippetPerRelatedConcept.get(relatedConcept));
                }
            }
            snippet.getValue().setRelatedSnippets(related);
        }

        return nalSnippetPerConcept;
    }

    /**
     * <p>selectExistingConcepts.</p>
     *
     * @param concepts a {@link java.util.Collection} object.
     * @return a {@link java.util.Set} object.
     */
    @Override
    public Set<String> selectExistingConcepts(Collection<String> concepts) {
        final Set<String> existingConcepts = new HashSet<>(concepts.size());
        if (concepts.isEmpty()) {
            return existingConcepts;
        }

        RowCallbackHandler callbackHandler = rs -> existingConcepts.add(rs.getString(1));

        List<List<String>> conceptBlocks = InClauseUtils.getIdsPerInClause(concepts);
        for (List<String> conceptBlock : conceptBlocks) {
            getJdbcOperations().query(generateSelectExistingConcepts(conceptBlock.size()), prepareInClauseStatement(conceptBlock),
                    callbackHandler);
        }

        return existingConcepts;
    }

    /**
     * <p>insertSnippets.</p>
     *
     * @param nalSnippetDOs a {@link java.util.List} object.
     */
    @Override
    public void insertSnippets(List<NalSnippetBean> nalSnippetDOs) {
        if (!nalSnippetDOs.isEmpty()) {
            LOG.info("Insert {} snippets into NAL_SNIPPET", nalSnippetDOs.size());
            batchInsert(nalSnippetDOs, this::insertSnippetsAsBatch);
        }
    }

    /**
     * <p>insertRelations.</p>
     *
     * @param relatedSnippetDOs a {@link java.util.List} object.
     */
    @Override
    public void insertRelations(List<NalRelatedBean> relatedSnippetDOs) {
        if (!relatedSnippetDOs.isEmpty()) {
            LOG.info("Insert {} relations into RELATED_SNIPPET", relatedSnippetDOs.size());
            batchInsert(relatedSnippetDOs, this::insertRelationsAsBatch);
        }
    }

    /**
     * <p>batchInsert.</p>
     *
     * @param recordDOs a {@link java.util.List} object.
     * @param closure   a {@link RPClosure} object.
     */
    private <T> void batchInsert(List<T> recordDOs, RPClosure<int[], List<T>> closure) {
        for (List<T> batch : prepareBatches(recordDOs)) {
            LOG.debug(IConfiguration.NAL, "Inserting batch (size {})...", batch.size());
            closure.call(batch);
            LOG.debug(IConfiguration.NAL,"batch inserted.", batch.size());
        }
    }

    /**
     * <p>insertSnippetsAsBatch.</p>
     *
     * @param nalSnippetDOs a {@link java.util.List} object.
     * @return an array of int.
     */
    private int[] insertSnippetsAsBatch(final List<NalSnippetBean> nalSnippetDOs) {
        final LobCreator lobCreator = lobHandler.getLobCreator();

        BatchPreparedStatementSetter pss = new BatchPreparedStatementSetter() {

            public void setValues(PreparedStatement ps, int i) throws SQLException {
                NalSnippetBean nalSnippetDO = nalSnippetDOs.get(i);

                ps.setString(1, nalSnippetDO.getConcept());
                ps.setString(2, nalSnippetDO.getConceptScheme());

                lobCreator.setClobAsCharacterStream(ps, 3,
                        new InputStreamReader(nalSnippetDO.getRdfSnippet(), StandardCharsets.UTF_8),
                        -1);
            }

            public int getBatchSize() {
                return nalSnippetDOs.size();
            }
        };

        lobCreator.close();

        return getJdbcOperations().batchUpdate(INSERT, pss);
    }

    /**
     * <p>insertRelationsAsBatch.</p>
     *
     * @param relatedSnippetDOs a {@link java.util.List} object.
     * @return an array of int.
     */
    private int[] insertRelationsAsBatch(final List<NalRelatedBean> relatedSnippetDOs) {
        BatchPreparedStatementSetter pss = new BatchPreparedStatementSetter() {

            public void setValues(PreparedStatement ps, int i) throws SQLException {
                NalRelatedBean nalSnippetDO = relatedSnippetDOs.get(i);

                ps.setString(1, nalSnippetDO.getUri());
                ps.setString(2, nalSnippetDO.getRelatedUri());
            }

            public int getBatchSize() {
                return relatedSnippetDOs.size();
            }
        };

        return getJdbcOperations().batchUpdate(INSERT_RELATIONS, pss);
    }

    /**
     * <p>prepareBatches.</p>
     *
     * @param records a {@link java.util.List} object.
     * @return a {@link java.util.List} object.
     */
    private <T> List<List<T>> prepareBatches(final List<T> records) {
        if (batchSize <= 0) {
            return new ArrayList<>();
        }

        int size = records.size();

        int count = size / batchSize;
        int modulus = size % batchSize;

        List<List<T>> batches = new ArrayList<>(count + (modulus > 0 ? 1 : 0));
        for (int i = 0; i < count; i++) {
            batches.add(records.subList(i * batchSize, (i + 1) * batchSize));
        }

        if (modulus > 0) {
            int from = count * batchSize;
            batches.add(records.subList(from, from + modulus));
        }

        return batches;
    }

    /**
     * <p>getStringBytes.</p>
     *
     * @param string a {@link java.lang.String} object.
     * @return an array of byte.
     */
    private byte[] getStringBytes(String string) {
        return string != null ? string.getBytes(StandardCharsets.UTF_8) : new byte[]{};
    }

    /**
     * <p>prepareInClauseStatement.</p>
     *
     * @param concepts a {@link java.util.List} object.
     * @return a {@link org.springframework.jdbc.core.PreparedStatementSetter} object.
     */
    private PreparedStatementSetter prepareInClauseStatement(final List<String> concepts) {
        return ps -> {
            for (int i = 0; i < concepts.size(); i++) {
                ps.setString(i + 1, concepts.get(i));
            }
        };
    }

    /**
     * <p>generateSelect.</p>
     *
     * @param numberOfParams a int.
     * @return a {@link java.lang.String} object.
     */
    private String generateSelect(int numberOfParams) {
        return SELECT_SNIPPETS + StringUtils.repeat("?", ",", numberOfParams) + CLOSING_BRACKET;
    }

    /**
     * <p>generateSelectInclRelated.</p>
     *
     * @param numberOfParams a int.
     * @return a {@link java.lang.String} object.
     */
    private String generateSelectInclRelated(int numberOfParams) {
        return SELECT_SNIPPETS_INCL_RELATED + StringUtils.repeat("?", ",", numberOfParams) + CLOSING_BRACKET;
    }

    /**
     * <p>generateSelectExistingConcepts.</p>
     *
     * @param numberOfParams a int.
     * @return a {@link java.lang.String} object.
     */
    private String generateSelectExistingConcepts(int numberOfParams) {
        return SELECT_EXISTING_CONCEPTS + StringUtils.repeat("?", ",", numberOfParams) + CLOSING_BRACKET;
    }

    /**
     * <p>getJdbcOperations.</p>
     *
     * @return a {@link org.springframework.jdbc.core.JdbcOperations} object.
     */
    private JdbcOperations getJdbcOperations() {
        return database.getJdbcOperations();
    }

    @Override
    public int deleteNalSnippetOfConceptScheme(String conceptSchemeUri) {
        PreparedStatementSetter pss = ps -> ps.setString(1, conceptSchemeUri);
        return getJdbcOperations().update(DELETE_NAL_SNIPPET, pss);
    }

    @Override
    public int deleteAllNalSnippet() {
        return getJdbcOperations().update(DELETE_ALL_NAL_SNIPPET);
    }

    @Override
    public int deleteRelatedSnippetOfRelatedResourceUri(String conceptSchemeUri) {
        PreparedStatementSetter pss = ps -> ps.setString(1, getConceptSchemeUriPattern(conceptSchemeUri));
        return getJdbcOperations().update(DELETE_RELATED_SNIPPET, pss);
    }

    String getConceptSchemeUriPattern(String conceptSchemeUri) {

        if(conceptSchemeUri.contains(EUROVOC_KEYWORD)) {
            return EUROVOC_CONCEPT_SCHEME_URI + "%";
        }
        return conceptSchemeUri + "%";
    }

    @Override
    public int deleteAllRelatedSnippet() {
        return getJdbcOperations().update(DELETE_ALL_RELATED_SNIPPET);
    }
}
