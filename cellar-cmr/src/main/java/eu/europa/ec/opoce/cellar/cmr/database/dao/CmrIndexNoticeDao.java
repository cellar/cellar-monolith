package eu.europa.ec.opoce.cellar.cmr.database.dao;

import eu.europa.ec.opoce.cellar.cl.dao.BaseDao;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.IndexNotice;

/**
 * <p>CmrIndexNoticeDao interface.</p>
 *
 * @author Pieter.Fannes
 */
public interface CmrIndexNoticeDao extends BaseDao<IndexNotice, Long> {

    /**
     * <p>findNotice.</p>
     *
     * @param noticeId a {@link java.lang.String} object.
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.notice.indexing.IndexNotice} object.
     */
    IndexNotice findNotice(final String noticeId);
}
