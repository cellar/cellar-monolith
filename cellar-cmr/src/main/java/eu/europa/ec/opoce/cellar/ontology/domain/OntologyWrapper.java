/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr
 *             FILE : OntologyWrapper.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 26 août 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ontology.domain;

import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.configuration.OntologyConfiguration;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.Ontology;
import eu.europa.ec.opoce.cellar.semantic.OntologyUri;
import eu.europa.ec.opoce.cellar.semantic.UriResolver;
import eu.europa.ec.opoce.cellar.semantic.inferredontology.InferredOntologyConfiguration;
import eu.europa.ec.opoce.cellar.semantic.ontology.cache.OntologyBuilder;
import eu.europa.ec.opoce.cellar.semantic.ontology.data.FallbackOntologyData;
import eu.europa.ec.opoce.cellar.semantic.ontology.data.OntologyData;
import eu.europa.ec.opoce.cellar.semantic.validation.OntologyUriValidation;
import eu.europa.ec.opoce.cellar.semantic.validation.ValidationResults;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.apache.jena.vocabulary.XSD;

import java.util.Collections;
import java.util.EnumSet;
import java.util.Set;

import static eu.europa.ec.opoce.cellar.common.Namespace.cdm;
import static eu.europa.ec.opoce.cellar.common.Namespace.cmr;
import static eu.europa.ec.opoce.cellar.common.Namespace.publications_ontology;
import static eu.europa.ec.opoce.cellar.common.Namespace.skos;
import static eu.europa.ec.opoce.cellar.common.Namespace.skosUri;
import static eu.europa.ec.opoce.cellar.common.Namespace.skosXl;
import static eu.europa.ec.opoce.cellar.common.Namespace.skosXlUri;

/**
 * @author ARHS Developments
 * @version $Revision$
 */
public class OntologyWrapper {

    private static final Set<OntologyUri> defaultAdditionalOntologyUris = EnumSet.of(OntologyUri.rdf, OntologyUri.rdfs, OntologyUri.owl);

    private static final String[] ALLOWED_URIS = new String[]{
            "http://www.w3.org/2004/02/skos/core#",
            "http://www.w3.org/2008/05/skos-xl#",
            "http://publications.europa.eu/ontology/annotation#",
            "http://publications.europa.eu/ontology/cdm#",
            "http://publications.europa.eu/ontology/cdm/cmr#",
            "http://publications.europa.eu/ontology/datatype#",
            "http://publications.europa.eu/ontology/tdm#",
            "http://publications.europa.eu/ontology/cdm/derived_class#",
            "http://publications.europa.eu/ontology/cdm/import#",
            "http://publications.europa.eu/ontology/cdm/indexation#",
            "http://publications.europa.eu/ontology/authority",
            "http://publications.europa.eu/ontology/authority/op-code",
            "http://publications.europa.eu/ontology/dorie#",
            "http://www.w3.org/2001/XMLSchema#",
            "http://purl.org/dc/terms/",
            "http://vocab.deri.ie/void#",
            "http://www.w3.org/ns/adms#",
            "http://publications.europa.eu/resource/authority/op-code",
            "http://publications.europa.eu/resource/authority",
            "http://purl.org/dc/elements/1.1/",
            "http://www.w3.org/ns/dcat#",
            "http://purl.org/dc/dcmitype/",
            "http://publications.europa.eu/ontology/euvoc#",
            "http://xmlns.com/foaf/0.1/",
            "http://www.w3.org/2003/01/geo/wgs84_pos#",
            "http://purl.org/goodrelations/v1#",
            "http://labs.mondeca.com/vocab/voaf#",
            "http://purl.org/dc/dcam/",
            "http://lemon-model.net/lemon#",
            "http://www.lexinfo.net/ontology/2.0/lexinfo#",
            "http://lexvo.org/ontology#",
            "http://www.w3.org/ns/org#",
            "http://www.w3.org/2002/07/owl#",
            "http://www.w3.org/2006/12/owl2-xml#",
            "http://www.w3.org/2006/time#",
            "http://www.w3.org/ns/person#",
            "http://www.w3.org/ns/prov#",
            "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
            "http://www.w3.org/2000/01/rdf-schema#",
            "http://schema.org/",
            "http://psi.egovpt.org/types/",
            "http://www.w3.org/2006/vcard/ns#",
            "http://purl.org/vocab/vann/",
            "http://purl.org/vocommons/voaf#",
            "http://rdfs.org/ns/void#",
            "http://www.w3.org/2003/06/sw-vocab-status/ns#",
            "http://www.w3.org/2007/05/powder-s#",
            "http://xmlns.com/wot/0.1/",
            "http://www.w3.org/2000/10/swap/pim/contact#",
            "http://www.w3.org/1999/xhtml/vocab"
    };

    private Ontology ontology;

    private OntologyData ontologyData;

    private UriResolver uriResolver;

    private Ontology fallbackOntology;

    private FallbackOntologyData fallbackOntologyData;

    public OntologyWrapper(OntologyConfiguration ontologyConfiguration, InferredOntologyConfiguration inferredOntologyConfiguration,
                    UriResolver uriResolver) {

        this.uriResolver = uriResolver;

        initMain(ontologyConfiguration, inferredOntologyConfiguration);
        initFallback(ontologyConfiguration);
    }

    private void initMain(final OntologyConfiguration ontologyConfiguration,
                          final InferredOntologyConfiguration inferredOntologyConfiguration) {

        ontology = new Ontology(inferredOntologyConfiguration.getDefaultQueries(), ontologyConfiguration.getCdmOntologyUri(),
                uriResolver, defaultAdditionalOntologyUris, true);

        ValidationResults validationResults = new ValidationResults();
        validateOntologyUris(validationResults, new String[]{RDF.getURI()});
        validateOntologyUris(validationResults, new String[]{RDFS.getURI()});
        validateOntologyUris(validationResults, new String[]{RDFS.getURI(), OWL.getURI()});
        validateOntologyUris(validationResults, new String[]{skosUri}, skos, XSD.getURI(), DCTerms.getURI());
        validateOntologyUris(validationResults, new String[]{skosXlUri}, skos, skosXl, XSD.getURI(), DCTerms.getURI());
        validateOntologyUris(validationResults, ontology.getLoadedOntologyUris(), ALLOWED_URIS);

        checkValidation(validationResults);
    }

    private void initFallback(final OntologyConfiguration ontologyConfiguration) {
        fallbackOntology = new Ontology(null, ontologyConfiguration.getFallbackOntologyUri(), this.uriResolver,
                Collections.emptySet(), true);

        ValidationResults validationResults = new ValidationResults();
        validateOntologyUris(validationResults, fallbackOntology, skos, skosXl, publications_ontology, cdm, cmr, DCTerms.getURI());

        checkValidation(validationResults);
    }

    private static void checkValidation(ValidationResults validationResults) {
        if (validationResults.matchLevel(ValidationResults.Level.ERROR)) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withCode(CmrErrors.VALIDATION_ONTOLOGY_ERROR)
                    .withMessage(validationResults.getWorstMessage())
                    .build();
        }
    }

    private void validateOntologyUris(final ValidationResults validationResults, final String[] ontologyUris,
                                      final String... allowedUriPrefixes) {
        final Ontology toValidateOntology = new Ontology(null, ontologyUris, uriResolver, Collections.emptySet(), false);
        try {
            validateOntologyUris(validationResults, toValidateOntology, allowedUriPrefixes);
        } finally {
            toValidateOntology.closeQuietly();
        }
    }

    private void validateOntologyUris(final ValidationResults validationResults, final Ontology ontology,
                                      final String... allowedUriPrefixes) {
        new OntologyUriValidation(validationResults, ontology.getOntModel(), allowedUriPrefixes).execute();
    }

    public Ontology getOntology() {
        return ontology;
    }

    public OntologyData getOntologyData() {
        if (ontologyData == null) {
            ontologyData = OntologyBuilder.build(OntologyData.class, ontology);
        }
        return ontologyData;
    }

    public Ontology getFallbackOntology() {
        return fallbackOntology;
    }

    public FallbackOntologyData getFallbackOntologyData() {
        if (fallbackOntologyData == null) {
            fallbackOntologyData = OntologyBuilder.build(FallbackOntologyData.class, this.fallbackOntology);
        }
        return fallbackOntologyData;
    }
}
