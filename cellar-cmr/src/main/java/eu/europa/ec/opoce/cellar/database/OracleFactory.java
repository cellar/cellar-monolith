package eu.europa.ec.opoce.cellar.database;

import oracle.spatial.rdf.client.jena.Oracle;

/**
 * Interface to implement Factory pattern for the retrieval
 * of <code>oracle.spatial.rdf.client.jena.Oracle</code> instances.
 */
public interface OracleFactory {

    /**
     * Gets a <code>oracle.spatial.rdf.client.jena.Oracle</code> instance
     *
     * @return a Oracle instance
     */
    public Oracle getOracle();
}
