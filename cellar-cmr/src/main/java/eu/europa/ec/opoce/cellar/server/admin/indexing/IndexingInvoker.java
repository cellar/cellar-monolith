package eu.europa.ec.opoce.cellar.server.admin.indexing;

import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService.Priority;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequest.RequestType;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestGenerationService;
import eu.europa.ec.opoce.cellar.common.MessageFormatter;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * This class handles all the calls from the IndexingController
 * It works as a service invoker for all the functionality that is needed concerning indexing in the admin interface.
 */
@Component
public class IndexingInvoker {

    private static final Logger LOG = LogManager.getLogger(IndexingInvoker.class);

    private static final String ADMINISTRATOR = "administrator";
    private static final Priority PRIORITY = Priority.NORMAL;

    private final CmrIndexRequestGenerationService cmrIndexRequestGenerationService;

    private final IdentifierService identifierService;

    private final CellarResourceDao cellarResourceDao;

    @Autowired
    public IndexingInvoker(CmrIndexRequestGenerationService cmrIndexRequestGenerationService, @Qualifier("pidManagerService") IdentifierService identifierService,
                           CellarResourceDao cellarResourceDao) {
        this.cmrIndexRequestGenerationService = cmrIndexRequestGenerationService;
        this.identifierService = identifierService;
        this.cellarResourceDao = cellarResourceDao;
    }

    /**
     * This method adds a production system id to the indexing queue.
     *
     * @param workId         the id of the work
     * @param calcNotice   the calc notice
     * @param calcEmbedded the calc embedded
     * @return a boolean whether or not the insert action succeeded
     */
    public boolean addWork(String workId, boolean calcNotice, boolean calcEmbedded, List<String> languages) {
        return addWork(workId, calcNotice, calcEmbedded, PRIORITY, languages);
    }

    /**
     * This method adds a production system id to the indexing queue.
     *
     * @param workId       the id of the work
     * @param calcNotice   the calc notice
     * @param calcEmbedded the calc embedded
     * @param priority     the priority in the indexing queue
     * @return a boolean whether or not the insert action succeeded
     */
    public boolean addWork(String workId, boolean calcNotice, boolean calcEmbedded, Priority priority, List<String> languages) {
        try {
            if (StringUtils.isBlank(workId)) {
                throw ExceptionBuilder.get(CellarException.class)
                        .withCode(CmrErrors.INDEXING_REQUEST_SERVICE_ERROR)
                        .withMessage("Cannot add work for indexing, the work id is not set.")
                        .build();
            }

            final Collection<RequestType> requestTypes = RequestType.getStatus(false, calcEmbedded, calcNotice, false, false);
            final String cellarId = identifierService.getCellarPrefixed(workId);

            filter(cellarResourceDao.findHierarchy(cellarId), languages)
                    .forEach(cr -> adminIndexRequest(requestTypes, priority, cr.getCellarId()));
            return true;

        } catch (final CellarException e) {
            LOG.error("Cannot add the work for indexing", e);
            throw e;
        }
    }

    private static Collection<CellarResource> filter(Collection<CellarResource> resources, List<String> languages) {
        int count = (int) resources.stream()
                .filter(r -> r.getCellarType() == DigitalObjectType.EXPRESSION)
                .count();

        if (count > languages.size()) {
            List<String> ignore = resources.stream()
                    .filter(cr -> cr.getCellarType() == DigitalObjectType.EXPRESSION)
                    .filter(cr -> cr.getLanguages().stream().noneMatch(anyMatch(languages)))
                    .map(CellarResource::getCellarId)
                    .collect(Collectors.toList());

            return resources.stream()
                    .filter(cr -> cr.getCellarType() != DigitalObjectType.ITEM && cr.getCellarType() != DigitalObjectType.WORK)
                    .filter(cr -> noneMatch(cr.getCellarId(), ignore))
                    .collect(Collectors.toList());
        }
        return Collections.singletonList(resources.stream()
                .filter(cr -> cr.getCellarType() == DigitalObjectType.WORK)
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("Work not found")));
    }

    private static boolean noneMatch(String cellarId, List<String> ignore) {
        return ignore.stream().noneMatch(cellarId::startsWith);
    }

    private static Predicate<String> anyMatch(final List<String> languages) {
        return language -> languages.stream().anyMatch(l -> l.equalsIgnoreCase(language));
    }

    /**
     * This method adds new item's to the indexing queue.
     *
     * @param types    the types
     * @param priority the priority
     * @param uri      the uri
     */
    private void adminIndexRequest(final Collection<RequestType> types, final Priority priority, final String uri) {
        try {
            LOG.info("Add request with uri {} and priority {} to indexqueue", uri, priority);
            cmrIndexRequestGenerationService.generateIndexRequest(uri, ADMINISTRATOR, types, priority, CmrIndexRequest.Reason.Administrator);
        } catch (Exception e) {
            final String message = MessageFormatter.format("Adding request for reindexing failed: {}", e.getMessage());
            LOG.error(message, e);

            throw e instanceof CellarException ? (CellarException) e : ExceptionBuilder.get(CellarException.class).withCause(e).build();
        }
    }

}
