/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.retrieve.link.builder
 *             FILE : LinkFormatBuilder.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 27, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.retrieve.link.builder;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.utils.MementoUtils;
import eu.europa.ec.opoce.cellar.cmr.service.IMementoQueryConfiguration;
import eu.europa.ec.opoce.cellar.cmr.sparql.utils.QueryUtils;
import eu.europa.ec.opoce.cellar.common.http.headers.RelType;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.ExtendedMediaType;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;

import org.apache.jena.query.QuerySolution;
import org.apache.jena.rdf.model.RDFNode;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 27, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Configurable
public class LinkFormatBuilder {

    //Memento Query configuration
    @Autowired
    private IMementoQueryConfiguration mementoQueryConfiguration;

    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    @Autowired
    @Qualifier("pidManagerService")
    private IdentifierService identifierService;

    private final String cellarUri;

    private StringBuilder linkFormat;

    private TimemapInfo selfTimemapInfo;

    private LinkFormatBuilder(final String cellarUri) {
        this.cellarUri = cellarUri;

        this.linkFormat = new StringBuilder();
        this.selfTimemapInfo = null;
    }

    public static LinkFormatBuilder get(final String cellarUri) {
        return new LinkFormatBuilder(cellarUri);
    }

    public String getLinkFormat() {
        return this.linkFormat.toString();
    }

    public LinkFormatBuilder buildSelf() {

        final TimemapInfo timemapInfo = this.getSelfTimemapInfo();
        if (timemapInfo != null) {
            this.linkFormat
                    .append(MementoUtils.getMementoLink(this.cellarUri, RelType.SELF, ExtendedMediaType.APPLICATION_LINK_FORMAT_VALUE,
                            timemapInfo.getStartDate(), timemapInfo.getEndDate(), timemapInfo.getTypeOfDate()))
                    .append("\n");
        }

        return this;
    }

    public LinkFormatBuilder buildTimemaps() {

        TimemapInfo timemapInfo = null;

        // build timemap
        for (final String timemapResource : this.getTimemapResources()) {
            timemapInfo = this.getTimemapInfo(timemapResource);
            if (timemapInfo != null) {
                this.linkFormat.append(
                        MementoUtils.getMementoLink(timemapResource, RelType.TIMEMAP, ExtendedMediaType.APPLICATION_LINK_FORMAT_VALUE,
                                timemapInfo.getStartDate(), timemapInfo.getEndDate(), timemapInfo.getTypeOfDate()))
                        .append("\n");
            }
        }

        // add self
        timemapInfo = this.getSelfTimemapInfo();
        if (timemapInfo != null) {
            this.linkFormat
                    .append(MementoUtils.getMementoLink(this.cellarUri, RelType.TIMEMAP, ExtendedMediaType.APPLICATION_LINK_FORMAT_VALUE,
                            timemapInfo.getStartDate(), timemapInfo.getEndDate(), timemapInfo.getTypeOfDate()))
                    .append("\n");
        }

        return this;
    }

    public LinkFormatBuilder buildMementos() {
        final Map<Pattern, Object> placeHoldersMap = QueryUtils.preparePlaceHoldersMap(cellarUri);
        final String query = this.mementoQueryConfiguration.getQueryForRelatedMementos(placeHoldersMap);
        final List<QuerySolution> querySolutions
                = QueryUtils.executeSelectQuery(this.cellarConfiguration.getCellarServiceSparqlUri(), query);
        final Set<String> mementoUris = new HashSet<>();

        for (QuerySolution qs : querySolutions) {
            RDFNode memento = qs.get("memento");
            RDFNode date = qs.get("date");
            Date dateObj = QueryUtils.getDateOrNull(date);

            if (memento.isURIResource() && null != dateObj) {
                final String cellarUri = this.identifierService.getCellarUriOrNull(memento.asResource().getURI()); // not supposed to be null, it relies on the sparql queries
                if (mementoUris.add(cellarUri)) {
                    this.linkFormat.append(MementoUtils.getMementoLink(cellarUri, RelType.MEMENTO, dateObj)).append("\n");
                }
            }
        }

        return this;
    }

    public LinkFormatBuilder buildOriginalTimegates() {
        final Map<Pattern, Object> placeHoldersMap = QueryUtils.preparePlaceHoldersMap(cellarUri);
        final String query = this.mementoQueryConfiguration.getQueryForURIG(placeHoldersMap);
        final List<QuerySolution> querySolutions
                = QueryUtils.executeSelectQuery(this.cellarConfiguration.getCellarServiceSparqlUri(), query);
        final Set<String> originalTimegateUris = new HashSet<>();

        //Iterate over the predecessors in order to find the top one
        for (QuerySolution qs : querySolutions) {
            final RDFNode predecessor = qs.get("predecessor");
            if (predecessor.isURIResource()) {
                final String cellarUri = this.identifierService.getCellarUriOrNull(predecessor.asResource().getURI());
                if (originalTimegateUris.add(cellarUri)) {
                    this.linkFormat.append(MementoUtils.getMementoLink(cellarUri, RelType.ORIGINAL_TIMEGATE)).append("\n");
                }
            }
        }

        return this;
    }

    private TimemapInfo getSelfTimemapInfo() {
        if (this.selfTimemapInfo != null) {
            return this.selfTimemapInfo;
        }

        this.selfTimemapInfo = this.getTimemapInfo(this.cellarUri);

        return this.selfTimemapInfo;
    }

    private TimemapInfo getTimemapInfo(final String timemapResource) {
        final Map<Pattern, Object> placeHoldersMap = QueryUtils.preparePlaceHoldersMap(timemapResource);
        final String query = this.mementoQueryConfiguration.getQueryForTimeMapInfo(placeHoldersMap);
        final List<QuerySolution> querySolutions
                = QueryUtils.executeSelectQuery(cellarConfiguration.getCellarServiceSparqlUri(), query);

        for (QuerySolution qs : querySolutions) {
            final RDFNode startDateNode = qs.get("startdate");
            final RDFNode endDateNode = qs.get("enddate");
            final RDFNode typeOfDateNode = qs.get("typeofdate");

            final Date startDate = QueryUtils.getDateOrNull(startDateNode);
            final Date endDate = QueryUtils.getDateOrNull(endDateNode);

            if (startDate != null && endDate != null) {
                return new TimemapInfo(startDate, endDate, typeOfDateNode.toString());
            }
        }

        return null;
    }

    private Set<String> getTimemapResources() {
        final Map<Pattern, Object> placeHoldersMap = QueryUtils.preparePlaceHoldersMap(this.cellarUri);
        final String query = this.mementoQueryConfiguration.getQueryForRelatedEvolutiveWorks(placeHoldersMap);

        return QueryUtils.executeSelectQuery(this.cellarConfiguration.getCellarServiceSparqlUri(), query).stream()
                .map(qs -> qs.getResource("evolutive_work"))
                .filter(RDFNode::isURIResource)
                .map(n -> n.asResource().getURI())
                .map(uri -> identifierService.getCellarUriOrNull(uri))
                .collect(Collectors.toSet());
    }

    private static class TimemapInfo {

        private final Date startDate;
        private final Date endDate;
        private final String typeOfDate;

        public TimemapInfo(final Date startDate, final Date endDate, final String typeOfDate) {
            this.startDate = startDate;
            this.endDate = endDate;
            this.typeOfDate = typeOfDate;
        }

        /**
         * @return the startDate
         */
        public Date getStartDate() {
            return startDate;
        }

        /**
         * @return the endDate
         */
        public Date getEndDate() {
            return endDate;
        }

        /**
         * @return the typeOfDate
         */
        public String getTypeOfDate() {
            return typeOfDate;
        }
    }
}
