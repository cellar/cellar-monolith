package eu.europa.ec.opoce.cellar.cmr;

import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;

/**
 * <p>RelatedObject class.</p>
 */
public class RelatedObject {

    private final String resourceUri;
    private final DigitalObjectType type;
    private final String property;

    /**
     * <p>Constructor for RelatedObject.</p>
     *
     * @param resourceUri a {@link java.lang.String} object.
     * @param type        a {@link java.lang.String} object.
     * @param property    a {@link java.lang.String} object.
     */
    public RelatedObject(String resourceUri, String type, String property) {
        this(resourceUri, DigitalObjectType.getByValue(type), property);
    }

    /**
     * <p>Constructor for RelatedObject.</p>
     *
     * @param resourceUri a {@link java.lang.String} object.
     * @param type        a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType} object.
     * @param property    a {@link java.lang.String} object.
     */
    public RelatedObject(String resourceUri, DigitalObjectType type, String property) {
        this.resourceUri = resourceUri;
        this.type = type;
        this.property = property;
    }

    /**
     * <p>Getter for the field <code>resourceUri</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getResourceUri() {
        return resourceUri;
    }

    /**
     * <p>Getter for the field <code>type</code>.</p>
     *
     * @return a {@link eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType} object.
     */
    public DigitalObjectType getType() {
        return type;
    }

    /**
     * <p>Getter for the field <code>property</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getProperty() {
        return property;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof RelatedObject))
            return false;

        RelatedObject that = (RelatedObject) o;

        if (!property.equals(that.property))
            return false;
        if (!resourceUri.equals(that.resourceUri))
            return false;
        if (type != that.type)
            return false;

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int result = resourceUri.hashCode();
        result = 31 * result + type.hashCode();
        result = 31 * result + property.hashCode();
        return result;
    }
}
