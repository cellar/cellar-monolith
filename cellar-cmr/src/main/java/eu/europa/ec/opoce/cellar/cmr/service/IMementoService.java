package eu.europa.ec.opoce.cellar.cmr.service;

import eu.europa.ec.opoce.cellar.common.http.headers.ILink;

import java.util.Collection;
import java.util.Date;

/**
 * Interface for the Memento service methods
 * @author ARHS Developments
 */
public interface IMementoService {

    /**
     * Verifies whether a given URI refers to a resource that may links Mementos - directly or indirectly.<br />
     * For example, such resource may be an Evolutive Work (indirectly) or an Intermediate Evolutive Work (directly or indirectly).
     * 
     * @param uri the URI of the resource to check
     * @return true if the URI is indeed related to a Memento, false otherwise
     */
    boolean isEvolutiveWork(final String uri);

    boolean isEvolutiveWorkOrMemento(final String uri);

    /**
     * Retrieves the URI and date-time of the first Memento related to a given evolutive work
     * @param uri the URI of the evolutive work
     * @return an {@code ILink} object containing the URI and date-time of the first Memento related to the specified evolutive work
     */
    ILink getFirstMementoCellarUri(final String uri);

    /**
     * Retrieves the URI and date-time of the last Memento related to a given evolutive work
     * @param uri the URI of the evolutive work
     * @return an {@code ILink} object containing the URI and date-time of the last Memento related to the specified evolutive work
     */
    ILink getLastMementoCellarUri(final String uri);

    /**
     * Retrieves the date-time associated to a given Memento, identified by a URI
     * @param uri the URI identifying the Memento
     * @return the date-time associated to the Memento, or null if no results were returned by the SPARQL end-point 
     */
    Date getMementoDateTime(final String uri);

    /**
     * Retrieves the location information of next redirect based on the provided URI and accept-date-time 
     * @param uri the URI of the related resource
     * @param acceptDateTime the accept-date-time
     * @return the location information of next redirect, or null if no results were returned by the SPARQL end-point
     */
    String getNearestInFutureCellarUri(final String uri, final Date acceptDateTime);

    /**
     * Retrieves the location information of next redirect based on the provided URI and accept-date-time 
     * @param uri the URI of the related resource
     * @param acceptDateTime the accept-date-time
     * @return the location information of next redirect, or null if no results were returned by the SPARQL end-point
     */
    String getNearestInPastCellarUri(final String uri, final Date acceptDateTime);

    /**
     * Retrieves the URI of the related original resource and returns it as a String
     * @param uri the URI of the target resource
     * @return the original resource's URI as a String, or null if the query did not return any result
     */
    String getOriginalCellarUri(final String uri);

    /**
     * Returns the full links related to a given resource, i.e.:
     * - Its original timegate;
     * - Its timemap;
     * - Its first and last memento locations
     * @param uri the resource's URI
     * @return a {@code Collection} containing the full links related to the resource referenced
     * by the provided URI
     */
    Collection<ILink> getFullLinks(final String uri, final Date acceptDateTime);

}
