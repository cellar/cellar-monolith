/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.xml
 *             FILE : XmlTreeRedirectResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 4, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.redirect.xml;

import eu.europa.ec.opoce.cellar.common.http.headers.IURLTokenable;
import eu.europa.ec.opoce.cellar.server.dissemination.AcceptLanguage;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.Structure;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.IDisseminationResolver;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Apr 4, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class XmlTreeRedirectResolver extends XmlRedirectResolver {

    /**
     * Instantiates a new xml tree redirect resolver.
     *
     * @param cellarResource the cellar resource
     * @param decoding the decoding
     * @param acceptLanguage the accept language
     * @param provideAlternates the provide alternates
     */
    protected XmlTreeRedirectResolver(final CellarResourceBean cellarResource, final String decoding, final AcceptLanguage acceptLanguage,
            final boolean provideAlternates) {
        super(cellarResource, decoding, acceptLanguage, provideAlternates);
    }

    /**
     * Gets the.
     *
     * @param cellarResource the cellar resource
     * @param decoding the decoding
     * @param acceptLanguage the accept language
     * @param provideAlternates the provide alternates
     * @return the i dissemination resolver
     */
    public static IDisseminationResolver get(final CellarResourceBean cellarResource, final String decoding,
            final AcceptLanguage acceptLanguage, final boolean provideAlternates) {
        return new XmlTreeRedirectResolver(cellarResource, decoding, acceptLanguage, provideAlternates);
    }

    /** {@inheritDoc} */
    @Override
    protected void initDisseminationRequest() {
        super.initDisseminationRequest();
    }

    /** {@inheritDoc} */
    @Override
    protected void doHandleWorkDisseminationRequest() {
        // this.throwErrorIfHasAcceptLanguageBean();

        this.setDecodingLanguage();
    }

    /** {@inheritDoc} */
    @Override
    protected void doHandleDossierDisseminationRequest() {
        //TODO: phase-2: if accept-language is available, this header must be used as decoding language
        this.setDecodingLanguage();
    }

    /** {@inheritDoc} */
    @Override
    protected void doHandleAgentDisseminationRequest() {
        //TODO: phase-2: if accept-language is available, this header must be used as decoding language
        this.setDecodingLanguage();
    }

    /** {@inheritDoc} */
    @Override
    protected void doHandleTopLevelEventDisseminationRequest() {
        //TODO: phase-2: if accept-language is available, this header must be used as decoding language
        this.setDecodingLanguage();
    }

    /** {@inheritDoc} */
    @Override
    protected IURLTokenable getTypeStructure() {
        return Structure.TREE;
    }

}
