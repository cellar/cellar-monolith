package eu.europa.ec.opoce.cellar.cmr.transformers;

import java.util.Collection;

import org.apache.commons.collections15.Transformer;
import org.apache.commons.lang.StringUtils;

/**
 * <p>Collection2String class.</p>
 */
public class Collection2String implements Transformer<Collection<String>, String> {

    /**
     * Constant <code>instance</code>
     */
    public static Transformer<Collection<String>, String> instance = new Collection2String();

    /**
     * {@inheritDoc}
     */
    @Override
    public String transform(Collection<String> input) {
        return StringUtils.join(input, ", ");
    }
}
