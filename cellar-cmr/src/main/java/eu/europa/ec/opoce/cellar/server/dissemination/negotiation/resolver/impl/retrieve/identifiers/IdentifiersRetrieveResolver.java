/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.retrieve.identifiers
 *             FILE : IdentifiersRetrieveResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : May 8, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.retrieve.identifiers;

import eu.europa.ec.opoce.cellar.common.http.headers.IURLTokenable;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.enums.Type;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.IDisseminationResolver;
import eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.retrieve.RetrieveResolver;

import org.springframework.http.ResponseEntity;
import org.w3c.dom.Document;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : May 8, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class IdentifiersRetrieveResolver extends RetrieveResolver {

    public static IDisseminationResolver get(final String identifier, final String eTag, final String lastModified) {
        return new IdentifiersRetrieveResolver(identifier, lastModified, eTag);
    }

    /**
     * @param identifier
     * @param eTag
     */
    protected IdentifiersRetrieveResolver(final String identifier, final String eTag, final String lastModified) {
        super(identifier, eTag, lastModified, true);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.server.dissemination.negotiation.resolver.impl.retrieve.RetrieveResolver#doHandleDisseminationRequest()
     */
    @Override
    public ResponseEntity<Document> doHandleDisseminationRequest() {
        return this.disseminationService.doIdentifiersRequest(this.eTag, this.lastModified, this.cellarResource);
    }

    @Override
    protected IURLTokenable getTypeStructure() {
        return Type.IDENTIFIERS;
    }

}
