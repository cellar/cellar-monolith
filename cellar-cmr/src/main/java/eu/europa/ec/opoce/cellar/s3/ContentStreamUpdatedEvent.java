package eu.europa.ec.opoce.cellar.s3;

import eu.europa.ec.opoce.cellar.cmr.ContentType;

/**
 * @author ARHS Developments
 */
public class ContentStreamUpdatedEvent {

    private final String cellarId;
    private final String version;
    private final ContentType contentType;

    public ContentStreamUpdatedEvent(String cellarId, String version, ContentType contentType) {
        this.cellarId = cellarId;
        this.version = version;
        this.contentType = contentType;
    }

    public String getCellarId() {
        return cellarId;
    }

    public String getVersion() {
        return version;
    }

    public ContentType getContentType() {
        return contentType;
    }
}
