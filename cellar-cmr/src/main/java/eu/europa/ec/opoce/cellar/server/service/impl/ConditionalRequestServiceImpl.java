/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.service.impl
 *             FILE : ConditionalRequestServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 20, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.service.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.impl.CellarStaticConfiguration;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.daobeans.InverseRelation;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.database.dao.InverseRelationDao;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.HierarchyElement;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement;
import eu.europa.ec.opoce.cellar.common.util.TimeUtils;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.ontology.service.OntologyService;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import eu.europa.ec.opoce.cellar.server.conditionalRequest.ConditionalRequestVersions;
import eu.europa.ec.opoce.cellar.server.conditionalRequest.ETagType;
import eu.europa.ec.opoce.cellar.server.service.CellarResourceBean;
import eu.europa.ec.opoce.cellar.server.service.IConditionalRequestService;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.google.common.collect.Iterables;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.OptionalLong;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * <class_description>  .
 * <br/><br/>
 * <notes>  .
 * <br/><br/>
 * ON : Oct 20, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class ConditionalRequestServiceImpl implements IConditionalRequestService {
	
	private static final long EPOCH_MILLIS = Instant.EPOCH.toEpochMilli();
	
    @Autowired
    private ContentStreamService contentStreamService;

    @Autowired
    private InverseRelationDao inverseRelationDao;

    @Autowired
    @Qualifier("pidManagerService")
    private IdentifierService pidManagerService;

    @Autowired
    private CellarResourceDao cellarResourceDao;

    @Autowired
    private OntologyService ontologyService;

    @Autowired
    private CellarStaticConfiguration cellarStaticConfiguration;

    /**
     * Calculate version.
     *
     * @param metsElements             the mets elements
     * @param etagType                 the etag type
     * @param eTag                     the e tag
     * @param lastModified             the last modified
     * @param isEmbeddedNoticeRequired the is embedded notice required
     * @return the conditional request versions
     */
    private ConditionalRequestVersions calculateVersion(final Collection<MetsElement> metsElements, final ETagType etagType,
                                                        final String eTag, final String lastModified, final boolean isEmbeddedNoticeRequired) {
        final Date newLastModified = this.getLastModified(metsElements, isEmbeddedNoticeRequired);

        return new ConditionalRequestVersions(eTag, etagType.buildETag(newLastModified), lastModified, newLastModified);
    }

    @Override
    public ConditionalRequestVersions calculateVersionNow(final ETagType etagType, final String eTag, final String lastModified) {
        final Date now = new Date();
        final Integer refreshDelayInMinutes = cellarStaticConfiguration.getCellarServiceDisseminationDailyOjCacheRefreshMinutes();

        Date lastModifiedDate = new Date();
        if (StringUtils.isNotBlank(lastModified)) {
            lastModifiedDate = TimeUtils.getHTTP11DateSilently(lastModified);
        }

        final Date endDate = DateUtils.addMinutes(lastModifiedDate, refreshDelayInMinutes);
        String newETag = eTag;
        Date newLastModifiedDate = lastModifiedDate;
        if (now.after(endDate)) {//only returns new eTag and new last modification date if time span has expired
            newETag = etagType.buildETag(now);
            newLastModifiedDate = now;
        }
        return new ConditionalRequestVersions(eTag, newETag, lastModified, newLastModifiedDate);
    }

    @Override
    public ConditionalRequestVersions calculateVersionWithEmbedding(final Collection<MetsElement> metsElements, final ETagType etagType,
                                                                    final String eTag, final String lastModified) {
        return calculateVersion(metsElements, etagType, eTag, lastModified, true);
    }

    @Override
    public ConditionalRequestVersions calculateVersionWithEmbedding(
            final HierarchyElement<? extends MetsElement, ? extends MetsElement> element, final ETagType etagType, final String eTag,
            final String lastModified) {

        return this.calculateVersionWithEmbedding(element.getFlatHierarchy(etagType.getDigitalObjectTypes()), etagType, eTag, lastModified);
    }

    @Override
    public ConditionalRequestVersions calculateVersionWithEmbedding(final MetsElement metsElement, final ETagType etagType,
                                                                    final String eTag, final String lastModified) {
        return this.calculateVersionWithEmbedding(Collections.singletonList(metsElement), etagType, eTag, lastModified);
    }

    @Override
    public ConditionalRequestVersions calculateVersionWithoutEmbedding(final CellarResourceBean element, final ETagType etagType,
                                                                       final String eTag, final String lastModified) {

        final Date newLastModified = element.getLastModificationDate();

        return new ConditionalRequestVersions(eTag, etagType.buildETag(newLastModified), lastModified, newLastModified);
    }

    @Override
    public ConditionalRequestVersions calculateVersionWithoutEmbedding(final Collection<MetsElement> metsElements, final ETagType etagType,
                                                                       final String eTag, final String lastModified) {
        return calculateVersion(metsElements, etagType, eTag, lastModified, false);

    }

    @Override
    public ConditionalRequestVersions calculateVersionWithoutEmbedding(final CellarResource resource, final ETagType etagType,
                                                                       final String eTag, final String lastModified) {

        String version = resource.getVersion(ContentType.FILE).orElse(null);
        Date newLastModified = contentStreamService.getLastModificationDate(resource.getCellarId(), version, ContentType.FILE);

        return new ConditionalRequestVersions(eTag, etagType.buildETag(newLastModified), lastModified, newLastModified);
    }

    /**
     * Retrieves the resources inversely related to the provided digital object hierarchy
     * and returns the most recent embedded notice creation timestamp among them.
     *
     * @param elements the input digital object hierarchy.
     * @return the most recent creation timestamp of the referenced embedded notices.
     */
    private long getInverseRelationsEmbeddedNoticesLatestTimestamp(final Collection<MetsElement> elements) {
    	Set<String> cellarIds = elements.stream()
    		.map(e -> e.getCellarId().getIdentifier())
    		.collect(Collectors.toSet());
    	// Retrieve and iterate over the inversely related resources.
    	Set<String> inverselyRelatedPids = new HashSet<>();
    	for (final InverseRelation invRel : this.inverseRelationDao.findRelationsByTargets(cellarIds)) {
    		for (final String property : invRel.getPropertiesTargetSource()) {
    			// Gather the inversely related Production identifiers having the necessary annotation.
    			if (ontologyService.getWrapper().getOntologyData().getEmbeddingNotices().contains(property) ) {
    				inverselyRelatedPids.add(invRel.getSource());
    				break;
    			}
    		}
    	}
    	// Resolve the applicable inversely related Production identifiers to Cellar identifiers, and retrieve the matching Cellar resources.
    	Collection<String> inverselyRelatedCellarIds = resolvePidsToCellarIds(inverselyRelatedPids);
    	Collection<CellarResource> inverselyRelatedResources = this.cellarResourceDao.findCellarIds(inverselyRelatedCellarIds);
    	// Extract the embedded notice creation dates and return the latest one.
    	List<Date> embeddedNoticeCreationDates = inverselyRelatedResources.stream()
    		.map(CellarResource::getEmbeddedNoticeCreationDate)
    		.collect(Collectors.toList());
        return getMaxDateMillis(embeddedNoticeCreationDates);
    }
    
    /**
     * Resolves the provided collection of Production Identifiers to Cellar Identifiers.
     * @param inverselyRelatedPids the Production Identifiers to resolve.
     * @return the resolved Cellar Identifiers.
     */
    private Collection<String> resolvePidsToCellarIds(Collection<String> inverselyRelatedPids) {
    	int maxInClauseParams = this.cellarStaticConfiguration.getCellarDatabaseInClauseParams();
    	if (inverselyRelatedPids.size() > maxInClauseParams) {
    		Set<String> inverselyRelatedCellarIds = new HashSet<>();
    		Iterable<List<String>> subGroups = Iterables.partition(inverselyRelatedPids, maxInClauseParams);
    		subGroups.forEach(group -> inverselyRelatedCellarIds.addAll(this.pidManagerService.getCellarUUIDs(group)));
    		return inverselyRelatedCellarIds;
    	}
    	else {
    		return this.pidManagerService.getCellarUUIDs(new ArrayList<>(inverselyRelatedPids));
    	}
    }
    
    /**
     * Extracts the target date attribute from the provided digital object hierarchy as identified by
     * via the supplied mapper function, and returns the the most recent timestamp among
     * the extracted dates.
     * @param elements elements the input digital object hierarchy.
     * @param mapper the mapper function 
     * @return
     */
    private long getHierarchyLatestTimestamp(final Collection<MetsElement> elements, Function<? super MetsElement, ? extends Date> mapper) {
    	List<Date> dates = elements.stream()
        		.map(mapper)
        		.collect(Collectors.toList());
    	return getMaxDateMillis(dates);
    }
    
    /**
     * Converts the provided collection of dates to timestamps
     * and returns the maximum.
     * @param dates the input date collection.
     * @return the maximum timestamp.
     */
    private long getMaxDateMillis(final Collection<Date> dates) {
    	OptionalLong maxValue = dates.stream()
        		.map(Date::getTime)
        		.mapToLong(t -> t)
        		.max();
    	return maxValue.orElse(EPOCH_MILLIS);
    }

    /**
     * Get Last Modified.
     *
     * @param elements                 the mets elements
     * @param isEmbeddedNoticeRequired the is embedded notice required
     * @return the most recent date of the elements
     */
    private Date getLastModified(final Collection<MetsElement> elements, final boolean isEmbeddedNoticeRequired) {
        if (elements.isEmpty()) {
            throw ExceptionBuilder.get(CellarException.class).build();
        }
        long embeddedNoticeCreationTimestamp = EPOCH_MILLIS;
        long inverseRelationsEmbeddedNoticeCreationTimestamp = EPOCH_MILLIS;
        
        long hierarchyLastModificationTimestamp = getHierarchyLatestTimestamp(elements, MetsElement::getLastModificationDate);
        if (isEmbeddedNoticeRequired) {
        	embeddedNoticeCreationTimestamp = getHierarchyLatestTimestamp(elements, MetsElement::getEmbeddedNoticeCreationDate);
        	inverseRelationsEmbeddedNoticeCreationTimestamp = getInverseRelationsEmbeddedNoticesLatestTimestamp(elements);
        }

        long lastModifiedTimestamp = Collections.max(Arrays.asList(hierarchyLastModificationTimestamp, embeddedNoticeCreationTimestamp, inverseRelationsEmbeddedNoticeCreationTimestamp));        
        return new Date(lastModifiedTimestamp);
    }

}
