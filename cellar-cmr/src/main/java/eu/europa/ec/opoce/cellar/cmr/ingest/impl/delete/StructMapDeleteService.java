/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.ingest.impl.delete
 *             FILE : StructMapDeleteService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 5, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.ingest.impl.delete;

import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.cl.domain.response.IOperation;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.EmbargoDatabaseService;
import eu.europa.ec.opoce.cellar.cmr.MetaDataIngestionHelper;
import eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData;
import eu.europa.ec.opoce.cellar.cmr.ingest.IStructMapLoader;
import eu.europa.ec.opoce.cellar.cmr.ingest.impl.AbstractStructMapService;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestGenerationService;
import eu.europa.ec.opoce.cellar.common.metadata.IModelLoader;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.s3.ContentStreamExceptions;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.Lang;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 5, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class StructMapDeleteService extends AbstractStructMapService {

    private static final Logger LOG = LogManager.getLogger(StructMapDeleteService.class);

    private MetaDataIngestionHelper metaDataIngestionHelper;
    private IStructMapLoader structMapLoader;
    private CmrIndexRequestGenerationService cmrIndexRequestGenerationService;
    private EmbargoDatabaseService embargoDatabaseService;

    private final Map<String, StructMap> currentlyManagedStructMaps = new HashMap<>();

    @Autowired
    public StructMapDeleteService(MetaDataIngestionHelper metaDataIngestionHelper,
                                  IStructMapLoader structMapLoader,
                                  CmrIndexRequestGenerationService cmrIndexRequestGenerationService,
                                  EmbargoDatabaseService embargoDatabaseService) {
        this.metaDataIngestionHelper = metaDataIngestionHelper;
        this.structMapLoader = structMapLoader;
        this.cmrIndexRequestGenerationService = cmrIndexRequestGenerationService;
        this.embargoDatabaseService = embargoDatabaseService;
    }

    @Override
    public void execute(final CalculatedData calculatedData, final IOperation<?> operation) {
        deleteTimed(calculatedData, operation);
    }

    private void deleteTimed(final CalculatedData calculatedData, final IOperation<?> operation) {
        final StructMap structMap = calculatedData.getStructMap();
        final String structMapId = structMap.getId();
        this.currentlyManagedStructMaps.put(structMapId, structMap);

        this.checkCellarIdFilled(structMap.getDigitalObject());

        final DigitalObject rootDigitalObject = structMap.getDigitalObject();
        final String rootCellarId = rootDigitalObject.getCellarId().getIdentifier();

        // Collect all DigitalObjects to delete from the structMap in the mets
        final List<DigitalObject> digitalObjectsToDelete = new ArrayList<DigitalObject>();
        this.collectObjectsToDelete(rootDigitalObject, digitalObjectsToDelete);

        // Load 'original' structMap and use it to find ALL objects (inkl. subtrees) to delete
        final StructMap originalStructMap = this.structMapLoader.load(calculatedData);
        final List<DigitalObject> allDigitalObjectsToDelete = new ArrayList<DigitalObject>();
        this.collectSubTreesOfObjectsToDelete(originalStructMap.getDigitalObject(), digitalObjectsToDelete, allDigitalObjectsToDelete);

        // Now, as all objects to delete (incl. their subtrees) are collected, add them to the calculated data
        calculatedData.setRemovedDigitalObjects(allDigitalObjectsToDelete);

        // Load the old struct map and clean it from above calculated objects to remove
        // This is the 'virtual structMap', i.e.: the original structMap minus the elements in the ingested structMap incl. their subtrees 
        final StructMap cleanedStructMap = this.structMapLoader.loadAndClean(calculatedData);

        // Make sure the cleaned struct map is constructed properly
        if (null == cleanedStructMap) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CmrErrors.INGESTION_SERVICE_ERROR)
                    .withMessage("Deletion on the new 'virtual structMap' with id '{}' not possible as it was not constructed properly")
                    .withMessageArgs(structMap.getId()).build();
        }

        // Update the calcualtedData object with new (and from now on valid) structMap
        calculatedData.replaceStructMap(cleanedStructMap);

        this.log(LOG, structMap, "Structmap '{}' loaded with previous data and cleaned with data to delete", structMap.getId());

        // Load the model data with the proper model loader
        final IModelLoader<CalculatedData> modelLoader = this.createModelLoader(structMap);
        modelLoader.load(calculatedData);

        try {

            // Calculate data for database and S3
            this.metadataCalculator.calculate(calculatedData);
            this.log(LOG, structMap, "loaded all metadata in memory for structmap'{}'", structMap.getId());

            // Ingest the calculated data in S3 and the database
            this.metaDataIngestionHelper.ingest(calculatedData, operation);

            if (this.embargoDatabaseService.isMetadataSavedPrivate(structMap.getDigitalObject())) {
                return;
            }

            String version = rootDigitalObject.getVersions().get(ContentType.DIRECT_INFERRED);
            String inferredModel = getContentStreamService().getContentAsString(rootCellarId, version, ContentType.DIRECT_INFERRED)
                    .orElseThrow(ContentStreamExceptions.notFound(rootCellarId, version, ContentType.DIRECT_INFERRED));

            final Model directAndInferredModel = JenaUtils.read(inferredModel, Lang.NT);
            this.cmrIndexRequestGenerationService.insertIndexingRequestsFromDeleteIngest(calculatedData, directAndInferredModel);
            this.log(LOG, structMap, "created requests for indexing for structmap '{}'", structMap.getId());

            //TODO this code is located here temporary. It will be move when Virtuoso will be in the same transaction that S3
            this.metaDataIngestionHelper.ingestVirtuoso(calculatedData);
        } finally {
            modelLoader.closeQuietly();
        }

        this.currentlyManagedStructMaps.remove(structMapId);
    }

    private void collectObjectsToDelete(final DigitalObject rootDigitalObject, final List<DigitalObject> digitalObjectsToDelete) {

        final List<DigitalObject> childObjects = rootDigitalObject.getChildObjects();

        if (childObjects.size() == 0) {
            // Only leave-elements are deleted, i.e.: delete all elements which have no child
            digitalObjectsToDelete.add(rootDigitalObject);
        } else {
            for (final DigitalObject childDigitalObject : childObjects) {
                this.collectObjectsToDelete(childDigitalObject, digitalObjectsToDelete);
            }
        }
    }

    private void collectSubTreesOfObjectsToDelete(final DigitalObject currentDigitalObject,
                                                  final List<DigitalObject> digitalObjectsToDelete, final List<DigitalObject> allDigitalObjectsToDelete) {

        if (digitalObjectsToDelete.contains(currentDigitalObject)) {
            this.addObjectWithChildrenWhichAreToDelete(currentDigitalObject, allDigitalObjectsToDelete);
        } else {
            for (final DigitalObject child : currentDigitalObject.getChildObjects()) {
                this.collectSubTreesOfObjectsToDelete(child, digitalObjectsToDelete, allDigitalObjectsToDelete);
            }
        }

    }

    private void addObjectWithChildrenWhichAreToDelete(final DigitalObject currentDigitalObject,
                                                       final List<DigitalObject> allDigitalObjectsToDelete) {
        allDigitalObjectsToDelete.add(currentDigitalObject);
        for (final DigitalObject child : currentDigitalObject.getChildObjects()) {
            this.addObjectWithChildrenWhichAreToDelete(child, allDigitalObjectsToDelete);
        }
    }

    public StructMap getCurrentlyManagedStructMapById(final String structMapId) {
        return this.currentlyManagedStructMaps.get(structMapId);
    }

}
