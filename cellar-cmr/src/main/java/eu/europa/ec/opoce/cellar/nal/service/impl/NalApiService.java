package eu.europa.ec.opoce.cellar.nal.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.apache.commons.lang.StringUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.NalOntoUtils;
import eu.europa.ec.opoce.cellar.nal.domain.Concept;
import eu.europa.ec.opoce.cellar.nal.domain.ConceptScheme;
import eu.europa.ec.opoce.cellar.nal.domain.Domain;
import eu.europa.ec.opoce.cellar.nal.domain.Language;
import eu.europa.ec.opoce.cellar.nal.domain.RequiredLanguageBean;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.nal.service.JenaGatewayNalApi;
import eu.europa.ec.opoce.cellar.nal.service.LanguageService;
import eu.europa.ec.opoce.cellar.nal.service.NalApi;
import eu.europa.ec.opoce.cellar.nal.service.NalConfigurationService;

@Service
public class NalApiService implements NalApi {

    private static final Logger LOG = LogManager.getLogger(NalApiService.class);

    private final LanguageService languageService;
    private JenaGatewayNalApi jenaGatewayNalApi;
    private NalConfigurationService nalConfigurationService;

    @Autowired
    public NalApiService(JenaGatewayNalApi jenaGatewayNalApi, LanguageService languageService, NalConfigurationService nalConfigurationService) {
        this.jenaGatewayNalApi = jenaGatewayNalApi;
        this.languageService = languageService;
        this.nalConfigurationService = nalConfigurationService;
    }

    /**
     * {@inheritDoc}
     * <p/>
     * Returns all the languages in which the concept scheme is currently available.
     * The result list is defaulted to the required languages:
     * - when the concept schema URI is not provided or
     * - when the concept schema does not supply itself the list of supported languages.
     */
    @Override
    public Language[] getSupportedLanguages(final String conceptScheme) {
        LOG.info("Started retrieving supported languages for conceptscheme '{}'.", conceptScheme);
        final List<RequiredLanguageBean> requiredLanguages = languageService.getRequiredLanguages();
        final List<Language> supportedLanguages = new ArrayList<>(requiredLanguages.size());
        for (final RequiredLanguageBean requiredLanguage : requiredLanguages) {
            final LanguageBean language = languageService.getByUri(requiredLanguage.getUri());
            if (language == null) {
                throw ExceptionBuilder.get(CellarException.class).withMessage("Missing language details for required language '{}'")
                        .withMessageArgs(requiredLanguage.getUri()).build();
            }

            final String code = language.getIsoCodeThreeChar();
            if (StringUtils.isBlank(code)) {
                throw ExceptionBuilder.get(CellarException.class).withMessage("Missing 3-char iso code required language '{}'")
                        .withMessageArgs(requiredLanguage.getUri()).build();
            }

            supportedLanguages.add(new Language(code));
        }

        return supportedLanguages.toArray(new Language[supportedLanguages.size()]);
    }

    /**
     * {@inheritDoc}
     * <p/>
     * Returns the conceptScheme. Method is used to ask for a specific skos:ConceptScheme
     */
    @Override
    public ConceptScheme getConceptScheme(final String conceptScheme) {
        LOG.info("Started retrieving concept scheme for conceptscheme '{}'.", conceptScheme);
        if (StringUtils.isBlank(conceptScheme)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument conceptScheme cannot be blank!").build();
        }
        Model nalModel = nalConfigurationService.getNalModel(conceptScheme, true);
        return jenaGatewayNalApi.getConceptScheme(nalModel, NalOntoUtils.getBaseUri(conceptScheme));
    }

    /**
     * {@inheritDoc}
     * <p/>
     * Returns all conceptScheme's
     */
    @Override
    public ConceptScheme[] getConceptSchemes(final Date if_modified_since) {
        LOG.info("Started retrieving concept schemes.");
        Model allNalModel = nalConfigurationService.getAllNalModel(true, NalOntoUtils.AUTHORITY_ONTOLOGY_URI);
        return jenaGatewayNalApi.getConceptSchemesOneQuery(allNalModel, if_modified_since);
    }

    /**
     * {@inheritDoc}
     * <p/>
     * Get the top concepts of the conceptScheme
     */
    @Override
    public Concept[] getTopConcepts(final String conceptScheme, final String language) {
        LOG.info("Started retrieving top concepts for conceptscheme '{}' and language '{}'.", conceptScheme, language);
        if (StringUtils.isBlank(conceptScheme)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument conceptScheme cannot be blank!").build();
        }
        if (StringUtils.isBlank(language)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument language cannot be blank!").build();
        }
        Model nalModel = nalConfigurationService.getNalModel(NalOntoUtils.getBaseUri(conceptScheme), true);

        return jenaGatewayNalApi.getTopConcepts(nalModel, conceptScheme, language);
    }

    /**
     * {@inheritDoc}
     * <p/>
     * Returns a list of concepts having a specific semantic relation with the given concept.
     * <p/>
     * Note: with URI %escaping skos:broader becomes http://www.w3.org/2004/02/skos/core%23broader
     */
    @Override
    public Concept[] getConceptRelatives(final String concept, final String relation, final String language) {
        LOG.info("Started retrieving concept relatives for conceptscheme '{}', relation '{}', language '{}'.", concept, relation, language);
        if (StringUtils.isBlank(concept)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument concept cannot be blank!").build();
        }
        if (StringUtils.isBlank(relation)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument relation cannot be blank!").build();
        }
        if (StringUtils.isBlank(language)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument language cannot be blank!").build();
        }

        Model nalModel = nalConfigurationService.getNalModel(NalOntoUtils.getBaseUri(concept), true);
        return jenaGatewayNalApi.getRelatedConceptsOneQuery(nalModel, concept, relation, language);
    }

    /**
     * {@inheritDoc}
     * <p/>
     * Get the translation of a given concept into a specified language.
     */
    @Override
    public Concept getConcept(final String concept, final String language) {
        LOG.info("Started retrieving concept for concept '{}' and language '{}'.", concept, language);
        if (StringUtils.isBlank(concept)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument concept cannot be blank!").build();
        }
        if (StringUtils.isBlank(language)) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Required argument language cannot be blank!").build();
        }
        Model nalModel = nalConfigurationService.getNalModel(NalOntoUtils.getBaseUri(concept), true);
        return jenaGatewayNalApi.getConcept(nalModel, concept, language);
    }

    /**
     * {@inheritDoc}
     * <p/>
     * Get The Domains facets of the EUROVOC thesaurus
     */
    @Override
    public Domain[] getDomains() {
        throw new UnsupportedOperationException("Method not supported by NAL API! Only supported by EUROVOC API.");
    }
}
