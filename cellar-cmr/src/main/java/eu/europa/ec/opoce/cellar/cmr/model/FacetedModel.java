/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.ontology
 *        FILE : ModelUtils.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 23-05-2013
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.model;

import eu.europa.ec.opoce.cellar.common.util.Pair;
import org.apache.jena.graph.Triple;
import org.apache.jena.rdf.model.Model;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;

/**
 * <class_description> Model that can be retrieved in several formats ("facets").
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 23-05-2013
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public class FacetedModel implements IFacetedModel {

    @SuppressWarnings("unused")
    private static final Logger LOG = LogManager.getLogger(FacetedModel.class);

    private Model model;
    private Map<String, Model> modelPerContext;
    private Map<String, Collection<Triple>> triplesPerContext;
    private Map<Pair<Triple, String>, String> quadruplePerRowid;

    public FacetedModel() {
    }

    @SuppressWarnings("unchecked")
    public FacetedModel(final Map<?, ?> data) {
        if (data != null && data.entrySet().size() > 0) {
            final Entry<?, ?> firstEntry = data.entrySet().iterator().next();
            if (firstEntry.getKey() instanceof String && firstEntry.getValue() instanceof Model) {
                this.initWithModelPerContext((Map<String, Model>) data);
            } else if (firstEntry.getKey() instanceof String && firstEntry.getValue() instanceof Map<?, ?>) {
                this.initWithTriplesPerContext((Map<String, Collection<Triple>>) data);
            } else if (firstEntry.getKey() instanceof Pair<?, ?> && firstEntry.getValue() instanceof String) {
                this.initWithQuadruplePerRowid((Map<Pair<Triple, String>, String>) data);
            }
        }
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cmr.model.IFacetedModel#asModel()
     */
    @Override
    public Model asModel() {
        return this.model;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cmr.model.IFacetedModel#asModelPerContext()
     */
    @Override
    public Map<String, Model> asModelPerContext() {
        return this.modelPerContext;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cmr.model.IFacetedModel#asTriplesPerContext()
     */
    @Override
    public Map<String, Collection<Triple>> asTriplesPerContext() {
        return this.triplesPerContext;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cmr.model.IFacetedModel#asQuadruplePerRowid()
     */
    @Override
    public Map<Pair<Triple, String>, String> asQuadruplePerRowid() {
        return this.quadruplePerRowid;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cmr.model.IFacetedModel#closeQuietly()
     */
    @Override
    public void closeQuietly() {
        ModelUtils.closeQuietly(this.model);
        ModelUtils.closeQuietly(this.modelPerContext);
    }

    private void initWithModelPerContext(final Map<String, Model> modelPerContext) {
        this.modelPerContext = modelPerContext;

        for (Entry<String, Model> entry : this.modelPerContext.entrySet()) {
            final String currContext = entry.getKey();
            final Model currModel = entry.getValue();
            this.model.add(currModel);
            this.triplesPerContext.put(currContext, ModelUtils.getTriplesFromModel(currModel));
        }
    }

    private void initWithTriplesPerContext(Map<String, Collection<Triple>> triplesPerContext) {
        this.triplesPerContext = triplesPerContext;

        this.model = ModelUtils.getModelFromTriplesPerContext(this.triplesPerContext);
        this.modelPerContext = ModelUtils.getModelPerContextFromTriplesPerContext(this.triplesPerContext);
    }

    private void initWithQuadruplePerRowid(final Map<Pair<Triple, String>, String> quadruplesPerRowid) {
        this.quadruplePerRowid = quadruplesPerRowid;

        this.model = ModelUtils.getModelFromMapQuads(quadruplesPerRowid.keySet());
        this.modelPerContext = ModelUtils.getModelsFromPairs(quadruplesPerRowid.keySet());
        this.triplesPerContext = ModelUtils.getTriplesPerContextFromModelPerContext(this.modelPerContext);
    }

}
