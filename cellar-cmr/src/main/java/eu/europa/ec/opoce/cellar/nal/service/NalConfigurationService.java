/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.nal.rework
 *             FILE : NalConfigurationService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 02 02, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-02-02 09:09:41 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.nal.service;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.RDFFormat;

import java.util.List;

/**
 * @author ARHS Developments
 */
public interface NalConfigurationService {

    List<String> getAllNalUriList();

    String getNal(String nalUri, boolean inferredNal);

    String getNal(String nalUri, boolean inferredNal, RDFFormat format);
    /**
     * Return the nal model as RDFXML
     *
     * @param nalUri
     * @param inferredNal
     * @return
     */
    Model getNalModel(String nalUri, boolean inferredNal);

    Model getAllNalModel(boolean inferred, String ontology);

}
