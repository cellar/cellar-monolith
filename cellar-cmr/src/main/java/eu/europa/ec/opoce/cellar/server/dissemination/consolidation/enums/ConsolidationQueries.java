/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.server.dissemination.consolidation.enums
 *             FILE : ConsolidationQueries.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 16, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.dissemination.consolidation.enums;

import eu.europa.ec.opoce.cellar.common.Placeholder;
import eu.europa.ec.opoce.cellar.common.util.PlaceHolderUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.regex.Pattern;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 16, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public enum ConsolidationQueries {

    /*
     * Compute original resource (URI-G) in a hierarchy
     */
    SELECT_URI_G_TEMPLATE("PREFIX cdm: <http://publications.europa.eu/ontology/cdm#> " + //
            "select distinct ?predecessor where { " + //
            "?predecessor cdm:complex_work_has_member_work* <${uri}> . " + "?predecessor ?p ?o. " + //
            "filter not exists { " + //
            "?anotherWork cdm:complex_work_has_member_work ?predecessor . }} "),

    /*
     * Determine in which dimension an evolutive work should 
     * perform datetime negotiation in (indicated by cdm:datetime_negotiation)
     */
    SELECT_DATETIME_PROPERTY_TEMPLATE("PREFIX cdm: <http://publications.europa.eu/ontology/cdm#> " + //
            "select distinct ?prop where { " + //
            "<${uri}> cdm:datetime_negotiation ?prop . } "),

    /*
     * Compute location information of next redirect based on current uri and accept-datetime parameter
     */
    SELECT_LOCATION_TEMPLATE("PREFIX cdm: <http://publications.europa.eu/ontology/cdm#> " + //
            "SELECT distinct ?successor (" + //
            "bif:datediff( 'minute', xsd:dateTime(str(?date)) ,'${accept_datetime}'^^xsd:dateTime) as ?diff_date) " + //
            "WHERE { " + //
            "<${uri}> cdm:datetime_negotiation ?datetime_property; " + //
            "cdm:complex_work_has_member_work+ ?individual_work; " + //
            "cdm:complex_work_has_member_work ?successor. " + //
            "?successor cdm:complex_work_has_member_work? ?individual_work. " + //
            "?individual_work ?datetime_property ?date. " + //
            "FILTER (xsd:dateTime(?date) <= '${accept_datetime}'^^xsd:dateTime) } " + //
            "ORDER BY ASC(?diff_date) LIMIT 1 "),

    /*
     * Perform sparql describe query for given uri
     */
    DESCRIBE_URI_TEMPLATE("PREFIX cdm: <http://publications.europa.eu/ontology/cdm#> " + //
            "DESCRIBE <${uri}> "),

    /*
     * Test whether given work is instance of cdm:complex_work
     */
    SELECT_EVOLUTIVE_WORK_TEMPLATE("PREFIX cdm: <http://publications.europa.eu/ontology/cdm#> " + //
            "SELECT ?p where { <${uri}> a <http://publications.europa.eu/ontology/cdm#evolutive_work>; ?p ?o . }"),

    /*
     * Return memento datetime of given resource (corresponds to cdm:work_date_document)
     */
    SELECT_MEMENTO_DATETIME_TEMPLATE("PREFIX cdm: <http://publications.europa.eu/ontology/cdm#> " + //
            "select ?date where " + //
            "{<${uri}> ?p ?date;" + //
            "^cdm:complex_work_has_member_work ?tg . " + //
            "?tg cdm:datetime_negotiation ?p . }"),

    /*
     * Return related complex works
     */
    SELECT_RELATED_EVOLUTIVE_WORKS_TEMPLATE("prefix cdm: <http://publications.europa.eu/ontology/cdm#> " + //
            "select distinct ?evolutive_work where {" + //
            "<${uri}> cdm:complex_work_has_member_work|^cdm:complex_work_has_member_work ?evolutive_work . " + //
            "?evolutive_work a cdm:evolutive_work . }"),

    /*
     * Return related mementos together with their memento-datetime
     */
    SELECT_RELATED_MEMENTOS_TEMPLATE("prefix cdm: <http://publications.europa.eu/ontology/cdm#> " + //
            "select distinct ?memento ?date where {" + //
            "<${uri}> cdm:complex_work_has_member_work ?memento . " + //
            "?memento cdm:work_date_creation ?date . " + //
            "filter not exists { ?memento cdm:complex_work_has_member_work ?member . }} "),

    /*
     * Return timeamp related information (startdate, enddate and type of date)
     */
    SELECT_TIMEMAP_INFO_TEMPLATE("prefix cdm: <http://publications.europa.eu/ontology/cdm#> " + //
            "select (min(?o) as ?startdate) (max(?o) as ?enddate) (?p as ?typeofdate) where {" + //
            "<${uri}> cdm:datetime_negotiation ?p;  " + //
            "cdm:complex_work_has_member_work ?member . " + //
            "?member ?p ?o . } ");

    private static final Logger LOG = LogManager.getLogger(ConsolidationQueries.class);

    private static final String URI_PLACEHOLDER = "\\$\\{uri\\}";
    private static final String ACCEPT_DATETIME_PLACEHOLDER = "\\$\\{accept_datetime\\}";

    private final static Pattern URI_PLACEHOLDER_PATTERN;
    private final static Pattern ACCEPT_DATETIME_PLACEHOLDER_PATTERN;

    /**
     * Static constructor.
     */
    static {
        URI_PLACEHOLDER_PATTERN = Pattern.compile(URI_PLACEHOLDER, Pattern.DOTALL);
        ACCEPT_DATETIME_PLACEHOLDER_PATTERN = Pattern.compile(ACCEPT_DATETIME_PLACEHOLDER, Pattern.DOTALL);
    }

    private final String queryTemplate;

    private ConsolidationQueries(final String queryTemplate) {
        this.queryTemplate = queryTemplate;
    }

    /**
     * @return the queryTemplate
     */
    public String getQueryTemplate() {
        return this.queryTemplate;
    }

    public String getQuery(final String uri) {

        String sparqlQuery = this.getQueryTemplate();

        if (uri != null) {
            final List<Placeholder> placeholders = PlaceHolderUtils.getPlaceholders(sparqlQuery, URI_PLACEHOLDER_PATTERN);
            sparqlQuery = PlaceHolderUtils.replacePlaceholders(sparqlQuery, placeholders, uri);
        }

        LOG.info("{} query (uri = {}): {}", this.name(), uri, sparqlQuery);

        return sparqlQuery;
    }

    public String getQuery(final String uri, final String dateTime) {

        String sparqlQuery = this.getQuery(uri);

        if (dateTime != null) {
            final List<Placeholder> placeholders = PlaceHolderUtils.getPlaceholders(sparqlQuery, ACCEPT_DATETIME_PLACEHOLDER_PATTERN);
            sparqlQuery = PlaceHolderUtils.replacePlaceholders(sparqlQuery, placeholders, dateTime);
        }

        LOG.info("{} query (uri = {}, dateTime = {}): {}", this.name(), uri, dateTime, sparqlQuery);

        return sparqlQuery;
    }
}
