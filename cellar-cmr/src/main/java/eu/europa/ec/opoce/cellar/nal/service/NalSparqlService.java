/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.nal.sparql
 *             FILE : NalSparqlService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 03 02, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-03-02 07:14:42 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.nal.service;

import org.apache.jena.query.Query;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import eu.europa.ec.opoce.cellar.nal.domain.NalRelatedBean;

import java.util.List;

/**
 * @author ARHS Developments
 */
public interface NalSparqlService {
    List<Query> getSkosInferenceSparqlList();

    List<Resource> getConceptsFor(Model model, String conceptScheme);

    List<Resource> getEurovocMicrothesauri(Model model);

    List<Resource> getEurovocDomains(Model model);

    List<NalRelatedBean> getBroaderTransitives(Model model, String conceptScheme);

    List<NalRelatedBean> getFacets(Model model);
}
