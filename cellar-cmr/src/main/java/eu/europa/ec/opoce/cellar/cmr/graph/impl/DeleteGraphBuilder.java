/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.graph.impl
 *             FILE : DeleteGraphBuilder.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 18, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.graph.impl;

import java.util.List;

import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 18, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class DeleteGraphBuilder extends IngestionGraphBuilder {

    /**
     * @see eu.europa.ec.opoce.cellar.cmr.graph.IGraphBuilder#buildRelations()
     */
    @Override
    public GraphBuilder buildRelations() {
        this.initializeFromObjects();
        return this.buildInverseRelationsOnSource().buildInverseRelationsOnTarget();
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cmr.graph.impl.IngestionGraphBuilder#isLoadMetadataModel()
     */
    @Override
    protected boolean isLoadMetadataModel() {
        return false;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cmr.graph.impl.IngestionGraphBuilder#checkUnknownPids(eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject, java.util.List)
     */
    @Override
    protected void checkUnknownPids(final DigitalObject digitalObject, final List<String> productionIdentifiers) {
    }

}
