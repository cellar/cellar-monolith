/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice.core
 *             FILE : StringGetterRDFVisitor.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 24 Apr 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.notice.core;

import org.apache.jena.rdf.model.AnonId;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.RDFVisitor;
import org.apache.jena.rdf.model.Resource;

/**
 * @author ARHS Developments
 */
public class StringGetterRDFVisitor implements RDFVisitor {

    private static final RDFVisitor instance = new StringGetterRDFVisitor();

    @Override
    public Object visitBlank(final Resource r, final AnonId id) {
        return "";
    }

    @Override
    public Object visitURI(final Resource r, final String uri) {
        return uri;
    }

    @Override
    public Object visitLiteral(final Literal literal) {
        return literal.getLexicalForm();
    }

    public static RDFVisitor getInstance() {
        return instance;
    }

}
