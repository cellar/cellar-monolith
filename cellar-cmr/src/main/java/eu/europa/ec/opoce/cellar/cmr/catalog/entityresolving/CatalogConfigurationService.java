package eu.europa.ec.opoce.cellar.cmr.catalog.entityresolving;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import javax.annotation.PostConstruct;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.thoughtworks.xstream.XStream;

@Service
public class CatalogConfigurationService {

    private EntityConfigurationSettings settings;

    private static final String URI_CATALOG_PATH = "cmr/resources/uri/uri-catalog-to-use.xml";

    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    /**
     * <p>initialize.</p>
     */
    @PostConstruct
    public void initialize() {
        final File cellarFolderRoot = new File(cellarConfiguration.getCellarFolderRoot());
        File catalogFile = new File(cellarFolderRoot, URI_CATALOG_PATH);
        XStream fromXml = new XStream();
        fromXml.setMode(XStream.NO_REFERENCES);
        fromXml.registerConverter(new EntitySnippetConfiguration.UriPathConverter(cellarFolderRoot));
        fromXml.alias("entity-configuration", EntityConfigurationSettings.class);
        //XStream 1.4.7 and onwards requires whitelisting deserialization entry points
        fromXml.allowTypes(new Class[]{
                eu.europa.ec.opoce.cellar.cmr.catalog.entityresolving.EntityConfigurationSettings.class
        });
        InputStream inputStream=null;
        try {
            inputStream = new FileInputStream(catalogFile);
            settings = (EntityConfigurationSettings) fromXml.fromXML(inputStream);
        } catch (FileNotFoundException e) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.ENTITY_RESOLVING_ERROR)
                    .withMessage("the basedirectory for the application is not configured corretly: {} is not found in basedirectory {}")
                    .withMessageArgs(URI_CATALOG_PATH, cellarFolderRoot).withCause(e).build();
        }
        finally{
            IOUtils.closeQuietly(inputStream);
        }
    }

    /**
     * <p>getOwlCatalog.</p>
     *
     * @return file that contains the catalog for the owl-files
     */
    public File getOwlCatalog() {
        return new File(cellarConfiguration.getCellarFolderRoot(), "cmr/resources/owl/owl-catalog.xml"); //TODO
    }

    /**
     * <p>getUriCatalog.</p>
     *
     * @param baseUri the baseUri that is used in the application
     * @return file that contains the catalog for the entity resolving, depending on the baseUri
     */
    public File getUriCatalog(String baseUri) {
        return settings.getConfiguration().getUriCatalog(baseUri);
    }
}
