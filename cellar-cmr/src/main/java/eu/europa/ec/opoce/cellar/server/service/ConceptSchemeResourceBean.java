package eu.europa.ec.opoce.cellar.server.service;

/**
 * <p>ConceptSchemeResourceBean class.</p>
 */
public class ConceptSchemeResourceBean {

    /** The concept scheme id. */
    private String conceptSchemeId;

    /**
     * Instantiates a new nal concept scheme resource bean.
     *
     * @param conceptId the concept id
     * @param conceptSchemeId the concept scheme id
     */
    public ConceptSchemeResourceBean(final String conceptSchemeId) {
        super();
        this.conceptSchemeId = conceptSchemeId;
    }

    /**
     * Gets the concept scheme id.
     *
     * @return the concept scheme id
     */
    public String getConceptSchemeId() {
        return this.conceptSchemeId;
    }

    /**
     * Sets the concept scheme id.
     *
     * @param conceptSchemeId the new concept scheme id
     */
    public void setConceptSchemeId(final String conceptSchemeId) {
        this.conceptSchemeId = conceptSchemeId;
    }

}
