package eu.europa.ec.opoce.cellar.virtuoso.impl;

import com.google.common.base.MoreObjects;
import eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;

public class VirtuosoWriteEvent {

    private final CalculatedData calculatedData;

    public VirtuosoWriteEvent(CalculatedData calculatedData) {
        this.calculatedData = calculatedData;
    }

    public CalculatedData getCalculatedData() {
        return calculatedData;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("ID", id(calculatedData.getStructMap()))
                .add("root ID", workId(calculatedData.getStructMap()))
                .toString();
    }

    private static String id(StructMap structMap) {
        return structMap != null ? structMap.getId(): "not found";
    }


    private static String workId(StructMap structMap) {
        return structMap != null && structMap.getDigitalObject() != null && structMap.getDigitalObject().getCellarId() != null ?
                structMap.getDigitalObject().getCellarId().getIdentifier() : "not found";
    }

}
