package eu.europa.ec.opoce.cellar.cmr.notice.xslt.function;

import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import eu.europa.ec.opoce.cellar.domain.content.mets.DefaultDigitalObjectTypeVisitor;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.nal.service.LanguageService;
import eu.europa.ec.opoce.cellar.nal.service.impl.LanguageServiceImpl;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import eu.europa.ec.opoce.cellar.semantic.exception.CellarSemanticException;
import eu.europa.ec.opoce.cellar.semantic.xml.DomUtils;
import net.sf.saxon.dom.DOMNodeList;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.slf4j.helpers.MessageFormatter;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static eu.europa.ec.opoce.cellar.cmr.ContentType.EMBEDDED_NOTICE;
import static eu.europa.ec.opoce.cellar.cmr.notice.xslt.function.EmbeddedNoticeRetrievalMethodType.*;
import static eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType.EXPRESSION;

/**
 * <p>XsltEmbeddedNoticeCalculator class.</p>
 */
public class XsltEmbeddedNoticeCalculator {

    private static final Logger LOG = LogManager.getLogger(XsltEmbeddedNoticeCalculator.class);

    /**
     * Component <code>cellarResourceDao</code>
     */
    private static final CellarResourceDao cellarResourceDao = ServiceLocator.getService(CellarResourceDao.class);

    private static final ContentStreamService CONTENT_STREAM_SERVICE = ServiceLocator.getService(ContentStreamService.class);

    /**
     * Component <code>languageService</code>
     */
    private static final LanguageService languageService = ServiceLocator.getService(LanguageServiceImpl.class);

    /**
     * <p>Constructor for XsltEmbeddedNoticeCalculator.</p>
     */
    private XsltEmbeddedNoticeCalculator() {
    }

    /**
     * <p>getEmbeddedNotice.</p>
     *
     * @param cellarId        a {@link java.lang.String} object.
     * @param contentLanguage a {@link java.lang.String} object.
     * @return a {@link org.w3c.dom.NodeList} object.
     */
    public static NodeList getEmbeddedNotice(final String cellarId, final String contentLanguage) {
        final List<Node> nodeList = new ArrayList<>();
        final List<CellarResource> resources = getCellarResources(cellarId, contentLanguage);
        resources.sort(Comparator.comparing(CellarResource::getCellarId));
        for (final CellarResource cellarResource : resources) {
            final NodeList childNodes = getNodes(cellarResource.getCellarId(), cellarResource.getEmbeddedNotice());
            if (childNodes != null) {
                for (int i = 0; i < childNodes.getLength(); i++) {
                    nodeList.add(childNodes.item(i));
                }
            }
        }
        return new DOMNodeList(nodeList);
    }

    /**
     * @since 8.1.0 EMBEDDED_NOTICE is now stored in S3 instead of Oracle (CLOB).
     * The call to Oracle is still needed to retrieve the version of the embedded
     * notice.
     */
    private static List<CellarResource> getCellarResources(final String cellarId, final String contentLanguage) {
        ArrayList<CellarResource> result = new ArrayList<>();
        final CellarResource askedResource = cellarResourceDao.findCellarId(cellarId);
        if (askedResource != null) {
            final DigitalObjectType cellarType = askedResource.getCellarType();
            final Collection<CellarResource> accept = cellarType
                    .accept(new DefaultDigitalObjectTypeVisitor<Object, Collection<CellarResource>>() {

                        @Override
                        public Collection<CellarResource> visitAgent(final Object in) {
                        	return cellarResourceDao.retrieveWithEmbeddedNoticeUsingCache(SINGLE_RESOURCE, cellarId);
                        }

                        @Override
                        public Collection<CellarResource> visitTopLevelEvent(final Object in) {
                        	return cellarResourceDao.retrieveWithEmbeddedNoticeUsingCache(SINGLE_RESOURCE, cellarId);
                        }

                        @Override
                        public Collection<CellarResource> visitWork(final Object in) {
                            if (!askedResource.isUnderEmbargo()) {
                                final Collection<CellarResource> expressions = getExpressionsFallbackAware(cellarId, contentLanguage);
                                // TODO: dv: cache mechanism for language-related queries

                                if ((expressions == null) || expressions.isEmpty()) {
                                	return cellarResourceDao.retrieveWithEmbeddedNoticeUsingCache(SINGLE_RESOURCE, cellarId);
                                }
                                return expressions.stream()
                                		.map(e -> cellarResourceDao.retrieveWithEmbeddedNoticeUsingCache(SPECIFIC_HIERARCHY, e.getCellarId()))
                                        .flatMap(Collection::stream)
                                        .collect(Collectors.toSet());
                            } else {
                                return Collections.emptyList();
                            }
                        }

                        @Override
                        public Collection<CellarResource> visitExpression(final Object in) {
                        	return cellarResourceDao.retrieveWithEmbeddedNoticeUsingCache(SPECIFIC_HIERARCHY, cellarId);
                        }

                        @Override
                        public Collection<CellarResource> visitManifestation(final Object in) {
                        	return cellarResourceDao.retrieveWithEmbeddedNoticeUsingCache(SPECIFIC_HIERARCHY, cellarId);
                        }

                        @Override
                        public Collection<CellarResource> visitItem(final Object in) {
                            return Collections.emptyList();
                        }

                        @Override
                        public Collection<CellarResource> visitDossier(final Object in) {
                        	return cellarResourceDao.retrieveWithEmbeddedNoticeUsingCache(STARTING_WITH, cellarId);
                        }

                        @Override
                        public Collection<CellarResource> visitEvent(final Object in) {
                        	return cellarResourceDao.retrieveWithEmbeddedNoticeUsingCache(STARTING_WITH, cellarId.substring(0, cellarId.indexOf('.')));
                        }
                    }, null);
            result = new ArrayList<>(accept);
        }
        return result;
    }

    // TODO use
    private static CellarResource addEmbedded(CellarResource resource) {
        String embeddedNotice = CONTENT_STREAM_SERVICE.getContentAsString(resource.getCellarId(), resource.getVersion(EMBEDDED_NOTICE)
                .orElse(null), EMBEDDED_NOTICE)
                .orElse("");
        // Enrich the current resource
        resource.setEmbeddedNotice(embeddedNotice);
        return resource;
    }

    /**
     * <p>getNodes.</p>
     *
     * @param cellarId a {@link java.lang.String} object.
     * @param notice   a {@link java.lang.String} object.
     * @return a {@link org.w3c.dom.NodeList} object.
     */
    private static NodeList getNodes(final String cellarId, final String notice) {
        if (StringUtils.isEmpty(notice)) {
            return DomUtils.getDocumentBuilder().newDocument().getChildNodes();
        }
        try {
            final Document document = DomUtils.read(notice);
            return document.getChildNodes();
        } catch (final CellarSemanticException ex) {
            LOG.warn(MessageFormatter.format("could not read embedded notice part for: {}", cellarId).getMessage(), ex);
            return DomUtils.getDocumentBuilder().newDocument().getChildNodes();
        }
    }

    // TODO: this can be improved by a *GENERAL, NOT CUSTOMIZED* database caching mechanism (see CELLAR-71)
    private static Collection<CellarResource> getExpressionsFallbackAware(final String cellarId, final String contentLanguage) {
        Collection<CellarResource> expressions = getExpressions(cellarId, contentLanguage);
        if ((expressions == null) || expressions.isEmpty()) {
        	Map<String, List<CellarResource>> languagesToCellarResources = new HashMap<>();
        	Collection<CellarResource> allExpressions = cellarResourceDao.findExpressions(cellarId);
        	for (CellarResource expression : allExpressions) {
        		for (String language : expression.getLanguages()) {
        			languagesToCellarResources.computeIfAbsent(language, k -> new ArrayList<>()).add(expression);
        		}
        	}
        	
            final LanguageBean languageBean = languageService.getByThreeChar(contentLanguage);
            final List<String> fallbackLanguageUris = languageService.getFallbackLanguagesFor(languageBean.getUri());
            for (final String fallbackLanguageUri : fallbackLanguageUris) {
                final String fallbackLanguage = languageService.getByUri(fallbackLanguageUri).getIsoCodeThreeChar();
                expressions = languagesToCellarResources.get(fallbackLanguage);
                if ((expressions != null) && !expressions.isEmpty()) {
                	return expressions;
                }
            }
        }
        return expressions;
    }
    
    /**
     * Returns the expression belonging to the tree that the provided CellarID is part of,
     * whose language matches the provided content language.
     * 
     * @param cellarId the CellarID whose tree will be checked.
     * @param contentLanguage the content language to look for.
     * @return the expression matching the requested language.
     */
    private static List<CellarResource> getExpressions(final String cellarId, final String contentLanguage) {
    		return cellarResourceDao.findResourcesWithTypeAndLanguage(cellarId, EXPRESSION, contentLanguage).stream()
    				.collect(Collectors.toList());
    }
    
}
