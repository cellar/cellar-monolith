/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.ontology
 *        FILE : ModelUtils.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 23-05-2013
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.model;

import eu.europa.ec.opoce.cellar.common.util.Pair;

import java.util.Collection;
import java.util.Map;

import org.apache.jena.graph.Triple;
import org.apache.jena.rdf.model.Model;

/**
 * <class_description> Model that can be retrieved in several formats.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 23-05-2013
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IFacetedModel {

    Model asModel();

    Map<String, Model> asModelPerContext();

    Map<String, Collection<Triple>> asTriplesPerContext();

    Map<Pair<Triple, String>, String> asQuadruplePerRowid();

    void closeQuietly();

}
