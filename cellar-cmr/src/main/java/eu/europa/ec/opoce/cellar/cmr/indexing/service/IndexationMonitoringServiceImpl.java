/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.indexing.service
 *             FILE : IndexationMonitoringServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 08 03, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-08-03 11:32:46 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.indexing.service;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import eu.europa.ec.opoce.cellar.cmr.indexing.IndexationMonitoringService;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestBatch;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestsCount;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration.INDEXATION;

/**
 * This class is thread-safe.
 *
 * @author ARHS Developments
 */
@Service
public class IndexationMonitoringServiceImpl implements IndexationMonitoringService {

    private static final Logger LOG = LogManager.getLogger(IndexationMonitoringServiceImpl.class);

    private final ConcurrentMap<String, CmrIndexRequestBatch> inProgressRequests = new ConcurrentHashMap<>();
    private final LoadingCache<CmrIndexRequestBatch, CmrIndexRequestsCount> history = CacheBuilder.newBuilder()
            .expireAfterAccess(60, TimeUnit.SECONDS)
            .build(new CacheLoader<CmrIndexRequestBatch, CmrIndexRequestsCount>() {
                @Override
                public CmrIndexRequestsCount load(CmrIndexRequestBatch cmrIndexRequest) throws Exception {
                    return toIndexRequestCount(cmrIndexRequest, new Date());
                }
            });

    @Override
    public void register(CmrIndexRequestBatch cmrIndexRequest) {
        inProgressRequests.put(cmrIndexRequest.getGroupByUri(), cmrIndexRequest);
    }

    @Override
    public void unregister(CmrIndexRequestBatch cmrIndexRequest) {
        inProgressRequests.remove(cmrIndexRequest.getGroupByUri());
        history.getUnchecked(cmrIndexRequest);
    }

    @Override
    public List<CmrIndexRequestsCount> getInProgressRequests(int limit) {
        return inProgressRequests.values().stream()
                .map(c -> toIndexRequestCount(c, null))
                .sorted(Comparator.comparing(CmrIndexRequestsCount::getPriority))
                .limit(limit)
                .peek(c -> LOG.debug(INDEXATION, "In progress = {}", c))
                .collect(Collectors.toList());
    }

    @Override
    public List<CmrIndexRequestsCount> getWaitingGroup(int limit) {
        return Collections.emptyList();
    }

    @Override
    public List<CmrIndexRequestsCount> getHistory(int limit) {
        return history.asMap().values().stream()
                .sorted(Comparator.comparing(CmrIndexRequestsCount::getDoneAt))
                .limit(limit)
                .peek(c -> LOG.debug(INDEXATION,"History = {}", c))
                .collect(Collectors.toList());
    }

    private static CmrIndexRequestsCount toIndexRequestCount(CmrIndexRequestBatch request, Date date) {
        CmrIndexRequestsCount count = new CmrIndexRequestsCount();
        count.setPriority(request.getPriority().getPriorityValue());
        count.setGroupURI(request.getGroupByUri());
        count.setCreatedAt(request.getCreatedOn());
        count.setDoneAt(date);
        return count;
    }
}
