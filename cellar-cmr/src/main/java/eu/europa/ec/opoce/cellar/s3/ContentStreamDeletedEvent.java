package eu.europa.ec.opoce.cellar.s3;

import eu.europa.ec.opoce.cellar.cmr.ContentType;

public class ContentStreamDeletedEvent {

    private final String cellarID;
    private final String version;
    private final ContentType contentType;

    public ContentStreamDeletedEvent(String cellarID, String version, ContentType contentType) {
        this.cellarID = cellarID;
        this.version = version;
        this.contentType = contentType;
    }

    public String getCellarID() {
        return cellarID;
    }

    public String getVersion() {
        return version;
    }

    public ContentType getContentType() {
        return contentType;
    }
}
