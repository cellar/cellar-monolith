/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.modelload.service
 *             FILE : ModelLoadRequestProcessingService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 01 10, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-01-10 09:16:28 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.modelload.service;

import eu.europa.ec.opoce.cellar.modelload.domain.ModelLoadRequest;

/**
 * @author ARHS Developments
 */
public interface ModelLoadRequestProcessingService {
    void process(ModelLoadRequest modelLoadRequest);
}
