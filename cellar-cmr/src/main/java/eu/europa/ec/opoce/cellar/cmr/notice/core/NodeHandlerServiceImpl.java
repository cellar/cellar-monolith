/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.notice.core
 *             FILE : NodeHandlerUtils.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 24 Apr 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cmr.notice.core;

import eu.europa.ec.opoce.cellar.cmr.utils.XmlNoticeUtils;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaQueries;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import eu.europa.ec.opoce.cellar.semantic.xml.XmlBuilder;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.RDF;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author ARHS Developments
 */
@Service
public class NodeHandlerServiceImpl implements NodeHandlerService {

    private Rdf2XmlService rdf2XmlServiceImpl;

    @Autowired
    private NodeHandlerServiceImpl(final Rdf2XmlService rdf2XmlServiceImpl) {
        this.rdf2XmlServiceImpl = rdf2XmlServiceImpl;
    }

    @Override
    public void writeMemberList(final NodeHandler personalNodeHandler, final XmlBuilder xmlbuilder, final Resource resource,
            final Property currentProperty) {
        Resource rest = handleFirstAndReturnRest(personalNodeHandler, xmlbuilder, resource, currentProperty);
        while (!rest.equals(RDF.nil)) {
            rest = handleFirstAndReturnRest(personalNodeHandler, xmlbuilder, rest, currentProperty);
        }
    }

    @Override
    public void writeConceptAllLevelsWithAnnotations(final XmlBuilder xmlBuilder, final Property property, final Resource resource,
            Map<LanguageBean, List<LanguageBean>> cache, Set<LanguageBean> languages, NoticeType noticeType, Model model,
            Set<Resource> allSubjectResources) {
        xmlBuilder.attribute("type", "concept_level");
        writeConceptNumberedLevelWithPossibleAnnotations(xmlBuilder, property, resource, cache, languages, noticeType, model,
                allSubjectResources);

        final List<Resource> broaderResources = JenaQueries.getResources(resource, CellarProperty.skos_broaderTransitiveP, false);
        for (final Resource broaderResource : broaderResources) {
            writeConceptNumberedLevelWithPossibleAnnotations(xmlBuilder, property, broaderResource, cache, languages, noticeType, model,
                    allSubjectResources);
        }
    }

    private Resource handleFirstAndReturnRest(final NodeHandler personalNodeHandler, final XmlBuilder xmlbuilder,
                                              final Resource resource, final Property currentProperty) {
        final Resource first = JenaQueries.getResource(resource, RDF.first, true, true);
        personalNodeHandler.setCurrent(xmlbuilder, currentProperty, RDF.first);
        first.visitWith(personalNodeHandler);
        final Resource restResource = JenaQueries.getResource(resource, RDF.rest, true, true);
        return restResource;
    }

    private void writeConceptNumberedLevelWithPossibleAnnotations(final XmlBuilder xmlBuilder, final Property property,
            final Resource resource, Map<LanguageBean, List<LanguageBean>> cache, Set<LanguageBean> languages, NoticeType noticeType,
            Model model, Set<Resource> allSubjectResources) {
        final String tagName = XmlNoticeUtils.tagize(property);

        final Statement statement = resource.getProperty(CellarProperty.cmr_levelP);
        final String s = statement == null ? "" : statement.getLiteral().getLexicalForm();
        final XmlBuilder conceptBuilder = xmlBuilder.child(tagName + '_' + s);
        writeConcept(conceptBuilder, resource, cache, languages, noticeType);
        addAnnotations(conceptBuilder, property, resource, model, allSubjectResources);
    }

    @Override
    public void writeConcept(final XmlBuilder conceptBuilder, final Resource conceptResource, Map<LanguageBean, List<LanguageBean>> cache,
            Set<LanguageBean> languages, NoticeType noticeType) {
        rdf2XmlServiceImpl.writeConcept(conceptBuilder, conceptResource, cache, languages, noticeType);
    }

    @Override
    public void addAnnotations(final XmlBuilder parentBuilder, final Property property, final RDFNode object,
                               final Model model, final Set<Resource> allSubjectResources) {
        rdf2XmlServiceImpl.writeAnnotation(parentBuilder, property, object, model, allSubjectResources);
    }
}
