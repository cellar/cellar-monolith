/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.virtuoso.util
 *        FILE : BackupVirtuosoFailureBuilder.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 01-08-2015
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.virtuoso.util;

import eu.europa.ec.opoce.cellar.CmrErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.ExceptionUtils;
import eu.europa.ec.opoce.cellar.exception.VirtuosoBackupException;
import eu.europa.ec.opoce.cellar.exception.VirtuosoException;
import eu.europa.ec.opoce.cellar.exception.VirtuosoOperationException;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.virtuosobackup.BackupType;
import eu.europa.ec.opoce.cellar.virtuosobackup.VirtuosoBackup.OperationType;
import eu.europa.ec.opoce.cellar.virtuosobackup.service.IVirtuosoBackupService;

import java.util.*;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.jena.rdf.model.Model;

/**
 * <class_description> General purpose closure with no parameters and void return type.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 01-08-2015
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public class BackupVirtuosoFailureBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(BackupVirtuosoFailureBuilder.class);

    private static final IVirtuosoBackupService virtuosoBackupService = ServiceLocator.getService(IVirtuosoBackupService.class);

    private static final ICellarConfiguration cellarConfiguration = ServiceLocator.getService("cellarConfiguration",
            ICellarConfiguration.class);

    private final Exception causeOfVirtuosoFailure;
    private final String virtuosoStackTrace;
    private final boolean isVirtuosoNotAvailable;

    /** Collection containing the cellar IDs of the digital objects to drop from Virtuoso */
    private Collection<String> digitalObjectsToDrop;
    /** Collection containing the URIs of the NALs to drop from Virtuoso */
    private Collection<String> nalsToDrop;
    /** Collection containing the URIs of the ontologies to drop from Virtuoso */
    private Collection<String> ontologiesToDrop;
    /** Map containing the digital objects to insert into Virtuoso */
    private Map<DigitalObject, Model> digitalObjectsToInsert;
    /** Map containing the ontologies to insert into Virtuoso */
    private Map<String, Model> ontologiesToInsert;
    /** Map containing the SKOS to insert into Virtuoso */
    private Map<String, Model> skosToInsert;
    /** Map containing the NALs to insert into Virtuoso */
    private Map<String, Model> nalsToInsert;
    /** Map containing the rule set queries to apply after inserting an ontology or NAL into Virtuoso */
    private Map<String, String> ruleSetQueries;

    private Collection<String> sparqlLoadModelToDrop;

    private Map<String,Model> sparqlLoadModelToInsert;

    /**
     * Normalization session instance.
     */
    private VirtuosoNormalizer virtuosoNormalizer;

    /**
     * Gets a new builder for conveniently backing up.
     * 
     * @param causeOfVirtuosoFailure the exception thrown at Virtuoso failure
     * @return the builder newly created
     */
    public static BackupVirtuosoFailureBuilder get(final Exception causeOfVirtuosoFailure) {
        return new BackupVirtuosoFailureBuilder(causeOfVirtuosoFailure);
    }

    /**
     * Instantiates a new notice builder. It is private in order to avoid direct instantiation.
     */
    private BackupVirtuosoFailureBuilder(final Exception causeOfVirtuosoFailure) {
        if (causeOfVirtuosoFailure == null) {
            throw ExceptionBuilder.get(VirtuosoException.class)
                    .withMessage("Cannot instantiate a builder for backing up Virtuoso failures without a proper exception.").build();
        }

        this.causeOfVirtuosoFailure = causeOfVirtuosoFailure;
        this.virtuosoStackTrace = ExceptionUtils.getThrowablesHeaders(this.causeOfVirtuosoFailure);
        this.isVirtuosoNotAvailable = VirtuosoUtils.isVirtuosoNotAvailable(this.causeOfVirtuosoFailure);
    }

    /**
     * Sets the digital objects to drop.
     *
     * @param digitalObjectsToDrop the digital objects to drop
     * @return the builder
     */
    public BackupVirtuosoFailureBuilder digitalObjectsToDrop(final Collection<String> digitalObjectsToDrop) {
        this.digitalObjectsToDrop = digitalObjectsToDrop;
        return this;
    }

    /**
     * Sets the URIs of the ontologies to drop.
     * @param ontoUris the URIs of the ontologies to drop
     * @return a builder instance
     */
    public BackupVirtuosoFailureBuilder ontologiesToDrop(final Collection<String> ontoUris) {

        this.ontologiesToDrop = ontoUris;
        return this;
    }

    /**
     * Sets the URIs of the NALs to drop.
     * @param nalUris the URIs of the NALs to drop
     * @return a builder instance
     */
    public BackupVirtuosoFailureBuilder nalsToDrop(final Collection<String> nalUris) {

        this.nalsToDrop = nalUris;
        return this;
    }

    public BackupVirtuosoFailureBuilder sparqlLoadModelToDrop(final Collection<String> loadUris){
        this.sparqlLoadModelToDrop =loadUris;
        return this;
    }

    /**
     * Sets the digital objects to insert.
     *
     * @param digitalObjectsToInsert the digital objects to insert
     * @return the builder instance
     */
    public BackupVirtuosoFailureBuilder digitalObjectsToInsert(final Map<DigitalObject, Model> digitalObjectsToInsert,
            final VirtuosoNormalizer virtuosoNormalizer) {
        this.digitalObjectsToInsert = digitalObjectsToInsert;
        this.virtuosoNormalizer = virtuosoNormalizer;
        return this;
    }

    /**
     * Sets the ontologies to insert
     * @param ontologiesToInsert the ontologies to insert
     * @return the builder instance
     */
    public BackupVirtuosoFailureBuilder ontologiesToInsert(final Map<String, Model> ontologiesToInsert) {

        this.ontologiesToInsert = ontologiesToInsert;
        return this;
    }

    /**
     * Sets the skos to insert
     * @param skosToInsert the skos to insert
     * @return the builder instance
     */
    public BackupVirtuosoFailureBuilder skosToInsert(final Map<String, Model> skosToInsert) {

        this.skosToInsert = skosToInsert;
        return this;
    }

    /**
     * Sets the NALs to insert
     * @param nalsToInsert the NALs to insert
     * @return the builder instance
     */
    public BackupVirtuosoFailureBuilder nalsToInsert(final Map<String, Model> nalsToInsert) {

        this.nalsToInsert = nalsToInsert;
        return this;
    }

    public BackupVirtuosoFailureBuilder sparqlLoadModelToInsert(final Map<String,Model> sparqlLoadModelToInsert){
        this.sparqlLoadModelToInsert =sparqlLoadModelToInsert;
        return this;
    }

    /**
     * Sets the rule set queries to execute after insertion of the object into Virtuoso
     * @param ruleSetQueries the rule set queries to execute
     * @return the builder instance
     */
    public BackupVirtuosoFailureBuilder ruleSetQueriesToExecute(final Map<String, String> ruleSetQueries) {

        this.ruleSetQueries = ruleSetQueries;
        return this;
    }

    public void execute() throws VirtuosoException {
        // if the exception originated from a backup operation, do not try to backup again!
        if (this.getCauseOfVirtuosoFailure() instanceof VirtuosoBackupException) {
            throw (VirtuosoBackupException) this.getCauseOfVirtuosoFailure();
        }

        // the backup is enabled
        if (cellarConfiguration.isVirtuosoIngestionBackupEnabled()) {
            // try to backup
            try {
                this.populateBackupWithDrops();
                this.populateBackupWithInserts();
            }
            // the backup has failed: let's log it
            catch (VirtuosoBackupException backupException) {
                LOG.warn("The backup operation has failed: this will cause a misalignment between Virtuoso and the rest of the system.",
                        backupException);
            }
        }
        // the backup is not enabled: let's log it
        else {
            LOG.warn("The backup is not enabled: this will cause a misalignment between Virtuoso and the rest of the system.");
        }

        // throws up the original exception wrapped in a VirtuosoOperationException, in order to trigger the rollback on Virtuoso
        throw ExceptionBuilder.get(VirtuosoOperationException.class).withCode(CmrErrors.VIRTUOSO_OPERATION_FAILED)
                .withCause(this.getCauseOfVirtuosoFailure()).build();
    }

    private void populateBackupWithDrops() throws VirtuosoBackupException {
        if (digitalObjectsToDrop != null) {
            populateBackupWithDODrops();
        }
        if (ontologiesToDrop != null) {
            populateBackupWithOntologyDrops();
        }
        if (nalsToDrop != null) {
            populateBackupWithNalDrops();
        }

        if(sparqlLoadModelToDrop != null){
            populateBackupWithSparqlLoadDrops();
        }

    }

    private void populateBackupWithDODrops() throws VirtuosoBackupException {

        for (final String cellarIdToDrop : digitalObjectsToDrop) {
            virtuosoBackupService.populateVirtuosoBackup(cellarIdToDrop, BackupType.DIGITAL_OBJECT, null, null, new Date(),
                    OperationType.DROP, isVirtuosoNotAvailable, virtuosoStackTrace);
        }
    }

    private void populateBackupWithOntologyDrops() throws VirtuosoBackupException {

        for (final String ontoUri : ontologiesToDrop) {
            virtuosoBackupService.populateVirtuosoBackup(ontoUri, BackupType.ONTOLOGY, null, null, new Date(), OperationType.DROP,
                    isVirtuosoNotAvailable, virtuosoStackTrace);
        }
    }

    private void populateBackupWithNalDrops() throws VirtuosoBackupException {

        for (final String nalUri : nalsToDrop) {
            virtuosoBackupService.populateVirtuosoBackup(nalUri, BackupType.NAL, null, null, new Date(), OperationType.DROP,
                    isVirtuosoNotAvailable, virtuosoStackTrace);
        }
    }

    private void populateBackupWithInserts() throws VirtuosoBackupException {

        if (digitalObjectsToInsert != null) {
            populateBackupWithDOInserts();
        }
        if (ontologiesToInsert != null) {
            populateBackupWithOntologyInserts();
        }
        if (nalsToInsert != null) {
            populateBackupWithNalInserts();
        }
        if(sparqlLoadModelToInsert !=null){
            populateBackupWithSparqlLoadInserts();
        }
    }

    private void populateBackupWithDOInserts() throws VirtuosoBackupException {

        // get direct model of digital objects that are supposed to be inserted
        for (final Entry<DigitalObject, Model> currEntry : this.digitalObjectsToInsert.entrySet()) {
            final String cellarId = currEntry.getKey().getCellarId().getIdentifier();
            final Model nonNormalizedModel = currEntry.getValue();
            String string = JenaUtils.toString(nonNormalizedModel);
            Set<String> nonNormalizedUris = this.virtuosoNormalizer.getNonNormalizedUris(currEntry.getKey());
            virtuosoBackupService.populateVirtuosoBackup(cellarId, BackupType.DIGITAL_OBJECT, string, nonNormalizedUris, new Date(),
                    OperationType.INSERT, this.isVirtuosoNotAvailable, this.virtuosoStackTrace);
        }
    }

    private void populateBackupWithOntologyInserts() throws VirtuosoBackupException {

        for (final Entry<String, Model> entry : ontologiesToInsert.entrySet()) {
            final String ontoUri = entry.getKey();
            final String stringifiedModel = JenaUtils.toString(entry.getValue());

            String stringifiedSkosModel = null;
            String ruleSetQuery = null;

            if (skosToInsert != null && skosToInsert.keySet().contains(ontoUri)) {
                stringifiedSkosModel = JenaUtils.toString(skosToInsert.get(ontoUri));
            }

            if (ruleSetQueries != null && ruleSetQueries.keySet().contains(ontoUri)) {
                ruleSetQuery = ruleSetQueries.get(ontoUri);
            }

            virtuosoBackupService.populateVirtuosoBackup(ontoUri, BackupType.ONTOLOGY, stringifiedModel, stringifiedSkosModel, ruleSetQuery,
                    new Date(), OperationType.INSERT, isVirtuosoNotAvailable, virtuosoStackTrace);
        }
    }

    private void populateBackupWithNalInserts() throws VirtuosoBackupException {

        for (final Entry<String, Model> entry : this.nalsToInsert.entrySet()) {
            final String nalUri = entry.getKey();
            final String stringifiedModel = JenaUtils.toString(entry.getValue());

            String stringifiedSkosModel = null;
            String ruleSetQuery = null;

            if (skosToInsert != null && skosToInsert.keySet().contains(nalUri)) {
                stringifiedSkosModel = JenaUtils.toString(skosToInsert.get(nalUri));
            }

            if (ruleSetQueries != null && ruleSetQueries.keySet().contains(nalUri)) {
                ruleSetQuery = ruleSetQueries.get(nalUri);
            }

            virtuosoBackupService.populateVirtuosoBackup(nalUri, BackupType.NAL, stringifiedModel, stringifiedSkosModel, ruleSetQuery,
                    new Date(), OperationType.INSERT, isVirtuosoNotAvailable, virtuosoStackTrace);
        }
    }
    private void populateBackupWithSparqlLoadInserts() throws VirtuosoBackupException {

        for (final Entry<String, Model> entry : this.sparqlLoadModelToInsert.entrySet()) {
            final String loadUri = entry.getKey();
            final String stringifiedModel = JenaUtils.toString(entry.getValue());

            String stringifiedSkosModel = null;
            String ruleSetQuery = null;

            if (skosToInsert != null && skosToInsert.keySet().contains(loadUri)) {
                stringifiedSkosModel = JenaUtils.toString(skosToInsert.get(loadUri));
            }

            if (ruleSetQueries != null && ruleSetQueries.keySet().contains(loadUri)) {
                ruleSetQuery = ruleSetQueries.get(loadUri);
            }

            virtuosoBackupService.populateVirtuosoBackup(loadUri, BackupType.SPARQL_LOAD, stringifiedModel, stringifiedSkosModel, ruleSetQuery,
                    new Date(), OperationType.INSERT, isVirtuosoNotAvailable, virtuosoStackTrace);
        }
    }
    private void populateBackupWithSparqlLoadDrops() throws VirtuosoBackupException{
        for (final String loadUri : sparqlLoadModelToDrop) {
            virtuosoBackupService.populateVirtuosoBackup(loadUri, BackupType.SPARQL_LOAD, null, null, new Date(), OperationType.DROP,
                    isVirtuosoNotAvailable, virtuosoStackTrace);
        }
    }

    public Exception getCauseOfVirtuosoFailure() {
        return causeOfVirtuosoFailure;
    }

    public String getVirtuosoStackTrace() {
        return virtuosoStackTrace;
    }

    public boolean isVirtuosoNotAvailable() {
        return isVirtuosoNotAvailable;
    }

}
