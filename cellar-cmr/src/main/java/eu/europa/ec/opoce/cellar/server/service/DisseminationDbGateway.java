/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : CellarIdentifier.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 24 mars 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.server.service;

import eu.europa.ec.opoce.cellar.ccr.service.LanguagesNalSkosLoaderService;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Agent;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Content;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Dossier;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Event;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Expression;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.HierarchyElement;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Manifestation;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.TopLevelEvent;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Work;
import eu.europa.ec.opoce.cellar.cmr.transformers.IscoCode2LanguageBean;
import eu.europa.ec.opoce.cellar.domain.content.mets.CellarIdentifiedObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.monitoring.Watch;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaQueries;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarProperty;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * The Class DisseminationDbGateway.
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 24 mars 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class DisseminationDbGateway {

    private static final Logger LOG = LoggerFactory.getLogger(DisseminationDbGateway.class);

    private final LanguagesNalSkosLoaderService languagesNalSkosLoaderService;

    private final CellarResourceDao cellarResourceDao;

    private final IscoCode2LanguageBean iscoCode2LanguageBean;

    private final ContentStreamService contentStreamService;

    private final IdentifierService identifierService;

    @Autowired
    public DisseminationDbGateway(LanguagesNalSkosLoaderService languagesNalSkosLoaderService, CellarResourceDao cellarResourceDao,
                                  ContentStreamService contentStreamService, @Qualifier("pidManagerService") IdentifierService identifierService) {
        this.languagesNalSkosLoaderService = languagesNalSkosLoaderService;
        this.cellarResourceDao = cellarResourceDao;
        this.contentStreamService = contentStreamService;
        this.identifierService = identifierService;
        this.iscoCode2LanguageBean = new IscoCode2LanguageBean(this.languagesNalSkosLoaderService);
    }

    public CellarResource getCellarResource(final String cellarId) {
        final CellarResource resource = checkForResourceWithId(cellarId);
        if (resource == null) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                    .withMessage("Resource with id '{}' not found").withMessageArgs(cellarId).build();
        }

        return resource;
    }

    @Watch(value = "Find cellar resource", arguments = {
            @Watch.WatchArgument(name = "cellarId", expression = "cellarId")
    })
    public CellarResource checkForResourceWithId(final String cellarId) {
        if (!StringUtils.startsWith(cellarId, "cellar:")) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                    .withMessage("Invalid cellar id specified: '{}'").withMessageArgs(cellarId).build();
        }
        return this.cellarResourceDao.findCellarId(cellarId);
    }

    public DigitalObjectType getType(final String cellarId) {
        return getCellarResource(cellarId).getCellarType();
    }

    public MetsElement getCompleteTree(final String cellarId) {
        return getCompleteTree(cellarId, DigitalObjectType.WORK, DigitalObjectType.EXPRESSION,
                DigitalObjectType.MANIFESTATION, DigitalObjectType.ITEM);
    }

    /**
     * Gets the complete tree.
     *
     * @param cellarId           the cellar id
     * @param digitalObjectTypes the digital object types
     * @return the complete tree
     */
    public MetsElement getCompleteTree(final String cellarId, final DigitalObjectType... digitalObjectTypes) {
        if (!cellarId.startsWith("cellar:")) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                    .withMessage("Invalid cellar id specified: '{}'").withMessageArgs(cellarId).build();
        }

        final List<CellarResource> resources = this.cellarResourceDao.findTreeSortByCellarId(cellarId);

        if (resources.isEmpty()) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                    .withMessage("Resource id '{}' is not found.").withMessageArgs(cellarId).build();
        }

        switch (resources.get(0).getCellarType()) { // check the root type
            case WORK:
                return this.getWorkTree(resources, digitalObjectTypes);
            case AGENT:
                return createAgent(resources.get(0));
            case TOPLEVELEVENT:
                return createTopLevelEvent(resources.get(0));
            case DOSSIER:
                return getCompleteDossierTree(resources);
            default:
                throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                        .withMessage("The object '{}' is of type '{}', which is not a valid root element for a tree hierarchy.")
                        .withMessageArgs(cellarId, resources.get(0).getCellarType()).build();
        }
    }

    /**
     * Gets the work tree.
     *
     * @param resources          the resources
     * @param digitalObjectTypes the digital object types
     * @return the work tree
     */
    public Work getWorkTree(final Collection<CellarResource> resources, final DigitalObjectType... digitalObjectTypes) {
        Work work = null;
        Expression expression = null;
        Manifestation manifestation = null;
        final List<DigitalObjectType> includingTypes = Arrays.asList(digitalObjectTypes);
        final boolean includeWork = includingTypes.contains(DigitalObjectType.WORK);
        final boolean includeExpression = includingTypes.contains(DigitalObjectType.EXPRESSION);
        final boolean includeManifestion = includingTypes.contains(DigitalObjectType.MANIFESTATION);
        final boolean includeItems = includingTypes.contains(DigitalObjectType.ITEM);
        for (final CellarResource resource : resources) {
            final DigitalObjectType cellarType = resource.getCellarType();
            if (includeWork && (cellarType == DigitalObjectType.WORK)) {
                work = createWork(resource);
            } else if (includeExpression && (cellarType == DigitalObjectType.EXPRESSION)) {
                expression = createExpressionWithRelations(resource, work);
            } else if (includeManifestion && (cellarType == DigitalObjectType.MANIFESTATION)) {
                manifestation = createManifestationWithRelations(resource, expression);
            } else if (includeItems && (cellarType == DigitalObjectType.ITEM)) {
                createContentWithRelations(resource, manifestation);
            }
        }

        return work;
    }

    /**
     * Gets the work tree for indexation.
     *
     * @param cellarId the cellar id
     * @return the work tree for indexation
     */
    public Work getWorkTreeForIndexation(final String cellarId) {
        return this.getWorkTree(cellarId, true);
    }

    /**
     * Gets the work tree.
     *
     * @param cellarId the cellar id
     * @return the work tree
     */
    public Work getWorkTree(final String cellarId) {
        return this.getWorkTree(cellarId, false);
    }

    /**
     * <p>getWorkTree.</p>
     *
     * @param cellarId      a {@link java.lang.String} object.
     * @param forIndexation the for indexation
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Work} object.
     */
    private Work getWorkTree(final String cellarId, final boolean forIndexation) {
        if (!cellarId.startsWith("cellar:")) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                    .withMessage("Invalid cellar id specified: '{}'").withMessageArgs(cellarId).build();
        }

        final Collection<CellarResource> resources = this.cellarResourceDao.findTree(cellarId);

        if (resources.isEmpty()) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                    .withMessage("Resource id '{}' is not found.").withMessageArgs(cellarId).build();
        }

        Work work = null;
        final Set<Expression> expressions = new HashSet<Expression>();
        final Set<Manifestation> manifestations = new HashSet<Manifestation>();

        for (final CellarResource resource : resources) {
            final DigitalObjectType cellarType = resource.getCellarType();
            if (cellarType == DigitalObjectType.WORK) {
                work = createWork(resource);
                if (forIndexation) {
                    extractDoNotIndexProperty(work);
                }
            } else if (cellarType == DigitalObjectType.EXPRESSION) {
                final Expression createExpression = createExpression(resource);
                boolean do_not_index = false;
                if (forIndexation) {
                    do_not_index = extractDoNotIndexProperty(createExpression);
                }
                if (!do_not_index) {
                    expressions.add(createExpression);
                }
            } else if (cellarType == DigitalObjectType.MANIFESTATION) {
                final Manifestation createManifestation = createManifestation(resource);
                boolean do_not_index = false;
                if (forIndexation) {
                    do_not_index = extractDoNotIndexProperty(createManifestation);
                }
                if (!do_not_index) {
                    manifestations.add(createManifestation);
                }
            }
        }

        if ((null != work) && !work.isDoNotIndex()) {
            work.addChildren(expressions);
            for (final Expression expression : expressions) {
                expression.setParent(work);
                final Set<Manifestation> manifestationsPerExpression = new HashSet<Manifestation>();
                for (final Manifestation manifestation : manifestations) {
                    final ContentIdentifier manifestationCellarId = manifestation.getCellarId();
                    final String manifestationIdentifier = manifestationCellarId.getIdentifier();
                    final ContentIdentifier expressionCellarId = expression.getCellarId();
                    final String expressionIdentifier = expressionCellarId.getIdentifier();
                    if (manifestationIdentifier.startsWith(expressionIdentifier)) {
                        manifestation.setParent(expression);
                        manifestationsPerExpression.add(manifestation);
                    }
                }
                expression.addChildren(manifestationsPerExpression);
                manifestations.removeAll(manifestationsPerExpression);
            }
        }
        return work;
    }

    /**
     * Extract do not index property from the metadata.
     *
     * @param hierarchyElement the hierarchy element
     * @return true, if successful
     */
    private boolean extractDoNotIndexProperty(@SuppressWarnings("rawtypes") final HierarchyElement hierarchyElement) {
        boolean result = false;
        final Property dataTypePropertyDoNotIndexp = CellarProperty.cdm_do_not_indexP;
        final ContentIdentifier contentIdentifier = hierarchyElement.getCellarId();
        final String cellarId = contentIdentifier.getIdentifier();
        final String doNotIndex = extractPropertyLiteralValue(cellarId, hierarchyElement.getVersions(), dataTypePropertyDoNotIndexp);
        if (StringUtils.isNotBlank(doNotIndex)) {
            result = Boolean.parseBoolean(doNotIndex);
            hierarchyElement.setDoNotIndex(result);
        }
        return result;
    }

    public String extractPropertyLiteralValue(final String cellarId, Map<ContentType, String> versions, final Property property) {
        String directVersion = versions.get(ContentType.DIRECT);
        final String direct = contentStreamService.getContentAsString(cellarId, directVersion, ContentType.DIRECT)
                .orElseThrow(() -> new IllegalStateException("Direct metadata not found for " + cellarId + "(" + directVersion + ")"));
        final Model directModel = JenaUtils.read(direct, Lang.NT);
        for (String pid : identifierService.getIdentifier(cellarId).getPids()) {
            final Resource resource = directModel.getResource(identifierService.getUri(pid));
            if (resource != null) {
                final String annotationLiteralValue = JenaQueries.getLiteralValue(resource, property, false, true);
                if (StringUtils.isNotBlank(annotationLiteralValue)) {
                    return annotationLiteralValue;
                }
            }
        }
        return null;
    }

    /**
     * Construct the dossier complete tree.
     *
     * @param resources the resources to use for the construction
     * @return the dossier complete tree
     */
    public Dossier getCompleteDossierTree(final Collection<CellarResource> resources) {
        Dossier dossier = null;

        for (final CellarResource resource : resources) {
            if (resource.getCellarType() == DigitalObjectType.DOSSIER) {
                dossier = createDossier(resource);
            } else if (resource.getCellarType() == DigitalObjectType.EVENT) {
                createEventWithRelations(resource, dossier);
            }
        }

        return dossier;
    }

    /**
     * <p>getDossierTree.</p>
     *
     * @param cellarId a {@link java.lang.String} object.
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Dossier} object.
     */
    public Dossier getDossierTree(final String cellarId) {
        if (!cellarId.startsWith("cellar:")) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                    .withMessage("Invalid cellar id specified: '{}'").withMessageArgs(cellarId).build();
        }

        final Collection<CellarResource> resources = this.cellarResourceDao.findTree(cellarId);

        if (resources.isEmpty()) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                    .withMessage("Resource id '{}' is not found.").withMessageArgs(cellarId).build();
        }

        Dossier dossier = null;
        final Set<Event> events = new HashSet<Event>();

        for (final CellarResource resource : resources) {
            if (resource.getCellarType() == DigitalObjectType.DOSSIER) {
                dossier = createDossier(resource);
            } else if (resource.getCellarType() == DigitalObjectType.EVENT) {
                events.add(createEvent(resource));
            }
        }

        if (null != dossier) {
            dossier.addChildren(events);
        }
        for (final Event event : events) {
            event.setParent(dossier);
        }

        return dossier;
    }

    /**
     * <p>getDossierBranch.</p>
     *
     * @param cellarId a {@link java.lang.String} object.
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Dossier} object.
     */
    public Dossier getDossierBranch(final String cellarId) {
        if (!cellarId.startsWith("cellar:")) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                    .withMessage("Invalid cellar id specified: '{}'").withMessageArgs(cellarId).build();
        }

        final Collection<CellarResource> resources = this.cellarResourceDao.findHierarchy(cellarId);

        if (resources.isEmpty()) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                    .withMessage("Resource id '{}' is not found.").withMessageArgs(cellarId).build();
        }

        Dossier dossier = null;
        final Set<Event> events = new HashSet<Event>();
        for (final CellarResource cellarResource : resources) {
            if (cellarResource.getCellarType() == DigitalObjectType.DOSSIER) {
                dossier = createDossier(cellarResource);
            } else if (cellarResource.getCellarType() == DigitalObjectType.EVENT) {
                events.add(createEvent(cellarResource));
            }
        }

        if (null != dossier) {
            dossier.addChildren(events);
        }
        for (final Event event : events) {
            event.setParent(dossier);
        }

        return dossier;
    }

    /**
     * <p>getWorkBranch.</p>
     *
     * @param cellarId a {@link java.lang.String} object.
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Work} object.
     */
    public Work getWorkBranch(final String cellarId) {
        if (!cellarId.startsWith("cellar:")) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                    .withMessage("Invalid cellar id specified: '{}'").withMessageArgs(cellarId).build();
        }

        final Collection<CellarResource> resources = this.cellarResourceDao.findHierarchy(cellarId);

        if (resources.isEmpty()) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                    .withMessage("Resource id '{}' is not found.").withMessageArgs(cellarId).build();
        }

        Work work = null;
        final Set<Expression> expressions = new HashSet<Expression>();
        final Set<Manifestation> manifestations = new HashSet<Manifestation>();

        for (final CellarResource resource : resources) {
            if (resource.getCellarType() == DigitalObjectType.WORK) {
                work = createWork(resource);
            } else if (resource.getCellarType() == DigitalObjectType.EXPRESSION) {
                expressions.add(createExpression(resource));
            } else if (resource.getCellarType() == DigitalObjectType.MANIFESTATION) {
                manifestations.add(createManifestation(resource));
            }
        }

        if (null != work) {
            work.addChildren(expressions);
        }
        for (final Expression expression : expressions) {
            expression.setParent(work);
            expression.addChildren(manifestations);
            for (final Manifestation manifestation : manifestations) {
                manifestation.setParent(expression);
            }
        }
        return work;
    }

    /**
     * <p>getWorkBranch.</p>
     *
     * @param cellarId  a {@link java.lang.String} object.
     * @param languages a {@link java.util.List} object.
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Work} object.
     */
    public Work getWorkBranch(final String cellarId, final List<LanguageBean> languages) {
        if (!cellarId.startsWith("cellar:")) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                    .withMessage("Invalid cellar id specified: '{}'").withMessageArgs(cellarId).build();
        }

        for (final LanguageBean languageBean : languages) {
            if (null != languageBean) {
                final List<CellarResource> resources = this.cellarResourceDao.findExpressions(cellarId, languageBean.getIsoCodeThreeChar());
                if ((resources != null) && !resources.isEmpty()) {
                    return this.getWorkBranch(resources.get(0).getCellarId());
                }
            }
        }
        throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                .withMessage("No expression found with language(s) '{}' for work with cellar id: '{}'")
                .withMessageArgs(LanguageBean.toString(languages), cellarId).build();
    }

    /**
     * <p>getExpressionFromWork.</p>
     *
     * @param cellarId  a {@link java.lang.String} object.
     * @param languages a {@link java.util.List} object.
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Expression} object.
     */
    public List<Expression> getExpressionFromWork(final String cellarId, final List<LanguageBean> languages) {

        final List<Expression> expressions = languages.stream().map(languageBean -> {
            List<Expression> result = this.getExpressionsFromWork(cellarId, languageBean);
            if (result == null) {
                result = Arrays.asList(new Expression[]{});
            }
            return result;
        }).flatMap(l -> l.stream()).collect(Collectors.toList());

        if ((expressions == null) || expressions.isEmpty()) {
            final DigitalObjectType type = getType(cellarId);
            if ((type == DigitalObjectType.EXPRESSION) || (type == DigitalObjectType.MANIFESTATION)) {
                throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                        .withMessage("{} is not in the requested language '{}'")
                        .withMessageArgs(type.toString(), LanguageBean.toString(languages)).build();
            }

            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                    .withMessage("No expression found for cellar id: '{}', with languages: '{}'")
                    .withMessageArgs(cellarId, LanguageBean.toString(languages)).build();
        }

        return expressions;
    }

    /**
     * <p>getExpressionFromWork.</p>
     *
     * @param cellarId a {@link java.lang.String} object.
     * @param language a {@link eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean} object.
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Expression} object.
     */
    public List<Expression> getExpressionsFromWork(final String cellarId, final LanguageBean language) {
        List<Expression> result = null;
        if (!cellarId.startsWith("cellar:")) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                    .withMessage("Invalid cellar id specified: '{}'").withMessageArgs(cellarId).build();
        }
        if (null == language) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                    .withMessage("No language specified to retrieve expression from work").build();
        }

        final List<CellarResource> expressions = this.cellarResourceDao.findExpressions(cellarId, language.getIsoCodeThreeChar());

        if ((expressions != null) && !expressions.isEmpty()) {
            result = expressions.stream().map(element -> createExpression(element)).collect(Collectors.toList());
        }

        return result;
    }

    /**
     * <p>getExpressionsInHierarchy.</p>
     *
     * @param cellarId a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    public List<CellarResource> getExpressionsInHierarchy(final String cellarId) {
        if (!cellarId.startsWith("cellar:")) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                    .withMessage("Invalid cellar id specified: '{}'").withMessageArgs(cellarId).build();
        }
        return new ArrayList<CellarResource>(this.cellarResourceDao.findExpressions(cellarId));
    }

    /**
     * Gets the manifestation branch.
     *
     * @param cellarManifestationId the cellar manifestation id
     * @return the manifestation branch
     */
    public Manifestation getManifestationBranch(final String cellarManifestationId) {

        final Collection<CellarResource> resources = this.cellarResourceDao.findStartingWithSortByCellarId(cellarManifestationId);

        Manifestation manifestation = null;

        for (final CellarResource resource : resources) {
            if (resource.getCellarType() == DigitalObjectType.MANIFESTATION) {
                if (manifestation != null) {
                    throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                            .withMessage("Invalid cellar id specified: '{}', more than one '{}' was found!")
                            .withMessageArgs(cellarManifestationId, resource.getCellarType()).build();
                }
                manifestation = createManifestation(resource);
            } else if (resource.getCellarType() == DigitalObjectType.ITEM) {
                createContentWithRelations(resource, manifestation);
            } else {
                throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.BAD_REQUEST)
                        .withMessage("Invalid cellar id specified: '{}', '{}' not allowed in this selection!")
                        .withMessageArgs(cellarManifestationId, resource.getCellarType()).build();
            }
        }

        return manifestation;
    }

    /**
     * <p>getManifestationsInHierarchy.</p>
     *
     * @param cellarId a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    public List<CellarResource> getManifestationsInHierarchy(final String cellarId) {
        if (!cellarId.startsWith("cellar:")) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                    .withMessage("Invalid cellar id specified: '{}'").withMessageArgs(cellarId).build();
        }
        return new ArrayList<CellarResource>(this.cellarResourceDao.findManifestations(cellarId));
    }

    /**
     * <p>checkForChildExpression.</p>
     *
     * @param cellarId a {@link java.lang.String} object.
     * @param language a {@link eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean} object.
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.CellarResource} object.
     */
    public List<CellarResource> checkForChildExpressions(final String cellarId, final LanguageBean language) {
        if (StringUtils.isBlank(cellarId)) {
            throw ExceptionBuilder.get(CellarException.class).build();
        }
        if (language == null) {
            throw ExceptionBuilder.get(CellarException.class).build();
        }

        final String threeCharCode = language.getIsoCodeThreeChar();
        if (StringUtils.isBlank(threeCharCode)) {
            throw ExceptionBuilder.get(CellarException.class).build();
        }

        final List<CellarResource> resources = this.cellarResourceDao.findExpressions(cellarId, threeCharCode);
        return (resources != null) && !resources.isEmpty() ? resources : null;
    }

    /**
     * <p>createWork.</p>
     *
     * @param resource a {@link eu.europa.ec.opoce.cellar.cmr.CellarResource} object.
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Work} object.
     */
    public Work createWork(final CellarResource resource) {
        final Work work = new Work(resource.getVersions());
        work.setCellarId(resource.getCellarId());
        work.setEmbargoDate(resource.getEmbargoDate());
        work.setLastModificationDate(resource.getLastModificationDate());
        work.setEmbeddedNoticeCreationDate(resource.getEmbeddedNoticeCreationDate());
        return work;
    }

    /**
     * <p>createExpression.</p>
     *
     * @param resource a {@link eu.europa.ec.opoce.cellar.cmr.CellarResource} object.
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Expression} object.
     */
    public Expression createExpression(final CellarResource resource) {
        final Expression expression = new Expression(resource.getVersions());
        expression.setCellarId(resource.getCellarId());
        setLanguages(expression, resource.getLanguages());
        expression.setLastModificationDate(resource.getLastModificationDate());
        expression.setEmbargoDate(resource.getEmbargoDate());
        expression.setEmbeddedNoticeCreationDate(resource.getEmbeddedNoticeCreationDate());
        return expression;
    }

    /**
     * Create an expression based on the resource and with the specified parent.
     *
     * @param resource the corresponding resource
     * @param work     the parent
     * @return the expression
     */
    public Expression createExpressionWithRelations(final CellarResource resource, final Work work) {
        final Expression expression = new Expression(resource.getVersions());
        expression.setCellarId(resource.getCellarId());
        setLanguages(expression, resource.getLanguages());
        expression.setLastModificationDate(resource.getLastModificationDate());
        expression.setEmbargoDate(resource.getEmbargoDate());
        expression.setParent(work);
        work.addChild(expression);

        return expression;
    }

    /**
     * <p>getAgent.</p>
     *
     * @param cellarId a {@link java.lang.String} object.
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Agent} object.
     */
    public Agent getAgent(final String cellarId) {
        if (!cellarId.startsWith("cellar:")) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                    .withMessage("Invalid cellar id specified: '{}'").withMessageArgs(cellarId).build();
        }

        final CellarResource resource = this.cellarResourceDao.findCellarId(cellarId);

        if (null == resource) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                    .withMessage("Resource id '{}' is not found.").withMessageArgs(cellarId).build();
        }

        if (resource.getCellarType() == DigitalObjectType.AGENT) {
            return createAgent(resource);
        }

        throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                .withMessage("Cellar id '{}' is not of type Agent").withMessageArgs(cellarId).build();
    }

    /**
     * <p>getTopLevelEvent.</p>
     *
     * @param cellarId a {@link java.lang.String} object.
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.TopLevelEvent} object.
     */
    public TopLevelEvent getTopLevelEvent(final String cellarId) {
        if (!cellarId.startsWith("cellar:")) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                    .withMessage("Invalid cellar id specified: '{}'").withMessageArgs(cellarId).build();
        }

        final CellarResource resource = this.cellarResourceDao.findCellarId(cellarId);

        if (null == resource) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                    .withMessage("Resource id '{}' is not found.").withMessageArgs(cellarId).build();
        }

        if (resource.getCellarType() == DigitalObjectType.TOPLEVELEVENT) {
            return createTopLevelEvent(resource);
        }

        throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                .withMessage("Cellar id '{}' is not of type TopLevelEvent").withMessageArgs(cellarId).build();
    }

    /**
     * <p>getCellarIdentifiedObject.</p>
     *
     * @param cellarId a {@link java.lang.String} object.
     * @return a {@link eu.europa.ec.opoce.cellar.domain.content.mets.CellarIdentifiedObject} object.
     */
    public CellarIdentifiedObject getCellarIdentifiedObject(final String cellarId) {
        if (!cellarId.startsWith("cellar:")) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                    .withMessage("Invalid cellar id specified: '{}'").withMessageArgs(cellarId).build();
        }

        final CellarResource resource = this.cellarResourceDao.findCellarId(cellarId);
        if (null == resource) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                    .withMessage("Resource id '{}' is not found.").withMessageArgs(cellarId).build();
        }

        CellarIdentifiedObject retObject;
        switch (resource.getCellarType()) {
            case WORK:
                retObject = createWork(resource);
                break;
            case EXPRESSION:
                retObject = createExpression(resource);
                break;
            case MANIFESTATION:
                retObject = createManifestation(resource);
                break;
            case DOSSIER:
                retObject = createDossier(resource);
                break;
            case EVENT:
                retObject = createEvent(resource);
                break;
            case AGENT:
                retObject = createAgent(resource);
                break;
            case TOPLEVELEVENT:
                retObject = createTopLevelEvent(resource);
                break;
            default:
                throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withMessage("Unknown Cellar type '{}' for object with cellar id '{}'.")
                        .withMessageArgs(resource.getCellarType(), cellarId).build();
        }
        return retObject;
    }

    /**
     * <p>createManifestation.</p>
     *
     * @param resource a {@link eu.europa.ec.opoce.cellar.cmr.CellarResource} object.
     * @return a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Manifestation} object.
     */
    public Manifestation createManifestation(final CellarResource resource) {
        final Manifestation manifestation = new Manifestation(resource.getVersions());
        manifestation.setCellarId(resource.getCellarId());
        manifestation.setLastModificationDate(resource.getLastModificationDate());
        manifestation.setEmbargoDate(resource.getEmbargoDate());
        manifestation.setEmbeddedNoticeCreationDate(resource.getEmbeddedNoticeCreationDate());
        return manifestation;
    }

    /**
     * Create a manifestation based on the resource and with the specified parent.
     *
     * @param resource   the corresponding resource
     * @param expression the parent
     * @return the manifestation
     */
    private Manifestation createManifestationWithRelations(final CellarResource resource, final Expression expression) {
        final Manifestation manifestation = new Manifestation(resource.getVersions());
        manifestation.setCellarId(resource.getCellarId());
        manifestation.setLastModificationDate(resource.getLastModificationDate());
        manifestation.setEmbargoDate(resource.getEmbargoDate());
        manifestation.setParent(expression);
        expression.addChild(manifestation);

        return manifestation;
    }

    /**
     * Create a content based on the resource and with the specified parent.
     *
     * @param resource      the corresponding resource
     * @param manifestation the parent
     * @return the content
     */
    private Content createContentWithRelations(final CellarResource resource, final Manifestation manifestation) {
        final Content content = new Content(resource.getVersions());
        final String cellarId = resource.getCellarId();

        content.setCellarId(cellarId);
        content.setLastModificationDate(resource.getLastModificationDate());
        content.setEmbargoDate(resource.getEmbargoDate());
        content.setParent(manifestation);
        content.setVisible(!content.isUnderEmbargo());

        manifestation.addChild(content);
        return content;
    }

    private Agent createAgent(final CellarResource resource) {
        final Agent agent = new Agent(resource.getVersions());
        agent.setCellarId(resource.getCellarId());
        agent.setLastModificationDate(resource.getLastModificationDate());
        agent.setEmbargoDate(resource.getEmbargoDate());
        agent.setEmbeddedNoticeCreationDate(resource.getEmbeddedNoticeCreationDate());
        return agent;
    }

    private TopLevelEvent createTopLevelEvent(final CellarResource resource) {
        final TopLevelEvent topLevelEvent = new TopLevelEvent(resource.getVersions());
        topLevelEvent.setCellarId(resource.getCellarId());
        topLevelEvent.setLastModificationDate(resource.getLastModificationDate());
        topLevelEvent.setEmbargoDate(resource.getEmbargoDate());
        topLevelEvent.setEmbeddedNoticeCreationDate(resource.getEmbeddedNoticeCreationDate());
        return topLevelEvent;
    }

    private Dossier createDossier(final CellarResource resource) {
        final Dossier dossier = new Dossier(resource.getVersions());
        dossier.setCellarId(resource.getCellarId());
        dossier.setLastModificationDate(resource.getLastModificationDate());
        dossier.setEmbargoDate(resource.getEmbargoDate());
        dossier.setEmbeddedNoticeCreationDate(resource.getEmbeddedNoticeCreationDate());
        return dossier;
    }

    private Event createEvent(final CellarResource resource) {
        final Event event = new Event(resource.getVersions());
        event.setCellarId(resource.getCellarId());
        event.setLastModificationDate(resource.getLastModificationDate());
        event.setEmbargoDate(resource.getEmbargoDate());
        event.setEmbeddedNoticeCreationDate(resource.getEmbeddedNoticeCreationDate());
        return event;
    }

    /**
     * Create an event based on the resource and with the specified parent.
     *
     * @param resource the corresponding resource
     * @param dossier  the parent
     * @return the event
     */
    private Event createEventWithRelations(final CellarResource resource, final Dossier dossier) {
        final Event event = new Event(resource.getVersions());
        event.setCellarId(resource.getCellarId());
        event.setLastModificationDate(resource.getLastModificationDate());
        event.setEmbargoDate(resource.getEmbargoDate());
        event.setParent(dossier);
        dossier.addChild(event);
        return event;
    }

    /**
     * <p>setLanguages.</p>
     *
     * @param expression a {@link eu.europa.ec.opoce.cellar.cmr.metsbeans.Expression} object.
     * @param languages  a {@link java.util.List} object.
     */
    private void setLanguages(final Expression expression, final List<String> languages) {
        for (final String language : languages) {
            final LanguageBean languageBean = parseLanguage(language);
            if (null != languageBean) {
                expression.addLang(languageBean);
            }
        }
    }

    /**
     * <p>parseLanguage.</p>
     *
     * @param language a {@link java.lang.String} object.
     * @return a {@link eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean} object.
     */
    private LanguageBean parseLanguage(final String language) {
        try {
            return this.iscoCode2LanguageBean.transform(language);
        } catch (final CellarException ignore) {
            LOG.warn("Language with code '{}' not found", language);
            return null;
        }
    }
}
