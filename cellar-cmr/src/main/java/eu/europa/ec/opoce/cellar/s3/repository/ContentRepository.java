package eu.europa.ec.opoce.cellar.s3.repository;

import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.s3.domain.ContentVersion;
import org.springframework.core.io.Resource;

import java.io.InputStream;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Predicate;

public interface ContentRepository {

    void isAccessible();

    Optional<InputStream> getContent(String cellarID, String version, ContentType contentType);

    Optional<String> getLargeContent(String cellarID, String version, ContentType contentType);

    List<ContentVersion> getVersions(String cellarID, ContentType contentType);

    List<ContentVersion> getVersions(String cellarID, ContentType contentType, int maxResults);

    String restore(String cellarID, String version, ContentType contentType);

    Map<String, Object> getContentMetadata(String cellarID, String version, ContentType contentType);

    String putContent(String cellarID, Resource resource, ContentType contentType);

    String putContent(String cellarID, Resource resource, ContentType contentType, Map<String, String> metadata);

    EnumMap<ContentType, String> putContents(String cellarID, Map<ContentType, String> contents);

    /**
     * Delete an object from the repository.
     *
     * @param cellarID the cellar identifier used to find the object
     * @param type     the type of the content (i.e. the file in Amazon S3)
     */
    void delete(String cellarID, ContentType type);

    void deleteContent(String cellarID, String version, ContentType type);

    Map<String, String> deleteRecursively(String baseCellarID);

    Map<String, String> deleteVersions(Map<String, String> keyVersions);

    boolean exists(String cellarID, ContentType type);

    String updateMetadata(String id, String version, ContentType contentType, Map<String, String> userMetadata);

    List<String> listStartWith(String prefix);

    void deleteBucket();

    void deleteBucket(Predicate<String> filter);

    void deleteBucket(Predicate<String> filter, String bucketName);

    String getDefaultBucketName();

    String tag(String key, String version, ContentType contentType, Map<String, String> tags);
}
