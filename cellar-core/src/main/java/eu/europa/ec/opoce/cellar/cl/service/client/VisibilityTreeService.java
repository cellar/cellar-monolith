/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.client
 *             FILE : VisibilityTreeService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 20, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.client;

import eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement;

/**
 * <class_description> Visibility tree service.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 20, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface VisibilityTreeService {

    /**
     * Get the hierarchy corresponding to the production identifier.
     * @param productionId the production identifier
     * @return if the production identifier exists, the hierarchy. Otherwise, null.
     */
    MetsElement getHierarchy(final String productionId);
}
