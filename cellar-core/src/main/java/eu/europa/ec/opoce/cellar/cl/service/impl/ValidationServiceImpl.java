package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.cl.domain.admin.ValidatorRule;
import eu.europa.ec.opoce.cellar.cl.domain.admin.ValidatorRule.VALIDATOR_TYPE;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.cl.domain.validator.SystemValidator;
import eu.europa.ec.opoce.cellar.cl.domain.validator.ValidationDetails;
import eu.europa.ec.opoce.cellar.cl.domain.validator.ValidationResult;
import eu.europa.ec.opoce.cellar.cl.domain.validator.ValidationStrategy;
import eu.europa.ec.opoce.cellar.cl.service.client.ValidationService;
import eu.europa.ec.opoce.cellar.cl.service.client.ValidatorRuleService;
import eu.europa.ec.opoce.cellar.monitoring.Watch;
import eu.europa.ec.opoce.cellar.support.DynamicClassLoader;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Service to manage System and Custom Validator
 * 
 * @author dcraeye
 * 
 */
@Service
public class ValidationServiceImpl implements ValidationService {

    private final ValidatorRuleService validatorRuleService;

    @Autowired
    public ValidationServiceImpl(ValidatorRuleService validatorRuleService) {
        this.validatorRuleService = validatorRuleService;
    }

    @Override
    public ValidationResult validate(ValidationDetails validationParameters) {
        ValidationResult result = new ValidationResult();

        for (ValidationStrategy validationStrategy : getValidationStrategyList()) {

            ValidationResult validationResult = validationStrategy.validate(validationParameters);
            result.setValid(result.isValid() && validationResult.isValid());

            if (!validationResult.isValid())
                result.setStatusCode(validationResult.getStatusCode());

            for (String validationMessage : validationResult.getMessages()) {
                result.addMessage(validationMessage);
            }
        }

        return result;
    }

    @Override
    public List<ValidationStrategy> getValidationStrategyList() {
        return DynamicClassLoader.loadServiceClasses(ValidationStrategy.class);
    }

    @Watch(value = "Validation", arguments = {
            @Watch.WatchArgument(name = "SIP resource", expression = "sipResource.metsID")
    })
    @Override
    public ValidationResult executeSystemValidator(Object objectToValidate, SIPResource sipResource, SystemValidator validator) {
        ValidationDetails validationParameters = new ValidationDetails();
        validationParameters.setToValidate(objectToValidate);
        validationParameters.setSipResource(sipResource);

        return executeSystemValidator(validationParameters, validator);
    }

    @Override
    public ValidationResult executeSystemValidator(Object objectToValidate, SIPResource sipResource, SystemValidator validator,
            String validatorQualifierName) {
        ValidationDetails validationParameters = new ValidationDetails();
        validationParameters.setToValidate(objectToValidate);
        validationParameters.setSipResource(sipResource);

        return executeSystemValidator(validationParameters, validator, validatorQualifierName);
    }

    @Override
    public ValidationResult executeSystemValidator(ValidationDetails validationParameters, SystemValidator validator) {
        return executeSystemValidator(validationParameters, validator, "");
    }

    @Override
    public ValidationResult executeSystemValidator(ValidationDetails validationParameters, SystemValidator validator,
            String validatorQualifierName) {
        //substring before $ because we can have classes like : eu.europa.ec.opoce.cellar.cl.domain.validator.XmlValidator$$EnhancerByCGLIB$$8c7c8f3c
        //if theses classes have been instantiate by spring using a QualifierName.
        String validatorClass = StringUtils.substringBefore(validator.getClass().getName(), "$")
                + (StringUtils.isNotBlank(validatorQualifierName) ? "@qualifier." + validatorQualifierName : "");

        ValidatorRule vr = validatorRuleService.getRuleByValidatorClass(validatorClass);
        if (vr == null) {
            vr = new ValidatorRule();
            vr.setEnabled(true);
            vr.setKey(StringUtils.substringAfterLast(validatorClass, "."));
            vr.setType(VALIDATOR_TYPE.SYSTEM);
            vr.setValidatorClass(validatorClass);

            validatorRuleService.createRule(vr);
        }

        ValidationResult result = new ValidationResult();
        result.setValid(true);

        if (vr.isEnabled()) {
            result = validator.validate(validationParameters);
        }

        return result;
    }

}
