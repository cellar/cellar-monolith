package eu.europa.ec.opoce.cellar.support;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.util.Assert;

/**
 * An String template is a String that contained variables marked of in braces
 * (<code>{</code>, <code>}</code>), which can be expanded to produce another String. <p>See {@link #expand(Map)},
 * {@link #expand(Object[])}, and {@link #match(String)} for example usages.
 * 
 * @author phvdveld
 * 
 */
public class StringTemplate {

    /** Captures URI template variable names. */
    private static final Pattern NAMES_PATTERN = Pattern.compile("\\{([^/]+?)\\}");

    /** Replaces template variables in the URI template. */
    private static final String VALUE_REGEX = "(.*)";

    private final List<String> variableNames;

    private final Pattern matchPattern;

    private final String strTemplate;

    /**
     * Construct a new {@link StringTemplate} with the given template.
     * @param strTemplate the template string
     */
    public StringTemplate(String strTemplate) {
        Parser parser = new Parser(strTemplate);
        this.strTemplate = strTemplate;
        this.variableNames = parser.getVariableNames();
        this.matchPattern = parser.getMatchPattern();
    }

    /**
     * Return the names of the variables in the template, in order.
     * @return the template variable names
     */
    public List<String> getVariableNames() {
        return this.variableNames;
    }

    /**
     * Given the Map of variables, expands this template into a URI. The Map keys represent variable names, the Map values
     * variable values. The order of variables is not significant. <p>Example:
     * <pre>
     * StringTemplate template = new StringTemplate("http://example.com/hotels/{hotel}/bookings/{booking}");
     * Map&lt;String, String&gt; uriVariables = new HashMap&lt;String, String&gt;();
     * uriVariables.put("booking", "42");
     * uriVariables.put("hotel", "1");
     * System.out.println(template.expand(uriVariables));
     * </pre>
     * will print: <blockquote><code>http://example.com/hotels/1/bookings/42</code></blockquote>
     * @param variableValues the map of URI variables
     * @return the expanded String
     * @throws IllegalArgumentException if <code>variableValues</code> is <code>null</code>; or if it does not contain values
     * for all the variable names
     */
    public String expand(Map<String, ?> variableValues) {
        Assert.notNull(variableValues, "'uriVariables' must not be null");
        Object[] values = new String[this.variableNames.size()];
        for (int i = 0; i < this.variableNames.size(); i++) {
            String name = this.variableNames.get(i);
            if (!variableValues.containsKey(name)) {
                throw new IllegalArgumentException("'uriVariables' Map has no value for '" + name + "'");
            }
            values[i] = variableValues.get(name);
        }
        return expand(values);
    }

    /**
     * Given an array of variables, expand this template into a full URI. The array represent variable values.
     * The order of variables is significant. <p>Example:
     * <pre>
     * StringTemplate template = new StringTemplate("http://example.com/hotels/{hotel}/bookings/{booking}");
     * System.out.println(template.expand("1", "42));
     * </pre> will print: <blockquote><code>http://example.com/hotels/1/bookings/42</code></blockquote>
     * @param variableValues the array of variables
     * @return the expanded String
     * @throws IllegalArgumentException if <code>variableValues</code> is <code>null</code>; or if it does not contain
     * sufficient variables
     */
    public String expand(Object... variableValues) {
        Assert.notNull(variableValues, "'variableValues' must not be null");

        // deactivated this check to be able to use the template with variables that have to be expanded in different sections of the map object
        /*if (variableValues.length != this.variableNames.size()) {
            throw new IllegalArgumentException(
                    "Invalid amount of variables values in [" + this.strTemplate + "]: expected " +
                            this.variableNames.size() + "; got " + variableValues.length);
        }*/
        Matcher matcher = NAMES_PATTERN.matcher(this.strTemplate);
        StringBuffer buffer = new StringBuffer();
        int i = 0;
        while (matcher.find()) {
            Object strVariable = variableValues[i++];
            String replacement = Matcher.quoteReplacement(strVariable != null ? strVariable.toString() : "");
            matcher.appendReplacement(buffer, replacement);
        }
        matcher.appendTail(buffer);
        return buffer.toString();
    }

    /**
     * Indicate whether the given String matches this template.
     * @param toMatch the String to match to
     * @return <code>true</code> if it matches; <code>false</code> otherwise
     */
    public boolean matches(String toMatch) {
        if (toMatch == null) {
            return false;
        }
        Matcher matcher = this.matchPattern.matcher(toMatch);
        return matcher.matches();
    }

    /**
     * Match the given String to a map of variable values. Keys in the returned map are variable names, values are variable
     * values, as occurred in the given URI. <p>Example: <pre class="code"> StringTemplate template = new
     * StringTemplate("http://example.com/hotels/{hotel}/bookings/{booking}"); System.out.println(template.match("http://example.com/hotels/1/bookings/42"));
     * </pre> will print: <blockquote><code>{hotel=1, booking=42}</code></blockquote>
     * @param toMatch the String to match to
     * @return a map of variable values
     */
    public Map<String, String> match(String toMatch) {
        Assert.notNull(toMatch, "'uri' must not be null");
        Map<String, String> result = new LinkedHashMap<String, String>(this.variableNames.size());
        Matcher matcher = this.matchPattern.matcher(toMatch);
        if (matcher.find()) {
            for (int i = 1; i <= matcher.groupCount(); i++) {
                String name = this.variableNames.get(i - 1);
                String value = matcher.group(i);
                result.put(name, value);
            }
        }
        return result;
    }

    @Override
    public String toString() {
        return this.strTemplate;
    }

    /**
     * Static inner class to parse template strings into a matching regular expression.
     */
    private static class Parser {

        private final List<String> variableNames = new LinkedList<String>();

        private final StringBuilder patternBuilder = new StringBuilder();

        private Parser(String uriTemplate) {
            Assert.hasText(uriTemplate, "'uriTemplate' must not be null");
            Matcher m = NAMES_PATTERN.matcher(uriTemplate);
            int end = 0;
            while (m.find()) {
                this.patternBuilder.append(quote(uriTemplate, end, m.start()));
                this.patternBuilder.append(VALUE_REGEX);
                this.variableNames.add(m.group(1));
                end = m.end();
            }
            this.patternBuilder.append(quote(uriTemplate, end, uriTemplate.length()));
            int lastIdx = this.patternBuilder.length() - 1;
            if (lastIdx >= 0 && this.patternBuilder.charAt(lastIdx) == '/') {
                this.patternBuilder.deleteCharAt(lastIdx);
            }
        }

        private String quote(String fullPath, int start, int end) {
            if (start == end) {
                return "";
            }
            return Pattern.quote(fullPath.substring(start, end));
        }

        private List<String> getVariableNames() {
            return Collections.unmodifiableList(this.variableNames);
        }

        private Pattern getMatchPattern() {
            return Pattern.compile(this.patternBuilder.toString());
        }
    }
}
