/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.export
 *             FILE : IEntityRewriterService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 15, 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2016-09-14 16:59:33 +0200 (Wed, 14 Sep 2016) $
 *          VERSION : $LastChangedRevision: 11689 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2016 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.export;

/**
 * <class_description> Service able to rewrite metadata using entities.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 15, 2016
 *
 * @author ARHS Developments
 * @version $Revision: 11689 $
 */
public interface IEntityRewriterService {

    String rewriteMetadataWithEntity(final String metadataAsString);
}
