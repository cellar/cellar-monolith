/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.LicenseHolder
 *             FILE : ExtractionWorkIdentifier.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 5, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.licenseHolder;

import javax.persistence.*;

/**
 * <class_description> Class that represents the informations about an extraction work identifier.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 5, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Entity
@Table(name = "EXTRACTION_WORK_IDENTIFIER")
@NamedQueries({
        @NamedQuery(name = "findCountExtractionWorkIdentifiers", query = "select count(w.id) from ExtractionWorkIdentifier w where w.extractionExecution.id = :executionId"),
        @NamedQuery(name = "findCountMatchExtractionWorkIdentifiers", query = "select count(w.id) from ExtractionWorkIdentifier w where w.extractionExecution.id = :executionId and w.workId like :filterValue"),
        @NamedQuery(name = "findWorkIdentifiers", query = "select w from ExtractionWorkIdentifier w where w.extractionExecution.id = ?0")})
public class ExtractionWorkIdentifier {

    /**
     * Internal id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    /**
     * The work identifier.
     */
    @Column(name = "WORK_ID", nullable = false)
    private String workId;

    /**
     * The parent extraction execution.
     */
    @ManyToOne
    @JoinColumn(name = "EXTRACTION_EXECUTION_ID")
    private ExtractionExecution extractionExecution;

    /**
     * Constructor
     */
    public ExtractionWorkIdentifier() {

    }

    /**
     * Constructor.
     * @param workId the work identifier
     */
    public ExtractionWorkIdentifier(final String workId) {
        this.workId = workId;
    }

    /**
     * Constructor.
     * @param workId the work identifier
     * @param extractionExecution the parent extraction execution
     */
    public ExtractionWorkIdentifier(final String workId, final ExtractionExecution extractionExecution) {
        this.workId = workId;
        this.extractionExecution = extractionExecution;
    }

    /**
     * Gets the value of the id.
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the value of the id.
     * @param id the id to set
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the work identifier.
     * @return the work identifier
     */
    public String getWorkId() {
        return this.workId;
    }

    /**
     * Sets the work identifier.
     * @param workId the work identifier to set
     */
    public void setWorkId(final String workId) {
        this.workId = workId;
    }

    /**
     * Gets the extraction execution identifier.
     * @return the extraction execution identifier
     */
    @Column(name = "EXTRACTION_EXECUTION_ID", nullable = false, insertable = false, updatable = false)
    public Long getExtractionExecutionId() {
        return this.extractionExecution.getId();
    }

    /**
     * Gets the parent extraction execution.
     * @return the parent extraction execution 
     */
    public ExtractionExecution getExtractionExecution() {
        return this.extractionExecution;
    }

    /**
     * Sets the parent extraction execution.
     * @param extractionExecution the parent extraction execution
     */
    public void setExtractionExecution(ExtractionExecution extractionExecution) {
        this.extractionExecution = extractionExecution;
    }
}
