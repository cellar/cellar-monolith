/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState
 *             FILE : AbstractBatch.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 18, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.springframework.stereotype.Component;

import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ArchiveTreeManager;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 18, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component
public abstract class AbstractBatch extends AbstractExecution {

    /**
     * Execute a batch.
     * @param extractionExecution the execution concerned
     * @param firstOfBatch the first index of the batch (inclusive)
     * @param lastOfBatch the last index of the batch (exclusive)
     * @param archiveTreeManager the archive tree manger
     * @param readWriteLock the reader writer lock for the execution
     * @param batchWorker the batch workers counter
     * @throws Exception
     */
    public abstract void execute(final ExtractionExecution extractionExecution, final int firstOfBatch, final int lastOfBatch,
            final ArchiveTreeManager archiveTreeManager, final ReentrantReadWriteLock readWriteLock, final AtomicInteger batchWorker)
                    throws Exception;
}
