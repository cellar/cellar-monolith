/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *        FILE : NonTransactionalReadOnlyPidManagerServiceImpl.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 16-03-2012
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.IS3Configuration;
import eu.europa.ec.opoce.cellar.cl.configuration.PrefixConfigurationService;
import eu.europa.ec.opoce.cellar.cl.dao.CellarIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.dao.ProductionIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.admin.ProductionIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.NotImplementedExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.PidManagerException;
import eu.europa.ec.opoce.cellar.semantic.helper.MessageFormatter;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <class_description> Non-transactional implementation of the {@link PidManagerService}.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 16-03-2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service("nonTransactionalReadOnlyPidManagerService")
public class NonTransactionalReadOnlyPidManagerServiceImpl implements PidManagerService, IdentifierService {

    /** Logger instance. */
    @SuppressWarnings("unused")
    private static final Logger LOGGER = LoggerFactory.getLogger(NonTransactionalReadOnlyPidManagerServiceImpl.class);

    /** The Cellar configuration class. */
    @Autowired
    private ICellarConfiguration cellarConfiguration;

    /** The S3 configuration class. */
    @Autowired
    @Qualifier("cellarConfiguration")
    private IS3Configuration s3Configuration;

    /** The prefix configuration server. */
    @Autowired(required = true)
    private PrefixConfigurationService prefixConfigurationService;

    /**
     * Bean for storing configuration parameters.
     */
    private PidManagerConfigParams pidManagerConfigParams;

    /**
     * The {@link ProductionIdentifierDao}.
     */
    @Autowired
    private ProductionIdentifierDao productionIdentifierDao;

    /**
     * The {@link CellarIdentifierDao}.
     */
    @Autowired
    private CellarIdentifierDao cellarIdentifierDao;

    /**
     * This method will initialize the configuration parameter of the {@link NonTransactionalReadOnlyPidManagerServiceImpl}.
     */
    @PostConstruct
    void initialize() {
        this.pidManagerConfigParams = new PidManagerConfigParams(
                this.cellarConfiguration.getCellarUriResourceBase(),
                this.cellarConfiguration.getCellarUriDisseminationBase(),
                this.s3Configuration.getCellarNamespace()
        );
    }

    /**
     * Gets the production identifier by identifier.
     *
     * @param identifier the identifier
     * @return the production identifier by identifier
     * @see eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService#getProductionIdentifierByIdentifier(java.lang.String)
     */
    @Override
    public ProductionIdentifier getProductionIdentifierByIdentifier(final String identifier) {
        return this.productionIdentifierDao.getProductionIdentifier(identifier);
    }

    /**
     * Gets the unbound resources.
     *
     * @return the unbound resources
     * @see eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService#getUnboundResources()
     */
    @Override
    public List<ProductionIdentifier> getUnboundResources() {
        return this.productionIdentifierDao.getUnboundResources();
    }

    /**
     * Gets the uri.
     *
     * @param cellarId the cellar id
     * @return the uri
     * @see eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService#getURI(java.lang.String)
     */
    @Override
    public String getURI(final String cellarId) {
        String cmrBaseURI = this.pidManagerConfigParams.getDissiminationBaseURI();
        if (!cmrBaseURI.endsWith("/")) {
            cmrBaseURI = cmrBaseURI + "/";
        }

        final String uri = new StringBuilder(cmrBaseURI).append("resource/").append(StringUtils.replace(cellarId, ":", "/")).toString();
        return uri;
    }

    /**
     * Gets the uri.
     *
     * @param productionIdentifier the production identifier
     * @return the uri
     * @see eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService#getURI(eu.europa.ec.opoce.cellar.cl.domain.admin.ProductionIdentifier)
     */
    @Override
    public String getURI(final ProductionIdentifier productionIdentifier) {
        CellarIdentifier cellarIdentifier = productionIdentifier.getCellarIdentifier();
        if ((cellarIdentifier == null) && (productionIdentifier.getProductionId() != null)) {
            final ProductionIdentifier foundPid = this.productionIdentifierDao
                    .getProductionIdentifier(productionIdentifier.getProductionId());
            if (foundPid == null) {
                throw ExceptionBuilder.get(PidManagerException.class).withCode(CoreErrors.E_033)
                        .withMessage("Current production ID : " + productionIdentifier.getProductionId()).build();
            }
            cellarIdentifier = foundPid.getCellarIdentifier();
        }

        return (cellarIdentifier != null) && (cellarIdentifier.getUuid() != null) ? this.getURI(cellarIdentifier.getUuid()) : null;
    }

    /**
     * Creates the production identifier.
     *
     * @param uri the uri
     * @return the production identifier
     * @see eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService#createProductionIdentifier(java.lang.String)
     */
    @Override
    public ProductionIdentifier createProductionIdentifier(final String uri) {
        final String productionId = this.getIdFromURI(uri);
        return this.initializeProductionIdentifier(productionId);
    }

    /**
     * Gets the production identifier by uri.
     *
     * @param uri the uri
     * @return the production identifier by uri
     * @see eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService#getProductionIdentifierByURI(java.lang.String)
     */
    @Override
    public ProductionIdentifier getProductionIdentifierByURI(final String uri) {
        final ProductionIdentifier productionIdentifier = this.productionIdentifierDao.getProductionIdentifier(this.getIdFromURI(uri));
        return productionIdentifier;
    }

    /**
     * Check same cellar identifier.
     *
     * @param productionIdentifiers the production identifiers
     * @return the cellar identifier
     * @see eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService#checkSameCellarIdentifier(java.util.List)
     */
    @Override
    public CellarIdentifier checkSameCellarIdentifier(final List<ProductionIdentifier> productionIdentifiers) {
        CellarIdentifier tempCellarIdentifier = null;
        for (final ProductionIdentifier productionIdentifier : productionIdentifiers) {
            CellarIdentifier currentCellarIdentifier = null;
            final ProductionIdentifier currentProductionIdentifier = this.productionIdentifierDao
                    .getProductionIdentifier(productionIdentifier.getProductionId());
            currentCellarIdentifier = currentProductionIdentifier.getCellarIdentifier();

            if (currentCellarIdentifier == null) {
                throw ExceptionBuilder.get(PidManagerException.class).withCode(CoreErrors.E_034)
                        .withMessage("Current production ID : " + currentProductionIdentifier.getProductionId()).build();
            }
            if ((tempCellarIdentifier != null) && !tempCellarIdentifier.equals(currentCellarIdentifier)) {
                throw ExceptionBuilder
                        .get(PidManagerException.class).withCode(CoreErrors.E_035).withMessage("Current production ID : "
                                + currentProductionIdentifier.getProductionId() + " -- Cellar ID " + currentCellarIdentifier.getUuid())
                        .build();
            }

            tempCellarIdentifier = currentCellarIdentifier;
        }

        return tempCellarIdentifier;
    }

    /**
     * Get all the production identifiers as a list of Strings.  These production identifiers correspond to the identifiers of <code>ids</code>.
     * @param ids the searched production identifiers
     * @return a list of production identifiers corresponding to ids, the list contains at least ids
     */
    public List<String> getAllPIDSilently(final List<String> ids) {
        final Set<String> pids = new HashSet<String>(ids);

        CellarIdentifier cellarId = null;
        ProductionIdentifier pid = null;
        List<ProductionIdentifier> productionIdentifiers = null;

        for (final String id : ids) {
            if (this.isCellarId(id)) {
                cellarId = this.cellarIdentifierDao.getCellarIdentifier(id);
            } else {
                pid = this.productionIdentifierDao.getProductionIdentifier(id);
                if (pid == null) {
                    cellarId = null;
                } else {
                    cellarId = pid.getCellarIdentifier();
                }
            }

            if (cellarId != null) {
                productionIdentifiers = this.productionIdentifierDao.getProductionIdentifiers(cellarId);
                for (final ProductionIdentifier productionIdentifier : productionIdentifiers) {
                    pids.add(productionIdentifier.getProductionId());
                }
            }
        }

        return new LinkedList<String>(pids);
    }

    /**
     * Checks if is cellar uuid.
     *
     * @param pid the pid
     * @return true, if is cellar uuid
     * @see eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService#isCellarUUID(java.lang.String)
     */
    @Override
    public boolean isCellarUUID(final String pid) {
        return this.isCellarId(pid);
    }

    /**
     * Gets the cellar uuid.
     *
     * @param pid the pid
     * @return the cellar uuid
     * @see eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService#getCellarUUID(java.lang.String)
     */
    @Override
    public String getCellarUUID(final String pid) {
        String cellarId = null;
        final ProductionIdentifier productionIdentifier = this.productionIdentifierDao.getProductionIdentifier(pid);
        if (productionIdentifier == null) {
            throw new IllegalArgumentException("Production identifier '" + pid + "' not found.");
        }
        cellarId = productionIdentifier.getCellarIdentifier().getUuid();

        return cellarId;
    }

    /**
     * Gets the cellar uuid if exists.
     *
     * @param pid the pid
     * @return the cellar uuid if exists
     * @see eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService#getCellarUUIDIfExists(java.lang.String)
     */
    @Override
    public String getCellarUUIDIfExists(final String pid) {
        String cellarId = null;
        final ProductionIdentifier productionIdentifier = this.productionIdentifierDao.getProductionIdentifier(pid);
        if (productionIdentifier != null) {
            cellarId = productionIdentifier.getCellarIdentifier().getUuid();
        }

        return cellarId;
    }

    /** {@inheritDoc} */
    @Override
    public Collection<String> getUnknownProductionIdentifers(final List<String> pids) {
        final List<ProductionIdentifier> productionIdentifiers = this.productionIdentifierDao.getProductionIdentifiers(pids);

        final Set<String> set = new HashSet<String>(pids);

        for (final ProductionIdentifier pi : productionIdentifiers) {
            set.remove(pi.getProductionId());
        }

        return set;
    }

    /**
     * Gets the cellar uui ds.
     *
     * @param pids the pids
     * @return the cellar uui ds
     * @see eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService#getCellarUUIDs(java.util.List)
     */
    @Override
    public Set<String> getCellarUUIDs(final List<String> pids) {
        final List<ProductionIdentifier> productionIdentifiers = this.productionIdentifierDao.getProductionIdentifiers(pids);

        final Set<String> cellarIds = new HashSet<String>();
        String cellarId = null;

        for (final ProductionIdentifier productionIdentifier : productionIdentifiers) {
            cellarId = productionIdentifier.getCellarIdentifier().getUuid();
            if (cellarId != null) {
                cellarIds.add(cellarId);
            }
        }

        return cellarIds;
    }

    /**
     * Gets the cellar uuid if exists in.
     *
     * @param pids the pids
     * @return the cellar uuid if exists in
     * @see eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService#getCellarUUIDIfExistsIn(java.util.List)
     */
    @Override
    public String getCellarUUIDIfExistsIn(final List<String> pids) {
        final List<ProductionIdentifier> productionIdentifiers = this.productionIdentifierDao.getProductionIdentifiers(pids);

        String cellarId = null;
        for (final ProductionIdentifier productionIdentifier : productionIdentifiers) {
            cellarId = productionIdentifier.getCellarIdentifier().getUuid();
            if (cellarId != null) {
                return cellarId;
            }
        }

        return null;
    }

    /**
     * Sets a new value for the cellarConfguration.
     *
     * @param cellarConfguration
     *            the cellarConfguration to set
     */
    public void setCellarConfguration(final ICellarConfiguration cellarConfguration) {
        this.cellarConfiguration = cellarConfguration;
    }

    /**
     * Sets a new value for the pidManagerConfigParams.
     *
     * @param pidManagerConfigParams
     *            the pidManagerConfigParams to set
     */
    public void setPidManagerConfigParams(final PidManagerConfigParams pidManagerConfigParams) {
        this.pidManagerConfigParams = pidManagerConfigParams;
    }

    /**
     * Sets a new value for the productionIdentifierDao.
     *
     * @param productionIdentifierDao
     *            the productionIdentifierDao to set
     */
    public void setProductionIdentifierDao(final ProductionIdentifierDao productionIdentifierDao) {
        this.productionIdentifierDao = productionIdentifierDao;
    }

    /**
     * Sets a new value for the cellarIdentifierDao.
     *
     * @param cellarIdentifierDao
     *            the cellarIdentifierDao to set
     */
    public void setCellarIdentifierDao(final CellarIdentifierDao cellarIdentifierDao) {
        this.cellarIdentifierDao = cellarIdentifierDao;
    }

    /**
     * Checks if is work.
     *
     * @param cellarId the cellar id
     * @return true, if is work
     * @see eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService#isWork(java.lang.String)
     */
    @Override
    public boolean isWork(final String cellarId) {
        return DigitalObjectType.isWorkDossierTopLevelEventOrAgent(cellarId);
    }

    /**
     * Checks if is expression or event.
     *
     * @param cellarId the cellar id
     * @return true, if is expression or event
     * @see eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService#isExpressionOrEvent(java.lang.String)
     */
    @Override
    public boolean isExpressionOrEvent(final String cellarId) {
        return DigitalObjectType.isExpressionOrEvent(cellarId);
    }

    /**
     * Checks if is manifestation.
     *
     * @param cellarId the cellar id
     * @return true, if is manifestation
     * @see eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService#isManifestation(java.lang.String)
     */
    @Override
    public boolean isManifestation(final String cellarId) {
        return DigitalObjectType.isManifestation(cellarId);
    }

    /**
     * Checks if is manifestation content.
     *
     * @param cellarId the cellar id
     * @return true, if is manifestation content
     * @see eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService#isManifestationContent(java.lang.String)
     */
    @Override
    public boolean isManifestationContent(final String cellarId) {
        return DigitalObjectType.isItem(cellarId);
    }

    /**
     * Gets the tree identifiers.
     *
     * @param cellarIdentifier the cellar identifier
     * @return the tree identifiers
     * @see eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService#getTreeIdentifiers(java.lang.String)
     */
    @Override
    public List<Identifier> getTreeIdentifiers(final String cellarIdentifier) {
        return this.cellarIdentifierDao.getTreePid(cellarIdentifier);
    }

    /**
     * Gets the tree identifiers.
     *
     * @param cellarIdentifier the cellar identifier
     * @param withContentStream the with content stream
     * @return the tree identifiers
     * @see eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService#getTreeIdentifiers(java.lang.String, boolean)
     */
    @Override
    public List<Identifier> getTreeIdentifiers(final String cellarIdentifier, final boolean withContentStream) {
        return this.cellarIdentifierDao.getTreePid(cellarIdentifier, withContentStream);
    }

    /**
     * Gets the tree content pid.
     *
     * @param cellarIdentifier the cellar identifier
     * @return the tree content pid
     * @see eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService#getTreeContentPid(java.lang.String)
     */
    @Override
    public List<Identifier> getTreeContentPid(final String cellarIdentifier) {
        return this.cellarIdentifierDao.getTreeContentPid(cellarIdentifier);
    }

    /**
     * Gets the all pid if cellar id exists.
     *
     * @param identifier the identifier
     * @return the all pid if cellar id exists
     * @see eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService#getAllPIDIfCellarIdExists(eu.europa.ec.opoce.cellar.cl.domain.content.Identifier)
     */
    @Override
    public List<String> getAllPIDIfCellarIdExists(final Identifier identifier) {

        if (identifier.cellarIdExists()) {
            final CellarIdentifier cellarIdentifier = this.cellarIdentifierDao.getCellarIdentifier(identifier.getCellarId());
            if (cellarIdentifier != null) {
                final List<String> pids = this
                        .getProductionIdentifiersAsStrings(this.productionIdentifierDao.getProductionIdentifiers(cellarIdentifier));
                final Set<String> pidsSet = new HashSet<String>();
                if (identifier.getPids() != null) {
                    pidsSet.addAll(identifier.getPids());
                }

                pidsSet.addAll(pids);
                return new LinkedList<String>(pidsSet);
            }
        }

        return null;
    }

    /**
     * Gets the identifier.
     *
     * @param id the id
     * @return the identifier
     * @see eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService#getIdentifier(java.lang.String)
     */
    @Override
    public Identifier getIdentifier(final String id) {
        Identifier identifier = null;
        CellarIdentifier cellarID = null;

        if (this.isCellarId(id)) {
            cellarID = this.cellarIdentifierDao.getCellarIdentifier(id);
        } else {
            final ProductionIdentifier pid = this.productionIdentifierDao.getProductionIdentifier(id);
            if (pid == null) {
                throw new IllegalArgumentException("Production identifier '" + id + "' not found.");
            }
            cellarID = pid.getCellarIdentifier();
        }

        if (cellarID == null) {
            throw new IllegalArgumentException("Id '" + id + "' is neither a cellar id/production id or is not found in db.");
        }

        final List<String> pids = new ArrayList<String>();
        final List<ProductionIdentifier> pidList = this.productionIdentifierDao.getProductionIdentifiers(cellarID);
        for (final ProductionIdentifier prodId : pidList) {
            pids.add(prodId.getProductionId());
        }

        identifier = new Identifier(cellarID.getUuid(), pids);

        return identifier;
    }

    /**
     * Gets the identifiers.
     *
     * @param ids the ids
     * @return the identifiers
     * @see eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService#getIdentifiers(java.util.Collection)
     */
    @Override
    public Collection<Identifier> getIdentifiers(final Collection<String> ids) {
        final List<String> cellarIds = new ArrayList<String>();
        final List<String> productionIds = new ArrayList<String>();
        for (final String id : ids) {
            if (this.isCellarId(id)) {
                cellarIds.add(id);
            } else {
                productionIds.add(id);
            }
        }

        final List<ProductionIdentifier> pidList = this.productionIdentifierDao.getProductionIdentifiersByCellarIds(cellarIds,
                this.cellarConfiguration.getCellarDatabaseInClauseParams());
        pidList.addAll(this.productionIdentifierDao.getProductionIdentifiersByProductionIds(productionIds,
                this.cellarConfiguration.getCellarDatabaseInClauseParams()));
        if (pidList.isEmpty()) {
            throw new IllegalArgumentException(ids + " does not contain any existing cellar or production id.");
        }

        final Map<String, Identifier> cellarIdsMap = new HashMap<String, Identifier>();
        for (final ProductionIdentifier pid : pidList) {
            final CellarIdentifier cellarID = pid.getCellarIdentifier();
            final String uuid = cellarID.getUuid();
            Identifier identifier = cellarIdsMap.get(uuid);
            if (identifier == null) {
                identifier = new Identifier(cellarID.getUuid(), new ArrayList<>());
                cellarIdsMap.put(uuid, identifier);
            }
            identifier.getPids().add(pid.getProductionId());
        }

        return cellarIdsMap.values();
    }

    /**
     * <p>getPrefixedFromUri.</p>
     *
     * @param uri a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    @Override
    public String getPrefixedFromUri(final String uri) {
        final String result = this.getPrefixedOrNullFromUri(uri);
        if (result == null) {
            throw new IllegalArgumentException(MessageFormatter.format("unknown uri: {}", uri));
        }
        return result;
    }

    /**
     * <p>isUri.</p>
     *
     * @param uriOrPrefix a {@link java.lang.String} object.
     * @return a boolean.
     */
    @Override
    public boolean isUri(final String uriOrPrefix) {
        if (StringUtils.trimToNull(uriOrPrefix) == null) {
            throw ExceptionBuilder.get(CellarException.class).build();
        }
        return uriOrPrefix.startsWith("http://");
    }

    /**
     * <p>getUri.</p>
     *
     * @param uriOrPrefix a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    @Override
    public String getUri(final String uriOrPrefix) {
        if (StringUtils.trimToNull(uriOrPrefix) == null) {
            throw ExceptionBuilder.get(CellarException.class).build();
        }
        return this.isUri(uriOrPrefix) ? uriOrPrefix : this.getUriFromPrefix(uriOrPrefix);
    }

    /**
     * <p>getPrefixed.</p>
     *
     * @param uriOrPrefix a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    @Override
    public String getPrefixed(final String uriOrPrefix) {
        if (StringUtils.trimToNull(uriOrPrefix) == null) {
            throw ExceptionBuilder.get(CellarException.class).build();
        }
        return this.isUri(uriOrPrefix) ? this.getPrefixedFromUri(uriOrPrefix) : uriOrPrefix;
    }

    /**
     * <p>getPrefixedOrNull.</p>
     *
     * @param uriOrPrefix a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    @Override
    public String getPrefixedOrNull(final String uriOrPrefix) {
        if (StringUtils.trimToNull(uriOrPrefix) == null) {
            throw ExceptionBuilder.get(CellarException.class).build();
        }
        return this.isUri(uriOrPrefix) ? this.getPrefixedOrNullFromUri(uriOrPrefix) : uriOrPrefix;
    }

    /**
     * <p>getCellarPrefixed.</p>
     *
     * @param uriOrPrefix a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    @Override
    public String getCellarPrefixed(final String uriOrPrefix) {
        final String id = this.getPrefixed(uriOrPrefix);
        return id.startsWith(PREFIX_CELLAR) ? id : this.getCellarUUID(id);
    }

    /**
     * <p>getCellarPrefixedOrNull.</p>
     *
     * @param uriOrPrefix a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    @Override
    public String getCellarPrefixedOrNull(final String uriOrPrefix) {
        final String id = this.getPrefixedOrNull(uriOrPrefix);
        return (id == null) || id.startsWith(PREFIX_CELLAR) ? id : this.getCellarUUIDIfExists(id);
    }

    /**
     * Delete production identifier.
     *
     * @param identifier the identifier
     * @see eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService#deleteProductionIdentifier(java.lang.String)
     */
    @Override
    public void deleteProductionIdentifier(final String identifier) {
        throwUp();
    }

    /**
     * Delete production identifier.
     *
     * @param productionIdentifier the production identifier
     * @see eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService#deleteProductionIdentifier(eu.europa.ec.opoce.cellar.cl.domain.admin.ProductionIdentifier)
     */
    @Override
    public void deleteProductionIdentifier(final ProductionIdentifier productionIdentifier) {
        throwUp();
    }

    /**
     * Delete cellar identifier.
     *
     * @param cellarIdentifier the cellar identifier
     * @see eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService#deleteCellarIdentifier(java.lang.String)
     */
    @Override
    public void deleteCellarIdentifier(final String cellarIdentifier) {
        throwUp();
    }

    /**
     * Checks if is cellar identifier exists.
     *
     * @param cellarIdentifier the cellar identifier
     * @return true, if is cellar identifier exists
     * @see eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService#isCellarIdentifierExists(java.lang.String)
     */
    @Override
    public boolean isCellarIdentifierExists(final String cellarIdentifier) {
        throwUp();
        return false;
    }

    /**
     * Delete cellar identifier.
     *
     * @param cellarIdentifier the cellar identifier
     * @see eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService#deleteCellarIdentifier(eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier)
     */
    @Override
    public void deleteCellarIdentifier(final CellarIdentifier cellarIdentifier) {
        throwUp();
    }

    /**
     * Save.
     *
     * @param productionIdentifier the production identifier
     * @param ignoreDuplicatedPids the ignore duplicated pids
     * @see eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService#save(eu.europa.ec.opoce.cellar.cl.domain.admin.ProductionIdentifier, boolean)
     */
    @Override
    public void save(final ProductionIdentifier productionIdentifier, final boolean ignoreDuplicatedPids) {
        throwUp();
    }

    /**
     * Update.
     *
     * @param cellarIdentifier the cellar identifier
     * @see eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService#update(eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier)
     */
    @Override
    public void update(final CellarIdentifier cellarIdentifier) {
        throwUp();
    }

    /**
     * Save.
     *
     * @param digitalObject the digital object
     * @param cellarId the cellar id
     * @param ignoreDuplicatedPids the ignore duplicated pids
     * @see eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService#save(List, String, boolean, Boolean)
     */
    @Override
    public void save(final DigitalObject digitalObject, final String cellarId, final boolean ignoreDuplicatedPids) {
        throwUp();
    }

    /** {@inheritDoc} */
    @Override
    public void save(final List<ContentIdentifier> contentIdentifiers, final String cellarId,
                     final boolean ignoreDuplicatedPids, final Boolean readOnly) {
        throwUp();
    }

    /**
     * Assign and save cellar identifiers.
     *
     * @param structMap the struct map
     * @param sipResource the sip resource
     * @return true, if successful
     * @see eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService#assignAndSaveCellarIdentifiers(eu.europa.ec.opoce.cellar.domain.content.mets.StructMap, eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource)
     */
    @Override
    public boolean assignAndSaveCellarIdentifiers(final StructMap structMap, final SIPResource sipResource) {
        throwUp();
        return false;
    }

    /**
     * Save datastream identifiers.
     *
     * @param structMap the struct map
     * @param sipResource the sip resource
     * @see eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService#saveDatastreamIdentifiers(eu.europa.ec.opoce.cellar.domain.content.mets.StructMap, eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource)
     */
    @Override
    public void saveDatastreamIdentifiers(final StructMap structMap, final SIPResource sipResource) {
        throwUp();
    }

    /**
     * Save datastream identifiers.
     *
     * @param digitalObject the digital object
     * @param sipResource the sip resource
     * @see eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService#saveDatastreamIdentifiers(eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject, eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource)
     */
    @Override
    public void saveDatastreamIdentifiers(final DigitalObject digitalObject, final SIPResource sipResource) {
        throwUp();
    }

    /**
     * Map all the given content identifiers to the same cellar identifier.
     * This method also create the missing production identifiers.
     *
     * @param contentIds the content ids
     * @param cid the cid
     * @return the list
     */
    @Override
    public List<String> mapProductionIdentifiersToCellarIdentifier(final List<ContentIdentifier> contentIds, final CellarIdentifier cid) {
        throwUp();
        return null;
    }

    /**
     * Gets the uri from prefix.
     *
     * @param prefixed the prefixed
     * @return the uri from prefix
     */
    private String getUriFromPrefix(final String prefixed) {
        if (StringUtils.trimToNull(prefixed) == null) {
            throw ExceptionBuilder.get(CellarException.class).build();
        }
        String result = null;
        String prefixWithColon;
        for (final String prefix : this.prefixConfigurationService.getPrefixUris().keySet()) {
            prefixWithColon = prefix + ":";
            //PIDS may now have multiple parts e.g.: dataset:bla:bla:bla...
            if (prefixed.startsWith(prefixWithColon)) {
                //http://[dissemination_base]/prefix/
                final String prefixUri = this.prefixConfigurationService.getPrefixUri(prefix);
                //http://[dissemination_base]/prefix/bla:bla:bla
                final String fullUri = prefixed.replace(prefixWithColon, prefixUri);
                //bla:bla:bla
                final String secondPartOfIdentifier = StringUtils.substringAfter(fullUri, prefixUri);
                //bla/bla/bla
                final String secondPartOfIdentifierAsUri = secondPartOfIdentifier.replace(":", "/");
                //http://[dissemination_base]/prefix/bla/bla/bla
                result = prefixUri + secondPartOfIdentifierAsUri;
                break;
            }
        }
        if (result == null) {
            throw new IllegalArgumentException(MessageFormatter.format("unknown prefixed: {}", prefixed));
        }
        return result;
    }

    /**
     * Gets the prefixed or null from uri.
     *
     * @param uri the uri
     * @return the prefixed or null from uri
     */
    private String getPrefixedOrNullFromUri(final String uri) {
        //http://[dissemination_base]/prefix/bla/bla/bla
        if (StringUtils.trimToNull(uri) == null) {
            throw ExceptionBuilder.get(CellarException.class).build();
        }
        for (final String prefix : this.prefixConfigurationService.getPrefixUris().keySet()) {
            //http://[dissemination_base]/prefix
            final String prefixUri = this.prefixConfigurationService.getPrefixUri(prefix);
            if (uri.startsWith(prefixUri)) {
                //prefix:bla/bla/bla
                return uri.replace(prefixUri, prefix + ":");
            }
        }
        return null;
    }

    /**
     * CREATE a {@link ProductionIdentifier} with a give productionId an the current {@link Date}.
     *
     * @param productionId            the productionid to assign to the new {@link ProductionIdentifier}.
     * @return the {@link ProductionIdentifier} created.
     */
    private ProductionIdentifier initializeProductionIdentifier(final String productionId) {
        final ProductionIdentifier pid = new ProductionIdentifier();
        pid.setProductionId(productionId);
        pid.setCreationDate(new Date());
        return pid;
    }

    /**
     * Parses the {@link URI} of the resource and extracts the identifier.
     *
     * @param uri
     *            the {@link URI} of the resource.
     * @return the identifier.
     */
    private String getIdFromURI(final String uri) {
        String identifier = null;
        identifier = StringUtils.remove(uri, this.pidManagerConfigParams.getCellarResourceBaseURI());
        return identifier;
    }

    /**
     * Indicates whether the provided string is a cellar id or not.
     * @param str the string to test
     * @return true if the string is a cellar id, false otherwise
     */
    private boolean isCellarId(final String str) {
        return StringUtils.startsWith(str, this.pidManagerConfigParams.getCellarS3Namespace());
    }

    /**
     * Gets the production identifiers as strings.
     *
     * @param productionIdentifiers the production identifiers
     * @return the production identifiers as strings
     */
    private List<String> getProductionIdentifiersAsStrings(final List<ProductionIdentifier> productionIdentifiers) {
        final List<String> pids = new LinkedList<String>();
        for (final ProductionIdentifier productionIdentifier : productionIdentifiers) {
            pids.add(productionIdentifier.getProductionId());
        }

        return pids;
    }

    /**
     * Throw up.
     */
    private static void throwUp(/*eeeks!*/) {
        throw NotImplementedExceptionBuilder.get().withMessage("This method is not implemented for this service.").build();
    }

    /** {@inheritDoc} */
    @Override
    public CellarIdentifier getCellarIdentifierByUuid(final String uuid) {
        return this.cellarIdentifierDao.getCellarIdentifier(uuid);
    }

    /** {@inheritDoc} */
    @Override
    public String getCellarUriOrNull(final String uriOrPrefix) {
        final String cellarPrefixed = this.getCellarPrefixedOrNull(uriOrPrefix);

        if (cellarPrefixed == null) {
            return null;
        }

        return this.getUri(cellarPrefixed);
    }

}
