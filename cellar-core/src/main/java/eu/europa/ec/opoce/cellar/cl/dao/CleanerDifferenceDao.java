/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.dao
 *             FILE : CleanerTripleDao.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Sep 27, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao;

import eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner.CleanerDifference;
import eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner.CleanerDifferencePaginatedList;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Sep 27, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface CleanerDifferenceDao extends BaseDao<CleanerDifference, Long> {

    Long getCleanerDifferenceCount();

    void findCleanerDifferences(final CleanerDifferencePaginatedList cdpl);
}
