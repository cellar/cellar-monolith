/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.dao
 *             FILE : ExtractionConfigurationDao.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 5, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao;

import java.util.List;

import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionConfiguration;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 5, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface ExtractionConfigurationDao extends BaseDao<ExtractionConfiguration, Long> {

    /**
     * Returns all job configurations.
     * @return all job configuration
     */
    public List<Object> findJobExtractionConfigurations();

    /**
     * Returns all ad-hoc configurations.
     * @return all ad-hoc configuration
     */
    public List<Object> findAdhocExtractionConfigurations();

    /**
     * Adds an extraction configuration.
     * @param extractionConfiguration the extraction configuration to add
     */
    public void addExtractionConfiguration(final ExtractionConfiguration extractionConfiguration);

    /**
     * Returns the identified configuration.
     * @param id the identifier of configuration
     * @return the identified configuration
     */
    public ExtractionConfiguration findExtractionConfiguration(final Long id);

    /**
     * Deletes the identified configuration.
     * @param configurationId the configuration identifier
     */
    public void deleteExtractionConfiguration(final Long id);
}
