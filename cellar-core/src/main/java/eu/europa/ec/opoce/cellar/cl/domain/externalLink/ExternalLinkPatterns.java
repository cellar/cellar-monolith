/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.externalLink
 *             FILE : ExternalLinkPatterns.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 18, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.externalLink;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <class_description> Class that represents a list of {@link ExternalLinkPattern} objects.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 18, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlType(name = "externalLinkPatternsType")
@XmlRootElement(name = "externalLinkPatterns")
public class ExternalLinkPatterns {

    /**
     * The list of {@link ExternalLinkPattern} objects to wrap.
     */
    private List<ExternalLinkPattern> externalLinkPatterns;

    /**
     * Constructor.
     */
    public ExternalLinkPatterns() {
        externalLinkPatterns = new ArrayList<ExternalLinkPattern>();
    }

    /**
     * Constructor.
     * @param externalLinkPatterns list of {@link ExternalLinkPattern} objects
     */
    public ExternalLinkPatterns(final List<ExternalLinkPattern> externalLinkPatterns) {
        this.externalLinkPatterns = externalLinkPatterns;
    }

    /**
     * Gets the list of {@link ExternalLinkPattern} objects.
     * @return the list of {@link ExternalLinkPattern}
     */
    @XmlElement(name = "externalLinkPattern")
    public List<ExternalLinkPattern> getExternalLinkPatterns() {
        return this.externalLinkPatterns;
    }

    /**
     * Sets the list of {@link ExternalLinkPattern} objects.
     * @param externalLinkPatterns the list of {@link ExternalLinkPattern} to set
     */
    public void setExternalLinkPatterns(final List<ExternalLinkPattern> externalLinkPatterns) {
        this.externalLinkPatterns = externalLinkPatterns;
    }
}
