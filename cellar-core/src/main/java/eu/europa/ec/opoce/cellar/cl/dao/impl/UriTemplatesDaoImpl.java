/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.dao.impl
 *             FILE : BatchJobDaoImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 11, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao.impl;

import eu.europa.ec.opoce.cellar.cl.dao.UriTemplatesDao;
import eu.europa.ec.opoce.cellar.cl.domain.sparql.UriTemplates;

import java.util.List;

import org.springframework.stereotype.Repository;

/**
 * <class_description> Uri Templates dao.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 11, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Repository
public class UriTemplatesDaoImpl extends BaseDaoImpl<UriTemplates, Long> implements UriTemplatesDao {

    /**
     * {@inheritDoc}
     */
    @Override
    public UriTemplates saveObject(final UriTemplates uriTemplates) {
        return super.saveObject(uriTemplates);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UriTemplates updateObject(final UriTemplates uriTemplates) {
        return super.updateObject(uriTemplates);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteObject(final UriTemplates uriTemplates) {
        super.deleteObject(uriTemplates);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UriTemplates> findAll() {
        return super.findAll();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UriTemplates> findAllOrderedBySequence() {
        return super.findObjectsByNamedQuery("findAllOrderedBySequence");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UriTemplates> findUriTemplatesBySequence(final Long sequence) {
        return super.findObjectsByNamedQuery("findUriTemplatesBySequence", sequence);
    }

}
