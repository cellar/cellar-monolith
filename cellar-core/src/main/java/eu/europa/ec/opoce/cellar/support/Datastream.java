/**
 * 
 */
package eu.europa.ec.opoce.cellar.support;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * @author dsavares
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class Datastream {

    @XmlAttribute(name = "dsid")
    private String dsid;
    @XmlAttribute(name = "label")
    private String label;
    @XmlAttribute(name = "mimeType")
    private String mimeType;

    /**
     * Sets a new value for the mimeType.
     * @param mimeType the mimeType to set
     */
    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    /**
     * Gets the value of the mimeType.
     * @return the mimeType
     */
    public String getMimeType() {
        return mimeType;
    }

    /**
     * Sets a new value for the label.
     * @param label the label to set
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Gets the value of the label.
     * @return the label
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets a new value for the dsid.
     * @param dsid the dsid to set
     */
    public void setDsid(String dsid) {
        this.dsid = dsid;
    }

    /**
     * Gets the value of the dsid.
     * @return the dsid
     */
    public String getDsid() {
        return dsid;
    }

}
