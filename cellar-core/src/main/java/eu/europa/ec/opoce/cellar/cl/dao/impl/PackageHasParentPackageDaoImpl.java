/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.dao.impl
 *        FILE : PackageHasParentPackageDaoImpl.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 10-12-2021
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2021 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Repository;

import eu.europa.ec.opoce.cellar.cl.dao.PackageHasParentPackageDao;
import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.PackageHasParentPackage;

/**
 * Provides methods for accessing the PACKAGE_HAS_PARENT_PACKAGE
 * DB table.
 * @author EUROPEAN DYNAMICS S.A.
 */
@Repository
public class PackageHasParentPackageDaoImpl extends BaseDaoImpl<PackageHasParentPackage, Long> implements PackageHasParentPackageDao {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PackageHasParentPackage findBySipId(Long sipId) {
		Map<String, Object> params = new HashMap<>();
		params.put("sipId", sipId);
		return super.findObjectByNamedQueryWithNamedParams("PackageHasParentPackage.findBySipId", params);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<PackageHasParentPackage> findBySipIdOrParentSipIdIn(Collection<Long> sipIds) {
		Map<String, Object> params = new HashMap<>();
		params.put("sipIds", sipIds);
		return super.findObjectsByNamedQueryWithNamedParams("PackageHasParentPackage.findBySipIdOrParentSipIdIn", params);
	}

}
