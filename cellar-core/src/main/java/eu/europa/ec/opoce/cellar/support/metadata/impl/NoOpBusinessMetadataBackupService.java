package eu.europa.ec.opoce.cellar.support.metadata.impl;

import eu.europa.ec.opoce.cellar.cl.domain.response.DigitalObjectOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.IOperation;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;

public class NoOpBusinessMetadataBackupService extends AbstractBusinessMetadataBackupService {

    public NoOpBusinessMetadataBackupService(ContentStreamService contentStreamService) {
        super(contentStreamService);
    }

    @Override
    public void backupBusinessMetadata(StructMap structMap, IOperation<DigitalObjectOperation> digitalObjectOperationIOperation) {
        // NO OP
    }
}
