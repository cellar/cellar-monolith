package eu.europa.ec.opoce.cellar.ccr.helper;

import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.cl.dao.ProductionIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.admin.ProductionIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.StructMapProcessorException;

import org.apache.commons.lang.StringUtils;

public class ProductionIdentifierHelper {

    public enum IdentifierType {
        CELLAR, PRODUCTION;

        @Override
        public String toString() {
            return super.toString().toLowerCase();
        }
    }

    /**
     * Returns the production identifier corresponding at this digital object.
     * @param digitalObject the digital object to find the production identifier.
     * @return the corresponding production identifier or null if no pid found for this DO.
     */
    public static ProductionIdentifier getProductionIdentifier(DigitalObject digitalObject,
            ProductionIdentifierDao productionIdentifierDao) {
        ProductionIdentifier pid = null;
        for (ContentIdentifier id : digitalObject.getContentids()) {
            pid = productionIdentifierDao.getProductionIdentifier(id.getIdentifier());
            if (pid != null) {
                break;
            }
        }
        return pid;
    }

    /**
     * Gets the datastream name.
     * Split the cellarIdentifier of a content after the '/'
     * i.e. xxxx/DOC_x => DOC_x is the datastream Name
     * 
     * @return datastream id like DOC_x or null if no datastream id found.
     */
    public static final String getDatastreamName(CellarIdentifier cellarIdentifier) {
        String dataStreamId = StringUtils.substringAfterLast(cellarIdentifier.getUuid(), "/");
        return StringUtils.isNotBlank(dataStreamId) ? dataStreamId : null;
    }

    /**
     * Gets the uuid without datastream id if applicable.
     * 
     * @return uuid without datastream id if it exists.
     */
    public static final String getUUIDWithoutDatastreamName(CellarIdentifier cellarIdentifier) {
        String dataStreamId = StringUtils.substringBeforeLast(cellarIdentifier.getUuid(), "/");
        return StringUtils.isNotBlank(dataStreamId) ? dataStreamId : null;
    }

    /**
     * Method that just construct and throws an exception if the given pid is null. 
     * 
     * @param pid the production identifier to test.
     * @param digitalObject the digital object.
     */
    public static void checkProductionIdentifierExists(final ProductionIdentifier pid, final DigitalObject digitalObject) {
        if (pid == null) {
            throwNonExistingIdentifierException(IdentifierType.PRODUCTION, digitalObject);
        }
    }

    /**
     * Method that just construct and throws an exception if the given cellarId is null. 
     * 
     * @param pid the production identifier to test.
     * @param digitalObject the digital object.
     */
    public static void checkCellarIdExists(final String cellarId, final DigitalObject digitalObject) {
        if (cellarId == null) {
            throwNonExistingIdentifierException(IdentifierType.CELLAR, digitalObject);
        }
    }

    /**
     * Method that just throws an exception informing that the operation being attempted is not possible
     * because the identifier of type {@link identifierType} does not exist. 
     * 
     * @param identifierType the type of identifier ("cellar or "production"}.
     * @param digitalObject the digital object.
     */
    public static void throwNonExistingIdentifierException(final IdentifierType identifierType, final DigitalObject digitalObject) {
        throw ExceptionBuilder.get(StructMapProcessorException.class) //
                .withCode(CoreErrors.E_027) //
                .withMessage("Unable to perform operation on {} identified by [{}], because it has no {} identifier.") //
                .withMessageArgs(digitalObject.getType().getLabel(), StringUtils.join(digitalObject.getContentids(), ", "), identifierType) //
                .build();
    }

}
