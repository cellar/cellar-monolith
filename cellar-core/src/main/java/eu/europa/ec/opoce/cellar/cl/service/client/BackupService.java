package eu.europa.ec.opoce.cellar.cl.service.client;

import eu.europa.ec.opoce.cellar.exception.SIPException;

import java.io.File;

/**
 * Interface for SIP Backup.
 * 
 * @author dcraeye
 * 
 */
public interface BackupService {

    /**
     * Backup foxml folder.
     *
     * @param foxmlFolder the foxml folder
     * @throws SIPException the SIP exception
     */
    void backupFoxmlFolder(String foxmlFolder) throws SIPException;

    /**
     * Backup mets folder.
     *
     * @param metsFolderName the mets folder name
     * @throws SIPException the SIP exception
     */
    void backupMetsFolder(File metsFolderName) throws SIPException;

    /**
     * Backup mets folder.
     *
     * @param metsFolderName the mets folder name
     * @throws SIPException the SIP exception
     */
    void backupMetsFolder(String metsFolderName) throws SIPException;

    /**
     * Backup sip file.
     *
     * @param sipFile the sip file
     * @throws SIPException the SIP exception
     */
    boolean backupSIPFile(File sipFile) throws SIPException;

    /**
     * Error mets folder.
     *
     * @param metsFolderName the mets folder name
     * @throws SIPException the SIP exception
     */
    void errorMetsFolder(File metsFolderName) throws SIPException;

    /**
     * Error mets folder.
     * Moves the mets folder into the error folder
     * @param metsFolderName the mets folder name
     * @throws SIPException the SIP exception
     */
    void errorMetsFolder(String metsFolderName) throws SIPException;

    /**
     * Error sip file.
     * Moves the SIP file into the error folder
     * @param sipFile the sip file
     * @throws SIPException the SIP exception
     */
    void errorSIPFile(File sipFile) throws SIPException;

    /**
     * Error sip file.
     * Moves or copies the SIP file into the error folder depending on the deleteSource flag
     * @param sipFile the sip file
     * @param deleteSource the delete source
     * @throws SIPException the SIP exception
     */
    void errorSIPFile(File sipFile, boolean deleteSource) throws SIPException;

}
