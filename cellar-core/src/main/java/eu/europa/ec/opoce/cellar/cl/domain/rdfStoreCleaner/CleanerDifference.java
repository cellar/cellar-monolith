/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner
 *             FILE : CleanerTriple.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Sep 27, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import java.util.Date;

import eu.europa.ec.opoce.cellar.cl.configuration.enums.CellarServiceRDFStoreCleanerMode;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Sep 27, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Entity
@Table(name = "CLEANER_DIFFERENCE")
@NamedQueries({
        @NamedQuery(name = "findCountCleanerDifferences", query = "select count(c.id) from CleanerDifference c")})
public class CleanerDifference {

    /*
     * create table CLEANER_TRIPLE (ID number(19,0) not null, OPERATION_TYPE number(1,0) not null, 
     * QUERY_TYPE number(1,0) not null, CMR_TABLE number(1,0) not null, CELLAR_ID varchar2(500 byte) not null, 
     * TRIPLES clob not null, OPERATION_DATE timestamp not null, primary key (ID)) LOB (TRIPLES) STORE AS 
     * (TABLESPACE CELLAR_CLEANER_LOB_01);
     */

    public static enum QueryType {
        add, delete;
    };

    public static enum CmrTable {
        CMR_METADATA, CMR_INVERSE, CMR_INVERSE_RELATIONS;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "OPERATION_TYPE", nullable = false)
    private CellarServiceRDFStoreCleanerMode operationType;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "QUERY_TYPE", nullable = false)
    private QueryType queryType;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "CMR_TABLE", nullable = false)
    private CmrTable cmrTable;

    @Column(name = "CELLAR_ID", nullable = false)
    private String cellarId;

    @Column(name = "DIFFERENCE", nullable = false)
    private String difference;

    @Column(name = "OPERATION_DATE", nullable = false)
    private Date operationDate;

    public CleanerDifference() {

    }

    public CleanerDifference(final CellarServiceRDFStoreCleanerMode operationType, final QueryType queryType, final CmrTable cmrTable,
            final String cellarId, final String difference) {
        this.operationType = operationType;
        this.queryType = queryType;
        this.cmrTable = cmrTable;
        this.cellarId = cellarId;
        this.difference = difference;
        this.operationDate = new Date();
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the operationType
     */
    public CellarServiceRDFStoreCleanerMode getOperationType() {
        return operationType;
    }

    /**
     * @param operationType the operationType to set
     */
    public void setOperationType(CellarServiceRDFStoreCleanerMode operationType) {
        this.operationType = operationType;
    }

    /**
     * @return the queryType
     */
    public QueryType getQueryType() {
        return queryType;
    }

    /**
     * @param queryType the queryType to set
     */
    public void setQueryType(QueryType queryType) {
        this.queryType = queryType;
    }

    /**
     * @return the cmrTable
     */
    public CmrTable getCmrTable() {
        return cmrTable;
    }

    /**
     * @param cmrTable the cmrTable to set
     */
    public void setCmrTable(CmrTable cmrTable) {
        this.cmrTable = cmrTable;
    }

    /**
     * @return the cellarId
     */
    public String getCellarId() {
        return cellarId;
    }

    /**
     * @param cellarId the cellarId to set
     */
    public void setCellarId(String cellarId) {
        this.cellarId = cellarId;
    }

    /**
     * @return the triples
     */
    public String getDifference() {
        return difference;
    }

    /**
     * @param triples the triples to set
     */
    public void setDifference(String difference) {
        this.difference = difference;
    }

    /**
     * @return the operationDate
     */
    public Date getOperationDate() {
        return operationDate;
    }

    /**
     * @param operationDate the operationDate to set
     */
    public void setOperationDate(Date operationDate) {
        this.operationDate = operationDate;
    }
}
