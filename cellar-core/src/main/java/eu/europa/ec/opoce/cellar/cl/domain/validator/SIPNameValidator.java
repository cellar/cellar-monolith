package eu.europa.ec.opoce.cellar.cl.domain.validator;

import java.io.File;

import org.springframework.stereotype.Component;

import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.cl.exception.ValidationException;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.support.FileExtensionUtils;

/**
 * Implementation of the {@link ContentValidator}.
 * Validate the sip file name. Throws a CellarException if the file name is not in the format
 * {@code"<metsDocumentID>.zip". Where <metsDocumentID> is the ID of the mets document}
 * (ex: oj:JOL_2006_088_R_0063_01.zip)
 * 
 * @param sipFile
 *            the sip file name.
 * @param sipResource
 *            the sipResource object which containt the mets document ID.
 * 
 * @throws MyException
 *             if the sip file name id not valid.
 * 
 * @author dcraeye
 *
 */
@Component("sipNameValidator")
public class SIPNameValidator implements SystemValidator {

    /**
     * {@inheritDoc}
     */
    @Override
    public ValidationResult validate(ValidationDetails validationDetails) {
        ValidationResult result = new ValidationResult();

        if (validationDetails.getToValidate() == null) {
            throw ExceptionBuilder.get(ValidationException.class).withCode(CoreErrors.E_070).withMessage("object cannot be null")
                    .withCause(new NullPointerException()).build();
        }
        if (!(validationDetails.getToValidate() instanceof File)) {
            throw ExceptionBuilder.get(ValidationException.class).withCode(CoreErrors.E_072)
                    .withMessage(validationDetails.getToValidate().getClass().toString()).build();
        }

        result.setValid(validateSIPFileName((File) validationDetails.getToValidate(), validationDetails.getSipResource().getMetsID()));

        return result;
    }

    private boolean validateSIPFileName(File sipFile, String metsId) throws CellarException {
        return sipFile.getName().equals(metsId + FileExtensionUtils.SIP);
    }
}
