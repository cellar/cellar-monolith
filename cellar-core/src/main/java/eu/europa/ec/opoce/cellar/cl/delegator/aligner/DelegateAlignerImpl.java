package eu.europa.ec.opoce.cellar.cl.delegator.aligner;

import eu.europa.ec.opoce.cellar.cl.concurrency.Concurrent;
import eu.europa.ec.opoce.cellar.cl.concurrency.IConcurrentArgsResolver;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.enums.CellarServiceRDFStoreCleanerMode;
import eu.europa.ec.opoce.cellar.cl.dao.aligner.AlignerMisalignmentDao;
import eu.europa.ec.opoce.cellar.cl.delegator.AbstractDelegator;
import eu.europa.ec.opoce.cellar.cl.domain.aligner.AlignerMisalignment;
import eu.europa.ec.opoce.cellar.cl.domain.aligner.Hierarchy;
import eu.europa.ec.opoce.cellar.cl.domain.aligner.HierarchyNode;
import eu.europa.ec.opoce.cellar.cl.enums.aligner.Repository;
import eu.europa.ec.opoce.cellar.cl.service.aligner.IHierarchyAlignerService;
import eu.europa.ec.opoce.cellar.cl.service.aligner.IHierarchyLoaderService;
import eu.europa.ec.opoce.cellar.cmr.AlignerStatus;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.database.transaction.RDFTransactional;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.OffIngestionOperationType;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class DelegateAlignerImpl.
 */
@Component
public class DelegateAlignerImpl implements DelegateAligner, IConcurrentArgsResolver {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(DelegateAlignerImpl.class);

    /** The cellar configuration. */
    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    /** The cellar resource dao. */
    @Autowired
    private CellarResourceDao cellarResourceDao;

    /** The hierarchy aligner service. */
    @Autowired
    private IHierarchyAlignerService hierarchyAlignerService;

    /** The hierarchy loader service. */
    @Autowired
    private IHierarchyLoaderService hierarchyLoaderService;

    /** The aligner misalignment dao. */
    @Autowired
    private AlignerMisalignmentDao alignerMisalignmentDao;

    /** {@inheritDoc} */
    @Concurrent(locker = OffIngestionOperationType.ALIGNING_SCHEDULING)
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public void startProcessing(final AbstractDelegator delegator) {
        try {
            final Collection<CellarResource> processableCellarResources = delegator.getCellarResources();
            for (final CellarResource cellarResource : processableCellarResources) {
                cellarResource.setAlignerStatus(AlignerStatus.PROCESSING);
                this.cellarResourceDao.updateObject(cellarResource);
            }
        } catch (final Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
    }

    /** {@inheritDoc} */
    @Override
    @Concurrent(locker = OffIngestionOperationType.ALIGNING)
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @RDFTransactional
    public void align(final AbstractDelegator delegator) {

        try {

            final String rootCellarId = delegator.getRootCellarId();
            final Hierarchy hierarchy = this.hierarchyLoaderService.getHierarchy(rootCellarId);

            final Map<String, HierarchyNode> nodes = hierarchy.getNodes();
            final String cellarServiceAlignerModeString = this.cellarConfiguration.getCellarServiceAlignerMode();
            final CellarServiceRDFStoreCleanerMode cellarServiceAlignerMode = CellarServiceRDFStoreCleanerMode
                    .resolve(cellarServiceAlignerModeString);
            final Date nowDate = new Date();

            for (final Entry<String, HierarchyNode> entry : nodes.entrySet()) {
                final HierarchyNode hierarchyNode = entry.getValue();
                final ContentIdentifier contentIdentifier = hierarchyNode.getCellarId();
                final String cellarId = contentIdentifier.getIdentifier();
                final List<Repository> missingRepos = hierarchyNode.getMissingRepositories();
                for (final Repository repository : missingRepos) {
                    final AlignerMisalignment alignerMisalignment = new AlignerMisalignment();
                    alignerMisalignment.setCellarId(cellarId);
                    alignerMisalignment.setOperationDate(nowDate);
                    alignerMisalignment.setOperationType(cellarServiceAlignerMode);
                    alignerMisalignment.setRepository(repository);
                    this.alignerMisalignmentDao.save(alignerMisalignment);
                }
            }

            if (CellarServiceRDFStoreCleanerMode.fix.equals(cellarServiceAlignerMode)) {
                this.setStatuses(hierarchy, AlignerStatus.FIXED, AlignerStatus.VALID);
                this.hierarchyAlignerService.fixHierarchy(hierarchy);
            } else {
                this.setStatuses(hierarchy, AlignerStatus.CORRUPT, AlignerStatus.VALID);
            }

        } catch (final Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            for (final CellarResource cellarResource : delegator.getCellarResources()) {
                cellarResource.setAlignerStatus(AlignerStatus.ERROR);
                this.cellarResourceDao.updateObject(cellarResource);
            }

        }
    }

    /**
     * Sets the statuses.
     *
     * @param map the map
     * @param statusForEvicted the status for evicted
     * @param statusForNonEvicted the status for non evicted
     */
    private void setStatuses(final Hierarchy hierarchy, final AlignerStatus statusForEvicted, final AlignerStatus statusForNonEvicted) {
        final Map<String, HierarchyNode> map = hierarchy.getNodes();
        for (final Entry<String, HierarchyNode> entry : map.entrySet()) {
            final HierarchyNode node = entry.getValue();
            final ContentIdentifier cellarId = node.getCellarId();
            final String identifier = cellarId.getIdentifier();
            final CellarResource cellarResource = this.cellarResourceDao.findCellarId(identifier);
            if (cellarResource != null) {
                if (node.isEvicted()) {
                    cellarResource.setAlignerStatus(statusForEvicted);
                    this.cellarResourceDao.updateObject(cellarResource);
                } else {
                    cellarResource.setAlignerStatus(statusForNonEvicted);
                    this.cellarResourceDao.updateObject(cellarResource);
                }
            }
        }
    }

    /** {@inheritDoc} */
    @Override
    public Object[] resolveArgsForConcurrency(final Object[] in) {
        return new Object[] {
                ((AbstractDelegator) in[0]).getRootCellarId()};
    }

}
