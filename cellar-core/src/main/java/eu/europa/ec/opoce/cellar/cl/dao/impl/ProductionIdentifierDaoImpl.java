package eu.europa.ec.opoce.cellar.cl.dao.impl;

import eu.europa.ec.opoce.cellar.cl.dao.ProductionIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.admin.ProductionIdentifier;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Implementation of the {@code ProdIdCellarIdMappingDao}.
 *
 * @author dsavares
 */
@Repository
public class ProductionIdentifierDaoImpl extends BaseDaoImpl<ProductionIdentifier, Long> implements ProductionIdentifierDao {

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteProductionIdentifier(final ProductionIdentifier productionIdentifier) {
        super.deleteObject(productionIdentifier);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ProductionIdentifier> getProductionIdentifiers() {
        return super.findAll();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProductionIdentifier getProductionIdentifier(final String identifier) {
        return super.findObjectByNamedQuery("getByIdentifier", identifier);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ProductionIdentifier> getProductionIdentifiers(final List<String> identifiers) {

        return getHibernateTemplate().execute(session -> {
            final Query query = session.getNamedQuery("getByIdentifiers");
            query.setParameterList("pids", identifiers);
            return query.list();
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ProductionIdentifier> getUnboundResources() {
        return super.findObjectsByNamedQuery("getUnboundResources");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ProductionIdentifier getProductionIdentifier(final Long id) {
        return super.getObject(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveProductionIdentifier(final ProductionIdentifier productionIdentifier) {
        super.saveObject(productionIdentifier);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isProductionIdentifierExists(ProductionIdentifier productionIdentifier) {
        return this.getProductionIdentifier(productionIdentifier.getProductionId()) != null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ProductionIdentifier> getProductionIdentifiers(CellarIdentifier cellarIdentifier) {
        return super.findObjectsByNamedQuery("getProductionIdentifiersByCellarIdentifier", cellarIdentifier.getId());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ProductionIdentifier> getProductionIdentifiersByCellarId(final String cellarId) {
        return super.findObjectsByNamedQuery("getProductionIdentifiersByCellarId", cellarId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ProductionIdentifier> getProductionIdentifiersByCellarIds(final List<String> cellarIds, final int pageSize) {
        return super.findObjectsByNamedQueryWithPagedParams("getProductionIdentifiersByCellarIds", "ids", cellarIds, pageSize);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ProductionIdentifier> getProductionIdentifiersByProductionIds(final List<String> productionIds, final int pageSize) {
        return super.findObjectsByNamedQueryWithPagedParams("getProductionIdentifiersByProductionIds", "ids", productionIds, pageSize);
    }
}
