/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *        FILE : PidManagerServiceImpl.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 16-03-2012
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import com.google.common.base.Throwables;
import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration;
import eu.europa.ec.opoce.cellar.cl.dao.CellarIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.dao.ProductionIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.admin.ProductionIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.cl.domain.history.service.IdentifiersHistoryService;
import eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentStream;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectStrategy;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsDocument;
import eu.europa.ec.opoce.cellar.domain.content.mets.OperationType;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.PidManagerException;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * <class_description> Implementation of the {@link PidManagerService}.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 16-03-2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service("pidManagerService")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PidManagerServiceImpl extends NonTransactionalReadOnlyPidManagerServiceImpl {

    private static final Logger LOG = LogManager.getLogger(PidManagerServiceImpl.class);

    @Autowired
    private ProductionIdentifierDao productionIdentifierDao;

    @Autowired
    private CellarIdentifierDao cellarIdentifierDao;

    @Autowired
    private IdentifiersHistoryService identifiersHistoryService;

    /**
     * Delete production identifier.
     *
     * @param identifier the identifier
     * @see eu.europa.ec.opoce.cellar.cl.service.impl.NonTransactionalReadOnlyPidManagerServiceImpl#deleteProductionIdentifier(java.lang.String)
     */
    @Override
    @Transactional
    public void deleteProductionIdentifier(final String identifier) {
        final ProductionIdentifier pid = this.getProductionIdentifierByIdentifier(identifier);
        if (pid == null) {
            throw ExceptionBuilder.get(PidManagerException.class).withCode(CoreErrors.E_037).withMessage(identifier + " doesn't exists.")
                    .build();
        }

        this.productionIdentifierDao.deleteProductionIdentifier(pid);
    }

    /**
     * Delete production identifier.
     *
     * @param productionIdentifier the production identifier
     * @see eu.europa.ec.opoce.cellar.cl.service.impl.NonTransactionalReadOnlyPidManagerServiceImpl#deleteProductionIdentifier(eu.europa.ec.opoce.cellar.cl.domain.admin.ProductionIdentifier)
     */
    @Override
    @Transactional
    public void deleteProductionIdentifier(final ProductionIdentifier productionIdentifier) {
        this.productionIdentifierDao.deleteProductionIdentifier(productionIdentifier);
    }

    /**
     * Delete cellar identifier.
     *
     * @param cellarIdentifier the cellar identifier
     * @see eu.europa.ec.opoce.cellar.cl.service.impl.NonTransactionalReadOnlyPidManagerServiceImpl#deleteCellarIdentifier(java.lang.String)
     */
    @Override
    @Transactional
    public void deleteCellarIdentifier(final String cellarIdentifier) {
        final CellarIdentifier cid = this.cellarIdentifierDao.getCellarIdentifier(cellarIdentifier);
        if (cid == null) {
            throw ExceptionBuilder.get(PidManagerException.class).withCode(CoreErrors.E_037)
                    .withMessage(cellarIdentifier + " doesn't exists.").build();
        }
        this.deleteCellarIdentifier(cid);
    }

    /**
     * Delete cellar identifier.
     *
     * @param cellarIdentifier the cellar identifier
     * @see eu.europa.ec.opoce.cellar.cl.service.impl.NonTransactionalReadOnlyPidManagerServiceImpl#deleteCellarIdentifier(eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier)
     */
    @Override
    @Transactional
    public void deleteCellarIdentifier(final CellarIdentifier cellarIdentifier) {
        //First delete linked production identifiers.
        final List<ProductionIdentifier> linkedProductionIdentifierList = this.productionIdentifierDao
                .getProductionIdentifiers(cellarIdentifier);
        for (final ProductionIdentifier productionIdentifier : linkedProductionIdentifierList) {
            this.productionIdentifierDao.deleteProductionIdentifier(productionIdentifier);
        }

        //Now delete cellar identifier.
        this.cellarIdentifierDao.deleteCellarIdentifier(cellarIdentifier);
    }

    /**
     * Checks if is cellar identifier exists.
     *
     * @param cellarIdentifier the cellar identifier
     * @return true, if is cellar identifier exists
     * @see eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService#isCellarIdentifierExists(java.lang.String)
     */
    @Override
    public boolean isCellarIdentifierExists(final String cellarIdentifier) {
        return this.cellarIdentifierDao.isCellarIdentifierExists(new CellarIdentifier(cellarIdentifier));
    }

    /**
     * Save.
     *
     * @param productionIdentifier the production identifier
     * @param ignoreDuplicatedPids the ignore duplicated pids
     * @see eu.europa.ec.opoce.cellar.cl.service.impl.NonTransactionalReadOnlyPidManagerServiceImpl#save(eu.europa.ec.opoce.cellar.cl.domain.admin.ProductionIdentifier, boolean)
     */
    @Override
    @Transactional
    public void save(final ProductionIdentifier productionIdentifier, final boolean ignoreDuplicatedPids) {
        //does it exist already ?
        final CellarIdentifier productionIdentifierCellarIdentifier = productionIdentifier.getCellarIdentifier();
        final String uuid = productionIdentifierCellarIdentifier.getUuid();
        CellarIdentifier cellarIdentifier = this.cellarIdentifierDao.getCellarIdentifier(uuid);
        if (cellarIdentifier == null) {
            cellarIdentifier = this.cellarIdentifierDao.saveCellarIdentifier(productionIdentifierCellarIdentifier);
        }
        productionIdentifier.setCellarIdentifier(cellarIdentifier);
        try {
            this.saveNewProductionIdentifier(productionIdentifier);
        } catch (final PidManagerException e) {
            if (!ignoreDuplicatedPids) {
                throw e;
            }
        }
    }

    /**
     * Update.
     *
     * @param cellarIdentifier the cellar identifier
     * @see eu.europa.ec.opoce.cellar.cl.service.impl.NonTransactionalReadOnlyPidManagerServiceImpl#update(eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier)
     */
    @Override
    @Transactional
    public void update(final CellarIdentifier cellarIdentifier) {
        this.cellarIdentifierDao.updateCellarIdentifier(cellarIdentifier);
    }

    /**
     * Save.
     *
     * @param digitalObject        the digital object
     * @param cellarId             the cellar id
     * @param ignoreDuplicatedPids the ignore duplicated pids
     * @see eu.europa.ec.opoce.cellar.cl.service.impl.NonTransactionalReadOnlyPidManagerServiceImpl#save(ProductionIdentifier, boolean)
     */
    @Override
    @Transactional
    public void save(final DigitalObject digitalObject, final String cellarId, final boolean ignoreDuplicatedPids) {
        try {
            this.mapProductionIdentifierToCellarIdentifier(digitalObject, cellarId);
        } catch (final PidManagerException e) {
            if (!ignoreDuplicatedPids) {
                throw e;
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void save(final List<ContentIdentifier> contentids, final String cellarId,
                     final boolean ignoreDuplicatedPids, final Boolean readOnly) {
        try {
            this.mapProductionIdentifierToCellarIdentifier(contentids, cellarId, readOnly);
        } catch (final PidManagerException e) {
            if (!ignoreDuplicatedPids) {
                throw e;
            }
        }
    }

    /**
     * Assign and save cellar identifiers.
     *
     * @param structMap   the struct map
     * @param sipResource the sip resource
     * @return true, if successful
     */
    @Override
    @Transactional
    public boolean assignAndSaveCellarIdentifiers(final StructMap structMap, final SIPResource sipResource) {
        final AssignAndSaveCellarIdentifiersStrategy strategy = new AssignAndSaveCellarIdentifiersStrategy(sipResource);
        structMap.getDigitalObject().applyStrategyOnSelfAndChildren(strategy);
        return strategy.isSuccess();
    }

    /**
     * Save datastream identifiers.
     *
     * @param structMap   the struct map
     * @param sipResource the sip resource
     * @see eu.europa.ec.opoce.cellar.cl.service.impl.NonTransactionalReadOnlyPidManagerServiceImpl#saveDatastreamIdentifiers(eu.europa.ec.opoce.cellar.domain.content.mets.StructMap, eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource)
     */
    @Override
    @Transactional
    public void saveDatastreamIdentifiers(final StructMap structMap, final SIPResource sipResource) {
        structMap.getDigitalObject().applyStrategyOnSelfAndChildren(new SaveDatastreamIdentifiersStrategy());
    }

    /**
     * Save datastream identifiers.
     *
     * @param digitalObject the digital object
     * @param sipResource   the sip resource
     * @see eu.europa.ec.opoce.cellar.cl.service.impl.NonTransactionalReadOnlyPidManagerServiceImpl#saveDatastreamIdentifiers(eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject, eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource)
     */
    @Override
    @Transactional
    public void saveDatastreamIdentifiers(final DigitalObject digitalObject, final SIPResource sipResource) {
        digitalObject.applyStrategyOnSelfAndChildren(new SaveDatastreamIdentifiersStrategy());
    }

    /**
     * Map all the given content identifiers to the same cellar identifier.
     * This method also create the missing production identifiers.
     *
     * @param contentIds the content ids
     * @param cid        the cid
     * @return the list
     */
    @Override
    public List<String> mapProductionIdentifiersToCellarIdentifier(final List<ContentIdentifier> contentIds, final CellarIdentifier cid) {
        final List<String> createdPidList = new LinkedList<String>();
        for (final ContentIdentifier contentId : contentIds) {
            final ProductionIdentifier pid = this.initializeProductionIdentifier(cid, contentId);
            if (this.saveProductionIdentifierIfNotExists(pid)) {
                createdPidList.add(pid.getProductionId());
            }

        }
        return createdPidList;
    }

    /**
     * Initialize production identifier.
     *
     * @param cid the cid
     * @param pid the pid
     * @return the production identifier
     */
    private ProductionIdentifier initializeProductionIdentifier(final CellarIdentifier cid, final ContentIdentifier pid) {
        final ProductionIdentifier productionIdentifier = this.initializeProductionIdentifier(pid.getIdentifier());
        productionIdentifier.setCellarIdentifier(cid);
        return productionIdentifier;
    }

    /**
     * CREATE a {@link ProductionIdentifier} with a give productionId an the current {@link Date}.
     *
     * @param productionId the productionid to assign to the new {@link ProductionIdentifier}.
     * @return the {@link ProductionIdentifier} created.
     */
    private ProductionIdentifier initializeProductionIdentifier(final String productionId) {
        final ProductionIdentifier pid = new ProductionIdentifier();
        pid.setProductionId(productionId);
        pid.setCreationDate(new Date());
        return pid;
    }

    /**
     * Save new cellar identifier.
     *
     * @param cid the cid
     */
    private final void saveNewCellarIdentifier(final CellarIdentifier cid) {
        if (this.cellarIdentifierDao.getCellarIdentifier(cid.getUuid()) != null) {
            throw ExceptionBuilder.get(PidManagerException.class).withCode(CoreErrors.E_031).withMessage("Cellar id '" + cid + "'").build();
        }
        this.cellarIdentifierDao.saveCellarIdentifier(cid);
    }

    /**
     * Save production identifier if not exists.
     *
     * @param pid the pid
     * @return true, if successful
     */
    private final boolean saveProductionIdentifierIfNotExists(final ProductionIdentifier pid) {
        if (this.productionIdentifierDao.getProductionIdentifier(pid.getProductionId()) == null) {
            this.productionIdentifierDao.saveProductionIdentifier(pid);
            return true;
        }
        return false;
    }

    /**
     * Save new production identifier.
     *
     * @param pid the pid
     * @throws PidManagerException the pid manager exception
     */
    private final void saveNewProductionIdentifier(final ProductionIdentifier pid) throws PidManagerException {
        final String productionId = pid.getProductionId();
        if (this.productionIdentifierDao.getProductionIdentifier(productionId) != null) {
            throw ExceptionBuilder.get(PidManagerException.class).withCode(CoreErrors.E_032)
                    .withMessage("Production id [" + productionId + "]").build();
        }
        this.productionIdentifierDao.saveProductionIdentifier(pid);
    }

    /**
     * Map production identifier to cellar identifier.
     *
     * @param digitalObject the digital object
     * @param uuid          the uuid
     */
    private void mapProductionIdentifierToCellarIdentifier(final DigitalObject digitalObject, final String uuid) {
        final List<ContentIdentifier> contentids = digitalObject.getContentids();
        mapProductionIdentifierToCellarIdentifier(contentids, uuid, digitalObject.getReadOnly());
    }

    /**
     * Map production identifier to cellar identifier.
     *
     * @param contentids the contentids
     * @param uuid       the uuid
     * @param readOnly   the read only
     */
    private void mapProductionIdentifierToCellarIdentifier(final List<ContentIdentifier> contentids, final String uuid,
                                                           final Boolean readOnly) {
        final CellarIdentifier cellarIdentifier = new CellarIdentifier(uuid);

        if (readOnly == null) {
            cellarIdentifier.setReadOnly(false);
        } else {
            cellarIdentifier.setReadOnly(readOnly);
        }

        if (ContentType.isCellarContentStreamUuid(uuid)) {
            final String substringBefore = StringUtils.substringBefore(uuid, "/");
            Integer maxDocIndex = this.cellarIdentifierDao.getMaxDocIndex(substringBefore);
            if (maxDocIndex == null) {
                maxDocIndex = 0;
            }
            cellarIdentifier.setDocIndex(maxDocIndex + 1);
        }

        this.saveCellarIdentifier(contentids, uuid, cellarIdentifier);
    }

    /**
     * Map production identifier to cellar identifier.
     *
     * @param contentIds        the content ids
     * @param contentIdentifier the content identifier
     * @param readOnly          the read only
     */
    private void mapProductionIdentifierToCellarIdentifier(final List<ContentIdentifier> contentIds,
                                                           final ContentIdentifier contentIdentifier, final Boolean readOnly) {
        final String cellarId = contentIdentifier.getIdentifier();
        final CellarIdentifier cellarIdentifier = new CellarIdentifier(cellarId);
        final int docIndex = contentIdentifier.getDocIndex();
        cellarIdentifier.setDocIndex(docIndex);
        final String uuid = cellarIdentifier.getUuid();

        if (readOnly == null) {
            cellarIdentifier.setReadOnly(false);
        } else {
            cellarIdentifier.setReadOnly(readOnly);
        }
        this.saveCellarIdentifier(contentIds, uuid, cellarIdentifier);
    }

    /**
     * Save cellar identifier.
     *
     * @param contentIds       the content ids
     * @param uuid             the uuid
     * @param cellarIdentifier the cellar identifier
     */
    private void saveCellarIdentifier(final List<ContentIdentifier> contentIds, final String uuid,
                                      final CellarIdentifier cellarIdentifier) {
        this.saveNewCellarIdentifier(cellarIdentifier);
        for (final ContentIdentifier contentId : contentIds) {
            if (!contentId.getIdentifier().equals(uuid)) {
                final ProductionIdentifier pid = this.initializeProductionIdentifier(cellarIdentifier, contentId);
                this.saveNewProductionIdentifier(pid);
            }
        }
    }

    /**
     * DigitalObjectStrategy to save datastream identifiers.
     *
     * @author ARHS Developments
     * @version $$Revision$$$
     */
    private final class SaveDatastreamIdentifiersStrategy implements DigitalObjectStrategy {

        /**
         * {@inheritDoc}
         */
        @Override
        public void applyStrategy(final DigitalObject digitalObject) {
            final DigitalObjectType digitalObjectType = digitalObject.getType();
            final boolean ignoreDuplicatedPids = OperationType.CreateOrIgnore
                    .equals(digitalObject.getStructMap().getMetsDocument().getType());
            if (digitalObjectType.equals(DigitalObjectType.MANIFESTATION)) {
                for (final ContentStream contentStream : digitalObject.getContentStreams()) {
                    try {
                        final ContentIdentifier cellarId = contentStream.getCellarId();
                        final List<ContentIdentifier> contentids = contentStream.getContentids();
                        final Boolean readOnly = contentStream.getReadOnly();
                        mapProductionIdentifierToCellarIdentifier(contentids, cellarId, readOnly);
                    } catch (final PidManagerException e) {
                        if (!ignoreDuplicatedPids) {
                            throw e;
                        }
                    }
                }
            }
        }
    }

    /**
     * DigitalObjectStrategy to save assign and save cellar identifiers.
     *
     * @author ARHS Developments
     * @version $$Revision$$$
     */
    private final class AssignAndSaveCellarIdentifiersStrategy implements DigitalObjectStrategy {

        String workPrefixId;
        String dossierPrefixId;
        String expressionPrefixId;
        Integer expressionId;
        Integer manifestationId;
        SIPResource resource;
        boolean success = true;

        protected AssignAndSaveCellarIdentifiersStrategy(final SIPResource resource) {
            this.resource = resource;
        }

        @Override
        public void applyStrategy(final DigitalObject digitalObject) {
            LOG.debug(IConfiguration.INGESTION, "Generate cellar identifier for {} (type={})", digitalObject, digitalObject.getType());
            final DigitalObjectType digitalObjectType = digitalObject.getType();
            final ContentIdentifier cellarId = digitalObject.getCellarId();
            String generatedCellarUUID = cellarId != null ? cellarId.getIdentifier() : null;
            final TYPE sipType = this.resource.getSipType();
            String generateCellarID;
            switch (digitalObjectType) {
                case WORK:
                    this.expressionId = 1;
                    generateCellarID = identifiersHistoryService.generateCellarID(digitalObject, null, null, sipType);
                    generatedCellarUUID = generatedCellarUUID != null ? generatedCellarUUID : generateCellarID;
                    this.workPrefixId = generatedCellarUUID;
                    break;
                case EXPRESSION:
                    this.manifestationId = 1;
                    generateCellarID = identifiersHistoryService.generateCellarID(digitalObject, this.workPrefixId, this.expressionId++,
                            sipType);
                    generatedCellarUUID = generatedCellarUUID != null ? generatedCellarUUID : generateCellarID;
                    this.expressionPrefixId = generatedCellarUUID;
                    break;
                case MANIFESTATION:
                    generateCellarID = identifiersHistoryService.generateCellarID(digitalObject, this.expressionPrefixId,
                            this.manifestationId++, sipType);
                    generatedCellarUUID = generatedCellarUUID != null ? generatedCellarUUID : generateCellarID;
                    break;
                case DOSSIER:
                    this.expressionId = 1;
                    generateCellarID = identifiersHistoryService.generateCellarID(digitalObject, null, null, sipType);
                    generatedCellarUUID = generatedCellarUUID != null ? generatedCellarUUID : generateCellarID;
                    this.dossierPrefixId = generatedCellarUUID;
                    break;
                case EVENT:
                    generateCellarID = identifiersHistoryService.generateCellarID(digitalObject, this.dossierPrefixId, this.expressionId++,
                            sipType);
                    generatedCellarUUID = generatedCellarUUID != null ? generatedCellarUUID : generateCellarID;
                    break;
                case AGENT:
                    generateCellarID = identifiersHistoryService.generateCellarID(digitalObject, null, null, sipType);
                    generatedCellarUUID = generatedCellarUUID != null ? generatedCellarUUID : generateCellarID;
                    break;
                case TOPLEVELEVENT:
                    generateCellarID = identifiersHistoryService.generateCellarID(digitalObject, null, null, sipType);
                    generatedCellarUUID = generatedCellarUUID != null ? generatedCellarUUID : generateCellarID;
                    break;
                default:
                    throw ExceptionBuilder.get(PidManagerException.class).withCode(CoreErrors.E_036)
                            .withMessage("Type [" + digitalObjectType + "]").build();
            }

            LOG.debug(IConfiguration.INGESTION, "Generated ID for {}: {}", digitalObject, generatedCellarUUID);

            final StructMap structMap = digitalObject.getStructMap();
            final MetsDocument metsDocument = structMap.getMetsDocument();
            final OperationType type = metsDocument.getType();
            final boolean ignoreDuplicatedPids = OperationType.CreateOrIgnore.equals(type);
            try {

                mapProductionIdentifierToCellarIdentifier(digitalObject, generatedCellarUUID);
            } catch (final PidManagerException e) {
                if (!ignoreDuplicatedPids) {
                    throw e;
                } else {
                    LOG.warn("Duplicate pid, operation type = create_or_ignore => error ignored.");
                    LOG.debug(IConfiguration.INGESTION, "Duplicate pid, root cause: ", () -> Throwables.getStackTraceAsString(e));
                    success = false;
                }
            }

            final ContentIdentifier contentIdentifier = new ContentIdentifier(generatedCellarUUID);
            digitalObject.setCellarId(contentIdentifier);
        }

        public boolean isSuccess() {
            return this.success;
        }
    }

    @Transactional(readOnly = true)
    @Override
    public CellarIdentifier getCellarIdentifierByUuid(final String uuid) {
        return this.cellarIdentifierDao.getCellarIdentifier(uuid);
    }

}
