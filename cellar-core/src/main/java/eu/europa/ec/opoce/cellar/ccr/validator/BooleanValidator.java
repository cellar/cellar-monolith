package eu.europa.ec.opoce.cellar.ccr.validator;

import org.springframework.validation.Errors;

/**
 * Simple <i>validator</i> to verify that a boolean value matches one of authorized values. 
 *  
 * @author lderavet
 */
public class BooleanValidator extends AbstractPropertyValidator {

    /**
     * Verifies that a value matches at least one of the authorized values.
     * 
     * @param target the value to verify against the list of authorized values.
     * @param errors the error object that is updated if the value is not authorized.
     */
    public void validate(Object target, Errors errors) {
        if (!getAuthorizedValues().contains(target)) {
            errors.reject("invalid.versionable.value", "[ERROR] The versionable value [" + target + "] is not authorized");
        }
    }
}
