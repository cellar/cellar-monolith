package eu.europa.ec.opoce.cellar.cl.dao;

import java.util.List;

import eu.europa.ec.opoce.cellar.cl.domain.admin.ValidatorRule;

/**
 * Dao for the the Entity {@link ValidatorRule}.
 * 
 * @author dcraeye
 * 
 */
public interface ValidatorRuleDao {

    /**
     * Return all persistent instances of {@link ValidatorRule}.
     * 
     * @return a {@link List} containing 0 or more persistent instances.
     */
    List<ValidatorRule> getRules();

    /**
     * Persist the given transient instance.
     * 
     * @param rule
     *            the transient instance to persist
     */
    void createRule(ValidatorRule rule);

    /**
     * Return the persistent instance with the given identifier, or null if not found.
     * 
     * @param id
     *            the identifier of the persistent instance
     * 
     * @return the persistent instance, or null if not found
     */
    ValidatorRule getRule(Long id);

    /**
     * Return the persistent instance with the given classname, or null if not found.
     * 
     * @param id
     *            the identifier of the persistent instance
     * 
     * @return the persistent instance, or null if not found
     */
    ValidatorRule getRuleByValidatorClass(final String validatorClass);

    /**
     * Return all system validators.
     * 
     * @return the system validators.
     */
    List<ValidatorRule> getSystemValidators();

    /**
     * Return all custom validators.
     * 
     * @return the custom validators.
     */
    List<ValidatorRule> getCustomValidators();

    /**
     * 
     * Update the given persistent instance.
     * 
     * @param rule
     *            the persistent instance to update
     */
    void updateRule(ValidatorRule rule);

    /**
     * Delete the given persistent instance.
     * 
     * @param rule
     *            the persistent instance to delete
     */
    void deleteRule(ValidatorRule rule);

}
