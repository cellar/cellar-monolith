/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.dao.impl
 *             FILE : ExternalLinkDaoImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 14, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao.impl;

import eu.europa.ec.opoce.cellar.cl.dao.ExternalLinkPatternDao;
import eu.europa.ec.opoce.cellar.cl.domain.externalLink.ExternalLinkPattern;

import java.util.List;

import org.springframework.stereotype.Repository;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 14, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Repository
public class ExternalLinkPatternDaoImpl extends BaseDaoImpl<ExternalLinkPattern, Long> implements ExternalLinkPatternDao {

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ExternalLinkPattern> getExternalLinkPatterns() {
        return super.findAll();
    }
}
