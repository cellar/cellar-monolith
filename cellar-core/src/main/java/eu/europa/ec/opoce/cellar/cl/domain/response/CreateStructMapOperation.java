package eu.europa.ec.opoce.cellar.cl.domain.response;

public class CreateStructMapOperation extends StructMapOperation {

    /** class's uid. */
    private static final long serialVersionUID = 8326607516293707541L;

    public CreateStructMapOperation() {
        super();
        this.setOperationType(ResponseOperationType.CREATE);
    }

    public CreateStructMapOperation(String id) {
        super(ResponseOperationType.CREATE, id);
    }

}
