/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service
 *             FILE : AutomaticReindexationService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 20 sept. 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2016 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service;

import eu.europa.ec.opoce.cellar.server.admin.sparql.Query;

import java.io.IOException;

/**
 * The Interface AutomaticReindexationService.
 * <class_description> This class is the service class that exposes the actions over the scheduler allowing to stop the running thread and enable or disable the next runs.
 * <br/><br/>
 * <notes>
 * <br/><br/>
 * ON : 20 sept. 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface AutomaticReindexationService {

    /**
     * Disable the current and future scheduled tasks.
     */
    void disable();

    /**
     * Enable future scheduled tasks.
     */
    void enable();

    /**
     * Stop current running thread.
     */
    void stop();

    /**
     * Pause.
     */
    void pause();

    /**
     * Resume.
     */
    void resume();

    /**
     * Checks the running thread is running.
     *
     * @return true, if is running
     */
    boolean isRunning();

    /**
     * Checks if is paused.
     *
     * @return true, if is paused
     */
    boolean isPaused();

    /**
     * Gets the example queries.
     *
     * @return the example queries
     */
    Query[] getExampleQueries();

    /**
     * Gets the query.
     *
     * @param query the query
     * @return the query
     * @throws IOException Signals that an I/O exception has occurred.
     */
    String getQuery(final String query) throws IOException;
}
