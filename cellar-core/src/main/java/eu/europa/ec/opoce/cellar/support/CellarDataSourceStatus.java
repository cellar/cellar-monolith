/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.support
 *        FILE : CellarDataSourceStatus.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 14-05-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.support;

import org.springframework.stereotype.Component;

/**
 * <class_description> This is an utility component used for checking whether the data source for Cellar is online or not.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 14-05-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
@Component
public class CellarDataSourceStatus extends AbstractDataSourceStatus {

    /**
     * Gets the data source name.
     *
     * @return the data source name
     * @see eu.europa.ec.opoce.cellar.support.AbstractDataSourceStatus#getDataSourceName()
     */
    @Override
    protected String getDataSourceName() {
        return this.cellarConfiguration.getCellarDatasourceCellar();
    }

    /** {@inheritDoc} */
    @Override
    protected String getVerificationQuery() {
        return "select * from dual";
    }

}
