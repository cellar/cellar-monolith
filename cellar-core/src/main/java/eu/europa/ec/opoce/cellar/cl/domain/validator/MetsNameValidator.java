package eu.europa.ec.opoce.cellar.cl.domain.validator;

import java.io.File;

import org.springframework.stereotype.Component;

import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.cl.exception.ValidationException;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.support.FileExtensionUtils;

/**
 * Implementation of the {@link ContentValidator}.
 * Validate the mets file name. Throws a CellarException if the file name is not in the format
 * {@code"<metsDocumentID>.mets.xml". Where <metsDocumentID> is the ID of the mets document}
 * (ex: oj:JOL_2006_088_R_0063_01.mets.xml)
 * 
 * @param metsFile
 *            the mets fiel name.
 * @param metsId
 *            the mets document ID.
 * 
 * @throws MyException
 *             if the mets file name id not valid.
 * 
 * @author dcraeye
 *
 */
@Component("metsNameValidator")
public class MetsNameValidator implements SystemValidator {

    /**
     * {@inheritDoc}
     */
    @Override
    public ValidationResult validate(ValidationDetails validationDetails) {
        ValidationResult result = new ValidationResult();

        if (validationDetails.getToValidate() == null) {
            throw ExceptionBuilder.get(ValidationException.class).withCode(CoreErrors.E_070).withMessage("object cannot be null")
                    .withCause(new NullPointerException()).build();
        }
        if (!(validationDetails.getToValidate() instanceof File)) {
            throw ExceptionBuilder.get(ValidationException.class).withCode(CoreErrors.E_072)
                    .withMessage(validationDetails.getToValidate().getClass().toString()).build();
        }
        File metsFile = (File) validationDetails.getToValidate();

        result.setValid(validateMetsFileName(metsFile, validationDetails.getSipResource().getMetsID()));

        return result;
    }

    private boolean validateMetsFileName(File metsFile, String metsId) throws CellarException {
        return metsFile.getName().equals(metsId + FileExtensionUtils.METS);
    }
}
