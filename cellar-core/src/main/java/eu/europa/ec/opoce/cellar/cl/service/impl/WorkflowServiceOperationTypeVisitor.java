/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *             FILE : WorkflowServiceOperationTypeVisitor.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jul 3, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.cl.service.client.WorkflowService;
import eu.europa.ec.opoce.cellar.domain.content.mets.impl.NonImplementedOperationTypeVisitor;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jul 3, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class WorkflowServiceOperationTypeVisitor extends NonImplementedOperationTypeVisitor {

    private final SIPResource sipResource;
    private final WorkflowService workflowService;

    public WorkflowServiceOperationTypeVisitor(final SIPResource sipResource, final WorkflowService workflowService) {
        this.sipResource = sipResource;
        this.workflowService = workflowService;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.domain.content.mets.impl.NonImplementedOperationTypeVisitor#visitRead(java.lang.Void)
     */
    @Override
    public Void visitRead(final Void in) {
        this.workflowService.readMets(this.sipResource);
        return null;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.domain.content.mets.impl.NonImplementedOperationTypeVisitor#visitCreate(java.lang.Void)
     */
    @Override
    public Void visitCreate(final Void in) {
        this.workflowService.createMets(this.sipResource);
        return null;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.domain.content.mets.impl.NonImplementedOperationTypeVisitor#visitCreateOrIgnore(java.lang.Void)
     */
    @Override
    public Void visitCreateOrIgnore(final Void in) {
        return this.visitCreate(in);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.domain.content.mets.impl.NonImplementedOperationTypeVisitor#visitUpdate(java.lang.Void)
     */
    @Override
    public Void visitUpdate(final Void in) {
        this.workflowService.updateMets(this.sipResource);
        return null;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.domain.content.mets.impl.NonImplementedOperationTypeVisitor#visitDelete(java.lang.Void)
     */
    @Override
    public Void visitDelete(final Void in) {
        this.workflowService.deleteMets(this.sipResource);
        return null;
    }

}
