package eu.europa.ec.opoce.cellar.cl.delegator.aligner;

import eu.europa.ec.opoce.cellar.cl.delegator.AbstractDelegator;

/**
 * The Interface DelegateAligner.
 */
public interface DelegateAligner {

    /**
     * Start processing.
     *
     * @param delegator the delegator
     */
    void startProcessing(final AbstractDelegator delegator);

    /**
     * Align.
     *
     * @param delegator the delegator
     */
    void align(final AbstractDelegator delegator);
}
