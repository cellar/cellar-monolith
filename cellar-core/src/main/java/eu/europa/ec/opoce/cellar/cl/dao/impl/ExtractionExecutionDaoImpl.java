/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.dao.impl
 *             FILE : ExtractionExecutionDaoImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 5, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao.impl;

import eu.europa.ec.opoce.cellar.cl.dao.ExtractionExecutionDao;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionConfiguration.CONFIGURATION_TYPE;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution.EXECUTION_STATUS;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page. <br/>
 * <br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc. <br/>
 * <br/>
 * ON : Oct 5, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Repository
public class ExtractionExecutionDaoImpl extends BaseDaoImpl<ExtractionExecution, Long>
        implements ExtractionExecutionDao {

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Object> findExtractionExecutionsNumberOfDocuments(final Long extractionConfigurationId) {
        return getHibernateTemplate().execute(session -> {
            final Query queryObject = session.createQuery(
                    "select e, (select count(w.id) from ExtractionWorkIdentifier w where w.extractionExecution.id = e.id) from ExtractionExecution e where e.extractionConfiguration.id = :configurationId order by e.startDate desc");
            queryObject.setParameter("configurationId", extractionConfigurationId);

            return queryObject.list();
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ExtractionExecution> deleteExtractionExecutions(final Long configurationId) {
        return getHibernateTemplate().execute(session -> {
            final Query queryObject = session.getNamedQuery("findExtractionConfigurationId");
            queryObject.setParameter(0, configurationId);

            final List<ExtractionExecution> extractionsExecutions = queryObject.list();
            for (final ExtractionExecution extractionExecution : extractionsExecutions) {
                session.delete(extractionExecution);
            }

            return extractionsExecutions;
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ExtractionExecution> deleteExtractionExecutions(final CONFIGURATION_TYPE configurationType,
                                                                final int numberOfDays, final int batchSize) {
        return getHibernateTemplate().execute(session -> {
            final Query queryObject = session.getNamedQuery("findTypeOldExtractionExecutions");
            queryObject.setMaxResults(batchSize);
            queryObject.setParameter("type", configurationType);

            final Calendar calendar = Calendar.getInstance();
            calendar.clear();
            calendar.setTime(new Date());
            calendar.add(Calendar.DATE, -numberOfDays);
            queryObject.setParameter("lastDate", calendar.getTime());

            final List<EXECUTION_STATUS> listStatus = Arrays.asList(EXECUTION_STATUS.ARCHIVING_ERROR,
                    EXECUTION_STATUS.CLEANING_ERROR, EXECUTION_STATUS.PARSING_ERROR, EXECUTION_STATUS.SPARQL_ERROR,
                    EXECUTION_STATUS.DONE);
            queryObject.setParameterList("status", listStatus);

            final List<ExtractionExecution> extractionsExecutions = queryObject.list();
            for (final ExtractionExecution extractionExecution : extractionsExecutions) {
                session.delete(extractionExecution);
            }

            return extractionsExecutions;
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ExtractionExecution> updatePendingExtractionExecutions(final int batchSize, final String instanceId) {
        return getHibernateTemplate().execute(session -> {
            final Query queryObject = session.getNamedQuery("findStatusExtractionExecutions");
            queryObject.setMaxResults(batchSize);

            final List<EXECUTION_STATUS> listStatus = Arrays.asList(EXECUTION_STATUS.PENDING,
                    EXECUTION_STATUS.ARCHIVING_PENDING, EXECUTION_STATUS.PARSING_PENDING,
                    EXECUTION_STATUS.SPARQL_PENDING, EXECUTION_STATUS.CLEANING_PENDING);
            queryObject.setParameterList("status", listStatus);

            final List<ExtractionExecution> extractionsExecutions = queryObject.list();
            for (final ExtractionExecution extractionExecution : extractionsExecutions) {
                session.refresh(extractionExecution, LockOptions.UPGRADE);
                // starts the execution
                switch (extractionExecution.getExecutionStatus()) {
                    case PENDING:
                        extractionExecution.setExecutionStatus(EXECUTION_STATUS.SPARQL_EXECUTING);
                        break;
                    case ARCHIVING_PENDING:
                        extractionExecution.setExecutionStatus(EXECUTION_STATUS.ARCHIVING_CLEANING);
                        break;
                    case PARSING_PENDING:
                        extractionExecution.setExecutionStatus(EXECUTION_STATUS.PARSING_CLEANING);
                        break;
                    case SPARQL_PENDING:
                        extractionExecution.setExecutionStatus(EXECUTION_STATUS.SPARQL_CLEANING);
                        break;
                    case CLEANING_PENDING:
                        extractionExecution.setExecutionStatus(EXECUTION_STATUS.CLEANING);
                        break;
                    default:
                        continue;
                }
                extractionExecution.setEffectiveExecutionDate(new Date());
                extractionExecution.setInstanceId(instanceId);

                session.update(extractionExecution);
            }

            return extractionsExecutions;
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int updateNewExtractionExecutions(final int batchSize) {
        return getHibernateTemplate().execute(session -> {
            final Query queryObject = session.getNamedQuery("findStatusEndedExtractionExecutions");
            queryObject.setMaxResults(batchSize);
            queryObject.setParameter("status", EXECUTION_STATUS.NEW);
            queryObject.setParameter("now", new Date());

            final List<ExtractionExecution> extractionsExecutions = queryObject.list();
            for (final ExtractionExecution extractionExecution : extractionsExecutions) {
                session.refresh(extractionExecution, LockOptions.UPGRADE);
                if (extractionExecution.getExecutionStatus() == EXECUTION_STATUS.NEW) {
                    extractionExecution.setExecutionStatus(EXECUTION_STATUS.PENDING); // schedules
                    // the
                    // execution
                    extractionExecution.setInstanceId(null);
                    session.update(extractionExecution);
                }
            }

            return extractionsExecutions.size();
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addExtractionExecution(final ExtractionExecution extractionExecution) {
        super.saveObject(extractionExecution);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExtractionExecution updatePendingExtractionExecution(final String instanceId) {
        final List<ExtractionExecution> executions = this.updatePendingExtractionExecutions(1, instanceId);

        if (executions == null || executions.isEmpty()) {
            return null;
        }

        return executions.get(0); // return the first
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ExtractionExecution> findExtractionExecutions(final Long extractionConfigurationId) {
        return super.findObjectsByNamedQuery("findExtractionConfigurationId", extractionConfigurationId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExtractionExecution findExtractionExecution(final Long id) {
        return super.getObject(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void restartStepExtractionExecution(final Long id) {
        final ExtractionExecution extractionExecution = super.getObject(id);

        if (extractionExecution == null) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("The execution " + id + " doesn't exist.")
                    .build();
        }

        // restarts the step
        switch (extractionExecution.getExecutionStatus()) {
            case ARCHIVING_ERROR:
                extractionExecution.setExecutionStatus(EXECUTION_STATUS.ARCHIVING_PENDING);
                break;
            case PARSING_ERROR:
                extractionExecution.setExecutionStatus(EXECUTION_STATUS.PARSING_PENDING);
                break;
            case SPARQL_ERROR:
                extractionExecution.setExecutionStatus(EXECUTION_STATUS.SPARQL_PENDING);
                break;
            default:
                throw ExceptionBuilder.get(CellarException.class)
                        .withMessage("Cannot restart the last step of an execution in status "
                                + extractionExecution.getExecutionStatus() + ".")
                        .build();
        }
        super.updateObject(extractionExecution);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int restartStepExtractionExecution() {
        return getHibernateTemplate().execute(session -> {
            final Query queryObject = session.getNamedQuery("findStatusExtractionExecutions");

            final List<EXECUTION_STATUS> listStatus = Arrays.asList(EXECUTION_STATUS.ARCHIVING_ERROR,
                    EXECUTION_STATUS.PARSING_ERROR, EXECUTION_STATUS.SPARQL_ERROR);
            queryObject.setParameterList("status", listStatus);

            final List<ExtractionExecution> extractionsExecutions = queryObject.list();
            for (final ExtractionExecution extractionExecution : extractionsExecutions) {
                // restarts the step
                switch (extractionExecution.getExecutionStatus()) {
                    case ARCHIVING_ERROR:
                        extractionExecution.setExecutionStatus(EXECUTION_STATUS.ARCHIVING_PENDING);
                        break;
                    case PARSING_ERROR:
                        extractionExecution.setExecutionStatus(EXECUTION_STATUS.PARSING_PENDING);
                        break;
                    case SPARQL_ERROR:
                        extractionExecution.setExecutionStatus(EXECUTION_STATUS.SPARQL_PENDING);
                        break;
                    default:
                        throw ExceptionBuilder.get(CellarException.class)
                                .withMessage("Cannot restart the last step of an execution in status "
                                        + extractionExecution.getExecutionStatus() + ".")
                                .build();
                }

                session.update(extractionExecution);
            }

            return extractionsExecutions.size();
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void restartExtractionExecution(final Long id) {
        final ExtractionExecution extractionExecution = super.getObject(id);

        if (extractionExecution == null) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("The execution " + id + " doesn't exist.")
                    .build();
        }

        if (!extractionExecution.isRestartable()) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage(
                            "Cannot restart an execution in status " + extractionExecution.getExecutionStatus() + ".")
                    .build();
        }

        // restarts the execution
        extractionExecution.setExecutionStatus(EXECUTION_STATUS.CLEANING_PENDING);

        super.updateObject(extractionExecution);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int restartExtractionExecution() {
        return getHibernateTemplate().execute(session -> {
            final Query queryObject = session.getNamedQuery("findStatusExtractionExecutions");

            final List<EXECUTION_STATUS> listStatus = Arrays.asList(EXECUTION_STATUS.ARCHIVING_ERROR,
                    EXECUTION_STATUS.PARSING_ERROR, EXECUTION_STATUS.SPARQL_ERROR);
            queryObject.setParameterList("status", listStatus);

            final List<ExtractionExecution> extractionsExecutions = queryObject.list();
            for (final ExtractionExecution extractionExecution : extractionsExecutions) {
                if (!extractionExecution.isRestartable()) {
                    throw ExceptionBuilder.get(CellarException.class)
                            .withMessage("Cannot restart an execution in status "
                                    + extractionExecution.getExecutionStatus() + ".")
                            .build();
                }

                // restarts the execution
                extractionExecution.setExecutionStatus(EXECUTION_STATUS.CLEANING_PENDING);

                session.update(extractionExecution);
            }

            return extractionsExecutions.size();
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ExtractionExecution> findAdhocExtractionExecutions() {
        return super.findObjectsByNamedQuery("findTypeExtractionExecutions", CONFIGURATION_TYPE.STD);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int cleanExtractionExecutions(final int batchSize, final String instanceId) {
        return getHibernateTemplate().execute(session -> {
            final Query queryObject = session.getNamedQuery("findStatusInstanceExtractionExecutions");
            queryObject.setMaxResults(batchSize);
            queryObject.setParameter("instanceId", instanceId);

            final List<EXECUTION_STATUS> listStatus = Arrays.asList(EXECUTION_STATUS.ARCHIVING,
                    EXECUTION_STATUS.ARCHIVING_CLEANING, EXECUTION_STATUS.ARCHIVING_FINALIZE,
                    EXECUTION_STATUS.ARCHIVING_INIT, EXECUTION_STATUS.CLEANING, EXECUTION_STATUS.PARSING,
                    EXECUTION_STATUS.PARSING_CLEANING, EXECUTION_STATUS.SPARQL_CLEANING,
                    EXECUTION_STATUS.SPARQL_EXECUTING);

            queryObject.setParameterList("status", listStatus);

            final List<ExtractionExecution> extractionsExecutions = queryObject.list();
            for (final ExtractionExecution extractionExecution : extractionsExecutions) {
                // cleans the not achieved executions for this instance
                switch (extractionExecution.getExecutionStatus()) {
                    case ARCHIVING:
                    case ARCHIVING_CLEANING:
                    case ARCHIVING_FINALIZE:
                    case ARCHIVING_INIT:
                        extractionExecution.setExecutionStatus(EXECUTION_STATUS.ARCHIVING_ERROR);
                        break;
                    case CLEANING:
                        extractionExecution.setExecutionStatus(EXECUTION_STATUS.CLEANING_ERROR);
                        break;
                    case PARSING:
                    case PARSING_CLEANING:
                        extractionExecution.setExecutionStatus(EXECUTION_STATUS.PARSING_ERROR);
                        break;
                    case SPARQL_CLEANING:
                    case SPARQL_EXECUTING:
                        extractionExecution.setExecutionStatus(EXECUTION_STATUS.SPARQL_ERROR);
                        break;
                    // should never get here!
                    default:
                        break;
                }
                extractionExecution.setErrorDescription("Execution not properly achieved.");
                extractionExecution.setErrorStacktrace("Execution not properly achieved.");

                session.update(extractionExecution);
            }

            return extractionsExecutions.size();
        });
    }
}
