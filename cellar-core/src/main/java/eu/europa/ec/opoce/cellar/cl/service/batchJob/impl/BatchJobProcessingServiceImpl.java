/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *             FILE : BatchJobProcessingServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 13, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.batchJob.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.dao.BatchJobDao;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob.STATUS;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.BatchJobProcessingService;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.BatchJobService;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor.IBatchJobProcessor;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.manager.BatchJobPreProcessingRunable;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.manager.BatchJobProcessingRunable;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.manager.BatchJobRunable;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.manager.BatchJobThreadPoolManager;
import eu.europa.ec.opoce.cellar.cl.service.client.SparqlExecutingService;
import eu.europa.ec.opoce.cellar.cl.service.client.UriFoundDelegator;
import eu.europa.ec.opoce.cellar.common.factory.IFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.HashSet;
import java.util.List;

/**
 * <class_description> Batch job work identifier service.
 *
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 13, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class BatchJobProcessingServiceImpl implements BatchJobProcessingService {

    private static final Logger LOG = LogManager.getLogger(BatchJobProcessingServiceImpl.class);

    /**
     * Cellar configuration.
     */
    @Autowired
    @Qualifier("cellarConfiguration")
    protected ICellarConfiguration cellarConfiguration;

    /**
     * The sparql executing service.
     */
    @Autowired
    private SparqlExecutingService sparqlExecutingService;

    /**
     * Batch job dao.
     */
    @Autowired
    private BatchJobDao batchJobDao;

    /**
     * Batch job service.
     */
    @Autowired
    private BatchJobService batchJobService;

    /** The batch job processor factory. */
    @Autowired
    @Qualifier("batchJobProcessorFactory")
    private IFactory<IBatchJobProcessor, BatchJob> batchJobProcessorFactory;

    /** The batch job processor factory. */
    @Autowired
    @Qualifier("batchJobPreProcessorFactory")
    private IFactory<IBatchJobProcessor, BatchJob> batchJobPreProcessorFactory;

    /** The batch job thread pool manager. */
    @Autowired
    private BatchJobThreadPoolManager batchJobThreadPoolManager;

    /**
     * {@inheritDoc}
     */
    @Override
    public void cleanBatchJobsRunning() {
        final List<BatchJob> findBatchJobsWithAnyStatus = batchJobDao.findBatchJobsWithAnyStatus(STATUS.EXECUTING_SPARQL,
                STATUS.PROCESSING);
        final String errorDescription = "Execution not properly achieved.";
        for (final BatchJob batchJob : findBatchJobsWithAnyStatus) {
            batchJobService.updateErrorBatchJob(batchJob, errorDescription, errorDescription);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void executeSparql(final BatchJob batchJob) {
        File tempFile = null;
        try {
            tempFile = this.sparqlExecutingService.executeSparqlQueryFormat(batchJob.getSparqlQuery(), null);
            final HashSet<String> workIds = new HashSet<String>();

            this.sparqlExecutingService.parseSparqlReponse(tempFile.getAbsolutePath(), new UriFoundDelegator() {

                @Override
                public void onUri(final String uri) {
                    workIds.add(uri);
                }

                @Override
                public void onEnd() {
                    LOG.info("Parsing returns {} elements", workIds.size());
                }
            });

            this.batchJobService.updateReadyBatchJob(batchJob, workIds);

        } catch (final Exception exception) {
            LOG.error("Batch job execution error", exception);

            this.batchJobService.updateErrorBatchJob(batchJob, exception); // updates the error description with the exception description
        } finally {
            if (tempFile != null) {
                try {
                    tempFile.delete();
                } catch (final Exception e) {
                    LOG.error(e.getMessage(), e);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void processBatchJobs(final List<BatchJob> batchJobs) {
        for (final BatchJob batchJob : batchJobs) {
            final BatchJobRunable batchJobRunable = new BatchJobProcessingRunable(batchJob, batchJobService, batchJobProcessorFactory);
            if (!batchJobThreadPoolManager.contains(batchJobRunable)) {
                batchJobThreadPoolManager.submit(batchJobRunable);
            }
        }
    }

    /** {@inheritDoc} */
    @Override
    public void preProcessBatchJobs(final List<BatchJob> batchJobs) {
        for (final BatchJob batchJob : batchJobs) {
            final BatchJobRunable batchJobRunable = new BatchJobPreProcessingRunable(batchJob, batchJobService,
                    batchJobPreProcessorFactory);
            if (!batchJobThreadPoolManager.contains(batchJobRunable)) {
                batchJobThreadPoolManager.submit(batchJobRunable);
            }
        }
    }

}
