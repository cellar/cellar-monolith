/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.export
 *             FILE : IExportService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 25-Oct-2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2016 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.export;

import eu.europa.ec.opoce.cellar.cl.domain.export.ExportedPackage;

import java.io.File;
import java.io.IOException;

import javax.xml.bind.JAXBException;

/**
 * The Interface IExportService.
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 25-Oct-2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IExportService {

    /**
     * Export.
     *
     * @param workId the work id
     * @param metadataOnly the metadata only
     * @return the exported package
     * @throws JAXBException the JAXB exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    ExportedPackage export(String workId, boolean metadataOnly, boolean useAgnosticURL)
            throws JAXBException, IOException;

    /**
     * Export.
     *
     * @param workId the work id
     * @param metadataOnly the metadata only
     * @param destinationFolder the destination folder
     * @param useAgnosticURL the use agnostic url
     * @return the exported package
     * @throws JAXBException the JAXB exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    ExportedPackage export(String workId, boolean metadataOnly, File destinationFolder, boolean useAgnosticURL)
            throws JAXBException, IOException;
}
