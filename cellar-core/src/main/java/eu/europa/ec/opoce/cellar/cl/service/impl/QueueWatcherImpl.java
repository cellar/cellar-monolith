/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *        FILE : QueueWatcherImpl.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 13-03-2012
 *
 * MODIFIED BY : EUROPEAN DYNAMICS S.A.
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2021 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.impl.CellarPersistentConfiguration;
import eu.europa.ec.opoce.cellar.cl.dao.PackageQueueDao;
import eu.europa.ec.opoce.cellar.cl.domain.enums.ProcessStatus;
import eu.europa.ec.opoce.cellar.cl.domain.package_history.PackageHistory;
import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.PackageQueue;
import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.SIPPackageProcessingStatus;
import eu.europa.ec.opoce.cellar.cl.runnable.IngestionRequestRunnable;
import eu.europa.ec.opoce.cellar.cl.service.IngestionService;
import eu.europa.ec.opoce.cellar.cl.service.client.PackageHistoryService;
import eu.europa.ec.opoce.cellar.cl.service.client.SIPDependencyCheckerStorageService;
import eu.europa.ec.opoce.cellar.cmr.service.EmbargoService;
import eu.europa.ec.opoce.cellar.common.concurrent.FutureCallback;
import eu.europa.ec.opoce.cellar.common.concurrent.ListenableFuture;
import eu.europa.ec.opoce.cellar.common.concurrent.ListeningExecutorService;
import eu.europa.ec.opoce.cellar.ingestion.SIPState;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;
import eu.europa.ec.opoce.cellar.ingestion.service.QueueWatcher;
import eu.europa.ec.opoce.cellar.logging.LogContext;
import eu.europa.ec.opoce.cellar.logging.ThreadContextConcurrencyStrategy;

import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.google.common.base.Stopwatch;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.google.common.base.Throwables.getStackTraceAsString;
import static eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration.INGESTION;
import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.CMR_INGESTION;

/**
 * <class_description> Worker thread that periodically polls the database
 * in order to retrieve the references of the highest priority SIP packages
 * so as to locate them in the ingestion folders an submit them to the ingestion
 * executor. <br/>
 * <br/>
 * <notes> <br/>
 * <br/>
 * ON : 13-03-2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service("queueWatcher")
public class QueueWatcherImpl implements QueueWatcher {

    private static final Logger LOG = LogManager.getLogger(QueueWatcherImpl.class);

    private static final long DELETE_SIP_DELAY_SECONDS = 3;

    private static final long ZERO_LENGTH = 0L;
    
    /**
     * The Cellar configuration storing runtime parameters.
     */
    private final CellarPersistentConfiguration cellarConfiguration;

    /**
     * Concurrent set based on a ConcurrentHashMap. This queue will contain the
     * SIPWork objects that were found and will be treated by the priority
     * thread pool executor once the object has been treated it's removed from
     * the vector this object is accessed by all threads the sipiterator uses it
     * to know which objects were already fetched the ingestion runnables use it
     * to remove items from queue when done
     */


    private static EmbargoService embargoService;
    private ConcurrentMap<SIPWork, Boolean> ingestionTreatments;

    private final ListeningExecutorService priorityThreadPoolExecutor;
    
    private final ScheduledExecutorService cleanupThread;

    private final IngestionService ingestionService;
    
    private final PackageQueueDao packageQueueDao;
    
    private final FileSystemService fileSystemService;

    private final SIPDependencyCheckerStorageService sipDependencyCheckerStorageService;
    
    private final PackageHistoryService packageHistoryService;
    
    @Autowired
    public QueueWatcherImpl(IngestionService ingestionService, @Qualifier("cellarConfiguration") CellarPersistentConfiguration cellarConfiguration,
                          @Qualifier("ingestionRequestExecutor") ListeningExecutorService ingestionExecutor, PackageQueueDao packageQueueDao,
                          FileSystemService fileSystemService, SIPDependencyCheckerStorageService sipDependencyCheckerStorageService,
                          PackageHistoryService packageHistoryService, EmbargoService embargoS) {
        this.ingestionService = ingestionService;
        this.cellarConfiguration = cellarConfiguration;
        this.priorityThreadPoolExecutor = ingestionExecutor;
        this.cleanupThread = Executors.newSingleThreadScheduledExecutor();
        this.priorityThreadPoolExecutor.setConcurrencyStrategy(new ThreadContextConcurrencyStrategy());
        this.packageQueueDao = packageQueueDao;
        this.fileSystemService = fileSystemService;
        this.sipDependencyCheckerStorageService = sipDependencyCheckerStorageService;
        this.packageHistoryService = packageHistoryService;
        embargoService=embargoS;
    }

    @PostConstruct
    public void initialize() {
        this.ingestionTreatments = new ConcurrentHashMap<>();
        resetIngestingPackagesToScheduled();
    }

	/**
	 * In case an abrupt application termination occurred while some packages were
	 * still being ingested, resets their status to 'SCHEDULED' so that their ingestion
	 * process can start again, without being blocked.
	 */
	private void resetIngestingPackagesToScheduled() {
		try {
			this.sipDependencyCheckerStorageService.updatePackagesStatus(Arrays.asList(SIPPackageProcessingStatus.INGESTING), SIPPackageProcessingStatus.SCHEDULED);
		}
		catch (Exception e) {
			LOG.warn("Could not reset package status from 'INGESTING' to 'SCHEDULED'.", e);
		}
	}
    
    @Override
    public boolean isIngestionTreatmentSetEmpty() {
        return this.getIngestionTreatments().isEmpty();
    }

    /**
     * Pre destroy.
     */
    @PreDestroy
    public void preDestroy() {
        LOG.info("Shutting down internal cleanup thread");
        List<Runnable> remaining = cleanupThread.shutdownNow();
        for (Runnable runnable : remaining) {
            runnable.run();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reconfigureThreadPoolExecutor() {
        // initialize thread pool executor with properties
        this.priorityThreadPoolExecutor.setCorePoolSize(this.cellarConfiguration.getCellarServiceIngestionPoolThreads());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @LogContext(CMR_INGESTION)
    public void watchQueue() {
    	// If ingestion is enabled and as long as the ingestion thread pool is not full,
    	// polls the DB queue for the next SIP reference to ingest.
    	while (cellarConfiguration.isCellarServiceIngestionEnabled() && shouldRequestPackage()) {
    		PackageQueue queueHead = requestPackageReferenceFromQueue();
        	if (queueHead != null) {
        		String sipName = queueHead.getName();
        		TYPE sipType = queueHead.getType();
        		Date sipDetectionDate = queueHead.getDetectionDate();
                Long sipPackageHistoryId = queueHead.getPackageHistoryId();
        		// Locate the SIP file in the ingestion folders based on the info obtained from the DB queue.
        		File sipFile = this.fileSystemService.getSipFromReceptionFolder(sipName, sipType);
        		if (sipFile != null && (sipFile.length() > ZERO_LENGTH)) {
        			if (isCurrentlyIngesting(sipName)) {
        				LOG.error("Package [name={}, type={}] referenced by the DB queue is already being ingested.", sipName, sipType);
        				break;
        			}
        			// Update the status of the SIP to INGESTING
                    // and the status of PackageHistory to RUNNING
                    // before submitting it to the ingestion executor.
        			boolean databaseSynced = this.sipDependencyCheckerStorageService.updatePackageStatus(sipName, SIPPackageProcessingStatus.INGESTING);
        			// On successful sync, proceed with submission to the executor.
        			if (databaseSynced) {
                        // Load the corresponding PACKAGE_HISTORY object and update its PREPROCESS_INGESTION_STATUS to RUNNING
                        PackageHistory packageHistory = this.packageHistoryService.getEntry(sipPackageHistoryId);
                        if (packageHistory != null){
                            packageHistory = this.packageHistoryService.updatePackageHistoryStatus(packageHistory, ProcessStatus.R);
                        }
        				// Create a submission-ready SIPWork object based on the SIP file.
            			SIPWork sipWork = SIPWork.createSIPProcessObject(sipFile, sipType, sipDetectionDate, packageHistory);
            			// Insert an entry to the ingestions-tracking map.
        				this.ingestionTreatments.put(sipWork, true);
        				// Submit to the executor.
                        ListenableFuture<IngestionRequestRunnable> future = this.priorityThreadPoolExecutor.submit(new IngestionRequestRunnable(this.ingestionService, sipWork));
                        future.addCallback(new CleanupCallback(this.ingestionTreatments, sipWork, this.cleanupThread, this.sipDependencyCheckerStorageService));
        			}
        			else {
        				LOG.error("The status of package [name={}, type={}] could not be updated.", sipName, sipType);
        			}
        		}
        		else {
        			LOG.warn("Package [name={}, type={}] referenced by the DB queue does not exist in the ingestion folders.", sipName, sipType);
        		}
        	}
        	else {
        		break;
        	}
    	}
    }

    /**
     * Checks if a SIP package reference should be requested from the DB queue for ingestion.
     * A SIP will be requested as long as the number of SIP packages currently being
     * ingested is less than the capacity of the thread pool.
     * @return true if a SIP package reference should be fetched from the DB queue,
     * false otherwise.
     */
    private boolean shouldRequestPackage() {
    	return this.ingestionTreatments.keySet().size() < this.cellarConfiguration.getCellarServiceIngestionPoolThreads();
    }
    
    /**
     * Gets the next prioritized conflict-free reference of the SIP to ingest.
     * PACKAGE_QUEUE view contains at most 1 row, the head of the DB queue.
     * @return the head of the SIP reference DB queue.
     */
    private PackageQueue requestPackageReferenceFromQueue() {
    	PackageQueue queueHead = null;
    	Stopwatch sw = Stopwatch.createStarted();
    	List<PackageQueue> packageQueue = this.packageQueueDao.findAll();
    	long elapsed = sw.elapsed(TimeUnit.MILLISECONDS);
    	if (CollectionUtils.isNotEmpty(packageQueue)) {
    		queueHead = packageQueue.get(0);
    	}
        if(cellarConfiguration.isDebugVerboseLoggingEnabled()) {
        	if (queueHead != null) {
        		LOG.info("Retrieved package reference for [name={}, type={}] from DB queue in {}ms.", queueHead.getName(), queueHead.getType(), elapsed);
        	}
        }
        return queueHead;
    }
    
    /**
     * Checks if the package being submitted for ingestion is currently
     * being ingested.
     * @param sipName
     * @return
     */
    private boolean isCurrentlyIngesting(String sipName) {
    	Set<String> ingestingSipNames = this.ingestionTreatments.keySet().stream()
    			.map(SIPWork::getSipFile)
    			.map(File::getName)
    			.collect(Collectors.toSet());
    	return ingestingSipNames.contains(sipName);
    }
    
    static class CleanupCallback implements FutureCallback<IngestionRequestRunnable> {

        private final ConcurrentMap<SIPWork, Boolean> treatments;
        private final SIPWork sipWork;
        private final ScheduledExecutorService cleanupThread;
        private final SIPDependencyCheckerStorageService sipDependencyCheckerStorageService;

        CleanupCallback(ConcurrentMap<SIPWork, Boolean> treatments, SIPWork sipWork, ScheduledExecutorService cleanupThread,
        		SIPDependencyCheckerStorageService sipDependencyCheckerStorageService) {
        	
            this.treatments = treatments;
            this.sipWork = sipWork;
            this.cleanupThread = cleanupThread;
            this.sipDependencyCheckerStorageService = sipDependencyCheckerStorageService;
        }

        @Override
        public void onSuccess(IngestionRequestRunnable ingestionRequest) {
            SIPWork sipWork = ingestionRequest.getSipWork();
            if (sipWork.getState() == SIPState.SIP_FILE_REMOVE_FAILED) {
                this.cleanupThread.schedule(() -> {
                    try {
                        Files.delete(sipWork.getSipFile().toPath());
                        this.treatments.remove(sipWork);
                    } catch (IOException e) {
                        if (LOG.isDebugEnabled(INGESTION)) {
                            LOG.debug(INGESTION, "Failed to delete {}: {}", sipWork, getStackTraceAsString(e));
                        }
                        // retry
                        onSuccess(ingestionRequest);
                    }
                }, DELETE_SIP_DELAY_SECONDS, TimeUnit.SECONDS);
            } else {
                this.treatments.remove(sipWork);
            }
            if(ThreadContext.containsKey("is_embargo")&&ThreadContext.get("is_embargo").equals("true")){
                embargoService.reschedule();
                ThreadContext.remove("is_embargo");
            }
            sipWork.setState(SIPState.SUCCESS);
            updateSipStatusToFinished();
        }

        @Override
        public void onFailure(Throwable e) {
            LOG.error("Unexpected error during the ingestion", e);
            // Avoid blocking the re-ingestion of the package by cleaning
            // the map anyway
            this.treatments.remove(this.sipWork);
            updateSipStatusToFinished();
        }
        
        /**
         * Sets the status of the ingested SIP to 'FINISHED'.
         */
        private void updateSipStatusToFinished() {
        	String sipName = this.sipWork.getSipFile().getName();
        	try {
        		boolean found = this.sipDependencyCheckerStorageService.updatePackageStatus(sipName, SIPPackageProcessingStatus.FINISHED);
        		if (!found) {
        			LOG.warn("The SIP_PACKAGE entry corresponding to {} could not be found.", this.sipWork);
        		}
        	}
        	catch (Exception e) {
        		LOG.warn("An error occurred while updating the status of the SIP_PACKAGE entry corresponding to {} to 'FINISHED'.", this.sipWork, e);
        	}
        }
        
    }

    @Override
    public ConcurrentMap<SIPWork, Boolean> getIngestionTreatments() {
        return this.ingestionTreatments;
    }

}
