package eu.europa.ec.opoce.cellar.cl.service.client;

import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;

/**
 * Service to instantiate the workflow
 * 
 * @author dbacquel
 * 
 */
public interface WorkflowService {

    void createMets(final SIPResource sipResource);

    void updateMets(final SIPResource sipResource);

    void readMets(final SIPResource sipResource);

    void deleteMets(final SIPResource sipResource);

}
