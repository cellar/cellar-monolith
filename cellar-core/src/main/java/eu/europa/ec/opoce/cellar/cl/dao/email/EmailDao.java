package eu.europa.ec.opoce.cellar.cl.dao.email;

import eu.europa.ec.opoce.cellar.cl.dao.BaseDao;
import eu.europa.ec.opoce.cellar.cl.domain.email.Email;

import java.util.Date;
import java.util.List;

public interface EmailDao extends BaseDao<Email,Long> {
	
	/**
	 * Retrieves all email entries eligible to be sent.
	 * @return the email entries eligible to be sent.
	 */
    List<Email> getEmailsToSend();
    
    /**
     * Retrieves all email entries created before the provided
     * cutoff date.
     * @param cutoffDate the cutoff date.
     * @return the corresponding email entries.
     */
    List<Email> getEmailsCreatedBefore(Date cutoffDate);
    
}
