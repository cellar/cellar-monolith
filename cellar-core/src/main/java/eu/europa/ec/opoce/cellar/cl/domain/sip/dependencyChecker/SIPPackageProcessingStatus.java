/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker
 *        FILE : SIPPackageProcessingStatus.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 02-12-2021
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2021 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker;

import org.apache.commons.lang.StringUtils;

/**
 * 
 * @author EUROPEAN DYNAMICS S.A.
 */
public enum SIPPackageProcessingStatus {
	
	/**
	 * The resource dependencies of the SIP have been computed
	 * and is available for scheduling.
	 */
	NEW("N"),
	/**
	 * A priority has been assigned to the SIP and
	 * it is scheduled for ingestion.
	 */
	SCHEDULED("S"),
	/**
	 * The SIP has been submitted to the ingestion executor.
	 */
	INGESTING("I"),
	/**
	 * The ingestion of the SIP has been finished.
	 */
	FINISHED("F");

	String value;
	private SIPPackageProcessingStatus(final String value){
		this.value=value;
	}

	public static SIPPackageProcessingStatus findByStatus(final String status) {
		if (StringUtils.isBlank(status)) {
			return null;
		}

		for (final SIPPackageProcessingStatus executionStatus : values()) {
			if (status.equals(executionStatus.value)) {
				return executionStatus;
			}
		}

		return null;
	}

	public String getValue() {
		return this.value;
	}
}
