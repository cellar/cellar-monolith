/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.aligner
 *             FILE : HierarchyAlignerService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Aug 22, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.aligner;

import eu.europa.ec.opoce.cellar.cl.domain.AbstractBatchManager.ManagerStatus;
import eu.europa.ec.opoce.cellar.cl.domain.aligner.Hierarchy;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Aug 22, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IHierarchyAlignerService {

    /**
     * Align hierarchy.
     *
     * @param productionId the production id
     * @throws InterruptedException the interrupted exception
     */
    void alignHierarchy(final String productionId) throws InterruptedException;

    /**
     * Align unknown.
     *
     * @return true, if successful
     */
    boolean alignUnknown();

    /**
     * Start aligning.
     */
    void startAligning();

    /**
     * Stop aligning.
     */
    void stopAligning();

    /**
     * Fix hierarchy.
     *
     * @param hierarchy the hierarchy
     */
    void fixHierarchy(final Hierarchy hierarchy);

    /**
     * Gets the manager status.
     *
     * @return the manager status
     */
    ManagerStatus getManagerStatus();

}
