/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.ccr.domain.impl.update
 *        FILE : UpdateStructMapTransaction.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 08-05-2013
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.domain.impl.update;

import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.ccr.domain.impl.TransactionConfiguration;
import eu.europa.ec.opoce.cellar.ccr.helper.DigitalObjectHelper;
import eu.europa.ec.opoce.cellar.ccr.helper.ProductionIdentifierHelper;
import eu.europa.ec.opoce.cellar.ccr.helper.ProductionIdentifierHelper.IdentifierType;
import eu.europa.ec.opoce.cellar.ccr.helper.ResponseHelper;
import eu.europa.ec.opoce.cellar.ccr.strategy.AppendNewExpressionDigitalObjectStrategy;
import eu.europa.ec.opoce.cellar.ccr.strategy.AppendNewManifestationDigitalObjectStrategy;
import eu.europa.ec.opoce.cellar.ccr.strategy.WriteContentStreamStrategy;
import eu.europa.ec.opoce.cellar.cl.dao.ProductionIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.admin.ProductionIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.cl.domain.history.service.IdentifiersHistoryService;
import eu.europa.ec.opoce.cellar.cl.domain.response.DigitalObjectOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.ResponseOperationType;
import eu.europa.ec.opoce.cellar.cl.domain.response.UpdateStructMapOperation;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierResolver;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData;
import eu.europa.ec.opoce.cellar.cmr.service.EmbargoService;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMapTypeHelper;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.StructMapProcessorException;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

/**
 * <class_description> This class provides methods for the update process of a StructMap into database and S3.
 * Every digital objects (with their contents and metadatas) under a given StructMap will be created in database and S3.
 * If an error occurs during this process, the entire process is rolled back, in order that all digital objects (with metadata and content)
 * will be removed from the database and S3 if they are created, or set to the previous version if they are updated.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 08-05-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component("structMapUpdate")
public class UpdateStructMapTransaction extends AbstractUpdateStructMapTransaction {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateStructMapTransaction.class);

    @Autowired
    @Qualifier("pidManagerService")
    private IdentifierService identifierService;

    @Autowired
    private IdentifierResolver identifierResolver;

    @Autowired
    @Qualifier("pidManagerService")
    private PidManagerService pidManagerService;

    @Autowired
    private ProductionIdentifierDao productionIdentifierDao;

    @Autowired
    private IdentifiersHistoryService identifiersHistoryService;

    @Autowired
    private EmbargoService embargoHandlingService;

    @Autowired
    private ContentStreamService contentStreamService;

    /**
     * Instantiates a new update struct map transaction.
     */
    public UpdateStructMapTransaction() {
        // load existing direct and inferred model
        super(TransactionConfiguration.get().withLoadExistingDirectAndInferredModel(true));
    }

    /**
     * Process all digital objects of the hierarchy contained into 'calculatedData' in depth first traversal.
     *
     * @param operation                          a reference to the generated response object
     * @param calculatedData                     the calculated data where to get the hierarchy from
     * @param metsDirectory                      a reference to the directory where the SIP has been exploded.
     * @param sipResource                        needed for audit
     * @param hiddenContentStreamIdentifierList  actual removed digital object list.
     * @param appendedDOList                     actual appended digital object list
     * @param nextExpressionCellarIdSuffixNumber the next expression cellar identifier.
     */
    @Override
    protected void processHierarchy(final UpdateStructMapOperation operation, final CalculatedData calculatedData, final File metsDirectory,
                                    final SIPResource sipResource, final List<CellarIdentifier> hiddenContentStreamIdentifierList,
                                    final List<DigitalObject> appendedDOList, final AtomicInteger nextExpressionCellarIdSuffixNumber) {
        this.internalProcessHierarchy(operation, calculatedData.getStructMap().getDigitalObject(), calculatedData, metsDirectory,
                sipResource, hiddenContentStreamIdentifierList, appendedDOList, nextExpressionCellarIdSuffixNumber);
    }

    /**
     * Internal process hierarchy.
     *
     * @param operation                          the operation
     * @param currDigitalObject                  the curr digital object
     * @param calculatedData                     the calculated data
     * @param metsDirectory                      the mets directory
     * @param sipResource                        the sip resource
     * @param hiddenContentStreamIdentifierList  the hidden content stream identifier list
     * @param appendedDOList                     the appended DO list
     * @param nextExpressionCellarIdSuffixNumber the next expression cellar id suffix number
     */
    private void internalProcessHierarchy(final UpdateStructMapOperation operation, final DigitalObject currDigitalObject,
                                          final CalculatedData calculatedData, final File metsDirectory, final SIPResource sipResource,
                                          final List<CellarIdentifier> hiddenContentStreamIdentifierList, final List<DigitalObject> appendedDOList,
                                          final AtomicInteger nextExpressionCellarIdSuffixNumber) {
        final StructMap structMap = calculatedData.getStructMap();
        final String rootCellarId = calculatedData.getRootCellarId();
        final ProductionIdentifier pid = ProductionIdentifierHelper.getProductionIdentifier(currDigitalObject,
                this.productionIdentifierDao);
        final DigitalObjectType digitalObjectType = currDigitalObject.getType();

        switch (digitalObjectType) {
            case WORK: /* U1.1 */ {
                this.checkAssignAndSetPidToResponse(pid, operation, currDigitalObject);
                //We delete all expressions which doesn't exist in this mets but exist in s3.
                final ContentIdentifier cellarId = currDigitalObject.getCellarId();
                final String identifier = cellarId.getIdentifier();
                final List<ContentIdentifier> contentids = currDigitalObject.getContentids();
                if (StructMapTypeHelper.isTreeAllType(structMap) || StructMapTypeHelper.isTreeMetadataType(structMap)) {
                    checkReadOnlyOnSubTree(currDigitalObject, identifier, contentids);
                    DigitalObjectHelper.deleteNoMoreExistingDO(pid, currDigitalObject, operation, true,
                            StructMapTypeHelper.isTreeMetadataType(structMap), this.identifierService, this.pidManagerService,
                            this.productionIdentifierDao);
                }
                if (StructMapTypeHelper.isTreeContentType(structMap)) {
                    DigitalObjectHelper.verifyThatNoDOChildrenShouldBeDeleted(pid, currDigitalObject, this.productionIdentifierDao,
                            this.identifierService);
                }
                checkReadOnlyOnItself(currDigitalObject, identifier, contentids);
                break;
            }
            case EXPRESSION: /* U1.2 */ {
                if (pid == null) {
                    // if it is a tree.* update, add this new Expression and all associated manifestations+contents
                    if (StructMapTypeHelper.isTreeType(structMap.getMetadataOperationType())) {
                        final AppendNewExpressionDigitalObjectStrategy appendNewDigitalObjectStrategy = new AppendNewExpressionDigitalObjectStrategy(
                                rootCellarId, sipResource, metsDirectory, operation, appendedDOList, nextExpressionCellarIdSuffixNumber,
                                pidManagerService, contentStreamService, identifiersHistoryService, cellarIdentifierService);
                        appendNewDigitalObjectStrategy.applyStrategy(currDigitalObject);
                    } else {
                        ProductionIdentifierHelper.throwNonExistingIdentifierException(IdentifierType.PRODUCTION, currDigitalObject);
                    }
                } else {
                    // UPDATE this expression
                    this.assignAndSetPidToResponse(pid, operation, currDigitalObject);

                    final ContentIdentifier cellarId = currDigitalObject.getCellarId();
                    final String identifier = cellarId.getIdentifier();
                    final List<ContentIdentifier> contentids = currDigitalObject.getContentids();

                    //We delete manifestations which doesn't exist in this mets but exist in s3.
                    if (StructMapTypeHelper.isTreeAllType(structMap) || StructMapTypeHelper.isTreeMetadataType(structMap)) {
                        checkReadOnlyOnSubTree(currDigitalObject, identifier, contentids);
                        DigitalObjectHelper.deleteNoMoreExistingDO(pid, currDigitalObject, operation, false,
                                StructMapTypeHelper.isTreeMetadataType(structMap), this.identifierService, this.pidManagerService,
                                this.productionIdentifierDao);
                    }
                    if (StructMapTypeHelper.isTreeContentType(structMap)) {
                        DigitalObjectHelper.verifyThatNoDOChildrenShouldBeDeleted(pid, currDigitalObject, this.productionIdentifierDao,
                                this.identifierService);
                    }
                    checkReadOnlyOnItself(currDigitalObject, identifier, contentids);
                }
                break;
            }
            case MANIFESTATION: /* U1.5-6 */ {

                if (appendedDOList.contains(currDigitalObject)) {
                    break;
                }
                if (pid == null) { // if it is a tree.* update, add this new Manifestation and all associated contents
                    if (StructMapTypeHelper.isTreeType(structMap.getMetadataOperationType())) {
                        final String expressionUUID = currDigitalObject.getParentObject().getCellarId().getIdentifier();
                        final AppendNewManifestationDigitalObjectStrategy appendNewDigitalObjectStrategy = new AppendNewManifestationDigitalObjectStrategy(
                                expressionUUID, sipResource, metsDirectory, operation, appendedDOList, pidManagerService, contentStreamService,
                                identifiersHistoryService, cellarIdentifierService, cellarResourceDao);
                        appendNewDigitalObjectStrategy.applyStrategy(currDigitalObject);
                    } else {
                        ProductionIdentifierHelper.throwNonExistingIdentifierException(IdentifierType.PRODUCTION, currDigitalObject);
                    }
                } else {
                    // UPDATE this manifestation
                    checkDate(pid, currDigitalObject);
                    this.assignAndSetPidToResponse(pid, operation, currDigitalObject);

                    final ContentIdentifier cellarId = currDigitalObject.getCellarId();
                    final String identifier = cellarId.getIdentifier();
                    final List<ContentIdentifier> contentids = currDigitalObject.getContentids();
                    checkReadOnlyOnSubTree(currDigitalObject, identifier, contentids);
                    /* U1.5 */
                    //Only update content if we are on a Tree.ALL, Tree.content, Node.all or Node.content
                    if (StructMapTypeHelper.isTreeAllType(structMap) || StructMapTypeHelper.isNodeAllType(structMap)
                            || StructMapTypeHelper.isNodeContentType(structMap) || StructMapTypeHelper.isTreeContentType(structMap)) {
                        updateContent(operation, pid.getCellarIdentifier(), currDigitalObject, metsDirectory);
                    } else {
                        DigitalObjectHelper.setCellarIdOnAllContent(currDigitalObject, this.identifierResolver);
                    }
                }
                break;
            }
            case DOSSIER: /* U1.3-4 */ {
                ProductionIdentifierHelper.checkProductionIdentifierExists(pid, currDigitalObject);

                if (StructMapTypeHelper.isNodeType(structMap.getMetadataOperationType())) { /* U1.3 */
                    this.assignAndSetPidToResponse(pid, operation, currDigitalObject);
                    final ContentIdentifier cellarId = currDigitalObject.getCellarId();
                    final String identifier = cellarId.getIdentifier();
                    final List<ContentIdentifier> contentids = currDigitalObject.getContentids();
                    checkReadOnlyOnItself(currDigitalObject, identifier, contentids);
                } else if (StructMapTypeHelper.isTreeType(structMap.getMetadataOperationType())) { /* U1.4 */

                    this.assignAndSetPidToResponse(pid, operation, currDigitalObject);

                    final ContentIdentifier cellarId = currDigitalObject.getCellarId();
                    final String identifier = cellarId.getIdentifier();
                    final List<ContentIdentifier> contentids = currDigitalObject.getContentids();
                    checkReadOnlyOnSubTree(currDigitalObject, identifier, contentids);

                    final CellarIdentifier rootCellarIdentifierForReplaceTree = pid.getCellarIdentifier();
                    this.replaceDossierTree(operation, rootCellarIdentifierForReplaceTree, currDigitalObject, metsDirectory);
                } else {
                    throw ExceptionBuilder.get(StructMapProcessorException.class).withCode(CoreErrors.E_026)
                            .withMessage("StructMapType allowed are 'node', 'tree' but found [" + structMap.getMetadataOperationType() + "]")
                            .build();
                }

                break;
            }
            case EVENT:
            case AGENT:
            case TOPLEVELEVENT: {
                this.checkAssignAndSetPidToResponse(pid, operation, currDigitalObject);
                final ContentIdentifier cellarId = currDigitalObject.getCellarId();
                final String identifier = cellarId.getIdentifier();
                final List<ContentIdentifier> contentids = currDigitalObject.getContentids();
                checkReadOnlyOnItself(currDigitalObject, identifier, contentids);
                break;
            }
            default:
                throw ExceptionBuilder.get(StructMapProcessorException.class).withCode(CoreErrors.E_036)
                        .withMessage("Type [" + digitalObjectType + "]").build();
        }

        for (final DigitalObject childDigitalObject : currDigitalObject.getChildObjects()) {
            this.internalProcessHierarchy(operation, childDigitalObject, calculatedData, metsDirectory, sipResource,
                    hiddenContentStreamIdentifierList, appendedDOList, nextExpressionCellarIdSuffixNumber);
        }
    }

    /**
     * Replace all event of a dossier by new ones.
     *
     * @param operation     the response
     * @param rootCellarId  the root digital object for new ones.
     * @param dossierDO     the dossier from which all event should be 'replaced'
     * @param metsDirectory a reference to the directory where the SIP has been exploded.
     * @throws StructMapProcessorException if an error occurs.
     */
    private void replaceDossierTree(final UpdateStructMapOperation operation, final CellarIdentifier rootCellarId,
                                    final DigitalObject dossierDO, final File metsDirectory) throws StructMapProcessorException {

        //Construct two map to retrieve existing cellarId, event, ...
        final Map<DigitalObject, String> eventCellarIdMap = new HashMap<>();
        final Map<String, DigitalObject> cellarIdEventMap = new HashMap<>();

        for (final DigitalObject event : dossierDO.getChildObjects()) {
            final ProductionIdentifier pid = ProductionIdentifierHelper.getProductionIdentifier(event, this.productionIdentifierDao);
            if (pid != null) {
                eventCellarIdMap.put(event, pid.getCellarIdentifier().getUuid());
                cellarIdEventMap.put(pid.getCellarIdentifier().getUuid(), event);
            }
        }

        List<String> deleteList = cellarResourceDao.findStartingWith(rootCellarId.getUuid()).stream()
                .map(CellarResource::getCellarId)
                .filter(s -> !s.equals(rootCellarId.getUuid()))
                .sorted(String::compareTo)
                .collect(Collectors.toList());
        int lastIndexFound = 0;
        for (final String cellarIdToDelete : deleteList) {
            //dossierID.xxxx => take xxxx
            final int currentIndex = Integer.parseInt(StringUtils.substringAfterLast(cellarIdToDelete, "."));
            if (lastIndexFound < currentIndex) {
                lastIndexFound = currentIndex;
            }

            //if this cellar id doesn't exist in map, then delete this "old" event.
            if (!cellarIdEventMap.containsKey(cellarIdToDelete)) {
                this.pidManagerService.deleteCellarIdentifier(cellarIdToDelete);
                operation.addOperation(new DigitalObjectOperation(cellarIdToDelete, ResponseOperationType.DELETE));

                contentStreamService.deleteRecursively(cellarIdToDelete);
                existingMetadataLoader.evictCache(cellarIdToDelete);
            }
        }

        final ContentIdentifier dossierCID = dossierDO.getCellarId();
        int currentNumberOfEvent = lastIndexFound + 1;
        //Assign CellarId to each new event of this dossier.
        for (final DigitalObject event : dossierDO.getChildObjects()) {
            final String foundedEventCellarId = eventCellarIdMap.get(event);

            //This event is not an update but a create!!!
            if (foundedEventCellarId == null) {
                final String generatedEventCellarUUID = identifiersHistoryService.generateCellarID(event, dossierCID.getIdentifier(),
                        currentNumberOfEvent++, TYPE.AUTHENTICOJ);
                this.pidManagerService.save(event, generatedEventCellarUUID, false);
                event.setCellarId(new ContentIdentifier(generatedEventCellarUUID));

                // TODO: manage ingestedDigitalObjectPidList like
                event.applyStrategyOnSelfAndChildren(new WriteContentStreamStrategy(metsDirectory, contentStreamService)); //TODO AJ, onSelfAndChildren ???

                //set for new event the create operation entries
                //set false for dmd parameters because at this state, cmr has not yet updated the dmd
                operation.addOperation(ResponseHelper.fillCreateOperation(event));
            } else {
                //It's an update, this event already exist in S3.
                event.setCellarId(new ContentIdentifier(foundedEventCellarId));

                final DigitalObjectOperation digitalObjectOperation = new DigitalObjectOperation(foundedEventCellarId,
                        ResponseOperationType.UPDATE);
                operation.addOperation(digitalObjectOperation);
            }
        }
    }

}
