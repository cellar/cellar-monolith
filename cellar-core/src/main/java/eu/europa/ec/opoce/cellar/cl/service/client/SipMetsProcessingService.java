/**
 * 
 */
package eu.europa.ec.opoce.cellar.cl.service.client;

import java.util.List;

import eu.europa.ec.opoce.cellar.cl.domain.sip.SipMetsProcessing;

/**
 * Service for the the Entity {@link SipMetsProcessing}.
 * 
 * @author dcraeye
 * 
 */
public interface SipMetsProcessingService {

    /**
     * Persist the given transient instance.
     * 
     * @param sipMetsProcessing
     *            the transient instance to be persisted
     */
    void createSipMetsProcessing(SipMetsProcessing sipMetsProcessing);

    /**
     * Delete the given persistent instance.
     * 
     * @param sipMetsProcessing
     *            the persistent instance to delete
     */
    void deleteSipMetsProcessing(SipMetsProcessing sipMetsProcessing);

    /**
     * Return all persistent instances of {@link SipMetsProcessing}.
     * 
     * @return a {@link List} containing 0 or more persistent instances.
     */
    List<SipMetsProcessing> getSipMetsProcessingList();

    /**
     * Update a SipMetsProcessing.
     * 
     * @param sipMetsProcessing
     *            the {@link SipMetsProcessing} to create
     */
    void updateSipMetsProcessing(SipMetsProcessing sipMetsProcessing);

    /**
     * Get the {@link SipMetsProcessing} corresponding to this id.
     * @param sipMetsProcessingId id of the sipMetsProcessing to returns.
     * @returns the corresponding {@link SipMetsProcessing}
     */
    SipMetsProcessing getSipMetsProcessing(Long sipMetsProcessingId);

    /**
     * Return a SipMetsProcessing where the sipName equals the given sipName  
     * @param sipName to retrieve.
     * @return
     */
    SipMetsProcessing getBySipName(final String sipName);

    /**
     * Return a SipMetsProcessing where the metsfolder equals the given metsFolder  
     * @param metsFolder to retrieve.
     * @param metsFolder
     * @return
     */
    SipMetsProcessing getByMetsFolder(final String metsFolder);

    /**
     * Return all SipMetsProcessing order by PRIORITY DESC, ID and STRUCTMAP_COUNT. 
     * @return a list of SipMetsProcessing.
     */
    List<SipMetsProcessing> getAll();

    /**
     * Return all SipMetsProcessing order by PRIORITY DESC. 
     * @return list of SipMetsProcessing.
     */
    List<SipMetsProcessing> getAllByPriority();

    /**
     * Return all SipMetsProcessing order by STRUCTMAP_COUNT. 
     * @return a list of SipMetsProcessing.
     */
    List<SipMetsProcessing> getAllBySMCount();

    /**
     * Return all SipMetsProcessing order by DIGITAL_OBJECT_COUNT. 
     * @return a list of SipMetsProcessing.
     */
    List<SipMetsProcessing> getAllByDOCount();

}
