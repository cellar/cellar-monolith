/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.dao
 *             FILE : ExtractionExecutionDao.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 5, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao;

import java.util.List;

import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionConfiguration.CONFIGURATION_TYPE;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 5, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface ExtractionExecutionDao extends BaseDao<ExtractionExecution, Long> {

    /**
     * Returns a list of pending executions and updates these executions to working.
     * @param batchSize the maximum list size
     * @param instanceId the instance identifier
     * @return a list of pending executions
     */
    List<ExtractionExecution> updatePendingExtractionExecutions(final int batchSize, final String instanceId);

    /**
     * Returns a pending execution and updates this execution to working.
     * @param instanceId the instance identifier
     * @return a pending execution
     */
    ExtractionExecution updatePendingExtractionExecution(final String instanceId);

    /**
     * Schedules the executions of the day.
     * @param batchSize the batch size to use
     * @return the number of executions scheduled
     */
    int updateNewExtractionExecutions(final int batchSize);

    /**
     * Adds an extraction execution.
     * @param extractionExecution the execution to add
     */
    void addExtractionExecution(final ExtractionExecution extractionExecution);

    /**
     * Deletes the executions outdated corresponding to a type of configuration.
     * @param configurationType the configuration type
     * @param numberOfDays the number of days to compute the deadline
     * @param batchSize the batch size to use
     * @return a list of the deleted executions
     */
    List<ExtractionExecution> deleteExtractionExecutions(final CONFIGURATION_TYPE configurationType, final int numberOfDays,
            final int batchSize);

    /**
     * Deletes the executions of the identified configuration.
     * @param configurationId the configuration identifier
     * @return a list of the deleted executions
     */
    List<ExtractionExecution> deleteExtractionExecutions(final Long configurationId);

    /**
     * Returns a list of executions of the identified configuration.
     * @param extractionConfigurationId the identifier of configuration
     * @return a list of executions
     */
    List<ExtractionExecution> findExtractionExecutions(final Long extractionConfigurationId);

    /**
     * Returns a list of executions (with the number of documents) of the identified configuration.
     * @param extractionConfigurationId the identifier of configuration
     * @return a list of executions
     */
    List<Object> findExtractionExecutionsNumberOfDocuments(final Long extractionConfigurationId);

    /**
     * Returns the identified execution.
     * @param id the identifier of execution
     * @return the identified execution
     */
    ExtractionExecution findExtractionExecution(final Long id);

    /**
     * Restarts the step of the identified execution.
     * @param id the identifier of execution
     */
    void restartStepExtractionExecution(final Long id);

    /**
     * Restarts the steps.
     */
    int restartStepExtractionExecution();

    /**
     * Restarts the identified execution.
     * @param id the identifier of execution
     */
    void restartExtractionExecution(final Long id);

    /**
     * Restarts the executions.
     */
    int restartExtractionExecution();

    /**
     * Returns a list of ad-hoc executions.
     * @return a list of ad-hoc executions
     */
    List<ExtractionExecution> findAdhocExtractionExecutions();

    /**
     * Cleans the unfinished executions on the identified instance. 
     * @param batchSize the batch size to use
     * @param instanceId the identifier of instance
     * @return the number of executions restarted
     */
    int cleanExtractionExecutions(final int batchSize, final String instanceId);
}
