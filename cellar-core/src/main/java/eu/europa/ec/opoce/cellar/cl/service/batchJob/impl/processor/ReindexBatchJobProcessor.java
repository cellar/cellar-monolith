/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor
 *             FILE : ReindexBatchJobProcessor.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 4, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor;

import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob.BATCH_JOB_TYPE;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.nal.service.LanguageService;
import eu.europa.ec.opoce.cellar.server.admin.indexing.IndexingInvoker;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 4, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Configurable
public class ReindexBatchJobProcessor extends AbstractBatchJobProcessor {

    private static final Logger LOG = LogManager.getLogger(ReindexBatchJobProcessor.class);

    /**
     * Service to generate and save the indexing requests.
     */
    @Autowired
    private IndexingInvoker indexingInvoker;

    @Autowired
    @Qualifier("pidManagerService")
    private IdentifierService identifierService;

    @Autowired
    private LanguageService languageService;

    public ReindexBatchJobProcessor(final BatchJob batchJob) {
        super(batchJob);
    }

    @Override
    protected BATCH_JOB_TYPE getBatchJobType() {
        return BATCH_JOB_TYPE.REINDEX;
    }

    @Override
    protected void doExecute(final String workId, final BatchJob batchJob) {
        try {
            final String cellarId = this.identifierService.getCellarPrefixed(workId);
            this.indexingInvoker.addWork(cellarId, this.batchJob.isGenerateIndexNotice(), this.batchJob.isGenerateEmbeddedNotice(),
                    this.batchJob.getPriority(), languageService.getRequiredLanguageCodes());
        } catch (final Exception exception) {
            LOG.warn("Cannot add the indexing request of '{}'", workId);
        }
    }

    @Override
    protected void preExecute() {
        batchJobService.updateProcessingBatchJob(batchJob);
    }

    @Override
    protected void postExecute() {
        this.batchJobService.updateDoneBatchJob(batchJob);
    }
}
