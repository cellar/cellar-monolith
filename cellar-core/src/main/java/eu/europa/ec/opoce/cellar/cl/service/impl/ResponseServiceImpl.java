package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.service.client.ResponseService;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.ResponseOperationException;
import eu.europa.ec.opoce.cellar.support.FileExtensionUtils;

import java.io.File;
import java.io.FilenameFilter;

import javax.annotation.PostConstruct;

import org.apache.commons.io.filefilter.NameFileFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * Implementation of the {@link ResponseService}.
 * @author phvdveld
 *
 */
@Service
public class ResponseServiceImpl implements ResponseService {

    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    private File authenticOJResponseDirectory;
    private File dailyResponseDirectory;
    private File bulkResponseDirectory;
    private File bulkLowPriorityResponseDirectory;

    @PostConstruct
    public void initialize() {
        this.authenticOJResponseDirectory = new File(this.cellarConfiguration.getCellarFolderAuthenticOJResponse());
        this.dailyResponseDirectory = new File(this.cellarConfiguration.getCellarFolderDailyResponse());
        this.bulkResponseDirectory = new File(this.cellarConfiguration.getCellarFolderBulkResponse());
        this.bulkLowPriorityResponseDirectory = new File(this.cellarConfiguration.getCellarFolderBulkLowPriorityResponse());
    }

    @Override
    public File getSIPAuthenticOJResponse(final String metsId) {
        return this.getSIPResponse(metsId, this.authenticOJResponseDirectory);
    }

    @Override
    public File getSIPDailyResponse(final String metsId) {
        return this.getSIPResponse(metsId, this.dailyResponseDirectory);
    }

    @Override
    public File getSIPBulkResponse(final String metsId) {
        return this.getSIPResponse(metsId, this.bulkResponseDirectory);
    }
    
    @Override
    public File getSIPBulkLowPriorityResponse(final String metsId) {
        return this.getSIPResponse(metsId, this.bulkLowPriorityResponseDirectory);
    }

    private File getSIPResponse(final String metsId, final File folder) {
        FilenameFilter filter = new NameFileFilter(metsId + FileExtensionUtils.SIP);
        File[] files = folder.listFiles(filter);
        if (files.length != 1) {
            throw ExceptionBuilder.get(ResponseOperationException.class).withCode(CoreErrors.E_061)
                    .withMessage("File: " + metsId + FileExtensionUtils.SIP).build();
        }
        return files[0];
    }

}
