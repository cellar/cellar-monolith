/**
 * 
 */
package eu.europa.ec.opoce.cellar.support;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author dsavares
 * 
 */
@XmlRootElement(name = "sparql", namespace = SparqlUtils.NAME_SPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class Sparql {

    @XmlElementWrapper(name = "head", namespace = SparqlUtils.NAME_SPACE)
    @XmlElements(value = {
            @XmlElement(name = "variable", namespace = SparqlUtils.NAME_SPACE)})
    private List<SparqlVariable> variables;

    @XmlElementWrapper(name = "results", namespace = SparqlUtils.NAME_SPACE)
    @XmlElements(value = {
            @XmlElement(name = "result", namespace = SparqlUtils.NAME_SPACE)})
    private List<SparqlResult> results;

    public void setVariables(List<SparqlVariable> variables) {

        this.variables = variables;
    }

    public List<SparqlVariable> getVariables() {
        if (variables == null) {
            variables = new ArrayList<SparqlVariable>();
        }
        return variables;
    }

    /**
     * Sets a new value for the results.
     * @param results the results to set
     */
    public void setResults(List<SparqlResult> results) {
        this.results = results;
    }

    /**
     * Gets the value of the results.
     * @return the results
     */
    public List<SparqlResult> getResults() {
        if (results == null) {
            results = new ArrayList<SparqlResult>();
        }
        return results;
    }

    @Override
    public String toString() {
        return "Sparql [head=" + variables + ", results=" + results + "]";
    }

}
