/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.event
 *        FILE : UserAccessRequestAcceptanceEvent.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 16-03-2023
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2023 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.event;

import eu.europa.ec.opoce.cellar.cl.domain.admin.UserAccessRequest;

/**
 * Event object signifying the acceptance of a user access request.
 * @author EUROPEAN DYNAMICS S.A
 */
public class UserAccessRequestAcceptanceEvent {

	/**
	 * 
	 */
	private UserAccessRequest userAccessRequest;

	public UserAccessRequestAcceptanceEvent(UserAccessRequest userAccessRequest) {
		this.userAccessRequest = userAccessRequest;
	}

	public UserAccessRequest getUserAccessRequest() {
		return userAccessRequest;
	}
	
}
