/**
 * 
 */
package eu.europa.ec.opoce.cellar.cl.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import eu.europa.ec.opoce.cellar.cl.dao.SipMetsProcessingDao;
import eu.europa.ec.opoce.cellar.cl.domain.sip.SipMetsProcessing;

/**
 * Implementation of the {@link SipMetsProcessingDao}.
 * @author dcraeye
 *
 */
@Repository
public class SipMetsProcessingDaoImpl extends BaseDaoImpl<SipMetsProcessing, Long> implements SipMetsProcessingDao {

    /**
     * {@inheritDoc}
     */
    @Override
    public void createSipMetsProcessing(final SipMetsProcessing sipMetsProcessing) {
        super.saveObject(sipMetsProcessing);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteSipMetsProcessing(final SipMetsProcessing sipMetsProcessing) {
        super.deleteObject(sipMetsProcessing);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SipMetsProcessing> getSipMetsProcessingList() {
        return super.findAll();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SipMetsProcessing getSipMetsProcessing(final Long id) {
        return super.getObject(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateSipMetsProcessing(SipMetsProcessing sipMetsProcessing) {
        super.updateObject(sipMetsProcessing);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SipMetsProcessing getBySipName(final String sipName) {
        return super.findObjectByNamedQuery("getBySipName", sipName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SipMetsProcessing getByMetsFolder(final String metsFolder) {
        return super.findObjectByNamedQuery("getByMetsFolder", metsFolder);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SipMetsProcessing> getAll() {
        return super.findObjectsByNamedQuery("getAll", (Object[]) null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SipMetsProcessing> getAllByPriority() {
        return super.findObjectsByNamedQuery("getAllByPriority", (Object[]) null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SipMetsProcessing> getAllBySMCount() {
        return super.findObjectsByNamedQuery("getAllBySMCount", (Object[]) null);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SipMetsProcessing> getAllByDOCount() {
        return super.findObjectsByNamedQuery("getAllByDOCount", (Object[]) null);
    }

}
