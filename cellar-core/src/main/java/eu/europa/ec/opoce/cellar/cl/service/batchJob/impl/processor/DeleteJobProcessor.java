package eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor;

import eu.europa.ec.opoce.cellar.cl.concurrency.Concurrent;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob.BATCH_JOB_TYPE;
import eu.europa.ec.opoce.cellar.domain.content.mets.OffIngestionOperationType;
import eu.europa.ec.opoce.cellar.virtuoso.VirtuosoService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import java.util.Collections;
import java.util.List;

/**
 * <class_description> Batch job delete processor.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : September 1, 2022
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Configurable
public class DeleteJobProcessor extends AbstractBatchJobProcessor {

    /**
     * Logger instance.
     */
    private static final Logger LOG = LogManager.getLogger(DeleteJobProcessor.class);
    public static final String SOME_GRAPHS_FAILED_TO_BE_DELETED = "The following graphs failed to be deleted:";
    @Autowired
    private VirtuosoService virtuosoService;

    /**
     * Instantiates a new delete batch job processor.
     *
     * @param batchJob the batch job
     */
    public DeleteJobProcessor(final BatchJob batchJob) {
        super(batchJob);
    }

    @Override
    protected void doExecute(String workId,
                             BatchJob batchJob) {
        // uses execute method instead
    }

    @Concurrent(locker = OffIngestionOperationType.VIRTUOSO_CLEANUP)
    protected boolean execute(final String workId, final BatchJob batchJob) {
        try {
            LOG.info("Deleting graphs from virtuoso '{}'", workId);
            this.virtuosoService.drop(Collections.singleton(workId), false, true);
            LOG.info("The graphs with id '{}' has been successfully deleted", workId);
        } catch (final Exception exception) {
            if (exception.getMessage() != null && exception.getMessage().contains("not found")) {
                LOG.info("Id '{}' not found in virtuoso, skipped'", workId);
                return true;
            }
            LOG.error("Cannot delete graphs", exception);
            addErrorToBatchJob(exception, batchJob, workId);
            return false;
        }
        return true;
    }

    private void addErrorToBatchJob(Exception exception,
                                    BatchJob batchJob,
                                    String workId) {
        String stacktrace = String.format("<li> {%s}: %s </li>", workId, exception.getLocalizedMessage());
        if (batchJob.getErrorDescription() !=null && batchJob.getErrorDescription().equals(SOME_GRAPHS_FAILED_TO_BE_DELETED)) {
            stacktrace = batchJob.getErrorStacktrace() + "\n " + stacktrace;

        }
        batchJobService.updateErrorBatchJob(batchJob, SOME_GRAPHS_FAILED_TO_BE_DELETED, stacktrace);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BATCH_JOB_TYPE getBatchJobType() {
        return BATCH_JOB_TYPE.DELETE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void preExecute() {
        this.batchJobService.updateProcessingBatchJob(this.batchJob);
    }

    @Override
    protected void postExecute() {
        // uses next method instead
    }

    /**
     * {@inheritDoc}
     */
    protected void postExecute(boolean success) {
        if (success) {
            this.batchJobService.updateDoneBatchJob(this.batchJob);
        }
    }

    @Override
    public void execute() {

        preExecute();

        this.initBatchJobProcessing();

        int i = 0;

        List<String> workIds = null;
        boolean success = false;
        while (true) {
            workIds = this.batchJobWorkIdentifierDao.findWorkIds(this.batchJob.getId(), this.getBatchJobType(), i * BATCH_SIZE,
                    (i + 1) * BATCH_SIZE);
            if (workIds.size() == 0 && i==0){
                success = true; // no work id found no error to show
            }
            for (final String workId : workIds) { // adds the batch
                if (this.execute(workId, this.batchJob)){
                    success = true; // if at least one execute status is done
                }
            }

            if (workIds.size() < BATCH_SIZE) {
                break;
            }

            i++;
        }

        postExecute(success);
    }
}
