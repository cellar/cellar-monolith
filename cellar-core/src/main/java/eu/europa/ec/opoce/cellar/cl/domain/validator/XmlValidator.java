package eu.europa.ec.opoce.cellar.cl.domain.validator;

import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.cl.exception.ValidationException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

/**
 * {@link Validator} implementation that uses xsd files to create a validation schema. This
 * schema is then used to validate XML documents. The expected type of object for this
 * {@link Validator} is a file containing the XML.
 */
@Component("xmlValidator")
public class XmlValidator implements SystemValidator {

    /** class's logger. */
    private transient static final Logger LOGGER = LoggerFactory.getLogger(XmlValidator.class);

    /** Schema used for validation of the xml. */
    private transient Schema schema;

    /**
     * Default constructor.
     */
    public XmlValidator() {
    }

    /**
     * Create a new XmlValidatorService for the given schema.
     *
     * @param xsdFiles
     *            the xsd files needed to validate corresponding xml
     */
    public XmlValidator(final String... xsdFiles) {
        final StreamSource[] streamSources = new StreamSource[xsdFiles.length];
        try {
            for (int i = 0; i < xsdFiles.length; i++) {
                streamSources[i] = new StreamSource(XmlValidator.class.getResourceAsStream(xsdFiles[i]));
            }

            final SchemaFactory xmlSchemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            final ResourceResolver resourceResolver = new ResourceResolver();
            xmlSchemaFactory.setResourceResolver(resourceResolver);

            this.schema = xmlSchemaFactory.newSchema(streamSources);
            // createSources(xsdFiles));
        } catch (final SAXParseException e) {
            LOGGER.info(String.format("CellarError in validation of XSD at line %s col %s", e.getLineNumber(), e.getColumnNumber()));
        } catch (final SAXException e) {
            LOGGER.info("CellarError in validation of XSD");
        }
        finally{
            for(StreamSource streamSource:streamSources){
                IOUtils.closeQuietly(streamSource.getInputStream());
                IOUtils.closeQuietly(streamSource.getReader());
            }
        }
    }

    /**
     * Helper method to create a {@link Source} from a given {@link File}.
     *
     * @param file
     *            - the file to create the source from
     * @return a new {@link Source}
     * @throws FileNotFoundException
     */

    /**
     * Validates the given object.
     *
     * @param toValidate
     *            a {@link File} containing the XML
     * @param sipResource needed by the EvtAuditService to audit this method
     * @return true if the XML is valid, false otherwise
     * @throws ValidationException
     *             if the provided object is not a file
     */
    @Override
    public ValidationResult validate(final ValidationDetails validationDetails) {
        final ValidationResult result = new ValidationResult();

        if (validationDetails.getToValidate() == null) {
            throw ExceptionBuilder.get(ValidationException.class).withCode(CoreErrors.E_054)
                    .withMessage("Object to validate cannot be null").build();
        }
        if (!(validationDetails.getToValidate() instanceof File)) {
            throw ExceptionBuilder.get(ValidationException.class).withCode(CoreErrors.E_054)
                    .withMessage("This service has been called with the wrong input type").build();
        }

        final File xmlFile = (File) validationDetails.getToValidate();
        boolean isValid = false;
        if (this.schema != null) {
            try(InputStream fis= Files.newInputStream(xmlFile.toPath());) {
                final Source xmlSource = new StreamSource(fis);
                this.schema.newValidator().validate(xmlSource);
                isValid = true;
            } catch (final SAXParseException e) {
                throw ExceptionBuilder.get(ValidationException.class).withMessage("cannot validate {} at line {} col {}")
                        .withMessageArgs(xmlFile.getPath(), e.getLineNumber(), e.getColumnNumber()).withCause(e).build();
            } catch (final SAXException e) {
                throw ExceptionBuilder.get(ValidationException.class).withMessage("cannot validate {}").withMessageArgs(xmlFile.getPath())
                        .withCause(e).build();
            } catch (final IOException e) {
                throw ExceptionBuilder.get(ValidationException.class).withMessage("Processing the mets: Cannot read the XML file {}")
                        .withMessageArgs(xmlFile.getPath()).withCause(e).build();
            }
        }

        result.setValid(isValid);

        return result;
    }

}
