package eu.europa.ec.opoce.cellar.ccr.s3;

import java.io.IOException;
import java.nio.charset.Charset;

import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestClientException;

/**
 * Most of this is copied from <see>org.springframework.web.client.DefaultResponseErrorHandler</see>
 */
public class FullMessageErrorHandler implements ResponseErrorHandler {

    /** {@inheritDoc} */
    public boolean hasError(ClientHttpResponse response) throws IOException {
        HttpStatus statusCode = response.getStatusCode();
        return (statusCode.series() == HttpStatus.Series.CLIENT_ERROR || statusCode.series() == HttpStatus.Series.SERVER_ERROR);
    }

    /** {@inheritDoc} */
    public void handleError(ClientHttpResponse response) throws IOException {
        HttpStatus statusCode = response.getStatusCode();
        HttpStatus.Series series = statusCode.series();

        if (series == HttpStatus.Series.CLIENT_ERROR) {
            throw new HttpClientErrorException(statusCode, response.getStatusText() + " - " + readResponse(response));
        } else if (series == HttpStatus.Series.SERVER_ERROR) {
            throw new HttpServerErrorException(statusCode, response.getStatusText() + " - " + readResponse(response));
        } else {
            throw new RestClientException("Unknown status code [" + statusCode + "]");
        }
    }

    private String readResponse(ClientHttpResponse response) {
        try {
            return IOUtils.toString(response.getBody(), (Charset) null);
        } catch (IOException e) {
            throw new RestClientException("Failed to read response", e);
        }
    }
}
