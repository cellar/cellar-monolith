/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.client
 *             FILE : ExtractionService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 8, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.client;

import java.io.IOException;
import java.util.List;

import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ArchiveTreeManager;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionWorkIdentifier;

/**
 * <class_description> Service for scheduling, cleaning and extracting {@link ExtractionExecution} instances.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 8, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface ExtractionArchivingService {

    /**
     * Creates a tar.gz archive.
     * @param directoryWithFilesPath the source directory
     * @param tarGzPath the destination tar.gz path
     * @throws IOException
     */
    public void createTarGzOfFiles(final String directoryWithFilesPath, final String tarGzPath) throws IOException;

    /**
     * Extracts a list of work identifiers.
     * @param language the required language for the extraction
     * @param workIdentifiers the list of work identifiers
     * @param archiveTreeManager the tree manager of the destination directory
     * @throws IOException
     */
    public void extractWorkIdentifiers(final String language, final List<ExtractionWorkIdentifier> workIdentifiers,
            final ArchiveTreeManager archiveTreeManager) throws IOException;
}
