/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : BatchJobPreProcessorFactory.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 16 juin 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor;

import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.common.factory.IFactory;
import org.springframework.stereotype.Component;

/**
 * A factory for creating BatchJobPreProcessor objects.
 */
@Component("batchJobPreProcessorFactory")
public class BatchJobPreProcessorFactory implements IFactory<IBatchJobProcessor, BatchJob> {

    /**
     * Return the needed pre processing according to the batch job type.
     *
     * @param batchJob the batch job
     * @return the i batch job processor
     */
    @Override
    public IBatchJobProcessor create(final BatchJob batchJob) {
        switch (batchJob.getJobType()) {
            case EMBARGO:
            case EXPORT:
            case REINDEX:
            case LAST_MODIFICATION_DATE_UPDATE:
            case DELETE:
                return new CommonBatchJobPreProcessor(batchJob);
            case UPDATE:
            default:
                return null;
        }
    }

}
