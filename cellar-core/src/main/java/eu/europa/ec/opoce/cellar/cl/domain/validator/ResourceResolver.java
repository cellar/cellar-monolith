/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.validator
 *             FILE : ResourceResolver.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 19 Apr 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.validator;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.CharEncoding;
import org.apache.xerces.dom.DOMInputImpl;
import org.apache.xerces.xni.parser.XMLInputSource;
import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSResourceResolver;

import java.io.InputStream;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 19 Apr 2017
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class ResourceResolver implements LSResourceResolver {

    /**
     * Resolve a reference to a resource
     * @param type The type of resource, for example a schema, source XML document, or query
     * @param namespace The target namespace (in the case of a schema document)
     * @param publicId The public ID
     * @param systemId The system identifier (as written, possibly a relative URI)
     * @param baseURI The base URI against which the system identifier should be resolved
     * @return an LSInput object typically containing the character stream or byte stream identified
     * by the supplied parameters; or null if the reference cannot be resolved or if the resolver chooses
     * not to resolve it.
     */
    @Override
    public LSInput resolveResource(final String type, final String namespaceURI, final String publicId, final String systemId,
            final String baseURI) {
        LSInput result = null;
        InputStream is=null;
        try {
            is = Thread.currentThread().getContextClassLoader().getResourceAsStream("xsd/" + systemId);
            if (is != null) {
                final XMLInputSource xmlInputSource = new XMLInputSource(publicId, systemId, null, is, CharEncoding.UTF_8);
                final LSInput di = new DOMInputImpl();
                di.setBaseURI(xmlInputSource.getBaseSystemId());
                di.setByteStream(xmlInputSource.getByteStream());
                di.setCharacterStream(xmlInputSource.getCharacterStream());
                di.setEncoding(xmlInputSource.getEncoding());
                di.setPublicId(xmlInputSource.getPublicId());
                di.setSystemId(xmlInputSource.getSystemId());
                result = di;
            }
        }finally {
            IOUtils.closeQuietly(is);
        }
        return result;
    }

}
