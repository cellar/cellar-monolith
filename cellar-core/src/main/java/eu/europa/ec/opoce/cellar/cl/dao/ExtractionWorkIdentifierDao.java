/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.dao
 *             FILE : ExtractionWorkIdentifierDao.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 5, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao;

import java.util.List;

import eu.europa.ec.opoce.cellar.cl.domain.common.CellarPaginatedList;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionWorkIdentifier;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 5, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface ExtractionWorkIdentifierDao extends BaseDao<ExtractionWorkIdentifier, Long> {

    /**
     * Deletes the {@link ExtractionWorkIdentifier} instances belonging to the {@link ExtractionExecution} identified.
     * @param executionId the identifier of the {@link ExtractionExecution} instance
     */
    public void deleteWorkIdentifiersExecutionId(final Long executionId);

    /**
     * Adds {@link ExtractionWorkIdentifier} instances.
     * @param workIdentifiers the work identifiers to add
     */
    public void addWorkIdentifierBatch(final List<ExtractionWorkIdentifier> workIdentifiers);

    /**
     * Finds {@link ExtractionWorkIdentifier} instances of the execution identified.
     * @param extractionExecutionId the identifier of {@link ExtractionExecution} instance
     * @param first the first of batch (inclusive)
     * @param last the last of batch (exclusive)
     * @return a list of {@link ExtractionWorkIdentifier} instances
     */
    public List<ExtractionWorkIdentifier> findWorkIdentifiers(final Long extractionExecutionId, final int first, final int last);

    /**
     * Returns the number of work identifiers corresponding to the identified execution.
     * @param executionId the execution identifier
     * @return the number of work identifiers corresponding to the identified execution
     */
    public Long getWorkIdentifierCount(final Long executionId);

    /**
     * Returns a list of work identifiers of the identified execution.
     * @param extractionExecutionId the identified of execution
     * @return a lisf of work identifiers
     */
    public List<ExtractionWorkIdentifier> findWorkIdentifiers(final Long extractionExecutionId);

    /**
     * Returns a list of filtered work identifiers of the identified execution.
     * @param extractionExecutionId the identified of execution
     * @param workIdFilter the filter
     * @return a lisf of work identifiers
     */
    public List<ExtractionWorkIdentifier> findMatchWorkIdWorkIdentifiers(final Long extractionExecutionId, final String workIdFilter);

    /**
     * Returns an externally sorted and paginated list of work identifiers belonging to the identified execution.
     * @param extractionExecutionId the identifier of execution
     * @param workIdentifiersList the sorted and paginated list to update
     * @return an externally sorted and paginated list of work identifiers
     */
    public CellarPaginatedList<ExtractionWorkIdentifier> findWorkIdentifiers(final Long extractionExecutionId,
            final CellarPaginatedList<ExtractionWorkIdentifier> workIdentifiersList);
}
