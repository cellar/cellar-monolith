package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;

import java.io.File;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * This bean contains runtime parameters needed to handle the application's folders.
 * @author phvdveld
 *
 */
@Configuration
@Component
public class FileSystemConfiguration {

    /** class's logger. */
    private transient static final Logger LOGGER = LoggerFactory.getLogger(FileSystemConfiguration.class);

    @Autowired
    private ICellarConfiguration cellarConfiguration;

    private File foxmlFolder;

    @PostConstruct
    public void initialize() {
        foxmlFolder = initializeDirectory(cellarConfiguration.getCellarFolderFoxml());
    }

    /** 
     * Initialize a single directory and makes it if it does not exist.
     * @param directoryPath
     * @return
     */
    private File initializeDirectory(String directoryPath) {
        File directory = new File(directoryPath);
        if (!directory.exists()) {
            directory.mkdir();
            LOGGER.debug("created directory {}", directory);
        }
        return directory;
    }

    /**
     * Gets the foxml folder as a File
     * @return a File
     */
    public File getFoxmlFolder() {
        return foxmlFolder;
    }
}
