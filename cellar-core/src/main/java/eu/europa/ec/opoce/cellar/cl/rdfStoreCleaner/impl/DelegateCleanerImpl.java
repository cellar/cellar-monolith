/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.rdfStoreCleaner
 *             FILE : AbstractCleaner.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 11, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.rdfStoreCleaner.impl;

import eu.europa.ec.opoce.cellar.cl.concurrency.Concurrent;
import eu.europa.ec.opoce.cellar.cl.concurrency.IConcurrentArgsResolver;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.enums.CellarServiceRDFStoreCleanerMode;
import eu.europa.ec.opoce.cellar.cl.delegator.AbstractDelegator;
import eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner.CleanerDifference.QueryType;
import eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner.CleanerHierarchyData;
import eu.europa.ec.opoce.cellar.cl.rdfStoreCleaner.DelegateCleaner;
import eu.europa.ec.opoce.cellar.cl.service.rdfStoreCleaner.DigitalObjectCleanerService;
import eu.europa.ec.opoce.cellar.cl.service.rdfStoreCleaner.DigitalObjectLoaderService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.CellarResource.CleanerStatus;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.quad.ModelQuadService;
import eu.europa.ec.opoce.cellar.cmr.service.IInverseRelationService;
import eu.europa.ec.opoce.cellar.database.transaction.RDFTransactional;
import eu.europa.ec.opoce.cellar.domain.content.mets.OffIngestionOperationType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 11, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component
public class DelegateCleanerImpl implements DelegateCleaner, IConcurrentArgsResolver {

    protected static final Logger LOG = LogManager.getLogger(DelegateCleanerImpl.class);

    /**
     * The Cellar configuration storing runtime parameters.
     */
    @Autowired
    @Qualifier("cellarConfiguration")
    protected ICellarConfiguration cellarConfiguration;

    @Autowired
    @Qualifier("modelQuadService")
    protected ModelQuadService modelQuadService;

    @Autowired
    protected IInverseRelationService inverseRelationService;

    @Autowired
    protected DigitalObjectCleanerService digitalObjectCleanerService;

    @Autowired
    protected DigitalObjectLoaderService digitalObjectLoaderService;

    @Autowired
    protected CellarResourceDao cellarResourceDao;

    /**
     * @see eu.europa.ec.opoce.cellar.cl.rdfStoreCleaner.DelegateCleaner#startProcessing(eu.europa.ec.opoce.cellar.cl.delegator.AbstractDelegator)
     */
    @Override
    @Concurrent(locker = OffIngestionOperationType.CLEANING_SCHEDULING)
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void startProcessing(final AbstractDelegator delegator) {
        try {
            for (final CellarResource cellarResource : delegator.getProcessableCellarResources()) {
                cellarResource.setCleanerStatus(CleanerStatus.PROCESSING);
                this.cellarResourceDao.updateObject(cellarResource);
            }
        } catch (final Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.rdfStoreCleaner.DelegateCleaner#clean(eu.europa.ec.opoce.cellar.cl.delegator.AbstractDelegator)
     */
    @Override
    @Concurrent(locker = OffIngestionOperationType.CLEANING)
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @RDFTransactional
    public void clean(final AbstractDelegator delegator) {
        final CleanerHierarchyData cleanerHierarchyData = new CleanerHierarchyData(delegator.getRootCellarId());

        try {
            final Collection<CellarResource> cellarResources = delegator.getCellarResources();

            this.digitalObjectLoaderService.loadHierarchyUris(delegator.getRootCellarId(), cleanerHierarchyData);

            for (final CellarResource cellarResource : cellarResources) {
                this.digitalObjectCleanerService.cleanDigitalObject(cellarResource, cleanerHierarchyData);
            }

            if (this.cellarConfiguration.getCellarServiceRDFStoreCleanerMode() == CellarServiceRDFStoreCleanerMode.fix) {
                this.fix(cleanerHierarchyData);
            }
        } catch (final Exception ex) {
            LOG.error(ex.getMessage(), ex);
            for (final CellarResource cellarResource : delegator.getCellarResources()) {
                cellarResource.setCleanerStatus(CleanerStatus.ERROR);
                this.cellarResourceDao.updateObject(cellarResource);
            }

        } finally {
            cleanerHierarchyData.closeQuietly();
        }
    }

    @Override
    public Object[] resolveArgsForConcurrency(Object[] in) {
        return new Object[] {
                ((AbstractDelegator) in[0]).getRootCellarId()};
    }

    private void fix(final CleanerHierarchyData cleanerHierarchyData) {
        if (cleanerHierarchyData.isRDFStoreFixable()) {
            long startTime = System.currentTimeMillis();
            LOG.info("Updating metadata snippets of Oracle RDF Store...");

            this.modelQuadService.updateDataInOracle(cleanerHierarchyData.isUnderEmbargo(),
                    new ArrayList<String>(cleanerHierarchyData.getMetadataFixes().get(QueryType.delete).keySet()),
                    new ArrayList<String>(cleanerHierarchyData.getInverseFixes().get(QueryType.delete).keySet()),
                    cleanerHierarchyData.getMetadataFixes().get(QueryType.add), cleanerHierarchyData.getInverseFixes().get(QueryType.add));

            LOG.info("Updating metadata snippets of Oracle RDF Store done in {} ms.", String.valueOf(System.currentTimeMillis() - startTime));
        }

        if (cleanerHierarchyData.isInverseRelationsFixable()) {
            long startTime = System.currentTimeMillis();
            LOG.info("Updating inverse relations of Oracle relational...");

            this.inverseRelationService.updateInverseRelations(cleanerHierarchyData.getInverseRelationFixes().get(QueryType.delete),
                    cleanerHierarchyData.getInverseRelationFixes().get(QueryType.add));

            LOG.info("Updating inverse relations of Oracle relational done in {} ms.", String.valueOf(System.currentTimeMillis() - startTime));
        }
    }

}
