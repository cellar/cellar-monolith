/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner
 *             FILE : CleanerHierarchyData.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 3, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner;

import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner.CleanerDifference.QueryType;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.daobeans.InverseRelation;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.RDFStoreCleanerException;
import org.apache.jena.rdf.model.Model;

import java.util.*;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 3, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class CleanerHierarchyData {

    private final String cellarId;

    private Boolean underEmbargo;

    private final List<CleanerDigitalObjectData> cleanerDigitalObjectDatas;

    private final Map<QueryType, Map<String, Model>> metadataFixes;
    private final Map<QueryType, Map<String, Model>> inverseFixes;
    private final Map<QueryType, Collection<InverseRelation>> inverseRelationFixes;

    private final Set<String> metadataUris;

    public CleanerHierarchyData(final String cellarId) {
        this.cellarId = cellarId;

        this.cleanerDigitalObjectDatas = new LinkedList<>();

        this.metadataFixes = new HashMap<>();
        this.metadataFixes.put(QueryType.add, new HashMap<>());
        this.metadataFixes.put(QueryType.delete, new HashMap<>());

        this.inverseFixes = new HashMap<>();
        this.inverseFixes.put(QueryType.add, new HashMap<>());
        this.inverseFixes.put(QueryType.delete, new HashMap<>());

        this.inverseRelationFixes = new HashMap<>();
        this.inverseRelationFixes.put(QueryType.add, new LinkedList<>());
        this.inverseRelationFixes.put(QueryType.delete, new LinkedList<>());

        this.metadataUris = new HashSet<>();
    }

    public String getCellarId() {
        return this.cellarId;
    }


    public Boolean isUnderEmbargo() {
        return this.underEmbargo;
    }

    public void setUnderEmbargo(final Boolean underEmbargo) {
        if (this.underEmbargo != null && this.underEmbargo != underEmbargo) {
            throw ExceptionBuilder.get(RDFStoreCleanerException.class).withCode(CoreErrors.E_500)
                    .withMessage("The elements of the hierarchy identified by {} have not the same embargo status.")
                    .withMessageArgs(this.cellarId).build();
        }

        this.underEmbargo = underEmbargo;
    }

    public CleanerDigitalObjectData getCleanerDigitalObjectDataInstance(final CellarResource cellarResource) {
        final CleanerDigitalObjectData cleanerDigitalObjectData = new CleanerDigitalObjectData(cellarResource.getCellarId(),
                cellarResource.getCellarType(), cellarResource.getVersions(), this);
        this.cleanerDigitalObjectDatas.add(cleanerDigitalObjectData);
        return cleanerDigitalObjectData;
    }

    public boolean isRDFStoreFixable() {
        return !this.metadataFixes.get(QueryType.add).isEmpty()
                || !this.metadataFixes.get(QueryType.delete).isEmpty()
                || !this.inverseFixes.get(QueryType.add).isEmpty()
                || !this.inverseFixes.get(QueryType.delete).isEmpty();
    }

    public boolean isInverseRelationsFixable() {
        return !this.inverseRelationFixes.get(QueryType.add).isEmpty()
                || !this.inverseRelationFixes.get(QueryType.delete).isEmpty();
    }

    public Map<QueryType, Map<String, Model>> getMetadataFixes() {
        return metadataFixes;
    }

    public void putMetadataFixe(final QueryType queryType, final String cellarId, final Model model) {
        this.metadataFixes.get(queryType).put(cellarId, model);
    }

    public Map<QueryType, Map<String, Model>> getInverseFixes() {
        return inverseFixes;
    }

    public void putInverseFixe(final QueryType queryType, final String cellarId, final Model model) {
        this.inverseFixes.get(queryType).put(cellarId, model);
    }

    public Map<QueryType, Collection<InverseRelation>> getInverseRelationFixes() {
        return inverseRelationFixes;
    }

    public void putInverseRelationFixe(final QueryType queryType, final Collection<InverseRelation> inverseRelations) {
        this.inverseRelationFixes.get(queryType).addAll(inverseRelations);
    }

    public Set<String> getMetadataUris() {
        return metadataUris;
    }

    public void addMetadataUri(final String metadataUri) {
        this.metadataUris.add(metadataUri);
    }

    public void closeQuietly() {
        for (final CleanerDigitalObjectData cdod : this.cleanerDigitalObjectDatas) {
            cdod.closeQuietly();
        }
    }
}
