/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.scheduling
 *             FILE : ProcessingScheduler.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 13, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.scheduling;

import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.BatchJobProcessingService;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.BatchJobService;
import eu.europa.ec.opoce.cellar.logging.LogContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.BATCH_PROCESSING;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 13, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@LogContext
public class SparqlUpdateProcessingScheduler implements Runnable {

    /**
     * Constant <code>logger</code>
     */
    private static final Logger LOG = LogManager.getLogger(SparqlUpdateProcessingScheduler.class);

    /**
     * The batch job service.
     */
    private final BatchJobService batchJobService;

    /**
     * The batch job processing service.
     */
    private final BatchJobProcessingService batchJobProcessingService;

    /**
     * The cron configuration
     */
    private final String cron;

    /**
     * Constructs batch job scheduler.
     *
     * @param batchJobService           the batch job service used by the scheduler
     * @param batchJobProcessingService the batch job processing service used by the scheduler
     */
    public SparqlUpdateProcessingScheduler(final BatchJobService batchJobService, final BatchJobProcessingService batchJobProcessingService,
                                           final String cron) {
        this.batchJobService = batchJobService;
        this.batchJobProcessingService = batchJobProcessingService;
        this.cron = cron;
    }

    /**
     * Run the batch jobs.
     */
    @Override
    @LogContext(BATCH_PROCESSING)
    public void run() {
        final List<BatchJob> batchJobs = batchJobService.findByStatusAndCron(BatchJob.STATUS.AUTO, cron);
        if (batchJobs == null || batchJobs.isEmpty()) { // there is no job available
            LOG.info("No batch jobs to execute");
        } else {
            batchJobProcessingService.processBatchJobs(batchJobs); // executes the update batch job
            LOG.info("{} batches sent for processing", batchJobs.size());
            LOG.debug("Batches: {}", () -> batchJobs);
        }
    }
}
