/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.dao.datatable.impl
 *             FILE : HibernateBasedDatatableDaoImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 1 juil. 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao.datatable.impl;

import eu.europa.ec.opoce.cellar.cl.dao.datatable.DatatableDao;
import eu.europa.ec.opoce.cellar.cl.domain.datatable.DatatableColumn;
import eu.europa.ec.opoce.cellar.cl.domain.datatable.DatatableColumnsWrapper;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class HibernateBasedDatatableDaoImpl.
 *
 * @author ARHS Developments
 * @version $Revision$
 * @param <T>
 */
@Transactional(readOnly = true)
@Repository("jdbcBasedDatatableDao")
public class JdbcBasedDatatableDaoImpl extends NamedParameterJdbcDaoSupport implements DatatableDao {

    @Autowired
    @Qualifier("cellarDataSource")
    private DataSource dataSource;

    @PostConstruct
    private void initialize() {
        setDataSource(dataSource);
    }

    /** {@inheritDoc} */
    @Override
    public List<String[]> findAll(final DatatableColumnsWrapper columnsWrapper, final int startPage, final int pageSize,
            final RowMapper<String[]> rowMapper) {
        //CELLAR db is oracle 11g which only allows the use of ROWNUM for pagination
        //to reduce the response time we need to build the query with jdbc template
        //otherwise hibernate wraps the query and applies the pagination parameters in the outer query meaning that the inner query select all records
        // this must be avoided because the number of records is high - cellar-1469
        final DatatableColumn[] columns = columnsWrapper.getColumns();
        final String tableName = columnsWrapper.getTableName();

        final List<DatatableColumn> columnsArrayAsList = Arrays.asList(columns);
        final StringBuffer stringBuffer = new StringBuffer("SELECT ");
        Iterator<DatatableColumn> iterator = columnsArrayAsList.iterator();
        while (iterator.hasNext()) {
            final DatatableColumn next = iterator.next();
            stringBuffer.append(next.getName() + "_" + next.getIndex());
            if (iterator.hasNext()) {
                stringBuffer.append(",");
            }
        }
        stringBuffer.append(" FROM ( SELECT rownum rnum, ");
        iterator = columnsArrayAsList.iterator();
        while (iterator.hasNext()) {
            final DatatableColumn next = iterator.next();
            stringBuffer.append(next.getDbName());
            stringBuffer.append(" AS ");
            stringBuffer.append(next.getName() + "_" + next.getIndex());
            if (iterator.hasNext()) {
                stringBuffer.append(",");
            }
        }
        stringBuffer.append(" FROM ");
        stringBuffer.append(tableName);
        stringBuffer.append(" WHERE rownum <= ");
        stringBuffer.append(startPage + pageSize);

        final HashMap<String, Object> buildWhereClause = addWhereClauseToQuery(columns, stringBuffer, true);

        stringBuffer.append(" ) ");
        stringBuffer.append(" WHERE rnum >= ");
        stringBuffer.append(startPage + 1);

        final String sortingOrder = columnsWrapper.getSortOrder().toUpperCase();
        final DatatableColumn sortingColumn = columns[columnsWrapper.getSortByColumnIndex()];
        final String sortingColumnName = sortingColumn.getName();
        stringBuffer.append(" ORDER BY ");
        stringBuffer.append(sortingColumnName + "_" + sortingColumn.getIndex());
        stringBuffer.append(" ");
        stringBuffer.append(sortingOrder);

        return getNamedParameterJdbcTemplate().query(stringBuffer.toString(), buildWhereClause, rowMapper);

    }

    /** {@inheritDoc} */
    @Override
    public Integer count(final DatatableColumnsWrapper columnsWrapper) {
        final StringBuffer stringBuffer = getCountSelect(columnsWrapper);
        final HashMap<String, Object> namedParameters = addWhereClauseToQuery(columnsWrapper.getColumns(), stringBuffer, false);
        final int result = getNamedParameterJdbcTemplate().queryForObject(stringBuffer.toString(), namedParameters, Long.class).intValue();
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public Integer countAll(final DatatableColumnsWrapper columnsWrapper) {
        final StringBuffer stringBuffer = getCountSelect(columnsWrapper);
        final int result = getNamedParameterJdbcTemplate()
                .queryForObject(stringBuffer.toString(), new HashMap<String, Object>(), Long.class).intValue();
        return result;

    }

    /**
     * @param columnsWrapper
     * @return
     */
    private StringBuffer getCountSelect(final DatatableColumnsWrapper columnsWrapper) {
        final String tableName = columnsWrapper.getTableName();
        final StringBuffer stringBuffer = new StringBuffer("SELECT count(*) ");
        stringBuffer.append(" FROM ");
        stringBuffer.append(tableName);
        return stringBuffer;
    }

    /**
     * Builds the where clause.
     *
     * @param columns the columns
     * @param criteria the criteria
     */
    protected HashMap<String, Object> addWhereClauseToQuery(final DatatableColumn[] columns, final StringBuffer stringBuffer,
            boolean concatenate) {
        int count = concatenate ? 1 : 0;
        final HashMap<String, Object> result = new HashMap<String, Object>();
        for (final DatatableColumn column : columns) {
            if (column.isSearchable() && (column.getSearchValue() != null)) {
                if (count > 0) {
                    stringBuffer.append(" AND ");
                } else {
                    stringBuffer.append(" WHERE ");
                }
                stringBuffer.append(column.getDbName());
                stringBuffer.append(column.getComparisonType());
                stringBuffer.append(column.getName());
                result.put(column.getName(), column.getSearchValue());
                count++;
            }
        }
        return result;
    }

}
