/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.support.metadata.impl
 *             FILE : AppendBusinessMetadataBackupService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 17-05-2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.support.metadata.impl;

import eu.europa.ec.opoce.cellar.cl.domain.response.DigitalObjectOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.IOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.MetadataResponseOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.ResponseOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.ResponseOperationType;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;

/**
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 17-05-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class AppendBusinessMetadataBackupService extends AbstractBusinessMetadataBackupService {

    public AppendBusinessMetadataBackupService(ContentStreamService contentStreamService) {
        super(contentStreamService);
    }

    @Override
    public void backupBusinessMetadata(final StructMap structMap, final IOperation<DigitalObjectOperation> operation) {
        structMap.getDigitalObject().applyStrategyOnSelfAndChildren(digitalObject -> {
            DigitalObjectOperation digitalObjectOperation = operation.getOperation(digitalObject.getCellarId().getIdentifier());
            if (digitalObjectOperation == null) {
                digitalObjectOperation = new DigitalObjectOperation(digitalObject, operation.getOperationType());
            }

            // the default status of the object is: to be appended (that is, created)
            ResponseOperation response = new MetadataResponseOperation(digitalObject.getCellarId().getIdentifier(), "MD", ResponseOperationType.CREATE);
            response = setVersions(digitalObject.getCellarId(), reduce(digitalObject.getUpdatedTypes()), response);

            // if the object has no business metadata, it does mean that it is not involved in append: log it as to be kept unmodified
            if (digitalObject.getBusinessMetadata() == null) {
                response.setOperationType(ResponseOperationType.KEEP);
            }
            // else, the parent object's metadata are modified by this new child: log back the parent - if any - as to be updated
            else {
                final DigitalObject parent = digitalObject.getParentObject();
                if (parent != null) {
                    final DigitalObjectOperation parentOperation = operation.getOperation(parent.getCellarId().getIdentifier());
                    if (parentOperation != null && parentOperation.getMetadataIdentifierOperations().size() == 1) {
                        parentOperation.getMetadataIdentifierOperations().get(0).setOperationType(ResponseOperationType.UPDATE);
                    }
                }
            }

            digitalObjectOperation.addMetadataIdentifier((MetadataResponseOperation) response);
            if (operation.getOperation(digitalObject.getCellarId().getIdentifier()) == null) {
                operation.addOperation(digitalObjectOperation);
            }
        });
    }

}
