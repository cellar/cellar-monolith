package eu.europa.ec.opoce.cellar.cl.dao.impl;

import eu.europa.ec.opoce.cellar.cl.dao.StructMapPidDao;
import eu.europa.ec.opoce.cellar.cl.domain.structmap_pid.StructMapPid;
import eu.europa.ec.opoce.cellar.cl.domain.structmap_status_history.StructMapStatusHistory;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This DAO is responsible for managing the 'StructMap Pid' objects.
 *
 * @author EUROPEAN DYNAMICS S.A.
 */

@Repository
public class StructMapPidDaoImpl extends BaseDaoImpl<StructMapPid, Long> implements StructMapPidDao {
    
    /**
     * {@inheritDoc}
     */
    @Override
    public List<StructMapPid> getByStructMapStatusHistory(StructMapStatusHistory structMapStatusHistory) {
        Map<String, Object> params = new HashMap<>();
        params.put("spidStructMapStatusHistory", structMapStatusHistory);
        return super.findObjectsByNamedQueryWithNamedParams("StructMapPid.findByStructMapStatusHistory", params);
    }
}
