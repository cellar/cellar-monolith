package eu.europa.ec.opoce.cellar.cl.domain.response;

public class ContentResponseOperation extends ResponseOperation {

    /** class's uid. */
    private static final long serialVersionUID = 8326607516293707540L;

    private String cellarId;

    public ContentResponseOperation() {
    }

    public ContentResponseOperation(String cellarId, String id, ResponseOperationType operationType) {
        super(id, operationType);
        this.cellarId = cellarId;
    }

    public String getCellarId() {
        return cellarId;
    }

    public void setCellarId(String cellarId) {
        this.cellarId = cellarId;
    }

}
