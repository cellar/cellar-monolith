package eu.europa.ec.opoce.cellar.exception;

/**
 * This is a {@link MetsUploadException} buildable by an {@link ExceptionBuilder}.
 */
public class MetsUploadException extends CellarException {

    public MetsUploadException(ExceptionBuilder<? extends BuildableException> builder) {
        super(builder);
    }
}
