/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.client
 *             FILE : ExternalLinkService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 14, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.client;

import eu.europa.ec.opoce.cellar.cl.domain.externalLink.ExternalLink;
import eu.europa.ec.opoce.cellar.exception.ExternalLinkException;

/**
 * <class_description> External link generation manager.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 14, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface ExternalLinkService {

    /**
     * Returns the external link generated corresponding to the language and the document identifier.
     * @param id the document identifier
     * @param langIso2Char the language code (ISO 2 characters)
     * @return the external link generated
     * @throws ExternalLinkException if there is an error in the construction of the link
     */
    ExternalLink convert(final String id, final String langIso2Char) throws ExternalLinkException;
}
