/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.dao.impl
 *             FILE : GroupDaoImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 10, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao.impl;

import org.springframework.stereotype.Repository;

import eu.europa.ec.opoce.cellar.cl.dao.GroupDao;
import eu.europa.ec.opoce.cellar.cl.domain.admin.Group;

/**
 * <class_description> Implementation of the interface {@link GroupDao}.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 10, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Repository
public class GroupDaoImpl extends BaseDaoImpl<Group, Long> implements GroupDao {

}
