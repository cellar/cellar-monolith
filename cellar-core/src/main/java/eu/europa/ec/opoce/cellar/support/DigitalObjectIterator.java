/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.support
 *             FILE : DigitalObjectIterator.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 04-01-2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.support;

import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * <class_description> Iterates over a tree of digital objects.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 04-01-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class DigitalObjectIterator implements Iterator<DigitalObject>, Iterable<DigitalObject> {

    private List<DigitalObject> digitalObjectTreeList;
    private int index;

    public DigitalObjectIterator(DigitalObject digitalObject) {
        this.digitalObjectTreeList = new DigitalObjectTreeBuilder(digitalObject).build();
        this.index = 0;
    }

    public DigitalObjectIterator(StructMap structMap) {
        this(structMap.getDigitalObject());
    }

    @Override
    public boolean hasNext() {
        return this.index < this.digitalObjectTreeList.size();
    }

    @Override
    public DigitalObject next() {
        return this.digitalObjectTreeList.get(this.index++);
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Cannot delete on a digital object iterator");
    }

    @Override
    public Iterator<DigitalObject> iterator() {
        return this.digitalObjectTreeList.iterator();
    }

    /**
     * Builds a tree of digital objects as a list of elements.
     * @author phvdveld
     */
    private static final class DigitalObjectTreeBuilder {

        private DigitalObject rootElement;

        DigitalObjectTreeBuilder(DigitalObject rootElement) {
            this.rootElement = rootElement;
        }

        List<DigitalObject> build() {
            return buildTreeList(this.rootElement, new LinkedList<DigitalObject>());
        }

        /**
         * builds a tree of digital object by appending the node and its children to the provided tree list.
         * @param node
         * @param tree
         * @return
         */
        private List<DigitalObject> buildTreeList(DigitalObject node, List<DigitalObject> tree) {

            if (node != null) {
                tree.add(node);
                for (DigitalObject child : node.getChildObjects()) {
                    buildTreeList(child, tree);
                }
            }
            return tree;
        }
    }
}
