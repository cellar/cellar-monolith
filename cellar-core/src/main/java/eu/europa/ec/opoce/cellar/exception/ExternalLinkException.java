/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.exception
 *             FILE : ExternalLinkException.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 17, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.exception;

/**
 * <class_description> External link generation execption.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 17, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class ExternalLinkException extends CellarException {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Constructor.
     * @param builder
     */
    public ExternalLinkException(ExceptionBuilder<? extends BuildableException> builder) {
        super(builder);
    }

}
