/**
 *
 */
package eu.europa.ec.opoce.cellar.support;

import eu.europa.ec.opoce.cellar.server.admin.sparql.Query;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;

import javax.xml.bind.JAXBException;

import org.apache.commons.lang.StringUtils;

/**
 * The Class SparqlUtils.
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 20 sept. 2016
 *
 * @author dsavares
 */
public class SparqlUtils extends JaxbUtils {

    /** The Constant NAME_SPACE. */
    public static final String NAME_SPACE = "http://www.w3.org/2001/sw/DataAccess/rf1/result";

    /** The Constant CONTENT_TYPE_QUERY. */
    public static final String CONTENT_TYPE_QUERY = "text/plain";

    /**
     * Unmarshall.
     *
     * @param sparqlString the sparql string
     * @return the sparql
     * @throws JAXBException the JAXB exception
     */
    public static Sparql unmarshall(final String sparqlString) throws JAXBException {
        return unmarshall(sparqlString, Sparql.class);
    }

    /**
     * <p>getQueryFileFilter.</p>
     *
     * @return a {@link java.io.FileFilter} object.
     *
     */
    public static FileFilter getQueryFileFilter() {
        return new FileFilter() {

            @Override
            public boolean accept(final File file) {
                if (file.isFile()) {
                    final int delim = file.getName().lastIndexOf(".");
                    return file.getName().substring(delim + 1, file.getName().length()).equalsIgnoreCase("sparqlq");
                }
                return false;
            }
        };
    }

    /**
     * Gets the example queries from folder.
     *
     * @param absolutePathToQueries the absolute path to queries
     * @return the example queries
     */
    public static Query[] getQueries(final String absolutePathToQueries) {
        Query[] queries = new Query[1];
        final File queriesFolder = new File(absolutePathToQueries);
        if (queriesFolder.isDirectory()) {
            final File[] fileList = queriesFolder.listFiles(getQueryFileFilter());
            queries = new Query[fileList.length + 1];
            int i = 1;
            for (final File file : fileList) {
                final Query q = new Query(file.getName(), file.getName());
                queries[i] = q;
                i++;
            }
        }
        queries[0] = new Query("None", "");
        return queries;
    }

    /**
     * Gets the query.
     *
     * @param absolutePathToQueries the absolute path to queries
     * @param query the query
     * @return the query
     * @throws IOException Signals that an I/O exception has occurred.
     */
    public static String getQuery(final String absolutePathToQueries, final String query) throws IOException {
        if (StringUtils.isNotBlank(query)) {
            final FileInputStream stream = new FileInputStream(new File(absolutePathToQueries, query));
            try {
                final FileChannel fc = stream.getChannel();
                final MappedByteBuffer bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size());
                return Charset.defaultCharset().decode(bb).toString();
            } finally {
                stream.close();
            }
        }
        return "";
    }

}
