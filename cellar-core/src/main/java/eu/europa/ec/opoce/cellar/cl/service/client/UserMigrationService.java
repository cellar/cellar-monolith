/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.service.client
 *        FILE : UserMigrationService.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 15-03-2023
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2023 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.client;

/**
 * Provides access to functionality for associating an existing CELLAR user
 * with a pre-authenticated EU-Login user.
 * @author EUROPEAN DYNAMICS S.A
 */
public interface UserMigrationService {

	/**
	 * Performs migration of an existing CELLAR user by changing the related
	 * username in the database to match the user's EU-Login ID.
	 * @param newUsername the EU-Login ID to set the username to.
	 * @param currentUsername the current username; to be used for looking up
	 * the existing CELLAR user in the database.
	 * @param password the user's existing CELLAR password; to be matched with the existing user's
	 * password as stored in the database.
	 */
	void migrateUser(String newUsername, String currentUsername, String password);
	
}
