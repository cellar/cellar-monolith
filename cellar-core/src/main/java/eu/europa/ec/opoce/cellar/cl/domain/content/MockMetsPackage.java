/**
 * 
 */
package eu.europa.ec.opoce.cellar.cl.domain.content;

import java.io.InputStream;

/**
 * The MetsPackage.
 * <p>
 * A MetsPackage is an object able to provide the stream of the Mets file and the streams of the
 * files described inside it:
 * </p>
 * 
 * @author dsavares
 * 
 */
@Deprecated
public interface MockMetsPackage {

    /**
     * Gets the {@link InputStream} of the Mets file.
     * 
     * @return {@link InputStream} of the file.
     */
    public InputStream getMetsStream();

    /**
     * Gets the {@link InputStream} of a file described in the Mets file.
     * 
     * @param fileRef
     *            the reference to the file.
     * @return {@link InputStream} of the file.
     */
    public InputStream getFileStream(String fileRef);
}
