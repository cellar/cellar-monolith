/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ccr.domain.impl.update
 *             FILE : DeleteStructMapTransaction.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 11, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.domain.impl;

import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.ccr.helper.DigitalObjectHelper;
import eu.europa.ec.opoce.cellar.ccr.helper.ProductionIdentifierHelper;
import eu.europa.ec.opoce.cellar.cl.dao.ProductionIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.admin.ProductionIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.cl.domain.response.DeleteStructMapOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.StructMapOperation;
import eu.europa.ec.opoce.cellar.cl.service.client.CellarIdentifierService;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.MetaDataIngestionHelper;
import eu.europa.ec.opoce.cellar.cmr.MetaDataIngestionService;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.database.dao.InverseRelationDao;
import eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestGenerationService;
import eu.europa.ec.opoce.cellar.cmr.quad.ModelQuadService;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsPackage;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.StructMapProcessorException;
import eu.europa.ec.opoce.cellar.feed.domain.enums.FeedItemType;
import eu.europa.ec.opoce.cellar.feed.domain.enums.FeedPriority;
import eu.europa.ec.opoce.cellar.feed.domain.impl.ingestion.IngestionHistory;
import eu.europa.ec.opoce.cellar.feed.service.HistoryService;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.Lang;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static eu.europa.ec.opoce.cellar.cmr.ContentType.DIRECT_INFERRED;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 11, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component("structMapDelete")
public class DeleteStructMapTransaction extends WriteStructMapTransaction<CellarIdentifier> {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteStructMapTransaction.class);

    @Autowired
    @Qualifier("pidManagerService")
    private IdentifierService identifierService;

    @Autowired
    @Qualifier("pidManagerService")
    private PidManagerService pidManagerService;

    @Autowired
    private ProductionIdentifierDao productionIdentifierDao;

    @Autowired
    private MetaDataIngestionService<CalculatedData> metaDataIngestionService;

    @Autowired
    @Qualifier("modelQuadService")
    private ModelQuadService modelQuadService;

    @Autowired
    private CellarResourceDao cellarResourceDao;

    @Autowired
    private InverseRelationDao inverseRelationDao;

    @Autowired
    private CmrIndexRequestGenerationService cmrIndexRequestGenerationService;

    @Autowired
    private HistoryService historyService;

    @Autowired
    private MetaDataIngestionHelper metaDataIngestionHelper;

    @Autowired
    private CellarIdentifierService cellarIdentifierService;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    /**
     * Instantiates a new delete struct map transaction.
     */
    public DeleteStructMapTransaction() {
        // load existing direct model, and existing direct and inferred model from S3
        super(TransactionConfiguration.get(). //
                withLoadExistingDirectModel(true). //
                withLoadExistingDirectAndInferredModel(true) //
        );
    }

    /**
     * Provide the update of each digital object found under the given structmap (in depth first traversal).
     * The top digital object (WORK/DOSSIER/AGENT/TOPLEVELEVENT) MUST exist in the database.
     *
     * @param metsPackage    to find metadata associated for every digital object under the structMap.
     * @param calculatedData the calculated data
     * @param metsDirectory  a reference to the directory where the SIP has been exploded.
     * @param sipResource    the sipResource to audit this method.
     * @return the struct map operation
     */
    @Override
    public StructMapOperation executeWriteTreatment(final MetsPackage metsPackage, final CalculatedData calculatedData,
                                                    final File metsDirectory, final SIPResource sipResource) {
        final List<CellarIdentifier> hiddenContentStreamIdentifierList = new ArrayList<>();

        final DeleteStructMapOperation operation = new DeleteStructMapOperation();
        final StructMap structMap = calculatedData.getStructMap();
        operation.setId(structMap.getId());
        try {
            final DigitalObject rootDigitalObject = structMap.getDigitalObject();

            // A work/dossier/agent/toplevelevent element must be deleted with a special procedure, for all other deletions we use the common ingestion process
            if (rootDigitalObject.getChildObjects().size() == 0) {
                this.deleteDirectly(rootDigitalObject, calculatedData, operation);
            } else {
                // 1. Delete digital objects' hierarchy in CELLAR database and on CCR (= S3)
                this.deleteFromCCR(operation, rootDigitalObject);

                // 2. Delete corresponding metadata on CMR (= RDF store)
                this.metaDataIngestionService.ingest(calculatedData, operation);
            }

        } catch (final Exception exception) {
            // rollback the entire process for this structmap
            this.rollback(operation, calculatedData, exception, hiddenContentStreamIdentifierList);
        }

        // now, we are sure that we can physically delete the objects from CCR (= S3) (these objects have a type 'deleted')
        delete(operation);

        return operation;
    }

    /**
     * If we are deleting an work/dossier/agent/toplevelevent, we cannot use the common ingestion process as it relies on the structMap root element which is supposed to be deleted.
     * Thus, we need to delete the complete hierarchy "directly".
     *
     * @param rootDigitalObject the root digital object
     * @param calculatedData    the calculated data
     * @param operation         the operation
     */
    private void deleteDirectly(final DigitalObject rootDigitalObject, final CalculatedData calculatedData,
                                final DeleteStructMapOperation operation) {
        final ProductionIdentifier pid = ProductionIdentifierHelper.getProductionIdentifier(rootDigitalObject,
                this.productionIdentifierDao);
        DigitalObjectHelper.assignCellarIdentifier(rootDigitalObject, pid);
        final String rootDigitalObjectCellarId = rootDigitalObject.getCellarId().getIdentifier();

        // 1. Retrieve all digital object's identifiers before anything is lost due to deletion, for later use
        final List<Identifier> allIdentifiersToDelete = identifierService.getTreeIdentifiers(rootDigitalObjectCellarId, true);

        // 2. Delete digital objects' hierarchy from CCR layer (CELLAR database and S3)
        this.deleteFromCCR(operation, rootDigitalObject);

        // 3. Delete corresponding metadata from CMR layer (CMR database and RDF Store)
        this.deleteFromCMR(allIdentifiersToDelete, calculatedData.getMetsPackage().getSipType());

        // 4. Request the indexing (first: add work to removed digital objects in CalculatedData object - usually done at common ingestion process)
        final List<DigitalObject> onlyWorkDigitalObject = new ArrayList<>();
        onlyWorkDigitalObject.add(rootDigitalObject);
        calculatedData.setRemovedDigitalObjects(onlyWorkDigitalObject);
        final String version = rootDigitalObject.getVersions().get(DIRECT_INFERRED);
        try (InputStream is = contentStreamService.getContent(rootDigitalObjectCellarId, version, DIRECT_INFERRED).orElseThrow(() ->
                new IllegalStateException("Cannot found " + DIRECT_INFERRED + " " + rootDigitalObjectCellarId + "(" + version + ")"))) {
            final Model directAndInferredModel = JenaUtils.read(is, Lang.NT);
            cmrIndexRequestGenerationService.insertIndexingRequestsFromDeleteIngest(calculatedData, directAndInferredModel);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }

        // 5. Log the structMap action
        final StructMap structMap = rootDigitalObject.getStructMap();
        LOGGER.info("Created requests for indexing for structmap '{}'", structMap.getId());

        // 6. Delete from Virtuoso
        final List<String> allCellarIdToDelete = allIdentifiersToDelete.stream().map(Identifier::getCellarId).collect(Collectors.toList());
        this.metaDataIngestionHelper.deleteVirtuoso(allCellarIdToDelete);
    }

    /**
     * Delete from CCR layer (CELLAR database and S3).
     *
     * @param operation         the operation
     * @param currDigitalObject the curr digital object
     */
    private void deleteFromCCR(final DeleteStructMapOperation operation, final DigitalObject currDigitalObject) {
        final ProductionIdentifier pid = ProductionIdentifierHelper.getProductionIdentifier(currDigitalObject,
                this.productionIdentifierDao);
        ProductionIdentifierHelper.checkProductionIdentifierExists(pid, currDigitalObject);
        DigitalObjectHelper.assignCellarIdentifier(currDigitalObject, pid);

        // Only leave-elements are deleted, i.e.: delete all elements which have no child
        final List<DigitalObject> childObjects = currDigitalObject.getChildObjects();
        if (childObjects.size() == 0) {
            // If the current digital object or one of its parent digital objects is marked as read only the digital object cannot be deleted.
            if (cellarIdentifierService.isAnyParentReadOnly(currDigitalObject)
                    || cellarIdentifierService.isAnyChildReadOnly(currDigitalObject)) {
                throw ExceptionBuilder.get(StructMapProcessorException.class)
                        .withCode(CoreErrors.E_391)
                        .withMessage("Cannot delete {} - [{}|{}] as a parent,child element or itself is marked as read only.")
                        .withMessageArgs(currDigitalObject.getType().toString(),
                                currDigitalObject.getCellarId().getIdentifier(), currDigitalObject.getContentids())
                        .build();
            }
            DigitalObjectHelper.deleteDigitalObjectWithSubtree(currDigitalObject, operation, false, this.identifierService,
                    this.pidManagerService);
        } else {
            for (final DigitalObject childDigitalObject : childObjects) {
                this.deleteFromCCR(operation, childDigitalObject);
            }
        }
    }

    /**
     * Delete from CMR layer (CMR database and RDF Store).
     *
     * @param allIdentifiers identifiers to delete
     * @param sipType        the SIP type
     */
    private void deleteFromCMR(final List<Identifier> allIdentifiers, final SIPWork.TYPE sipType) {
        // Delete corresponding metadata on CMR (= RDF store) brutally
        this.modelQuadService.deleteContexts(allIdentifiers);

        // Delete from CMR_CELLAR_RESOURCE_MD
        allIdentifiers.stream()
                .map(i -> cellarResourceDao.findCellarId(i.getCellarId()))
                .filter(Objects::nonNull)
                .forEach(r -> {
                    cellarResourceDao.deleteObject(r);
                    IngestionHistory ingestionHistory = toHistory(r, sipType);
                    historyService.store(ingestionHistory);
                    applicationEventPublisher.publishEvent(ingestionHistory);
                });

        // Delete from CMR_INVERSE_RELATIONS
        final Collection<String> cellarIds = allIdentifiers.stream()
                .map(Identifier::getCellarId)
                .collect(Collectors.toList());
        this.inverseRelationDao.findRelationsByTargets(cellarIds)
                .forEach(inverseRelationDao::deleteObject);
    }

    private IngestionHistory toHistory(final CellarResource resource, final SIPWork.TYPE sipType) {
        final String cellarId = resource.getCellarId();
        final String classes = String.join(",", historyService.getOwlClassesForCellarId(resource.getCellarId(),
                resource.getVersion(DIRECT_INFERRED).orElse(null)));
        final String identifiers = String.join(",", historyService.getProductionIdentifierNamesForCellarId(cellarId));

        final IngestionHistory historyObject = new IngestionHistory(cellarId);

        if (resource.isUnderEmbargo()) {
            historyObject.setWemiClass(resource.getCellarType());
            historyObject.setRelatives(false);
            historyObject.setActionType(FeedItemType.delete_embargoed);
            historyObject.setFeedActionType(FeedItemType.no_feed);
            historyObject.setPriority(FeedPriority.fromSIPType(sipType));
            historyObject.setType(resource.getMimeType());
            historyObject.setClasses(classes);
            historyObject.setIdentifiers(identifiers);
        }else {
            historyObject.setWemiClass(resource.getCellarType());
            historyObject.setRelatives(false);
            historyObject.setActionType(FeedItemType.delete);
            historyObject.setFeedActionType(FeedItemType.delete);
            historyObject.setPriority(FeedPriority.fromSIPType(sipType));
            historyObject.setType(resource.getMimeType());
            historyObject.setClasses(classes);
            historyObject.setIdentifiers(identifiers);
        }

        return historyObject;
    }
}
