/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker
 *        FILE : PackageList.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 02-12-2021
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2021 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * Domain object representing the PACKAGE_LIST database view.
 * @author EUROPEAN DYNAMICS S.A.
 */
@Entity
@Table(name = "PACKAGE_LIST")
@NamedQueries({
	@NamedQuery(name = "PackageList.findAllOrdered", query = "SELECT pl FROM PackageList pl ORDER BY pl.ordering ASC")
})
public class PackageList {

	/**
	 * The ID of the package.
	 */
	@Id
	@Column(name = "SIP_ID", insertable = false, updatable = false, nullable = false)
	private Long sipId;
	/**
	 * The position of that the package with the corresponding ID has within the database queue.
	 */
	@Column(name = "ORDERING", insertable = false, updatable = false, nullable = false)
	private int ordering;
	
	
	public Long getSipId() {
		return sipId;
	}
	
	public int getOrdering() {
		return ordering;
	}
	
	public void setSipId(Long sipId) {
		this.sipId = sipId;
	}
	
	public void setOrdering(int ordering) {
		this.ordering = ordering;
	}
	
}
