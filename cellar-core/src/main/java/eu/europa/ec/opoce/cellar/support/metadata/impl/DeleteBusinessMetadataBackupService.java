/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.support.metadata.impl
 *             FILE : UpdateBusinessMetadataBackupService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 17-05-2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.support.metadata.impl;

import eu.europa.ec.opoce.cellar.cl.domain.response.DigitalObjectOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.IOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.MetadataResponseOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.ResponseOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.ResponseOperationType;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import org.apache.commons.lang.StringUtils;

/**
 * <class_description>
 * <br/><br/>
 * <notes>
 * <br/><br/>.
 *
 * @author ARHS Developments
 */
public class DeleteBusinessMetadataBackupService extends AbstractBusinessMetadataBackupService {

    public DeleteBusinessMetadataBackupService(ContentStreamService contentStreamService) {
        super(contentStreamService);
    }

    @Override
    public void backupBusinessMetadata(final StructMap structMap, final IOperation<DigitalObjectOperation> operation) {
        structMap.getDigitalObject().applyStrategyOnSelfAndChildren(digitalObject -> {
            final ContentIdentifier cellarId = digitalObject.getCellarId();
            final String identifier = cellarId.getIdentifier();
            DigitalObjectOperation digitalObjectOperation = operation.getOperation(identifier);
            if (digitalObjectOperation == null) {
                final ResponseOperationType operationType = operation.getOperationType();
                digitalObjectOperation = new DigitalObjectOperation(digitalObject, operationType);
            }

            ResponseOperation response = new MetadataResponseOperation(identifier, "MD", ResponseOperationType.DELETE);
            response = setVersions(cellarId, reduce(digitalObject.getUpdatedTypes()), response);
            final String lastVersion = response.getLastVersion();
            if (StringUtils.isNotBlank(lastVersion)) {
                response.setOperationType(ResponseOperationType.DELETE);
            }
            digitalObjectOperation.addMetadataIdentifier((MetadataResponseOperation) response);
            if (operation.getOperation(identifier) == null) {
                digitalObjectOperation.setForRollbackPurposes(true);
                operation.addOperation(digitalObjectOperation);
            }
        });
    }

}
