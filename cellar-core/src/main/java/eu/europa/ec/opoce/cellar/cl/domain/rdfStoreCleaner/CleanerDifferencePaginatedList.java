/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner
 *             FILE : CleanerDifferencePaginatedList.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 10, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner;

import eu.europa.ec.opoce.cellar.cl.configuration.enums.CellarServiceRDFStoreCleanerMode;
import eu.europa.ec.opoce.cellar.cl.domain.common.CellarPaginatedList;
import eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner.CleanerDifference.CmrTable;
import eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner.CleanerDifference.QueryType;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 10, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class CleanerDifferencePaginatedList extends CellarPaginatedList<CleanerDifference> {

    private CellarServiceRDFStoreCleanerMode operationType;

    private QueryType queryType;

    private CmrTable cmrTable;

    /**
     * @param objectsPerPage
     * @param pageNumber
     * @param sortCriterion
     * @param sortDir
     * @param filterValue
     */
    public CleanerDifferencePaginatedList(final Integer pageNumber, final String sortCriterion, final String sortDir, final String cellarId,
            final CellarServiceRDFStoreCleanerMode operationType, final QueryType queryType, final CmrTable cmrTable) {
        this(DEFAULT_OBJETS_PER_PAGE, pageNumber, sortCriterion, sortDir, cellarId, operationType, queryType, cmrTable);
    }

    /**
     * @param objectsPerPage
     * @param pageNumber
     * @param sortCriterion
     * @param sortDir
     * @param filterValue
     */
    public CleanerDifferencePaginatedList(final int objectsPerPage, final Integer pageNumber, final String sortCriterion,
            final String sortDir, final String cellarId, final CellarServiceRDFStoreCleanerMode operationType, final QueryType queryType,
            final CmrTable cmrTable) {
        super(objectsPerPage, pageNumber, sortCriterion, sortDir, cellarId);
        this.operationType = operationType;
        this.queryType = queryType;
        this.cmrTable = cmrTable;
    }

    /**
     * @return the operationType
     */
    public CellarServiceRDFStoreCleanerMode getOperationType() {
        return operationType;
    }

    /**
     * @param operationType the operationType to set
     */
    public void setOperationType(CellarServiceRDFStoreCleanerMode operationType) {
        this.operationType = operationType;
    }

    /**
     * @return the queryType
     */
    public QueryType getQueryType() {
        return queryType;
    }

    /**
     * @param queryType the queryType to set
     */
    public void setQueryType(QueryType queryType) {
        this.queryType = queryType;
    }

    /**
     * @return the cmrTable
     */
    public CmrTable getCmrTable() {
        return cmrTable;
    }

    /**
     * @param cmrTable the cmrTable to set
     */
    public void setCmrTable(CmrTable cmrTable) {
        this.cmrTable = cmrTable;
    }

}
