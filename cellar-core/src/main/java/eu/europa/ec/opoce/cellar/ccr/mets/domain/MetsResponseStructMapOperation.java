package eu.europa.ec.opoce.cellar.ccr.mets.domain;

import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.cl.domain.response.ReadStructMapOperation;

/**
 * Implementation of the {@link ReadStructMapOperation} that holds a reference to the create mets archive file.
 * @author phvdveld
 *
 */
public class MetsResponseStructMapOperation extends ReadStructMapOperation {

    private static final long serialVersionUID = 744305649708171844L;

    private SIPResource metsResponse;

    public SIPResource getMetsResponse() {
        return metsResponse;
    }

    public void setMetsResponse(SIPResource metsResponse) {
        this.metsResponse = metsResponse;
    }
}
