/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.ccr.service.impl
 *        FILE : AbstractMetsResponseService.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 19-06-2012
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.ccr.service.client.MetsResponseService;
import eu.europa.ec.opoce.cellar.ccr.velocity.VelocityMarshaller;
import eu.europa.ec.opoce.cellar.cl.dao.StructMapResponseDao;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.cl.domain.validation.MetadataValidationResult;
import eu.europa.ec.opoce.cellar.cl.domain.validator.SystemValidator;
import eu.europa.ec.opoce.cellar.cl.service.client.AuditTrailEventService;
import eu.europa.ec.opoce.cellar.cl.service.client.ValidationService;
import eu.europa.ec.opoce.cellar.common.util.ZipUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentStream;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsDocument;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.MetsException;
import eu.europa.ec.opoce.cellar.semantic.validation.ValidationResultCache;
import eu.europa.ec.opoce.cellar.support.FileExtensionUtils;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.ws.rs.core.MediaType;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Map;

/**
 * <class_description> Abstract implementation of the {@link MetsResponseService}.<br/>
 * Service to handle the creation of responses in xml mets format and generating SIPResource archives with the generated files.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 19-06-2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public abstract class AbstractMetsResponseService implements MetsResponseService {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractMetsResponseService.class);
    private static final String LOG_RESPONSE_EXTENSION = ".log";
    private static final String METS_VIEW_NAME = "mets_response.vm";
    private static final String ERRORSIP_PREFIX = "";
    private static final String SUCCESSSIP_PREFIX = "";
    private static final String METADATA_VALIDATION_RESULT_EXTENSION = ".validationResult.json";
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);

    private VelocityMarshaller metsMarshaller;
    private SystemValidator metsResponseValidator;
    private AuditTrailEventService auditTrailEventService;
    private ValidationService validationService;

    @Autowired
    public AbstractMetsResponseService(@Qualifier("metsMarshaller") VelocityMarshaller metsMarshaller,
                                       @Qualifier("metsResponseValidator") SystemValidator metsResponseValidator,
                                       AuditTrailEventService auditTrailEventService, ValidationService validationService) {
        this.metsMarshaller = metsMarshaller;
        this.metsResponseValidator = metsResponseValidator;
        this.auditTrailEventService = auditTrailEventService;
        this.validationService = validationService;
    }

    /**
     * Creates the xml mets response from the provided MetsDocument and creates the SIPResource to
     * be returned.
     *
     * @param metsDocument the metsDocument to parse.
     * @param error        to defined if the response should be create in the success folder or in the error
     *                     folder.
     * @return a SIPResource containing the mets and its attached files
     */
    @Override
    public SIPResource createResponse(final MetsDocument metsDocument, boolean error) {
        final File metsResponseFolder = getMetsFolder(metsDocument);

        // creates the response mets file into the mets response folder
        final File metsResponseFile = makeResponseFile(metsResponseFolder, metsDocument);

        // marshals the mets document
        metsMarshaller.marshal(METS_VIEW_NAME, metsDocument, metsResponseFile);

        // creates the SIP resource, within the response archive
        final SIPResource sipResource = makeSIPResource(metsDocument, metsResponseFile.getParentFile(), error);

        // validates
        try {
            validationService.executeSystemValidator(metsResponseFile, sipResource, this.metsResponseValidator, "metsValidator");
        } catch (final Exception e) {
            LOG.error(e.getMessage(), e);
        }

        // deletes the mets response folder
        try {
            FileUtils.deleteDirectory(metsResponseFolder);
        } catch (final IOException e) {
            throw ExceptionBuilder.get(MetsException.class).withCode(CoreErrors.E_015)
                    .withMessage("cannot delete " + metsResponseFolder.getPath()).withCause(e).build();
        }

        return sipResource;
    }

    /**
     * Makes the sip ressource containing the mets file and the attached data files.
     *
     * @param metsDocument the mets document
     * @param metsFolder   the mets folder
     * @param error        the error
     * @return the SIP resource
     */
    private SIPResource makeSIPResource(final MetsDocument metsDocument, File metsFolder, boolean error) {
        final SIPResource sip = new SIPResource();
        sip.setMetsID(metsDocument.getDocumentId());
        final File archiveFile = makeResponseArchive(metsDocument, metsFolder, error);
        sip.setFolderName(archiveFile.getParent());
        sip.setArchiveName(archiveFile.getName());

        return sip;
    }

    /**
     * Makes a zipped entry with all the mets response files.
     *
     * @param metsDocument the mets document
     * @param metsFolder   the mets folder
     * @param error        the error
     * @return the file
     */
    private File makeResponseArchive(final MetsDocument metsDocument, File metsFolder, boolean error) {
        final File archiveDir = new File(this.resolveResponseFolder(), error ? "error" : "success");
        archiveDir.mkdirs();

        final String prefix = error ? ERRORSIP_PREFIX : SUCCESSSIP_PREFIX;
        File archive = new File(archiveDir, prefix + metsDocument.getDocumentId() + FileExtensionUtils.ZIP);
        int index = 0;
        while (archive.exists()) {
            archive = new File(archiveDir, prefix + metsDocument.getDocumentId() + "." + (index++) + FileExtensionUtils.ZIP);
        }

        try {
            ZipUtils.zipFiles(archive, metsFolder);
        } catch (final IOException e) {
            throw ExceptionBuilder.get(MetsException.class).withCode(CoreErrors.E_015).withMessage("cannot compress " + archive.getPath())
                    .withCause(e).build();
        }
        return archive;
    }

    /**
     * Gets the file in which to unmarshall the mets response.
     *
     * @param metsFolder   the mets folder
     * @param metsDocument the mets document
     * @return the file
     */
    private File makeResponseFile(File metsFolder, MetsDocument metsDocument) {
        return new File(metsFolder, metsDocument.getDocumentId() + FileExtensionUtils.RESPONSE_METS);
    }

    /**
     * Create a StructMap entity that will contain the logs of the transaction in the different
     * languages and also the generated response.
     *
     * @param inMetsDocument the in mets document
     * @param sipResource    SIPResource instance with SIP metadata.
     * @return a modified mets document.
     */
    @Override
    public MetsDocument loadStructMapResponse(final MetsDocument inMetsDocument, final SIPResource sipResource) {
        final String metsDocumentId = inMetsDocument.getDocumentId();
        final File responseFile = this.resolveResponseDao().getResponseFile(metsDocumentId);
        final File metsFolder = getMetsFolder(inMetsDocument);

        try {
            FileUtils.moveFileToDirectory(responseFile, getMetsFolder(inMetsDocument), true);
        } catch (final IOException e) {
            LOG.error(e.getMessage());
        }

        final MetsDocument outMetsDocument = new MetsDocument();
        outMetsDocument.setDocumentId(metsDocumentId);
        outMetsDocument.setCreateDate(inMetsDocument.getCreateDate());
        outMetsDocument.setType(inMetsDocument.getType());
        outMetsDocument.setProfile(inMetsDocument.getProfile());
        outMetsDocument.setHeader(sipResource.getMetsHdr());

        final Map<String, StructMap> inStructMaps = inMetsDocument.getStructMaps();
        if (inStructMaps.isEmpty()) {
            final StructMap outStructMap = new StructMap(outMetsDocument);
            outStructMap.setId("SM-response-" + outMetsDocument.getDocumentId());
            outStructMap.setDigitalObject(new DigitalObject(outStructMap));
            outMetsDocument.addStructMap(outStructMap);
        } else {
            inStructMaps.forEach((key, inStructMap) -> {
                final StructMap outStructMap = new StructMap(outMetsDocument);
                final DigitalObject outDigitalObject = new DigitalObject(outStructMap);

                outStructMap.setDigitalObject(outDigitalObject);
                outStructMap.setId(inStructMap.getId());
                outDigitalObject.setContentids(inStructMap.getDigitalObject().getContentids());

                addValidationFiles(metsFolder, inStructMap, outStructMap);

                outMetsDocument.addStructMap(outStructMap);
            });
        }

        final DigitalObject firstOutDigitalObject = outMetsDocument.getStructMaps().values().iterator().next().getDigitalObject();
        firstOutDigitalObject.addContentStream(new ContentStream(MediaType.APPLICATION_XML, responseFile.getName()));
        final File logFile = createLogFile(inMetsDocument);
        firstOutDigitalObject.addContentStream(new ContentStream(MediaType.TEXT_PLAIN, logFile.getName()));

        return outMetsDocument;
    }

    private static void addValidationFiles(final File metsFolder, final StructMap inStructMap, final StructMap outStructMap) {
        try {
            int idx = 1;
            for (MetadataValidationResult result : ValidationResultCache.get(inStructMap)) {
                if (!result.isIgnored()) {
                    final String validationInfoFilename = inStructMap.getId() + "_" + (idx++) + METADATA_VALIDATION_RESULT_EXTENSION;
                    final File validationInfoFile = createValidationInfoFile(metsFolder, validationInfoFilename, result);
                    outStructMap.getDigitalObject().addContentStream(new ContentStream(MediaType.APPLICATION_JSON, validationInfoFile.getName()));
                }
            }
        } finally {
            ValidationResultCache.remove(inStructMap);
        }
    }

    private static File createValidationInfoFile(final File metsFolder, final String filename, final MetadataValidationResult validationResult) {
        final File validationInfoFile = new File(metsFolder, filename);
        try {
            OBJECT_MAPPER.writeValue(validationInfoFile, validationResult);
        } catch (IOException e) {
            LOG.warn("Failed to write validation info ({}) to Mets response: {}", filename, e.getMessage());
        }

        return validationInfoFile;
    }

    /**
     * Extract all audit logs, having the document identifier (from the given metsDocument) as attribute
     * and in the given language.
     *
     * @param metsDocument to retrieve the document identifier.
     * @return a File that contains all logs.
     */
    private File createLogFile(final MetsDocument metsDocument) {
        final File logFile = new File(getMetsFolder(metsDocument), "log_en" + LOG_RESPONSE_EXTENSION);
        FileWriter fw = null;
        try {
            final StringBuffer content = this.auditTrailEventService.getXMLLogs();
            fw = new FileWriter(logFile);
            fw.write(content.toString());
        } catch (final IOException e) {
            LOG.error(e.getMessage());
            return null;
        } finally {
            if (fw != null) {
                try {
                    fw.close();
                } catch (final IOException e) {
                    LOG.error("Failed closing writer", e);
                }
            }
        }

        return logFile;
    }

    /**
     * Gets the folder in which the mets files for the response will be created.
     * If the folder does not exist, it is created.
     *
     * @param metsDocument to retrieve the mets document identifier.
     * @return a File that point to the response folder.
     */
    @Override
    public File getMetsFolder(MetsDocument metsDocument) {
        final File metsFolder = new File(this.resolveResponseFolder(), metsDocument.getDocumentId());
        if (!metsFolder.exists()) {
            metsFolder.mkdir();
        }
        return metsFolder;
    }

    /**
     * Resolve response folder.
     *
     * @return the file
     */
    protected abstract File resolveResponseFolder();

    /**
     * Resolve response dao.
     *
     * @return the struct map response dao
     */
    protected abstract StructMapResponseDao resolveResponseDao();
}
