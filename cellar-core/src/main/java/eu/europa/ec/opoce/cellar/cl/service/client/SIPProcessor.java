package eu.europa.ec.opoce.cellar.cl.service.client;

/**
 * The SIP Processor is a service of the CELLAR system that is responsible for treat a sip file.
 *  
 * @author dcraeye
 *
 */
public interface SIPProcessor extends Runnable {

    void run();

}
