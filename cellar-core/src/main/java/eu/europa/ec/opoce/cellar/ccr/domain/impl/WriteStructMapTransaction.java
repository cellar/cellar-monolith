/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.ccr.domain.impl
 *        FILE : WriteStructMapTransaction.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 08-05-2013
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.domain.impl;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.ccr.helper.ProductionIdentifierHelper;
import eu.europa.ec.opoce.cellar.ccr.s3.metadata.ExistingMetadataLoader.Repository;
import eu.europa.ec.opoce.cellar.cl.concurrency.Concurrent;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration;
import eu.europa.ec.opoce.cellar.cl.dao.ProductionIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.domain.admin.ProductionIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventProcess;
import eu.europa.ec.opoce.cellar.cl.domain.audit.Auditable;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.cl.domain.enums.ProcessStatus;
import eu.europa.ec.opoce.cellar.cl.domain.response.DigitalObjectOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.ResponseOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.ResponseOperationType;
import eu.europa.ec.opoce.cellar.cl.domain.response.StructMapOperation;
import eu.europa.ec.opoce.cellar.cl.domain.structmap_status_history.StructMapStatusHistory;
import eu.europa.ec.opoce.cellar.cl.service.client.StructMapStatusHistoryService;
import eu.europa.ec.opoce.cellar.cmr.DataStreamWrapper;
import eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData;
import eu.europa.ec.opoce.cellar.common.closure.ClosureException;
import eu.europa.ec.opoce.cellar.common.metadata.IExistingMetadataLoader;
import eu.europa.ec.opoce.cellar.database.transaction.RDFTransactional;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsPackage;
import eu.europa.ec.opoce.cellar.domain.content.mets.OperationSubType;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.CellarValidationException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.StructMapProcessorException;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

import javax.annotation.Resource;
import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * <class_description> This class provides useful methods for create/append/update process at the StructMap level.
 * Standard behaviors like the rollback (Database/S3), content's version, metadata managing are defined here.
 * The developer need only to subclass this abstract class in the specific create/append/update classes.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 08-05-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 * @param <R> the generic type
 */
public abstract class WriteStructMapTransaction<R> extends BaseStructMapTransaction {

    private static final Logger LOG = LogManager.getLogger(WriteStructMapTransaction.class);

    private @Autowired ProductionIdentifierDao productionIdentifierDao;

    @Resource(name = "existingMetadataLoader")
    protected IExistingMetadataLoader<String, Map<String, Model>, DataStreamWrapper> existingMetadataLoader;

    @Autowired
    @Qualifier("cellarConfiguration")
    protected ICellarConfiguration cellarConfiguration;

    private TransactionConfiguration configuration;

    @Autowired
    protected ContentStreamService contentStreamService;
    
    @Autowired
    private StructMapStatusHistoryService structMapStatusHistoryService;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    /**
     * Instantiates a new write struct map transaction.
     *
     * @param configuration the configuration
     */
    protected WriteStructMapTransaction(final TransactionConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     * Execute treatment.
     *
     * @param metsPackage the mets package
     * @param structMap the struct map
     * @param metsDirectory the mets directory
     * @param sipResource the sip resource
     * @return the struct map operation
     * @see eu.europa.ec.opoce.cellar.domain.content.mets.StructMapTransaction#executeTreatment(eu.europa.ec.opoce.cellar.domain.content.mets.MetsPackage,
     *      eu.europa.ec.opoce.cellar.domain.content.mets.StructMap, java.io.File, eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource,
     *      eu.europa.ec.opoce.cellar.cl.domain.structmap_status_history.StructMapStatusHistory)
     */
    @Override
    @Auditable(process = AuditTrailEventProcess.Ingestion)
    @Concurrent
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = StructMapProcessorException.class)
    @RDFTransactional(rollbackFor = StructMapProcessorException.class)
    public StructMapOperation executeTreatment(final MetsPackage metsPackage, final StructMap structMap, final File metsDirectory,
                                               final SIPResource sipResource, StructMapStatusHistory structMapStatusHistory) {
        
        // in case it is needed, populate with the existing metadata
        final ProductionIdentifier rootPid = ProductionIdentifierHelper.getProductionIdentifier(structMap.getDigitalObject(),
                this.productionIdentifierDao);
        
        // if the operation is a DELETE, ensure that it is targeting an existing resource
        final ExceptionBuilder<CellarException> exceptionBuilder = ExceptionBuilder.get(CellarException.class);
        if (OperationSubType.Delete.equals(structMap.resolveOperationSubType()) && (null == rootPid)) {
            if (structMapStatusHistory != null) {
                this.structMapStatusHistoryService.updateStructMapIngestionStatus(structMapStatusHistory, ProcessStatus.F);
            }
            throw exceptionBuilder.withMessage("Cannot delete an nonexistent resource").withCode(CommonErrors.TARGET_RESOURCE_DOES_NOT_EXIST)
                    .build();
        }
        final String rootCellarId = (rootPid != null ? rootPid.getCellarIdentifier().getUuid() : null);
        // Update CELLAR_ID field of STRUCTMAP_STATUS_HISTORY, in case it is null
        if ((structMapStatusHistory != null) && (structMapStatusHistory.getCellarId() == null) && (rootCellarId != null)){
            structMapStatusHistory = this.structMapStatusHistoryService.updateStructMapCellarId(structMapStatusHistory, rootCellarId);
        }
        
        final CalculatedData calculatedData = new CalculatedData(metsPackage, structMap, rootCellarId, structMapStatusHistory, sipResource.getPackageHistory());
        this.populateWithExistingMetadata(calculatedData);

        // execute the writing treatment
        StructMapOperation structMapOperation = executeWriteTreatment(metsPackage, calculatedData, metsDirectory, sipResource);
        // NOTICE: This event is published AFTER the 'executeWriteTreatment' method (i.e. ingestion process), since it is only used for
        // the AFTER_COMMIT phase of the current transaction. This is also attributed to the need for the latest version of the
        // 'structMapStatusHistory' object included in the 'calculatedData' object after the ingestion process (see 'afterCommit' method below).
        applicationEventPublisher.publishEvent(new WriteStructMapEvent(sipResource, structMap, calculatedData.getStructMapStatusHistory()));
        return structMapOperation;
    }

    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
    public void afterCommit(WriteStructMapEvent event) {
        LOG.debug(IConfiguration.INGESTION, "Transaction committed for {}", event.getSipResource());
        
        // Update STRUCTMAP_STATUS_HISTORY entry
        if (event.getStructMapStatusHistory() != null){
            // Set INGESTION_STATUS of STRUCTMAP_STATUS_HISTORY entry to SUCCESS
            StructMapStatusHistory structMapStatusHistory = this.structMapStatusHistoryService.updateStructMapIngestionStatus(event.getStructMapStatusHistory(), ProcessStatus.S);
            // Update the CELLAR_ID of STRUCTMAP_STATUS_HISTORY entry, in case it is still null
            if(structMapStatusHistory.getCellarId() == null){
                ProductionIdentifier rootPid = ProductionIdentifierHelper.getProductionIdentifier(event.getStructMap().getDigitalObject(),
                        this.productionIdentifierDao);
                final String rootCellarId = (rootPid != null ? rootPid.getCellarIdentifier().getUuid() : null);
                this.structMapStatusHistoryService.updateStructMapCellarId(structMapStatusHistory, rootCellarId);
            }
        }
        
    }

    /**
     * Execute the writing treatment.
     *
     * @param metsPackage the mets package
     * @param calculatedData the calculated data
     * @param metsDirectory the mets directory
     * @param sipResource the sip resource
     * @return the struct map operation
     */
    protected abstract StructMapOperation executeWriteTreatment(final MetsPackage metsPackage, final CalculatedData calculatedData,
            final File metsDirectory, final SIPResource sipResource);

    /**
     * Perform a correct rollback on S3.
     * If the {@link CellarException} that originally caused the rollback is not silent, throw the original - in case wrapped - exception,
     * in order to eventually trigger the rollback on the database as well.
     *
     * @param operation the {@link StructMapOperation} object to cycle for getting rollback's information
     * @param calculatedData the {@link CalculatedData} object that contains current structmap and package information
     * @param exception the exception that originally caused the rollback
     * @param objectsToRollback the digital objects to rollback
     * @throws CellarException the cellar exception
     */
    protected final void rollback(final StructMapOperation operation, final CalculatedData calculatedData,
                                  final Throwable exception, final List<R> objectsToRollback) throws CellarException {
        
        if (calculatedData.getStructMapStatusHistory() != null){
            // Reset STRUCTMAP_STATUS_ENTRY due to FAILED ingestion
            this.structMapStatusHistoryService.resetStructMapAfterFailedIngestion(calculatedData.getStructMapStatusHistory());
        }
        
        Throwable originalExc = exception;
        if (originalExc instanceof ClosureException) {
            originalExc = originalExc.getCause();
        }

        CellarException wrappedExc;
        if (originalExc instanceof CellarValidationException) {
            wrappedExc = (CellarValidationException) originalExc;
        } else {
            wrappedExc = ExceptionBuilder.get(StructMapProcessorException.class)
                    .withCode(CoreErrors.E_040)
                    .withMessage("Ingestion failed")
                    .withCause(originalExc)
                    .build();
        }

        // rollback and throw here only if the exception is not silent
        if (!wrappedExc.isSilent()) {
            // throw the exception
            throw wrappedExc;
        }
    }

    /**
     * Gets the existing metadata loader.
     *
     * @return the existing metadata loader
     */
    protected IExistingMetadataLoader<String, Map<String, Model>, DataStreamWrapper> getExistingMetadataLoader() {
        return this.existingMetadataLoader;
    }

    /**
     * Populate the given 'calculatedData' with possible existing metadata of the root digital object, then return the production id of it.< /br>
     * By default, load all: existing direct model, existing direct and inferred model and existing inverse model
     *
     * @param calculatedData the object to populate
     * @return the productionId of the root object
     */
    private void populateWithExistingMetadata(final CalculatedData calculatedData) {
        final String rootCellarId = calculatedData.getRootCellarId();
        if (StringUtils.isBlank(rootCellarId)) {
            return;
        }

        // load existing direct model only if needed by the underlying transaction
        if (this.configuration.isLoadExistingDirectModel()) {
            final Map<String, Model> existingDirectModels = this.getExistingMetadataLoader().loadDirect(calculatedData.getRootCellarId());
            calculatedData.setExistingDirectModel(existingDirectModels);
        }

        // load existing direct and inferred model only if needed by the underlying transaction
        if (this.configuration.isLoadExistingDirectAndInferredModel()) {
            // if ingestion optimization is disabled load it from S3, otherwise from RDF Store
            Repository repository = Repository.S3;
            if (this.cellarConfiguration.isCellarDatabaseRdfIngestionOptimizationEnabled()) {
                repository = Repository.rdfStore;
            }

            // load
            final Map<String, Model> existingDirectAndInferredModels = this.getExistingMetadataLoader()
                    .loadDirectAndInferred(calculatedData.getRootCellarId(), repository);
            calculatedData.setExistingDirectAndInferredModel(existingDirectAndInferredModels);
        }

        // load existing inverse model only if ingestion optimization is enabled
        if (this.cellarConfiguration.isCellarDatabaseRdfIngestionOptimizationEnabled()) {
            final Map<String, Model> existingInverseModels = this.getExistingMetadataLoader().loadInverse(calculatedData.getRootCellarId());
            calculatedData.setExistingInverseModel(existingInverseModels);
        }
    }

    protected void delete(final StructMapOperation operation) {
        for (DigitalObjectOperation op : operation.getOperations().values()) {
            if (!op.isForRollbackPurposes()) {
                final ResponseOperation responseOp = op.getCellarIdentifierOperation();
                if (responseOp.getOperationType() == ResponseOperationType.DELETE) {
                    LOG.debug(IConfiguration.INGESTION, "Mark [{}] as deleted from S3.", responseOp.getId());
                    contentStreamService.deleteRecursively(responseOp.getId());
                }
            }
        }
    }


}
