/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *        FILE : ObjectNatureUnawarePidManagerService.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 16-03-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService;

import org.springframework.stereotype.Service;

/**
 * <class_description> Implementation of the {@link PidManagerService}. <br>
 * It is the same as {@link PidManagerService}, except that it implements a mock version of methods:<br>
 * - <code>boolean isWork(final String pid)</code><br>
 * - <code>boolean isExpressionOrEvent(final String pid)</code><br>
 * - <code>boolean isExpressionOrEvent(final String pid)</code><br>
 * - <code>boolean isManifestation(final String pid)</code><br>
 * - <code>boolean isManifestationContent(final String pid)</code><br>
 * <br>
 * These methods always return true, that means it does not check the nature of the objects.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 16-03-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
@Service("objectNatureUnawarePidManagerService")
public class ObjectNatureUnawarePidManagerService extends PidManagerServiceImpl {

    @Override
    public boolean isWork(final String pid) {
        return true;
    }

    @Override
    public boolean isExpressionOrEvent(final String pid) {
        return true;
    }

    @Override
    public boolean isManifestation(final String pid) {
        return true;
    }

    @Override
    public boolean isManifestationContent(final String pid) {
        return true;
    }

}
