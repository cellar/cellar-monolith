/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *             FILE : ExternalLinkCacheServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 14, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import eu.europa.ec.opoce.cellar.cl.dao.ExternalLinkPatternDao;
import eu.europa.ec.opoce.cellar.cl.domain.externalLink.ExternalLinkPattern;
import eu.europa.ec.opoce.cellar.cl.service.client.ExternalLinkPatternCacheService;
import eu.europa.ec.opoce.cellar.common.Placeholder;

/**
 * <class_description> External link patterns cache manager implementation.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 14, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class ExternalLinkPatternCacheServiceImpl implements ExternalLinkPatternCacheService {

    private final static String PLACEHOLDER_REGEX = "\\$\\{.*?\\}\\$";
    private final static Pattern PLACEHOLDER_PATTERN;

    /**
     * The external link pattern DAO.
     */
    @Autowired
    private ExternalLinkPatternDao externalLinkPatternDao;

    /**
     * Static constructor.
     */
    static {
        PLACEHOLDER_PATTERN = Pattern.compile(PLACEHOLDER_REGEX, Pattern.DOTALL);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Cacheable("externalLinkList")
    public List<ExternalLinkPattern> getExternalLinkList() {
        final List<ExternalLinkPattern> externalLinkPatterns = this.externalLinkPatternDao.getExternalLinkPatterns();

        // construct the pattern object
        for (ExternalLinkPattern elp : externalLinkPatterns)
            elp.constructMatchPatternObject();

        return externalLinkPatterns;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Cacheable(value = "externalLinkPlaceholders", key = "#externalLink.id")
    public LinkedList<Placeholder> getExternalLinkPlaceholders(final ExternalLinkPattern externalLink) {
        final Matcher matcher = PLACEHOLDER_PATTERN.matcher(externalLink.getUrlPattern());

        final LinkedList<Placeholder> placeholders = new LinkedList<Placeholder>();

        // extract the placeholders
        while (matcher.find()) {
            placeholders.add(new Placeholder(matcher.start(), matcher.end(), matcher.group()));
        }

        return placeholders;
    }

    /**
     * Sets the external link pattern DAO.
     * @param externalLinkPatternDao the DAO to set
     */
    public void setExternalLinkPatternDao(final ExternalLinkPatternDao externalLinkPatternDao) {
        this.externalLinkPatternDao = externalLinkPatternDao;
    }
}
