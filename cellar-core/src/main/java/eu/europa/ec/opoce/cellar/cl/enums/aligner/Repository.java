/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.aligner.enums
 *             FILE : Repository.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : May 21, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.enums.aligner;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : May 21, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public enum Repository {
    S3, RDF_STORE, CELLAR_IDENTIFIER, CMR_CELLAR_RESOURCE_MD, CMR_INVERSE_DISS;
}
