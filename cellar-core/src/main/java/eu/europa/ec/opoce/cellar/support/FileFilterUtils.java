package eu.europa.ec.opoce.cellar.support;

import java.io.FileFilter;

import org.apache.commons.io.filefilter.SuffixFileFilter;

public abstract class FileFilterUtils {

    public static final FileFilter SIP_FILTER = new SuffixFileFilter(FileExtensionUtils.SIP);
    public static final FileFilter METS_FILTER = new SuffixFileFilter(FileExtensionUtils.METS);
    public static final FileFilter FOXML_FILTER = new SuffixFileFilter(FileExtensionUtils.FOXML);
}
