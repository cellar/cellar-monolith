/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ccr.s3.metadata
 *             FILE : ExistingMetadataLoader.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Aug 20, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.s3.metadata;

import eu.europa.ec.opoce.cellar.cl.configuration.IS3Configuration;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.common.metadata.IExistingMetadataLoader;
import eu.europa.ec.opoce.cellar.common.util.Pair;
import org.apache.jena.graph.Triple;
import org.apache.jena.rdf.model.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Aug 20, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service("existingMetadataLoader")
public class ExistingMetadataLoader implements IExistingMetadataLoader<String, Map<?, ?>, Map<ContentType, String>> {

    @Autowired
    @Qualifier("cellarConfiguration")
    private IS3Configuration s3Configuration;

    @Autowired
    @Qualifier("cmrBasedExistingMetadataLoader")
    private IExistingMetadataLoader<String, Map<Pair<Triple, String>, String>, Map<ContentType, String>> cmrBasedExistingMetadataLoader;

    @Autowired
    @Qualifier("cachedS3BasedExistingMetadataLoader")
    private IExistingMetadataLoader<String, Map<String, Model>, Map<ContentType, String>> cachedS3BasedExistingMetadataLoader;

    @Autowired
    @Qualifier("uncachedS3BasedExistingMetadataLoader")
    private IExistingMetadataLoader<String, Map<String, Model>, Map<ContentType, String>> uncachedS3BasedExistingMetadataLoader;

    /**
     * @see eu.europa.ec.opoce.cellar.common.metadata.IExistingMetadataLoader#restart()
     */
    @Override
    public void restart() {
        this.resolveS3ExistingMetadataLoader().restart();
        this.cmrBasedExistingMetadataLoader.restart();
    }

    /**
     * @see eu.europa.ec.opoce.cellar.common.metadata.IExistingMetadataLoader#loadDirect(java.lang.Object)
     */
    @Override
    public Map<?, ?> loadDirect(final String in) {
        return this.resolveS3ExistingMetadataLoader().loadDirect(in);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.common.metadata.IExistingMetadataLoader#loadDirectAndInferred(java.lang.Object, java.lang.Object[])
     */
    @Override
    public Map<?, ?> loadDirectAndInferred(final String in, final Object... options) {
        if (options != null && options.length == 1 && options[0] instanceof Repository && options[0] == Repository.S3) {
            return this.resolveS3ExistingMetadataLoader().loadDirectAndInferred(in);
        } else {
            return this.cmrBasedExistingMetadataLoader.loadDirectAndInferred(in);
        }
    }

    /**
     * @see eu.europa.ec.opoce.cellar.common.metadata.IExistingMetadataLoader#loadInverse(java.lang.Object)
     */
    @Override
    public Map<?, ?> loadInverse(final String in) {
        return this.cmrBasedExistingMetadataLoader.loadInverse(in);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.common.metadata.IExistingMetadataLoader#updateCache(Object, Object)
     */
    @Override
    public void updateCache(final String cellarId, final Map<ContentType, String> cacheData) {
        this.resolveS3ExistingMetadataLoader().updateCache(cellarId, cacheData);
        this.cmrBasedExistingMetadataLoader.updateCache(cellarId, cacheData);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.common.metadata.IExistingMetadataLoader#getCache(Object)
     */
    @Override
    public Map<ContentType, String> getCache(final String cellarId) {
        return this.resolveS3ExistingMetadataLoader().getCache(cellarId);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.common.metadata.IExistingMetadataLoader#evictCache(Object)
     */
    @Override
    public void evictCache(final String cellarId) {
        this.resolveS3ExistingMetadataLoader().evictCache(cellarId);
        this.cmrBasedExistingMetadataLoader.evictCache(cellarId);
    }

    public enum Repository {
        S3, rdfStore
    }

    private IExistingMetadataLoader<String, Map<String, Model>, Map<ContentType, String>> resolveS3ExistingMetadataLoader() {
        return this.s3Configuration.isS3ServiceIngestionCacheEnabled() ?
                this.cachedS3BasedExistingMetadataLoader : this.uncachedS3BasedExistingMetadataLoader;
    }

}
