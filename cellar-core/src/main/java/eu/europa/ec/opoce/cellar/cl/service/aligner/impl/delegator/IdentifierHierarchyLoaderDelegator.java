/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.aligner.impl.delegator
 *             FILE : IdentifierHierarchyLoaderDelegator.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : May 27, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.aligner.impl.delegator;

import eu.europa.ec.opoce.cellar.cl.dao.CellarIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.aligner.HierarchyNode;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;

import java.util.Collection;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : May 27, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class IdentifierHierarchyLoaderDelegator implements IHierarchyLoaderDelegator<CellarIdentifier> {

    private final CellarIdentifierDao cellarIdentifierDao;

    public IdentifierHierarchyLoaderDelegator(final CellarIdentifierDao cellarIdentifierDao) {
        this.cellarIdentifierDao = cellarIdentifierDao;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.aligner.impl.delegator.IHierarchyLoaderDelegator#loadResources()
     */
    @Override
    public Collection<CellarIdentifier> loadResources(final String key) {
        return this.cellarIdentifierDao.getChildrenPids(key);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.aligner.impl.delegator.IHierarchyLoaderDelegator#getCellarId(java.lang.Object)
     */
    @Override
    public String getCellarId(final CellarIdentifier obj) {
        return obj.getUuid();
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.aligner.impl.delegator.IHierarchyLoaderDelegator#getDigitalObjectType(java.lang.Object)
     */
    @Override
    public DigitalObjectType getDigitalObjectType(final CellarIdentifier obj) {
        return null;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.aligner.impl.delegator.IHierarchyLoaderDelegator#select(java.lang.Object)
     */
    @Override
    public void select(final HierarchyNode node, final CellarIdentifier obj) {

    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.aligner.impl.delegator.IHierarchyLoaderDelegator#getSelectedResources()
     */
    @Override
    public Collection<CellarIdentifier> getSelectedResources() {
        return null;
    }
}
