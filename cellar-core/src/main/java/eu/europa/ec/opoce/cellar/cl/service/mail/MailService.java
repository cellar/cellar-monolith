package eu.europa.ec.opoce.cellar.cl.service.mail;

import eu.europa.ec.opoce.cellar.support.mail.EmailTemplate;

import java.util.Locale;
import java.util.Map;

public interface MailService {

    void createEmail(String to, Locale locale, EmailTemplate template, Map<String, Object> model);

}
