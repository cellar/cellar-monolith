/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.datatable
 *             FILE : AbstractDatatableMapper.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 1 juil. 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.datatable;

import eu.europa.ec.opoce.cellar.cl.dao.datatable.DatatableDao;
import eu.europa.ec.opoce.cellar.cl.domain.datatable.DataTableBodyResponse;
import eu.europa.ec.opoce.cellar.cl.domain.datatable.DatatableColumn;
import eu.europa.ec.opoce.cellar.cl.domain.datatable.DatatableColumnsWrapper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.jdbc.core.RowMapper;

/**
 * The Class AbstractDatatableMapper.
 * <class_description> used to map an object to be used by the server side paginated datatable.
 * <br/><br/>
 * <notes> .
 * <br/><br/>
 * ON : 1 juil. 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Configurable
public abstract class AbstractDatatableMapper {

    /** The Constant PAGE_SIZE. */
    private static final int PAGE_SIZE = 25;

    /**
     * Gets the datatable dao.
     *
     * @return the datatable dao
     */
    protected abstract DatatableDao getDatatableDao();

    /**
     * Gets the data table body response.
     *
     * @param parametersMap the parameters map
     * @return the data table body response
     */
    @SuppressWarnings({
            "unchecked", "rawtypes"})
    public DataTableBodyResponse getDataTableBodyResponse(final Map<String, String[]> parametersMap) {
        final DataTableBodyResponse result = new DataTableBodyResponse();
        final List<DatatableColumn> columns = new ArrayList<DatatableColumn>();
        final String[] rowNames = this.getRowNames();
        final Boolean[] filteredRows = this.getFilteredRows();
        for (int i = 0; i < rowNames.length; i++) {
            final DatatableColumn column = new DatatableColumn();
            column.setIndex(i);
            column.setName(rowNames[i]);
            column.setDbName(getRowName(rowNames[i]));
            final String[] searchParameter = parametersMap.get("columns[" + i + "][search][value]");
            if (searchParameter != null) {
                column.setSearchable(filteredRows[i]);
                final Object transformedValue = this.getTransformedValue(column.getIndex(), searchParameter[0]);
                column.setSearchValue(transformedValue);
                column.setComparisonType(getComparisonType(column.getIndex(), searchParameter[0]));
            }
            columns.add(column);
        }
        final DatatableColumnsWrapper columnsWrapper = new DatatableColumnsWrapper();
        columnsWrapper.setColumns(columns.toArray(new DatatableColumn[] {}));
        columnsWrapper.setTableName(getTableName());

        final String[] sortByColumnIndexParameter = parametersMap.get("order[0][column]");
        final int sortByColumnIndex = sortByColumnIndexParameter != null ? Integer.parseInt(sortByColumnIndexParameter[0]) : 0;

        final String[] sortOrderParameter = parametersMap.get("order[0][dir]");
        final String sortOrder = sortOrderParameter != null ? sortOrderParameter[0] : "asc";
        columnsWrapper.setSortByColumnIndex(sortByColumnIndex);
        columnsWrapper.setSortOrder(sortOrder);

        final String[] paginationPageStartParameter = parametersMap.get("start");
        // pagination parameters
        final int startIndex = paginationPageStartParameter != null ? Integer.parseInt(paginationPageStartParameter[0]) : 0;
        final int pageSize = PAGE_SIZE;//fixed because the poc didnt have the choice of page size

        final List<String[]> bodyResult = this.getDatatableDao().findAll(columnsWrapper, startIndex, pageSize, this.getRowMapper());
        final Integer totalFilteredRecordsCount = this.getDatatableDao().count(columnsWrapper);
        result.setData(bodyResult);
        final String totalFilteredRecordsCountString = Integer.toString(totalFilteredRecordsCount);
        result.setRecordsFiltered(totalFilteredRecordsCountString);

        final long totalInDBCount = this.getDatatableDao().countAll(columnsWrapper);
        final String totalInDBCountString = Long.toString(totalInDBCount);
        result.setRecordsTotal(totalInDBCountString);
        final String[] drawParameter = parametersMap.get("draw");
        if (drawParameter != null) {
            result.setDraw(drawParameter[0]);
        }
        return result;
    }

    /**
     * Gets the row names.
     * The name of the rows ordered
     * Row names must be the same as the mapped entity property name
     * And the same ordering as in the data-table
     *
     * @return the row names
     */
    abstract protected String[] getRowNames();

    /**
     * An array to identify if a column is filtered (search boxes) or not.
     * has to keep the same number and order as row names
     *
     * @return the filtered row names
     */
    abstract protected Boolean[] getFilteredRows();

    /**
     * Gets the table name.
     *
     * @return the table name
     */
    abstract protected String getTableName();

    /**
     * Gets the row name.
     *
     * @param columnId the column id
     * @return the row name
     */
    abstract protected String getRowName(String columnId);

    /**
     * Gets the result row mapper.
     * The datatable plugin expects a list of string arrays
     * The dao takes this resolver to transform the entity objects
     * @return the result transformer
     */
    abstract protected RowMapper<String[]> getRowMapper();

    /**
     * Gets the transformed value.
     * The datatable js plugin returns the string value, the dao needs the entity type objects
     * this method does the transformation following the same order as in the datatable
     * @param columnIndex the column index
     * @param columnValue the column value
     * @return the transformed value
     */
    abstract protected Object getTransformedValue(final int columnIndex, final String columnValue);

    /**
     * Gets the comparison type : like or equal
     * depending on the type of the object the comparison type should be different
     * @param columnIndex the column index
     * @param columnValue the column value
     * @return the comparison type
     */
    abstract protected String getComparisonType(final int columnIndex, final String columnValue);
}
