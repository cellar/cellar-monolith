/**
 * 
 */
package eu.europa.ec.opoce.cellar.support;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * @author dsavares
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SparqlDocument {

    @XmlAttribute
    private String uri;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    @Override
    public String toString() {
        return "Document [uri=" + uri + "]";
    }

}
