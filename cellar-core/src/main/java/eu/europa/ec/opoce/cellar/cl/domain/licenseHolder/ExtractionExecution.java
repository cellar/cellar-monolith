/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.LicenseHolder
 *             FILE : ExtractionExecution.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 5, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.licenseHolder;

import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionConfiguration.CONFIGURATION_TYPE;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;
import java.util.Set;

/**
 * <class_description> Class that represents the informations about an
 * extraction execution. <br/>
 * <br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc. <br/>
 * <br/>
 * ON : Oct 5, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Entity
@Table(name = "EXTRACTION_EXECUTION")
@NamedQueries({
		@NamedQuery(name = "findStatusExtractionExecutions", query = "select e from ExtractionExecution e where e.executionStatus in (:status)"),
		@NamedQuery(name = "findStatusEndedExtractionExecutions", query = "select e from ExtractionExecution e where e.executionStatus = :status and (e.endDate is null or e.endDate < :now)"),
		@NamedQuery(name = "findTypeOldExtractionExecutions", query = "select e from ExtractionExecution e, ExtractionConfiguration c where c.id = e.extractionConfiguration.id and c.configurationType = :type and e.executionStatus in (:status) and e.endDate <= :lastDate"),
		@NamedQuery(name = "findExtractionConfigurationId", query = "select e from ExtractionExecution e, ExtractionConfiguration c where c.id = e.extractionConfiguration.id and c.id = ?0"),
		@NamedQuery(name = "findTypeExtractionExecutions", query = "select e from ExtractionExecution e, ExtractionConfiguration c where c.id = e.extractionConfiguration.id and c.configurationType = ?0"),
		@NamedQuery(name = "findStatusInstanceExtractionExecutions", query = "select e from ExtractionExecution e where e.executionStatus in (:status) and e.instanceId = :instanceId") })
public class ExtractionExecution {

	/**
	 * 
	 * <class_description> The extraction execution status. <br/>
	 * <br/>
	 * <notes> Includes guaranteed invariants, usage instructions and/or
	 * examples, reminders about desired improvements, etc. <br/>
	 * <br/>
	 * ON : Nov 26, 2012
	 *
	 * @author ARHS Developments
	 * @version $Revision$
	 */
	public static enum EXECUTION_STATUS {
		NEW, PENDING, SPARQL_EXECUTING, SPARQL_ERROR, SPARQL_PENDING, SPARQL_CLEANING, PARSING, PARSING_ERROR, PARSING_PENDING, PARSING_CLEANING, ARCHIVING_INIT, ARCHIVING, ARCHIVING_FINALIZE, ARCHIVING_ERROR, ARCHIVING_PENDING, ARCHIVING_CLEANING, CLEANING_PENDING, CLEANING, CLEANING_ERROR, DONE;
	};

	/**
	 * Internal id.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	private Long id;

	/**
	 * The execution status.
	 */
	@Enumerated(EnumType.STRING)
	@Column(name = "EXECUTION_STATUS", nullable = false)
	private EXECUTION_STATUS executionStatus;

	/**
	 * The effective execution date.
	 */
	@Column(name = "EFFECTIVE_EXECUTION_DATE")
	private Date effectiveExecutionDate;

	/**
	 * The execution start date.
	 */
	@Column(name = "START_DATE")
	private Date startDate;

	/**
	 * The execution end date.
	 */
	@Column(name = "END_DATE")
	private Date endDate;

	/**
	 * The error description, if any.
	 */
	@Column(name = "ERROR_DESCRIPTION")
	private String errorDescription;

	/**
	 * The error stacktrace, if any.
	 */
	@Column(name = "ERROR_STACKTRACE")
	private String errorStacktrace;

	/**
	 * The SPARQL temp file path.
	 */
	@Column(name = "SPARQL_TEMP_FILE_PATH")
	private String sparqlTempFilePath;

	/**
	 * The archive temp directory path.
	 */
	@Column(name = "ARCHIVE_TEMP_DIRECTORY_PATH")
	private String archiveTempDirectoryPath;

	/**
	 * The destination archive file path.
	 */
	@Column(name = "ARCHIVE_FILE_PATH")
	private String archiveFilePath;

	/**
	 * Indicates if the next execution is already generated.
	 */
	@Column(name = "NEXT_EXECUTION_GENERATED", nullable = false)
	private Boolean nextExecutionGenerated;

	/**
	 * The instance identifier of the executor.
	 */
	@Column(name = "INSTANCE_ID")
	private String instanceId;

	/**
	 * The extraction work identifiers corresponding to this execution.
	 */
	@OneToMany(mappedBy = "extractionExecution", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<ExtractionWorkIdentifier> extractionWorkIdentifiers;

	/**
	 * The parent extraction configuration.
	 */
	@ManyToOne
	@JoinColumn(name = "EXTRACTION_CONFIGURATION_ID")
	private ExtractionConfiguration extractionConfiguration;

	/**
	 * Constructor.
	 */
	public ExtractionExecution() {

	}

	/**
	 * Constructs a new reindexation job.
	 * 
	 * @param jobName
	 *            the name of the job
	 * @param sparqlQuery
	 *            the SPARQL query of the job
	 * @param priority
	 *            the priority of the job
	 */
	public ExtractionExecution(final Date startDate, final Date endDate,
			final ExtractionConfiguration extractionConfiguration) {
		this.startDate = startDate;
		this.endDate = endDate;
		this.extractionConfiguration = extractionConfiguration;
		this.nextExecutionGenerated = false;

		if (this.endDate == null || this.endDate.compareTo(new Date()) <= 0)
			this.executionStatus = EXECUTION_STATUS.PENDING;
		else
			this.executionStatus = EXECUTION_STATUS.NEW; // the default status
	}

	/**
	 * Gets the value of the id.
	 * 
	 * @return the id
	 */
	public Long getId() {
		return this.id;
	}

	/**
	 * Sets the value of the id.
	 * 
	 * @param id
	 *            the id to set
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * Gets the execution status.
	 * 
	 * @return the execution status
	 */
	public EXECUTION_STATUS getExecutionStatus() {
		return this.executionStatus;
	}

	/**
	 * Sets the execution status.
	 * 
	 * @param executionStatus
	 *            the execution status to set
	 */
	public void setExecutionStatus(final EXECUTION_STATUS executionStatus) {
		switch (executionStatus) // checks the status consistency
		{
		case PENDING:
			if (this.executionStatus != null && this.executionStatus != EXECUTION_STATUS.PENDING
					&& this.executionStatus != EXECUTION_STATUS.NEW) {
				throw ExceptionBuilder.get(CellarException.class)
						.withMessage("Cannot change the extraction configuration status in " + executionStatus
								+ " if it is " + this.executionStatus + ".")
						.build();
			}
			break;
		case SPARQL_EXECUTING:
			if (this.executionStatus != null && this.executionStatus != EXECUTION_STATUS.SPARQL_EXECUTING
					&& this.executionStatus != EXECUTION_STATUS.PENDING
					&& this.executionStatus != EXECUTION_STATUS.SPARQL_CLEANING
					&& this.executionStatus != EXECUTION_STATUS.CLEANING) {
				throw ExceptionBuilder.get(CellarException.class)
						.withMessage("Cannot change the extraction configuration status in " + executionStatus
								+ " if it is " + this.executionStatus + ".")
						.build();
			}
			break;
		case SPARQL_ERROR:
			if (this.executionStatus != null && this.executionStatus != EXECUTION_STATUS.SPARQL_ERROR
					&& this.executionStatus != EXECUTION_STATUS.SPARQL_EXECUTING
					&& this.executionStatus != EXECUTION_STATUS.SPARQL_CLEANING) {
				throw ExceptionBuilder.get(CellarException.class)
						.withMessage("Cannot change the extraction configuration status in " + executionStatus
								+ " if it is " + this.executionStatus + ".")
						.build();
			}
			break;
		case SPARQL_PENDING:
			if (this.executionStatus != null && this.executionStatus != EXECUTION_STATUS.SPARQL_PENDING
					&& this.executionStatus != EXECUTION_STATUS.SPARQL_ERROR) {
				throw ExceptionBuilder.get(CellarException.class)
						.withMessage("Cannot change the extraction configuration status in " + executionStatus
								+ " if it is " + this.executionStatus + ".")
						.build();
			}
			break;
		case SPARQL_CLEANING:
			if (this.executionStatus != null && this.executionStatus != EXECUTION_STATUS.SPARQL_CLEANING
					&& this.executionStatus != EXECUTION_STATUS.SPARQL_PENDING) {
				throw ExceptionBuilder.get(CellarException.class)
						.withMessage("Cannot change the extraction configuration status in " + executionStatus
								+ " if it is " + this.executionStatus + ".")
						.build();
			}
			break;
		case PARSING:
			if (this.executionStatus != null && this.executionStatus != EXECUTION_STATUS.PARSING
					&& this.executionStatus != EXECUTION_STATUS.SPARQL_EXECUTING
					&& this.executionStatus != EXECUTION_STATUS.PARSING_CLEANING) {
				throw ExceptionBuilder.get(CellarException.class)
						.withMessage("Cannot change the extraction configuration status in " + executionStatus
								+ " if it is " + this.executionStatus + ".")
						.build();
			}
			break;
		case PARSING_ERROR:
			if (this.executionStatus != null && this.executionStatus != EXECUTION_STATUS.PARSING_ERROR
					&& this.executionStatus != EXECUTION_STATUS.PARSING
					&& this.executionStatus != EXECUTION_STATUS.PARSING_CLEANING) {
				throw ExceptionBuilder.get(CellarException.class)
						.withMessage("Cannot change the extraction configuration status in " + executionStatus
								+ " if it is " + this.executionStatus + ".")
						.build();
			}
			break;
		case PARSING_PENDING:
			if (this.executionStatus != null && this.executionStatus != EXECUTION_STATUS.PARSING_PENDING
					&& this.executionStatus != EXECUTION_STATUS.PARSING_ERROR) {
				throw ExceptionBuilder.get(CellarException.class)
						.withMessage("Cannot change the extraction configuration status in " + executionStatus
								+ " if it is " + this.executionStatus + ".")
						.build();
			}
			break;
		case PARSING_CLEANING:
			if (this.executionStatus != null && this.executionStatus != EXECUTION_STATUS.PARSING_CLEANING
					&& this.executionStatus != EXECUTION_STATUS.PARSING_PENDING) {
				throw ExceptionBuilder.get(CellarException.class)
						.withMessage("Cannot change the extraction configuration status in " + executionStatus
								+ " if it is " + this.executionStatus + ".")
						.build();
			}
			break;
		case ARCHIVING_INIT:
			if (this.executionStatus != null && this.executionStatus != EXECUTION_STATUS.ARCHIVING_INIT
					&& this.executionStatus != EXECUTION_STATUS.PARSING
					&& this.executionStatus != EXECUTION_STATUS.ARCHIVING_CLEANING) {
				throw ExceptionBuilder.get(CellarException.class)
						.withMessage("Cannot change the extraction configuration status in " + executionStatus
								+ " if it is " + this.executionStatus + ".")
						.build();
			}
			break;
		case ARCHIVING:
			if (this.executionStatus != null && this.executionStatus != EXECUTION_STATUS.ARCHIVING
					&& this.executionStatus != EXECUTION_STATUS.ARCHIVING_INIT) {
				throw ExceptionBuilder.get(CellarException.class)
						.withMessage("Cannot change the extraction configuration status in " + executionStatus
								+ " if it is " + this.executionStatus + ".")
						.build();
			}
			break;
		case ARCHIVING_FINALIZE:
			if (this.executionStatus != null && this.executionStatus != EXECUTION_STATUS.ARCHIVING_FINALIZE
					&& this.executionStatus != EXECUTION_STATUS.ARCHIVING
					&& this.executionStatus != EXECUTION_STATUS.ARCHIVING_INIT) {
				throw ExceptionBuilder.get(CellarException.class)
						.withMessage("Cannot change the extraction configuration status in " + executionStatus
								+ " if it is " + this.executionStatus + ".")
						.build();
			}
			break;
		case ARCHIVING_ERROR:
			if (this.executionStatus != null && this.executionStatus != EXECUTION_STATUS.ARCHIVING_ERROR
					&& this.executionStatus != EXECUTION_STATUS.ARCHIVING_INIT
					&& this.executionStatus != EXECUTION_STATUS.ARCHIVING
					&& this.executionStatus != EXECUTION_STATUS.ARCHIVING_FINALIZE
					&& this.executionStatus != EXECUTION_STATUS.ARCHIVING_CLEANING) {
				throw ExceptionBuilder.get(CellarException.class)
						.withMessage("Cannot change the extraction configuration status in " + executionStatus
								+ " if it is " + this.executionStatus + ".")
						.build();
			}
			break;
		case ARCHIVING_PENDING:
			if (this.executionStatus != null && this.executionStatus != EXECUTION_STATUS.ARCHIVING_PENDING
					&& this.executionStatus != EXECUTION_STATUS.ARCHIVING_ERROR) {
				throw ExceptionBuilder.get(CellarException.class)
						.withMessage("Cannot change the extraction configuration status in " + executionStatus
								+ " if it is " + this.executionStatus + ".")
						.build();
			}
			break;
		case ARCHIVING_CLEANING:
			if (this.executionStatus != null && this.executionStatus != EXECUTION_STATUS.ARCHIVING_CLEANING
					&& this.executionStatus != EXECUTION_STATUS.ARCHIVING_PENDING) {
				throw ExceptionBuilder.get(CellarException.class)
						.withMessage("Cannot change the extraction configuration status in " + executionStatus
								+ " if it is " + this.executionStatus + ".")
						.build();
			}
			break;
		case CLEANING_PENDING:
			if (this.executionStatus != null && this.executionStatus != EXECUTION_STATUS.CLEANING_PENDING
					&& this.executionStatus != EXECUTION_STATUS.SPARQL_ERROR
					&& this.executionStatus != EXECUTION_STATUS.PARSING_ERROR
					&& this.executionStatus != EXECUTION_STATUS.ARCHIVING_ERROR
					&& this.executionStatus != EXECUTION_STATUS.DONE) {
				throw ExceptionBuilder.get(CellarException.class)
						.withMessage("Cannot change the extraction configuration status in " + executionStatus
								+ " if it is " + this.executionStatus + ".")
						.build();
			}
			break;
		case CLEANING:
			if (this.executionStatus != null && this.executionStatus != EXECUTION_STATUS.CLEANING
					&& this.executionStatus != EXECUTION_STATUS.CLEANING_PENDING) {
				throw ExceptionBuilder.get(CellarException.class)
						.withMessage("Cannot change the extraction configuration status in " + executionStatus
								+ " if it is " + this.executionStatus + ".")
						.build();
			}
			break;
		case CLEANING_ERROR:
			if (this.executionStatus != null && this.executionStatus != EXECUTION_STATUS.CLEANING_ERROR
					&& this.executionStatus != EXECUTION_STATUS.CLEANING) {
				throw ExceptionBuilder.get(CellarException.class)
						.withMessage("Cannot change the extraction configuration status in " + executionStatus
								+ " if it is " + this.executionStatus + ".")
						.build();
			}
			break;
		case DONE:
			if (this.executionStatus != null && this.executionStatus != EXECUTION_STATUS.DONE
					&& this.executionStatus != EXECUTION_STATUS.ARCHIVING_FINALIZE) {
				throw ExceptionBuilder.get(CellarException.class)
						.withMessage("Cannot change the extraction configuration status in " + executionStatus
								+ " if it is " + this.executionStatus + ".")
						.build();
			}
			break;
		// should never get here!
		default:
			break;
		}

		this.executionStatus = executionStatus;
	}

	/**
	 * Gets the start date of the execution.
	 * 
	 * @return the start date of the execution
	 */
	public Date getStartDate() {
		return this.startDate;
	}

	/**
	 * Sets the start date of the execution.
	 * 
	 * @param startDate
	 *            the start date of the execution to set
	 */
	public void setStartdDate(final Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * Gets the end date of the execution.
	 * 
	 * @return the end date of the execution
	 */
	public Date getEndDate() {
		return this.endDate;
	}

	/**
	 * Sets the end date of the execution.
	 * 
	 * @param endDate
	 *            the end date of the execution
	 */
	public void setEndDate(final Date endDate) {
		this.endDate = endDate;
	}

	/**
	 * Gets the effective execution date of the execution.
	 * 
	 * @return the effective execution date of the execution
	 */
	public Date getEffectiveExecutionDate() {
		return this.effectiveExecutionDate;
	}

	/**
	 * Sets the effective execution date of the execution.
	 * 
	 * @param effectiveExecutionDate
	 *            the effective execution date to set
	 */
	public void setEffectiveExecutionDate(final Date effectiveExecutionDate) {
		this.effectiveExecutionDate = effectiveExecutionDate;
	}

	/**
	 * Gets the error description, if any.
	 * 
	 * @return the error description, if any. Otherwise, null.
	 */
	public String getErrorDescription() {
		return this.errorDescription;
	}

	/**
	 * Sets the error description.
	 * 
	 * @param errorDescription
	 *            the error description to set.
	 */
	public void setErrorDescription(final String errorDescription) {
		this.errorDescription = errorDescription;
	}

	/**
	 * Gets the error stacktrace, if any.
	 * 
	 * @return the error stacktrace, if any. Otherwise, null.
	 */
	public String getErrorStacktrace() {
		return this.errorStacktrace;
	}

	/**
	 * Sets the error stacktrace.
	 * 
	 * @param errorStacktrace
	 *            the error stacktrace to set.
	 */
	public void setErrorStacktrace(final String errorStacktrace) {
		this.errorStacktrace = errorStacktrace;
	}

	/**
	 * Gets the SPARQL temp file path.
	 * 
	 * @return the SPARQL temp file path
	 */
	public String getSparqlTempFilePath() {
		return this.sparqlTempFilePath;
	}

	/**
	 * Sets the SPARQL temp file path.
	 * 
	 * @param sparqlTempFilePath
	 *            the SPARQL temp file path to set
	 */
	public void setSparqlTempFilePath(final String sparqlTempFilePath) {
		this.sparqlTempFilePath = sparqlTempFilePath;
	}

	/**
	 * Gets the archive temp directory path.
	 * 
	 * @return the archive temp directory path
	 */
	public String getArchiveTempDirectoryPath() {
		return this.archiveTempDirectoryPath;
	}

	/**
	 * Sets the archive temp directory path.
	 * 
	 * @param archiveTempDirectoryPath
	 *            the archive temp directory path to set
	 */
	public void setArchiveTempDirectoryPath(final String archiveTempDirectoryPath) {
		this.archiveTempDirectoryPath = archiveTempDirectoryPath;
	}

	/**
	 * Gets the archive file path.
	 * 
	 * @return the archive file path
	 */
	public String getArchiveFilePath() {
		return this.archiveFilePath;
	}

	/**
	 * Sets the archive file path.
	 * 
	 * @param archiveFilePath
	 *            the archive file path to set
	 */
	public void setArchiveFilePath(final String archiveFilePath) {
		this.archiveFilePath = archiveFilePath;
	}

	/**
	 * Gets the indicator of the next execution generation.
	 * 
	 * @return the indicator of the next execution generation
	 */
	public Boolean getNextExecutionGenerated() {
		return this.nextExecutionGenerated;
	}

	/**
	 * Sets the indicator of the next execution generation.
	 * 
	 * @param nextExecutionGenerated
	 *            the indicator of the next execution generation to set
	 */
	public void setNextExecutionGenerated(final Boolean nextExecutionGenerated) {
		this.nextExecutionGenerated = nextExecutionGenerated;
	}

	/**
	 * Gets the instance identifier of the executor.
	 * 
	 * @return the instance identifier of the executor
	 */
	public String getInstanceId() {
		return this.instanceId;
	}

	/**
	 * Sets the instance identifier of the executor.
	 * 
	 * @param instanceId
	 *            the instance identifier of the executor to set
	 */
	public void setInstanceId(final String instanceId) {
		this.instanceId = instanceId;
	}

	/**
	 * Gets the works corresponding to the SPARQL query executed.
	 * 
	 * @return the works corresponding the SPARQL query executed. Otherwise,
	 *         false
	 */
	public Set<ExtractionWorkIdentifier> getExtractionWorkIdentifiers() {
		return this.extractionWorkIdentifiers;
	}

	/**
	 * Sets the works corresponding to the SPARQL query executed.
	 * 
	 * @param extractionWorkIdentifiers
	 *            the works to set
	 */
	public void setExtractionWorkIdentifiers(final Set<ExtractionWorkIdentifier> extractionWorkIdentifiers) {
		this.extractionWorkIdentifiers = extractionWorkIdentifiers;
	}

	/**
	 * Gets the parent extraction configuration.
	 * 
	 * @return the parent extraction configuration
	 */
	public ExtractionConfiguration getExtractionConfiguration() {
		return this.extractionConfiguration;
	}

	/**
	 * Sets the parent extraction configuration.
	 * 
	 * @param extractionConfiguration
	 *            the parent extraction configuration to set
	 */
	public void setExtractionConfiguration(final ExtractionConfiguration extractionConfiguration) {
		this.extractionConfiguration = extractionConfiguration;
	}

	/**
	 * Returns true if the execution is restartable. Otherwise, false.
	 * 
	 * @return true if the execution is restartable. Otherwise, false.
	 */
	public boolean isRestartable() {
		return (this.executionStatus == EXECUTION_STATUS.ARCHIVING_ERROR
				|| this.executionStatus == EXECUTION_STATUS.CLEANING_ERROR
				|| this.executionStatus == EXECUTION_STATUS.DONE
				|| this.executionStatus == EXECUTION_STATUS.PARSING_ERROR
				|| this.executionStatus == EXECUTION_STATUS.SPARQL_ERROR);
	}

	/**
	 * Returns true if the current step is restartable. Otherwise, false.
	 * 
	 * @return true if the current step is restartable. Otherwise, false.
	 */
	public boolean isStepRestartable() {
		return (this.executionStatus == EXECUTION_STATUS.ARCHIVING_ERROR
				|| this.executionStatus == EXECUTION_STATUS.PARSING_ERROR
				|| this.executionStatus == EXECUTION_STATUS.SPARQL_ERROR);
	}

	/**
	 * Generates the next execution.
	 * 
	 * @return the generated execution
	 */
	public ExtractionExecution getNextExtractionExecution() {
		final Date startDate = this.endDate;

		if (this.extractionConfiguration.getConfigurationType() == CONFIGURATION_TYPE.STD)
			return null; // there is no execution to generate

		final Calendar endCalendar = Calendar.getInstance();
		endCalendar.clear();
		endCalendar.setFirstDayOfWeek(Calendar.MONDAY);
		endCalendar.setTime(startDate);

		switch (this.extractionConfiguration.getConfigurationType()) {
		case STDD: // adds a day
			endCalendar.add(Calendar.DATE, 1);
			break;
		case STDW: // adds a week
			endCalendar.add(Calendar.DATE, 7);
			break;
		case STDM: // adds a month
			endCalendar.add(Calendar.MONTH, 1);
			break;
		// should never get here!
		default:
			break;
		}

		return new ExtractionExecution(startDate, endCalendar.getTime(), this.extractionConfiguration);
	}
}
