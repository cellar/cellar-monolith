package eu.europa.ec.opoce.cellar.cl.domain.email;

import org.apache.commons.lang.StringUtils;

public enum EmailStatus {
    NEW("N"),
    SENT("S"),
    FAILED("F");

    private final String value;

    public String getValue() {
        return value;
    }

    EmailStatus(String value) {
        this.value = value;
    }
    public static EmailStatus findByValue(final String value) {
        if (StringUtils.isBlank(value)) {
            return null;
        }
        for (final EmailStatus status: values()) {
            if (value.equals(status.value)) {
                return status;
            }
        }
        return null;
    }
}
