/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *             FILE : SecurityServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 11, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.ccr.service.database.DefaultTransactionService;
import eu.europa.ec.opoce.cellar.cl.dao.GroupDao;
import eu.europa.ec.opoce.cellar.cl.dao.RoleDao;
import eu.europa.ec.opoce.cellar.cl.dao.UserDao;
import eu.europa.ec.opoce.cellar.cl.domain.admin.Group;
import eu.europa.ec.opoce.cellar.cl.domain.admin.Role;
import eu.europa.ec.opoce.cellar.cl.domain.admin.User;
import eu.europa.ec.opoce.cellar.cl.service.client.SecurityService;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <class_description> Security manager service.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 11, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class SecurityServiceImpl extends DefaultTransactionService implements SecurityService {

    /**
     * Dao for the the Entity {@link Role}.  
     */
    @Autowired
    private RoleDao roleDao;

    /**
     * Dao for the the Entity {@link Group}.  
     */
    @Autowired
    private GroupDao groupDao;

    /**
     * Dao for the the Entity {@link User}.  
     */
    @Autowired
    private UserDao userDao;


    /**
     * {@inheritDoc}
     */
    @Override
    public List<Group> findAllGroups() {
        return this.groupDao.findAll();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public User findUser(final Long id) {
        return this.userDao.getObject(id);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public User findUserByUsername(final String username) {
        return this.userDao.findUserByUsername(username);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Group findGroup(final Long id) {
        return this.groupDao.getObject(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void createUser(final User user) {
        this.userDao.saveObject(user);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void updateUser(final User user) {
        this.userDao.updateObject(user);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Role> findAllRoles() {
        return this.roleDao.findAll();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void createGroup(final Group group) {
        this.groupDao.saveObject(group);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void updateGroup(final Group group) {
        this.groupDao.updateObject(group);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Role findRole(final Long id) {
        return this.roleDao.getObject(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<User> findAllUsers() {
        return this.userDao.findAll();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void deleteUser(final Long id) {
        final User user = this.userDao.getObject(id);

        if (user != null) { // if the user exists
            this.userDao.deleteObject(user);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void deleteGroup(final Long id) {
        final Group group = this.groupDao.getObject(id);

        if (group != null) { // if the user exists
            this.groupDao.deleteObject(group);
        }
    }
    
    @Override
    public List<User> findUsersHavingRole(String role) {
    	return this.findAllUsers().stream()
    		.filter(u ->  u.getGroup().getRoles().stream()
    				.map(Role::getRoleAccess)
    				.anyMatch(r -> r.equals(role)))
    		.collect(Collectors.toList());
    }
    
}
