package eu.europa.ec.opoce.cellar.cl.manager;

import eu.europa.ec.opoce.cellar.cl.domain.AbstractBatchManager;
import eu.europa.ec.opoce.cellar.cl.manager.HierarchyAlignerManager.HierarchyAlignerBatchManager;
import eu.europa.ec.opoce.cellar.cl.service.aligner.IHierarchyAlignerService;
import eu.europa.ec.opoce.cellar.logging.LogContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.HIERARCHY_ALIGNER;

/**
 * The Class HierarchyAlignerManager.
 */
@Component("hierarchyAlignerManager")
public class HierarchyAlignerManager extends AbstractBatchManager<HierarchyAlignerBatchManager> {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOG = LogManager.getLogger(HierarchyAlignerManager.class);

    /**
     * The hierarchy aligner service.
     */
    @Autowired
    private IHierarchyAlignerService hierarchyAlignerService;

    @LogContext
    class HierarchyAlignerBatchManager extends Thread {

        @Override
        @LogContext(HIERARCHY_ALIGNER)
        public void run() {
            while (isEnabled()) {
                if (!hierarchyAlignerService.alignUnknown()) {
                    break;
                }
            }
            LOG.info("There are no more Cellar resources to realign.");
        }
    }

    @Override
    protected HierarchyAlignerBatchManager getThreadInstance() {
        return new HierarchyAlignerBatchManager();
    }

}
