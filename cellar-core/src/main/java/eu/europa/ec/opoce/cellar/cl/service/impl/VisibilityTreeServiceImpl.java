/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *             FILE : VisibilityTreeServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 20, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cl.service.client.VisibilityTreeService;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement;
import eu.europa.ec.opoce.cellar.server.service.DisseminationDbGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * <class_description> Visibility tree service implementation.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 20, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class VisibilityTreeServiceImpl implements VisibilityTreeService {

    @Autowired
    private DisseminationDbGateway disseminationDbGateway;

    @Autowired
    @Qualifier("pidManagerService")
    private IdentifierService identifierService;

    @Override
    public MetsElement getHierarchy(final String productionId) {
        final String cellarId = this.identifierService.getCellarPrefixed(productionId);

        return this.disseminationDbGateway.getCompleteTree(cellarId);
    }
}
