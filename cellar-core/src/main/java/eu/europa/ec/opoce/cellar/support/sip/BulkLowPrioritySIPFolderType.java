package eu.europa.ec.opoce.cellar.support.sip;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;

/**
 * The Class BulkLowPrioritySIPFolderType.
 */
public class BulkLowPrioritySIPFolderType extends CellarReceptionFolderType {

	/** {@inheritDoc} */
	@Override
	public String getReceptionFolderAbsolutePath(ICellarConfiguration cellarConfiguration) {
		return cellarConfiguration.getCellarFolderBulkLowPriorityReception();
	}

	/** {@inheritDoc} */
	@Override
	public TYPE getSipWorkType() {
		return TYPE.BULK_LOWPRIORITY;
	}

}
