/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.dao
 *        FILE : RelationProdIdDao.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 01-07-2023
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2023 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao;

import java.util.Collection;

import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.RelationProdId;

/**
 * DAO providing access to operations on the {@code RelationProdId} domain object.
 * @author EUROPEAN DYNAMICS S.A.
 */
public interface RelationProdIdDao extends BaseDao<RelationProdId, Long> {

	/**
	 * Deletes the production identifiers related to the package references
	 * with the provided IDs.
	 * @param sipIds The SIP IDs whose related production identifiers should be
	 * deleted.
	 * @return the number of deleted rows.
	 */
	int deleteBySipIdIn(Collection<Long> sipIds);
	
}
