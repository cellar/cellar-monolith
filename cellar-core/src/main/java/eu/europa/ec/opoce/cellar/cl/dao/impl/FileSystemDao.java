package eu.europa.ec.opoce.cellar.cl.dao.impl;

import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditBuilder;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventAction;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventProcess;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventType;
import eu.europa.ec.opoce.cellar.common.util.FileSystemUtils;
import eu.europa.ec.opoce.cellar.common.util.ZipUtils;
import eu.europa.ec.opoce.cellar.core.dao.FileDao;
import eu.europa.ec.opoce.cellar.core.exception.UncompressException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.FileSystemException;
import eu.europa.ec.opoce.cellar.exception.MetsException;
import eu.europa.ec.opoce.cellar.exception.NotImplementedExceptionBuilder;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;
import eu.europa.ec.opoce.cellar.support.FileExtensionUtils;
import eu.europa.ec.opoce.cellar.support.FileFilterUtils;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.PostConstruct;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;


/**
 * This DAO is responsible for managing the files using the file system.
 *
 * @author dsavares
 *
 */
@Component
public class FileSystemDao implements FileDao {

    /** Logger instance. */
    private static final Logger LOG = LogManager.getLogger(FileSystemDao.class);

    /**
     * The Cellar configuration storing runtime parameters.
     */
    private ICellarConfiguration cellarConfiguration;

    /** The next val. */
    private static final AtomicInteger NEXT = new AtomicInteger();

    @Autowired
    public FileSystemDao(@Qualifier("cellarConfiguration")ICellarConfiguration cellarConfiguration) {
        this.cellarConfiguration = cellarConfiguration;
    }

    /**
     * Initialize.
     */
    @PostConstruct
    public void initialize() {
        createFolder(cellarConfiguration.getCellarFolderRoot());
        createFolder(cellarConfiguration.getCellarFolderCmrLog());
        createFolder(cellarConfiguration.getCellarFolderTemporaryDissemination());
        createFolder(cellarConfiguration.getCellarFolderTemporaryWork());
        createFolder(cellarConfiguration.getCellarFolderTemporaryStore());
        createFolder(cellarConfiguration.getCellarFolderExportRest());
        createFolder(cellarConfiguration.getCellarFolderExportBatchJob());
        createFolder(cellarConfiguration.getCellarFolderExportBatchJobTemporary());
        createFolder(cellarConfiguration.getCellarFolderExportUpdateResponse());
        createFolder(cellarConfiguration.getCellarFolderLicenseHolderArchiveExtraction());
        createFolder(cellarConfiguration.getCellarFolderLicenseHolderArchiveAdhocExtraction());
        createFolder(cellarConfiguration.getCellarFolderLicenseHolderTemporaryArchive());
        createFolder(cellarConfiguration.getCellarFolderLicenseHolderTemporarySparql());
        createFolder(cellarConfiguration.getCellarFolderBatchJobSparqlQueries());
        createFolder(cellarConfiguration.getCellarFolderSparqlResponseTemplates());

        if (cellarConfiguration.isCellarServiceIngestionEnabled()) {
            createFolder(cellarConfiguration.getCellarFolderAuthenticOJReception());
            createFolder(cellarConfiguration.getCellarFolderDailyReception());
            createFolder(cellarConfiguration.getCellarFolderBulkReception());
            createFolder(cellarConfiguration.getCellarFolderBulkLowPriorityReception());
            createFolder(cellarConfiguration.getCellarFolderTemporaryWork());
            createFolder(cellarConfiguration.getCellarFolderFoxml());
            createFolder(cellarConfiguration.getCellarFolderAuthenticOJResponse());
            createFolder(cellarConfiguration.getCellarFolderDailyResponse());
            createFolder(cellarConfiguration.getCellarFolderBulkResponse());
            createFolder(cellarConfiguration.getCellarFolderBulkLowPriorityResponse());
            createFolder(cellarConfiguration.getCellarFolderError());
            createFolder(cellarConfiguration.getCellarFolderBackup());
            createFolder(cellarConfiguration.getCellarFolderLock());
        }
    }

    /**
     * Copy data from the zip stream to the destination stream. The destination stream is closed
     * inside the method.
     *
     * @param zipStream
     *            the stream to read.
     * @param destinationStream
     *            the stream to write.
     * @throws IOException
     *             if an I/O error occurs.
     */
    private static void copyFromZipStreamToDestinationStream(final InputStream zipStream, final OutputStream destinationStream)
            throws IOException {
        try {
            IOUtils.copy(zipStream, destinationStream);
        } finally {
            destinationStream.close();
        }
    }

    /**
     * Return a file that doesn't exist in the target dir.
     * @param dirname the prefix of the target file to returned
     * @param targetDir the dir where the target file must be created
     * @return a file that doesn't exist in the target dir.
     */
    private static synchronized File getNonExistingTargetDirectory(final String dirname, final String targetDir) {
        File target = new File(targetDir, dirname);
        int i = 1;
        while (target.exists()) {
            target = new File(targetDir, dirname + "_" + i++);
        }

        target.mkdirs();

        return target;
    }

    /**
     * Checks if is valid.
     *
     * @param zipFile the alleged zipfile
     * @return true, if the file is a valid zip file
     */
    public static boolean isZipFileValid(final File zipFile) {
        try (ZipFile zipfile = new ZipFile(zipFile)) {
            return true;
        } catch (final IOException e) {
            return false;
        }
    }

    private void copyDirectory(File sourceFolder, File errorDir) throws IOException {
        if (sourceFolder.exists()) {
            FileUtils.copyDirectory(sourceFolder, errorDir);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void copySIPToErrorFolder(final File sipFile) throws IOException {
        final File nonExistingTargetFile = getNonExistingTargetFile(sipFile.getName(), this.cellarConfiguration.getCellarFolderError());
        copyFile(sipFile, nonExistingTargetFile);
    }

    private void copyFile(File sourceFile, File destinationFile) throws IOException {
        if (sourceFile.exists()) {
            FileUtils.copyFile(sourceFile, destinationFile);
        }
    }

    /**
     * Creates the directory <i>folder</i> including any necessary but nonexistent parent directories.
     * @param folder folder to create.
     */
    public static void createFolder(final String folder) {
        try {
            Path path = Paths.get(folder);
            if (!Files.exists(path)) {
                Files.createDirectories(path);
                LOG.info(IConfiguration.CONFIG,"Create folder [{}]", path);
            }
        } catch (final Exception e) {
            throw ExceptionBuilder.get(FileSystemException.class).withCode(CoreErrors.E_010)
                    .withMessage("Folder [" + folder + "] cannot be created.")
                    .withCause(e).build();
        }
    }

    /**
     * Deletes the given file in the given directory.
     * @param fileName the name of the file (or directory) to delete in the provided directory
     * @param directoryPath the path to the directory in which to delete the file
     * @throws IOException in case deletion is unsuccessful
     */
    private void deleteFileFromDirectory(final String fileName, final String directoryPath) throws IOException {
        final File toDelete = new File(directoryPath, fileName);
        if (toDelete.exists()) {
            FileUtils.forceDelete(toDelete);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteFromBulkReceptionFolder(final String fileName) throws IOException {
        deleteFileFromDirectory(fileName, this.cellarConfiguration.getCellarFolderBulkReception());
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteFromBulkLowPriorityReceptionFolder(final String fileName) throws IOException {
        deleteFileFromDirectory(fileName, this.cellarConfiguration.getCellarFolderBulkLowPriorityReception());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteFromDailyReceptionFolder(final String fileName) throws IOException {
        deleteFileFromDirectory(fileName, this.cellarConfiguration.getCellarFolderDailyReception());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteFromAuthenticOJReceptionFolder(final String fileName) throws IOException {
        deleteFileFromDirectory(fileName, this.cellarConfiguration.getCellarFolderAuthenticOJReception());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteFromFoxmlFolder(final String foxmlFolderName) throws IOException {
        deleteFileFromDirectory(foxmlFolderName, this.cellarConfiguration.getCellarFolderFoxml());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteFromTemporaryWorkFolder(final String folderName) throws IOException {
        deleteFileFromDirectory(folderName, this.cellarConfiguration.getCellarFolderTemporaryWork());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public File[] getAllFilesFromUncompressedLocation(final String location) throws IOException {
        String uncompressedFolder = this.cellarConfiguration.getCellarFolderTemporaryWork() + File.separator + location;
        Collection<File> filesCollection = FileUtils.listFiles(new File(uncompressedFolder), null, true);
        return createRelativePathToFolderFilesList(uncompressedFolder, filesCollection);
    }

    @Nonnull
    private File[] createRelativePathToFolderFilesList(String rootFolder, Collection<File> filesCollection) {
        File[] filesList = filesCollection.toArray(new File[filesCollection.size()]);
        for (int i = 0; i < filesList.length; i++) {
            Path uncompressedFolderPath = Paths.get(rootFolder);
            Path relativePath = uncompressedFolderPath.relativize(filesList[i].toPath());
            filesList[i] = relativePath.normalize().toFile();
        }
        return filesList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public File[] getAllSipArchivesFromBulkReceptionFolder(final int maxSIPToReturn) throws IOException {
        return getAllSipArchivesFromReceptionFolder(new File(this.cellarConfiguration.getCellarFolderBulkReception()), maxSIPToReturn);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public File[] getAllSipArchivesFromBulkLowPriorityReceptionFolder(final int maxSIPToReturn) throws IOException {
        return getAllSipArchivesFromReceptionFolder(new File(this.cellarConfiguration.getCellarFolderBulkLowPriorityReception()), maxSIPToReturn);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public File[] getAllSipArchivesFromDailyReceptionFolder() throws IOException {
        return getAllSipArchivesFromReceptionFolder(new File(this.cellarConfiguration.getCellarFolderDailyReception()), Integer.MAX_VALUE);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public File[] getAllSipArchivesFromAuthenticOJReceptionFolder() throws IOException {
        return getAllSipArchivesFromReceptionFolder(new File(this.cellarConfiguration.getCellarFolderAuthenticOJReception()),
                Integer.MAX_VALUE);
    }

    /**
     * Gets the all sip archives from reception folder.
     *
     * @param processingFolder the processing folder
     * @param maxSIPToReturn the max sip to return
     * @return the all sip archives from reception folder
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private File[] getAllSipArchivesFromReceptionFolder(final File processingFolder, final int maxSIPToReturn) throws IOException {

        final File[] filesToTryToMove = processingFolder.listFiles(FileFilterUtils.SIP_FILTER);
        if (filesToTryToMove != null && filesToTryToMove.length > 0) {
            final File processingMoveFolder = new File(processingFolder, "move_" + getNextVal() + "_" + System.currentTimeMillis());
            processingMoveFolder.mkdirs();

            final List<File> filesToProcess = new LinkedList<>();
            for (final File file : filesToTryToMove) {
                try {
                    FileUtils.copyFileToDirectory(file, processingMoveFolder, true);

                    if (isZipFileValid(new File(processingMoveFolder, file.getName()))) {
                        filesToProcess.add(file);
                    }

                    if (filesToProcess.size() >= maxSIPToReturn) {
                        break;
                    }
                } catch (final Exception e) {
                    //nothing to do.
                    e.printStackTrace();
                }
            }

            FileUtils.forceDelete(processingMoveFolder);

            return filesToProcess.toArray(new File[0]);
        }

        return filesToTryToMove;
    }

    /** {@inheritDoc} */
    @Override
    public File getArchiveFolderBatchJobExport() {
        final File archiveFolderExport = new File(this.cellarConfiguration.getCellarFolderExportBatchJob());
        return createFolderIfNotExistAndReturnPath(archiveFolderExport);
    }

    @Nonnull
    private File createFolderIfNotExistAndReturnPath(File folderPath) {
        if (!folderPath.exists()) {
            folderPath.mkdirs();
        }
        return folderPath;
    }

    /** {@inheritDoc} */
    @Override
    public File getArchiveTemporaryFolderBatchJobExport() {
        final File archiveFolderExport = new File(this.cellarConfiguration.getCellarFolderExportBatchJobTemporary());
        return createFolderIfNotExistAndReturnPath(archiveFolderExport);
    }

    /** {@inheritDoc} */
    @Override
    public File getArchiveFolderExportRest() {
        final File archiveFolderExportRest = new File(this.cellarConfiguration.getCellarFolderExportRest());
        return createFolderIfNotExistAndReturnPath(archiveFolderExportRest);
    }

    /** {@inheritDoc} */
    @Override
    public File getArchiveFolderExportUpdateResponse() {
        final File archiveFolderExportUpdateResponse = new File(this.cellarConfiguration.getCellarFolderExportUpdateResponse());
        return createFolderIfNotExistAndReturnPath(archiveFolderExportUpdateResponse);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public File getMetsFileFromTemporaryWorkFolder(final String folderName) throws IOException {
        final File temporaryWorkFolder = new File(this.cellarConfiguration.getCellarFolderTemporaryWork());
        final File archiveFolder = new File(temporaryWorkFolder, folderName);

        final File[] listFiles = archiveFolder.listFiles(FileFilterUtils.METS_FILTER);

        if (ArrayUtils.isEmpty(listFiles)) {
            FileUtils.deleteDirectory(archiveFolder);
            throw ExceptionBuilder.get(MetsException.class).withCode(CoreErrors.E_021).withMessage("Folder: " + archiveFolder.getPath())
                    .withMetsDocumentId(folderName).build();
        }
        if (listFiles.length > 1) {
            FileUtils.deleteDirectory(archiveFolder);
            throw ExceptionBuilder.get(MetsException.class).withCode(CoreErrors.E_022)
                    .withMessage("Mets: " + StringUtils.join(listFiles, ", ") + " in folder: " + archiveFolder.getPath())
                    .withMetsDocumentId(folderName).build();
        }

        return listFiles[0];
    }

    /**
     * Gets the next val.
     *
     * @return the next val
     */
    private int getNextVal() {
        return NEXT.incrementAndGet();
    }

    /**
     * This method give a target file that doesn't exist in the target dir.
     *
     * @param filename the filename
     * @param targetDir the target dir
     * @return the non existing target file
     */
    private synchronized File getNonExistingTargetFile(final String filename, final String targetDir) {
        File targetFile = new File(targetDir, filename);

        int i = 1;
        while (targetFile.exists()) {
            targetFile = new File(targetDir, FilenameUtils.getBaseName(filename) + "_" + (i++) + "." + FilenameUtils.getExtension(filename));
        }
        return targetFile;
    }

    /** {@inheritDoc} */
    @Override
    public File getResponseBulkFolder() {
        final File responseFolder = new File(this.cellarConfiguration.getCellarFolderBulkResponse());
        return createFolderIfNotExistAndReturnPath(responseFolder);
    }
    
    /** {@inheritDoc} */
    @Override
    public File getResponseBulkLowPriorityFolder() {
        final File responseFolder = new File(this.cellarConfiguration.getCellarFolderBulkLowPriorityResponse());
        return createFolderIfNotExistAndReturnPath(responseFolder);
    }

    /** {@inheritDoc} */
    @Override
    public File getResponseDailyFolder() {
        final File responseFolder = new File(this.cellarConfiguration.getCellarFolderDailyResponse());
        return createFolderIfNotExistAndReturnPath(responseFolder);
    }

    /** {@inheritDoc} */
    @Override
    public File getResponseAuthenticOJFolder() {
        final File responseFolder = new File(this.cellarConfiguration.getCellarFolderAuthenticOJResponse());
        return createFolderIfNotExistAndReturnPath(responseFolder);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moveFoxmlToBackupFolder(final String foxmlFolderPath) throws IOException {
        final File foxmlFolder = new File(this.cellarConfiguration.getCellarFolderFoxml(), foxmlFolderPath);
        final File backupDir = getNonExistingTargetDirectory(foxmlFolderPath, this.cellarConfiguration.getCellarFolderBackup());
        moveFolder(foxmlFolder, backupDir);
    }

    private void moveFolder(File sourceFolder, File destinationFolder) throws IOException {
        if (sourceFolder.exists()) {
            copyDirectory(sourceFolder, destinationFolder);
            /* only delete when the copy is finished */
            FileUtils.deleteQuietly(sourceFolder);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moveMetsToBackupFolder(final String metsFolderPath) throws IOException {
        final File metsFolder = getMetsFolderFromCellarConfiguration(metsFolderPath);
        final File backupDir = getNonExistingTargetDirectory(metsFolderPath, this.cellarConfiguration.getCellarFolderBackup());
        moveFolder(metsFolder, backupDir);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moveMetsToErrorFolder(final String metsFolderPath) throws IOException {
        final File metsFolder = getMetsFolderFromCellarConfiguration(metsFolderPath);
        final File errorDir = getNonExistingTargetDirectory(metsFolderPath, this.cellarConfiguration.getCellarFolderError());
        moveFolder(metsFolder, errorDir);
    }

    @Nonnull
    private File getMetsFolderFromCellarConfiguration(String metsFolderPath) {
        Path metsFolder = Paths.get(this.cellarConfiguration.getCellarFolderTemporaryWork(), metsFolderPath);
        return metsFolder.toAbsolutePath().normalize().toFile();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moveSIPToBackupFolder(final File sipFile) throws IOException {
        final File nonExistingTargetFile = getNonExistingTargetFile(sipFile.getName(), this.cellarConfiguration.getCellarFolderBackup());
        moveFile(sipFile, nonExistingTargetFile);
    }

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public String moveSIPToDailyReceptionFolder(final File sipFile, final String metsId) throws IOException {
        String sipName = metsId + FileExtensionUtils.SIP;
        File sipInBackup = new File(this.cellarConfiguration.getCellarFolderDailyReception(), sipName);
        moveFile(sipFile, sipInBackup);
        return sipName;
    }
    
    /**
     * {@inheritDoc}
     *
     */
    @Override
    public String moveSIPToBulkReceptionFolder(final File sipFile, final String metsId) throws IOException {
        String sipName = metsId + FileExtensionUtils.SIP;
        File sipInBackup = new File(this.cellarConfiguration.getCellarFolderBulkReception(), sipName);
        moveFile(sipFile, sipInBackup);
        return sipName;
    }

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public String moveSIPToAuthenticOJReceptionFolder(final File sipFile, final String metsId) throws IOException {
        String authenticOJReceptionFolder = this.cellarConfiguration.getCellarFolderAuthenticOJReception();
        String sipName = metsId + FileExtensionUtils.SIP;
        File sipInAuthenticRecepFolder = new File(authenticOJReceptionFolder, sipName);
        moveFile(sipFile, sipInAuthenticRecepFolder);
        return sipName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moveSIPToErrorFolder(final File sipFile) throws IOException {
        final File nonExistingTargetFile = getNonExistingTargetFile(sipFile.getName(), this.cellarConfiguration.getCellarFolderError());
        moveFile(sipFile, nonExistingTargetFile);
    }

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public File moveSIPToReceptionFolderByPriority(final File sipFile,
                                                     final String metsId,
                                                     TYPE priority) throws IOException {
        String sipName = metsId + FileExtensionUtils.SIP;
        String receptionFolder = this.getReceptionFolderByPriority(priority);
        File sipInBackup = new File(receptionFolder, sipName);
        moveFile(sipFile, sipInBackup);
        return sipInBackup;
    }

    private void moveFile(File sourceFile, File destinationFile) throws IOException {
        if (sourceFile.exists()) {
            FileUtils.moveFile(sourceFile, destinationFile);
        }
    }

    /**
     * Sets a new value for the cellarConfguration.
     *
     * @param cellarConfiguration
     *            the cellarConfguration to set
     */
    public final void setCellarConfiguration(final ICellarConfiguration cellarConfiguration) {
        this.cellarConfiguration = cellarConfiguration;
    }

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public String storeArchiveInDailyReceptionFolder(final byte[] sipObjectStream) throws IOException {
        throw NotImplementedExceptionBuilder.get().build();
    }

    /**
     * {@inheritDoc}
     *
     */
    @Override
    public String storeArchiveInAuthenticOJReceptionFolder(final byte[] sipObjectStream) throws IOException {
        throw NotImplementedExceptionBuilder.get().build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String uncompress(final File sipObjectArchive) throws UncompressException {
        return this.uncompress(sipObjectArchive, false, false);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public String uncompressMetadataOnly(final File sipObjectArchive) throws UncompressException {
        return this.uncompress(sipObjectArchive, false, true);
    }

    /**
     * Uncompress.
     *
     * @param sipObjectArchive the sip object archive
     * @param silent the silent
     * @param metadataFileFilter true to only extract metadata files (.mets.xml and .rdf)
     * @return the string
     * @throws UncompressException the uncompress exception
     */
    private String uncompress(final File sipObjectArchive, final boolean silent, final boolean metadataFileFilter) throws UncompressException {
        // this exception is silent when it can be due to a file not being copied completely
        if (FileSystemUtils.isFileLocked(sipObjectArchive)) {
            throw ExceptionBuilder.get(UncompressException.class).withCode(CoreErrors.E_018)
                    .withMessage("The SIP to uncompress is used by another process (probably still being copied): SIP[{}]")
                    .withMessageArgs(sipObjectArchive.getName()).silent(silent).build();
        }

        ZipInputStream zis = null;
        if (StringUtils.isEmpty(sipObjectArchive.getName())) {
            throw ExceptionBuilder.get(UncompressException.class).withCode(CoreErrors.E_011).build();
        }
        final String folderName = StringUtils.removeEnd(sipObjectArchive.getName(), FileExtensionUtils.SIP);
        final File temporaryWorkFolder = new File(this.cellarConfiguration.getCellarFolderTemporaryWork());

        final long startTime = System.currentTimeMillis();
        //Log the start of process
        AuditBuilder.get(AuditTrailEventProcess.Ingestion).withAction(AuditTrailEventAction.UnzippingPackage)
                .withType(AuditTrailEventType.Start)
                .withMessage("Unzipping package ({} MB)...")
                .withMessageArgs(String.format("%.4f", (double) sipObjectArchive.length() / FileUtils.ONE_MB))
                .withLogger(LOG).withLogDatabase(false).logEvent();
        try {
            final File location = new File(temporaryWorkFolder, folderName);
            if (!location.exists()) {
                try {
                    location.mkdirs();
                } catch (final Exception e) {
                    throw ExceptionBuilder.get(UncompressException.class).withCode(CoreErrors.E_010).withCause(e)
                            .withMessage("Folder [{}] where ZIP must be extracted cannot be created.")
                            .withMessageArgs(location.getParentFile()).build();
                }
            }

            FileInputStream fis;
            try {
                fis = new FileInputStream(sipObjectArchive);
            } catch (final FileNotFoundException e) {
                FileUtils.deleteQuietly(location);
                throw ExceptionBuilder.get(UncompressException.class).withCode(CoreErrors.E_013).withCause(e)
                        .withMessage("The SIP to uncompress is not available anymore: SIP[{}]").withMessageArgs(sipObjectArchive.getName())
                        .build();
            }

            zis = new ZipInputStream(fis);
            try {
                ZipEntry entry = zis.getNextEntry();
                if (entry == null) {
                    throw ExceptionBuilder.get(UncompressException.class).withCode(CoreErrors.E_012).withMessage("[{}]")
                            .withMessageArgs(sipObjectArchive.getName()).build();
                }

                while (entry != null) {
                    if (location.exists()) {
                        if (entry.isDirectory()) {
                            Path path = Paths.get(location + File.separator + entry.getName());
                            Files.createDirectories(path);
                        } else {
                        	if (metadataFileFilter && !isMetadataFile(entry)) {
                    			entry = zis.getNextEntry();
                        		continue;
                        	}
                            final File file = new File(location, ZipUtils.validateFilenameInDir(entry.getName()));
                            String parentFolder = file.getParent();
                            createFolderIfNotExistAndReturnPath(new File(parentFolder));
                            copyFromZipStreamToDestinationStream(zis, new FileOutputStream(file));
                        }
                    }
                    entry = zis.getNextEntry();
                }
            } catch (final Exception e) {
                FileUtils.deleteQuietly(location);
                // this exception is silent when it can be due to a file not being copied completely
                throw ExceptionBuilder.get(UncompressException.class).withCode(CoreErrors.E_012).withMessage("File [{}] is corrupted.")
                        .withMessageArgs(sipObjectArchive.getName()).withCause(e).silent(silent).root(true).build();
            } finally {
                org.apache.commons.io.IOUtils.closeQuietly(fis);
            }
        } finally {
            org.apache.commons.io.IOUtils.closeQuietly(zis);
        }

        //Log the end of process
        AuditBuilder.get(AuditTrailEventProcess.Ingestion).withAction(AuditTrailEventAction.UnzippingPackage)
                .withType(AuditTrailEventType.End).withDuration(System.currentTimeMillis() - startTime)
                .withMessage("Unzipping package done in {} ms.")
                .withMessageArgs(System.currentTimeMillis() - startTime)
                .withLogger(LOG).withLogDatabase(false).logEvent();

        return folderName;
    }
    
    /**
     * Checks if the provided ZIP entry is a metadata file.
     * @param entry the ZIP entry to check
     * @return whether the ZIP entry is a metadata file.
     */
    private boolean isMetadataFile(ZipEntry entry) {
    	return entry.getName().endsWith(FileExtensionUtils.METS) ||
				entry.getName().endsWith(FileExtensionUtils.RDF);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String uncompressQuietly(final File sipObjectArchive) throws UncompressException {
        return this.uncompress(sipObjectArchive, true, false);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public String uncompressQuietlyMetadataOnly(final File sipObjectArchive) throws UncompressException {
        return this.uncompress(sipObjectArchive, true, true);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public File getSipFromReceptionFolder(String sipFileName, TYPE receptionFolderType) {
        String receptionFolder = getReceptionFolderByPriority(receptionFolderType);
        if (receptionFolder == null) return null;
        return new File(receptionFolder, sipFileName);
    }

    private String getReceptionFolderByPriority(TYPE receptionFolderPriority) {
        String receptionFolder = null;
        switch(receptionFolderPriority) {
            case AUTHENTICOJ:
                receptionFolder = this.cellarConfiguration.getCellarFolderAuthenticOJReception();
                break;
            case DAILY:
                receptionFolder = this.cellarConfiguration.getCellarFolderDailyReception();
                break;
            case BULK:
                receptionFolder = this.cellarConfiguration.getCellarFolderBulkReception();
                break;
            case BULK_LOWPRIORITY:
                receptionFolder = this.cellarConfiguration.getCellarFolderBulkLowPriorityReception();
                break;
            default:
                return null;
        }
        return receptionFolder;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moveToResponseErrorFolder(List<File> files, TYPE folderType) {
		String errorResponseFolderPath = new File(getResponseFolder(folderType), "error").getAbsolutePath();
		for (File file : files) {
			if (file.isFile()) {
				File targetFile = getNonExistingTargetFile(file.getName(), errorResponseFolderPath);
				try {
					moveFile(file, targetFile);
				} catch (IOException e) {
					LOG.warn("Could not move file [{}] to the {} error response folder.", file, folderType);
				}
			}
			else if (file.isDirectory()) {
				File targetDirectory = getNonExistingTargetDirectory(file.getName(), errorResponseFolderPath);
				try {
					moveFolder(file, targetDirectory);
				} catch (IOException e) {
					LOG.warn("Could not move folder [{}] to the {} error response folder.", file, folderType);
				}
			}
		}
    }
    
    /**
     * Returns the response folder corresponding to the provided
     * folder type.
     * @param folderType the folder type.
     * @return the response folder corresponding to the provided
     * folder type.
     */
    private File getResponseFolder(TYPE folderType) {
    	switch(folderType) {
	    	case AUTHENTICOJ:
	    		return getResponseAuthenticOJFolder();
	    	case DAILY:
	    		return getResponseDailyFolder();
	    	case BULK:
	    		return getResponseBulkFolder();
	    	case BULK_LOWPRIORITY:
	    		return getResponseBulkLowPriorityFolder();
	    	default:
	    		return null;
    	}
    }
    
}
