/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.dao.impl
 *        FILE : StructMapBulkResponseDao.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 19-06-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.dao.StructMapResponseDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

/**
 * <class_description> Implementation of the {@link StructMapResponseDao} for the bulk ingestions.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 19-06-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
@Repository("structMapBulkResponseDao")
public class StructMapBulkResponseDao extends AbstractStructMapResponseDao {
    private ICellarConfiguration cellarConfiguration;

    @Autowired
    public StructMapBulkResponseDao(@Qualifier("cellarConfiguration") ICellarConfiguration cellarConfiguration) {
        this.cellarConfiguration = cellarConfiguration;
    }

    protected String resolveResponseFolderPath() {
        return this.cellarConfiguration.getCellarFolderBulkResponse();
    }
}
