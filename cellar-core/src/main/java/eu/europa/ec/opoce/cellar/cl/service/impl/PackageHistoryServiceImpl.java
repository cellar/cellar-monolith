package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.cl.dao.PackageHistoryDao;
import eu.europa.ec.opoce.cellar.cl.domain.enums.ProcessStatus;
import eu.europa.ec.opoce.cellar.cl.domain.package_history.PackageHistory;
import eu.europa.ec.opoce.cellar.cl.service.client.PackageHistoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.Date;

/**
 * <class_description> Service for logging entries in the 'PACKAGE_HISTORY' table.
 *
 * ON : 14-06-2023
 *
 * @author EUROPEAN DYNAMICS S.A.
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class PackageHistoryServiceImpl implements PackageHistoryService {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(PackageHistoryServiceImpl.class);
    
    private final PackageHistoryDao packageHistoryDao;
    
    @Autowired
    public PackageHistoryServiceImpl(PackageHistoryDao packageHistoryDao) {
        this.packageHistoryDao = packageHistoryDao;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public PackageHistory saveEntry(PackageHistory packageHistory) {
        try{
            this.packageHistoryDao.saveObject(packageHistory);
            return packageHistory;
        }
        catch (final Exception e) {
            // Log error message
            LOGGER.error("An exception occurred while saving PackageHistory '" + packageHistory + "'.", e);
            // Mark this event transaction with rollback: this allows us not to throw the exception up and handle the rollback here
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return null;
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public PackageHistory getEntry(Long id) {
        try{
            return this.packageHistoryDao.getObject(id);
        }
        catch (Exception e) {
            // Log error message
            LOGGER.error("An exception occurred while retrieving the PackageHistory with id : " + id, e);
            return null;
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public PackageHistory findByPackageNameAndDate(String metsPackageName, Date receivedDate) {
        try{
        return this.packageHistoryDao.getByPackageNameAndDate(metsPackageName, receivedDate);
        }
        catch (Exception e) {
            // Log error message
            LOGGER.error("An exception occurred while retrieving the PackageHistory with metsPackageName : "
                    + metsPackageName +" and receivedDate : " + receivedDate, e);
            return null;
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public PackageHistory updatePackageHistoryStatus(PackageHistory packageHistory, ProcessStatus newStatus) {
    	try{
            PackageHistory clonePackageHistory = new PackageHistory(packageHistory);
    		clonePackageHistory.setPreprocessIngestionStatus(newStatus);
            return this.packageHistoryDao.updateObject(clonePackageHistory);
        }
        catch (Exception e) {
            // Log error message
            LOGGER.error("An exception occurred while updating the PREPROCESS_INGESTION_STATUS '" + newStatus
                    + "' of PackageHistory '" + packageHistory + "'.", e);
            // Mark this event transaction with rollback: this allows us not to throw the exception up and handle the rollback here
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            // Return original object
            return packageHistory;
        }
    }
}
