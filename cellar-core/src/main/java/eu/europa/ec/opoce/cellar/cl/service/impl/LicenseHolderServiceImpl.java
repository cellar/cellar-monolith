/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *             FILE : LicenseHolderServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 5, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.ccr.service.database.DefaultTransactionService;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.dao.ExtractionConfigurationDao;
import eu.europa.ec.opoce.cellar.cl.dao.ExtractionExecutionDao;
import eu.europa.ec.opoce.cellar.cl.dao.ExtractionWorkIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.domain.common.CellarPaginatedList;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionConfiguration.CONFIGURATION_TYPE;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionWorkIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState.AbstractExecution;
import eu.europa.ec.opoce.cellar.cl.service.client.ExtractionExecutionService;
import eu.europa.ec.opoce.cellar.cl.service.client.LicenseHolderService;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.server.admin.resource.ResourceConfigInvoker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <class_description> Implementation of the {@link LicenseHolderService}. <br/>
 * <br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc. <br/>
 * <br/>
 * ON : Oct 5, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class LicenseHolderServiceImpl extends DefaultTransactionService implements LicenseHolderService {

	private final static int BATCH_SIZE = 1000;

	/**
	 * Dao for the the Entity {@link ExtractionConfiguration}.
	 */
	@Autowired
	private ExtractionConfigurationDao extractionConfigurationDao;

	/**
	 * Dao for the the Entity {@link ExtractionExecution}.
	 */
	@Autowired
	private ExtractionExecutionDao extractionExecutionDao;

	/**
	 * Dao for the the Entity {@link ExtractionWorkIdentifier}.
	 */
	@Autowired
	private ExtractionWorkIdentifierDao extractionWorkIdentifierDao;

	/**
	 * Language manager.
	 */
	@Autowired
	private ResourceConfigInvoker languageService;

	/**
	 * Extraction service.
	 */
	@Autowired
	private ExtractionExecutionService extractionExecutionService;

	/**
	 * The Cellar configuration storing runtime parameters.
	 */
	@Autowired
	@Qualifier("cellarConfiguration")
	private ICellarConfiguration cellarConfiguration;

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public void addExtractionConfiguration(final String configurationName, final String extractionTypeString,
			final String extractionLanguage, final String path, final Date startDate, final String sparqlQuery) {
		final CONFIGURATION_TYPE configurationType = CONFIGURATION_TYPE.valueOf(extractionTypeString);
		this.addExtractionConfiguration(configurationName, configurationType, extractionLanguage, path, startDate,
				sparqlQuery);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public void addAdhocExtractionConfiguration(final String configurationName, final String extractionLanguage,
			final String path, final Date executionStartDate, final String sparqlQuery) {
		final String cleanedPath = AbstractExecution.cleanPath(path);

		if (StringUtils.isBlank(cleanedPath)) {
			throw ExceptionBuilder.get(CellarException.class).withMessage("The normalized path is incorrect: '{}'.")
					.withMessageArgs(cleanedPath).build();
		}

		// adds the configuration
		final ExtractionConfiguration extractionConfiguration = new ExtractionConfiguration(configurationName,
				CONFIGURATION_TYPE.STD, extractionLanguage, cleanedPath, executionStartDate, sparqlQuery);
		this.extractionConfigurationDao.addExtractionConfiguration(extractionConfiguration);

		// generates and add the execution
		final ExtractionExecution extractionExecution = extractionConfiguration.getExtractionExecution();
		this.extractionExecutionDao.addExtractionExecution(extractionExecution);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public void addNextExtractionExecution(final ExtractionExecution lastExtractionExecution) {
		final ExtractionExecution extractionExecution = lastExtractionExecution.getNextExtractionExecution(); // constructs
																												// the
																												// next
																												// execution

		if (extractionExecution != null) {
			this.extractionExecutionDao.addExtractionExecution(extractionExecution);
		}

		// the next execution is generated
		lastExtractionExecution.setNextExecutionGenerated(true);
		this.extractionExecutionDao.updateObject(lastExtractionExecution);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CONFIGURATION_TYPE> getFrequencies() {
		return Arrays.asList(CONFIGURATION_TYPE.STDD, CONFIGURATION_TYPE.STDW, CONFIGURATION_TYPE.STDM);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Date> getDeadlines() {
		final List<Date> deadlines = new ArrayList<Date>(3);
		final Calendar limitCalendar = Calendar.getInstance();

		// Daily
		int keptDays = this.cellarConfiguration.getCellarServiceLicenseHolderKeptDaysDaily();
		if (keptDays > 0) {
			limitCalendar.setTime(new Date());
			limitCalendar.add(Calendar.DATE, -keptDays);
			deadlines.add(limitCalendar.getTime());
		} else {
			deadlines.add(null);
		}

		// Weekly
		keptDays = this.cellarConfiguration.getCellarServiceLicenseHolderKeptDaysWeekly();
		if (keptDays > 0) {
			limitCalendar.setTime(new Date());
			limitCalendar.add(Calendar.DATE, -keptDays);
			deadlines.add(limitCalendar.getTime());
		} else {
			deadlines.add(null);
		}

		// Monthly
		keptDays = this.cellarConfiguration.getCellarServiceLicenseHolderKeptDaysMonthly();
		if (keptDays > 0) {
			limitCalendar.setTime(new Date());
			limitCalendar.add(Calendar.DATE, -keptDays);
			deadlines.add(limitCalendar.getTime());
		} else {
			deadlines.add(null);
		}

		return deadlines;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public void updateExtractionExecution(final ExtractionExecution extractionExecution) {
		this.extractionExecutionDao.saveOrUpdateObject(extractionExecution);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public void deleteWorkIdentifiers(final Long executionId) {
		this.extractionWorkIdentifierDao.deleteWorkIdentifiersExecutionId(executionId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public void addWorkIdentifiers(final ExtractionExecution extractionExecution, final HashSet<String> workIds) {
		final List<ExtractionWorkIdentifier> workIdentifiers = new ArrayList<ExtractionWorkIdentifier>(BATCH_SIZE);
		for (final String workId : workIds) {
			workIdentifiers.add(new ExtractionWorkIdentifier(workId, extractionExecution));
			if (workIdentifiers.size() >= BATCH_SIZE) {
				this.extractionWorkIdentifierDao.addWorkIdentifierBatch(workIdentifiers);
				workIdentifiers.clear();
			}
		}

		if (workIdentifiers.size() > 0) {
			this.extractionWorkIdentifierDao.addWorkIdentifierBatch(workIdentifiers);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public List<ExtractionExecution> deleteExtractionExecutions(final CONFIGURATION_TYPE configurationType,
			final int numberOfDays) {
		final List<ExtractionExecution> extractionExecutions = this.extractionExecutionDao
				.deleteExtractionExecutions(configurationType, numberOfDays, BATCH_SIZE);
		this.addLanguageBean(extractionExecutions);
		return extractionExecutions;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public void deleteExtractionConfiguration(final Long configurationId) {
		this.extractionConfigurationDao.deleteExtractionConfiguration(configurationId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public void deleteExtractionConfiguration(final Long configurationId, final boolean cleanFinalArchive) {
		this.extractionExecutionService.cleanAndDeleteExtractionConfiguration(configurationId, cleanFinalArchive);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ExtractionWorkIdentifier> findWorkIdentifiers(final Long extractionExecutionId, final int first,
			final int last) {
		return this.extractionWorkIdentifierDao.findWorkIdentifiers(extractionExecutionId, first, last);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ExtractionWorkIdentifier> findWorkIdentifiers(final Long extractionExecutionId) {
		return this.extractionWorkIdentifierDao.findWorkIdentifiers(extractionExecutionId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ExtractionWorkIdentifier> findMatchWorkIdWorkIdentifiers(final Long extractionExecutionId,
			final String workIdFilter) {
		return this.extractionWorkIdentifierDao.findMatchWorkIdWorkIdentifiers(extractionExecutionId, workIdFilter);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CellarPaginatedList<ExtractionWorkIdentifier> findWorkIdentifiers(final Long extractionExecutionId,
			final CellarPaginatedList<ExtractionWorkIdentifier> workIdentifiersList) {
		workIdentifiersList.setFullListSize(
				this.extractionWorkIdentifierDao.getWorkIdentifierCount(extractionExecutionId).intValue());
		return this.extractionWorkIdentifierDao.findWorkIdentifiers(extractionExecutionId, workIdentifiersList);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public List<ExtractionExecution> updatePendingExtractionExecutions(final int batchSize) {
		final List<ExtractionExecution> extractionExecutions = this.extractionExecutionDao
				.updatePendingExtractionExecutions(batchSize, this.cellarConfiguration.getCellarInstanceId());
		this.addLanguageBean(extractionExecutions);
		return extractionExecutions;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public int updateNewExtractionExecutions() {
		return this.extractionExecutionDao.updateNewExtractionExecutions(BATCH_SIZE);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public ExtractionExecution updatePendingExtractionExecution() {
		final ExtractionExecution extractionExecution = this.extractionExecutionDao
				.updatePendingExtractionExecution(this.cellarConfiguration.getCellarInstanceId());

		if (extractionExecution == null) {
			return null;
		}

		this.addLanguageBean(extractionExecution.getExtractionConfiguration());
		return extractionExecution;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Long getWorkIdentifierCount(final Long executionId) {
		return this.extractionWorkIdentifierDao.getWorkIdentifierCount(executionId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getArchiveNameOverview(final CONFIGURATION_TYPE configurationType,
			final String languageIsoCodeThreeChar) {
		String languageCode = null;
		if (languageIsoCodeThreeChar != null) {
			final LanguageBean languageBean = this.languageService.getLanguage(languageIsoCodeThreeChar);
			if (languageBean != null) {
				languageCode = languageBean.getCode();
			}
		}

		return AbstractExecution.constructArchiveFileName(configurationType, languageCode, null, null, null);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getArchivePathOverview(final CONFIGURATION_TYPE configurationType,
			final String languageIsoCodeThreeChar) {
		String languageCode = null;
		if (languageIsoCodeThreeChar != null) {
			final LanguageBean languageBean = this.languageService.getLanguage(languageIsoCodeThreeChar);
			if (languageBean != null) {
				languageCode = languageBean.getCode();
			}
		}

		return AbstractExecution.constructRelativeArchiveFilePathBaseUpdate(configurationType, languageCode, null);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Object> findJobExtractionConfigurations() {
		final List<Object> configurations = this.extractionConfigurationDao.findJobExtractionConfigurations();
		this.addLanguageCode(configurations, 2);
		return configurations;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Object> findAdhocExtractionConfigurations() {
		final List<Object> configurations = this.extractionConfigurationDao.findAdhocExtractionConfigurations();
		this.addLanguageBean(configurations, 0);
		return configurations;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean[][] getExtractionExecutionsOptions(final List<ExtractionExecution> extractionExecutions) {
		final boolean[][] extractionExecutionsOptions = new boolean[extractionExecutions.size()][2];
		// constructs the array of actions for the extractionExecutions
		int i = 0;
		for (final ExtractionExecution extractionExecution : extractionExecutions) {
			extractionExecutionsOptions[i][0] = extractionExecution.isStepRestartable();
			extractionExecutionsOptions[i][1] = extractionExecution.isRestartable();
			i++;
		}

		return extractionExecutionsOptions;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean[][] getExtractionExecutionsOptions(final List<Object> extractionExecutions, final int index) {
		Object[] values = null;
		ExtractionExecution extractionExecution = null;
		final boolean[][] extractionExecutionsOptions = new boolean[extractionExecutions.size()][2];

		int i = 0;
		for (final Object e : extractionExecutions) {
			values = (Object[]) e;
			extractionExecution = (ExtractionExecution) values[index];
			extractionExecutionsOptions[i][0] = extractionExecution.isStepRestartable();
			extractionExecutionsOptions[i][1] = extractionExecution.isRestartable();
			i++;
		}

		return extractionExecutionsOptions;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ExtractionConfiguration findExtractionConfiguration(final Long id) {
		final ExtractionConfiguration extractionConfiguration = this.extractionConfigurationDao
				.findExtractionConfiguration(id);
		this.addLanguageBean(extractionConfiguration);
		return extractionConfiguration;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ExtractionExecution> findExtractionExecutions(final Long extractionConfigurationId) {
		final List<ExtractionExecution> extractionExecutions = this.extractionExecutionDao
				.findExtractionExecutions(extractionConfigurationId);
		this.addLanguageBean(extractionExecutions);
		return extractionExecutions;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Object> findExtractionExecutionsNumberOfDocuments(final Long extractionConfigurationId) {
		return this.extractionExecutionDao.findExtractionExecutionsNumberOfDocuments(extractionConfigurationId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ExtractionExecution findExtractionExecution(final Long id) {
		return this.extractionExecutionDao.findExtractionExecution(id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public void restartStepExtractionExecution(final Long id) {
		this.extractionExecutionDao.restartStepExtractionExecution(id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public int restartStepExtractionExecution() {
		return this.extractionExecutionDao.restartStepExtractionExecution();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public void restartExtractionExecution(final Long id) {
		this.extractionExecutionDao.restartExtractionExecution(id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public int restartExtractionExecution() {
		return this.extractionExecutionDao.restartExtractionExecution();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isStartDateValid(final CONFIGURATION_TYPE configurationType, final Date startDate) {
		int keptDays = 0;
		switch (configurationType) { // selects the number of days corresponding
										// to the frequency
		case STDD:
			keptDays = this.cellarConfiguration.getCellarServiceLicenseHolderKeptDaysDaily();
			break;
		case STDW:
			keptDays = this.cellarConfiguration.getCellarServiceLicenseHolderKeptDaysWeekly();
			break;
		case STDM:
			keptDays = this.cellarConfiguration.getCellarServiceLicenseHolderKeptDaysMonthly();
			break;
		// should never get here!
		default:
			break;
		}
		final Calendar limitCalendar = Calendar.getInstance();
		limitCalendar.setTime(new Date());
		limitCalendar.add(Calendar.DATE, -keptDays);

		return startDate.compareTo(limitCalendar.getTime()) > 0;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	public int cleanExtractionExecutions(final String instanceId) {
		return this.extractionExecutionDao.cleanExtractionExecutions(BATCH_SIZE, instanceId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getAbsoluteArchiveFilePath(final ExtractionExecution extractionExecution) {
		if (StringUtils.isBlank(extractionExecution.getArchiveFilePath())) {
			return null;
		}

		if (extractionExecution.getExtractionConfiguration().getConfigurationType() == CONFIGURATION_TYPE.STD) {
			return this.cellarConfiguration.getCellarFolderLicenseHolderArchiveAdhocExtraction()
					+ extractionExecution.getArchiveFilePath();
		} else {
			return this.cellarConfiguration.getCellarFolderLicenseHolderArchiveExtraction()
					+ extractionExecution.getArchiveFilePath();
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getAbsoluteArchiveTempDirectoryPath(final ExtractionExecution extractionExecution) {
		if (StringUtils.isNotBlank(extractionExecution.getArchiveTempDirectoryPath())) {
			return this.cellarConfiguration.getCellarFolderLicenseHolderTemporaryArchive()
					+ extractionExecution.getArchiveTempDirectoryPath();
		}

		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String getAbsoluteSparqlTempFilePath(final ExtractionExecution extractionExecution) {
		if (StringUtils.isNotBlank(extractionExecution.getSparqlTempFilePath())) {
			return this.cellarConfiguration.getCellarFolderLicenseHolderTemporarySparql()
					+ extractionExecution.getSparqlTempFilePath();
		}

		return null;
	}

	/**
	 * Sets the language management service.
	 * 
	 * @param languageService
	 *            the new service to set
	 */
	public void setLanguageService(final ResourceConfigInvoker languageService) {
		this.languageService = languageService;
	}

	/**
	 * Sets the cellar configuration.
	 * 
	 * @param cellarConfiguration
	 *            the configuration to set
	 */
	public void setCellarConfiguration(final ICellarConfiguration cellarConfiguration) {
		this.cellarConfiguration = cellarConfiguration;
	}

	/**
	 * {@inheritDoc}
	 */
	private void addExtractionConfiguration(final String configurationName, final CONFIGURATION_TYPE configurationType,
			final String extractionLanguage, final String path, final Date startDate, final String sparqlQuery) {
		final String cleanedPath = AbstractExecution.cleanPath(path);

		if (StringUtils.isBlank(cleanedPath)) {
			throw ExceptionBuilder.get(CellarException.class).withMessage("The normalized path is incorrect: '{}'.")
					.withMessageArgs(cleanedPath).build();
		}

		// adds the configuration
		final ExtractionConfiguration extractionConfiguration = new ExtractionConfiguration(configurationName,
				configurationType, extractionLanguage, cleanedPath, startDate, sparqlQuery);
		this.extractionConfigurationDao.addExtractionConfiguration(extractionConfiguration);

		// generates and add the first execution
		final ExtractionExecution extractionExecution = extractionConfiguration.getExtractionExecution();
		this.extractionExecutionDao.addExtractionExecution(extractionExecution);
	}

	/**
	 * Adds the language beans.
	 */
	private void addLanguageBean(final List<ExtractionExecution> extractionExecutions) {
		final Set<ExtractionConfiguration> extractionConfigurations = Collections
				.newSetFromMap(new IdentityHashMap<ExtractionConfiguration, Boolean>());
		ExtractionConfiguration extractionConfiguration = null;
		for (final ExtractionExecution extractionExecution : extractionExecutions) {
			extractionConfiguration = extractionExecution.getExtractionConfiguration();
			if (extractionConfigurations.add(extractionConfiguration)) {
				this.addLanguageBean(extractionConfiguration);
			}
		}
	}

	/**
	 * Adds the language beans.
	 */
	private void addLanguageBean(final List<Object> executions, final int index) {
		Object[] values = null;
		ExtractionExecution extractionExecution = null;
		for (final Object e : executions) {
			values = (Object[]) e;
			extractionExecution = (ExtractionExecution) values[index];
			this.addLanguageBean(extractionExecution.getExtractionConfiguration());
		}
	}

	/**
	 * Adds the language codes.
	 */
	private void addLanguageCode(final List<Object> configurations, final int index) {
		Object[] values = null;
		LanguageBean languageBean = null;
		for (final Object c : configurations) {
			values = (Object[]) c;
			languageBean = this.languageService.getLanguage((String) values[index]);
			if (languageBean != null) {
				values[index] = languageBean.getCode();
			}
		}
	}

	/**
	 * Adds the language bean.
	 */
	private void addLanguageBean(final ExtractionConfiguration extractionConfiguration) {
		extractionConfiguration
				.setLanguageBean(this.languageService.getLanguage(extractionConfiguration.getExtractionLanguage()));
	}
}
