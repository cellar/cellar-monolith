/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.support.metadata.impl
 *             FILE : BusinessMetadataBackupFactory.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 17-05-2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.support.metadata.impl;

import eu.europa.ec.opoce.cellar.cl.domain.response.DigitalObjectOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.IOperation;
import eu.europa.ec.opoce.cellar.common.factory.IFactory;
import eu.europa.ec.opoce.cellar.common.metadata.IBusinessMetadataBackupService;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * <class_description> Interface for factories that choose between implementations of {@link eu.europa.ec.opoce.cellar.common.metadata.IBusinessMetadataBackupService}
 * on the basis of the provided {@link operation} parameter.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 17-05-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component("businessMetadataBackupFactory")
public class BusinessMetadataBackupFactory implements
        IFactory<IBusinessMetadataBackupService<StructMap, IOperation<DigitalObjectOperation>>, IOperation<DigitalObjectOperation>> {

    private final ContentStreamService contentStreamService;

    @Autowired
    public BusinessMetadataBackupFactory(ContentStreamService contentStreamService) {
        this.contentStreamService = contentStreamService;
    }

    @Override
    public IBusinessMetadataBackupService<StructMap, IOperation<DigitalObjectOperation>> create(final IOperation<DigitalObjectOperation> operation) {
        switch (operation.getOperationType()) {
            case APPEND:
                return new AppendBusinessMetadataBackupService(contentStreamService);
            case UPDATE:
                return new UpdateBusinessMetadataBackupService(contentStreamService);
            case DELETE:
                return new DeleteBusinessMetadataBackupService(contentStreamService);
            default:
                return new NoOpBusinessMetadataBackupService(contentStreamService);
        }
    }
}
