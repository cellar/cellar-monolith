/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ccr.service.client
 *             FILE : ContentLanguageService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jul 31, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.service.client;

import eu.europa.ec.opoce.cellar.cmr.CellarResource;

/**
 * <class_description> Content stream languages service interface.
 * <br/><br/>
 * ON : Jul 31, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface ContentLanguageService {

    /**
     * Return the content languages of the identified resource in a comma separated list.
     * @param resource the current resource
     * @return the content languages of the identified resource in a comma separated list
     */
    String getContentLanguages(CellarResource resource);
}
