/**
 * 
 */
package eu.europa.ec.opoce.cellar.support;

import javax.xml.bind.JAXBException;

/**
 * @author dsavares
 * 
 */
public class ObjectDatastreamsUtils extends JaxbUtils {

    public static final String NAME_SPACE = "http://www.s3.info/definitions/1/0/access/";

    public static ObjectDatastreams unmarshall(String sparqlString) throws JAXBException {
        return unmarshall(sparqlString, ObjectDatastreams.class);
    }

}
