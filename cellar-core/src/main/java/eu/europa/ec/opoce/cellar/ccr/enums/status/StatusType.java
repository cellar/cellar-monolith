package eu.europa.ec.opoce.cellar.ccr.enums.status;

/**
 * Enumerates the different types of Status types that can exist.
 *
 * @author EUROPEAN DYNAMICS
 */
public enum StatusType {
    PACKAGE_LEVEL,
    OBJECT_LEVEL,
    LOOKUP
}
