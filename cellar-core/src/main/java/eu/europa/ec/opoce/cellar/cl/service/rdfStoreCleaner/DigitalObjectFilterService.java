/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.rdfStoreCleaner
 *             FILE : DigitalObjectFilterService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 12, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.rdfStoreCleaner;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Statement;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 12, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface DigitalObjectFilterService {

    void filter(final Model model);

    boolean isFiltered(final Statement s);
}
