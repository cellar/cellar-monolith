/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor
 *             FILE : LastModificationDateUpdateBatchJobProcessor.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 7, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-06-07 07:49:00 +0200 (Wed, 07 Jun 2017) $
 *          VERSION : $LastChangedRevision: $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.server.admin.lastmodification.LastModificationDateUpdateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import java.util.Date;

/**
 * @author ARHS Developments
 */
@Configurable
public class LastModificationDateUpdateBatchJobProcessor extends AbstractBatchJobProcessor {

    @Autowired
    private LastModificationDateUpdateService dateUpdateService;

    @Autowired
    private ICellarConfiguration cellarConfiguration;

    /**
     * Instantiates a new abstract batch job processor.
     *
     * @param batchJob the batch job
     */
    public LastModificationDateUpdateBatchJobProcessor(BatchJob batchJob) {
        super(batchJob);
    }

    @Override
    protected void doExecute(String workId, BatchJob batchJob) {
        String id = workId;
        if (workId.startsWith("http:")) {
            id = parse(workId);
        }
        dateUpdateService.updateLastModificationDate(id, new Date());
    }

    private String parse(String workId) {
        return workId.replace(cellarConfiguration.getCellarUriResourceBase() + "/", "cellar:");
    }

    @Override
    protected BatchJob.BATCH_JOB_TYPE getBatchJobType() {
        return BatchJob.BATCH_JOB_TYPE.LAST_MODIFICATION_DATE_UPDATE;
    }

    @Override
    protected void preExecute() {
        batchJobService.updateProcessingBatchJob(batchJob);
    }

    @Override
    protected void postExecute() {
        batchJobService.updateDoneBatchJob(batchJob);
    }
}
