package eu.europa.ec.opoce.cellar.ccr.validator;

import java.util.List;

import org.springframework.validation.Validator;

/**
 * <p>The property validators are used to validate the value of properties. It can be used to verify that the value of a parameter is set to an authorized value.
 * 
 * <p>For instance, the list of authorized values is:
 * 
 * <ul>
 *    <li>application/pdf</li>
 *    <li>application/xml</li>
 * </ul>
 * 
 * A <i>validator</i> can be used to make sure the specified value is one of the value in the authorized list.
 * 
 * @author lderavet
 *
 */
public interface PropertyValidator extends Validator {

    /**
     * Set the list of authorized values.
     * 
     * @param values the list of authorized values.
     */
    void setAuthorizedValues(List<String> values);
}
