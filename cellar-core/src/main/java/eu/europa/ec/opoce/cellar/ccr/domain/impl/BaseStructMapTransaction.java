/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ccr.domain.impl
 *             FILE : BaseStructMapTransaction.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 23-12-2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.domain.impl;

import eu.europa.ec.opoce.cellar.ccr.service.database.DefaultTransactionService;
import eu.europa.ec.opoce.cellar.cl.domain.audit.*;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.cl.domain.response.StructMapOperation;
import eu.europa.ec.opoce.cellar.cl.domain.structmap_status_history.StructMapStatusHistory;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsPackage;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMapTransaction;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 23-12-2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public abstract class BaseStructMapTransaction extends DefaultTransactionService
        implements StructMapTransaction<StructMapOperation>, IAuditableActionResolver, IAuditableObjectIdentifierResolver,
        IAuditableStructMapStatusHistoryResolver, IAuditableSIPResourceResolver {

    /**
     * @see eu.europa.ec.opoce.cellar.cl.domain.audit.IAuditableActionResolver#resolveAction(java.lang.Object[])
     */
    @Override
    public AuditTrailEventAction resolveAction(final Object[] args) {
        return AuditTrailEventAction.valueOf(((StructMap) args[1]).resolveOperationSubType().toString());
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.domain.audit.IAuditableObjectIdentifierResolver#resolveObjectIdentifier(java.lang.Object[])
     */
    @Override
    public String resolveObjectIdentifier(final Object[] args) {
        return ((MetsPackage) args[0]).getZipFile().getName();
    }
    
    /**
     *
     * @see eu.europa.ec.opoce.cellar.cl.domain.audit.IAuditableStructMapStatusHistoryResolver#resolveStructMapStatusHistory(java.lang.Object[])
     */
    @Override
    public StructMapStatusHistory resolveStructMapStatusHistory(final Object[] args){
        return ((StructMapStatusHistory) args[4]);
    }
    
    /**
     *
     * @see IAuditableSIPResourceResolver#resolveSIPResource(java.lang.Object[])
     */
    @Override
    public SIPResource resolveSIPResource(final Object[] args){
        return ((SIPResource) args[3]);
    }

}
