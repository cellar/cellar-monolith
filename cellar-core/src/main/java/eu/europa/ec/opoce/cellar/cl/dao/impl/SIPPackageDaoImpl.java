/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.dao.impl
 *        FILE : SIPPackageDaoImpl.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 03-12-2021
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2021 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import eu.europa.ec.opoce.cellar.cl.dao.SIPPackageDao;
import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.SIPPackage;
import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.SIPPackageProcessingStatus;
import eu.europa.ec.opoce.cellar.common.util.Counter;
import eu.europa.ec.opoce.cellar.common.util.IterableUtils;

/**
 * 
 * @author EUROPEAN DYNAMICS S.A.
 */
@Repository
public class SIPPackageDaoImpl extends BaseDaoImpl<SIPPackage, Long> implements SIPPackageDao {
	
	/**
	 *{@inheritDoc}
	 */
	@Override
	public SIPPackage findBySipName(String sipName) {
		Map<String, Object> params = new HashMap<>();
		params.put("sipName", sipName);
		return super.findObjectByNamedQueryWithNamedParams("SIPPackage.findBySipName", params);
	}
	
	/**
	 *{@inheritDoc}
	 */
	@Override
	public List<SIPPackage> findBySipStatusIn(Collection<SIPPackageProcessingStatus> sipStatuses) {
		Map<String, Object> params = new HashMap<>();
		Collection<String> sipStatusValues=sipStatuses.stream().map(a->a.getValue()).collect(Collectors.toList());
		params.put("sipStatuses", sipStatusValues);
		return super.findObjectsByNamedQueryWithNamedParams("SIPPackage.findBySipStatusIn", params);
	}
	
	/**
	 *{@inheritDoc}
	 */
	@Override
	public long countBySipStatusIn(Collection<SIPPackageProcessingStatus> sipStatuses) {
		return getHibernateTemplate().execute(session -> {
				Query query = session.getNamedQuery("SIPPackage.countBySipStatusIn");
				Collection<String> sipStatusValues=sipStatuses.stream().map(a->a.getValue()).collect(Collectors.toList());
				query.setParameterList("sipStatuses", sipStatusValues);
				return Long.valueOf(query.uniqueResult().toString());
			});
	}
	
	/**
	 *{@inheritDoc}
	 */
	@Override
	public List<SIPPackage> findScheduledOrdered() {
		return super.findObjectsByNamedQuery("SIPPackage.findScheduledOrdered");
	}

	/**
	 *{@inheritDoc}
	 */
	@Override
	public int deleteByIdIn(Collection<Long> ids) {
		Map<String, Object> params = new HashMap<>();
		params.put("ids", ids);
		
		final Counter numDeleted = new Counter();
		IterableUtils.pageUp(params, this.cellarConfiguration.getCellarDatabaseInClauseParams(), innerParamMap -> 
			numDeleted.increaseBy(super.updateObjectsByNamedQueryWithNamedParams("SIPPackage.deleteByIdIn", innerParamMap))
		);
		return (int) numDeleted.getVal();
	}
	
}
