/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *             FILE : ExtractionExecutionServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 9, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ArchiveTreeManager;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ArchivingBatchRunnable;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionConfiguration.CONFIGURATION_TYPE;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution.EXECUTION_STATUS;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecutionRunnable;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState.AbstractBatch;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState.AbstractTask;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState.ExecutionStateManager;
import eu.europa.ec.opoce.cellar.cl.service.client.ExtractionExecutionService;
import eu.europa.ec.opoce.cellar.cl.service.client.LicenseHolderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * <class_description> Implementation of the {@link ExtractionExecutionService}.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 9, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class ExtractionExecutionServiceImpl implements ExtractionExecutionService {

    /**
     * Logger instance.
     */
    private static final Logger LOG = LogManager.getLogger(ExtractionExecutionServiceImpl.class);

    @Autowired
    private LicenseHolderService licenseHolderService;

    /**
     * The license holder states manager.
     */
    @Autowired
    @Qualifier("executionStateManager")
    protected ExecutionStateManager executionStateManager;

    /**
     * The Cellar configuration storing runtime parameters.
     */
    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    /**
     * Threadpool for the tasks.
     */
    @Autowired
    @Qualifier("taskExecutor")
    private TaskExecutor taskExecutor;

    /**
     * Threadpool for the batches.
     */
    @Autowired
    @Qualifier("archivingBatchExecutor")
    private TaskExecutor archivingBatchExecutor;

    /**
     * {@inheritDoc}
     */
    @Override
    public void cleanExtractionExecutions() {
        final String instanceId = this.cellarConfiguration.getCellarInstanceId();

        LOG.info("Clean the executions of {}", instanceId);

        int numberOfExtractions = this.licenseHolderService.cleanExtractionExecutions(instanceId);
        while (numberOfExtractions > 0) {
            numberOfExtractions = this.licenseHolderService.cleanExtractionExecutions(instanceId);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void cleanAndDeleteExtractionExecutions() {
        LOG.info("Clean and delete old executions");

        int keptDays = this.cellarConfiguration.getCellarServiceLicenseHolderKeptDaysDaily();
        if (keptDays > 0) {
            this.cleanAndDeleteExtractionExecutions(CONFIGURATION_TYPE.STDD, keptDays);
        }

        keptDays = this.cellarConfiguration.getCellarServiceLicenseHolderKeptDaysWeekly();
        if (keptDays > 0) {
            this.cleanAndDeleteExtractionExecutions(CONFIGURATION_TYPE.STDW, keptDays);
        }

        keptDays = this.cellarConfiguration.getCellarServiceLicenseHolderKeptDaysMonthly();
        if (keptDays > 0) {
            this.cleanAndDeleteExtractionExecutions(CONFIGURATION_TYPE.STDM, keptDays);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void cleanAndDeleteExtractionConfiguration(final Long configurationId, final boolean cleanFinalArchive) {
        final List<ExtractionExecution> extractionExecutions = this.licenseHolderService.findExtractionExecutions(configurationId);

        this.executionStateManager.getCleaningState(EXECUTION_STATUS.CLEANING).clean(extractionExecutions, cleanFinalArchive);

        this.licenseHolderService.deleteExtractionConfiguration(configurationId);
    }

    private void cleanAndDeleteExtractionExecutions(final CONFIGURATION_TYPE configurationType, final int keptDays) {
        List<ExtractionExecution> extractionExecutions = null;
        try {
            while (true) {
                extractionExecutions = this.licenseHolderService.deleteExtractionExecutions(configurationType, keptDays);
                if (extractionExecutions == null || extractionExecutions.isEmpty())
                    break;

                this.executionStateManager.getCleaningState(EXECUTION_STATUS.CLEANING).clean(extractionExecutions, true);
            }
        } catch (Exception exception) {
            LOG.error("Cannot clean the executions " + configurationType + " older than " + keptDays, exception);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void scheduleExtractionExecutions() {
        LOG.info("Schedule extractions");

        try {
            int numberOfExtractions = this.licenseHolderService.updateNewExtractionExecutions();
            while (numberOfExtractions > 0)
                numberOfExtractions = this.licenseHolderService.updateNewExtractionExecutions();
        } catch (Exception exception) {
            LOG.error("Cannot schedule the executions", exception);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void executeExtractionExecutions() {
        LOG.info("Execute extractions");

        final int batchSize = this.cellarConfiguration.getCellarServiceLicenseHolderWorkerTaskExecutorQueueCapacity();
        final List<ExtractionExecution> executions = this.licenseHolderService.updatePendingExtractionExecutions(batchSize);

        if (executions == null || executions.isEmpty())
            return;

        for (ExtractionExecution extractionExecution : executions) {
            this.addExecutionTask(extractionExecution);
        }

        ExtractionExecution execution = null;
        while (true) {

            execution = this.licenseHolderService.updatePendingExtractionExecution();
            if (execution == null)
                break;

            this.addExecutionTask(execution);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addExecutionTask(final ExtractionExecution extractionExecution) {
        LOG.info("Adding the execution {} to the taskExecutor thread pool", extractionExecution.getId());

        this.taskExecutor.execute(new ExtractionExecutionRunnable(extractionExecution, this));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addArchivingBatchTask(final ExtractionExecution extractionExecution, final int firstOfBatch, final int lastOfBatch,
            final ArchiveTreeManager archiveTreeManager, final ReentrantReadWriteLock readWriteLock,
            final AtomicInteger archivingBatchWorker) {
        LOG.info("Adding the archiving batch {} from {} to {} to the archivingBatchExecutor thread pool",
                extractionExecution.getId(), firstOfBatch, lastOfBatch);

        this.archivingBatchExecutor.execute(new ArchivingBatchRunnable(extractionExecution, firstOfBatch, lastOfBatch, archiveTreeManager,
                readWriteLock, archivingBatchWorker, this));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void executeExtractionExecution(final ExtractionExecution extractionExecution) throws Exception {
        AbstractTask task = this.executionStateManager.getTaskState(extractionExecution.getExecutionStatus());

        while (task != null)
            task = task.execute(extractionExecution);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void executeBatch(final ExtractionExecution extractionExecution, final int firstOfBatch, final int lastOfBatch,
            final ArchiveTreeManager archiveTreeManager, final ReentrantReadWriteLock readWriteLock,
            final AtomicInteger archivingBatchWorker) throws Exception {
        final AbstractBatch archivingBatchState = this.executionStateManager.getBatchState(EXECUTION_STATUS.ARCHIVING);

        archivingBatchState.execute(extractionExecution, firstOfBatch, lastOfBatch, archiveTreeManager, readWriteLock,
                archivingBatchWorker);
    }

    /**
     * {@inheritDoc}
     * @throws Exception 
     */
    @Override
    public void clean(final ExtractionExecution extractionExecution, final boolean cleanFinalArchive) throws Exception {
        this.executionStateManager.getCleaningState(EXECUTION_STATUS.CLEANING).clean(extractionExecution, cleanFinalArchive);
    }
}
