/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.scheduling.licenseHolder
 *             FILE : ExecutionCleaner.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 17, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.scheduling.licenseHolder;

import eu.europa.ec.opoce.cellar.cl.service.client.ExtractionExecutionService;
import eu.europa.ec.opoce.cellar.logging.LogContext;

import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.CMR_LICENSE_HOLDER;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 17, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@LogContext
public class ExecutionCleaner implements Runnable {

    private final ExtractionExecutionService extractionExecutionService;

    public ExecutionCleaner(final ExtractionExecutionService extractionExecutionService) {
        this.extractionExecutionService = extractionExecutionService;
    }

    @Override
    @LogContext(CMR_LICENSE_HOLDER)
    public void run() {
        extractionExecutionService.cleanAndDeleteExtractionExecutions();
    }

}
