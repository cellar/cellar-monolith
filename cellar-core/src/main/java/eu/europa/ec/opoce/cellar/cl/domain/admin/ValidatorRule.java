package eu.europa.ec.opoce.cellar.cl.domain.admin;

import javax.persistence.*;

/**
 * Maps a relation between valdiator systems and allowed validations.
 * 
 * @author dcraeye
 * 
 */
@Entity
@Table(name = "VALIDATOR_RULE")
@NamedQueries({
        @NamedQuery(name = "getByValidatorClass", query = "from ValidatorRule where VALIDATOR_CLASS = ?0"),
        @NamedQuery(name = "getSystemValidators", query = "from ValidatorRule where VALIDATOR_TYPE = 'SYSTEM'"),
        @NamedQuery(name = "getCustomValidators", query = "from ValidatorRule where VALIDATOR_TYPE = 'CUSTOM'")})
public class ValidatorRule {

    public static enum VALIDATOR_TYPE {
        SYSTEM, CUSTOM
    }

    /**
     * Internal id.
     */
    private Long id;

    /**
     * Validator classname.
     */
    private String validatorClass;

    /**
     * Type of the validator
     */
    private VALIDATOR_TYPE type;

    /**     * Key of the validator rule.
     */
    private String key;

    /**
     * Flag for enabling,disabling the validator rule.
     */
    private boolean enabled;

    /**
     * Gets the value of the id.
     * 
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    public Long getId() {
        return id;
    }

    /**
     * Set the value of the id.
     * 
     * @param id
     *            the id to set
     * @return a reference to the current instance for chaining
     */
    public ValidatorRule setId(final Long id) {
        this.id = id;
        return this;
    }

    /**
     * Get the validator class.
     * 
     * @return the system valdiator class
     */
    @Column(name = "VALIDATOR_CLASS")
    public String getValidatorClass() {
        return validatorClass;
    }

    /**
     * Sets the validator class name.
     * 
     * @param validatorClass
     *            the validator classname
     */
    public void setValidatorClass(final String validatorClass) {
        this.validatorClass = validatorClass;
    }

    /**
     * Get the validator type.
     * 
     * @return the validator type
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "VALIDATOR_TYPE")
    public VALIDATOR_TYPE getType() {
        return type;
    }

    /**
     * Sets the validator type.
     * 
     * @param type
     *            the validator type
     */
    public void setType(VALIDATOR_TYPE type) {
        this.type = type;
    }

    /**
     * Gets the key of the validator rule.
     * @return the key of the validator
     */
    @Column(name = "VALIDATOR_KEY")
    public String getKey() {
        return key;
    }

    /**
     * Set the key of the validator.
     * @param key to set.
     */
    public void setKey(String key) {
        this.key = key;
    }

    /**
     * Gets the value of the flag "enabled".
     * 
     * @return the value of the flag
     */
    @Column(name = "VALIDATOR_ENABLED", length = 1, nullable = false)
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Sets a new value for the enabled field.
     * 
     * @param enabled
     *            enable or disable the validator rule.
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

}
