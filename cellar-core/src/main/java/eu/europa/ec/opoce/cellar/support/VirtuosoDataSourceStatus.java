package eu.europa.ec.opoce.cellar.support;

import org.springframework.stereotype.Component;

/**
 * The Class VirtuosoDataSourceStatus.
 */
@Component
public class VirtuosoDataSourceStatus extends AbstractDataSourceStatus {

    /** {@inheritDoc} */
    @Override
    protected String getDataSourceName() {
        return this.cellarConfiguration.getCellarDatasourceVirtuoso();
    }

    /** {@inheritDoc} */
    @Override
    protected String getVerificationQuery() {
        return "select count(1) from DB.DBA.ADMIN_SESSION";
    }

}
