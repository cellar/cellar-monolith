package eu.europa.ec.opoce.cellar.ccr.service.client;

import eu.europa.ec.opoce.cellar.ccr.enums.status.StatusType;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;

/**
 * This interface defines the contract for handling Status Service operations.
 *
 * @author EUROPEAN DYNAMICS S.A.
 */
public interface StatusService {
    /**
     * Retrieves the response from Status Service based on the provided parameters and status type.
     *
     * @param id         The id (UUID or PID or package name) of the resource.
     * @param ingestion  A boolean value specifying whether to include ingestion-specific status information in the response or not.
     * @param indexation A boolean value specifying whether to include indexation-specific status information in the response or not.
     * @param datetime   The datetime acts as a filter on the time the corresponding package was received. Cellar returns
     *                   the latest status information less than or equal the given timestamp. If not specified, the latest status information for the object is returned.
     * @param verbosity  An integer specifying the level of verbosity of a response, with 1 corresponding to a coarse level of granularity and 2 to a detailed level of information.
     * @param statusType The type of status requested (package-level, object-level, or lookup).
     * @return A string containing the JSON status response based on the specified status type and parameters.
     * @throws RuntimeException If an invalid or unsupported status type is provided.
     */
    String getStatusResponse(String id, boolean ingestion, boolean indexation, LocalDateTime datetime, int verbosity, StatusType statusType);

    /**
     * Constructs the complete "package-level" Status Service endpoint URL with the specified UUID
     *
     * @param uuid The UUID associated with the package.
     * @return The complete package-level status service endpoint URL with the UUID as a query parameter.
     */
    String getStatusServicePackageLevelEndpointWithUuid(String uuid);

    /**
     * Constructs the complete "object-level" Status Service endpoint URL with the specified PID
     *
     * @param pid The PID associated with the package.
     * @return The complete package-level status service endpoint URL with the UUID as a query parameter.
     */
    String getStatusServiceObjectLevelEndpointWithPid(String pid);

    /**
     * Constructs the complete "lookup" Status Service endpoint URL with the specified package name
     *
     * @param packageName The packageName associated with the package.
     * @return The complete package-level status service endpoint URL with the UUID as a query parameter.
     */
    String getStatusServiceLookupEndpointWithPackageName(String packageName);

    /**
     * Constructs the complete "package-level" Cellar-proxied Status Service endpoint URL with the specified UUID
     *
     * @param uuid The UUID associated with the package.
     * @return The complete package-level status service endpoint URL with the UUID as a query parameter.
     */
    String getCellarProxiedStatusServicePackageLevelEndpointWithUuid(String uuid);

    /**
     * Constructs the complete "object-level" Cellar-proxied Status Service endpoint URL with the specified PID
     *
     * @param pid The PID associated with the package.
     * @return The complete package-level status service endpoint URL with the UUID as a query parameter.
     */
    String getCellarProxiedStatusServiceObjectLevelEndpointWithPid(String pid);

    /**
     * Constructs the complete "lookup" Cellar-proxied Status Service endpoint URL with the specified package name
     *
     * @param packageName The packageName associated with the package.
     * @return The complete package-level status service endpoint URL with the UUID as a query parameter.
     */
    String getCellarProxiedStatusServiceLookupEndpointWithPackageName(String packageName);

    void setRestTemplate(RestTemplate restTemplate);
}
