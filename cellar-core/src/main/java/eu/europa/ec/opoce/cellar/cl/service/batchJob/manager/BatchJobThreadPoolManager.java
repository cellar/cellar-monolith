/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : BatchJobThreadPoolManager.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 16 juin 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.batchJob.manager;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.common.util.BoundedPriorityBlockingQueue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Comparator;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * The Class BatchJobThreadPoolManager.
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 16 juin 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class BatchJobThreadPoolManager {

    private static final Logger LOG = LogManager.getLogger(BatchJobThreadPoolManager.class);

    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    /** The thread pool executor. */
    private ThreadPoolExecutor threadPoolExecutor;

    private final Comparator<Runnable> batchJobProcessorRunableComparator = new Comparator<Runnable>() {

        /** {@inheritDoc} */
        @Override
        public int compare(final Runnable thisOne, final Runnable other) {
            int result = 0;
            final BatchJobRunable thisRunable = (BatchJobRunable) thisOne;
            final BatchJobRunable thatRunable = (BatchJobRunable) other;
            result = thisRunable.compareTo(thatRunable);
            return result;
        }
    };

    /**
     * Initialize.
     */
    @PostConstruct
    public void initialize() {
        final Integer corePoolSize = Integer.parseInt(cellarConfiguration.getCellarServiceBatchJobPoolCorePoolSize());
        final Integer maximumPoolSize = Integer.parseInt(cellarConfiguration.getCellarServiceBatchJobPoolMaxPoolSize());
        final Integer queueSize = Integer.parseInt(cellarConfiguration.getCellarServiceBatchJobPoolQueueSize());
        final BoundedPriorityBlockingQueue<Runnable> boundedPriorityBlockingQueue = new BoundedPriorityBlockingQueue<Runnable>(queueSize,
                batchJobProcessorRunableComparator);
        final ThreadPoolExecutor.CallerRunsPolicy rejectedExecutionHandler = new ThreadPoolExecutor.CallerRunsPolicy();
        threadPoolExecutor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, 120, TimeUnit.MINUTES, boundedPriorityBlockingQueue,
                rejectedExecutionHandler);
        LOG.info("Batch job thread pool initialized: {}", threadPoolExecutor);
    }

    /**
     * Submit.
     *
     * @param batchJobRunable the batch job runable
     */
    public void submit(final BatchJobRunable batchJobRunable) {
        this.threadPoolExecutor.execute(batchJobRunable);
    }

    /**
     * Clear.
     */
    public void clear() {
        threadPoolExecutor.getQueue().clear();
    }

    /**
     * Contains.
     *
     * @param batchJobRunable the batch job runable
     * @return true, if successful
     */
    public boolean contains(final BatchJobRunable batchJobRunable) {
        return threadPoolExecutor.getQueue().contains(batchJobRunable);
    }

    public ThreadPoolExecutor getThreadPoolExecutor() {
        return threadPoolExecutor;
    }
}
