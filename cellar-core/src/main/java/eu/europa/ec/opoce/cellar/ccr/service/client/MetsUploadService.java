package eu.europa.ec.opoce.cellar.ccr.service.client;

import eu.europa.ec.opoce.cellar.ccr.mets.response.MetsResponse;
import eu.europa.ec.opoce.cellar.cl.domain.package_history.PackageHistory;
import eu.europa.ec.opoce.cellar.exception.MetsUploadException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

/**
 * This interface defines the contract for handling METS Upload operations.
 *
 * @author EUROPEAN DYNAMICS S.A.
 */
 public interface MetsUploadService {

   /**
    * Uploads a METS package and moves it to a reception folder.
    *
    * @param fileData                The METS package file data as a {@link MultipartFile}.
    * @param priorityStr             The priority as a String for processing the uploaded package.
    * @param callbackURI             The callback URI to be used for notifying the status of the package.
    * @return A {@link MetsResponse} The object representing the response of the METS upload process.
    * @throws MetsUploadException    If an error occurs during the METS upload process.
    */
    MetsResponse metsUpload(MultipartFile fileData, String priorityStr, String callbackURI);

   /**
    * Retrieve Status Service response and sends a callback request with the status JSON response to the specified callback URL.
    *
    * @param sipFile        The SIP file being processed.
    * @param isError        Flag indicating whether there was an error during ingestion.
    * @param packageHistory The package history information.
    */
    void getStatusResponseAndSendCallbackRequest(File sipFile, boolean isError, PackageHistory packageHistory);

   /**
    * Creates a new entry in the package history database with the information from the SIP file and callback URL.
    *
    * @param sipFile     The SIP file being processed.
    * @param callbackURL The URL to send the callback request.
    * @return            The UUID of the created package history entry.
    */
    PackageHistory createPackageHistoryDb(File sipFile, String callbackURL);
    void setRestTemplate(RestTemplate restTemplate);
}
