/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.ccr.domain.impl.create
 *        FILE : CreateStructMapTransaction.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 08-05-2013
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.domain.impl.create;

import eu.europa.ec.opoce.cellar.ccr.domain.impl.TransactionConfiguration;
import eu.europa.ec.opoce.cellar.ccr.strategy.WriteContentStreamStrategy;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.cl.domain.response.CreateStructMapOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.StructMapOperation;
import eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService;
import eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.PidManagerException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.List;

/**
 * <class_description> This class provides methods for the create process of a StructMap into database and S3.
 * Every digital objects (with their contents and metadatas) under a given StructMap will be created in database and S3.
 * If an error occurs during this process, the entire process is rolled back, in order that all digital objects (with metadata and content)
 * will be removed from the database and S3.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 08-05-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component("structMapCreate")
public class CreateStructMapTransaction extends AbstractCreateStructMapTransaction {

    private static final Logger LOG = LogManager.getLogger(CreateStructMapTransaction.class);

    private final PidManagerService pidManagerService;


    /**
     * Instantiates a new creates the struct map transaction.
     */
    @Autowired
    public CreateStructMapTransaction(@Qualifier("pidManagerService") PidManagerService pidManagerService) {
        // do not load any existing model (it is a create, no model is supposed to exist)
        super(TransactionConfiguration.get());
        this.pidManagerService = pidManagerService;
    }

    /**
     * Navigating the hierarchy in {@link calculatedData} in depth first traversal, create for all digital object
     * the corresponding cellar identifier in database, and the foxml to ingest to S3.
     *
     * @param calculatedData               the calculated data where to get the hierarchy from
     * @param metsDirectory                a reference to the directory where the SIP has been exploded
     * @param sipResource                  needed for audit
     * @param ingestedDigitalObjectPidList this list contains all pid which are already created
     */
    @Override
    protected void processHierarchy(final CalculatedData calculatedData, final File metsDirectory, final SIPResource sipResource,
            final List<String> ingestedDigitalObjectPidList) {
        final StructMap structMap = calculatedData.getStructMap();

        boolean success = pidManagerService.assignAndSaveCellarIdentifiers(structMap, sipResource);

        structMap.getDigitalObject().applyStrategyOnSelfAndChildren(new WriteContentStreamStrategy(metsDirectory, contentStreamService)); // TODO AJ, onSelfAndChildren ???

        this.pidManagerService.saveDatastreamIdentifiers(structMap, sipResource);

        // if the initial call to assignAndSaveCellarIdentifiers had a non-blocking failure (for instance, when a duplicated id was found but the operation was configured to ignore it)
        // it means that we have to throw a silent exception here, that is, AFTER all the ingestion process has been completed
        if (!success) {
            throw ExceptionBuilder.get(PidManagerException.class).silent(true).build();
        }
    }

    @Override
    protected StructMapOperation newStructMapOperation() {
        return new CreateStructMapOperation();
    }

}
