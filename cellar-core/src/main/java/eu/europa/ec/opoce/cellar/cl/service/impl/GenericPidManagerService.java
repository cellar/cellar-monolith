/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *        FILE : GenericPidManagerService.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 16-03-2012
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.op.cellar.api.service.ICellarResponse;
import eu.europa.ec.op.cellar.api.service.ICellarService;
import eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService;
import eu.europa.ec.opoce.cellar.common.util.XMLUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPathExpression;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * <class_description> Implementation of the {@link PidManagerService}. <br>
 * It is the same as {@link PidManagerService}, except that it implements more powerful version of methods:<br>
 * - <code>boolean isWork(final String pid)</code><br>
 * - <code>boolean isExpressionOrEvent(final String pid)</code><br>
 * - <code>boolean isExpressionOrEvent(final String pid)</code><br>
 * - <code>boolean isManifestation(final String pid)</code><br>
 * - <code>boolean isManifestationContent(final String pid)</code><br>
 * <br>
 * These methods accept in input any type of pid (cellar:*, oj:*, celex:*, etc), and they check if such pids identify the relative object type
 * (ie: work, expression or event, manifestation, or manifestation content).
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 16-03-2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service("genericPidManagerService")
public class GenericPidManagerService extends PidManagerServiceImpl {

    /** Logger instance. */
    private static final Logger LOGGER = LoggerFactory.getLogger(GenericPidManagerService.class);

    private static final String CELLARID_XPATH_EXPR = "//NOTICE/OBJECT/SAMEAS[URI/TYPE='cellar']/URI/IDENTIFIER";

    @Autowired
    private ICellarService cellarApiService;

    @Override
    public boolean isWork(final String pid) {
        final String cellarId = this.convertToCellarId(pid);
        if (cellarId == null) {
            return false;
        }
        return super.isWork(cellarId);
    }

    @Override
    public boolean isExpressionOrEvent(final String pid) {
        final String cellarId = this.convertToCellarId(pid);
        if (cellarId == null) {
            return false;
        }
        return super.isExpressionOrEvent(cellarId);
    }

    @Override
    public boolean isManifestation(final String pid) {
        final String cellarId = this.convertToCellarId(pid);
        if (cellarId == null) {
            return false;
        }
        return super.isManifestation(cellarId);
    }

    @Override
    public boolean isManifestationContent(final String pid) {
        final String cellarId = this.convertToCellarId(pid);
        if (cellarId == null) {
            return false;
        }
        return super.isManifestationContent(cellarId);
    }

    private String convertToCellarId(final String pid) {
        // if pid is already a cellar identifier, returns it
        if (pid.startsWith(ContentIdentifier.CELLAR_PREFIX + ":")) {
            return pid;
        }

        String cellarId = null;
        final Writer writer = new StringWriter();
        final List<String> pids = new ArrayList<String>();
        pids.add(pid);
        try {
            // retrieves via cellar-api call the sameAs pids linked to pid
            final ICellarResponse sameAsResponse = this.cellarApiService.getIdentifierMapping(pids);
            IOUtils.copy(sameAsResponse.getResponse(), writer, "UTF-8");
            // retrieve the cellar identifier from the sameAs pids
            DocumentBuilderFactory df = DocumentBuilderFactory.newInstance();
            df.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
            df.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
            DocumentBuilder builder = df.newDocumentBuilder();
            final XPathExpression cellarIdExpr = XMLUtils.newXPathFactory().newXPath().compile(CELLARID_XPATH_EXPR);
            cellarId = cellarIdExpr.evaluate( builder.parse(new ByteArrayInputStream(writer.toString().getBytes(StandardCharsets.UTF_8))));

        } catch (final Exception e) {
            LOGGER.error("A problem occurred while retrieving the cellarId corresponding to pid='" + pid + "'.", e);
            return null;
        } finally {
            try {
                writer.close();
            } catch (final IOException e) {
                LOGGER.warn("Unable to close response writer", e);
            }
        }

        // return the cellar identifier
        if (cellarId == null) {
            LOGGER.error("No cellarId has been retrieved corresponding to pid='{}'.", pid);
        }
        return cellarId;
    }

}
