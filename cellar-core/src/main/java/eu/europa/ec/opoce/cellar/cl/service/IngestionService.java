package eu.europa.ec.opoce.cellar.cl.service;

import eu.europa.ec.opoce.cellar.ingestion.SIPWork;

/**
 * The Interface IngestionService.
 */
public interface IngestionService {

    /**
     * Treatment on a sip file. - Uncompress the sip in the temporary work folder (=> mets) -
     * Validate the name of the sip and the mets id -
     *
     * @param sipWork the sip work
     */
    void sipTreatment(final SIPWork sipWork);

}
