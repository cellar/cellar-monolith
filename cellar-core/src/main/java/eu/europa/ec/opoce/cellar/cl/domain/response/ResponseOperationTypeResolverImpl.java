/*   Copyright (C) <2018>  <Publications Office of the European Union>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Affero General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Affero General Public License for more details.
 *
 *    You should have received a copy of the GNU Affero General Public License
 *    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *    contact: <https://publications.europa.eu/en/web/about-us/contact>
 */
package eu.europa.ec.opoce.cellar.cl.domain.response;

import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.ResponseOperationTypeResolver;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class ResponseOperationTypeResolverImpl implements ResponseOperationTypeResolver {

    @Override
    public ResponseOperationType resolve(CellarResource cellarResource, IOperation<?> operation) {
        if(operation.isCreate()) {
            return ResponseOperationType.CREATE;
        }
        IOperation<DigitalObjectOperation> doop = (IOperation<DigitalObjectOperation>) operation;
        String id = cellarResource.getCellarId();
        if (cellarResource.getCellarType() == DigitalObjectType.ITEM) {
            id = id.substring(0, id.lastIndexOf("/"));
        }

        for (Map.Entry<String, DigitalObjectOperation> e : doop.getOperations().entrySet()) {
            if (e.getKey().equals(id)) {
                return e.getValue().getOperationType();
            }
        }
        return operation.getOperationType();
    }
}
