/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *             FILE : ScheduledReindexationServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 13 sept. 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.cl.dao.ScheduledReindexationDao;
import eu.europa.ec.opoce.cellar.cl.domain.index.ScheduledReindexation;
import eu.europa.ec.opoce.cellar.cl.service.ScheduledReindexationService;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.core.util.CronExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class ScheduledReindexationServiceImpl.
 * <class_description> The service class to retrieve the ScheduledReindexation DAO
 * <br/><br/>
 * ON : 13 sept. 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
@Transactional
public class ScheduledReindexationServiceImpl implements ScheduledReindexationService {

    /** The scheduled reindexation dao. */
    @Autowired
    private ScheduledReindexationDao scheduledReindexationDao;

    /** {@inheritDoc} */
    @Override
    public ScheduledReindexation getScheduledReindexationConfiguration() {
        final List<ScheduledReindexation> findAll = this.scheduledReindexationDao.findAll();
        return (findAll != null) && !findAll.isEmpty() ? findAll.get(0) : null;
    }

    /** {@inheritDoc} */
    @Override
    public ScheduledReindexation saveOrUpdate(final ScheduledReindexation scheduledReindexation) {
        return this.scheduledReindexationDao.saveOrUpdateObject(scheduledReindexation);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isValid(final ScheduledReindexation scheduledReindexation) {
        boolean result = true;
        final boolean chooseAllContent = scheduledReindexation.isAllEligibleContent();
        final boolean chooseAllContentForPeriod = scheduledReindexation.isAllEligibleContentWithinPeriod();
        final String period = scheduledReindexation.getPeriod();
        final boolean useSparql = scheduledReindexation.isUseSparql();
        final String sparqlQuery = scheduledReindexation.getSparqlQuery();
        final String duration = scheduledReindexation.getDuration();
        final String cronExpression = scheduledReindexation.getCron();
        // no choice made
        if (!chooseAllContent && !chooseAllContentForPeriod && !useSparql) {
            result = false;
        }
        // multiple choices active
        if (result && chooseAllContent && chooseAllContentForPeriod && useSparql) {
            result = false;
        }
        //period is blank or non numeric
        if (result && chooseAllContentForPeriod && (StringUtils.isBlank(period) || !isValidAlphanumericCode(period, new String[] {
                "d", "w", "m"}))) {
            result = false;
        }
        //duration is blank or non numeric
        if (result && (StringUtils.isBlank(duration) || !isValidAlphanumericCode(duration, new String[] {
                "m", "h"}))) {
            result = false;
        }
        //cron is blank or non valid
        if (result && (StringUtils.isBlank(cronExpression) || !CronExpression.isValidExpression(cronExpression))) {
            result = false;
        }
        //sparql is blank
        if (result && useSparql) {
            if (StringUtils.isBlank(sparqlQuery)) {
                result = false;
            }
        }
        return result;
    }

    /**
     * Validate alphanumeric code.
     *
     * @param value the value to validate
     * @param validCodes the valid codes
     * @return true, if successful
     */
    private boolean isValidAlphanumericCode(final String value, final String[] validCodes) {
        boolean result = false;
        if (StringUtils.isNotBlank(value)) {
            result = StringUtils.endsWithAny(value, validCodes) && StringUtils.isNumeric(value.substring(0, value.length() - 1));
        }
        return result;
    }
}
