/**
 * 
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPObject;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.cl.domain.validator.SystemValidator;
import eu.europa.ec.opoce.cellar.cl.domain.validator.ValidationResult;
import eu.europa.ec.opoce.cellar.cl.service.client.FileService;
import eu.europa.ec.opoce.cellar.cl.service.client.SIPLoader;
import eu.europa.ec.opoce.cellar.cl.service.client.ValidationService;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.InvalidMetsFileException;
import eu.europa.ec.opoce.cellar.exception.SIPException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * The SIP Loader is a service of the CELLAR system that is responsible for loading a list of files
 * from a specific location in the temporary work folder. The list of files is stored in a
 * {@link SIPObject}.
 * 
 * @author dsavares
 * 
 */
@Service
public class SIPLoaderImpl implements SIPLoader {

    /** Logger instance. */
    private static final Logger LOG = LoggerFactory.getLogger(SIPLoaderImpl.class);

    /**
     * The service for accessing the file system.
     */
    @Autowired
    private FileService fileService;

    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    @Qualifier("contentValidator")
    private @Autowired SystemValidator contentValidator;

    private @Autowired ValidationService validationService;

    /**
     * {@inheritDoc}
     */
    @Override
    public SIPObject loadSIPObject(final SIPResource sipResource) throws SIPException {
        final SIPObject sipObject = new SIPObject();
        sipObject.setSipType(sipResource.getSipType());

        //TODO : get folder file by the FileService
        sipObject.setZipExtractionFolder(new File(this.cellarConfiguration.getCellarFolderTemporaryWork(), sipResource.getFolderName()));

        switch (sipResource.getSipType()) {
        case AUTHENTICOJ:
            sipObject.setZipFile(new File(this.cellarConfiguration.getCellarFolderAuthenticOJReception(), sipResource.getArchiveName()));
            break;
        case DAILY:
            sipObject.setZipFile(new File(this.cellarConfiguration.getCellarFolderDailyReception(), sipResource.getArchiveName()));
            break;
        case BULK:
            sipObject.setZipFile(new File(this.cellarConfiguration.getCellarFolderBulkReception(), sipResource.getArchiveName()));
            break;
        case BULK_LOWPRIORITY:
            sipObject.setZipFile(new File(this.cellarConfiguration.getCellarFolderBulkLowPriorityReception(), sipResource.getArchiveName()));
            break;
        default:
            break;
        }

        try {
            //this can be returning null if the path cannot be resolved
            final File[] files = this.fileService.getAllFilesFromUncompressedLocation(sipResource.getFolderName());
            final List<File> asList = files != null ? Arrays.asList(files) : new ArrayList<File>();
            sipObject.setFileList(asList);
        } catch (final IOException e) {
            LOG.error("An error occurred loading the file list from this location [" + sipResource.getFolderName() + "]", e);
            throw ExceptionBuilder.get(SIPException.class).withCode(CoreErrors.E_014)
                    .withMessage("Mets folder : " + sipResource.getFolderName()).build();
        }

        //Validate that all met's linked files are present in the SIP.
        final ValidationResult validationResults = this.validationService.executeSystemValidator(sipObject, sipResource,
                this.contentValidator);
        if (!validationResults.isValid()) {
            final File metsFile = sipObject.getMetsFile();

            String errorCause = StringUtils.join(validationResults.getMessages(), ", ");
            if (StringUtils.isBlank(errorCause)) {
                errorCause = metsFile.getPath();
            }

            throw ExceptionBuilder.get(InvalidMetsFileException.class)
                    .withCode(CoreErrors.E_024).withCause(new Throwable()).withMessage(errorCause).build();
        }
        return sipObject;
    }

}
