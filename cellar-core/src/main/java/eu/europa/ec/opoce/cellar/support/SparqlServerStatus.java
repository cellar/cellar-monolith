/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.support
 *        FILE : SparqlServerStatus.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 14-05-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.support;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * <class_description> This is an utility component used for checking whether the SPARQL endpoint is online or not.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 14-05-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
@Component
public class SparqlServerStatus extends AbstractServiceStatus {

    @Autowired
    public SparqlServerStatus(@Qualifier("cellarConfiguration") ICellarConfiguration cellarConfiguration) {
        super("SPARQL endpoint", cellarConfiguration);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.support.AbstractServiceStatus#resolveServiceUrl()
     */
    @Override
    protected String resolveServiceUrl() {
        return this.cellarConfiguration.getCellarServiceSparqlUri();
    }
}
