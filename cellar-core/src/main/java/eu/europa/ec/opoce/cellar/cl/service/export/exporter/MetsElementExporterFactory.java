/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor.export
 *             FILE : MetsElementExportFactory.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 24, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.export.exporter;

import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.cl.service.export.exporter.impl.ContentStreamMetsElementExporter;
import eu.europa.ec.opoce.cellar.cl.service.export.exporter.impl.MetadataMetsElementExporter;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement;
import eu.europa.ec.opoce.cellar.common.factory.IFactory;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.ExportException;
import org.springframework.stereotype.Component;

/**
 * <class_description> METS element exporter factory: build the right exporter object.
 * <p>
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 24, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component("metsElementExporterFactory")
public class MetsElementExporterFactory implements IFactory<IMetsElementExporter, MetsElement> {

    /**
     * Create the right exporter according to the METS element type.
     */
    @Override
    public IMetsElementExporter create(final MetsElement metsElement) {

        switch (metsElement.getType()) {
            case AGENT:
            case DOSSIER:
            case EVENT:
            case WORK:
            case EXPRESSION:
            case MANIFESTATION:
            case TOPLEVELEVENT:
                return new MetadataMetsElementExporter(metsElement);
            case ITEM:
                return new ContentStreamMetsElementExporter(metsElement);
            default:
                throw ExceptionBuilder.get(ExportException.class).withCode(CoreErrors.E_250)
                        .withMessage("The export is not supported for resource of type {}.").withMessageArgs(metsElement.getType()).build();
        }
    }

}
