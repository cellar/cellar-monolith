/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.ccr.service.impl
 *        FILE : MetsBulkResponseService.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 19-06-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.service.impl;

import eu.europa.ec.opoce.cellar.ccr.service.client.MetsResponseService;
import eu.europa.ec.opoce.cellar.ccr.velocity.VelocityMarshaller;
import eu.europa.ec.opoce.cellar.cl.dao.StructMapResponseDao;
import eu.europa.ec.opoce.cellar.cl.domain.validator.SystemValidator;
import eu.europa.ec.opoce.cellar.cl.service.client.AuditTrailEventService;
import eu.europa.ec.opoce.cellar.cl.service.client.FileService;
import eu.europa.ec.opoce.cellar.cl.service.client.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.File;

/**
 * <class_description> Implementation of the {@link MetsResponseService} for the bulk ingestions.<br/>
 * Service to handle the creation of responses in xml mets format and generating SIPResource archives with the generated files.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 19-06-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
@Service("metsBulkResponseService")
public class MetsBulkResponseService extends AbstractMetsResponseService {

    private FileService fileService;

    private StructMapResponseDao responseDao;

    @Autowired
    public MetsBulkResponseService(FileService fileService, @Qualifier("structMapBulkResponseDao") StructMapResponseDao responseDao,
                                   @Qualifier("metsMarshaller") VelocityMarshaller metsMarshaller,
                                   @Qualifier("metsResponseValidator") SystemValidator metsResponseValidator,
                                   AuditTrailEventService auditTrailEventService, ValidationService validationService) {
        super(metsMarshaller, metsResponseValidator, auditTrailEventService, validationService);
        this.fileService = fileService;
        this.responseDao = responseDao;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.ccr.service.impl.AbstractMetsResponseService#resolveResponseFolder()
     */
    @Override
    protected File resolveResponseFolder() {
        return this.fileService.getResponseBulkFolder();
    }

    /**
     * @see eu.europa.ec.opoce.cellar.ccr.service.impl.AbstractMetsResponseService#resolveResponseDao()
     */
    @Override
    protected StructMapResponseDao resolveResponseDao() {
        return this.responseDao;
    }
}
