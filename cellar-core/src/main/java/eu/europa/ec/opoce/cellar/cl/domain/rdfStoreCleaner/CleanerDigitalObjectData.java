/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner
 *             FILE : CleanerDigitalObjectData.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 3, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner;

import eu.europa.ec.opoce.cellar.cl.configuration.enums.CellarServiceRDFStoreCleanerMode;
import eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner.CleanerDifference.CmrTable;
import eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner.CleanerDifference.QueryType;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.daobeans.InverseRelation;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import org.apache.commons.collections15.CollectionUtils;
import org.apache.jena.rdf.model.Model;

import java.util.*;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page. <br/>
 * <br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc. <br/>
 * <br/>
 * ON : Oct 3, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class CleanerDigitalObjectData {

    private final CleanerHierarchyData parent;

    private final String cellarId;
    private final DigitalObjectType digitalObjectType;
    private final Map<ContentType, String> versions;

    private boolean valid;

    private Map<ContentType, Model> metadataFromS3;
    private Model metadataFromOracle;
    private Model inverseFromOracle;
    private Model metadataRebuiltForOracle;
    private Model inverseRebuiltForOracle;
    private Set<String> sameAsUris;
    private Collection<InverseRelation> inverseRelations;
    private Collection<InverseRelation> inverseRelationsRebuilt;

    private final List<CleanerDifference> cleanerDifferences;

    CleanerDigitalObjectData(final String cellarId, final DigitalObjectType digitalObjectType,
                             Map<ContentType, String> versions, final CleanerHierarchyData parent) {
        this.cellarId = cellarId;
        this.digitalObjectType = digitalObjectType;
        this.versions = versions;
        this.parent = parent;
        this.valid = true;

        this.cleanerDifferences = new LinkedList<>();
    }

    public boolean isValid() {
        return valid;
    }

    public DigitalObjectType getDigitalObjectType() {
        return digitalObjectType;
    }

    public Map<ContentType, Model> getMetadataFromS3() {
        return metadataFromS3;
    }

    public void setMetadataFromS3(Map<ContentType, Model> metadataFromS3) {
        this.metadataFromS3 = metadataFromS3;
    }

    public Model getMetadataFromOracle() {
        return metadataFromOracle;
    }

    public void setMetadataFromOracle(Model metadataFromOracle) {
        this.metadataFromOracle = metadataFromOracle;
    }

    public Model getInverseFromOracle() {
        return inverseFromOracle;
    }

    public void setInverseFromOracle(Model inverseFromOracle) {
        this.inverseFromOracle = inverseFromOracle;
    }

    public Model getMetadataRebuiltForOracle() {
        return metadataRebuiltForOracle;
    }

    public void setMetadataRebuiltForOracle(Model metadataRebuiltForOracle) {
        this.metadataRebuiltForOracle = metadataRebuiltForOracle;
    }

    public Model getInverseRebuiltForOracle() {
        return inverseRebuiltForOracle;
    }

    public void setInverseRebuiltForOracle(Model inverseRebuiltForOracle) {
        this.inverseRebuiltForOracle = inverseRebuiltForOracle;
    }

    public Set<String> getSameAsUris() {
        return sameAsUris;
    }

    public void setSameAsUris(Set<String> sameAsUris) {
        this.sameAsUris = sameAsUris;
    }

    public Collection<InverseRelation> getInverseRelations() {
        return inverseRelations;
    }

    public void setInverseRelations(Collection<InverseRelation> inverseRelations) {
        this.inverseRelations = inverseRelations;
    }

    public Collection<InverseRelation> getInverseRelationsRebuilt() {
        return inverseRelationsRebuilt;
    }

    public void setInverseRelationsRebuilt(Collection<InverseRelation> inverseRelationsRebuilt) {
        this.inverseRelationsRebuilt = inverseRelationsRebuilt;
    }

    public CleanerHierarchyData getParent() {
        return parent;
    }

    public String getCellarId() {
        return cellarId;
    }

    public List<CleanerDifference> getCleanerDifferences() {
        return cleanerDifferences;
    }

    public void addCleanerDifference(final CleanerDifference cleanerDifference) {
        this.cleanerDifferences.add(cleanerDifference);
    }

    public void setUnderEmbargo(final Boolean underEmbargo) {
        this.parent.setUnderEmbargo(underEmbargo);
    }

    public Map<ContentType, String> getVersions() {
        return versions;
    }

    public void diagnostic(final CellarServiceRDFStoreCleanerMode operationType) {
        Model oldModel = this.getMetadataFromOracle();
        Model newModel = this.getMetadataRebuiltForOracle();

        this.valid = this.diagnosticModel(oldModel, newModel, operationType, CmrTable.CMR_METADATA);

        oldModel = this.getInverseFromOracle();
        newModel = this.getInverseRebuiltForOracle();

        this.valid &= this.diagnosticModel(oldModel, newModel, operationType, CmrTable.CMR_INVERSE);

        final Collection<InverseRelation> oldInverseRelations = this.getInverseRelations();
        final Collection<InverseRelation> newInverseRelations = this.getInverseRelationsRebuilt();

        this.valid &= this.diagnosticInverseRelations(oldInverseRelations, newInverseRelations, operationType);
    }

    private boolean diagnosticInverseRelations(final Collection<InverseRelation> oldInverseRelations,
                                               final Collection<InverseRelation> newInverseRelations,
                                               final CellarServiceRDFStoreCleanerMode operationType) {
        if (!CollectionUtils.isEqualCollection(newInverseRelations, oldInverseRelations)) {
            this.diagnosticInverseRelationsDifference(oldInverseRelations, newInverseRelations, operationType,
                    QueryType.delete);
            this.diagnosticInverseRelationsDifference(newInverseRelations, oldInverseRelations, operationType,
                    QueryType.add);
            return false;
        }

        return true;
    }

    private void diagnosticInverseRelationsDifference(final Collection<InverseRelation> inverseRelations1,
                                                      final Collection<InverseRelation> inverseRelations2, final CellarServiceRDFStoreCleanerMode operationType,
                                                      final QueryType queryType) {
        final Collection<InverseRelation> difference = CollectionUtils.subtract(inverseRelations1, inverseRelations2);

        if (!difference.isEmpty()) {
            final CleanerDifference cleanerDifference = new CleanerDifference(operationType, queryType,
                    CmrTable.CMR_INVERSE_RELATIONS, this.getCellarId(), difference.toString());
            this.addCleanerDifference(cleanerDifference);
            this.parent.putInverseRelationFixe(queryType, difference);
        }
    }

    private boolean diagnosticModel(final Model oldModel, final Model newModel,
                                    final CellarServiceRDFStoreCleanerMode operationType, final CmrTable cmrTable) {
        if (!oldModel.isIsomorphicWith(newModel)) {
            this.diagnosticModelDifference(newModel, oldModel, operationType, QueryType.add, cmrTable);
            this.diagnosticModelDifference(oldModel, newModel, operationType, QueryType.delete, cmrTable);

            switch (cmrTable) {
                case CMR_METADATA:
                    this.parent.putMetadataFixe(QueryType.add, this.getCellarId(), newModel);
                    this.parent.putMetadataFixe(QueryType.delete, this.getCellarId(), null);
                    break;
                case CMR_INVERSE:
                    this.parent.putInverseFixe(QueryType.add, this.getCellarId(), newModel);
                    this.parent.putInverseFixe(QueryType.delete, this.getCellarId(), null);
                    break;
                // should never get here!
                default:
                    break;
            }

            return false;
        }

        return true;
    }

    private void diagnosticModelDifference(final Model model1, final Model model2,
                                           final CellarServiceRDFStoreCleanerMode operationType, final QueryType queryType, final CmrTable cmrTable) {
        final Model difference = model1.difference(model2);
        try {
            if (!difference.isEmpty()) {
                final CleanerDifference cleanerDifference = new CleanerDifference(operationType, queryType, cmrTable,
                        this.getCellarId(), JenaUtils.toString(difference));
                this.addCleanerDifference(cleanerDifference);
            }
        } finally {
            JenaUtils.closeQuietly(difference);
        }
    }

    /**
     * <p>
     * closeQuietly closes all the models that are open. Make sure to call this
     * after an instance of this class in not needed anymore
     * </p>
     */
    public void closeQuietly() {
        closeQuietly(this.metadataFromS3);

        JenaUtils.closeQuietly(metadataFromOracle);
        JenaUtils.closeQuietly(inverseFromOracle);
        JenaUtils.closeQuietly(metadataRebuiltForOracle);
        JenaUtils.closeQuietly(inverseRebuiltForOracle);
    }

    /**
     * <p>
     * closeQuietly closes all models that can be found in a map.
     * </p>
     *
     * @param map a {@link java.util.Map} object.
     */
    private static void closeQuietly(final Map<?, Model> map) {
        if (map != null) {
            JenaUtils.closeQuietly(map.values());
        }
    }
}
