/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.export
 *             FILE : ExportedPackage.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 25-Oct-2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2016 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.export;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * The Class ExportedPackage.
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 25-Oct-2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlType(name = "exportType")
@XmlRootElement(name = "export")
public class ExportedPackage {

    /** The work identifier. */
    private String workIdentifier;

    /** The exported package path. */
    private String exportedPackagePath;

    /**
     * Instantiates a new exported package.
     */
    public ExportedPackage() {
    }

    /**
     * Instantiates a new exported package.
     *
     * @param workIdentifier the work identifier
     * @param exportedPackagePath the exported package path
     */
    public ExportedPackage(final String workIdentifier, final String exportedPackagePath) {
        this.workIdentifier = workIdentifier;
        this.exportedPackagePath = exportedPackagePath;
    }

    /**
     * Gets the work identifier.
     *
     * @return the work identifier
     */
    @XmlElement(name = "identifier")
    public String getWorkIdentifier() {
        return this.workIdentifier;
    }

    /**
     * Sets the work identifier.
     *
     * @param workIdentifier the new work identifier
     */
    public void setWorkIdentifier(final String workIdentifier) {
        this.workIdentifier = workIdentifier;
    }

    /**
     * Gets the exported package path.
     *
     * @return the exported package path
     */
    @XmlElement(name = "exported_package")
    public String getExportedPackagePath() {
        return this.exportedPackagePath;
    }

    /**
     * Sets the exported package path.
     *
     * @param exportedPackagePath the new exported package path
     */
    public void setExportedPackagePath(final String exportedPackagePath) {
        this.exportedPackagePath = exportedPackagePath;
    }
}
