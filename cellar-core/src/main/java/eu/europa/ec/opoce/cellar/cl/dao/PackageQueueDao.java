/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.dao
 *        FILE : PackageQueueDao.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 15-12-2021
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2021 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao;

import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.PackageQueue;

/**
 * Interface providing access to the database view containing
 * the prioritized references of the SIP packages available
 * for ingestion.
 * @author EUROPEAN DYNAMICS S.A.
 */
public interface PackageQueueDao extends BaseDao<PackageQueue, Long> {
	
}
