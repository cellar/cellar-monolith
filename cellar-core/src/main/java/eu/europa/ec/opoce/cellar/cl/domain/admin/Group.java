/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : Group.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 9, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.admin;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

/**
 * <class_description> Class that represents a group of roles.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 9, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Entity
@Table(name = "GROUPS")
public class Group {

    /**
     * Internal id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    /**
     * The group name.
     */
    @Column(name = "GROUP_NAME", nullable = false)
    private String groupName;

    /**
     * The set of roles (tabs) allowed.
     */
    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "GROUP_ROLES", joinColumns = {
            @JoinColumn(name = "GROUP_ID", referencedColumnName = "ID")}, inverseJoinColumns = {
                    @JoinColumn(name = "ROLE_ID", referencedColumnName = "ID")})
    private Set<Role> roles;

    /**
     * Default constructor.
     */
    public Group() {

    }

    /**
     * Constructor.
     * @param id the identifier of the group
     */
    public Group(final Long id) {
        this.id = id;
    }

    /**
     * Constructor.
     * @param id the identifier of the group
     * @param groupName the name of the group
     */
    public Group(final Long id, final String groupName) {
        this.id = id;
        this.groupName = groupName;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * @param id the id to set
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * @return the groupName
     */
    public String getGroupName() {
        return this.groupName;
    }

    /**
     * @param groupName the groupName to set
     */
    public void setGroupName(final String groupName) {
        this.groupName = groupName;
    }

    /**
     * @return the roles
     */
    public Set<Role> getRoles() {
        return this.roles;
    }

    /**
     * @param roles the roles to set
     */
    public void setRoles(final Set<Role> roles) {
        this.roles = roles;
    }
}
