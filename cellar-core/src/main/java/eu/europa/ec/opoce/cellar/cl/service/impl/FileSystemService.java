package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.cl.service.client.FileService;
import eu.europa.ec.opoce.cellar.core.dao.FileDao;
import eu.europa.ec.opoce.cellar.core.exception.UncompressException;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;
import eu.europa.ec.opoce.cellar.monitoring.Watch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * A FileSystemService is a Service responsible for managing files.
 *
 * @author dsavares
 */
@Service
public class FileSystemService implements FileService {

    /**
     * The Dao for accessing the file system.
     */
    @Autowired
    private FileDao fileDao;

    /**
     * {@inheritDoc}
     */
    @Override
    public File[] getAllSipArchivesFromAuthenticOJReceptionFolder() throws IOException {
        return this.fileDao.getAllSipArchivesFromAuthenticOJReceptionFolder();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public File[] getAllSipArchivesFromDailyReceptionFolder() throws IOException {
        return this.fileDao.getAllSipArchivesFromDailyReceptionFolder();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public File[] getAllSipArchivesFromBulkReceptionFolder(final int maxSIPToReturn) throws IOException {
        return this.fileDao.getAllSipArchivesFromBulkReceptionFolder(maxSIPToReturn);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public File[] getAllSipArchivesFromBulkLowPriorityReceptionFolder(final int maxSIPToReturn) throws IOException {
        return this.fileDao.getAllSipArchivesFromBulkLowPriorityReceptionFolder(maxSIPToReturn);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public File getMetsFileFromTemporaryWorkFolder(final String folderName) throws IOException {
        return this.fileDao.getMetsFileFromTemporaryWorkFolder(folderName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteFromTemporaryWorkFolder(final String folderName) throws IOException {
        this.fileDao.deleteFromTemporaryWorkFolder(folderName);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteFromAuthenticOJReceptionFolder(final String fileName) throws IOException {
        this.fileDao.deleteFromAuthenticOJReceptionFolder(fileName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteFromDailyReceptionFolder(final String fileName) throws IOException {
        this.fileDao.deleteFromDailyReceptionFolder(fileName);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteFromBulkReceptionFolder(final String fileName) throws IOException {
        this.fileDao.deleteFromBulkReceptionFolder(fileName);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteFromBulkLowPriorityReceptionFolder(final String fileName) throws IOException {
        this.fileDao.deleteFromBulkLowPriorityReceptionFolder(fileName);
    }

    @Watch(value = "Uncompress file", arguments = {
            @Watch.WatchArgument(name = "name", expression = "archive.path")
    })
    @Override
    public String uncompress(final File archive) throws UncompressException {
        return this.fileDao.uncompress(archive);
    }
    
    @Override
    public String uncompressMetadataOnly(final File archive) throws UncompressException {
        return this.fileDao.uncompressMetadataOnly(archive);
    }

    @Watch(value = "Uncompress Quietly file", arguments = {
            @Watch.WatchArgument(name = "name", expression = "archive.path")
    })
    @Override
    public String uncompressQuietly(final File archive) throws UncompressException {
        return this.fileDao.uncompressQuietly(archive);
    }
    
    @Override
    public String uncompressQuietlyMetadataOnly(final File archive) throws UncompressException {
        return this.fileDao.uncompressQuietlyMetadataOnly(archive);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String storeArchiveInDailyReceptionFolder(final byte[] sipObjectStream) throws IOException {
        return this.fileDao.storeArchiveInDailyReceptionFolder(sipObjectStream);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public File[] getAllFilesFromUncompressedLocation(final String location) throws IOException {
        return this.fileDao.getAllFilesFromUncompressedLocation(location);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moveSIPToErrorFolder(final File sipFile) throws IOException {
        this.fileDao.moveSIPToErrorFolder(sipFile);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void copySIPToErrorFolder(final File sipFile) throws IOException {
        this.fileDao.copySIPToErrorFolder(sipFile);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moveSIPToBackupFolder(final File sipFile) throws IOException {
        this.fileDao.moveSIPToBackupFolder(sipFile);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moveMetsToErrorFolder(final String metsFolderPath) throws IOException {
        this.fileDao.moveMetsToErrorFolder(metsFolderPath);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moveFoxmlToBackupFolder(final String foxmlFolderPath) throws IOException {
        this.fileDao.moveFoxmlToBackupFolder(foxmlFolderPath);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void moveMetsToBackupFolder(final String metsFolderPath) throws IOException {
        this.fileDao.moveMetsToBackupFolder(metsFolderPath);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteFoxmlFolder(final String foxmlFolderName) throws IOException {
        this.fileDao.deleteFromFoxmlFolder(foxmlFolderName);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String moveSIPToAuthenticOJReceptionFolder(final File sipFile, final String metsId) throws IOException {
        return this.fileDao.moveSIPToAuthenticOJReceptionFolder(sipFile, metsId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String moveSIPToDailyReceptionFolder(final File sipFile, final String metsId) throws IOException {
        return this.fileDao.moveSIPToDailyReceptionFolder(sipFile, metsId);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public String moveSIPToBulkReceptionFolder(final File sipFile, final String metsId) throws IOException {
        return this.fileDao.moveSIPToBulkReceptionFolder(sipFile, metsId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public File getResponseAuthenticOJFolder() {
        return this.fileDao.getResponseAuthenticOJFolder();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public File getResponseDailyFolder() {
        return this.fileDao.getResponseDailyFolder();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public File getResponseBulkFolder() {
        return this.fileDao.getResponseBulkFolder();
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public File getResponseBulkLowPriorityFolder() {
        return this.fileDao.getResponseBulkLowPriorityFolder();
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public File getSipFromReceptionFolder(String sipFileName, TYPE receptionFolderType) {
    	return this.fileDao.getSipFromReceptionFolder(sipFileName, receptionFolderType);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public void moveToResponseErrorFolder(List<File> files, TYPE receptionFolderType) {
    	this.fileDao.moveToResponseErrorFolder(files, receptionFolderType);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public File moveSIPToReceptionFolderByPriority(File sipFile,
                                                     String metsId,
                                                     TYPE priority) throws IOException {
        return this.fileDao.moveSIPToReceptionFolderByPriority(sipFile, metsId, priority);
    }

}
