/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *        FILE : SipDependencyCheckerMetsParser.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 21-11-2021
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2021 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.XMLEvent;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.MetsAnalyzerException;

/**
 * METS parser for extracting the values necessary for performing resource discovery
 * for the related SIP file.
 * @author EUROPEAN DYNAMICS S.A.
 */
@Component
public class SipDependencyCheckerMetsParser {
	
	/**
	 * Parses the provided METS file and extracts the information necessary for performing
	 * resource discovery.
	 * @param metsFile the METS file to parse
	 * @param businessMetadataRefsPerId the href attribute values of the business metadata file references (<mdRef>),
	 * organized based on the ID attribute of the <dmdSec> element that contains each reference.
	 * @param digitalObjectContentIdsPerDmdid the digital objects' (<div>) CONTENTIDS attribute value,
	 * organized based on their DMDID attribute.
	 * @return the METS type (Create/Update/Delete)
	 */
    public Map<String, String> parse(final File metsFile, Map<String, String> businessMetadataRefsPerId, Map<String, List<String>> digitalObjectContentIdsPerDmdid) {
        FileInputStream fis = null;
        XMLEventReader xmlReader = null;
        Map<String, String> metsInfo = new HashMap<>();
        boolean metsIdStartFound = false;
		String structMapName = null;
		boolean structMapExpectsRootEntity = false;
		int numberOfStructMaps = 0;
        try {
            final XMLInputFactory xmlIf = XMLInputFactory.newInstance();
            xmlIf.setProperty("javax.xml.stream.supportDTD", false);
            fis = new FileInputStream(metsFile);
            xmlReader = xmlIf.createXMLEventReader(fis);
            XMLEvent xmlEvent;
            boolean isParentDmdSec = false;
            String dmdSecId = null;
			String topLevelContentIds = null;
            while (xmlReader.hasNext()) {
            	xmlEvent = xmlReader.nextEvent();
            	if (xmlEvent.isStartElement()) {
            		final String tag = xmlEvent.asStartElement().getName().getLocalPart();
            		if (StringUtils.isNotBlank(tag)) {        			
            			switch(tag) {
            				case "metsDocumentID":
            					metsIdStartFound = true;
            					break;
	            			case "mets":
	            				metsInfo.put("metsType", getAttributeValue(xmlEvent, "TYPE"));
	            				break;
	            			case "dmdSec":
	            				dmdSecId = getAttributeValue(xmlEvent, "ID");
	            				isParentDmdSec = true;
	            				break;
	            			case "mdRef":
	            				if (isParentDmdSec) {
	            					businessMetadataRefsPerId.put(dmdSecId, getAttributeValue(xmlEvent, "href"));
	            				}
	            				break;
							case "structMap":
								structMapName = getAttributeValue(xmlEvent, "ID");
								structMapExpectsRootEntity = true;
								break;
	            			case "div":
	            				String dmdId = getAttributeValue(xmlEvent, "DMDID");
	            				String contentIds = getAttributeValue(xmlEvent, "CONTENTIDS");
	            				digitalObjectContentIdsPerDmdid.computeIfAbsent(dmdId, v -> new ArrayList<>());
	            				digitalObjectContentIdsPerDmdid.get(dmdId).add(contentIds);
								// Store top-level production identifiers for later (See "structMap" ending element)
								String entityType = getAttributeValue(xmlEvent, "TYPE");
								if(structMapExpectsRootEntity
										&& (StringUtils.isNotBlank(entityType))
										&& (entityType.equalsIgnoreCase("WORK")
											|| entityType.equalsIgnoreCase("DOSSIER")
											|| entityType.equalsIgnoreCase("AGENT")
											|| entityType.equalsIgnoreCase("EVENT")
											)
								){
									topLevelContentIds = contentIds;
									structMapExpectsRootEntity = false;
								}
	            				break;
	        				default:
            			}
            		}
            	}
            	else if (xmlEvent.isEndElement()) {
            		final String tag = xmlEvent.asEndElement().getName().getLocalPart();

					if (StringUtils.isNotBlank(tag)){
						switch(tag) {
							case "structMap":
								// Add a prefix to distinguish structmaps in case the name is the same.The package will be invalid (identified in the convert step of the package pre-processing procedure) and the ingestion will fail. 
								// The prefix has been added in order for the map to contain the correct number of structmaps and status information to be properly provided even in this case.
								metsInfo.put(numberOfStructMaps + "_structMap_" + structMapName, topLevelContentIds);
								++numberOfStructMaps;
								break;
							case "dmdSec":
								isParentDmdSec = false;
								break;
							default:
						}
					}
					
				}
            	else if (metsIdStartFound && xmlEvent.isCharacters()) {
            		metsInfo.put("metsId", getValue(xmlEvent));
            		metsIdStartFound = false;
            	}
            }
        }
        catch (final FileNotFoundException fnfe) {
        	handleException(metsFile, fnfe);
        }
        catch (final XMLStreamException xmlse) {
        	handleException(metsFile, xmlse);
        }
        finally {
	        // Close streams.
	        if (xmlReader != null) {
	            try {
	                xmlReader.close();
	            }
	            catch (final XMLStreamException e) {
	            	
	            }
	        }
	        if (fis != null) {
	            try {
	                fis.close();
	            }
	            catch (final IOException e) {
	            	
	            }
	        }
        }
        return metsInfo;
    }
    
    /**
     * Iterates over the attributes of the provided XML start tag and
     * returns the value of the attribute matching the one provided.
     * @param xmlEvent the XML start tag whose attributes will be parsed
     * @param attributeToLookFor the attribute to search for
     * @return the value of the specified attribute
     */
    @SuppressWarnings("rawtypes")
    private String getAttributeValue(XMLEvent xmlEvent, String attributeToLookFor) {
    	final Iterator attrIter = xmlEvent.asStartElement().getAttributes();
		while (attrIter.hasNext()) {
			Attribute attr = (Attribute) attrIter.next();
			String attrName = StringUtils.trim(attr.getName().getLocalPart());
			if (attributeToLookFor.equals(attrName)) {
				return StringUtils.trim(attr.getValue());
			}
		}
    	return StringUtils.EMPTY;
    }

    /**
     * Extracts the characters content of the provided XML element.
     * @param xmlEvent the XML element whose characters will be extracted.
     * @return the extracted characters.
     */
    private String getValue(XMLEvent xmlEvent) {
    	if (!xmlEvent.asCharacters().isWhiteSpace()) {
    		return StringUtils.trim(xmlEvent.asCharacters().getData());
    	}
    	return StringUtils.EMPTY;
    }
    
	private static ExceptionBuilder<MetsAnalyzerException> prepareExceptionBuilder(final File metsFile, final Throwable cause)
            throws MetsAnalyzerException {
        return ExceptionBuilder.get(MetsAnalyzerException.class).withMessage("An error occurred while parsing METS file [{}]")
        		.withCode(CoreErrors.E_025)
        		.withMessageArgs(metsFile)
                .withCause(cause);
    }

    private static void handleException(final File metsFile, final Throwable cause) throws MetsAnalyzerException {
        throw prepareExceptionBuilder(metsFile, cause).build();
    }
    
}
