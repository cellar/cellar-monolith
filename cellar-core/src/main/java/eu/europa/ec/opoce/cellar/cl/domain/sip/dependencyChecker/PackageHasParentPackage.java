/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker
 *        FILE : PackageHasParentPackage.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 10-12-2021
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2021 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Domain object representing the PACKAGE_HAS_PARENT_PACKAGE database table.
 * @author EUROPEAN DYNAMICS S.A.
 */
@Entity
@Table(name = "PACKAGE_HAS_PARENT_PACKAGE")
@NamedQueries({
	@NamedQuery(name = "PackageHasParentPackage.findBySipId",
		query = "SELECT phpp FROM PackageHasParentPackage phpp WHERE phpp.sipId = :sipId"),
	@NamedQuery(name = "PackageHasParentPackage.findBySipIdOrParentSipIdIn",
		query="SELECT phpp FROM PackageHasParentPackage phpp WHERE phpp.sipId IN (:sipIds) OR phpp.parentSipId IN (:sipIds)")
})
public class PackageHasParentPackage {

	/**
	 * The ID.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "package_has_parent_package_seq_gen")
	@SequenceGenerator(name = "package_has_parent_package_seq_gen", sequenceName = "PCKG_HAS_PARENT_PCKG_SEQ", allocationSize = 1)
	@Column(name = "ID", unique = true, nullable = false)
	private Long id;
	/**
	 * The SIP ID, referencing the ID column of the SIP_PACKAGE table.
	 */
	@Column(name = "SIP_ID", unique = true, nullable = false)
	private Long sipId;
	/**
	 * The parent SIP ID of the SIP ID, referencing the ID column of the SIP_PACKAGE table.
	 */
	@Column(name = "PARENT_SIP_ID", unique = true, nullable = true)
	private Long parentSipId;
	
	
	public PackageHasParentPackage() {
		
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getSipId() {
		return sipId;
	}

	public void setSipId(Long sipId) {
		this.sipId = sipId;
	}

	public Long getParentSipId() {
		return parentSipId;
	}

	public void setParentSipId(Long parentSipId) {
		this.parentSipId = parentSipId;
	}


	@Override
	public int hashCode() {
		return Objects.hash(sipId);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PackageHasParentPackage other = (PackageHasParentPackage) obj;
		return Objects.equals(sipId, other.sipId);
	}
	
}
