/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *        FILE : SIPDependencyCheckerImpl.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 21-11-2021
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2021 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.MessageCode;
import eu.europa.ec.opoce.cellar.ccr.service.client.MetsResponseService;
import eu.europa.ec.opoce.cellar.ccr.service.client.MetsUploadService;
import eu.europa.ec.opoce.cellar.ccr.service.impl.MetsServiceFactory;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.PrefixConfigurationService;
import eu.europa.ec.opoce.cellar.cl.dao.ProductionIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.dao.StructMapResponseDao;
import eu.europa.ec.opoce.cellar.cl.domain.admin.ProductionIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditBuilder;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventAction;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventProcess;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventType;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPObject;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResourceDependencies;
import eu.europa.ec.opoce.cellar.cl.domain.enums.ProcessMonitorPhase;
import eu.europa.ec.opoce.cellar.cl.domain.enums.ProcessStatus;
import eu.europa.ec.opoce.cellar.cl.domain.package_history.PackageHistory;
import eu.europa.ec.opoce.cellar.cl.domain.response.StructMapResponse;
import eu.europa.ec.opoce.cellar.cl.domain.structmap_pid.StructMapPid;
import eu.europa.ec.opoce.cellar.cl.domain.structmap_status_history.StructMapStatusHistory;
import eu.europa.ec.opoce.cellar.cl.service.client.*;
import eu.europa.ec.opoce.cellar.cmr.model.ModelUtils;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import eu.europa.ec.opoce.cellar.common.service.TestingThreadService;
import eu.europa.ec.opoce.cellar.common.util.CellarIdUtils;
import eu.europa.ec.opoce.cellar.common.util.FileSystemUtils;
import eu.europa.ec.opoce.cellar.core.exception.UncompressException;
import eu.europa.ec.opoce.cellar.domain.content.mets.*;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.MetsException;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.rdf.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.file.attribute.FileTime;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import static eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils.closeQuietly;

/**
 * Service for performing the discovery of resources that a SIP
 * package depends on, in terms of Production Identifiers.
 * Includes the complete functionality from SIP loading and extraction
 * to the normalization and filtering of the PIDs.
 * @author EUROPEAN DYNAMICS S.A.
 */
@Service
public class SIPDependencyCheckerImpl implements SIPDependencyChecker {
	
	private static final Logger LOG = LogManager.getLogger(SIPDependencyCheckerImpl.class);
	
	private FileService fileService;
	
	private MetsAnalyzer metsAnalyzer;
	
	private IdentifierService identifierService;
	
	private SipDependencyCheckerMetsParser sipDependencyCheckerMetsParser;
	
	private PrefixConfigurationService prefixConfigurationService;
	
	private ICellarConfiguration cellarConfiguration;
	
	private BackupService backupService;
	
	private MetsServiceFactory metsServiceFactory;
	
	private PackageHistoryService packageHistoryService;
	
	private StructMapStatusHistoryService structMapStatusHistoryService;
	
	private StructMapPidService structMapPidService;
    
    private TestingThreadService testingThreadService;
	
	private ProductionIdentifierDao productionIdentifierDao;

	private final MetsUploadService metsUploadService;

	private String[] cellarPrefixUris;

	private final PrefixConfiguration prefixConfiguration = ServiceLocator.getService(PrefixConfigurationService.class).getPrefixConfiguration();

	
	@Autowired
	public SIPDependencyCheckerImpl (FileService fileService, MetsAnalyzer metsAnalyzer,
									 @Qualifier("pidManagerService") IdentifierService identifierService, SipDependencyCheckerMetsParser sipDependencyCheckerMetsParser,
									 PrefixConfigurationService prefixConfigurationService, @Qualifier("cellarConfiguration") ICellarConfiguration cellarConfiguration,
									 BackupService backupService, MetsServiceFactory metsServiceFactory, PackageHistoryService packageHistoryService,
									 StructMapStatusHistoryService structMapStatusHistoryService, StructMapPidService structMapPidService,
                                     TestingThreadService testingThreadService,ProductionIdentifierDao productionIdentifierDao,
									 MetsUploadService metsUploadService) {
		this.fileService = fileService;
		this.metsAnalyzer = metsAnalyzer;
		this.identifierService = identifierService;
		this.sipDependencyCheckerMetsParser = sipDependencyCheckerMetsParser;
		this.prefixConfigurationService = prefixConfigurationService;
		this.cellarConfiguration = cellarConfiguration;
		this.backupService = backupService;
		this.metsServiceFactory = metsServiceFactory;
		this.packageHistoryService = packageHistoryService;
		this.structMapStatusHistoryService = structMapStatusHistoryService;
		this.structMapPidService = structMapPidService;
        this.testingThreadService = testingThreadService;
		this.productionIdentifierDao = productionIdentifierDao;
		this.metsUploadService = metsUploadService;
	}
	
	@PostConstruct
    private void init() {
        this.cellarPrefixUris = this.prefixConfigurationService.getPrefixConfiguration().getPrefixUris().values().toArray(new String[0]);
    }
	
	/**
     * {@inheritDoc}
     */
	@Override
	public SIPResourceDependencies discoverResources(SIPWork sipWork) {
		long startTime = System.currentTimeMillis();
		// Insert (or find already existing) PACKAGE_HISTORY entry
		PackageHistory packageHistory = this.retrieveOrCreatePackageHistoryEntry(sipWork);
		sipWork.setPackageHistory(packageHistory);
		// Log the starting of the process in the log file and database.
		AuditBuilder.get(AuditTrailEventProcess.Ingestion).withAction(AuditTrailEventAction.SipDependencyChecker)
				.withType(AuditTrailEventType.Start).withObject(sipWork.getSipFile().getName())
				.withPackageHistory(sipWork.getPackageHistory())
				.withLogger(LOG).withLogDatabase(true).logEvent();
		
		final File sipFile = sipWork.getSipFile();

		// Gather basic SIP info.
		final String sipName = sipFile.getName();
		final TYPE sipType = sipWork.getType();
		
		// Uncompress the SIP.
		String sipUncompressionFolder = null;
		try {
			sipUncompressionFolder = uncompress(sipFile);
		} catch (Exception e) {
			MessageCode code = (e instanceof UncompressException) ? ((UncompressException) e).getCode() : CoreErrors.E_012;
			String message = "Could not uncompress SIP file [" + sipName + "]";
			handleException(sipWork, null, code, e, message, null, null, null);
			return null;
		}

		// Retrieve the METS file from the decompression folder inside the temporary work folder.
		File metsFile = null;
		try {
			metsFile = this.fileService.getMetsFileFromTemporaryWorkFolder(sipUncompressionFolder);
		} catch (Exception e) {
			MessageCode code = (e instanceof MetsException) ? ((MetsException) e).getCode() : CoreErrors.E_025;
			String message = "An error occurred while retrieving the METS file from the uncompression folder for SIP [" + sipName + "]";
			handleException(sipWork, null, code, e, message, sipUncompressionFolder, null, null);
			return null;
		}
		
        // Initialize a MetsPackage object with the SIP extraction folder and the list of files contained inside it.
        MetsPackage resolvedMetsPackage = null;
		try {
			resolvedMetsPackage = initMetsPackageForRelationDiscovery(sipUncompressionFolder);
		} catch (Exception e) {
			String message = "An error occurred while retrieving the file list from the uncompression folder for SIP [" + sipName + "]";
			handleException(sipWork, null, CoreErrors.E_014, e, message, sipUncompressionFolder, null, metsFile);
			return null;
		}
        
        // Parse the METS file and extract required information:
        // 1. Root & children PIDs
        // 2. METS-type
        // 3. Business metadata RDF references
        Map<String, String> businessMetadataRefsPerId = new HashMap<>();
        Map<String, List<String>> contentIdsPerDmdid = new HashMap<>();
        Map<String, String> metsInfo = new HashMap<>();
        try {
        	metsInfo = this.sipDependencyCheckerMetsParser.parse(metsFile, businessMetadataRefsPerId, contentIdsPerDmdid);
        } catch (Exception e) {
        	String message = "An error occurred while parsing the METS file for SIP [" + sipName + "]";
			handleException(sipWork, null, CommonErrors.ERROR_WHILE_PARSING_THE_METS_DOCUMENT, e, message, sipUncompressionFolder, null, metsFile);
        	return null;
        }
        List<StructMapStatusHistory> structMapStatusHistories = new ArrayList<>();
		// Insert entries to STRUCTMAP_STATUS_HISTORY and STRUCTMAP_PID tables.
        try {
        	structMapStatusHistories = this.insertStructMapRelatedEntriesToDatabase(metsInfo, packageHistory);
        } catch (Exception e) {
        	LOG.error("An exception occured while inserting entries to the database structmap status and structmap pids tables for SIP [" + sipName + "]", e);
        }
		
		// Check whether to pause thread execution for testing purposes
		if (this.cellarConfiguration.isTestEnabled()
				&& this.cellarConfiguration.getTestProcessMonitorPhase().equals(ProcessMonitorPhase.Ingestion_SipDependencyChecker)) {
			this.testingThreadService.checkpointFreezingControl(ProcessMonitorPhase.Ingestion_SipDependencyChecker);
		}
        
        String metsId = metsInfo.get("metsId");
        
        // Get the operation type from the METS parser results, along with the required
        // relation types to look for.
        OperationType operationType = null;
        OperationTypeRelationTypes relTypes = null;
        try {
	    	operationType = OperationType.getByXmlValue(metsInfo.get("metsType"));
	    	relTypes = OperationTypeRelationTypes.findByValue(operationType.name());
        } catch (Exception e) {
        	String message = "Invalid METS operation type for SIP [" + sipName + "]";
        	handleException(sipWork, structMapStatusHistories, CommonErrors.UNKNOWN_METS_OPERATION, e, message, sipUncompressionFolder, metsId, metsFile);
        	return null;
        }
    	
        // Compute root & children PIDs.
        Set<String> rootAndChildrenPids = computeRootAndChildren(contentIdsPerDmdid);
        
        Set<String> directRelations = new HashSet<>();
        // Compute direct relations if applicable.
        if (relTypes.isSearchDirect()) {
        	final Model metadataModel = ModelFactory.createDefaultModel();
        	try {
        		directRelations = computeDirectRelations(contentIdsPerDmdid, businessMetadataRefsPerId, resolvedMetsPackage, metadataModel);
        	} catch (Exception e) {
        		MessageCode code = null;
        		if (e instanceof CellarException) {
        			code = ((CellarException) e).getCode();
        		}
        		if (code == null) {
        			code = CoreErrors.E_040;
        		}
        		String message = e.getMessage() + " for SIP [" + sipName + "]";
        		handleException(sipWork, structMapStatusHistories, code, e, message, sipUncompressionFolder, metsId, metsFile);
        		return null;
        	}
        	finally {
        		closeQuietly(metadataModel);
        	}
        }
		
        // Log the ending of the process in the log file and database.
		AuditBuilder.get(AuditTrailEventProcess.Ingestion).withAction(AuditTrailEventAction.SipDependencyChecker)
				.withType(AuditTrailEventType.End).withDuration(System.currentTimeMillis() - startTime).withObject(sipWork.getSipFile().getName())
				.withPackageHistory(sipWork.getPackageHistory())
				.withLogger(LOG).withLogDatabase(true).logEvent();
		
		return new SIPResourceDependencies(sipName, sipType, getTimestampAttribute(sipWork.getSipFile()),
				operationType,
				sipWork.getPackageHistory() != null ? sipWork.getPackageHistory().getId() : null,
				rootAndChildrenPids, directRelations);
	}
	
	/**
	 * Computes the root and children PIDs based on the data extracted from the 'div'
	 * elements of the SIP's METS file.
	 * @param contentIdsPerDmdid the CONTENTIDS attribute values of the 'div' elements
	 * of the METS file, categorized based on their DMDID attribute value.
	 * @return the set of calculated root and children PIDs that the current SIP depends on,
	 * after applying normalization.
	 */
	private Set<String> computeRootAndChildren(Map<String, List<String>> contentIdsPerDmdid) {
		return contentIdsPerDmdid.values().stream()
			.flatMap(List::stream)
			.filter(Objects::nonNull)
			.map(s -> Arrays.asList(s.split(" ")))
			.flatMap(List::stream)
			.map(this::convertToBaseCellarIdIfApplicable)
			.filter(StringUtils::isNotBlank)
			.collect(Collectors.toSet());
	}
	
	/**
	 * Computes the PIDs of the resources having a direct relationship with the
	 * current SIP.
	 * @param contentIdsPerDmdid the CONTENTIDS attribute values of the 'div' elements
	 * of the METS file, categorized based on their DMDID attribute value.
	 * @param businessMetadataRefsPerId the file-system-level references of the RDF files
	 * containing the digital objects' business metadata, categorized in terms of their id.
	 * @param resolvedMetsPackage the object containing the description of the METS file.
	 * @param metadataModel the metadata model built on the business metadata.
	 * @return the filtered and normalized PIDs of the direct-relation resources that the
	 * current SIP depends on.
	 */
	private Set<String> computeDirectRelations(Map<String, List<String>> contentIdsPerDmdid, Map<String, String> businessMetadataRefsPerId,
			MetsPackage resolvedMetsPackage, Model metadataModel) {
    	// Performs an initial filtering/cleanup of the URIs contained within each business metadata
		// RDF file, based on the values of the CONTENTIDS attribute of the 'div' elements associated
		// with it.
    	for (Entry<String, List<String>> divEntry : contentIdsPerDmdid.entrySet()) {
    		String dmdId = divEntry.getKey();
    		if (StringUtils.isNotBlank(dmdId)) {
    			String mdRef = businessMetadataRefsPerId.get(dmdId);
        		if (StringUtils.isNotBlank(mdRef)) {
        			Set<String> uris = divEntry.getValue().stream()
        			.map(s -> Arrays.asList(s.split(" ")))
        			.flatMap(List::stream)
        			.map(this::convertToUri)
        			.collect(Collectors.toSet());
        			ModelUtils.loadAllowedCellarResourcesOnly(resolvedMetsPackage, mdRef, this.cellarPrefixUris, uris, metadataModel);
        		}
    		}	
    	}
    	// Performs subsequent filtering on the metadata model built from the business metadata RDF files,
    	// as well as normalization of the related PIDs.
    	return extractDirectRelations(metadataModel).stream()
        			.map(this::convertToBaseCellarIdIfApplicable)
        			.collect(Collectors.toSet());
	}
	
	/**
	 * Converts the provided PID to base-Cellar ID, if the PID is a Cellar ID.
	 * @param pid the PID to convert, if applicable.
	 * @return the converted base-Cellar ID, or the provided PID unchanged. 
	 */
	private String convertToBaseCellarIdIfApplicable(String pid) {
		if (StringUtils.isNotEmpty(pid) && this.identifierService.isCellarUUID(pid)) {
			return CellarIdUtils.getCellarIdBase(pid);
		}
		return pid;
	}

	/**
	 * Filters and extracts the direct relation resource URIs from the provided
	 * metadata model.
	 * @param metadataModel the metadata model to parse.
	 * @return the filtered direct relation URIs extracted from the model.
	 */
	private Set<String> extractDirectRelations(Model metadataModel) {
		final Selector selector = new SimpleSelector() {

			// Filtering criterion definition.
			@Override
			public boolean selects(final Statement s) {
				final Resource subject = s.getSubject();
				final RDFNode object = s.getObject();
				// The object must be a URI.
				if (object.isURIResource()) {
					final String uriObject = object.asResource().getURI();
					final String uriSubject = subject.getURI();
					// The object URI and the subject URI must start with one of
					// the Cellar prefix URIs.
					if (StringUtils.startsWithAny(uriSubject, cellarPrefixUris)
							&& StringUtils.startsWithAny(uriObject, cellarPrefixUris)) {
						return true;
					}
				}
				return false;
			}
		};
		
		// Performs the filtering.
		Set<String> directRelations = new HashSet<>();
		String uriObject = null;
		String prefixObject = null;
		String prefixSubject = null;
		Statement statement = null;
		for (final StmtIterator it = metadataModel.listStatements(selector); it.hasNext();) {
			statement = it.next();
			uriObject = statement.getObject().asResource().getURI();
			prefixObject = this.identifierService.getPrefixedFromUri(uriObject);
			directRelations.add(prefixObject);
			prefixSubject = this.identifierService.getPrefixedFromUri(statement.getSubject().getURI());
			directRelations.add(prefixSubject);
		}
		return directRelations;
	}
	
	/**
	 * Converts the provided PID to CELLAR URI.
	 * @param contentId the PID to convert.
	 * @return the CELLAR URI corresponding to the provided PID.
	 */
	private String convertToUri(String contentId) {

		if (StringUtils.isBlank(contentId)) {
			throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.MISSING_ID_ON_STRUCTMAP)
					.withMessage("Work structmap content-id is empty")
					.build();
		}

		final StringTokenizer stk = new StringTokenizer(contentId, ":");
        final String prefixProduction = stk.nextToken();
        final String alias = prefixConfiguration.getPrefixUri(prefixProduction);
        
        if (null == alias) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.USED_PREFIX_IS_NOT_ALLOWED)
                    .withMessage("Alias not found for production prefix: {} in configuration").withMessageArgs(prefixProduction)
                    .build();
        }
        
        return contentId.replace(":", "/").replaceFirst(prefixProduction + "/", alias);
	}
	
	/**
	 * Initializes a MetsPackage by setting up its SIP extraction folder path
	 * along with the complete list of files contained within.
	 * @param sipDecompressionFolder the folder where the SIP under consideration
	 * has been extracted into.
	 * @return the initialized MetsPackage.
	 * @throws IOException
	 */
	private MetsPackage initMetsPackageForRelationDiscovery(String sipDecompressionFolder) throws IOException {
		SIPObject sipObject = new SIPObject();
        final File temporaryWorkFolder = new File(this.cellarConfiguration.getCellarFolderTemporaryWork());
        final File archiveFolder = new File(temporaryWorkFolder, sipDecompressionFolder);
		sipObject.setZipExtractionFolder(archiveFolder);
        final File[] files = this.fileService.getAllFilesFromUncompressedLocation(sipDecompressionFolder);
        final List<File> asList = files != null ? Arrays.asList(files) : new ArrayList<>();
        sipObject.setFileList(asList);

        return new ResolvedMetsPackage(sipObject);
	}
	
	/**
	 * Performs uncompression of the SIP.
	 * @param sipFile the SIP file to uncompress.
	 * @return the name of the folder that the SIP was extracted into.
	 * @throws IOException
	 */
	private String uncompress(File sipFile) throws IOException {
		String folder = null;
		// Uncompresses the file.
		try {
			folder = this.fileService.uncompressQuietlyMetadataOnly(sipFile);
		}
		catch (UncompressException e) {
			// File is corrupted, probably because it is still being copied: retry to uncompress after the specified delay.
            final int retryDelay = this.cellarConfiguration.getCellarServiceIngestionSipCopyRetryPauseSeconds() * 1000;
            final int timeout = this.cellarConfiguration.getCellarServiceIngestionSipCopyTimeoutSeconds() * 1000;
            if (CoreErrors.E_012.equals(e.getCode())) {
                FileSystemUtils.waitFileCompletionByChecksum(sipFile, retryDelay, timeout);
                // Retry to uncompress.
                folder = this.fileService.uncompressMetadataOnly(sipFile);
            } else if (CoreErrors.E_018.equals(e.getCode())) {
                // File is locked, probably because it is still being copied: retry to uncompress after the specified delay.
                FileSystemUtils.waitFileCompletionByLock(sipFile, retryDelay, timeout);
                // Retry to uncompress.
                folder = this.fileService.uncompressMetadataOnly(sipFile);
            } else {
                // Issue is not caused due to copying.
                throw e;
            }
		}
		return folder;
	}

	/**
	 * Exception handler. Logs the exception, backups the SIP file, and creates
	 * and writes a response package to the file system.
	 * @param sipWork the SIPWork object representing the SIP file.
	 * @param structMapStatusHistories list of STRUCTMAP_STATUS_HISTORY entries of this package
	 * @param code the CELLAR exception code.
	 * @param exception the exception.
	 * @param exceptionMessage the custom message to attach to the exception.
	 * @param metsFile the folder that the SIP was extracted into.
	 */
	private void handleException(SIPWork sipWork, List<StructMapStatusHistory> structMapStatusHistories,
								 MessageCode code, Exception exception, String exceptionMessage,
								 String sipUncompressionFolder, String metsId, File metsFile) {
		// Log the exception in the log file and database.
		AuditBuilder.get(AuditTrailEventProcess.Ingestion).withAction(AuditTrailEventAction.SipDependencyChecker)
	        .withType(AuditTrailEventType.Fail).withObject(sipWork.getSipFile().getName())
	        .withPackageHistory(sipWork.getPackageHistory())
	        .withCode(code)
	        .withMessage(exceptionMessage)
	        .withException(exception)
	        .withLogger(LOG).withLogDatabase(true).logEvent();
		
		// PACKAGE_HISTORY: Set the PREPROCESS_INGESTION_STATUS to FAILED
		if (sipWork.getPackageHistory() != null){
			this.packageHistoryService.updatePackageHistoryStatus(sipWork.getPackageHistory(), ProcessStatus.F);
			// STRUCTMAP_STATUS_HISTORY: Mark the INGESTION_STATUS of the corresponding STRUCTMAP_STATUS_HISTORY entries to FAILED
			this.structMapStatusHistoryService.updateMultipleStructMapIngestionStatus(
					structMapStatusHistories == null ? Collections.emptyList() : structMapStatusHistories,
					ProcessStatus.F);
		}
		
		// Extract the SIP name, METS ID, and METS header.
		String sipName = sipWork.getSipFile().getName();
		String metsIdToUse = (metsId != null) ? metsId : StringUtils.substringBefore(sipName, ".");
		String metsHdr = (metsFile != null) ? extractMetsHdr(metsFile) : StringUtils.EMPTY;
		
		// Perform SIP backup if applicable.
		handleSipBackup(sipWork, sipUncompressionFolder);
		
		final StructMapResponse response = new StructMapResponse();
        response.setMetsDocumentId(metsIdToUse);
        
        // Creates and writes a StructMap XML response.
        SIPResource sipResource = new SIPResource();
        sipResource.setSipType(sipWork.getType());
        final StructMapResponseDao structMapResponseDao = this.metsServiceFactory.getStructMapResponseDao(sipResource);
        structMapResponseDao.save(response);
        
        final MetsDocument metsDocument = new MetsDocument();
        metsDocument.setType(OperationType.Create);
        metsDocument.setDocumentId(metsIdToUse);
        final MetsResponseService metsResponseService = this.metsServiceFactory.getMetsResponseService(sipResource);
        
        // Writes the response package to the file system.
        sipResource.setMetsHdr(metsHdr);
        final MetsDocument responseMetsDocument = metsResponseService.loadStructMapResponse(metsDocument, sipResource);
        // Send callback request if a callback UEI exists in PACKAGE_HISTORY entry (only for ingestions triggered by METS Upload)
		metsUploadService.getStatusResponseAndSendCallbackRequest(metsFile, true, sipWork.getPackageHistory());
        final SIPResource responseSipResource = metsResponseService.createResponse(responseMetsDocument, true);
        try {
            final String responseSipResourceArchiveName = responseSipResource.getArchiveName();
            // If by any reason the error response file does not have the same name as the SIP,
            // it renames (by moving) the file to a package named like the SIP package.
            if (!StringUtils.equals(responseSipResourceArchiveName, sipName)) {
                final String responseSipResourceFolderName = responseSipResource.getFolderName();
                final File srcFile = new File(responseSipResourceFolderName, responseSipResourceArchiveName);
                final File destFile = new File(responseSipResourceFolderName, sipName);
                FileUtils.moveFile(srcFile, destFile);
            }
        } catch (final IOException e) {
            if (LOG.isErrorEnabled()) {
                LOG.error(e.getMessage(), e);
            }
        }
	}
	
	/**
	 * Performs backup of the SIP file, if applicable.
	 * @see eu.europa.ec.opoce.cellar.cl.service.impl.IngestionServiceImpl#handleSipBackup(SIPWork, SIPResource)
	 * @param sipWork the SIPWork object representing the SIP file.
	 * @param folderName the folder that the SIP was extracted into.
	 */
	private void handleSipBackup(SIPWork sipWork, String folderName) {
        try {
        	// Moves the SIP file into the error folder.
            this.backupService.errorSIPFile(sipWork.getSipFile());
            // In case of a corrupted file there is no SIP extraction folder.
            if (folderName != null) {
                // Moves the SIP extraction folder into the error folder.
                this.backupService.errorMetsFolder(folderName);
            }
        } catch (final Exception e) {
            if (LOG.isErrorEnabled()) {
                LOG.error(e.getMessage(), e);
            }
        }
    }
	
	/**
	 * Extracts the METS header section of the METS.
	 * @param metsFile the METS file to parse.
	 * @return the METS header.
	 */
	private String extractMetsHdr(File metsFile) {
		String metsHdr = null;
		try {
			metsHdr = this.metsAnalyzer.getMetsHdr(metsFile);
		} catch (IOException e) {
			// Do nothing.
		}
		return (metsHdr != null) ? metsHdr : StringUtils.EMPTY;
	}
	
	
	/**
	 * Gets either the "lastAccessed", "lastModified", or "lastChanged" timestamp
	 * attribute of the provided file. The actual attribute that
	 * is returned depends on the configuration. 
	 * @param file the file whose timestamp attribute shall be retrieved.
	 * @return the file's timestamp attribute.
	 */
	private Date getTimestampAttribute(File file) {
		String attributeName = this.cellarConfiguration.getCellarServiceSipQueueManagerFileAttributeDateType();
		FileTime attributeValue = (FileTime) FileSystemUtils.readAttribute(file, attributeName);
		if (attributeValue == null) {
			LOG.warn("Attribute [{}] of package [{}] could not be retrieved. Defaulting to current date.", attributeName, file);
			return new Date();
		}
		else {
			return new Date(attributeValue.toMillis());
		}
	}
	
	/**
	 * Insert (or find already existing) PACKAGE_HISTORY entry of CELLAROWNER schema
	 * @param sipWork current SIPWork
	 * @return the {@link PackageHistory} entry
	 */
	private PackageHistory retrieveOrCreatePackageHistoryEntry(SIPWork sipWork){
		
		// Identifying elements of a Package History entry
		String packageName = sipWork.getSipFile().getName();
		Date receivedDate = getTimestampAttribute(sipWork.getSipFile());

		// Search if this entry already exists in PACKAGE_HISTORY table
		PackageHistory packageHistory = this.packageHistoryService.findByPackageNameAndDate(packageName, receivedDate);

		if (packageHistory != null){
			LOG.debug("PACKAGE_HISTORY: Entry already exists -> {}", packageHistory);
		}
		else{
			// Define entry
			packageHistory = new PackageHistory(String.valueOf(UUID.randomUUID()), receivedDate, packageName, null, ProcessStatus.W);
			// Update in database
			packageHistory = this.packageHistoryService.saveEntry(packageHistory);
			LOG.debug("PACKAGE_HISTORY: Saved {}", packageHistory);
		}
		return packageHistory;
	}
	
	/**
	 * Insert entries to STRUCTMAP_STATUS_HISTORY and STRUCTMAP_PID tables of CELLAROWNER schema
	 * @param metsInfo information parsed from METS file.
	 * @param packageHistory package history entry that the current structmap belongs to.
	 *
	 * @return the list of {@link StructMapStatusHistory} objects
	 */
	private List<StructMapStatusHistory> insertStructMapRelatedEntriesToDatabase(Map<String, String> metsInfo, PackageHistory packageHistory) {
		
		List<StructMapStatusHistory> structMapStatusHistories = new ArrayList<>();
		
		// Provided that the PACKAGE_HISTORY entry was saved without exception ...
		if (packageHistory != null){
			// Iterate over the structMap info and insert corresponding entries to STRUCTMAP_STATUS_HISTORY
			// and STRUCTMAP_PID tables (recall "key" : "structMap_<name>", "value" : "top-level production identifiers")
			for(Map.Entry<String, String> entry : metsInfo.entrySet()){
				if(entry.getKey().contains("structMap")){
					
					String structMapName = entry.getKey()
							.replaceFirst("[0-9]+","")	// Replacing number prefix
							.replaceFirst("_structMap_","");	// Replacing _structMap_ label
					
					// Define STRUCTMAP_STATUS_HISTORY entry
					StructMapStatusHistory structMapStatusHistory = new StructMapStatusHistory();
					structMapStatusHistory.setStructMapName(structMapName);
					structMapStatusHistory.setPackageHistory(packageHistory);
					structMapStatusHistory.setIngestionStatus(ProcessStatus.W);
					
					List<StructMapPid> structMapPidEntries = new ArrayList<>();
					// Provided that there are CONTENT_IDS ...
					if (StringUtils.isNotBlank(entry.getValue())){
						// Recall the whitespace separation of entries
						Set<String> currentContentIds = new HashSet<>(Arrays.asList(entry.getValue().split("\\s+")));
						// Search in db for any existing "same-as" production identifiers
						List<ProductionIdentifier> psiFromDb = this.productionIdentifierDao.
								getProductionIdentifiersByProductionIds(new ArrayList<>(currentContentIds),
										this.cellarConfiguration.getCellarDatabaseInClauseParams());
						
						// Provided that there are existing production identifiers ...
						if(!psiFromDb.isEmpty()){
							// Set the corresponding CELLAR_ID in the STRUCTMAP_STATUS_HISTORY entry
							structMapStatusHistory.setCellarId(psiFromDb.get(0).getCellarIdentifier().getUuid());
							// Add the non-overlapping production identifiers to the set
							currentContentIds.addAll(psiFromDb.stream().map(ProductionIdentifier::getProductionId).collect(Collectors.toList()));
						}
						
						// Define the corresponding entries in the STRUCTMAP_PID table
						for( String psi : currentContentIds ){
							// Define STRUCTMAP_PID entry
							StructMapPid structMapPid = new StructMapPid();
							structMapPid.setProductionId(psi);
							structMapPidEntries.add(structMapPid);
						}
					}
					
					// Save STRUCTMAP_STATUS_HISTORY entry
					structMapStatusHistory = this.structMapStatusHistoryService.saveEntry(structMapStatusHistory);
					if(structMapStatusHistory != null){
						structMapStatusHistories.add(structMapStatusHistory);
						LOG.debug("STRUCTMAP_STATUS_HISTORY: Saved {}", structMapStatusHistory);
						for(StructMapPid structMapPid : structMapPidEntries ){
							// Save STRUCTMAP_PID entry
							structMapPid.setStructMapStatusHistory(structMapStatusHistory);
							structMapPid = this.structMapPidService.saveEntry(structMapPid);
							LOG.debug("STRUCTMAP_PID: Saved {}", structMapPid);
						}
					}	
				}
			}
		}		
		return structMapStatusHistories;
	}
	
	/**
	 * Contains the resource relationship types to compute based on the METS
	 * operation type.
	 */
	public enum OperationTypeRelationTypes {
		
		/**
		 * METS operation type CREATE.
		 */
		CREATE(true),
		/**
		 * METS operation type CREATE_OR_IGNORE.
		 */
		CREATEORIGNORE(true),
		/**
		 * METS operation type UPDATE.
		 */
		UPDATE(true),
		/**
		 * METS operation type DELETE.
		 */
		DELETE(false),
		/**
		 * METS operation type READ.
		 */
		READ(false);
		
		private boolean searchDirect;
		
		private OperationTypeRelationTypes(boolean searchDirect) {
			this.searchDirect = searchDirect;
		}
		
		/**
		 * Returns whether direct relations should be computed.
		 * @return whether direct relations should be computed.
		 */
		public boolean isSearchDirect() {
			return searchDirect;
		}
		
		/**
		 * Returns the entry corresponding to the provided value.
		 * @param value the value to look for.
		 * @return the corresponding entry.
		 */
		public static OperationTypeRelationTypes findByValue(String value) {
			for (OperationTypeRelationTypes otrt : OperationTypeRelationTypes.values()) {
				if (otrt.name().equalsIgnoreCase(value)) {
					return otrt;
				}
			}
			throw ExceptionBuilder.get(CellarException.class).withCode(CommonErrors.UNKNOWN_METS_OPERATION)
            	.withMessage("Cannot find OperationTypeRelationTypes with value '{}'.").withMessageArgs(value).build();
		}
	}
	
}
