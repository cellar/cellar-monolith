/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState
 *             FILE : ArchivingCleaning.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 5, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState;

import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution.EXECUTION_STATUS;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;

/**
 * <class_description> Archiving cleaning logic.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 5, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component("archivingCleaning")
public class ArchivingCleaning extends AbstractCleaning {

    private static final Logger LOG = LogManager.getLogger(ArchivingCleaning.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractTask execute(final ExtractionExecution extractionExecution) throws Exception {
        try {
            info(LOG, extractionExecution, "Archiving cleaning %s; %s",
                    super.licenseHolderService.getAbsoluteArchiveTempDirectoryPath(extractionExecution),
                    super.licenseHolderService.getAbsoluteArchiveFilePath(extractionExecution));

            this.clean(extractionExecution, true); // cleans execution and archive
            super.cleanError(extractionExecution); // cleans the error description

            return this.next(extractionExecution);
        } catch (Exception exception) {
            this.error(extractionExecution, exception);
            error(LOG, extractionExecution, exception);
            throw exception;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractTask next(final ExtractionExecution extractionExecution) {
        extractionExecution.setExecutionStatus(EXECUTION_STATUS.ARCHIVING_INIT);
        super.licenseHolderService.updateExtractionExecution(extractionExecution);

        return super.executionStateManager.getTaskState(EXECUTION_STATUS.ARCHIVING_INIT);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clean(final ExtractionExecution extractionExecution, final boolean cleanFinalArchive) throws Exception {
        final String absoluteArchiveTempDirectoryPath = super.licenseHolderService.getAbsoluteArchiveTempDirectoryPath(extractionExecution);
        if (absoluteArchiveTempDirectoryPath != null) { // if the path exists
            try {
                super.cleanFile(absoluteArchiveTempDirectoryPath);
            } catch (FileNotFoundException fileNotFoundException) {
                error(LOG, extractionExecution, fileNotFoundException);
            }
            extractionExecution.setArchiveTempDirectoryPath(null);
        }

        if (cleanFinalArchive && StringUtils.isNotBlank(extractionExecution.getArchiveFilePath())) {
            super.cleanArchiveTree(extractionExecution);
            extractionExecution.setArchiveFilePath(null);
        }
    }

    /**
     * Error.
     * @param extractionExecution the execution
     * @param exception the error description
     */
    protected void error(final ExtractionExecution extractionExecution, final Exception exception) {
        extractionExecution.setExecutionStatus(EXECUTION_STATUS.ARCHIVING_ERROR);
        super.error(extractionExecution, exception, false);
    }
}
