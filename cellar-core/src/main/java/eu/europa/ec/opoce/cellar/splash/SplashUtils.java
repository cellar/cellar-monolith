/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.splash
 *        FILE : SplashUtils.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 21-06-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.splash;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrSubstitutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static eu.europa.ec.opoce.cellar.AppConstants.NEWLINE;

/**
 * <class_description> Cellar splash utilities. <br/>
 * <br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc. <br/>
 * <br/>
 * ON : 21-06-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public class SplashUtils {

	/** class's logger. */
	private transient static final Logger LOGGER = LoggerFactory.getLogger(SplashUtils.class);

	// the width of border, where no char is writable
	private static final int BORDER_WIDTH = 2;

	// the character that builds the frame
	private static final char FRAME_CHAR = '#';

	// the character that builds the inner frames
	private static final char INNERFRAME_CHAR = '-';

	public static String loadSplash(final String splashPath, final Map<String, String> splashParams) {
		String splash = SplashUtils.loadSplash(splashPath);
		if (splash != null) {
			splash = StrSubstitutor.replace(splash, splashParams);
		}
		return splash;
	}

	public static String justifyPlainSplash(final String splash) {
		if (splash == null) {
			return splash;
		}

		String justifiedSplash = "";

		// get the length of the longest line as the splash width
		int splashWidth = getSplashWitdh(splash);
		if (splashWidth == -1) {
			return splash;
		}
		splashWidth = splashWidth + BORDER_WIDTH;

		// justify the splash
		BufferedReader br = null;
		try {
			br = new BufferedReader(new StringReader(splash));
			String currLine = null;
			boolean inHeader = false;
			while ((currLine = br.readLine()) != null) {
				String myCurrLine = currLine;

				// get the padding char
				char padChar = ' ';
				if (myCurrLine.endsWith("" + FRAME_CHAR)) {
					padChar = FRAME_CHAR;
				} else if (myCurrLine.endsWith("" + INNERFRAME_CHAR)) {
					padChar = INNERFRAME_CHAR;
				}

				// justify the line, if needed
				if (inHeader) {
					myCurrLine = FRAME_CHAR + StringUtils.center(myCurrLine, splashWidth - 1, padChar) + FRAME_CHAR;
				} else {
					myCurrLine = FRAME_CHAR + StringUtils.rightPad(myCurrLine, splashWidth - 1, padChar) + FRAME_CHAR;
				}

				// concatenate the line to the splash, but only if it all of its
				// placeholders have been replaced
				if (!myCurrLine.contains("${")) {
					if (justifiedSplash.length() > 0) {
						myCurrLine = NEWLINE + myCurrLine;
					}
					justifiedSplash += myCurrLine;
				}

				// check if the line is inner to the header or footer
				if (isHeaderSeparator(currLine)) {
					inHeader = !inHeader;
				}
			}
		} catch (IOException e) {
			LOGGER.warn("Unable to justify splash screen: it will be displayed as it is.", e);
			return splash;
		} finally {
			IOUtils.closeQuietly(br);
		}

		return justifiedSplash;
	}

	public static String justifyHtmlSplash(final String splash) {
		if (splash == null) {
			return splash;
		}

		String justifiedSplash = "";

		List<String> lines = new ArrayList<String>();
		try {
			lines = IOUtils.readLines(new StringReader(splash));
		} catch (IOException e) {
			LOGGER.warn("Unable to justify splash screen: it will be displayed as it is.", e);
			return splash;
		}
		for (String line : lines) {
			if (line.contains("${")) {
				continue;
			}

			String htmlSpacedLine = "";
			for (int i = 0; i < line.length(); i++) {
				char ch = line.charAt(i);
				if (ch == ' ') {
					htmlSpacedLine += "&nbsp;";
				} else {
					htmlSpacedLine += ch;
				}
			}
			justifiedSplash += htmlSpacedLine + "<br/>";
		}

		return justifiedSplash;
	}

	private static String loadSplash(final String splashPath) {
		// loads splash with unresolved placeholders
		final InputStream splashIS = SplashScreen.class.getClassLoader().getResourceAsStream(splashPath);
		if (splashIS == null) {
			LOGGER.warn("Unable to find splash screen file named '" + splashPath + "'.");
			return null;
		}
		byte[] splashBytes = null;
		try {
			splashBytes = new byte[splashIS.available()];
			splashIS.read(splashBytes);
			splashIS.close();
		} catch (IOException exc) {
			LOGGER.warn("Unable to load splash screen file named '" + splashPath + "'.", exc);
			return null;
		}
		return new String(splashBytes);
	}

	private static int getSplashWitdh(final String splash) {
		int splashWidth = 0;

		BufferedReader br = null;
		try {
			br = new BufferedReader(new StringReader(splash));
			String thisLine = null;

			while ((thisLine = br.readLine()) != null) {
				if (thisLine.length() > splashWidth) {
					splashWidth = thisLine.length();
				}
			}
		} catch (IOException e) {
			LOGGER.warn("Unable to justify splash screen: it will be displayed as it is.", e);
			return -1;
		} finally {
			IOUtils.closeQuietly(br);
		}

		return splashWidth;
	}

	private static boolean isHeaderSeparator(final String line) {
		return line.startsWith("" + FRAME_CHAR + FRAME_CHAR) && line.endsWith("" + FRAME_CHAR + FRAME_CHAR);
	}

}
