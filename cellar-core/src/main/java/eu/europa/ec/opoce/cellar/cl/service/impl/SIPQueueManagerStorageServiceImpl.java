/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *        FILE : SIPQueueManagerStorageServiceImpl.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 10-12-2021
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2021 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eu.europa.ec.opoce.cellar.ccr.service.database.DefaultTransactionService;
import eu.europa.ec.opoce.cellar.cl.dao.PackageHasParentPackageDao;
import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.PackageHasParentPackage;
import eu.europa.ec.opoce.cellar.cl.service.client.SIPQueueManagerStorageService;

/**
 * Implementation of the SIP Queue Manager Storage Service,
 * whose purpose is to maintain the appropriate structure of the DB queue
 * when new entries are added or existing entries are removed.
 * @author EUROPEAN DYNAMICS S.A.
 */
@Service
public class SIPQueueManagerStorageServiceImpl extends DefaultTransactionService implements SIPQueueManagerStorageService {

	
	private PackageHasParentPackageDao packageHasParentPackageDao;
	
	@Autowired
	public SIPQueueManagerStorageServiceImpl(PackageHasParentPackageDao packageHasParentPackageDao) {
		this.packageHasParentPackageDao = packageHasParentPackageDao;
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public void updateQueueOnInsert(Set<PackageHasParentPackage> queueEntriesToInsert, Map<Long, Long> queueEntriesToUpdate) {
		// Retrieve all entries from the DB queue.
		List<PackageHasParentPackage> queueEntriesFromDatabase = this.packageHasParentPackageDao.findAll();
		Map<Long, PackageHasParentPackage> queueEntriesFromDatabaseIndexed = queueEntriesFromDatabase.stream()
				.collect(Collectors.toMap(PackageHasParentPackage::getSipId, e -> e));
		
		// Insert new entries in the DB queue.
		for (PackageHasParentPackage phpp : queueEntriesToInsert) {
			this.packageHasParentPackageDao.saveObject(phpp);
		}
		
		// Update the PARENT_SIP_ID for the entries that already exist in the queue and that are affected by the new insertions.
		for (Map.Entry<Long, Long> queueEntryToUpdate : queueEntriesToUpdate.entrySet()) {
			Long sipIdToLookFor = queueEntryToUpdate.getKey();
			Long parentSipIdValueToSet = queueEntryToUpdate.getValue();
			PackageHasParentPackage queueEntryFromDatabaseToUpdate = queueEntriesFromDatabaseIndexed.get(sipIdToLookFor);
			if (queueEntryFromDatabaseToUpdate != null) {
				if (isShouldUpdateEntry(parentSipIdValueToSet, queueEntryFromDatabaseToUpdate.getParentSipId())) {
					queueEntryFromDatabaseToUpdate.setParentSipId(parentSipIdValueToSet);
					this.packageHasParentPackageDao.updateObject(queueEntryFromDatabaseToUpdate);
				}
			}
			else {
				// If a queue entry marked for update is no longer available, throw exception.
				throw new IllegalStateException("Attempted to update an entry in the DB queue that is no longer available.");
			}
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public List<PackageHasParentPackage> findBySipIdOrParentSipIdIn(Collection<Long> sipIdsToRemove) {
		return this.packageHasParentPackageDao.findBySipIdOrParentSipIdIn(sipIdsToRemove);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public void updateQueueOnRemoval(Set<PackageHasParentPackage> queueEntriesToRemove, Set<PackageHasParentPackage> queueEntriesToUpdate) {
		for (PackageHasParentPackage phpp : queueEntriesToUpdate) {
			this.packageHasParentPackageDao.updateObject(phpp);
		}
		this.packageHasParentPackageDao.deleteObjects(queueEntriesToRemove);
	}
	
	private boolean isShouldUpdateEntry(Long parentSipIdValueToSet, Long parentSipIdValueToBeUpdated) {
		if (parentSipIdValueToSet == null && parentSipIdValueToBeUpdated == null) {
			return false;
		}
		return parentSipIdValueToSet == null || parentSipIdValueToBeUpdated == null
					|| !parentSipIdValueToSet.equals(parentSipIdValueToBeUpdated);
	}
	
}
