package eu.europa.ec.opoce.cellar.support;

import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.InputStream;
import java.util.*;

public class MetsParser {

    private static final Logger LOG = LoggerFactory.getLogger(eu.europa.ec.opoce.cellar.support.MetsParser.class);

    private Map<String, String> identifierToStructMap;

    private Map<String, List<String>> structMapToProductionIdentifier;

    public MetsParser(final InputStream xml) {
        try {
            identifierToStructMap = new HashMap<String, String>();
            structMapToProductionIdentifier = new HashMap<String, List<String>>();
            SAXParser saxParser;
            final SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
            saxParserFactory.setFeature("http://xml.org/sax/features/external-general-entities", false);
            saxParserFactory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            saxParserFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            saxParser = saxParserFactory.newSAXParser();
            saxParser.parse(xml, new StructMapHandler());
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
    }

    public List<String> getProductionIdentifierListForStructMap(final String structMapID) {
        return structMapToProductionIdentifier.get(structMapID);
    }

    public String getStructMapForProductionIdentifier(final String productionIdentifier) {
        return identifierToStructMap.get(productionIdentifier);
    }

    public Set<String> getAllProductionIdentifiers() {
        return identifierToStructMap.keySet();
    }

    public Set<String> getAllStructMaps() {
        return structMapToProductionIdentifier.keySet();
    }

    class StructMapHandler extends DefaultHandler {

        private String currentStructMapId;

        @Override
        public void startElement(final String uri, final String localName, final String qName, final Attributes attributes)
                throws SAXException {
            if ("structMap".equals(qName)) {
                currentStructMapId = attributes.getValue("ID");
            } else if ("div".equals(qName)) {
                if (isToBeStored(attributes))
                    addObjectId(attributes.getValue("CONTENTIDS"));
            }
        }

        void addObjectId(final String objectIdentifiers) {
            if (objectIdentifiers != null) {
                StringTokenizer stringTokenizer = new StringTokenizer(objectIdentifiers, " ");
                while (stringTokenizer.hasMoreElements()) {
                    String identifier = stringTokenizer.nextElement().toString();
                    identifierToStructMap.put(identifier, currentStructMapId);
                    List<String> productionIdentifierList = structMapToProductionIdentifier.get(currentStructMapId);
                    if (productionIdentifierList == null)
                        structMapToProductionIdentifier.put(currentStructMapId, productionIdentifierList = new ArrayList<String>());
                    productionIdentifierList.add(identifier);
                }
            }
        }

        boolean isToBeStored(final Attributes attributes) {
            boolean isToBeStored = true;

            final String typeAttr = attributes.getValue("TYPE");
            try {
                final DigitalObjectType dot = DigitalObjectType.getByValue(typeAttr);
                isToBeStored = dot.isTopLevel();
            } catch (CellarException exc) {
                isToBeStored = false;
            }

            return isToBeStored;
        }
    }
}
