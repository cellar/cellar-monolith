package eu.europa.ec.opoce.cellar.ccr.service.client;

import java.io.File;

import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsDocument;

/**
 * Service to handle the creation of responses in xml mets format and generating SIPResource
 * archives with the generated files.
 * 
 * @author phvdveld
 * 
 */
public interface MetsResponseService {

    /**
     * Creates the xml mets response from the provided MetsDocument and creates the SIPResource to
     * be returned.
     * 
     * @param metsDocument
     *            the metsDocument to parse.
     * @param error
     *            to defined if the response should be create in the success folder or in the error
     *            folder.
     * @return a SIPResource containing the mets and its attached files
     */
    SIPResource createResponse(MetsDocument metsDocument, boolean error);

    /**
     * Gets the folder in which the mets files for the response will be created. If the folder does
     * not exist, it is created.
     * 
     * @param metsDocument
     * @return a File
     */
    File getMetsFolder(MetsDocument metsDocument);

    /**
     * Create a StructMap entity that will contain the logs of the transaction in the different
     * languages and also the generated response.
     * 
     * @param metsDocument
     *            MetsDocument instance.
     * @param sipResource
     *            SIPResource instance with SIP metadata.
     * @return a modified mets document.
     */
    MetsDocument loadStructMapResponse(MetsDocument metsDocument, SIPResource sipResource);
}
