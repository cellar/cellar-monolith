package eu.europa.ec.opoce.cellar.cl.service.mail;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.dao.email.EmailDao;
import eu.europa.ec.opoce.cellar.cl.domain.email.Email;
import eu.europa.ec.opoce.cellar.cl.domain.email.EmailStatus;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;

import static eu.europa.ec.op.cellar.api.util.StringHelper.UTF_8;

@Service
public class MailSenderServiceImpl implements MailSenderService {

    private static final Logger LOGGER = LogManager.getLogger(MailSenderServiceImpl.class);
    
    private final JavaMailSender mailSender;
    
    private final EmailDao emailDao;
    
    private final ICellarConfiguration cellarConfiguration;

    @Autowired
    public MailSenderServiceImpl(JavaMailSender mailSender, EmailDao emailDao, @Qualifier("cellarStaticConfiguration") ICellarConfiguration cellarConfiguration) {
        this.mailSender = mailSender;
        this.emailDao = emailDao;
        this.cellarConfiguration = cellarConfiguration;
    }

    @Override
    public void sendHtmlMessage(Email email) throws MessagingException, UnsupportedEncodingException {
        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage, true, UTF_8);
        mimeMessageHelper.setFrom(this.cellarConfiguration.getCellarMailSmtpFrom());
        mimeMessageHelper.setTo(email.getAddress());
        mimeMessageHelper.setSubject(email.getSubject());
        mimeMessageHelper.setText(email.getContent(), true);
        mailSender.send(mimeMessage);
    }
    
    @Override
    public List<Email> getEmailsToSend() {
        return this.emailDao.getEmailsToSend();
    }

    @Override
    @Transactional(readOnly = false, rollbackFor = Throwable.class)
    public void updateEmailOnSuccess(Email email) {
        email.setSentOn(new Date());
        email.setStatus(EmailStatus.SENT);
        emailDao.updateObject(email);
    }

    @Override
    @Transactional(readOnly = false, rollbackFor = Throwable.class)
    public void updateEmailOnFailure(Email email) {
        email.setStatus(EmailStatus.FAILED);
        emailDao.updateObject(email);
    }
    
    @Override
    @Transactional
    public int deleteEmailEntriesCreatedBefore(Date cleanupCutoffDate) {
    	List<Email> emailsToDelete = this.emailDao.getEmailsCreatedBefore(cleanupCutoffDate);
    	this.emailDao.deleteObjects(emailsToDelete);
    	return emailsToDelete.size();
    }
    
}
