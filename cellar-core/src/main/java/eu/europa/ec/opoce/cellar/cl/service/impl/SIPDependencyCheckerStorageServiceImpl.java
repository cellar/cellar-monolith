/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *        FILE : SIPDependencyCheckerStorageServiceImpl.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 03-12-2021
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2021 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;


import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eu.europa.ec.opoce.cellar.ccr.service.database.DefaultTransactionService;
import eu.europa.ec.opoce.cellar.cl.dao.RelationProdIdDao;
import eu.europa.ec.opoce.cellar.cl.dao.SIPPackageDao;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResourceDependencies;
import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.RelationProdId;
import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.SIPPackage;
import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.SIPPackageProcessingStatus;
import eu.europa.ec.opoce.cellar.cl.service.client.SIPDependencyCheckerStorageService;
import eu.europa.ec.opoce.cellar.cl.service.client.SIPQueueManager;

/**
 * Implementation of the SIP Dependency Checker Storage Service,
 * whose purpose is to provide access to the DB tables related to the
 * SIP Dependency Checker Service.
 * @author EUROPEAN DYNAMICS S.A.
 */
@Service
public class SIPDependencyCheckerStorageServiceImpl extends DefaultTransactionService implements SIPDependencyCheckerStorageService {

	private SIPPackageDao sipPackageDao;
	
	private RelationProdIdDao relationProdIdDao;
	
	private SIPQueueManager sipQueueManager; 
	
	@Autowired
	public SIPDependencyCheckerStorageServiceImpl(SIPPackageDao sipPackageDao, RelationProdIdDao relationProdIdDao, SIPQueueManager sipQueueManager) {
		this.sipPackageDao = sipPackageDao;
		this.relationProdIdDao = relationProdIdDao;
		this.sipQueueManager = sipQueueManager;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<SIPPackage> findAll() {
		return this.sipPackageDao.findAll();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public SIPPackage findSipBySipName(String sipName) {
		return this.sipPackageDao.findBySipName(sipName);
	}

	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public void storeResults(SIPResourceDependencies sipResourceDependencies) {		
		SIPPackage sipPackage = createSipPackageFromSource(sipResourceDependencies);
		Set<String> rootAndChildrenPids = sipResourceDependencies.getRootAndChildren();
		Set<String> directRelationsPids = sipResourceDependencies.getDirectRelations();
		
		for (String pid : rootAndChildrenPids) {
			// If the same PID exists as a direct relation PID,
			// remove it in order to avoid storing duplicate PIDs for a given SIP.
			directRelationsPids.remove(pid);
			
			RelationProdId relatedProdId = new RelationProdId(pid, false);
			sipPackage.addRelatedProdId(relatedProdId);
		}
		for (String pid : directRelationsPids) {
			RelationProdId relatedProdId = new RelationProdId(pid, true);
			sipPackage.addRelatedProdId(relatedProdId);
		}
		
		this.sipPackageDao.saveObject(sipPackage);
	}
	
	/**
	 * Converts the provided SIPResourceDependencies object containing the results of the
	 * dependency check procedure into a SIPPackage domain object that can be persisted
	 * in the database.
	 * @param sipResourceDependencies the object containing the results of the dependency
	 * check procedure.
	 * @return the persistence-ready SIPPackage object.
	 */
	private SIPPackage createSipPackageFromSource(SIPResourceDependencies sipResourceDependencies) {
		return new SIPPackage(sipResourceDependencies.getSipName(),
				sipResourceDependencies.getSipType(),
				sipResourceDependencies.getSipDetectionDate(),
				sipResourceDependencies.getOperationType(),
				sipResourceDependencies.getPackageHistoryId());
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public void syncDatabaseTables(Set<SIPPackage> packagesToDelete) {
		if (!packagesToDelete.isEmpty()) {
			// Update the DB queue.
			this.sipQueueManager.prioritizePackagesOnRemoval(packagesToDelete);
			
			Set<Long> sipIdsToDelete = packagesToDelete.stream().map(SIPPackage::getId).collect(Collectors.toSet());
			// Remove production identifiers related to the package references.
			this.relationProdIdDao.deleteBySipIdIn(sipIdsToDelete);
			// Remove packages references.
			this.sipPackageDao.deleteByIdIn(sipIdsToDelete);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<SIPPackage> findPackagesWithStatus(Collection<SIPPackageProcessingStatus> sipStatuses) {
		if (CollectionUtils.isNotEmpty(sipStatuses)) {
			return this.sipPackageDao.findBySipStatusIn(new HashSet<>(sipStatuses));
		}
		return Collections.emptyList();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public boolean updatePackageStatus(String sipName, SIPPackageProcessingStatus sipStatus) {
		SIPPackage ingestingSip = this.findSipBySipName(sipName);
		if (ingestingSip == null) {
			return false;
		}
		ingestingSip.setStatus(sipStatus);
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	public void updatePackagesStatus(Collection<SIPPackageProcessingStatus> sipStatusesToFind, SIPPackageProcessingStatus sipStatusToSet) {
		List<SIPPackage> sipPackages = findPackagesWithStatus(sipStatusesToFind);
		for (SIPPackage sipPackage : sipPackages) {
			sipPackage.setStatus(sipStatusToSet);
		}
	}
	
}
