/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker
 *        FILE : SIPPackage.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 02-12-2021
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2021 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker;

import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import eu.europa.ec.opoce.cellar.domain.content.mets.OperationType;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;

/**
 * Domain object representing the SIP_PACKAGE database table.
 * @author EUROPEAN DYNAMICS S.A.
 */
@Entity
@Table(name = "SIP_PACKAGE")
@NamedQueries({
    	@NamedQuery(name = "SIPPackage.findBySipName", query = "SELECT sip FROM SIPPackage sip WHERE sip.name = :sipName"),
    	@NamedQuery(name = "SIPPackage.findBySipStatusIn", query = "SELECT sip FROM SIPPackage sip WHERE sip.status IN (:sipStatuses)"),
    	@NamedQuery(name = "SIPPackage.countBySipStatusIn", query = "SELECT COUNT(sip) FROM SIPPackage sip WHERE sip.status IN (:sipStatuses)"),
    	@NamedQuery(name = "SIPPackage.findScheduledOrdered",
    		query = "SELECT sip "
    				+ "FROM SIPPackage sip "
    				+ "INNER JOIN PackageList pl ON pl.sipId = sip.id "
    				+ "WHERE sip.status = 'S' "
    				+ "ORDER BY pl.ordering ASC"),
    	@NamedQuery(name = "SIPPackage.deleteByIdIn", query = "DELETE FROM SIPPackage WHERE id IN (:ids)")
    })
public class SIPPackage implements Comparable<SIPPackage> {

	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "package_seq_gen")
	@SequenceGenerator(name = "package_seq_gen", sequenceName = "SIP_PCKG_SEQ", allocationSize = 1)
	@Column(name = "ID", unique = true, nullable = false)
	private Long id;
	/**
	 * 
	 */
	@Column(name = "NAME", unique = true, nullable  = false, length = 255)
	private String name;
	/**
	 * 
	 */
	@Column(name = "TYPE", nullable = false, length = 20)
	@Enumerated(EnumType.STRING)
	private TYPE type;
	/**
	 * 
	 */
	@Column(name = "DETECTION_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date detectionDate;
	/**
	 * 
	 */
	@Column(name = "OPERATION", nullable = false, length = 10)
	@Enumerated(EnumType.STRING)
	private OperationType operationType;
	/**
	 * 
	 */
	@Column(name = "STATUS", nullable = false, length = 10)
	private String status;
	/**
	 *
	 */
	@Column(name = "PACKAGE_HISTORY_ID", unique = true)
	private Long packageHistoryId;
	/**
	 * 
	 */
	@OneToMany(mappedBy = "sipPackage", cascade = CascadeType.ALL)
	private Set<RelationProdId> relatedProdIds;
	/**
	 * 
	 */
	@Transient
	private Date startDate;
	
	
	public SIPPackage() {
	}

	public SIPPackage(String name, TYPE type, Date detectionDate, OperationType operationType, Long packageHistoryId) {
		this.name = name;
		this.type = type;
		this.detectionDate = detectionDate;
		this.operationType = operationType;
		this.status = SIPPackageProcessingStatus.NEW.getValue();
		this.packageHistoryId = packageHistoryId;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public TYPE getType() {
		return type;
	}

	public void setType(TYPE type) {
		this.type = type;
	}

	public Date getDetectionDate() {
		return detectionDate;
	}

	public void setDetectionDate(Date detectionDate) {
		this.detectionDate = detectionDate;
	}

	public OperationType getOperationType() {
		return operationType;
	}

	public void setOperationType(OperationType operationType) {
		this.operationType = operationType;
	}

	public SIPPackageProcessingStatus getStatus() {
		return SIPPackageProcessingStatus.findByStatus(this.status);
	}

	public void setStatus(SIPPackageProcessingStatus status) {
		this.status = status.getValue();
	}
	
	public Long getPackageHistoryId() {
		return packageHistoryId;
	}
	
	public void setPackageHistoryId(Long packageHistoryId) {
		this.packageHistoryId = packageHistoryId;
	}
	
	public Set<RelationProdId> getRelatedProdIds() {
		if (relatedProdIds == null) {
			relatedProdIds = new HashSet<>();
		}
		return relatedProdIds;
	}

	public void setRelatedProdIds(Set<RelationProdId> relatedProdIds) {
		this.relatedProdIds = relatedProdIds;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	
	public void addRelatedProdId(RelationProdId relProdId) {
		getRelatedProdIds().add(relProdId);
		relProdId.setSipPackage(this);
	}
	
	public void removeRelatedProdId(RelationProdId relProdId) {
		getRelatedProdIds().remove(relProdId);
		relProdId.setSipPackage(null);
	}

	
	/**
	 * Converts a SIPWork object to its equivalent SIPPackage form.
	 * @param sw the SIPWork object to convert.
	 * @return the equivalent SIPPackage.
	 */
	public static SIPPackage toSIPPackage(SIPWork sw) {
		SIPPackage sp = new SIPPackage(sw.getSipFile().getName(), sw.getType(), sw.getCreationDate(),
				null,
				sw.getPackageHistory() != null ? sw.getPackageHistory().getId() : null);
		sp.setStartDate(sw.getStartDate());
		return sp;
	}
	
	
	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SIPPackage other = (SIPPackage) obj;
		return Objects.equals(name, other.name);
	}

	@Override
	public int compareTo(SIPPackage o) {
		// First criterion - SIP type ('AUTHENTICOJ'/'DAILY'/'BULK').
		int result = this.type.compareTo(o.getType());
		// Second criterion - SIP detection date.
		if (result == 0) {
			result = this.detectionDate.compareTo(o.getDetectionDate());
		}
		// Third criterion - SIP operation type ('Create'/'Update'/'Delete').
		if (result == 0) {
			result = this.operationType.compareTo(o.getOperationType());
		}
		// Fourth criterion - SIP name lexicographic ordering.
		// This is arbitrarily chosen as the final tie-breaker since SIP names
		// are unique and provide the necessary determinism in the ordering.
		if (result == 0) {
			result = this.name.compareToIgnoreCase(o.getName());
		}
		
		return result;
	}

	@Override
	public String toString() {
		return "SIPPackage [name: " + this.name + ", type: " + this.type + ", opereationType: " + this.operationType + ", detectionDate: " + this.detectionDate + ", status: " + this.status
				+ ", packageHistoryId: " + this.packageHistoryId + "]";
	}
	
}
