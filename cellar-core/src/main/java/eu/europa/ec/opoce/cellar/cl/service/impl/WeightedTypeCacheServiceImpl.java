/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *             FILE : WeightedTypeCacheServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 10, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.op.cellar.api.service.impl.ManifestationType;
import eu.europa.ec.op.cellar.api.util.WeightedManifestationTypes;
import eu.europa.ec.opoce.cellar.ccr.service.FileTypesNalSkosLoaderService;
import eu.europa.ec.opoce.cellar.cl.domain.mimeType.MimeTypeMapping;
import eu.europa.ec.opoce.cellar.cl.service.client.WeightedTypeCacheService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <class_description> Weighted manifestation types manager implementation.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 10, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class WeightedTypeCacheServiceImpl implements WeightedTypeCacheService {

    private static final Logger LOGGER = LogManager.getLogger(WeightedTypeCacheServiceImpl.class);

    @Autowired
    private FileTypesNalSkosLoaderService fileTypesNalSkosLoaderService;

    /**
     * {@inheritDoc}
     */
    @Override
    @Cacheable("weightedManifestationTypes")
    public WeightedManifestationTypes getWeightedManifestationTypes(final String contentExtension) {
        final WeightedManifestationTypes contentTypes = new WeightedManifestationTypes();
        final List<MimeTypeMapping> mimeTypeMappings = this.fileTypesNalSkosLoaderService.getMimeTypeMappingsByExtension(contentExtension);
        if (mimeTypeMappings == null || mimeTypeMappings.isEmpty()) {
            contentTypes.addWeightedManifestation(ManifestationType.DEFAULT, null);
            return contentTypes;
        }

        for (final MimeTypeMapping mimeTypeMapping : mimeTypeMappings) {
            Double weight = null;
            if (!contentTypes.getWeightedManifestationTypes().isEmpty()) {
                weight = 1D;
            }

            try {
                final ManifestationType manifestationType = ManifestationType.valueOf(mimeTypeMapping.getManifestationType().toUpperCase());
                contentTypes.addWeightedManifestation(manifestationType, weight);
            } catch (final IllegalArgumentException e) {
                LOGGER.debug("No manifestation type '{}' is available: ignoring it", mimeTypeMapping.getManifestationType());
            }
        }
        if (contentTypes.getWeightedManifestationTypes().isEmpty()) {
            LOGGER.debug("No matching manifestation type was found.");

            contentTypes.addWeightedManifestation(ManifestationType.DEFAULT, null);
        }

        return contentTypes;
    }

    public void setFileTypesNalSkosLoaderService(final FileTypesNalSkosLoaderService fileTypesNalSkosLoaderService) {//junit test purposes
        this.fileTypesNalSkosLoaderService = fileTypesNalSkosLoaderService;
    }

}
