package eu.europa.ec.opoce.cellar.cl.domain.response;

public class ReadStructMapOperation extends StructMapOperation {

    /** class's uid. */
    private static final long serialVersionUID = 8326607516293707544L;

    public ReadStructMapOperation() {
        super();
        this.setOperationType(ResponseOperationType.READ);
    }

    public ReadStructMapOperation(String id) {
        super(ResponseOperationType.READ, id);
    }

}
