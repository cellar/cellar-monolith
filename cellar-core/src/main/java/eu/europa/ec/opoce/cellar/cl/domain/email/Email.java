package eu.europa.ec.opoce.cellar.cl.domain.email;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "EMAIL")
@NamedQueries(value = {
	@NamedQuery(name = "getAllEmailsToSend", query =
	        "from Email where status = :new or status = :failed order by createdOn"
	),
	@NamedQuery(name = "getEmailsCreatedBefore", query =
    	"SELECT e FROM Email e WHERE e.createdOn <= :cutoffDate"
)
})
public class Email {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Column(name = "ADDRESS", nullable = false)
    private String address;

    @Column(name = "SUBJECT")
    private String subject;

    @Column(name = "CONTENT")
    private String content;

    @Column(name = "STATUS", nullable = false)
    @Convert(converter = EmailStatusConverter.class)
    private EmailStatus status;

    @Column(name = "CREATED_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "SENT_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date sentOn;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public EmailStatus getStatus() {
        return status;
    }

    public void setStatus(EmailStatus status) {
        this.status = status;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getSentOn() {
        return sentOn;
    }

    public void setSentOn(Date sentOn) {
        this.sentOn = sentOn;
    }
}
