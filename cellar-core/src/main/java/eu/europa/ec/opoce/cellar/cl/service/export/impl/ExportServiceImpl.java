/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.export.impl
 *             FILE : ExportServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 25-Oct-2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2016 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.export.impl;

import com.google.common.annotations.VisibleForTesting;
import eu.europa.ec.opoce.cellar.cl.concurrency.Concurrent;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.export.ExportedPackage;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cl.service.export.IExportService;
import eu.europa.ec.opoce.cellar.cl.service.export.exporter.IMetsElementExporter;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Work;
import eu.europa.ec.opoce.cellar.common.factory.IFactory;
import eu.europa.ec.opoce.cellar.common.util.CellarIdUtils;
import eu.europa.ec.opoce.cellar.core.dao.FileDao;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.domain.content.mets.OffIngestionOperationType;
import eu.europa.ec.opoce.cellar.exception.HttpStatusAwareExceptionBuilder;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.Mets;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.MetsType.MetsHdr;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.MetsType.MetsHdr.Agent;
import eu.europa.ec.opoce.cellar.mets.v2.builder.MetsBuilder;
import eu.europa.ec.opoce.cellar.mets.v2.builder.StructMapBuilder;
import eu.europa.ec.opoce.cellar.mets.v2.builder.enums.MetsProfile;
import eu.europa.ec.opoce.cellar.mets.v2.builder.enums.MetsType;
import eu.europa.ec.opoce.cellar.server.admin.nal.OntoAdminConfigurationService;
import eu.europa.ec.opoce.cellar.server.service.DisseminationDbGateway;
import eu.europa.ec.opoce.cellar.server.service.DisseminationException;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static eu.europa.ec.opoce.cellar.cl.service.export.impl.ExportServiceImpl.RecordStatus.COMPLETE;
import static eu.europa.ec.opoce.cellar.cl.service.export.impl.ExportServiceImpl.RecordStatus.PARTIAL;

/**
 * The Class ExportServiceImpl.
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 25-Oct-2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class ExportServiceImpl implements IExportService {

    private static final Logger LOG = LogManager.getLogger(ExportServiceImpl.class);

    private final DisseminationDbGateway disseminationDbGateway;
    private final IdentifierService identifierService;
    private final IFactory<IMetsElementExporter, MetsElement> metsElementExporterFactory;
    private final FileDao fileDao;
    private final ICellarConfiguration configuration;
    private final OntoAdminConfigurationService ontoConfig;
    private final CellarResourceDao cellarResourceDao;
    private final JAXBContext jaxbContext;

    @Autowired
    public ExportServiceImpl(DisseminationDbGateway disseminationDbGateway, @Qualifier("pidManagerService") IdentifierService identifierService,
                             @Qualifier("metsElementExporterFactory") IFactory<IMetsElementExporter, MetsElement> metsElementExporterFactory,
                             FileDao fileDao, @Qualifier("cellarStaticConfiguration") ICellarConfiguration configuration,
                             OntoAdminConfigurationService ontoConfig, CellarResourceDao cellarResourceDao) {
        this.disseminationDbGateway = disseminationDbGateway;
        this.identifierService = identifierService;
        this.metsElementExporterFactory = metsElementExporterFactory;
        this.fileDao = fileDao;
        this.configuration = configuration;
        this.ontoConfig = ontoConfig;
        this.cellarResourceDao = cellarResourceDao;
        try {
            this.jaxbContext = JAXBContext.newInstance(Mets.class);
        } catch (JAXBException e) {
            throw new RuntimeException("Cannot initialized JAXBContext for Mets", e);
        }
    }

    enum RecordStatus {
        COMPLETE,
        PARTIAL
    }

    @Override
    public ExportedPackage export(final String workId, final boolean metadataOnly, final boolean useAgnosticURL)
            throws JAXBException, IOException {
        return this.export(workId, metadataOnly, this.fileDao.getArchiveFolderExportRest(), useAgnosticURL);
    }

    @Override
    @Concurrent(locker = OffIngestionOperationType.EXPORT)
    public ExportedPackage export(final String workId, final boolean metadataOnly, final File destinationFolder,
                                  final boolean useAgnosticURL) throws JAXBException, IOException {

        final ExportedPackage exportedPackage = new ExportedPackage();
        LOG.info("Mets export processing started");

        final String cellarId = this.identifierService.getCellarPrefixedOrNull(workId);
        if (StringUtils.isBlank(cellarId)) {
            throw HttpStatusAwareExceptionBuilder.getInstance(DisseminationException.class).withHttpStatus(HttpStatus.NOT_FOUND)
                    .withMessage("Resource id '{}' is not found.").withMessageArgs(workId).build();
        }

        exportedPackage.setWorkIdentifier(workId);

        LOG.info("The mets export is exporting '{}'", cellarId);

        DigitalObjectType[] includeTypes = new DigitalObjectType[]{
                DigitalObjectType.WORK, DigitalObjectType.EXPRESSION, DigitalObjectType.MANIFESTATION, DigitalObjectType.ITEM};
        if (metadataOnly) {
            //excluding ITEM excludes the call to the ContentStreamMetsElementExporter which exports the content streams
            //all other DO types use the MetadataMetsElementExporter which exports the metadata
            includeTypes = new DigitalObjectType[]{
                    DigitalObjectType.WORK, DigitalObjectType.EXPRESSION
            };
        }
        final MetsElement metsElement = this.disseminationDbGateway.getCompleteTree(cellarId, includeTypes);

        final String metsDocumentId = getMetsDocumentId(workId);

        final Date createdDate = new Date();
        final MetsBuilder metsBuilder = MetsBuilder.create()
                .withMetsDocumentID(metsDocumentId)
                .withCREATEDATE(createdDate)
                .withPROFILE(MetsProfile.v2)
                .withTYPE(MetsType.create)
                .withAgnosticResourceURL(useAgnosticURL)
                .withMetsHdr(createMetsHdr(configuration.getCellarConfigurationVersion(),
                        getCDMVersion(), resolveRecordStatus(metsElement, metadataOnly).toString(), createdDate));

        final String structMapId = getStructMapId(cellarId);
        final StructMapBuilder structMapBuilder = StructMapBuilder.get().withID(structMapId);

        FileOutputStream fos = null;
        ZipOutputStream zos = null;

        try {
            final File destinationPackage = new File(destinationFolder, getZipFilename(metsDocumentId));
            exportedPackage.setExportedPackagePath(destinationPackage.getAbsolutePath());
            fos = new FileOutputStream(destinationPackage);
            zos = new ZipOutputStream(fos);

            this.metsElementExporterFactory.create(metsElement).exportMetsElement(metsBuilder, structMapBuilder, metsElement, zos);

            final Mets mets = metsBuilder.withStructMap(structMapBuilder.build()).build();

            final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true); // output pretty printed

            final OutputStream os = null;
            try {
                LOG.info("Add the mets file '{}' in the package.", getMetsXmlFilename(metsDocumentId));
                zos.putNextEntry(new ZipEntry(getMetsXmlFilename(metsDocumentId)));
                jaxbMarshaller.marshal(mets, zos);
            } finally {
                IOUtils.closeQuietly(os);
            }
        } finally {
            IOUtils.closeQuietly(zos);
            IOUtils.closeQuietly(fos);
        }

        LOG.info("Mets export processing finished");

        return exportedPackage;
    }

    /**
     * The record status is determined by the following rules:
     * <p>
     * <ul>
     *     <li>RecordStatus.COMPLETE if one of the following conditions is met:
     *     <ul>
     *         <li>metadataOnly=false</li>
     *         <li>metadataOnly=true and there is no third level object in the
     *          hierarchy associated with the root Cellar identifier. This is
     *          true for Works having expressions only, Dossiers, Agents and
     *          TopLevelEvents.
     *          </li>
     *     </ul>
     *     </li>
     *     <li>RecordStatus.PARTIAL otherwise</li>
     * </ul>
     *
     * @return the record status
     */
    @VisibleForTesting
    RecordStatus resolveRecordStatus(MetsElement metsElement, boolean metadataOnly) {
        if (!metadataOnly) {
            return COMPLETE;
        } else {
            switch (metsElement.getType()) {
                case DOSSIER:
                case AGENT:
                case TOPLEVELEVENT:
                    return COMPLETE;
                default:
                    return hasThirdLevelObject(metsElement) ? PARTIAL : COMPLETE;
            }
        }
    }

    private boolean hasThirdLevelObject(MetsElement metsElement) {
        if (metsElement instanceof Work) {
            Work work = (Work) metsElement;
            String id = work.getCellarId().getIdentifier();
            return cellarResourceDao.countManifestations(id.replace("cellar:", "")) > 0;
        }
        return false;
    }

    @VisibleForTesting
    static MetsHdr createMetsHdr(String cellarVersion, String cdmVersion, String recordStatus, Date createDate) {
        Agent agent = new Agent();
        agent.getNote().add("VERSION_CELLAR:" + cellarVersion);
        agent.getNote().add("VERSION_CDM:" + cdmVersion);
        agent.setName("CELLAR");
        agent.setROLE("CREATOR");
        agent.setTYPE("OTHER");
        agent.setOTHERTYPE("SOFTWARE");
        MetsHdr hdr = new MetsHdr();
        hdr.getAgent().add(agent);
        hdr.setRECORDSTATUS(recordStatus);
        hdr.setCREATEDATE(MetsBuilder.fromDate(createDate));
        return hdr;
    }

    @VisibleForTesting
    String getCDMVersion() {
        return ontoConfig.getGroupedOntologyVersions().stream()
                .filter(v -> v.getUri().equals("http://publications.europa.eu/ontology/cdm"))
                .map(v -> v.getNalOntoVersion().getVersionNumber())
                .findFirst()
                .orElse("undefined");
    }

    /**
     * Gets the mets document id.
     *
     * @param pid the pid
     * @return the mets document id
     */
    private static String getMetsDocumentId(final String pid) {
        return CellarIdUtils.getCellarIdAsFilename(pid + "_" + UUID.randomUUID().toString());
    }

    /**
     * Gets the struct map id.
     *
     * @param pid the pid
     * @return the struct map id
     */
    private static String getStructMapId(final String pid) {
        return CellarIdUtils.getCellarIdAsFilename("structMap_" + pid);
    }

    /**
     * Gets the mets xml filename.
     *
     * @param metsDocumentId the mets document id
     * @return the mets xml filename
     */
    private static String getMetsXmlFilename(final String metsDocumentId) {
        return metsDocumentId + ".mets.xml";
    }

    /**
     * Gets the zip filename.
     *
     * @param metsDocumentId the mets document id
     * @return the zip filename
     */
    private static String getZipFilename(final String metsDocumentId) {
        return metsDocumentId + ".zip";
    }

}
