/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.rdfStoreCleaner.client
 *             FILE : DigitalObjectLoaderService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 1, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.rdfStoreCleaner;

import eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner.CleanerDigitalObjectData;
import eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner.CleanerHierarchyData;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 1, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface DigitalObjectLoaderService {

    void loadHierarchyUris(final String rootCellarId, final CleanerHierarchyData cleanerHierarchyData);

    void loadDigitalObject(final CleanerDigitalObjectData cleanerDigitalObjectData);
}
