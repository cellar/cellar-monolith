/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.exception
 *        FILE : CompressException.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 18-12-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.exception;

/**
 * This is a {@link CompressException} buildable by an {@link ExceptionBuilder}.</br>
 * It signals that an compress exception of some kind has occurred. This class is the general class
 * of exceptions produced by failed or interrupted compress operations.</br>
 * </br>
 * ON : 18-12-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public class CompressException extends CellarException {

    private static final long serialVersionUID = 1L;

    /**
     * Constructs a new exception with its associated builder.
     * 
     * @param builder the builder to use for building the exception
     */
    public CompressException(final ExceptionBuilder<? extends BuildableException> builder) {
        super(builder);
    }
}
