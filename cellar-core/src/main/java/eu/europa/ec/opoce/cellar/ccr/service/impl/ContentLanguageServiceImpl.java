/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ccr.service.impl
 *             FILE : ContentLanguageServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jul 31, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.service.impl;

import eu.europa.ec.opoce.cellar.ccr.service.client.ContentLanguageService;
import eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <class_description> Content stream languages service implementation.
 * <br/><br/>
 * ON : Jul 31, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class ContentLanguageServiceImpl implements ContentLanguageService {

    /**
     * Give access to the PID tables.
     */
    @Autowired
    @Qualifier("pidManagerService")
    private PidManagerService pidManagerService;

    /**
     * {@inheritDoc}
     */
    @Override
    public String getContentLanguages(CellarResource resource) {
        List<String> languages = null;
        String pid = resource.getCellarId();
        if (pidManagerService.isManifestation(pid)) {
            pid = StringUtils.substringBeforeLast(pid, ".");
        }

        if (pidManagerService.isExpressionOrEvent(pid)) {
            languages = resource.getLanguages();
        }

        return StringUtils.join(languages, ",");
    }
}
