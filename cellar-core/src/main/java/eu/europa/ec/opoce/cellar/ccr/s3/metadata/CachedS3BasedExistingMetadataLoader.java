/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.ingest.impl
 *             FILE : CachedS3BasedExistingMetadataLoader.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 19-03-2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.s3.metadata;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.IS3Configuration;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.common.map.LRUSynchronizedMap;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.Lang;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

/**
 * <class_description> Service for loading existing data from S3.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 19-03-2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service("cachedS3BasedExistingMetadataLoader")
public class CachedS3BasedExistingMetadataLoader extends UncachedS3BasedExistingMetadataLoader {

    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    @Autowired
    @Qualifier("cellarConfiguration")
    private IS3Configuration s3Configuration;

    @Autowired
    private CellarResourceDao cellarResourceDao;

    @Autowired
    private ContentStreamService contentStreamService;

    // [root object's context - hierarchy object's datastreams map] for caching already processed entries
    private Map<String, Map<ContentType, String>> cachedDatastreams;

    @PostConstruct
    private void initialize() {
        this.cachedDatastreams = new LRUSynchronizedMap<>(
                this.cellarConfiguration.getCellarServiceIngestionPoolThreads()
                        * 60);
    }

    @Override
    public void restart() {
        initialize();
    }

    /**
     * @see eu.europa.ec.opoce.cellar.common.metadata.IExistingMetadataLoader#updateCache(Object, Object)
     */
    @Override
    public void updateCache(final String cellarId, final Map<ContentType, String> models) {
        this.cachedDatastreams.put(cellarId, models);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.common.metadata.IExistingMetadataLoader#evictCache(Object)
     */
    @Override
    public void evictCache(final String cellarId) {
        this.cachedDatastreams.remove(cellarId);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.common.metadata.IExistingMetadataLoader#getCache(Object)
     */
    @Override
    public Map<ContentType, String> getCache(final String cellarId) {
        return this.cachedDatastreams.get(cellarId);
    }

    @Override
    protected Map<String, Model> load(final String cellarId, final ContentType contentType) {
        final Map<String, Model> models = new HashMap<>();

        for (CellarResource resource : cellarResourceDao.findHierarchy(cellarId)) {
            if (resource.getCellarType() != DigitalObjectType.ITEM) {
                final String datastreamContext = resource.getCellarId();
                Map<ContentType, String> cachedModel = this.getCache(datastreamContext);
                Model model;
                if (cachedModel != null) {
                    model = JenaUtils.read(cachedModel.get(contentType), Lang.NT);
                } else {
                    final Map<ContentType, String> versions = ContentType.extract(resource.getVersions(), ContentType.DIRECT, ContentType.DIRECT_INFERRED);
                    final EnumMap<ContentType, String> m = contentStreamService.getContentsAsString(resource.getCellarId(), versions);
                    model = JenaUtils.read(m.get(contentType), Lang.NT);

                    updateCache(datastreamContext, m);
                }
                models.put(resource.getCellarId(), model);
            }
        }

        return models;
    }
}
