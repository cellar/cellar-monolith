/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : User.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 9, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.admin;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


/**
 * <class_description> Class that represents a user.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 9, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Entity
@Table(name = "USERS", uniqueConstraints = @UniqueConstraint(columnNames = {
        "USERNAME","EMAIL"}) )
@NamedQueries(value = {
		@NamedQuery(name = "findUserByUsername", query = "SELECT u FROM User u WHERE u.username = :username")
	})
public class User {

    /**
     * Internal id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    /**
     * The user name.
     */
    @Column(name = "USERNAME", nullable = false)
    private String username;

    /**
     * The user password hashed.
     */
    @Column(name = "PASSWORD")
    private String password;
    
    /**
     * The user e-mail.
     */
    @Column(name = "EMAIL")
    private String email;

    /**
     * Indicates if the user account is enabled.
     */
    @Column(name = "ENABLED", nullable = false)
    private boolean enabled;

    /**
     * The group of the user.
     */
    @ManyToOne
    @JoinColumn(name = "GROUP_ID")
    private Group group;

    /**
     * Default constructor.
     */
    public User() {

    }

    /**
     * Constructor.
     * @param id the identifier of the user.
     */
    public User(final Long id) {
        this.id = id;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(final String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(final String password) {
        this.password = password;
    }
    
    /**
     * @return the e-mail
     */
    public String getEmail() {
		return email;
	}

    /**
     * @param email the e-mail to set
     */
	public void setEmail(String email) {
		this.email = email;
	}

    /**
     * @return the enabled
     */
    public boolean isEnabled() {
        return this.enabled;
    }

    /**
     * @param enabled the enabled to set
     */
    public void setEnabled(final boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * @return the group
     */
    public Group getGroup() {
        return this.group;
    }

    /**
     * @param group the group to set
     */
    public void setGroup(final Group group) {
        this.group = group;
    }

    /**
     * @return the String corresponding to User object
     */
	@Override
	public String toString() {
		return "User [username=" + username + ", password=" + password + ", email=" + email + ", enabled=" + enabled
				+ ", group=" + group + "]";
	}
    
}
