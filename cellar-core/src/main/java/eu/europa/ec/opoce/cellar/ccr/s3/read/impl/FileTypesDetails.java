/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ccr.s3.read.impl
 *             FILE : FileTypesDetails.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 19 Apr 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.s3.read.impl;

import java.util.Objects;
import java.util.Set;

/**
 * The Class FileTypesDetails.
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 26 août 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class FileTypesDetails implements Comparable<FileTypesDetails> {

    /** The manifestation type. */
    private String manifestationType;

    /** The mime type. */
    private Set<String> mimeTypeSet;

    /** The extension. */
    private Set<String> extensionSet;

    /** The eudor type. */
    private String eudorType;

    /** The priority weight. */
    private Long priorityWeight;

    /** The textual. */
    private boolean textual;

    /**
     * Gets the manifestation type.
     *
     * @return the manifestation type
     */
    String getManifestationType() {
        return this.manifestationType;
    }

    /**
     * Sets the manifestation type.
     *
     * @param manifestationType the new manifestation type
     */
    void setManifestationType(final String manifestationType) {
        this.manifestationType = manifestationType;
    }

    /**
     * Gets the mime type.
     *
     * @return the mime type list
     */
    Set<String> getMimeTypeSet() {
        return this.mimeTypeSet;
    }

    /**
     * Sets the mime type.
     *
     * @param mimeTypeSet the mime type list
     */
    void setMimeTypeSet(final Set<String> mimeTypeSet) {
        this.mimeTypeSet = mimeTypeSet;
    }

    /**
     * Gets the extension.
     *
     * @return the extension
     */
    Set<String> getExtensionSet() {
        return this.extensionSet;
    }

    /**
     * Sets the extension.
     *
     * @param extensionSet the extension list
     */
    void setExtensionSet(final Set<String> extensionSet) {
        this.extensionSet = extensionSet;
    }

    /**
     * Gets the eudor type.
     *
     * @return the eudor type
     */
    String getEudorType() {
        return this.eudorType;
    }

    /**
     * Sets the eudor type.
     *
     * @param eudorType the new eudor type
     */
    void setEudorType(final String eudorType) {
        this.eudorType = eudorType;
    }

    /**
     * Gets the priority weight.
     *
     * @return the priority weight
     */
    Long getPriorityWeight() {
        return this.priorityWeight;
    }

    /**
     * Sets the priority weight.
     *
     * @param priorityWeight the new priority weight
     */
    void setPriorityWeight(final Long priorityWeight) {
        this.priorityWeight = priorityWeight;
    }

    /**
     * Checks if is textual.
     *
     * @return true, if is textual
     */
    boolean isTextual() {
        return this.textual;
    }

    /**
     * Sets the textual.
     *
     * @param isTextual the new textual
     */
    void setTextual(final boolean textual) {
        this.textual = textual;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object obj) {
        final FileTypesDetails that = (FileTypesDetails) obj;
        final boolean result = Objects.equals(this.getManifestationType(), that.getManifestationType())
                && Objects.equals(this.getMimeTypeSet(), that.getMimeTypeSet())
                && Objects.equals(this.getExtensionSet(), that.getExtensionSet())
                && Objects.equals(this.getEudorType(), that.getEudorType())
                && Objects.equals(this.getPriorityWeight(), that.getPriorityWeight())
                && Objects.equals(this.isTextual(), that.isTextual());
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return Objects.hash(manifestationType, mimeTypeSet, extensionSet, eudorType, priorityWeight, textual);
    }

    /** {@inheritDoc} */
    @Override
    public int compareTo(final FileTypesDetails o) {
        int result = 0;
        if (!equals(o)) {
            final Long thisPriority = getPriorityWeight();
            final Long thatPriority = o.getPriorityWeight();
            if ((thisPriority != null) && (thatPriority != null)) {
                result = thisPriority.compareTo(thatPriority);
            }
        }
        return result;
    }
}
