package eu.europa.ec.opoce.cellar.ccr.service;

import eu.europa.ec.opoce.cellar.cl.service.client.MimeTypeCacheService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.IndexingBestContentStrategy;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * This class implements the best content for indexing strategy by comparing a list of preferred formats with
 * format(s) from document datastreams of all manifestations attached to an 'expression' digital object.
 *
 * @author lderavet
 */
@Component("bestContentStrategy")
public class BestContentStrategy implements IndexingBestContentStrategy {

    private static final Logger LOG = LogManager.getLogger(BestContentStrategy.class);

    /**
     * The manager of mime types supported by Cellar.
     */
    @Autowired
    private MimeTypeCacheService mimeTypeCacheService;

    /**
     * key that correspond to the ITQL query value in the <code>select</code> clause.
     */
    private static final String DOCUMENT_TYPE_KEY = "\"type\"";

    /**
     * <p>Determine what the best content is for indexing. It takes the list of <i>manifestation-types</i> that has been set and check
     * if one or more document datastreams among all manifestation digital objects have a matching type.
     *
     * @param manifestations map that contains all manifestations digital objects attached to an expression and all their document datastream.
     * @return a list of document datastreams (e.g. <code>{"document"=info:s3/cellar:214/DOC_1, "type"=xml}, {"document"=info:s3/cellar:214/DOC_2, "type"=xml}, {"document"=info:s3/cellar:214/DOC_3, "type"=xml}</code> that match the <i>manifestation-type</i>.
     * It returns an empty list if no documents with a matching type has been found.
     *
     * deprecation: Unnecessary complex method based on string manipulation... use getBestContent(Map<String, List<CellarResource>>)
     */
    @Deprecated
    public List<Map<String, String>> getBestContent(LinkedMultiValueMap<String, Map<String, String>> manifestations) {
        final List<String> typesPriorityList = this.mimeTypeCacheService.getMimeTypesByPriorityDescList(); // gets the mime types ordered by priority
        for (Iterator<String> types = typesPriorityList.iterator(); types.hasNext(); ) {
            String type = types.next();

            Set<String> registeredManifestations = manifestations.keySet();

            for (Iterator<String> manifestationIt = registeredManifestations.iterator(); manifestationIt.hasNext(); ) {
                String manifestation = manifestationIt.next();

                List<Map<String, String>> documents = manifestations.get(manifestation);

                for (Iterator<Map<String, String>> iterator = documents.iterator(); iterator.hasNext(); ) {
                    Map<String, String> document = iterator.next();

                    String documentType = document.get(DOCUMENT_TYPE_KEY);

                    if (type.equalsIgnoreCase(documentType)) {
                        LOG.debug("Find best content for manifestations [{}] with Mime types [{}] -> [{}]", manifestations, typesPriorityList, documents);
                        return documents;
                    }
                }
            }
        }

        return new ArrayList(0);
    }

    @Override
    public List<CellarResource> getBestContent(Map<String, List<CellarResource>> manifestations) {
        final List<String> mimeTypes = this.mimeTypeCacheService.getMimeTypesByPriorityDescList();
        for (String mimeType : mimeTypes) {
            for (Map.Entry<String, List<CellarResource>> manifestation : manifestations.entrySet()) {
                if (matchMimeTypes(manifestation.getValue(), mimeType)) {
                    return manifestation.getValue();
                }
            }
        }
        return Collections.emptyList();
    }

    private static boolean matchMimeTypes(List<CellarResource> resources, String mimeType) {
        return resources.stream().anyMatch(r -> r.getMimeType().equals(mimeType));
    }
}
