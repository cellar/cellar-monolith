/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState
 *             FILE : Cleaning.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 5, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState;

import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution.EXECUTION_STATUS;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * <class_description> Cleaning logic.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 5, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component("cleaning")
public class Cleaning extends AbstractCleaning {

    private static final Logger LOG = LogManager.getLogger(Cleaning.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractTask execute(final ExtractionExecution extractionExecution) throws Exception {
        try {
            info(LOG, extractionExecution, "Cleaning %s; %s; %s",
                    super.licenseHolderService.getAbsoluteArchiveTempDirectoryPath(extractionExecution),
                    super.licenseHolderService.getAbsoluteArchiveFilePath(extractionExecution),
                    super.licenseHolderService.getAbsoluteSparqlTempFilePath(extractionExecution));

            this.clean(extractionExecution, true);
            return this.next(extractionExecution);
        } catch (Exception exception) {
            this.error(extractionExecution, exception);
            error(LOG, extractionExecution, exception);
            throw exception;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractTask next(final ExtractionExecution extractionExecution) {
        extractionExecution.setExecutionStatus(EXECUTION_STATUS.SPARQL_EXECUTING);
        super.licenseHolderService.updateExtractionExecution(extractionExecution);
        return super.executionStateManager.getTaskState(EXECUTION_STATUS.SPARQL_EXECUTING);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clean(final ExtractionExecution extractionExecution, final boolean cleanFinalArchive) throws Exception {
        // cleans all the steps
        super.executionStateManager.getCleaningState(EXECUTION_STATUS.ARCHIVING_CLEANING).clean(extractionExecution, cleanFinalArchive);
        super.executionStateManager.getCleaningState(EXECUTION_STATUS.PARSING_CLEANING).clean(extractionExecution, cleanFinalArchive);
        super.executionStateManager.getCleaningState(EXECUTION_STATUS.SPARQL_CLEANING).clean(extractionExecution, cleanFinalArchive);
        super.cleanError(extractionExecution);
    }

    /**
     * Error.
     * @param extractionExecution the execution
     * @param exception the error description
     */
    protected void error(final ExtractionExecution extractionExecution, final Exception exception) {
        extractionExecution.setExecutionStatus(EXECUTION_STATUS.CLEANING_ERROR);
        super.error(extractionExecution, exception, false);
    }
}
