/**
 * 
 */
package eu.europa.ec.opoce.cellar.cl.dao.impl;

import eu.europa.ec.opoce.cellar.cl.dao.AuditTrailEventDao;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEvent;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventProcess;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

/**
 * This DAO is responsible for managing the audit trail event objects.
 * 
 * @author dcraeye
 */
@Repository
public class AuditTrailEventDaoImpl extends BaseDaoImpl<AuditTrailEvent, Long> implements AuditTrailEventDao {

    @Override
    public void deleteAuditTrailEvent(final AuditTrailEvent auditTrailEvent) {
        super.deleteObject(auditTrailEvent);
    }

    /**
     * {@inheritDoc} 
     */
    @Override
    public void deleteAuditTrailEvent(final Date endDate) {
        final Session session = this.getHibernateTemplate().getSessionFactory().getCurrentSession();
        final String hqlDelete = "delete AuditTrailEvent a where a.date < :endDate";

        session.createQuery(hqlDelete).setDate("endDate", endDate).executeUpdate();
    }

    @Override
    public AuditTrailEvent getAuditTrailEvent(final Long id) {
        return super.getObject(id);
    }

    @Override
    public List<AuditTrailEvent> getAuditTrailEvents() {
        return super.findAll();
    }

    @Override
    public void saveAuditTrailEvent(final AuditTrailEvent auditTrailEvent) {
        super.saveObject(auditTrailEvent);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AuditTrailEvent> getLogsByProcess(final AuditTrailEventProcess process) {
        return this.findObjectsByNamedQuery("getLogsByProcess", process);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AuditTrailEvent> getLogsByIdentifier(final String id) {
        return this.findObjectsByNamedQuery("getLogsByIdentifier", id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AuditTrailEvent> getLogsByIdentifierWildcard(final String id) {
        return this.findObjectsByNamedQuery("getLogsByIdentifierWildcard", id.replaceAll("\\*", "%"));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AuditTrailEvent> getLogsByProcessAndIdentifierWildcard(final AuditTrailEventProcess process, final String id) {
        return this.findObjectsByNamedQuery("getLogsByProcessAndIdentifierWildcard", process, id.replaceAll("\\*", "%"));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AuditTrailEvent> getLogsByProcessAndIdentifier(final AuditTrailEventProcess process, final String id) {
        return this.findObjectsByNamedQuery("getLogsByProcessAndIdentifier", process, id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AuditTrailEvent> getLogs(final Date from, final Date to) {
        return this.findObjectsByNamedQuery("getLogsByDate", from, to);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AuditTrailEvent> getLogs(final Date from, final Date to, final int start, final int end) {
        return this.findPaginatedObjectsByNamedQuery("getLogsByDate", start, end, from, to);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AuditTrailEvent> getLogsByThreadName(final String threadName) {
        return this.findObjectsByNamedQuery("getLogsByThreadName", threadName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long countLogsBetweenDates(final Date dateFrom, final Date dateTo, final AuditTrailEventProcess process,
            final String objectIdentifier) {
        return getHibernateTemplate().execute(session -> {
            Query queryCount = null;

            if (!eu.europa.ec.opoce.cellar.common.util.StringUtils.isEmpty(objectIdentifier) && process != null) {
                queryCount = session.getNamedQuery("countLogsBetweenDatesByIdentifierAndProcess");
                queryCount.setParameter("objectIdentifier", objectIdentifier);
                queryCount.setParameter("process", process);
            } else if (!eu.europa.ec.opoce.cellar.common.util.StringUtils.isEmpty(objectIdentifier) && process == null) {
                queryCount = session.getNamedQuery("countLogsBetweenDatesByIdentifier");
                queryCount.setParameter("objectIdentifier", objectIdentifier);
            } else if (eu.europa.ec.opoce.cellar.common.util.StringUtils.isEmpty(objectIdentifier) && process != null) {
                queryCount = session.getNamedQuery("countLogsBetweenDatesByProcess");
                queryCount.setParameter("process", process);
            } else {
                queryCount = session.getNamedQuery("countLogsBetweenDates");
            }
            queryCount.setParameter("dateFrom", dateFrom);
            queryCount.setParameter("dateTo", dateTo);

            return Long.valueOf(queryCount.uniqueResult().toString());
        });

    }
}
