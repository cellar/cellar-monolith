/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.licenseHolder
 *             FILE : ArchivingBatchRunnable.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 12, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.licenseHolder;

import eu.europa.ec.opoce.cellar.cl.service.client.ExtractionExecutionService;
import eu.europa.ec.opoce.cellar.logging.LogContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.CMR_LICENSE_HOLDER;

/**
 * <class_description> Class that represents the archiving batch thread.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 12, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@LogContext
public class ArchivingBatchRunnable implements Runnable {

    /**
     * Logger instance.
     */
    private static final Logger LOG = LogManager.getLogger(ArchivingBatchRunnable.class);

    private final ExtractionExecutionService extractionExecutionService;
    private final ExtractionExecution extractionExecution;
    private final int firstOfBatch;
    private final int lastOfBatch;
    private final ArchiveTreeManager archiveTreeManager;
    private final ReentrantReadWriteLock readWriteLock;
    private final AtomicInteger archivingBatchWorker;

    /**
     * Constructor.
     *
     * @param extractionExecution        the extraction execution concerned
     * @param firstOfBatch               the index of the first (inclusive)
     * @param lastOfBatch                the index of the last (exclusive)
     * @param archiveTreeManager         the archive tree manager
     * @param readWriteLock              the reader writer lock corresponding to the execution
     * @param archivingBatchWorker       the batch worker counter
     * @param extractionExecutionService the extraction execution service
     */
    public ArchivingBatchRunnable(final ExtractionExecution extractionExecution, final int firstOfBatch, final int lastOfBatch,
                                  final ArchiveTreeManager archiveTreeManager, final ReentrantReadWriteLock readWriteLock,
                                  final AtomicInteger archivingBatchWorker, final ExtractionExecutionService extractionExecutionService) {
        this.extractionExecution = extractionExecution;
        this.firstOfBatch = firstOfBatch;
        this.lastOfBatch = lastOfBatch;
        this.archiveTreeManager = archiveTreeManager;
        this.readWriteLock = readWriteLock;
        this.archivingBatchWorker = archivingBatchWorker;
        this.extractionExecutionService = extractionExecutionService;
    }

    @Override
    @LogContext(CMR_LICENSE_HOLDER)
    public void run() {
        try {
            extractionExecutionService.executeBatch(extractionExecution, firstOfBatch, lastOfBatch, archiveTreeManager,
                    readWriteLock, archivingBatchWorker);
        } catch (Exception dataIntegrityViolationException) {
            LOG.info("Thread id {} - ExtractionExecution {}: Cleaning {}; {}; ", Thread.currentThread().getId(),
                    extractionExecution.getId(), extractionExecution.getArchiveTempDirectoryPath(),
                    extractionExecution.getArchiveFilePath(), extractionExecution.getSparqlTempFilePath());
            try {
                // cleans the execution
                extractionExecutionService.clean(extractionExecution, true);
            } catch (Exception exception) {
                LOG.error(exception.getMessage(), exception);
            }
        }
    }

}
