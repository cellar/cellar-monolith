package eu.europa.ec.opoce.cellar.ccr.mets.service;

import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.ccr.mets.exception.MetsMarshallingException;
import eu.europa.ec.opoce.cellar.ccr.mets.transform.MetsMapTransformer;
import eu.europa.ec.opoce.cellar.ccr.velocity.VelocityMarshaller;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsDocument;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import net.sf.saxon.TransformerFactoryImpl;
import org.apache.commons.lang.CharEncoding;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import javax.xml.XMLConstants;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.sax.SAXTransformerFactory;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

@Component("metsMarshaller")
public class MetsMarshallerImpl implements VelocityMarshaller {

    /** the velocity engine to make the foxml from the template. */
    @Qualifier("metsVelocityEngine")
    private @Autowired VelocityEngine velocityEngine;

    /** configuration for this marshaller. */
    private @Autowired MetsMarshallerConfiguration configuration;



    private static EntityResolver ent;

    private Logger LOG= LogManager.getLogger(getClass());

    public MetsMarshallerImpl()  {
         ent = new EntityResolver() {

            @Override
            public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {

                return new InputSource();
            }
        };
    }

    @Override
    public void marshal(Object toMarshal, File output) {
        this.marshal(configuration.getViewName(), toMarshal, output);

    }

    public void marshal(String viewName, Object toMarshal, File output) {
        try {
            Writer writer = new StringWriter();
            ModelMap model = new ModelMap();
            //TODO: PHVDV Add generic type
            model.put(configuration.getModelName(), new MetsMapTransformer().transform((MetsDocument) toMarshal));
            VelocityContext velocityContext = new VelocityContext(model);
            final String path = Optional.ofNullable(velocityEngine.getProperty("loader.path")).map(Object::toString).orElse("");
            velocityEngine.mergeTemplate(path + viewName, CharEncoding.UTF_8, velocityContext, writer);
            writer.close();
            try(FileWriter fileWriter=new FileWriter(output)) {
                Writer fw = new BufferedWriter(fileWriter);
                String formated = formatXml(writer.toString());
                fw.write(formated);
                fw.close();
            }
        } catch (IOException | SAXException e) {
            throw ExceptionBuilder.get(MetsMarshallingException.class).withCode(CoreErrors.E_050)
                    .withMessage("Unable to marshal the mets [" + output.getName() + "]").withCause(e).build();
        }
    }

    public String formatXml(String xml) throws SAXException {
        XMLReader reader = XMLReaderFactory.createXMLReader();
        reader.setEntityResolver(ent);
        try {
            final TransformerFactory trfactory =
                    SAXTransformerFactory.newInstance(TransformerFactoryImpl.class.getName(),
                    TransformerFactoryImpl.class.getClassLoader());
            trfactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            Transformer serializer = trfactory.newTransformer();
            serializer.setOutputProperty(OutputKeys.INDENT, "yes");
            serializer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            Source xmlSource = new SAXSource(reader,new InputSource(new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8))));
            try(ByteArrayOutputStream bos=new ByteArrayOutputStream()) {
                StreamResult res = new StreamResult(bos);
                serializer.transform(xmlSource, res);
                return new String(((ByteArrayOutputStream) res.getOutputStream()).toByteArray());
            }
        } catch (Exception e) {
            LOG.error("Unable to format the response mets",e);
            return xml;
        }
    }

    public void setMetsMarshallerConfiguration(MetsMarshallerConfiguration configuration) {
        this.configuration = configuration;
    }

}
