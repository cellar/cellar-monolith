/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor.export
 *             FILE : IMetsElementExport.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 24, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.export.exporter;

import eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement;
import eu.europa.ec.opoce.cellar.mets.v2.builder.IDivFptrAware;
import eu.europa.ec.opoce.cellar.mets.v2.builder.MetsBuilder;

import java.io.IOException;
import java.util.zip.ZipOutputStream;

import org.apache.jena.rdf.model.Model;

/**
 * <class_description> METS element export interface.
 *
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 24, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IMetsElementExporter {

    /**
     * Export the METS element.
     *
     * @param metsBuilder METS builder
     * @param parentDivFptrAware parent object
     * @param metsElement METS element to be exported
     * @param zos ZIP file
     * @throws IOException Signals that an I/O exception has occurred.
     */
    void exportMetsElement(final MetsBuilder metsBuilder, final IDivFptrAware<?> parentDivFptrAware, final MetsElement metsElement,
            final ZipOutputStream zos) throws IOException;

    /**
     * Export mets element.
     *
     * @param model the model
     * @param metsBuilder the mets builder
     * @param parentDivFptrAware the parent div fptr aware
     * @param metsElement the mets element
     * @param zos the zos
     * @throws IOException Signals that an I/O exception has occurred.
     */
    void exportMetsElement(final Model model, final MetsBuilder metsBuilder, final IDivFptrAware<?> parentDivFptrAware,
            final MetsElement metsElement, final ZipOutputStream zos) throws IOException;

    /**
     * Creates the model.
     *
     * @param model the model
     * @param metsElement the mets element
     */
    void createModel(final Model model, final MetsElement metsElement);

}
