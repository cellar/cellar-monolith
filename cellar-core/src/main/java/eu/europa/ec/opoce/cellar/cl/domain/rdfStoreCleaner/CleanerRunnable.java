/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner
 *             FILE : CleanerRunnable.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 14, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner;

import eu.europa.ec.opoce.cellar.cl.delegator.AbstractDelegator;
import eu.europa.ec.opoce.cellar.cl.rdfStoreCleaner.DelegateCleaner;
import eu.europa.ec.opoce.cellar.logging.LogContext;

import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.RDF_STORE_CLEANER;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 14, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@LogContext
public class CleanerRunnable implements Runnable {

    private final DelegateCleaner delegateCleaner;

    private final AbstractDelegator delegator;

    public CleanerRunnable(final AbstractDelegator delegator, DelegateCleaner delegateCleaner) {
        this.delegator = delegator;
        this.delegateCleaner = delegateCleaner;
    }

    @Override
    @LogContext(RDF_STORE_CLEANER)
    public void run() {
        delegateCleaner.clean(delegator);
    }

}
