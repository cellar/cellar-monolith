/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.support.cache
 *             FILE : AbstractCachedTableRecordsService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 18 août 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.support.cache;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;

import java.util.Calendar;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * The Class AbstractCachedTableRecordsService.
 * <class_description> This class is an abstraction of common structure used in cached in memory objects
 * <br/><br/>
 * ON : 18 août 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public abstract class AbstractCachedTableRecordsService {

    /** The Constant LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractCachedTableRecordsService.class);
    /** The cellar configuration. */
    @Autowired
    @Qualifier("cellarConfiguration")
    protected ICellarConfiguration cellarConfiguration;

    /** The last modification date. */
    protected AtomicLong lastModificationDate;

    /** The read and write lock */
    protected final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();

    /**
     * Gets the cache period.
     *
     * @return the cache period
     */
    abstract protected String getCachePeriod();

    /**
     * Can reload.
     *
     * @return true, if successful
     */
    abstract protected boolean canReload();

    /**
     * Should reload.
     *
     * @return true, if successful
     */
    abstract protected boolean shouldReload();

    /**
     * Reload.
     */
    abstract protected void reload();

    /**
     * Initialize.
     */
    @PostConstruct
    abstract protected void initialize();

    /**
     * Reload if necessary.
     */

    protected void reloadIfNecessary() {
        final boolean reload = checkLastModificationDate();
        if (reload) {
            reload();
        }
    }

    /**
     * Check last modification date.
     *
     * @return true, if successful
     */
    private boolean checkLastModificationDate() {
        boolean reload = false;
        if (this.lastModificationDate != null && canReload()) {
            //parse last modification date into a calendar object
            final Calendar lastModifiedDateCalendar = Calendar.getInstance();
            lastModifiedDateCalendar.setTimeInMillis(this.lastModificationDate.get());

            //default cache period - 1 month
            Integer amount = 1;
            int periodToAdd = Calendar.MONTH;

            //parse the configuration
            try {
                final String cachePeriod = getCachePeriod();
                final int length = cachePeriod.length();
                amount = Integer.parseInt(cachePeriod.substring(0, length - 1));
                final String periodString = cachePeriod.substring(length - 1, length);
                if (periodString.equalsIgnoreCase("m")) {
                    periodToAdd = Calendar.MONTH;
                } else if (periodString.equalsIgnoreCase("h")) {
                    periodToAdd = Calendar.HOUR;
                } else if (periodString.equalsIgnoreCase("n")) {
                    periodToAdd = Calendar.MINUTE;
                } else if (periodString.equalsIgnoreCase("w")) {
                    periodToAdd = Calendar.WEEK_OF_YEAR;
                } else if (periodString.equalsIgnoreCase("d")) {
                    periodToAdd = Calendar.DAY_OF_YEAR;
                }
            } catch (final Exception e) {
                LOGGER.warn("Unexpected exception was thrown when trying to parse Cache Period property: [" + getCachePeriod() + "]",
                        e.getMessage());
                LOGGER.warn("The default period will be used instead");
            }
            // if the period has passed re-initialize the in-memory map
            lastModifiedDateCalendar.add(periodToAdd, amount);
            final Calendar nowCalendar = Calendar.getInstance();
            reload = nowCalendar.after(lastModifiedDateCalendar);

        } else if (this.lastModificationDate == null || shouldReload()) {
            reload = true;
        }
        return reload;
    }

    /**
     * Update last modification date.
     */
    protected void updateLastModificationDate() {
        //set the last modification date
        final Calendar cal = Calendar.getInstance();
        this.lastModificationDate = new AtomicLong(cal.getTimeInMillis());
    }

}
