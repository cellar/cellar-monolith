/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ccr.s3.read.impl
 *             FILE : FileTypesNalSkosLoaderServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 26 août 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.s3.read.impl;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import eu.europa.ec.opoce.cellar.ccr.service.FileTypesNalSkosLoaderService;
import eu.europa.ec.opoce.cellar.cl.domain.mimeType.MimeTypeMapping;
import eu.europa.ec.opoce.cellar.common.Namespace;
import eu.europa.ec.opoce.cellar.nal.NalOntoUtils;
import eu.europa.ec.opoce.cellar.nal.dao.NalConfig;
import eu.europa.ec.opoce.cellar.support.cache.AbstractNalCachedTableRecordsService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * <class_description>
 * This service loads inside an in-memory map the file-types listed in the NAL file types stored in S3</br>
 * Only the file-types with all necessary codes will be kept in memory
 * Each file-type description has the following format
 * <p>
 * {@code
 * <skos:Concept rdf:about="http://publications.europa.eu/resource/authority/file-type/TIFF" at:deprecated="false">
 * <rdf:type rdf:resource="http://publications.europa.eu/ontology/euvoc#FileType"/>
 * <dc:identifier>TIFF</dc:identifier>
 * <at:authority-code>TIFF</at:authority-code>
 * <at:op-code>TIFF</at:op-code>
 * <atold:op-code>TIFF</atold:op-code>
 * <at:start.use>2012-09-01</at:start.use>
 * <at:op-mapped-code>
 * <at:MappedCode>
 * <dc:source>eudor-type-cellar</dc:source>
 * <at:legacy-code>TIFF</at:legacy-code>
 * </at:MappedCode>
 * </at:op-mapped-code>
 * <at:op-mapped-code>
 * <at:MappedCode>
 * <dc:source>file-extension</dc:source>
 * <at:legacy-code>.tiff</at:legacy-code>
 * </at:MappedCode>
 * </at:op-mapped-code>
 * <at:op-mapped-code>
 * <at:MappedCode>
 * <dc:source>file-extension</dc:source>
 * <at:legacy-code>.tif</at:legacy-code>
 * </at:MappedCode>
 * </at:op-mapped-code>
 * <at:op-mapped-code>
 * <at:MappedCode>
 * <dc:source>is-textual-cellar</dc:source>
 * <at:legacy-code>false</at:legacy-code>
 * </at:MappedCode>
 * </at:op-mapped-code>
 * <at:op-mapped-code>
 * <at:MappedCode>
 * <dc:source>manifestation-type-cellar</dc:source>
 * <at:legacy-code>tiff</at:legacy-code>
 * </at:MappedCode>
 * </at:op-mapped-code>
 * <at:op-mapped-code>
 * <at:MappedCode>
 * <dc:source>mime-type-cellar</dc:source>
 * <at:legacy-code>image/tiff</at:legacy-code>
 * </at:MappedCode>
 * </at:op-mapped-code>
 * <at:op-mapped-code>
 * <at:MappedCode>
 * <dc:source>mime-type-cellar</dc:source>
 * <at:legacy-code>image/tiff-fx</at:legacy-code>
 * </at:MappedCode>
 * </at:op-mapped-code>
 * <at:op-mapped-code>
 * <at:MappedCode>
 * <dc:source>priority-weight-cellar</dc:source>
 * <at:legacy-code>7</at:legacy-code>
 * </at:MappedCode>
 * </at:op-mapped-code>
 * </skos:Concept>
 * }
 * In case of error it should not prevent CELLAR from booting, the splash screen will show the that the file-types were not loaded and an error message is logged</br>
 * The interface allows the retrieval of file-type for a specified file extension , manifestation type or mime type</br>
 * if the file type is not mapped it returns null</br>
 * A last modification date is stored each time the skos file type file is loaded </br>
 * Whenever the service attempts to retrieve a file type it calls the last modification date is verified against the property cellar.service.fileTypes.skos.cachePeriod</br>
 * If the specified time period has passed the in-memory map is reloaded</br>
 * <br/>
 * Requirements for a concept to be loaded into the cache:
 * - only one manifestation-type-cellar must exist AND
 * - at least one mime-type-cellar must exist AND
 * - at least one file-extension must exist
 *
 * @author ARHS Developments
 * @version $Revision: 11439 $$
 */
@Service
public class FileTypesNalSkosLoaderServiceImpl extends AbstractNalCachedTableRecordsService implements FileTypesNalSkosLoaderService {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(FileTypesNalSkosLoaderServiceImpl.class);

    private static final String EMBEDDED_NAL_FILE_TYPE_NAME = "file-type.rdf";

    /**
     * The eudor type cellar.
     */
    private static final String EUDOR_TYPE_CELLAR = "eudor-type-cellar";

    /**
     * The file extension.
     */
    private static final String FILE_EXTENSION = "file-extension";

    /**
     * The manifestation type cellar.
     */
    private static final String MANIFESTATION_TYPE_CELLAR = "manifestation-type-cellar";

    /**
     * The mime type cellar.
     */
    private static final String MIME_TYPE_CELLAR = "mime-type-cellar";

    /**
     * The priority weight cellar.
     */
    private static final String PRIORITY_WEIGHT_CELLAR = "priority-weight-cellar";

    /**
     * The is textual cellar.
     */
    private final static String IS_TEXTUAL_CELLAR = "is-textual-cellar";

    /**
     * The file types details mappings by extension map.
     */
    private ConcurrentHashMap<String, List<MimeTypeMapping>> mimeTypeMappingByExtensionMap;

    /**
     * The file types details mapping by manifestation type map.
     */
    private ConcurrentHashMap<String, List<MimeTypeMapping>> mimeTypeMappingByManifestationTypeMap;

    /**
     * The file types details mapping by mime type type map.
     */
    private ConcurrentHashMap<String, List<MimeTypeMapping>> mimeTypeMappingByMimeTypeMap;

    /**
     * The file types details sorted set.
     */
    private SortedSet<FileTypesDetails> fileTypesDetailsSortedSet;

    /**
     * The mime type mapping by priority desc.
     */
    private Set<MimeTypeMapping> mimeTypeMappingByPriorityDesc;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MimeTypeMapping> getMimeTypeMappingByMimeType(String mimeType) {
        this.rwl.readLock().lock();
        try {
            reloadIfNecessary();
            return this.mimeTypeMappingByMimeTypeMap.get(mimeType);
        } finally {
            if (this.rwl != null && this.rwl.readLock() != null) {
                this.rwl.readLock().unlock();
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean areAllItemsLoaded() {
        boolean result;
        this.rwl.readLock().lock();
        try {
            result = (this.fileTypesDetailsSortedSet != null) && !this.fileTypesDetailsSortedSet.isEmpty();
        } finally {
            if (this.rwl != null && this.rwl.readLock() != null) {
                this.rwl.readLock().unlock();
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int numberOfItemDescriptionsLoaded() {
        int result;
        this.rwl.readLock().lock();
        try {
            result = this.fileTypesDetailsSortedSet.size();
        } finally {
            if (this.rwl != null && this.rwl.readLock() != null) {
                this.rwl.readLock().unlock();
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MimeTypeMapping> getMimeTypeMappingsByExtension(final String contentExtension) {
        this.rwl.readLock().lock();
        try {
            reloadIfNecessary();
            final List<MimeTypeMapping> result = this.mimeTypeMappingByExtensionMap.get(contentExtension);
            return result != null ? result : new ArrayList<>();
        } finally {
            if (this.rwl != null && this.rwl.readLock() != null) {
                this.rwl.readLock().unlock();
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<MimeTypeMapping> getMimeTypeMappingsByPriorityDesc() {
        this.rwl.readLock().lock();
        try {
            reloadIfNecessary();
            return new TreeSet<>(mimeTypeMappingByPriorityDesc).descendingSet();
        } finally {
            if (this.rwl != null && this.rwl.readLock() != null) {
                this.rwl.readLock().unlock();
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MimeTypeMapping> getMimeTypeMappingByManifestationType(final String manifestationType) {
        this.rwl.readLock().lock();
        try {
            reloadIfNecessary();
            return mimeTypeMappingByManifestationTypeMap.get(manifestationType);
        } finally {
            if (this.rwl != null && this.rwl.readLock() != null) {
                this.rwl.readLock().unlock();
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<String> getAllPossibleExtensions() {
        this.rwl.readLock().lock();
        try {
            reloadIfNecessary();
            Set<String> result = retrieveAllPossibleExtensions(this.fileTypesDetailsSortedSet);
            if (result == null) {
                result = new HashSet<>();
            }
            return result;
        } finally {
            if (this.rwl != null && this.rwl.readLock() != null) {
                this.rwl.readLock().unlock();
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<String> getFileExtensionMatchingMimetype(final String mimeType) {
        final Set<String> result = new HashSet<>();
        this.rwl.readLock().lock();
        try {
            reloadIfNecessary();
            if (StringUtils.isNotBlank(mimeType)) {
                final List<MimeTypeMapping> mimeTypeMappingList = mimeTypeMappingByMimeTypeMap.get(mimeType);
                if (mimeTypeMappingList != null) {
                    for (final MimeTypeMapping mimeTypeMapping : mimeTypeMappingList) {
                        if (mimeType.equals(mimeTypeMapping.getMimeType())) {
                            result.add(mimeType);
                        }
                    }
                }
            }
        } finally {
            if (this.rwl != null && this.rwl.readLock() != null) {
                this.rwl.readLock().unlock();
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void resetInMemoryMaps() {
        this.mimeTypeMappingByExtensionMap = new ConcurrentHashMap<>();
        this.fileTypesDetailsSortedSet = new ConcurrentSkipListSet<>();
        this.mimeTypeMappingByManifestationTypeMap = new ConcurrentHashMap<>();
        this.mimeTypeMappingByMimeTypeMap = new ConcurrentHashMap<>();
        this.mimeTypeMappingByPriorityDesc = new ConcurrentSkipListSet<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void reload() {
        LOGGER.info("The file-types skos will be reloaded");
        try {
            initialize();
        } catch (final Exception e) {
            LOGGER.warn("It was not possible to re-load the file-types skos", e);
            LOGGER.warn("The previously loaded file-types skos will be used");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getCachePeriod() {
        return this.cellarConfiguration.getCellarServiceFileTypesSkosCachePeriod();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean canReload() {
        boolean result;
        this.rwl.readLock().lock();
        try {
            result = (this.fileTypesDetailsSortedSet != null) && !this.fileTypesDetailsSortedSet.isEmpty();
        } finally {
            if (this.rwl != null && this.rwl.readLock() != null) {
                this.rwl.readLock().unlock();
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean shouldReload() {
        boolean result;
        this.rwl.readLock().lock();
        try {
            result = (this.fileTypesDetailsSortedSet == null) || this.fileTypesDetailsSortedSet.isEmpty();
        } finally {
            if (this.rwl != null && this.rwl.readLock() != null) {
                this.rwl.readLock().unlock();
            }
        }
        return result;
    }

    @Override
    protected void finishProcess() {
        LOGGER.info("NAL file-type has finished the processing. {} items descriptions were loaded.", this.fileTypesDetailsSortedSet.size());
    }

    @Override
    protected NalConfig getNalConfig() {
        return nalConfigDao.findByUri(NalOntoUtils.NAL_FILE_TYPE_URI).orElse(null);
    }

    @Override
    protected String getEmbeddedNal() throws IOException {
        URL url = FileTypesNalSkosLoaderServiceImpl.class.getResource(EMBEDDED_NAL_FILE_TYPE_NAME);
        return Resources.toString(url, Charsets.UTF_8);
    }

    @Override
    protected String getNalNamespace() {
        return Namespace.fileType_ontology;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void collectMappedCodes(final String subjectURI, final String opCode, final HashMap<String, String> mappedCodes, String identifier) {
        final FileTypesDetails fileTypesDetails = generateFileTypeDetails(mappedCodes);
        if (mandatoryInfoPresent(fileTypesDetails)) {
            this.fileTypesDetailsSortedSet.add(fileTypesDetails);
            fillMimeTypeMappingWithFileTypesDetails(fileTypesDetails);
        }
    }

    /**
     * Generate file type details.
     *
     * @param mappedCodes the mapped codes
     * @return the file types details
     */
    private FileTypesDetails generateFileTypeDetails(HashMap<String, String> mappedCodes) {
        final long priority = getPriority(mappedCodes);
        final String eudorType = mappedCodes.get(EUDOR_TYPE_CELLAR);
        final Set<String> extensionList = getExtensionSet(mappedCodes);
        final String manifestationType = mappedCodes.get(MANIFESTATION_TYPE_CELLAR);
        final Set<String> mimeTypeList = getMimeTypeSet(mappedCodes);

        FileTypesDetails details = new FileTypesDetails();
        details.setEudorType(eudorType);
        details.setExtensionSet(extensionList);
        details.setManifestationType(manifestationType);
        details.setMimeTypeSet(mimeTypeList);
        details.setPriorityWeight(priority);
        details.setTextual(Boolean.parseBoolean(mappedCodes.get(IS_TEXTUAL_CELLAR)));
        return details;
    }

    /**
     * Mandatory info present.
     *
     * @param fileTypesDetails the file types details
     * @return true, if successful
     */
    private boolean mandatoryInfoPresent(FileTypesDetails fileTypesDetails) {
        final String manifestationType = fileTypesDetails.getManifestationType();
        final Set<String> extensionSet = fileTypesDetails.getExtensionSet();
        final Set<String> mimeTypeSet = fileTypesDetails.getMimeTypeSet();
        return StringUtils.isNotBlank(manifestationType) && (extensionSet != null) && !extensionSet.isEmpty() && (mimeTypeSet != null)
                && !mimeTypeSet.isEmpty();
    }

    /**
     * Fill mime type mapping with file types details.
     *
     * @param fileTypesDetails the file types details
     */
    private void fillMimeTypeMappingWithFileTypesDetails(FileTypesDetails fileTypesDetails) {
        final Set<MimeTypeMapping> extractedMimeTypeMappingSet = extractMimeTypeMapping(fileTypesDetails);
        fillMyManifestationTypeMap(fileTypesDetails, extractedMimeTypeMappingSet);
        mimeTypeMappingByPriorityDesc.addAll(extractedMimeTypeMappingSet);

        for (final MimeTypeMapping mimeTypeMapping : extractedMimeTypeMappingSet) {
            fillByMimeTypeMap(mimeTypeMapping);
            fillByExtensionMap(mimeTypeMapping);
        }
    }

    /**
     * Fill by extension map.
     *
     * @param mimeTypeMapping the mime type mapping
     */
    private void fillByExtensionMap(MimeTypeMapping mimeTypeMapping) {
        final String extension = mimeTypeMapping.getExtension();
        List<MimeTypeMapping> mimeTypeMappingList = new LinkedList<>();
        if (mimeTypeMappingByExtensionMap.containsKey(extension)) {
            mimeTypeMappingList = mimeTypeMappingByExtensionMap.get(extension);
        }
        mimeTypeMappingList.add(mimeTypeMapping);
        mimeTypeMappingByExtensionMap.put(extension, mimeTypeMappingList);
    }

    /**
     * Fill by mime type map.
     *
     * @param mimeTypeMapping the mime type mapping
     */
    private void fillByMimeTypeMap(MimeTypeMapping mimeTypeMapping) {
        final String mimeType = mimeTypeMapping.getMimeType();
        List<MimeTypeMapping> mimeTypeMappingList = new LinkedList<>();
        if (mimeTypeMappingByMimeTypeMap.containsKey(mimeType)) {
            mimeTypeMappingList = mimeTypeMappingByMimeTypeMap.get(mimeType);
        }
        mimeTypeMappingList.add(mimeTypeMapping);
        mimeTypeMappingByMimeTypeMap.put(mimeType, mimeTypeMappingList);
    }

    /**
     * Fill my manifestation type map.
     *
     * @param fileTypesDetails             the file types details
     * @param extractedMimeTypeMappingList the extracted mime type mapping list
     */
    private void fillMyManifestationTypeMap(FileTypesDetails fileTypesDetails, Set<MimeTypeMapping> extractedMimeTypeMappingList) {
        final String manifestationType = fileTypesDetails.getManifestationType();
        mimeTypeMappingByManifestationTypeMap.put(manifestationType, new ArrayList<>(extractedMimeTypeMappingList));
    }

    /**
     * Extract mime type mapping.
     *
     * @param fileTypesDetails the file types details
     * @return the sets the
     */
    private Set<MimeTypeMapping> extractMimeTypeMapping(FileTypesDetails fileTypesDetails) {
        final Set<MimeTypeMapping> mimeTypeMappingSet = new HashSet<>();
        for (final String mimeType : fileTypesDetails.getMimeTypeSet()) {
            for (final String extension : fileTypesDetails.getExtensionSet()) {
                final MimeTypeMapping mimeTypeMapping = createCommonMimeTypeMapping(fileTypesDetails);
                mimeTypeMapping.setExtension(extension);
                mimeTypeMapping.setMimeType(mimeType);
                mimeTypeMappingSet.add(mimeTypeMapping);
            }
        }
        return mimeTypeMappingSet;
    }

    /**
     * Creates the common mime type mapping.
     *
     * @param fileTypesDetails the file types details
     * @return the mime type mapping
     */
    private MimeTypeMapping createCommonMimeTypeMapping(FileTypesDetails fileTypesDetails) {
        final MimeTypeMapping mimeTypeMapping = new MimeTypeMapping();
        mimeTypeMapping.setEudorType(fileTypesDetails.getEudorType());
        mimeTypeMapping.setManifestationType(fileTypesDetails.getManifestationType());
        mimeTypeMapping.setPriority(fileTypesDetails.getPriorityWeight());
        mimeTypeMapping.setTextual(fileTypesDetails.isTextual());
        return mimeTypeMapping;
    }

    /**
     * Gets the mime type set.
     *
     * @param mappedCodes the mapped codes
     * @return the mime type set
     */
    private Set<String> getMimeTypeSet(HashMap<String, String> mappedCodes) {
        final Set<String> mimeTypeSet = new HashSet<>();
        final String mimeTypeListAsString = mappedCodes.get(MIME_TYPE_CELLAR);
        if (StringUtils.isNotBlank(mimeTypeListAsString)) {
            mimeTypeSet.addAll(Arrays.asList(mimeTypeListAsString.split(",")));
        }
        return mimeTypeSet;
    }

    /**
     * Gets the extension set.
     *
     * @param mappedCodes the mapped codes
     * @return the extension set
     */
    private Set<String> getExtensionSet(HashMap<String, String> mappedCodes) {
        String extension = mappedCodes.get(FILE_EXTENSION);
        final Set<String> extensionSet = new HashSet<>();
        if (extension != null) {//the file-type NAL was created with the extensions starting with a dot
            extension = extension.replace(".", "");
            extensionSet.addAll(Arrays.asList(extension.split(",")));
        }
        return extensionSet;
    }

    /**
     * Gets the priority.
     *
     * @param mappedCodes the mapped codes
     * @return the priority
     */
    private long getPriority(HashMap<String, String> mappedCodes) {
        return mappedCodes.get(PRIORITY_WEIGHT_CELLAR) != null ? Long.parseLong(mappedCodes.get(PRIORITY_WEIGHT_CELLAR)) : 0;
    }

    /**
     * Retrieve all possible extensions.
     *
     * @param input the input
     * @return the sets the
     */
    private Set<String> retrieveAllPossibleExtensions(final Collection<FileTypesDetails> input) {
        final Set<String> result = new TreeSet<>();
        input.stream().map(FileTypesDetails::getExtensionSet).forEach(result::addAll);
        return result;
    }

}
