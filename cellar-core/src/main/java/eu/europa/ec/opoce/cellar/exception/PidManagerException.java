/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.exception
 *        FILE : PidManagerException.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 30-01-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.exception;

/**
 * This is a {@link PidManagerException} buildable by an {@link ExceptionBuilder}.</br>
 * It is usually thrown during the execution of PidManager services.</br>
 * </br>
 * ON : 30-01-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public class PidManagerException extends CellarException {

    private static final long serialVersionUID = 1L;

    /**
     * Constructs a new exception with its associated builder.
     * 
     * @param builder the builder to use for building the exception
     */
    public PidManagerException(final ExceptionBuilder<? extends BuildableException> builder) {
        super(builder);
    }
}
