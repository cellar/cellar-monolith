/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner
 *             FILE : RDFStoreCleanerScheduler.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Sep 23, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner;

import eu.europa.ec.opoce.cellar.cl.configuration.enums.CellarServiceRDFStoreCleanerMode;
import eu.europa.ec.opoce.cellar.cl.domain.AbstractBatchManager;
import eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner.RDFStoreCleanerManager.RDFStoreCleanerBatchManager;
import eu.europa.ec.opoce.cellar.cl.service.rdfStoreCleaner.RDFStoreCleanerService;
import eu.europa.ec.opoce.cellar.logging.LogContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.RDF_STORE_CLEANER;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Sep 23, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component("rdfStoreCleanerManager")
public class RDFStoreCleanerManager extends AbstractBatchManager<RDFStoreCleanerBatchManager> {

    private static final Logger LOG = LogManager.getLogger(RDFStoreCleanerManager.class);

    /**
     * The rdf store cleaner service.
     */
    @Autowired
    private RDFStoreCleanerService rdfStoreCleanerService;

    @LogContext
    class RDFStoreCleanerBatchManager extends Thread {

        @Override
        @LogContext(RDF_STORE_CLEANER)
        public void run() {
            while (isEnabled()) {
                if (!rdfStoreCleanerService.cleanRDFStoreUnknown()) {
                    break;
                }
            }

            if (cellarConfiguration.getCellarServiceRDFStoreCleanerMode() == CellarServiceRDFStoreCleanerMode.fix) {
                while (isEnabled()) {
                    if (!rdfStoreCleanerService.cleanRDFStoreCorrupt()) {
                        break;
                    }
                }
            }

            LOG.info("There is no more Cellar resource cleaning to schedule.");
        }
    }

    @Override
    protected RDFStoreCleanerBatchManager getThreadInstance() {
        return new RDFStoreCleanerBatchManager();
    }
}
