/**
 *
 */
package eu.europa.ec.opoce.cellar.cl.dao.impl;

import eu.europa.ec.opoce.cellar.cl.dao.CellarIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation of the Dao for the the Entity {@link CellarIdentifier}.
 *
 * @author dsavares
 */
@Repository
public class CellarIdentifierDaoImpl extends BaseDaoImpl<CellarIdentifier, Long> implements CellarIdentifierDao {

    private static final String REQ_GET_IDS_WITH_CS = "select c.uuid as cellarId, p.production_id as prodId from cellar_identifier c "
            + "inner join production_identifier p on c.id = p.cellar_identifier_id " + "where uuid like ?0";
    private static final String REQ_GET_IDS = "select c.uuid as cellarId, p.production_id as prodId from cellar_identifier c "
            + "inner join production_identifier p on c.id = p.cellar_identifier_id " + "where uuid like ?0 and uuid not like ?1";

    @Override
    public void deleteCellarIdentifier(final CellarIdentifier cellarIdentifier) {
        super.deleteObject(cellarIdentifier);
    }

    @Override
    public CellarIdentifier getCellarIdentifier(final Long id) {
        return super.getObject(id);
    }

    @Override
    public CellarIdentifier getCellarIdentifier(final String identifier, final String fileRef) {
        return super.findObjectByNamedQuery("getByFileRef", "cellar:" + identifier + "/%", fileRef);
    }

    @Override
    public CellarIdentifier saveCellarIdentifier(final CellarIdentifier cellarIdentifier) {
        return super.saveObject(cellarIdentifier);
    }

    @Override
    public void updateCellarIdentifier(final CellarIdentifier cellarIdentifier) {
        super.updateObject(cellarIdentifier);
    }

    @Override
    public CellarIdentifier getCellarIdentifier(final String identifier) {
        return super.findObjectByNamedQuery("getByCellarIdentifier", identifier);
    }

    @Override
    public boolean isCellarIdentifierExists(final CellarIdentifier cellarIdentifier) {
        return this.getCellarIdentifier(cellarIdentifier.getUuid()) != null;
    }

    @Override
    public List<CellarIdentifier> getExpressionChildrenPids(final String expressionCellarId) {
        return super.findObjectsByNamedQuery("getByCellarIdentifierLike", expressionCellarId + ".__");
    }

    @Override
    public List<CellarIdentifier> getChildrenPids(final String cellarId) {
        final List<CellarIdentifier> childrenPids = new ArrayList<>();
        final List<CellarIdentifier> cellarIdentifier = super.findObjectsByNamedQuery("getByCellarIdentifierOrLike", cellarId,
                cellarId + ".%");
        childrenPids.addAll(cellarIdentifier);
        return childrenPids;
    }

    @Override
    public List<CellarIdentifier> getAllChildrenPids(final String cellarId) {
        final List<CellarIdentifier> result = new ArrayList<CellarIdentifier>();
        final List<CellarIdentifier> cellarIdentifier = super.findObjectsByNamedQuery("getByCellarIdentifierOrLike", cellarId,
                cellarId + "%");
        result.addAll(cellarIdentifier);
        return result;
    }

    @Override
    public List<Identifier> getTreePid(final String cellarId) {
        return this.getHibernateTemplate().execute(session -> {
            final SQLQuery query = session.createSQLQuery(REQ_GET_IDS_WITH_CS);
            query.addScalar("cellarId", StringType.INSTANCE);
            query.addScalar("prodId", StringType.INSTANCE);
            query.setString(0, cellarId + "%");
            return buildIdList(query.list());
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Identifier> getTreePid(final String cellarId, final boolean withContentStream) {
        if (withContentStream) {
            return this.getTreePid(cellarId);
        }
        return this.getHibernateTemplate().execute(session -> {
            final SQLQuery query = session.createSQLQuery(REQ_GET_IDS);
            query.addScalar("cellarId", StringType.INSTANCE);
            query.addScalar("prodId", StringType.INSTANCE);
            query.setString(0, cellarId + "%");
            query.setString(1, cellarId + "%/%");
            return buildIdList(query.list());
        });
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Identifier> getTreeContentPid(final String cellarId) {
        final Session session = this.getHibernateTemplate().getSessionFactory().getCurrentSession();
        final SQLQuery query = session.createSQLQuery(REQ_GET_IDS_WITH_CS);
        query.addScalar("cellarId", StringType.INSTANCE);
        query.addScalar("prodId", StringType.INSTANCE);
        query.setString(0, cellarId + "%/%");

        return this.buildIdList(query.list());
    }

    /**
     * Build a <code>List</code> of <code>Identifier</code> from a SQL result list.
     *
     * @param sqlResults SQL result list.
     * @return <code>List</code> od <code>Identifier</code>.
     */
    private List<Identifier> buildIdList(final List<Object[]> sqlResults) {
        final Map<String, Identifier> resultMap = new HashMap<String, Identifier>();
        for (final Object[] result : sqlResults) {
            Identifier identifier = resultMap.get(result[0]);
            if (identifier == null) {
                identifier = new Identifier((String) result[0], new ArrayList<String>());
                resultMap.put((String) result[0], identifier);
            }
            identifier.getPids().add((String) result[1]);
        }
        return new ArrayList<Identifier>(resultMap.values());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer getMaxDocIndex(final String uuid) {
        final Integer result = this.getHibernateTemplate().execute(session -> {
            final String string = uuid + "%";
            final Query queryObject = buildNamedQuery(session, "getMaxDocIndex", string);
            final Integer uniqueResult = (Integer) queryObject.uniqueResult();
            return uniqueResult;
        });
        return result;
    }

}
