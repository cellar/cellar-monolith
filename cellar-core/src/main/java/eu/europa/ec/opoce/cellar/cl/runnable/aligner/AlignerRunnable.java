package eu.europa.ec.opoce.cellar.cl.runnable.aligner;

import eu.europa.ec.opoce.cellar.cl.delegator.AbstractDelegator;
import eu.europa.ec.opoce.cellar.cl.delegator.aligner.DelegateAligner;
import eu.europa.ec.opoce.cellar.logging.LogContext;

import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.HIERARCHY_ALIGNER_CLEANER;

/**
 * The Class AlignerRunnable.
 */
@LogContext
public class AlignerRunnable implements Runnable {

    /**
     * The delegate aligner.
     */
    private final DelegateAligner delegateAligner;

    /**
     * The delegator.
     */
    private final AbstractDelegator delegator;

    /**
     * Instantiates a new aligner runnable.
     *
     * @param delegator the delegator
     */
    public AlignerRunnable(final AbstractDelegator delegator, DelegateAligner delegateAligner) {
        this.delegator = delegator;
        this.delegateAligner = delegateAligner;
    }

    @Override
    @LogContext(HIERARCHY_ALIGNER_CLEANER)
    public void run() {
        delegateAligner.align(delegator);
    }

}
