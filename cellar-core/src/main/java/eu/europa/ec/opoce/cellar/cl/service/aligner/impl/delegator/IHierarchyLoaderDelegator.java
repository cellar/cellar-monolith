/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.aligner.impl.delegator
 *             FILE : IHierarchyLoaderDelegator.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : May 27, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.aligner.impl.delegator;

import eu.europa.ec.opoce.cellar.cl.domain.aligner.HierarchyNode;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;

import java.util.Collection;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : May 27, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IHierarchyLoaderDelegator<T> {

    Collection<T> loadResources(final String key);

    String getCellarId(final T obj);

    DigitalObjectType getDigitalObjectType(final T obj);

    void select(final HierarchyNode node, final T obj);

    Collection<T> getSelectedResources();
}
