/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : BatchJobPreProcessingRunable.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 16 juin 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.batchJob.manager;

import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.BatchJobService;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor.IBatchJobProcessor;
import eu.europa.ec.opoce.cellar.common.factory.IFactory;

public class BatchJobPreProcessingRunable extends BatchJobRunable {

    /**
     * Instantiates a new batch job  runable.
     *
     * @param batchJob the batch job
     * @param batchJobService the batch job service
     * @param batchJobPreProcessorFactory the batch job pre processor factory
     */
    public BatchJobPreProcessingRunable(final BatchJob batchJob, final BatchJobService batchJobService,
            final IFactory<IBatchJobProcessor, BatchJob> batchJobPreProcessorFactory) {
        super(batchJob, batchJobService, batchJobPreProcessorFactory);
    }

    /** {@inheritDoc} */
    @Override
    public void run() {
        try {
            final IBatchJobProcessor batchJobProcessor = this.batchJobProcessorFactory.create(batchJob);
            batchJobProcessor.execute();
        } catch (final Exception exception) {
            batchJobService.updateErrorBatchJob(batchJob, exception);
        }
    }
}
