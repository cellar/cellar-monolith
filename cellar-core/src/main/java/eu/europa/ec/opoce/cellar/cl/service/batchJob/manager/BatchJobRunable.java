/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : BatchJobRunable.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 16 juin 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.batchJob.manager;

import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.BatchJobService;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor.IBatchJobProcessor;
import eu.europa.ec.opoce.cellar.common.factory.IFactory;

import java.util.Date;
import java.util.Objects;

/**
 * The Class BatchJobRunable.
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 16 juin 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public abstract class BatchJobRunable implements Runnable, Comparable<BatchJobRunable> {

    /** The batch job . */
    protected BatchJob batchJob;
    /**
     * Batch job service.
     */
    protected BatchJobService batchJobService;

    /** The batch job processor factory. */
    protected IFactory<IBatchJobProcessor, BatchJob> batchJobProcessorFactory;

    /**
     * Gets the batch job .
     *
     * @return the batch job
     */
    public BatchJob getBatchJob() {
        return batchJob;
    }

    /**
     * Instantiates a new batch job  runable.
     *
     * @param batchJob the batch job
     * @param batchJobService the batch job service
     * @param batchJobProcessorFactory the batch job processor factory
     */
    public BatchJobRunable(final BatchJob batchJob, final BatchJobService batchJobService,
            final IFactory<IBatchJobProcessor, BatchJob> batchJobProcessorFactory) {
        this.batchJob = batchJob;
        this.batchJobProcessorFactory = batchJobProcessorFactory;
        this.batchJobService = batchJobService;
    }

    /** {@inheritDoc} */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof BatchJobRunable)) {
            return false;
        }
        final BatchJobRunable other = (BatchJobRunable) obj;
        final Long thisJobId = batchJob.getId();
        final Long otherJobId = other.getBatchJob().getId();
        return Objects.equals(thisJobId, otherJobId);
    }

    /** {@inheritDoc} */
    @Override
    public int hashCode() {
        return Objects.hash(batchJob.getId());
    }

    /** {@inheritDoc} */
    @Override
    public int compareTo(final BatchJobRunable other) {
        int result = 0;
        final Date thisOneLastModificationDate = getBatchJob().getLastModified();
        final Date otherLastModificationDate = other.getBatchJob().getLastModified();
        result = thisOneLastModificationDate.compareTo(otherLastModificationDate);
        if (result == 0) {
            final Date thisOneCreationDate = getBatchJob().getCreationDate();
            final Date otherCreationDate = other.getBatchJob().getCreationDate();
            result = thisOneCreationDate.compareTo(otherCreationDate);
        }
        if (result == 0) {
            result = getBatchJob().getId().compareTo(other.getBatchJob().getId());
        }
        return result;
    }
}
