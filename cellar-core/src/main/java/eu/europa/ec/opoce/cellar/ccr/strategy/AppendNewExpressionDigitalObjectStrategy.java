/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : CellarIdentifier.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 10 mars 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.strategy;

import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.ccr.helper.ResponseHelper;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.cl.domain.history.service.IdentifiersHistoryService;
import eu.europa.ec.opoce.cellar.cl.domain.response.UpdateStructMapOperation;
import eu.europa.ec.opoce.cellar.cl.service.client.CellarIdentifierService;
import eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectStrategy;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.domain.content.mets.OperationType;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.StructMapProcessorException;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import java.io.File;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *  It's the append strategy for expression.
 *  We create all missing digital object into S3 and database.
 *  This class contains a recursive method applyStrategy that allow to apply the same process for
 *  each digital object and their children.
 *
 *  The process consist to generate the next cellar identifier of a digital object.
 *  Store it in the database.
 *  After, we set in the data model StructMap the created cellar identifier.
 *  At the end, all missing digital object exist in S3.
 *  Datamodel and database are synchronized.
 */
public final class AppendNewExpressionDigitalObjectStrategy implements DigitalObjectStrategy {

    /** Logger instance. */
    private static final Logger LOGGER = LoggerFactory.getLogger(AppendNewExpressionDigitalObjectStrategy.class);

    private final PidManagerService pidManagerService;

    private final ContentStreamService contentStreamService;

    /** The identifiers history service. */
    private final IdentifiersHistoryService identifiersHistoryService;

    /** The cellar identifier service. */
    private final CellarIdentifierService cellarIdentifierService;

    private final String workUUID;
    private final SIPResource sipResource;
    private final List<DigitalObject> appendedDigitalObjectList;
    private final AtomicInteger nextExpressionCellarIdSuffix;
    private String generatedExpressionCellarUUID;
    private final File metsDirectory;
    private final UpdateStructMapOperation operation;

    public AppendNewExpressionDigitalObjectStrategy( //
            final String workUUID, //
            final SIPResource sipResource, //
            final File metsDirectory, //
            final UpdateStructMapOperation operation, //
            final List<DigitalObject> appendedDigitalObjectList, //
            final AtomicInteger nextExpressionCellarIdSuffix, //
            final PidManagerService pidManagerService, //
            ContentStreamService contentStreamService, final IdentifiersHistoryService identifiersHistoryService, //
            final CellarIdentifierService cellarIdentifierService) {
        this.workUUID = workUUID;
        this.sipResource = sipResource;
        this.appendedDigitalObjectList = appendedDigitalObjectList;
        this.nextExpressionCellarIdSuffix = nextExpressionCellarIdSuffix;
        this.metsDirectory = metsDirectory;
        this.operation = operation;
        this.pidManagerService = pidManagerService;
        this.contentStreamService = contentStreamService;
        this.identifiersHistoryService = identifiersHistoryService;
        this.cellarIdentifierService = cellarIdentifierService;
    }

    @Override
    public void applyStrategy(final DigitalObject digitalObject) {
        Assert.isTrue(DigitalObjectType.EXPRESSION.equals(digitalObject.getType()), "Strategy only compatible with Expression type.");

        if (cellarIdentifierService.isAnyParentReadOnly(digitalObject)) {
            final ExceptionBuilder<StructMapProcessorException> exceptionBuilder = ExceptionBuilder.get(StructMapProcessorException.class);
            exceptionBuilder.withCode(CoreErrors.E_391);
            exceptionBuilder.withMessage("Cannot append {} as a parent element or itself is marked as read only.");
            exceptionBuilder.withMessageArgs(digitalObject.getType().toString());
            throw exceptionBuilder.build();
        }

        this.generatedExpressionCellarUUID = identifiersHistoryService.generateCellarID(digitalObject, this.workUUID,
                this.nextExpressionCellarIdSuffix.getAndIncrement(), TYPE.AUTHENTICOJ);
        final String generatedCellarUUID = this.generatedExpressionCellarUUID;

        if (generatedCellarUUID == null) {
            return;
        }

        // Generate cellarId and link it with pid of this DigitalObject.
        LOGGER.debug("Append : Check PIDs : {} ", StringUtils.join(digitalObject.getContentids(), ", "));
        LOGGER.debug("Append : And generate for them : {}", generatedCellarUUID);

        final boolean ignoreDuplicatedPids = OperationType.CreateOrIgnore.equals(digitalObject.getStructMap().getMetsDocument().getType());
        this.pidManagerService.save(digitalObject, generatedCellarUUID, ignoreDuplicatedPids);

        final ContentIdentifier contentIdentifier = new ContentIdentifier(generatedCellarUUID);
        digitalObject.setCellarId(contentIdentifier);

        this.appendedDigitalObjectList.add(digitalObject);

        new WriteContentStreamStrategy(metsDirectory, contentStreamService).applyStrategy(digitalObject);

        operation.addOperation(ResponseHelper.fillCreateOperation(digitalObject));
    }

}
