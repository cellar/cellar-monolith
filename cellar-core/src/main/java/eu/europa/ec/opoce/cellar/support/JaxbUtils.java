/**
 * 
 */
package eu.europa.ec.opoce.cellar.support;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.bind.*;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.sax.SAXSource;
import java.io.StringReader;
import java.io.StringWriter;

/**
 * @author dsavares
 * 
 */
public class JaxbUtils {

    /**
     * Unmarshall an xml to a java object.
     * 
     * @param <T> The type of the object to fill with xml data.
     * @param xmlString the string with the xml data.
     * @param clazz the class of the object to fill.
     * @return the object filled.
     * @throws JAXBException if an error occurs during unmarshalling. 
     */
    @SuppressWarnings("unchecked")
    protected static <T> T unmarshall(String xmlString, Class<T> clazz) throws JAXBException {
        SAXParserFactory spf = SAXParserFactory.newInstance();
        try {
            spf.setFeature("http://xml.org/sax/features/external-general-entities", false);
            spf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
            spf.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);

            SAXSource xmlSource = new SAXSource(spf.newSAXParser().getXMLReader(),
                    new InputSource(new StringReader(xmlString)));
            JAXBContext jc = JAXBContext.newInstance(clazz);
            Unmarshaller unmarshaller = jc.createUnmarshaller();
            unmarshaller.setSchema(null);
            unmarshaller.setEventHandler(new javax.xml.bind.helpers.DefaultValidationEventHandler());
            return (T) unmarshaller.unmarshal(xmlSource);
        } catch (ParserConfigurationException
                  | SAXException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * <p>Marshall a java object to an xml.</p>
     * <p>The output is not formatted</p>
     * 
     * @param <T> The type of the object holding the data to fill the xml.
     * @param clazz the class of the object holding the data.
     * @param objectToMarshall the object holding the data
     * @return the string with the xml data
     * @throws JAXBException if an error occurs during marshalling. 
     */
    protected static <T> String marshallNotFormatted(T objectToMarshall, Class<T> clazz) throws JAXBException {
        return marshall(objectToMarshall, clazz, false);
    }

    /**
     * <p>Marshall a java object to an xml.</p>
     * <p>The output is formatted</p>
     * 
     * @param <T> The type of the object holding the data to fill the xml.
     * @param clazz the class of the object holding the data.
     * @param objectToMarshall the object holding the data
     * @return the string with the xml data
     * @throws JAXBException if an error occurs during marshalling. 
     */
    protected static <T> String marshall(T objectToMarshall, Class<T> clazz) throws JAXBException {
        return marshall(objectToMarshall, clazz, true);
    }

    private static <T> String marshall(T objectToMarshall, Class<T> clazz, boolean formattedOutput)
            throws JAXBException, PropertyException {
        JAXBContext jc = JAXBContext.newInstance(clazz);
        Marshaller marshaller = jc.createMarshaller();
        marshaller.setSchema(null);
        marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, formattedOutput);
        marshaller.setEventHandler(new javax.xml.bind.helpers.DefaultValidationEventHandler());

        StringWriter sw = new StringWriter();
        marshaller.marshal(objectToMarshall, sw);
        return sw.toString();
    }

}
