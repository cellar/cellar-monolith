package eu.europa.ec.opoce.cellar.cl.service.client;

import java.io.File;

/**
 * The response service is responsible for providing the generated responses given a mets id.
 * @author phvdveld
 *
 */
public interface ResponseService {

    /**
     * Gets the sip authenticOJ response corresponding to the given mets id.
     * @param metsId the mets id to retrieve the response sip 
     * @return a SIP file.
     */
    File getSIPAuthenticOJResponse(String metsId);

    /**
     * Gets the sip daily response corresponding to the given mets id.
     * @param metsId the mets id to retrieve the response sip 
     * @return a SIP file.
     */
    File getSIPDailyResponse(String metsId);

    /**
     * Gets the sip bulk response corresponding to the given mets id.
     * @param metsId the mets id to retrieve the response sip 
     * @return a SIP file.
     */
    File getSIPBulkResponse(String metsId);
   
    /**
     * Gets the sip low priority bulk response corresponding to the given mets id.
     * @param metsId the mets id to retrieve the response sip 
     * @return a SIP file.
     */
    File getSIPBulkLowPriorityResponse(String metsId);
}
