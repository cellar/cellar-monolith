package eu.europa.ec.opoce.cellar.ccr.utils;

import eu.europa.ec.opoce.cellar.common.util.StringUtils;
import eu.europa.ec.opoce.cellar.support.FileExtensionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Utility class for Mets Upload API.
 * <br><br>
 * This class cannot be instantiated and all its methods are static.
 *
 * @author EUROPEAN DYNAMICS S.A.
 */
public final class MetsUploadUtils {
    private static final Logger LOG = LogManager.getLogger(MetsUploadUtils.class);

    private MetsUploadUtils() {}

    /**
     * Resolves the METS ID from a ZIP file containing METS data.
     *
     * @param sipFile The ZIP file containing METS data.
     * @return The METS ID extracted from the ZIP file.
     * @throws IOException If an I/O error occurs while processing the ZIP file.
     */
    public static String resolveMetsId(final File sipFile) throws IOException {

        final ZipFile zipFile = new ZipFile(sipFile);
        try {
            return zipFile.stream()
                    .map(ZipEntry::getName)
                    .filter(name -> name.endsWith(FileExtensionUtils.METS))
                    .findFirst()
                    .map(name -> StringUtils.removeEnd(name, FileExtensionUtils.METS))
                    .orElseThrow(() -> new IOException("Mets file not found (" + FileExtensionUtils.METS + ")"));
        } finally {
            safeClose(zipFile);
        }
    }

    private static void safeClose(ZipFile zf) {
        if (zf != null) {
            try {
                zf.close();
            } catch (IOException e) {
                LOG.error(e.getMessage(), e);
            }
        }
    }

    /**
     * This class provides constants to be used throughout the METS Upload API.
     *
     * @author EUROPEAN DYNAMICS S.A.
     */
    public static final class MetsUploadConstants {
        public static final String METS_UPLOAD_API_EXCEPTION_LOG_MESSAGE = "Failed to upload mets package.";
        
        public static final String METS_UPLOAD_API_EXCEPTION_LOG_MESSAGE_WITH_REASON = "Failed to upload mets package. Reason: [{}]";

        private MetsUploadConstants() {
        }
    }
}
