/**
 * 
 */
package eu.europa.ec.opoce.cellar.support;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * @author dsavares
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SparqlVariable {

    @XmlAttribute
    private String name;

    @Override
    public String toString() {
        return "Variable [name=" + getName() + "]";
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
