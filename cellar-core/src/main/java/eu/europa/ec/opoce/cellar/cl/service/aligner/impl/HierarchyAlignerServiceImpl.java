/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.aligner.impl
 *             FILE : HierarchyAlignerServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Aug 22, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.aligner.impl;

import eu.europa.ec.opoce.cellar.cl.concurrency.Concurrent;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.enums.CellarServiceRDFStoreCleanerMode;
import eu.europa.ec.opoce.cellar.cl.dao.ProductionIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.dao.aligner.AlignerMisalignmentDao;
import eu.europa.ec.opoce.cellar.cl.delegator.aligner.DelegateAligner;
import eu.europa.ec.opoce.cellar.cl.domain.AbstractBatchManager.ManagerStatus;
import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.admin.ProductionIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.aligner.AlignerMisalignment;
import eu.europa.ec.opoce.cellar.cl.domain.aligner.Hierarchy;
import eu.europa.ec.opoce.cellar.cl.domain.aligner.HierarchyNode;
import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;
import eu.europa.ec.opoce.cellar.cl.enums.aligner.Repository;
import eu.europa.ec.opoce.cellar.cl.manager.HierarchyAlignerManager;
import eu.europa.ec.opoce.cellar.cl.rdfStoreCleaner.delegator.HierarchyDelegator;
import eu.europa.ec.opoce.cellar.cl.runnable.aligner.AlignerRunnable;
import eu.europa.ec.opoce.cellar.cl.service.aligner.IHierarchyAlignerService;
import eu.europa.ec.opoce.cellar.cmr.AlignerStatus;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.daobeans.InverseRelation;
import eu.europa.ec.opoce.cellar.cmr.database.dao.InverseRelationDao;
import eu.europa.ec.opoce.cellar.cmr.model.ModelUtils;
import eu.europa.ec.opoce.cellar.cmr.quad.ModelQuadService;
import eu.europa.ec.opoce.cellar.common.util.Triple;
import eu.europa.ec.opoce.cellar.database.transaction.RDFTransactional;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.domain.content.mets.OffIngestionOperationType;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Selector;
import org.apache.jena.rdf.model.SimpleSelector;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFFormat;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import static eu.europa.ec.opoce.cellar.cmr.ContentType.DIRECT_INFERRED;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Aug 22, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class HierarchyAlignerServiceImpl extends AbstractHierarchyLoaderServiceImpl implements IHierarchyAlignerService {

    private static final Logger LOG = LogManager.getLogger(HierarchyAlignerServiceImpl.class);

    /**
     * The triple filter patterns.
     */
    @Autowired
    @Qualifier("hierarchyAlignerFilterPatterns")
    private ArrayList<Triple<String, String, String>> tripleFilterPatterns;

    /**
     * The production identifier dao.
     */
    @Autowired
    private ProductionIdentifierDao productionIdentifierDao;

    @Autowired
    private ContentStreamService contentStreamService;

    /**
     * The model quad service.
     */
    @Autowired
    @Qualifier("modelQuadService")
    protected ModelQuadService modelQuadService;

    /**
     * The inverse relation dao.
     */
    @Autowired
    private InverseRelationDao inverseRelationDao;

    /**
     * The aligner executor.
     */
    @Autowired
    @Qualifier("alignerExecutor")
    private TaskExecutor alignerExecutor;

    /**
     * The delegate aligner.
     */
    @Autowired
    private DelegateAligner delegateAligner;

    /**
     * The Cellar configuration storing runtime parameters.
     */
    @Autowired
    @Qualifier("cellarConfiguration")
    protected ICellarConfiguration cellarConfiguration;

    /**
     * The hierarchy aligner manager.
     */
    @Autowired
    private HierarchyAlignerManager hierarchyAlignerManager;

    /**
     * The aligner misalignment dao.
     */
    @Autowired
    private AlignerMisalignmentDao alignerMisalignmentDao;

    /**
     * Align hierarchy.
     *
     * @param productionId the production id
     * @see eu.europa.ec.opoce.cellar.cl.service.aligner.IHierarchyAlignerService#alignHierarchy(java.lang.String)
     */
    @Override
    @Concurrent(locker = OffIngestionOperationType.ALIGNING)
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @RDFTransactional
    public void alignHierarchy(final String productionId) {
        final String rootCellarId = this.getRootCellarId(productionId);
        final Hierarchy hierarchy = this.loadHierarchy(rootCellarId);

        final Map<String, HierarchyNode> nodes = hierarchy.getNodes();
        final Date nowDate = new Date();

        for (final Entry<String, HierarchyNode> entry : nodes.entrySet()) {
            final HierarchyNode hierarchyNode = entry.getValue();
            final ContentIdentifier contentIdentifier = hierarchyNode.getCellarId();
            final String cellarId = contentIdentifier.getIdentifier();
            final List<Repository> missingRepos = hierarchyNode.getMissingRepositories();
            for (final Repository repository : missingRepos) {
                final AlignerMisalignment alignerMisalignment = new AlignerMisalignment();
                alignerMisalignment.setCellarId(cellarId);
                alignerMisalignment.setOperationDate(nowDate);
                alignerMisalignment.setOperationType(CellarServiceRDFStoreCleanerMode.fix);
                alignerMisalignment.setRepository(repository);
                this.alignerMisalignmentDao.save(alignerMisalignment);
            }
        }

        this.fixHierarchy(hierarchy);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void fixHierarchy(final Hierarchy hierarchy) {
        final Map<Repository, List<HierarchyNode>> evictedHierarchyNodes = hierarchy.getEvictedHierarchyNodes();

        final Set<String> aggregatedCellarIds = new HashSet<>();
        final Set<Identifier> cmrCellarIdentifiers = new HashSet<>();

        CellarResource cellarResource;
        CellarIdentifier cellarIdentifier;
        List<ProductionIdentifier> productionIdentifiers;
        for (final Map.Entry<Repository, List<HierarchyNode>> e : evictedHierarchyNodes.entrySet()) {

            for (final HierarchyNode r : e.getValue()) {

                final String currCellarId = r.getCellarId().getIdentifier();
                aggregatedCellarIds.add(currCellarId);
                switch (e.getKey()) {
                    case CMR_CELLAR_RESOURCE_MD:
                        cellarResource = this.cellarResourceDao.findCellarId(currCellarId);
                        if (cellarResource != null) {
                            this.cellarResourceDao.deleteObject(cellarResource);
                        }
                        break;
                    case CELLAR_IDENTIFIER:
                        cellarIdentifier = this.cellarIdentifierDao.getCellarIdentifier(currCellarId);
                        productionIdentifiers = this.productionIdentifierDao.getProductionIdentifiers(cellarIdentifier);

                        r.setProductionIdentifiers(productionIdentifiers);
                        for (final ProductionIdentifier pi : productionIdentifiers) {
                            this.productionIdentifierDao.deleteProductionIdentifier(pi);
                        }

                        if (cellarIdentifier != null) {
                            this.cellarIdentifierDao.deleteCellarIdentifier(cellarIdentifier);
                        }
                        break;
                    case S3:
                        contentStreamService.deleteRecursively(currCellarId);
                        break;
                    case RDF_STORE:
                    case CMR_INVERSE_DISS:
                        final Identifier currIdentifier = new Identifier(currCellarId);
                        final Collection<ProductionIdentifier> currPids = productionIdentifierDao.getProductionIdentifiersByCellarId(currCellarId);
                        if (currPids != null) {
                            currIdentifier.setPids(currPids.stream()
                                    .map(ProductionIdentifier::getProductionId)
                                    .collect(Collectors.toList())
                            );
                        }
                        cmrCellarIdentifiers.add(currIdentifier);

                        break;
                }
            }
        }

        if (!cmrCellarIdentifiers.isEmpty()) {
            this.modelQuadService.deleteContexts(cmrCellarIdentifiers);
        }

        if (!aggregatedCellarIds.isEmpty()) {
            final Collection<InverseRelation> inverseRelations = this.inverseRelationDao.findRelationsByTargets(aggregatedCellarIds);
            for (final InverseRelation inverseRelation : inverseRelations) {
                this.inverseRelationDao.deleteObject(inverseRelation);
            }

            this.updateParents(hierarchy, aggregatedCellarIds);
        }

        LOG.info("The following entries have been removed: {}.", evictedHierarchyNodes);
    }

    /**
     * Update parents.
     *
     * @param hierarchy           the hierarchy
     * @param aggregatedCellarIds the aggregated cellar ids
     */
    private void updateParents(final Hierarchy hierarchy, final Set<String> aggregatedCellarIds) {

        Model directInferred;
        HierarchyNode node, parent;
        for (final String cellarId : aggregatedCellarIds) {
            node = hierarchy.getHierarchyNodeOrNull(cellarId);
            parent = node.getParent();

            if (node.getType() != DigitalObjectType.ITEM && parent != null && !aggregatedCellarIds.contains(parent.getCellarId().getIdentifier())) {
                String parentId = parent.getCellarId().getIdentifier();
                String parentVersion = parent.getVersions().get(DIRECT_INFERRED);
                String model = contentStreamService.getContentAsString(parentId, parentVersion, DIRECT_INFERRED)
                        .orElseThrow(() -> new IllegalArgumentException("DIRECT_INFERRED model not found for " + parentId + " (" + parentVersion + ")"));
                directInferred = JenaUtils.read(model, Lang.NT);
                filter(directInferred, node);
                String newVersion = contentStreamService.writeContent(parentId, new ByteArrayResource(JenaUtils.bytes(directInferred, RDFFormat.NT)), DIRECT_INFERRED);
                parent.getVersions().put(DIRECT_INFERRED, newVersion);
            }
        }
    }

    /**
     * Filter.
     *
     * @param model         the model
     * @param hierarchyNode the hierarchy node
     */
    private void filter(final Model model, final HierarchyNode hierarchyNode) {
        final Selector selector = new SimpleSelector() {

            @Override
            public boolean selects(final Statement s) {
                return isFiltered(s, hierarchyNode);
            }
        };

        final long startSize = model.size();

        model.remove(model.listStatements(selector));

        LOG.info("{} triple(s) has/have been filtered.", String.valueOf(startSize - model.size()));
    }

    /**
     * Checks if is filtered.
     *
     * @param s             the s
     * @param hierarchyNode the hierarchy node
     * @return true, if is filtered
     */
    private boolean isFiltered(final Statement s, final HierarchyNode hierarchyNode) {
        boolean filter;
        for (final Triple<String, String, String> pattern : this.tripleFilterPatterns) {
            filter = false;

            if (StringUtils.equalsIgnoreCase(pattern.getOne(), "${uri}")) {
                if (StringUtils.isNotBlank(hierarchyNode.getCellarId().getIdentifier())) {
                    filter = ModelUtils.match(s.getSubject(), this.identifierService.getUri(hierarchyNode.getCellarId().getIdentifier()));
                }

                if (!filter) {
                    final Collection<ProductionIdentifier> productionIdentifiers = hierarchyNode.getProductionIdentifiers();
                    if (productionIdentifiers != null) {
                        for (final ProductionIdentifier pid : productionIdentifiers) {
                            filter = ModelUtils.match(s.getSubject(), this.identifierService.getUri(pid.getProductionId()));
                            if (filter) {
                                break;
                            }
                        }
                    }
                }
            } else {
                filter = ModelUtils.match(s.getSubject(), pattern.getOne());
            }

            filter = filter && ModelUtils.match(s.getPredicate(), pattern.getTwo());

            filter = filter && ModelUtils.match(s.getObject(), pattern.getThree());

            if (filter) {
                return true;
            }
        }

        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean alignUnknown() {

        final int batchSize = this.cellarConfiguration.getCellarServiceAlignerHierarchiesBatchSize();
        final Collection<CellarResource> cellarResources = this.cellarResourceDao.findRootsPerAlignerStatus(AlignerStatus.UNKNOWN,
                batchSize);

        HierarchyDelegator delegator;
        for (final CellarResource cellarResource : cellarResources) {
            delegator = new HierarchyDelegator(cellarResource);

            this.delegateAligner.startProcessing(delegator);

            this.alignerExecutor.execute(new AlignerRunnable(delegator, delegateAligner));
        }

        return cellarResources.size() >= batchSize;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void startAligning() {
        this.hierarchyAlignerManager.startWorking();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void stopAligning() {
        this.hierarchyAlignerManager.stopWorking();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ManagerStatus getManagerStatus() {
        return this.hierarchyAlignerManager.getManagerStatus();
    }

}
