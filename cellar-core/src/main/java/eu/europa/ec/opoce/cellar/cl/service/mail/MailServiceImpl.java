/* ----------------------------------------------------------------------
 *          PROJECT : EUR-Lex
 *
 *          PACKAGE : eu.europa.ec.op.eurlex.frontoffice.service.messaging
 *             FILE : MailService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 20 Jan 2011
 *
 *      MODIFIED BY: ARHS Developments
 *               ON: $LastChangedDate: 2010-09-02 09:35:22 +0200 (Thu, 02 Sep 2010) 2011-09-27 11:59:43 +0200 (jeu., 27 sept. 2011) $
 *          VERSION: $LastChangedRevision: 293 $
 *
 * ----------------------------------------------------------------------
 * Copyright (c) 2011 European Commission - Publications Office
 * ----------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.mail;

import eu.europa.ec.opoce.cellar.cl.dao.email.EmailDao;
import eu.europa.ec.opoce.cellar.cl.domain.email.Email;
import eu.europa.ec.opoce.cellar.cl.domain.email.EmailStatus;
import eu.europa.ec.opoce.cellar.support.mail.EmailTemplate;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import java.io.IOException;
import java.io.StringWriter;
import java.util.Date;
import java.util.Locale;
import java.util.Map;


@Service
public class MailServiceImpl implements MailService {

    private static final Logger LOGGER = LogManager.getLogger(MailServiceImpl.class);
    private final FreeMarkerConfigurer freemarkerConfig;
    private final EmailDao emailDao;

    public MailServiceImpl(FreeMarkerConfigurer freemarkerConfig,
                           EmailDao emailDao) {
        this.freemarkerConfig = freemarkerConfig;
        this.emailDao = emailDao;
    }

    @Override
    @Transactional(readOnly = false, rollbackFor = Throwable.class)
    public void createEmail(String to,
                            Locale locale,
                            EmailTemplate template,
                            Map<String, Object> model) {
        Email email = new Email();
        email.setAddress(to);
        email.setStatus(EmailStatus.NEW);
        String body = processTemplate(template.getBodyTemplate(), locale, model);
        email.setContent(body);
        String subject = processTemplate(template.getTitleTemplate(), locale, model);
        email.setSubject(subject);
        email.setCreatedOn(new Date());
        this.emailDao.saveObject(email);
    }

    private String processTemplate(String template,
                                  Locale locale,
                                  Map<String, Object> model) {
        try {
            final StringWriter textWriter = new StringWriter();
            Template textTemplate = this.freemarkerConfig.getConfiguration().getTemplate(template, locale, "UTF-8");
            textTemplate.process(model, textWriter);
            return textWriter.toString();
        } catch (TemplateException e) {
            LOGGER.error("Could not process template " + template + ". Please verify the template body syntax.", e);
        } catch (IOException e) {
            LOGGER.error("Could not read template " + template + ". Please make sure that the template exists.", e);
        }
        return "??? " + template + " - " + locale + " ???";
    }

}
