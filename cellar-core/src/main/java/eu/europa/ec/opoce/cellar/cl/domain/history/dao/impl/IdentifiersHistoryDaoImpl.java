/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : CellarIdentifier.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 8 mars 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.history.dao.impl;

import eu.europa.ec.opoce.cellar.cl.dao.impl.BaseDaoImpl;
import eu.europa.ec.opoce.cellar.cl.domain.history.IdentifiersHistory;
import eu.europa.ec.opoce.cellar.cl.domain.history.dao.IdentifiersHistoryDao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

/**
 * The Class IdentifiersHistoryDaoImpl.
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 8 mars 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Repository
public class IdentifiersHistoryDaoImpl extends BaseDaoImpl<IdentifiersHistory, Long> implements IdentifiersHistoryDao {

    /** {@inheritDoc} */
    @Override
    public IdentifiersHistory getLatest(final Collection<String> productionIds) {
        final HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("productionIds", productionIds);
        final List<IdentifiersHistory> resultList = findObjectsByNamedQueryWithNamedParamsLimit("getDeleted", params, 0, 1);
        final IdentifiersHistory result = resultList.isEmpty() ? null : resultList.get(0);
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public IdentifiersHistory getLatest(final String productionId) {
        final HashMap<String, Object> params = new HashMap<String, Object>();
        final ArrayList<String> productionIds = new ArrayList<String>();
        productionIds.add(productionId);
        params.put("productionIds", productionIds);
        final List<IdentifiersHistory> resultList = findObjectsByNamedQueryWithNamedParamsLimit("getDeleted", params, 0, 1);
        final IdentifiersHistory result = resultList.isEmpty() ? null : resultList.get(0);
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<IdentifiersHistory> getDeleted(final Collection<String> productionIds) {
        final HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("productionIds", new ArrayList<String>(productionIds));
        final List<IdentifiersHistory> result = findObjectsByNamedQueryWithNamedParams("getDeleted", params);
        return result;

    }

    /** {@inheritDoc} */
    @Override
    public void delete(final Long id) {
        final IdentifiersHistory object = getObject(id);
        deleteObject(object);
    }

    /** {@inheritDoc} */
    @Override
    public IdentifiersHistory update(final IdentifiersHistory identifiersHistory) {
        final IdentifiersHistory result = updateObject(identifiersHistory);
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<IdentifiersHistory> getDeleted(final String productionId) {
        final HashMap<String, Object> params = new HashMap<String, Object>();
        final ArrayList<String> productionIds = new ArrayList<String>();
        productionIds.add(productionId);
        params.put("productionIds", productionIds);
        final List<IdentifiersHistory> result = findObjectsByNamedQueryWithNamedParams("getDeleted", params);
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<IdentifiersHistory> getIdentifierHistoryTree(final String productionId, final String cellarId) {
        final HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("productionId", productionId + '%');
        params.put("cellarId", cellarId + '%');
        final List<IdentifiersHistory> result = findObjectsByNamedQueryWithNamedParams("getIdentifierHistoryTree", params);
        return result;

    }

    /** {@inheritDoc} */
    @Override
    public List<IdentifiersHistory> getAllRecordsStartingWithCellarId(final String cellarId) {
        final HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("cellarId", cellarId + '%');
        final List<IdentifiersHistory> result = findObjectsByNamedQueryWithNamedParams("getIdentifierHistoryStartingWithCellarId", params);
        return result;
    }

}
