package eu.europa.ec.opoce.cellar.support;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ServiceLoader;

public class DynamicClassLoader {

    public static final <T> List<T> loadServiceClasses(Class<T> clazz) {
        List<T> serviceClassList = new LinkedList<T>();

        ServiceLoader<T> serviceLoader = ServiceLoader.load(clazz);
        Iterator<T> serviceLoaderIter = serviceLoader.iterator();
        while (serviceLoaderIter.hasNext()) {
            T t = serviceLoaderIter.next();

            serviceClassList.add(t);
        }

        return serviceClassList;
    }

}
