/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.dao.impl
 *             FILE : BatchJobDaoImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 11, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao.impl;

import eu.europa.ec.opoce.cellar.cl.dao.BatchJobDao;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob.BATCH_JOB_TYPE;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob.STATUS;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

/**
 * <class_description> Batch job dao.
 *
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 11, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Repository
public class BatchJobDaoImpl extends BaseDaoImpl<BatchJob, Long> implements BatchJobDao {

    /** {@inheritDoc} */
    @Override
    public List<BatchJob> findBatchJobsByStatus(final STATUS status) {
        final Integer cellarServiceBatchJobThreadPoolSelectionSize = Integer
                .parseInt(cellarConfiguration.getCellarServiceBatchJobPoolSelectionSize());
        final List<BatchJob> batchJobs = super.findPaginatedObjectsByNamedQuery("findBatchJobsByStatus", 0,
                cellarServiceBatchJobThreadPoolSelectionSize, status);
        return batchJobs;
    }

    /** {@inheritDoc} */
    @Override
    public List<BatchJob> findBatchJobsWithAnyStatus(final STATUS... statuses) {
        return this.getHibernateTemplate().execute(session -> {
            final List<STATUS> listParameter = Arrays.asList(statuses);
            final Query queryObject = buildNamedQuery(session, "findBatchJobsByInStatus");
            queryObject.setParameterList("status", listParameter);
            return queryObject.list();
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<BatchJob> updateFirstBatchJobs(final STATUS oldStatus, final STATUS newStatus) {
        final Integer cellarServiceBatchJobThreadPoolSelectionSize = Integer
                .parseInt(cellarConfiguration.getCellarServiceBatchJobPoolSelectionSize());
        final List<BatchJob> batchJobs = super.findPaginatedObjectsByNamedQuery("findBatchJobsByStatus", 0,
                cellarServiceBatchJobThreadPoolSelectionSize, oldStatus);

        if (batchJobs.isEmpty()) {
            return null;
        }

        for (final BatchJob batchJob : batchJobs) {
            batchJob.setJobStatus(newStatus);
            this.updateObject(batchJob);
        }

        return batchJobs;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<BatchJob> updateFirstUpdateBatchJobsByStatusCron(final STATUS oldStatus, final STATUS newStatus, final String cron) {
        final Integer cellarServiceBatchJobThreadPoolSelectionSize = Integer
                .parseInt(cellarConfiguration.getCellarServiceBatchJobPoolSelectionSize());
        final List<BatchJob> batchJobs = super.findPaginatedObjectsByNamedQuery("findUpdateBatchJobByStatusCron", 0,
                cellarServiceBatchJobThreadPoolSelectionSize, oldStatus, cron);
        if (batchJobs.isEmpty()) {
            return null;
        }

        for (final BatchJob batchJob : batchJobs) {
            batchJob.setJobStatus(newStatus);
            this.updateObject(batchJob);
        }

        return batchJobs;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<BatchJob> findBatchJobsByStatusCron(final STATUS status, final String cron) {
        final Integer cellarServiceBatchJobThreadPoolSelectionSize = Integer
                .parseInt(cellarConfiguration.getCellarServiceBatchJobPoolSelectionSize());
        final List<BatchJob> batchJobs = super.findPaginatedObjectsByNamedQuery("findUpdateBatchJobByStatusCron", 0,
                cellarServiceBatchJobThreadPoolSelectionSize, status, cron);
        return batchJobs;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BatchJob updateObject(final BatchJob batchJob) {
        batchJob.modifiedNow();
        return super.updateObject(batchJob);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<BatchJob> findBatchJobsByType(final BATCH_JOB_TYPE type) {
        return super.findObjectsByNamedQuery("findBatchJobsByType", type);
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<String> findUpdateBatchJobCrons() {
        final List<String> findByNamedQuery = (List<String>) super.findByNamedQuery("findUpdateBatchJobCrons");
        return findByNamedQuery;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BatchJob findBatchJobByIdType(final Long id, final BATCH_JOB_TYPE jobType) {
        return super.findObjectByNamedQuery("findBatchJobByIdType", id, jobType);
    }

}
