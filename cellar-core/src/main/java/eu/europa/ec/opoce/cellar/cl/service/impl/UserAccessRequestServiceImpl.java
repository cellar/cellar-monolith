/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *        FILE : UserAccessRequestServiceImpl.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 10-03-2023
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2023 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

import eu.europa.ec.opoce.cellar.cl.dao.UserAccessRequestDao;
import eu.europa.ec.opoce.cellar.cl.domain.admin.User;
import eu.europa.ec.opoce.cellar.cl.domain.admin.UserAccessRequest;
import eu.europa.ec.opoce.cellar.cl.service.client.SecurityService;
import eu.europa.ec.opoce.cellar.cl.service.client.UserAccessRequestService;
import eu.europa.ec.opoce.cellar.cl.service.mail.MailService;
import eu.europa.ec.opoce.cellar.event.UserAccessRequestAcceptanceEvent;
import eu.europa.ec.opoce.cellar.support.mail.EmailTemplate;

/**
 * Provides access to the user access request functionalities.
 * @author EUROPEAN DYNAMICS S.A
 */
@Service
public class UserAccessRequestServiceImpl implements UserAccessRequestService {

	/**
	 * 
	 */
	private static final Logger LOG = LogManager.getLogger(UserAccessRequestServiceImpl.class);
	/**
	 * 
	 */
	private static final String ROLE_SECURITY = "ROLE_SECURITY";
	/**
	 * 
	 */
	private static final String ADMIN_NOTIFICATION_EMAIL_SUBJECT_TEMPLATE = "user-access-request/uar_subject.ftl";
	/**
	 * 
	 */
	private static final String ADMIN_NOTIFICATION_EMAIL_BODY_TEMPLATE = "user-access-request/uar_body.ftl";
	/**
	 * 
	 */
	private static final String USER_NOTIFICATION_EMAIL_SUBJECT_TEMPLATE = "user-access-request-acceptance/uar_acceptance_subject.ftl";
	/**
	 * 
	 */
	private static final String USER_NOTIFICATION_EMAIL_BODY_TEMPLATE = "user-access-request-acceptance/uar_acceptance_body.ftl";
	/**
	 * 
	 */
	private UserAccessRequestDao userAccessRequestDao;
	/**
	 * 
	 */
	private SecurityService securityService;
	/**
	 * 
	 */
	private MailService mailService;
	/**
	 * 
	 */
	private ApplicationEventPublisher applicationEventPublisher;
	
	@Autowired
    public UserAccessRequestServiceImpl(UserAccessRequestDao userAccessRequestDao, SecurityService securityService, MailService mailService,
    		ApplicationEventPublisher applicationEventPublisher) {
		this.userAccessRequestDao = userAccessRequestDao;
		this.securityService = securityService;
		this.mailService = mailService;
		this.applicationEventPublisher = applicationEventPublisher;
	}

	
	/**
     * {@inheritDoc}
     */
	@Override
	public List<UserAccessRequest> findAllUserAccessRequests() {
		return this.userAccessRequestDao.findAll();
	}

	/**
     * {@inheritDoc}
     */
	@Override
	public UserAccessRequest findUserAccessRequest(Long id) {
		return this.userAccessRequestDao.getObject(id);
	}
	
	@Override
	public UserAccessRequest findByUsername(String username) {
		return this.userAccessRequestDao.findByUsername(username);
	}

	/**
     * {@inheritDoc}
     */
	@Override
	@Transactional
	public void deleteUserAccessRequest(UserAccessRequest uar) {
        if (uar != null) {
            this.userAccessRequestDao.deleteObject(uar);
            LOG.info("User access request for principal [{}] has been deleted.", uar.getUsername());
        }
	}
	
	@Override
	public void notifyAdminsViaEmail(Map<String, Object> model) {
		List<User> admins = this.securityService.findUsersHavingRole(ROLE_SECURITY);
		if (admins == null || admins.isEmpty()) {
			LOG.warn("An administrator email could not be found.");
			return;
		}
		EmailTemplate emailTemplate = new EmailTemplate();
		emailTemplate.setTitleTemplate(ADMIN_NOTIFICATION_EMAIL_SUBJECT_TEMPLATE);
		emailTemplate.setBodyTemplate(ADMIN_NOTIFICATION_EMAIL_BODY_TEMPLATE);
		for (User admin : admins) {
			String adminEmail = admin.getEmail();
			if (adminEmail != null && !adminEmail.isEmpty()) {
				this.mailService.createEmail(adminEmail, Locale.ENGLISH, emailTemplate, model);
				LOG.info("User access request notification email has been scheduled.");
			}
		}
	}
	
	@Override
	@Transactional
	public void createUserAccessRequest(String euLoginId, String email) {
		UserAccessRequest uar = new UserAccessRequest();
		uar.setUsername(euLoginId);
		uar.setEmail(email);
		this.userAccessRequestDao.saveObject(uar);
		LOG.info("An access request has been submitted for principal [{}].", euLoginId);
	}
	
	@Override
	@Transactional
	public void acceptUserAccessRequest(User user, long userAccessRequestId) {
		UserAccessRequest uarToDelete = findUserAccessRequest(userAccessRequestId);
		deleteUserAccessRequest(uarToDelete);
		this.securityService.createUser(user);
		this.applicationEventPublisher.publishEvent(new UserAccessRequestAcceptanceEvent(uarToDelete));
		
	}
	
	@TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
	void notifyUserViaEmail(UserAccessRequestAcceptanceEvent uarEvent) {
		EmailTemplate emailTemplate = new EmailTemplate();
		emailTemplate.setTitleTemplate(USER_NOTIFICATION_EMAIL_SUBJECT_TEMPLATE);
		emailTemplate.setBodyTemplate(USER_NOTIFICATION_EMAIL_BODY_TEMPLATE);
		this.mailService.createEmail(uarEvent.getUserAccessRequest().getEmail(), Locale.ENGLISH, emailTemplate, null);
		LOG.info("Access request for principal [{}] has been accepted.", uarEvent.getUserAccessRequest().getUsername());
	}
	
}
