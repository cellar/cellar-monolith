package eu.europa.ec.opoce.cellar.cl.domain;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * The Class AbstractBatchManager.
 *
 * @param <T> the generic type
 */
public abstract class AbstractBatchManager<T extends Thread> {

    /**
     * The Cellar configuration storing runtime parameters.
     */
    @Autowired
    @Qualifier("cellarConfiguration")
    protected ICellarConfiguration cellarConfiguration;

    /** The thread. */
    private T thread;

    /** The is enabled. */
    private boolean isEnabled;

    /**
     * The Enum ManagerStatus.
     */
    public enum ManagerStatus {

        /** The working. */
        WORKING, /** The not working. */
        NOT_WORKING, /** The stopping. */
        STOPPING;
    }

    /**
     * Gets the thread instance.
     *
     * @return the thread instance
     */
    protected abstract T getThreadInstance();

    /**
     * Start working.
     */
    public synchronized void startWorking() {
        if ((this.thread == null) || (!this.thread.isAlive() && !this.thread.isInterrupted())) {
            this.isEnabled = true;
            this.thread = this.getThreadInstance();
            this.thread.start();
        }
    }

    /**
     * Stop working.
     */
    public synchronized void stopWorking() {
        this.isEnabled = false;
    }

    /**
     * Checks if is enabled.
     *
     * @return true, if is enabled
     */
    public synchronized boolean isEnabled() {
        return this.isEnabled;
    }

    /**
     * Gets the manager status.
     *
     * @return the manager status
     */
    public synchronized ManagerStatus getManagerStatus() {
        if ((this.thread != null) && this.thread.isAlive() && !this.thread.isInterrupted()) {
            if (this.isEnabled) {
                return ManagerStatus.WORKING;
            } else {
                return ManagerStatus.STOPPING;
            }
        }

        return ManagerStatus.NOT_WORKING;
    }
}
