/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState
 *             FILE : ExecutionStateManager.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Nov 25, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState;

import java.util.Map;

import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution.EXECUTION_STATUS;

/**
 * <class_description> The logics manager.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Nov 25, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class ExecutionStateManager {

    /**
     * The task states.
     */
    private Map<EXECUTION_STATUS, AbstractTask> taskStates;

    /**
     * The batch states.
     */
    private Map<EXECUTION_STATUS, AbstractBatch> batchStates;

    /**
     * Gets the task states.
     * @return the task states
     */
    public Map<EXECUTION_STATUS, AbstractTask> getTaskStates() {
        return this.taskStates;
    }

    /**
     * Sets the task states.
     * @param taskStates the task states to set
     */
    public void setTaskStates(final Map<EXECUTION_STATUS, AbstractTask> taskStates) {
        this.taskStates = taskStates;
    }

    /**
     * Gets the batch states.
     * @return the batch states
     */
    public Map<EXECUTION_STATUS, AbstractBatch> getBatchStates() {
        return this.batchStates;
    }

    /**
     * Sets the batch states.
     * @param batchStates the batch states to set
     */
    public void setBatchStates(final Map<EXECUTION_STATUS, AbstractBatch> batchStates) {
        this.batchStates = batchStates;
    }

    /**
     * Gets the task state corresponding to the identified status.
     * @param executionStatus the task status
     * @return the task state corresponding to the identified status
     */
    public AbstractTask getTaskState(final EXECUTION_STATUS executionStatus) {
        return this.taskStates.get(executionStatus);
    }

    /**
     * Gets the batch state corresponding to the identified status.
     * @param executionStatus the batch status
     * @return the batch state corresponding to the identified status
     */
    public AbstractBatch getBatchState(final EXECUTION_STATUS executionStatus) {
        return this.batchStates.get(executionStatus);
    }

    /**
     * Gets the cleaning state corresponding to the identified status.
     * @param executionStatus the cleaning status
     * @return the cleaning state corresponding to the identified status
     */
    public AbstractCleaning getCleaningState(final EXECUTION_STATUS executionStatus) {
        final AbstractTask task = this.taskStates.get(executionStatus);
        return (task instanceof AbstractCleaning ? (AbstractCleaning) task : null);
    }
}
