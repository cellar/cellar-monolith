package eu.europa.ec.opoce.cellar.cl.service.client;

/**
 * The Mets processor is a service of the CELLAR system that is responsible for treat a mets.
 *  
 * @author dcraeye
 *
 */
public interface MetsProcessor extends Runnable {

    void run();

}
