/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.dao.impl
 *             FILE : ScheduledReindexationDaoImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 12 sept. 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao.impl;

import eu.europa.ec.opoce.cellar.cl.dao.ScheduledReindexationDao;
import eu.europa.ec.opoce.cellar.cl.domain.index.ScheduledReindexation;

import org.springframework.stereotype.Repository;

/**
 * The Class ScheduledReindexationDaoImpl.
 * <class_description> The dao implementation class for the SCHEDULED_REINDEX
 *
 * ON : 12 sept. 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Repository
public class ScheduledReindexationDaoImpl extends BaseDaoImpl<ScheduledReindexation, Long> implements ScheduledReindexationDao {
}
