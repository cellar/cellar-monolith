package eu.europa.ec.opoce.cellar.cl.service.client;

import eu.europa.ec.opoce.cellar.domain.content.mets.OperationType;
import eu.europa.ec.opoce.cellar.exception.MetsAnalyzerException;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 * Mets analyzer.
 * 
 * @author dcraeye
 */
public interface MetsAnalyzer {

    /**
     * Get the METS ID from a METS file.
     * 
     * @param metsFileName
     *            the filename of the METS.
     * @return the mets identifier or null if we can't found it in this METS file.
     */
    String getMetsIdFromTemporaryWorkFolder(String metsFileName);

    /**
     * Get the METS ID from a SIP file.
     * 
     * @param metsFile
     *            the METS file.
     * @return the mets identifier or null if we can't found it in this METS file.
     */
    String getMetsIdFromTemporaryWorkFolder(File metsFile);

    /**
     * Get the METS TYPE from a METS file.
     * 
     * @param metsFileName
     *            the filename of the METS.
     * @return the mets type or null if we can't found it in this METS file.
     */
    OperationType getMetsTypeFromTemporaryWorkFolder(String metsFileName);

    /**
     * Get the METS ID from a SIP file.
     * 
     * @param metsFile
     *            the METS file.
     * @return the mets type (CREATE or UPDATE)
     * @throws MetsAnalyzerException
     *             if the type is not create or update or not found.
     */
    OperationType getMetsTypeFromTemporaryWorkFolder(File metsFile);

    /**
     * Get the METS File List from a METS file.
     * 
     * @param metsFileName
     *            the filename of the METS.
     * @return the mets file list or null if we can't found it in this METS file.
     */
    List<String> getMetsLinkedFileListFromTemporaryWorkFolder(String metsFileName);

    /**
     * Get the METS File List from a SIP file.
     * 
     * @param metsFile
     *            the METS file.
     * @return the mets file list or null if we can't found it in this METS file.
     */
    List<String> getMetsLinkedFileListFromTemporaryWorkFolder(File metsFile);

    /**
     * Get the METS FPTR FILEID List from a METS file.
     * @param metsFileName
     *            the filename of the METS.
     * @return the mets FPTR FILEID list or null if we can't found it in this METS file.
     */
    List<String> getMetsFptrFileIDListFromTemporaryWorkFolder(String metsFileName);

    /**
     * Get the METS FPTR FILEID List from a METS file.
     * @param metsFile
     *            the METS file.
     * @return the mets FPTR FILEID list or null if we can't found it in this METS file.
     */
    List<String> getMetsFptrFileIDListFromTemporaryWorkFolder(File metsFile);

    /**
     * Get the METS FILE ID List from a METS file.
     * @param metsFileName
     *            the filename of the METS.
     * @return the mets FILE ID list or null if we can't found it in this METS file.
     */
    List<String> getMetsFileIDListFromTemporaryWorkFolder(String metsFileName);

    /**
     * Get the METS FILE ID List from a METS file.
     * @param metsFile
     *            the METS file.
     * @return the mets FILE ID list or null if we can't found it in this METS file.
     */
    List<String> getMetsFileIDListFromTemporaryWorkFolder(File metsFile);

    int countMetsStructMap(File metsFile) throws Exception;

    int countMetsDigitalObject(File metsFile) throws Exception;

    /**
     * Get the <metsHdr> element and content from METS file.
     * 
     * @param metsFile
     *            METS file
     * @throws FileNotFoundException
     *             Raised if METS file cannot be accessed.
     * @throws IOException
     *             Raised if METS file cannot be read.
     * @return The XML snippet delimited by <metsHdr> and </metsHdr> tags included.
     */
    String getMetsHdr(File metsFile) throws FileNotFoundException, IOException;

    /**
     * Read the metsfile and extract href references.
     * Used to retrieve the file names that should be contained in the SIP file
     *
     * @param metsFile the mets file
     * @return the list
     */
    List<String> readAndExtractHREFreferences(File metsFile);
    
}
