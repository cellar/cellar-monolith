package eu.europa.ec.opoce.cellar.cl.service.client;

import eu.europa.ec.opoce.cellar.cl.domain.content.SIPObject;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.exception.SIPException;

/**
 * The SIP Loader is a service of the CELLAR system that is responsible for loading a list of files
 * from a specific location in the temporary work folder. The list of files is stored in a
 * {@link SIPObject}.
 * 
 * @author dsavares
 * 
 */
public interface SIPLoader {

    /**
     * Load a Sip object from the specified location.
     * 
     * @param sipResource
     *            the sipResource to find the location where the files needed in the Sip object are stored. 
     *            The path (from the sipResource) is relative to the <i>temporary work folder</i>
     * @exception SIPException
     *                Triggered if SIP file is not accessible.
     * @return the SipObject containing the list of files.
     */
    SIPObject loadSIPObject(SIPResource sipResource) throws SIPException;

}
