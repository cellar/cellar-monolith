package eu.europa.ec.opoce.cellar.cl.domain.response;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public abstract class StructMapOperation implements IOperation<DigitalObjectOperation> {

    /** class's uid. */
    private static final long serialVersionUID = 8326607516293707546L;

    private ResponseOperationType operationType;
    private String id;

    private Map<String, DigitalObjectOperation> digitalObjectOperations;

    public StructMapOperation() {
        this.digitalObjectOperations = new HashMap<String, DigitalObjectOperation>();
    }

    protected StructMapOperation(final ResponseOperationType operationType, final String structMapPID) {
        this.operationType = operationType;
        this.id = structMapPID;
        this.digitalObjectOperations = new HashMap<String, DigitalObjectOperation>();
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.domain.response.IOperation#isCreate()
     */
    @Override
    public boolean isCreate() {
        return ResponseOperationType.CREATE.equals(this.operationType);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.domain.response.IOperation#isUpdate()
     */
    @Override
    public boolean isUpdate() {
        return ResponseOperationType.UPDATE.equals(this.operationType);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.domain.response.IOperation#isRead()
     */
    @Override
    public boolean isRead() {
        return ResponseOperationType.READ.equals(this.operationType);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.domain.response.IOperation#isDelte()
     */
    @Override
    public boolean isDelete() {
        return ResponseOperationType.DELETE.equals(this.operationType);
    }

    /**
     *
     */
    @Override
    public void addAllOperations(final Collection<DigitalObjectOperation> digitalObjectOperations) {
        for (final DigitalObjectOperation digitalObjectOperation : digitalObjectOperations) {
            this.addOperation(digitalObjectOperation);
        }
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.domain.response.IOperation#addOperation(java.lang.Object)
     */
    @Override
    public void addOperation(final DigitalObjectOperation digitalObjectOperation) {
        this.digitalObjectOperations.put(digitalObjectOperation.getCellarIdentifierOperation().getId(), digitalObjectOperation);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.domain.response.IOperation#getOperation(java.lang.String)
     */
    @Override
    public DigitalObjectOperation getOperation(final String cellarId) {
        return this.digitalObjectOperations.get(cellarId);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.domain.response.IOperation#listOperations()
     */
    @Override
    public Iterable<DigitalObjectOperation> listOperations() {
        return this.digitalObjectOperations.values();
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.domain.response.IOperation#getOperations()
     */
    @Override
    public Map<String, DigitalObjectOperation> getOperations() {
        return this.digitalObjectOperations;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.domain.response.IOperation#setOperations(java.util.Map)
     */
    @Override
    public void setOperations(final Map<String, DigitalObjectOperation> digitalObjectOperations) {
        this.digitalObjectOperations = digitalObjectOperations;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.domain.response.IOperation#getOperationType()
     */
    @Override
    public ResponseOperationType getOperationType() {
        return operationType;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.domain.response.IOperation#setOperationType(eu.europa.ec.opoce.cellar.cl.domain.response.ResponseOperationType)
     */
    @Override
    public void setOperationType(final ResponseOperationType operationType) {
        this.operationType = operationType;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.domain.response.IOperation#getId()
     */
    @Override
    public String getId() {
        return id;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.domain.response.IOperation#setId(java.lang.String)
     */
    @Override
    public void setId(final String id) {
        this.id = id;
    }

}
