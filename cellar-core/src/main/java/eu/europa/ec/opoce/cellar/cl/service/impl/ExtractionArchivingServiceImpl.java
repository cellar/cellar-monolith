/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *             FILE : ExtractionServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 8, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.op.cellar.api.service.ICellarResponse;
import eu.europa.ec.op.cellar.api.service.ICellarService;
import eu.europa.ec.op.cellar.api.service.impl.CellarException;
import eu.europa.ec.op.cellar.api.util.WeightedLanguagePairs;
import eu.europa.ec.op.cellar.api.util.WeightedManifestationTypes;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ArchiveTreeManager;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionWorkIdentifier;
import eu.europa.ec.opoce.cellar.cl.service.client.ExtractionArchivingService;
import eu.europa.ec.opoce.cellar.cl.service.client.WeightedTypeCacheService;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.Locale;
import java.util.zip.GZIPOutputStream;

/**
 * <class_description> Implementation of the {@link ExtractionArchivingService}.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 8, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class ExtractionArchivingServiceImpl implements ExtractionArchivingService {

    private static final Logger LOG = LogManager.getLogger(ExtractionArchivingServiceImpl.class);

    private static final String ENCODING = "UTF-8";
    private static final String FORMAT_BRANCH = "xml";
    private static final String FORMAT_CONTENT = "html";

    /**
     * Cellar-API.
     */
    @Autowired
    private ICellarService cellarApiService;

    /**
     * Weighted manifestation types cached manager.
     */
    @Autowired
    private WeightedTypeCacheService weightedTypeCacheService;

    /**
     * {@inheritDoc}
     */
    @Override
    public void createTarGzOfFiles(final String directoryWithFilesPath, final String tarGzPath) throws IOException {
        final String tarGzDirectoryPath = FilenameUtils.getFullPath(tarGzPath);

        final File tarGzDirectory = new File(tarGzDirectoryPath);
        FileUtils.forceMkdir(tarGzDirectory);

        try (FileOutputStream fos = new FileOutputStream(new File(tarGzPath));
             BufferedOutputStream bos = new BufferedOutputStream(fos);
             GZIPOutputStream gcos = new GZIPOutputStream(bos);
             TarArchiveOutputStream taos = new TarArchiveOutputStream(gcos)) {

            final File directory = new File(directoryWithFilesPath);
            final File[] children = directory.listFiles();

            if (ArrayUtils.isNotEmpty(children)) {
                for (final File child : children) { // adds all the files
                    this.addFileToTarGz(taos, child.getAbsolutePath(), "");
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void extractWorkIdentifiers(final String language, final List<ExtractionWorkIdentifier> workIdentifiers,
            final ArchiveTreeManager archiveTreeManager) throws IOException {
        String fileName = null;
        ICellarResponse cellarResponse;

        final WeightedManifestationTypes contentTypes = this.weightedTypeCacheService.getWeightedManifestationTypes(FORMAT_CONTENT);
        final WeightedLanguagePairs langPairs = new WeightedLanguagePairs();
        langPairs.addLanguagePair(null, new Locale(language));

        for (final ExtractionWorkIdentifier workIdentifier : workIdentifiers) {
            fileName = this.extractFileName(workIdentifier);

            // extracts the branch notice
            try {
                cellarResponse = this.extractBranchNotice(workIdentifier.getWorkId(), langPairs);
                this.writeInFile(archiveTreeManager.getFolderPath(), fileName + "." + FORMAT_BRANCH, cellarResponse); // writes the response in file
            } catch (final CellarException cellarException) {
                LOG.debug(String.format("Thread id %s: Cannot extract the branch notice %s - language %s - code %s",
                        Thread.currentThread().getId(), workIdentifier.getWorkId(), language, cellarException.getResponseCode()),
                        cellarException);
            }

            // extracts the content stream
            try {
                cellarResponse = this.extractContentStream(workIdentifier.getWorkId(), langPairs, contentTypes);
                this.writeInFile(archiveTreeManager.getFolderPath(), fileName + "." + FORMAT_CONTENT, cellarResponse); // writes the response in file
            } catch (final CellarException cellarException) {
                LOG.debug(String.format("Thread id %s: Cannot extract the content stream %s - language %s - code %s",
                        Thread.currentThread().getId(), workIdentifier.getWorkId(), language, cellarException.getResponseCode()),
                        cellarException);
            }

        }
    }

    /**
     * Extracts the branch notice corresponding to cellarId and language.
     * @param cellarId the cellar identifier
     * @param language the language required for the branch notice
     * @return the cellar response
     * @throws CellarException if the cellar-api cannot extract the branch notice
     */
    private ICellarResponse extractBranchNotice(final String cellarId, final WeightedLanguagePairs language) throws CellarException {
        return this.cellarApiService.getBranchNotice(cellarId, language);
    }

    /**
     * Extracts the content stream corresponding to cellarId and language.
     * @param cellarId the cellar identifier
     * @param language the language required for the branch notice
     * @param contentTypes the manifestation types
     * @return the cellar response
     * @throws CellarException if the cellar-api cannot extract the content stream
     */
    private ICellarResponse extractContentStream(final String cellarId, final WeightedLanguagePairs language,
            final WeightedManifestationTypes contentTypes) throws CellarException {
        return this.cellarApiService.getManifestation(cellarId, contentTypes, language);
    }

    /**
     * Writes the response of cellar in a file.
     * @param destinationDirectoryPath the destination directory path
     * @param fileName the destination file name
     * @param cellarResponse the cellar response
     * @throws IOException
     */
    private void writeInFile(final String destinationDirectoryPath, final String fileName, final ICellarResponse cellarResponse)
            throws IOException {
        InputStream inputStream = null;
        FileOutputStream fileOutputStream = null;
        OutputStreamWriter writer = null;
        try {
            inputStream = cellarResponse.getResponse();
            fileOutputStream = new FileOutputStream(destinationDirectoryPath + "/" + fileName);
            writer = new OutputStreamWriter(fileOutputStream, ENCODING);
            org.apache.commons.io.IOUtils.copy(inputStream, writer, ENCODING);
            writer.flush();
        } finally {
            org.apache.commons.io.IOUtils.closeQuietly(writer);
            org.apache.commons.io.IOUtils.closeQuietly(fileOutputStream);
            org.apache.commons.io.IOUtils.closeQuietly(inputStream);
        }
    }

    /**
     * Adds a file to the tar.gz archive.
     * @param taos the destination tar.gz archive
     * @param path the file of the path to add
     * @param base the base path in the archive
     * @throws IOException
     */
    private void addFileToTarGz(final TarArchiveOutputStream taos, final String path, final String base) throws IOException {
        final File file = new File(path);
        final String entryName = base + file.getName(); // the entry name in the tar.gz archive
        final TarArchiveEntry tarArchiveEntry = new TarArchiveEntry(file, entryName);

        taos.setLongFileMode(TarArchiveOutputStream.LONGFILE_GNU);
        taos.putArchiveEntry(tarArchiveEntry);

        if (file.isFile()) {
            final FileInputStream fis = new FileInputStream(file);
            try {
                IOUtils.copy(fis, taos);
            } finally {
                org.apache.commons.io.IOUtils.closeQuietly(fis);
            }
            taos.closeArchiveEntry();
        } else { // a directory
            taos.closeArchiveEntry();

            final File[] children = file.listFiles(); // all files of the directory
            if (children != null) {
                for (final File child : children) {
                    this.addFileToTarGz(taos, child.getAbsolutePath(), entryName + "/");
                }
            }
        }
    }

    /**
     * Extracts the destination filename of a work identifier and escapes the '/'.
     * @param workIdentifier the work identifier
     * @return the filename
     */
    private String extractFileName(final ExtractionWorkIdentifier workIdentifier) {
        final String workId = workIdentifier.getWorkId();
        return workId.substring(workId.indexOf('/') + 1).replace('/', '_');
    }

    /**
     * Sets a new {@link ICellarService} instance.
     * @param cellarApiService the new service instance to set
     */
    public void setCellarApiService(final ICellarService cellarApiService) {
        this.cellarApiService = cellarApiService;
    }

    /**
     * Sets a new {@link WeightedTypeCacheService} instance.
     * @param weightedTypeCacheService the new service instance to set
     */
    public void setMimeTypeMappingService(final WeightedTypeCacheService weightedTypeCacheService) {
        this.weightedTypeCacheService = weightedTypeCacheService;
    }
}
