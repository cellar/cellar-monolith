/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.support.metadata.impl
 *             FILE : UpdateBusinessMetadataBackupService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 17-05-2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.support.metadata.impl;

import eu.europa.ec.opoce.cellar.cl.domain.response.DigitalObjectOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.IOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.MetadataResponseOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.ResponseOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.ResponseOperationType;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import org.apache.commons.lang.StringUtils;

/**
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 17-05-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class UpdateBusinessMetadataBackupService extends AbstractBusinessMetadataBackupService {

    public UpdateBusinessMetadataBackupService(ContentStreamService contentStreamService) {
        super(contentStreamService);
    }

    @Override
    public void backupBusinessMetadata(final StructMap structMap, final IOperation<DigitalObjectOperation> operation) {
        structMap.getDigitalObject().applyStrategyOnSelfAndChildren(digitalObject -> {
            DigitalObjectOperation digitalObjectOperation = operation.getOperation(digitalObject.getCellarId().getIdentifier());
            if (digitalObjectOperation == null) {
                digitalObjectOperation = new DigitalObjectOperation(digitalObject, operation.getOperationType());
            }

            if (digitalObject.getBusinessMetadata() != null || digitalObject.getTechnicalMetadata() != null) {
                ResponseOperation response = new MetadataResponseOperation(digitalObject.getCellarId().getIdentifier(), "MD", ResponseOperationType.CREATE);
                response = setVersions(digitalObject.getCellarId(), reduce(digitalObject.getUpdatedTypes()), response);
                if (StringUtils.isNotBlank(response.getLastVersion())) {
                    response.setOperationType(ResponseOperationType.UPDATE);
                }
                digitalObjectOperation.addMetadataIdentifier((MetadataResponseOperation) response);
                if (operation.getOperation(digitalObject.getCellarId().getIdentifier()) == null) {
                    operation.addOperation(digitalObjectOperation);
                }
            }
        });
    }

}
