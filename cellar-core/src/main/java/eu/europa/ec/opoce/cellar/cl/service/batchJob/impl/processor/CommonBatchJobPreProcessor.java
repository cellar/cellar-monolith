/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : CommonBatchJobPreProcessor.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 16 juin 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor;

import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.BatchJobProcessingService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * The Class CommonBatchJobPreProcessor.
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 16 juin 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Configurable
public class CommonBatchJobPreProcessor extends AbstractBatchJobPreProcessor {

    /** The batch job processing service. */
    @Autowired
    private BatchJobProcessingService batchJobProcessingService;

    /**
     * Instantiates a new common batch job pre processor.
     *
     * @param batchJob the batch job
     */
    public CommonBatchJobPreProcessor(final BatchJob batchJob) {
        super(batchJob);
    }

    /** {@inheritDoc} */
    @Override
    protected void doExecute(final BatchJob batchJob) {
        this.batchJobProcessingService.executeSparql(batchJob);
    }

    /** {@inheritDoc} */
    @Override
    protected void preExecute() {
        batchJobService.updateExecutingSparqlBatchJob(batchJob);
    }

    /** {@inheritDoc} */
    @Override
    protected void postExecute() {
        // TODO Auto-generated method stub
    }

}
