/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.aligner.impl
 *             FILE : HierarchyLoaderServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : May 21, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.aligner.impl;

import org.springframework.stereotype.Service;

import eu.europa.ec.opoce.cellar.cl.concurrency.Concurrent;
import eu.europa.ec.opoce.cellar.cl.domain.aligner.Hierarchy;
import eu.europa.ec.opoce.cellar.cl.service.aligner.IHierarchyLoaderService;
import eu.europa.ec.opoce.cellar.domain.content.mets.OffIngestionOperationType;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : May 21, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class HierarchyLoaderServiceImpl extends AbstractHierarchyLoaderServiceImpl implements IHierarchyLoaderService {

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.aligner.IHierarchyLoaderService#getHierarchy(java.lang.String)
     */
    @Override
    @Concurrent(locker = OffIngestionOperationType.ALIGNING)
    public Hierarchy getHierarchy(final String productionId) throws InterruptedException {
        final String rootCellarId = this.getRootCellarId(productionId);
        return this.loadHierarchy(rootCellarId);
    }

}
