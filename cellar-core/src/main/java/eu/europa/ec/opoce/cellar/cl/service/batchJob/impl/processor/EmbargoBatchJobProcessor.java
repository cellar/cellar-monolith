/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor
 *             FILE : EmbargoBatchJobController.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 4, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor;

import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob.BATCH_JOB_TYPE;
import eu.europa.ec.opoce.cellar.server.admin.embargo.EmbargoHandlingServiceInvoker;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * <class_description> Batch job embargo processor.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 4, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Configurable
public class EmbargoBatchJobProcessor extends AbstractBatchJobProcessor {

    private static final Logger LOG = LogManager.getLogger(EmbargoBatchJobProcessor.class);

    /**
     * Embargo service.
     */
    @Autowired
    private EmbargoHandlingServiceInvoker embargoService;

    /**
     * Instantiates a new embargo batch job processor.
     *
     * @param batchJob the batch job
     */
    public EmbargoBatchJobProcessor(final BatchJob batchJob) {
        super(batchJob);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BATCH_JOB_TYPE getBatchJobType() {
        return BATCH_JOB_TYPE.EMBARGO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void doExecute(final String workId, final BatchJob batchJob) {
        try {
            this.embargoService.addToEmbargo(workId, this.batchJob.getEmbargoDate());
        } catch (final Exception exception) {
            LOG.warn("Cannot add the indexing request of '{}'", workId);
        }
    }

    @Override
    protected void preExecute() {
        batchJobService.updateProcessingBatchJob(batchJob);
    }

    @Override
    protected void postExecute() {
        this.batchJobService.updateDoneBatchJob(batchJob);
    }
}
