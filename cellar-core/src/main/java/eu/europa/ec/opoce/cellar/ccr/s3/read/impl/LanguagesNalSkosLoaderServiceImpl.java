/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ccr.s3.read.impl
 *             FILE : LanguagesSkosServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 2, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$$
 *          VERSION : $LastChangedRevision$$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.s3.read.impl;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.ccr.service.LanguagesNalSkosLoaderService;
import eu.europa.ec.opoce.cellar.common.Namespace;
import eu.europa.ec.opoce.cellar.exception.CellarConfigurationException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.nal.NalOntoUtils;
import eu.europa.ec.opoce.cellar.nal.dao.NalConfig;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.support.cache.AbstractNalCachedTableRecordsService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

/**
 * <class_description>
 * This service loads inside an in-memory map the languages listed in the NAL Languages stored in S3</br>
 * Only the languages with both 3 char and 2 char codes will be kept in memory
 * Each language description has the following format
 * {@code
 * <skos:Concept rdf:about="http://publications.europa.eu/resource/authority/language/ENG" at:deprecated="false" at:protocol.order="EU-08">
 * <skos:inScheme rdf:resource="http://publications.europa.eu/resource/authority/language"/>
 * <at:authority-code>ENG</at:authority-code>
 * <at:op-code>ENG</at:op-code>
 * <atold:op-code>ENG</atold:op-code>
 * <dc:identifier>ENG</dc:identifier>
 * <at:start.use>1950-05-09</at:start.use>
 * <skos:prefLabel xml:lang="bg">английски</skos:prefLabel>
 * ...
 * <skos:prefLabel xml:lang="uk">англійська мо́ва</skos:prefLabel>
 * <at:op-mapped-code>
 * <at:MappedCode>
 * <dc:source>iso-639-3</dc:source>
 * <at:legacy-code>eng</at:legacy-code>
 * </at:MappedCode>
 * </at:op-mapped-code>
 * <at:op-mapped-code>
 * <at:MappedCode>
 * <dc:source>iso-639-2b</dc:source>
 * <at:legacy-code>eng</at:legacy-code>
 * </at:MappedCode>
 * </at:op-mapped-code>
 * <at:op-mapped-code>
 * <at:MappedCode>
 * <dc:source>iso-639-2t</dc:source>
 * <at:legacy-code>eng</at:legacy-code>
 * </at:MappedCode>
 * </at:op-mapped-code>
 * <at:op-mapped-code>
 * <at:MappedCode>
 * <dc:source>iso-639-1</dc:source>
 * <at:legacy-code>en</at:legacy-code>
 * </at:MappedCode>
 * </at:op-mapped-code>
 * ...
 * </skos:Concept>
 * }
 * In case of error it should not prevent CELLAR from booting, the splash screen will show the that the languages were not loaded and an error message is logged</br>
 * The interface allows the retrieval of LanguageBeans for a specified URI</br>
 * if the URI is not mapped it returns null</br>
 * A last modification date is stored each time the skos languages file is loaded </br>
 * Whenever the getLanguageBean method is called the last modification date is verified against the property cellar.service.languages.skos.cachePeriod</br>
 * If the specified time period has passed the in-memory map is reloaded</br>
 * <p>
 * In case of an invalid NAL language provided, we can have two scenarios
 * - start of CELLAR -> we load the fallback NAL language and log it as an error, OP needs to fix the NAL language
 * - during a refresh of the cache -> CELLAR will only replace the existing NAL language collection if the processing
 * of the new one has no error. If there are errors, a log error is created and the previous NAL is still used.
 * <br/>
 * NAL language is a little bit more special than NAL file type. An invalid NAL language could potentially corrupt metadata in CELLAR
 * Because of this, we only update the internal collections to be used later on if the NAL language does not contain any invalid information.
 * NAL file type does not have that problem as we just risk missing a possible combinations of manifestion type - extension
 * that could not be responsible of a metadata corruption
 * ON : Oct 2, 2015
 *
 * @author ARHS Developments
 * @version $Revision$$
 */
@Service
public class LanguagesNalSkosLoaderServiceImpl extends AbstractNalCachedTableRecordsService implements LanguagesNalSkosLoaderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LanguagesNalSkosLoaderServiceImpl.class);
    private static final String FALLBACK_NAL_LANGUAGE_NAME = "language.rdf";
    private static final String TWO_CHARS_KEY_CODE = "iso-639-1";
    private static final String THREE_CHARS_KEY_CODE = "identifier";
    private ConcurrentHashMap<String, LanguageDetails> concurrentHashMapByUri;
    private ConcurrentHashMap<String, LanguageDetails> concurrentHashMapByTwoCharCode;
    private ConcurrentHashMap<String, LanguageDetails> concurrentHashMapByThreeCharCode;

    // collections can only be updated if the NAL language is valid (known during the process)
    // these temporary collections hold the data being processed before replacing matching maps content when the
    // process was successful
    private ConcurrentHashMap<String, LanguageDetails> concurrentHashMapByUriBeingLoaded;
    private ConcurrentHashMap<String, LanguageDetails> concurrentHashMapByTwoCharCodeBeingLoaded;
    private ConcurrentHashMap<String, LanguageDetails> concurrentHashMapByThreeCharCodeBeingLoaded;

    /**
     * {@inheritDoc}
     */
    @Override
    public LanguageBean getLanguageBeanByUri(final String languageUri) {
        LanguageBean result = null;
        rwl.readLock().lock();
        try {
            this.reloadIfNecessary();

            if (this.concurrentHashMapByUri != null) {
                result = this.createLanguageBean(this.concurrentHashMapByUri, languageUri);
            }
            if (result == null) {
                final ExceptionBuilder<CellarConfigurationException> exceptionBuilder = ExceptionBuilder
                        .get(CellarConfigurationException.class);
                exceptionBuilder.withCode(CommonErrors.CELLAR_INTERNAL_ERROR);
                exceptionBuilder.withMessage("CELLAR is unable to locate the language {}");
                exceptionBuilder.withMessageArgs(languageUri);
                throw exceptionBuilder.build();
            }
        } finally {
            if (this.rwl != null && this.rwl.readLock() != null) {
                this.rwl.readLock().unlock();
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LanguageBean getLanguageBeanByThreeCharCode(final String threeCharCode) {
        LanguageBean result = null;
        rwl.readLock().lock();
        try {
            this.reloadIfNecessary();
            if (this.concurrentHashMapByThreeCharCode != null) {
                result = this.createLanguageBean(this.concurrentHashMapByThreeCharCode, threeCharCode);
            }
            if (result == null) {
                final ExceptionBuilder<CellarConfigurationException> exceptionBuilder = ExceptionBuilder
                        .get(CellarConfigurationException.class);
                exceptionBuilder.withCode(CommonErrors.CELLAR_INTERNAL_ERROR);
                exceptionBuilder.withMessage("CELLAR is unable to locate the language with iso code {}");
                exceptionBuilder.withMessageArgs(threeCharCode);
                throw exceptionBuilder.build();
            }
        } finally {
            if (this.rwl != null && this.rwl.readLock() != null) {
                this.rwl.readLock().unlock();
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LanguageBean getLanguageBeanByTwoCharCode(final String twoCharCode) {
        LanguageBean result = null;
        rwl.readLock().lock();
        try {
            this.reloadIfNecessary();
            if (this.concurrentHashMapByTwoCharCode != null) {
                result = this.createLanguageBean(this.concurrentHashMapByTwoCharCode, twoCharCode);
            }
            if (result == null) {
                final ExceptionBuilder<CellarConfigurationException> exceptionBuilder = ExceptionBuilder
                        .get(CellarConfigurationException.class);
                exceptionBuilder.withCode(CommonErrors.CELLAR_INTERNAL_ERROR);
                exceptionBuilder.withMessage("CELLAR is unable to locate the language with iso code {}");
                exceptionBuilder.withMessageArgs(twoCharCode);
                throw exceptionBuilder.build();
            }
        } finally {
            if (this.rwl != null && this.rwl.readLock() != null) {
                this.rwl.readLock().unlock();
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean areAllItemsLoaded() {
        boolean result;
        rwl.readLock().lock();
        try {
            result = (this.concurrentHashMapByUri != null) && !this.concurrentHashMapByUri.isEmpty();
        } finally {
            if (this.rwl != null && this.rwl.readLock() != null) {
                this.rwl.readLock().unlock();
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int numberOfItemDescriptionsLoaded() {
        int result = 0;
        rwl.readLock().lock();
        try {
            if (this.concurrentHashMapByUri != null) {
                result = this.concurrentHashMapByUri.size();
            }
        } finally {
            if (this.rwl != null && this.rwl.readLock() != null) {
                this.rwl.readLock().unlock();
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void resetInMemoryMaps() {
        //reset the maps contents
        this.concurrentHashMapByUriBeingLoaded = new ConcurrentHashMap<>();
        this.concurrentHashMapByTwoCharCodeBeingLoaded = new ConcurrentHashMap<>();
        this.concurrentHashMapByThreeCharCodeBeingLoaded = new ConcurrentHashMap<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void collectMappedCodes(final String subjectURI, final String opCode, final HashMap<String, String> mappedCodes, String identifier) {
        final LanguageDetails languageDetails = new LanguageDetails();
        languageDetails.setOpCode(opCode);
        languageDetails.setUri(subjectURI);
        final boolean identifierNotBlank = StringUtils.isNotBlank(identifier);

        final String isoCodeTwoChar = mappedCodes.get(TWO_CHARS_KEY_CODE);
        final boolean code2cNotBlank = StringUtils.isNotBlank(isoCodeTwoChar);

        final boolean opCodeNotBlank = StringUtils.isNotBlank(opCode);
        if (opCodeNotBlank && identifierNotBlank) {
            if ("op_datpro".equalsIgnoreCase(identifier)) {
                return;
            }
            if (identifier.length() != 3) {
                LOGGER.error("Invalid 3 char code provided '{}', length must be 3. The current languages cannot be " +
                        "updated and the NAL language must be fixed and ingested again.", identifier);
                processingErrors = true;
                return;
            }

            //populate the auxiliary maps with the language description if it has all necessary codes
            final HashMap<String, String> codes = new HashMap<>();
            codes.put(THREE_CHARS_KEY_CODE, identifier);
            if (code2cNotBlank) {
                codes.put(TWO_CHARS_KEY_CODE, isoCodeTwoChar);
            }
            languageDetails.setCodes(codes);

            // temporarily store the data in tmp maps. This process needs a big refactoring investment
            concurrentHashMapByUriBeingLoaded.put(subjectURI, languageDetails);
            concurrentHashMapByThreeCharCodeBeingLoaded.put(identifier, languageDetails);
            if (code2cNotBlank) {
                concurrentHashMapByTwoCharCodeBeingLoaded.put(isoCodeTwoChar, languageDetails);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void reload() {
        LOGGER.info("The languages skos will be reloaded");
        try {
            this.reloadModel();
        } catch (final Exception e) {
            LOGGER.warn("It was not possible to re-load the languages skos", e);
            LOGGER.warn("The previously loaded languages skos will be used");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected String getCachePeriod() {
        return this.cellarConfiguration.getCellarServiceLanguagesSkosCachePeriod();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean canReload() {
        boolean result;
        rwl.readLock().lock();
        try {
            result = (this.concurrentHashMapByUri != null) && !this.concurrentHashMapByUri.isEmpty();
        } finally {
            if (this.rwl != null && this.rwl.readLock() != null) {
                this.rwl.readLock().unlock();
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean shouldReload() {
        boolean result;
        rwl.readLock().lock();
        try {
            result = (this.concurrentHashMapByUri == null) || this.concurrentHashMapByUri.isEmpty();
        } finally {
            if (this.rwl != null && this.rwl.readLock() != null) {
                this.rwl.readLock().unlock();
            }
        }
        return result;
    }

    // this method should only be called in case of a successful processing of the NAL because
    // we only update the collections if that's the case
    @Override
    protected void finishProcess() {
        concurrentHashMapByUri = new ConcurrentHashMap<>(concurrentHashMapByUriBeingLoaded);
        concurrentHashMapByTwoCharCode = new ConcurrentHashMap<>(concurrentHashMapByTwoCharCodeBeingLoaded);
        concurrentHashMapByThreeCharCode = new ConcurrentHashMap<>(concurrentHashMapByThreeCharCodeBeingLoaded);
        LOGGER.info("{} languages were successfully loaded, cache has been updated.", concurrentHashMapByUri.size());
    }

    @Override
    protected NalConfig getNalConfig() {
        return nalConfigDao.findByUri(NalOntoUtils.NAL_LANGUAGE_URI).orElse(null);
    }

    @Override
    protected String getEmbeddedNal() throws IOException {
        URL url = LanguagesNalSkosLoaderServiceImpl.class.getResource(FALLBACK_NAL_LANGUAGE_NAME);
        return Resources.toString(url, Charsets.UTF_8);
    }

    @Override
    protected String getNalNamespace() {
        return Namespace.language_ontology;
    }

    @VisibleForTesting
    ConcurrentHashMap<String, LanguageDetails> getMapByUri() {
        return concurrentHashMapByUri;
    }

    /**
     * Creates the language bean.
     *
     * @param map the map
     * @param key the key
     * @return the language bean
     */
    private LanguageBean createLanguageBean(final ConcurrentHashMap<String, LanguageDetails> map, final String key) {
        //return a new instance of the corresponding language bean
        LanguageBean result = null;
        final LanguageDetails languageDetails = map.get(key);
        if (languageDetails != null) {
            result = new LanguageBean();
            result.setUri(languageDetails.getUri());
            result.setCode(languageDetails.getOpCode());
            result.setIsoCodeThreeChar(languageDetails.getCodes().get(THREE_CHARS_KEY_CODE));
            final String twoChars = languageDetails.getCodes().get(TWO_CHARS_KEY_CODE);
            if (twoChars != null) {
                result.setIsoCodeTwoChar(twoChars);
            }
        }
        return result;
    }
}
