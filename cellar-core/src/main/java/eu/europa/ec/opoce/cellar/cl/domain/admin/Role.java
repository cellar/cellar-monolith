/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : Role.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 9, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.admin;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <class_description> Class that represents a role allowed.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 9, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Entity
@Table(name = "ROLES")
public class Role {

    /**
     * Internal id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    /**
     * The role name.
     */
    @Column(name = "ROLE_NAME", nullable = false)
    private String roleName;

    /**
     * The role access identifier.
     */
    @Column(name = "ROLE_ACCESS", nullable = false)
    private String roleAccess;

    /**
     * Default constructor.
     */
    public Role() {

    }

    /**
     * Constructor.
     * @param id the identifier of the role
     */
    public Role(final Long id) {
        this.id = id;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * @param id the id to set
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * @return the roleName
     */
    public String getRoleName() {
        return this.roleName;
    }

    /**
     * @param roleName the roleName to set
     */
    public void setRoleName(final String roleName) {
        this.roleName = roleName;
    }

    /**
     * @return the roleAccess
     */
    public String getRoleAccess() {
        return this.roleAccess;
    }

    /**
     * @param roleAccess the roleAccess to set
     */
    public void setRoleAccess(final String roleAccess) {
        this.roleAccess = roleAccess;
    }
}
