package eu.europa.ec.opoce.cellar.support.sip;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;
import eu.europa.ec.opoce.cellar.support.FileExtensionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FalseFileFilter;
import org.apache.commons.io.filefilter.FileFileFilter;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * The Class CellarReceptionFolderType.
 */
public abstract class CellarReceptionFolderType implements Comparable<CellarReceptionFolderType> {

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger(CellarReceptionFolderType.class);

    /** {@inheritDoc} */
    @Override
    public int compareTo(final CellarReceptionFolderType o) {
        return this.getSipWorkType().compareTo(o.getSipWorkType());
    }

    /**
     * Gets the files from folder.
     *
     * @param cellarConfiguration
     *            the cellar configuration
     * @return the files
     */
    public List<File> getFilesFromFolder(final ICellarConfiguration cellarConfiguration) {
        // get the folder
        final File receptionFolder = new File(this.getReceptionFolderAbsolutePath(cellarConfiguration));
        List<File> result = new ArrayList<File>();
        if (receptionFolder.isDirectory()) {
            // define suffix filter
            final SuffixFileFilter suffixFileFilter = new SuffixFileFilter(FileExtensionUtils.SIP);
            // define file filter
            @SuppressWarnings("serial")
            final IOFileFilter fileFilter = new FileFileFilter() {

                @Override
                public boolean accept(final File file) {
                    // only accept if the suffix matches
                   return suffixFileFilter.accept(file);
                }
                
            };
            final Collection<File> listFiles = FileUtils.listFiles(receptionFolder, fileFilter, FalseFileFilter.INSTANCE);
            if (!listFiles.isEmpty()) {
                result = new LinkedList<File>(listFiles);
                // for performances issues the list is randomized
                if (this.shouldItBeSorted()) {
                    switch (cellarConfiguration.getCellarServiceIngestionPickupMode()) {
                        case random:
                            LOG.info("Randomizing fetched files from [{}]", this.getSipWorkType().toString());
                            Collections.shuffle(result); // randomize the list of
                            // packages
                            break;
                        case none:
                        default:
                            break;
                    }
                }
            }
        }
        return result;
    }

    /**
     * Performs reordering of the provided list of SIP files according to the configuration.
     * @param cellarConfiguration the Cellar configuration.
     * @param sipList the list of SIP files.
     * @return the reordered list of SIP files.
     */
    protected List<File> reorderSipList(final ICellarConfiguration cellarConfiguration, List<File> sipList) {
    	if (this.shouldItBeSorted()) {
            switch (cellarConfiguration.getCellarServiceIngestionPickupMode()) {
                case random:
                    LOG.info("Randomizing fetched files from [{}]", this.getSipWorkType().toString());
                    Collections.shuffle(sipList);
                    break;
                case none:
                default:
                    break;
            }
        }
    	return sipList;
    }
    
    /**
     * Gets the limit of number of files to fetch. For some reasons we should
     * only fetch a certain amount of files and not all at once <=0 will mean
     * all files > 0 will limit the number of files we should fetch
     *
     * @return the limit of files
     */
    protected int getLimitOfFiles(ICellarConfiguration cellarConfiguration) {
        return 0;
    }

    /**
     * Gets the reception folder absolute path.
     *
     * @param cellarConfiguration the cellar configuration
     * @return the reception folder absolute path
     */
    public abstract String getReceptionFolderAbsolutePath(ICellarConfiguration cellarConfiguration);

    /**
     * Gets the sip work type.
     *
     * @return the sip work type
     */
    public abstract TYPE getSipWorkType();

    /**
     * Should it be sorted.
     *
     * @return true, if successful
     */
    protected boolean shouldItBeSorted() {
        return false;
    }

}
