/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.client
 *             FILE : BatchJobService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 12, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.batchJob;

import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob.BATCH_JOB_TYPE;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob.STATUS;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJobWorkIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.common.CellarPaginatedList;
import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService.Priority;
import eu.europa.ec.opoce.cellar.server.admin.sparql.Query;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;

/**
 * <class_description> The batch job service.
 *
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 12, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface BatchJobService {

    /**
     * Returns the list of priorities.
     * @return the list of priorities
     */
    List<Priority> getPriorities();

    /**
     * Returns the queries.
     * @return the queries
     */
    Query[] getQueries();

    /**
     * Returns the query identified by the specified query name.
     *
     * @param query the query name
     * @return the query
     * @throws IOException Signals that an I/O exception has occurred.
     */
    String getQuery(String query) throws IOException;

    /**
     * Returns all instances of {@link BatchJob}.
     *
     * @param jobType the job type
     * @return The list of {@link BatchJob} instances
     */
    List<BatchJob> findBatchJobs(final BATCH_JOB_TYPE jobType);

    /**
     * Find batch jobs by status.
     *
     * @param jobType the job type
     * @return the list
     */
    List<BatchJob> findBatchJobsByStatus(final BatchJob.STATUS jobType);

    /**
     * Find update batch job crons.
     *
     * @return the list of all CRONs
     */
    List<String> findUpdateBatchJobCrons();

    /**
     * Returns all instances of {@link BatchJobWorkIdentifier} belonging to the {@link BatchJob} identified.
     *
     * @param batchJobId the identifier of the {@link BatchJob}
     * @param jobType the job type
     * @return The list of {@link BatchJobWorkIdentifier} instances
     */
    List<BatchJobWorkIdentifier> findWorkIdentifiersByBatchJobId(final Long batchJobId, final BATCH_JOB_TYPE jobType);

    /**
     * Returns the {@link BatchJob} corresponding to the identifier.
     *
     * @param id the identifier of the {@link BatchJob}
     * @param jobType the job type
     * @return The {@link BatchJob} instance
     */
    BatchJob getBatchJob(final Long id, final BATCH_JOB_TYPE jobType);

    /**
     * Returns all instances of {@link BatchJobWorkIdentifier} belonging to the {@link BatchJob} identified by the <code>batchJobId</code> and matching with the pagination information.
     *
     * @param batchJobId the identifier of the {@link BatchJob}
     * @param jobType the job type
     * @param workIdentifiersList the pagination informations
     * @return The paginated list of {@link BatchJobWorkIdentifier} instances
     */
    CellarPaginatedList<BatchJobWorkIdentifier> findWorkIdentifiers(final Long batchJobId, final BATCH_JOB_TYPE jobType,
            final CellarPaginatedList<BatchJobWorkIdentifier> workIdentifiersList);

    /**
     * Deletes a {@link BatchJob}.
     * @param batchJobId the identifier of the job to delete
     */
    void deleteBatchJob(final Long batchJobId);

    /**
     * Starts the indexing of a {@link BatchJob}.
     * @param batchJobId the identifier of the job to indexing
     */
    void startBatchJob(final Long batchJobId);

    /**
     * Restarts a {@link BatchJob}.
     * @param batchJobId the identifier of the job to restart
     */
    void restartBatchJob(final Long batchJobId);

    /**
     * Adds a new {@link BatchJob}.
     * @param batchJob the instance to add
     */
    void addBatchJob(final BatchJob batchJob);

    /**
     * Adds the batch job work identifiers.
     * @param batchJob the parent batch job
     * @param workIds the work identifiers to add
     */
    void addBatchJobWorkIdentifiers(final BatchJob batchJob, final HashSet<String> workIds);

    /**
     * Find by status and cron.
     *
     * @param status the status
     * @param cron the cron
     * @return the list
     */
    List<BatchJob> findByStatusAndCron(STATUS status, String cron);

    /**
     * Finds the work identifiers.
     * @param batchJobId the batch job identifier
     * @param jobType the batch job type
     * @param first the index of the first
     * @param last the index of the last
     * @return the list of work identifiers
     */
    List<String> findWorkIds(final Long batchJobId, final BATCH_JOB_TYPE jobType, final int first, final int last);

    /**
     * Updates the batch job to error.
     * @param batchJob the concerned batch job
     * @param error the error
     */
    void updateErrorBatchJob(final BatchJob batchJob, final Throwable error);

    /**
     * Update error batch job.
     *
     * @param batchJob the batch job
     * @param errorDescription the error description
     * @param errorStackTrace the error stack trace
     */
    void updateErrorBatchJob(final BatchJob batchJob, final String errorDescription, final String errorStackTrace);

    /**
     * Updates the batch job to ready.
     * @param batchJob the concerned batch job
     * @param workIds the work identifiers
     */
    void updateReadyBatchJob(final BatchJob batchJob, final HashSet<String> workIds);

    /**
     * Updates the batch job to done.
     * @param batchJob the concerned batch job
     */
    void updateDoneBatchJob(final BatchJob batchJob);

    /**
     * Updates the batch job to processing.
     * @param batchJob the concerned batch job
     */
    void updateProcessingBatchJob(final BatchJob batchJob);

    /**
     * Update executing sparql batch job.
     *
     * @param batchJob the batch job
     */
    void updateExecutingSparqlBatchJob(final BatchJob batchJob);

    /**
     * Updates the batch job.
     * @param batchJob updated batch job
     */
    void updateBatchJob(final BatchJob batchJob);
}
