/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.concurrency
 *             FILE : ICellarThreadManager.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : May 3, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.concurrency;


/**
 * <class_description> Threads statistics manager interface.
 * <br/><br/>
 * ON : May 3, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface ICellarThreadsManager {

    /**
     * Increment the waiting threads count.
     */
    void incWaitingThreadsCount(final ICellarLockSession cellarLockSession);


    /**
     * Return the indexing waiting threads count
     * @return the indexing waiting threads count
     */
    int getIndexingWaitingThreadsCount();

    /**
     * Return the indexing working threads count
     * @return the indexing working threads count
     */
     int getIndexingWorkingThreadsCount();

    /**
     * Return the non-indexing working threads count
     * @return the non-indexing working threads count
     */
     int getNonIndexingWorkingThreadsCount();

    /**
     * Return the non-indexing waiting threads count
     * @return the non-indexing waiting threads count
     */
     int getNonIndexingWaitingThreadsCount();

    /**
     * Decrement the waiting threads count.
     */
    void decWaitingThreadsCount(final ICellarLockSession cellarLockSession);

    /**
     * Return the working threads count.
     * @return the working threads count
     */

    /**
     * Increment the working threads count.
     * @param cellarLockSession the lock session corresponding to the working session
     */
    void incWorkingThreadsCount(final ICellarLockSession cellarLockSession);

    /**
     * Decrement the working threads count.
     * @param cellarLockSession the lock session corresponding to the working session
     */
    void decWorkingThreadsCount(final ICellarLockSession cellarLockSession);
}
