/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState
 *             FILE : ParsingCleaning.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 5, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState;

import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution.EXECUTION_STATUS;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * <class_description> Parsing cleaning logic.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 5, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component("parsingCleaning")
public class ParsingCleaning extends AbstractCleaning {

    private static final Logger LOG = LogManager.getLogger(ParsingCleaning.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractTask execute(final ExtractionExecution extractionExecution) throws Exception {
        try {
            info(LOG, extractionExecution, "Parsing cleaning");

            this.clean(extractionExecution, true);
            super.cleanError(extractionExecution);

            return this.next(extractionExecution);
        } catch (Exception exception) {
            this.error(extractionExecution, exception);
            error(LOG, extractionExecution, exception);
            throw exception;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractTask next(final ExtractionExecution extractionExecution) {
        extractionExecution.setExecutionStatus(EXECUTION_STATUS.PARSING);
        super.licenseHolderService.updateExtractionExecution(extractionExecution);

        return super.executionStateManager.getTaskState(EXECUTION_STATUS.PARSING);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clean(final ExtractionExecution extractionExecution, final boolean cleanFinalArchive) throws Exception {
        super.licenseHolderService.deleteWorkIdentifiers(extractionExecution.getId());
    }

    /**
     * Error.
     * @param extractionExecution the execution
     * @param exception the error description
     */
    protected void error(final ExtractionExecution extractionExecution, final Exception exception) {
        extractionExecution.setExecutionStatus(EXECUTION_STATUS.PARSING_ERROR);
        super.error(extractionExecution, exception, false);
    }
}
