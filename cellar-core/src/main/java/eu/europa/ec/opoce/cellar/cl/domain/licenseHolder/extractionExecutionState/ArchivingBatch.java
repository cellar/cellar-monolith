/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState
 *             FILE : ArchivingBatch.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 12, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState;

import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ArchiveTreeManager;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution.EXECUTION_STATUS;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionWorkIdentifier;
import eu.europa.ec.opoce.cellar.cl.service.client.ExtractionArchivingService;
import eu.europa.ec.opoce.cellar.cl.service.client.ExtractionExecutionService;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

/**
 * <class_description> Archiving batch logic.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 12, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component("archivingBatch")
public class ArchivingBatch extends AbstractBatch {

    /**
     * Logger instance.
     */
    private static final org.apache.logging.log4j.Logger LOGGER = LogManager.getLogger(ArchivingBatch.class);

    @Autowired
    private ExtractionArchivingService extractionArchivingService;

    @Autowired
    private ExtractionExecutionService extractionExecutionService;

    /**
     * {@inheritDoc}
     */
    @Override
    public void execute(final ExtractionExecution extractionExecution, final int firstOfBatch, final int lastOfBatch,
            final ArchiveTreeManager archiveTreeManager, final ReentrantReadWriteLock readWriteLock, final AtomicInteger batchWorker)
                    throws Exception {
        final String extractionLanguage = extractionExecution.getExtractionConfiguration().getLanguageBean().getIsoCodeTwoChar();
        EXECUTION_STATUS executionStatus;
        final ReadLock readLock = readWriteLock.readLock();

        try {
            try {
                readLock.lock();
                executionStatus = extractionExecution.getExecutionStatus(); // gets the shared status
            } finally {
                readLock.unlock();
            }

            if (executionStatus == EXECUTION_STATUS.ARCHIVING_ERROR) { // if error, the execution is stopped
                info(LOGGER, extractionExecution, "The current archiving batch is aborted because the execution is in ARCHIVING_ERROR");
                return;
            }

            info(LOGGER, extractionExecution, "Archiving batch from %s to %s", firstOfBatch, lastOfBatch);

            final List<ExtractionWorkIdentifier> workIdentifiers = super.licenseHolderService
                    .findWorkIdentifiers(extractionExecution.getId(), firstOfBatch, lastOfBatch); // gets the work identifiers for this batch
            this.extractionArchivingService.extractWorkIdentifiers(extractionLanguage, workIdentifiers, archiveTreeManager); // extracts the work identifiers

            if (batchWorker.getAndDecrement() == 1) // checks the previous value
                this.archivingFinalize(extractionExecution);

        } catch (Exception exception) {
            final WriteLock writeLock = readWriteLock.writeLock();

            try { // updates the status and the error description
                writeLock.lock();
                extractionExecution.setExecutionStatus(EXECUTION_STATUS.ARCHIVING_ERROR);
                super.error(extractionExecution, exception, true);
            } finally {
                writeLock.unlock();
            }
            error(LOGGER, extractionExecution, exception);
            throw exception;
        } finally {
            readLock.unlock();
        }
    }

    /**
     * Prepare the archiving finalize.
     * @param extractionExecution the execution to finalize
     */
    private void archivingFinalize(final ExtractionExecution extractionExecution) {
        extractionExecution.setExecutionStatus(EXECUTION_STATUS.ARCHIVING_FINALIZE);
        this.licenseHolderService.updateExtractionExecution(extractionExecution);
        this.extractionExecutionService.addExecutionTask(extractionExecution);
    }
}
