package eu.europa.ec.opoce.cellar.ccr.velocity;

import java.io.File;

/**
 * Marshall objects using velocity templates.
 * @author phvdveld
 *
 */
public interface VelocityMarshaller {

    /**
     * Marshal the content of the object into a file.
     * @param toMarshal the object to be marshalled
     * @param output {@link File} to be written. If this file already exists, it will be overwritten.
     * @throws MetsMarshallingException If any unexpected errors occur while marshalling
     */
    void marshal(Object toMarshal, File output);

    /**
     * Marshal the content of the object into a file.
     * @param viewName the view to use for marshalling
     * @param toMarshal the object to be marshalled
     * @param output {@link File} to be written. If this file already exists, it will be overwritten.
     * @throws MetsMarshallingException If any unexpected errors occur while marshalling
     */
    void marshal(String viewName, Object toMarshal, File output);
}
