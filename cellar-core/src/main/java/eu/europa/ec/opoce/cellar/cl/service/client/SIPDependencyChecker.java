/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.service.client
 *        FILE : SIPDependencyChecker.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 21-11-2021
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2021 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.client;

import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResourceDependencies;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork;

/**
 * Service for performing the discovery of resources that a SIP
 * package depends on, in terms of Production Identifiers.
 * Includes the complete functionality from SIP loading and extraction
 * to the normalization and filtering of the PIDs.
 * @author EUROPEAN DYNAMICS S.A.
 */
public interface SIPDependencyChecker {

	/**
	 * Performs the discovery of resources that a SIP
	 * package depends on, in terms of Production Identifiers.
	 * Includes the complete functionality from SIP loading and extraction
	 * to the normalization and filtering of the PIDs.
	 * @param sipWork the SIPWork object describing a single SIP package.
	 * @return the PIDs that the SIP depends on, along with additional
	 * information about the SIP itself.
	 */
	SIPResourceDependencies discoverResources(SIPWork sipWork);
	
}
