/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.concurrency.impl
 *             FILE : CellarLockingStrategy.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 7, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.concurrency.impl;

import eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLock.MODE;
import eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLockingStrategy;

/**
 * <class_description> Cellar locking strategy implementation.
 * <br/><br/>
 * ON : Jun 7, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class CellarLockingStrategy implements ICellarLockingStrategy {

    /**
     * Ingestion locking strategy.
     */
    public static final CellarLockingStrategy INGESTION_LOCKING_STRATEGY = new CellarLockingStrategy(MODE.WRITE, MODE.WRITE, MODE.DEFAULT);

    public static final CellarLockingStrategy CLEANING_LOCKING_STRATEGY = new CellarLockingStrategy(MODE.WRITE, MODE.WRITE, MODE.DEFAULT);

    public static final CellarLockingStrategy ALIGNING_LOCKING_STRATEGY = new CellarLockingStrategy(MODE.WRITE, MODE.WRITE, MODE.DEFAULT);

    /**
     * Indexing locking strategy.
     */
    public static final CellarLockingStrategy INDEXING_LOCKING_STRATEGY = new CellarLockingStrategy(MODE.WRITE, MODE.WRITE, MODE.DEFAULT);

    /**
     * Locking mode to use for root.
     */
    private final MODE rootLockingMode;

    /**
     * Locking mode to use for children.
     */
    private final MODE childrenLockingMode;

    /**
     * Locking mode to use for relations.
     */
    private final MODE relationsLockingMode;

    /**
     * Constructor.
     * @param rootLockingMode locking mode to use for root
     * @param childrenLockingMode locking mode to use for children
     * @param relationsLockingMode locking mode to use for relations
     */
    public CellarLockingStrategy(final MODE rootLockingMode, final MODE childrenLockingMode, final MODE relationsLockingMode) {
        this.rootLockingMode = rootLockingMode;
        this.childrenLockingMode = childrenLockingMode;
        this.relationsLockingMode = relationsLockingMode;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MODE getRootLockingMode() {
        return this.rootLockingMode;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MODE getChildrenLockingMode() {
        return this.childrenLockingMode;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MODE getRelationsLockingMode() {
        return this.relationsLockingMode;
    }

}
