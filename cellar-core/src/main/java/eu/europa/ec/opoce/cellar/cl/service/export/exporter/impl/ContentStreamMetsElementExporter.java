/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor.export
 *             FILE : ContentStreamMetsElementExport.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 24, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.export.exporter.impl;

import com.amazonaws.services.s3.Headers;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement;
import eu.europa.ec.opoce.cellar.common.util.CellarIdUtils;
import eu.europa.ec.opoce.cellar.common.util.IOUtils;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.FileType;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.MetsType.FileSec.FileGrp;
import eu.europa.ec.opoce.cellar.mets.v2.builder.FLocatBuilder;
import eu.europa.ec.opoce.cellar.mets.v2.builder.FileBuilder;
import eu.europa.ec.opoce.cellar.mets.v2.builder.FileGrpBuilder;
import eu.europa.ec.opoce.cellar.mets.v2.builder.FptrBuilder;
import eu.europa.ec.opoce.cellar.mets.v2.builder.IContentIdAware;
import eu.europa.ec.opoce.cellar.mets.v2.builder.IDivFptrAware;
import eu.europa.ec.opoce.cellar.mets.v2.builder.MetsBuilder;
import eu.europa.ec.opoce.cellar.mets.v2.builder.enums.LocType;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.rdf.model.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Configurable;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static eu.europa.ec.opoce.cellar.cmr.ContentType.FILE;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 24, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Configurable
public class ContentStreamMetsElementExporter extends AbstractMetsElementExporter {

    private static final Logger LOG = LoggerFactory.getLogger(ContentStreamMetsElementExporter.class);

    private final FptrBuilder fptrBuilder;

    public ContentStreamMetsElementExporter(MetsElement element) {
        super(element);
        this.fptrBuilder = FptrBuilder.get();
    }

    @Override
    protected void doExportMetsElement(final MetsBuilder metsBuilder, final IDivFptrAware<?> parentDivFptrAware,
                                       final MetsElement metsElement, final ZipOutputStream zos) throws IOException {


        final Map<String, Object> metadata = contentStreamService.getMetadata(getCellarId(), getVersions().get(FILE), FILE);
        final String filename = (String) metadata.get(ContentStreamService.Metadata.FILENAME);
        final String mimeType = (String) metadata.get(Headers.CONTENT_TYPE);

        LOG.info("Add the file '{}' in the package.", filename);
        String relativeFilePathFromRootDirectory = CellarIdUtils.createPathFromCellarId(this.getCellarId());
        String relativeFileName = filename;
        if (StringUtils.isNotBlank(relativeFilePathFromRootDirectory)) {
            relativeFileName = relativeFilePathFromRootDirectory + "/" + filename;
        }
        zos.putNextEntry(new ZipEntry(relativeFileName));

        String contentStreamChecksum;
        try (InputStream contentStream = contentStreamService.getContent(getCellarId(), getVersions().get(FILE), FILE)
                .orElseThrow(() -> new IllegalStateException(filename + " not found for " + getCellarId() + "(" + getVersions().get(FILE) + ")"))) {
            contentStreamChecksum = IOUtils.copyAndDigestHex(contentStream, zos, IOUtils.SHA_256);
        }

        final FileType file = FileBuilder.get()
                .withID(getFileId())
                .withFLocat(FLocatBuilder.get()
                        .withHref(relativeFileName)
                        .withLOCTYPE(LocType.URL)
                        .build())
                .withMIMETYPE(mimeType)
                .withCHECKSUMTYPE(IOUtils.SHA_256)
                .withCHECKSUM(contentStreamChecksum)
                .build(); // checksum is not computed

        this.fptrBuilder.withFILEID(file);
        final FileGrpBuilder fileGrpBuilder = FileGrpBuilder.get();
        final FileGrp fileGrp = fileGrpBuilder.withFile(file).build();
        metsBuilder.withFileGrp(fileGrp).build();

        parentDivFptrAware.withFptr(this.fptrBuilder.build());
    }

    /* (non-Javadoc)
     * @see eu.europa.ec.opoce.cellar.cl.service.export.exporter.impl.AbstractMetsElementExporter#getContentIdAware()
     */
    @Override
    protected IContentIdAware<?> getContentIdAware() {
        return this.fptrBuilder;
    }

    private String getFileId() {
        return CellarIdUtils.getCellarIdAsFilename("file_" + this.getCellarId());
    }

    @Override
    protected void doExportMetsElement(final Model model, final MetsBuilder metsBuilder, final IDivFptrAware<?> parentDivFptrAware,
                                       final MetsElement metsElement, final ZipOutputStream zos) {
        //Do Nothing
    }

    @Override
    public void doCreateModel(final Model model, final MetsElement metsElement) {
        //Do Nothing
    }

}
