/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : CellarIdentifier.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 10 mars 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.strategy;

import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.ccr.helper.ProductionIdentifierHelper;
import eu.europa.ec.opoce.cellar.cl.dao.ProductionIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.domain.admin.ProductionIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.cl.domain.history.service.IdentifiersHistoryService;
import eu.europa.ec.opoce.cellar.cl.service.client.CellarIdentifierService;
import eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentStream;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectStrategy;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.domain.content.mets.OperationType;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.StructMapProcessorException;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This class represents the append strategy.
 * This class contains a recursive method applyStrategy that allow to apply the same process for
 * each digital object and their children.
 * <p>
 * The process consist to find (or generate the next) cellar identifier of a digital object.
 * Store it in the database.
 * After, we set in the data model StructMap the created cellar identifier.
 * At the end, all missing digital object exist in S3.
 * Datamodel and database are synchronized.
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class AppendStrategy implements DigitalObjectStrategy {

    private final PidManagerService pidManagerService;
    private final ProductionIdentifierDao productionIdentifierDao;
    private final ContentStreamService contentStreamService;
    private final IdentifiersHistoryService identifiersHistoryService;
    private final CellarIdentifierService cellarIdentifierService;
    private final CellarResourceDao cellarResourceDao;

    // get number of children of the root element (== number of events or expressions)
    private final String workUUID;
    private final SIPResource sipResource;
    private final File metsDirectory;
    private final List<String> ingestedDigitalObjectPidList;
    private int lastExpressionCellarIdSuffix = -1;
    private final Map<String, Integer> lastAttachedManifestationForThisExpressionMap = new HashMap<String, Integer>();

    public AppendStrategy(final String workUUID, DigitalObjectType rootType, final SIPResource sipResource, final File metsDirectory,
                          final PidManagerService pidManagerService,
                          final CellarResourceDao cellarResourceDao, final ProductionIdentifierDao productionIdentifierDao,
                          ContentStreamService contentStreamService, final List<String> ingestedDigitalObjectPidList, final IdentifiersHistoryService identifiersHistoryService,
                          final CellarIdentifierService cellarIdentifierService) {
        this.workUUID = workUUID;
        this.sipResource = sipResource;
        this.metsDirectory = metsDirectory;
        this.contentStreamService = contentStreamService;
        this.ingestedDigitalObjectPidList = ingestedDigitalObjectPidList;

        this.pidManagerService = pidManagerService;
        this.cellarResourceDao = cellarResourceDao;
        this.productionIdentifierDao = productionIdentifierDao;

        this.lastExpressionCellarIdSuffix = lastCellarIdSuffix(workUUID, rootType);
        this.identifiersHistoryService = identifiersHistoryService;
        this.cellarIdentifierService = cellarIdentifierService;
    }

    private int lastCellarIdSuffix(String cellarID, DigitalObjectType rootType) {
        int maxCellarIdSuffix = 0;
        final List<String> childrenPids = getChildrenPIDs(cellarID, rootType).stream()
                .map(CellarResource::getCellarId)
                .sorted()
                .collect(Collectors.toList());
        for (final String childrenPid : childrenPids) {
            final int currentCellarIdSuffix = Integer.parseInt(StringUtils.substringAfterLast(childrenPid, "."));
            if (maxCellarIdSuffix < currentCellarIdSuffix) {
                maxCellarIdSuffix = currentCellarIdSuffix;
            }
        }

        return maxCellarIdSuffix;
    }

    private List<CellarResource> getChildrenPIDs(final String digitalObjectPID, DigitalObjectType rootType) {
        DigitalObjectType type = DigitalObjectType.EXPRESSION;
        if (rootType == DigitalObjectType.DOSSIER) {
            type = DigitalObjectType.EVENT;
        } else if (rootType == DigitalObjectType.EXPRESSION) {
            type = DigitalObjectType.MANIFESTATION;
        }
        return new ArrayList<>(cellarResourceDao.findResourceStartWith(digitalObjectPID, type));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void applyStrategy(final DigitalObject digitalObject) {
        // Treatment of all elements that have a DMDID
        if (digitalObject.getBusinessMetadata() != null) {
            if (cellarIdentifierService.isAnyParentReadOnly(digitalObject)) {
                final ExceptionBuilder<StructMapProcessorException> exceptionBuilder = ExceptionBuilder
                        .get(StructMapProcessorException.class);
                exceptionBuilder.withCode(CoreErrors.E_391);
                exceptionBuilder.withMessage("Cannot append {} as a parent element or itself is marked as read only.");
                exceptionBuilder.withMessageArgs(digitalObject.getType().toString());
                throw exceptionBuilder.build();
            }
            String generatedCellarUUID;
            final DigitalObjectType digitalObjectType = digitalObject.getType();
            final boolean ignoreDuplicatedPids = OperationType.CreateOrIgnore
                    .equals(digitalObject.getStructMap().getMetsDocument().getType());

            if (DigitalObjectType.EVENT.equals(digitalObjectType) || DigitalObjectType.EXPRESSION.equals(digitalObjectType)) {
                // compute and save a new cellarIdentifier for this new expression or event.
                this.lastExpressionCellarIdSuffix++;

                generatedCellarUUID = identifiersHistoryService.generateCellarID(digitalObject, this.workUUID, lastExpressionCellarIdSuffix,
                        TYPE.AUTHENTICOJ);
                this.pidManagerService.save(digitalObject, generatedCellarUUID, ignoreDuplicatedPids);
                final ContentIdentifier contentIdentifier = new ContentIdentifier(generatedCellarUUID);
                digitalObject.setCellarId(contentIdentifier);
                this.lastAttachedManifestationForThisExpressionMap.put(generatedCellarUUID, 0); // they just created the new expression => no manifestation
            }

            if (DigitalObjectType.MANIFESTATION.equals(digitalObjectType)) {
                final ContentIdentifier parentExpressionCID = digitalObject.getParentObject().getCellarId();

                Integer lastManifestation = this.lastAttachedManifestationForThisExpressionMap.get(parentExpressionCID.getIdentifier());

                // If the parent expression already exist in s3, check the existing attached manifestation.
                if (lastManifestation == null) {
                    lastManifestation = lastCellarIdSuffix(parentExpressionCID.getIdentifier(), DigitalObjectType.EXPRESSION);
                    this.lastAttachedManifestationForThisExpressionMap.put(parentExpressionCID.getIdentifier(), lastManifestation);
                }

                lastManifestation++;

                // compute a new CellarIdentifier for this manifestation and save it
                generatedCellarUUID = identifiersHistoryService.generateCellarID(digitalObject, parentExpressionCID.getIdentifier(),
                        lastManifestation, TYPE.AUTHENTICOJ);

                this.pidManagerService.save(digitalObject, generatedCellarUUID, ignoreDuplicatedPids);

                // set in the map, the new number of attached manifestation for this expression.
                // check via s3 will no be anaymore correct! => needs this map.
                this.lastAttachedManifestationForThisExpressionMap.put(parentExpressionCID.getIdentifier(), lastManifestation);

                final ContentIdentifier contentIdentifier = new ContentIdentifier(generatedCellarUUID);
                digitalObject.setCellarId(contentIdentifier);
            }


            new WriteContentStreamStrategy(metsDirectory, contentStreamService).applyStrategy(digitalObject);

            // TODO: todo
            /*if (foxmlFileList.size() != 1) {
                throw ExceptionBuilder.get(StructMapProcessorException.class).withCode(CoreErrors.E_052)
                        .withMessage("The foxml list size = " + foxmlFileList.size() + " but is should be 1.").build();
            }*/


            if (DigitalObjectType.MANIFESTATION.equals(digitalObjectType)) {
                // save new content identifier created during the foxml generation
                for (final ContentStream contentStream : digitalObject.getContentStreams()) {
                    pidManagerService.save(contentStream.getContentids(), contentStream.getCellarId().getIdentifier(),
                            ignoreDuplicatedPids, digitalObject.getReadOnly());
                }
            }

            try {
                //TODO: manage the ingestedDigitalObjectPidList
            } catch (final Exception e) {
                throw ExceptionBuilder.get(StructMapProcessorException.class).withCode(CoreErrors.E_105).withMessage(e.getMessage())
                        .build();
            }
        } else {
            // these pid must exist!
            final ProductionIdentifier pid = ProductionIdentifierHelper.getProductionIdentifier(digitalObject,
                    this.productionIdentifierDao);
            ProductionIdentifierHelper.checkProductionIdentifierExists(pid, digitalObject);
            digitalObject.setCellarId(new ContentIdentifier(pid.getCellarIdentifier().getUuid()));
        }
    }
}
