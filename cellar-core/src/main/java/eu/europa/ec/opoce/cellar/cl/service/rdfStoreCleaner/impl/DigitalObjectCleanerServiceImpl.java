/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.rdfStoreCleaner.impl
 *             FILE : DigitalObjectCleanerServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 1, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.rdfStoreCleaner.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.dao.CleanerDifferenceDao;
import eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner.CleanerDifference;
import eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner.CleanerDigitalObjectData;
import eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner.CleanerHierarchyData;
import eu.europa.ec.opoce.cellar.cl.service.rdfStoreCleaner.DigitalObjectCleanerService;
import eu.europa.ec.opoce.cellar.cl.service.rdfStoreCleaner.DigitalObjectLoaderService;
import eu.europa.ec.opoce.cellar.cl.service.rdfStoreCleaner.DigitalObjectRebuilderService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.CellarResource.CleanerStatus;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 1, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class DigitalObjectCleanerServiceImpl implements DigitalObjectCleanerService {

    private static final Logger LOG = LogManager.getLogger(DigitalObjectCleanerServiceImpl.class);

    /**
     * The Cellar configuration storing runtime parameters.
     */
    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    @Autowired
    private DigitalObjectLoaderService digitalObjectLoaderService;

    @Autowired
    private DigitalObjectRebuilderService digitalObjectRebuilderService;

    @Autowired
    private CleanerDifferenceDao cleanerDifferenceDao;

    @Autowired
    private CellarResourceDao cellarResourceDao;

    @Override
    public void cleanDigitalObject(final CellarResource cellarResource, final CleanerHierarchyData cleanerHierarchyData) {

        final CleanerDigitalObjectData cleanerDigitalObjectData = cleanerHierarchyData.getCleanerDigitalObjectDataInstance(cellarResource);

        this.digitalObjectLoaderService.loadDigitalObject(cleanerDigitalObjectData);
        this.digitalObjectRebuilderService.rebuildDigitalObject(cleanerDigitalObjectData);

        cleanerDigitalObjectData.diagnostic(this.cellarConfiguration.getCellarServiceRDFStoreCleanerMode());

        if (cleanerDigitalObjectData.isValid()) {
            cellarResource.setCleanerStatus(CleanerStatus.VALID);
        } else {

            for (final CleanerDifference rd : cleanerDigitalObjectData.getCleanerDifferences()) {
                this.cleanerDifferenceDao.saveObject(rd);
            }

            cellarResource
                    .setCleanerStatus(CleanerStatus.getDifferenceStatus(this.cellarConfiguration.getCellarServiceRDFStoreCleanerMode()));
        }

        this.cellarResourceDao.updateObject(cellarResource);

        LOG.info("{} is {}.", cellarResource.getCellarId(), cellarResource.getCleanerStatus());
    }
}
