/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.service.client
 *        FILE : SIPDependencyCheckerConfiguration
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 29-11-2021
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2021 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.client;

import static eu.europa.ec.opoce.cellar.common.concurrent.MoreExecutors.newFixedListeningPriorityExecutor;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import eu.europa.ec.opoce.cellar.cl.configuration.impl.CellarPersistentConfiguration;
import eu.europa.ec.opoce.cellar.common.concurrent.ListeningExecutorService;
import eu.europa.ec.opoce.cellar.common.concurrent.NamedThreadFactory;

/**
 * 
 * @author EUROPEAN DYNAMICS S.A.
 */
@Configuration
public class SIPDependencyCheckerConfiguration {
	
	@Bean("sipDependencyCheckerExecutor")
    public ListeningExecutorService sipDependencyCheckerExecutor(@Qualifier("cellarConfiguration") CellarPersistentConfiguration cellarConfiguration) {
        return newFixedListeningPriorityExecutor(cellarConfiguration.getCellarServiceSipDependencyCheckerPoolThreads(),
                new NamedThreadFactory("sipDependencyCheck"));
    }
	
}
