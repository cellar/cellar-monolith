/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.dao
 *             FILE : BatchJobDao.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 11, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao;

import eu.europa.ec.opoce.cellar.cl.domain.sparql.UriTemplates;

import java.util.List;

/**
 * <class_description> Uri Templates dao.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 11, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface UriTemplatesDao extends BaseDao<UriTemplates, Long> {

    /**
     * Find all ordered by sequence
     * 
     * @return all ordered by sequence
     */
    public List<UriTemplates> findAllOrderedBySequence();

    /**
     * Find Uri Templates Where sequence is superior to a given sequence
     * 
     * @return  Uri Templates Where sequence is superior to a given sequence
     */
    public List<UriTemplates> findUriTemplatesBySequence(Long sequence);
}
