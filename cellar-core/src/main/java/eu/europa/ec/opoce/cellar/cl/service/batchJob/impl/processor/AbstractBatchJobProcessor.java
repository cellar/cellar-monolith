/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor
 *             FILE : AbstractBatchJobProcessor.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 4, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor;

import eu.europa.ec.opoce.cellar.cl.dao.BatchJobWorkIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob.BATCH_JOB_TYPE;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.BatchJobService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * <class_description> Common batch job processor service.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 4, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public abstract class AbstractBatchJobProcessor implements IBatchJobProcessor {

    /**
     * Batch size of processing requests.
     */
    protected static final int BATCH_SIZE = 50;

    /**
     * Batch job work identifier dao.
     */
    @Autowired
    protected BatchJobWorkIdentifierDao batchJobWorkIdentifierDao;

    /**
     * The batch job service.
     */
    @Autowired(required = true)
    protected BatchJobService batchJobService;

    /**
     * Batch job to be processed.
     */
    protected final BatchJob batchJob;

    /**
     * Instantiates a new abstract batch job processor.
     *
     * @param batchJob the batch job
     */
    public AbstractBatchJobProcessor(final BatchJob batchJob) {
        this.batchJob = batchJob;
    }

    /**
     * Process the mentioned work.
     * @param workId work identifier
     * @param batchJob the batchJob
     */
    protected abstract void doExecute(final String workId, final BatchJob batchJob);

    /**
     * Get the batch job processor type.
     * @return batch job processor type
     */
    protected abstract BATCH_JOB_TYPE getBatchJobType();

    /**
     * Pre execute.
     */
    protected abstract void preExecute();

    /**
     * Post execute.
     */
    protected abstract void postExecute();

    /**
     * Process the batch job.
     */
    @Override
    public void execute() {

        preExecute();

        this.initBatchJobProcessing();

        int i = 0;

        List<String> workIds = null;

        while (true) {
            workIds = this.batchJobWorkIdentifierDao.findWorkIds(this.batchJob.getId(), this.getBatchJobType(), i * BATCH_SIZE,
                    (i + 1) * BATCH_SIZE);

            for (final String workId : workIds) { // adds the batch
                this.doExecute(workId, this.batchJob);
            }

            if (workIds.size() < BATCH_SIZE) {
                break;
            }

            i++;
        }

        postExecute();
    }

    /**
     * Init method executed before processing.
     */
    protected void initBatchJobProcessing() {

    }
}
