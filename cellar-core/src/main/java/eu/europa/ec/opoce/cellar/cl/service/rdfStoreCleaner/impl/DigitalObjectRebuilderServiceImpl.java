/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.rdfStoreCleaner.impl
 *             FILE : DigitalObjectRebuilderServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 1, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.rdfStoreCleaner.impl;

import eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner.CleanerDigitalObjectData;
import eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner.CleanerHierarchyData;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cl.service.rdfStoreCleaner.DigitalObjectFilterService;
import eu.europa.ec.opoce.cellar.cl.service.rdfStoreCleaner.DigitalObjectRebuilderService;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.InferenceService;
import eu.europa.ec.opoce.cellar.cmr.InverseCalculationService;
import eu.europa.ec.opoce.cellar.cmr.MetaDataIngestionHelper;
import eu.europa.ec.opoce.cellar.cmr.daobeans.InverseRelation;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import org.apache.jena.rdf.model.*;
import org.apache.jena.vocabulary.RDF;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 1, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class DigitalObjectRebuilderServiceImpl implements DigitalObjectRebuilderService {

    protected static final Logger LOG = LogManager.getLogger(DigitalObjectRebuilderServiceImpl.class);

    @Autowired
    @Qualifier("pidManagerService")
    private IdentifierService identifierService;

    @Autowired
    protected InverseCalculationService inverseCalculationService;

    @Autowired
    private MetaDataIngestionHelper metaDataIngestionHelper;

    @Autowired
    protected InferenceService inferenceService;

    @Autowired
    protected DigitalObjectFilterService digitalObjectFilterService;

    @Override
    public void rebuildDigitalObject(final CleanerDigitalObjectData cleanerDigitalObjectData) {
        this.rebuildDirectAndInferred(cleanerDigitalObjectData);
        this.rebuildInverse(cleanerDigitalObjectData);
        this.rebuildInverseRelations(cleanerDigitalObjectData);
    }

    private void rebuildInverseRelations(final CleanerDigitalObjectData cleanerDigitalObjectData) {
        long startTime = System.currentTimeMillis();
        LOG.info("Rebuilding inverse relations for Oracle relational...");

        final String cellarId = cleanerDigitalObjectData.getCellarId();
        final DigitalObjectType digitalObjectType = cleanerDigitalObjectData.getDigitalObjectType();
        final CleanerHierarchyData parent = cleanerDigitalObjectData.getParent();
        final Set<String> metadataUris = parent.getMetadataUris();
        final Set<String> sameAsUris = cleanerDigitalObjectData.getSameAsUris();
        final Map<ContentType, Model> metadataFromS3 = cleanerDigitalObjectData.getMetadataFromS3();
        final Model directInferredModel = metadataFromS3.get(ContentType.DIRECT_INFERRED);
        final Model inverseModel = metadataFromS3.get(ContentType.INVERSE);
        final Collection<InverseRelation> inverseRelationsRebuilt = this.metaDataIngestionHelper.getInverseRelations(cellarId,
                digitalObjectType, metadataUris, sameAsUris, directInferredModel, inverseModel);

        cleanerDigitalObjectData.setInverseRelationsRebuilt(inverseRelationsRebuilt);

        LOG.info("Rebuilding inverse relations for Oracle relational done in {} ms.", (System.currentTimeMillis() - startTime));
    }

    private void rebuildInverse(final CleanerDigitalObjectData cleanerDigitalObjectData) {
        long startTime = System.currentTimeMillis();
        LOG.info("Rebuilding inverse metadata snippets for Oracle RDF Store...");

        Model inverseModel = null;
        Model inverseWithSuperProperties = null;
        try {
            // calculate the basic inverse model
            final Map<ContentType, Model> metadataFromS3 = cleanerDigitalObjectData.getMetadataFromS3();
            final Model inputModel = metadataFromS3.get(ContentType.DIRECT);
            inverseModel = this.inverseCalculationService.getInverseModel(inputModel);
            // add types in the inverse model
            this.addTypes(inverseModel, cleanerDigitalObjectData.getCellarId(), cleanerDigitalObjectData.getSameAsUris(),
                    metadataFromS3.get(ContentType.DIRECT_INFERRED));
            // infer properties so all triples with the super properties are also in the model
            inverseWithSuperProperties = this.inferenceService.calculateInferredSuperpropertyModel(inverseModel);
            // put the so-calculated inverse model onto the map

            this.digitalObjectFilterService.filter(inverseWithSuperProperties);

            final Model model = metadataFromS3.get(ContentType.SAME_AS);
            cleanerDigitalObjectData.setInverseRebuiltForOracle(JenaUtils.create(inverseWithSuperProperties, model));
            metadataFromS3.put(ContentType.INVERSE, JenaUtils.create(inverseWithSuperProperties, model));

        } finally {
            JenaUtils.closeQuietly(inverseModel, inverseWithSuperProperties);
        }

        LOG.info("Rebuilding inverse metadata snippets for Oracle RDF Store done in {} ms.", (System.currentTimeMillis() - startTime));
    }

    private void rebuildDirectAndInferred(final CleanerDigitalObjectData cleanerDigitalObjectData) {
        long startTime = System.currentTimeMillis();
        LOG.info("Rebuilding direct and inferred metadata snippets for Oracle RDF Store...");

        final Map<ContentType, Model> metadataFromS3 = cleanerDigitalObjectData.getMetadataFromS3();
        final Model model = metadataFromS3.get(ContentType.DIRECT_INFERRED);
        final Model metadataRebuiltForOracle = JenaUtils.create(model);

        final Selector selector = new SimpleSelector() {

            @Override
            public boolean selects(final Statement s) {

                if (digitalObjectFilterService.isFiltered(s)) {
                    return true;
                }

                final Resource subject = s.getSubject();

                final boolean uriResource = subject.isURIResource();
                final Set<String> sameAsUris = cleanerDigitalObjectData.getSameAsUris();
                final String uri = subject.getURI();
                return (uriResource && !sameAsUris.contains(uri) && !ContentType.isCellarContentStreamUri(uri));
            }
        };

        metadataRebuiltForOracle.remove(metadataRebuiltForOracle.listStatements(selector));

        cleanerDigitalObjectData.setMetadataRebuiltForOracle(metadataRebuiltForOracle);

        LOG.info("Rebuilding direct and inferred snippets for Oracle RDF Store done in {} ms.", (System.currentTimeMillis() - startTime));
    }

    private void addTypes(final Model inverseModel, final String cellarId, final Set<String> sameAsUris, final Model inferredModel) {

        final String cellarUri = this.identifierService.getUri(cellarId);

        final Resource resourceCellar = ResourceFactory.createResource(cellarUri);
        for (final RDFNode rdfNode : inferredModel.listObjectsOfProperty(resourceCellar, RDF.type).toSet()) {
            inverseModel.add(resourceCellar, RDF.type, rdfNode);
        }
        for (final String sameAsUri : sameAsUris) {

            if (!sameAsUri.equalsIgnoreCase(cellarUri)) {
                final Resource resourcePs = ResourceFactory.createResource(sameAsUri);
                for (final RDFNode rdfNode : inferredModel.listObjectsOfProperty(resourcePs, RDF.type).toSet()) {
                    inverseModel.add(resourcePs, RDF.type, rdfNode);
                }
            }
        }
    }
}
