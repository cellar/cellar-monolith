/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.dao.impl
 *        FILE : PackageQueueDaoImpl.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 03-12-2021
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2021 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao.impl;

import org.springframework.stereotype.Repository;

import eu.europa.ec.opoce.cellar.cl.dao.PackageQueueDao;
import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.PackageQueue;

/**
 * Provides access to the database view containing
 * the prioritized references of the SIP packages available
 * for ingestion.
 * @author EUROPEAN DYNAMICS S.A.
 */
@Repository
public class PackageQueueDaoImpl extends BaseDaoImpl<PackageQueue, Long> implements PackageQueueDao {
	
}
