/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ccr.validation
 *             FILE : ValidationServiceStatus.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 04 12, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-04-12 11:38:52 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.validation;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.support.AbstractServiceStatus;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * @author ARHS Developments
 */
@Component("validationServiceStatus")
public class ValidationServiceStatus extends AbstractServiceStatus {

    public ValidationServiceStatus(@Qualifier("cellarConfiguration") ICellarConfiguration cellarConfiguration) {
        super("SHACL", cellarConfiguration);
    }

    @Override
    protected String resolveServiceUrl() {
        return this.cellarConfiguration.getCellarServiceIntegrationValidationBaseUrl() + "/health";
    }
}
