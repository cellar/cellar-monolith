/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.dao.impl
 *        FILE : AbstractStructMapResponseDao.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 19-06-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao.impl;

import eu.europa.ec.opoce.cellar.cl.dao.StructMapResponseDao;
import eu.europa.ec.opoce.cellar.cl.domain.response.StructMapResponse;
import eu.europa.ec.opoce.cellar.common.util.XMLUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

/**
 * @author ARHS Developments
 */
public abstract class AbstractStructMapResponseDao implements StructMapResponseDao {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractStructMapResponseDao.class);
    public static final String INGEST_RESPONSE_EXTENSION = ".response.xml";

    @Override
    public File save(final StructMapResponse structMapResponse) {
        final File rootDir = resolveResponseFolder(true);
        final File ingestResponseFile = new File(rootDir, resolveResponseFilename(structMapResponse.getMetsDocumentId()));

        try {
            XMLUtils.saveObject(structMapResponse, ingestResponseFile);
        } catch (IOException e) {
            LOG.error(e.getMessage());
            return null;
        }

        return ingestResponseFile;
    }

    protected abstract String resolveResponseFolderPath();

    private File resolveResponseFolder(final boolean createIfNotExist) {
        final String rootDirPath = this.resolveResponseFolderPath();
        final File rootDir = new File(rootDirPath);

        if(createIfNotExist) {
            rootDir.mkdirs();
        }

        return rootDir;
    }

    @Override
    public File getResponseFile(final String metsDocumentId) {
        return new File(this.resolveResponseFolderPath(), resolveResponseFilename(metsDocumentId));
    }

    private String resolveResponseFilename(final String metsDocumentId) {
        return metsDocumentId + INGEST_RESPONSE_EXTENSION;
    }
}
