package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService;

/**
 * This class is used for storing configuration parameters of the {@link PidManagerService}.
 * 
 * @author dsavares
 * 
 */
public class PidManagerConfigParams {

    private String cellarResourceBaseURI;
    private String dissiminationBaseURI;
    private String cellarS3Namespace;

    /**
     * This constructor will initialize all the parameters.
     * 
     * @param cellarResourceBaseURI
     *            the CMR base URI
     * @param dissiminationBaseURI
     *            the dissimination base URI
     */
    public PidManagerConfigParams(final String cellarResourceBaseURI, final String dissiminationBaseURI,
            final String cellarS3Namespace) {
        this.cellarResourceBaseURI = cellarResourceBaseURI;
        this.dissiminationBaseURI = dissiminationBaseURI;
        this.cellarS3Namespace = cellarS3Namespace;
    }

    /**
     * Sets a new value for the cellarResourceBaseURI.
     * 
     * @param cellarResourceBaseURI
     *            the cellarResourceBaseURI to set
     */
    public void setCellarResourceBaseURI(final String cellarResourceBaseURI) {
        this.cellarResourceBaseURI = cellarResourceBaseURI;
    }

    /**
     * Gets the value of the cMRBaseURI.
     * 
     * @return the cMRBaseURI
     */
    public String getCellarResourceBaseURI() {
        return cellarResourceBaseURI;
    }

    /**
     * Sets a new value for the dissiminationResourceBaseURI.
     * 
     * @param dissiminationBaseURI
     *            the dissiminationBaseURI to set
     */
    public void setDissiminationBaseURI(final String dissiminationBaseURI) {
        this.dissiminationBaseURI = dissiminationBaseURI;
    }

    /**
     * Gets the value of the dissiminationBaseURI.
     * 
     * @return the dissiminationURI
     */
    public String getDissiminationBaseURI() {
        return dissiminationBaseURI;
    }

    /**
     * Gets the value of the cellarS3Namespace.
     * @return the cellarS3Namespace
     */
    public String getCellarS3Namespace() {
        return cellarS3Namespace;
    }

    /**
     * Set the value of the cellarS3Namespace.
     */
    public void setCellarS3Namespace(String cellarS3Namespace) {
        this.cellarS3Namespace = cellarS3Namespace;
    }
}
