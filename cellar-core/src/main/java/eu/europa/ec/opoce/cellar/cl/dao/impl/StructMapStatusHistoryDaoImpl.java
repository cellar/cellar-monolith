package eu.europa.ec.opoce.cellar.cl.dao.impl;

import eu.europa.ec.opoce.cellar.cl.dao.StructMapStatusHistoryDao;
import eu.europa.ec.opoce.cellar.cl.domain.package_history.PackageHistory;
import eu.europa.ec.opoce.cellar.cl.domain.structmap_status_history.StructMapStatusHistory;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This DAO is responsible for managing the 'StructMap Status History' objects.
 *
 * @author EUROPEAN DYNAMICS S.A.
 */

@Repository
public class StructMapStatusHistoryDaoImpl extends BaseDaoImpl<StructMapStatusHistory, Long> implements StructMapStatusHistoryDao {
    
    /**
     * {@inheritDoc}
     */
    @Override
    public StructMapStatusHistory getByNameAndPackageHistory(String structMapName, PackageHistory packageHistory) {
        Map<String, Object> params = new HashMap<>();
        params.put("sshStructMapName", structMapName);
        params.put("sshPackageHistory", packageHistory);
        return super.findObjectByNamedQueryWithNamedParams("StructMapStatusHistory.findByNameAndPackageHistory", params);
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public List<StructMapStatusHistory> getByPackageHistory(PackageHistory packageHistory) {
        Map<String, Object> params = new HashMap<>();
        params.put("sshPackageHistory", packageHistory);
        return super.findObjectsByNamedQueryWithNamedParams("StructMapStatusHistory.findByPackageHistory", params);
    }
}
