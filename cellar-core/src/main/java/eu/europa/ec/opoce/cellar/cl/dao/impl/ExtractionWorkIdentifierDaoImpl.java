/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.dao.impl
 *             FILE : ExtractionWorkIdentifierDaoImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 5, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao.impl;

import eu.europa.ec.opoce.cellar.cl.dao.ExtractionWorkIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.domain.common.CellarPaginatedList;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionWorkIdentifier;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 5, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Repository
public class ExtractionWorkIdentifierDaoImpl extends BaseDaoImpl<ExtractionWorkIdentifier, Long> implements ExtractionWorkIdentifierDao {

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteWorkIdentifiersExecutionId(final Long executionId) {
        super.getHibernateTemplate().bulkUpdate("DELETE FROM ExtractionWorkIdentifier WHERE extractionExecution.id = ?0", executionId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addWorkIdentifierBatch(final List<ExtractionWorkIdentifier> workIdentifiers) {
        for (final ExtractionWorkIdentifier extractionWorkIdentifier : workIdentifiers) {
            super.getHibernateTemplate().saveOrUpdate(extractionWorkIdentifier);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ExtractionWorkIdentifier> findWorkIdentifiers(final Long extractionExecutionId, final int first, final int last) {

        return getHibernateTemplate().execute(session -> {
            final Query queryObject = session.getNamedQuery("findWorkIdentifiers");
            queryObject.setFirstResult(first);
            queryObject.setMaxResults(last - first);
            queryObject.setParameter(0, extractionExecutionId);
            return queryObject.list();
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CellarPaginatedList<ExtractionWorkIdentifier> findWorkIdentifiers(final Long extractionExecutionId,
                                                                             final CellarPaginatedList<ExtractionWorkIdentifier> workIdentifiersList) {

        return getHibernateTemplate().execute(session -> {
            // gets the page objects
            final Query queryObject = session.createQuery(
                    "select w from ExtractionWorkIdentifier w where w.extractionExecution.id = :executionId and w.workId like :filterValue order by w."
                            + workIdentifiersList.getSortCriterion() + " " + workIdentifiersList.getSortDirectionCode());
            queryObject.setParameter("executionId", extractionExecutionId);
            queryObject.setParameter("filterValue", "%" + workIdentifiersList.getFilterValue() + "%");
            queryObject.setFirstResult(workIdentifiersList.getObjectsPerPage() * (workIdentifiersList.getPageNumber() - 1));
            queryObject.setMaxResults(workIdentifiersList.getObjectsPerPage());

            workIdentifiersList.setList(queryObject.list());

            return workIdentifiersList;
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ExtractionWorkIdentifier> findWorkIdentifiers(final Long extractionExecutionId) {
        return super.findObjectsByNamedQuery("findWorkIdentifiers", extractionExecutionId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ExtractionWorkIdentifier> findMatchWorkIdWorkIdentifiers(final Long extractionExecutionId, final String workIdFilter) {
        return super.findObjectsByNamedQuery("findMatchWorkIdWorkIdentifiers", extractionExecutionId, "%" + workIdFilter + "%");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long getWorkIdentifierCount(final Long executionId) {
        return getHibernateTemplate().execute(session -> {
            final Query queryObject = session.getNamedQuery("findCountExtractionWorkIdentifiers");
            queryObject.setParameter("executionId", executionId);
            return (Long) queryObject.uniqueResult();
        });
    }
}
