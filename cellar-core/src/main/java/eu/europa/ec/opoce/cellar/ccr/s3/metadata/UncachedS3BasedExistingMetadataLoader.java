/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cmr.ingest.impl
 *             FILE : UncachedS3BasedExistingMetadataLoader.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 19-03-2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.s3.metadata;

import eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.model.ModelUtils;
import eu.europa.ec.opoce.cellar.common.metadata.IExistingMetadataLoader;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.Lang;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * <class_description> Service for loading existing data from S3.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 19-03-2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service("uncachedS3BasedExistingMetadataLoader")
public class UncachedS3BasedExistingMetadataLoader implements IExistingMetadataLoader<String, Map<String, Model>, Map<ContentType, String>> {

    private static final Logger LOG = LogManager.getLogger(UncachedS3BasedExistingMetadataLoader.class);

    @Autowired
    private CellarResourceDao cellarResourceDao;

    @Autowired
    private ContentStreamService contentStreamService;

    /**
     * @see IExistingMetadataLoader#loadDirect(java.lang.Object)
     */
    @Override
    public Map<String, Model> loadDirect(final String cellarId) {
        return this.load(cellarId, ContentType.DIRECT);
    }

    /**
     * @see IExistingMetadataLoader#loadInferred(java.lang.Object)
     */
    @Override
    public Map<String, Model> loadInferred(final String cellarId) {
        final Map<String, Model> direct = this.loadDirect(cellarId);
        final Map<String, Model> directAndInferred = this.loadDirectAndInferred(cellarId);

        final Map<String, Model> inferred = new HashMap<>();
        try {
            for (final Entry<String, Model> directAndInferredEntry : directAndInferred.entrySet()) {
                final String directAndInferredContext = directAndInferredEntry.getKey();
                final Model directAndInferredModel = directAndInferredEntry.getValue();
                final Model directModel = direct.get(directAndInferredContext);
                if (!ModelUtils.isBlank(directModel)) {
                    inferred.put(directAndInferredContext, directAndInferredModel.difference(directModel));
                }
            }
        } finally {
            ModelUtils.closeQuietly(direct);
            ModelUtils.closeQuietly(directAndInferred);
        }

        return inferred;
    }

    /**
     * @see IExistingMetadataLoader#loadDirectAndInferred(java.lang.Object, java.lang.Object[])
     */
    @Override
    public Map<String, Model> loadDirectAndInferred(final String cellarId, final Object... options) {
        return this.load(cellarId, ContentType.DIRECT_INFERRED);
    }

    protected Map<String, Model> load(final String cellarId, final ContentType contentType) {
        final Map<String, Model> models = new HashMap<>();
        final Collection<CellarResource> resources = this.cellarResourceDao.findHierarchy(cellarId);
        for (final CellarResource resource : resources) {
            if (resource.getCellarType() != DigitalObjectType.ITEM) {
                String version = resource.getVersion(contentType).orElse(null);
                if (contentType == ContentType.DIRECT && LOG.isInfoEnabled(IConfiguration.INGESTION_MD_UPDATE)) {
                    LOG.info(IConfiguration.INGESTION_MD_UPDATE, "Ingestion {} ({}) -> read  (db): {}", cellarId, contentType, resource.getVersions());
                    ThreadContext.put("INGESTION_MD_UPDATE_EXISTING_READ_DIRECT" + resource.getCellarId(), version);
                }
                final String model = contentStreamService.getContentAsString(resource.getCellarId(), version, contentType)
                        .orElseThrow(() -> new IllegalStateException(contentType + " not found for " + resource.getCellarId() + "(" + version + ")"));
                models.put(resource.getCellarId(), ModelUtils.read(model, Lang.NT));
            }
        }
        return models;
    }
}
