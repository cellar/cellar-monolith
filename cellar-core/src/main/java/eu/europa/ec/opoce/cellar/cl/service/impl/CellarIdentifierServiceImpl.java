/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : CellarIdentifier.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 25 mai 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.cl.dao.CellarIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;
import eu.europa.ec.opoce.cellar.cl.service.client.CellarIdentifierService;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class CellarIdentifierServiceImpl.
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 25 mai 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
@Transactional
public class CellarIdentifierServiceImpl implements CellarIdentifierService {

    /** The cellar identifier dao. */
    @Autowired
    private CellarIdentifierDao cellarIdentifierDao;
    @Autowired
    public CellarIdentifierServiceImpl(final CellarIdentifierDao cellarIdentifierDao){
        this.cellarIdentifierDao=cellarIdentifierDao;
    }

    /** {@inheritDoc} */
    @Override
    public Boolean isAnyParentReadOnly(final DigitalObject digitalObject) {
        return recursiveUpwardsCheck(digitalObject);
    }

    /** {@inheritDoc} */
    @Override
    public Boolean isAnyChildReadOnly(final DigitalObject digitalObject) {
        final ContentIdentifier cellarId = digitalObject.getCellarId();
        final String identifier = cellarId.getIdentifier();
        final CellarIdentifier cellarIdentifier = cellarIdentifierDao.getCellarIdentifier(identifier);
        return downwardsCheck(cellarIdentifier);
    }

    /**
     * Recursive upwards check.
     *
     * @param digitalObject the digital object
     * @return the boolean
     */
    private Boolean recursiveUpwardsCheck(final DigitalObject digitalObject) {
        Boolean result = Boolean.FALSE;
        final ContentIdentifier cellarId = digitalObject.getCellarId();
        final DigitalObject parentObject = digitalObject.getParentObject();
        CellarIdentifier cellarIdentifier = null;
        if (cellarId != null) {
            final String identifier = cellarId.getIdentifier();
            cellarIdentifier = cellarIdentifierDao.getCellarIdentifier(identifier);
        }
        if (cellarIdentifier == null || !Boolean.TRUE.equals(cellarIdentifier.getReadOnly())) {
            //if the current is not yet persistent check parent
            if (parentObject != null) {
                result = recursiveUpwardsCheck(parentObject);
            }
        } else {
            result = cellarIdentifier.getReadOnly();
        }

        return result != null ? result : Boolean.FALSE;
    }

    /**
     * Recursive downwards check.
     *
     * @param cellarIdentifier the cellar identifier
     * @return the boolean
     */
    public Boolean downwardsCheck(final CellarIdentifier cellarIdentifier) {
        Boolean result = Boolean.FALSE;
        if (cellarIdentifier == null)
            return false;
        if (!Boolean.TRUE.equals(cellarIdentifier.getReadOnly())) {
            final String uuid = cellarIdentifier.getUuid();
            final List<CellarIdentifier> childrenPids = cellarIdentifierDao.getAllChildrenPids(uuid);
            //if the current is not yet persistent check childs
            if (childrenPids != null) {
                //remove itself - the query uses a like clause
                childrenPids.remove(cellarIdentifier);
                for (final CellarIdentifier childPid : childrenPids) {
                    result = childPid.getReadOnly();
                    if (Boolean.TRUE.equals(result)) {
                        break;
                    }
                }
            }
        } else {
            result = cellarIdentifier.getReadOnly();
        }

        return result != null ? result : Boolean.FALSE;
    }

    /** {@inheritDoc} */
    @Override
    public CellarIdentifier getCellarIdentifier(final String cellarId) {
        return cellarIdentifierDao.getCellarIdentifier(cellarId);
    }

    /** {@inheritDoc} */
    @Override
    public Boolean isReadOnly(final String cellarId) {
        final CellarIdentifier cellarIdentifier = cellarIdentifierDao.getCellarIdentifier(cellarId);
        return cellarIdentifier == null ? Boolean.FALSE : cellarIdentifier.getReadOnly();
    }

    /** {@inheritDoc} */
    @Override
    public void updateCellarIdentifier(final CellarIdentifier cellarIdentifier) {
        cellarIdentifierDao.updateCellarIdentifier(cellarIdentifier);
    }

}
