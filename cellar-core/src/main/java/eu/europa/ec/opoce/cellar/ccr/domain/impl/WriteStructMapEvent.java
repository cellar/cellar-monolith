package eu.europa.ec.opoce.cellar.ccr.domain.impl;

import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.cl.domain.structmap_status_history.StructMapStatusHistory;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;

public class WriteStructMapEvent {
    
    private final SIPResource sipResource;
    
    private final StructMap structMap;
    
    private final StructMapStatusHistory structMapStatusHistory;
    
    public WriteStructMapEvent(SIPResource sipResource, StructMap structMap, StructMapStatusHistory structMapStatusHistory) {
        this.sipResource = sipResource;
        this.structMap = structMap;
        this.structMapStatusHistory = structMapStatusHistory;
    }
    
    public SIPResource getSipResource() {
        return sipResource;
    }
    
    public StructMap getStructMap() {
        return structMap;
    }
    
    public StructMapStatusHistory getStructMapStatusHistory() {
        return structMapStatusHistory;
    }
}
