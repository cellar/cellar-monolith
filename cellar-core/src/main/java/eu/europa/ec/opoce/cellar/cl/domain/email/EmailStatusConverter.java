package eu.europa.ec.opoce.cellar.cl.domain.email;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class EmailStatusConverter implements AttributeConverter<EmailStatus, String> {

    @Override
    public String convertToDatabaseColumn(EmailStatus status) {
        if (status == null) {
            return null;
        }
        return status.getValue();
    }

    @Override
    public EmailStatus convertToEntityAttribute(String code) {
        if (code == null) {
            return null;
        }
        return EmailStatus.findByValue(code);
    }

}
