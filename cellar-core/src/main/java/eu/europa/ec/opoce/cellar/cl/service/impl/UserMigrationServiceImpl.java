/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *        FILE : UserMigrationServiceImpl.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 15-03-2023
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2023 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jasypt.springsecurity4.crypto.password.PBEPasswordEncoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eu.europa.ec.opoce.cellar.cl.domain.admin.User;
import eu.europa.ec.opoce.cellar.cl.service.client.SecurityService;
import eu.europa.ec.opoce.cellar.cl.service.client.UserMigrationService;

/**
 * Provides functionality for associating an existing CELLAR user
 * with a pre-authenticated EU-Login user.
 * @author EUROPEAN DYNAMICS S.A
 */
@Service
public class UserMigrationServiceImpl implements UserMigrationService {

	/**
	 * 
	 */
	private static final Logger LOG = LogManager.getLogger(UserMigrationServiceImpl.class);
	/**
	 * 
	 */
	private final PBEPasswordEncoder passwordEncoder;
	/**
	 * 
	 */
	private final SecurityService securityService;
	
	@Autowired
	public UserMigrationServiceImpl(PBEPasswordEncoder passwordEncoder, SecurityService securityService) {
		this.passwordEncoder = passwordEncoder;
		this.securityService = securityService;
	}

	@Override
	@Transactional
	public void migrateUser(String newUsername, String currentUsername, String password) {
		User user = this.securityService.findUserByUsername(currentUsername);
		if (user == null) {
			LOG.warn("Unauthorized principal [{}]: The provided CELLAR user could not be found.", newUsername);
			throw new UserNotFoundException("");
		}
		if (StringUtils.isEmpty(user.getPassword())) {
			LOG.warn("Unauthorized principal [{}]: The provided CELLAR user is already migrated.", newUsername);
			throw new UserAlreadyMigratedException("");
		}
		if (!this.passwordEncoder.matches(password, user.getPassword())) {
			LOG.warn("Unauthorized principal [{}]: The provided password is invalid.", newUsername);
			throw new InvalidPasswordException("");
		}
		user.setUsername(newUsername);
		user.setPassword(null);
		LOG.info("CELLAR username [{}] has been successfully changed to [{}].", currentUsername, newUsername);
	}
	
	
	public static class UserNotFoundException extends RuntimeException {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
	
		private UserNotFoundException(String message) {
			super(message);
		}
		
	}
	
	public static class InvalidPasswordException extends RuntimeException {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
	
		private InvalidPasswordException(String message) {
			super(message);
		}
		
	}
	
	public static class UserAlreadyMigratedException extends RuntimeException {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
	
		private UserAlreadyMigratedException(String message) {
			super(message);
		}
		
	}
	
	
}
