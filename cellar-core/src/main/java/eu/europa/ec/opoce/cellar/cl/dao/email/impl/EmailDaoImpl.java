package eu.europa.ec.opoce.cellar.cl.dao.email.impl;

import eu.europa.ec.opoce.cellar.cl.dao.email.EmailDao;
import eu.europa.ec.opoce.cellar.cl.dao.impl.BaseDaoImpl;
import eu.europa.ec.opoce.cellar.cl.domain.email.Email;
import eu.europa.ec.opoce.cellar.cl.domain.email.EmailStatus;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Repository
public class EmailDaoImpl extends BaseDaoImpl<Email, Long> implements EmailDao {
	
    @Override
    public List<Email> getEmailsToSend() {
        final HashMap<String, Object> params = new HashMap<>();
        params.put("new", EmailStatus.NEW);
        params.put("failed", EmailStatus.FAILED);
        return this.findObjectsByNamedQueryWithNamedParams("getAllEmailsToSend", params);
    }
    
    
    @Override
    public List<Email> getEmailsCreatedBefore(Date cutoffDate) {
        final HashMap<String, Object> params = new HashMap<>();
        params.put("cutoffDate", cutoffDate);
        return this.findObjectsByNamedQueryWithNamedParams("getEmailsCreatedBefore", params);
    }
    
}
