/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor
 *             FILE : BatchJobProcessorFactory.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 5, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor;

import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.common.factory.IFactory;
import org.springframework.stereotype.Component;

/**
 * <class_description> Batch job processor factory.
 * <p>
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 5, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component("batchJobProcessorFactory")
public class BatchJobProcessorFactory implements IFactory<IBatchJobProcessor, BatchJob> {

    /**
     * Return the needed processing according to the batch job type.
     */
    @Override
    public IBatchJobProcessor create(final BatchJob batchJob) {
        switch (batchJob.getJobType()) {
            case EMBARGO:
                return new EmbargoBatchJobProcessor(batchJob);
            case EXPORT:
                return new ExportBatchJobProcessor(batchJob);
            case REINDEX:
                return new ReindexBatchJobProcessor(batchJob);
            case UPDATE:
                return new UpdateBatchJobProcessor(batchJob);
            case LAST_MODIFICATION_DATE_UPDATE:
                return new LastModificationDateUpdateBatchJobProcessor(batchJob);
            case DELETE:
                return new DeleteJobProcessor(batchJob);
            default:
                return null;
        }
    }

}
