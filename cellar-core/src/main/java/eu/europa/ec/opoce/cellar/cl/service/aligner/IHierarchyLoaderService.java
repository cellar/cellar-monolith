/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.aligner
 *             FILE : HierarchyLoaderService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : May 21, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.aligner;

import eu.europa.ec.opoce.cellar.cl.domain.aligner.Hierarchy;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : May 21, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IHierarchyLoaderService {

    Hierarchy getHierarchy(final String productionId) throws InterruptedException;
}
