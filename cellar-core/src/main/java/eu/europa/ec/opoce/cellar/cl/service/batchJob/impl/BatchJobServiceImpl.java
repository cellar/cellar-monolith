/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *             FILE : BatchJobServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 12, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.batchJob.impl;

import eu.europa.ec.opoce.cellar.ccr.service.database.DefaultTransactionService;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.dao.BatchJobDao;
import eu.europa.ec.opoce.cellar.cl.dao.BatchJobWorkIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob.BATCH_JOB_TYPE;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob.STATUS;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJobWorkIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.common.CellarPaginatedList;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.BatchJobService;
import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService.Priority;
import eu.europa.ec.opoce.cellar.common.util.ErrorUtils;
import eu.europa.ec.opoce.cellar.server.admin.sparql.Query;
import eu.europa.ec.opoce.cellar.support.SparqlUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * <class_description> Batch job service.
 *
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 12, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class BatchJobServiceImpl extends DefaultTransactionService implements BatchJobService {

    /**
     * Batch job dao.
     */
    @Autowired
    private BatchJobDao batchJobDao;

    /**
     * Batch job work identifier dao.
     */
    @Autowired
    private BatchJobWorkIdentifierDao batchJobWorkIdentifierDao;

    /**
     * The Cellar configuration storing runtime parameters.
     */
    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Priority> getPriorities() {
        return Arrays.asList(Priority.values());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Query[] getQueries() {
        final String cellarFolderBatchJobSparqlQueries = this.cellarConfiguration.getCellarFolderBatchJobSparqlQueries();
        final Query[] result = SparqlUtils.getQueries(cellarFolderBatchJobSparqlQueries);
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getQuery(final String query) throws IOException {
        final String cellarFolderBatchJobSparqlQueries = this.cellarConfiguration.getCellarFolderBatchJobSparqlQueries();
        final String result = SparqlUtils.getQuery(cellarFolderBatchJobSparqlQueries, query);
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public List<BatchJob> findBatchJobs(final BATCH_JOB_TYPE jobType) {
        return this.batchJobDao.findBatchJobsByType(jobType);
    }

    /** {@inheritDoc} */
    @Override
    @Transactional
    public List<BatchJob> findBatchJobsByStatus(final BatchJob.STATUS status) {
        return this.batchJobDao.findBatchJobsByStatus(status);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public List<String> findUpdateBatchJobCrons() {
        return this.batchJobDao.findUpdateBatchJobCrons();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public List<BatchJobWorkIdentifier> findWorkIdentifiersByBatchJobId(final Long batchJobId, final BATCH_JOB_TYPE jobType) {
        return this.batchJobWorkIdentifierDao.findWorkIdentifiers(batchJobId, jobType);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public BatchJob getBatchJob(final Long id, final BATCH_JOB_TYPE jobType) {
        return this.batchJobDao.findBatchJobByIdType(id, jobType);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public CellarPaginatedList<BatchJobWorkIdentifier> findWorkIdentifiers(final Long batchJobId, final BATCH_JOB_TYPE jobType,
            final CellarPaginatedList<BatchJobWorkIdentifier> workIdentifiersList) {
        return this.batchJobWorkIdentifierDao.findWorkIdentifiers(batchJobId, jobType, workIdentifiersList);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void deleteBatchJob(final Long batchJobId) {
        final BatchJob batchJob = this.batchJobDao.getObject(batchJobId);
        if (batchJob != null) {
            this.batchJobDao.deleteObject(batchJob);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void startBatchJob(final Long batchJobId) {
        final BatchJob batchJob = this.batchJobDao.getObject(batchJobId);
        if (batchJob != null) {
            batchJob.setJobStatus(STATUS.WAITING_PROCESSING);
            this.batchJobDao.updateObject(batchJob);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void restartBatchJob(final Long batchJobId) {
        final BatchJob batchJob = this.batchJobDao.getObject(batchJobId);
        if (batchJob != null) {
            if (BATCH_JOB_TYPE.UPDATE.equals(batchJob.getJobType())) {
                batchJob.setJobStatus(STATUS.AUTO);
            } else {
                batchJob.setJobStatus(STATUS.NEW);
            }
            batchJob.setErrorDescription(null);
            batchJob.setErrorStacktrace(null);
            this.batchJobDao.updateObject(batchJob);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void addBatchJob(final BatchJob batchJob) {
        this.batchJobDao.saveObject(batchJob);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void addBatchJobWorkIdentifiers(final BatchJob batchJob, final HashSet<String> workIds) {
        this.batchJobWorkIdentifierDao.deleteWorkIdentifiers(batchJob.getId(), batchJob.getJobType());

        for (final String workId : workIds) {
            this.batchJobWorkIdentifierDao.saveOrUpdateObject(new BatchJobWorkIdentifier(workId, batchJob));
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public List<BatchJob> findByStatusAndCron(final STATUS status, final String cron) {
        return this.batchJobDao.findBatchJobsByStatusCron(status, cron);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> findWorkIds(final Long batchJobId, final BATCH_JOB_TYPE jobType, final int first, final int last) {
        return this.batchJobWorkIdentifierDao.findWorkIds(batchJobId, jobType, first, last);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.NESTED)
    public void updateErrorBatchJob(final BatchJob batchJob, final Throwable error) {
        batchJob.setErrorDescription(error.getLocalizedMessage());
        batchJob.setErrorStacktrace(ErrorUtils.getHtmlStacktrace(error));
        batchJob.setJobStatus(STATUS.ERROR);
        this.batchJobDao.updateObject(batchJob);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.NESTED)
    public void updateErrorBatchJob(final BatchJob batchJob, final String errorDescription, final String errorStackTrace) {
        batchJob.setErrorDescription(errorDescription);
        batchJob.setErrorStacktrace(errorStackTrace);
        batchJob.setJobStatus(STATUS.ERROR);
        this.batchJobDao.updateObject(batchJob);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.NESTED)
    public void updateReadyBatchJob(final BatchJob batchJob, final HashSet<String> workIds) {
        this.batchJobWorkIdentifierDao.deleteWorkIdentifiers(batchJob.getId(), batchJob.getJobType());
        for (final String workId : workIds) {
            this.batchJobWorkIdentifierDao.saveOrUpdateObject(new BatchJobWorkIdentifier(workId, batchJob));
        }
        batchJob.setNumberItems((long) workIds.size());
        batchJob.setJobStatus(STATUS.READY);
        this.batchJobDao.updateObject(batchJob);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.NESTED)
    public void updateDoneBatchJob(final BatchJob batchJob) {
        batchJob.setJobStatus(STATUS.DONE);
        this.batchJobDao.updateObject(batchJob);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.NESTED)
    public void updateProcessingBatchJob(final BatchJob batchJob) {
        batchJob.setJobStatus(STATUS.PROCESSING);
        this.batchJobDao.updateObject(batchJob);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.NESTED)
    public void updateExecutingSparqlBatchJob(final BatchJob batchJob) {
        batchJob.setJobStatus(STATUS.EXECUTING_SPARQL);
        this.batchJobDao.updateObject(batchJob);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void updateBatchJob(final BatchJob batchJob) {
        this.batchJobDao.updateObject(batchJob);
    }
}
