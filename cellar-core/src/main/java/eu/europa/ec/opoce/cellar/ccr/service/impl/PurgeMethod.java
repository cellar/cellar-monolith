/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ccr.service.impl
 *             FILE : PurgeMethod.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 19 Apr 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.service.impl;

import org.apache.commons.httpclient.HttpMethodBase;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 19 Apr 2017
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class PurgeMethod extends HttpMethodBase {

    /**
     * Instantiates a new purge method.
     */
    public PurgeMethod() {
        super();
        setFollowRedirects(true);
    }

    /**
     * Instantiates a new purge method.
     *
     * @param url the url
     */
    public PurgeMethod(String url) {
        super(url);
        setFollowRedirects(true);
    }

    /** {@inheritDoc} */
    @Override
    public String getName() {
        return "PURGE";
    }
}
