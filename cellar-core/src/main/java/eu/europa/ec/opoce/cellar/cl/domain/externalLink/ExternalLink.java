/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.externalLink
 *             FILE : ExternalLink.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 16, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.externalLink;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <class_description> Class that represents an external link generated.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 16, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlType(name = "mappingType")
@XmlRootElement(name = "mapping")
public class ExternalLink {

    /**
     * The external link generated.
     */
    private String url;

    /**
     * Constructor.
     */
    public ExternalLink() {
    }

    /**
     * Constructor.
     * @param url the external link generated
     */
    public ExternalLink(final String url) {
        this.url = url;
    }

    /**
     * Gets the external link generated.
     * @return the external link generated
     */
    @XmlElement(name = "url")
    public String getUrl() {
        return this.url;
    }

    /**
     * Sets the external link generated.
     * @param url the external link to set
     */
    public void setUrl(final String url) {
        this.url = url;
    }
}
