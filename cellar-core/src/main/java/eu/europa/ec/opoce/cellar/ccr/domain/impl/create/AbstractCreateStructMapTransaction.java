/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.ccr.domain.impl.create
 *        FILE : AbstractCreateStructMapTransaction.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 08-05-2013
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.domain.impl.create;

import eu.europa.ec.opoce.cellar.ccr.domain.impl.TransactionConfiguration;
import eu.europa.ec.opoce.cellar.ccr.domain.impl.WriteStructMapTransaction;
import eu.europa.ec.opoce.cellar.ccr.helper.ResponseHelper;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.cl.domain.response.CreateStructMapOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.ResponseOperationType;
import eu.europa.ec.opoce.cellar.cl.domain.response.StructMapOperation;
import eu.europa.ec.opoce.cellar.cmr.MetaDataIngestionService;
import eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsPackage;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.exception.MetadataValidationResultAwareException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

/**
 * <class_description> This class provides methods for the create/append process of a StructMap into database and S3.
 * Every digital objects (with their contents and metadatas) under a given StructMap will be created or appended in database and S3.
 * If an error occurs during this process, the entire process is rolled back, in order that all digital objects (with metadata and content)
 * will be removed from the database and S3.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 08-05-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public abstract class AbstractCreateStructMapTransaction extends WriteStructMapTransaction<String> {

    /**
     * The Constant LOGGER.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractCreateStructMapTransaction.class);

    /**
     * The meta data ingestion service.
     */
    @Autowired
    private MetaDataIngestionService<CalculatedData> metaDataIngestionService;

    /**
     * Instantiates a new abstract create struct map transaction.
     *
     * @param configuration the configuration
     */
    protected AbstractCreateStructMapTransaction(final TransactionConfiguration configuration) {
        super(configuration);
    }

    /**
     * Execute write treatment.
     *
     * @param metsPackage    the mets package
     * @param calculatedData the calculated data
     * @param metsDirectory  the mets directory
     * @param sipResource    the sip resource
     * @return the struct map operation
     * @see eu.europa.ec.opoce.cellar.ccr.domain.impl.WriteStructMapTransaction#executeWriteTreatment(eu.europa.ec.opoce.cellar.domain.content.mets.MetsPackage, eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData, java.io.File, eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource)
     */
    @Override
    public StructMapOperation executeWriteTreatment(final MetsPackage metsPackage, final CalculatedData calculatedData,
                                                    final File metsDirectory, final SIPResource sipResource) {
        // the operation
        final StructMapOperation operation = this.newStructMapOperation();

        // list of newly ingested digital objects
        final List<String> ingestedDigitalObjectPidList = new LinkedList<String>();

        // if true, the rollback is to be performed silently, that is, by returning a successful result anyway
        final StructMap structMap = calculatedData.getStructMap();
        boolean rollbackSilently = false;
        try {
            // 1. manage CELLAR database and digital objects' hierarchy on CCR
            this.processHierarchy(calculatedData, metsDirectory, sipResource, ingestedDigitalObjectPidList);

            // 2. manage metadata on CCR and CMR
            this.metaDataIngestionService.ingest(calculatedData, operation);
        } catch (final MetadataValidationResultAwareException mvre) {
            this.rollback(operation, calculatedData, mvre, ingestedDigitalObjectPidList);
            throw mvre;
        } catch (final Exception exception) {
            // rollback the entire process for this structmap
            // throw an exception only if the exception that originally caused the rollback is not silent
            this.rollback(operation, calculatedData, exception, ingestedDigitalObjectPidList);
            // this indicates that a silent exception has been caught: it has to be rolled back afterwards (see further)
            rollbackSilently = true;
        }

        StructMapOperation structMapOperation = new CreateStructMapOperation();
        structMapOperation.setId(structMap.getId());
        try {
            structMap.getDigitalObject().applyStrategyOnSelfAndChildren(digitalObject -> structMapOperation.addOperation(ResponseHelper.fillCreateOperation(digitalObject)));
        } catch (final Exception e) {
            structMapOperation.setOperationType(ResponseOperationType.ERROR);
            LOGGER.error("An exception occurred during creation of structMapOperation for structMap [" + structMap.getId()
                    + " of metsDocument [" + structMap.getMetsDocument().getDocumentId() + "].", e);
        }

        // if a silent exception has been caught, a rollback is needed anyway
        // example: when a duplicated production id was found, but the operation was set to silently ignore it
        if (rollbackSilently) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
        return structMapOperation;
    }

    /**
     * Process hierarchy.
     *
     * @param calculatedData               the calculated data
     * @param metsDirectory                the mets directory
     * @param sipResource                  the sip resource
     * @param ingestedDigitalObjectPidList the ingested digital object pid list
     */
    protected abstract void processHierarchy(final CalculatedData calculatedData, final File metsDirectory, final SIPResource sipResource,
                                             final List<String> ingestedDigitalObjectPidList);

    /**
     * New struct map operation.
     *
     * @return the struct map operation
     */
    protected abstract StructMapOperation newStructMapOperation();

}
