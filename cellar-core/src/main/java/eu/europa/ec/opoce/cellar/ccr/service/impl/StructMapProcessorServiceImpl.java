/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ccr.service.impl
 *             FILE : StructMapProcessorServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 25-06-2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.service.impl;

import eu.europa.ec.opoce.cellar.ccr.service.client.IStructMapProcessorService;
import eu.europa.ec.opoce.cellar.ccr.service.client.MetsUploadService;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.cl.domain.enums.ProcessMonitorPhase;
import eu.europa.ec.opoce.cellar.cl.domain.enums.ProcessStatus;
import eu.europa.ec.opoce.cellar.cl.domain.response.CreateStructMapOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.ResponseOperationType;
import eu.europa.ec.opoce.cellar.cl.domain.response.StructMapOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.StructMapResponse;
import eu.europa.ec.opoce.cellar.cl.domain.structmap_status_history.StructMapStatusHistory;
import eu.europa.ec.opoce.cellar.cl.service.client.BackupService;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cl.service.client.PackageHistoryService;
import eu.europa.ec.opoce.cellar.cl.service.client.StructMapStatusHistoryService;
import eu.europa.ec.opoce.cellar.cmr.model.ModelUtils;
import eu.europa.ec.opoce.cellar.common.service.TestingThreadService;
import eu.europa.ec.opoce.cellar.domain.content.mets.*;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.virtuoso.VirtuosoService;
import eu.europa.ec.opoce.cellar.virtuoso.util.VirtuosoUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import virtuoso.jena.driver.VirtDataset;

import java.io.File;
import java.io.StringWriter;
import java.util.List;

import javax.sql.DataSource;

import static eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration.VIRTUOSO;

/**
 * @author ARHS Developments
 */
@Service
public class StructMapProcessorServiceImpl implements IStructMapProcessorService {

    private static final Logger LOG = LogManager.getLogger(StructMapProcessorServiceImpl.class);

    @Autowired
    private BackupService backupService;

    @Autowired
    private MetsServiceFactory metsServiceFactory;

    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    @Autowired
    private VirtuosoService virtuosoService;

    @Autowired
    @Qualifier("pidManagerService")
    private IdentifierService identifierService;
    
    @Autowired
    private PackageHistoryService packageHistoryService;
    
    @Autowired
    private StructMapStatusHistoryService structMapStatusHistoryService;
    
    @Autowired
    private TestingThreadService testingThreadService;
    
    /**
     * The virtuoso data source.
     */
    @Qualifier("virtuosoDataSource")
    @Autowired
    private DataSource virtuosoDataSource;

    @Autowired
    private MetsUploadService metsUploadService;

    /**
     * Process the mets document (create/append/update/read) according to the given transaction.
     *
     * @param metsPackage the mets package
     * @param metsDocument the mets document to process
     * @param sipResource needed to audit the method
     * @return the StructMap response
     */
    @Override
    public StructMapResponse processMetsDocument(final MetsPackage metsPackage, final MetsDocument metsDocument,
            final SIPResource sipResource) {
        LOG.info("Processing {}", metsDocument);

        final StructMapResponse structMapResponse = this.internalProcessMetsDocument(metsPackage, metsDocument, sipResource);
        this.verifyStructMapResponse(structMapResponse);

        try {
            this.metsServiceFactory.getStructMapResponseDao(sipResource).save(structMapResponse);
            final MetsDocument responseMetsDocument = this.metsServiceFactory.getMetsResponseService(sipResource)
                    .loadStructMapResponse(metsDocument, sipResource);
            final boolean error = isError(structMapResponse);
            // Send callback request if a callback UEI exists in PACKAGE_HISTORY entry (only for ingestions triggered by METS Upload)
            metsUploadService.getStatusResponseAndSendCallbackRequest(metsPackage.getZipFile(), error, sipResource.getPackageHistory());
            this.metsServiceFactory.getMetsResponseService(sipResource).createResponse(responseMetsDocument, error);
        } catch (final Exception e) {
            LOG.error("An exception occured while saving response for metsdocument [" + metsDocument.getDocumentId() + "].", e);
        }
        return structMapResponse;
    }

    /**
     * Process the mets document (create/append/update/read) according to the given transaction.
     *
     * @param metsPackage the mets package
     * @param metsDocument the mets document to process
     * @param sipResource needed to audit the method
     * @return the StructMap response
     */
    private StructMapResponse internalProcessMetsDocument(final MetsPackage metsPackage, final MetsDocument metsDocument,
            final SIPResource sipResource) {
        final MetsPackage toUseMetsPackage = new ResolvedMetsPackage(metsPackage);
        final String metsDocumentId = metsDocument.getDocumentId();
        final StructMapResponse response = new StructMapResponse();
        response.setMetsDocumentId(metsDocumentId);
        final File metsDirectory = new File(this.cellarConfiguration.getCellarFolderTemporaryWork(), sipResource.getFolderName());
        
        // gets the connection to the dataset
        VirtDataset dataset = null;
        try {
        	// The dataset is needed only if the Virtuoso log level is enabled in order to read virtuoso graphs and write some debug level logs
	        if(LOG.isDebugEnabled(VIRTUOSO)) {
	        	dataset = new VirtDataset(virtuosoDataSource);
	        }
            // Load all StructMapStatusHistory entries for this package
            List<StructMapStatusHistory> structMapStatusHistoryList = this.structMapStatusHistoryService.getByPackageHistory(sipResource.getPackageHistory());
            
	        for (final StructMap structMap : metsDocument.getStructMaps().values()) {
	        	final String structMapId = structMap.getId();
	        	ThreadContext.put("structMap", structMapId);
                
                StructMapStatusHistory structMapStatusHistory = null;
	        	StructMapOperation operation = new CreateStructMapOperation();
	        	try {
                    // Pick the corresponding StructMapStatusHistory entry from the list above
                    structMapStatusHistory = structMapStatusHistoryList.stream()
                            .filter(ssh -> ssh.getStructMapName().equalsIgnoreCase(structMapId))
                            .findFirst()
                            .orElse(null);
                    // Set Ingestion status of STRUCTMAP_STATUS_HISTORY entry to RUNNING
                    if(structMapStatusHistory != null){
                        structMapStatusHistory = this.structMapStatusHistoryService.updateStructMapIngestionStatus(structMapStatusHistory, ProcessStatus.R);
                    }
                    
                    // Check whether to pause thread execution for testing purposes
                    if (this.cellarConfiguration.isTestEnabled()
                            && this.cellarConfiguration.getTestProcessMonitorPhase().equals(ProcessMonitorPhase.Ingestion_StructMapProcessing)) {
                        this.testingThreadService.checkpointFreezingControl(ProcessMonitorPhase.Ingestion_StructMapProcessing);
                    }
                    
	        		// execute the treatment of the package
	        		final StructMapTransaction<?> structMapTransaction = structMap.resolveOperationSubType().getStructMapTransaction();
	        		operation = (StructMapOperation) structMapTransaction.executeTreatment(toUseMetsPackage, structMap, metsDirectory,
	        				sipResource, structMapStatusHistory);
		
	        		if(LOG.isDebugEnabled(VIRTUOSO)) {
	        			virtuosoCheck(operation, dataset);
		                }
		            } catch (final CellarException e) {
		                operation.setOperationType(ResponseOperationType.ERROR);
		            } finally {
		                // set the result of the operation into the struct map operation's array of the response
		                response.getStructMapOperations().put(structMapId, operation);
		            }
		        }
	        } finally {
	        	LOG.debug(VIRTUOSO, "Close Virtuoso dataset (triples count={})", (dataset != null ? dataset.getCount() : 0));
	        	VirtuosoUtils.close(dataset);
	        }
        this.backupService.backupFoxmlFolder(sipResource.getFolderName());
        
        // Check whether to pause thread execution for testing purposes
        if (this.cellarConfiguration.isTestEnabled()
                && this.cellarConfiguration.getTestProcessMonitorPhase().equals(ProcessMonitorPhase.Ingestion_Complete)) {
            this.testingThreadService.checkpointFreezingControl(ProcessMonitorPhase.Ingestion_Complete);
        }

        return response;
    }

    private void virtuosoCheck(final StructMapOperation operation, VirtDataset dataset) {
        operation.getOperations().keySet().forEach(cellarId -> {
            final String cellarUri = identifierService.getUri(cellarId);
            Model model = null;
            try {
                LOG.debug(VIRTUOSO, "Ingested '{}'. Checking if the graph is present in Virtuoso", cellarUri);
                model = this.virtuosoService.read(cellarUri, dataset);
                LOG.debug(VIRTUOSO, "Graph '{}' is {}", cellarUri, model.isEmpty() ? "not present" : "present");

                final StringWriter sw = new StringWriter();
                model.write(sw);
                LOG.debug(VIRTUOSO, sw.toString());
            } finally {
                ModelUtils.closeQuietly(model);
            }
        });
    }

    /**
     * Verify that all StructMapOperation of this StructMapResponse does not contain the type ERROR.
     * If we get a SMO with an error type, we move the mets to the erro folder.
     *
     * @param response the response
     */
    private void verifyStructMapResponse(final StructMapResponse response) {
        for (final StructMapOperation smo : response.getStructMapOperations().values()) {
            if (ResponseOperationType.ERROR.equals(smo.getOperationType())) {
                // during the creation at least one structmap of this sip caused an exception
                // we move the mets to the error folder
                this.backupService.errorMetsFolder(response.getMetsDocumentId());
                break;
            }
        }
    }

    /**
     * Check the state of all {@link StructMapResponse} containning in the given response.
     * If one of them has his type set to ERROR, the method return true, false otherwise.
     * @param response the response to check.
     * @return true if one {@link StructMapResponse}'s type equals ERROR, false otherwise.
     */
    private static boolean isError(final StructMapResponse response) {
        for (final StructMapOperation smo : response.getStructMapOperations().values()) {
            if (ResponseOperationType.ERROR.equals(smo.getOperationType())) {
                return true;
            }
        }
        return false;
    }
}
