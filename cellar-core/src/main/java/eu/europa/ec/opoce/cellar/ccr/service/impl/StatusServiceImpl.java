package eu.europa.ec.opoce.cellar.ccr.service.impl;

import eu.europa.ec.opoce.cellar.ccr.enums.status.StatusType;
import eu.europa.ec.opoce.cellar.ccr.service.client.MetsUploadService;
import eu.europa.ec.opoce.cellar.ccr.service.client.StatusService;
import eu.europa.ec.opoce.cellar.ccr.utils.StatusServiceUtils;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;

import static eu.europa.ec.opoce.cellar.ccr.utils.StatusServiceUtils.StatusServiceConstants.*;

/**
 * Implementation of the {@link MetsUploadService} interface.
 *
 * @author EUROPEAN DYNAMICS S.A.
 */
@Service
public class StatusServiceImpl implements StatusService {
    private ICellarConfiguration cellarConfiguration;
    private RestTemplate restTemplate;


    public StatusServiceImpl(@Qualifier("cellarConfiguration") ICellarConfiguration cellarConfiguration) {
        this.cellarConfiguration = cellarConfiguration;
        this.restTemplate = new RestTemplate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getStatusResponse(String id, boolean ingestion, boolean indexation, LocalDateTime datetime, int verbosity, StatusType statusType) {
        if (statusType == StatusType.PACKAGE_LEVEL) {
            return this.getPackageLevelStatusResponse(id, ingestion, indexation, verbosity);
        }

        if (statusType == StatusType.OBJECT_LEVEL) {
            return this.getObjectLevelStatusResponse(id, ingestion, indexation, datetime, verbosity);
        }

        if (statusType == StatusType.LOOKUP) {
            return this.getLookupStatusResponse(id);
        }

        throw new RuntimeException("Unsupported StatusType: " + statusType);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getStatusServicePackageLevelEndpointWithUuid(String uuid) {
        return this.cellarConfiguration.getCellarStatusServiceBaseUrl() + this.cellarConfiguration.getCellarStatusServicePackageLevelEndpoint()
                + "?id=" + uuid;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getStatusServiceObjectLevelEndpointWithPid(String pid) {
        return this.cellarConfiguration.getCellarStatusServiceBaseUrl() + this.cellarConfiguration.getCellarStatusServiceObjectLevelEndpoint()
                + "?id=" + pid;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getStatusServiceLookupEndpointWithPackageName(String packageName) {
        return this.cellarConfiguration.getCellarStatusServiceBaseUrl() + this.cellarConfiguration.getCellarStatusServiceLookupEndpoint()
                + "?id=" + packageName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarProxiedStatusServicePackageLevelEndpointWithUuid(String uuid) {
        return StatusServiceUtils.buildBaseUrl() + ADMIN + CELLAR_STATUS_V1_BASE_URL + CELLAR_STATUS_PACKAGE_ENDPOINT
                + "?id=" + uuid;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarProxiedStatusServiceObjectLevelEndpointWithPid(String pid) {
        return StatusServiceUtils.buildBaseUrl() + ADMIN + CELLAR_STATUS_V1_BASE_URL + CELLAR_STATUS_OBJECT_ENDPOINT
                + "?id=" + pid;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getCellarProxiedStatusServiceLookupEndpointWithPackageName(String packageName) {
        return StatusServiceUtils.buildBaseUrl() + ADMIN + CELLAR_STATUS_V1_BASE_URL + CELLAR_STATUS_LOOKUP_ENDPOINT
                + "?id=" + packageName;
    }

    /**
     * {@inheritDoc}
     */
    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    private String getPackageLevelStatusResponse(String id, boolean ingestion, boolean indexation, int verbosity) {
        String statusServiceUrl = this.constructStatusServiceURI(id, ingestion, indexation, null, verbosity, StatusType.PACKAGE_LEVEL);

        return restTemplate.getForEntity(statusServiceUrl, String.class).getBody();
    }

    private String getObjectLevelStatusResponse(String id, boolean ingestion, boolean indexation, LocalDateTime datetime, int verbosity) {
        String statusServiceUrl = this.constructStatusServiceURI(id, ingestion, indexation, datetime, verbosity, StatusType.OBJECT_LEVEL);

        return restTemplate.getForEntity(statusServiceUrl, String.class).getBody();
    }

    private String getLookupStatusResponse(String id) {
        String statusServiceUrl = this.constructStatusServiceURI(id, false, false, null, -1, StatusType.LOOKUP);

        return restTemplate.getForEntity(statusServiceUrl, String.class).getBody();
    }

    private String constructStatusServiceURI(String id, boolean ingestion, boolean indexation, LocalDateTime datetime, int verbosity, StatusType statusType) {
        StringBuilder sb = new StringBuilder();

        if (statusType == StatusType.PACKAGE_LEVEL) {
            sb.append(this.getStatusServicePackageLevelEndpointWithUuid(id));
        }

        if (statusType == StatusType.OBJECT_LEVEL) {
            sb.append(this.getStatusServiceObjectLevelEndpointWithPid(id));
        }

        if (statusType == StatusType.LOOKUP) {
            sb.append(this.getStatusServiceLookupEndpointWithPackageName(id));
        }

        if (statusType != StatusType.LOOKUP) {
            sb.append("&ingestion=").append(ingestion);
            sb.append("&indexation=").append(indexation);
            sb.append("&verbosity=").append(verbosity);
        }
        sb.append(datetime != null ? "&datetime=" + datetime : "");

        return sb.toString();
    }
}
