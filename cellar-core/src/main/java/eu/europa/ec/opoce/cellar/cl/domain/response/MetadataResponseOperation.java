package eu.europa.ec.opoce.cellar.cl.domain.response;

public class MetadataResponseOperation extends ResponseOperation {

    private static final long serialVersionUID = 8326607516293707543L;

    private String datastreamName;

    public MetadataResponseOperation() {
    }

    public MetadataResponseOperation(final String cellarId, final String datastreamName, final ResponseOperationType operationType) {
        super(cellarId, operationType);
        this.datastreamName = datastreamName;
    }

    public String getDatastreamName() {
        return datastreamName;
    }

    public void setDatastreamName(String datastreamName) {
        this.datastreamName = datastreamName;
    }

}
