package eu.europa.ec.opoce.cellar.cl.service.client;

import java.io.IOException;
import java.io.InputStream;

/**
 * Interface of the Content Store.
 * This service manage content on a store.
 * 
 * @author dcraeye
 *
 */
public interface ContentStore {

    /**
     * Store the content using the storename.
     * @param content the content to store
     * @param storeName the name used for storing this content.
     * @throws IOException if an exception occurs during the saving.
     */
    void storeContent(InputStream content, String storeName) throws IOException;

    /**
     * Retrieve the content corresponding to the given storedName.
     * @param storedName the name used to find the content.
     * 
     * @return an input stream to the finded content or null if not found.
     */
    InputStream getContent(String storedName);

}
