package eu.europa.ec.opoce.cellar.cl.service;

import eu.europa.ec.opoce.cellar.cl.configuration.impl.CellarPersistentConfiguration;
import eu.europa.ec.opoce.cellar.common.concurrent.ListeningExecutorService;
import eu.europa.ec.opoce.cellar.common.concurrent.NamedThreadFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static eu.europa.ec.opoce.cellar.common.concurrent.MoreExecutors.newFixedListeningPriorityExecutor;

/**
 * @author ARHS Developments
 */
@Configuration
public class IngestionConfiguration {

    @Bean("ingestionRequestExecutor")
    public ListeningExecutorService ingestionRequestExecutor(@Qualifier("cellarConfiguration") CellarPersistentConfiguration cellarConfiguration) {
        return newFixedListeningPriorityExecutor(cellarConfiguration.getCellarServiceIngestionPoolThreads(),
                new NamedThreadFactory("ingestion"));
    }
}
