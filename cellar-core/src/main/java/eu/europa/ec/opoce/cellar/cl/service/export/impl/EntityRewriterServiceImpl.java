/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.export.impl
 *             FILE : EntityRewriterServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 15, 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2016-09-14 16:59:33 +0200 (Wed, 14 Sep 2016) $
 *          VERSION : $LastChangedRevision: 11689 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2016 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.export.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.PrefixConfigurationService;
import eu.europa.ec.opoce.cellar.cl.service.export.IEntityRewriterService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Map;

/**
 * <class_description> Service able to rewrite metadata using entities.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 15, 2016
 *
 * @author ARHS Developments
 * @version $Revision: 11689 $
 */
@Service
public class EntityRewriterServiceImpl implements IEntityRewriterService {

    private static final String ENTITY_TEMPLATE = "&resource;%s/";

    @Autowired
    @Qualifier("systemAgnosticHeader")
    private String systemAgnosticHeader;

    @Autowired
    private PrefixConfigurationService prefixConfigurationService;

    private String[] uris;
    private String[] entities;

    @PostConstruct
    private void init() {
        final Map<String, String> prefixUris = this.prefixConfigurationService.getPrefixUris();
        this.uris = new String[prefixUris.size()];
        this.entities = new String[prefixUris.size()];

        int i = 0;
        for (Map.Entry<String, String> entry : prefixUris.entrySet()) {

            this.uris[i] = entry.getValue();
            this.entities[i] = String.format(ENTITY_TEMPLATE, entry.getKey());

            i++;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String rewriteMetadataWithEntity(final String metadataAsString) {
        return this.systemAgnosticHeader + StringUtils.replaceEach(metadataAsString, this.uris, this.entities);
    }
}
