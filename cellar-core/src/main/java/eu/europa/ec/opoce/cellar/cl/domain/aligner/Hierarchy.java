/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.aligner
 *             FILE : Hierarchy.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : May 20, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.aligner;

import eu.europa.ec.opoce.cellar.cl.enums.aligner.Repository;
import eu.europa.ec.opoce.cellar.common.util.CellarIdUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : May 20, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class Hierarchy {

    /** The root. */
    private HierarchyNode root;

    /** The cellar id. */
    private final String cellarId;

    /** The nodes. */
    private final Map<String, HierarchyNode> nodes;

    /** The embargo date. */
    private Date embargoDate;

    /**
     * Instantiates a new hierarchy.
     *
     * @param cellarId the cellar id
     */
    public Hierarchy(final String cellarId) {
        this.cellarId = cellarId;
        this.nodes = new HashMap<String, HierarchyNode>();
    }

    /**
     * Gets the root.
     *
     * @return the root
     */
    public HierarchyNode getRoot() {
        return this.root;
    }

    /**
     * Gets the cellar id.
     *
     * @return the cellarId
     */
    public String getCellarId() {
        return this.cellarId;
    }

    /**
     * Checks if is aligned.
     *
     * @return true, if is aligned
     */
    public boolean isAligned() {
        boolean isAligned = true;

        for (final HierarchyNode n : this.nodes.values()) {
            isAligned &= n.isAligned();
        }

        return isAligned;
    }

    /**
     * Gets the evicted hierarchy nodes.
     *
     * @return the evicted hierarchy nodes
     */
    public Map<Repository, List<HierarchyNode>> getEvictedHierarchyNodes() {
        final Map<Repository, List<HierarchyNode>> evictedCellarIds = new HashMap<Repository, List<HierarchyNode>>();
        if (this.root != null) {
            this.root.addEvictedHierarchyNodes(evictedCellarIds);
        }

        return evictedCellarIds;
    }

    /**
     * Gets the hierarchy node instance.
     *
     * @param cellarId the cellar id
     * @param type the type
     * @return the hierarchy node instance
     */
    public HierarchyNode getHierarchyNodeInstance(final String cellarId, final DigitalObjectType type) {
        HierarchyNode node = this.getHierarchyNodeOrNull(cellarId);

        if (node != null) {
            if (type != null) {
                node.setType(type);
            }
            return node;
        }

        node = new HierarchyNode(cellarId, type);
        // this is needed because of the peculiar way that S3 has in identifying content streams
        if (node.hasProbableType(DigitalObjectType.ITEM)) {
            node.setType(DigitalObjectType.ITEM);
        }

        final DigitalObjectType nodeType = node.getType();
        if (nodeType.isTopLevel()) {
            node.setEmbargoDate(this.getEmbargoDate());
            this.root = node;
        } else {
            final HierarchyNode parent = this.getHierarchyNodeInstance(CellarIdUtils.getParentCellarId(cellarId), nodeType.getParentType());
            parent.addChild(node);
            node.setParent(parent);
        }

        this.addHierarchyNode(node);

        return node;
    }

    /**
     * Gets the hierarchy node or null.
     *
     * @param cellarId the cellar id
     * @return the hierarchy node or null
     */
    public HierarchyNode getHierarchyNodeOrNull(final String cellarId) {
        return this.nodes.get(cellarId);
    }

    /**
     * Adds the hierarchy node.
     *
     * @param hierarchyNode the hierarchy node
     */
    private void addHierarchyNode(final HierarchyNode hierarchyNode) {
        this.nodes.put(hierarchyNode.getCellarId().getIdentifier(), hierarchyNode);
    }

    /**
     * Gets the embargo date.
     *
     * @return the embargo date
     */
    public Date getEmbargoDate() {
        return this.embargoDate;
    }

    /**
     * Sets the embargo date.
     *
     * @param embargoDate the new embargo date
     */
    public void setEmbargoDate(final Date embargoDate) {
        this.embargoDate = embargoDate;
    }

    /**
     * Gets the nodes.
     *
     * @return the nodes
     */
    public Map<String, HierarchyNode> getNodes() {
        return this.nodes;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return StringUtils.join(this.nodes.values(), ";");
    }
}
