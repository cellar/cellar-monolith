/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : BatchJobProcessingRunable.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 16 juin 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.batchJob.manager;

import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.BatchJobService;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor.IBatchJobProcessor;
import eu.europa.ec.opoce.cellar.common.factory.IFactory;

/**
 * The Class BatchJobProcessingRunable.
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 16 juin 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class BatchJobProcessingRunable extends BatchJobRunable {

    /**
     * Instantiates a new batch job processing runable.
     *
     * @param batchJob the batch job
     * @param batchJobService the batch job service
     * @param batchJobProcessorFactory the batch job processor factory
     */
    public BatchJobProcessingRunable(final BatchJob batchJob, final BatchJobService batchJobService,
            final IFactory<IBatchJobProcessor, BatchJob> batchJobProcessorFactory) {
        super(batchJob, batchJobService, batchJobProcessorFactory);
    }

    /** {@inheritDoc} */
    @Override
    public void run() {
        try {
            final IBatchJobProcessor batchJobProcessor = batchJobProcessorFactory.create(batchJob);
            batchJobProcessor.execute();
        } catch (final Exception exception) {
            batchJobService.updateErrorBatchJob(batchJob, exception);
        }

    }
}
