/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor.export
 *             FILE : MetadataMetsElementExport.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 24, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.export.exporter.impl;

import com.google.common.base.Strings;
import eu.europa.ec.opoce.cellar.cl.service.export.IEntityRewriterService;
import eu.europa.ec.opoce.cellar.cl.service.export.exporter.IMetsElementExporter;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.HierarchyElement;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement;
import eu.europa.ec.opoce.cellar.cmr.model.ModelUtils;
import eu.europa.ec.opoce.cellar.common.factory.IFactory;
import eu.europa.ec.opoce.cellar.common.util.CellarIdUtils;
import eu.europa.ec.opoce.cellar.common.util.IOUtils;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.MdSecType;
import eu.europa.ec.opoce.cellar.mets.v2.builder.*;
import eu.europa.ec.opoce.cellar.mets.v2.builder.enums.LocType;
import eu.europa.ec.opoce.cellar.mets.v2.builder.enums.MdType;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static eu.europa.ec.opoce.cellar.cmr.ContentType.DIRECT;
import static eu.europa.ec.opoce.cellar.cmr.ContentType.TECHNICAL;

/**
 * @author ARHS Developments
 */
@Configurable
public class MetadataMetsElementExporter extends AbstractMetsElementExporter {

    private static final Logger LOGGER = LoggerFactory.getLogger(MetadataMetsElementExporter.class);

    @Autowired
    @Qualifier("metsElementExporterFactory")
    private IFactory<IMetsElementExporter, MetsElement> metsElementExporterFactory;

    @Autowired
    private IEntityRewriterService entityRewriterService;

    private final DivBuilder divBuilder;

    public MetadataMetsElementExporter(MetsElement element) {
        super(element);
        this.divBuilder = DivBuilder.get();
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void doExportMetsElement(final MetsBuilder metsBuilder, final IDivFptrAware<?> parentDivFptrAware,
                                       final MetsElement metsElement, final ZipOutputStream zos) throws IOException {
        final Map<ContentType, String> models = getModels();
        final String directModel = models.get(DIRECT);
        final String tmdModel = models.get(TECHNICAL);

        this.exportMetsElement(metsBuilder, parentDivFptrAware, metsElement, zos, directModel, tmdModel);

        // export children
        for (final MetsElement child : ((HierarchyElement<MetsElement, MetsElement>) metsElement).getChildren()) {
            LOGGER.info("Export the child identified byt the cellar id '{}'.", child.getCellarId().getIdentifier());
            this.metsElementExporterFactory.create(child).exportMetsElement(metsBuilder, this.divBuilder, child, zos);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void doExportMetsElement(final Model model, final MetsBuilder metsBuilder, final IDivFptrAware<?> parentDivFptrAware,
                                       final MetsElement metsElement, final ZipOutputStream zos) throws IOException {
        final Map<ContentType, String> models = getModels();
        final Set<String> uris = metsElement.getAllUris();
        final List<String> pids = this.identifierService.getIdentifier(getCellarId()).getPids();
        for (final String pid : pids) {
            uris.add(this.identifierService.getUri(pid));
        }

        Model dmd = null;
        String dmdModel;
        try {
            dmd = this.snippetExtractorService.getSnippetWithoutRecursivity(model, uris);
            dmdModel = JenaUtils.toString(dmd);
        } finally {
            ModelUtils.closeQuietly(dmd);
        }

        this.exportMetsElement(metsBuilder, parentDivFptrAware, metsElement, zos, dmdModel, models.get(TECHNICAL));

        // export children
        for (final MetsElement child : ((HierarchyElement<MetsElement, MetsElement>) metsElement).getChildren()) {
            LOGGER.info("Export the child identified byt the cellar id '{}'.", child.getCellarId().getIdentifier());
            this.metsElementExporterFactory.create(child).exportMetsElement(model, metsBuilder, this.divBuilder, child, zos);
        }
    }

    private Map<ContentType, String> getModels() {
        // TODO remove TECHNICAL in the next line when the versions will be set by the lambda
        Map<ContentType, String> versions = ContentType.extract(getVersions(), DIRECT, TECHNICAL);
        if (getVersions().containsKey(ContentType.TECHNICAL)) {
            versions.put(ContentType.TECHNICAL, getVersions().get(ContentType.TECHNICAL));
        }
        // Retrieve the content streams and translate them from NT to RDF_XML for backward compatibility
        return contentStreamService.getContentsAsString(getCellarId(), versions)
                .entrySet().stream()
                .filter(m -> !Strings.isNullOrEmpty(m.getValue()))
                .collect(Collectors.toMap(Map.Entry::getKey, e -> JenaUtils.translate(e.getValue(), RDFFormat.NT, RDFFormat.RDFXML_PLAIN)));
    }

    private void exportMetsElement(final MetsBuilder metsBuilder, final IDivFptrAware<?> parentDivFptrAware, final MetsElement metsElement,
                                   final ZipOutputStream zos, final String directModel, @Nullable final String tmdModel)
            throws IOException {
        this.divBuilder.withTYPE(metsElement.getType());

        // compute metadata IDs
        final String mdId = getMdId();
        final String mdFilename = getMdFilename(mdId);

        LOGGER.info("Add the metadata file '{}' in the package.", mdFilename);
        String dmdReplaced = directModel;
        if (metsBuilder.getUseAgnosticURL()) {
            dmdReplaced = this.entityRewriterService.rewriteMetadataWithEntity(directModel);
        }

        String relativeFilePathFromRootDirectory = CellarIdUtils.createPathFromCellarId(this.getCellarId());
        String relativeMdFileName = mdFilename;
        if (StringUtils.isNotBlank(relativeFilePathFromRootDirectory))
            relativeMdFileName = relativeFilePathFromRootDirectory + "/" + mdFilename;
        zos.putNextEntry(new ZipEntry(relativeMdFileName));

        final String dmdChecksum = IOUtils.copyAndDigestHex(dmdReplaced, zos, IOUtils.SHA_256);

        final MdSecType mdSec = MdSecBuilder.get().withID(mdId)
                .withMdRef(MdRefBuilder.get()
                        .withHref(relativeMdFileName)
                        .withLOCTYPE(LocType.URL)
                        .withMDTYPE(MdType.OTHER)
                        .withOTHERMDTYPE("INSTANCE")
                        .withMIMETYPE("application/rdf+xml")
                        .withCHECKSUMTYPE(IOUtils.SHA_256)
                        .withCHECKSUM(dmdChecksum)
                        .build())
                .build();

        this.divBuilder.withDMDID(mdSec);
        metsBuilder.withDmdSec(mdSec);

        if (tmdModel != null) { // when technical metadata exists
            // compute technical metadata IDs
            final String techMdId = getTechMdId();
            final String techMdFilename = getTechMdFilename(techMdId);

            LOGGER.info("Add the metadata file '{}' in the package.", techMdFilename);
            String relativeTechMdFileName = techMdFilename;
            if (StringUtils.isNotBlank(relativeFilePathFromRootDirectory))
                relativeTechMdFileName = relativeFilePathFromRootDirectory + "/" + techMdFilename;
            zos.putNextEntry(new ZipEntry(relativeTechMdFileName));

            String modelString = tmdModel;
            if (metsBuilder.getUseAgnosticURL()) {
                modelString = this.entityRewriterService.rewriteMetadataWithEntity(tmdModel);
            }
            final String tmdChecksum = IOUtils.copyAndDigestHex(modelString, zos, IOUtils.SHA_256);

            final MdSecType amdSec = MdSecBuilder.get().withID(techMdId)
                    .withMdRef(MdRefBuilder.get().withHref(relativeTechMdFileName)
                            .withLOCTYPE(LocType.URL)
                            .withMDTYPE(MdType.OTHER)
                            .withOTHERMDTYPE("INSTANCE")
                            .withMIMETYPE("application/rdf+xml")
                            .withCHECKSUMTYPE(IOUtils.SHA_256)
                            .withCHECKSUM(tmdChecksum).build())
                    .build();

            this.divBuilder.withADMID(amdSec);
            metsBuilder.withAmdSec(AmdSecBuilder.get().withID(getAmdId()).withMdRef(amdSec).build());
        }

        parentDivFptrAware.withDiv(this.divBuilder.build());

    }

    @SuppressWarnings("unchecked")
    @Override
    protected void doCreateModel(final Model model, final MetsElement metsElement) {
        final String directModel = contentStreamService.getContentAsString(getCellarId(), getVersions().get(DIRECT), DIRECT)
                .orElseThrow(() -> new IllegalStateException("Direct metadata not found " + getCellarId() + "(" + getVersions().get(DIRECT)));
        model.add(JenaUtils.read(directModel, Lang.NT));

        // Add children in model
        for (final MetsElement child : ((HierarchyElement<MetsElement, MetsElement>) metsElement).getChildren()) {
            LOGGER.info("Add the child identified by the cellar id '{}' in the model.", child.getCellarId().getIdentifier());
            this.metsElementExporterFactory.create(child).createModel(model, child);
        }
    }

    @Override
    protected IContentIdAware<?> getContentIdAware() {
        return this.divBuilder;
    }

    private String getMdId() {
        return CellarIdUtils.getCellarIdAsFilename("md_" + getCellarId());
    }

    private String getMdFilename(final String mdId) {
        return mdId + ".rdf";
    }

    private String getTechMdId() {
        return CellarIdUtils.getCellarIdAsFilename("techMd_" + getCellarId());
    }

    private String getTechMdFilename(final String techMdId) {
        return techMdId + ".rdf";
    }

    private String getAmdId() {
        return CellarIdUtils.getCellarIdAsFilename("amd_" + getCellarId());
    }
}
