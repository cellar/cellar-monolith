/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : StructMapResponse.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 20 juin 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.response;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Response generated at the end of the workflow to give the status.
 *
 * @author dcraeye
 *
 */
public class StructMapResponse implements Serializable {

    /** class's uid. */
    private static final long serialVersionUID = 8326607516293707547L;

    /** The mets document id. */
    private String metsDocumentId;

    /** The struct map operations. */
    private Map<String, StructMapOperation> structMapOperations = new HashMap<String, StructMapOperation>();

    /**
     * Instantiates a new struct map response.
     */
    public StructMapResponse() {
    }

    /**
     * Gets the mets document id.
     *
     * @return the mets document id
     */
    public String getMetsDocumentId() {
        return metsDocumentId;
    }

    /**
     * Sets the mets document id.
     *
     * @param metsDocumentId the new mets document id
     */
    public void setMetsDocumentId(final String metsDocumentId) {
        this.metsDocumentId = metsDocumentId;
    }

    /**
     * Gets the struct map operations.
     *
     * @return the struct map operations
     */
    public Map<String, StructMapOperation> getStructMapOperations() {
        return structMapOperations;
    }

    /**
     * Gets the struct map operation.
     *
     * @param id the id
     * @return the struct map operation
     */
    public StructMapOperation getStructMapOperation(final String id) {
        return structMapOperations.get(id);
    }

    /**
     * Sets the struct map operations.
     *
     * @param structMapOperations the struct map operations
     */
    public void setStructMapOperations(final Map<String, StructMapOperation> structMapOperations) {
        this.structMapOperations = structMapOperations;
    }

}
