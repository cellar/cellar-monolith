package eu.europa.ec.opoce.cellar.cl.domain.validator;

import org.springframework.beans.factory.annotation.Autowired;

import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.cl.dao.ProductionIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.domain.admin.ProductionIdentifier;
import eu.europa.ec.opoce.cellar.cl.exception.ValidationException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

/**
 * Validates this production identifier already exists.
 * @author dcraeye
 *
 */
public class ProductionIdentifierValidator implements SystemValidator {

    private @Autowired ProductionIdentifierDao productionIdentifierDao;

    @Override
    public ValidationResult validate(ValidationDetails validationDetails) {
        ValidationResult result = new ValidationResult();

        if (validationDetails.getToValidate() == null) {
            throw ExceptionBuilder.get(ValidationException.class).withCode(CoreErrors.E_070).withMessage("object cannot be null")
                    .withCause(new NullPointerException()).build();
        }
        if (!(validationDetails.getToValidate() instanceof ProductionIdentifier)) {
            throw ExceptionBuilder.get(ValidationException.class).withCode(CoreErrors.E_073)
                    .withMessage(validationDetails.getToValidate().getClass().toString()).build();
        }

        result.setValid(productionIdentifierDao
                .getProductionIdentifier(((ProductionIdentifier) validationDetails.getToValidate()).getProductionId()) != null);

        return result;
    }
}
