/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *             FILE : WorkflowServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 30 juin 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.ccr.service.FileTypesNalSkosLoaderService;
import eu.europa.ec.opoce.cellar.ccr.service.client.IStructMapProcessorService;
import eu.europa.ec.opoce.cellar.ccr.strategy.InboundTransformationStrategy;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditBuilder;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventAction;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventProcess;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventType;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPObject;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.cl.domain.enums.ProcessMonitorPhase;
import eu.europa.ec.opoce.cellar.cl.domain.enums.ProcessStatus;
import eu.europa.ec.opoce.cellar.cl.domain.validator.ValidationDetails;
import eu.europa.ec.opoce.cellar.cl.domain.validator.ValidationResult;
import eu.europa.ec.opoce.cellar.cl.service.client.*;
import eu.europa.ec.opoce.cellar.common.service.TestingThreadService;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsDocument;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.exception.CellarConversionException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.monitoring.Watch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Arrays;

/**
 * Service to instantiate the workflow.
 * <p>
 * For the create, update or reading process of a METS, the process are the same :
 * 1. Fill a SIPObject with information coming from the given mets (xml).
 * During this step, apply some METS validation.
 * 2. Apply custom validations.
 * 3. Construct a MetsDocument. (data model of the mets)
 * 4. Apply custom transformations.
 * 5. Start the MetsDocument's processing depending the mets's TYPE.
 * 6. Audit the response
 * 7. Archive the mets.
 *
 * @author dcraeye
 */
@Service
public class WorkflowServiceImpl implements WorkflowService {

    private static final Logger LOG = LogManager.getLogger(WorkflowServiceImpl.class);

    @Autowired
    private SIPLoader sipLoader;

    @Autowired
    private MetsProcessorService metsProcessorService;

    @Autowired
    private IStructMapProcessorService structMapProcessorService;

    @Autowired
    private ValidationService validationService;

    @Autowired
    private FileTypesNalSkosLoaderService fileTypesNalSkosLoaderService;

    @Autowired
    private FileService fileService;
    
    @Autowired
    private PackageHistoryService packageHistoryService;
    
    @Autowired
    private TestingThreadService testingThreadService;

    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    /**
     * Process the creation of this mets with all step defined above.
     *
     * @param sipResource contains the location of the mets file to process.
     */
    @Override
    public void createMets(final SIPResource sipResource) {
        this.ingestMets(sipResource);
    }

    /**
     * Process the update of this mets with all step defined above.
     *
     * @param sipResource contains the location of the mets file to process.
     */
    @Override
    public void updateMets(final SIPResource sipResource) {
        this.ingestMets(sipResource);
    }

    /**
     * Process the read of this mets with all step defined above.
     *
     * @param sipResource contains the location of the mets file to process.
     */
    @Watch(value = "METS Read", arguments = {
            @Watch.WatchArgument(name = "Sip resource mets ID", expression = "sipResource.metsID")
    })
    @Override
    public void readMets(final SIPResource sipResource) {
        final SIPObject sipObject = this.sipLoader.loadSIPObject(sipResource);
        final MetsDocument metsDocument = this.metsProcessorService.convert(sipObject);
        this.structMapProcessorService.processMetsDocument(sipObject, metsDocument, sipResource);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteMets(final SIPResource sipResource) {
        this.ingestMets(sipResource);
    }

    /**
     * Ingest mets.
     *
     * @param sipResource the sip resource
     */
    private void ingestMets(final SIPResource sipResource) {
        //To be used as timer
        long startTime = System.currentTimeMillis();
        // construct SIP Object
        final SIPObject sipObject = this.sipLoader.loadSIPObject(sipResource);
        // Log the start of process
        AuditBuilder.get(AuditTrailEventProcess.Ingestion).withAction(AuditTrailEventAction.Convert)
                .withType(AuditTrailEventType.Start).withObject(sipObject.getZipFile() != null ? sipObject.getZipFile().getName() : sipResource.getMetsID() + ".zip")
                .withPackageHistory(sipResource.getPackageHistory())
                .withLogger(LOG).withLogDatabase(true).logEvent();
        
        // Check whether to pause thread execution for testing purposes
        if (this.cellarConfiguration.isTestEnabled()
                && this.cellarConfiguration.getTestProcessMonitorPhase().equals(ProcessMonitorPhase.Ingestion_Convert)) {
            this.testingThreadService.checkpointFreezingControl(ProcessMonitorPhase.Ingestion_Convert);
        }
        
        // custom validate
        this.customValidate(sipObject, sipResource);

        // convert to METS document
        final MetsDocument metsDocument = this.convert(sipObject);

        // custom transform
        this.customTransform(sipObject, sipResource, metsDocument);
        
        // Log the end of process
        AuditBuilder.get(AuditTrailEventProcess.Ingestion).withAction(AuditTrailEventAction.Convert)
                .withType(AuditTrailEventType.End).withDuration(System.currentTimeMillis() - startTime)
                .withObject(sipObject.getZipFile() != null ? sipObject.getZipFile().getName() : sipResource.getMetsID() + ".zip")
                .withPackageHistory(sipResource.getPackageHistory())
                .withLogger(LOG).withLogDatabase(true).logEvent();

        // Update status of PackageHistory entry to SUCCESS
        if (sipResource.getPackageHistory() != null) {
            sipResource.setPackageHistory(
                    this.packageHistoryService.updatePackageHistoryStatus(sipResource.getPackageHistory(), ProcessStatus.S)
            );
        }

        // ingest
        this.structMapProcessorService.processMetsDocument(sipObject, metsDocument, sipResource);
    }
    
    /**
     * This method apply the custom validation strategy (if any).
     *
     * @param sipObject   to find all files of the mets
     * @param sipResource contains the location of the mets file to process
     */
    private void customValidate(final SIPObject sipObject, final SIPResource sipResource) {
        final ValidationDetails validationParameters = new ValidationDetails();
        validationParameters.setToValidate(sipObject);
        validationParameters.setSipResource(sipResource);

        final ValidationResult result = this.validationService.validate(validationParameters);
        if (!result.isValid()) {
            throw ExceptionBuilder.get(CellarConversionException.class).withCode(CoreErrors.E_000).root(true)
                    .withMessage("A problem occurred while custom validating the SIP with METS id [{}].")
                    .withMessageArgs(sipResource.getMetsID()).build();
        }
    }

    /**
     * This method converts the SIP object into a METS document.
     *
     * @param sipObject the SIP object to convert
     * @return the METS document
     */
    private MetsDocument convert(final SIPObject sipObject) throws CellarConversionException {
        // convert Mets Data Model
        MetsDocument metsDocument;
        try {
            metsDocument = this.metsProcessorService.convert(sipObject);
        } catch (final Exception e) {
            throw ExceptionBuilder.get(CellarConversionException.class).withCode(CoreErrors.E_000).withCause(e).root(true)
                    .withMessage("A problem occurred while converting the SIP object.").build();
        }
        return metsDocument;
    }

    /**
     * This method apply the custom transformation strategy (if any).
     *
     * @param sipObject    to find all files of the mets
     * @param sipResource  contains the location of the mets file to process
     * @param metsDocument the data model of the mets
     */
    private void customTransform(final SIPObject sipObject, final SIPResource sipResource, final MetsDocument metsDocument) {
        // Load in the sipObject all files found in the mets folder of this mets.
        // New transformed files must be added to the list that has been loader during the call of sipLoader.loadSIPObject
        try {
            for (final StructMap structMap : metsDocument.getStructMaps().values()) {
                structMap.getDigitalObject().applyStrategyOnSelfAndChildren(new InboundTransformationStrategy(sipResource, this.cellarConfiguration));
            }
            final File[] files = this.fileService.getAllFilesFromUncompressedLocation(sipResource.getFolderName());
            sipObject.setFileList(Arrays.asList(files));
        } catch (final Exception e) {
            throw ExceptionBuilder.get(CellarConversionException.class).withCode(CoreErrors.E_000).withCause(e).root(true)
                    .withMessage("A problem occurred while custom transforming the SIP with METS id [{}].")
                    .withMessageArgs(sipResource.getMetsID()).build();
        }
    }

}
