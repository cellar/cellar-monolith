/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.LicenseHolder
 *             FILE : ExtractionConfiguration.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 5, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.licenseHolder;

import java.util.Calendar;
import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;

/**
 * <class_description> Class that represents the informations about an extraction configuration.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 5, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Entity
@Table(name = "EXTRACTION_CONFIGURATION", uniqueConstraints = @UniqueConstraint(columnNames = {
        "PATH"}) )
public class ExtractionConfiguration {

    /**
     * 
     * <class_description> The extraction configuration type.
     * <br/><br/>
     * <notes> Includes guaranteed invariants, usage instructions and/or examples,
     *         reminders about desired improvements, etc.
     * <br/><br/>
     * ON : Nov 26, 2012
     *
     * @author ARHS Developments
     * @version $Revision$
     */
    public static enum CONFIGURATION_TYPE {
        /**
         * Ad-hoc extraction
         */
        STD("ADHOC"),

        /**
         * Daily extraction
         */
        STDD("DAILY"),

        /**
         * Monthly extraction
         */
        STDM("MONTHLY"),

        /**
         * Weekly extraction
         */
        STDW("WEEKLY");

        private final String definition;

        /**
         * Constructor.
         */
        private CONFIGURATION_TYPE(final String definition) {
            this.definition = definition;
        }

        /**
         * Gets the type definition.
         * @return the type definition
         */
        public String getDefinition() {
            return this.definition;
        }
    };

    /** 
     * Internal id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    /**
     * The extraction configuration name.
     */
    @Column(name = "CONFIGURATION_NAME", nullable = false)
    private String configurationName;

    /**
     * The extraction configuration type.
     */
    @Enumerated(EnumType.STRING)
    @Column(name = "CONFIGURATION_TYPE", nullable = false)
    private CONFIGURATION_TYPE configurationType;

    /**
     * The language of the extraction (decoding and content language).
     */
    @Column(name = "EXTRACTION_LANGUAGE", nullable = false)
    private String extractionLanguage;

    /**
     * The relative file system path in which the archive will be generated.
     */
    @Column(name = "PATH", nullable = false)
    private String path;

    /**
     * The creation date of the extraction.
     */
    @Column(name = "CREATION_DATE", nullable = false)
    private Date creationDate;

    /**
     * The start date of the first extraction execution.
     */
    @Column(name = "START_DATE")
    private Date startDate;

    /**
     * The SPARQL query to find the works.
     */
    @Column(name = "SPARQL_QUERY", nullable = false)
    private String sparqlQuery;

    /**
     * The extraction executions corresponding to this configuration.
     */
    @OneToMany(mappedBy = "extractionConfiguration", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<ExtractionExecution> extractionExecutions;

    /**
     * Language bean corresponding to the extraction language.
     */
    @Transient
    private LanguageBean languageBean;

    /**
     * Constructor.
     */
    public ExtractionConfiguration() {

    }

    /**
     * Constructor.
     * @param configurationName the name of the configuration
     * @param configurationType the type of the configuration
     * @param extractionLanguage the extraction language
     * @param path the destination path
     * @param startDate the start date of the configuration
     * @param sparqlQuery the SPARQL query of the configuration
     */
    public ExtractionConfiguration(final String configurationName, final CONFIGURATION_TYPE configurationType,
            final String extractionLanguage, final String path, final Date startDate, final String sparqlQuery) {
        this.configurationName = configurationName;
        this.configurationType = configurationType;
        this.extractionLanguage = extractionLanguage;
        this.path = path;
        this.creationDate = new Date(); // now
        this.startDate = startDate;
        this.sparqlQuery = sparqlQuery;
    }

    /**
     * Gets the value of the id.
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the value of the id.
     * @param id the id to set
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the configuration name.
     * @return the job name.
     */
    public String getConfigurationName() {
        return this.configurationName;
    }

    /**
     * Sets the configuration name.
     * @param configurationName the configuration name to set
     */
    public void setConfigurationName(final String configurationName) {
        this.configurationName = configurationName;
    }

    /**
     * Gets the configuration type.
     * @return the configuration type.
     */
    public CONFIGURATION_TYPE getConfigurationType() {
        return this.configurationType;
    }

    /**
     * Sets the configuration type.
     * @param configurationType the configuration type to set
     */
    public void setConfigurationType(final CONFIGURATION_TYPE configurationType) {
        this.configurationType = configurationType;
    }

    /**
     * Gets the extraction language.
     * @return the extraction language
     */
    public String getExtractionLanguage() {
        return this.extractionLanguage;
    }

    /**
     * Sets the extraction language.
     * @param extractionLanguage the extraction language to set
     */
    public void setExtractionLanguage(final String extractionLanguage) {
        this.extractionLanguage = extractionLanguage;
    }

    /**
     * Gets the destination path.
     * @return the destination path
     */
    public String getPath() {
        return this.path;
    }

    /**
     * Sets the destination path.
     * @param path the destination path to set.
     */
    public void setPath(final String path) {
        this.path = path;
    }

    /**
     * Gets the creation date of the configuration.
     * @return the creation date of the configuration.
     */
    public Date getCreationDate() {
        return this.creationDate;
    }

    /**
     * Sets the creation date of the configuration.
     * @param creationDate the creation date to set.
     */
    public void setCreationDate(final Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * Gets the start date of the job.
     * @return the start date of the job.
     */
    public Date getStartDate() {
        return this.startDate;
    }

    /**
     * Sets the start date of the job.
     * @param startDate the start date of the job to set.
     */
    public void setStartDate(final Date startDate) {
        this.startDate = startDate;
    }

    /**
     * Gets the SPARQL query to find the works.
     * @return the SPARQL query.
     */
    public String getSparqlQuery() {
        return this.sparqlQuery;
    }

    /**
     * Sets the SPARQL query to find the works.
     * @param sparqlQuery the SPARQL query to set.
     */
    public void setSparqlQuery(final String sparqlQuery) {
        this.sparqlQuery = sparqlQuery;
    }

    /**
     * Gets the executions of this configuration.
     * @return the executions of this configuration
     */
    public Set<ExtractionExecution> getExtractionExecutions() {
        return this.extractionExecutions;
    }

    /**
     * Sets the executions of this configuration.
     * @param extractionExecutions the executions to set.
     */
    public void setExtractionExecutions(final Set<ExtractionExecution> extractionExecutions) {
        this.extractionExecutions = extractionExecutions;
    }

    /**
     * Gets the language bean corresponding to the extraction language.
     * @return the language bean corresponding to the extraction language
     */
    public LanguageBean getLanguageBean() {
        return this.languageBean;
    }

    /**
     * Sets the language bean corresponding to the extraction language.
     * @return the language bean to set
     */
    public void setLanguageBean(final LanguageBean languageBean) {
        this.languageBean = languageBean;
    }

    /**
     * Generates the first execution for the configuration.
     * @return the generated execution
     */
    public ExtractionExecution getExtractionExecution() {
        Date startDate = null;
        Date endDate = null;

        if (this.configurationType != CONFIGURATION_TYPE.STD) { // if it isn't an ad-hoc extraction
            startDate = this.startDate;

            final Calendar startCalendar = Calendar.getInstance();
            startCalendar.clear();
            startCalendar.setFirstDayOfWeek(Calendar.MONDAY);

            if (startDate != null)
                startCalendar.setTime(startDate); // the start date indicated in the configuration
            else
                startCalendar.setTime(new Date()); // now

            final Calendar endCalendar = Calendar.getInstance();
            endCalendar.clear();
            endCalendar.setFirstDayOfWeek(Calendar.MONDAY);
            endCalendar.set(Calendar.YEAR, startCalendar.get(Calendar.YEAR));

            if (startDate == null)
                startDate = endCalendar.getTime(); // starts now

            switch (this.configurationType) {
            case STDD: // adds a day
                endCalendar.set(Calendar.MONTH, startCalendar.get(Calendar.MONTH));
                endCalendar.set(Calendar.DATE, startCalendar.get(Calendar.DATE));
                endCalendar.add(Calendar.DATE, 1);
                break;
            case STDW: // adds a week
                endCalendar.set(Calendar.WEEK_OF_YEAR, startCalendar.get(Calendar.WEEK_OF_YEAR));
                endCalendar.add(Calendar.DATE, 7);
                break;
            case STDM: // adds a month
                endCalendar.set(Calendar.MONTH, startCalendar.get(Calendar.MONTH));
                endCalendar.add(Calendar.MONTH, 1);
                break;
			// should never get here!
            default:
				break;
            }
            endDate = endCalendar.getTime();
        } else
            // ad-hoc extraction
            endDate = this.startDate;

        return new ExtractionExecution(startDate, endDate, this);
    }
}
