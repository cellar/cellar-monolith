/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.support.cache
 *        FILE : EmbeddedNoticeCacheConfiguration.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 03-01-2023
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2023 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.support.cache;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.ehcache.EhCacheFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import net.sf.ehcache.CacheManager;

/**
 * Provides definition and configuration for the embedded notice cache.
 * @author EUROPEAN DYNAMICS S.A
 */
@Configuration
public class EmbeddedNoticeCacheConfiguration {

	@Bean
	public EhCacheFactoryBean embeddedNoticeCache(@Qualifier("cellarStaticConfiguration") ICellarConfiguration cellarConfiguration, @Qualifier("realCacheManager") CacheManager cacheManager) {
		EhCacheFactoryBean embeddedNoticeCacheFactory = new EhCacheFactoryBean();
		embeddedNoticeCacheFactory.setCacheManager(cacheManager);
		embeddedNoticeCacheFactory.setMaxEntriesLocalHeap(0);
		embeddedNoticeCacheFactory.setMaxBytesLocalHeap(cellarConfiguration.getCellarServiceEmbeddedNoticeCacheMaxBytesLocalHeap());
		embeddedNoticeCacheFactory.setTimeToLiveSeconds(cellarConfiguration.getCellarServiceEmbeddedNoticeCacheTimeToLiveSeconds());
		embeddedNoticeCacheFactory.setEternal(false);
		embeddedNoticeCacheFactory.setOverflowToDisk(true);
		embeddedNoticeCacheFactory.setTimeToIdleSeconds(0);
		embeddedNoticeCacheFactory.setMaxEntriesLocalDisk(0);
		embeddedNoticeCacheFactory.setMaxBytesLocalDisk(cellarConfiguration.getCellarServiceEmbeddedNoticeCacheMaxBytesLocalDisk());
		embeddedNoticeCacheFactory.setDisabled(cellarConfiguration.isCellarServiceIngestionEnabled());
		embeddedNoticeCacheFactory.setStatistics(cellarConfiguration.isCellarServiceEmbeddedNoticeCacheEvaluationStatisticsEnabled());
		
		return embeddedNoticeCacheFactory;
	}
	
}
