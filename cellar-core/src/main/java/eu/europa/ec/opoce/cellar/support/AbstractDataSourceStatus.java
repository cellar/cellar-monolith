/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.support
 *        FILE : AbstractDataSourceStatus.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 14-05-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.support;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.status.IDataSourceStatus;
import eu.europa.ec.opoce.cellar.cmr.database.util.JdbcUtils;
import eu.europa.ec.opoce.cellar.exception.CellarConfigurationException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * <class_description> This is an abstract class used for checking whether a particular data source is online or not.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 14-05-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
@Component
public abstract class AbstractDataSourceStatus implements IDataSourceStatus {

    /** class's logger. */
    @SuppressWarnings("unused")
    private transient static final Logger LOGGER = LoggerFactory.getLogger(AbstractDataSourceStatus.class);

    /** The cellar configuration. */
    @Autowired
    @Qualifier("cellarConfiguration")
    protected ICellarConfiguration cellarConfiguration;

    /**
     * Check.
     *
     * @throws CellarConfigurationException the cellar configuration exception
     * @see eu.europa.ec.opoce.cellar.cl.status.IServiceStatus#check()
     */
    @Override
    public void check() throws CellarConfigurationException {
        try {
            this.checkDataSource(this.getDataSourceName());
        } catch (final Exception e) {
            throw ExceptionBuilder.get(CellarConfigurationException.class).withCode(CommonErrors.SERVICE_NOT_AVAILABLE)
                    .withMessage("Data source '{}' is NOT available.").withMessageArgs(this.getDataSourceName()).withCause(e).build();
        }
    }

    /**
     * Gets the URL to the database connection.
     * 
     * @return the URL of the connection to the database
     * @throws CellarConfigurationException the cellar configuration exception
     */
    @Override
    public String getDatabaseUrl() throws CellarConfigurationException {
        String databaseConnectionUrl = null;

        try {
            final DataSource dataSource = JdbcUtils.resolveDataSource(this.getDataSourceName());
            databaseConnectionUrl = JdbcUtils.getDatabaseUrl(dataSource);
        } catch (final Exception e) {
            throw ExceptionBuilder.get(CellarConfigurationException.class).withCode(CommonErrors.SERVICE_NOT_AVAILABLE)
                    .withMessage("Data source '{}' is NOT available.").withMessageArgs(this.getDataSourceName()).withCause(e).build();
        }

        return databaseConnectionUrl;
    }

    /**
     * Gets the data source name.
     *
     * @return the data source name
     */
    protected abstract String getDataSourceName();

    /**
     * Gets the verification query.
     *
     * @return the verification query
     */
    protected abstract String getVerificationQuery();

    /**
     * Check data source.
     *
     * @param dataSourceName the data source name
     * @throws SQLException the SQL exception
     * @throws NamingException the naming exception
     */
    private void checkDataSource(final String dataSourceName) throws SQLException, NamingException {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rset = null;
        try {
            final DataSource dataSource = JdbcUtils.resolveDataSource(dataSourceName);
            conn = dataSource.getConnection();
            stmt = conn.createStatement();
            final String verificationQuery = this.getVerificationQuery();
            rset = stmt.executeQuery(verificationQuery);
        } finally {
            JdbcUtils.closeResultSet(rset);
            JdbcUtils.closeStatement(stmt);
            JdbcUtils.closeConnection(conn);
        }
    }

}
