package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.IS3Configuration;
import eu.europa.ec.opoce.cellar.cl.dao.CellarIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.dao.ProductionIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.admin.ProductionIdentifier;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierResolver;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Implementation of the {@link IdentifierResolver}.
 * 
 * @author phvdveld
 * 
 */
@Component
public class IdentifierResolverImpl implements IdentifierResolver {

    /**
     * Cellar namespace.
     */
    private String cellarNamespace;

    /** database access for {@link ProductionIdentifier}. */
    private @Autowired ProductionIdentifierDao productionIdentifierDao;

    /** database access for {@link CellarIdentifier}. */
    private @Autowired CellarIdentifierDao cellarIdentifierDao;

    /**
     * S3 configuration holder.
     */
    @Autowired
    @Qualifier("cellarConfiguration")
    private IS3Configuration s3Configuration;

    @PostConstruct
    public void initialize() {
        this.cellarNamespace = this.s3Configuration.getCellarNamespace();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CellarIdentifier resolveToCellarId(final String id) {
        CellarIdentifier cellarId;
        if (this.isCellarId(id)) {
            cellarId = this.cellarIdentifierDao.getCellarIdentifier(id);
        } else {
            final ProductionIdentifier pid = this.productionIdentifierDao.getProductionIdentifier(id);
            if (pid == null) {
                throw new IllegalArgumentException("PID " + id + " not found.");
            }
            cellarId = pid.getCellarIdentifier();
        }

        if (cellarId == null) {
            throw new IllegalArgumentException("Id " + id + " is neither a cellar id nor a production id");
        }
        return cellarId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CellarIdentifier resolveToCellarId(final String namespace, final String id) {
        String idWithNamespace;
        if (this.hasNamespace(id)) {
            idWithNamespace = id;
        } else {
            idWithNamespace = namespace + ":" + id;
        }
        return this.resolveToCellarId(idWithNamespace);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDatastreamId(final String UUID, final String fileRef) {
        String result = null;
        final CellarIdentifier id = this.cellarIdentifierDao.getCellarIdentifier(UUID, fileRef);
        if (id != null) {
            final String[] split = id.getUuid().split("/");
            if (split.length > 1) {
                final String datastreamName = split[1];
                if (ContentType.isCellarContentType(datastreamName)) {
                    result = datastreamName;
                }
            }
        }
        return result;
    }

    /**
     * Indicates whether the provided string has a namespace or not.
     * 
     * @param str
     *            the string to test
     * @return true if the string has a namespace, false otherwise
     */
    private boolean hasNamespace(final String str) {
        final String[] tokens = str.split(":");
        if (tokens.length == 1) {
            return false;
        } else if (tokens.length == 2) {
            return true;
        } else {
            throw new IllegalArgumentException("identifier is not correctly formatted");
        }
    }

    /**
     * Indicates whether the provided string is a cellar id or not.
     * 
     * @param str
     *            the string to test
     * @return true if the string is a cellar id, false otherwise
     */
    private boolean isCellarId(final String str) {
        return str.startsWith(this.cellarNamespace);
    }

}
