/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.dao.impl
 *             FILE : UserDaoImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 10, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import eu.europa.ec.opoce.cellar.cl.dao.UserDao;
import eu.europa.ec.opoce.cellar.cl.domain.admin.User;

/**
 * <class_description> Implementation of the interface {@link User}.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 10, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Repository
public class UserDaoImpl extends BaseDaoImpl<User, Long> implements UserDao {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public User findUserByUsername(String username) {
		Map<String, Object> params = new HashMap<>();
		params.put("username", username);
		return findObjectByNamedQueryWithNamedParams("findUserByUsername", params);
	}

	
	
}
