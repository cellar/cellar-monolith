package eu.europa.ec.opoce.cellar.cl.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import eu.europa.ec.opoce.cellar.cl.dao.ValidatorRuleDao;
import eu.europa.ec.opoce.cellar.cl.domain.admin.ValidatorRule;

/**
 * Implementation of the {@link ValidatorRuleDao}.
 *
 * @author dcraeye
 *
 */
@Repository
public class ValidatorRuleDaoImpl extends BaseDaoImpl<ValidatorRule, Long> implements ValidatorRuleDao {

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ValidatorRule> getRules() {
        return super.findAll();
    }

    /**
     * {@inheritDoc}
     */

    @Override
    public synchronized void createRule(final ValidatorRule rule) {
        //This method is synchronized to be sure to not create two validator rule for the same validator

        //Verify before create object that a other thread has not already create this validator rule.
        if (getRuleByValidatorClass(rule.getValidatorClass()) == null) {
            super.saveObject(rule);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ValidatorRule getRule(final Long id) {
        return super.getObject(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ValidatorRule getRuleByValidatorClass(final String validatorClass) {
        return super.findObjectByNamedQuery("getByValidatorClass", validatorClass);
    }

    @Override
    public List<ValidatorRule> getSystemValidators() {
        return super.findObjectsByNamedQuery("getSystemValidators");
    }

    @Override
    public List<ValidatorRule> getCustomValidators() {
        return super.findObjectsByNamedQuery("getCustomValidators");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateRule(final ValidatorRule rule) {
        super.updateObject(rule);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteRule(final ValidatorRule rule) {
        super.deleteObject(rule);
    }

}
