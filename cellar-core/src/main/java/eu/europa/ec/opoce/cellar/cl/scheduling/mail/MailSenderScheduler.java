/* ----------------------------------------------------------------------
 *          PROJECT : EUR-Lex
 *
 *          PACKAGE : eu.europa.ec.op.eurlex.frontoffice.service.messaging
 *             FILE : IMailSenderJob.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 20 Jan 2011
 *
 *      MODIFIED BY: ARHS Developments
 *               ON: $LastChangedDate: 2010-09-02 09:35:22 +0200 (Thu, 02 Sep 2010) 2011-09-27 11:59:43 +0200 (jeu., 27 sept. 2011) $
 *          VERSION: $LastChangedRevision: 293 $
 *
 * ----------------------------------------------------------------------
 * Copyright (c) 2011 European Commission - Publications Office
 * ----------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.scheduling.mail;


import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.service.mail.MailSenderService;
import eu.europa.ec.opoce.cellar.common.spring.CloseableConcurrentTaskScheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class MailSenderScheduler {
    private final ICellarConfiguration cellarConfiguration;
    private final MailSenderService mailSenderService;

    @Autowired
    public MailSenderScheduler(MailSenderService mailSenderService, @Qualifier("cellarStaticConfiguration") ICellarConfiguration cellarConfiguration) {
        this.cellarConfiguration = cellarConfiguration;
        this.mailSenderService = mailSenderService;
    }

    @PostConstruct
    public void init() {
        if (cellarConfiguration.getCellarServiceEmailEnabled()) {
            CloseableConcurrentTaskScheduler.newScheduler("mail-sending")
                    .schedule(new MailSenderRunnable(mailSenderService, cellarConfiguration),
                    new CronTrigger(this.cellarConfiguration.getCellarEmailJobCron()));
        }
    }

}
