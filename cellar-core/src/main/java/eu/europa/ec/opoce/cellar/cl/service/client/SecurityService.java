/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.client
 *             FILE : SecurityService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 11, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.client;

import java.util.List;

import eu.europa.ec.opoce.cellar.cl.domain.admin.Group;
import eu.europa.ec.opoce.cellar.cl.domain.admin.Role;
import eu.europa.ec.opoce.cellar.cl.domain.admin.User;

/**
 * <class_description> Security manager.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 11, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface SecurityService {

    /**
     * Finds all the {@link Group} instances.
     * @return the {@link Group} instances
     */
    List<Group> findAllGroups();

    /**
     * Finds the {@link User} instance corresponding to the identifier.
     * @param id the identifier of the user
     * @return the {@link User} instance
     */
    User findUser(final Long id);

    /**
     * Finds the {@link User} instance corresponding to the provided username.
     * @param username the username of the user
     * @return the {@link User} instance
     */
    User findUserByUsername(final String username);
    
    /**
     * Finds the {@link Group} instance corresponding to the identifier.
     * @param id the identifier of the group
     * @return the {@link Group} instance
     */
    Group findGroup(final Long id);

    /**
     * Creates a new user.
     * @param user the new user to create
     */
    void createUser(final User user);

    /**
     * Updates a user.
     * @param user the user to update
     */
    void updateUser(final User user);

    /**
     * Finds all the {@link Role} instances.
     * @return the {@link Role} instances
     */
    List<Role> findAllRoles();

    /**
     * Creates a new group.
     * @param group the new group to create
     */
    void createGroup(final Group group);

    /**
     * Updates a group.
     * @param group the group to update
     */
    void updateGroup(final Group group);

    /**
     * Finds the {@link Role} instance corresponding to the identifier.
     * @param id the identifier of the role
     * @return the {@link Role} instance
     */
    Role findRole(final Long id);

    /**
     * Finds all the {@link User} instances.
     * @return the {@link User} instances
     */
    List<User> findAllUsers();

    /**
     * Deletes the {@link User} corresponding to the identifier.
     * @param id the identifier of user
     */
    void deleteUser(final Long id);

    /**
     * Deletes the {@link Group} corresponding to the identifier.
     * @param id the identifier of group
     */
    void deleteGroup(final Long id);
    
    /**
     * Retrieves the users having the provided role.
     * @param role the role to look for.
     * @return the matching users.
     */
    public List<User> findUsersHavingRole(String role);
    
}
