package eu.europa.ec.opoce.cellar.cl.dao.impl;

import eu.europa.ec.opoce.cellar.cl.dao.PackageHistoryDao;
import eu.europa.ec.opoce.cellar.cl.domain.package_history.PackageHistory;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * This DAO is responsible for managing the 'Package History' objects.
 *
 * @author EUROPEAN DYNAMICS S.A.
 */

@Repository
public class PackageHistoryDaoImpl extends BaseDaoImpl<PackageHistory, Long> implements PackageHistoryDao{
    
    /**
     *{@inheritDoc}
     */
    @Override
    public PackageHistory getByPackageNameAndDate(String metsPackageName, Date receivedDate) {
        Map<String, Object> params = new HashMap<>();
        params.put("phMetsPackageName", metsPackageName);
        params.put("phReceivedDate", receivedDate);
        return super.findObjectByNamedQueryWithNamedParams("PackageHistory.findByPackageNameAndDate", params);
    }
}
