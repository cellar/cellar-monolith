/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.concurrency.impl
 *             FILE : CellarIdSharedLock.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 30, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.concurrency.impl;

import java.util.Arrays;
import java.util.List;

/**
 * <class_description> The Cellar lock implementation to use when the cellar id exists.
 * <br/><br/>
 * ON : Apr 30, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class CellarIdLock extends CellarLock {

    private String cellarId;

    /**
     * Constructor.
     * @param cellarId the cellar id
     */
    public CellarIdLock(final String cellarId) {
        super();
        this.cellarId = cellarId;
    }

    /**
     * Return the cellar id.
     * @return the cellar id
     */
    public String getCellarId() {
        return this.cellarId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getPids() {
        return Arrays.asList(this.cellarId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return this.cellarId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MODE getDefaultMode() {
        return MODE.WRITE;
    }
}
