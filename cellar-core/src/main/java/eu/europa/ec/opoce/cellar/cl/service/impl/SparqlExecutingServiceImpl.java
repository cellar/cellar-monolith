/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *             FILE : SparqlExecutingServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Sep 24, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import com.google.common.base.Stopwatch;
import eu.europa.ec.opoce.cellar.ccr.service.database.DefaultTransactionService;
import eu.europa.ec.opoce.cellar.cl.configuration.impl.CellarPersistentConfiguration;
import eu.europa.ec.opoce.cellar.cl.service.client.SparqlExecutingService;
import eu.europa.ec.opoce.cellar.cl.service.client.UriFoundDelegator;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Timer;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ConnectionPoolTimeoutException;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.time.Duration;
import java.util.Stack;

import static eu.europa.ec.opoce.cellar.AppConstants.NEWLINE;
import static org.springframework.http.HttpStatus.OK;

/**
 * <class_description> Implementation of the {@link SparqlExecutingService}.
 * <br/>
 * <br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc. <br/>
 * <br/>
 * ON : Sep 24, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class SparqlExecutingServiceImpl extends DefaultTransactionService implements SparqlExecutingService {

    /**
     * Logger instance.
     */
    private static final Logger LOG = LogManager.getLogger(SparqlExecutingServiceImpl.class);

    /**
     * The Constant PREFIX_TEMP_FILE.
     */
    private static final String PREFIX_TEMP_FILE = "sparqlQuery_";

    /**
     * The Constant SUFFIX_TEMP_FILE.
     */
    private static final String SUFFIX_TEMP_FILE = ".xml";

    /**
     * HTTP client to call the SPARQL endpoint.
     */
    private volatile HttpClient sparqlClient;

    /**
     * The properties of the Cellar.
     */
    private final CellarPersistentConfiguration cellarConfiguration;

    private final Timer sparqlReadSuccessTimer = Metrics.timer("external.sparql.read.200.timer");
    private final Counter sparqlRead503Counter = Metrics.counter("external.sparql.read.503.count");
    private final Counter sparqlRead404Counter = Metrics.counter("external.sparql.read.404.count");
    private final Counter sparqlReadSuccessCounter = Metrics.counter("external.sparql.read.200.count");

    @Autowired
    public SparqlExecutingServiceImpl(@Qualifier("cellarConfiguration") CellarPersistentConfiguration cellarConfiguration) {
        this.cellarConfiguration = cellarConfiguration;
    }

    /**
     * Post constructor of the bean.
     */
    @PostConstruct
    private void init() {
        sparqlClient = newClient(cellarConfiguration.getCellarServiceSparqlTimeout(),
                cellarConfiguration.getCellarServiceSparqlMaxConnections(),
                cellarConfiguration.getCellarServiceSparqlTimeoutConnections());
    }

    @Override
    public void updateTimeout() {
        init();
    }

    private static CloseableHttpClient newClient(int timeout, int maxConnections, int timeoutConnections) {
        return HttpClientBuilder.create().setDefaultRequestConfig(
                RequestConfig.custom()
                        .setConnectionRequestTimeout(timeoutConnections)
                        .setSocketTimeout(timeout)
                        .setConnectTimeout(timeout)
                        .build())
                .setMaxConnTotal(maxConnections)
                .build();
    }

    /**
     * <class_description> Enum of the reindexation job status.
     * <p>
     * <br/>
     * <br/>
     * <notes> Includes guaranteed invariants, usage instructions and/or
     * examples, reminders about desired improvements, etc. <br/>
     * <br/>
     * ON : Oct 2, 2012
     *
     * @author ARHS Developments
     * @version $Revision$
     */
    public enum SPARQLNODE {
        SPARQL,
        RESULTS,
        RESULT,
        BINDINGURI,
        OTHER;
    }

    ;

    @Override
    public File executeSparqlQueryFormat(final String query, final String format) throws Exception {
        File result = null;
        HttpResponse response = null;
        try {
            Stopwatch elapsed = Stopwatch.createStarted();
            response = handleHttpMethod(query, format);
            validateSparqlResponseCode(query, response, elapsed.elapsed());
            result = writeDataStreamToTempFile(response.getEntity().getContent());
        } catch (Exception e) {
            if (e instanceof ConnectionPoolTimeoutException) {
                LOG.error("Connection pool timeout, try to increase cellar.service.sparql.max-connections or " +
                        "cellar.service.sparql.timeout-connections.");
            }
            throw e;
        } finally {
            if (response != null) {
                EntityUtils.consumeQuietly(response.getEntity());
            }
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void executeSparqlQuery(final String query, final String filePath, final String format) throws Exception {
        HttpResponse response = null;
        try {
            Stopwatch elapsed = Stopwatch.createStarted();
            response = handleHttpMethod(query, format);
            validateSparqlResponseCode(query, response, elapsed.elapsed());
            writeDataStreamToFile(response.getEntity().getContent(), filePath);
        } finally {
            if (response != null) {
                EntityUtils.consumeQuietly(response.getEntity());
            }
        }
    }

    /**
     * @param query
     * @param response
     * @param elapsed
     */
    private void validateSparqlResponseCode(final String query, HttpResponse response, Duration elapsed) {
        final int responseCode = response != null ? response.getStatusLine().getStatusCode() : 0;

        switch (responseCode) {
            case 200:
                sparqlReadSuccessTimer.record(elapsed);
                sparqlReadSuccessCounter.increment();
                break;
            case 503:
                sparqlRead503Counter.increment();
                break;
            case 404:
                sparqlRead404Counter.increment();
                break;
            default:
                LOG.debug("No counter for code {}", responseCode);
        }
        if (responseCode != OK.value()) {
            throw ExceptionBuilder.get(CellarException.class)
                    .withMessage("The query:" + NEWLINE + "{}" + NEWLINE + "run against the SPARQL endpoint located at:" +
                            NEWLINE + "{}" + NEWLINE + "is not valid. The SPARQL endpoint returns the http status {}")
                    .withMessageArgs(query, this.cellarConfiguration.getCellarServiceSparqlUri(), responseCode)
                    .build();
        }
    }

    /**
     * Handle http method.
     *
     * @param query  the query
     * @param format the format
     * @return the http method
     * @throws UnsupportedEncodingException the unsupported encoding exception
     * @throws IOException                  Signals that an I/O exception has occurred.
     * @throws HttpException                the http exception
     */
    private HttpResponse handleHttpMethod(final String query, final String format)
            throws UnsupportedEncodingException, IOException, HttpException {
        // performs sparql request
        String sparqlServiceUri = this.cellarConfiguration.getCellarServiceSparqlUri() + "?query=" + URLEncoder.encode(query, "UTF-8");
        if (format != null) {
            sparqlServiceUri = sparqlServiceUri + "&format=" + URLEncoder.encode(format, "UTF-8");
        }
        LOG.info("Execute request (call Virtuoso): {}", sparqlServiceUri);
        final HttpUriRequest httpUriRequest = new HttpGet(sparqlServiceUri);
        return this.sparqlClient.execute(httpUriRequest);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void parseSparqlReponse(final String reponseFilePath, final UriFoundDelegator uriFoundDelegator) throws Exception {
        XMLStreamReader reader = null;
        FileInputStream fis = null;
        try {

            final XMLInputFactory factory = XMLInputFactory.newInstance();
            factory.setProperty(XMLInputFactory.IS_REPLACING_ENTITY_REFERENCES, false);
            factory.setProperty(XMLInputFactory.IS_VALIDATING, false);
            factory.setProperty(XMLInputFactory.SUPPORT_DTD, false);

            fis = new FileInputStream(reponseFilePath);
            reader = factory.createXMLStreamReader(fis);
            final Stack<SPARQLNODE> sparqlXPath = new Stack<SPARQLNODE>();

            int eventType;
            String localPart;
            while (true) { // parses the sparql response
                eventType = reader.next();
                switch (eventType) {
                    case XMLEvent.START_ELEMENT:
                        localPart = reader.getName().getLocalPart();
                        if (sparqlXPath.isEmpty() && localPart.equals("sparql")) {
                            sparqlXPath.push(SPARQLNODE.SPARQL);
                        } else if (!sparqlXPath.isEmpty()) {
                            if ((sparqlXPath.peek() == SPARQLNODE.SPARQL) && localPart.equals("results")) {
                                sparqlXPath.push(SPARQLNODE.RESULTS);
                            } else if ((sparqlXPath.peek() == SPARQLNODE.RESULTS) && localPart.equals("result")) {
                                sparqlXPath.push(SPARQLNODE.RESULT);
                            } else if ((sparqlXPath.peek() == SPARQLNODE.RESULT) && localPart.equals("binding")) {
                                if (reader.isAttributeSpecified(0)) {
                                    final String attribute = reader.getAttributeValue(null, "name");
                                    if (attribute.equals("uri")) {
                                        sparqlXPath.push(SPARQLNODE.BINDINGURI); // sparql/results/result/binding[@name='uri']
                                    } else {
                                        sparqlXPath.push(SPARQLNODE.OTHER);
                                    }
                                }
                            } else if ((sparqlXPath.peek() == SPARQLNODE.BINDINGURI) && localPart.equals("uri")) {
                                uriFoundDelegator.onUri(reader.getElementText());
                            } else {
                                sparqlXPath.push(SPARQLNODE.OTHER);
                            }
                        }

                        break;
                    case XMLEvent.END_ELEMENT:
                        if (!sparqlXPath.isEmpty()) {
                            sparqlXPath.pop();
                        }
                        break;
                    case XMLEvent.END_DOCUMENT:
                        uriFoundDelegator.onEnd();
                        return;
                }
            }
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }

                if (fis != null) {
                    fis.close();
                }
            } catch (final Exception exception) {
                LOG.error("Cannot close the temp file", exception);
            }
        }
    }

    /**
     * Writes the data stream in the temp file.
     *
     * @param dataStream the stream to copy
     * @return the file
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private File writeDataStreamToTempFile(final InputStream dataStream) throws IOException {
        // creates a temp file for the sparql endpoint response
        final File tempDir = new File(cellarConfiguration.getCellarFolderTemporaryStore());
        final File result = File.createTempFile(PREFIX_TEMP_FILE, SUFFIX_TEMP_FILE, tempDir);
        result.createNewFile();
        writeToFile(dataStream, result);
        return result;
    }

    /**
     * Write data stream to file.
     *
     * @param dataStream the data stream
     * @param filePath   the file path
     * @return the file
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private File writeDataStreamToFile(final InputStream dataStream, String filePath) throws IOException {
        final File result = new File(filePath);
        writeToFile(dataStream, result);
        return result;
    }

    /**
     * Write to file.
     *
     * @param dataStream the data stream
     * @param result     the result
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void writeToFile(final InputStream dataStream, final File result) throws IOException {
        FileWriter writer = null;
        try {
            writer = new FileWriter(result);
            IOUtils.copy(dataStream, writer, "UTF-8");
            writer.flush();
        } finally {
            IOUtils.closeQuietly(writer);
            IOUtils.closeQuietly(dataStream);
        }
    }
}
