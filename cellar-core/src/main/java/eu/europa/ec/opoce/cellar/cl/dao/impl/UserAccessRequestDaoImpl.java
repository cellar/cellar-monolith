/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.dao.impl
 *        FILE : UserAccessRequestDaoImpl.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 22-02-2023
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2023 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import eu.europa.ec.opoce.cellar.cl.dao.UserAccessRequestDao;
import eu.europa.ec.opoce.cellar.cl.domain.admin.UserAccessRequest;

/**
 * <class_description> Implementation of the interface {@link UserAccessRequestDao}.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 22, 2023
 *
 * @author EUROPEAN DYNAMICS S.A.
 */
@Repository
public class UserAccessRequestDaoImpl extends BaseDaoImpl<UserAccessRequest, Long> implements UserAccessRequestDao{

	@Override
	public UserAccessRequest findByUsername(String username) {
		Map<String, Object> params = new HashMap<>();
		params.put("username", username);
		return findObjectByNamedQueryWithNamedParams("UserAccessRequest.findByUsername", params);
	}

}
