/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.spring
 *             FILE : ShutdownListener.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 06 12, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-06-12 13:35:46 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.spring;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditBuilder;
import eu.europa.ec.opoce.cellar.cl.domain.audit.Auditable;
import eu.europa.ec.opoce.cellar.cl.scheduling.BatchJobScheduler;
import eu.europa.ec.opoce.cellar.cl.scheduling.licenseHolder.LicenseHolderScheduler;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.manager.BatchJobThreadPoolManager;
import eu.europa.ec.opoce.cellar.cmr.scheduling.Scheduler;
import eu.europa.ec.opoce.cellar.common.concurrent.ListeningExecutorService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import static eu.europa.ec.opoce.cellar.cl.domain.audit.Auditable.LogLevel.ERROR;
import static eu.europa.ec.opoce.cellar.cl.domain.audit.Auditable.LogLevel.INFO;
import static eu.europa.ec.opoce.cellar.cl.domain.audit.Auditable.LogLevel.WARN;

/**
 * @author ARHS Developments
 */
@Component
public class GracefulShutdownListener implements ApplicationListener<ContextClosedEvent> {

    private static final Logger LOG = LogManager.getLogger(GracefulShutdownListener.class);

    private final AtomicBoolean running = new AtomicBoolean(false);

    private final ICellarConfiguration configuration;

    // Direct ThreadPoolExecutors
    private final Executors executors;

    // Encapsulated ThreadPoolExecutors
    private final Scheduler scheduler;
    private final BatchJobScheduler batchJobScheduler;
    private final LicenseHolderScheduler licenseHolderScheduler;

    @Autowired
    GracefulShutdownListener(@Qualifier("cellarStaticConfiguration") ICellarConfiguration configuration, Executors executors,
                             Scheduler scheduler, BatchJobScheduler batchJobScheduler, LicenseHolderScheduler licenseHolderScheduler) {
        this.configuration = configuration;
        this.executors = executors;
        this.scheduler = scheduler;
        this.batchJobScheduler = batchJobScheduler;
        this.licenseHolderScheduler = licenseHolderScheduler;
    }

    @Override
    public void onApplicationEvent(ContextClosedEvent event) {
        if (configuration.isGracefulShutdownEnabled() && running.compareAndSet(false, true)) {
            log("Start graceful shutdown ...");
            long timeout = configuration.getGracefulShutdownTimeout();
            TimeUnit unit = TimeUnit.MILLISECONDS;
            boolean awaitTermination = false;
            if (timeout < 0) { // Infinite timeout
                timeout = 5;
                unit = TimeUnit.SECONDS;
                awaitTermination = true;
            }

            try {
                log("Shutdown CMR scheduler ...");
                scheduler.shutdown(awaitTermination, timeout, unit);
                log("Shutdown Batch scheduler ...");
                batchJobScheduler.shutdown(awaitTermination, timeout, unit);
                log("Shutdown License holder scheduler ...");
                licenseHolderScheduler.shutdown(awaitTermination, timeout, unit);

                for (NamedThreadPoolExecutor executor : executors.executors) {
                    shutdownGracefully(executor, awaitTermination, timeout, unit);
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                AuditBuilder.get()
                        .withLogger(LOG)
                        .withLogLevel(ERROR)
                        .withMessage("Graceful shutdown interrupted.")
                        .withException(e)
                        .logEvent();
            }
        }
    }

    private static void shutdownGracefully(NamedThreadPoolExecutor ntpe, boolean awaitTermination, long timeout,
                                           TimeUnit unit) throws InterruptedException {
        final ThreadPoolExecutor tpe = ntpe.tpe;
        log("Shutdown [{}], {} task(s) remaining", ntpe.name, tpe.getQueue().size());

        // Request shutdown => new tasks will be rejected
        tpe.shutdown();
        tpe.setRejectedExecutionHandler(new VerboseRejectedExecutionHandler(ntpe.name));

        if (awaitTermination) {
            while (!tpe.isTerminated()) {
                tpe.awaitTermination(timeout, unit);
            }
        } else {
            log("Await termination (timeout={} {})", timeout, unit);
            if (!tpe.awaitTermination(timeout, unit)) {
                log(WARN, "Timeout reached => Shutdown now [{}]", ntpe.name);
                tpe.shutdownNow();
            }
        }
        log("[{}] shutdown.", ntpe.name);
    }

    private static void log(String message, Object... args) {
        log(INFO, message, args);
    }

    private static void log(Auditable.LogLevel logLevel, String message, Object... args) {
        AuditBuilder.get()
                .withLogger(LOG)
                .withLogLevel(logLevel)
                .withMessage(message)
                .withMessageArgs(args)
                .logEvent();
    }

    @Component
    static class Executors {

        final List<NamedThreadPoolExecutor> executors = new ArrayList<>();

        @Autowired
        Executors(BatchJobThreadPoolManager batchManager,
                  @Qualifier("ingestionRequestExecutor") ListeningExecutorService ingestionExecutor,
                  @Qualifier("rdfStoreCleanerExecutor") ThreadPoolTaskExecutor rdfCleanerExecutor,
                  @Qualifier("alignerExecutor") ThreadPoolTaskExecutor alignerExecutor,
                  @Qualifier("taskExecutor") ThreadPoolTaskExecutor taskExecutor,
                  @Qualifier("archivingBatchExecutor") ThreadPoolTaskExecutor archivingExecutor) {

            register("Batch Job", batchManager.getThreadPoolExecutor());
            register("Ingestion", (ThreadPoolExecutor) ingestionExecutor);
            register("RDFStore cleaner", rdfCleanerExecutor.getThreadPoolExecutor());
            register("Aligner", alignerExecutor.getThreadPoolExecutor());
            register("Task", taskExecutor.getThreadPoolExecutor());
            register("Archiving", archivingExecutor.getThreadPoolExecutor());
        }

        protected void register(String name, ThreadPoolExecutor threadPoolExecutor) {
            executors.add(new NamedThreadPoolExecutor(name, threadPoolExecutor));
        }
    }

    static class NamedThreadPoolExecutor {
        final String name;
        final ThreadPoolExecutor tpe;

        NamedThreadPoolExecutor(String name, ThreadPoolExecutor tpe) {
            this.name = name;
            this.tpe = tpe;
        }
    }

    static class VerboseRejectedExecutionHandler implements RejectedExecutionHandler {

        final String name;

        VerboseRejectedExecutionHandler(String name) {
            this.name = name;
        }

        @Override
        public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
            AuditBuilder.get()
                    .withLogger(LOG)
                    .withMessage("Task rejected by [{}]")
                    .withMessageArgs(name)
                    .logEvent();
        }

    }

}
