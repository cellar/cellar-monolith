package eu.europa.ec.opoce.cellar.support.sip;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration;
import eu.europa.ec.opoce.cellar.cl.service.client.FileService;
import eu.europa.ec.opoce.cellar.common.util.FileSystemUtils;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;
import eu.europa.ec.opoce.cellar.support.FileExtensionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.nio.file.attribute.FileTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * The Class SIPIterator.
 */
public class SIPIterator implements Iterator<SIPWork> {

	/** Logger instance. */
	private static final Logger LOG = LogManager.getLogger(SIPIterator.class);

	/**
	 * Creates the sip process object.
	 *
	 * @param sipFileToProcess
	 *            the sip file to process
	 * @param type
	 *            the type
	 * @return the SIP work
	 */
	private SIPWork createSIPProcessObject(final File sipFileToProcess, final TYPE type) {
		final SIPWork sipProcessObject = new SIPWork();
		sipProcessObject.setSipFile(sipFileToProcess);
		sipProcessObject.setType(type);
		sipProcessObject.setCreationDate(getTimestampAttribute(sipFileToProcess));
		return sipProcessObject;
	}

	/** The cellar configuration. */
	protected ICellarConfiguration cellarConfiguration;

	/** The sip iterator. */
	private Iterator<SIPWork> sipIterator;

	/** The cellar reception folder types. */
	private SortedSet<CellarReceptionFolderType> cellarReceptionFolderTypes = new TreeSet<CellarReceptionFolderType>();

	/** The file service. */
	private FileService fileService;
	
	/**
	 * Instantiates a new abstract sip iterator.
	 *
	 * @param cellarConfiguration
	 *            the cellar configuration
	 * @param fileService
	 *            the file Service
	 */
	public SIPIterator(final ICellarConfiguration cellarConfiguration, final FileService fileService) {
		this.cellarConfiguration = cellarConfiguration;
		this.fileService = fileService;
	}

	/**
	 * Adds the cellar reception folder types.
	 *
	 * @param folderType the cellar reception folder type
	 */
	public void addCellarReceptionFolderTypes(final CellarReceptionFolderType folderType) {
		final String folderPath = folderType.getReceptionFolderAbsolutePath(this.cellarConfiguration);
		this.cellarReceptionFolderTypes.add(folderType);
		LOG.info(IConfiguration.CONFIG, "SIP folder [{}] added for type [{}]", folderPath, folderType.getSipWorkType());
	}

	/** {@inheritDoc} */
	@Override
	public boolean hasNext() {
		return this.sipIterator.hasNext();
	}

	public List<SIPWork> getSIPList() {
		// loads all files from all given folders
		// a linked list to keep the order set by the tree set and also in case
		// the files are shuffled
		final List<SIPWork> result = new LinkedList<>();
		Set<String> sipFileNames = new HashSet<>();
		for (final CellarReceptionFolderType cellarReceptionFolderType : this.cellarReceptionFolderTypes) {
			final TYPE sipWorkType = cellarReceptionFolderType.getSipWorkType();
			
			// Iterates the ingestion folders and splits the detected files into
			// two groups: the actual SIP files, and all other files and folders.
    		File receptionFolder = new File(cellarReceptionFolderType.getReceptionFolderAbsolutePath(this.cellarConfiguration));
    		Map<Boolean, List<File>> ingestionFolderContents = Arrays.asList(receptionFolder.listFiles()).stream()
    			.collect(Collectors.partitioningBy(this::isSipFile));
			
    		// Move all non-SIP files and folders to the corresponding error response folders.
    		List<File> nonSipFiles = ingestionFolderContents.get(Boolean.FALSE);
    		this.fileService.moveToResponseErrorFolder(nonSipFiles, sipWorkType);
    		
    		List<File> sipFiles = ingestionFolderContents.get(Boolean.TRUE);
    		cellarReceptionFolderType.reorderSipList(this.cellarConfiguration, sipFiles);
    		final int size = sipFiles.size();
    		LOG.debug("{} SIP files available in ingestion folder [{}].", size, sipWorkType);
			for (final File file : sipFiles) {
				// In case a SIP file has the same name with one of a higher priority
				// which is already fetched, then disregard the one with the lower priority.
				if (!sipFileNames.contains(file.getName())) {
					// builds the SIPWork objects
					final SIPWork sipWork = createSIPProcessObject(file, sipWorkType);
					result.add(sipWork);
					sipFileNames.add(file.getName());
				}
			}
		}
		return result;
	}

	/**
	 * Checks if the provided file is a SIP file.
	 * @param f the file to check.
	 * @return true if the file is a SIP file.
	 */
	private boolean isSipFile(File f) {
		return f.isFile() && f.getName().endsWith(FileExtensionUtils.SIP);
	}
	
	/**
	 * Load sip iterator. Respecting an order defined by the SIPWork.TYPE
	 */
	public void loadSIPIterator() {
		this.sipIterator = this.getSIPList().iterator();
	}

	/** {@inheritDoc} */
	@Override
	public SIPWork next() {
		return this.sipIterator.next();
	}

	/** {@inheritDoc} */
	@Override
	public void remove() {
		this.sipIterator.remove();
	}

	/**
	 * Sets the cellar reception folder types.
	 *
	 * @param cellarReceptionFolderTypes
	 *            the new cellar reception folder types
	 */
	public void setCellarReceptionFolderTypes(final SortedSet<CellarReceptionFolderType> cellarReceptionFolderTypes) {
		this.cellarReceptionFolderTypes = cellarReceptionFolderTypes;
	}

	/**
	 * Gets either the "lastAccessed", "lastModified", "lastChanged" timestamp
	 * attribute of the provided file. The actual attribute that
	 * is returned depends on the configuration. 
	 * @param file the file whose timestamp attribute shall be retrieved.
	 * @return the file's timestamp attribute.
	 */
	private Date getTimestampAttribute(File file) {
		String attributeName = this.cellarConfiguration.getCellarServiceSipQueueManagerFileAttributeDateType();
		FileTime attributeValue = (FileTime) FileSystemUtils.readAttribute(file, attributeName);
		if (attributeValue == null) {
			LOG.warn("Attribute [{}] of package [{}] could not be retrieved. Ignoring package.", attributeName, file);
			return new Date(0L);
		}
		else {
			return new Date(attributeValue.toMillis());
		}
	}
	
}
