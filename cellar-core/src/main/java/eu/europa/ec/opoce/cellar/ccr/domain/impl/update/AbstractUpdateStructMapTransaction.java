/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.ccr.domain.impl.update
 *        FILE : AbstractUpdateStructMapTransaction.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 08-05-2013
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.domain.impl.update;

import com.amazonaws.services.s3.Headers;
import com.google.common.annotations.VisibleForTesting;
import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.ccr.domain.impl.TransactionConfiguration;
import eu.europa.ec.opoce.cellar.ccr.domain.impl.WriteStructMapTransaction;
import eu.europa.ec.opoce.cellar.ccr.helper.DigitalObjectHelper;
import eu.europa.ec.opoce.cellar.ccr.helper.ProductionIdentifierHelper;
import eu.europa.ec.opoce.cellar.ccr.helper.ResponseHelper;
import eu.europa.ec.opoce.cellar.ccr.s3.S3DataStreamNamingService;
import eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration;
import eu.europa.ec.opoce.cellar.cl.dao.ProductionIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.admin.ProductionIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.cl.domain.response.ContentResponseOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.DigitalObjectOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.ResponseOperationType;
import eu.europa.ec.opoce.cellar.cl.domain.response.StructMapOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.UpdateStructMapOperation;
import eu.europa.ec.opoce.cellar.cl.service.client.CellarIdentifierService;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierResolver;
import eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.MetaDataIngestionHelper;
import eu.europa.ec.opoce.cellar.cmr.MetaDataIngestionService;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentStream;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsPackage;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.S3OperationException;
import eu.europa.ec.opoce.cellar.exception.StructMapProcessorException;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import eu.europa.ec.opoce.cellar.s3.domain.ContentVersion;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.FileSystemResource;
import org.springframework.web.client.HttpClientErrorException;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;

import static eu.europa.ec.opoce.cellar.cmr.ContentType.DIRECT;
import static eu.europa.ec.opoce.cellar.cmr.ContentType.FILE;

/**
 * <class_description> This class provides methods for the update/merge process of a StructMap into database and S3.
 * Every digital objects (with their contents and metadatas) under a given StructMap will be created in database and S3.
 * If an error occurs during this process, the entire process is rolled back, in order that all digital objects (with metadata and content)
 * will be removed from the database and S3 if they are created, or set to the previous version if they are updated.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 08-05-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public abstract class AbstractUpdateStructMapTransaction extends WriteStructMapTransaction<CellarIdentifier> {

    private static final Logger LOG = LogManager.getLogger(AbstractUpdateStructMapTransaction.class);

    private static final String CONTENT_PREFIX = "DOC_";

    @Autowired
    private IdentifierResolver identifierResolver;

    @Autowired
    @Qualifier("pidManagerService")
    private PidManagerService pidManagerService;

    @Autowired
    private ProductionIdentifierDao productionIdentifierDao;

    @Autowired
    private ContentStreamService contentStreamService;

    @Autowired
    private MetaDataIngestionService<CalculatedData> metaDataIngestionService;

    @Autowired
    private S3DataStreamNamingService s3DataStreamNamingService;

    @Autowired
    protected CellarIdentifierService cellarIdentifierService;

    @Autowired
    protected CellarResourceDao cellarResourceDao;

    /**
     * Instantiates a new abstract update struct map transaction.
     *
     * @param configuration the configuration
     */
    protected AbstractUpdateStructMapTransaction(final TransactionConfiguration configuration) {
        super(configuration);
    }

    /**
     * Provide the update of each digital object found under the given structmap (in depth first traversal).
     * The top digital object (WORK/DOSSIER/AGENT/TOPEVENTLEVEL) MUST exist in the database.
     *
     * @param metsPackage    to find metadata associated for every digital object under the structMap.
     * @param calculatedData the calculated data
     * @param metsDirectory  a reference to the directory where the SIP has been exploded.
     * @param sipResource    the sipResource to audit this method.
     * @return the struct map operation
     */
    @Override
    public StructMapOperation executeWriteTreatment(final MetsPackage metsPackage, final CalculatedData calculatedData,
                                                    final File metsDirectory, final SIPResource sipResource) {
        final List<CellarIdentifier> hiddenContentStreamIdentifierList = new ArrayList<>();

        final UpdateStructMapOperation operation = new UpdateStructMapOperation();
        final StructMap structMap = calculatedData.getStructMap();
        operation.setId(structMap.getId());
        try {
            final List<DigitalObject> appendedDOList = new ArrayList<>();

            // get the next available expression's suffix
            final int nextExpressionIdSuffix = nextCellarIdSuffix(calculatedData.getRootCellarId());

            // 2. manage CELLAR database and digital objects' hierarchy on CCR
            processHierarchy(operation, calculatedData, metsDirectory, sipResource, hiddenContentStreamIdentifierList, appendedDOList,
                    new AtomicInteger(nextExpressionIdSuffix));

            // 3. manage metadata on CCR and CMR
            metaDataIngestionService.ingest(calculatedData, operation);
        } catch (final Exception exception) {
            // rollback the entire process for this structmap
            this.rollback(operation, calculatedData, exception, hiddenContentStreamIdentifierList);
        }

        // now, we are sure that we can physically delete the objects from CCR
        // (these objects have a type 'deleted')
//        delete(operation); // TODO AJ ?

        return operation;
    }


    /**
     * Returns the next cellar suffix for an expression or manifestation.
     * If we have already in S3 some expressions like xxx.0001, xxx.0003, xxx.0005 the next suffix will be 0006
     * If we have already in S3 some manifestations like xxx.yyyy.01, xxx.yyyy.03, xxx.yyyy.05 the next suffix will be 06
     *
     * @param cellarID the cellarID
     * @return the next cellar suffix for an expression (resp. manifestation).
     */
    private int nextCellarIdSuffix(String cellarID) {
        int maxCellarIdSuffix = 0;
        final List<String> childrenPids = getChildrenPIDs(cellarID).stream()
                .map(CellarResource::getCellarId)
                .sorted()
                .collect(Collectors.toList());
        for (final String childrenPid : childrenPids) {
            final int currentCellarIdSuffix = Integer.parseInt(StringUtils.substringAfterLast(childrenPid, "."));
            if (maxCellarIdSuffix < currentCellarIdSuffix) {
                maxCellarIdSuffix = currentCellarIdSuffix;
            }
        }

        return maxCellarIdSuffix + 1;
    }

    private List<CellarResource> getChildrenPIDs(final String digitalObjectPID) {
        return new ArrayList<>(cellarResourceDao.findResourceStartWith(digitalObjectPID, DigitalObjectType.EXPRESSION));
    }

    /**
     * Process all digital objects under this structmap in depth first traversal; the corresponding action to do depends on the update's type.
     *
     * @param operation                          a reference to the generated response object.
     * @param calculatedData                     the calculated data
     * @param metsDirectory                      a reference to the directory where the SIP has been exploded.
     * @param sipResource                        needed for audit
     * @param hiddenContentStreamIdentifierList  actual removed digital object list.
     * @param appendedDOList                     actual appended digital object list
     * @param nextExpressionCellarIdSuffixNumber the next expression cellar identifier.
     */
    protected abstract void processHierarchy(final UpdateStructMapOperation operation, final CalculatedData calculatedData,
                                             final File metsDirectory, final SIPResource sipResource, final List<CellarIdentifier> hiddenContentStreamIdentifierList,
                                             final List<DigitalObject> appendedDOList, final AtomicInteger nextExpressionCellarIdSuffixNumber);

    /**
     * Update a content of a digital object.
     * // get all current existing datastream.
     * // during update, remove all datastream defined in the mets...at the end, all content datastream already in this set
     * // must be deleted.
     *
     * @param operation       the response operation.
     * @param manifestationId the manifestation cellar identifier where the content should be updated.
     * @param manifestation   the manifestation object where the content should be updated.
     * @param metsDirectory   a reference to the directory where the SIP has been exploded.
     */
    protected void updateContent(final StructMapOperation operation, final CellarIdentifier manifestationId,
                                 final DigitalObject manifestation, final File metsDirectory) {
        final ContentIdentifier manifestationCellarId = manifestation.getCellarId();
        final String manifestationCellarIdentifier = manifestationCellarId.getIdentifier();
        try {
            final DigitalObjectOperation digitalObjectOperation = getCellarDigitalObjectOperation(operation, manifestation, ResponseOperationType.UPDATE);
            final String uuid = manifestationId.getUuid();

            Collection<CellarResource> manifestationAndItems = cellarResourceDao.findStartingWith(uuid);
            CellarResource currentManifestation = manifestationAndItems.stream()
                    .filter(r -> r.getCellarType() == DigitalObjectType.MANIFESTATION)
                    .findFirst()
                    .orElseThrow(() -> new IllegalStateException("Manifestation not found for: " + uuid));

            final Map<String, CellarResource> items = manifestationAndItems.stream()
                    .filter(r -> r.getCellarType() == DigitalObjectType.ITEM)
                    .collect(Collectors.toMap(CellarResource::getCellarId, Function.identity()));
            // Map<CellarID, MimeType> to avoid a query to S3 or Oracle later in the process
            final Map<String, String> existingContentCellarIdSet = items.values().stream()
                    .collect(Collectors.toMap(CellarResource::getCellarId, CellarResource::getMimeType));

            //one manifestation can have many content-streams
            final List<ContentStream> contentStreams = manifestation.getContentStreams();
            for (ContentStream contentStream : contentStreams) {
                //a set to avoid duplicate
                final Set<CellarIdentifier> contents = new HashSet<>();
                boolean updateCID = true;
                int nextDatastreamId = -1;
                final List<ContentResponseOperation> currentContentResponseOperation = new LinkedList<>();
                //one content-stream may have multiple content ids
                //one cellar identifier may have multiple contentIds
                CellarIdentifier currentContentCellarIdentifier = null;
                for (final ContentIdentifier contentId : contentStream.getContentids()) {
                    final String identifier = contentId.getIdentifier();
                    if (currentContentCellarIdentifier == null) {
                        try {
                            currentContentCellarIdentifier = identifierResolver.resolveToCellarId(identifier);
                        } catch (final IllegalArgumentException e) {
                            // we did not find this cellar id!
                        }
                        if (currentContentCellarIdentifier == null) {
                            //if its a new content id it has to be created
                            currentContentCellarIdentifier = new CellarIdentifier();
                            // the content stream is the same for all content ids
                            if (nextDatastreamId == -1) {
                                nextDatastreamId = getNextDatastreamIndex(currentManifestation, items.keySet());
                            }
                            this.s3DataStreamNamingService.renameCellarIdentifier(currentContentCellarIdentifier, contentStream.getLabel(), uuid, nextDatastreamId);

                            final ProductionIdentifier contentPID = new ProductionIdentifier();
                            contentPID.setProductionId(identifier);
                            contentPID.setCellarIdentifier(currentContentCellarIdentifier);
                            contentPID.setCreationDate(new Date());
                            this.pidManagerService.save(contentPID, false);

                            updateCID = false;
                        }
                    } else {
                        //in case there's already a cellar identifier and we need to add a new content id
                        //we just need to create an additional product identifier and link it to the existing cellar id
                        if (productionIdentifierDao.getProductionIdentifier(identifier) == null) {
                            final ProductionIdentifier contentPID = new ProductionIdentifier();
                            contentPID.setProductionId(identifier);
                            contentPID.setCellarIdentifier(currentContentCellarIdentifier);
                            contentPID.setCreationDate(new Date());
                            pidManagerService.save(contentPID, false);
                        }
                    }

                    //only if this is not an additional content id to add
                    final String currentContentCellarIdentifierUuid = currentContentCellarIdentifier.getUuid();
                    final ContentIdentifier cellarId = new ContentIdentifier(currentContentCellarIdentifierUuid);
                    contentStream.setCellarId(cellarId);
                    final String substringAfterLast = StringUtils.substringAfterLast(currentContentCellarIdentifierUuid, "/");
                    contentStream.setCCR_datastream(substringAfterLast);
                    final String cellarIdIdentifier = cellarId.getIdentifier();

                    //if it's not a fake content
                    if (StringUtils.isNotBlank(contentStream.getFileRef())) {
                        final ResponseOperationType operationType = updateCID ? ResponseOperationType.UPDATE : ResponseOperationType.CREATE;
                        final ContentResponseOperation contentResponseOperation = new ContentResponseOperation(cellarIdIdentifier, identifier, operationType);
                        digitalObjectOperation.addContentIdentifier(contentResponseOperation);
                        currentContentResponseOperation.add(contentResponseOperation);
                    }

                    //it's not null and the set will avoid having duplicates
                    contents.add(currentContentCellarIdentifier);
                }

                if (contents.isEmpty()) {
                    throw ExceptionBuilder.get(StructMapProcessorException.class).withCode(CoreErrors.E_110)
                            .withMessage("Unable to retrieve the content identifier of a content stream of manifestation [{}].")
                            .withMessageArgs(manifestationCellarIdentifier).build();
                }

                for (final CellarIdentifier item : contents) {
                    final String contentCellarIdentifier = ProductionIdentifierHelper.getUUIDWithoutDatastreamName(item);

                    // Filref = null => this dataStream is left in S3 without updating.
                    final String itemId = item.getUuid();
                    if (StringUtils.isNotBlank(contentStream.getFileRef())) {
                        String lastVersion = "";
                        String lastMimeType = "application/xml";
                        final String datastreamName = ProductionIdentifierHelper.getDatastreamName(item);
                        if (items.containsKey(itemId)) {
                            String version = items.get(itemId).getVersion(FILE).orElse(null);
                            //remove from existing content datastream list this datastream.
                            existingContentCellarIdSet.remove(itemId);

                            final long startGettingVersion = System.currentTimeMillis();
                            // latest version is stored in oracle to access to the data in S3
                            final List<ContentVersion> versions = contentStreamService.getVersions(itemId, FILE, 1);
                            Map<String, Object> metadata = contentStreamService.getMetadata(itemId, version, FILE);
                            lastMimeType = (String) metadata.get(Headers.CONTENT_TYPE);
                            lastVersion = ResponseHelper.formatDate(versions.get(0).getLastModified());

                            LOG.info("Getting new version of {}/{} = {} in {} ms.", contentCellarIdentifier, datastreamName, lastVersion,
                                    (System.currentTimeMillis() - startGettingVersion));
                        }

                        final File file = getFile(metsDirectory, contentStream.getFileRef());
                        Map<String, String> metadata = new HashMap<>();
                        metadata.put(Headers.CONTENT_TYPE, contentStream.getMimeType());
                        metadata.put(ContentStreamService.Metadata.FILENAME, MetaDataIngestionHelper.dropPath(contentStream.getFileRef()));
                        String newVersion = contentStreamService.writeContent(item.getUuid(), new FileSystemResource(file), ContentType.FILE, metadata);
                        LOG.debug(IConfiguration.INGESTION, "{} updated with {} ({}) Content-Type={}", item.getUuid(), contentStream.getFileRef(), newVersion,
                                contentStream.getMimeType());

                        for (final ContentResponseOperation contentResponseOperation : currentContentResponseOperation) {
                            if (contentResponseOperation.isUpdate()) {
                                contentResponseOperation.setLastVersion(lastVersion);
                                contentResponseOperation.setLastMimeType(lastMimeType);
                            }
                            // backward compatibility, for some reasons the version is the date...
                            // To avoid to perform another request to S3, the current date is set.
                            contentResponseOperation.setNewVersion(ResponseHelper.formatDate(new Date()));
                        }
                    } else {
                        // in this case, the content id has been put in the mets to avoid to remove it from s3..
                        existingContentCellarIdSet.remove(itemId);
                    }
                }
            }

            // These datastream must be deleted : They are not declared in the update mets file.
            for (final Map.Entry<String, String> e : existingContentCellarIdSet.entrySet()) {
                String cellarIdToDelete = e.getKey();
                pidManagerService.deleteCellarIdentifier(cellarIdToDelete);

                final String datastreamName = StringUtils.substringAfterLast(cellarIdToDelete, "/");
                final ContentResponseOperation contentResponseOperation = new ContentResponseOperation(cellarIdToDelete, datastreamName,
                        ResponseOperationType.DELETE);
                contentResponseOperation.setLastMimeType(e.getValue());
                digitalObjectOperation.addContentIdentifier(contentResponseOperation);
            }

            if (!digitalObjectOperation.getContentIdentifierOperations().isEmpty()) {
                operation.addOperation(digitalObjectOperation);
            }

        } catch (final HttpClientErrorException e) {
            throw ExceptionBuilder.get(StructMapProcessorException.class).withCode(CoreErrors.E_117)
                    .withMessage("An error occurred while updating the content of the manifestation [{}].")
                    .withMessageArgs(manifestationCellarIdentifier).withCause(e).build();
        }
    }

    /**
     * Check assign and set pid to response.
     *
     * @param pid           the pid
     * @param operation     the operation
     * @param digitalObject the digital object
     */
    protected final void checkAssignAndSetPidToResponse(final ProductionIdentifier pid, final StructMapOperation operation,
                                                        final DigitalObject digitalObject) {
        ProductionIdentifierHelper.checkProductionIdentifierExists(pid, digitalObject);
        this.assignAndSetPidToResponse(pid, operation, digitalObject);
    }

    /**
     * Assign and set pid to response.
     *
     * @param pid           the pid
     * @param operation     the operation
     * @param digitalObject the digital object
     */
    protected final void assignAndSetPidToResponse(final ProductionIdentifier pid, final StructMapOperation operation,
                                                   final DigitalObject digitalObject) {
        DigitalObjectHelper.assignCellarIdentifier(digitalObject, pid);
        this.setPidToResponse(operation, digitalObject, pid.getCellarIdentifier());
    }

    /**
     * Sets the pid to response.
     *
     * @param operation        the operation
     * @param digitalObject    the digital object
     * @param cellarIdentifier the cellar identifier
     */
    protected final void setPidToResponse(final StructMapOperation operation, final DigitalObject digitalObject,
                                          final CellarIdentifier cellarIdentifier) {
        final List<String> createdPidList = this.pidManagerService.mapProductionIdentifiersToCellarIdentifier(digitalObject.getContentids(),
                cellarIdentifier);

        if (!createdPidList.isEmpty()) {
            final DigitalObjectOperation digitalObjectOperation = getCellarDigitalObjectOperation(operation, digitalObject,
                    ResponseOperationType.UPDATE);

            if (LOG.isDebugEnabled()) {
                LOG.debug(
                        "Assign cellarId[" + cellarIdentifier + "] to all these new PIDs [" + StringUtils.join(createdPidList, ", ") + "]");
            }

            for (final String createPid : createdPidList) {
                digitalObjectOperation.addProductionIdentifier(createPid, ResponseOperationType.CREATE);
            }
            operation.addOperation(digitalObjectOperation);
        }
    }

    /**
     * Gets the cellar digital object operation.
     *
     * @param operation     the operation
     * @param digitalObject the digital object
     * @param type          the type
     * @return the cellar digital object operation
     */
    private static DigitalObjectOperation getCellarDigitalObjectOperation(final StructMapOperation operation,
                                                                          final DigitalObject digitalObject, final ResponseOperationType type) {
        DigitalObjectOperation digitalObjectOperation = operation.getOperations().get(digitalObject.getCellarId().getIdentifier());
        if (digitalObjectOperation == null) {
            digitalObjectOperation = new DigitalObjectOperation(digitalObject, type);
        }

        return digitalObjectOperation;
    }

    /**
     * Gets the file.
     *
     * @param metsDirectory the mets directory
     * @param fileRef       the file ref
     * @return the file
     */
    private static File getFile(final File metsDirectory, final String fileRef) {
        return new File(metsDirectory, fileRef);
    }

    /**
     * Gets the next datastream index.
     *
     * @param manifestation the manifestation to update
     * @throws S3OperationException the s3 operation exception
     */
    @VisibleForTesting
    int getNextDatastreamIndex(CellarResource manifestation, Set<String> itemCellarIds) throws S3OperationException {
        Map<String, Object> metadata = contentStreamService.getMetadata(manifestation.getCellarId(), manifestation.getVersion(DIRECT).orElse(null), DIRECT);
        Object index = metadata.getOrDefault(ContentStreamService.Metadata.MANIFESTATION_LAST_INDEX, String.valueOf(lastIndex(itemCellarIds)));
        int idx = Integer.parseInt((String) index);
        return createOrUpdateDatastreamDocumentIndex(manifestation, idx);
    }

    private int lastIndex(Set<String> itemCellarIds) throws S3OperationException {
        return itemCellarIds.stream()
                .map(id -> StringUtils.substringAfter(id, CONTENT_PREFIX))
                .mapToInt(Integer::parseInt)
                .max()
                .orElse(0);
    }

    /**
     * Creates the or update datastream document index.
     *
     * @param index the index
     */
    private int createOrUpdateDatastreamDocumentIndex(final CellarResource manifestation, final int index) {
        String idx = Integer.toString(index + 1);
        Map<String, String> metadata = Collections.singletonMap(ContentStreamService.Metadata.MANIFESTATION_LAST_INDEX, idx);
        String newVersion = contentStreamService.updateMetadata(manifestation.getCellarId(), manifestation.getVersion(DIRECT).orElse(null), DIRECT, metadata);
        manifestation.getVersions().put(DIRECT, newVersion);
        return index + 1;
    }

    /**
     * Check read only on itself.
     *
     * @param currDigitalObject the curr digital object
     * @param identifier        the identifier
     * @param contentids        the contentids
     */
    protected void checkReadOnlyOnItself(final DigitalObject currDigitalObject, final String identifier,
                                         final List<? extends Object> contentids) {
        final Boolean readOnly = cellarIdentifierService.isReadOnly(identifier);
        if (readOnly) {
            final ExceptionBuilder<StructMapProcessorException> exceptionBuilder = ExceptionBuilder.get(StructMapProcessorException.class);
            exceptionBuilder.withCode(CoreErrors.E_391);
            exceptionBuilder.withMessage("Cannot update {} - [{}|{}] as it is marked as read only.");
            exceptionBuilder.withMessageArgs(currDigitalObject.getType().toString(), identifier, Arrays.toString(contentids.toArray()));
            throw exceptionBuilder.build();
        }
    }

    /**
     * Check read only on sub tree.
     *
     * @param currDigitalObject the curr digital object
     * @param identifier        the identifier
     * @param contentids        the contentids
     */
    protected void checkReadOnlyOnSubTree(final DigitalObject currDigitalObject, final String identifier,
                                          final List<? extends Object> contentids) {
        final Boolean anyChildReadOnly = cellarIdentifierService.isAnyChildReadOnly(currDigitalObject);
        if (anyChildReadOnly) {
            final ExceptionBuilder<StructMapProcessorException> exceptionBuilder = ExceptionBuilder.get(StructMapProcessorException.class);
            exceptionBuilder.withCode(CoreErrors.E_391);
            exceptionBuilder.withMessage("Cannot update {} - [{}|{}] as a child element or itself is marked as read only.");
            exceptionBuilder.withMessageArgs(currDigitalObject.getType().toString(), identifier, Arrays.toString(contentids.toArray()));
            throw exceptionBuilder.build();
        }
    }

    protected void checkDate(final ProductionIdentifier pid, final DigitalObject digitalObject) {
        final Calendar aLMD = Calendar.getInstance();
        try {
            Map<String, Object> metadata = contentStreamService.getMetadata(pid.getCellarIdentifier().getUuid(), digitalObject.getVersions().get(DIRECT), DIRECT);
            Date currentDate = (Date) metadata.get(Headers.LAST_MODIFIED);
            if (currentDate != null) {
                aLMD.setTime(currentDate);
            }
        } catch (final Exception ex) {
            throw ExceptionBuilder.get(StructMapProcessorException.class).withCode(CoreErrors.E_117)
                    .withMessage("Unable to retrieve the date of [" + pid.getCellarIdentifier().getUuid() + "]").build();
        }
        if (digitalObject.getBusinessMetadata() != null) {
            final Date currentDMDLastModificationDate = digitalObject.getBusinessMetadata().getDigitalObject().getLastModificationDate();
            final Calendar cLMD = Calendar.getInstance();
            if (currentDMDLastModificationDate != null) {
                cLMD.setTime(currentDMDLastModificationDate);
            }
            //if same date...then don't update
            if (aLMD.equals(cLMD)) {
                throw ExceptionBuilder.get(StructMapProcessorException.class).withCode(CoreErrors.E_100)
                        .withMessage("Date mismatch [" + aLMD.getTime() + " / " + cLMD.getTime() + "]").build();
            }
        }

    }

}
