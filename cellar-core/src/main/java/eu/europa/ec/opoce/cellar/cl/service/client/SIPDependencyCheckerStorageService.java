/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.service.client
 *        FILE : SIPDependencyCheckerStorageService.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 03-12-2021
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2021 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.client;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResourceDependencies;
import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.SIPPackage;
import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.SIPPackageProcessingStatus;

/**
 * Interface providing access to the SIP Dependency Checker Storage Service,
 * whose purpose is to provide access to the DB tables related to the
 * SIP Dependency Checker Service.
 * @author EUROPEAN DYNAMICS S.A.
 */
public interface SIPDependencyCheckerStorageService {
	
	/**
	 * Stores the results of the dependency checker in the database.
	 * @param sipResourceDependencies the object containing the results
	 * of the dependency checking procedure.
	 */
	void storeResults(SIPResourceDependencies sipResourceDependencies);
	
	/**
	 * Retrieves the SIP package from the database that matches the provided
	 * SIP name.
	 * NOTE: Does not pre-initialize the 'SIPPackage.relatedProdIds' collection,
	 * which is Lazy; this method should not be used for accessing the children
	 * elements.
	 * @param sipName the name of the SIP package to retrieve.
	 * @return the matching SIP package.
	 */
	SIPPackage findSipBySipName(String sipName);
	
	/**
	 * Synchronizes the database tables with the ingestion folders
	 * by removing from the tables the SIP entries that do not correspond to the
	 * provided collection of SIP names, while also updating the package references
	 * of any other package affected by their removal.
	 * The tables that are synchronized are SIP_PACKAGE, RELATION_PROD_ID, and PACKAGE_HAS_PARENT_PACKAGE.
	 * @param packagesToDelete the set of packages that will be deleted.
	 */
	void syncDatabaseTables(Set<SIPPackage> packagesToDelete);
	
	/**
	 * Retrieves the SIP packages whose status is one of the provided ones.
	 * @param sipStatuses the SIP statuses to look for.
	 * @return the matching SIP packages.
	 */
	List<SIPPackage> findPackagesWithStatus(Collection<SIPPackageProcessingStatus> sipStatuses);
	
	/**
	 * Sets the status of the defined SIP to the status provided.
	 * @param sipName the name of the SIP package to update its status.
	 * @param sipStatus the status to set.
	 * @return true if the corresponding SIP package was found and updated, false otherwise.
	 */
	boolean updatePackageStatus(String sipName, SIPPackageProcessingStatus sipStatus);
	
	/**
	 * Retrieves all packages from the DB.
	 * @return all packages as a list.
	 */
	List<SIPPackage> findAll();
	
	/**
	 * Retrieves the SIP packages having a given status,
	 * and sets their status to the provided one.
	 * @param sipStatusesToFind the SIP statuses to look for.
	 * @param sipStatusToSet the status to set.
	 */
	void updatePackagesStatus(Collection<SIPPackageProcessingStatus> sipStatusesToFind, SIPPackageProcessingStatus sipStatusToSet);
	
}
