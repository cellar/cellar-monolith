package eu.europa.ec.opoce.cellar.cl.service.mail;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;

import java.util.Properties;

@Configuration
public class MailConfiguration {
	
	/**
	 * 
	 */
    private static final String AUTH_ENABLED = "mail.smtp.auth";
    /**
     * 
     */
    private static final String STARTTLS_ENABLED = "mail.smtp.starttls.enable";
    /**
     * 
     */
    private static final String STARTTLS_REQUIRED = "mail.smtp.starttls.required";
    
    
    /**
     * 
     */
    private final ICellarConfiguration cellarConfiguration;
    
    
    @Autowired
    public MailConfiguration(@Qualifier("cellarStaticConfiguration") ICellarConfiguration cellarConfiguration) {
        this.cellarConfiguration = cellarConfiguration;
    }

    @Bean
    public JavaMailSender javaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        initMailSender(mailSender);
        return mailSender;
    }
    
    private void initMailSender(JavaMailSenderImpl mailSender) {
        boolean isAuthEnabled = this.cellarConfiguration.getCellarMailSmtpAuth();
        mailSender.setHost(this.cellarConfiguration.getCellarMailHost());
        mailSender.setPort(this.cellarConfiguration.getCellarMailPort());
        if (isAuthEnabled) {
        	mailSender.setUsername(this.cellarConfiguration.getCellarMailSmtpUser());
        	mailSender.setPassword(this.cellarConfiguration.getCellarMailSmtpPassword());
        }
        Properties props = mailSender.getJavaMailProperties();
        props.put(AUTH_ENABLED, isAuthEnabled);
        props.put(STARTTLS_ENABLED, this.cellarConfiguration.getCellarMailSmtpStarttlsEnable());
        props.put(STARTTLS_REQUIRED, this.cellarConfiguration.getCellarMailSmtpStarttlsRequired());
    }
    
}
