/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.virtuoso.service.impl
 *             FILE : VirtuosoExceptionMessagesLoaderServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 18 août 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.virtuoso.service.impl;

import eu.europa.ec.opoce.cellar.cl.domain.virtuoso.VirtuosoExceptionMessages;
import eu.europa.ec.opoce.cellar.cl.domain.virtuoso.service.VirtuosoExceptionMessagesLoaderService;
import eu.europa.ec.opoce.cellar.cl.domain.virtuoso.service.VirtuosoExceptionMessagesService;
import eu.europa.ec.opoce.cellar.support.cache.AbstractCachedTableRecordsService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * The Class VirtuosoExceptionMessagesLoaderServiceImpl.
 * <class_description> This service should be used to check if a given virtuoso exception message should trigger the retry strategy
 * <br/><br/>
 * ON : 18 août 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Transactional
@Service
public class VirtuosoExceptionMessagesLoaderServiceImpl extends AbstractCachedTableRecordsService
        implements VirtuosoExceptionMessagesLoaderService {

    private static final Logger LOG = LogManager.getLogger(VirtuosoExceptionMessagesLoaderServiceImpl.class);

    /** The cached messages. */
    private Set<String> cachedMessages;

    /** The virtuoso exception messages service. */
    @Autowired
    private VirtuosoExceptionMessagesService virtuosoExceptionMessagesService;

    /** {@inheritDoc} */
    @Override
    public boolean containsSimilarMessage(final String exceptionMessage) {
        boolean result = false;
        rwl.readLock().lock();
        try {
            this.reloadIfNecessary();
            final List<String> messages = cachedMessages.stream()
                    .filter(message -> message.contains(exceptionMessage) || exceptionMessage.contains(message))
                    .collect(Collectors.toList());
            result = !messages.isEmpty();
        } finally {
            if (this.rwl != null && this.rwl.readLock() != null) {
                this.rwl.readLock().unlock();
            }
        }
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public Set<String> getAllMessages() {
        Set<String> result = null;
        rwl.readLock().lock();
        try {
            this.reloadIfNecessary();
            result = Collections.unmodifiableSet(cachedMessages);
        } finally {
            if (this.rwl != null && this.rwl.readLock() != null) {
                this.rwl.readLock().unlock();
            }
        }
        return result;
    }

    /** {@inheritDoc} */
    @Override
    protected String getCachePeriod() {
        final String result = this.cellarConfiguration.getVirtuosoIngestionBackupExceptionMessagesCachePeriod();
        return result;
    }

    /** {@inheritDoc} */
    @Override
    protected boolean canReload() {
        final boolean result = this.cachedMessages != null && !this.cachedMessages.isEmpty();
        return result;
    }

    /** {@inheritDoc} */
    @Override
    protected boolean shouldReload() {
        final boolean result = this.cachedMessages == null || this.cachedMessages.isEmpty();
        return result;
    }

    /** {@inheritDoc} */
    @Override
    protected void reload() {
        LOG.info("The Virtuoso exception messages will be reloaded");
        try {
            this.initialize();
        } catch (final Exception e) {
            LOG.warn("It was not possible to re-load the Virtuoso exception messages", e);
            LOG.warn("The previously loaded Virtuoso exception messages will be used");
        }

    }

    /** {@inheritDoc} */
    @Override
    protected void initialize() {
        final List<VirtuosoExceptionMessages> virtuosoExceptionMessages = virtuosoExceptionMessagesService.findAll();
        final Set<String> messages = virtuosoExceptionMessages.stream()
                .map(virtuosoExceptionMessage -> virtuosoExceptionMessage.getMessage()).collect(Collectors.toSet());
        cachedMessages = Collections.synchronizedSet(messages);
        updateLastModificationDate();
    }

}
