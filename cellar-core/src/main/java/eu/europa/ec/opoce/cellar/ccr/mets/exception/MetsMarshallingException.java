/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.ccr.mets.exception
 *        FILE : MetsMarshallingException.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 30-01-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.mets.exception;

import eu.europa.ec.opoce.cellar.exception.BuildableException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.MetsException;

/**
 * This is a {@link MetsMarshallingException} buildable by an {@link ExceptionBuilder}.</br>
 * It is usually thrown when a given object fails to marshall from a given source mets file.</br>
 * </br>
 * ON : 30-01-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public class MetsMarshallingException extends MetsException {

    private static final long serialVersionUID = 1L;

    /**
     * Constructs a new exception with its associated builder.
     * 
     * @param builder the builder to use for building the exception
     */
    public MetsMarshallingException(final ExceptionBuilder<? extends BuildableException> builder) {
        super(builder);
    }

}
