package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.ccr.service.database.DefaultTransactionService;
import eu.europa.ec.opoce.cellar.cl.dao.ValidatorRuleDao;
import eu.europa.ec.opoce.cellar.cl.domain.admin.ValidatorRule;
import eu.europa.ec.opoce.cellar.cl.service.client.ValidatorRuleService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of the {@link ValidatorRuleService}. This service support transactions.
 * 
 * @author dcraeye
 * 
 */
@Service
public class ValidatorRuleServiceImpl extends DefaultTransactionService implements ValidatorRuleService {

    /**
     * The {@link ValidatorRuleDao}.
     */
    @Autowired
    private ValidatorRuleDao validatorRuleDao;

    private static Map<String, ValidatorRule> cache = null;

    @PostConstruct
    public void initialize() {
        List<ValidatorRule> list = validatorRuleDao.getRules();
        cache = new HashMap<String, ValidatorRule>(list.size());

        for (ValidatorRule validatorRule : list) {
            cache.put(validatorRule.getValidatorClass(), validatorRule);
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ValidatorRule> getRules() {
        return new ArrayList<ValidatorRule>(cache.values());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void createRule(final ValidatorRule rule) {
        validatorRuleDao.createRule(rule);

        cache.put(rule.getValidatorClass(), rule);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ValidatorRule getRule(final Long id) {
        return validatorRuleDao.getRule(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ValidatorRule getRuleByValidatorClass(final String validatorClass) {
        return cache.get(validatorClass);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ValidatorRule> getSystemValidators() {
        return validatorRuleDao.getSystemValidators();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ValidatorRule> getCustomValidators() {
        return validatorRuleDao.getCustomValidators();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void updateRule(final ValidatorRule rule) {
        validatorRuleDao.updateRule(rule);

        cache.put(rule.getValidatorClass(), rule);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void deleteRule(final ValidatorRule rule) {
        validatorRuleDao.deleteRule(rule);

        cache.remove(rule.getValidatorClass());
    }

}
