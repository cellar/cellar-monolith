package eu.europa.ec.opoce.cellar.cl.domain.response;

public class AppendStructMapOperation extends StructMapOperation {

    /** class's uid. */
    private static final long serialVersionUID = 8326607516293707539L;

    public AppendStructMapOperation() {
        super();
        this.setOperationType(ResponseOperationType.APPEND);
    }

    public AppendStructMapOperation(String id) {
        super(ResponseOperationType.APPEND, id);
    }

}
