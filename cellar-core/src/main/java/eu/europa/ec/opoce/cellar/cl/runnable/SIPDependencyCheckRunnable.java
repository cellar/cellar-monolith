/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.runnable
 *        FILE : SIPDependencyCheckRunnable.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 29-11-2021
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2021 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.runnable;

import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.SIP_DEPENDENCY_CHECKER;

import java.io.File;
import java.util.Date;
import java.util.UUID;
import java.util.concurrent.ConcurrentMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditableAdvice;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResourceDependencies;
import eu.europa.ec.opoce.cellar.cl.service.client.SIPDependencyChecker;
import eu.europa.ec.opoce.cellar.cl.service.client.SIPDependencyCheckerStorageService;
import eu.europa.ec.opoce.cellar.cl.service.impl.SIPQueueManagerProxyImpl;
import eu.europa.ec.opoce.cellar.common.concurrent.PriorityRunnable;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork;
import eu.europa.ec.opoce.cellar.logging.LogContext;

/**
 * 
 * @author EUROPEAN DYNAMICS S.A.
 */
@LogContext
public class SIPDependencyCheckRunnable implements PriorityRunnable {

	private static final Logger LOG = LogManager.getLogger(SIPDependencyCheckRunnable.class);
	
	private final SIPWork sipWork;
	private final SIPDependencyChecker sipDependencyChecker;
	private final SIPDependencyCheckerStorageService sipDependencyCheckerStorageService;
	private final ConcurrentMap<SIPWork, Boolean> resourceDiscoveryTreatments;
	private final SIPQueueManagerProxyImpl sipQueueManagerProxyImpl;
	private final ICellarConfiguration cellarConfiguration;
	
	private static final String SIP_DEPENDENCY_CHECK_KEY = "SIPDependencyCheck_key";
	
	
	public SIPDependencyCheckRunnable(SIPWork sipWork, SIPDependencyChecker sipDependencyChecker,
			SIPDependencyCheckerStorageService sipDependencyCheckerStorageService, ICellarConfiguration cellarConfiguration,
			SIPQueueManagerProxyImpl sipQueueManagerProxyImpl, ConcurrentMap<SIPWork, Boolean> resourceDiscoveryTreatments) {
		this.sipWork = sipWork;
		this.sipDependencyChecker = sipDependencyChecker;
		this.sipDependencyCheckerStorageService = sipDependencyCheckerStorageService;
		this.sipQueueManagerProxyImpl = sipQueueManagerProxyImpl;
		this.cellarConfiguration = cellarConfiguration;
		this.resourceDiscoveryTreatments = resourceDiscoveryTreatments;
	}
	
	@Override
	@LogContext(SIP_DEPENDENCY_CHECKER)
	public void run() {
		SIPResourceDependencies sipResourceDependencies;
		try {
			this.sipWork.setStartDate(new Date());
			final File sipFile = sipWork.getSipFile();
			setAuditContext(sipFile.getName());
			// Perform discovery of resource dependencies for the provided SIP.
			sipResourceDependencies = this.sipDependencyChecker.discoverResources(this.sipWork);
			if (sipResourceDependencies != null) {
				try {
					this.sipDependencyCheckerStorageService.storeResults(sipResourceDependencies);
					LOG.info("SIP resource dependency check finished successfully for {}.", this.sipWork);
					if (this.cellarConfiguration.isCellarServiceDorieDedicatedEnabled()) {
						LOG.debug("Invoking SIPQueueManager directly.");
						this.sipQueueManagerProxyImpl.prioritizePackagesOnInsertExclusivelyAsynchronously();
					}
				} catch (Exception e) {
					LOG.warn("An entry corresponding to SIP {} already exists in SIP_PACKAGE.", this.sipWork);
				}
			}
			else {
				LOG.error("SIP resource dependency check failed for {}.", this.sipWork);
			}
		} catch (Exception e) {
			// Nothing to do, exceptions should have been handled by the thread
            // running the dependency checking process.
            LOG.error("An exception was thrown during the SIP dependency checking process for {}.", this.sipWork, e);
		}
		finally {
			// Always cleanup the treatments map and the audit context.
			this.resourceDiscoveryTreatments.remove(this.sipWork);
			clearAuditContext();
		}
	}

	@Override
	public int compareTo(PriorityRunnable o) {
		SIPDependencyCheckRunnable other = (SIPDependencyCheckRunnable) o;
		return this.sipWork.compareTo(other.sipWork);
	}
	
    /**
     * Add a unique key to the process in order to debug it more
     * easily.
     *
     * @param sipFileName the name of the current SIP
     * @see <a href="https://logging.apache.org/log4j/2.x/manual/thread-context.html">Log4j2 ThreadContext</a>
     */
    private static void setAuditContext(String sipFileName) {
		ThreadContext.put(SIP_DEPENDENCY_CHECK_KEY, UUID.randomUUID().toString());
		ThreadContext.put("sip", sipFileName);
		ThreadContext.put(AuditableAdvice.AUDIT_KEY, ThreadContext.get(SIP_DEPENDENCY_CHECK_KEY));
    }

    /**
     * Clear the {@link ThreadContext}
     *
     * @see #setAuditContext(String)
     */
    private static void clearAuditContext() {
		ThreadContext.remove(SIP_DEPENDENCY_CHECK_KEY);
		ThreadContext.remove("sip");
		ThreadContext.remove(AuditableAdvice.AUDIT_KEY);
    }

}
