/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.dao
 *             FILE : BatchJobDao.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 11, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao;

import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob.BATCH_JOB_TYPE;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob.STATUS;

import java.util.List;

/**
 * <class_description> Batch job dao.
 *
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 11, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface BatchJobDao extends BaseDao<BatchJob, Long> {

    /**
     * Finds a batch job by job identifier and type.
     * @param id the identifier of the job
     * @param jobType the job type
     * @return a batch job if exists, otherwise null
     */
    BatchJob findBatchJobByIdType(final Long id, final BATCH_JOB_TYPE jobType);

    /**
     * Returns the first X jobs with the status <code>oldStatus</code> and updates this job status with <code>newStatus</code>.
     * The number of jobs updated is defined by the configuration property cellar.service.batchJob.threadPool.selectionSize
     * @param oldStatus the desired status
     * @param newStatus the new status
     * @return if a job exists with the desired status, the job. Otherwise, null.
     */
    List<BatchJob> updateFirstBatchJobs(final STATUS oldStatus, final STATUS newStatus);

    /**
     * Returns the first update jobs with the status <code>oldStatus</code> and cron <code>cron</code> and updates these jobs statuses with <code>newStatus</code>.
     * The number of jobs updated is defined by the configuration property cellar.service.batchJob.threadPool.selectionSize
     * @param oldStatus the desired status
     * @param newStatus the new status
     * @param cron the cron
     * @return if a job exists with the desired status, the job. Otherwise, null.
     */
    List<BatchJob> updateFirstUpdateBatchJobsByStatusCron(final STATUS oldStatus, final STATUS newStatus, final String cron);

    /**
     * Find batch jobs by status cron.
     *
     * @param status the status
     * @param cron the cron
     * @return the list
     */
    List<BatchJob> findBatchJobsByStatusCron(final STATUS status, final String cron);

    /**
     * Finds the jobs by type.
     * @param type the searched type
     * @return the jobs
     */
    List<BatchJob> findBatchJobsByType(final BATCH_JOB_TYPE type);

    /**
     * Finds all CRONs.
     * @return the CRONs
     */
    List<String> findUpdateBatchJobCrons();

    /**
     * Find batch jobs by status.
     *
     * @param status the status
     * @return the list
     */
    List<BatchJob> findBatchJobsByStatus(final STATUS status);

    /**
     * Find batch jobs with any status.
     *
     * @param statuses the statuses
     * @return the list
     */
    List<BatchJob> findBatchJobsWithAnyStatus(final STATUS... statuses);
}
