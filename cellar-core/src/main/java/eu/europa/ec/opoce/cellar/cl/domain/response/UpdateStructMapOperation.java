package eu.europa.ec.opoce.cellar.cl.domain.response;

public class UpdateStructMapOperation extends StructMapOperation {

    /** class's uid. */
    private static final long serialVersionUID = 3526607516473707124L;

    public UpdateStructMapOperation() {
        super();
        this.setOperationType(ResponseOperationType.UPDATE);
    }

    public UpdateStructMapOperation(final String id) {
        super(ResponseOperationType.UPDATE, id);
    }

}
