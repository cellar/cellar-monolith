/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.dao.impl
 *        FILE : StructMapBulkLowPriorityResponseDao.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A
 *          ON : 28-02-2022

 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2022 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.dao.StructMapResponseDao;

/**
 * <class_description> Implementation of the {@link StructMapResponseDao} for the bulk ingestions.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 */
@Repository("structMapBulkLowPriorityResponseDao")
public class StructMapBulkLowPriorityResponseDao extends AbstractStructMapResponseDao {
    private ICellarConfiguration cellarConfiguration;

    @Autowired
    public StructMapBulkLowPriorityResponseDao(@Qualifier("cellarConfiguration") ICellarConfiguration cellarConfiguration) {
        this.cellarConfiguration = cellarConfiguration;
    }

    protected String resolveResponseFolderPath() {
        return this.cellarConfiguration.getCellarFolderBulkLowPriorityResponse();
    }
}
