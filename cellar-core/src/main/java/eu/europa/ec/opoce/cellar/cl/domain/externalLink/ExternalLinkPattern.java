/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.externalLink
 *             FILE : ExternalLink.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 14, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.externalLink;

import java.util.regex.Pattern;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

/**
 * <class_description> Class that represents the match pattern to find the URL pattern.  The url pattern is used to generate the external link.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 14, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Entity
@Table(name = "EXTERNAL_LINK_PATTERN")
@XmlAccessorType(XmlAccessType.PROPERTY)
@XmlType(name = "externalLinkPatternType")
@XmlRootElement(name = "externalLinkPattern")
public class ExternalLinkPattern {

    /** 
     * Internal id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    /**
     * The math pattern of this link.
     */
    @Column(name = "MATCH_PATTERN")
    private String matchPattern;

    /**
     * The URL pattern of this link.
     */
    @Column(name = "URL_PATTERN")
    private String urlPattern;

    /**
     * The match pattern object used to check the compatibility.
     */
    @Transient
    private Pattern matchPatternObject;

    /**
     * Constructor.
     */
    public ExternalLinkPattern() {
    }

    /**
     * Constructor.
     * @param id the unique identifier of the pattern
     * @param matchPattern the regex match pattern
     * @param urlPattern the URL pattern
     */
    public ExternalLinkPattern(final Long id, final String matchPattern, final String urlPattern) {
        this.id = id;
        this.matchPattern = matchPattern;
        this.urlPattern = urlPattern;
    }

    /**
     * Gets the unique identifier.
     * @return the identifier
     */
    @XmlElement(name = "id")
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the unique identifier
     * @param id the identifier to set
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the regex match pattern.
     * @return the regex match pattern
     */
    @XmlElement(name = "matchPattern")
    public String getMatchPattern() {
        return this.matchPattern;
    }

    /**
     * Sets the regex match pattern.
     * @param matchPattern the regex match pattern to set
     */
    public void setMatchPattern(final String matchPattern) {
        this.matchPattern = matchPattern;
    }

    /**
     * Gets the URL pattern used to generate external links.
     * @return the URL pattern
     */
    @XmlElement(name = "urlPattern")
    public String getUrlPattern() {
        return this.urlPattern;
    }

    /**
     * Sets the URL pattern used to generate external links.
     * @param urlPattern the URL pattern to set
     */
    public void setUrlPattern(final String urlPattern) {
        this.urlPattern = urlPattern;
    }

    /**
     * Gets the regex match pattern object.
     * @return the pattern object
     */
    @XmlTransient
    public Pattern getMatchPatternObject() {
        return this.matchPatternObject;
    }

    /**
     * Sets the regex match pattern object.
     * @return the pattern object to set
     */
    public void setMatchPatternObject(final Pattern matchPatternObject) {
        this.matchPatternObject = matchPatternObject;
    }

    /**
     * Construct/Init the regex match pattern object.
     */
    public void constructMatchPatternObject() {
        this.matchPatternObject = Pattern.compile(this.matchPattern);
    }
}
