/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *             FILE : ConcurrencyControllerImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 30, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import com.google.common.base.Stopwatch;
import eu.europa.ec.opoce.cellar.cl.concurrency.*;
import eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLock.MODE;
import eu.europa.ec.opoce.cellar.cl.concurrency.impl.*;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditBuilder;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventAction;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventType;
import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;
import eu.europa.ec.opoce.cellar.cl.service.client.IConcurrencyController;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.graph.IGraph;
import eu.europa.ec.opoce.cellar.cmr.graph.impl.BaseAndInverseRelationsGraphBuilder;
import eu.europa.ec.opoce.cellar.cmr.graph.impl.GraphBuilder;
import eu.europa.ec.opoce.cellar.cmr.graph.impl.IngestionGraphBuilder;
import eu.europa.ec.opoce.cellar.cmr.graph.impl.ReadGraphBuilder;
import eu.europa.ec.opoce.cellar.common.factory.IFactory;
import eu.europa.ec.opoce.cellar.common.spring.CloseableConcurrentTaskScheduler;
import eu.europa.ec.opoce.cellar.common.util.CellarIdUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsPackage;
import eu.europa.ec.opoce.cellar.domain.content.mets.OffIngestionOperationType;
import eu.europa.ec.opoce.cellar.domain.content.mets.OperationSubType;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

/**
 * <class_description> Concurrency controller implementation.
 * <br/><br/>
 * ON : Apr 30, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class ConcurrencyControllerImpl implements IConcurrencyController {

    private static final Logger LOG = LogManager.getLogger(ConcurrencyControllerImpl.class);

    private final ReentrantLock nonIndexingLock = new ResourceAcquisitionLock(true, CellarLockManagerType.NON_INDEXING);
    
    private final ReentrantLock indexingOnlyLock = new ResourceAcquisitionLock(true, CellarLockManagerType.INDEXING_ONLY);

    /**
     * The Cellar configuration storing runtime parameters.
     */
    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    /**
     * Identifier manager service.
     */
    @Autowired
    @Qualifier("pidManagerService")
    private IdentifierService identifierService;

    /**
     * Graph builder's factory.
     */
    @Autowired
    @Qualifier("ingestionGraphBuilderFactory")
    private IFactory<IngestionGraphBuilder, OperationSubType> ingestionGraphBuilderFactory;

    /**
     * Generic Cellar Locks Manager for all operation types except for Indexation.
     */
    private final CellarLocksManager nonIndexingCellarLockManager;
    
    /**
     * Indexing-specific Cellar Locks Manager.
     */
    private final CellarLocksManager indexingOnlyCellarLockManager;

    /**
     * Threads statistics manager.
     */
    private final ICellarThreadsManager cellarThreadsManager;

    private CloseableConcurrentTaskScheduler autoLoggingScheduler;
    private ScheduledFuture<?> autoLoggingFuture;

    /**
     * Default constructor.
     */
    public ConcurrencyControllerImpl() {
        this.nonIndexingCellarLockManager = new CellarLocksManager();
        this.indexingOnlyCellarLockManager = new CellarLocksManager();
        this.cellarThreadsManager = new CellarThreadsManager();
    }

    /**
     * Initialization method.
     */
    @PostConstruct
    public void initialize() {
        if (this.cellarConfiguration.isCellarServiceIngestionConcurrencyControllerAutologgingEnabled()) {
            final ConcurrencyLoggingScheduler loggingScheduler = new ConcurrencyLoggingScheduler(this.nonIndexingCellarLockManager,
            		this.indexingOnlyCellarLockManager, this.cellarThreadsManager);
            autoLoggingScheduler = CloseableConcurrentTaskScheduler.newScheduler("concurrency-controller");
            autoLoggingFuture = autoLoggingScheduler.schedule(loggingScheduler,
                    new CronTrigger(this.cellarConfiguration.getCellarServiceIngestionConcurrencyControllerAutologgingCronSettings()));
            LOG.info(IConfiguration.CONFIG, "Concurrency controller auto-logging started.");
        } else {
            LOG.info(IConfiguration.CONFIG, "Concurrency controller auto-logging disabled.");
        }
    }

    @PreDestroy
    public void preDestroy() {
        if (autoLoggingScheduler != null) {
            autoLoggingFuture.cancel(false);
            autoLoggingScheduler.shutdownNow();
        }
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.client.IConcurrencyController#lockForIngestion(eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLockSession, eu.europa.ec.opoce.cellar.domain.content.mets.MetsPackage, eu.europa.ec.opoce.cellar.domain.content.mets.StructMap)
     */
    @Override
    public void lockForIngestion(final ICellarLockSession cellarLockSession, final MetsPackage metsPackage, final StructMap structMap)
            throws InterruptedException {
        final IngestionGraphBuilder graphBuilder = this.ingestionGraphBuilderFactory.create(structMap.resolveOperationSubType())
                .withStructMap(structMap).withMetsPackage(metsPackage).initialize();

        try {
            if (!(graphBuilder instanceof ReadGraphBuilder)) {
                this.lock(cellarLockSession, graphBuilder, CellarLockingStrategy.INGESTION_LOCKING_STRATEGY, CellarLockManagerType.NON_INDEXING);
            }
        } finally {
            graphBuilder.closeQuietly();
        }

        this.cellarThreadsManager.incWorkingThreadsCount(cellarLockSession);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.client.IConcurrencyController#lockForEmbargo(eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLockSession, java.lang.String)
     */
    @Override
    public void lockForEmbargo(final ICellarLockSession cellarLockSession, final String cellarId) throws InterruptedException {
        this.lockRoot(cellarLockSession, cellarId, CellarLockManagerType.NON_INDEXING, MODE.WRITE);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.client.IConcurrencyController#lockForExport(eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLockSession, java.lang.String)
     */
    @Override
    public void lockForExport(ICellarLockSession cellarLockSession, String cellarId) throws InterruptedException {
        this.lockRoot(cellarLockSession, cellarId, CellarLockManagerType.NON_INDEXING, MODE.READ);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.client.IConcurrencyController#lockForCleaning(eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLockSession, java.lang.String)
     */
    @Override
    public void lockForCleaning(final ICellarLockSession cellarLockSession, final String cellarId) throws InterruptedException {
        this.lockBaseAndInverseRelations(cellarLockSession, cellarId, CellarLockingStrategy.CLEANING_LOCKING_STRATEGY, CellarLockManagerType.NON_INDEXING);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.client.IConcurrencyController#lockForCleaningScheduling(eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLockSession, java.lang.String)
     */
    @Override
    public void lockForCleaningScheduling(final ICellarLockSession cellarLockSession, final String cellarId) throws InterruptedException {
        this.lockRoot(cellarLockSession, cellarId, CellarLockManagerType.NON_INDEXING, MODE.WRITE);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.client.IConcurrencyController#lockForAligning(eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLockSession, java.lang.String)
     */
    @Override
    public void lockForAligning(final ICellarLockSession cellarLockSession, final String cellarId) throws InterruptedException {
        this.lockBaseAndInverseRelations(cellarLockSession, cellarId, CellarLockingStrategy.ALIGNING_LOCKING_STRATEGY, CellarLockManagerType.NON_INDEXING);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.client.IConcurrencyController#lockForAligningScheduling(eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLockSession, java.lang.String)
     */
    @Override
    public void lockForAligningScheduling(final ICellarLockSession cellarLockSession, final String cellarId) throws InterruptedException {
        this.lockRoot(cellarLockSession, cellarId, CellarLockManagerType.NON_INDEXING, MODE.WRITE);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.client.IConcurrencyController#lockForIndexing(eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLockSession, java.lang.String)
     */
    @Override
    public void lockForIndexing(final ICellarLockSession cellarLockSession, final String cellarIdUri) throws InterruptedException {
        this.lockForIndexingEmbedding(cellarLockSession, cellarIdUri, CellarLockingStrategy.INDEXING_LOCKING_STRATEGY, CellarLockManagerType.INDEXING_ONLY);
    }
    
    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.client.IConcurrencyController#lockForEmbeddedNoticeSynchronization(eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLockSession, java.lang.String)
     */
    @Override
    public void lockForEmbeddedNoticeSynchronization(final ICellarLockSession cellarLockSession, final String cellarIdUri) throws InterruptedException {
        this.lockForIndexingEmbedding(cellarLockSession, cellarIdUri, CellarLockingStrategy.INDEXING_LOCKING_STRATEGY, CellarLockManagerType.NON_INDEXING);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.client.IConcurrencyController#lockForVirtuosoBackup(eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLockSession, java.lang.String)
     */
    @Override
    public void lockForVirtuosoBackup(final ICellarLockSession cellarLockSession, final String cellarId) throws InterruptedException {
        this.lockRoot(cellarLockSession, cellarId, CellarLockManagerType.NON_INDEXING, MODE.WRITE);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.client.IConcurrencyController#unlock(eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLockSession)
     */
    @Override
    public void unlock(final ICellarLockSession cellarLockSession) {
        if (cellarLockSession == null) {
            return;
        }
        
        if (cellarLockSession.isWorking()) {
            this.cellarThreadsManager.decWorkingThreadsCount(cellarLockSession);
            //Log the end of process
            AuditBuilder.get(cellarLockSession.getLocker()).withAction(AuditTrailEventAction.ConcurrencyControllerLocking)
                    .withType(AuditTrailEventType.End).withDuration(System.currentTimeMillis() - cellarLockSession.getLockAcquisitionTime())
                    .withMessage("The concurrency controller unlocks session {}.")
                    .withMessageArgs(cellarLockSession)
                    .withLogger(LOG).withLogDatabase(false).logEvent();
        }

    	// Select the appropriate Cellar lock manager based on the operation type.
    	CellarLockManagerType cellarLockManagerType = (cellarLockSession.getLocker() == OffIngestionOperationType.INDEXING) ? 
    			CellarLockManagerType.INDEXING_ONLY : CellarLockManagerType.NON_INDEXING;
        
        this.unlock(cellarLockSession, cellarLockManagerType, cellarLockSession.getReadCellarLocksPath(), MODE.READ);
        this.unlock(cellarLockSession, cellarLockManagerType, cellarLockSession.getWriteCellarLocksPath(), MODE.WRITE);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.client.IConcurrencyController#getConcurrencyControllerStatistics()
     */
    @Override
    public Map<String, List<ICellarLockStatistic>> getConcurrencyControllerStatistics() {
    	Map<String, List<ICellarLockStatistic>> allCellarLockStatistics = new HashMap<>();
    	for (Map.Entry<String, List<ICellarLockStatistic>> entry : this.nonIndexingCellarLockManager.getCellarLockStatistics().entrySet()) {
    		allCellarLockStatistics.put("Non-Indexing CellarLockManager " + entry.getKey(), entry.getValue());
    	}
    	for (Map.Entry<String, List<ICellarLockStatistic>> entry : this.indexingOnlyCellarLockManager.getCellarLockStatistics().entrySet()) {
    		allCellarLockStatistics.put("Indexing-only CellarLockManager " + entry.getKey(), entry.getValue());
    	}
    	return allCellarLockStatistics;
    }

    /**
     * Lock the resources corresponding to the struct map and the corresponding relations.
     *
     * @param cellarLockSession the lock session
     * @param graphBuilder the graph builder to use to determine base and relations
     * @param cellarLockingStrategy the locking strategy to apply
     * @throws InterruptedException
     */
    private void lock(final ICellarLockSession cellarLockSession, final GraphBuilder graphBuilder,
                      final ICellarLockingStrategy cellarLockingStrategy, CellarLockManagerType cellarLockManagerType) 
                    		  throws InterruptedException {
        final IGraph graph = graphBuilder.getGraph();
        graphBuilder.buildBase(); // build the graph base
        ReentrantLock lock = this.getResourceAcquisitionLock(cellarLockManagerType);
        long counter = 0;

        // Delay for the thread to sleep between unsuccessful resource-locking attempts.
        long threadSleepDuration = this.cellarConfiguration.getCellarServiceIngestionConcurrencyControllerThreadDelayMilliseconds();
        boolean isResourceLocksAcquisitionSuccess = false;

        //Log the start of process
        AuditBuilder.get(cellarLockSession.getLocker()).withAction(AuditTrailEventAction.AcquireMainLock)
                .withType(AuditTrailEventType.Start)
                .withMessage("Attempting to acquire main-lock for resource {}.")
                .withMessageArgs(graph.getRoot())
                .withLogger(LOG).withLogDatabase(false).logEvent();
        Stopwatch externalStopwatch = Stopwatch.createStarted();
        cellarThreadsManager.incWaitingThreadsCount(cellarLockSession);
        o: while (true) {
            counter++;
            Stopwatch internalStopwatch = Stopwatch.createStarted();
            // Main-lock; locks the resource-locking phase so that it is always performed by a single thread at a time.
            lock.lock();
            try {
            	// Resource-locking phase
                LOG.debug("Main-lock acquired in {}sec, attempt {}", internalStopwatch.elapsed(TimeUnit.SECONDS), counter);
                // Try to lock root. Otherwise release all locks and retry.
            	if (!this.tryLockResource(graph.getRoot(), cellarLockSession, cellarLockManagerType, cellarLockingStrategy.getRootLockingMode())) {
            		LOG.debug("Could not lock root resource {}. Releasing locks.", graph.getRoot());
            		continue o;
            	}
            	// If root was successfully locked, try to lock all children; if unsuccessful, release all locks and retry.
	            for (final Identifier object : graph.getChildren()) {
	                if (!this.tryLockResource(object, cellarLockSession, cellarLockManagerType, cellarLockingStrategy.getChildrenLockingMode())) {
	                	LOG.debug("Could not lock child resource {}. Releasing locks.", object);
	                    continue o;
	                }
	            }
	            graphBuilder.buildRelations(); // build the graph relations
	            // If children were successfully locked, try to lock all relations; if unsuccessful, release all locks and retry.
	            for (final Identifier relation : graph.getRelations()) {
	                if (!this.tryLockResource(relation, cellarLockSession, cellarLockManagerType, cellarLockingStrategy.getRelationsLockingMode())) {
	                	LOG.debug("Could not lock relation resource {}. Releasing locks.", relation);
	                    continue o;
	                }
	            }
	            logLocksAcquisitionStatisticsOnLockingCompletion(cellarLockSession, counter, lock, externalStopwatch);
	            isResourceLocksAcquisitionSuccess = true;
            } catch (DataRetrievalFailureException e) {
        		LOG.warn("The requested table entries expected to exist could not be found. Retrying.", e);
            	unlock(cellarLockSession);
            	continue o;
            } finally {
            	// Always unlock the resource-locking phase.
            	lock.unlock();
            	// If the necessary resource-locks were not acquired, wait for the predefined period before retrying.
            	if(!isResourceLocksAcquisitionSuccess && (threadSleepDuration > 0)) {
            		LOG.debug("Could not acquire all resource-locks; waiting for {}ms before retrying.", threadSleepDuration);
            		Thread.sleep(threadSleepDuration);
            	}
            }
            //Log the start of process
            AuditBuilder.get(cellarLockSession.getLocker()).withAction(AuditTrailEventAction.ConcurrencyControllerLocking)
                    .withType(AuditTrailEventType.Start)
                    .withMessage("The concurrency controller has locked the objects {} and the relations {} on session {}.")
                    .withMessageArgs(graph.getObjects(), graph.getRelations(), cellarLockSession)
                    .withLogger(LOG).withLogDatabase(false).logEvent();

            cellarThreadsManager.decWaitingThreadsCount(cellarLockSession);
            return;
        }
    }

    /**
     * Try to lock the specified resource.  If the resource is already locked, the lock session is released before to wait the resource locked.
     * @param identifier the identifier of the concerned resource
     * @param cellarLockSession the current lock session
     * @param mode the locking mode to use. If the mode is DEFAULT, the default mode of the returned lock is used.
     * @param cellarLockManagerType the set of Cellar lock sessions that this lock session should be added to.
     * @return true if the resource is successfully locked. Otherwise, false.
     * @throws InterruptedException
     */
    private boolean tryLockResource(final Identifier identifier, final ICellarLockSession cellarLockSession, CellarLockManagerType cellarLockManagerType, final MODE mode) {
        final CellarLock lock = this.getIncrementedLock(identifier, cellarLockManagerType);

        if (!lock.tryLock(mode)) { // try to lock the resource
            // the resource is already in use
            this.unlock(cellarLockSession); // unlock the current path (resources already locked)
            storeConflictingLockersIds(cellarLockSession, lock); // store the locker IDs of the threads possessing the lock
            return false; // retry to lock the resources
        } else {
            lock.addLockSession(cellarLockSession);
        	cellarLockSession.addCellarLock(lock, mode); // the resource is available, we add the lock to the current path
        }

        return true;
    }

    /**
     * Lock the specified resource.
     * @param identifier the identifier of the concerned resource
     * @param cellarLockSession the current lock session
     * @param cellarLockManagerType the set of Cellar lock sessions that this lock session should be added to.
     * @param mode the locking mode to use
     * @throws InterruptedException
     */
    private void lockResource(final Identifier identifier, final ICellarLockSession cellarLockSession, final CellarLockManagerType cellarLockManagerType, final MODE mode)
            throws InterruptedException {
        final CellarLock lock = this.getIncrementedLock(identifier, cellarLockManagerType);

        this.cellarThreadsManager.incWaitingThreadsCount(cellarLockSession);
        try {
            LOG.debug("Attempting to lock root resource {}.", identifier);
        	lock.lock(mode); // try to lock the resource in order to wait the availability
        	LOG.debug("Root resource {} has been locked.", identifier);
        } finally {
            this.cellarThreadsManager.decWaitingThreadsCount(cellarLockSession);
        }
        lock.addLockSession(cellarLockSession);
        cellarLockSession.addCellarLock(lock, mode); // the resource is available, we add the lock to the current path
        //Log the start of process
        AuditBuilder.get(cellarLockSession.getLocker()).withAction(AuditTrailEventAction.ConcurrencyControllerLocking)
                .withType(AuditTrailEventType.Start)
                .withMessage("The concurrency controller locked on session {}.")
                .withMessageArgs(cellarLockSession)
                .withLogger(LOG).withLogDatabase(false).logEvent();
    }

    /**
     * Lock for indexing/embedding.
     *
     * @param cellarLockSession the current lock session corresponding to this struct map
     * @param cellarIdUri the URI of the root resource concerned by the indexing/embedding
     * @param cellarLockingStrategy the locking strategy to apply
     * @throws InterruptedException
     */
    private void lockForIndexingEmbedding(final ICellarLockSession cellarLockSession, final String cellarIdUri,
                                          final ICellarLockingStrategy cellarLockingStrategy, final CellarLockManagerType cellarLockManagerType) 
                                        		  throws InterruptedException {
        final String cellarId = this.identifierService.getCellarPrefixed(cellarIdUri);
        this.lockBaseAndInverseRelations(cellarLockSession, cellarId, cellarLockingStrategy, cellarLockManagerType);
    }

    private void lockRoot(final ICellarLockSession cellarLockSession, final String cellarId, final CellarLockManagerType cellarLockManagerType, final MODE mode) throws InterruptedException {
        final Identifier identifier = new Identifier(CellarIdUtils.getCellarIdBase(cellarId));
        this.lockResource(identifier, cellarLockSession, cellarLockManagerType, mode);
        this.cellarThreadsManager.incWorkingThreadsCount(cellarLockSession);
    }

    private void lockBaseAndInverseRelations(final ICellarLockSession cellarLockSession, final String cellarId,
                                             final ICellarLockingStrategy cellarLockingStrategy, final CellarLockManagerType cellarLockManagerType) 
                                            		 throws InterruptedException {
        final GraphBuilder graphBuilder = new BaseAndInverseRelationsGraphBuilder().withRootCellarId(cellarId).initialize();
        this.lock(cellarLockSession, graphBuilder, cellarLockingStrategy, cellarLockManagerType);
        this.cellarThreadsManager.incWorkingThreadsCount(cellarLockSession);
    }

    /**
     * Return the lock object corresponding to identifier.  The lock object is already incremented and ready to call the lock method.
     * @param identifier the identifier of the concerned resource
     * @param cellarLockManagerType the set of Cellar lock sessions that this Cellar lock session should be added to.
     * @return the incremented lock object
     */
    private CellarLock getIncrementedLock(final Identifier identifier, final CellarLockManagerType cellarLockManagerType) {
        CellarLocksManager cellarLockManager = null; 
    	switch (cellarLockManagerType) {
	        case INDEXING_ONLY:
	        	cellarLockManager = this.indexingOnlyCellarLockManager;
	        	break;
	        case NON_INDEXING:
	        default:
	        	cellarLockManager = this.nonIndexingCellarLockManager;
        }
    	if (identifier.cellarIdExists()) {
            return cellarLockManager.getIncrementedCellarIdLock(identifier);
        } else {
            return cellarLockManager.getIncrementedProductionIdsLock(identifier);
        }
    }

    /**
     * Unlocks the lock objects.
     * @param cellarLocks the lock objects to unlock
     * @param mode the locking mode to use
     */
    private void unlock(final ICellarLockSession cellarLockSession, final CellarLockManagerType cellarLockManagerType, final List<ICellarLock> cellarLocks, final MODE mode) {
        for (final ICellarLock cellarLock : cellarLocks) {
            this.unlock(cellarLockSession, cellarLockManagerType, cellarLock, mode);
        }
        cellarLocks.clear();
    }

    /**
     * Unlock the lock object.
     * @param cellarLock the lock object to unlock
     * @param mode the locking mode to use
     */
    private void unlock(final ICellarLockSession cellarLockSession, final CellarLockManagerType cellarLockManagerType, final ICellarLock cellarLock, final MODE mode) {
        CellarLocksManager cellarLockManager = null; 
    	switch (cellarLockManagerType) {
	        case INDEXING_ONLY:
	        	cellarLockManager = this.indexingOnlyCellarLockManager;
	        	break;
	        case NON_INDEXING:
	        default:
	        	cellarLockManager = this.nonIndexingCellarLockManager;
        }
    	if (cellarLock instanceof ProductionIdLock) {
    		cellarLockManager.unlock(cellarLockSession, (ProductionIdLock) cellarLock, mode);
        } else {
        	cellarLockManager.unlock(cellarLockSession, (CellarIdLock) cellarLock, mode);
        }
    }
    
    /**
     * Returns the appropriate main lock to be used during the resource-acquisition phase based on the
     * type of task requesting it, i.e Indexing task or other.
     * @param cellarLockManagerType the CellarLockManager used by the current thread.
     * @return the appropriate resource-acquisition lock.
     */
    private final ReentrantLock getResourceAcquisitionLock(CellarLockManagerType cellarLockManagerType) {
    	return (cellarLockManagerType == CellarLockManagerType.INDEXING_ONLY) ? this.indexingOnlyLock : this.nonIndexingLock;
    }
    
    /**
     * Retrieves the locker IDs of the threads that already possess the provided resource-lock
     * that the current thread failed to lock and stores them in the current thread's cellar lock session.
     * @param currentCellarLockSession the current thread cellar lock session.
     * @param lock the resource-lock that the current thread failed to acquire.
     */
    private void storeConflictingLockersIds(final ICellarLockSession currentCellarLockSession, final CellarLock lock) {
    	final Set<String> conflictingLockerIds = lock.getCellarLockSessions().stream()
    			.map(ICellarLockSession::getLockerId)
    			.collect(Collectors.toSet());
    	currentCellarLockSession.getConflictedLockerIds().addAll(conflictingLockerIds);
    }
    
    /**
     * Logs the statistics related to the resource acquisition phase upon successful completion.
     * @param cellarLockSession the cellar lock session.
     * @param iterationCounter the number of iterations that the main resource-lock acquisition phase.
     * performed before managing to successfully acquire all needed resource-locks.
     * @param lock the main-lock used for the resource acquisition phase.
     * @param externalStopwatch the stopwatch keeping track of the overall time it took for the current
     * thread to successfully complete the resource acquisition phase.
     */
    private void logLocksAcquisitionStatisticsOnLockingCompletion(final ICellarLockSession cellarLockSession, final long iterationCounter, final ReentrantLock lock, final Stopwatch externalStopwatch) {
    	Set<String> conflictedLockerIds = cellarLockSession.getConflictedLockerIds();
    	StringBuilder sb = new StringBuilder(); 
    	conflictedLockerIds.forEach(lockerId -> {
    		sb.append(lockerId);
    		sb.append(", ");
    		});
    	if (!conflictedLockerIds.isEmpty()) {
    		sb.delete(sb.length() - 2, sb.length());
    	}
        AuditBuilder.get(cellarLockSession.getLocker()).withAction(AuditTrailEventAction.AcquireMainLock)
	        .withType(AuditTrailEventType.End).withDuration(externalStopwatch.elapsed(TimeUnit.MILLISECONDS))
	        .withMessage("All resource-locks were acquired after {} attempts. Conflicted locker IDs: [{}]. Releasing {} main-lock. Overall time {}sec.")
	        .withMessageArgs(iterationCounter, sb, lock, externalStopwatch.elapsed(TimeUnit.SECONDS))
	        .withLogger(LOG).withLogDatabase(false).logEvent();
    }

}
