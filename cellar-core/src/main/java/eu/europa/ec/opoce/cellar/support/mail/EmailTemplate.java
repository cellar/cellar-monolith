package eu.europa.ec.opoce.cellar.support.mail;

public class EmailTemplate {


    private String bodyTemplate;
    private String titleTemplate;

    public String getBodyTemplate() {
        return this.bodyTemplate;
    }

    public void setBodyTemplate(String bodyTemplate) {
        this.bodyTemplate = bodyTemplate;
    }

    public String getTitleTemplate() {
        return this.titleTemplate;
    }

    public void setTitleTemplate(String titleTemplate) {
        this.titleTemplate = titleTemplate;
    }
}
