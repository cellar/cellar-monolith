package eu.europa.ec.opoce.cellar.support;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/** Helper class for manipulating files in a test environment.
 * @author phvdveld
 */
public final class TemporaryFileUtils {

    /** static LOGGER. */
    private static final Logger LOGGER = LoggerFactory.getLogger(TemporaryFileUtils.class);

    /** Length of a generated file. */
    private static final int FILE_LENGTH = 15;

    /** Hides the constructor. */
    private TemporaryFileUtils() {
    }

    /** Gets the temp directory.
     * @return a path string for the temp directory
     */
    private static String getTempDirectoryPath() {
        String tempdir = System.getProperty("java.io.tmpdir");

        if (!(tempdir.endsWith("/") || tempdir.endsWith("\\"))) {
            tempdir = tempdir + System.getProperty("file.separator");
        }
        return tempdir;
    }

    /** Gets the system's the temporary directory.
     * @return a {@link File}
     */
    public static File getTemporaryDirectory() {
        return new File(FilenameUtils.normalize(getTempDirectoryPath()));
    }

    /** Gets an new empty temporary File.
     * @return a {@link File}
     */
    public static File getTemporaryFile() {
        return getTemporaryFile("", ".tmp");
    }

    /** Gets an new empty temporary File.
     * @return a {@link File}
     */
    public static File getTemporaryFile(final String suffix) {
        return getTemporaryFile("", suffix);
    }

    /** Gets an new empty temporary File.
     * @return a {@link File}
     */
    public static File getTemporaryFile(final String prefix, final String suffix) {
        String fileName = getTempDirectoryPath() + prefix + RandomStringUtils.randomAlphanumeric(FILE_LENGTH) + suffix;
        File temporaryFile = new File(FilenameUtils.normalize(fileName));
        return temporaryFile;
    }

    /** Gets a temporary file containing the given string. The file is marked for deletion on exit.
     * @param data - the data to put in the temporary file
     * @return a File containing the given data
     */
    public static File toTemporaryFile(final String data) {
        File temporaryFile = getTemporaryFile();
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(new FileWriter(temporaryFile));
            writer.write(data);
            writer.flush();
        } catch (FileNotFoundException e) {
            LOGGER.error("file not found", e);
        } catch (IOException e) {
            LOGGER.error("unable to access file", e);
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
        temporaryFile.deleteOnExit();
        return temporaryFile;
    }
}
