/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.response
 *             FILE : DeleteStructMapOpertation.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 10, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.response;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 10, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class DeleteStructMapOperation extends StructMapOperation {

    /** class's uid. */
    private static final long serialVersionUID = 8326607516293707539L;

    public DeleteStructMapOperation() {
        super();
        this.setOperationType(ResponseOperationType.DELETE);
    }

    public DeleteStructMapOperation(final String id) {
        super(ResponseOperationType.DELETE, id);
    }

}
