/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.rdfStoreCleaner.client
 *             FILE : RDFStoreCleanerService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 1, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.rdfStoreCleaner;

import eu.europa.ec.opoce.cellar.cl.domain.AbstractBatchManager.ManagerStatus;
import eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner.CleanerDifference;
import eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner.CleanerDifferencePaginatedList;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 1, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface RDFStoreCleanerService {

    /**
     * Clean rdf store unknown.
     *
     * @return true, if successful
     */
    boolean cleanRDFStoreUnknown();

    /**
     * Clean rdf store corrupt.
     *
     * @return true, if successful
     */
    boolean cleanRDFStoreCorrupt();

    /**
     * Find cleaner differences.
     *
     * @param cdpl the cdpl
     */
    void findCleanerDifferences(final CleanerDifferencePaginatedList cdpl);

    /**
     * Gets the cleaner difference.
     *
     * @param id the id
     * @return the cleaner difference
     */
    CleanerDifference getCleanerDifference(final Long id);

    /**
     * Gets the cleaning status.
     *
     * @return the cleaning status
     */
    ManagerStatus getCleaningStatus();

    /**
     * Gets the number metadata resources.
     *
     * @return the number metadata resources
     */
    long getNumberMetadataResources();

    /**
     * Gets the number metadata resources diagnosable.
     *
     * @return the number metadata resources diagnosable
     */
    long getNumberMetadataResourcesDiagnosable();

    /**
     * Gets the number metadata resources fixable.
     *
     * @return the number metadata resources fixable
     */
    long getNumberMetadataResourcesFixable();

    /**
     * Gets the number metadata resources error.
     *
     * @return the number metadata resources error
     */
    long getNumberMetadataResourcesError();

    /**
     * Gets the number rows cleaner difference.
     *
     * @return the number rows cleaner difference
     */
    long getNumberRowsCleanerDifference();

    /**
     * Start cleaning.
     */
    void startCleaning();

    /**
     * Stop cleaning.
     */
    void stopCleaning();
}
