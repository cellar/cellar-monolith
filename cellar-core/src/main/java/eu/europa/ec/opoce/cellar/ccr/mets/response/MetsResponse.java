package eu.europa.ec.opoce.cellar.ccr.mets.response;

public class MetsResponse {
    private String status_endpoint;
    private String received;

    public String getStatus_endpoint() {
        return status_endpoint;
    }

    public void setStatus_endpoint(String status_endpoint) {
        this.status_endpoint = status_endpoint;
    }

    public String getReceived() {
        return received;
    }

    public void setReceived(String received) {
        this.received = received;
    }
}
