/**
 * 
 */
package eu.europa.ec.opoce.cellar.cl.dao;

import java.util.List;

import eu.europa.ec.opoce.cellar.cl.domain.sip.SipMetsProcessing;

/**
 * Dao for the the Entity {@link SipMetsProcessing}.
 * @author dcraeye
 *
 */
public interface SipMetsProcessingDao {

    /**
     * Persist the given transient instance.
     * 
     * @param sipMetsProcessing
     *            the transient instance to be persisted
     */
    void createSipMetsProcessing(SipMetsProcessing sipMetsProcessing);

    /**
     * Delete the given persistent instance.
     * 
     * @param mimeType
     *            the persistent instance to delete
     */
    void deleteSipMetsProcessing(SipMetsProcessing sipMetsProcessing);

    /**
     * Return all persistent instances of {@link SipMetsProcessing}.
     * 
     * @return a {@link List} containing 0 or more persistent instances.
     */
    List<SipMetsProcessing> getSipMetsProcessingList();

    /**
     * Return the persistent instance with the given internal identifier, or null
     * if not found.
     * 
     * @param id
     *            the internal identifier of the persistent instance
     * 
     * @return the persistent instance, or null if not found
     */
    SipMetsProcessing getSipMetsProcessing(Long id);

    /**
     * Update the given persistent instance.
     * 
     * @param mimeTypeMapping
     *            the persistent instance to update
     */
    void updateSipMetsProcessing(SipMetsProcessing sipMetsProcessing);

    /**
     * Return a SipMetsProcessing where the sipName equals the given sipName  
     * @param sipName to retrieve.
     * @return
     */
    SipMetsProcessing getBySipName(final String sipName);

    /**
     * Return a SipMetsProcessing where the metsfolder equals the given metsFolder  
     * @param metsFolder to retrieve.
     * @param metsFolder
     * @return
     */
    SipMetsProcessing getByMetsFolder(final String metsFolder);

    /**
     * Return all SipMetsProcessing order by PRIORITY DESC, ID and STRUCTMAP_COUNT. 
     * @return a list of SipMetsProcessing.
     */
    List<SipMetsProcessing> getAll();

    /**
     * Return all SipMetsProcessing order by PRIORITY DESC. 
     * @return list of SipMetsProcessing.
     */
    List<SipMetsProcessing> getAllByPriority();

    /**
     * Return all SipMetsProcessing order by STRUCTMAP_COUNT. 
     * @return a list of SipMetsProcessing.
     */
    List<SipMetsProcessing> getAllBySMCount();

    /**
     * Return all SipMetsProcessing order by DIGITAL_OBJECT_COUNT. 
     * @return a list of SipMetsProcessing.
     */
    List<SipMetsProcessing> getAllByDOCount();

}
