/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.dao
 *             FILE : BatchJobWorkIdentifierDao.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 11, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao;

import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob.BATCH_JOB_TYPE;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJobWorkIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.common.CellarPaginatedList;

import java.util.List;

/**
 * <class_description> Batch job work identifier dao.
 *
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 11, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface BatchJobWorkIdentifierDao extends BaseDao<BatchJobWorkIdentifier, Long> {

    /**
     * Returns a list of unique cellar ids corresponding to <code>batchJobId</code>.
     * @param batchJobId the identifier of the job
     * @param first start index of the list (inclusive)
     * @param last end index of the list (exclusive)
     * @return the list of unique cellar ids
     */
    List<String> findWorkIds(final Long batchJobId, final BATCH_JOB_TYPE jobType, final int first, final int last);

    /**
     * Returns a paginated list of works of the batch job corresponding to <code>batchJobId</code> and matching with the pagination informations.
     * @param batchJobId the identifier of the job
     * @param workIdentifiersList the pagination informations
     * @return the paginated list of works
     */
    CellarPaginatedList<BatchJobWorkIdentifier> findWorkIdentifiers(final Long batchJobId, final BATCH_JOB_TYPE jobType,
            final CellarPaginatedList<BatchJobWorkIdentifier> workIdentifiersList);

    /**
     * Returns the list of works of the batch job corresponding to <code>batchJobId</code>.
     * @param batchJobId the identifier of the job
     * @return the list of works of the batch job
     */
    List<BatchJobWorkIdentifier> findWorkIdentifiers(final Long batchJobId, final BATCH_JOB_TYPE jobType);

    /**
     * Deletes the batch job corresponding to <code>batchJobId</code>.
     * @param batchJobId the identifier of the job
     */
    void deleteWorkIdentifiers(final Long batchJobId, final BATCH_JOB_TYPE jobType);

}
