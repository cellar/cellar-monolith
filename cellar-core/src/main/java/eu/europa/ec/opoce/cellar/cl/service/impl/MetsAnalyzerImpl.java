package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditBuilder;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventAction;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventProcess;
import eu.europa.ec.opoce.cellar.cl.domain.audit.Auditable.LogLevel;
import eu.europa.ec.opoce.cellar.cl.service.client.MetsAnalyzer;
import eu.europa.ec.opoce.cellar.common.CellarCatalogResolver;
import eu.europa.ec.opoce.cellar.common.util.StringUtils;
import eu.europa.ec.opoce.cellar.common.util.XMLUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.OperationType;
import eu.europa.ec.opoce.cellar.exception.*;
import eu.europa.ec.opoce.cellar.monitoring.Watch;
import org.apache.commons.collections15.ListUtils;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Implementation of the {@link MetsAnalyzer}.
 *
 * @author dcraeye
 *
 */
@Component
public class MetsAnalyzerImpl implements MetsAnalyzer {

    /** class's logger. */
    private transient static final Logger LOGGER = LogManager.getLogger(MetsAnalyzerImpl.class);

    /** The Constant TAG_METS_DOCUMENT_ID. */
    private final static String TAG_METS_DOCUMENT_ID = "metsDocumentID";

    /** The Constant TAG_METS. */
    private final static String TAG_METS = "mets";

    /** The Constant METS_ATTRIBUTE_TYPE. */
    private final static String METS_ATTRIBUTE_TYPE = "TYPE";

    /** The Constant ATTRIBUTE_HREF. */
    private final static String ATTRIBUTE_HREF = "href";
    private static final String BEHAVIOR_SECTION_TAG_NAME = "behaviorSec";
    private static final String BEHAVIOR_TAG_NAME = "behavior";
    private static final String MECHANISM_TAG_NAME = "mechanism";

    /** The Constant ATTRIBUTE_FILEID. */
    private static final String ATTRIBUTE_FILEID = "FILEID";

    /** The Constant ATTRIBUTE_ID. */
    private static final String ATTRIBUTE_ID = "ID";

    /** The Constant TAG_FILE. */
    private static final String TAG_FILE = "file";

    /** The Constant TAG_FPTR. */
    private static final String TAG_FPTR = "fptr";

    private ICellarConfiguration cellarConfiguration;

    @Autowired
    public MetsAnalyzerImpl(@Qualifier("cellarConfiguration") ICellarConfiguration cellarConfiguration) {
        this.cellarConfiguration = cellarConfiguration;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getMetsIdFromTemporaryWorkFolder(final String metsFileName) {
        return this.getMetsIdFromTemporaryWorkFolder(new File(this.cellarConfiguration.getCellarFolderTemporaryWork(), metsFileName));
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public String getMetsIdFromTemporaryWorkFolder(final File metsFile) {
        final List<String> asList = Arrays.asList(TAG_METS_DOCUMENT_ID);
        final List<String> parse = this.parse(metsFile, asList, ListUtils.EMPTY_LIST, "", true, 10, false, false);
        return parse.get(0);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OperationType getMetsTypeFromTemporaryWorkFolder(final String metsFileName) {
        return this.getMetsTypeFromTemporaryWorkFolder(new File(this.cellarConfiguration.getCellarFolderTemporaryWork(), metsFileName));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OperationType getMetsTypeFromTemporaryWorkFolder(final File metsFile) {
        @SuppressWarnings("unchecked")
        final List<String> values = this.parse(metsFile, Arrays.asList(TAG_METS), ListUtils.EMPTY_LIST, METS_ATTRIBUTE_TYPE, true, 10,
                false, false);
        final OperationType retType = OperationType.getByXmlValue(values.get(0));
        if (retType == null) {
            throw ExceptionBuilder.get(CellarException.class).withCode(CoreErrors.E_026).withMessage("[{}] is not a valid METS type.")
                    .withMessageArgs(values.get(0)).build();
        }
        return retType;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getMetsLinkedFileListFromTemporaryWorkFolder(final String metsFileName) {
        return this.getMetsLinkedFileListFromTemporaryWorkFolder(
                new File(this.cellarConfiguration.getCellarFolderTemporaryWork(), metsFileName));
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<String> getMetsLinkedFileListFromTemporaryWorkFolder(final File metsFile) {
        return this.parse(metsFile, ListUtils.EMPTY_LIST, Arrays.asList("mechanism"), ATTRIBUTE_HREF, false, Integer.MAX_VALUE, true,
                false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getMetsFptrFileIDListFromTemporaryWorkFolder(final String metsFileName) {
        return this.getMetsFptrFileIDListFromTemporaryWorkFolder(
                new File(this.cellarConfiguration.getCellarFolderTemporaryWork(), metsFileName));
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<String> getMetsFptrFileIDListFromTemporaryWorkFolder(final File metsFile) {
        return this.parse(metsFile, Arrays.asList(TAG_FPTR), ListUtils.EMPTY_LIST, ATTRIBUTE_FILEID, false, Integer.MAX_VALUE, true, true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getMetsFileIDListFromTemporaryWorkFolder(final String metsFileName) {
        return this
                .getMetsFileIDListFromTemporaryWorkFolder(new File(this.cellarConfiguration.getCellarFolderTemporaryWork(), metsFileName));
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<String> getMetsFileIDListFromTemporaryWorkFolder(final File metsFile) {
        return this.parse(metsFile, Arrays.asList(TAG_FILE), ListUtils.EMPTY_LIST, ATTRIBUTE_ID, false, Integer.MAX_VALUE, true, true);
    }

    @Watch(value = "METS Analyze", arguments = {
            @Watch.WatchArgument(name = "Mets file", expression = "metsFile.name")
    })
    @Override
    public int countMetsStructMap(final File metsFile) throws Exception {
        final DocumentBuilder builder = createDocumentBuilderForCounting();
        try(final FileInputStream fis = new FileInputStream(metsFile);) {
            final Document doc = builder.parse(fis);
            final XPathExpression expr = XMLUtils.newXPathFactory().newXPath().compile("//mets/structMap");
            return ((NodeList) expr.evaluate(doc, XPathConstants.NODESET)).getLength();
        }
    }

    @Watch(value = "METS Analyze", arguments = {
            @Watch.WatchArgument(name = "Mets file", expression = "metsFile.name")
    })
    @Override
    public int countMetsDigitalObject(final File metsFile) throws Exception {
        final DocumentBuilder builder = createDocumentBuilderForCounting();
        try(final FileInputStream fis = new FileInputStream(metsFile);) {

            final Document doc = builder.parse(fis);
            final XPathExpression expr = XMLUtils.newXPathFactory().newXPath()
                    .compile("//div[normalize-space(@TYPE)='work' " + " or normalize-space(@TYPE)='expression'"
                            + " or normalize-space(@TYPE)='manifestation'" + " or normalize-space(@TYPE)='dossier'"
                            + " or normalize-space(@TYPE)='event'" + " or normalize-space(@TYPE)='agent']");
            final Object result = expr.evaluate(doc, XPathConstants.NODESET);
            return ((NodeList) result).getLength();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getMetsHdr(final File metsFile) throws FileNotFoundException, IOException {

        final Pattern p = Pattern.compile("(<metsHdr(.*)metsHdr>)", Pattern.DOTALL);
        final Matcher m = p.matcher(FileUtils.readFileToString(metsFile, (Charset) null));
        if (m.find()) {
            return m.group(0);
        }
        return null;
    }

    /**
     * Inspect the mets file to retrieve some values.
     *
     * @param metsFile            the mets file to parse
     * @param inTagList            the tags to inspect. If tags is empty or null, search on all tag.
     * @param notInTagList            the tags to not inspect.
     * @param attribute            the attribute to retrieve. If empty, attribute values are not inspected.
     * @param unique            true to stop the parsing when founding one value.
     * @param maxTagToTreat            max inspected tag. (Used to accelerate the parsing)
     * @param emptyValueAccepted the empty value accepted
     * @param attributeMustHaveAValue the attribute must have a value
     * @return the list of corresponding values. This list can be empty.
     */
    private List<String> parse(final File metsFile, final List<String> inTagList, final List<String> notInTagList, final String attribute,
            final boolean unique, final int maxTagToTreat, final boolean emptyValueAccepted, final boolean attributeMustHaveAValue) {

        boolean onTag = false;
        final List<String> values = new ArrayList<String>();
        boolean end = false;
        int treatedTag = 0;

        FileInputStream fis = null;
        XMLEventReader xmlER = null;

        try {
            // Construct the reader
            final XMLInputFactory xmlIF = XMLInputFactory.newInstance();
            // xmlIF.setProperty("javax.xml.stream.isReplacingEntityReferences", false);
            // xmlIF.setProperty("javax.xml.stream.isSupportingExternalEntities", true);
            // xmlIF.setProperty("javax.xml.stream.isValidating", false);
            // xmlIF.setProperty("javax.xml.stream.isNamespaceAware", false);
            xmlIF.setProperty("javax.xml.stream.supportDTD", false);

            fis = new FileInputStream(metsFile);
            xmlER = xmlIF.createXMLEventReader(fis);

            XMLEvent xmlEvent;
            while (xmlER.hasNext() && !end && treatedTag < maxTagToTreat) {
                xmlEvent = xmlER.nextEvent();
                if (xmlEvent.isStartElement()) {
                    treatedTag++;
                    final String tag = xmlEvent.asStartElement().getName().getLocalPart();

                    final boolean inTagsContainsTag = inTagList.contains(tag);
                    final boolean notInTagsContainsTag = notInTagList.contains(tag);

                    if (inTagsContainsTag && !notInTagsContainsTag) {
                        if (StringUtils.isNotBlank(attribute)) {
                            final String value = this.getAttributeVal(xmlEvent, attribute);
                            if (StringUtils.isNotBlank(value)) {
                                values.add(value);
                                end = unique;
                            } else if (attributeMustHaveAValue) {
                                throw ExceptionBuilder.get(MetsAnalyzerException.class).withCode(CoreErrors.E_025)
                                        .withMessage("Mets [" + metsFile.getName() + "] has a tag : <"
                                                + StringUtils.join(inTagList, "> or <") + ">"
                                                + (StringUtils.isNotBlank(attribute) ? " with attribute (" + attribute + ")" : "")
                                                + " missing or without a value")
                                        .build();
                            }
                        }
                        if (StringUtils.isNotBlank(tag)) {
                            onTag = true;
                        }
                    } else {
                        onTag = false;
                    }
                }

                if (xmlEvent.isCharacters()) {
                    if (onTag && !xmlEvent.asCharacters().isWhiteSpace()) {
                        final String value = StringUtils.trim(xmlEvent.asCharacters().getData());
                        if (StringUtils.isNotBlank(value)) {
                            values.add(value);
                            end = unique;
                        }
                    }
                }

            }

        } catch (final FileNotFoundException fnfe) {
            handleException(metsFile, fnfe);
        } catch (final XMLStreamException xmlse) {
            handleException(metsFile, xmlse);
        } finally {
            // close open stream
            if (xmlER != null) {
                try {
                    xmlER.close();
                } catch (final XMLStreamException e) {
                    // nothing to do
                }
            }
            if (fis != null) {
                try {
                    fis.close();
                } catch (final IOException e) {
                    // nothing to do
                }
            }

        }

        if (!emptyValueAccepted && values.isEmpty()) {
            throw ExceptionBuilder.get(MetsAnalyzerException.class).withCode(CoreErrors.E_025)
                    .withMessage("Mets [" + metsFile.getName() + "] has no tag : <" + StringUtils.join(inTagList, "> or <") + ">"
                            + (StringUtils.isNotBlank(attribute) ? " with attribute (" + attribute + ")" : ""))
                    .build();
        }

        return values;
    }

    /** {@inheritDoc} */
    @Override
    public List<String> readAndExtractHREFreferences(final File metsFile) {
        final List<String> result = new ArrayList<String>();


        XMLEventReader xmlER = null;
        try(final FileInputStream fis=new FileInputStream(metsFile)) {
            final XMLInputFactory xmlIF = XMLInputFactory.newInstance();
            xmlIF.setProperty("javax.xml.stream.supportDTD", false);
            
            xmlER = xmlIF.createXMLEventReader(fis);
            final boolean gotoNextTag = false;
            while (xmlER.hasNext()) {
                XMLEvent event = null;
                if (!gotoNextTag) {
                    event = xmlER.nextEvent();
                } else {
                    event = xmlER.nextTag();
                }
                if (event.isStartElement()) {
                    final StartElement asStartElement = event.asStartElement();
                    final String currentTagName = asStartElement.getName().getLocalPart();
                    final String[] ignoreTagNames = new String[] {
                            BEHAVIOR_SECTION_TAG_NAME, BEHAVIOR_TAG_NAME, MECHANISM_TAG_NAME};
                    if (!Arrays.asList(ignoreTagNames).contains(currentTagName)) {
                        // handle attributes
                        @SuppressWarnings("unchecked")
                        final Iterator<Attribute> attributes = asStartElement.getAttributes();
                        while (attributes.hasNext()) {
                            final Attribute attribute = attributes.next();
                            if (attribute.getName().getLocalPart().contains(ATTRIBUTE_HREF)) {
                                String sanitizedAttribute = StringUtils.replaceBackslashesWithSlashes(attribute.getValue());
                                result.add(sanitizedAttribute);
                            }
                        }
                    }
                }
            }
        } catch (final Exception e) {
            throw ExceptionBuilder.get(CellarValidationException.class).withCode(CoreErrors.E_013)
                    .withMessage("Unexpected exception ocurred while validating contents of the SIP package. - " + e.getMessage()).build();
        } finally {
            // close open stream
            if (xmlER != null) {
                try {
                    xmlER.close();
                } catch (final XMLStreamException e) {
                    // nothing to do
                }
            }
        }
        return result;
    }

    /**
     * Gets the attribute val.
     *
     * @param xmlEvent the xml event
     * @param attributeName the attribute name
     * @return the attribute val
     */
    private String getAttributeVal(final XMLEvent xmlEvent, final String attributeName) {
        @SuppressWarnings("rawtypes")
        final Iterator attrIter = xmlEvent.asStartElement().getAttributes();
        while (attrIter.hasNext()) {
            final Attribute attribute = (Attribute) attrIter.next();

            if (attributeName.equals(StringUtils.trim(attribute.getName().getLocalPart()))) {
                LOGGER.debug("[" + attributeName + "] ==>> " + attribute.getValue());
                return StringUtils.trim(attribute.getValue());
            }

        }
        return "";
    }

    /**
     * Creates the document builder for counting.
     *
     * @return the document builder
     * @throws ParserConfigurationException the parser configuration exception
     */
    private static DocumentBuilder createDocumentBuilderForCounting() throws ParserConfigurationException {
        final DocumentBuilderFactory domFactory = DocumentBuilderFactory.newInstance();
        domFactory.setNamespaceAware(false); // never forget this!
        final DocumentBuilder builder = domFactory.newDocumentBuilder();
        builder.setEntityResolver(CellarCatalogResolver.getInstance());
        return builder;
    }

    private static ExceptionBuilder<MetsAnalyzerException> prepareExceptionBuilder(final File metsFile, final Throwable cause)
            throws MetsAnalyzerException {
        return ExceptionBuilder.get(MetsAnalyzerException.class).withCode(CoreErrors.E_025).withMessage("[" + metsFile.getName() + "]")
                .withCause(cause);
    }

    private static void handleException(final File metsFile, final Throwable cause) throws MetsAnalyzerException {
        throw prepareExceptionBuilder(metsFile, cause).build();
    }

    private static void logWarn(final File metsFile, final Throwable cause) {
        final BuildableException exception = prepareExceptionBuilder(metsFile, cause).root(true).build();

        AuditBuilder.get(AuditTrailEventProcess.Ingestion) //
                .withAction(AuditTrailEventAction.Validate) //
                .withMessage(exception.getMessage()) //
                .withException(exception) //
                .withLogger(LOGGER) //
                .withLogLevel(LogLevel.WARN) //
                .logEvent();
    }
    
}
