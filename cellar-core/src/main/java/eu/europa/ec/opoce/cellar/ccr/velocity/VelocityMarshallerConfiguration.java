package eu.europa.ec.opoce.cellar.ccr.velocity;

public interface VelocityMarshallerConfiguration {

    String getModelName();

    String getViewName();

}
