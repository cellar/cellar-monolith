/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *             FILE : MimeTypeCacheServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 6, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.ccr.service.FileTypesNalSkosLoaderService;
import eu.europa.ec.opoce.cellar.cl.domain.mimeType.MimeTypeMapping;
import eu.europa.ec.opoce.cellar.cl.service.client.MimeTypeCacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

/**
 * <class_description> Mime type cache manager.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 6, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class MimeTypeCacheServiceImpl implements MimeTypeCacheService {

    /**
     * Mime type dao.
     */
    @Autowired
    private FileTypesNalSkosLoaderService fileTypesNalSkosLoaderService;

    /**
     * {@inheritDoc}
     */
    @Override
    @Cacheable("mimeTypesSet")
    public Set<String> getMimeTypesSet() {
        final Set<MimeTypeMapping> mimeTypeMappings = this.fileTypesNalSkosLoaderService.getMimeTypeMappingsByPriorityDesc();
        if (mimeTypeMappings == null)
            return new HashSet<>();
        return mimeTypeMappings.stream().map(MimeTypeMapping::getMimeType).collect(Collectors.toSet());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Cacheable("mimeTypesByPriorityDescList")
    public List<String> getMimeTypesByPriorityDescList() {
        final Set<MimeTypeMapping> mimeTypeMappings = this.fileTypesNalSkosLoaderService.getMimeTypeMappingsByPriorityDesc();
        if (mimeTypeMappings == null)
            return new ArrayList<>();

        return mimeTypeMappings.stream()
                .sorted((elementOne, elementTwo) -> Long.compare(elementTwo.getPriority(), elementOne.getPriority()))
                .map(MimeTypeMapping::getMimeType).collect(Collectors.toList());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Cacheable("manifestationTypesMap")
    public Map<String, List<MimeTypeMapping>> getManifestationTypesMap() {
        Set<MimeTypeMapping> mimeTypeMappings = this.fileTypesNalSkosLoaderService.getMimeTypeMappingsByPriorityDesc();
        if (mimeTypeMappings == null)
            return new HashMap<>();
        return mimeTypeMappings.stream().collect(groupingBy(MimeTypeMapping::getManifestationType));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Cacheable("mimeTypeMap")
    public Map<String, List<MimeTypeMapping>> getMimeTypeMap() {
        Set<MimeTypeMapping> mimeTypeMappings = this.fileTypesNalSkosLoaderService.getMimeTypeMappingsByPriorityDesc();
        if (mimeTypeMappings == null)
            return new HashMap<>();
        return mimeTypeMappings.stream().collect(groupingBy(MimeTypeMapping::getMimeType));
    }

    void setSkosLoaderService(FileTypesNalSkosLoaderService skosLoaderService) {
        this.fileTypesNalSkosLoaderService = skosLoaderService;
    }
}
