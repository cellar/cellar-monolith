/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.splash
 *        FILE : SplashScreen.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 02-04-2012
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.splash;

import com.google.common.base.Throwables;
import eu.europa.ec.opoce.cellar.ccr.service.FileTypesNalSkosLoaderService;
import eu.europa.ec.opoce.cellar.ccr.service.LanguagesNalSkosLoaderService;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.IS3Configuration;
import eu.europa.ec.opoce.cellar.cl.configuration.impl.CellarStaticConfiguration;
import eu.europa.ec.opoce.cellar.cl.status.IDataSourceStatus;
import eu.europa.ec.opoce.cellar.cl.status.IServiceStatus;
import eu.europa.ec.opoce.cellar.common.util.StringUtils;
import eu.europa.ec.opoce.cellar.exception.CellarConfigurationException;
import eu.europa.ec.opoce.cellar.s3.repository.ContentRepository;
import eu.europa.ec.opoce.cellar.server.admin.nal.GroupedNalOntoVersion;
import eu.europa.ec.opoce.cellar.server.admin.nal.NalOntoVersion;
import eu.europa.ec.opoce.cellar.server.admin.nal.OntoAdminConfigurationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static eu.europa.ec.opoce.cellar.AppConstants.NEWLINE;

/**
 * <class_description> Cellar splash screen. <br/>
 * <br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc. <br/>
 * <br/>
 * ON : 02-04-2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component
public class SplashScreen implements ApplicationListener<ContextRefreshedEvent> {

    private static final Logger LOG = LogManager.getLogger(SplashScreen.class);

    private static final String SPLASH_FILE = "splash/splash.txt";

    // number of contexts to be initialized before splashing. If new contexts
    // are added/removed, change this value accordingly
    /**
     * The Constant CTXTS_TO_INITIALIZE.
     */
    private static final int CTXTS_TO_INITIALIZE = 5;

    private static final String NA = "not evaluated";

    private static final String ENABLED = "enabled";

    private static final String DISABLED = "disabled";

    private static final String ONLINE = "online";

    private static final String PARTIALLY_ONLINE = "partially online";

    private static final String OFFLINE = "offline";

    private static final String INFO = "INFO";

    private static final String WARN = "WARN";

    private static final String ERROR = "ERROR";

    private List<String> infoMessages;

    private List<String> warnMessages;

    private List<String> errorMessages;

    @Autowired
    @Qualifier("cellarConfiguration")
    ICellarConfiguration cellarConfiguration;

    @Autowired
    @Qualifier("cellarConfiguration")
    IS3Configuration s3Configuration;

    @Autowired
    @Qualifier("validationServiceStatus")
    IServiceStatus validationServiceStatus;
    
    @Autowired
    @Qualifier("statusServiceStatus")
    IServiceStatus statusServiceStatus;

    @Autowired
    @Qualifier("sparqlServerStatus")
    IServiceStatus sparqlServerStatus;

    @Autowired
    @Qualifier("cellarDataSourceStatus")
    IDataSourceStatus cellarDataSourceStatus;

    @Autowired
    @Qualifier("cmrDataSourceStatus")
    IDataSourceStatus cmrDataSourceStatus;

    @Autowired
    @Qualifier("idolDataSourceStatus")
    IDataSourceStatus idolDataSourceStatus;

    @Autowired
    @Qualifier("virtuosoDataSourceStatus")
    IDataSourceStatus virtuosoDataSourceStatus;

    @Autowired
    LanguagesNalSkosLoaderService languagesNalSkosLoaderService;

    @Autowired
    FileTypesNalSkosLoaderService fileTypesNalSkosLoaderService;

    @Autowired
    OntoAdminConfigurationService ontoAdminConfigurationService;

    @Autowired
    private ContentRepository contentRepository;

    private Map<String, String> splashParams;

    private int initializedCtxts;

    @PostConstruct
    private void init() {
        this.initializedCtxts = 0;
        refresh();
    }

    public void refresh() {
        this.infoMessages = new ArrayList<String>();
        this.warnMessages = new ArrayList<String>();
        this.errorMessages = new ArrayList<String>();
        this.splashParams = loadSplashParams();
    }

    @Override
    public void onApplicationEvent(final ContextRefreshedEvent event) {
        if (splashParams != null && ++initializedCtxts == CTXTS_TO_INITIALIZE) {
            // info out all Cellar properties
            LOG.info(IConfiguration.CONFIG, "Cellar is configured as follows: " + cellarConfiguration);
            //splashes the screen
            LOG.info(IConfiguration.CONFIG, NEWLINE + NEWLINE + getPlainSplash() + NEWLINE + NEWLINE);
        }
    }

    private Map<String, String> loadSplashParams() {
        // checks some of the services
        boolean onlyForIngestion = cellarConfiguration.isCellarServiceIngestionEnabled();
        boolean s3Available = available("S3", () -> contentRepository.isAccessible(), true);
        boolean sparqlServerAvailable = available("SPARQL", () -> sparqlServerStatus.check(), onlyForIngestion);
        boolean validationServiceAvailable = available("SHACL", () -> validationServiceStatus.check(), onlyForIngestion);
        boolean statusServiceAvailable = available("Status API", () -> statusServiceStatus.check(), true);
        boolean cellarDSAvailable = available("Cellar", () -> cellarDataSourceStatus.check(), true);
        boolean cmrDSAvailable = available("CMR", () -> cmrDataSourceStatus.check(), true);
        boolean idolDSAvailable = available("IDOL", () -> idolDataSourceStatus.check(), true);
        boolean virtuosoDSAvailable = available("Virtuoso", () -> virtuosoDataSourceStatus.check(), onlyForIngestion);

        // collect the startup messages
        collectMessages(s3Available, sparqlServerAvailable, cellarDSAvailable, cmrDSAvailable, idolDSAvailable,
                virtuosoDSAvailable, validationServiceAvailable, statusServiceAvailable);

        // builds params
        final Map<String, String> params = new HashMap<String, String>();
        // Cellar
        params.put("cellarAvailability", ONLINE);
        params.put("cellarConfigurationVersion", this.cellarConfiguration.getCellarConfigurationVersion());
        params.put("cellarServerBaseUrl", this.cellarConfiguration.getCellarServerBaseUrl());
        params.put("cellarConfigurationPath", this.cellarConfiguration.getConfigurationPath());
        params.put("cellarServiceIndexingEnabled",
                checkStatus(this.cellarConfiguration.isCellarServiceIndexingEnabled(), ENABLED, DISABLED));
        params.put("cellarServiceIngestionEnabled",
                checkStatus(this.cellarConfiguration.isCellarServiceIngestionEnabled(), ENABLED, DISABLED));
        params.put("cellarServiceNalLoadEnabled", checkStatus(this.cellarConfiguration.isCellarServiceNalLoadEnabled(), ENABLED, DISABLED));
        params.put("cellarServiceOntoLoadEnabled",
                checkStatus(this.cellarConfiguration.isCellarServiceOntoLoadEnabled(), ENABLED, DISABLED));

        // S3
        params.put("s3Availability", checkStatus(s3Available, ONLINE, OFFLINE));
        params.put("s3Region", this.cellarConfiguration.getS3Region());
        params.put("s3Bucket", this.cellarConfiguration.getS3Bucket());

        // SHACL validation service
        params.put("validationServiceAvailability", checkStatus(validationServiceAvailable, ONLINE, OFFLINE));
        params.put("shaclServerBaseUrl", this.cellarConfiguration.getCellarServiceIntegrationValidationBaseUrl());

        // SPARQL endpoint
        params.put("sparqlAvailability", checkStatus(sparqlServerAvailable, ONLINE, OFFLINE));
        params.put("sparqlServiceUri", this.cellarConfiguration.getCellarServiceSparqlUri());
        
        // Status service
        params.put("cellarStatusServiceAvailability", checkStatus(statusServiceAvailable, ONLINE, OFFLINE));
        params.put("cellarStatusServiceBaseUrl", this.cellarConfiguration.getCellarStatusServiceBaseUrl());

        // Database
        params.put("databaseAvailability",
                checkGlobalDatabaseStatus(cellarDSAvailable, cmrDSAvailable, idolDSAvailable, virtuosoDSAvailable));
        params.put("databaseUrl", cellarDSAvailable ? this.cellarDataSourceStatus.getDatabaseUrl() : OFFLINE);
        params.put("databaseReadOnly", checkStatus(this.cellarConfiguration.isCellarDatabaseReadOnly(), "read only", "read/write"));
        params.put("databaseCellarAvailability", checkStatus(cellarDSAvailable, ONLINE, OFFLINE));
        params.put("databaseCmrAvailability", checkStatus(cmrDSAvailable, ONLINE, OFFLINE));
        params.put("databaseIdolDSAvailability", checkStatus(idolDSAvailable, ONLINE, OFFLINE));
        params.put("databaseVirtuosoDSAvailability", checkStatus(virtuosoDSAvailable, ONLINE, OFFLINE));

        // Copyright
        params.put("year", "" + Calendar.getInstance().get(Calendar.YEAR));

        // Messages
        params.put("health", buildHealthMessage());
        fillInMessages(params);

        // returns params
        return params;
    }

    /**
     * @param source the datasource to check (used for logging)
     * @param check  the check to execute
     * @return true is the service is reachable, otherwise false
     */
    private static boolean available(String source, Check check, boolean applicable) {
        try {
            if (applicable) {
                check.check();
                return true;
            }
        } catch (CellarConfigurationException e) {
            LOG.warn("{} not available: {}", source, Throwables.getStackTraceAsString(e).replaceAll("\n", NEWLINE));
        } catch (Exception e) {
            LOG.catching(e);
        }
        return false;
    }

    @FunctionalInterface
    interface Check {
        void check() throws CellarConfigurationException;
    }

    /**
     * Check status.
     *
     * @param status    the status
     * @param statusOn  the status on
     * @param statusOff the status off
     * @return the string
     */
    private static String checkStatus(final Boolean status, final String statusOn, final String statusOff) {
        return checkStatus(status, statusOn, statusOff, NA);
    }

    /**
     * Check status.
     *
     * @param status        the status
     * @param statusOn      the status on
     * @param statusOff     the status off
     * @param defaultStatus the default status
     * @return the string
     */
    private static String checkStatus(final Boolean status, final String statusOn, final String statusOff, final String defaultStatus) {
        return status == null ? defaultStatus : status ? statusOn : statusOff;
    }

    /**
     * Check global database status.
     *
     * @param statuses the statuses
     * @return the string
     */
    private static String checkGlobalDatabaseStatus(final Boolean... statuses) {
        Boolean globalAndStatus = null;
        Boolean globalOrStatus = null;
        boolean firstRun = true;
        for (final Boolean status : statuses) {
            final Boolean andStatus = status == null || status;
            final Boolean orStatus = status != null && status;
            if (firstRun) {
                globalAndStatus = andStatus;
                globalOrStatus = orStatus;
                firstRun = false;
            } else {
                globalAndStatus &= andStatus;
                globalOrStatus |= orStatus;
            }
        }
        return globalAndStatus ? ONLINE : globalOrStatus ? PARTIALLY_ONLINE : OFFLINE;
    }

    /**
     * Gets the plain splash.
     *
     * @return the plain splash
     */
    public String getPlainSplash() {
        String splash = SplashUtils.loadSplash(SPLASH_FILE, this.splashParams);
        splash = SplashUtils.justifyPlainSplash(splash);
        return splash;
    }

    /**
     * Gets the html splash.
     *
     * @return the html splash
     */
    public String getHtmlSplash() {
        String splash = SplashUtils.loadSplash(SPLASH_FILE, this.splashParams);
        splash = SplashUtils.justifyHtmlSplash(splash);
        return splash;
    }

    /**
     * Builds the health message.
     *
     * @return the string
     */
    private String buildHealthMessage() {
        String healthMessage = "";

        if (this.errorMessages.size() > 0) {
            healthMessage = "*** Cellar has blocking errors! ***";
        } else if (this.warnMessages.size() > 0) {
            healthMessage = "Cellar has warnings that can be fixed without restarting";
        } else {
            healthMessage = "Cellar is 100% healthy";
        }

        return healthMessage;
    }

    /**
     * Fill in messages.
     *
     * @param params the params
     */
    private void fillInMessages(final Map<String, String> params) {
        int index = 0;
        // info messages
        for (final String infoMessage : this.infoMessages) {
            params.put("msg" + index++, INFO + ": " + infoMessage);
        }
        // warning messages
        for (final String warnMessage : this.warnMessages) {
            params.put("msg" + index++, WARN + ": " + warnMessage);
        }
        // error messages
        for (final String errorMessage : this.errorMessages) {
            params.put("msg" + index++, ERROR + ": " + errorMessage);
        }
    }

    /**
     * Collect messages.
     *
     * @param s3serverAvailable         the s3 server available
     * @param sparqlServerAvailable     the sparql server available
     * @param cellarDSAvailable         the cellar ds available
     * @param cmrDSAvailable            the cmr ds available
     * @param idolDSAvailable           the idol ds available
     * @param virtuosoDSAvailable
     * @param validationServerAvailable
     * @param statusServiceAvailable    CELLAR Status Service available
     */
    private void collectMessages(final Boolean s3serverAvailable, final Boolean sparqlServerAvailable, final Boolean cellarDSAvailable,
                                 final Boolean cmrDSAvailable, final Boolean idolDSAvailable, final Boolean virtuosoDSAvailable,
                                 boolean validationServerAvailable, boolean statusServiceAvailable) {
        // NAL languages-related messages
        final boolean areLanguagesLoaded = this.languagesNalSkosLoaderService.areAllItemsLoaded();
        if (areLanguagesLoaded) {
            this.infoMessages.add(this.languagesNalSkosLoaderService.numberOfItemDescriptionsLoaded() + " NAL languages loaded");
        } else {
            this.warnMessages.add("No NAL language is loaded");
        }

        // NAL fileType-related messages
        final boolean areFileTypesLoaded = this.fileTypesNalSkosLoaderService.areAllItemsLoaded();
        if (areFileTypesLoaded) {
            this.infoMessages.add(this.fileTypesNalSkosLoaderService.numberOfItemDescriptionsLoaded() + " NAL file-types loaded");
        } else {
            this.warnMessages.add("No NAL file-type is loaded");
        }

        // Ontologies-related messages
        final Collection<GroupedNalOntoVersion> ontologies = this.ontoAdminConfigurationService.getGroupedOntologyVersions();
        if (ontologies != null && ontologies.size() > 0) {
            for (final GroupedNalOntoVersion ontology : ontologies) {
                final NalOntoVersion inUseOntology = ontology.getNalOntoVersion();
                final boolean versionKnown = inUseOntology != null && StringUtils.isNotBlank(inUseOntology.getVersionNumber());
                final String version = versionKnown ? inUseOntology.getVersionNumber() : "unknown version";
                this.infoMessages.add(ontology.getName() + " ontology loaded (" + version + ")");
            }
        } else {
            this.warnMessages.add("No ontology is loaded");
        }

        // configuration-related messages
        if (this.cellarConfiguration.getConfigurationPath() != null
                && !this.cellarConfiguration.getConfigurationPath().startsWith(CellarStaticConfiguration.WAR_INTERNAL_CP_PREFIX)) {
            this.infoMessages.add("Cellar is configured by an external property file");
        }
        if (this.cellarConfiguration.isCellarDatabaseReadOnly()) {
            if (this.cellarConfiguration.isCellarServiceIndexingEnabled()) {
                this.warnMessages.add("Indexing is enabled, but database is read-only");
            }
            if (this.cellarConfiguration.isCellarServiceIngestionEnabled()) {
                this.warnMessages.add("Ingestion is enabled, but database is read-only");
            }
            if (this.cellarConfiguration.isCellarServiceNalLoadEnabled()) {
                this.warnMessages.add("NAL load is enabled, but database is read-only");
            }
            if (this.cellarConfiguration.isCellarServiceOntoLoadEnabled()) {
                this.warnMessages.add("Ontologies load is enabled, but database is read-only");
            }
        }

        // S3-related messages
        if (!s3serverAvailable) {
            this.warnMessages.add("S3 is not accessible");
        }

        // SHACL validation service related messages
        if (!validationServerAvailable) {
            this.warnMessages.add("SHACL validation service is offline");
        }
        
        // Status service related messages
        if (!statusServiceAvailable) {
            this.warnMessages.add("Status API is offline");
        }

        // SPARQL Endpoint-related messages
        if (!sparqlServerAvailable) {
            this.warnMessages.add("SPARQL endpoint is offline");
        }

        // data source-related messages
        if (!cellarDSAvailable) {
            this.errorMessages.add("Cellar schema is offline!");
        }
        if (!cmrDSAvailable) {
            this.errorMessages.add("CMR schema is offline!");
        }
        if (idolDSAvailable == null) {
            this.infoMessages.add("Idol schema has not been evaluated because indexation is disabled");
        } else if (!idolDSAvailable) {
            this.errorMessages.add("Idol schema is offline!");
        }
        if (!virtuosoDSAvailable) {
            this.warnMessages.add("Virtuoso schema is offline");
        }
    }

}