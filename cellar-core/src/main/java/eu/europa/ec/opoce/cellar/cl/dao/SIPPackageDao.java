/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.dao
 *        FILE : SIPPackageDao.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 03-12-2021
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2021 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao;

import java.util.Collection;
import java.util.List;

import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.SIPPackage;
import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.SIPPackageProcessingStatus;

/**
 * 
 * @author EUROPEAN DYNAMICS S.A.
 */
public interface SIPPackageDao extends BaseDao<SIPPackage, Long>{
	
	/**
	 * Retrieves a SIP package reference based on its name.
	 * @param sipName the name of the SIP reference to look for.
	 * @return the SIP package reference matching the provided name.
	 */
	SIPPackage findBySipName(String sipName);
	
	/**
	 * Retrieves the SIP package references whose status is one of the provided ones.
	 * @param sipStatuses the SIP statuses to look for.
	 * @return the matching SIP package references.
	 */
	List<SIPPackage> findBySipStatusIn(Collection<SIPPackageProcessingStatus> sipStatuses);
	
	/**
	 * Counts the number of SIP package references whose status is one of the provided ones.
	 * @param sipStatuses the SIP statuses to look for.
	 * @return the number of rows having one of the provided SIP statuses.
	 */
	long countBySipStatusIn(Collection<SIPPackageProcessingStatus> sipStatuses);
	
	/**
	 * Retrieves the scheduled SIP package references as ordered by the SIP Queue Manager.
	 * @return the matching SIP package references.
	 */
	List<SIPPackage> findScheduledOrdered();
	
	/**
	 * Deletes the SIP package references having an ID belonging in the provided collection
	 * of IDs.
	 * @param ids the IDs of the SIP package references to be deleted. 
	 * @return the number of deleted rows.
	 */
	int deleteByIdIn(Collection<Long> ids);
	
}
