/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.concurrency.impl
 *             FILE : CellarLockManager.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : May 3, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.concurrency.impl;

import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLock.MODE;
import eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLockSession;
import eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLockStatistic;
import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.PidManagerException;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <class_description> Cellar locks manager implementation:
 *      - cellar id locking logic (lock the root)
 *      - no cellar id locking logic (lock the production ids)
 * <br/><br/>
 * ON : May 3, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class CellarLocksManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(CellarLocksManager.class);

    /**
     * Production ids locks.
     */
    private final HashMap<String, ProductionIdLock> productionIdsLocks;

    /**
     * Cellar ids locks.
     */
    private final HashMap<String, CellarIdLock> cellarIdsLocks;

    /**
     * Constructor.
     */
    public CellarLocksManager() {
        this.cellarIdsLocks = new HashMap<String, CellarIdLock>();
        this.productionIdsLocks = new HashMap<String, ProductionIdLock>();
    }

    /**
     * Return the locked identifiers count.
     * @return the locked identifiers count
     */
    public int getLockedIdentifiersCount() {
        int lockedIdentifierCount;

        synchronized (this.productionIdsLocks) {
            lockedIdentifierCount = this.productionIdsLocks.size();
        }

        synchronized (this.cellarIdsLocks) {
            lockedIdentifierCount += this.cellarIdsLocks.size();
        }

        return lockedIdentifierCount;
    }

    /**
     * Return the cellar locks statistics. The key from the map represents the type of logic.
     * @return the cellar locks statistics
     */
    public Map<String, List<ICellarLockStatistic>> getCellarLockStatistics() {
        final Map<String, List<ICellarLockStatistic>> cellarLockStatistics = new HashMap<String, List<ICellarLockStatistic>>();

        synchronized (this.productionIdsLocks) {
            final List<ICellarLockStatistic> productionIdsStatistics = new LinkedList<ICellarLockStatistic>();
            for (Map.Entry<String, ProductionIdLock> entry : this.productionIdsLocks.entrySet()) {
                productionIdsStatistics.add(new CellarLockStatistic(entry.getKey(), entry.getValue()));
            }
            cellarLockStatistics.put("Production ids", productionIdsStatistics);
        }

        synchronized (this.cellarIdsLocks) {
            final List<ICellarLockStatistic> cellarIdsStatistics = new LinkedList<ICellarLockStatistic>();
            for (Map.Entry<String, CellarIdLock> entry : this.cellarIdsLocks.entrySet()) {
                cellarIdsStatistics.add(new CellarLockStatistic(entry.getKey(), entry.getValue()));
            }
            cellarLockStatistics.put("Cellar ids", cellarIdsStatistics);
        }

        return cellarLockStatistics;
    }

    /**
     * Return the incremented cellar id lock corresponding to the specified identifier.  The lock is ready to call the lock method.
     * @param identifier the resource identifier
     * @return the incremented cellar id lock corresponding to the specified identifier
     */
    public CellarLock getIncrementedCellarIdLock(final Identifier identifier) {
        CellarIdLock cellarSharedLock = null;

        synchronized (this.cellarIdsLocks) {
            cellarSharedLock = this.cellarIdsLocks.get(identifier.getCellarId());
            if (cellarSharedLock == null) { // the lock doesn't exist
                cellarSharedLock = new CellarIdLock(identifier.getCellarId()); // create a new lock
                this.cellarIdsLocks.put(identifier.getCellarId(), cellarSharedLock);
            }
        }

        return cellarSharedLock;
    }

    /**
     * Return the incremented production id lock corresponding to the specified identifier.  The lock is ready to call the lock method.
     * @param identifier the resource identifier
     * @return the incremented production id lock corresponding to the specified identifier
     */
    public CellarLock getIncrementedProductionIdsLock(final Identifier identifier) {
        final List<String> pidsToAdd = new LinkedList<String>();

        ProductionIdLock obj = null;
        ProductionIdLock cellarSharedLock = null;

        synchronized (this.productionIdsLocks) {
            for (String pid : identifier.getPids()) {
                obj = this.productionIdsLocks.get(pid);
                if (obj == null) { // the lock doesn't exist
                    pidsToAdd.add(pid); // to insert in the shared hash
                } else if (cellarSharedLock == null) { // the lock to use isn't already known
                    cellarSharedLock = obj;
                } else if (obj != cellarSharedLock) { // lock objects incompatibility
                    throw ExceptionBuilder.get(PidManagerException.class)
                            .withMessage("Concurency controller: locks incompatibility: {} - {}").withMessage(obj.toString())
                            .withMessage(cellarSharedLock.toString()).withCode(CoreErrors.E_040).build();
                }
            }

            if (cellarSharedLock == null) {
                cellarSharedLock = new ProductionIdLock(pidsToAdd); // new lock to add
            } else {
                cellarSharedLock.addAllPids(pidsToAdd);
            }

            for (String pidToAdd : pidsToAdd) { // add the synonyms
                this.productionIdsLocks.put(pidToAdd, cellarSharedLock);
            }
        }

        return cellarSharedLock;
    }

    /**
     * Unlock the specified production id lock.
     * @param cellarLock the production id lock to unlock
     * @param mode the locking mode to use
     */
    public void unlock(final ICellarLockSession cellarLockSession, final ProductionIdLock cellarLock, final MODE mode) {
        synchronized (this.productionIdsLocks) {
            final boolean removed = cellarLock.removeLockSession(cellarLockSession);
            if (!removed) {
                LOGGER.warn("The lock {} is not backed by the session {}, which for this reason could not be removed.", cellarLock,
                        cellarLockSession);
            }

            if (cellarLock.isNotUsed()) { // the lock is not used anymore, the lock (with its synonyms) can be removed
                for (String pid : cellarLock.getPids()) {
                    this.productionIdsLocks.remove(pid);
                }
            }

            cellarLock.unlock(mode);
        }
    }

    /**
     * Unlock the specified cellar id lock.
     * @param cellarLock the cellar id lock to unlock
     * @param mode the locking mode to use
     */
    public void unlock(final ICellarLockSession cellarLockSession, final CellarIdLock cellarLock, final MODE mode) {
        synchronized (this.cellarIdsLocks) {
            final boolean removed = cellarLock.removeLockSession(cellarLockSession);
            if (!removed) {
                LOGGER.warn("The lock {} is not backed by the session {}, which for this reason could not be removed.", cellarLock,
                        cellarLockSession);
            }

            if (cellarLock.isNotUsed()) { // the lock is not used anymore, the lock can be removed
                this.cellarIdsLocks.remove(cellarLock.getCellarId());
            }

            cellarLock.unlock(mode);
        }
    }
}
