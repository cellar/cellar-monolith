/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.client
 *             FILE : WeightedTypeCacheService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 10, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.client;

import eu.europa.ec.op.cellar.api.util.WeightedManifestationTypes;

/**
 * <class_description> Weighted manifestation types manager.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 10, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface WeightedTypeCacheService {

    /**
     * Return the weighted manifestation types corresponding to the content extension.
     * @param contentExtension the content extension to return
     * @return the weighted manifestation types
     */
    WeightedManifestationTypes getWeightedManifestationTypes(final String contentExtension);
}
