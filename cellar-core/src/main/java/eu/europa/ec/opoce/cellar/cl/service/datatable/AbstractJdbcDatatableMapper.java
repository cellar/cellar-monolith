/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.datatable
 *             FILE : AbstractJdbcDatatableMapper.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 1 juil. 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.datatable;

import eu.europa.ec.opoce.cellar.cl.dao.datatable.DatatableDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

/**
 * The Class AbstractJdbcDatatableMapper.
 *
 * @author ARHS Developments
 * @version $Revision$
 */

public abstract class AbstractJdbcDatatableMapper extends AbstractDatatableMapper {

    /** The datatable dao. */
    @Autowired
    @Qualifier("jdbcBasedDatatableDao")
    protected DatatableDao datatableDao;

    /** {@inheritDoc} */
    @Override
    protected DatatableDao getDatatableDao() {
        return datatableDao;
    }

    /**
     * <class_description> An enum to be used when constructing a where clause with named parameters.
     * <br/><br/>
     * <notes> .
     * <br/><br/>
     * ON : 07-Mar-2017
     * The Enum ComparisonType.
     *
     * @author ARHS Developments
     * @version $$Revision$$$
     */
    protected enum ComparisonType {

        /** The equals. */
        EQUALS(" = :"),
        /** The like. */
        LIKE(" like :");

        /** The sql statement. */
        private final String sqlStatement;

        /**
         * Instantiates a new comparison type.
         *
         * @param sqlStatement the sql statement
         */
        private ComparisonType(String sqlStatement) {
            this.sqlStatement = sqlStatement;
        }

        /**
         * Gets the sql statement.
         *
         * @return the sql statement
         */
        public String getSqlStatement() {
            return sqlStatement;
        }
    }

}
