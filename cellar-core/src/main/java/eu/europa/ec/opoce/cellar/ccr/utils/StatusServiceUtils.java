package eu.europa.ec.opoce.cellar.ccr.utils;

import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;

/**
 * Utility class for Cellar-proxied Status Service API.
 * <br><br>
 * This class cannot be instantiated and all its methods are static.
 *
 * @author EUROPEAN DYNAMICS S.A.
 */
public final class StatusServiceUtils {

    private StatusServiceUtils() {
    }

    /**
     * Builds the base URL of the current web application. This method uses
     * {@link org.springframework.web.servlet.support.ServletUriComponentsBuilder#fromCurrentContextPath()}
     * to construct the base URL, which includes the protocol, host, and port of the
     * current web application.
     *
     * @return The base URL of the current web application as a String.
     */
    public static String buildBaseUrl() {
        return ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString();
    }

    /**
     * Extracts a specific header from the provided {@link HttpHeaders} or returns an empty {@link HttpHeaders} instance if the header is not present.
     *
     * @param originalHeaders The original {@link HttpHeaders} from which to extract the desired header.
     * @param headerToExtract The name of the header to extract.
     * @return {@link HttpHeaders} containing the extracted header, or an empty {@link HttpHeaders} instance if the header is not present.
     */
    public static HttpHeaders extractHeader(HttpHeaders originalHeaders, String headerToExtract) {
        HttpHeaders extractedHeaders = new HttpHeaders();

        if (originalHeaders != null) {
            List<String> desiredHeaderValues = originalHeaders.get(headerToExtract);

            if (desiredHeaderValues != null && !desiredHeaderValues.isEmpty()) {
                extractedHeaders.add(headerToExtract, desiredHeaderValues.get(0));
            }
        }

        return extractedHeaders;
    }

    /**
     * Extracts the value of a specific header from the provided {@link HttpHeaders}.
     *
     * @param originalHeaders The original {@link HttpHeaders} from which to extract the header value.
     * @param headerToExtract The name of the header whose value needs to be extracted.
     * @return The value of the specified header, or null if the header is not present.
     */
    public static String extractHeaderValue(HttpHeaders originalHeaders, String headerToExtract) {
        if (originalHeaders != null) {
            List<String> headerValues = originalHeaders.get(headerToExtract);

            if (headerValues != null && !headerValues.isEmpty()) {
                return headerValues.get(0);
            }
        }

        return null;
    }

    /**
     * This class provides constants to be used throughout the Cellar-proxied Status Service API.
     *
     * @author EUROPEAN DYNAMICS S.A.
     */
    public static final class StatusServiceConstants {
        public static final String ADMIN = "/admin";
        public static final String CELLAR_STATUS_V1_BASE_URL = "/v1/secapi/status";
        public static final String CELLAR_STATUS_PACKAGE_ENDPOINT = "/mets";
        public static final String CELLAR_STATUS_OBJECT_ENDPOINT = "/psi";
        public static final String CELLAR_STATUS_LOOKUP_ENDPOINT = "/package";

        public static final String STATUS_SERVICE_API_EXCEPTION_LOG_MESSAGE = "Failed to retrieve response from Status Service.";

        public static final String STATUS_SERVICE_API_EXCEPTION_LOG_MESSAGE_WITH_REASON = STATUS_SERVICE_API_EXCEPTION_LOG_MESSAGE + " Reason: [{}]";

        public static final String STATUS_SERVICE_API_EXCEPTION_LOG_MESSAGE_WITH_REASON_AND_CORRELATION_ID =
                STATUS_SERVICE_API_EXCEPTION_LOG_MESSAGE_WITH_REASON + " Correlation id : [{}]";

        private StatusServiceConstants() {
        }
    }
}
