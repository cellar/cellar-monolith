/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.rdfStoreCleaner.delegator
 *             FILE : HierarchyDelegator.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 14, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.rdfStoreCleaner.delegator;

import eu.europa.ec.opoce.cellar.cl.delegator.AbstractDelegator;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;

import java.util.Arrays;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 14, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Configurable
public class HierarchyDelegator extends AbstractDelegator {

    /** The cellar resource dao. */
    @Autowired
    private CellarResourceDao cellarResourceDao;

    /** The root. */
    private final CellarResource root;

    /**
     * Instantiates a new hierarchy delegator.
     *
     * @param root the root
     */
    public HierarchyDelegator(final CellarResource root) {
        super(root.getCellarId());
        this.root = root;
    }

    /**
     * Gets the root.
     *
     * @return the root
     */
    public CellarResource getRoot() {
        return this.root;
    }

    /** {@inheritDoc} */
    @Override
    public Collection<CellarResource> getCellarResources() {
        if (this.cellarResources == null) {
            this.cellarResources = this.cellarResourceDao.findTreeMetadataSortByCellarId(this.getRootCellarId());
        }

        return this.cellarResources;
    }

    /** {@inheritDoc} */
    @Override
    public Collection<CellarResource> getProcessableCellarResources() {
        return Arrays.asList(this.root);
    }
}
