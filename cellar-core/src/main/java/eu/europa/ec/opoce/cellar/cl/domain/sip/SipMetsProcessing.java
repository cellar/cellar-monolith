package eu.europa.ec.opoce.cellar.cl.domain.sip;

import javax.persistence.*;
import java.util.Date;

/**
 * This object represents a SIP-METS Processing.
 * 
 * @author dcraeye
 * 
 */
@Entity
@Table(name = "SIP_METS_PROCESSING")
@NamedQueries({
        @NamedQuery(name = "getBySipName", query = "from SipMetsProcessing where SIP_NAME = ?0 and SIP_PROCESSING_END_DATE is NULL"),
        @NamedQuery(name = "getByMetsFolder", query = "from SipMetsProcessing where METS_FOLDER = ?0 and SIP_PROCESSING_END_DATE is NULL"),
        @NamedQuery(name = "getAll", query = "from SipMetsProcessing where SIP_PROCESSING_END_DATE is NULL order by PRIORITY DESC, STRUCTMAP_COUNT DESC, ID"),
        @NamedQuery(name = "getAllByPriority", query = "from SipMetsProcessing where SIP_PROCESSING_END_DATE is NULL order by PRIORITY DESC, ID"),
        @NamedQuery(name = "getAllBySMCount", query = "from SipMetsProcessing where SIP_PROCESSING_END_DATE is NULL order by STRUCTMAP_COUNT DESC, ID"),
        @NamedQuery(name = "getAllByDOCount", query = "from SipMetsProcessing where SIP_PROCESSING_END_DATE is NULL order by DIGITAL_OBJECT_COUNT DESC, ID")})
public class SipMetsProcessing {

    /**
     * Internal id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    /**
     * The SIP processing start date.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "SIP_PROCESSING_START_DATE")
    public Date sipProcessingStartDate;

    /**
     * The SIP processing date.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "SIP_PROCESSING_END_DATE")
    public Date sipProcessingEndDate;

    /**
     * The sip name. 
     */
    @Column(name = "SIP_NAME")
    public String sipName;

    /**
     * The mets folder. (relative to the temporay work folder)
     */
    @Column(name = "METS_FOLDER")
    public String metsFolder;

    /**
     * The priority.
     */
    @Column(name = "PRIORITY")
    public int priority;

    /**
     * The sip reception folder.
     */
    @Column(name = "RECEPTION_FOLDER")
    public String receptionFolder;

    /**
     * The number of structmap found in this mets.
     */
    @Column(name = "STRUCTMAP_COUNT")
    public int structMapCount;

    /**
     * The number of digital object found in this mets.
     */
    @Column(name = "DIGITAL_OBJECT_COUNT")
    public int digitalObjectCount;

    /**
     * The type of this mets : create/update/read.
     */
    @Column(name = "METS_TYPE")
    public String type;

    /**
     * Create a new object.
     * 
     */
    public SipMetsProcessing() {
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getSipProcessingStartDate() {
        return sipProcessingStartDate;
    }

    public void setSipProcessingStartDate(Date sipProcessingStartDate) {
        this.sipProcessingStartDate = sipProcessingStartDate;
    }

    public Date getSipProcessingEndDate() {
        return sipProcessingEndDate;
    }

    public void setSipProcessingEndDate(Date sipProcessingEndDate) {
        this.sipProcessingEndDate = sipProcessingEndDate;
    }

    public String getSipName() {
        return sipName;
    }

    public void setSipName(String sipName) {
        this.sipName = sipName;
    }

    public String getMetsFolder() {
        return metsFolder;
    }

    public void setMetsFolder(String metsFolder) {
        this.metsFolder = metsFolder;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getStructMapCount() {
        return structMapCount;
    }

    public void setStructMapCount(int structMapCount) {
        this.structMapCount = structMapCount;
    }

    public int getDigitalObjectCount() {
        return digitalObjectCount;
    }

    public void setDigitalObjectCount(int digitalObjectCount) {
        this.digitalObjectCount = digitalObjectCount;
    }

    public String getReceptionFolder() {
        return receptionFolder;
    }

    public void setReceptionFolder(String receptionFolder) {
        this.receptionFolder = receptionFolder;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
