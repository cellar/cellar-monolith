package eu.europa.ec.opoce.cellar.cl.service.client;

import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;

/**
 * Handles the resolution of identifier to production identifiers and cellar identifiers.
 * @author phvdveld
 *
 */
public interface IdentifierResolver {

    /**
     * Gets the CellarIdentifier associated to the given string id, be it a cellar id or a production id.
     * @param id the string representation of the cellar id (ex:<code>cellar:110E8400-E29B-11D4-A716-4466554</code>) or the production id (ex: <code>oj:JOL_2006_088_R_0063_01.fra.PDF</code> or <code>celex:32006D0241.fra.PDF</code>)
     * @return a {@link CellarIdentifier}
     */
    CellarIdentifier resolveToCellarId(String id);

    /**
     * Gets the CellarIdentifier associated to the given string id, be it a cellar id or a production id.
     * 
     * @param namespace the namespace attached to the identifier
     * @param id the string representation of the cellar id (ex:<code>110E8400-E29B-11D4-A716-4466554</code>) or the production id (ex: <code>JOL_2006_088_R_0063_01.fra.PDF</code> or <code>32006D0241.fra.PDF</code>) without the namespace
     * @return a {@link CellarIdentifier}
     */
    CellarIdentifier resolveToCellarId(String namespace, String id);

    /**
     * Get the datastream id of the <code>CellarIdentifier</code> identified by the specified UUID and the specified file
     * name.
     * 
     * @param UUID
     *            CELLAR UUID.
     * @param fileRef
     *            File name
     * @return Datastream id.
     */
    String getDatastreamId(String UUID, String fileRef);

}
