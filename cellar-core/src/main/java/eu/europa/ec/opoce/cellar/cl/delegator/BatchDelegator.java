/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.rdfStoreCleaner.delegator
 *             FILE : BatchDelegator.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 14, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.delegator;

import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.common.util.CellarIdUtils;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 14, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class BatchDelegator extends AbstractDelegator {

    /**
     * Instantiates a new batch delegator.
     *
     * @param rootCellarId the root cellar id
     * @param cellarResources the cellar resources
     */
    public BatchDelegator(final String rootCellarId, final Collection<CellarResource> cellarResources) {
        super(rootCellarId, cellarResources);
    }

    /** {@inheritDoc} */
    @Override
    public Collection<CellarResource> getProcessableCellarResources() {
        return this.getCellarResources();
    }

    /**
     * Gets the batches by cellar base id.
     *
     * @param cellarResources the cellar resources
     * @return the batches by cellar base id
     */
    public static Map<String, Collection<CellarResource>> getBatchesByCellarBaseId(final Collection<CellarResource> cellarResources) {
        final Map<String, Collection<CellarResource>> batches = new HashMap<String, Collection<CellarResource>>();
        String cellarIdBase = null;
        Collection<CellarResource> batch = null;
        for (final CellarResource cr : cellarResources) {
            cellarIdBase = CellarIdUtils.getCellarIdBase(cr.getCellarId());
            batch = batches.get(cellarIdBase);

            if (batch == null) {
                batch = new LinkedList<CellarResource>();
                batches.put(cellarIdBase, batch);
            }

            batch.add(cr);
        }
        return batches;
    }

}
