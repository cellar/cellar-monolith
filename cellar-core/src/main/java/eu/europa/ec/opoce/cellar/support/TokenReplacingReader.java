/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cmr.web.controller.admin
 *        FILE : TokenReplacingReader.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 30-01-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.support;

import eu.europa.ec.opoce.cellar.exception.NotImplementedExceptionBuilder;

import java.io.IOException;
import java.io.PushbackReader;
import java.io.Reader;
import java.nio.CharBuffer;
import java.util.Map;

/**
 * <class_description>
 * Sometimes you need to replace strings or tokens in streams, arrays, files, or large strings.<br>
 * You could use the String.replace() method, but for large amounts of data, and high number of replacements, this performs badly and is prone to out of memory errors.<br>
 * Instead of using the String.replace() method it is presented here a different, more scalable solution called a TokenReplacingReader.<br>
 * <br>
 * Here is described how it works in theory.<br>
 * <br>
 * The TokenReplacingReader reads character data from a standard java.io.Reader.<br>
 * Your application then reads data via the TokenReplacingReader. The data your application reads from the TokenReplacingReader will be the data read from the Reader used by the TokenReplacingReader,
 * with all tokens replaced with replacers.<br>
 * When the TokenReplacingReader finds a token in that data, it obtains the replacer from the map <code>replacers</code>,where the key is the token and the value is the replacer: such replacer is then inserter
 * into the character stream instead of the token.<br>
 * <br>
 * The TokenReplacingReader is itself a subclass of java.io.Reader, so any class that can use a Reader, can use a TokenReplacingReader.
 *
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * ON : 02-08-2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class TokenReplacingReader extends Reader {

    protected PushbackReader pushbackReader;
    protected StringBuilder tokenNameBuffer;
    protected String tokenValue;
    protected int tokenValueIndex;
    protected Map<String, String> replacers;

    public TokenReplacingReader(final Reader source, final Map<String, String> replacers) {
        this.tokenNameBuffer = new StringBuilder();
        this.tokenValueIndex = 0;
        this.pushbackReader = new PushbackReader(source, 8);
        this.replacers = replacers;
    }

    public int read() throws IOException {
        if (this.tokenValue != null) {
            if (this.tokenValueIndex < this.tokenValue.length()) {
                return this.tokenValue.charAt(this.tokenValueIndex++);
            }
            if (this.tokenValueIndex == this.tokenValue.length()) {
                this.tokenValue = null;
                this.tokenValueIndex = 0;
            }
        }

        boolean keyFound = true;
        for (final String key : this.replacers.keySet()) {
            this.tokenNameBuffer.delete(0, this.tokenNameBuffer.length());
            keyFound = true;
            for (int i = 0; keyFound && i < key.length(); i++) {
                final int currChar = key.charAt(i);
                int data = this.pushbackReader.read();
                this.tokenNameBuffer.append((char) data);
                if (data != currChar) {
                    for (int j = this.tokenNameBuffer.length() - 1; j >= 0; j--) {
                        this.pushbackReader.unread(this.tokenNameBuffer.charAt(j));
                    }
                    keyFound = false;
                }
            }
            if (keyFound) {
                break;
            }
        }
        if (!keyFound) {
            return this.pushbackReader.read();
        }

        this.tokenValue = this.replacers.get(this.tokenNameBuffer.toString());

        if (this.tokenValue == null) {
            this.tokenValue = this.tokenNameBuffer.toString();
        }

        return this.tokenValue.charAt(this.tokenValueIndex++);
    }

    public int read(char cbuf[], int off, int len) throws IOException {
        int charsRead = 0;
        for (int i = 0; i < len; i++) {
            int nextChar = read();
            if (nextChar == -1 || nextChar == Integer.valueOf(Character.MAX_VALUE).intValue()) {
                charsRead = i;
                if (charsRead == 0) {
                    charsRead = -1;
                }
                break;
            }
            cbuf[off + i] = (char) nextChar;
        }
        return charsRead;
    }

    public void close() throws IOException {
        this.pushbackReader.close();
    }

    public boolean ready() throws IOException {
        return this.pushbackReader.ready();
    }

    public boolean markSupported() {
        return false;
    }

    public int read(CharBuffer target) throws IOException {
        throw NotImplementedExceptionBuilder.get().build();
    }

    public long skip(long n) throws IOException {
        throw NotImplementedExceptionBuilder.get().build();
    }

    public void mark(int readAheadLimit) throws IOException {
        throw NotImplementedExceptionBuilder.get().build();
    }

    public void reset() throws IOException {
        throw NotImplementedExceptionBuilder.get().build();
    }

}
