package eu.europa.ec.opoce.cellar.cl.dao.aligner.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.enums.CellarServiceRDFStoreCleanerMode;
import eu.europa.ec.opoce.cellar.cl.dao.aligner.AlignerMisalignmentDao;
import eu.europa.ec.opoce.cellar.cl.dao.impl.BaseDaoImpl;
import eu.europa.ec.opoce.cellar.cl.domain.aligner.AlignerMisalignment;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.transform.ResultTransformer;
import org.springframework.stereotype.Repository;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * The Class AlignerMisalignmentDaoImpl.
 */
@Repository
public class AlignerMisalignmentDaoImpl extends BaseDaoImpl<AlignerMisalignment, Long> implements AlignerMisalignmentDao {

    /**
     * {@inheritDoc}
     */
    @Override
    public Long count() {
        return this.getHibernateTemplate().execute(session -> {
            final Query queryObject = session.createQuery("select count(id) from AlignerMisalignment");
            return (Long) queryObject.uniqueResult();
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String[]> findAll(final String cellarId, final CellarServiceRDFStoreCleanerMode cellarServiceRDFStoreCleanerMode,
                                  final eu.europa.ec.opoce.cellar.cl.enums.aligner.Repository repository, final String sortOrder, final int column,
                                  final int startPage, final int pageSize) {

        final StringBuffer buffer = new StringBuffer("select am from AlignerMisalignment am");
        this.addWhereClause(cellarId, cellarServiceRDFStoreCleanerMode, repository, buffer);

        buffer.append(" ORDER BY ");
        switch (column) {
            case 0:
                buffer.append("cellarId ");
                break;
            case 1:
                buffer.append("operationDate ");
                break;
            case 2:
                buffer.append("operationType ");
                break;
            case 3:
                buffer.append("repository ");
                break;
            default:
                break;
        }
        buffer.append(sortOrder.toUpperCase());

        return this.getHibernateTemplate().execute(session -> {
            final Query queryObject = session.createQuery(buffer.toString());
            addParameters(cellarId, cellarServiceRDFStoreCleanerMode, repository, queryObject);
            queryObject.setFetchSize(pageSize);
            queryObject.setMaxResults(pageSize);
            queryObject.setFirstResult(startPage);

            final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            queryObject.setResultTransformer(new ResultTransformer() {

                @Override
                public Object transformTuple(final Object[] tuple, final String[] aliases) {
                    final AlignerMisalignment row = (AlignerMisalignment) tuple[0];
                    final String[] alignerMisalignmentRow = new String[]{
                            row.getCellarId(), simpleDateFormat.format(row.getOperationDate()), row.getOperationType().toString(),
                            row.getRepository().toString(), row.getCellarId()};
                    return alignerMisalignmentRow;
                }

                @SuppressWarnings("rawtypes")
                @Override
                public List transformList(final List collection) {
                    return collection;
                }
            });
            return queryObject.list();
        });
    }

    /**
     * Adds the parameters.
     *
     * @param cellarId                         the cellar id
     * @param cellarServiceRDFStoreCleanerMode the cellar service rdf store cleaner mode
     * @param repository                       the repository
     * @param queryObject                      the query object
     */
    private void addParameters(final String cellarId, final CellarServiceRDFStoreCleanerMode cellarServiceRDFStoreCleanerMode,
                               final eu.europa.ec.opoce.cellar.cl.enums.aligner.Repository repository, final Query queryObject) {
        if (StringUtils.isNotBlank(cellarId) || (cellarServiceRDFStoreCleanerMode != null) || (repository != null)) {
            if (StringUtils.isNotBlank(cellarId)) {
                queryObject.setParameter("cellarId", cellarId);
            }
            if (cellarServiceRDFStoreCleanerMode != null) {
                queryObject.setParameter("operationType", cellarServiceRDFStoreCleanerMode);
            }
            if (repository != null) {
                queryObject.setParameter("repository", repository);
            }
        }
    }

    /**
     * Adds the where clause.
     *
     * @param cellarId                         the cellar id
     * @param cellarServiceRDFStoreCleanerMode the cellar service rdf store cleaner mode
     * @param repository                       the repository
     * @param buffer                           the buffer
     */
    private void addWhereClause(final String cellarId, final CellarServiceRDFStoreCleanerMode cellarServiceRDFStoreCleanerMode,
                                final eu.europa.ec.opoce.cellar.cl.enums.aligner.Repository repository, final StringBuffer buffer) {
        if (StringUtils.isNotBlank(cellarId) || (cellarServiceRDFStoreCleanerMode != null) || (repository != null)) {
            //create where clause
            int count = 0;//number of parameters different than 0 to add the AND clause
            buffer.append(" WHERE ");
            if (StringUtils.isNotBlank(cellarId)) {
                buffer.append("cellarId like :cellarId");
                count++;
            }

            if (cellarServiceRDFStoreCleanerMode != null) {
                if (count > 0) {
                    buffer.append(" AND ");
                }
                buffer.append("operationType = :operationType");
                count++;
            }

            if (repository != null) {
                if (count > 0) {
                    buffer.append(" AND ");
                }
                buffer.append("repository = :repository");
            }

        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long count(final String cellarId, final CellarServiceRDFStoreCleanerMode cellarServiceRDFStoreCleanerMode,
                      final eu.europa.ec.opoce.cellar.cl.enums.aligner.Repository repository) {

        final StringBuffer buffer = new StringBuffer("select count(id) from AlignerMisalignment");
        this.addWhereClause(cellarId, cellarServiceRDFStoreCleanerMode, repository, buffer);

        return this.getHibernateTemplate().execute(session -> {
            final Query queryObject = session.createQuery(buffer.toString());
            addParameters(cellarId, cellarServiceRDFStoreCleanerMode, repository, queryObject);
            return (Long) queryObject.uniqueResult();
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AlignerMisalignment save(final AlignerMisalignment alignerMisalignment) {
        return super.saveObject(alignerMisalignment);
    }

}
