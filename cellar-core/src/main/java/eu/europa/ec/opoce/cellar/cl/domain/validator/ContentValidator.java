package eu.europa.ec.opoce.cellar.cl.domain.validator;

import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPObject;
import eu.europa.ec.opoce.cellar.cl.exception.ValidationException;
import eu.europa.ec.opoce.cellar.cl.service.client.MetsAnalyzer;
import eu.europa.ec.opoce.cellar.domain.content.mets.OperationType;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Implementation of the {@link ContentValidator}.
 *
 * @author dcraeye
 *
 */
@Component("contentValidator")
public class ContentValidator implements SystemValidator {

    /** get mets file information. */
    private MetsAnalyzer metsAnalyzer;

    @Autowired
    public ContentValidator(MetsAnalyzer metsAnalyzer) {
        this.metsAnalyzer = metsAnalyzer;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ValidationResult validate(final ValidationDetails validationDetails) {
        final ValidationResult result = new ValidationResult();

        if (validationDetails.getToValidate() == null) {
            throw ExceptionBuilder.get(ValidationException.class).withCode(CoreErrors.E_070).withMessage("object cannot be null")
                    .withCause(new NullPointerException()).build();
        }
        if (!(validationDetails.getToValidate() instanceof SIPObject)) {
            throw ExceptionBuilder.get(ValidationException.class).withCode(CoreErrors.E_071)
                    .withMessage(validationDetails.getToValidate().getClass().toString()).build();
        }
        final SIPObject sip = (SIPObject) validationDetails.getToValidate();

        //        final List<String> metsFileNames = this.metsAnalyzer.getMetsLinkedFileListFromTemporaryWorkFolder(sip.getMetsFile());
        final List<String> metsFileNames = this.metsAnalyzer.readAndExtractHREFreferences(sip.getMetsFile());

        for (final String metsFilename : metsFileNames) {
            if (StringUtils.startsWithIgnoreCase(metsFilename, "file://")) {
                if (!new File(StringUtils.substringAfter(metsFilename, "file://")).exists()) {
                    result.setStatusCode(-1);
                    result.addMessage(
                            "The file [" + metsFilename + "] referenced in sip [" + sip.getZipFile().getName() + "] doesn't exists.");
                    result.setValid(false);
                }
            } else if (StringUtils.startsWithIgnoreCase(metsFilename, "http://")) {
                // TODO: test if file exist on this url.
                continue;
            } else {
                if (!fileExists(sip.getZipExtractionFolder(), metsFilename)) {
                    result.setStatusCode(-1);
                    result.addMessage(
                            "The file [" + metsFilename + "] referenced in mets cannot be found in the sip [" + sip.getZipFile().getName()
                                    + "].");
                    result.setValid(false);
                }
            }
        }

        // VALIDATE that all FPTR tags have valid FILEID attribute if it's a CREATE.req Mets's TYPE
        final OperationType metsType = this.metsAnalyzer.getMetsTypeFromTemporaryWorkFolder(sip.getMetsFile());
        if (OperationType.Create.equals(metsType)) {
            final List<String> metsFptrFileIDList = this.metsAnalyzer.getMetsFptrFileIDListFromTemporaryWorkFolder(sip.getMetsFile());
            final List<String> existingFileIdInSipList = this.metsAnalyzer.getMetsFileIDListFromTemporaryWorkFolder(sip.getMetsFile());
            for (final String metsFptrFileID : metsFptrFileIDList) {
                if (!existingFileIdInSipList.contains(metsFptrFileID)) {
                    result.setStatusCode(-1);
                    result.addMessage(
                            "The FILEID [" + metsFptrFileID + "] reference an unknown FILE in the sip [" + sip.getZipFile().getName()
                                    + "].");
                    result.setValid(false);
                }
            }
        }

        return result;
    }

    private boolean fileExists(File zipExtractionFolder, String metsFilename) {
        Path path = Paths.get(zipExtractionFolder.toString() + File.separator + metsFilename);
        return Files.exists(path);
    }
}
