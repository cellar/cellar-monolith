/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.batchJob
 *             FILE : BatchJob.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 11, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.batchJob;

import eu.europa.ec.opoce.cellar.cl.service.client.IIndexingService.Priority;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

/**
 * <class_description> Batch job
 * <p>
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 11, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Entity
@Table(name = "BATCH_JOB")
@NamedQueries({
        @NamedQuery(name = "findBatchJobsByStatus", query = "select b from BatchJob b where b.jobStatus = ?0 order by lastModified ASC, creationDate ASC"),
        @NamedQuery(name = "findBatchJobsByInStatus", query = "select b from BatchJob b where b.jobStatus in (:status) order by lastModified ASC, creationDate ASC"),
        @NamedQuery(name = "findBatchJobsByType", query = "select b from BatchJob b where b.jobType = ?0 order by lastModified ASC, creationDate ASC"),
        @NamedQuery(name = "findBatchJobByIdType", query = "select b from BatchJob b where b.id = ?0 and b.jobType = ?1 order by lastModified ASC, creationDate ASC"),
        @NamedQuery(name = "findBatchJobByTypeStatus", query = "select b from BatchJob b where b.jobStatus = ?0 and b.jobType = ?1 order by lastModified ASC, creationDate ASC"),
        @NamedQuery(name = "findUpdateBatchJobCrons", query = "select distinct b.cron from BatchJob b where b.jobType = 'UPDATE'"),
        @NamedQuery(name = "findUpdateBatchJobByStatusCron", query = "select b from BatchJob b where b.jobType = 'UPDATE' and b.jobStatus = ?0 and b.cron = ?1 order by lastModified ASC, creationDate ASC")})
public class BatchJob {

    public enum STATUS {
        NEW,
        EXECUTING_SPARQL,
        READY,
        WAITING_PROCESSING,
        PROCESSING,
        ERROR,
        DONE,
        AUTO
    }

    public enum BATCH_JOB_TYPE {
        REINDEX,
        EMBARGO,
        EXPORT,
        UPDATE,
        LAST_MODIFICATION_DATE_UPDATE,
        DELETE
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "JOB_TYPE", nullable = false)
    private BATCH_JOB_TYPE jobType;

    @Column(name = "JOB_NAME", nullable = false)
    private String jobName;

    @Enumerated(EnumType.STRING)
    @Column(name = "JOB_STATUS", nullable = false)
    private STATUS jobStatus;

    @Column(name = "ERROR_DESCRIPTION")
    private String errorDescription;

    @Column(name = "ERROR_STACKTRACE")
    private String errorStacktrace;

    @Column(name = "CREATION_DATE", nullable = false)
    private Date creationDate;

    @Column(name = "LAST_MODIFIED", nullable = false)
    private Date lastModified;

    @Column(name = "NUMBER_ITEMS")
    private Long numberItems;

    @Column(name = "SPARQL_QUERY")
    private String sparqlQuery;

    @Column(name = "SPARQL_UPDATE_QUERY")
    private String sparqlUpdateQuery;

    @Column(name = "CRON_CONFIG")
    private String cron;

    @Enumerated(EnumType.STRING)
    @Column(name = "PRIORITY")
    private Priority indexingPriority;

    @Column(name = "EMBARGO_DATE")
    private Date embargoDate;

    @Column(name = "GENERATE_INDEX_NOTICE")
    private Boolean generateIndexNotice;

    @Column(name = "GENERATE_EMBEDDED_NOTICE")
    private Boolean generateEmbeddedNotice;

    @OneToMany(mappedBy = "batchJob", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Set<BatchJobWorkIdentifier> batchJobWorkIdentifiers;

    @Column(name = "METADATA_ONLY")
    private Boolean metadataOnly;

    @Column(name = "USE_AGNOSTIC_URL")
    private Boolean useAgnosticURL;

    @Column(name = "DESTINATION_FOLDER")
    private String destinationFolder;

    public BatchJob() {}

    public BatchJob(Builder builder) {
        this.id = builder.id;
        this.jobType = builder.type;
        this.jobName = builder.jobName;
        this.sparqlQuery = builder.sparqlQuery;
        this.cron = builder.cron;
        this.indexingPriority = builder.indexingPriority;
        this.sparqlUpdateQuery = builder.sparqlUpdateQuery;
        this.jobStatus = builder.jobStatus;
        this.embargoDate = builder.embargoDate;
        this.generateIndexNotice = builder.generateIndexNotice;
        this.generateEmbeddedNotice = builder.generateEmbeddedNotice;
        this.metadataOnly = builder.metadataOnly;
        this.destinationFolder = builder.destinationFolder;
        this.useAgnosticURL = builder.useAgnosticURL;
        this.creationDate = new Date();
        this.lastModified = new Date();
    }

    public static Builder create(BATCH_JOB_TYPE type, String jobName, String sparqlQuery) {
        return new Builder(type, jobName, sparqlQuery);
    }

    public static class Builder {

        private final BATCH_JOB_TYPE type;
        private final String jobName;
        private final String sparqlQuery;

        private Long id;
        private String sparqlUpdateQuery;
        private String cron;
        private Priority indexingPriority;
        private STATUS jobStatus = STATUS.NEW;
        private Date embargoDate;
        private Boolean generateIndexNotice;
        private Boolean generateEmbeddedNotice;
        private Boolean metadataOnly;
        private String destinationFolder;
        private Boolean useAgnosticURL;

        public Builder(BATCH_JOB_TYPE type, String jobName, String sparqlQuery) {
            this.type = type;
            this.jobName = jobName;
            this.sparqlQuery = sparqlQuery;
        }

        public Builder sparqlUpdateQuery(String sparqlUpdateQuery) {
            this.sparqlUpdateQuery = sparqlUpdateQuery;
            return this;
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Builder cron(String cron) {
            this.cron = cron;
            return this;
        }

        public Builder indexingPriority(Priority indexingPriority) {
            this.indexingPriority = indexingPriority;
            return this;
        }

        public Builder jobStatus(STATUS jobStatus) {
            this.jobStatus = jobStatus;
            return this;
        }

        public Builder embargoDate(Date embargoDate) {
            this.embargoDate = new Date(embargoDate.getTime());
            return this;
        }

        public Builder generateIndexNotice(Boolean generateIndexNotice) {
            this.generateIndexNotice = generateIndexNotice;
            return this;
        }

        public Builder generateEmbeddedNotice(Boolean generateEmbeddedNotice) {
            this.generateEmbeddedNotice = generateEmbeddedNotice;
            return this;
        }

        public Builder metadataOnly(Boolean metadataOnly) {
            this.metadataOnly = metadataOnly;
            return this;
        }

        public Builder destinationFolder(String destinationFolder) {
            this.destinationFolder = destinationFolder;
            return this;
        }

        public Builder useAgnosticURL(Boolean useAgnosticURL) {
            this.useAgnosticURL = useAgnosticURL;
            return this;
        }

        public BatchJob build() {
            return new BatchJob(this);
        }
    }

    /**
     * Gets the value of the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the value of the id.
     *
     * @param id the id to set.
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the job type.
     *
     * @return the job type
     */
    public BATCH_JOB_TYPE getJobType() {
        return this.jobType;
    }

    /**
     * Sets the job type.
     *
     * @param jobType the job type to set
     */
    public void setJobType(final BATCH_JOB_TYPE jobType) {
        this.jobType = jobType;
    }

    /**
     * Gets the job name.
     *
     * @return the job name.
     */
    public String getJobName() {
        return this.jobName;
    }

    /**
     * Sets the job name.
     *
     * @param jobName the job name to set.
     */
    public void setJobName(final String jobName) {
        this.jobName = jobName;
    }

    /**
     * Gets the job status.
     *
     * @return the job status.
     */
    public STATUS getJobStatus() {
        return this.jobStatus;
    }

    /**
     * Sets the job status.
     *
     * @param jobStatus the job status to set.
     */
    public void setJobStatus(final STATUS jobStatus) {
        this.jobStatus = jobStatus;
    }

    /**
     * Gets the error description, if any.
     *
     * @return the error description, if any. Otherwise, null.
     */
    public String getErrorDescription() {
        return this.errorDescription;
    }

    /**
     * Sets the error description.
     *
     * @param errorDescription the error description to set.
     */
    public void setErrorDescription(final String errorDescription) {
        this.errorDescription = errorDescription;
    }

    /**
     * Gets the error stacktrace, if any.
     *
     * @return the error stacktrace, if any. Otherwise, null.
     */
    public String getErrorStacktrace() {
        return this.errorStacktrace;
    }

    /**
     * Sets the error stacktrace.
     *
     * @param errorStacktrace the error stacktrace to set.
     */
    public void setErrorStacktrace(final String errorStacktrace) {
        this.errorStacktrace = errorStacktrace;
    }

    /**
     * Gets the creation date of the job.
     *
     * @return the creation date of the job.
     */
    public Date getCreationDate() {
        return new Date(creationDate.getTime());
    }

    /**
     * Sets the creation date of the job.
     *
     * @param creationDate the creation date to set.
     */
    public void setCreationDate(final Date creationDate) {
        this.creationDate = new Date(creationDate.getTime());
    }

    /**
     * Gets the last modified date of the job.
     *
     * @return the last modified date of the job.
     */
    public Date getLastModified() {
        return new Date(lastModified.getTime());
    }

    /**
     * Sets the last modified date of the job.
     *
     * @param lastModified the last modified date of the job.
     */
    public void setLastModified(final Date lastModified) {
        this.lastModified = new Date(lastModified.getTime());
    }

    /**
     * Gets the number of works to reindex.
     *
     * @return the number of works to reindex.
     */
    public Long getNumberItems() {
        return this.numberItems;
    }

    /**
     * Sets the number of works to reindex.
     *
     * @param numberItems the number of works to reindex.
     */
    public void setNumberItems(final Long numberItems) {
        this.numberItems = numberItems;
    }

    /**
     * Gets the SPARQL query to find the works.
     *
     * @return the SPARQL query.
     */
    public String getSparqlQuery() {
        return this.sparqlQuery;
    }

    /**
     * Sets the SPARQL query to find the works.
     *
     * @param sparqlQuery the SPARQL query to set.
     */
    public void setSparqlQuery(final String sparqlQuery) {
        this.sparqlQuery = sparqlQuery;
    }

    /**
     * Gets the works corresponding to the SPARQL query executed.
     *
     * @return The works corresponding the SPARQL query executed. Otherwise, false.
     */
    public Set<BatchJobWorkIdentifier> getBatchJobWorkIdentifiers() {
        return this.batchJobWorkIdentifiers;
    }

    /**
     * Sets the works corresponding to the SPARQL query executed.
     *
     * @param batchJobWorkIdentifiers the new batch job work identifiers
     */
    public void setBatchJobWorkIdentifiers(final Set<BatchJobWorkIdentifier> batchJobWorkIdentifiers) {
        this.batchJobWorkIdentifiers = batchJobWorkIdentifiers;
    }

    /**
     * Gets the priority of the job.
     *
     * @return the priority of the job
     */
    public Priority getPriority() {
        return this.indexingPriority;
    }

    /**
     * Sets the priority of the job.
     *
     * @param indexingPriority the new priority
     */
    public void setPriority(final Priority indexingPriority) {
        this.indexingPriority = indexingPriority;
    }

    /**
     * Gets the embargo date of the job.
     *
     * @return the embargo date of the job
     */
    public Date getEmbargoDate() {
        return new Date(embargoDate.getTime());
    }

    /**
     * Checks if is generate index notice.
     *
     * @return the generateIndexNotice
     */
    public boolean isGenerateIndexNotice() {
        return this.generateIndexNotice != null && this.generateIndexNotice;
    }

    /**
     * Sets the generate index notice.
     *
     * @param generateIndexNotice the generateIndexNotice to set
     */
    public void setGenerateIndexNotice(final boolean generateIndexNotice) {
        this.generateIndexNotice = generateIndexNotice;
    }

    /**
     * Checks if is generate embedded notice.
     *
     * @return the generateEmbeddedNotice
     */
    public boolean isGenerateEmbeddedNotice() {
        return this.generateEmbeddedNotice != null && this.generateEmbeddedNotice;
    }

    /**
     * Sets the generate embedded notice.
     *
     * @param generateEmbeddedNotice the generateEmbeddedNotice to set
     */
    public void setGenerateEmbeddedNotice(final boolean generateEmbeddedNotice) {
        this.generateEmbeddedNotice = generateEmbeddedNotice;
    }

    /**
     * Sets the embargo date of the job.
     *
     * @param embargoDate the embargo date of the job to set
     */
    public void setEmbargoDate(final Date embargoDate) {
        this.embargoDate = new Date(embargoDate.getTime());
    }

    /**
     * Returns true if this job is deletable.
     *
     * @return true if the job is deletable, otherwise null
     */
    public boolean isDeletable() {
        return this.jobStatus == STATUS.NEW || this.jobStatus == STATUS.READY || this.jobStatus == STATUS.WAITING_PROCESSING
                || this.jobStatus == STATUS.DONE || this.jobStatus == STATUS.ERROR || this.jobStatus == STATUS.AUTO;
    }

    /**
     * Returns true if this job is restartable.
     *
     * @return true if the job is restartable, otherwise null
     */
    public boolean isRestartable() {
        return this.jobStatus == STATUS.READY || this.jobStatus == STATUS.DONE || this.jobStatus == STATUS.ERROR;
    }

    /**
     * Returns true if the indexing of this job is startable.
     *
     * @return true if the indexing of this job is startable, otherwise null
     */
    public boolean isStartable() {
        return this.jobStatus == STATUS.READY;
    }

    /**
     * Updates the last modified date to now.
     */
    public void modifiedNow() {
        this.lastModified = new Date();
    }

    /**
     * Gets the sparql update query.
     *
     * @return the sparqlUpdateQuery
     */
    public String getSparqlUpdateQuery() {
        return this.sparqlUpdateQuery;
    }

    /**
     * Sets the sparql update query.
     *
     * @param sparqlUpdateQuery the sparqlUpdateQuery to set
     */
    public void setSparqlUpdateQuery(final String sparqlUpdateQuery) {
        this.sparqlUpdateQuery = sparqlUpdateQuery;
    }

    /**
     * Gets the cron.
     *
     * @return the cron
     */
    public String getCron() {
        return this.cron;
    }

    /**
     * Sets the cron.
     *
     * @param cron the cron to set
     */
    public void setCron(final String cron) {
        this.cron = cron;
    }

    /**
     * Gets the metadata only.
     *
     * @return the metadata only
     */
    public Boolean getMetadataOnly() {
        return this.metadataOnly;
    }

    /**
     * Sets the metadata only.
     *
     * @param metadataOnly the new metadata only
     */
    public void setMetadataOnly(final Boolean metadataOnly) {
        this.metadataOnly = metadataOnly;
    }

    /**
     * Gets the use agnostic url.
     *
     * @return the use agnostic url
     */
    public Boolean getUseAgnosticURL() {
        return this.useAgnosticURL;
    }

    /**
     * Sets the use agnostic url.
     *
     * @param useAgnosticURL the new use agnostic url
     */
    public void setUseAgnosticURL(final Boolean useAgnosticURL) {
        this.useAgnosticURL = useAgnosticURL;
    }

    /**
     * Gets the destination folder.
     *
     * @return the destination folder
     */
    public String getDestinationFolder() {
        return this.destinationFolder;
    }

    /**
     * Sets the destination folder.
     *
     * @param destinationFolder the new destination folder
     */
    public void setDestinationFolder(final String destinationFolder) {
        this.destinationFolder = destinationFolder;
    }
}
