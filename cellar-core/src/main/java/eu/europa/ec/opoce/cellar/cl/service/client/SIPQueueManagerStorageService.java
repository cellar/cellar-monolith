/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.service.client
 *        FILE : SIPQueueManagerStorageService.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 10-12-2021
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2021 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.client;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.PackageHasParentPackage;

/**
 * Interface providing access to the functionality
 * of the SIP Queue Manager Storage Service, whose purpose
 * is to maintain the appropriate structure of the DB queue
 * when new entries are added or existing entries are removed.
 * @author EUROPEAN DYNAMICS S.A.
 */
public interface SIPQueueManagerStorageService {

	/**
	 * Updates the contents of the DB queue when new entries are inserted.
	 * @param queueEntriesToInsert the entries to insert.
	 * @param queueEntriesToUpdate the entries to update.
	 */
	void updateQueueOnInsert(Set<PackageHasParentPackage> queueEntriesToInsert, Map<Long, Long> queueEntriesToUpdate);
	
	/**
	 * Gets the entries from the DB queue whose SIP_ID or PARENT_SIP_ID values
	 * match one of those provided.
	 * @param sipIds the SIP IDs to look for.
	 * @return the DB queue entries matching one of those provided.
	 */
	List<PackageHasParentPackage> findBySipIdOrParentSipIdIn(Collection<Long> sipIds);
	
	/**
	 * Updtes the contents of the DB queue when entries are removed.
	 * @param queueEntriesToRemove the entries to remove.
	 * @param queueEntriesToUpdate the entries to update.
	 */
	void updateQueueOnRemoval(Set<PackageHasParentPackage> queueEntriesToRemove, Set<PackageHasParentPackage> queueEntriesToUpdate);
	
}
