package eu.europa.ec.opoce.cellar.support;

import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

/**
 * Utilities to get the extension for predefined file types.
 * 
 * @author phvdveld
 * 
 */
public abstract class FileExtensionUtils {

    public static final String FOXML = ".foxml.xml";
    public static final String METS = ".mets.xml";
    public static final String RESPONSE_METS = METS;
    public static final String SIP = ".zip";
    public static final String VELOCITY = ".vm";
    public static final String ZIP = ".zip";
    public static final String XML = ".xml";
    public static final String RDF = ".rdf";

    
    public static enum FileExtensionType {

        Foxml(FOXML), Mets(METS), ResponseMets(RESPONSE_METS), Sip(SIP), Velocity(VELOCITY), Zip(ZIP), Xml(XML), Rdf(RDF);

        private String suffix;

        FileExtensionType(String suffix) {
            this.suffix = suffix;
        }

        public static FileExtensionType fromString(String suffix) {
            for (FileExtensionType type : FileExtensionType.values()) {
                if (type.suffix.equals(suffix)) {
                    return type;
                }
            }
            // TODO : PHVDV - add code to exception
            throw ExceptionBuilder.get(CellarException.class).withMessage("Cannot find {} with value [{}].")
                    .withMessageArgs(FileExtensionType.class.getSimpleName(), suffix).build();
        }

        public String getSuffix() {
            return suffix;
        }
    }
}
