/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.dao.impl
 *             FILE : CleanerTripleDaoImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Sep 27, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao.impl;

import eu.europa.ec.opoce.cellar.cl.dao.CleanerDifferenceDao;
import eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner.CleanerDifference;
import eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner.CleanerDifferencePaginatedList;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.springframework.orm.hibernate5.HibernateCallback;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Sep 27, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Repository
public class CleanerDifferenceDaoImpl extends BaseDaoImpl<CleanerDifference, Long> implements CleanerDifferenceDao {

    /**
     * {@inheritDoc}
     */
    @Override
    public Long getCleanerDifferenceCount() {
        return getHibernateTemplate().execute(session -> {
            final Query queryObject = session.getNamedQuery("findCountCleanerDifferences");
            return (Long) queryObject.uniqueResult();
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void findCleanerDifferences(final CleanerDifferencePaginatedList cdpl) {
        getHibernateTemplate().execute((HibernateCallback<Long>) session -> {
            final String count = "select count(c.id) from CleanerDifference c";

            final String where = getWhereClause(cdpl);

            Query queryCount = session.createQuery(count + where);

            cdpl.setFullListSize(Integer.valueOf(queryCount.uniqueResult().toString()));

            final String select = "select c from CleanerDifference c";

            Query queryObject = session
                    .createQuery(select + where + " order by c." + cdpl.getSortCriterion() + " " + cdpl.getSortDirectionCode());

            queryObject.setFirstResult(cdpl.getObjectsPerPage() * (cdpl.getPageNumber() - 1));
            queryObject.setMaxResults(cdpl.getObjectsPerPage());

            cdpl.setList((List<CleanerDifference>) queryObject.list());

            return null;
        });
    }

    private String getWhereClause(final CleanerDifferencePaginatedList cdpl) {

        final StringBuilder where = new StringBuilder(" where ");

        boolean whereExists = false;

        if (StringUtils.isNotBlank(cdpl.getFilterValue())) {

            where.append("c.cellarId = '");
            where.append(cdpl.getFilterValue());
            where.append("'");

            whereExists = true;
        }

        if (cdpl.getCmrTable() != null) {

            if (whereExists) {
                where.append(" and ");
            }

            where.append("c.cmrTable = ");
            where.append(cdpl.getCmrTable().ordinal());

            whereExists = true;
        }

        if (cdpl.getQueryType() != null) {

            if (whereExists) {
                where.append(" and ");
            }

            where.append("c.queryType = ");
            where.append(cdpl.getQueryType().ordinal());

            whereExists = true;
        }

        if (cdpl.getOperationType() != null) {

            if (whereExists) {
                where.append(" and ");
            }

            where.append("c.operationType = ");
            where.append(cdpl.getOperationType().ordinal());

            whereExists = true;
        }

        if (whereExists) {
            return where.toString();
        } else {
            return "";
        }
    }
}
