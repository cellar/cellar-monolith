package eu.europa.ec.opoce.cellar.support.sip;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;

/**
 * The Class AuthenticOJSIPFolderType.
 */
public class AuthenticOJSIPFolderType extends CellarReceptionFolderType {

    /** {@inheritDoc} */
    @Override
    public String getReceptionFolderAbsolutePath(ICellarConfiguration cellarConfiguration) {
        return cellarConfiguration.getCellarFolderAuthenticOJReception();
    }

    /** {@inheritDoc} */
    @Override
    public TYPE getSipWorkType() {
        return TYPE.AUTHENTICOJ;
    }

    /** {@inheritDoc} */
    @Override
    protected boolean shouldItBeSorted() {
        return true;
    }

}
