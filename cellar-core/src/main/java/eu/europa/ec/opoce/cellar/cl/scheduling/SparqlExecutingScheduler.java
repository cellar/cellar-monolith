/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.scheduling
 *             FILE : SparqlExecutingScheduler.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Sep 24, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.scheduling;

import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.BatchJobProcessingService;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.BatchJobService;
import eu.europa.ec.opoce.cellar.logging.LogContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.SPARQL_EXECUTING;

/**
 * <class_description> Service used to auto execute the SPARQL queries of the batch job instances with the 'NEW' status in the database.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Sep 24, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@LogContext
public class SparqlExecutingScheduler implements Runnable {

    /**
     * Constant <code>log</code>.
     */
    private static final Logger LOG = LogManager.getLogger(SparqlExecutingScheduler.class);

    /**
     * The batch job service.
     */
    private final BatchJobService batchJobService;

    /**
     * The batch job processing service.
     */
    private final BatchJobProcessingService batchJobProcessingService;

    /**
     * Constructs a new SPARQL executing scheduler.
     *
     * @param batchJobService           the batch job service used by the scheduler
     * @param batchJobProcessingService the batch job processing service
     */
    public SparqlExecutingScheduler(final BatchJobService batchJobService, final BatchJobProcessingService batchJobProcessingService) {
        this.batchJobService = batchJobService;
        this.batchJobProcessingService = batchJobProcessingService;
    }

    /**
     * Run the SPARQL queries.
     */
    @Override
    @LogContext(SPARQL_EXECUTING)
    public void run() {
        final List<BatchJob> batchJobs = batchJobService.findBatchJobsByStatus(BatchJob.STATUS.NEW); // gets and updates the first job with the status at 'WAITING_PROCESSING'
        if (batchJobs == null || batchJobs.isEmpty()) { // there is no job available
            LOG.info("No batch jobs to execute");
        } else {
            batchJobProcessingService.preProcessBatchJobs(batchJobs);
            LOG.info("{} SPARQL queries submitted for execution", batchJobs.size());
            LOG.debug("SPARQL queries: {}", () -> batchJobs);
        }
    }
}
