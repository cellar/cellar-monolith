/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.dao
 *             FILE : ScheduledReindexationDao.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 12 sept. 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao;

import eu.europa.ec.opoce.cellar.cl.domain.index.ScheduledReindexation;

/**
 * The Interface ScheduledReindexationDao.
 * <class_description> The dao interface class for the SCHEDULED_REINDEX
 * <br/><br/>
 * ON : 12 sept. 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface ScheduledReindexationDao extends BaseDao<ScheduledReindexation, Long> {

}
