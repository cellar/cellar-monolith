/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.client
 *             FILE : LicenseHolderService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 5, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.client;

import java.util.Date;
import java.util.HashSet;
import java.util.List;

import eu.europa.ec.opoce.cellar.cl.domain.common.CellarPaginatedList;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionConfiguration.CONFIGURATION_TYPE;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionWorkIdentifier;

/**
 * <class_description> Service for providing, finding and updating {@link ExtractionExecution} instances.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 5, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 * 
 */
public interface LicenseHolderService {

    /**
     * Updates the {@link ExtractionExecution} instance in the database.
     * @param extractionExecution the {@link ExtractionExecution} instance to update
     */
    void updateExtractionExecution(final ExtractionExecution extractionExecution);

    /**
     * Deletes the {@link ExtractionWorkIdentifier} instances belonging to the {@link ExtractionExecution} identified.
     * @param executionId the identifier of the {@link ExtractionExecution} instance
     */
    void deleteWorkIdentifiers(final Long executionId);

    /**
     * Constructs and adds {@link ExtractionWorkIdentifier} instances to the {@link ExtractionExecution} instance.
     * @param extractionExecution the {@link ExtractionExecution} instance
     * @param workIds the work identifiers to add
     */
    void addWorkIdentifiers(final ExtractionExecution extractionExecution, final HashSet<String> workIds);

    /**
     * Finds {@link ExtractionWorkIdentifier} instances of the execution identified.
     * @param extractionExecutionId the identifier of {@link ExtractionExecution} instance
     * @param first the first of batch (inclusive)
     * @param last the last of batch (exclusive)
     * @return a list of {@link ExtractionWorkIdentifier} instances
     */
    List<ExtractionWorkIdentifier> findWorkIdentifiers(final Long extractionExecutionId, final int first, final int last);

    /**
     * Returns a list of pending executions and updates these executions to working.
     * @param batchSize the maximum list size
     * @return a list of pending executions
     */
    List<ExtractionExecution> updatePendingExtractionExecutions(final int batchSize);

    /**
     * Schedules the executions of the day.
     * @return the number of executions scheduled
     */
    int updateNewExtractionExecutions();

    /**
     * Returns a pending execution and update this execution to working.
     * @return if a pending execution exists, this execution.  Otherwise, null.
     */
    ExtractionExecution updatePendingExtractionExecution();

    /**
     * Returns the number of work identifiers corresponding to the identified execution.
     * @param executionId the execution identifier
     * @return the number of work identifiers corresponding to the identified execution
     */
    Long getWorkIdentifierCount(final Long executionId);

    /**
     * Adds a job configuration.
     * @param configurationName the configuration name
     * @param extractionTypeString the configuration type
     * @param extractionLanguage the configuration language
     * @param path the destination path for the generated archives
     * @param startDate the execution start date
     * @param sparqlQuery the SPARQL query
     */
    void addExtractionConfiguration(final String configurationName, final String extractionTypeString, final String extractionLanguage,
            final String path, final Date startDate, final String sparqlQuery);

    /**
     * Adds an ad-hoc configuration
     * @param configurationName the configuration name
     * @param extractionLanguage the configuration language
     * @param path the destination path for the generated archives
     * @param executionStartDate the execution start date
     * @param sparqlQuery the SPARQL query
     */
    void addAdhocExtractionConfiguration(final String configurationName, final String extractionLanguage, final String path,
            final Date executionStartDate, final String sparqlQuery);

    /**
     * Adds the next execution.
     * @param lastExtractionExecution the previous execution
     */
    void addNextExtractionExecution(final ExtractionExecution lastExtractionExecution);

    /**
     * Deletes the executions outdated corresponding to a type of configuration.
     * @param configurationType the configuration type
     * @param numberOfDays the number of days to compute the deadline
     * @return a list of the deleted executions
     */
    List<ExtractionExecution> deleteExtractionExecutions(final CONFIGURATION_TYPE configurationType, final int numberOfDays);

    /**
     * Deletes and cleans the identified configuration.
     * @param configurationId the configuration identifier
     * @param cleanFinalArchive a boolean to specify if the final archives must be deleted
     */
    void deleteExtractionConfiguration(final Long configurationId, final boolean cleanFinalArchive);

    /**
     * Deletes the identified configuration.
     * @param configurationId the configuration identifier
     */
    void deleteExtractionConfiguration(final Long configurationId);

    /**
     * Returns the frequencies.
     * @return the frequencies
     */
    List<CONFIGURATION_TYPE> getFrequencies();

    /**
     * Returns the deadlines corresponding to the frequencies.
     * @return the deadlines corresponding to the frequencies
     */
    List<Date> getDeadlines();

    /**
     * Returns the archive name overview.
     * @param configurationType the selected type of configuration
     * @param languageIsoCodeThreeChar the selected language
     * @return the archive name overview
     */
    String getArchiveNameOverview(final CONFIGURATION_TYPE configurationType, final String languageIsoCodeThreeChar);

    /**
     * Returns the path name overview.
     * @param configurationType the selected type of configuration
     * @param languageIsoCodeThreeChar the selected language
     * @return the path name overview
     */
    String getArchivePathOverview(final CONFIGURATION_TYPE configurationType, final String languageIsoCodeThreeChar);

    /**
     * Returns all job configurations.
     * @return all job configuration
     */
    List<Object> findJobExtractionConfigurations();

    /**
     * Returns all ad-hoc configurations.
     * @return all ad-hoc configuration
     */
    List<Object> findAdhocExtractionConfigurations();

    /**
     * Returns the identified configuration.
     * @param id the identifier of configuration
     * @return the identified configuration
     */
    ExtractionConfiguration findExtractionConfiguration(final Long id);

    /**
     * Returns a list of executions of the identified configuration.
     * @param extractionConfigurationId the identifier of configuration
     * @return a list of executions
     */
    List<ExtractionExecution> findExtractionExecutions(final Long extractionConfigurationId);

    /**
     * Returns a list of executions (with the number of documents) of the identified configuration.
     * @param extractionConfigurationId the identifier of configuration
     * @return a list of executions
     */
    List<Object> findExtractionExecutionsNumberOfDocuments(final Long extractionConfigurationId);

    /**
     * Returns the identified execution.
     * @param id the identifier of execution
     * @return the identified execution
     */
    ExtractionExecution findExtractionExecution(final Long id);

    /**
     * Returns a list of work identifiers of the identified execution.
     * @param extractionExecutionId the identified of execution
     * @return a lisf of work identifiers
     */
    List<ExtractionWorkIdentifier> findWorkIdentifiers(final Long extractionExecutionId);

    /**
     * Returns an externally sorted and paginated list of work identifiers belonging to the identified execution.
     * @param extractionExecutionId the identifier of execution
     * @param workIdentifiersList the sorted and paginated list to update
     * @return an externally sorted and paginated list of work identifiers
     */
    CellarPaginatedList<ExtractionWorkIdentifier> findWorkIdentifiers(final Long extractionExecutionId,
            final CellarPaginatedList<ExtractionWorkIdentifier> workIdentifiersList);

    /**
     * Returns a list of filtered work identifiers of the identified execution.
     * @param extractionExecutionId the identified of execution
     * @param workIdFilter the filter
     * @return a lisf of work identifiers
     */
    List<ExtractionWorkIdentifier> findMatchWorkIdWorkIdentifiers(final Long extractionExecutionId, final String workIdFilter);

    /**
     * Returns the possible actions for the executions.
     * @param extractionExecutions the {@link ReindexationJob} instances concerned
     * @return an array of the possible actions. The column [0] indicates if the step is restartable and [1] indicates if the job is startable.
     */
    boolean[][] getExtractionExecutionsOptions(final List<ExtractionExecution> extractionExecutions);

    /**
     * Returns the possible actions for the executions.
     * @param extractionExecutions the {@link ReindexationJob} instances concerned
     * @param index index of the executions
     * @return an array of the possible actions. The column [0] indicates if the step is restartable and [1] indicates if the job is startable.
     */
    boolean[][] getExtractionExecutionsOptions(final List<Object> extractionExecutions, final int index);

    /**
     * Restarts the step of the identified execution.
     * @param id the identifier of execution
     */
    void restartStepExtractionExecution(final Long id);

    /**
     * Restarts the steps.
     */
    int restartStepExtractionExecution();

    /**
     * Restarts the identified execution.
     * @param id the identifier of execution
     */
    void restartExtractionExecution(final Long id);

    /**
     * Restarts the executions.
     */
    int restartExtractionExecution();

    /**
     * Checks if the start date is valid for the selected frequency.
     * @param configurationType the selected frequency
     * @param startDate the start date
     * @return True if the start date is valid. Otherwise, false.
     */
    boolean isStartDateValid(final CONFIGURATION_TYPE configurationType, final Date startDate);

    /**
     * Cleans the unfinished executions on the identified instance. 
     * @param instanceId the identifier of instance
     * @return the number of executions restarted
     */
    int cleanExtractionExecutions(final String instanceId);

    /**
     * Returns the absolute archive file path corresponding to the execution.
     * @param extractionExecution the execution
     * @return the absolute archive file path corresponding to the execution
     */
    String getAbsoluteArchiveFilePath(final ExtractionExecution extractionExecution);

    /**
     * Returns the absolute archive temp directory path corresponding to the execution.
     * @param extractionExecution the execution
     * @return the absolute archive temp directory path corresponding to the execution
     */
    String getAbsoluteArchiveTempDirectoryPath(final ExtractionExecution extractionExecution);

    /**
     * Returns the absolute SPARQL temp file path corresponding to the execution.
     * @param extractionExecution the execution
     * @return the absolute SPARQL temp file path corresponding to the execution
     */
    String getAbsoluteSparqlTempFilePath(final ExtractionExecution extractionExecution);
}
