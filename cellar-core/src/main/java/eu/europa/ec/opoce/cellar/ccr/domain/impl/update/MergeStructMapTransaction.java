/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.ccr.domain.impl.update
 *        FILE : MergeStructMapTransaction.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 24-03-2014
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.domain.impl.update;

import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.ccr.domain.impl.TransactionConfiguration;
import eu.europa.ec.opoce.cellar.ccr.helper.DigitalObjectHelper;
import eu.europa.ec.opoce.cellar.ccr.helper.ProductionIdentifierHelper;
import eu.europa.ec.opoce.cellar.cl.dao.ProductionIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.admin.ProductionIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.cl.domain.response.UpdateStructMapOperation;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierResolver;
import eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMapTypeHelper;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.StructMapProcessorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * <class_description> This class provides methods for the merge process of a StructMap into database and S3.
 * If an error occurs during this process, the entire process is rolled back, in order that all digital objects (with metadata and content)
 * will be removed from the database and S3 if they are created, or set to the previous version if they are updated.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 24-03-2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component("structMapMerge")
public class MergeStructMapTransaction extends AbstractUpdateStructMapTransaction {

    /**
     * Logger instance.
     */
    @SuppressWarnings("unused")
    private static final Logger LOGGER = LoggerFactory.getLogger(MergeStructMapTransaction.class);

    /**
     * The identifier resolver.
     */
    @Autowired
    private IdentifierResolver identifierResolver;

    /**
     * The production identifier dao.
     */
    @Autowired
    private ProductionIdentifierDao productionIdentifierDao;

    /**
     * Instantiates a new merge struct map transaction.
     */
    public MergeStructMapTransaction() {
        // load existing direct model, and existing direct and inferred model from S3
        super(TransactionConfiguration.get(). //
                withLoadExistingDirectModel(true). //
                withLoadExistingDirectAndInferredModel(true) //
        );
    }

    /**
     * Process all digital objects of the hierarchy contained into 'calculatedData' in depth first traversal.
     *
     * @param operation                          a reference to the generated response object
     * @param calculatedData                     the calculated data where to get the hierarchy from
     * @param metsDirectory                      a reference to the directory where the SIP has been exploded.
     * @param sipResource                        needed for audit
     * @param hiddenContentStreamIdentifierList  actual removed digital object list.
     * @param appendedDOList                     actual appended digital object list
     * @param nextExpressionCellarIdSuffixNumber the next expression cellar identifier.
     */
    @Override
    protected void processHierarchy(final UpdateStructMapOperation operation, final CalculatedData calculatedData, final File metsDirectory,
                                    final SIPResource sipResource, final List<CellarIdentifier> hiddenContentStreamIdentifierList,
                                    final List<DigitalObject> appendedDOList, final AtomicInteger nextExpressionCellarIdSuffixNumber) {

        internalProcessHierarchy(operation, calculatedData.getStructMap().getDigitalObject(), calculatedData, metsDirectory,
                appendedDOList);
    }

    /**
     * Internal process hierarchy.
     *
     * @param operation         the operation
     * @param currDigitalObject the curr digital object
     * @param calculatedData    the calculated data
     * @param metsDirectory     the mets directory
     * @param appendedDOList    the appended do list
     */
    private void internalProcessHierarchy(final UpdateStructMapOperation operation, final DigitalObject currDigitalObject,
                                          final CalculatedData calculatedData, final File metsDirectory, final List<DigitalObject> appendedDOList) {
        final StructMap structMap = calculatedData.getStructMap();
        final ProductionIdentifier pid = ProductionIdentifierHelper.getProductionIdentifier(currDigitalObject,
                this.productionIdentifierDao);
        final DigitalObjectType digitalObjectType = currDigitalObject.getType();

        switch (digitalObjectType) {
            case WORK:
            case EXPRESSION:
            case DOSSIER:
            case EVENT:
            case AGENT:
            case TOPLEVELEVENT: {
                this.checkAssignAndSetPidToResponse(pid, operation, currDigitalObject);

                final ContentIdentifier cellarId = currDigitalObject.getCellarId();
                final String identifier = cellarId.getIdentifier();
                final List<ContentIdentifier> contentids = currDigitalObject.getContentids();
                checkReadOnlyOnItself(currDigitalObject, identifier, contentids);
                break;
            }
            case MANIFESTATION: {

                if (appendedDOList.contains(currDigitalObject)) {
                    break;
                }
                ProductionIdentifierHelper.checkProductionIdentifierExists(pid, currDigitalObject);
                checkDate(pid, currDigitalObject);
                this.assignAndSetPidToResponse(pid, operation, currDigitalObject);

                final ContentIdentifier cellarId = currDigitalObject.getCellarId();
                final String identifier = cellarId.getIdentifier();
                final List<ContentIdentifier> contentids = currDigitalObject.getContentids();

                checkReadOnlyOnSubTree(currDigitalObject, identifier, contentids);

                // only update content if the content node is set
                if (StructMapTypeHelper.isNodeType(structMap.getContentOperationType())) {
                    this.updateContent(operation, pid.getCellarIdentifier(), currDigitalObject, metsDirectory);
                } else {
                    DigitalObjectHelper.setCellarIdOnAllContent(currDigitalObject, this.identifierResolver);
                }
                break;
            }
            default:
                throw ExceptionBuilder.get(StructMapProcessorException.class).withCode(CoreErrors.E_036)
                        .withMessage("Type [" + digitalObjectType + "]").build();
        }

        for (final DigitalObject childDigitalObject : currDigitalObject.getChildObjects()) {
            internalProcessHierarchy(operation, childDigitalObject, calculatedData, metsDirectory, appendedDOList);
        }
    }

}
