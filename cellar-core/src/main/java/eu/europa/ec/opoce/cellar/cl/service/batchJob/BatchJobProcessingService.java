/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.client
 *             FILE : BatchJobProcessingService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 12, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.batchJob;

import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;

import java.util.List;

/**
 * <class_description> The batch job processing service.
 *
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 12, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface BatchJobProcessingService {

    /**
     * Cleans the batch jobs running (not completed).
     */
    public void cleanBatchJobsRunning();

    /**
     * Executes the SPARQL of the specified batch job.
     * @param batchJob the batch job to execute
     */
    void executeSparql(final BatchJob batchJob);

    /**
     * Processes the batch jobs based on the SPARQL result.
     * @param batchJob the batch job to process
     */
    void processBatchJobs(final List<BatchJob> batchJobs);

    /**
     * Pre process batch jobs.
     *
     * @param batchJobs the batch jobs
     */
    void preProcessBatchJobs(final List<BatchJob> batchJobs);

}
