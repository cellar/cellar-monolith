/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.rdfStoreCleaner.impl
 *             FILE : DigitalObjectLoaderServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 1, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.rdfStoreCleaner.impl;

import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;
import eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner.CleanerDigitalObjectData;
import eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner.CleanerHierarchyData;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cl.service.rdfStoreCleaner.DigitalObjectFilterService;
import eu.europa.ec.opoce.cellar.cl.service.rdfStoreCleaner.DigitalObjectLoaderService;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.database.dao.InverseRelationDao;
import eu.europa.ec.opoce.cellar.common.util.Pair;
import eu.europa.ec.opoce.cellar.database.CmrTableName;
import eu.europa.ec.opoce.cellar.database.IRDFStoreRelationalGateway;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.RDFStoreCleanerException;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Selector;
import org.apache.jena.rdf.model.SimpleSelector;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.Lang;
import org.apache.jena.vocabulary.OWL;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 1, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class DigitalObjectLoaderServiceImpl implements DigitalObjectLoaderService {

    protected static final Logger LOG = LogManager.getLogger(DigitalObjectLoaderServiceImpl.class);

    @Autowired
    private IRDFStoreRelationalGateway clusterTripleDbGateway;

    @Autowired
    private InverseRelationDao inverseRelationDao;

    @Autowired
    @Qualifier("pidManagerService")
    private IdentifierService identifierService;

    @Autowired
    private DigitalObjectFilterService digitalObjectFilterService;

    @Autowired
    private ContentStreamService contentStreamService;

    @Override
    public void loadHierarchyUris(final String rootCellarId, final CleanerHierarchyData cleanerHierarchyData) {
        final List<Identifier> identifiers = this.identifierService.getTreeIdentifiers(rootCellarId, false);

        for (final Identifier identifier : identifiers) {
            cleanerHierarchyData.addMetadataUri(this.identifierService.getUri(identifier.getCellarId()));

            if (identifier.getPids() != null) {
                for (final String pid : identifier.getPids()) {
                    cleanerHierarchyData.addMetadataUri(this.identifierService.getUri(pid));
                }
            }
        }
    }

    @Override
    public void loadDigitalObject(final CleanerDigitalObjectData cleanerDigitalObjectData) {
        loadMetadataFromS3(cleanerDigitalObjectData);
        loadMetadataFromOracle(cleanerDigitalObjectData);
        loadSameAsUris(cleanerDigitalObjectData);
        loadInverseRelations(cleanerDigitalObjectData);
    }

    private void loadSameAsUris(final CleanerDigitalObjectData cleanerDigitalObjectData) {
        long startTime = System.currentTimeMillis();
        LOG.info("Loading sameAses...");

        final Map<ContentType, Model> metadataFromS3 = cleanerDigitalObjectData.getMetadataFromS3();
        final Model sameAsFromS3 = metadataFromS3.get(ContentType.SAME_AS);

        final Selector selectorSameAs = new SimpleSelector(null, OWL.sameAs, (String) null);
        final StmtIterator sameAsIterator = sameAsFromS3.listStatements(selectorSameAs);

        final Set<String> sameAsUris = new HashSet<>();
        cleanerDigitalObjectData.setSameAsUris(sameAsUris);

        Statement statement;
        try {
            while (sameAsIterator.hasNext()) {
                statement = sameAsIterator.next();
                sameAsUris.add(statement.getObject().asResource().getURI());
                sameAsUris.add(statement.getSubject().getURI());
            }
        } finally {
            sameAsIterator.close();
        }

        LOG.info("Loading sameAses done in {} ms.", String.valueOf(System.currentTimeMillis() - startTime));
    }

    private void loadMetadataFromS3(final CleanerDigitalObjectData data) {
        long startTime = System.currentTimeMillis();
        LOG.info("Loading metadata snippets from S3...");

        Map<ContentType, String> versions = data.getVersions();
        for (ContentType contentType : ContentType.values()) { // workaround for legacy data
            if (!versions.containsKey(contentType)) {
                versions.put(contentType, null);
            }
        }
        Map<ContentType, String> models = contentStreamService.getContentsAsString(data.getCellarId(), versions);


        final Map<ContentType, Model> metadataFromS3 = new HashMap<>();
        data.setMetadataFromS3(metadataFromS3);

        for (ContentType contentType : ContentType.values()) {
            String content = models.get(contentType);
            if (StringUtils.isNotBlank(content)) {
                metadataFromS3.put(contentType, JenaUtils.read(content, Lang.NT));
            }
        }

        LOG.info("Loading metadata snippets from S3 done in {} ms.", String.valueOf(System.currentTimeMillis() - startTime));
    }

    private void loadMetadataFromOracle(final CleanerDigitalObjectData cleanerDigitalObjectData) {
        long startTime = System.currentTimeMillis();
        LOG.info("Loading metadata snippets from Oracle RDF Store...");

        final Pair<Model, Boolean> metadataModelAndEmbargo = this.loadFromOracleByTableName(cleanerDigitalObjectData.getCellarId(),
                CmrTableName.CMR_METADATA);
        this.digitalObjectFilterService.filter(metadataModelAndEmbargo.getOne());
        cleanerDigitalObjectData.setMetadataFromOracle(metadataModelAndEmbargo.getOne());

        if (metadataModelAndEmbargo.getTwo() != null) {
            cleanerDigitalObjectData.setUnderEmbargo(metadataModelAndEmbargo.getTwo());
        }

        final Pair<Model, Boolean> inverseModelAndEmbargo = this.loadFromOracleByTableName(cleanerDigitalObjectData.getCellarId(),
                CmrTableName.CMR_INVERSE);
        this.digitalObjectFilterService.filter(inverseModelAndEmbargo.getOne());
        cleanerDigitalObjectData.setInverseFromOracle(inverseModelAndEmbargo.getOne());

        if (inverseModelAndEmbargo.getTwo() != null) {
            cleanerDigitalObjectData.setUnderEmbargo(inverseModelAndEmbargo.getTwo());
        }

        LOG.info("Loading metadata snippets from Oracle RDF Store done in {} ms.", String.valueOf(System.currentTimeMillis() - startTime));
    }

    private Pair<Model, Boolean> loadFromOracleByTableName(final String clusterCellarId, final CmrTableName tableName) {
        Model cluster = null;
        Boolean underEmbargo = null;
        if (this.clusterTripleDbGateway.isContextUsedInTable(tableName.getNormalTable(), clusterCellarId)) {
            cluster = this.clusterTripleDbGateway.selectModel(tableName.getNormalTable(), clusterCellarId);
            underEmbargo = false;
        }

        if (this.clusterTripleDbGateway.isContextUsedInTable(tableName.getEmbargoTable(true), clusterCellarId)) {
            if (underEmbargo != null) {
                throw ExceptionBuilder.get(RDFStoreCleanerException.class).withCode(CoreErrors.E_500)
                        .withMessage("The cluster identified by {} has simultaneously triples under embargo and not under embargo.")
                        .withMessageArgs(clusterCellarId).build();
            }

            cluster = this.clusterTripleDbGateway.selectModel(tableName.getEmbargoTable(true), clusterCellarId);
            underEmbargo = true;
        }

        return new Pair<>(cluster, underEmbargo);
    }

    private void loadInverseRelations(final CleanerDigitalObjectData cleanerDigitalObjectData) {
        long startTime = System.currentTimeMillis();
        LOG.info("Loading inverse relations from Oracle relational...");

        cleanerDigitalObjectData.setInverseRelations(this.inverseRelationDao.findRelationsByTarget(cleanerDigitalObjectData.getCellarId()));

        LOG.info("Loading inverse relations from Oracle relational done in {} ms.", String.valueOf(System.currentTimeMillis() - startTime));
    }
}
