/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ccr.service.impl
 *             FILE : IStructMapProcessorService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 25-06-2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.service.client;

import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.cl.domain.response.StructMapResponse;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsDocument;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsPackage;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;

/**
 * <class_description> Definition of the service to process treatment on a {@link StructMap}.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 25-06-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface IStructMapProcessorService {

    /**
     * Process the mets document (create/append/update/read) according to the given transaction.
     * 
     * @param metsPackage the mets package
     * @param metsDocument the mets document to process
     * @param sipResource the sip resource
     * @return the StructMap response
     */
    public StructMapResponse processMetsDocument(final MetsPackage metsPackage, final MetsDocument metsDocument,
            final SIPResource sipResource);

}
