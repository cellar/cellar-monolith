package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.service.client.BackupService;
import eu.europa.ec.opoce.cellar.cl.service.client.FileService;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.SIPException;
import eu.europa.ec.opoce.cellar.monitoring.Watch;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

/**
 * Backup SIP.
 *
 * @author dcraeye
 */
@Service
public class BackupServiceImpl implements BackupService {

    /** Logger instance. */
    private static final Logger LOG = LoggerFactory.getLogger(BackupServiceImpl.class);

    /** The cellar configuration. */
    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    @Autowired
    private  FileService fileService;

    @Override
    public void backupFoxmlFolder(final String foxmlFolder) throws SIPException {
        try {
            if (this.cellarConfiguration.isCellarServiceBackupProcessedFileEnabled()) {
                this.fileService.moveFoxmlToBackupFolder(foxmlFolder);
            } else {
                if (this.cellarConfiguration.isCellarServiceDeleteProcessedDataEnabled()) {
                    this.fileService.deleteFoxmlFolder(foxmlFolder);
                }
            }
        } catch (final IOException e) {
            LOG.error(e.getMessage());
        }
    }

    @Watch(value = "Backup Mets", arguments = {
            @Watch.WatchArgument(name = "Mets folder", expression = "metsFolder.name")
    })
    @Override
    public void backupMetsFolder(final File metsFolder) throws SIPException {
        if (metsFolder.exists()) {
            this.backupMetsFolder(metsFolder.getName());
        }
    }

    @Override
    public void backupMetsFolder(final String metsFolderName) throws SIPException {
        if (StringUtils.isNotEmpty(metsFolderName)
                && new File(this.cellarConfiguration.getCellarFolderTemporaryWork(), metsFolderName).exists()) {
            try {
                if (this.cellarConfiguration.isCellarServiceBackupProcessedFileEnabled()) {
                    this.fileService.moveMetsToBackupFolder(metsFolderName);
                } else {
                    if (this.cellarConfiguration.isCellarServiceDeleteProcessedDataEnabled()) {
                        this.fileService.deleteFromTemporaryWorkFolder(metsFolderName);
                    }
                }
            } catch (final IOException e) {
                LOG.error(e.getMessage());
            }
        }

    }

    @Watch(value = "Backup SIP", arguments = {
            @Watch.WatchArgument(name = "Sip file", expression = "sipFile.name")
    })
    @Override
    public boolean backupSIPFile(final File sipFile) throws SIPException {
        if ((sipFile != null) && sipFile.exists()) {
            try {
                if (this.cellarConfiguration.isCellarServiceBackupProcessedFileEnabled()) {
                    this.fileService.moveSIPToBackupFolder(sipFile);
                } else {
                    if (this.cellarConfiguration.isCellarServiceDeleteProcessedDataEnabled()) {
                        try {
                            Files.delete(sipFile.toPath());
                        } catch (IOException e) {
                            LOG.warn("Failed to delete SIPFile", e);
                            return false;
                        }
                    }
                }

            } catch (final IOException e) {
                LOG.error(e.getMessage());
                throw ExceptionBuilder.get(SIPException.class).withCode(CoreErrors.E_007)
                        .withMessage("[" + sipFile.getAbsolutePath() + "] -> " + this.cellarConfiguration.getCellarFolderBackup()).build();
            }
        }
        return true;
    }

    @Override
    public void errorMetsFolder(final File metsFolder) throws SIPException {
        if (metsFolder.exists()) {
            this.errorMetsFolder(metsFolder.getName());
        }
    }

    @Override
    public void errorMetsFolder(final String metsFolderName) throws SIPException {
        final String cellarFolderTemporaryWork = this.cellarConfiguration.getCellarFolderTemporaryWork();
        final File metsFolder = new File(cellarFolderTemporaryWork, metsFolderName);
        if (StringUtils.isNotEmpty(metsFolderName) && metsFolder.exists()) {
            try {
                this.fileService.moveMetsToErrorFolder(metsFolderName);
            } catch (final IOException e) {
                LOG.error(e.getMessage());
            }
        }
    }

    /** {@inheritDoc} */
    @Override
    public void errorSIPFile(final File sipFileName) throws SIPException {
        this.errorSIPFile(sipFileName, true);
    }

    /** {@inheritDoc} */
    @Override
    public void errorSIPFile(final File sipFileName, final boolean deleteSource) throws SIPException {
        if ((sipFileName != null) && sipFileName.exists()) {
            try {
                if (deleteSource) {
                    this.fileService.moveSIPToErrorFolder(sipFileName);
                } else {
                    this.fileService.copySIPToErrorFolder(sipFileName);
                }
            } catch (final IOException e) {
                LOG.error(e.getMessage());
                throw ExceptionBuilder.get(SIPException.class).withCode(CoreErrors.E_006)
                        .withMessage("[" + sipFileName.getAbsolutePath() + "] -> " + this.cellarConfiguration.getCellarFolderError())
                        .build();
            }
        }
    }
}
