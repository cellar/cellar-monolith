/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.dao.impl
 *        FILE : PackageListDaoImpl.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 03-12-2021
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2021 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import eu.europa.ec.opoce.cellar.cl.dao.PackageListDao;
import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.PackageList;

/**
 * Provides access to the database view containing the
 * references of the packages (their IDs) that are contained
 * in the database queue.
 * @author EUROPEAN DYNAMICS S.A.
 */
@Repository
public class PackageListDaoImpl extends BaseDaoImpl<PackageList, Long> implements PackageListDao {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<PackageList> getPackageListOrdered() {
		return super.findObjectsByNamedQuery("PackageList.findAllOrdered");
	}
	
}
