/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.aligner
 *             FILE : LevelObject.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : May 20, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.aligner;

import eu.europa.ec.opoce.cellar.cl.domain.admin.ProductionIdentifier;
import eu.europa.ec.opoce.cellar.cl.enums.aligner.Repository;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.DataStreamWrapper;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.HierarchyElement;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import org.apache.jena.rdf.model.Model;

import java.util.*;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : May 20, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@SuppressWarnings("serial")
public class HierarchyNode extends HierarchyElement<HierarchyNode, HierarchyNode> {

    /** The Constant WEM_REQUIRED_REPOSITORIES. */
    private static final Repository[] WEM_REQUIRED_REPOSITORIES = {
            Repository.CMR_CELLAR_RESOURCE_MD, //
            Repository.CELLAR_IDENTIFIER, //
            Repository.S3, //
            Repository.RDF_STORE, //
            Repository.CMR_INVERSE_DISS};

    /** The Constant WEM_EMBARGOED_REQUIRED_REPOSITORIES. */
    private static final Repository[] WEM_EMBARGOED_REQUIRED_REPOSITORIES = {
            Repository.CMR_CELLAR_RESOURCE_MD, //
            Repository.CELLAR_IDENTIFIER, //
            Repository.S3, //
            Repository.RDF_STORE};

    /** The Constant I_REQUIRED_REPOSITORIES. */
    private static final Repository[] I_REQUIRED_REPOSITORIES = {
            Repository.CMR_CELLAR_RESOURCE_MD, //
            Repository.CELLAR_IDENTIFIER, //
            Repository.S3};

    /** The data stream wrapper. */
    private DataStreamWrapper dataStreamWrapper;

    /** The metadata models. */
    private Map<ContentType, Model> metadataModels;

    /** The repositories. */
    private final Set<Repository> repositories;

    /** The production identifiers. */
    private Collection<ProductionIdentifier> productionIdentifiers;

    /**
     * Instantiates a new hierarchy node.
     *
     * @param cellarId the cellar id
     * @param type the type
     */
    public HierarchyNode(final String cellarId, final DigitalObjectType type) {
        super(type == null ? DigitalObjectType.UNDEFINED : type);
        this.setCellarId(cellarId);
        this.repositories = new HashSet<>();
    }

    /**
     * Checks if is evicted.
     *
     * @return true, if is evicted
     */
    public boolean isEvicted() {
        return ((this.getParent() != null) && this.getParent().isEvicted()) || !this.isAligned();
    }

    /**
     * Checks if is aligned.
     *
     * @return true, if is aligned
     */
    public boolean isAligned() {
        if (this.getType() == DigitalObjectType.ITEM) {
            return this.repositories.size() == I_REQUIRED_REPOSITORIES.length;
        } else {
            if (this.isUnderEmbargo()) {
                return this.repositories.size() == WEM_EMBARGOED_REQUIRED_REPOSITORIES.length;
            } else {
                return this.repositories.size() == WEM_REQUIRED_REPOSITORIES.length;
            }
        }
    }

    /**
     * Adds the evicted hierarchy nodes.
     *
     * @param evictedHierarchyNodes the evicted hierarchy nodes
     */
    public void addEvictedHierarchyNodes(final Map<Repository, List<HierarchyNode>> evictedHierarchyNodes) {
        if (this.isEvicted()) {
            List<HierarchyNode> hierarchyNodes;
            for (final Repository r : this.repositories) {
                hierarchyNodes = evictedHierarchyNodes.computeIfAbsent(r, k -> new LinkedList<>());
                hierarchyNodes.add(this);
            }
        }

        for (final HierarchyNode n : this.getChildren()) {
            n.addEvictedHierarchyNodes(evictedHierarchyNodes);
        }
    }

    /**
     * Gets the missing repositories.
     *
     * @return the missing repositories
     */
    public List<Repository> getMissingRepositories() {
        if (this.getType() == DigitalObjectType.ITEM) {
            return this.getMissingRepositories(I_REQUIRED_REPOSITORIES);
        } else {
            return this.getMissingRepositories(WEM_REQUIRED_REPOSITORIES);
        }
    }

    /**
     * Gets the missing repositories.
     *
     * @param requiredRepositories the required repositories
     * @return the missing repositories
     */
    private List<Repository> getMissingRepositories(final Repository[] requiredRepositories) {
        final List<Repository> missing = new LinkedList<>();
        for (final Repository r : requiredRepositories) {
            if (!this.repositories.contains(r)) {
                missing.add(r);
            }
        }

        return missing;
    }

    /**
     * Gets the repositories.
     *
     * @return the repositories
     */
    public Set<Repository> getRepositories() {
        return this.repositories;
    }

    /**
     * Adds the repository.
     *
     * @param repository the repository
     */
    public void addRepository(final Repository repository) {
        this.repositories.add(repository);
    }

    /**
     * Gets the data stream wrapper.
     *
     * @return the dataStreamWrapper
     */
    public DataStreamWrapper getDataStreamWrapper() {
        return this.dataStreamWrapper;
    }

    /**
     * Sets the data stream wrapper.
     *
     * @param dataStreamWrapper the dataStreamWrapper to set
     */
    public void setDataStreamWrapper(final DataStreamWrapper dataStreamWrapper) {
        this.dataStreamWrapper = dataStreamWrapper;
    }

    /**
     * Gets the metadata models.
     *
     * @return the metadataModels
     */
    public Map<ContentType, Model> getMetadataModels() {
        return this.metadataModels;
    }

    /**
     * Sets the metadata models.
     *
     * @param metadataModels the metadataModels to set
     */
    public void setMetadataModels(final Map<ContentType, Model> metadataModels) {
        this.metadataModels = metadataModels;
    }

    /**
     * Get the production identifiers of this element.
     * @return the production identifiers of this element.
     */
    public Collection<ProductionIdentifier> getProductionIdentifiers() {
        return this.productionIdentifiers;
    }

    /**
     * Set the production identifiers of this element.
     *
     * @param productionIdentifiers the new production identifiers
     */
    public void setProductionIdentifiers(final Collection<ProductionIdentifier> productionIdentifiers) {
        this.productionIdentifiers = productionIdentifiers;
    }

    /**
     * Check if the probable type is {@link DigitalObjectType} when undefined. When the type is defined, check it directly.
     *
     * @param type the type
     * @return the probable type.
     */
    public boolean hasProbableType(final DigitalObjectType type) {
        boolean hasProbableType = this.getType() == type;

        if (this.getType() == DigitalObjectType.UNDEFINED) {
            hasProbableType = type.getPattern().matcher(this.getCellarId().getIdentifier()).find();
        }

        return hasProbableType;
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        return String.format("[%s => %s]", this.getCellarId(), this.repositories);
    }
}
