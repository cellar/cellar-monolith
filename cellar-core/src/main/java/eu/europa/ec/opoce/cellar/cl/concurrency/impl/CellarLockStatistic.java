/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.concurrency.impl
 *             FILE : CellarLockStatistic.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : May 3, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.concurrency.impl;

import eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLockSession;
import eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLockStatistic;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * <class_description> Cellar lock statistics implementation.
 * <br/><br/>
 * ON : May 3, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class CellarLockStatistic implements ICellarLockStatistic {

    /**
     * The resource identifier (cellar id or production id).
     */
    private final String resourceIdentifier;

    /**
     * The number of locks.
     */
    private final int lockCount;

    /**
     * The lock object reference.
     */
    private final String lockObjectRef;

    /**
     * The CELLAR lock sessions.
     */
    private final Set<ICellarLockSession> cellarLockSessions;

    /**
     * Constructor.
     * @param resourceIdentifier the resource identifier (cellar id or production id)
     * @param cellarLock the CELLAR lock object
     */
    @JsonCreator
    public CellarLockStatistic(@JsonProperty("resourceIdentifier") final String resourceIdentifier,
            @JsonProperty("cellarLock") final CellarLock cellarLock) {
        this.resourceIdentifier = resourceIdentifier;
        this.lockCount = cellarLock != null ? cellarLock.getLockCount() : 0;
        this.lockObjectRef = Integer.toHexString(System.identityHashCode(cellarLock));
        this.cellarLockSessions = cellarLock != null ? cellarLock.getCellarLockSessions() : new HashSet<ICellarLockSession>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getResourceIdentifier() {
        return this.resourceIdentifier;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getLockCount() {
        return this.lockCount;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLockObjectRef() {
        return this.lockObjectRef;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<ICellarLockSession> getCellarLockSessions() {
        return this.cellarLockSessions;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder("{");
        builder.append("resourceIdentifier=" + this.getResourceIdentifier());
        builder.append(", lockCount=" + this.getLockCount());
        builder.append(", lockObjectRef=" + this.getLockObjectRef());
        builder.append(", cellarLockSessions=" + this.getCellarLockSessions());
        builder.append("}");
        return builder.toString();
    }

}
