package eu.europa.ec.opoce.cellar.ccr.validator;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.Assert;

/**
 * Abstract <i>validator</i> that provides common services to <i>validators</i> classes.
 * 
 * @author lderavet
 */
public abstract class AbstractPropertyValidator implements PropertyValidator {

    /** the list of authorized values an parameter's value will be compared to */
    private List<String> authorizedValues = new ArrayList<String>();

    /**
     * Only accept <code>String</code> values.
     * 
     * @param the <code>class</code> type a <i>validator</i> supports.
     */
    public boolean supports(Class<?> clazz) {
        return String.class.equals(clazz);
    }

    /**
     * Set the list of authorized values.
     * 
     * @param the list of authorized values.
     */
    public void setAuthorizedValues(List<String> values) {
        Assert.notNull(values, "[E-100] The list of mimetypes can't be 'null' !");
        Assert.notEmpty(values, "[E-100] The list of mimetypes can't be empty !");

        authorizedValues.addAll(values);
    }

    /**
     * Returns the list of authorized values.
     * 
     * @return the list of authorized values.
     */
    protected List<String> getAuthorizedValues() {
        return authorizedValues;
    }
}
