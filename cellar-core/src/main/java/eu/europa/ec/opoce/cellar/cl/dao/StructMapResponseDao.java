package eu.europa.ec.opoce.cellar.cl.dao;

import eu.europa.ec.opoce.cellar.cl.domain.response.StructMapResponse;

import java.io.File;

/**
 * @author dcraeye
 */
public interface StructMapResponseDao {

    /**
     * Persist the given StructMapResponse
     * 
     * @param structMapResponse
     *            the structMapResponse object to save
     * @return the file containing the structMapResponse
     */
    File save(StructMapResponse structMapResponse);

    /**
     * Returns the response file generated for this metsDocumentId.
     *
     * @param metsDocumentId the document id of the response
     */
    File getResponseFile(String metsDocumentId);
}
