/**
 * 
 */
package eu.europa.ec.opoce.cellar.support;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * @author dsavares
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class SparqlResult {

    @XmlElement(namespace = SparqlUtils.NAME_SPACE)
    private SparqlDocument document;

    @XmlElement(namespace = SparqlUtils.NAME_SPACE)
    private String type;

    public void setDocument(SparqlDocument document) {

        this.document = document;
    }

    public SparqlDocument getDocument() {
        if (document == null) {
            document = new SparqlDocument();
        }
        return document;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return "Result [document=" + document + ", type=" + type + "]";
    }

}
