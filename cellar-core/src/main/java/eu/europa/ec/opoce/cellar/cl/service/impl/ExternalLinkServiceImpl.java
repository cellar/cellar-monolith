/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *             FILE : ExternalLinkServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 14, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import bsh.EvalError;
import bsh.Interpreter;
import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.cl.domain.externalLink.ExternalLink;
import eu.europa.ec.opoce.cellar.cl.domain.externalLink.ExternalLinkPattern;
import eu.europa.ec.opoce.cellar.cl.service.client.ExternalLinkPatternCacheService;
import eu.europa.ec.opoce.cellar.cl.service.client.ExternalLinkService;
import eu.europa.ec.opoce.cellar.common.Placeholder;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.ExternalLinkException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;

/**
 * <class_description> External link generation manager implementation.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 14, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class ExternalLinkServiceImpl implements ExternalLinkService {

    private static final Logger LOG = LogManager.getLogger(ExternalLinkServiceImpl.class);

    /**
     * Document id data variable name.
     */
    private final static String ID_DATA_VAR_NAME = "idData";

    /**
     * Language variable name.
     */
    private final static String LANG_VAR_NAME = "lang";

    private final static int PLACEHOLDER_TAG_SIZE = 2; // ${ or }$

    /**
     * External link pattern cache manager.
     */
    @Autowired
    private ExternalLinkPatternCacheService externalLinkPatternService;

    /**
     * {@inheritDoc}
     */
    @Override
    public ExternalLink convert(final String id, final String langIso2Char) throws ExternalLinkException {
        List<ExternalLinkPattern> externalLinkPatterns = this.externalLinkPatternService.getExternalLinkList();

        // find a pattern
        ExternalLinkPattern externalLinkMatched = null;
        Matcher matcher = null;
        for (ExternalLinkPattern elp : externalLinkPatterns) {
            matcher = elp.getMatchPatternObject().matcher(id);
            if (matcher.find()) {
                externalLinkMatched = elp;
                break;
            }
        }

        if (externalLinkMatched == null) {
            LOG.error("Document id '{}' is not found.", id);
            return null; // pattern not found
        }

        return this.constructExternalLink(externalLinkMatched, matcher, langIso2Char);
    }

    /**
     * Constructs the external link.
     * @param externalLinkMatched the matched external link pattern
     * @param matcher the matcher
     * @param langIso2Char the language
     * @return the external link generated
     * @throws ExternalLinkException if there is an error in the execution of commands
     */
    private ExternalLink constructExternalLink(final ExternalLinkPattern externalLinkMatched, final Matcher matcher,
            final String langIso2Char) throws ExternalLinkException {

        final LinkedList<Placeholder> placeholders = this.externalLinkPatternService.getExternalLinkPlaceholders(externalLinkMatched);
        final ArrayList<String> placeholdersInterpreted = new ArrayList<String>(placeholders.size());
        final Interpreter interpreter = new Interpreter();

        // init variables
        try {
            interpreter.set(ID_DATA_VAR_NAME, this.getIdData(matcher));
            interpreter.set(LANG_VAR_NAME, langIso2Char);
        } catch (EvalError e) {
                LOG.error(String.format("Cannot init the variables: %s; %s", ID_DATA_VAR_NAME, LANG_VAR_NAME), e);

            throw ExceptionBuilder.get(ExternalLinkException.class).withCode(CommonErrors.CELLAR_INTERNAL_ERROR).withCause(e)
                    .withMessage("Cannot init the variables: {}; {}").withMessageArgs(ID_DATA_VAR_NAME, LANG_VAR_NAME).build();
        }

        // execute commands
        Object o = null;
        String cmd = null;
        int i = 0;
        for (Placeholder placeholder : placeholders) {
            try {
                cmd = placeholder.getValue();
                LOG.debug("Evaluate the command: '{}'", cmd);

                cmd = cmd.substring(PLACEHOLDER_TAG_SIZE);
                cmd = cmd.substring(0, cmd.length() - PLACEHOLDER_TAG_SIZE);

                o = interpreter.eval(cmd);
            } catch (EvalError e) {
                LOG.error("Cannot evaluate the command: '" + cmd + "'", e);

                throw ExceptionBuilder.get(ExternalLinkException.class).withCode(CommonErrors.CELLAR_INTERNAL_ERROR).withCause(e)
                        .withMessage("Cannot evaluate the command: '{}'").withMessageArgs(cmd).build();
            }

            if (o == null) {
                LOG.error("The command '{}' returns 'null'.", cmd);

                throw ExceptionBuilder.get(ExternalLinkException.class).withCode(CommonErrors.CELLAR_INTERNAL_ERROR)
                        .withMessage("The command '{}' returns 'null'.").withMessage(cmd).build();
            }

            placeholdersInterpreted.add(i, o.toString());
            i++;
        }

        // construct external link
        final StringBuilder urlInterpreted = new StringBuilder(externalLinkMatched.getUrlPattern());
        final Iterator<Placeholder> iterator = placeholders.descendingIterator();
        Placeholder placeholder = null;

        for (int j = placeholders.size() - 1; iterator.hasNext(); j--) {
            placeholder = iterator.next();
            urlInterpreted.replace(placeholder.getStart(), placeholder.getEnd(), placeholdersInterpreted.get(j));
        }

        return new ExternalLink(urlInterpreted.toString());
    }

    /**
     * Gets the data from the document identifier.
     * @param matcher the matcher used to extract data
     * @return the extracted data
     */
    private String[] getIdData(final Matcher matcher) {
        final int groupCount = matcher.groupCount();
        final String[] datas = new String[groupCount];

        for (int i = 1; i <= groupCount; i++)
            datas[i - 1] = matcher.group(i);

        return datas;
    }

    /**
     * Sets the external link pattern cache manager.
     * @param externalLinkPatternCacheService external link pattern cache manager
     */
    public void setExternalLinkPatternCacheService(final ExternalLinkPatternCacheService externalLinkPatternCacheService) {
        this.externalLinkPatternService = externalLinkPatternCacheService;
    }
}
