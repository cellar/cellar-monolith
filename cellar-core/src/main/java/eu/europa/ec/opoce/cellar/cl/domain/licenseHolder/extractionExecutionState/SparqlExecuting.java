/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState
 *             FILE : SparqlExecuting.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 5, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState;

import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution.EXECUTION_STATUS;
import eu.europa.ec.opoce.cellar.cl.service.client.SparqlExecutingService;
import eu.europa.ec.opoce.cellar.common.Placeholder;
import eu.europa.ec.opoce.cellar.common.util.PlaceHolderUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.regex.Pattern;

/**
 * <class_description> SPARQL executing logic.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 5, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component("sparqlExecuting")
public class SparqlExecuting extends AbstractTask {

    private static final Logger LOG = LogManager.getLogger(SparqlExecuting.class);

    private final static String START_DATE_PLACEHOLDER_REGEX = "\\$\\{start_date.*?\\}";
    private final static String END_DATE_PLACEHOLDER_REGEX = "\\$\\{end_date.*?\\}";

    private final static Pattern START_DATE_PLACEHOLDER_PATTERN;
    private final static Pattern END_DATE_PLACEHOLDER_PATTERN;

    @Autowired
    private SparqlExecutingService sparqlExecutingService;

    /**
     * Static constructor.
     */
    static {
        START_DATE_PLACEHOLDER_PATTERN = Pattern.compile(START_DATE_PLACEHOLDER_REGEX, Pattern.DOTALL);
        END_DATE_PLACEHOLDER_PATTERN = Pattern.compile(END_DATE_PLACEHOLDER_REGEX, Pattern.DOTALL);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractTask execute(final ExtractionExecution extractionExecution) throws Exception {
        try {
            if (!extractionExecution.getNextExecutionGenerated()) {
                super.licenseHolderService.addNextExtractionExecution(extractionExecution);
            }

            extractionExecution.setSparqlTempFilePath(super.contructRelativeTemporarySparqlFilePath(extractionExecution));

            final String sparqlQuery = this.constructSparqlQuery(extractionExecution); // replaces the placeholders
            final String absoluteSparqlTempFilePath = super.licenseHolderService.getAbsoluteSparqlTempFilePath(extractionExecution);
            info(LOG, extractionExecution, "SPARQL query '%s' executing fills %s", sparqlQuery, absoluteSparqlTempFilePath);

            this.sparqlExecutingService.executeSparqlQuery(sparqlQuery, absoluteSparqlTempFilePath, null);

            return this.next(extractionExecution);
        } catch (final Exception exception) {
            this.error(extractionExecution, exception);
            error(LOG, extractionExecution, exception);
            throw exception;
        }
    }

    /**
     * Constructs the SPARQL query by replacing the placeholders.
     * @param extractionExecution the execution with the interval informations
     * @return the SPARQL generated
     */
    private String constructSparqlQuery(final ExtractionExecution extractionExecution) {
        String sparqlQuery = extractionExecution.getExtractionConfiguration().getSparqlQuery();
        List<Placeholder> placeholders = null;

        if (extractionExecution.getStartDate() != null) {
            placeholders = PlaceHolderUtils.getPlaceholders(sparqlQuery, START_DATE_PLACEHOLDER_PATTERN);
            sparqlQuery = PlaceHolderUtils.replacePlaceholdersByDate(sparqlQuery, placeholders, extractionExecution.getStartDate());
        }

        if (extractionExecution.getEndDate() != null) {
            placeholders = PlaceHolderUtils.getPlaceholders(sparqlQuery, END_DATE_PLACEHOLDER_PATTERN);
            sparqlQuery = PlaceHolderUtils.replacePlaceholdersByDate(sparqlQuery, placeholders, extractionExecution.getEndDate());
        }

        return sparqlQuery;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractTask next(final ExtractionExecution extractionExecution) {
        extractionExecution.setExecutionStatus(EXECUTION_STATUS.PARSING);
        super.licenseHolderService.updateExtractionExecution(extractionExecution);

        return super.executionStateManager.getTaskState(EXECUTION_STATUS.PARSING);
    }

    /**
     * Error.
     * @param extractionExecution the execution
     * @param exception the error description
     */
    protected void error(final ExtractionExecution extractionExecution, final Exception exception) {
        extractionExecution.setExecutionStatus(EXECUTION_STATUS.SPARQL_ERROR);
        super.error(extractionExecution, exception, false);
    }
}
