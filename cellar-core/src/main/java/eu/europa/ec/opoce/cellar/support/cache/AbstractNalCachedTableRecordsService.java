/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.support.cache
 *             FILE : AbstractNalCachedTableRecordsService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 26 août 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.support.cache;

import eu.europa.ec.opoce.cellar.cl.configuration.IS3Configuration;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.database.dao.NalConfigDao;
import eu.europa.ec.opoce.cellar.nal.dao.NalConfig;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.semantic.validation.NalValidationUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.jena.rdf.model.*;
import org.apache.jena.rdf.model.impl.RDFDefaultErrorHandler;
import org.apache.jena.riot.Lang;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration.NAL;
import static java.lang.System.currentTimeMillis;

/**
 * The Class AbstractNalCachedTableRecordsService.
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 26 août 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public abstract class AbstractNalCachedTableRecordsService extends AbstractCachedTableRecordsService {

    private static final Logger LOG = LogManager.getLogger(AbstractNalCachedTableRecordsService.class);

    protected boolean processingErrors = false;

    @Autowired
    private ContentStreamService contentStreamService;

    /**
     * options to work with the S3 repository.
     */
    @Autowired
    @Qualifier("cellarConfiguration")
    protected IS3Configuration s3Configuration;

    @Autowired
    protected NalConfigDao nalConfigDao;

    /**
     * Logger instance.
     */
    private static final Logger LOGGER = LogManager.getLogger(AbstractNalCachedTableRecordsService.class);

    /**
     * Are all itemss loaded.
     *
     * @return true, if successful
     */
    abstract public boolean areAllItemsLoaded();

    /**
     * Number of item descriptions loaded.
     *
     * @return the int
     */
    abstract public int numberOfItemDescriptionsLoaded();

    /**
     * Iterate through skos concepts.
     *
     * @param model the model
     * @return boolean false if the process could not be completed successfully
     */
    public boolean iterateThroughSkosConcepts(final Model model) {

        final Property nullProperty = null;
        final String nullObject = null;

        final ResIterator subjectsIterator = NalValidationUtils.filterOutSkosConceptsFromNalModel(model).listSubjects();
        try  {
            //iterate subjects
            final List<Resource> subjectsList = subjectsIterator.toList();
            for (final Resource subject : subjectsList) {
                final String subjectURI = subject.getURI();
                //iterate only through languages
                if (subject.isURIResource() && subjectURI.startsWith(getNalNamespace())) {
                    String identifier = subject.getProperty(NalValidationUtils.dcIdentifierCreateProperty()).getString();
                    if (StringUtils.isNotBlank(identifier)) {
                        identifier = identifier.toLowerCase();
                    }
                    final String opCode = subject.getProperty(NalValidationUtils.opCodeCreateProperty()).getString();
                    final HashMap<String, String> mappedCodes = new HashMap<>();
                    //list all statements for the description of this language
                    final StmtIterator opMappedCodeNodes = subject.listProperties(NalValidationUtils.opMappedCodeCreateProperty());
                    try {
                        while (opMappedCodeNodes.hasNext()) {
                            final Statement opMappedCodeNode = opMappedCodeNodes.next();
                            final Resource opMappedCodeStatementResource = opMappedCodeNode.getObject().asResource();
                            final MappedCodes codes = new MappedCodes();
                            //get the statements of the blank node owned by the language
                            final StmtIterator opMappedCodeStatements = model
                                    .listStatements(opMappedCodeStatementResource, nullProperty, nullObject);
                            try {
                                while (opMappedCodeStatements.hasNext()) {
                                    final Statement opMappedCodeStatement = opMappedCodeStatements.next();
                                    final RDFNode object = opMappedCodeStatement.getObject();
                                    final Property predicate = opMappedCodeStatement.getPredicate();
                                    //retrieve the matching set of source/legacyCode
                                    if (predicate.equals(NalValidationUtils.sourceCreateProperty())) {
                                        codes.setSource(object.toString());
                                    }
                                    if (predicate.equals(NalValidationUtils.legacyCodeUriCreateProperty())) {
                                        codes.setLegacyCode(object.toString());
                                    }
                                }
                            } finally {
                                opMappedCodeStatements.close();
                            }
                            addCodeToMappedCodes(codes, mappedCodes);
                        }
                    } finally {
                        opMappedCodeNodes.close();
                    }

                    collectMappedCodes(subjectURI, opCode, mappedCodes, identifier);
                }
                if (processingErrors) {
                    return false;
                }
            }
        }
        catch (Exception e) {
            LOGGER.error("Failed to iterate skos concepts. The fallback will be used instead to allow normal function of cellar.", e);
            return false;
        } finally {
            subjectsIterator.close();
        }
        // end for loop for subjects
        return true;
    }

    /**
     * When CELLAR starts, we need to first check if the NAL language can be found in S3.
     * If it is not found, we load the fallback NAL that we are sure that it can be processed.
     * If we have the NAL in S3 but we get processing errors, we also load the fallback NAL to allow CELLAR to start
     */
    @Override
    public void initialize() {
        initializeS3Context();
        Model model = null;
        RDFDefaultErrorHandler.silent = true;
        NalConfig nalPid = getNalConfig();
        try {
            // retrieve NAL from S3 or fallback NAL if not present
            if (nalPid == null) {
                LOG.error("The required NAL file-type or language could not be found in the database, loading fallback NAL...");
                model = JenaUtils.read(getEmbeddedNal());
            } else {
                // If version is null, the latest version will be retrieved
                try {
                    model = getNalFromRepository(nalPid.getExternalPid(), nalPid.getVersion(), nalPid.getUri(), ContentType.DIRECT);
                }
                catch (Exception e) {
                    model = null;
                }
                if (model == null) {
                    LOG.error("The NAL '{}' could not be retrieved from S3, loading fallback NAL...", nalPid);
                    model = JenaUtils.read(getEmbeddedNal());
                }
            }

            // check that model can be processed without error
            // if we have error, we use the fallback NAL that is guaranteed to work
            resetInMemoryMaps();
            if (!iterateThroughSkosConcepts(model)) {
                LOG.error("The NAL '{}' could not be processed successfully, loading the fallback NAL...", nalPid);
                resetInMemoryMaps();
                model = JenaUtils.read(getEmbeddedNal());
                iterateThroughSkosConcepts(model);
            }
            updateLastModificationDate();
            finishProcess();
        } catch (final IOException ioe) {
            LOG.error("The NAL from S3 [{}] and the embedded NAL could not be loaded.", nalPid, ioe);
        } finally {
            RDFDefaultErrorHandler.silent = false;
            JenaUtils.closeQuietly(model);
        }
    }

    /**
     * This method should be called when we need to reload the NAL from S3 to update the cache.
     * In case there is a processing error of the NAL in S3, we need to log the error and stop the processing.
     * The fallback NAL should not be loaded! Instead, the current NAL being used must keep being used.
     */
    public void reloadModel() {
        Model model = null;
        NalConfig nalConfig = getNalConfig();
        try {
            try {
                model = getNalFromRepository(nalConfig.getExternalPid(),  nalConfig.getVersion() , nalConfig.getUri(), ContentType.DIRECT);
            }
            catch (Exception e){
              model = null;
            }

            if (model == null) {
                LOG.warn("NAL '{}' could not be retrieved from S3, the cache will not be updated.", nalConfig);
            } else {
                resetInMemoryMaps();
                if (!iterateThroughSkosConcepts(model)) {
                    LOG.error("NAL '{}' could not be processed successfully. The cache will not be updated.", nalConfig);
                } else {
                    finishProcess();
                    updateLastModificationDate();
                }
            }
        } finally {
            JenaUtils.closeQuietly(model);
        }
    }

    protected void initializeS3Context() {}

    protected abstract void finishProcess();

    protected abstract NalConfig getNalConfig();

    protected abstract String getNalNamespace();

    protected abstract void resetInMemoryMaps();

    /**
     * Get file from S3 and parse it into a Model.
     *
     * @param pid
     * @param version
     * @param uri
     * @param  contentType the content type to be retrieved from S3. Should be in accordance with the version ID that it is requested.
     * @return the model
     */
    protected Model getNalFromRepository(String pid, String version, String uri, ContentType contentType) {
        long start = currentTimeMillis();
        LOG.debug(NAL, "Start retrieve NAL {} ({}, {})", uri, pid, version);
        String modelString = contentStreamService.getLargeContentAsString(pid, version, contentType)
                .orElseThrow(() -> new IllegalStateException("Pid (" + pid + ") and version (" + version + ") found but there is no file in the repository"));
        LOG.debug(NAL, "Done retrieving NAL in {} ms ({} ({}, {}))", currentTimeMillis() - start, uri, pid, version);
        LOG.info("Skos - [{}] ({}) - was successfully loaded.", uri, pid);
        return JenaUtils.read(modelString, Lang.NT);
    }

    protected abstract String getEmbeddedNal() throws IOException;

    /**
     * collect mapped codes.
     *
     * @param subjectURI  the subject uri
     * @param opCode      the op code
     * @param mappedCodes the mapped codes
     * @param identifier
     */
    abstract protected void collectMappedCodes(final String subjectURI, final String opCode, final HashMap<String, String> mappedCodes, String identifier);

    private void addCodeToMappedCodes(MappedCodes code, Map<String, String> mappedCodes) {
        String source = code.getSource();
        String legacyCode = code.getLegacyCode();

        if (mappedCodes.containsKey(source)) {
            legacyCode += "," + mappedCodes.get(source);
        }

        mappedCodes.put(source, legacyCode);
    }

    /**
     * an inner class to hold the mapped codes of a NAL concept
     * {@code
     * <p>
     * <at:op-mapped-code>
     * <at:MappedCode>
     * <dc:source>iso-639-2b</dc:source>
     * <at:legacy-code>eng</at:legacy-code>
     * </at:MappedCode>
     * </at:op-mapped-code>
     * <p>
     * ...
     * <p>
     * <at:MappedCode>
     * <dc:source>eudor-type-cellar</dc:source>
     * <at:legacy-code>PDF</at:legacy-code>
     * </at:MappedCode>
     * <p>
     * }.
     *
     * @author ARHS Developments
     * @version $Revision: 11439 $$
     */
    protected class MappedCodes {

        /**
         * The source.
         */
        private String source;
        /**
         * The legacy code.
         */
        private String legacyCode;

        /**
         * Instantiates a new mapped codes.
         */
        public MappedCodes() {
        }

        /**
         * Gets the source.
         *
         * @return the source
         */
        public String getSource() {
            return this.source;
        }

        /**
         * Sets the source.
         *
         * @param source the new source
         */
        public void setSource(final String source) {
            this.source = source;
        }

        /**
         * Gets the legacy code.
         *
         * @return the legacy code
         */
        public String getLegacyCode() {
            return this.legacyCode;
        }

        /**
         * Sets the legacy code.
         *
         * @param legacyCode the new legacy code
         */
        public void setLegacyCode(final String legacyCode) {
            this.legacyCode = legacyCode;
        }
    }
}
