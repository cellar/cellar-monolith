/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.virtuoso.dao.impl
 *             FILE : VirtuosoExceptionMessagesDaoImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 18 août 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.virtuoso.dao.impl;

import eu.europa.ec.opoce.cellar.cl.dao.impl.BaseDaoImpl;
import eu.europa.ec.opoce.cellar.cl.domain.virtuoso.VirtuosoExceptionMessages;
import eu.europa.ec.opoce.cellar.cl.domain.virtuoso.dao.VirtuosoExceptionMessagesDao;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Repository;

/**
 * The Class VirtuosoExceptionMessagesDaoImpl.
 * <class_description> The dao class the accesses the repository to retrieve the virtuoso message objects
 * <br/><br/>
 * ON : 18 août 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Repository
public class VirtuosoExceptionMessagesDaoImpl extends BaseDaoImpl<VirtuosoExceptionMessages, Long> implements VirtuosoExceptionMessagesDao {

    /** {@inheritDoc} */
    @Override
    public List<VirtuosoExceptionMessages> findByMessage(final String message) {
        final HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("message", "%" + message + "%");
        final List<VirtuosoExceptionMessages> result = findObjectsByNamedQueryWithNamedParams("message", params);
        return result;
    }

}
