/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ccr.strategy
 *             FILE : InboundTransformationStrategy.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 19-Apr-2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.strategy;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.cl.domain.transformation.inbound.ContentStreamAndIdentifiers;
import eu.europa.ec.opoce.cellar.cl.domain.transformation.inbound.impl.ContentStreamAndIdentifiersImpl;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentStream;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectStrategy;
import eu.europa.ec.opoce.cellar.domain.content.mets.ManifestationTransformator;
import eu.europa.ec.opoce.cellar.domain.content.mets.Metadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Inbound transformation managing.
 * During the parsing of the mets inbound transformation information have been added to each digital object.
 * now, we will process every digital object (recursively) to apply corresponding transformation.
 *
 * @author dcraeye
 *
 */
public class InboundTransformationStrategy implements DigitalObjectStrategy {

    private final SIPResource sipResource;

    @Autowired
    @Qualifier("cellarConfiguration")
    private final ICellarConfiguration cellarConfiguration;

    /**
     * Processing inbound trasnformation.
     *
     * @param sipResource contains the location of the mets file to process.
     * @param cellarConfiguration the cellar configuration
     */
    public InboundTransformationStrategy(final SIPResource sipResource, final ICellarConfiguration cellarConfiguration) {
        this.sipResource = sipResource;
        this.cellarConfiguration = cellarConfiguration;
    }

    /**
     * Manage the inbound transformation of the current digital object.
     * @param digitalObject the current digital object.
     */
    @Override
    public void applyStrategy(final DigitalObject digitalObject) {

        //Check if transformation is needed
        if (digitalObject.getTransformatorList() != null) {

            boolean firstTransformation = true;
            for (final ManifestationTransformator transformator : digitalObject.getTransformatorList()) {
                final List<ContentStreamAndIdentifiers> inboundTransformationContentList = new LinkedList<>();
                final List<ContentStreamAndIdentifiers> inboundTransformationTechnicalMetadataList = new LinkedList<>();

                if (firstTransformation) {
                    this.fillFirstInboundTransformationList(transformator, inboundTransformationContentList,
                            inboundTransformationTechnicalMetadataList);
                    firstTransformation = false;
                }
            }
        }
    }

    /**
     * Fill two lists of {@link ContentStreamAndIdentifiers}.
     * - one with content stream usable for the transformator,
     * - and another with technical metadata.
     *
     * @param transformator the transformator that contains the transformed stream.
     * @param inboundTransformationContentList list of content.
     * @param inboundTransformationTechnicalMetadataList list of technical metadata.
     */
    private void fillFirstInboundTransformationList(final ManifestationTransformator transformator,
            final List<ContentStreamAndIdentifiers> inboundTransformationContentList,
            final List<ContentStreamAndIdentifiers> inboundTransformationTechnicalMetadataList) {
        // For each transformed content stream, store them on a disk (in the mets folder).
        for (final ContentStream contentStream : transformator.getContentStreamList()) {
            InputStream in = null;
            try {
                in = new FileInputStream(new File(
                        this.cellarConfiguration.getCellarFolderTemporaryWork(),
                        this.sipResource.getFolderName() + "/" +
                                contentStream.getFileRef()));

                final List<String> contentStreamIdentifierList = new ArrayList<>();
                for (final ContentIdentifier contentIdentifier : contentStream.getContentids()) {
                    contentStreamIdentifierList.add(contentIdentifier.getIdentifier());
                }
                inboundTransformationContentList
                        .add(new ContentStreamAndIdentifiersImpl(contentStreamIdentifierList, in, contentStream.getMimeType()));
            } catch (final FileNotFoundException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (in != null) {
                        in.close();
                    }
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }

            }
        }

        // For each transformed metadata , store them on a disk (in the mets folder).
        for (final Metadata technicalMetadata : transformator.getTechnicalMetadataList()) {
            InputStream in = null;
            try {
                in = new FileInputStream(new File(this.cellarConfiguration.getCellarFolderTemporaryWork(),
                        this.sipResource.getFolderName() + "/" + technicalMetadata.getMetadataStream().getFileRef()));

                final List<String> contentStreamIdentifierList = new ArrayList<>();
                contentStreamIdentifierList.add(technicalMetadata.getId());
                inboundTransformationTechnicalMetadataList.add(new ContentStreamAndIdentifiersImpl(contentStreamIdentifierList, in,
                        technicalMetadata.getMetadataStream().getManifestationType()));
            } catch (final FileNotFoundException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (in != null) {
                        in.close();
                    }
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        }
    }
}
