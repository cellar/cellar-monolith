/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ccr.s3.read.impl
 *             FILE : LanguageDetails.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 20 Apr 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.s3.read.impl;

import java.util.HashMap;
import java.util.Objects;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 20 Apr 2017
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class LanguageDetails {

    /**
     * The uri.
     */
    private String uri;

    /**
     * The op code.
     */
    private String opCode;

    /**
     * The codes.
     */
    private HashMap<String, String> codes;

    /**
     * Gets the uri.
     *
     * @return the uri
     */
    public String getUri() {
        return this.uri;
    }

    /**
     * Sets the uri.
     *
     * @param uri the new uri
     */
    public void setUri(final String uri) {
        this.uri = uri;
    }

    /**
     * Gets the op code.
     *
     * @return the op code
     */
    public String getOpCode() {
        return this.opCode;
    }

    /**
     * Sets the op code.
     *
     * @param opCode the new op code
     */
    public void setOpCode(final String opCode) {
        this.opCode = opCode;
    }

    /**
     * Gets the codes.
     *
     * @return the codes
     */
    public HashMap<String, String> getCodes() {
        return this.codes;
    }

    /**
     * Sets the codes.
     *
     * @param codes the codes
     */
    public void setCodes(final HashMap<String, String> codes) {
        this.codes = codes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LanguageDetails)) return false;
        LanguageDetails that = (LanguageDetails) o;
        return Objects.equals(getUri(), that.getUri()) &&
                Objects.equals(getOpCode(), that.getOpCode()) &&
                Objects.equals(getCodes(), that.getCodes());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUri(), getOpCode(), getCodes());
    }
}
