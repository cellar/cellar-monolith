package eu.europa.ec.opoce.cellar.cl.service.mail;

import eu.europa.ec.opoce.cellar.cl.domain.email.Email;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;

public interface MailSenderService {

    void sendHtmlMessage(Email email) throws MessagingException, UnsupportedEncodingException;
    
    List<Email> getEmailsToSend();

    void updateEmailOnSuccess(Email email);

    void updateEmailOnFailure(Email email);
    
    /**
     * Deletes from the <code>EMAIL</code> table those entries whose creation date is older
     * than the supplied date.
     * @param cleanupCutoffDate the cleanup cutoff date.
     * @return the number of email entries that were deleted.
     */
    int deleteEmailEntriesCreatedBefore(Date cleanupCutoffDate);
    
}
