/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.dao
 *        FILE : PackageListDao.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 03-12-2021
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2021 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao;

import java.util.List;

import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.PackageList;

/**
 * Provides access to the database view containing the
 * references of the packages (their IDs) that are contained
 * in the database queue.
 * @author EUROPEAN DYNAMICS S.A.
 */
public interface PackageListDao {

	/**
	 * Retrieves the SIP_IDs of all packages contained
	 * in the database queue, ordered by their logical
	 * ordering in the queue.
	 * @return
	 */
	List<PackageList> getPackageListOrdered();
	
}
