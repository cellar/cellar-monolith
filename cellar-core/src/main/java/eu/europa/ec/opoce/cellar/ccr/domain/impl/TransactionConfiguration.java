/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ccr.domain.impl
 *             FILE : TransactionConfiguration.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 25 Apr 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.domain.impl;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 25 Apr 2017
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class TransactionConfiguration {

    /** The load existing direct model. */
    private boolean loadExistingDirectModel;

    /** The load existing direct and inferred model. */
    private boolean loadExistingDirectAndInferredModel;

    /**
     * Instantiates a new transaction configuration.
     */
    private TransactionConfiguration() {
    }

    /**
     * Gets the.
     *
     * @return the transaction configuration
     */
    public static TransactionConfiguration get() {
        return new TransactionConfiguration();
    }

    /**
     * With load existing direct model.
     *
     * @param value the value
     * @return the transaction configuration
     */
    public TransactionConfiguration withLoadExistingDirectModel(final boolean value) {
        this.loadExistingDirectModel = value;
        return this;
    }

    /**
     * With load existing direct and inferred model.
     *
     * @param value the value
     * @return the transaction configuration
     */
    public TransactionConfiguration withLoadExistingDirectAndInferredModel(final boolean value) {
        this.loadExistingDirectAndInferredModel = value;
        return this;
    }

    /**
     * Checks if is load existing direct model.
     *
     * @return true, if is load existing direct model
     */
    public boolean isLoadExistingDirectModel() {
        return this.loadExistingDirectModel;
    }

    /**
     * Checks if is load existing direct and inferred model.
     *
     * @return true, if is load existing direct and inferred model
     */
    public boolean isLoadExistingDirectAndInferredModel() {
        return this.loadExistingDirectAndInferredModel;
    }

}
