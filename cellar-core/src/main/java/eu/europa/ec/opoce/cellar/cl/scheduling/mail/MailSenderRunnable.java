package eu.europa.ec.opoce.cellar.cl.scheduling.mail;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.email.Email;
import eu.europa.ec.opoce.cellar.cl.service.mail.MailSenderService;

import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Date;
import java.util.List;

public class MailSenderRunnable implements Runnable {

    private static final Logger LOGGER = LogManager.getLogger(MailSenderRunnable.class);
    private static final String MAIL_JOB = "mail";

    private static final long MILLIS_PER_DAY = 86400000;
    
    private final MailSenderService mailSenderService;

    private final ICellarConfiguration cellarConfiguration;
    
    public MailSenderRunnable(MailSenderService mailSenderService, ICellarConfiguration cellarConfiguration) {
        this.mailSenderService = mailSenderService;
        this.cellarConfiguration = cellarConfiguration;
    }

    private void send() {
        final List<Email> mails = this.mailSenderService.getEmailsToSend();
        
        if (CollectionUtils.isEmpty(mails)) {
        	LOGGER.info("Job [{}]: no emails to send.", MAIL_JOB);
        	return;
        }
        LOGGER.info("Job [{}] started: attempting to send {} emails.", MAIL_JOB, mails.size());
        int sentEmailsNum = 0;
        for (Email email : mails) {
            try {
                this.mailSenderService.sendHtmlMessage(email);
                this.mailSenderService.updateEmailOnSuccess(email);
                sentEmailsNum++;
            } catch (Exception e) {
                this.mailSenderService.updateEmailOnFailure(email);
                LOGGER.error("An error occurred while attempting to send email with ID [{}].", email.getId(), e);
            }
        }
        LOGGER.info("Job [{}] finished: {} emails have been sent.", MAIL_JOB, sentEmailsNum);
    }

    /**
     * Computes the email deletion cutoff date and calls the email cleanup service method
     * which removes old email entries from the EMAIL table.
     */
    private void cleanup() {
    	long maxTimeInMillisToKeep = this.cellarConfiguration.getCellarServiceEmailCleanupMaxAgeDays() * MILLIS_PER_DAY;
    	Date cutoffDate = new Date(System.currentTimeMillis() - maxTimeInMillisToKeep);
    	int numDeletedEntries = 0;
    	try {
    		numDeletedEntries = this.mailSenderService.deleteEmailEntriesCreatedBefore(cutoffDate);
    	}
    	catch(Exception e) {
    		LOGGER.error("An error occurred while performing deletion of email entries.", e);
    	}
    	if (numDeletedEntries > 0) {
    		 LOGGER.info("Job [{}]: {} email entries have been deleted.", MAIL_JOB, numDeletedEntries);
    	}
    }
    
    @Override
    public void run() {
        if (cellarConfiguration.getCellarServiceEmailEnabled()) {
            send();
            cleanup();
        }
    }
    
}
