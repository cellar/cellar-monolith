/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState
 *             FILE : ArchivingInit.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 12, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState;

import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ArchiveTreeManager;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution.EXECUTION_STATUS;
import eu.europa.ec.opoce.cellar.cl.service.client.ExtractionExecutionService;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * <class_description> Archiving init logic.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 12, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component("archivingInit")
public class ArchivingInit extends AbstractTask {

    /**
     * Logger instance.
     */
    private static final Logger LOG = LogManager.getLogger(ArchivingInit.class);

    @Autowired
    private ExtractionExecutionService extractionExecutionService;

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractTask execute(final ExtractionExecution extractionExecution) throws Exception {
        try {
            extractionExecution.setArchiveTempDirectoryPath(super.contructRelativeTemporaryArchiveDirectoryPath(extractionExecution));

            final String absoluteArchiveTempDirectoryPath = super.licenseHolderService
                    .getAbsoluteArchiveTempDirectoryPath(extractionExecution);

            info(LOG, extractionExecution, "Archiving init mkdirs %s", absoluteArchiveTempDirectoryPath);

            // constructs the temp directory
            final File tempDirectory = new File(absoluteArchiveTempDirectoryPath);
            FileUtils.forceMkdir(tempDirectory);

            final int batchSizePerThread = super.cellarConfiguration.getCellarServiceLicenseHolderBatchSizePerThread();
            final Long numberOfWork = super.licenseHolderService.getWorkIdentifierCount(extractionExecution.getId());

            if (numberOfWork <= 0) // no work to extract, we construct an empty archive
                return this.nextArchivingFinalize(extractionExecution);

            this.next(extractionExecution);

            // constructs and adds the batches
            final double numberOfBatch = Math.ceil(numberOfWork.doubleValue() / batchSizePerThread);
            final ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
            final AtomicInteger archivingBatchWorker = new AtomicInteger((int) numberOfBatch);
            final ArchiveTreeManager archiveTreeManager = new ArchiveTreeManager(
                    this.cellarConfiguration.getCellarServiceLicenseHolderBatchSizeFolder(), numberOfWork * 2,
                    absoluteArchiveTempDirectoryPath);

            info(LOG, extractionExecution, "Archiving init prepares %s archiving batches", numberOfBatch);

            for (int i = 0; i < numberOfBatch; i++)
                this.extractionExecutionService.addArchivingBatchTask(extractionExecution, i * batchSizePerThread,
                        (i + 1) * batchSizePerThread, archiveTreeManager, readWriteLock, archivingBatchWorker);

        } catch (Exception exception) {
            this.error(extractionExecution, exception);
            throw exception;
        }

        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractTask next(final ExtractionExecution extractionExecution) {
        extractionExecution.setExecutionStatus(EXECUTION_STATUS.ARCHIVING);
        super.licenseHolderService.updateExtractionExecution(extractionExecution);

        return null;
    }

    /**
     * The next step is the archiving finalize.
     * @param extractionExecution the execution
     * @return the next step
     */
    public AbstractTask nextArchivingFinalize(final ExtractionExecution extractionExecution) {
        extractionExecution.setExecutionStatus(EXECUTION_STATUS.ARCHIVING_FINALIZE);
        super.licenseHolderService.updateExtractionExecution(extractionExecution);

        return super.executionStateManager.getTaskState(EXECUTION_STATUS.ARCHIVING_FINALIZE);
    }

    /**
     * Error.
     * @param extractionExecution the execution
     * @param exception the error description
     */
    protected void error(final ExtractionExecution extractionExecution, final Exception exception) {
        extractionExecution.setExecutionStatus(EXECUTION_STATUS.ARCHIVING_ERROR);
        super.error(extractionExecution, exception, false);
    }
}
