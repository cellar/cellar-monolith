package eu.europa.ec.opoce.cellar.ccr.service;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.support.AbstractServiceStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * @author EUROPEAN DYNAMICS S.A.
 */
@Component("statusServiceStatus")
public class StatusServiceStatus extends AbstractServiceStatus {
    
    @Autowired
    public StatusServiceStatus(@Qualifier("cellarConfiguration") ICellarConfiguration cellarConfiguration) {
        super("Status API", cellarConfiguration);
    }
    
    /**
     * @see eu.europa.ec.opoce.cellar.support.AbstractServiceStatus#resolveServiceUrl()
     */
    @Override
    protected String resolveServiceUrl() {
        return this.cellarConfiguration.getCellarStatusServiceBaseUrl() + "/actuator/health";
    }
}
