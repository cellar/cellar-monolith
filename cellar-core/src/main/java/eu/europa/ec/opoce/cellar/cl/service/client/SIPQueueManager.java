/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.service.client
 *        FILE : SIPQueueManager.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 10-12-2021
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2021 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.client;

import java.util.Collection;
import java.util.concurrent.locks.ReentrantLock;

import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.SIPPackage;

/**
 * Interface providing access to the functionalities of the
 * SIP Queue Manager, whose purpose is to compute the modifications
 * to be performed to the DB queue when new packages need to be
 * added or existing ones to be removed.
 * @author EUROPEAN DYNAMICS S.A.
 */
public interface SIPQueueManager {

	/**
	 * Performs prioritization of the newly detected SIP packages
	 * (status 'NEW') by updating the DB queue appropriately.
	 */
	void prioritizePackagesOnInsert();
	
	/**
	 * Performs prioritization of the SIP packages when packages
	 * previously existing in the ingestion folders are no
	 * longer available.
	 * @param packagesToRemove the packages that are no longer available
	 * in the ingestion folders.
	 */
	void prioritizePackagesOnRemoval(Collection<SIPPackage> packagesToRemove);
	
	/**
	 * Returns the DB SIP queue lock.
	 * @return the DB SIP queue lock.
	 */
	ReentrantLock getSipQueueLock();
}