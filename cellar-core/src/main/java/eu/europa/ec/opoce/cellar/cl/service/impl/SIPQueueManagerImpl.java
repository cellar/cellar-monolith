/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *        FILE : SIPQueueManagerImpl.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 10-12-2021
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2021 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import eu.europa.ec.opoce.cellar.ccr.service.database.DefaultTransactionService;
import eu.europa.ec.opoce.cellar.cl.dao.PackageHasParentPackageDao;
import eu.europa.ec.opoce.cellar.cl.dao.PackageListDao;
import eu.europa.ec.opoce.cellar.cl.dao.SIPPackageDao;
import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.PackageHasParentPackage;
import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.PackageList;
import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.SIPPackage;
import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.SIPPackageProcessingStatus;
import eu.europa.ec.opoce.cellar.cl.service.client.SIPQueueManager;
import eu.europa.ec.opoce.cellar.cl.service.client.SIPQueueManagerStorageService;
import eu.europa.ec.opoce.cellar.logging.LogContext;

import static eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.SIPPackageProcessingStatus.*;
import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.SIP_QUEUE_MANAGER;

/**
 * Implementation of the SIP Queue Manager, whose purpose is to compute the modifications
 * to be performed to the DB queue when new packages need to be
 * added or existing ones to be removed.
 * @author EUROPEAN DYNAMICS S.A.
 */
@Service("sipQueueManager")
public class SIPQueueManagerImpl extends DefaultTransactionService implements SIPQueueManager {
	
	private static final Logger LOG = LogManager.getLogger(SIPQueueManagerImpl.class);
	
	private SIPPackageDao sipPackageDao;
	
	private PackageHasParentPackageDao packageHasParentPackageDao;
	
	private PackageListDao packageListDao;
	
	private SIPQueueManagerStorageService sipQueueManagerStorageService;
	
	private final ReentrantLock lock = new ReentrantLock(true);
	
	@Autowired
	public SIPQueueManagerImpl(SIPPackageDao sipPackageDao, PackageHasParentPackageDao packageHasParentPackageDao,
							   PackageListDao packageListDao, SIPQueueManagerStorageService sipQueueManagerStorageService) {
		this.sipPackageDao = sipPackageDao;
		this.packageHasParentPackageDao = packageHasParentPackageDao;
		this.packageListDao = packageListDao;
		this.sipQueueManagerStorageService = sipQueueManagerStorageService;
	}
	
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	@Transactional
	@LogContext(SIP_QUEUE_MANAGER)
	public void prioritizePackagesOnInsert() {
		// Get the number of NEW packages.
		long numNewPackages = this.sipPackageDao.countBySipStatusIn(Arrays.asList(NEW));
		if (numNewPackages == 0) {
			return;
		}

		// Get all package references from DB.
		List<SIPPackage> sipPackages = this.sipPackageDao.findAll();
		// Extract the package references with status 'NEW'.
		List<SIPPackage> newSipPackages = extractPackagesHavingStatus(sipPackages, NEW);

		// Construct a PriorityQueue based on the package references fetched from the DB.
		PriorityQueue<SIPPackage> sipQueue = new PriorityQueue<>(sipPackages);
		
		// The set containing the new entries to insert to the DB queue.
		Set<PackageHasParentPackage> databaseQueueEntriesToInsert = new HashSet<>();
		// The map containing the rows of the DB queue to update based on their
		// SIP_ID (K), and the value to set to the PARENT_SIP_ID (V).
		Map<Long, Long> databaseQueueEntriesToUpdate = new HashMap<>();
		
		// Iterate over the in-memory queue and compute the rows of the DB-queue to
		// insert as a whole or update their parent. These are:
		// a. The head of the queue (Insert or Update).
		// b. The packages with status 'NEW' (Insert).
		// c. The packages with status 'SCHEDULED' (already existing in the DB-queue)
		//    that have a 'NEW' package right in front of them (Update).
		SIPPackage previousSip = null;
		SIPPackage currentSip = null;
		while (sipQueue.peek() != null) {
			currentSip = sipQueue.peek();
			generateDbQueueUpdateData(currentSip, previousSip, databaseQueueEntriesToInsert, databaseQueueEntriesToUpdate);
			previousSip = sipQueue.poll();
		}	
		try {
			// Verify the integrity of the queue after its modification and before persisting the changes.
			verifySipIngestionQueueIntegrityOnInsert(databaseQueueEntriesToInsert, databaseQueueEntriesToUpdate);
		} catch (InvalidSipIngestionQueueState e) {
			// If the queue is about to enter an inconsistent state, reset it and abort.
			LOG.warn("Emptying and resetting the SIP ingestion queue in order to avoid entering an invalid state.", e);
			resetSipIngestionQueue();
			return;
		}
		try {
			// Update the DB queue with the calculated data.
			this.sipQueueManagerStorageService.updateQueueOnInsert(databaseQueueEntriesToInsert, databaseQueueEntriesToUpdate);
			// Set the status of the inserted package references to 'SCHEDULED'.
			updatePackageStatus(newSipPackages, SCHEDULED);
			logInsertedPackageReferences(sipPackages, newSipPackages);
		} catch (Exception e) {
			LOG.error("An error occurred while attempting to schedule {} new packages.", newSipPackages.size());
			throw new IllegalStateException(e);
		}
		
	}
	
	/**
	 * Examines the status of two consecutive entries of the in-memory queue
	 * and decides if a corresponding entry should be inserted in the DB queue,
	 * or if a corresponding entry already existing in the DB should be updated. 
	 * @param currentSip the current SIP reference.
	 * @param previousSip the parent SIP reference of the current SIP reference.
	 * @param databaseQueueEntriesToInsert the set containing the new entries to be
	 * inserted to the DB queue.
	 * @param databaseQueueEntriesToUpdate the set containing the current entries of
	 * the DB queue that are affected by the new entries and should thus be updated.
	 */
	private void generateDbQueueUpdateData(SIPPackage currentSip, SIPPackage previousSip,
			Set<PackageHasParentPackage> databaseQueueEntriesToInsert, Map<Long, Long> databaseQueueEntriesToUpdate) {
		if (currentSip.getStatus() != NEW) {
			// The package has already been scheduled, so a corresponding
			// reference row should already exist in the DB queue (PHPP table).
			if (previousSip == null) {
				// The current package reference is at the head of the queue.
				databaseQueueEntriesToUpdate.put(currentSip.getId(), null);
			}
			else {
				databaseQueueEntriesToUpdate.put(currentSip.getId(), previousSip.getId());
			}
		}
		else {
			// A new, never-before-scheduled package; a new reference row should be
			// persisted in the DB queue (PHPP table).
			PackageHasParentPackage phpp = new PackageHasParentPackage();
			phpp.setSipId(currentSip.getId());
			if (previousSip == null) {
				// The current package reference is at the head of the queue.
				phpp.setParentSipId(null);
			}
			else {
				phpp.setParentSipId(previousSip.getId());
			}
			databaseQueueEntriesToInsert.add(phpp);
		}
	}
	
	/**
	 * Extracts the package references having a status equal to the provided one.
	 * @param sipList the list of package references to parse.
	 * @param status the status to set.
	 * @return the package references having status equal to the one provided.
	 */
	private List<SIPPackage> extractPackagesHavingStatus(List<SIPPackage> sipList, SIPPackageProcessingStatus status) {
		return sipList.stream().filter(sip -> sip.getStatus() == status).collect(Collectors.toList());
	}
	
	/**
	 * Updates the status of the provided SIP package references to the specified value.
	 * @param sipPackages the package references whose status should be updated.
	 * @param statusToSet the status value to set the status of the package references to.
	 */
	private void updatePackageStatus(List<SIPPackage> sipPackages, SIPPackageProcessingStatus statusToSet) {
		for (SIPPackage sip : sipPackages) {
			sip.setStatus(statusToSet);
			this.sipPackageDao.updateObject(sip);
		}
	}
	
	/**
	 * Logs the entries corresponding to the new package references inserted in the DB package references queue.
	 * @param sipPackages the list containing all package references.
	 * @param newSipPackages the new packages inserted in the DB package references queue.
	 */
	private void logInsertedPackageReferences(List<SIPPackage> sipPackages, List<SIPPackage> newSipPackages) {
		LOG.info("{} entries inserted [SIP -> position]:", newSipPackages.size());
		Map<String, Long> packagePositions = new HashMap<>();
		// Prepare a SIP-name - position look up table.
		Collections.sort(sipPackages);
		long pos = 0;
		for (SIPPackage sipPackage : sipPackages) {
			packagePositions.put(sipPackage.getName(), pos++);
		}
		// Create a log entry for each package reference inserted in the DB queue.
		newSipPackages.forEach(p -> 
			LOG.info("{} -> {}", p, packagePositions.get(p.getName()))
		);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Transactional
	@Override
	@LogContext(SIP_QUEUE_MANAGER)
	public void prioritizePackagesOnRemoval(Collection<SIPPackage> packagesToRemove) {
		// Extract the IDs of the package references to remove.
		Set<Long> sipIdsToRemove = packagesToRemove.stream()
				.map(SIPPackage::getId)
				.collect(Collectors.toSet());
		
        // Get from the DB queue the entries whose SIP_ID or PARENT_SIP_ID matches the IDs of the package references to remove.
		// 'PackageHasParentPackage' (PHPP) entries have the following structure: [SIP_ID, PARENT_SIP_ID].
        List<PackageHasParentPackage> queueEntriesFromDatabase = this.packageHasParentPackageDao.findAll();
        Map<Long, PackageHasParentPackage> queueEntriesFromDatabaseIndexed = queueEntriesFromDatabase.stream()
        		.collect(Collectors.toMap(PackageHasParentPackage::getSipId, phpp -> phpp));
        
		// Retrieve the references of the packages (the SIP_ID) that are contained in the DB queue
        // following their actual/logical ordering in the queue.
        // 'PackageList' (PL) entries have the following structure: [SIP_ID].
		List<PackageList> packagesInDbQueueOrdered = this.packageListDao.getPackageListOrdered();
		
		// Initialize the sets that will contain the DB queue entries to be removed or updated.
		Set<PackageHasParentPackage> entriesToRemove = new HashSet<>();
		Set<PackageHasParentPackage> entriesToUpdate = new HashSet<>();
		
		// Compute the DB queue PHPP entries that will be removed.
		entriesToRemove = queueEntriesFromDatabase.stream()
				.filter(entry -> sipIdsToRemove.contains(entry.getSipId()))
				.collect(Collectors.toSet());

		// Compute the PL entries, i.e. the SIP_IDs of the package references that will remain in the DB queue.
		List<PackageList> packagesInDbQueueOrderedRemaining =
				packagesInDbQueueOrdered.stream()
					.filter(pl -> !sipIdsToRemove.contains(pl.getSipId()))
					.collect(Collectors.toList());
		
		updateParentSipIdReferences(packagesInDbQueueOrderedRemaining, queueEntriesFromDatabaseIndexed, entriesToUpdate);
		updateQueueHeadParentSipIdReference(packagesInDbQueueOrderedRemaining, queueEntriesFromDatabaseIndexed, entriesToUpdate);
		
		try {
			// Verify the integrity of the queue after its modification.
			verifySipIngestionQueueIntegrityOnRemoval(entriesToRemove, entriesToUpdate);
		} catch (InvalidSipIngestionQueueState e) {
			// If the queue is about to enter an inconsistent state, reset it and abort.
			LOG.warn("Emptying and resetting the SIP ingestion queue in order to avoid entering an invalid state.", e);
			resetSipIngestionQueue();
			return;
		}
		// Update the DB queue table with the computed modifications.
		this.sipQueueManagerStorageService.updateQueueOnRemoval(entriesToRemove, entriesToUpdate);
		logRemovedPackageReferences(packagesToRemove);
	}
	
	/**
	 * Logs the entries corresponding to the package references that were removed.
	 * @param packagesToRemove the package references removed from the queue.
	 */
	private void logRemovedPackageReferences(Collection<SIPPackage> packagesToRemove) {
		LOG.info("{} entries removed:", packagesToRemove.size());
		packagesToRemove.forEach(LOG::info);
	}
	
	/**
	 * Iterates over the  references (SIP_IDs) of the packages that will remain in the DB queue,
	 * and updates the PARENT_SIP_ID references of the packages in the DB queue (PHPP) whose parent
	 * will be removed.
	 * @param packagesInDbQueueOrderedRemaining the list of SIP_IDs that will remain in the queue.
	 * @param affectedQueueEntiesMap the entries of the DB queue that are affected by the removal
	 * of the packages.
	 * @param entriesToUpdate the DB queue entries whose PARENT_SIP_ID will be updated as
	 * a result of the removal of the packages.
	 */
	private void updateParentSipIdReferences(List<PackageList> packagesInDbQueueOrderedRemaining,
			Map<Long, PackageHasParentPackage> affectedQueueEntiesMap, Set<PackageHasParentPackage> entriesToUpdate) {
		for (int i = 0; i < packagesInDbQueueOrderedRemaining.size() - 1; i++) {
			long parentSipIdValueToSet = packagesInDbQueueOrderedRemaining.get(i).getSipId();
			long sipIdToLookFor = packagesInDbQueueOrderedRemaining.get(i+1).getSipId();
			
			PackageHasParentPackage dbQueueEntryToUpdate = affectedQueueEntiesMap.get(sipIdToLookFor);
			if (dbQueueEntryToUpdate != null) {
				dbQueueEntryToUpdate.setParentSipId(parentSipIdValueToSet);
				entriesToUpdate.add(dbQueueEntryToUpdate);
			}
		}
	}
	
	/**
	 * Identifies the new head of the DB queue, in case the current one is to be removed,
	 * and sets its PARENT_SIP_ID to null.
	 * @param packagesInDbQueueOrderedRemaining the list of SIP_IDs that will remain in the queue.
	 * @param affectedQueueEntiesMap the entries of the DB queue that are affected by the removal
	 * of the packages.
	 * @param entriesToUpdate the DB queue entries whose PARENT_SIP_ID will be updated as
	 * a result of the removal of the packages.
	 */
	private void updateQueueHeadParentSipIdReference(List<PackageList> packagesInDbQueueOrderedRemaining,
			Map<Long, PackageHasParentPackage> affectedQueueEntiesMap, Set<PackageHasParentPackage> entriesToUpdate) {
		if (!packagesInDbQueueOrderedRemaining.isEmpty()) {
			Long dbQueueHeadId = packagesInDbQueueOrderedRemaining.get(0).getSipId();
			PackageHasParentPackage newDbQueueHead = affectedQueueEntiesMap.get(dbQueueHeadId);
			if (newDbQueueHead != null) {
				newDbQueueHead.setParentSipId(null);
				entriesToUpdate.add(newDbQueueHead);
			}
		}
	}

	/**
	 * Prepares the modified SIP ingestion queue for integrity verification when
	 * new entries are about to be inserted.
	 * @param entriesToInsert
	 * @param entriesToUpdate
	 */
	private void verifySipIngestionQueueIntegrityOnInsert(Set<PackageHasParentPackage> entriesToInsert, Map<Long, Long> entriesToUpdate) {
		List<PackageHasParentPackage> sipQueueEntries = this.packageHasParentPackageDao.findAll();
		// First add the newly generated queue entries to the existing ones.
		sipQueueEntries.addAll(entriesToInsert);
		int queueSize = sipQueueEntries.size();
		Map<Long, Long> queueSipIdPairs = new HashMap<>();
		// Populate the map with the new and existing queue entries IDs.
		sipQueueEntries.forEach(entry -> queueSipIdPairs.put(entry.getSipId(), entry.getParentSipId()));
		// Then add the newly updated versions of the existing queue entries IDs, so that the final
		// version of the map reflects the final modified version of the queue.
		entriesToUpdate.forEach((k,v) -> queueSipIdPairs.put(k, v));
		verifySipIngestionQueueIntegrity(queueSipIdPairs, queueSize);
	}
	
	/**
	 * Prepares the modified SIP ingestion queue for integrity verification when
	 * existing entries are about to be removed.
	 * @param entriesToRemove the queue entries that will be removed.
	 * @param entriesToUpdate the queue entries that will be updated.
	 */
	private void verifySipIngestionQueueIntegrityOnRemoval(Set<PackageHasParentPackage> entriesToRemove, Set<PackageHasParentPackage> entriesToUpdate) {
		List<PackageHasParentPackage> sipQueueEntries = this.packageHasParentPackageDao.findAll();
		// First remove the necessary set of queue entries from the existing ones.
		sipQueueEntries.removeAll(entriesToRemove);
		int queueSize = sipQueueEntries.size();
		Map<Long, Long> queueSipIdPairs = new HashMap<>();
		// Populate the map with the IDs of the remaining subset of the existing queue entries.
		sipQueueEntries.forEach(entry -> queueSipIdPairs.put(entry.getSipId(), entry.getParentSipId()));
		// Then add the newly updated versions of the existing queue entries IDs, so that the final
		// version of the map reflects the final modified version of the queue.
		entriesToUpdate.forEach(entry -> queueSipIdPairs.put(entry.getSipId(), entry.getParentSipId()));
		verifySipIngestionQueueIntegrity(queueSipIdPairs, queueSize);
	}
	
	/**
	 * Performs integrity checks on the modified SIP ingestion queue.
	 * @param queueSipIdPairs the SIP_ID - PARENT_SIP_ID pairs of the modified queue.
	 * @param queueSize the size of the modified queue.
	 */
	private void verifySipIngestionQueueIntegrity(Map<Long, Long> queueSipIdPairs, int queueSize) {
		// Verify that all SIP IDs appear only once in SIP_ID and PARENT_SIP_ID.
		Set<Long> uniqueSipIds = queueSipIdPairs.keySet();
		Set<Long> uniqueParentSipIds = new HashSet<>(queueSipIdPairs.values());
		if ((uniqueSipIds.size() != queueSize) || (uniqueParentSipIds.size() != queueSize)) {
			throw new InvalidSipIngestionQueueState("The SIP ingestion queue does not contain unique entries.");
		}
		// Verify that all SIP_IDs are non-null.
		if (uniqueSipIds.contains(null)) {
			throw new InvalidSipIngestionQueueState("The SIP ingestion queue contains entries having null SIP_IDs.");
		}
		// Verify that a queue head entry exists in the queue, and remove it.
		if (queueSize > 0 && !uniqueParentSipIds.remove(null)) {
			throw new InvalidSipIngestionQueueState("The SIP ingestion queue does not contain a head element.");
		}
		// Verify that the queue can be traversed.
		boolean allParentSipIdsExistAsSipIds = uniqueSipIds.containsAll(uniqueParentSipIds);		
		boolean selfPointingNodeExists = queueSipIdPairs.entrySet().stream()
												.anyMatch(entry -> entry.getKey().equals(entry.getValue()));
		if (!allParentSipIdsExistAsSipIds || selfPointingNodeExists) {
			throw new InvalidSipIngestionQueueState("The SIP ingestion queue cannot be traversed.");
		}
	}
	
	/**
	 * Resets the SIP ingestion queue by emptying PACKAGE_HAS_PARENT_PACKAGE and setting the status
	 * of all SCHEDULED packages to NEW. The packages that during the emptying of PHPP had status equal to INGESTING or FINISHED
	 * are re-inserted to PHPP with PARENT_SIP_ID equal to null, so that on the next run of prioritizePackagesOnInsert()
	 * PHPP can be fully rebuilt properly.
	 */
	private void resetSipIngestionQueue() {
		this.packageHasParentPackageDao.deleteAll();
		List<SIPPackage> scheduledIngestingFinishedPackages = this.sipPackageDao.findBySipStatusIn(Arrays.asList(SCHEDULED, INGESTING, FINISHED));
		for (SIPPackage sip : scheduledIngestingFinishedPackages) {
			if (sip.getStatus() != SCHEDULED) {
				PackageHasParentPackage phpp = new PackageHasParentPackage();
				phpp.setSipId(sip.getId());
				this.packageHasParentPackageDao.saveObject(phpp);
			}
			else {
				sip.setStatus(NEW);
			}
		}
	}
	
	/**
	 * Exception thrown when the SIP ingestion queue enters an invalid state.
	 */
	private class InvalidSipIngestionQueueState extends RuntimeException {
		private static final long serialVersionUID = 1L;

		InvalidSipIngestionQueueState(String errMsg) {
			super(errMsg);
		}
		
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public ReentrantLock getSipQueueLock() {
		return this.lock;
	}
	
}
