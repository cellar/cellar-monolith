/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.dao
 *             FILE : ExternalLinkDao.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 14, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao;

import java.util.List;

import eu.europa.ec.opoce.cellar.cl.domain.externalLink.ExternalLinkPattern;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 14, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface ExternalLinkPatternDao extends BaseDao<ExternalLinkPattern, Long> {

    List<ExternalLinkPattern> getExternalLinkPatterns();
}
