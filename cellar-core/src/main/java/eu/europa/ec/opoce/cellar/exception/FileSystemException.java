/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.exception
 *        FILE : FileSystemException.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 30-01-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.exception;

/**
 * This is a {@link FileSystemException} buildable by an {@link ExceptionBuilder}.</br>
 * It signals that a file Exception of some kind has occurred. This class is the general class
 * of exceptions produced by file system operations.</br>
 * </br>
 * ON : 30-01-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
public class FileSystemException extends CellarException {

    private static final long serialVersionUID = 1L;

    /**
     * Constructs a new exception with its associated builder.
     * 
     * @param builder the builder to use for building the exception
     */
    public FileSystemException(final ExceptionBuilder<? extends BuildableException> builder) {
        super(builder);
    }
}
