/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker
 *        FILE : RelationProdId.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 02-12-2021
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2021 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

/**
 * Domain object representing the {@code RELATION_PROD_ID} database table.
 * 
 * <b>NOTE:</b> The production identifiers stored in the {@code RELATION_PROD_ID} table
 * are only used as part of the mechanism performing prioritization of the
 * packages to be ingested.
 * @author EUROPEAN DYNAMICS S.A.
 */
@Entity
@Table(name = "RELATION_PROD_ID")
@NamedQuery(name = "RelationProdId.deleteBySipIdIn", query = "DELETE FROM RelationProdId WHERE sipPackage.id IN (:sipIds)")
public class RelationProdId {

	/**
	 * 
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "rel_prod_id_seq_gen")
	@SequenceGenerator(name = "rel_prod_id_seq_gen", sequenceName = "REL_PROD_ID_SEQ", allocationSize = 1)
	@Column(name = "ID", nullable = false, unique = true)
	private Long id;
	/**
	 * 
	 */
	@Column(name = "PRODUCTION_ID", nullable = false, length = 700)
	private String productionId;
	/**
	 * 
	 */
	@Column(name = "IS_DIRECT_RELATION_ONLY", nullable = false, length = 1)
	@Type(type = "yes_no")
	private boolean isDirectRelOnly;
	/**
	 * 
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "SIP_ID", nullable = false)
	private SIPPackage sipPackage;
	
	
	public RelationProdId() {
	}

	public RelationProdId(String productionId, boolean isDirectRelOnly) {
		this.productionId = productionId;
		this.isDirectRelOnly = isDirectRelOnly;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProductionId() {
		return productionId;
	}

	public void setProductionId(String productionId) {
		this.productionId = productionId;
	}

	public boolean isDirectRelOnly() {
		return isDirectRelOnly;
	}

	public void setDirectRelOnly(boolean isDirectRelOnly) {
		this.isDirectRelOnly = isDirectRelOnly;
	}

	public SIPPackage getSipPackage() {
		return sipPackage;
	}

	public void setSipPackage(SIPPackage sipPackage) {
		this.sipPackage = sipPackage;
	}

}
