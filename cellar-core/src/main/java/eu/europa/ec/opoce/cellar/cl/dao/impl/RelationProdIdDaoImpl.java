/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.dao.impl
 *        FILE : RelationProdIdDaoImpl.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 01-07-2023
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2023 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import eu.europa.ec.opoce.cellar.cl.dao.RelationProdIdDao;
import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.RelationProdId;
import eu.europa.ec.opoce.cellar.common.util.Counter;
import eu.europa.ec.opoce.cellar.common.util.IterableUtils;

/**
 * 
 * @author EUROPEAN DYNAMICS S.A.
 */
@Repository
public class RelationProdIdDaoImpl extends BaseDaoImpl<RelationProdId, Long> implements RelationProdIdDao {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int deleteBySipIdIn(Collection<Long> sipIds) {
		Map<String, Object> params = new HashMap<>();
		params.put("sipIds", sipIds);
		
		final Counter numDeleted = new Counter();
		IterableUtils.pageUp(params, this.cellarConfiguration.getCellarDatabaseInClauseParams(), innerParamMap -> 
			numDeleted.increaseBy(super.updateObjectsByNamedQueryWithNamedParams("RelationProdId.deleteBySipIdIn", innerParamMap))
		);
		return (int) numDeleted.getVal();
	}

}
