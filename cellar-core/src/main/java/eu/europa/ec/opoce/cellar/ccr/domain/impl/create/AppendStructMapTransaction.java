/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.ccr.domain.impl.create
 *        FILE : AppendStructMapTransaction.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 08-05-2013
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.domain.impl.create;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.ccr.domain.impl.TransactionConfiguration;
import eu.europa.ec.opoce.cellar.ccr.strategy.AppendStrategy;
import eu.europa.ec.opoce.cellar.cl.dao.ProductionIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.cl.domain.history.service.IdentifiersHistoryService;
import eu.europa.ec.opoce.cellar.cl.domain.response.AppendStructMapOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.StructMapOperation;
import eu.europa.ec.opoce.cellar.cl.service.client.CellarIdentifierService;
import eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.ingest.CalculatedData;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.List;

/**
 * <class_description> This class provides methods for the append process of a StructMap into database and S3.
 * Every digital objects (with their contents and metadatas) under a given StructMap will be created in database and S3.
 * If an error occurs during this process, the entire process is rolled back, in order that all digital objects (with metadata and content)
 * will be removed from the database and S3.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 08-05-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component("structMapAppend")
public class AppendStructMapTransaction extends AbstractCreateStructMapTransaction {

    private static final Logger LOGGER = LoggerFactory.getLogger(AppendStructMapTransaction.class);

    @Autowired
    @Qualifier("pidManagerService")
    private PidManagerService pidManagerService;

    @Autowired
    private ProductionIdentifierDao productionIdentifierDao;

    @Autowired
    private CellarResourceDao cellarResourceDao;

    @Autowired
    private IdentifiersHistoryService identifiersHistoryService;

    @Autowired
    private CellarIdentifierService cellarIdentifierService;

    /**
     * Instantiates a new append struct map transaction.
     */
    public AppendStructMapTransaction() {
        // load existing direct and inferred model from S3
        super(TransactionConfiguration.get(). //
                withLoadExistingDirectAndInferredModel(true) //
        );
    }

    /**
     * Create for all digital object, under this structmap in depth first traversal, the corresponding
     * cellar identifier in database and digital object into S3.
     * These new digital object will be linked to existing DO.
     *
     * @param calculatedData               the calculated data
     * @param metsDirectory                a reference to the directory where the SIP has been exploded
     * @param sipResource                  needed for audit
     * @param ingestedDigitalObjectPidList this list contains all pid which are already created
     */
    @Override
    protected void processHierarchy(final CalculatedData calculatedData, final File metsDirectory, final SIPResource sipResource,
                                    final List<String> ingestedDigitalObjectPidList) {
        final StructMap structMap = calculatedData.getStructMap();
        final String rootCellarId = calculatedData.getRootCellarId();

        //Throw an exception if the rootCellarId is null -> attempt to append to an unexisting target resource
        final ExceptionBuilder<CellarException> builder = ExceptionBuilder.get(CellarException.class);
        if (null == rootCellarId) {
            throw builder.withMessage("Cannot append to an unexisting work").withCode(CommonErrors.TARGET_RESOURCE_DOES_NOT_EXIST).build();
        }

        final AppendStrategy digitalObjectStrategy = new AppendStrategy(rootCellarId, calculatedData.getStructMap().getDigitalObject().getType(),
                sipResource, metsDirectory, pidManagerService, cellarResourceDao, productionIdentifierDao, contentStreamService, ingestedDigitalObjectPidList,
                identifiersHistoryService, cellarIdentifierService);

        structMap.getDigitalObject().applyStrategyOnSelfAndChildren(digitalObjectStrategy);
    }

    /**
     * New struct map operation.
     *
     * @return the struct map operation
     * @see eu.europa.ec.opoce.cellar.ccr.domain.impl.AbstractCreateStructMapTransaction#newStructMapOperation()
     */
    @Override
    protected StructMapOperation newStructMapOperation() {
        return new AppendStructMapOperation();
    }

}
