/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.licenseHolder
 *             FILE : ExtractionExecutionRunnable.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 7, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.licenseHolder;

import eu.europa.ec.opoce.cellar.cl.service.client.ExtractionExecutionService;
import eu.europa.ec.opoce.cellar.logging.LogContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.CMR_LICENSE_HOLDER;

/**
 * <class_description> Class that represents the task executor thread.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 7, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@LogContext
public class ExtractionExecutionRunnable implements Runnable {

    private static final Logger LOG = LogManager.getLogger(ExtractionExecutionRunnable.class);

    private final ExtractionExecution extractionExecution;
    private final ExtractionExecutionService extractionExecutionService;

    /**
     * Constructor.
     *
     * @param extractionExecution        the extraction execution concerned
     * @param extractionExecutionService the extraction execution service
     */
    public ExtractionExecutionRunnable(final ExtractionExecution extractionExecution,
                                       final ExtractionExecutionService extractionExecutionService) {
        this.extractionExecution = extractionExecution;
        this.extractionExecutionService = extractionExecutionService;
    }

    @Override
    @LogContext(CMR_LICENSE_HOLDER)
    public void run() {
        try {
            // runs
            extractionExecutionService.executeExtractionExecution(extractionExecution);
        } catch (Exception dataIntegrityViolationException) {
            LOG.info("Thread id " + Thread.currentThread().getId() + " - ExtractionExecution " + extractionExecution.getId()
                    + ": Cleaning " + extractionExecution.getArchiveTempDirectoryPath() + "; "
                    + extractionExecution.getArchiveFilePath() + "; " + extractionExecution.getSparqlTempFilePath());
            try {
                // cleans the execution
                extractionExecutionService.clean(extractionExecution, true);
            } catch (Exception exception) {
                LOG.error(exception.getMessage(), exception);
            }
        }
    }

}
