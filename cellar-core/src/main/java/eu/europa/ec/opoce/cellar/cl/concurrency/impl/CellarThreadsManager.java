/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.concurrency.impl
 *             FILE : CellarThreadsManager.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : May 3, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.concurrency.impl;

import java.util.concurrent.atomic.AtomicInteger;

import eu.europa.ec.opoce.cellar.cl.concurrency.CellarLockManagerType;
import eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLockSession;
import eu.europa.ec.opoce.cellar.cl.concurrency.ICellarThreadsManager;
import eu.europa.ec.opoce.cellar.domain.content.mets.OffIngestionOperationType;

/**
 * <class_description> Threads statistics manager implementation. 
 * <br/><br/>
 * ON : May 3, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class CellarThreadsManager implements ICellarThreadsManager {


    private final AtomicInteger indexingWaitingThreadsCount;

    private final AtomicInteger indexingWorkingThreadsCount;

    private final AtomicInteger nonIndexingWorkingThreadsCount;



    private final AtomicInteger nonIndexingWaitingThreadsCount;
    /**
     * Constructor.
     */
    public CellarThreadsManager() {
        this.indexingWorkingThreadsCount=new AtomicInteger();
        this.indexingWaitingThreadsCount=new AtomicInteger();
        this.nonIndexingWorkingThreadsCount=new AtomicInteger();
        this.nonIndexingWaitingThreadsCount=new AtomicInteger();
    }



    /**
     * {@inheritDoc}
     */
    @Override
    public void incWaitingThreadsCount(final ICellarLockSession cellarLockSession) {
        CellarLockManagerType cellarLockManagerType = (cellarLockSession.getLocker() == OffIngestionOperationType.INDEXING) ?
                CellarLockManagerType.INDEXING_ONLY : CellarLockManagerType.NON_INDEXING;
        if(cellarLockManagerType.equals(CellarLockManagerType.INDEXING_ONLY)) {
            indexingWaitingThreadsCount.incrementAndGet();
        }
        else{
            nonIndexingWaitingThreadsCount.incrementAndGet();
        }

    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void decWaitingThreadsCount(final ICellarLockSession cellarLockSession) {
        CellarLockManagerType cellarLockManagerType = (cellarLockSession.getLocker() == OffIngestionOperationType.INDEXING) ?
                CellarLockManagerType.INDEXING_ONLY : CellarLockManagerType.NON_INDEXING;
        if(cellarLockManagerType.equals(CellarLockManagerType.INDEXING_ONLY)) {
            indexingWaitingThreadsCount.decrementAndGet();
        }
        else{
            nonIndexingWaitingThreadsCount.decrementAndGet();
        }
    }



    /**
     * {@inheritDoc}
     */
    @Override
    public void incWorkingThreadsCount(final ICellarLockSession cellarLockSession) {
        cellarLockSession.setWorking(true);
        CellarLockManagerType cellarLockManagerType = (cellarLockSession.getLocker() == OffIngestionOperationType.INDEXING) ?
                CellarLockManagerType.INDEXING_ONLY : CellarLockManagerType.NON_INDEXING;
        if(cellarLockManagerType.equals(CellarLockManagerType.INDEXING_ONLY)) {
            indexingWorkingThreadsCount.incrementAndGet();
        }
        else{
            nonIndexingWorkingThreadsCount.incrementAndGet();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void decWorkingThreadsCount(final ICellarLockSession cellarLockSession) {
        cellarLockSession.setWorking(false);
        CellarLockManagerType cellarLockManagerType = (cellarLockSession.getLocker() == OffIngestionOperationType.INDEXING) ?
                CellarLockManagerType.INDEXING_ONLY : CellarLockManagerType.NON_INDEXING;
        if(cellarLockManagerType.equals(CellarLockManagerType.INDEXING_ONLY)) {
            indexingWorkingThreadsCount.decrementAndGet();
        }
        else{
            nonIndexingWorkingThreadsCount.decrementAndGet();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getIndexingWaitingThreadsCount() {
        return indexingWaitingThreadsCount.get();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getIndexingWorkingThreadsCount() {
        return indexingWorkingThreadsCount.get();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNonIndexingWorkingThreadsCount() {
        return nonIndexingWorkingThreadsCount.get();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getNonIndexingWaitingThreadsCount() {
        return nonIndexingWaitingThreadsCount.get();
    }
}
