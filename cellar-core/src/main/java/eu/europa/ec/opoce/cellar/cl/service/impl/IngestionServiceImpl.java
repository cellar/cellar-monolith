package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.MessageCode;
import eu.europa.ec.opoce.cellar.ccr.service.client.MetsResponseService;
import eu.europa.ec.opoce.cellar.ccr.service.client.MetsUploadService;
import eu.europa.ec.opoce.cellar.ccr.service.impl.MetsServiceFactory;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.dao.StructMapResponseDao;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditBuilder;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventAction;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventProcess;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventType;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.cl.domain.enums.ProcessMonitorPhase;
import eu.europa.ec.opoce.cellar.cl.domain.enums.ProcessStatus;
import eu.europa.ec.opoce.cellar.cl.domain.response.CreateStructMapOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.ResponseOperationType;
import eu.europa.ec.opoce.cellar.cl.domain.response.StructMapOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.StructMapResponse;
import eu.europa.ec.opoce.cellar.cl.domain.sip.SipMetsProcessing;
import eu.europa.ec.opoce.cellar.cl.domain.validator.SystemValidator;
import eu.europa.ec.opoce.cellar.cl.domain.validator.ValidationResult;
import eu.europa.ec.opoce.cellar.cl.service.IngestionService;
import eu.europa.ec.opoce.cellar.cl.service.client.*;
import eu.europa.ec.opoce.cellar.common.service.TestingThreadService;
import eu.europa.ec.opoce.cellar.common.util.FileSystemUtils;
import eu.europa.ec.opoce.cellar.core.exception.UncompressException;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsDocument;
import eu.europa.ec.opoce.cellar.domain.content.mets.OperationType;
import eu.europa.ec.opoce.cellar.exception.CellarConversionException;
import eu.europa.ec.opoce.cellar.exception.InvalidMetsFileException;
import eu.europa.ec.opoce.cellar.exception.MetadataValidationResultAwareException;
import eu.europa.ec.opoce.cellar.ingestion.SIPState;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork;
import eu.europa.ec.opoce.cellar.monitoring.Watch;
import eu.europa.ec.opoce.cellar.support.FileExtensionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.Date;

@Service
public class IngestionServiceImpl implements IngestionService {

    private static final Logger LOG = LogManager.getLogger(IngestionServiceImpl.class);

    @Autowired
    private FileService fileService;

    @Autowired
    private MetsAnalyzer metsAnalyzer;

    @Autowired
    private SystemValidator metsNameValidator;

    @Autowired
    private SystemValidator sipNameValidator;

    @Autowired
    private ValidationService validationService;

    @Autowired
    private SipMetsProcessingService sipMetsProcessingService;

    @Autowired
    private BackupService backupService;

    @Autowired
    private WorkflowService workflowService;

    @Autowired
    private MetsServiceFactory metsServiceFactory;

    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;
    
    @Autowired
    private PackageHistoryService packageHistoryService;
    
    @Autowired
    private StructMapStatusHistoryService structMapStatusHistoryService;
    
    @Autowired
    private TestingThreadService testingThreadService;

    @Autowired
    private MetsUploadService metsUploadService;

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.IngestionService#sipTreatment(eu.europa.ec.opoce.cellar.ingestion.SIPWork)
     */
    @Watch(value = "CMR Ingestion", arguments = {
            @Watch.WatchArgument(name = "type", expression = "sipWork.type")
    })
    @Override
    public void sipTreatment(final SIPWork sipWork) {
        //Log on console and database the start of process
        AuditBuilder.get(AuditTrailEventProcess.Ingestion).withAction(AuditTrailEventAction.Validate)
                .withType(AuditTrailEventType.Start).withObject(sipWork.getSipFile().getName())
                .withPackageHistory(sipWork.getPackageHistory())
                .withLogger(LOG).withLogDatabase(true).logEvent();
        
        // Check whether to pause thread execution for testing purposes
        if (this.cellarConfiguration.isTestEnabled()
                && this.cellarConfiguration.getTestProcessMonitorPhase().equals(ProcessMonitorPhase.Ingestion_Validate)) {
            this.testingThreadService.checkpointFreezingControl(ProcessMonitorPhase.Ingestion_Validate);
        }
        
        //date used in logging and timestamps
        final Date nowDate = new Date();

        //clears the log keys
        final File sipFile = sipWork.getSipFile();
        final String sipFileName = sipFile.getName();

        //builds sipresource object
        final SIPResource sipResource = new SIPResource();
        sipResource.setSipType(sipWork.getType());
        sipResource.setPriority(sipWork.getPriority());
        sipResource.setArchiveName(sipFileName);
        sipResource.setPackageHistory(sipWork.getPackageHistory());

        //creates sipmetsprocessing object
        final SipMetsProcessing sipMetsProcessing = new SipMetsProcessing();
        sipMetsProcessing.setSipProcessingStartDate(nowDate);
        sipMetsProcessing.setPriority(sipWork.getPriority());
        sipMetsProcessing.setSipName(sipFileName);
        sipMetsProcessing.setReceptionFolder(sipFile.getParent());
        //checks if there's already an entry for the same sip file in the sipmetsprocessing table
        final SipMetsProcessing foundSipMetsProcessing = this.sipMetsProcessingService.getBySipName(sipFileName);
        if (foundSipMetsProcessing == null) {
            //if not it creates a new entry
            this.sipMetsProcessingService.createSipMetsProcessing(sipMetsProcessing);
        } else {
            //if yes it retrieves the id to update it
            sipMetsProcessing.setId(foundSipMetsProcessing.getId());
            this.sipMetsProcessingService.updateSipMetsProcessing(sipMetsProcessing);
        }

        // handle decompression of sip file
        //defines the temporary working folder
        String metsFolder;
        try {
            try {
                //tries to uncompress file
                metsFolder = this.fileService.uncompressQuietly(sipFile);
            } catch (final UncompressException e) {
                // file is corrupted, probably because still being copied: let's wait and try to uncompress it once more
                final int retryDelay = this.cellarConfiguration.getCellarServiceIngestionSipCopyRetryPauseSeconds() * 1000;
                final int timeout = this.cellarConfiguration.getCellarServiceIngestionSipCopyTimeoutSeconds() * 1000;
                if (CoreErrors.E_012.equals(e.getCode())) {
                    FileSystemUtils.waitFileCompletionByChecksum(sipFile, retryDelay, timeout);
                    //2nd attempt to uncompress
                    metsFolder = this.fileService.uncompress(sipFile);
                } else if (CoreErrors.E_018.equals(e.getCode())) {
                    // file is locked, probably because still being copied: let's wait and try to uncompress it once more
                    FileSystemUtils.waitFileCompletionByLock(sipFile, retryDelay, timeout);
                    //2nd attempt to uncompress
                    metsFolder = this.fileService.uncompress(sipFile);
                } else {
                    // file is not being copied: let's throw the exception
                    throw e;
                }
            }
            sipResource.setFolderName(metsFolder);
            sipMetsProcessing.setMetsFolder(metsFolder);
        } catch (final Exception e) {
            final String exceptionMessage = appendExceptionCauseToMessage(e, "Could not uncompress SIP file [" + sipFileName + "] ");
            this.handleException(sipWork, sipMetsProcessing, sipResource, CoreErrors.E_012, e, exceptionMessage,
                    AuditTrailEventAction.Validate, true);
            return;
        }

        //retrieves the mets file from the temporary working folder
        File metsFile = null;
        try {
            metsFile = this.fileService.getMetsFileFromTemporaryWorkFolder(metsFolder);
        } catch (final Exception e) {
            final String exceptionMessage = appendExceptionCauseToMessage(e,
                    "MetsAnalyzer has failed to retrieve SIP [" + sipFileName + "] out ot the temporary folder.");
            this.handleException(sipWork, sipMetsProcessing, sipResource, CoreErrors.E_025, e, exceptionMessage,
                    AuditTrailEventAction.Validate, true);
            return;
        }

        //parses the mets file and retrieves the mets id
        String metsID = null;
        try {
            metsID = this.metsAnalyzer.getMetsIdFromTemporaryWorkFolder(metsFile);
            sipResource.setMetsID(metsID);
        } catch (final Exception e) {
            final String exceptionMessage = appendExceptionCauseToMessage(e,
                    "MetsAnalyzer has failed to retrieve the \"METS Identifier\" of SIP [" + sipFileName + "].");
            this.handleException(sipWork, sipMetsProcessing, sipResource, CoreErrors.E_025, e, exceptionMessage,
                    AuditTrailEventAction.Validate, true);
            return;
        }

        //parses a second time the mets file and retrieves the operation type
        OperationType type = null;
        try {
            type = this.metsAnalyzer.getMetsTypeFromTemporaryWorkFolder(metsFile);
            final String typeString = type.toString();
            sipMetsProcessing.setType(typeString);
            this.sipMetsProcessingService.updateSipMetsProcessing(sipMetsProcessing);
        } catch (final Exception e) {
            final String exceptionMessage = appendExceptionCauseToMessage(e,
                    "MetsAnalyzer has failed to retrieve the \"METS type\" of SIP [" + sipFileName + "].");
            this.handleException(sipWork, sipMetsProcessing, sipResource, CoreErrors.E_025, e, exceptionMessage,
                    AuditTrailEventAction.Validate, true);
            return;
        }

        //parses the document twice to
        //get details from the mets structmap for the sipmetsprocessing object
        try {
            final int structMapCount = this.metsAnalyzer.countMetsStructMap(metsFile);
            final int digitalObjectCount = this.metsAnalyzer.countMetsDigitalObject(metsFile);
            LOG.info("Package complexity: {} structmap(s) - {} digital object(s).", structMapCount, digitalObjectCount);
            sipMetsProcessing.setStructMapCount(structMapCount);
            sipMetsProcessing.setDigitalObjectCount(digitalObjectCount);
            this.sipMetsProcessingService.updateSipMetsProcessing(sipMetsProcessing);
        } catch (final Exception e) {
            final String exceptionMessage = appendExceptionCauseToMessage(e,
                    "MetsAnalyzer has failed to retrieve the count of structMap (or digital object) for SIP [" + sipFileName + "].");
            this.handleException(sipWork, sipMetsProcessing, sipResource, CoreErrors.E_025, e, exceptionMessage,
                    AuditTrailEventAction.Validate, true);
            return;
        }

        // parses the mets file again to get the metsHdr from the METS document
        try {
            final String metsHdr = this.metsAnalyzer.getMetsHdr(metsFile);
            sipResource.setMetsHdr(metsHdr);
        } catch (final Exception e) {
            final String exceptionMessage = appendExceptionCauseToMessage(e,
                    "MetsAnalyzer has failed to retrieve the \"METS header\" of SIP [" + sipFileName + "].");
            this.handleException(sipWork, sipMetsProcessing, sipResource, CoreErrors.E_025, e, exceptionMessage,
                    AuditTrailEventAction.Validate, false);
            // continue the process as this is not blocking
        }

        // handles the validation of SIP file - SIPNameValidator
        try {
            final ValidationResult sipNameValidatorResult = this.validationService.executeSystemValidator(sipFile, sipResource,
                    this.sipNameValidator);
            if (!sipNameValidatorResult.isValid()) {
                final String exceptionMessage = "SIP filename [" + sipFileName + "] is not valid. It should be  [" + metsID
                        + FileExtensionUtils.SIP + "] (based on METS Identifier).";
                this.handleException(sipWork, sipMetsProcessing, sipResource, CoreErrors.E_013, null,
                        exceptionMessage, AuditTrailEventAction.Validate, true);
                return;
            }
        } catch (final Exception e) {
            final String exceptionMessage = appendExceptionCauseToMessage(e,
                    "MetsAnalyzer has failed to validate the \"SIP filename\" of SIP [" + sipFileName + "].");
            this.handleException(sipWork, sipMetsProcessing, sipResource, CoreErrors.E_013, e, exceptionMessage,
                    AuditTrailEventAction.Validate, true);
            return;
        }

        // handles the validation of the METS file - MetsNameValidator
        try {
            final ValidationResult metsNameValidatorResults = this.validationService.executeSystemValidator(metsFile, sipResource,
                    this.metsNameValidator);
            if (!metsNameValidatorResults.isValid()) {
                final String name = metsFile.getName();
                final String exceptionMessage = "METS filename [" + name + "] is not valid. It should be  [" + metsID
                        + FileExtensionUtils.METS + "] (based on METS Identifier).";
                this.handleException(sipWork, sipMetsProcessing, sipResource, CoreErrors.E_023, null,
                        exceptionMessage, AuditTrailEventAction.Validate, true);
                return;
            }
        } catch (final Exception e) {
            final String exceptionMessage = appendExceptionCauseToMessage(e,
                    "MetsAnalyzer has failed to validate the \"METS name\" of SIP [" + sipFileName + "].");
            this.handleException(sipWork, sipMetsProcessing, sipResource, CoreErrors.E_023, e, exceptionMessage,
                    AuditTrailEventAction.Validate, true);
            return;
        }
        // log on console and database the end of validation
        AuditBuilder.get(AuditTrailEventProcess.Ingestion).withAction(AuditTrailEventAction.Validate)
                .withType(AuditTrailEventType.End).withDuration(System.currentTimeMillis() - nowDate.getTime())
                .withObject(sipWork.getSipFile().getName())
                .withPackageHistory(sipWork.getPackageHistory())
                .withLogger(LOG).withLogDatabase(true).logEvent();
        // Now process the Mets
        try {
            final WorkflowServiceOperationTypeVisitor visitor = new WorkflowServiceOperationTypeVisitor(sipResource, this.workflowService);
            type.accept(visitor, null);
        } catch (final MetadataValidationResultAwareException mvrae) {
            final String exceptionMessage = appendExceptionCauseToMessage(mvrae,
                    "Metadata in SIP [" + sipFileName + "] didn't pass the validation.");
            this.handleException(sipWork, sipMetsProcessing, sipResource, CoreErrors.E_000, mvrae, exceptionMessage,
                    AuditTrailEventAction.Validate, true);
            return;
        } catch (final InvalidMetsFileException e) {
            final String exceptionMessage = appendExceptionCauseToMessage(e,
                    "Not all linked files in METS are present in the SIP [" + sipFileName + "].");
            this.handleException(sipWork, sipMetsProcessing, sipResource, CoreErrors.E_000, e, exceptionMessage,
                    AuditTrailEventAction.Validate, true);
            return;
        } catch (final CellarConversionException e) {
            final String exceptionMessage = appendExceptionCauseToMessage(e,
                    "MetsAnalyzer has failed to convert the SIP [" + sipFileName + "] to a METS document.");
            // necessary to log it to the database, as it is a conversion error (== custom validation, custom transformation, or conversion)
            this.handleException(sipWork, sipMetsProcessing, sipResource, CoreErrors.E_000, e, exceptionMessage,
                    AuditTrailEventAction.Convert, true);
            return;
        } catch (final Exception e) {
            final String exceptionMessage = appendExceptionCauseToMessage(e,
                    "MetsAnalyzer has failed to process the METS file of SIP [" + sipFileName + "].");
            this.handleException(sipWork, sipMetsProcessing, sipResource, CoreErrors.E_040, e, exceptionMessage,
                    AuditTrailEventAction.Undefined, true);
            return;
        }



        // depending on the property cellar.service.backupProcessedFile.enabled
        // it will either move the sip file into the backup folder or delete it from the temporary work folder
        boolean success = backupService.backupSIPFile(sipFile);
        if (!success) {
            sipWork.setState(SIPState.SIP_FILE_REMOVE_FAILED); // non-blocking error, delay the file deletion
        }
        // depending on the property cellar.service.backupProcessedFile.enabled
        // it will move the mets folder or delete it from the temporary work folder
        final File parentFile = metsFile.getParentFile();
        this.backupService.backupMetsFolder(parentFile);
        // updates the sipmetsprocessing object
        this.sipMetsProcessingService.updateSipMetsProcessing(sipMetsProcessing);
    }

    private String appendExceptionCauseToMessage(final Exception e, String exceptionMessage) {
        if ((e.getCause() != null) && StringUtils.isNotBlank(e.getCause().getMessage())) {
            exceptionMessage += "Caused by [" + e.getCause().getMessage() + "]";
        }
        return exceptionMessage;
    }

    /**
     * Handle exception.
     *
     * @param sipWork           the sip work
     * @param sipMetsProcessing the sip mets processing
     * @param sipResource       the sip resource
     * @param code              the code
     * @param cause             the cause
     * @param exceptionMessage  the exception message
     * @param logDatabase       if true, log the audit trail on the database
     */
    private void handleException(final SIPWork sipWork, final SipMetsProcessing sipMetsProcessing,
                                 final SIPResource sipResource, final MessageCode code, final Exception cause,
                                 final String exceptionMessage, final AuditTrailEventAction action, final boolean logDatabase) {
        
        // Update status of PACKAGE_HISTORY entry and corresponding STRUCTMAP_STATUS_HISTORY entries to FAILED
        // PACKAGE_HISTORY: Set the PREPROCESS_INGESTION_STATUS to FAILED
        if (sipWork.getPackageHistory() != null){
            this.packageHistoryService.updatePackageHistoryStatus(sipWork.getPackageHistory(), ProcessStatus.F);
            // PACKAGE_HISTORY: Mark the INGESTION_STATUS of the corresponding STRUCTMAP_STATUS_HISTORY entries to FAILED
            this.structMapStatusHistoryService.updateAllStructMapIngestionStatus(sipWork.getPackageHistory(), ProcessStatus.F);
        }
        
        String metsID = getMetsId(sipResource);
        handleSipBackup(sipWork, sipResource);

        // log console and database
        AuditBuilder.get(AuditTrailEventProcess.Ingestion).withAction(action)
                .withType(AuditTrailEventType.Fail).withObject(sipWork.getSipFile().getName())
                .withPackageHistory(sipWork.getPackageHistory())
                .withCode(code)
                .withMessage(exceptionMessage)
                .withException(cause)
                .withLogger(LOG).withLogDatabase(logDatabase).logEvent();

        // creates the structmap object for the error response
        final StructMapResponse response = new StructMapResponse();
        response.setMetsDocumentId(metsID);

        // creates the response mets document object
        final MetsDocument metsDocument = new MetsDocument();
        metsDocument.setType(OperationType.Create);
        metsDocument.setDocumentId(metsID);
        final MetsResponseService metsResponseService = this.metsServiceFactory.getMetsResponseService(sipResource);

        // put "unknown" as a hardcoded string for every structMapId as there is no info extracted yet at this level
        for (int i = 0; i < sipMetsProcessing.getStructMapCount(); i++) {
            StructMapOperation operation = new CreateStructMapOperation();
            operation.setOperationType(ResponseOperationType.ERROR);
            response.getStructMapOperations().put("unknown", operation);
        }

        // creates the error response file that indicates the mets id directly in the response folder, named "metsid".response.xml
        final StructMapResponseDao structMapResponseDao = this.metsServiceFactory.getStructMapResponseDao(sipResource);
        structMapResponseDao.save(response);

        // creates the folder named "metsid" in the response folder and places the previously created error response file inside plus the log file containing all event trail records linked to the "metsid"
        final MetsDocument responseMetsDocument = metsResponseService.loadStructMapResponse(metsDocument, sipResource);
        // Send callback request if a callback UEI exists in PACKAGE_HISTORY entry (only for ingestions triggered by METS Upload)
        metsUploadService.getStatusResponseAndSendCallbackRequest(sipWork.getSipFile(), true, sipResource.getPackageHistory());
        // creates the "error" folder (if it does not exist) and zips the previously created response folder named "metsid" and moves it inside along with the "metsid".mets.xml file
        final SIPResource responseSipResource = metsResponseService.createResponse(responseMetsDocument, true);
        try {
            final String responseSipResourceArchiveName = responseSipResource.getArchiveName();
            // if by any reason the error response file does not have the same name as the sip package
            // it renames (by moving) the file to a package named like the sip package
            if (!StringUtils.equals(responseSipResourceArchiveName, sipResource.getArchiveName())) {
                final String responseSipResourceFolderName = responseSipResource.getFolderName();
                final File srcFile = new File(responseSipResourceFolderName, responseSipResourceArchiveName);
                final File destFile = new File(responseSipResourceFolderName, sipResource.getArchiveName());
                FileUtils.moveFile(srcFile, destFile);
            }
        } catch (final IOException e) {
            if (LOG.isErrorEnabled()) {
                LOG.error(e.getMessage(), e);
            }
        }

        // updates the sipprocessing object
        sipMetsProcessing.setSipProcessingEndDate(new Date());
        this.sipMetsProcessingService.updateSipMetsProcessing(sipMetsProcessing);
    }

    private void handleSipBackup(SIPWork sipWork, SIPResource sipResource) {
        try {
            // if the worker is being killed - or the system is being shutting down - while it is still treating its SIP,
            // the SIP has to be copied (and not moved) to the error folder, in order to keep a copy in the reception folder to be treated by the next alive worker
            // TODO: CELLAR-358
            // en: if (SIPWatcherInfo.INSTANCE.isKillingWorkers()) {
            // dv: not completed -> continue with S3 rollback problem

            //moves the SIP file into the error folder
            this.backupService.errorSIPFile(sipWork.getSipFile());
            final String folderName = sipResource.getFolderName();
            if (folderName != null) {//in case of a corrupted file there is no mets document folder
                //moves the mets folder into the error folder
                this.backupService.errorMetsFolder(folderName);
            }
        } catch (final Exception e) {
            if (LOG.isErrorEnabled()) {
                LOG.error(e.getMessage(), e);
            }
        }
    }

    private String getMetsId(SIPResource sipResource) {
        String metsID = null;
        // get the identifier: when it is not possible to parse the mets file we take the name of the file without the extension
        metsID = sipResource.getMetsID();
        if (metsID == null) {
            metsID = StringUtils.substringBefore(sipResource.getArchiveName(), ".");
            sipResource.setMetsID(metsID);
        }
        return metsID;
    }
}
