package eu.europa.ec.opoce.cellar.cl.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.springframework.stereotype.Service;

import eu.europa.ec.opoce.cellar.cl.exception.XslTransformationException;
import eu.europa.ec.opoce.cellar.cl.service.client.XslTransformerService;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

/**
 * Implementation of the {@link XslTransformerService}.
 * 
 * @author phvdveld
 */
@Service
public class XslTransformerServiceImpl implements XslTransformerService {

    /** Transformer factory for xsl processing. */
    private static final String TRANSFORMER_FACTORY = "javax.xml.transform.TransformerFactory";

    /** Saxon implementation of the transformer factory. */
    private static final String TRANSFORMER_FACTORY_IMPL = "net.sf.saxon.TransformerFactoryImpl";

    /** factory to generate xsl transformers. */
    private final transient TransformerFactory transformerFactory;

    /**
     * Default constructor.
     */
    public XslTransformerServiceImpl() {
        System.setProperty(TRANSFORMER_FACTORY, TRANSFORMER_FACTORY_IMPL);
        this.transformerFactory = TransformerFactory.newInstance();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void transform(final File xmlFile, final File xslFile, final File foxmlFile) throws XslTransformationException {
        this.transform(new StreamSource(xmlFile), new StreamSource(xslFile), foxmlFile);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void transform(InputStream xmlStream, InputStream xslStream, File foxmlFile) throws XslTransformationException {
        this.transform(new StreamSource(xmlStream), new StreamSource(xslStream), foxmlFile);
    }

    private void transform(StreamSource xmlStream, StreamSource xslStream, File foxmlFile) throws XslTransformationException {
        try(FileOutputStream fos = new FileOutputStream(foxmlFile);) {

            this.transform(xmlStream, xslStream, new StreamResult(fos));
        } catch (FileNotFoundException e) {
            throw ExceptionBuilder.get(XslTransformationException.class)
                    .withMessage("the result file does not exist " + foxmlFile.getPath()).withCause(e).build();
        } catch (IOException e) {
            throw ExceptionBuilder.get(XslTransformationException.class)
                    .withMessage("I/O error on the foxml result file " + foxmlFile.getPath()).withCause(e).build();
        }
    }

    private void transform(StreamSource xmlStream, StreamSource xslStream, StreamResult foxmlStream) throws XslTransformationException {
        try {
            Transformer transformer = transformerFactory.newTransformer(xslStream);
            transformer.transform(xmlStream, foxmlStream);
        } catch (TransformerConfigurationException e) {
            throw ExceptionBuilder.get(XslTransformationException.class)
                    .withMessage("error in the transformer configuration while creating a new transformer for the xsl stream").withCause(e)
                    .build();
        } catch (TransformerException e) {
            throw ExceptionBuilder.get(XslTransformationException.class).withMessage("error in the transformation").withCause(e).build();
        }
    }
}
