/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor
 *             FILE : ExportBatchJobProcessor.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 5, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor;

import eu.europa.ec.opoce.cellar.core.dao.FileDao;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob.BATCH_JOB_TYPE;
import eu.europa.ec.opoce.cellar.cl.service.export.IExportService;
import eu.europa.ec.opoce.cellar.logging.LogContext;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import java.io.File;

import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.CMR_METS_EXPORT;

/**
 * <class_description> Batch job export processor.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 5, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Configurable
@LogContext
public class ExportBatchJobProcessor extends AbstractBatchJobProcessor {

    /**
     * Logger instance.
     */
    private static final Logger LOG = LogManager.getLogger(ExportBatchJobProcessor.class);

    /**
     * The Constant DEFAULT_FOLDER_NAME_PATTERN.
     */
    public static final String DEFAULT_FOLDER_NAME_PATTERN = "export-${id}";

    /**
     * Production identifiers service.
     * <p>
     * /**
     * File manager.
     */
    @Autowired
    private FileDao fileDao;

    /**
     * The export service.
     */
    @Autowired
    private IExportService exportService;

    /**
     * Instantiates a new export batch job processor.
     *
     * @param batchJob the batch job
     */
    public ExportBatchJobProcessor(final BatchJob batchJob) {
        super(batchJob);
    }

    @Override
    @LogContext(CMR_METS_EXPORT)
    protected void doExecute(final String workId, final BatchJob batchJob) {
        try {

            final String defaultFolderName = getDefaultFolderName(batchJob.getId());

            final File temporaryFolder = new File(fileDao.getArchiveTemporaryFolderBatchJobExport(),
                    defaultFolderName);
            if (!temporaryFolder.exists()) {
                temporaryFolder.mkdirs();
            }

            try {
                final String destinationFolderName = StringUtils.isNotBlank(batchJob.getDestinationFolder())
                        ? batchJob.getDestinationFolder() : defaultFolderName;

                final File destinationFolder = new File(fileDao.getArchiveFolderBatchJobExport(),
                        destinationFolderName);
                if (!destinationFolder.exists()) {
                    destinationFolder.mkdirs();
                }

                final Boolean metadataOnly = batchJob.getMetadataOnly();
                final Boolean useAgnosticURL = batchJob.getUseAgnosticURL();

                exportService.export(workId, metadataOnly != null && metadataOnly, temporaryFolder,
                        useAgnosticURL != null && useAgnosticURL);

                for (final File f : temporaryFolder.listFiles()) {
                    FileUtils.moveToDirectory(f, destinationFolder, true);
                }
            } finally {
                if (temporaryFolder.delete()) {
                    LOG.info("Temporary folder has been successfully deleted {}", temporaryFolder);
                } else {
                    LOG.warn("Temporary folder cannot be deleted {}", temporaryFolder);
                }
            }

        } catch (final Exception exception) {
            LOG.error("Cannot export mets", exception);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected BATCH_JOB_TYPE getBatchJobType() {
        return BATCH_JOB_TYPE.EXPORT;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void preExecute() {
        this.batchJobService.updateProcessingBatchJob(this.batchJob);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void postExecute() {
        this.batchJobService.updateDoneBatchJob(this.batchJob);
    }

    /**
     * Gets the default folder name.
     *
     * @param batchJobId the batch job id
     * @return the default folder name
     */
    public static String getDefaultFolderName(final Long batchJobId) {
        return DEFAULT_FOLDER_NAME_PATTERN.replace("${id}", batchJobId.toString());
    }
}
