/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState
 *             FILE : SparqlCleaning.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 5, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState;

import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution.EXECUTION_STATUS;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.io.FileNotFoundException;

/**
 * <class_description> SPARQL cleaning logic.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 5, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component("sparqlCleaning")
public class SparqlCleaning extends AbstractCleaning {

    /**
     * Logger instance.
     */
    private static final Logger LOG = LogManager.getLogger(SparqlCleaning.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractTask execute(final ExtractionExecution extractionExecution) throws Exception {
        try {
            info(LOG, extractionExecution, "SPARQL cleaning %s",
                    super.licenseHolderService.getAbsoluteSparqlTempFilePath(extractionExecution));

            this.clean(extractionExecution, true);
            super.cleanError(extractionExecution);

            return this.next(extractionExecution);
        } catch (Exception exception) {
            this.error(extractionExecution, exception);
            error(LOG, extractionExecution, exception);
            throw exception;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractTask next(final ExtractionExecution extractionExecution) {
        extractionExecution.setExecutionStatus(EXECUTION_STATUS.SPARQL_EXECUTING);
        super.licenseHolderService.updateExtractionExecution(extractionExecution);

        return super.executionStateManager.getTaskState(EXECUTION_STATUS.SPARQL_EXECUTING);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void clean(final ExtractionExecution extractionExecution, final boolean cleanFinalArchive) throws Exception {
        final String absoluteSparqlTempFilePath = super.licenseHolderService.getAbsoluteSparqlTempFilePath(extractionExecution);
        if (absoluteSparqlTempFilePath != null) {
            try {
                super.cleanFile(absoluteSparqlTempFilePath);
            } catch (FileNotFoundException fileNotFoundException) {
                error(LOG, extractionExecution, fileNotFoundException);
            }
            extractionExecution.setSparqlTempFilePath(null);
        }
    }

    /**
     * Error.
     * @param extractionExecution the execution
     * @param exception the error description
     */
    protected void error(final ExtractionExecution extractionExecution, final Exception exception) {
        extractionExecution.setExecutionStatus(EXECUTION_STATUS.SPARQL_ERROR);
        super.error(extractionExecution, exception, false);
    }
}
