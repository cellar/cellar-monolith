/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.virtuoso.service.impl
 *             FILE : VirtuosoExceptionMessagesServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 18 août 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.virtuoso.service.impl;

import eu.europa.ec.opoce.cellar.cl.domain.virtuoso.VirtuosoExceptionMessages;
import eu.europa.ec.opoce.cellar.cl.domain.virtuoso.dao.VirtuosoExceptionMessagesDao;
import eu.europa.ec.opoce.cellar.cl.domain.virtuoso.service.VirtuosoExceptionMessagesService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * The Class VirtuosoExceptionMessagesServiceImpl.
 * <class_description> The service class to access the DAO of the virtuoso messages objects.
 * <br/><br/>
 * ON : 18 août 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
@Transactional
public class VirtuosoExceptionMessagesServiceImpl implements VirtuosoExceptionMessagesService {

    /** The virtuoso exception messages dao. */
    @Autowired
    private VirtuosoExceptionMessagesDao virtuosoExceptionMessagesDao;

    /** {@inheritDoc} */
    @Override
    public List<VirtuosoExceptionMessages> findAll() {
        final List<VirtuosoExceptionMessages> result = virtuosoExceptionMessagesDao.findAll();
        return result;
    }

    /** {@inheritDoc} */
    @Override
    public List<VirtuosoExceptionMessages> findByMessage(final String message) {
        final List<VirtuosoExceptionMessages> result = virtuosoExceptionMessagesDao.findByMessage(message);
        return result;
    }

}
