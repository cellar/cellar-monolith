package eu.europa.ec.opoce.cellar.cl.domain.aligner;

import eu.europa.ec.opoce.cellar.cl.configuration.enums.CellarServiceRDFStoreCleanerMode;
import eu.europa.ec.opoce.cellar.cl.enums.aligner.Repository;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

/**
 * The Class AlignerMisalignment.
 */
@Entity
@Table(name = "ALIGNER_MISALIGNMENT")
public class AlignerMisalignment implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 760800388896090616L;

    /** The id. */
    private long id;

    /** The operation type. 
     * During which operation (diagnostic/fix) the misalignment has been detected*/
    private CellarServiceRDFStoreCleanerMode operationType;

    /** The repository.
     * The repository in which the resource should/has been removed */
    private Repository repository;

    /** The cellar id.
     * The concerned CELLAR resource */
    private String cellarId;

    /** The operation date.
     * The execution date */
    private Date operationDate;

    /**
     * Gets the id.
     *
     * @return the id
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    public long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(final long id) {
        this.id = id;
    }

    /**
     * Gets the operation type.
     * 
     * @return the operation type
     */
    @Column(name = "OPERATION_TYPE")
    public CellarServiceRDFStoreCleanerMode getOperationType() {
        return this.operationType;
    }

    /**
     * Sets the operation type.
     *
     * @param operationType the new operation type
     */
    public void setOperationType(final CellarServiceRDFStoreCleanerMode operationType) {
        this.operationType = operationType;
    }

    /**
     * Gets the repository.
     * 
     * @return the repository
     */
    @Column(name = "REPOSITORY")
    public Repository getRepository() {
        return this.repository;
    }

    /**
     * Sets the repository.
     *
     * @param repository the new repository
     */
    public void setRepository(final Repository repository) {
        this.repository = repository;
    }

    /**
     * Gets the cellar id.
     *
     * @return the cellar id
     */
    @Column(name = "CELLAR_ID")
    public String getCellarId() {
        return this.cellarId;
    }

    /**
     * Sets the cellar id.
     *
     * @param cellarId the new cellar id
     */
    public void setCellarId(final String cellarId) {
        this.cellarId = cellarId;
    }

    /**
     * Gets the operation date.
     *
     * @return the operation date
     */
    @Column(name = "OPERATION_DATE")
    public Date getOperationDate() {
        return this.operationDate;
    }

    /**
     * Sets the operation date.
     *
     * @param operationDate the new operation date
     */
    public void setOperationDate(final Date operationDate) {
        this.operationDate = operationDate;
    }

    /** {@inheritDoc} */
    @Override
    public final String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

}
