/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *        FILE : UserAccessRequest.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 22-02-2023
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2023 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.admin;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * <class_description> Class that represents a user access request.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 22, 2023
 *
 * @author EUROPEAN DYNAMICS S.A.
 */

@Entity
@Table(name = "USER_ACCESS_REQUEST", uniqueConstraints = @UniqueConstraint(columnNames = {
        "USERNAME", "EMAIL"}) )
@NamedQueries({
	@NamedQuery(name = "UserAccessRequest.findByUsername", query = "SELECT uar FROM UserAccessRequest uar WHERE uar.username = :username")
})
public class UserAccessRequest {
	
	/**
     * Internal id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    
    /**
     * The user name.
     */
    @Column(name = "USERNAME", nullable = false)
    private String username;
    
    /**
     * The e-mail address.
     */
    @Column(name = "EMAIL", nullable = false)
    private String email;
    
    
    /**
     * Default constructor.
     */
    public UserAccessRequest() {

    }
    
    /**
     * Constructor.
     * @param id the identifier of the user access request.
     */
    public UserAccessRequest(final Long id) {
        this.id = id;
    }

    /**
     * @return the id
     */
	public Long getId() {
		return id;
	}

	/**
     * @param id the id to set
     */
	public void setId(Long id) {
		this.id = id;
	}

	/**
     * @return the username
     */
	public String getUsername() {
		return username;
	}

	/**
     * @param username the username to set
     */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
     * @return the email
     */
	public String getEmail() {
		return email;
	}

	/**
     * @param email the e-mail to set
     */
	public void setEmail(String email) {
		this.email = email;
	}

}
