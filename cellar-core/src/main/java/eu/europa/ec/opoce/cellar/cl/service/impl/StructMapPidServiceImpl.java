package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.cl.dao.StructMapPidDao;
import eu.europa.ec.opoce.cellar.cl.domain.structmap_pid.StructMapPid;
import eu.europa.ec.opoce.cellar.cl.service.client.StructMapPidService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

/**
 * <class_description> Service for logging entries in the 'STRUCTMAP_PID' table.
 *
 * ON : 16-06-2023
 *
 * @author EUROPEAN DYNAMICS S.A.
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class StructMapPidServiceImpl implements StructMapPidService {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(StructMapPidServiceImpl.class);
    
    private final StructMapPidDao structMapPidDao;
    
    @Autowired
    public StructMapPidServiceImpl(StructMapPidDao structMapPidDao) {
        this.structMapPidDao = structMapPidDao;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public StructMapPid saveEntry(StructMapPid structMapPid) {
        try{
            this.structMapPidDao.saveObject(structMapPid);
            return structMapPid;
        }
        catch (final Exception e) {
            // Log error message
            LOGGER.error("An exception occurred while saving StructMapPid '" + structMapPid + "'.", e);
            // Mark this event transaction with rollback: this allows us not to throw the exception up and handle the rollback here
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return null;
        }
    }
}
