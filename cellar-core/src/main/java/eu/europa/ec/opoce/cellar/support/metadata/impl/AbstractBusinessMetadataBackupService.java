/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.support.metadata.impl
 *             FILE : AbstractBusinessMetadataBackupService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 17-05-2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.support.metadata.impl;

import eu.europa.ec.opoce.cellar.ccr.helper.ResponseHelper;
import eu.europa.ec.opoce.cellar.cl.domain.response.DigitalObjectOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.IOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.ResponseOperation;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.common.metadata.IBusinessMetadataBackupService;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;

import java.util.List;

/**
 * <class_description> The base implementation of {@link IBusinessMetadataBackupService}.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 17-05-2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public abstract class AbstractBusinessMetadataBackupService implements IBusinessMetadataBackupService<StructMap, IOperation<DigitalObjectOperation>> {

    private final ContentStreamService contentStreamService;

    protected AbstractBusinessMetadataBackupService(ContentStreamService contentStreamService) {
        this.contentStreamService = contentStreamService;
    }

    protected ResponseOperation setVersions(final ContentIdentifier contentIdentifier, final ContentType contentType,
                                            final ResponseOperation response) {
        return ResponseHelper.setVersions(contentIdentifier, contentType, response);
    }

    protected static ContentType reduce(List<ContentType> types) {
        if (types.contains(ContentType.DIRECT)) {
            return ContentType.DIRECT;
        } else if (!types.isEmpty()) {
            return types.get(0);
        }
        return ContentType.DIRECT;
    }

}
