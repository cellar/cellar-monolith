/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor.export
 *             FILE : AbstractMetsElementExport.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 24, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.export.exporter.impl;

import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cl.service.export.exporter.IMetsElementExporter;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.SnippetExtractorService;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement;
import eu.europa.ec.opoce.cellar.mets.v2.builder.IContentIdAware;
import eu.europa.ec.opoce.cellar.mets.v2.builder.IDivFptrAware;
import eu.europa.ec.opoce.cellar.mets.v2.builder.MetsBuilder;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import org.apache.jena.rdf.model.Model;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;

/**
 * <class_description> Abstract METS element exporter.
 * <p>
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 24, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public abstract class AbstractMetsElementExporter implements IMetsElementExporter {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractMetsElementExporter.class);

    @Autowired
    @Qualifier("pidManagerService")
    protected IdentifierService identifierService;

    @Autowired
    protected ContentStreamService contentStreamService;

    @Autowired
    protected SnippetExtractorService snippetExtractorService;

    /**
     * Mets element cellar id corresponding to this export session.
     */
    private String metsElementCellarId;
    private Map<ContentType, String> versions;

    public AbstractMetsElementExporter(MetsElement element) {
        this.metsElementCellarId = element.getCellarId().getIdentifier();
        this.versions = element.getVersions();
    }

    /**
     * Do export mets element.
     *
     * @param metsBuilder        the mets builder
     * @param parentDivFptrAware the parent div fptr aware
     * @param metsElement        the mets element
     * @param zos                the zos
     * @throws IOException           Signals that an I/O exception has occurred.
     */
    protected abstract void doExportMetsElement(final MetsBuilder metsBuilder, final IDivFptrAware<?> parentDivFptrAware,
                                                final MetsElement metsElement, final ZipOutputStream zos) throws IOException;

    /**
     * Do export mets element.
     *
     * @param model              the model
     * @param metsBuilder        the mets builder
     * @param parentDivFptrAware the parent div fptr aware
     * @param metsElement        the mets element
     * @param zos                the zos
     * @throws IOException           Signals that an I/O exception has occurred.
     */
    protected abstract void doExportMetsElement(final Model model, final MetsBuilder metsBuilder, final IDivFptrAware<?> parentDivFptrAware,
                                                final MetsElement metsElement, final ZipOutputStream zos) throws IOException;

    /**
     * Do create model.
     *
     * @param model       the model
     * @param metsElement the mets element
     */
    protected abstract void doCreateModel(final Model model, final MetsElement metsElement);

    /**
     * Gets the content id aware object corresponding to this export session.
     *
     * @return the content id aware object
     */
    protected abstract IContentIdAware<?> getContentIdAware();

    @Override
    public void exportMetsElement(final MetsBuilder metsBuilder, final IDivFptrAware<?> parentDivFptrAware, final MetsElement metsElement,
                                  final ZipOutputStream zos) throws IOException {

        this.getContentIdAware(metsElement);

        this.doExportMetsElement(metsBuilder, parentDivFptrAware, metsElement, zos);
    }

    @Override
    public void exportMetsElement(final Model model, final MetsBuilder metsBuilder, final IDivFptrAware<?> parentDivFptrAware,
                                  final MetsElement metsElement, final ZipOutputStream zos) throws IOException {

        this.getContentIdAware(metsElement);

        this.doExportMetsElement(model, metsBuilder, parentDivFptrAware, metsElement, zos);
    }

    private void getContentIdAware(final MetsElement metsElement) {
        this.metsElementCellarId = metsElement.getCellarId().getIdentifier();

        LOGGER.info("Mets element cellar id corresponding to this export session is '{}'", this.metsElementCellarId);

        final List<String> pids = this.identifierService.getIdentifier(this.metsElementCellarId).getPids();
        for (final String pid : pids) {
            this.getContentIdAware().withCONTENTID(pid); // pids may be encoded
        }

        if (!pids.contains(this.metsElementCellarId)) {
            this.getContentIdAware().withCONTENTID(this.metsElementCellarId);
        }
    }

    @Override
    public void createModel(final Model model, final MetsElement metsElement) {
        this.metsElementCellarId = metsElement.getCellarId().getIdentifier();
        this.versions = metsElement.getVersions();
        doCreateModel(model, metsElement);
    }

    protected String getCellarId() {
        return metsElementCellarId;
    }

    protected Map<ContentType, String> getVersions() {
        return versions;
    }
}
