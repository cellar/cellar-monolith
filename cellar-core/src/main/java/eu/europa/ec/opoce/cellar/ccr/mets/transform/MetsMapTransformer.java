package eu.europa.ec.opoce.cellar.ccr.mets.transform;

import eu.europa.ec.opoce.cellar.common.closure.RPClosure;
import eu.europa.ec.opoce.cellar.common.transformer.MapTransformer;
import eu.europa.ec.opoce.cellar.common.util.IterableUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentStream;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsDocument;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import eu.europa.ec.opoce.cellar.support.DigitalObjectIterator;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Builds the map of data used by velocity to fill the mets template.
 *
 * @author phvdveld
 */
public class MetsMapTransformer implements MapTransformer<MetsDocument> {

    @Override
    public Map<String, Object> transform(MetsDocument metsDocument) {
        Map<String, Object> map = new HashMap<>();
        putMetsHeader(map, metsDocument);

        for (StructMap structMap : metsDocument.getStructMaps().values()) {
            putStructMapSection(map, structMap);
            putTechnicalMetadataSection(map, structMap);
            putBusinessMetadataSection(map, structMap);
            putFileReferenceSection(map, structMap);
        }
        return map;
    }

    private void putMetsHeader(Map<String, Object> map, MetsDocument metsDocument) {
        map.put("type", metsDocument.getType());
        if (metsDocument.getCreateDate() == null) {
            metsDocument.setCreateDate(new Date());
        }
        map.put("header", metsDocument.getHeader());
    }

    private void putStructMapSection(Map<String, Object> map, StructMap structMap) {
        append(map, "structMap", makeSectionMap(structMap.getId(), structMap.getDigitalObject()));
    }

    private void putTechnicalMetadataSection(Map<String, Object> map, StructMap structMap) {
        Iterable<DigitalObject> iter = IterableUtils.getEach(new DigitalObjectIterator(structMap), TechnicalMetadataAccessor);
        if (iter.iterator().hasNext()) {
            append(map, "amdSec", makeSectionMap("amd_" + structMap.getId(), iter));
        }
    }

    private void putBusinessMetadataSection(Map<String, Object> map, StructMap structMap) {
        Iterable<DigitalObject> iter = IterableUtils.getFirst(new DigitalObjectIterator(structMap), BusinessMetadataAccessor);
        if (iter.iterator().hasNext()) {
            append(map, "dmdSec", makeSectionMap("dmd_" + structMap.getId(), iter));
        }
    }

    private void putFileReferenceSection(Map<String, Object> map, StructMap structMap) {
        Iterable<Object> iter = IterableUtils.filter(new DigitalObjectIterator(structMap), DigitalObjectFileReferenceAccessor,
                HasContentStreamFilter);
        if (iter.iterator().hasNext()) {
            append(map, "fileSec", makeSectionMap("file_" + structMap.getId(), iter));
        }
    }

    /**
     * Appends the given value to the list of values contained in the provided map.
     *
     * @param map   the map at which to append the value
     * @param key   the key at which to append the value
     * @param value the value to append
     */
    @SuppressWarnings("unchecked")
    private void append(Map<String, Object> map, String key, Map<String, Object> value) {
        List<Object> listToAppend;
        if (map.containsKey(key)) {
            listToAppend = (List<Object>) map.get(key);
        } else {
            listToAppend = new LinkedList<>();
        }
        listToAppend.add(value);
        map.put(key, listToAppend);
    }

    /**
     * Makes a section map containing an id an data associated to it.
     *
     * @param id   the id to give to the section
     * @param data the data to associate to the id
     * @return a {@link Map}
     */
    private static Map<String, Object> makeSectionMap(String id, Object data) {
        Map<String, Object> section = new HashMap<>();
        section.put("id", id);
        section.put("data", data);
        return section;
    }

    /**
     * Builds a content stream map from a ContentStream object.
     */
    public static final MapTransformer<ContentStream> ContentStreamMapBuilder = new MapTransformer<ContentStream>() {

        /**
         * Builds a content stream map from a ContentStream object.
         * @param contentStream the content stream
         * @return a velocity formatted map
         */
        @Override
        public Map<String, Object> transform(ContentStream contentStream) {
            Map<String, Object> map = new HashMap<>();
            map.put("id", contentStream.hashCode());
            map.put("fileRef", contentStream.getFileRef());
            map.put("mimeType", contentStream.getMimeType());
            return map;
        }
    };

    /**
     * Acces the content streams of a digital object.
     */
    public static final RPClosure<Object, DigitalObject> DigitalObjectFileReferenceAccessor =
            digitalObject -> IterableUtils.convertEach(digitalObject.getContentStreams(), ContentStreamMapBuilder);

    /**
     * Access the technical metadata of a digital object.
     */
    public static final RPClosure<Object, DigitalObject> TechnicalMetadataAccessor = DigitalObject::getTechnicalMetadata;

    /**
     * Access the business metadata of a digital object.
     */
    public static final RPClosure<Object, DigitalObject> BusinessMetadataAccessor = DigitalObject::getBusinessMetadata;

    /**
     * filters digital objects that have content streams.
     */
    public static final RPClosure<Boolean, DigitalObject> HasContentStreamFilter = digitalObject -> digitalObject.getContentStreams().size() > 0;
}
