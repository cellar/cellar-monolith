package eu.europa.ec.opoce.cellar.ccr.service.impl;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import eu.europa.ec.opoce.cellar.ccr.service.client.MetsResponseService;
import eu.europa.ec.opoce.cellar.ccr.velocity.VelocityMarshaller;
import eu.europa.ec.opoce.cellar.cl.dao.StructMapResponseDao;
import eu.europa.ec.opoce.cellar.cl.domain.validator.SystemValidator;
import eu.europa.ec.opoce.cellar.cl.service.client.AuditTrailEventService;
import eu.europa.ec.opoce.cellar.cl.service.client.FileService;
import eu.europa.ec.opoce.cellar.cl.service.client.ValidationService;

/**
 * <class_description> Implementation of the {@link MetsResponseService} for the bulk ingestions.<br/>
 * Service to handle the creation of responses in xml mets format and generating SIPResource archives with the generated files.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 */
@Service("metsBulkLowPriorityResponseService")
public class MetsBulkLowPriorityResponseService extends AbstractMetsResponseService {

    private FileService fileService;

    private StructMapResponseDao responseDao;

    @Autowired
    public MetsBulkLowPriorityResponseService(FileService fileService, @Qualifier("structMapBulkLowPriorityResponseDao") StructMapResponseDao responseDao,
                                   @Qualifier("metsMarshaller") VelocityMarshaller metsMarshaller,
                                   @Qualifier("metsResponseValidator") SystemValidator metsResponseValidator,
                                   AuditTrailEventService auditTrailEventService, ValidationService validationService) {
        super(metsMarshaller, metsResponseValidator, auditTrailEventService, validationService);
        this.fileService = fileService;
        this.responseDao = responseDao;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.ccr.service.impl.AbstractMetsResponseService#resolveResponseFolder()
     */
    @Override
    protected File resolveResponseFolder() {
        return this.fileService.getResponseBulkLowPriorityFolder();
    }

    /**
     * @see eu.europa.ec.opoce.cellar.ccr.service.impl.AbstractMetsResponseService#resolveResponseDao()
     */
    @Override
    protected StructMapResponseDao resolveResponseDao() {
        return this.responseDao;
    }
    
}
