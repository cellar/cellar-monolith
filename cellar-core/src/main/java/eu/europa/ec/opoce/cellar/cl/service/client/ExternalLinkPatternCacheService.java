/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.client
 *             FILE : ExternalLinkCacheService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 14, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.client;

import java.util.LinkedList;
import java.util.List;

import eu.europa.ec.opoce.cellar.cl.domain.externalLink.ExternalLinkPattern;
import eu.europa.ec.opoce.cellar.common.Placeholder;

/**
 * <class_description> External link patterns cache manager.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 14, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface ExternalLinkPatternCacheService {

    /**
     * Gets the list of all the external link patterns.
     * @return the list of all the external link patterns
     */
    List<ExternalLinkPattern> getExternalLinkList();

    /**
     * Gets the list of placeholders corresponding to the instance of {@link ExternalLinkPattern}.
     * @param externalLink the pattern that contains the URL pattern
     * @return the list of placeholders
     */
    LinkedList<Placeholder> getExternalLinkPlaceholders(final ExternalLinkPattern externalLink);
}
