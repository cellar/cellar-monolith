/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.aligner.impl
 *             FILE : AbstractHierarchyLoaderServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Aug 22, 2014
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2014 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.aligner.impl;

import eu.europa.ec.opoce.cellar.cl.concurrency.IConcurrentArgsResolver;
import eu.europa.ec.opoce.cellar.cl.dao.CellarIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.domain.aligner.Hierarchy;
import eu.europa.ec.opoce.cellar.cl.domain.aligner.HierarchyNode;
import eu.europa.ec.opoce.cellar.cl.enums.aligner.Repository;
import eu.europa.ec.opoce.cellar.cl.service.aligner.impl.delegator.IHierarchyLoaderDelegator;
import eu.europa.ec.opoce.cellar.cl.service.aligner.impl.delegator.IdentifierHierarchyLoaderDelegator;
import eu.europa.ec.opoce.cellar.cl.service.aligner.impl.delegator.ResourceHierarchyLoaderDelegator;
import eu.europa.ec.opoce.cellar.cl.service.aligner.impl.delegator.StringHierarchyLoaderDelegator;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.common.util.CellarIdUtils;
import eu.europa.ec.opoce.cellar.database.CmrTableName;
import eu.europa.ec.opoce.cellar.database.IRDFStoreRelationalGateway;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Aug 22, 2014
 *
 * @author ARHS Developments
 * @version $Revision$
 */
abstract class AbstractHierarchyLoaderServiceImpl implements IConcurrentArgsResolver {

    /**
     * Production identifiers service.
     */
    @Autowired
    @Qualifier("pidManagerService")
    protected IdentifierService identifierService;

    /**
     * The cellar resource dao.
     */
    @Autowired
    protected CellarResourceDao cellarResourceDao;

    /**
     * The cellar identifier dao.
     */
    @Autowired
    protected CellarIdentifierDao cellarIdentifierDao;

    @Autowired
    protected ContentStreamService contentStreamService;

    /**
     * The cluster triple db gateway.
     */
    @Autowired
    private IRDFStoreRelationalGateway clusterTripleDbGateway;

    /**
     * Resolve args for concurrency.
     *
     * @param in the in
     * @return the object[]
     * @see eu.europa.ec.opoce.cellar.cl.concurrency.IConcurrentArgsResolver#resolveArgsForConcurrency(java.lang.Object[])
     */
    @Override
    public Object[] resolveArgsForConcurrency(final Object[] in) {
        final String rootCellarId = this.getRootCellarId((String) in[0]);
        return new Object[]{
                rootCellarId};
    }

    /**
     * Gets the root cellar id.
     *
     * @param productionId the production id
     * @return the root cellar id
     */
    protected String getRootCellarId(final String productionId) {
        final String cellarId = this.identifierService.getCellarPrefixed(productionId);
        return CellarIdUtils.getCellarIdBase(cellarId);
    }

    /**
     * Load hierarchy.
     *
     * @param rootCellarId the root cellar id
     * @return the hierarchy
     */
    protected Hierarchy loadHierarchy(final String rootCellarId) {
        final Hierarchy hierarchy = new Hierarchy(rootCellarId);

        final CellarResource findCellarId = this.cellarResourceDao.findCellarId(rootCellarId);
        if (findCellarId != null) {
            final Date embargoDate = findCellarId.getEmbargoDate();
            if ((embargoDate != null) && embargoDate.after(new Date())) {
                hierarchy.setEmbargoDate(embargoDate);
            }
        }

        loadHierarchy(hierarchy, hierarchy.getCellarId(), new ResourceHierarchyLoaderDelegator(this.cellarResourceDao),
                Repository.CMR_CELLAR_RESOURCE_MD);

        final StringHierarchyLoaderDelegator wemS3HierarchyLoader = this.getWEMS3HierarchyLoader();
        loadHierarchy(hierarchy, hierarchy.getCellarId(), wemS3HierarchyLoader, Repository.S3);

        loadHierarchy(hierarchy, hierarchy.getCellarId(), new IdentifierHierarchyLoaderDelegator(this.cellarIdentifierDao),
                Repository.CELLAR_IDENTIFIER);

        loadHierarchy(hierarchy, hierarchy.getCellarId(), this.getRDFStoreHierarchyLoader(), Repository.RDF_STORE);

        loadHierarchy(hierarchy, hierarchy.getCellarId(), this.getCmrInverseDissHierarchyLoader(), Repository.CMR_INVERSE_DISS);

        setEmbargoDateInHierarchyElements(hierarchy, rootCellarId);
        
        return hierarchy;
    }

    /**
     * Gets the WEM S3 hierarchy loader.
     *
     * @return the WEM S3 hierarchy loader
     */
    private StringHierarchyLoaderDelegator getWEMS3HierarchyLoader() {
        return new StringHierarchyLoaderDelegator() {

            final Set<String> manifestations = new HashSet<>();

            @Override
            public Collection<String> loadResources(final String key) {
                return contentStreamService.listKeyStartWith(key).stream()
                        .map(k -> keepKey(k, ContentType.values()))
                        .map(k -> k.replaceAll("/", "."))
                        .map(k -> k.replaceAll(".DOC", "/DOC"))
                        .map(k -> "cellar:" + k)
                        .collect(Collectors.toSet());
            }

            private String keepKey(String key, ContentType[] values) {
                for (ContentType type : values) {
                    if (key.endsWith(type.file())) {
                        return key.replace("/" + type.file(), "");
                    }
                }
                return key;
            }

            @Override
            public void select(final HierarchyNode node, final String obj) {
                if (node.getType() == DigitalObjectType.MANIFESTATION) {
                    this.manifestations.add(obj);
                }
            }

            @Override
            public Collection<String> getSelectedResources() {
                return this.manifestations;
            }

        };
    }

    /**
     * Gets the RDF store hierarchy loader.
     *
     * @return the RDF store hierarchy loader
     */
    private StringHierarchyLoaderDelegator getRDFStoreHierarchyLoader() {
        return new StringHierarchyLoaderDelegator() {

            @Override
            public Collection<String> loadResources(final String key) {
                final Set<String> resources = new HashSet<>();

                resources.addAll(clusterTripleDbGateway
                        .selectCompleteContextNames(CmrTableName.CMR_INVERSE, key));
                resources.addAll(clusterTripleDbGateway
                        .selectCompleteContextNames(CmrTableName.CMR_INVERSE_EMBARGO, key));
                resources.addAll(clusterTripleDbGateway
                        .selectCompleteContextNames(CmrTableName.CMR_METADATA, key));
                resources.addAll(clusterTripleDbGateway
                        .selectCompleteContextNames(CmrTableName.CMR_METADATA_EMBARGO, key));

                return resources;
            }
        };
    }

    /**
     * Gets the cmr inverse diss hierarchy loader.
     *
     * @return the cmr inverse diss hierarchy loader
     */
    private StringHierarchyLoaderDelegator getCmrInverseDissHierarchyLoader() {
        return new StringHierarchyLoaderDelegator() {

            @Override
            public Collection<String> loadResources(final String key) {
                return clusterTripleDbGateway.selectCompleteContextNames(CmrTableName.CMR_INVERSE_DISS, key);
            }
        };
    }

    /**
     * Load hierarchy.
     *
     * @param <T>        the generic type
     * @param hierarchy  the hierarchy
     * @param key        the key
     * @param loader     the loader
     * @param repository the repository
     */
    private static <T> void loadHierarchy(final Hierarchy hierarchy, final String key, final IHierarchyLoaderDelegator<T> loader,
                                          final Repository repository) {
        HierarchyNode node = null;

        for (final T t : loader.loadResources(key)) {
            node = hierarchy.getHierarchyNodeInstance(loader.getCellarId(t), loader.getDigitalObjectType(t));
            node.addRepository(repository);
            loader.select(node, t);
        }
    }
    
    /**
     * Sets the embargo date of the hierarchy elements, when applicable.
     * 
     * @param hierarchy the element hierarchy.
     * @param rootCellarId the root Cellar Id of the hierarchy.
     */
    private void setEmbargoDateInHierarchyElements(final Hierarchy hierarchy, final String rootCellarId) {
    	Collection<CellarResource> resources = this.cellarResourceDao.findTree(rootCellarId);
    	for (CellarResource resource : resources) {
    		hierarchy.getNodes().get(resource.getCellarId()).setEmbargoDate(resource.getEmbargoDate());
    	}
    }
    
}
