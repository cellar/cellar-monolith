package eu.europa.ec.opoce.cellar.support.sip;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;

/**
 * The Class DailySIPFolderType.
 */
public class DailySIPFolderType extends CellarReceptionFolderType {

    /** {@inheritDoc} */
    @Override
    public String getReceptionFolderAbsolutePath(ICellarConfiguration cellarConfiguration) {
        return cellarConfiguration.getCellarFolderDailyReception();
    }

    /** {@inheritDoc} */
    @Override
    public TYPE getSipWorkType() {
        return TYPE.DAILY;
    }

    /** {@inheritDoc} */
    @Override
    protected boolean shouldItBeSorted() {
        return true;
    }

}
