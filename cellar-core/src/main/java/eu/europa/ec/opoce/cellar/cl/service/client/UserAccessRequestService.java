/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.service.client
 *        FILE : UserAccessRequestService.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 10-03-2023
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2023 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.client;

import java.util.List;
import java.util.Map;

import eu.europa.ec.opoce.cellar.cl.domain.admin.User;
import eu.europa.ec.opoce.cellar.cl.domain.admin.UserAccessRequest;

/**
 * Provides access to the user access request functionalities.
 * @author EUROPEAN DYNAMICS S.A
 */
public interface UserAccessRequestService {

    /**
     * Finds all the {@link UserAccessRequest} instances.
     * @return the {@link UserAccessRequest} instances
     */
    List<UserAccessRequest> findAllUserAccessRequests();

    /**
     * Finds the {@link UserAccessRequest} instance corresponding to the identifier.
     * @param id the identifier of the user access request
     * @return the {@link UserAccessRequest} instance
     */
	UserAccessRequest findUserAccessRequest(Long id);
	
	/**
	 * Retrieves a UserAccessRequest based on the corresponding username.
	 * @param username the username to look for.
	 * @return the matching UserAccessRequest.
	 */
	UserAccessRequest findByUsername(String username);

	/**
     * Deletes the provided {@link UserAccessRequest}.
     * @param uar the user access request to delete.
     */
	void deleteUserAccessRequest(UserAccessRequest uar);
	
	/**
	 * Sends a notification email with regards to a given user access request
	 * to the CELLAR administrators, which are the CELLAR users having the ROLE_SECURITY
	 * role.
	 * @param model the email model, containing the information to be embedded into the email template.
	 */
	void notifyAdminsViaEmail(Map<String, Object> model);
	
	/**
	 * Creates a new user access request by inserting a new row
	 * in table <code>USER_ACCESS_REQUEST</code>.
	 * @param euLoginId the EU-Login ID of the user requesting access.
	 * @param email the email of the user requesting access.
	 */
	void createUserAccessRequest(String euLoginId, String email);
	
	/**
	 * Accepts a user access request by inserting a new user row in <code>USER</code> table
	 * and removing the corresponding user access request from the <code>USER_ACCESS_REQUEST</code> table.
	 * @param user the <code>User</code> to persist.
	 * @param userAccessRequestId the ID of the user access request to remove.
	 */
	void acceptUserAccessRequest(User user, long userAccessRequestId);
}
