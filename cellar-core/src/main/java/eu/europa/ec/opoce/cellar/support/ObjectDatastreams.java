/**
 * 
 */
package eu.europa.ec.opoce.cellar.support;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

/**
* @author dsavares
*
*/

@XmlRootElement(name = "objectDatastreams", namespace = ObjectDatastreamsUtils.NAME_SPACE)
@XmlAccessorType(XmlAccessType.FIELD)
public class ObjectDatastreams {

    @XmlElements(value = {
            @XmlElement(name = "datastream", namespace = ObjectDatastreamsUtils.NAME_SPACE)})
    private List<Datastream> datastreams;

    /**
     * Sets a new value for the datastreams.
     * @param datastreams the datastreams to set
     */

    public void setDatastreams(List<Datastream> datastreams) {
        this.datastreams = datastreams;
    }

    /**
     * Gets the value of the datastreams.
     * @return the datastreams
     */
    public List<Datastream> getDatastreams() {
        return datastreams;
    }

}
