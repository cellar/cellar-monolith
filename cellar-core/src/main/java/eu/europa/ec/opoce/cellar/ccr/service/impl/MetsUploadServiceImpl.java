package eu.europa.ec.opoce.cellar.ccr.service.impl;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.ccr.enums.status.StatusType;
import eu.europa.ec.opoce.cellar.ccr.mets.response.MetsResponse;
import eu.europa.ec.opoce.cellar.ccr.service.client.MetsUploadService;
import eu.europa.ec.opoce.cellar.ccr.service.client.StatusService;
import eu.europa.ec.opoce.cellar.ccr.utils.MetsUploadUtils;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.enums.ProcessStatus;
import eu.europa.ec.opoce.cellar.cl.domain.package_history.PackageHistory;
import eu.europa.ec.opoce.cellar.cl.service.client.FileService;
import eu.europa.ec.opoce.cellar.cl.service.client.PackageHistoryService;
import eu.europa.ec.opoce.cellar.common.util.FileSystemUtils;
import eu.europa.ec.opoce.cellar.common.util.StringUtils;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.MetsUploadException;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork;
import eu.europa.ec.opoce.cellar.semantic.helper.DateFormats;
import eu.europa.ec.opoce.cellar.support.FileExtensionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.FileTime;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;

/**
 * Implementation of the {@link MetsUploadService} interface.
 *
 * @author EUROPEAN DYNAMICS S.A.
 */
@Service
public class MetsUploadServiceImpl implements MetsUploadService {

    private static final Logger log = LogManager.getLogger(MetsUploadServiceImpl.class);

    private final FileService fileService;
    private final StatusService statusService;
    private final PackageHistoryService packageHistoryService;
    private ICellarConfiguration cellarConfiguration;
    private RestTemplate restTemplate;

    public MetsUploadServiceImpl(FileService fileService, StatusService statusService, PackageHistoryService packageHistoryService,
                                 @Qualifier("cellarConfiguration") ICellarConfiguration cellarConfiguration) {
        this.fileService = fileService;
        this.statusService = statusService;
        this.packageHistoryService = packageHistoryService;
        this.cellarConfiguration = cellarConfiguration;
        this.restTemplate = createRestTemplateWithTimeout(cellarConfiguration.getCellarMetsUploadCallbackTimeout());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MetsResponse metsUpload(MultipartFile fileData, String priorityStr, String callbackURI) {
        Path tempFile = null;
        SIPWork.TYPE priority = this.convertAndValidatePriority(priorityStr);

        try {
            tempFile = Files.createTempFile("sip_" + System.currentTimeMillis(), ".zip");
            log.debug("Temp file has been created and upload of package is starting.");
            // Create channels to efficiently read data from the multipart file's input stream and write it to a temp file
            // Using ReadableByteChannel allows for more efficient reading
            try (FileChannel fileChannel = FileChannel.open(tempFile,
                    StandardOpenOption.WRITE, StandardOpenOption.CREATE);
                 ReadableByteChannel inputChannel = Channels.newChannel(fileData.getInputStream())) {
                log.debug("Input file channel was created successfully for transferring the upload file.");
                // Create a buffer with size defined in configuration for reading from the input stream
                ByteBuffer buffer = ByteBuffer.allocateDirect(cellarConfiguration.getCellarMetsUploadBufferSize());
                log.debug("Buffer space was successfully allocated with size in bytes: {}", cellarConfiguration.getCellarMetsUploadBufferSize());
                // Read data from the input channel into the buffer and write it to the temporary file
                while (inputChannel.read(buffer) != -1) {
                    buffer.flip();
                    while (buffer.hasRemaining()) {
                        fileChannel.write(buffer);
                    }
                    buffer.clear();
                }
            }
            log.info("Upload input package stream to temp file completed successfully.");
            String metsID = MetsUploadUtils.resolveMetsId(tempFile.toFile());
            log.info("Start moving tmp file to ingestion folder with priority [{}]. Upload package has been re-named to [{}] ", priority, metsID + FileExtensionUtils.SIP);
            File file = fileService.moveSIPToReceptionFolderByPriority(tempFile.toFile(), metsID, priority);
            log.info("Package was successfully moved to ingestion folder and ingestion will start soon...");
            final PackageHistory packageHistory = this.createPackageHistoryDb(file, callbackURI);

            return createMetsResponse(packageHistory);
        } catch (Exception e) {
            log.error("Failed to upload or move METS package [{}] to the ingestion folder.", fileData.getOriginalFilename(), e);
            throw ExceptionBuilder.get(MetsUploadException.class)
            	.withCode(CommonErrors.CELLAR_INTERNAL_ERROR)
            	.withCause(e)
            	.withMessage("Failed to upload or move METS package [{}] to the ingestion folder.")
            	.withMessageArgs(fileData.getOriginalFilename())
            	.build();
        } finally {
            if (tempFile != null) {
                try {
                    Files.deleteIfExists(tempFile);
                } catch (IOException e) {
                	log.error("Unable to delete the temporary file.");
                }
            }
        }
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public void getStatusResponseAndSendCallbackRequest(File sipFile, boolean isError, PackageHistory packageHistory) {
        if (StringUtils.isBlank(packageHistory.getCallbackUrl())) {
            return;
        }

        try {
            int verbosity = this.resolveVerbosity(isError);
            String statusResponse = statusService.getStatusResponse(packageHistory.getUuid(), true,
                    false, null, verbosity, StatusType.PACKAGE_LEVEL);

            // Send the callback request to the specified callback URI
            this.sendCallbackRequest(packageHistory, statusResponse);

            log.info("Send Callback request operation completed");
        } catch (Exception e) {
            log.warn("Could not retrieve the response from Mets Status Service for package with UUID: {}", packageHistory.getUuid(),e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public PackageHistory createPackageHistoryDb(File sipFile, String callbackURL) {
        // Create PackageHistory object
        PackageHistory packageHistory = new PackageHistory(String.valueOf(UUID.randomUUID()), getTimestampAttribute(sipFile), sipFile.getName(), callbackURL, ProcessStatus.NA);

        // Insert in database
        this.packageHistoryService.saveEntry(packageHistory);

        log.debug("PACKAGE_HISTORY: Saved {}", packageHistory);
        return packageHistory;
    }

    /**
     * {@inheritDoc}
     */
    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    private MetsResponse createMetsResponse(PackageHistory packageHistory) {
        MetsResponse metsResponse = new MetsResponse();
        metsResponse.setReceived(DateFormats.formatXmlDateTime(packageHistory.getReceivedDate()));
        metsResponse.setStatus_endpoint(statusService.getCellarProxiedStatusServicePackageLevelEndpointWithUuid(packageHistory.getUuid()));
        return metsResponse;
    }

    /**
     * Converts the priority from {@link String} to {@link SIPWork.TYPE} and validates it.
     *
     * @param priorityStr the priority as a {@link String}
     * @return the priority as a {@link SIPWork.TYPE}
     */
    private SIPWork.TYPE convertAndValidatePriority(String priorityStr) {
        try {
            return SIPWork.TYPE.valueOf(priorityStr);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Invalid value for parameter 'priority'. Allowed values: " + Arrays.toString(SIPWork.TYPE.values()));
        }
    }

    private void sendCallbackRequest(PackageHistory packageHistory, String jsonResponse) {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            HttpEntity<String> callbackRequest = new HttpEntity<>(jsonResponse, headers);

            restTemplate.exchange(packageHistory.getCallbackUrl(), HttpMethod.POST, callbackRequest, Void.class);
        } catch (Exception e) {
            log.warn("Failed to send callback request for package UUID: {}", packageHistory.getUuid(), e);
        }
    }

    /**
     * Creates a new RestTemplate instance with the specified connection timeout.
     *
     * @param timeout The connection timeout value in milliseconds.
     * @return A new RestTemplate instance with the specified connection timeout.
     */
    private RestTemplate createRestTemplateWithTimeout(int timeout) {
        ClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
        ((HttpComponentsClientHttpRequestFactory) requestFactory).setConnectTimeout(timeout);
        return new RestTemplate(requestFactory);
    }

    /**
     * Retrieves either the "lastAccessed", "lastModified", or "lastChanged" timestamp
     * attribute of the provided file. The actual attribute that
     * is returned depends on the cellar configuration.
     *
     * @param file the file whose timestamp attribute shall be retrieved.
     * @return the file's timestamp attribute.
     */
    private Date getTimestampAttribute(File file) {
        String attributeName = this.cellarConfiguration.getCellarServiceSipQueueManagerFileAttributeDateType();
        FileTime attributeValue = (FileTime) FileSystemUtils.readAttribute(file, attributeName);
        if (attributeValue == null) {
            log.warn("Attribute [{}] of package [{}] could not be retrieved. The current date is used as a default value.", attributeName, file);
            return new Date();
        } else {
            return new Date(attributeValue.toMillis());
        }
    }

    private int resolveVerbosity(boolean isError) {
        return isError ? 2 : 1;
    }

}
