package eu.europa.ec.opoce.cellar.ccr.helper;

import com.amazonaws.services.s3.Headers;
import eu.europa.ec.opoce.cellar.cl.domain.response.ContentResponseOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.DigitalObjectOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.ResponseOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.ResponseOperationType;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentStream;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import eu.europa.ec.opoce.cellar.s3.domain.ContentVersion;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * deprecated: The help should not have injected service and should contains
 * only utility function. When possible, find a way to refactor that class.
 */
@Deprecated
public class ResponseHelper {

    private static final ContentStreamService CONTENT_STREAM_SERVICE = ServiceLocator.getService(ContentStreamService.class);

    /**
     * Fills the provided operation with creation information about the given digital object.
     *
     * @param digitalObject
     */
    public static DigitalObjectOperation fillCreateOperation(final DigitalObject digitalObject) {
        final DigitalObjectOperation digitalObjectOperation = new DigitalObjectOperation(digitalObject, digitalObject.getBusinessMetadata() == null ? ResponseOperationType.APPEND : ResponseOperationType.CREATE);
        for (final ContentIdentifier cid : digitalObject.getContentids()) {
            digitalObjectOperation.addProductionIdentifier(cid.getIdentifier(), ResponseOperationType.CREATE);
        }

        if (digitalObject.getContentStreams() != null) {
            for (final ContentStream contentStream : digitalObject.getContentStreams()) {
                for (final ContentIdentifier contentStreamId : contentStream.getContentids()) {
                    ResponseOperation response = new ContentResponseOperation(contentStream.getCellarId().getIdentifier(), contentStreamId.getIdentifier(), ResponseOperationType.CREATE);
                    setVersions(contentStream.getCellarId(), ContentType.FILE, response);
                    digitalObjectOperation.addContentIdentifier((ContentResponseOperation) response);
                }
            }
        }
        return digitalObjectOperation;
    }

    public static ResponseOperation setVersions(final ContentIdentifier contentIdentifier, final ContentType contentType,
                                                final ResponseOperation response) {
        final List<ContentVersion> versions = CONTENT_STREAM_SERVICE.getVersions(contentIdentifier.getIdentifier(), contentType)
                .stream()
                .filter(v -> !v.isDeleted()) // drop delete marker
                .sorted(Comparator.comparing(ContentVersion::getLastModified).reversed())
                .collect(Collectors.toList());

        // In the response file, the version is the last modified date.
        response.setNewVersion(formatDate(versions.get(0).getLastModified()));
        if (versions.size() > 1) {
            Map<String, Object> metadata = CONTENT_STREAM_SERVICE.getMetadata(contentIdentifier.getIdentifier(), versions.get(1).getVersion(), contentType);
            response.setLastMimeType((String) metadata.get(Headers.CONTENT_TYPE));
            response.setLastVersion(formatDate(versions.get(1).getLastModified()));
        } else if (versions.size() == 1) {
            response.setLastVersion(null);
        }
        return response;
    }

    public static String formatDate(Date date) {
        return DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(date.toInstant().atZone(ZoneId.systemDefault()));
    }

}
