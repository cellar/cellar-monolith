package eu.europa.ec.opoce.cellar.cl.dao.aligner;

import eu.europa.ec.opoce.cellar.cl.configuration.enums.CellarServiceRDFStoreCleanerMode;
import eu.europa.ec.opoce.cellar.cl.domain.aligner.AlignerMisalignment;

import java.util.List;

/**
 * The Interface AlignerMisalignmentDao.
 */
public interface AlignerMisalignmentDao {

    /**
     * Count.
     *
     * @return the long
     */
    Long count();

    /**
     * Count.
     *
     * @param cellarId the cellar id
     * @param cellarServiceRDFStoreCleanerMode the cellar service rdf store cleaner mode
     * @param repository the repository
     * @return the long
     */
    Long count(final String cellarId, final CellarServiceRDFStoreCleanerMode cellarServiceRDFStoreCleanerMode,
            final eu.europa.ec.opoce.cellar.cl.enums.aligner.Repository repository);

    /**
     * Find all.
     *
     * @return the list
     */
    List<AlignerMisalignment> findAll();

    /**
     * Find all.
     *
     * @param cellarId the cellar id
     * @param cellarServiceRDFStoreCleanerMode the cellar service rdf store cleaner mode
     * @param repository the repository
     * @param sortOrder the sort order
     * @param column the column : 0-cellarID, 1-operationDate,2-operationType,3-repository
     * @param startPage the start page
     * @param pageSize the page size
     * @return the list
     */
    List<String[]> findAll(final String cellarId, final CellarServiceRDFStoreCleanerMode cellarServiceRDFStoreCleanerMode,
            final eu.europa.ec.opoce.cellar.cl.enums.aligner.Repository repository, final String sortOrder, final int column,
            final int startPage, final int pageSize);

    /**
     * Save.
     *
     * @param alignerMisalignment the aligner misalignment
     * @return the aligner misalignment
     */
    AlignerMisalignment save(final AlignerMisalignment alignerMisalignment);

}
