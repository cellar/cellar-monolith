/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.licenseHolder
 *             FILE : ArchiveTreeManager.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Nov 5, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.licenseHolder;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

/**
 * <class_description> Archive tree manager for providing paths in the archive.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Nov 5, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class ArchiveTreeManager {

    private static final String FOLDER = "/%d-%d";

    /**
     * Base destination folder.
     */
    private final String archiveTempDirectoryPath;

    /**
     * Maximum folder size.
     */
    private final int maxFolderSize;

    /**
     * Total maximum number of files in the archive.
     */
    private final Long maxNumberOfFiles;

    /**
     * Number of levels necessary to store maxNumberOfFiles files.
     */
    private final int numberOfLevels;

    /**
     * Current number of files already batched.
     */
    private int numberOfFilesBatched;

    /**
     * Current index in the batch.
     */
    private int fileIndexInBatch;

    /**
     * Current destination folder.
     */
    private String currentFolderPath;

    /**
     * Constructor.
     * @param maxFolderSize the maximum folder size
     * @param maxNumberOfFiles the total maximum number of files in the archive
     * @param archiveTempDirectoryPath the base destination folder
     * @throws IOException
     */
    public ArchiveTreeManager(final int maxFolderSize, final Long maxNumberOfFiles, final String archiveTempDirectoryPath)
            throws IOException {
        this.archiveTempDirectoryPath = archiveTempDirectoryPath;
        this.maxFolderSize = maxFolderSize;
        this.maxNumberOfFiles = maxNumberOfFiles;
        this.numberOfLevels = this.computeNumberOfLevels();
        this.numberOfFilesBatched = 0;
        this.fileIndexInBatch = 0;

        if (this.numberOfLevels > 0) {
            this.constructFolderPath();
            this.mkdirCurrentFolderPath();
        }
    }

    /**
     * Gets the next folder path.
     * @return the next folder path
     * @throws IOException
     */
    public String getFolderPath() throws IOException {
        if (numberOfLevels <= 0) // archive root
            return archiveTempDirectoryPath;

        synchronized (this) {
            if (this.fileIndexInBatch >= this.maxFolderSize) {
                this.numberOfFilesBatched += this.fileIndexInBatch;
                this.fileIndexInBatch = 0;
                this.constructFolderPath();
                this.mkdirCurrentFolderPath();
            }
            this.fileIndexInBatch++;
        }

        return this.currentFolderPath;
    }

    /**
     * Constructs the current folder path.
     */
    private void constructFolderPath() {
        final double fileIndex = this.numberOfFilesBatched + this.fileIndexInBatch;
        final StringBuilder relativePath = new StringBuilder();
        int levelSize, from = 0, to = 0;
        double res;
        for (int i = this.numberOfLevels; i > 0; i--) {
            levelSize = (int) Math.pow(this.maxFolderSize, i);
            res = fileIndex / levelSize;
            from = ((int) res) * levelSize;
            to = from + levelSize - 1;
            relativePath.append(String.format(FOLDER, from, to));
        }

        this.currentFolderPath = this.archiveTempDirectoryPath + relativePath.toString();
    }

    /**
     * Computes the number of levels for the maximum number of files.
     * @return the number of levels
     */
    private int computeNumberOfLevels() {
        if (this.maxFolderSize == 0)
            return 0;

        int numberOfLevels = 0;
        double tmp = this.maxNumberOfFiles;
        while (tmp > this.maxFolderSize) {
            tmp = tmp / this.maxFolderSize;
            numberOfLevels++;
        }

        return numberOfLevels;
    }

    /**
     * Creates the current directory.
     * @throws IOException
     */
    private void mkdirCurrentFolderPath() throws IOException {
        final File directory = new File(this.currentFolderPath);

        if (!directory.exists())
            FileUtils.forceMkdir(directory);
    }
}
