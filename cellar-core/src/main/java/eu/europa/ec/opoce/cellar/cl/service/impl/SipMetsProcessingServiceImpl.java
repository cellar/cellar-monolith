/**
 * 
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.ccr.service.database.DefaultTransactionService;
import eu.europa.ec.opoce.cellar.cl.dao.SipMetsProcessingDao;
import eu.europa.ec.opoce.cellar.cl.domain.sip.SipMetsProcessing;
import eu.europa.ec.opoce.cellar.cl.service.client.SipMetsProcessingService;
import eu.europa.ec.opoce.cellar.monitoring.Watch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * DAO layer implementation for accessing <code>SipMetsProcessing</code> instances in the
 * persistence layer.
 * 
 * @author dcraeye
 * 
 */
@Service
public class SipMetsProcessingServiceImpl extends DefaultTransactionService implements SipMetsProcessingService {

    @Autowired
    private SipMetsProcessingDao sipMetsProcessingDao;

    @Watch("Create SIP METS")
    @Override
    @Transactional
    public void createSipMetsProcessing(SipMetsProcessing sipMetsProcessing) {
        sipMetsProcessingDao.createSipMetsProcessing(sipMetsProcessing);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional
    public void deleteSipMetsProcessing(SipMetsProcessing sipMetsProcessing) {
        sipMetsProcessingDao.deleteSipMetsProcessing(sipMetsProcessing);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SipMetsProcessing> getSipMetsProcessingList() {
        return sipMetsProcessingDao.getSipMetsProcessingList();
    }

    @Watch(value = "Create SIP METS", arguments = {
            @Watch.WatchArgument(name = "Sip name", expression = "sipMetsProcessing.sipName"),
            @Watch.WatchArgument(name = "Sip priority", expression = "sipMetsProcessing.priority"),
            @Watch.WatchArgument(name = "Sip type", expression = "sipMetsProcessing.type")
    })
    @Override
    @Transactional
    public void updateSipMetsProcessing(SipMetsProcessing sipMetsProcessing) {
        sipMetsProcessingDao.updateSipMetsProcessing(sipMetsProcessing);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SipMetsProcessing getSipMetsProcessing(final Long id) {
        return sipMetsProcessingDao.getSipMetsProcessing(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SipMetsProcessing getBySipName(final String sipName) {
        return sipMetsProcessingDao.getBySipName(sipName);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SipMetsProcessing getByMetsFolder(final String metsFolder) {
        return sipMetsProcessingDao.getByMetsFolder(metsFolder);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SipMetsProcessing> getAll() {
        return sipMetsProcessingDao.getAll();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SipMetsProcessing> getAllByPriority() {
        return sipMetsProcessingDao.getAllByPriority();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SipMetsProcessing> getAllBySMCount() {
        return sipMetsProcessingDao.getAllBySMCount();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SipMetsProcessing> getAllByDOCount() {
        return sipMetsProcessingDao.getAllByDOCount();
    }

}
