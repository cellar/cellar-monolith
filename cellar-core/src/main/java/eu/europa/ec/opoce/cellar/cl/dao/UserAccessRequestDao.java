/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.dao
 *        FILE : UserAccessRequestDao.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 22-02-2023
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2023 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao;

import eu.europa.ec.opoce.cellar.cl.domain.admin.UserAccessRequest;

/**
 * <class_description> Dao for the entity {@link UserAccessRequest}.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 22, 2023
 *
 * @author EUROPEAN DYNAMICS S.A.
 */
public interface UserAccessRequestDao extends BaseDao<UserAccessRequest, Long>{

	/**
	 * Retrieves a UserAccessRequest based on the corresponding username.
	 * @param username the username to look for.
	 * @return the matching UserAccessRequest.
	 */
	UserAccessRequest findByUsername(String username);
	
}
