/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.ccr.service.impl
 *        FILE : MetsServiceFactory.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 19-06-2012
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.service.impl;

import eu.europa.ec.opoce.cellar.ccr.service.client.MetsResponseService;
import eu.europa.ec.opoce.cellar.cl.dao.StructMapResponseDao;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

/**
 * <class_description> Factory for selecting the correct implementations of {@link MetsResponseService} and {@link StructMapResponseDao}.<br/>
 * Its methods selects the correct implementations on the basis of the <code>partOfBulkIngest</code> property of the <code>sipResource</code> argument.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 19-06-2012
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class MetsServiceFactory {

    private final MetsResponseService metsAuthenticOJResponseService;

    private final MetsResponseService metsDailyResponseService;

    private final MetsResponseService metsBulkResponseService;
    
    private final MetsResponseService metsBulkLowPriorityResponseService;

    private final StructMapResponseDao structMapAuthenticOJResponseDao;

    private final StructMapResponseDao structMapDailyResponseDao;

    private final StructMapResponseDao structMapBulkResponseDao;
    
    private final StructMapResponseDao structMapBulkLowPriorityResponseDao;

    @Autowired
    public MetsServiceFactory(@Qualifier("metsAuthenticOJResponseService") MetsResponseService metsAuthenticOJResponseService,
                              @Qualifier("metsDailyResponseService") MetsResponseService metsDailyResponseService,
                              @Qualifier("metsBulkResponseService") MetsResponseService metsBulkResponseService,
                              @Qualifier("metsBulkLowPriorityResponseService") MetsResponseService metsBulkLowPriorityResponseService,
                              @Qualifier("structMapAuthenticOJResponseDao") StructMapResponseDao structMapAuthenticOJResponseDao,
                              @Qualifier("structMapDailyResponseDao") StructMapResponseDao structMapDailyResponseDao,
                              @Qualifier("structMapBulkResponseDao") StructMapResponseDao structMapBulkResponseDao,
                              @Qualifier("structMapBulkLowPriorityResponseDao") StructMapResponseDao structMapBulkLowPriorityResponseDao) {
        this.metsAuthenticOJResponseService = metsAuthenticOJResponseService;
        this.metsDailyResponseService = metsDailyResponseService;
        this.metsBulkResponseService = metsBulkResponseService;
        this.metsBulkLowPriorityResponseService = metsBulkLowPriorityResponseService;
        this.structMapAuthenticOJResponseDao = structMapAuthenticOJResponseDao;
        this.structMapDailyResponseDao = structMapDailyResponseDao;
        this.structMapBulkResponseDao = structMapBulkResponseDao;
        this.structMapBulkLowPriorityResponseDao = structMapBulkLowPriorityResponseDao;
    }

    /**
     * Selects the correct {@link MetsResponseService} implementation on the basis of the <code>partOfBulkIngest</code> property of the <code>sipResource</code> argument.
     * 
     * @param sipResource the argument on the basis of which the method selects the correct {@link MetsResponseService} implementation
     * @return the correct {@link MetsResponseService} implementation
     */
    public MetsResponseService getMetsResponseService(final SIPResource sipResource) {
        switch (sipResource.getSipType()) {
        case AUTHENTICOJ:
            return this.metsAuthenticOJResponseService;
        case DAILY:
            return this.metsDailyResponseService;
        case BULK:
            return this.metsBulkResponseService;
        case BULK_LOWPRIORITY:
        	return this.metsBulkLowPriorityResponseService;
        default:
        	return this.metsBulkResponseService;
        }
    }

    /**
     * Selects the correct {@link StructMapResponseDao} implementation on the basis of the <code>partOfBulkIngest</code> property of the <code>sipResource</code> argument.
     * 
     * @param sipResource the argument on the basis of which the method selects the correct {@link StructMapResponseDao} implementation
     * @return the correct {@link StructMapResponseDao} implementation
     */
    public StructMapResponseDao getStructMapResponseDao(final SIPResource sipResource) {
        switch (sipResource.getSipType()) {
        case AUTHENTICOJ:
            return this.structMapAuthenticOJResponseDao;
        case DAILY:
            return this.structMapDailyResponseDao;
        case BULK:
            return this.structMapBulkResponseDao;
        case BULK_LOWPRIORITY:
        	return this.structMapBulkLowPriorityResponseDao;
        default:
        	return this.structMapBulkResponseDao;
        }
    }

}
