/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor
 *             FILE : AbstractBatchJobProcessor.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 4, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2016-03-09 13:57:52 +0100 (mer., 09 mars 2016) $
 *          VERSION : $LastChangedRevision: 10409 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor;

import eu.europa.ec.opoce.cellar.cl.dao.BatchJobWorkIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.BatchJobService;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * <class_description> Common batch job processor service.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 4, 2015
 *
 * @author ARHS Developments
 * @version $Revision: 10409 $
 */
public abstract class AbstractBatchJobPreProcessor implements IBatchJobProcessor {

    /**
     * Batch job work identifier dao.
     */
    @Autowired
    protected BatchJobWorkIdentifierDao batchJobWorkIdentifierDao;

    /**
     * The batch job service.
     */
    @Autowired(required = true)
    protected BatchJobService batchJobService;

    /**
     * Batch job to be processed.
     */
    protected final BatchJob batchJob;

    /**
     * Instantiates a new abstract batch job processor.
     *
     * @param batchJob the batch job
     */
    public AbstractBatchJobPreProcessor(final BatchJob batchJob) {
        this.batchJob = batchJob;
    }

    /**
     * Process the mentioned work.
     * @param batchJob the batchJob
     */
    protected abstract void doExecute(final BatchJob batchJob);

    /**
     * Pre execute.
     */
    protected abstract void preExecute();

    /**
     * Post execute.
     */
    protected abstract void postExecute();

    /**
     * Process the batch job.
     */
    @Override
    public void execute() {

        preExecute();

        this.initBatchJobPreProcessing();

        doExecute(batchJob);

        postExecute();
    }

    /**
     * Init method executed before processing.
     */
    protected void initBatchJobPreProcessing() {

    }
}
