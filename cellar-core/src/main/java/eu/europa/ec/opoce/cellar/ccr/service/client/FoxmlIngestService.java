package eu.europa.ec.opoce.cellar.ccr.service.client;

import java.io.File;

public interface FoxmlIngestService {

    void ingest(Iterable<File> foxmlFiles);
}
