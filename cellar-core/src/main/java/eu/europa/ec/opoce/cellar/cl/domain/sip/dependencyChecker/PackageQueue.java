/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker
 *        FILE : PackageQueue.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 02-12-2021
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2021 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;

/**
 * Domain object representing the PACKAGE_QUEUE view.
 * @author EUROPEAN DYNAMICS S.A.
 */
@Entity
@Table(name = "PACKAGE_QUEUE")
public class PackageQueue {

	/**
	 * The SIP ID.
	 */
	@Id
	@Column(name = "SIP_ID", insertable = false, updatable = false, unique = true, nullable = false)
	private Long sipId;
	/**
	 * The name of the SIP.
	 */
	@Column(name = "NAME", insertable = false, updatable = false, unique = true, nullable  = false, length = 255)
	private String name;
	/**
	 * The type of the SIP.
	 */
	@Column(name = "TYPE", insertable = false, updatable = false, nullable = false, length = 20)
	@Enumerated(EnumType.STRING)
	private TYPE type;
	
	@Column(name = "DETECTION_DATE", insertable = false, updatable = false, nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date detectionDate;
	
	/**
	 * The id of the PACKAGE_HISTORY entry.
	 */
	@Column(name = "PACKAGE_HISTORY_ID", insertable = false, updatable = false)
	private Long packageHistoryId;
	
	
	public PackageQueue() {
		
	}
	
	
	public Long getSipId() {
		return sipId;
	}

	public void setSipId(Long sipId) {
		this.sipId = sipId;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public TYPE getType() {
		return type;
	}
	
	public void setType(TYPE type) {
		this.type = type;
	}

	public Date getDetectionDate() {
		return detectionDate;
	}

	public void setDetectionDate(Date detectionDate) {
		this.detectionDate = detectionDate;
	}
	
	public Long getPackageHistoryId() {
		return packageHistoryId;
	}
	
	public void setPackageHistoryId(Long packageHistoryId) {
		this.packageHistoryId = packageHistoryId;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PackageQueue other = (PackageQueue) obj;
		return Objects.equals(name, other.name);
	}
	
}
