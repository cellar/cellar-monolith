package eu.europa.ec.opoce.cellar.cl.service.client;

import java.util.List;

import eu.europa.ec.opoce.cellar.cl.domain.admin.ValidatorRule;

/**
 * The service for validator rule administration.  (CRUD)
 * @author dcraeye
 *
 */
public interface ValidatorRuleService {

    /**
     * Get all validator rules.
     * @return a {@link List} of {@link ValidatorRule}
     */
    List<ValidatorRule> getRules();

    /**
     * Add a new {@link ValidatorRule}
     */
    void createRule(ValidatorRule validatorRule);

    /**
     * Get the {@link ValidatorRule} corresponding to this id.
     * @param ruleId id of the validator rule to returns.
     * @returns the corresponding {@link ValidatorRule}
     */
    ValidatorRule getRule(Long ruleId);

    /**
     * Get the {@link ValidatorRule} corresponding to this class.
     * @param validatorClass of the validator rule to returns.
     * @returns the corresponding {@link ValidatorRule}
     */
    ValidatorRule getRuleByValidatorClass(String validatorClass);

    /**
     * Return all system validators.
     * 
     * @return the system validators.
     */
    List<ValidatorRule> getSystemValidators();

    /**
     * Return all custom validators.
     * 
     * @return the custom validators.
     */
    List<ValidatorRule> getCustomValidators();

    /**
     * Remove this {@link ValidatorRule}
     */
    void deleteRule(ValidatorRule validatorRule);

    /**
     * Update this {@link ValidatorRule}
     */
    void updateRule(ValidatorRule validatorRule);

}
