/**
 * 
 */
package eu.europa.ec.opoce.cellar.ccr.service.impl;

/**
 * Simple bean that is used to hold values needed to build the result of the service that get all
 * the documents attached to an expression.
 * 
 * @author dsavares
 * 
 */
public class DocumentURIsServiceConfiguration {

    /**
     * The name of velocity template used to format the result of the sercvice.
     */
    private String velocityTemplate;

    /**
     * Sets a new value for the velocityTemplate.
     * 
     * @param velocityTemplate
     *            the velocityTemplate to set
     */
    public void setVelocityTemplate(String velocityTemplate) {
        this.velocityTemplate = velocityTemplate;
    }

    /**
     * Gets the value of the velocityTemplate.
     * 
     * @return the velocityTemplate
     */
    public String getVelocityTemplate() {
        return velocityTemplate;
    }

}
