/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.scheduling
 *             FILE : BatchJobScheduler.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 13, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.scheduling;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.BatchJobProcessingService;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.BatchJobService;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.manager.BatchJobThreadPoolManager;
import eu.europa.ec.opoce.cellar.common.spring.CloseableConcurrentTaskScheduler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * <class_description> Batch job scheduler.
 *
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 13, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
@Lazy(false)
public class BatchJobScheduler {

    /** The logger. */
    private static final Logger LOGGER = LoggerFactory.getLogger(BatchJobScheduler.class);

    /**
     * Batch job processing service.
     */
    @Autowired(required = true)
    private BatchJobProcessingService batchJobProcessingService;

    /**
     * Batch job service.
     */
    @Autowired(required = true)
    private BatchJobService batchJobService;

    /**
     * The Cellar configuration storing runtime parameters.
     */
    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    /**
     * The SPARQL query executing scheduler.
     */
    private SparqlExecutingScheduler sparqlExecutingScheduler;

    /** The sparql executing future. */
    @SuppressWarnings("rawtypes")
    private ScheduledFuture sparqlExecutingFuture;

    /**
     * The batch processing scheduler.
     */
    private BatchProcessingScheduler processingScheduler;

    /** The processing scheduler future. */
    @SuppressWarnings("rawtypes")
    private ScheduledFuture processingSchedulerFuture;

    /** The batch job thread pool manager. */
    @Autowired
    private BatchJobThreadPoolManager batchJobThreadPoolManager;

    /** Different crons existing in database. */
    private List<String> crons;

    /** The update cron sparql update schedulers. */
    private Map<String, ScheduledFuture<?>> updateCronSparqlUpdateSchedulers;

    /** The stopped. */
    private AtomicBoolean stopped;

    private CloseableConcurrentTaskScheduler sparqlExecutingTaskScheduler;
    private CloseableConcurrentTaskScheduler processingExecutingTaskScheduler;
    private Map<CloseableConcurrentTaskScheduler, ScheduledFuture<?>> schedulers = new HashMap<>();

    /**
     * Post constructor initialization.
     */
    @PostConstruct
    public void init() {
        if (!this.cellarConfiguration.isCellarDatabaseReadOnly() && this.cellarConfiguration.isCellarServiceBatchJobEnabled()) {
            this.batchJobProcessingService.cleanBatchJobsRunning();

            this.sparqlExecutingScheduler = new SparqlExecutingScheduler(this.batchJobService, this.batchJobProcessingService);
            this.sparqlExecutingTaskScheduler = CloseableConcurrentTaskScheduler.newScheduler("sparql-executing");
            this.sparqlExecutingFuture = sparqlExecutingTaskScheduler.schedule(this.sparqlExecutingScheduler,
                    new CronTrigger(this.cellarConfiguration.getCellarServiceBatchJobSparqlCronSettings()));

            processingScheduler = new BatchProcessingScheduler(this.batchJobService, this.batchJobProcessingService);
            processingExecutingTaskScheduler = CloseableConcurrentTaskScheduler.newScheduler("processing");
            processingSchedulerFuture = processingExecutingTaskScheduler.schedule(this.processingScheduler,
                    new CronTrigger(this.cellarConfiguration.getCellarServiceBatchJobProcessingCronSettings()));

            updateCronSparqlUpdateSchedulers = new HashMap<>();
            this.crons = this.batchJobService.findUpdateBatchJobCrons();
            final String cellarServiceBatchJobSparqlUpdateMaxCron = this.cellarConfiguration.getCellarServiceBatchJobSparqlUpdateMaxCron();
            if (this.crons.size() > Integer.valueOf(cellarServiceBatchJobSparqlUpdateMaxCron)) {
                LOGGER.warn("The maximum of different Cron expression is {}", cellarServiceBatchJobSparqlUpdateMaxCron);
            }
            startCron();
        }
        stopped = new AtomicBoolean(false);
    }

    public void shutdown(boolean awaitTermination, long timeout, TimeUnit unit) throws InterruptedException {
        for (Entry<CloseableConcurrentTaskScheduler, ScheduledFuture<?>> scheduler : schedulers.entrySet()) {
            scheduler.getValue().cancel(!awaitTermination);
            scheduler.getKey().shutdown(awaitTermination, timeout, unit);
        }
        sparqlExecutingFuture.cancel(!awaitTermination);
        sparqlExecutingTaskScheduler.shutdown(awaitTermination, timeout, unit);
        processingSchedulerFuture.cancel(!awaitTermination);
        processingExecutingTaskScheduler.shutdown(awaitTermination, timeout, unit);
    }

    /**
     * Sets the configuration when the sparql executing-process will start.
     * @param cron the new cron-configuration that need to be used
     */
    public void setCronBatchJobSparqlCronSettings(final String cron) {
        if (!this.cellarConfiguration.isCellarDatabaseReadOnly() && this.cellarConfiguration.isCellarServiceBatchJobEnabled()) {
            sparqlExecutingFuture.cancel(false);
            sparqlExecutingTaskScheduler.shutdownNow();
            sparqlExecutingTaskScheduler = CloseableConcurrentTaskScheduler.newScheduler("sparql-executing");
            sparqlExecutingFuture = sparqlExecutingTaskScheduler.schedule(this.sparqlExecutingScheduler, new CronTrigger(cron));
        }
        this.cellarConfiguration.setCellarServiceBatchJobSparqlCronSettings(cron);
    }

    /**
     * Gets the configuration when the sparql executing-process will start.
     * @return cron configuration that is used for sparql executing
     */
    public String getCronBatchJobSparqlCronSettings() {
        return this.cellarConfiguration.getCellarServiceBatchJobSparqlCronSettings();
    }

    /**
     * Sets the configuration when the batch job executing-process will start.
     * @param cron the new cron-configuration that need to be used
     */
    public void setCronBatchJobProcessingCronSettings(final String cron) {
        if (!this.cellarConfiguration.isCellarDatabaseReadOnly() && this.cellarConfiguration.isCellarServiceBatchJobEnabled()) {
            this.processingSchedulerFuture.cancel(false);
            this.processingExecutingTaskScheduler.shutdownNow();
            this.processingExecutingTaskScheduler = CloseableConcurrentTaskScheduler.newScheduler("processing");
            this.processingSchedulerFuture = processingExecutingTaskScheduler.schedule(this.processingScheduler, new CronTrigger(cron));
        }
        this.cellarConfiguration.setCellarServiceBatchJobProcessingCronSettings(cron);
    }

    /**
     * Gets the configuration when the batch job executing-process will start.
     * @return cron configuration that is used
     */
    public String getCronBatchJobProcessingCronSettings() {
        return this.cellarConfiguration.getCellarServiceBatchJobProcessingCronSettings();
    }

    /**
     * Gets the crons.
     *
     * @return the crons
     */
    public synchronized List<String> getCrons() {
        return this.crons;
    }

    /**
     * Sets the crons.
     *
     * @param crons the crons to set
     */
    public synchronized void setCrons(final List<String> crons) {
        this.crons = crons;
    }

    /**
     * Stop schedulers.
     */
    @SuppressWarnings("rawtypes")
    public void stopSchedulers() {
        this.sparqlExecutingFuture.cancel(false);
        this.sparqlExecutingTaskScheduler.shutdownNow();

        this.processingSchedulerFuture.cancel(false);
        this.processingExecutingTaskScheduler.shutdownNow();

        batchJobThreadPoolManager.clear();
        for (final Entry<String, ScheduledFuture<?>> entry : updateCronSparqlUpdateSchedulers.entrySet()) {
            final ScheduledFuture updateCronSparqlUpdateScheduler = entry.getValue();
            updateCronSparqlUpdateScheduler.cancel(false);
        }
        updateCronSparqlUpdateSchedulers.clear();
        stopped.set(true);
    }

    /**
     * Restart schedulers.
     */
    public void restartSchedulers() {
        this.sparqlExecutingTaskScheduler = CloseableConcurrentTaskScheduler.newScheduler("sparql-executing");
        this.sparqlExecutingFuture = sparqlExecutingTaskScheduler.schedule(this.sparqlExecutingScheduler,
                new CronTrigger(this.cellarConfiguration.getCellarServiceBatchJobSparqlCronSettings()));

        this.processingExecutingTaskScheduler = CloseableConcurrentTaskScheduler.newScheduler("processing");
        this.processingSchedulerFuture = processingExecutingTaskScheduler.schedule(this.processingScheduler,
                new CronTrigger(this.cellarConfiguration.getCellarServiceBatchJobProcessingCronSettings()));

        this.crons = this.batchJobService.findUpdateBatchJobCrons();
        final String cellarServiceBatchJobSparqlUpdateMaxCron = this.cellarConfiguration.getCellarServiceBatchJobSparqlUpdateMaxCron();
        if (this.crons.size() > Integer.valueOf(cellarServiceBatchJobSparqlUpdateMaxCron)) {
            LOGGER.warn("The maximum of different Cron expression is {}", cellarServiceBatchJobSparqlUpdateMaxCron);
        }
        startCron();
        stopped.set(false);
    }

    private void startCron() {
        for (final String cron : this.crons) {
            final SparqlUpdateProcessingScheduler sparqlUpdateProcessingScheduler = new SparqlUpdateProcessingScheduler(
                    this.batchJobService, this.batchJobProcessingService, cron);
            final CloseableConcurrentTaskScheduler scheduler = CloseableConcurrentTaskScheduler.newScheduler("sparql-update");
            final ScheduledFuture<?> future = scheduler.schedule(sparqlUpdateProcessingScheduler, new CronTrigger(cron));
            updateCronSparqlUpdateSchedulers.put(cron, future);
            schedulers.put(scheduler, future);
        }
    }

    /**
     * Are schedulers running.
     *
     * @return true, if successful
     */
    public boolean areSchedulersRunning() {
        return !stopped.get();
    }

}
