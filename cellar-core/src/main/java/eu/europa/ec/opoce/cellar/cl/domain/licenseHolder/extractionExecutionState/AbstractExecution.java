/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.LicenseHolder
 *             FILE : ExtractionExecutionState.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 5, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionConfiguration.CONFIGURATION_TYPE;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution;
import eu.europa.ec.opoce.cellar.cl.service.client.LicenseHolderService;
import eu.europa.ec.opoce.cellar.common.util.ErrorUtils;
import eu.europa.ec.opoce.cellar.common.util.TimeUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * <class_description> Abstract class of the execution logic.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 5, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component
public abstract class AbstractExecution {

    protected final static Logger LOG = LogManager.getLogger(AbstractExecution.class);

    private final static String RELATIVE_TEMPORARY_ARCHIVE_DIRECTORY_PATH = "/${extraction_execution_id}-tmp-archive-directory";

    private final static String ARCHIVE_FILE_PATH_BASE = "/${lang}/${update_frequency}";
    private final static String ARCHIVE_FILE_PATH_UPDATE = "/${update_date}";
    private final static String ARCHIVE_FILE_PATH_JOB_PATH = "/${job_path}";
    private final static String ARCHIVE_FILE_NAME = "/${extraction_configuration_id}-${extraction_configuration_type}-${lang}-${start_date}-${end_date}.tar.gz";

    private final static String ADHOC_ARCHIVE_FILE_JOB_PATH = "/${job_path}";
    private final static String ADHOC_ARCHIVE_FILE_NAME = "/${extraction_configuration_id}-${extraction_configuration_type}-${lang}.tar.gz";

    private final static String TEMPORARY_SPARQL_REPONSE = "/${extraction_execution_id}-tmp-sparql-response.xml";

    private final static String LOG_BASE_FORMAT = "Thread id %s";
    private final static String LOG_EXECUTION_FORMAT = LOG_BASE_FORMAT + " - ExtractionExecution %s: %s";

    /**
     * The license holder manger.
     */
    @Autowired
    protected LicenseHolderService licenseHolderService;

    /**
     * Cellar configuration.
     */
    @Autowired
    @Qualifier("cellarConfiguration")
    protected ICellarConfiguration cellarConfiguration;

    /**
     * Execution logics manager.
     */
    @Autowired
    @Qualifier("executionStateManager")
    protected ExecutionStateManager executionStateManager;

    /**
     * Constructs the absolute archive path to kept.
     * @param extractionExecution the corresponding execution
     * @return the absolute archive path to kept
     */
    protected String constructAbsoluteArchivePathToKept(final ExtractionExecution extractionExecution) {
        if (extractionExecution.getExtractionConfiguration().getConfigurationType() == CONFIGURATION_TYPE.STD) // Ad-hoc
            return this.cellarConfiguration.getCellarFolderLicenseHolderArchiveAdhocExtraction()
                    + this.constructRelativeArchivePathToKept(extractionExecution);
        else
            return this.cellarConfiguration.getCellarFolderLicenseHolderArchiveExtraction() // Frequencies
                    + this.constructRelativeArchivePathToKept(extractionExecution);
    }

    /**
     * Constructs the relative temporary SPARQL file path.
     * @param extractionExecution the corresponding execution
     * @return the relative temporary SPARQL file path
     */
    protected String contructRelativeTemporarySparqlFilePath(final ExtractionExecution extractionExecution) {
        return TEMPORARY_SPARQL_REPONSE.replace("${extraction_execution_id}", extractionExecution.getId().toString());
    }

    /**
     * Constructs the relative temporary archive directory path.
     * @param extractionExecution the corresponding execution
     * @return the relative temporary archive directory path
     */
    protected String contructRelativeTemporaryArchiveDirectoryPath(final ExtractionExecution extractionExecution) {
        return RELATIVE_TEMPORARY_ARCHIVE_DIRECTORY_PATH.replace("${extraction_execution_id}", extractionExecution.getId().toString());
    }

    /**
     * Constructs the relative archive file path.
     * @param extractionExecution the corresponding execution
     * @return the relative archive file path
     */
    protected String constructRelativeArchiveFilePath(final ExtractionExecution extractionExecution) {
        // Unix path normalized
        return FilenameUtils.separatorsToUnix(FilenameUtils.normalize(constructRelativeArchiveFilePath(
                extractionExecution.getExtractionConfiguration().getConfigurationType(),
                extractionExecution.getExtractionConfiguration().getLanguageBean().getCode(), extractionExecution.getEndDate(),
                extractionExecution.getStartDate(), extractionExecution.getEndDate(),
                extractionExecution.getExtractionConfiguration().getPath(), extractionExecution.getExtractionConfiguration().getId())));
    }

    /**
     * Constructs the relative archive file path.
     * @param configurationType the configuration type
     * @param jobPath the custom path of the job
     * @return the relative archive file path
     */
    protected String constructRelativeArchiveFilePathJobPath(final CONFIGURATION_TYPE configurationType, final String jobPath) {
        String relativeArchiveFilePathJobPath = "";

        if (configurationType != CONFIGURATION_TYPE.STD)
            relativeArchiveFilePathJobPath = ARCHIVE_FILE_PATH_JOB_PATH;
        else
            relativeArchiveFilePathJobPath = ADHOC_ARCHIVE_FILE_JOB_PATH;

        if (jobPath != null)
            relativeArchiveFilePathJobPath = relativeArchiveFilePathJobPath.replace("${job_path}", jobPath);

        return relativeArchiveFilePathJobPath;
    }

    /**
     * Constructs the relative archive file path.
     * @param configurationType the configuration type
     * @param languageCode the language code of the job
     * @param updateDate the update date
     * @param startDate the execution start date
     * @param endDate the execution end date
     * @param jobPath the custom path of the job
     * @param extractionConfigurationId the configuration identifier
     * @return the relative archive file path
     */
    protected String constructRelativeArchiveFilePath(final CONFIGURATION_TYPE configurationType, final String languageCode,
            final Date updateDate, final Date startDate, final Date endDate, final String jobPath, final Long extractionConfigurationId) {
        return constructRelativeArchivePathToKept(configurationType, languageCode) + constructRelativeArchivePathToClean(configurationType,
                languageCode, updateDate, startDate, endDate, jobPath, extractionConfigurationId);
    }

    /**
     * Constructs the relative archive path to kept.
     * @param configurationType the configuration type
     * @param languageCode the language code of the job
     * @return the relative archive path to kept
     */
    protected String constructRelativeArchivePathToKept(final CONFIGURATION_TYPE configurationType, final String languageCode) {
        return constructRelativeArchiveFilePathBase(configurationType, languageCode);
    }

    /**
     * Constructs the relative archive path to kept.
     * @param extractionExecution the execution
     * @return the relative archive path to kept
     */
    protected String constructRelativeArchivePathToKept(final ExtractionExecution extractionExecution) {
        return constructRelativeArchivePathToKept(extractionExecution.getExtractionConfiguration().getConfigurationType(),
                extractionExecution.getExtractionConfiguration().getLanguageBean().getCode());
    }

    /**
     * Constructs the relative archive path to clean.
     * @param configurationType the configuration type
     * @param languageCode the language code of the job
     * @param updateDate the update date
     * @param startDate the execution start date
     * @param endDate the execution end date
     * @param jobPath the custom path of the job
     * @param extractionConfigurationId the configuration identifier
     * @return the relative archive path to clean
     */
    protected String constructRelativeArchivePathToClean(final CONFIGURATION_TYPE configurationType, final String languageCode,
            final Date updateDate, final Date startDate, final Date endDate, final String jobPath, final Long extractionConfigurationId) {
        return constructRelativeArchiveFilePathUpdate(configurationType, updateDate)
                + constructRelativeArchiveFilePathJobPath(configurationType, jobPath)
                + constructArchiveFileName(configurationType, languageCode, startDate, endDate, extractionConfigurationId);
    }

    /**
     * Constructs the relative archive path to clean.
     * @param extractionExecution the execution
     * @return the relative archive path to clean
     */
    protected String constructRelativeArchivePathToClean(final ExtractionExecution extractionExecution) {
        return constructRelativeArchivePathToClean(extractionExecution.getExtractionConfiguration().getConfigurationType(),
                extractionExecution.getExtractionConfiguration().getLanguageBean().getCode(), extractionExecution.getEndDate(),
                extractionExecution.getStartDate(), extractionExecution.getEndDate(),
                extractionExecution.getExtractionConfiguration().getPath(), extractionExecution.getExtractionConfiguration().getId());
    }

    /**
     * Sets the license holder service.
     * @param licenseHolderService the service to set
     */
    public void setLicenseHolderService(final LicenseHolderService licenseHolderService) {
        this.licenseHolderService = licenseHolderService;
    }

    /**
     * Sets the cellar configuration.
     * @param cellarConfiguration the configuration to set
     */
    public void setCellarConfiguration(final ICellarConfiguration cellarConfiguration) {
        this.cellarConfiguration = cellarConfiguration;
    }

    /**
     * Adds an error concerning an execution.
     * @param extractionExecution the execution concerned by the error
     * @param exception the exception
     * @param append True if the error must be appended. Otherwise, false.
     */
    protected void error(final ExtractionExecution extractionExecution, final Exception exception, final boolean append) {
        String errorDescription = exception.getMessage();
        String errorStacktrace = ErrorUtils.getHtmlStacktrace(exception);

        if (append) {
            errorDescription += ErrorUtils.getErrorSeparator() + extractionExecution.getErrorDescription();
            errorStacktrace += ErrorUtils.getErrorSeparator() + extractionExecution.getErrorStacktrace();
        }

        // adds the error description to the execution
        extractionExecution.setErrorDescription(errorDescription);
        extractionExecution.setErrorStacktrace(errorStacktrace);
        this.licenseHolderService.updateExtractionExecution(extractionExecution);
    }

    /**
     * Logs an info concerning an execution.
     * @param logger the logger to use
     * @param extractionExecution the execution concerned
     * @param messageFormat the info format
     * @param args the arguments referenced by the messageFormat
     */
    protected static void info(final Logger logger, final ExtractionExecution extractionExecution, final String messageFormat,
                               final Object... args) {
        logger.info(String.format(LOG_EXECUTION_FORMAT, Thread.currentThread().getId(), extractionExecution.getId(),
                String.format(messageFormat, args)));
    }

    /**
     * Logs an error concerning an execution.
     * @param logger the logger to use
     * @param extractionExecution the execution concerned
     * @param exception the error description
     */
    protected static void error(final Logger logger, final ExtractionExecution extractionExecution, final Exception exception) {
        logger.error(String.format(LOG_EXECUTION_FORMAT, Thread.currentThread().getId(), extractionExecution.getId(),
                exception.getMessage()), exception);
    }

    /**
     * Logs an error concerning an execution.
     *
     * @param logger              the logger to use
     * @param extractionExecution the execution concerned
     * @param message             the error message
     * @param exception           the error description
     */
    protected static void error(final Logger logger, final ExtractionExecution extractionExecution, final String message,
                                final Exception exception) {
        logger.error(String.format(LOG_EXECUTION_FORMAT, Thread.currentThread().getId(), extractionExecution.getId(), message),
                exception);
    }

    /**
     * Constructs the relative archive file path base for update.
     * @param configurationType the configuration type
     * @param languageCode the language code
     * @param updateDate the update date
     * @return the relative archive file path base for update
     */
    public static String constructRelativeArchiveFilePathBaseUpdate(final CONFIGURATION_TYPE configurationType, final String languageCode,
            final Date updateDate) {
        return constructRelativeArchiveFilePathBase(configurationType, languageCode)
                + constructRelativeArchiveFilePathUpdate(configurationType, updateDate) + "/";
    }

    /**
     * Constructs the relative archive file path base.
     * @param configurationType the configuration type
     * @param languageCode the language code
     * @return the relative archive file path base for update
     */
    public static String constructRelativeArchiveFilePathBase(final CONFIGURATION_TYPE configurationType, final String languageCode) {
        String relativeArchiveFilePathBase = "";

        if (configurationType != CONFIGURATION_TYPE.STD) {
            relativeArchiveFilePathBase = ARCHIVE_FILE_PATH_BASE;
            if (languageCode != null)
                relativeArchiveFilePathBase = relativeArchiveFilePathBase.replace("${lang}", languageCode);
            if (configurationType != null)
                relativeArchiveFilePathBase = relativeArchiveFilePathBase.replace("${update_frequency}", configurationType.getDefinition());
        }

        return relativeArchiveFilePathBase;
    }

    /**
     * Constructs the relative archive file path for update.
     * @param configurationType the configuration type
     * @param updateDatethe the update date
     * @return the relative archive file path for update
     */
    public static String constructRelativeArchiveFilePathUpdate(final CONFIGURATION_TYPE configurationType, final Date updateDate) {
        String relativeArchiveFilePathUpdate = "";

        if (configurationType != CONFIGURATION_TYPE.STD) {
            relativeArchiveFilePathUpdate = ARCHIVE_FILE_PATH_UPDATE;
            if (updateDate != null)
                relativeArchiveFilePathUpdate = relativeArchiveFilePathUpdate.replace("${update_date}",
                        TimeUtils.formatTimestampForyyyyMMdd(updateDate)); // date formated
        }

        return relativeArchiveFilePathUpdate;
    }

    /**
     * Construct the archive file name corresponding to an execution.
     * @param configurationType the configuration type
     * @param languageCode the language code
     * @param startDate the start date of the execution
     * @param endDate the end date of the execution
     * @param extractionConfigurationId
     * @return the archive file name
     */
    public static String constructArchiveFileName(final CONFIGURATION_TYPE configurationType, final String languageCode,
            final Date startDate, final Date endDate, final Long extractionConfigurationId) {
        String archiveFileName = null;

        if (configurationType != CONFIGURATION_TYPE.STD) { // for a job
            archiveFileName = ARCHIVE_FILE_NAME;

            if (startDate != null)
                archiveFileName = archiveFileName.replace("${start_date}", TimeUtils.formatTimestampForyyyyMMdd(startDate));
            if (endDate != null)
                archiveFileName = archiveFileName.replace("${end_date}", TimeUtils.formatTimestampForyyyyMMdd(endDate));
        } else {
            archiveFileName = ADHOC_ARCHIVE_FILE_NAME;
        }

        if (languageCode != null)
            archiveFileName = archiveFileName.replace("${lang}", languageCode);
        if (extractionConfigurationId != null)
            archiveFileName = archiveFileName.replace("${extraction_configuration_id}", extractionConfigurationId.toString());
        if (configurationType != null)
            archiveFileName = archiveFileName.replace("${extraction_configuration_type}", configurationType.toString());

        return archiveFileName;
    }

    /**
     * Cleans the path.
     * @param path the path to clean
     * @return the cleaned path
     */
    public static String cleanPath(final String path) {
        String cleanedPath = FilenameUtils.separatorsToUnix(path);
        cleanedPath = cleanedPath.replaceAll("/+", "/");
        if (!cleanedPath.startsWith("/"))
            cleanedPath = "/" + cleanedPath;

        if (!cleanedPath.endsWith("/"))
            cleanedPath = cleanedPath + "/";

        cleanedPath = FilenameUtils.normalize(cleanedPath);
        return FilenameUtils.separatorsToUnix(cleanedPath);
    }
}
