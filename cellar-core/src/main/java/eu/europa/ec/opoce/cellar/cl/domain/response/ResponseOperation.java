package eu.europa.ec.opoce.cellar.cl.domain.response;

import java.io.Serializable;

/**
 * Class that contains informations about a creation or an update of a SIP.
 */
public class ResponseOperation implements Serializable {

    /** class's uid. */
    private static final long serialVersionUID = 8326607516293707545L;

    private String id;
    private ResponseOperationType operationType;
    private String lastVersion = "";
    private String lastMimeType = "";
    private String newVersion = "";

    public ResponseOperation() {
    }

    public ResponseOperation(String id, ResponseOperationType operationType) {
        this.id = id;
        this.operationType = operationType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ResponseOperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(ResponseOperationType operationType) {
        this.operationType = operationType;
    }

    public boolean isCreate() {
        return ResponseOperationType.CREATE.equals(this.operationType);
    }

    public boolean isAppend() {
        return ResponseOperationType.APPEND.equals(this.operationType);
    }

    public boolean isUpdate() {
        return ResponseOperationType.UPDATE.equals(this.operationType);
    }

    public boolean isDelete() {
        return ResponseOperationType.DELETE.equals(this.operationType);
    }

    public String getLastVersion() {
        return lastVersion;
    }

    public void setLastVersion(String lastVersion) {
        this.lastVersion = lastVersion;
    }

    public String getLastMimeType() {
        return lastMimeType;
    }

    public void setLastMimeType(String lastMimeType) {
        this.lastMimeType = lastMimeType;
    }

    public String getNewVersion() {
        return newVersion;
    }

    public void setNewVersion(String newVersion) {
        this.newVersion = newVersion;
    }

}
