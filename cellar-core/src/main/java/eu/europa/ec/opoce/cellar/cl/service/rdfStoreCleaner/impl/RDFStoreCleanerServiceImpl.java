/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.rdfStoreCleaner.impl
 *             FILE : RDFStoreCleanerServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 1, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.rdfStoreCleaner.impl;

import eu.europa.ec.opoce.cellar.ccr.service.database.DefaultTransactionService;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.dao.CleanerDifferenceDao;
import eu.europa.ec.opoce.cellar.cl.delegator.BatchDelegator;
import eu.europa.ec.opoce.cellar.cl.domain.AbstractBatchManager.ManagerStatus;
import eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner.CleanerDifference;
import eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner.CleanerDifferencePaginatedList;
import eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner.CleanerRunnable;
import eu.europa.ec.opoce.cellar.cl.domain.rdfStoreCleaner.RDFStoreCleanerManager;
import eu.europa.ec.opoce.cellar.cl.rdfStoreCleaner.DelegateCleaner;
import eu.europa.ec.opoce.cellar.cl.rdfStoreCleaner.delegator.HierarchyDelegator;
import eu.europa.ec.opoce.cellar.cl.service.rdfStoreCleaner.RDFStoreCleanerService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.CellarResource.CleanerStatus;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Map;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 1, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class RDFStoreCleanerServiceImpl extends DefaultTransactionService implements RDFStoreCleanerService {

    /**
     * The Cellar configuration storing runtime parameters.
     */
    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    /**
     * Threadpool for the tasks.
     */
    @Autowired
    @Qualifier("rdfStoreCleanerExecutor")
    private TaskExecutor rdfStoreCleanerExecutor;

    /** The rdf store cleaner manager. */
    @Autowired
    private RDFStoreCleanerManager rdfStoreCleanerManager;

    /** The cellar resource dao. */
    @Autowired
    private CellarResourceDao cellarResourceDao;

    /** The cleaner difference dao. */
    @Autowired
    private CleanerDifferenceDao cleanerDifferenceDao;

    /** The delegate cleaner. */
    @Autowired
    private DelegateCleaner delegateCleaner;

    /** {@inheritDoc} */
    @Override
    public boolean cleanRDFStoreUnknown() {
        final int batchSize = this.cellarConfiguration.getCellarServiceRDFStoreCleanerHierarchiesBatchSize();
        final Collection<CellarResource> cellarResources = this.cellarResourceDao.findRootsPerCleanerStatus(CleanerStatus.UNKNOWN,
                batchSize);

        HierarchyDelegator delegator = null;
        for (final CellarResource cellarResource : cellarResources) {
            delegator = new HierarchyDelegator(cellarResource);

            this.delegateCleaner.startProcessing(delegator);

            this.rdfStoreCleanerExecutor.execute(new CleanerRunnable(delegator, delegateCleaner));
        }

        return cellarResources.size() >= batchSize;
    }

    /** {@inheritDoc} */
    @Override
    public boolean cleanRDFStoreCorrupt() {
        final int batchSize = this.cellarConfiguration.getCellarServiceRDFStoreCleanerResourcesBatchSize();
        final Collection<CellarResource> cellarResources = this.cellarResourceDao
                .findMetadataResourcesPerCleanerStatus(CleanerStatus.CORRUPT, batchSize);

        BatchDelegator delegator = null;
        for (final Map.Entry<String, Collection<CellarResource>> entry : BatchDelegator.getBatchesByCellarBaseId(cellarResources)
                .entrySet()) {
            delegator = new BatchDelegator(entry.getKey(), entry.getValue());

            this.delegateCleaner.startProcessing(delegator);

            this.rdfStoreCleanerExecutor.execute(new CleanerRunnable(delegator, delegateCleaner));
        }

        return cellarResources.size() >= batchSize;
    }

    /** {@inheritDoc} */
    @Override
    public void findCleanerDifferences(final CleanerDifferencePaginatedList cdpl) {
        this.cleanerDifferenceDao.findCleanerDifferences(cdpl);
    }

    /** {@inheritDoc} */
    @Override
    public CleanerDifference getCleanerDifference(final Long id) {
        return this.cleanerDifferenceDao.getObject(id);
    }

    /** {@inheritDoc} */
    @Override
    public ManagerStatus getCleaningStatus() {
        return this.rdfStoreCleanerManager.getManagerStatus();
    }

    /** {@inheritDoc} */
    @Override
    public long getNumberMetadataResources() {
        return this.cellarResourceDao.getNumberOfMetadataResources();
    }

    /** {@inheritDoc} */
    @Override
    public long getNumberMetadataResourcesDiagnosable() {
        return this.cellarResourceDao.getNumberOfMetadataResourcesDiagnosable();
    }

    /** {@inheritDoc} */
    @Override
    public long getNumberMetadataResourcesFixable() {
        return this.cellarResourceDao.getNumberOfMetadataResourcesFixable();
    }

    /** {@inheritDoc} */
    @Override
    public long getNumberMetadataResourcesError() {
        return this.cellarResourceDao.getNumberOfMetadataResourcesError();
    }

    /** {@inheritDoc} */
    @Override
    public long getNumberRowsCleanerDifference() {
        return this.cleanerDifferenceDao.getCleanerDifferenceCount().longValue();
    }

    /** {@inheritDoc} */
    @Override
    public void startCleaning() {
        this.rdfStoreCleanerManager.startWorking();
    }

    /** {@inheritDoc} */
    @Override
    public void stopCleaning() {
        this.rdfStoreCleanerManager.stopWorking();
    }
}
