/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState
 *             FILE : AbstractCleaningState.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 16, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState;

import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Component;

/**
 * <class_description> Abstract class of the cleaning logic.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 16, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component
public abstract class AbstractCleaning extends AbstractTask {

    /**
     * Cleans the execution.
     * @param extractionExecution the execution
     * @param cleanFinalArchive True if the final archive must be deleted. Otherwise, false.
     * @throws Exception
     */
    public abstract void clean(final ExtractionExecution extractionExecution, final boolean cleanFinalArchive) throws Exception;

    /**
     * Cleans the list of executions.
     * @param extractionExecutions the list of executions
     * @param cleanFinalArchive True if the final archives must be deleted. Otherwise, false.
     */
    public void clean(final List<ExtractionExecution> extractionExecutions, final boolean cleanFinalArchive) {
        for (final ExtractionExecution extractionExecution : extractionExecutions) {
            try {
                this.clean(extractionExecution, cleanFinalArchive);
            } catch (final Exception exception) {
                error(LOG, extractionExecution, "Cannot clean the execution " + extractionExecution.getId(), exception);
            }
        }
    }

    /**
     * Deletes the file corresponding to the path.
     * @param filePath the path of the file to delete
     * @throws IOException
     */
    protected void cleanFile(final String filePath) throws IOException {
        final File file = new File(filePath);
        final boolean exists = file.exists();
        if (exists) {
            FileUtils.forceDelete(file);
        }
    }

    /**
     * Cleans the archive tree.
     * @param extractionExecution the execution
     */
    protected void cleanArchiveTree(final ExtractionExecution extractionExecution) {
        String pathToKept = super.constructAbsoluteArchivePathToKept(extractionExecution);
        pathToKept = FilenameUtils.normalizeNoEndSeparator(pathToKept);
        String pathToClean = super.licenseHolderService.getAbsoluteArchiveFilePath(extractionExecution); // the path to clean
        if (pathToClean != null) {
            pathToClean = FilenameUtils.normalizeNoEndSeparator(pathToClean);

            File file = null;
            while (pathToClean.contains(pathToKept) && !pathToClean.equals(pathToKept)) {
                file = new File(pathToClean);
                if (!file.delete()) {
                    return; // cannot delete
                }
                pathToClean = FilenameUtils.getFullPathNoEndSeparator(pathToClean);
            }
        }
    }

    /**
     * Cleans the error description in the execution.
     * @param extractionExecution the execution
     */
    protected void cleanError(final ExtractionExecution extractionExecution) {
        extractionExecution.setErrorDescription(null);
        extractionExecution.setErrorStacktrace(null);
    }
}
