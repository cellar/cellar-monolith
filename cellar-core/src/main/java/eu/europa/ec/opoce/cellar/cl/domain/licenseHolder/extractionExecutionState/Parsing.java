/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState
 *             FILE : Parsing.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 5, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState;

import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution.EXECUTION_STATUS;
import eu.europa.ec.opoce.cellar.cl.service.client.SparqlExecutingService;
import eu.europa.ec.opoce.cellar.cl.service.client.UriFoundDelegator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;

/**
 * <class_description> Parsing logic.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 5, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component("parsing")
public class Parsing extends AbstractTask {

    /**
     * Logger instance.
     */
    private static final Logger LOG = LogManager.getLogger(Parsing.class);

    @Autowired
    private SparqlExecutingService sparqlExecutingService;

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractTask execute(final ExtractionExecution extractionExecution) throws Exception {
        try {
            final String absoluteSparqlTempFilePath = super.licenseHolderService.getAbsoluteSparqlTempFilePath(extractionExecution);
            info(LOG, extractionExecution, "Parsing the SPARQL response %s", absoluteSparqlTempFilePath);

            this.sparqlExecutingService.parseSparqlReponse(absoluteSparqlTempFilePath, new UriFoundDelegator() {

                private final HashSet<String> workIds = new HashSet<String>();
                private final String uriPrefix = cellarConfiguration.getCellarUriDisseminationBase() + "resource/";

                @Override
                public void onUri(String uri) {
                    if (uri.startsWith(uriPrefix)) {
                        uri = uri.substring(this.uriPrefix.length()); // reduces the identifier size
                        this.workIds.add(uri);
                    }
                }

                @Override
                public void onEnd() {
                    if (!this.workIds.isEmpty()) { // saves the current batch
                        licenseHolderService.addWorkIdentifiers(extractionExecution, workIds);
                        info(LOG, extractionExecution, "Parsing returns %s elements", this.workIds.size());
                    }
                }
            });

            return this.next(extractionExecution);
        } catch (Exception exception) {
            this.error(extractionExecution, exception);
            error(LOG, extractionExecution, exception);
            throw exception;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractTask next(final ExtractionExecution extractionExecution) {
        extractionExecution.setExecutionStatus(EXECUTION_STATUS.ARCHIVING_INIT);
        super.licenseHolderService.updateExtractionExecution(extractionExecution);

        return super.executionStateManager.getTaskState(EXECUTION_STATUS.ARCHIVING_INIT);
    }

    /**
     * Error.
     * @param extractionExecution the execution
     * @param exception the error description
     */
    protected void error(final ExtractionExecution extractionExecution, final Exception exception) {
        extractionExecution.setExecutionStatus(EXECUTION_STATUS.PARSING_ERROR);
        super.error(extractionExecution, exception, false);
    }
}
