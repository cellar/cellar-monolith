/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.dao.impl
 *             FILE : BatchJobWorkIdentifierDaoImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 11, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao.impl;

import eu.europa.ec.opoce.cellar.cl.dao.BatchJobWorkIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob.BATCH_JOB_TYPE;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJobWorkIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.common.CellarPaginatedList;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <class_description> Batch job work identifier dao.
 *
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 11, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Repository
public class BatchJobWorkIdentifierDaoImpl extends BaseDaoImpl<BatchJobWorkIdentifier, Long> implements BatchJobWorkIdentifierDao {

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> findWorkIds(final Long batchJobId, final BATCH_JOB_TYPE jobType, final int first, final int last) {
        return getHibernateTemplate().execute(session -> {
            final Query queryObject = session.getNamedQuery("findWorkIdsByBatchJobId");
            queryObject.setFirstResult(first);
            queryObject.setMaxResults(last - first);
            queryObject.setParameter(0, batchJobId);
            queryObject.setParameter(1, jobType);
            return queryObject.list();
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CellarPaginatedList<BatchJobWorkIdentifier> findWorkIdentifiers(final Long batchJobId, final BATCH_JOB_TYPE jobType,
            final CellarPaginatedList<BatchJobWorkIdentifier> workIdentifiersList) {
        return getHibernateTemplate().execute(session -> {
            final Query queryCount = session.getNamedQuery("countWorkIdentifiersByBatchJobId");
            queryCount.setParameter("batchJobId", batchJobId);
            queryCount.setParameter("jobType", jobType);
            queryCount.setParameter("filterValue", "%" + workIdentifiersList.getFilterValue() + "%");
            workIdentifiersList.setFullListSize(Integer.valueOf(queryCount.uniqueResult().toString()));

            final Query queryObject = session.createQuery(
                    "select w from BatchJobWorkIdentifier w where w.batchJob.id = :batchJobId and w.batchJob.jobType = :jobType and w.workId like :filterValue order by w."
                            + workIdentifiersList.getSortCriterion() + " " + workIdentifiersList.getSortDirectionCode());
            queryObject.setParameter("batchJobId", batchJobId);
            queryObject.setParameter("jobType", jobType);
            queryObject.setParameter("filterValue", "%" + workIdentifiersList.getFilterValue() + "%");
            queryObject.setFirstResult(workIdentifiersList.getObjectsPerPage() * (workIdentifiersList.getPageNumber() - 1));
            queryObject.setMaxResults(workIdentifiersList.getObjectsPerPage());

            workIdentifiersList.setList(queryObject.list());

            return workIdentifiersList;
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<BatchJobWorkIdentifier> findWorkIdentifiers(final Long batchJobId, final BATCH_JOB_TYPE jobType) {
        return super.findObjectsByNamedQuery("findWorkIdentifiersByBatchJobId", batchJobId, jobType);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteWorkIdentifiers(final Long batchJobId, final BATCH_JOB_TYPE jobType) {
        super.getHibernateTemplate().bulkUpdate("DELETE FROM BatchJobWorkIdentifier w WHERE w.batchJob.id = ?0", batchJobId);
    }

}
