package eu.europa.ec.opoce.cellar.support.sip;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;

/**
 * The Class BulkSIPFolderType.
 */
public class BulkSIPFolderType extends CellarReceptionFolderType {

    /** {@inheritDoc} */
    @Override
    public String getReceptionFolderAbsolutePath(ICellarConfiguration cellarConfiguration) {
        return cellarConfiguration.getCellarFolderBulkReception();
    }

    /** {@inheritDoc} */
    @Override
    public TYPE getSipWorkType() {
        return TYPE.BULK;
    }

}
