package eu.europa.ec.opoce.cellar.ccr.mets.service;

import org.springframework.context.annotation.Configuration;

import eu.europa.ec.opoce.cellar.ccr.velocity.VelocityMarshallerConfiguration;

@Configuration
public class MetsMarshallerConfiguration implements VelocityMarshallerConfiguration {

    /**
     * {@inheritDoc}
     */
    @Override
    public String getModelName() {
        return "metsDocument";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getViewName() {
        return "mets.vm";
    }
}
