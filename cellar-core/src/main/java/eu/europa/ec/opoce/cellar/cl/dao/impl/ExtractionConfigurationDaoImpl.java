/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.dao.impl
 *             FILE : ExtractionConfigurationDaoImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 5, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao.impl;

import eu.europa.ec.opoce.cellar.cl.dao.ExtractionConfigurationDao;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionConfiguration.CONFIGURATION_TYPE;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution.EXECUTION_STATUS;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.Arrays;
import java.util.List;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 5, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Repository
public class ExtractionConfigurationDaoImpl extends BaseDaoImpl<ExtractionConfiguration, Long> implements ExtractionConfigurationDao {

    private static final String SELECT = "select ";
    private static final String EXECUTION = "e";
    private static final String ID = "c.id";
    private static final String CONFIGURATION_NAME = "c.configurationName";
    private static final String EXTRACTION_LANGUAGE = "c.extractionLanguage";
    private static final String NUMBER_OF_DOCUMENTS = "(select count(w.id) from ExtractionWorkIdentifier w where w.extractionExecution.id = e.id)";
    private static final String TYPE = "c.configurationType";
    private static final String LAST_EXECUTION = "(select max(e.effectiveExecutionDate) from ExtractionExecution e where e.extractionConfiguration.id = c.id and e.executionStatus not in (:status1))";
    private static final String NEXT_EXECUTION = "(select max(e.endDate) from ExtractionExecution e where e.extractionConfiguration.id = c.id and e.executionStatus in (:status2))";
    private static final String PATH = "c.path";
    private static final String NUMBER_OF_EXECUTIONS = "(select count(e.id) from ExtractionExecution e where c.id = e.extractionConfiguration.id)";
    private static final String NUMBER_OF_ERRORS = "(select count(e.id) from ExtractionExecution e where c.id = e.extractionConfiguration.id and e.executionStatus in (:status3))";
    private static final String FROM_CONFIGURATION = " from ExtractionConfiguration c where c.configurationType in (:type)";
    private static final String FROM_CONFIGURATION_EXECUTION = " from ExtractionConfiguration c, ExtractionExecution e where c.configurationType in (:type) and c.id = e.extractionConfiguration.id";
    private static final String SEPARATOR = ", ";

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Object> findAdhocExtractionConfigurations() {
        return getHibernateTemplate().execute(session -> {
            final Query queryObject = session
                    .createQuery(SELECT + EXECUTION + SEPARATOR + NUMBER_OF_DOCUMENTS + FROM_CONFIGURATION_EXECUTION);

            final List<CONFIGURATION_TYPE> listType = Arrays.asList(CONFIGURATION_TYPE.STD);
            queryObject.setParameterList("type", listType);

            return queryObject.list();
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Object> findJobExtractionConfigurations() {
        return getHibernateTemplate().execute(session -> {
            final Query queryObject = session.createQuery(SELECT + ID + SEPARATOR + CONFIGURATION_NAME + SEPARATOR + EXTRACTION_LANGUAGE
                    + SEPARATOR + TYPE + SEPARATOR + LAST_EXECUTION + SEPARATOR + NEXT_EXECUTION + SEPARATOR + PATH + SEPARATOR
                    + NUMBER_OF_EXECUTIONS + SEPARATOR + NUMBER_OF_ERRORS + FROM_CONFIGURATION);

            final List<EXECUTION_STATUS> listStatus1 = Arrays.asList(EXECUTION_STATUS.NEW, EXECUTION_STATUS.PENDING);
            queryObject.setParameterList("status1", listStatus1);

            final List<EXECUTION_STATUS> listStatus2 = Arrays.asList(EXECUTION_STATUS.NEW, EXECUTION_STATUS.PENDING);
            queryObject.setParameterList("status2", listStatus2);

            final List<EXECUTION_STATUS> listStatus3 = Arrays.asList(EXECUTION_STATUS.ARCHIVING_ERROR, EXECUTION_STATUS.CLEANING_ERROR,
                    EXECUTION_STATUS.PARSING_ERROR, EXECUTION_STATUS.SPARQL_ERROR);
            queryObject.setParameterList("status3", listStatus3);

            final List<CONFIGURATION_TYPE> listType = Arrays.asList(CONFIGURATION_TYPE.STDD, CONFIGURATION_TYPE.STDW,
                    CONFIGURATION_TYPE.STDM);
            queryObject.setParameterList("type", listType);

            return queryObject.list();
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addExtractionConfiguration(final ExtractionConfiguration extractionConfiguration) {
        super.saveObject(extractionConfiguration);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ExtractionConfiguration findExtractionConfiguration(final Long id) {
        return super.getObject(id);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteExtractionConfiguration(final Long id) {
        ExtractionConfiguration extractionConfiguration = super.getObject(id);
        if (extractionConfiguration != null)
            super.deleteObject(extractionConfiguration);
    }
}
