/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *        FILE : SIPWatcherImpl.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 21-11-2021
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2021 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.impl.CellarPersistentConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditBuilder;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventAction;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventProcess;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventType;
import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.SIPPackage;
import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.SIPPackageProcessingStatus;
import eu.europa.ec.opoce.cellar.cl.runnable.SIPDependencyCheckRunnable;
import eu.europa.ec.opoce.cellar.cl.service.client.FileService;
import eu.europa.ec.opoce.cellar.cl.service.client.SIPDependencyChecker;
import eu.europa.ec.opoce.cellar.cl.service.client.SIPDependencyCheckerStorageService;
import eu.europa.ec.opoce.cellar.cl.service.client.SIPQueueManager;
import eu.europa.ec.opoce.cellar.common.concurrent.ListeningExecutorService;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;
import eu.europa.ec.opoce.cellar.ingestion.service.SIPWatcher;
import eu.europa.ec.opoce.cellar.logging.LogContext;
import eu.europa.ec.opoce.cellar.support.sip.*;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.stream.Collectors;

import static eu.europa.ec.opoce.cellar.AppConstants.NEWLINE;
import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.SIP_DEPENDENCY_CHECKER;

/**
 * Implementation of the SIP Watcher service, whose purpose
 * is to identify new packages to be processed by the SIP
 * dependency checker service, and packages that need to be
 * removed from the database.
 * @author EUROPEAN DYNAMICS S.A.
 */
@Service("sipWatcher")
public class SIPWatcherImpl implements SIPWatcher {

	private static final Logger LOG = LogManager.getLogger(SIPWatcherImpl.class);
	
	private SIPIterator sipIterator;
	
	private SIPDependencyChecker sipDependencyCheckerImpl;
	
	private final CellarPersistentConfiguration cellarConfiguration;
	
	private final ListeningExecutorService priorityThreadPoolExecutor;
	
	private final SIPDependencyCheckerStorageService sipDependencyCheckerStorageServiceImpl;
	
	private final SIPQueueManager sipQueueManager;
	
	private final SIPQueueManagerProxyImpl sipQueueManagerProxyImpl;
	
	private final FileService fileService;
	
	/**
	 * The map containing the packages that are being processed
	 * by the dependency checker executor.
	 */
	private ConcurrentMap<SIPWork, Boolean> dependencyCheckTreatments;

	
	@Autowired
	public SIPWatcherImpl(@Qualifier("cellarConfiguration") CellarPersistentConfiguration cellarConfiguration,
			SIPDependencyChecker sipDependencyCheckerImpl, SIPDependencyCheckerStorageService sipDependencyCheckerStorageServiceImpl,
			SIPQueueManager sipQueueManager, SIPQueueManagerProxyImpl sipQueueManagerProxyImpl, FileService fileService,
			@Qualifier("sipDependencyCheckerExecutor") ListeningExecutorService sipDependencyCheckExecutor) {
		this.cellarConfiguration = cellarConfiguration;
		this.sipDependencyCheckerImpl = sipDependencyCheckerImpl;
		this.sipDependencyCheckerStorageServiceImpl = sipDependencyCheckerStorageServiceImpl;
		this.sipQueueManagerProxyImpl = sipQueueManagerProxyImpl;
		this.sipQueueManager = sipQueueManager;
		this.fileService = fileService;
		this.priorityThreadPoolExecutor = sipDependencyCheckExecutor;
	}
	
	@PostConstruct
	public void initialize() {
		this.dependencyCheckTreatments = new ConcurrentHashMap<>();
		this.sipIterator = new SIPIterator(this.cellarConfiguration, this.fileService);
		this.sipIterator.setCellarReceptionFolderTypes(new TreeSet<>());
        this.sipIterator.addCellarReceptionFolderTypes(new AuthenticOJSIPFolderType());
        this.sipIterator.addCellarReceptionFolderTypes(new DailySIPFolderType());
        this.sipIterator.addCellarReceptionFolderTypes(new BulkSIPFolderType());
        this.sipIterator.addCellarReceptionFolderTypes(new BulkLowPrioritySIPFolderType());
	}
	
	public void reconfigureThreadPoolExecutor() {
		this.priorityThreadPoolExecutor.setCorePoolSize(
				this.cellarConfiguration.getCellarServiceSipDependencyCheckerPoolThreads());
	}
	
	/**
	 * Extracts the information needed to assess the validity
	 * of the SIP files contained in the ingestion folders.
	 * If SIP files with duplicate name are detected among different
	 * ingestion folders, then the file residing in the folder of the highest
	 * priority is maintained.
	 * @param ingestionFoldersPackages the List of SIP packages
	 * to parse, as fetched from the ingestion folders.
	 * @return the extracted SIP info.
	 */
	private Map<String, SIPWorkInfo> extractSipInfoFromSIPWork(List<SIPWork> ingestionFoldersPackages) {
		return ingestionFoldersPackages.stream()
				.filter(sw -> StringUtils.isNotBlank(sw.getSipFile().getName()))
				.filter(sw -> sw.getCreationDate().getTime() != 0L)
				.map(sw -> new SIPWorkInfo(sw.getSipFile().getName(), sw.getCreationDate().getTime(), sw.getType()))
				.collect(Collectors.toMap(SIPWorkInfo::getName, swi -> swi, (swa, swb) -> swa));
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Async("sipWatcherExecutor")
	public void watchFoldersAsync() {
		watchFolders();
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	@LogContext(SIP_DEPENDENCY_CHECKER)
	public void watchFolders() {
		// Execute only if configuration property is enabled
		if (this.cellarConfiguration.isCellarServiceSipDependencyCheckerEnabled()) {
			// Get all packages contained in the ingestion folders and extract their info.
			List<SIPWork> ingestionFoldersPackages = this.sipIterator.getSIPList();
			Map<String, SIPWorkInfo> ingestionFoldersSipInfo = extractSipInfoFromSIPWork(ingestionFoldersPackages);

			
			// Acquire isolated modification access to the DB SIP queue.
			this.sipQueueManager.getSipQueueLock().lock();
			try {
				// Get all packages from the DB.
				List<SIPPackage> dbPackages = this.sipDependencyCheckerStorageServiceImpl.findAll();
				
				// Synchronize the DB tables with the ingestion folders.
				Set<SIPPackage> packagesToDelete = computePackagesToRemove(dbPackages, ingestionFoldersSipInfo);
				performDatabaseSync(packagesToDelete);

				// Submit new packages to the dependency checker executor.
				List<SIPWork> packagesToSubmit = computePackagesToSubmit(ingestionFoldersPackages, dbPackages, packagesToDelete);
				submitNewPackages(packagesToSubmit);
			} finally {
				// Always release the lock.
				if (this.sipQueueManager != null && this.sipQueueManager.getSipQueueLock() != null) {
					this.sipQueueManager.getSipQueueLock().unlock();
				}
			}
		}
	}

	/**
	 * Computes the packages that should be removed from the database.
	 * @param dbPackages the packages as obtained from the database.
	 * @param ingestionFoldersSipInfo the complementary information about each package
	 * retrieved from the ingestion folders.
	 * @return the set of packages that should be removed from the database.
	 */
	private Set<SIPPackage> computePackagesToRemove(List<SIPPackage> dbPackages, Map<String, SIPWorkInfo> ingestionFoldersSipInfo) {
		Set<SIPPackage> packagesToDelete = new HashSet<>();
		for (SIPPackage sp : dbPackages) {
			if (shouldRemoveEntryFromDb(ingestionFoldersSipInfo, sp)) {
				packagesToDelete.add(sp);
			}
		}
		return packagesToDelete;
	}
	
	/**
	 * Checks if the provided SIP_PACKAGE entry should be removed.
	 * @param ingestionFoldersSipInfo the complementary information about each package
	 * retrieved from the ingestion folders.
	 * @param sp the SIP_PACKAGE entry to check.
	 * @return true if the entry should be removed, false otherwise.
	 */
	private boolean shouldRemoveEntryFromDb(Map<String, SIPWorkInfo> ingestionFoldersSipInfo, SIPPackage sp) {
		return !ingestionFoldersSipInfo.containsKey(sp.getName())
					|| (!ingestionFoldersSipInfo.get(sp.getName()).isDbSipEntryValid(sp) && sp.getStatus() != SIPPackageProcessingStatus.INGESTING)
					|| sp.getStatus() == SIPPackageProcessingStatus.FINISHED;
	}
	
	/**
	 * Computes the packages that should be submitted to the SIP dependency
	 * checker executor.
	 * @param ingestionFoldersPackages the packages retrieved from the ingestion folders.
	 * @param dbPackages the list of package references that existed in the database before performing
	 * the database synchronization/cleanup.
	 * @param deletedPackages the package references that have been removed from the database as part
	 * of the database synchronization/cleanup.
	 * @return the list of packages that should be submitted to the executor.
	 */
	private List<SIPWork> computePackagesToSubmit(List<SIPWork> ingestionFoldersPackages, List<SIPPackage> dbPackages, Set<SIPPackage> deletedPackages) {
		Set<String> dbSipNames = dbPackages.stream()
				.map(SIPPackage::getName)
				.collect(Collectors.toSet());
		Set<String> deletedSipNames = deletedPackages.stream()
				.map(SIPPackage::getName)
				.collect(Collectors.toSet());
		dbSipNames.removeAll(deletedSipNames);
		return ingestionFoldersPackages.stream()
				.filter(p -> !dbSipNames.contains(p.getSipFile().getName()))
				.collect(Collectors.toList());
	}
	
	/**
	 * Performs synchronization of the database tables with the state of the ingestion folders.
	 * The package entries in the tables that do not have a corresponding package in
	 * the ingestion folders are removed, and the references towards them in the database queue
	 * are also updated appropriately.
	 * @param packagesToDelete the list of packages to be removed from the database.
	 */
	private void performDatabaseSync(Set<SIPPackage> packagesToDelete) {
		if (!packagesToDelete.isEmpty()) {
			// Perform cleanup of DB tables; remove all entries that do not have
			// a matching SIP in the ingestion folders (based on the SIP name).
			try {
				logPackagesToRemove(packagesToDelete);
				this.sipDependencyCheckerStorageServiceImpl.syncDatabaseTables(packagesToDelete);
				LOG.info("{} entries have been removed from table SIP_PACKAGE.", packagesToDelete.size());
			} catch (Exception e) {
				LOG.error("An error occurred while cleaning and synchronizing the database tables.", e);
			}
		}
	}
	
	/**
	 * Logs the entries to be removed from table SIP_PACKAGE.
	 * @param packagesToDelete the packages to be removed from table SIP_PACKAGE.
	 */
	private void logPackagesToRemove(Set<SIPPackage> packagesToDelete) {
		if (LOG.isDebugEnabled()) {
			StringBuilder sb = new StringBuilder();
			packagesToDelete.forEach(p -> sb.append(p).append(NEWLINE + "\t"));
			LOG.debug("The following entries will be removed from table SIP_PACKAGE:" + NEWLINE + "\t{}", sb);
		}
	}
	
	/**
	 * Submits the provided packages to the SIP dependency checker executor.
	 * @param packagesToSubmit the list of packages to submit.
	 */
	private void submitNewPackages(List<SIPWork> packagesToSubmit) {
		// Iterate over the non-processed packages and submit them
		// to the dependency checker executor.
		Set<SIPWork> submittedPackages = new HashSet<>();
		for (SIPWork packageToSubmit : packagesToSubmit) {
			if (!this.dependencyCheckTreatments.containsKey(packageToSubmit) && packageToSubmit.getSipFile().exists()) {
				// Perform an additional check to verify that the SIP to be submitted
				// has not been processed yet.
				String sipName = packageToSubmit.getSipFile().getName();
				if (this.sipDependencyCheckerStorageServiceImpl.findSipBySipName(sipName) == null) {
					this.dependencyCheckTreatments.put(packageToSubmit, true);
					this.priorityThreadPoolExecutor.submit(new SIPDependencyCheckRunnable(packageToSubmit, this.sipDependencyCheckerImpl,
							this.sipDependencyCheckerStorageServiceImpl, this.cellarConfiguration, this.sipQueueManagerProxyImpl, this.dependencyCheckTreatments));
					submittedPackages.add(packageToSubmit);
				}
			}
		}
		logSubmittedPackages(submittedPackages);
	}
	
	/**
	 * Logs the packages submitted to the SIPDependencyChecker.
	 * @param submittedPackages the packages submitted to the SIPDependencyChecker.
	 */
	private void logSubmittedPackages(Set<SIPWork> submittedPackages) {
		if (!submittedPackages.isEmpty()) {
			//Log the finding of packages
			AuditBuilder.get(AuditTrailEventProcess.Ingestion).withAction(AuditTrailEventAction.Package)
					.withType(AuditTrailEventType.Found)
					.withMessage("{} packages have been submitted to the SIPDependencyChecker")
					.withMessageArgs(submittedPackages.size())
					.withLogger(LOG).withLogDatabase(false).logEvent();
			if (LOG.isDebugEnabled()) {
				StringBuilder sb = new StringBuilder();
				submittedPackages.forEach(sw -> sb.append(sw).append(NEWLINE + "\t"));
				LOG.debug("The following packages have been submitted to the SIPDependencyChecker:" + NEWLINE + "\t{}", sb);
			}
		}
	}
	
	/**
	 * Helper class containing additional attributes
	 * of the SIP files located in the ingestion folders,
	 * which are in turn needed to assess whether a corresponding
	 * package entry in the database (if it exists) is valid or not.
	 * @author EUROPEAN DYNAMICS S.A.
	 */
	private class SIPWorkInfo {
		/**
		 * The file name of the package.
		 */
		private String name;
		/**
		 * The last modification date of the file.
		 */
		private long timestamp;
		/**
		 * The file type.
		 */
		private TYPE type;
		
		private SIPWorkInfo(String name, long timestamp, TYPE type) {
			this.name = name;
			this.timestamp = timestamp;
			this.type = type;
		}

		private String getName() {
			return name;
		}
		
		private boolean isDbSipEntryValid(SIPPackage dbPackage) {
			return this.name.equals(dbPackage.getName()) && this.timestamp <= dbPackage.getDetectionDate().getTime() && this.type == dbPackage.getType();
		}	
	}
	
}
