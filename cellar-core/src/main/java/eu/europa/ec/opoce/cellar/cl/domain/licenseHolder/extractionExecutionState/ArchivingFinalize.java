/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState
 *             FILE : ArchivingFinalize.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 12, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState;

import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution.EXECUTION_STATUS;
import eu.europa.ec.opoce.cellar.cl.service.client.ExtractionArchivingService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * <class_description> Archiving finalize logic.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 12, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component("archivingFinalize")
public class ArchivingFinalize extends AbstractTask {

    private static final Logger LOG = LogManager.getLogger(ArchivingFinalize.class);

    @Autowired
    private ExtractionArchivingService extractionArchivingService;

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractTask execute(final ExtractionExecution extractionExecution) throws Exception {
        try {
            extractionExecution.setArchiveFilePath(super.constructRelativeArchiveFilePath(extractionExecution));
            final String absoluteArchiveFilePath = super.licenseHolderService.getAbsoluteArchiveFilePath(extractionExecution);

            info(LOG, extractionExecution, "Archiving finalize generates %s", absoluteArchiveFilePath);

            // constructs the tar.gz archive
            this.extractionArchivingService.createTarGzOfFiles(
                    super.licenseHolderService.getAbsoluteArchiveTempDirectoryPath(extractionExecution), absoluteArchiveFilePath);

            return this.next(extractionExecution);
        } catch (Exception exception) {
            this.error(extractionExecution, exception);
            error(LOG, extractionExecution, exception);
            throw exception;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractTask next(final ExtractionExecution extractionExecution) {
        extractionExecution.setExecutionStatus(EXECUTION_STATUS.DONE);
        super.licenseHolderService.updateExtractionExecution(extractionExecution);

        return null;
    }

    /**
     * Error
     * @param extractionExecution the execution
     * @param exception the error description
     */
    protected void error(final ExtractionExecution extractionExecution, final Exception exception) {
        extractionExecution.setExecutionStatus(EXECUTION_STATUS.ARCHIVING_ERROR);
        super.error(extractionExecution, exception, false);
    }
}
