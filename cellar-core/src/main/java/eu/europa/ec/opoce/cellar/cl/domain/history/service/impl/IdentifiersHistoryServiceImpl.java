/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : CellarIdentifier.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 9 mars 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.history.service.impl;

import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.ccr.service.database.DefaultTransactionService;
import eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.history.IdentifiersHistory;
import eu.europa.ec.opoce.cellar.cl.domain.history.dao.IdentifiersHistoryDao;
import eu.europa.ec.opoce.cellar.cl.domain.history.service.IdentifiersHistoryService;
import eu.europa.ec.opoce.cellar.common.util.DigitalObjectUtils;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.PidManagerException;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The Class IdentifiersHistoryServiceImpl.
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 9 mars 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
@Transactional
public class IdentifiersHistoryServiceImpl extends DefaultTransactionService implements IdentifiersHistoryService {

    private static final Logger LOG = LogManager.getLogger(IdentifiersHistoryServiceImpl.class);

    /** The identifiers history dao. */
    private IdentifiersHistoryDao identifiersHistoryDao;

    @Autowired
    public IdentifiersHistoryServiceImpl(IdentifiersHistoryDao identifiersHistoryDao) {
        this.identifiersHistoryDao = identifiersHistoryDao;
    }

    /** {@inheritDoc} */
    @Override
    public void delete(final Long id) {
        this.identifiersHistoryDao.delete(id);
    }

    /** {@inheritDoc} */
    @Override
    public String generateCellarID(final DigitalObject digitalObject, final String parentId, final Integer cardinal, final TYPE sipType) {
        LOG.debug(IConfiguration.INGESTION, "Generate ID for {}, parentID={}, cardinal={}, sipType={}",
                digitalObject, parentId, cardinal, sipType);
        // in case we already generated a CELLAR ID in the past and we try to recreate it, we should just return it instead
        String result = getCellarIdAlreadyGeneratedIfCardinalMatchesId(digitalObject, cardinal);
        LOG.debug(IConfiguration.INGESTION, "Cardinal matches ID: {}", result != null);
        if (StringUtils.isNotBlank(result)) {
            return result;
        }

        // else try to find a matching CELLAR ID in the history_pid_cid table based on the production identifier
        result = getCellarIdFromHistoryIfApplicable(digitalObject);
        LOG.debug(IConfiguration.INGESTION, "Doesn't match cardinal, cellar ID from HISTORY_PID_CID ? => {}",
                result != null ? ("found ! " + result) : "(not found)");
        if (StringUtils.isNotBlank(result)) {
            return result;
        }

        // for expressions|events|manifestations
        // finally pick the ID which is the maximum of history table, the input cardinal and same level digital object
        if (result == null) {
            Integer usedCardinal = cardinal;

            final DigitalObjectType type = digitalObject.getType();
            if (DigitalObjectType.EXPRESSION.equals(type) || DigitalObjectType.MANIFESTATION.equals(type) || DigitalObjectType.EVENT.equals(type)) {
                int nextIndexInDigitalObjectHierarchy = getNextIdOfSameLevelDigitalObject(digitalObject);
                int nextIndexFromHistory = getNextIndexFromHistory(parentId, type);
                usedCardinal = Math.max(Math.max(nextIndexInDigitalObjectHierarchy, nextIndexFromHistory), cardinal);
            }
            result = type.generateTimeBasedCellarID(parentId, usedCardinal, sipType);
            LOG.debug(IConfiguration.INGESTION, "ID generated : {}", result);
        }
        return result;
    }

    private String getCellarIdAlreadyGeneratedIfCardinalMatchesId(DigitalObject digitalObject, Integer cardinal) {
        ContentIdentifier contentIdentifier = digitalObject.getCellarId();
        if (contentIdentifier != null) {
            String cellarId = contentIdentifier.getIdentifier();
            if (cellarId.contains(".")) {
                int cellarIdIndex = Integer.parseInt(cellarId.substring(cellarId.lastIndexOf(".") + 1));
                if (cellarIdIndex == cardinal) {
                    return cellarId;
                }
            }
        }
        return null;
    }

    @Nullable
    private String getCellarIdFromHistoryIfApplicable(DigitalObject digitalObject) {
        String result = null;

        final List<String> productionIds = getProductionIdList(digitalObject);

        // check if there was already a record, in that case use the identifier found in history table
        final IdentifiersHistory latest = this.identifiersHistoryDao.getLatest(productionIds);
        if (latest != null) {
            //reuse cellar id
            result = latest.getCellarId();
            throwExceptionIfDigitalObjectHierarchyViolated(digitalObject, result);
        }
        return result;
    }

    private int getNextIdOfSameLevelDigitalObject(DigitalObject digitalObject) {
        int index = 0;
        DigitalObject parent = digitalObject.getParentObject();
        if (parent != null) {
            List<DigitalObject> sameLevelDOInHierarchyList = parent.getChildObjects();
            for (DigitalObject sameLevelDOInHierarchy : sameLevelDOInHierarchyList) {
                ContentIdentifier contentIdentifier = sameLevelDOInHierarchy.getCellarId();
                if (contentIdentifier != null) { // at this point, we are not sure to already have cellar id in the DO hierarchy
                    String cellarId = contentIdentifier.getIdentifier();
                    if (cellarId.contains(".")) { // we should only be here in case of expr, manif, event
                        int cellarIdIndex = Integer.parseInt(cellarId.substring(cellarId.lastIndexOf(".") + 1));
                        if (cellarIdIndex > index) {
                            index = cellarIdIndex;
                        }
                    }
                }
            }
        }
        return ++index;
    }

    private void throwExceptionIfDigitalObjectHierarchyViolated(DigitalObject digitalObject, String result) {
        final List<DigitalObject> parents = DigitalObjectUtils.getParents(digitalObject);
        for (final DigitalObject parent : parents) { //if a parent has a different UUID pattern for its cellar ID throw exception
            final ContentIdentifier parentCID = parent.getCellarId();
            final String parentCIDString = parentCID.getIdentifier();
            if (!result.contains(parentCIDString)) {//the child CID should be an extension of the parent CID
                throw ExceptionBuilder.get(PidManagerException.class).withCode(CoreErrors.E_036)
                        .withMessage(
                                "The digital object [{}] has an historic CID [{}] that does not match the CID pattern of the current parent digital object [{}]")
                        .withMessageArgs(Arrays.toString(digitalObject.getContentids().toArray()), result, parentCIDString).build();
            }
        }
    }

    @Nonnull
    private List<String> getProductionIdList(DigitalObject digitalObject) {
        return digitalObject.getContentids().stream()
                .map(ContentIdentifier::getIdentifier)
                .collect(Collectors.toList());
    }

    /**
     * Compute the next cardinal index from the historic.
     * retrieve the biggest expression/event/manifestation cardinal in the historic and increment it
     * return 1 by default
     *
     * @param parentId the parent id
     * @param type the type
     * @return the integer
     */
    private int getNextIndexFromHistory(final String parentId, final DigitalObjectType type) {
        int result = -1;
        final List<IdentifiersHistory> matchingCellarIds = this.identifiersHistoryDao.getAllRecordsStartingWithCellarId(parentId);
        if ((matchingCellarIds != null) && !matchingCellarIds.isEmpty()) {
            //w.: cellar:3a7eff8b-bef3-4550-8d82-21a5f06a167d
            //e.: cellar:3a7eff8b-bef3-4550-8d82-21a5f06a167d.0004
            //m.: cellar:3a7eff8b-bef3-4550-8d82-21a5f06a167d.0004.01
            //i.: cellar:3a7eff8b-bef3-4550-8d82-21a5f06a167d.0004.01/XXX
            final List<Integer> cardinals = matchingCellarIds.stream().map(historyRecord -> {
                final String historyCellarId = historyRecord.getCellarId();
                String cardinalString = StringUtils.substringAfter(historyCellarId, ".");
                if (DigitalObjectType.EXPRESSION.equals(type) || DigitalObjectType.EVENT.equals(type)) {
                    if (cardinalString.contains(".")) {
                        cardinalString = StringUtils.substringBefore(cardinalString, ".");
                    }
                } else if (DigitalObjectType.MANIFESTATION.equals(type)) {
                    cardinalString = StringUtils.substringAfter(cardinalString, ".");
                    if (cardinalString.contains("/")) {
                        cardinalString = StringUtils.substringBefore(cardinalString, "/");
                    }
                }
                return !cardinalString.isEmpty() ? Integer.parseInt(cardinalString) : 0;
            }).collect(Collectors.toList());
            if (cardinals != null) {
                Collections.sort(cardinals);//ASC
                result = cardinals.get(cardinals.size() - 1) + 1; //get next
            }
        }
        return result;
    }

}
