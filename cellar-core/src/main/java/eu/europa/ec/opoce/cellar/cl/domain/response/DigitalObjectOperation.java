package eu.europa.ec.opoce.cellar.cl.domain.response;

import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * DigitalObjectOperation used for a struct map response.
 * @author dcraeye
 *
 */
public class DigitalObjectOperation implements Serializable {

    /** class's uid. */
    private static final long serialVersionUID = 8326607516293707542L;

    /** The operation type. */
    private ResponseOperationType operationType;

    /** The digital object type. */
    private DigitalObjectType digitalObjectType;

    /** The cellar identifier operation. */
    private ResponseOperation cellarIdentifierOperation;

    /** The production identifier operations. */
    private List<ResponseOperation> productionIdentifierOperations = new LinkedList<ResponseOperation>();

    /** The metadata identifier operations. */
    private List<MetadataResponseOperation> metadataIdentifierOperations = new LinkedList<MetadataResponseOperation>();

    /** The content identifier operations. */
    private List<ContentResponseOperation> contentIdentifierOperations = new LinkedList<ContentResponseOperation>();

    /** The for rollback purposes.
     *  During an operation DigitalObjectOperation objects are created to list the actions to perform
     *  During  a delete it is necessary to keep track of the objects that have to be rolled-back and the objects that are supposed to be deleted
     *  this flag is to distinguish these sets of objects
     *  */
    private boolean forRollbackPurposes;

    /**
     * Instantiates a new digital object operation.
     */
    public DigitalObjectOperation() {
    }

    /**
     * Instantiates a new digital object operation.
     *
     * @param digitalObject the digital object
     * @param operationType the operation type
     */
    public DigitalObjectOperation(DigitalObject digitalObject, ResponseOperationType operationType) {
        this(digitalObject.getCellarId().getIdentifier(), digitalObject.getType(), operationType);
    }

    /**
     * Instantiates a new digital object operation.
     *
     * @param cellarId the cellar id
     * @param operationType the operation type
     */
    public DigitalObjectOperation(String cellarId, ResponseOperationType operationType) {
        this(cellarId, null, operationType);
    }

    /**
     * Instantiates a new digital object operation.
     *
     * @param cellarId the cellar id
     * @param digitalObjectType the digital object type
     * @param operationType the operation type
     */
    public DigitalObjectOperation(String cellarId, DigitalObjectType digitalObjectType, ResponseOperationType operationType) {
        this.cellarIdentifierOperation = new ResponseOperation(cellarId, operationType);
        this.operationType = operationType;
        this.digitalObjectType = digitalObjectType;
    }

    /**
     * Gets the operation type.
     *
     * @return the operation type
     */
    public ResponseOperationType getOperationType() {
        return operationType;
    }

    /**
     * Sets the operation type.
     *
     * @param operationType the new operation type
     */
    public void setOperationType(ResponseOperationType operationType) {
        this.operationType = operationType;
    }

    /**
     * Gets the digital object type.
     *
     * @return the digital object type
     */
    public DigitalObjectType getDigitalObjectType() {
        return digitalObjectType;
    }

    /**
     * Sets the digital object type.
     *
     * @param digitalObjectType the new digital object type
     */
    public void setDigitalObjectType(DigitalObjectType digitalObjectType) {
        this.digitalObjectType = digitalObjectType;
    }

    /**
     * Gets the cellar identifier operation.
     *
     * @return the cellar identifier operation
     */
    public ResponseOperation getCellarIdentifierOperation() {
        return cellarIdentifierOperation;
    }

    /**
     * Sets the cellar identifier operation.
     *
     * @param cellarIdentifierOperation the new cellar identifier operation
     */
    public void setCellarIdentifierOperation(ResponseOperation cellarIdentifierOperation) {
        this.cellarIdentifierOperation = cellarIdentifierOperation;
    }

    /**
     * Gets the production identifier operations.
     *
     * @return the production identifier operations
     */
    public List<ResponseOperation> getProductionIdentifierOperations() {
        return productionIdentifierOperations;
    }

    /**
     * Sets the production identifier operations.
     *
     * @param productionIdentifierOperations the new production identifier operations
     */
    public void setProductionIdentifierOperations(List<ResponseOperation> productionIdentifierOperations) {
        this.productionIdentifierOperations = productionIdentifierOperations;
    }

    /**
     * Gets the metadata identifier operations.
     *
     * @return the metadata identifier operations
     */
    public List<MetadataResponseOperation> getMetadataIdentifierOperations() {
        return metadataIdentifierOperations;
    }

    /**
     * Sets the metadata identifier operations.
     *
     * @param metadataIdentifierOperations the new metadata identifier operations
     */
    public void setMetadataIdentifierOperations(List<MetadataResponseOperation> metadataIdentifierOperations) {
        this.metadataIdentifierOperations = metadataIdentifierOperations;
    }

    /**
     * Gets the content identifier operations.
     *
     * @return the content identifier operations
     */
    public List<ContentResponseOperation> getContentIdentifierOperations() {
        return contentIdentifierOperations;
    }

    /**
     * Sets the content identifier operations.
     *
     * @param contentIdentifierOperations the new content identifier operations
     */
    public void setContentIdentifierOperations(List<ContentResponseOperation> contentIdentifierOperations) {
        this.contentIdentifierOperations = contentIdentifierOperations;
    }

    /**
     * Adds the production identifier.
     *
     * @param identifier the identifier
     * @param operationType the operation type
     */
    public void addProductionIdentifier(String identifier, ResponseOperationType operationType) {
        this.productionIdentifierOperations.add(new ResponseOperation(identifier, operationType));
    }

    /**
     * Adds the production identifier.
     *
     * @param productionIdentifierResponseOperation the production identifier response operation
     */
    public void addProductionIdentifier(ResponseOperation productionIdentifierResponseOperation) {
        this.productionIdentifierOperations.add(productionIdentifierResponseOperation);
    }

    /**
     * Adds the content identifier.
     *
     * @param cellarId the cellar id
     * @param identifier the identifier
     * @param operationType the operation type
     */
    public void addContentIdentifier(String cellarId, String identifier, ResponseOperationType operationType) {
        this.contentIdentifierOperations.add(new ContentResponseOperation(cellarId, identifier, operationType));
    }

    /**
     * Adds the content identifier.
     *
     * @param contentResponseOperation the content response operation
     */
    public void addContentIdentifier(ContentResponseOperation contentResponseOperation) {
        this.contentIdentifierOperations.add(contentResponseOperation);
    }

    /**
     * Adds the metadata identifier.
     *
     * @param cellarId the cellar id
     * @param datastreamName the datastream name
     * @param operationType the operation type
     */
    public void addMetadataIdentifier(String cellarId, String datastreamName, ResponseOperationType operationType) {
        this.metadataIdentifierOperations.add(new MetadataResponseOperation(cellarId, datastreamName, operationType));
    }

    /**
     * Adds the metadata identifier.
     *
     * @param metadataResponseOperation the metadata response operation
     */
    public void addMetadataIdentifier(MetadataResponseOperation metadataResponseOperation) {
        this.metadataIdentifierOperations.add(metadataResponseOperation);
    }

    /**
     * Checks if is for rollback purposes.
     *
     * @return true, if is for rollback purposes
     */
    public boolean isForRollbackPurposes() {
        return forRollbackPurposes;
    }

    /**
     * Sets the for rollback purposes.
     *
     * @param forRollbackPurposes the new for rollback purposes
     */
    public void setForRollbackPurposes(boolean forRollbackPurposes) {
        this.forRollbackPurposes = forRollbackPurposes;
    }

}
