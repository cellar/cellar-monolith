package eu.europa.ec.opoce.cellar.ccr.helper;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.cl.dao.ProductionIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.admin.ProductionIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;
import eu.europa.ec.opoce.cellar.cl.domain.response.DigitalObjectOperation;
import eu.europa.ec.opoce.cellar.cl.domain.response.ResponseOperationType;
import eu.europa.ec.opoce.cellar.cl.domain.response.StructMapOperation;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierResolver;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentStream;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.StructMapProcessorException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class DigitalObjectHelper {

    /** Logger instance. */
    private static final Logger LOGGER = LoggerFactory.getLogger(DigitalObjectHelper.class);

    /**
     * This method verifies that all digital objects under the given digital object have a valid production identifier.
     * If a digital object has not an existing production identifier, the method throws an exception.
     * @param pid the production identifier
     * @param digitalObject the digital object
     */
    public static void verifyThatNoDOChildrenShouldBeDeleted(final ProductionIdentifier pid, final DigitalObject digitalObject,
            final ProductionIdentifierDao productionIdentifierDao, final IdentifierService identifierService) {
        //construct a set with all child cellar id defined in mets (first level of child)
        final Set<String> doCellarIdInDMSet = new HashSet<>();
        for (final DigitalObject doChild : digitalObject.getAllChilds(true)) {
            final ProductionIdentifier doChildPid = ProductionIdentifierHelper.getProductionIdentifier(doChild, productionIdentifierDao);
            if (doChildPid != null) {
                doCellarIdInDMSet.add(doChildPid.getCellarIdentifier().getUuid());
            }
        }

        //Get all child cellar id defined in s3
        final List<Identifier> allChildren = identifierService.getTreeIdentifiers(pid.getCellarIdentifier().getUuid(), false);
        for (final Identifier identifier : allChildren) {
            if (!doCellarIdInDMSet.contains(identifier.getCellarId())) {
                throw ExceptionBuilder.get(StructMapProcessorException.class).withCode(CoreErrors.E_171).withMessage(
                        "Content operation doesn't accept to delete existing expression/manifestation [" + identifier.getCellarId() + "]")
                        .build();
            }
        }
    }

    /**
     * Delete recursively all digital object under an expression (depth transversal) or a manifestation.
     */
    public static void deleteNoMoreExistingDO(final ProductionIdentifier pid, final DigitalObject digitalObject,
            final StructMapOperation operation, final boolean expression, final boolean metadataOperation,
            final IdentifierService identifierService, final PidManagerService pidManagerService,
            final ProductionIdentifierDao productionIdentifierDao) {
        //construct a set with all child cellar id defined in mets (first level of child)
        final Set<String> doCellarIdInDMSet = new HashSet<>();
        for (final DigitalObject doChild : digitalObject.getChildObjects()) {
            final ProductionIdentifier doChildPid = ProductionIdentifierHelper.getProductionIdentifier(doChild, productionIdentifierDao);
            if (doChildPid != null) {
                doCellarIdInDMSet.add(doChildPid.getCellarIdentifier().getUuid());
            }
        }

        //Get all child cellar id defined in s3
        final List<Identifier> allChildren = identifierService.getTreeIdentifiers(pid.getCellarIdentifier().getUuid(), false);
        for (final Identifier identifier : allChildren) {

            if (expression && pidManagerService.isExpressionOrEvent(identifier.getCellarId())) {
                if (!doCellarIdInDMSet.contains(identifier.getCellarId())) {
                    deleteWemElementWithSubtree(identifier.getCellarId(), metadataOperation, operation, identifierService,
                            pidManagerService);
                }
            }

            if (!expression && pidManagerService.isManifestation(identifier.getCellarId())) {
                if (!doCellarIdInDMSet.contains(identifier.getCellarId())) {
                    deleteWemElementWithSubtree(identifier.getCellarId(), metadataOperation, operation, identifierService,
                            pidManagerService);
                }
            }
        }

    }

    /**
     * Delete recursively all digital object under an expression (depth transversal) or a manifestation.
     */
    public static void deleteDigitalObjectWithSubtree(final DigitalObject digitalObject,
            final StructMapOperation operation, final boolean metadataOperation, final IdentifierService identifierService,
            final PidManagerService pidManagerService) {

        final String cellarId = digitalObject.getCellarId().getIdentifier();
        final DigitalObjectType digitalObjectType = digitalObject.getType();

        if (digitalObjectType == DigitalObjectType.WORK //
                || digitalObjectType == DigitalObjectType.DOSSIER //
                || digitalObjectType == DigitalObjectType.AGENT //
                || digitalObjectType == DigitalObjectType.TOPLEVELEVENT //
                || digitalObjectType == DigitalObjectType.EXPRESSION //
                || digitalObjectType == DigitalObjectType.EVENT //
                || digitalObjectType == DigitalObjectType.MANIFESTATION) {
            deleteWemElementWithSubtree(cellarId, metadataOperation, operation, identifierService, pidManagerService);
        } else if (digitalObjectType == DigitalObjectType.ITEM) {
            // In fact, not yet possible to have ITEM here. Yet, cellar-mets.xsd allows only the types above.
            throw ExceptionBuilder.get(StructMapProcessorException.class).withCode(CoreErrors.E_171)
                    .withMessage(
                            "Content stream '{}' could not be deleted because direct deletion of content streams is not yet supported. Please delete the whole manifestation")
                    .withMessageArgs(cellarId).build();
        } else {
            throw ExceptionBuilder.get(StructMapProcessorException.class).withCode(CoreErrors.E_170)
                    .withMessage("The deletion of digital object of type '{}' is not supported")
                    .withMessageArgs(digitalObjectType.getLabel()).build();
        }
    }

    /**
     * Delete an WEM element with all sub-elements (if any).
     * @param cellarId the cellar identifier to delete.
     * @param metadataOperation if true an exception is thrown.
     * @param operation the response operation.
     */
    private static void deleteWemElementWithSubtree(final String cellarId, final boolean metadataOperation,
            final StructMapOperation operation, final IdentifierService identifierService, final PidManagerService pidManagerService) {
        // delete it with all children...
        // if it's a metadata operation and a child has a content, throw an exception.
        final List<Identifier> allChildrenCellarId = identifierService.getTreeIdentifiers(cellarId, true);
        for (final Identifier identifierToDelete : allChildrenCellarId) {
            final String cellarIdToDelete = identifierToDelete.getCellarId();

            if (pidManagerService.isWork(cellarIdToDelete) || pidManagerService.isExpressionOrEvent(cellarIdToDelete)
                    || pidManagerService.isManifestation(cellarIdToDelete)) {

                pidManagerService.deleteCellarIdentifier(cellarIdToDelete);
                final DigitalObjectOperation digitalObjectOperation = new DigitalObjectOperation(cellarIdToDelete,
                        ResponseOperationType.DELETE);
                operation.addOperation(digitalObjectOperation);
            } else {
                //it's manifestation's content node
                if (metadataOperation) {
                    throw ExceptionBuilder.get(StructMapProcessorException.class).withCode(CoreErrors.E_171)
                            .withMessage(
                                    "Metadata operation doesn't accept to delete manifestation with content [" + cellarIdToDelete + "]")
                            .build();
                } else {
                    // the content stream in S3 will be deleted with manifestation
                    // we remove only the corresponding cellar identifier
                    pidManagerService.deleteCellarIdentifier(cellarIdToDelete);

                    if (!cellarIdToDelete.contains("/") || !cellarIdToDelete.startsWith("cellar:")) {
                        LOGGER.error("The content stream cellar id '{}' does not contain a '/'.", cellarIdToDelete);
                    }
                }
            }
        }
    }

    /**
     * Find for all content identifier of a manifestation a corresponding cellar identifier.
     * If found, set it in data model.
     * If not found, throws an exception.
     * @param manifestation the manifestation digital object.
     * @throws StructMapProcessorException if no cellar identifier can be found for at least one content identifier.
     */
    public static void setCellarIdOnAllContent(final DigitalObject manifestation, final IdentifierResolver identifierResolver) {
        for (final ContentStream contentStream : manifestation.getContentStreams()) {
            CellarIdentifier currentContentCellarIdentifier = null;

            final List<String> contentIds = new LinkedList<>();
            for (final ContentIdentifier contentId : contentStream.getContentids()) {
                try {
                    contentIds.add(contentId.getIdentifier());
                    currentContentCellarIdentifier = identifierResolver.resolveToCellarId(contentId.getIdentifier());

                    if (currentContentCellarIdentifier != null) {
                        break;
                    }
                } catch (final IllegalArgumentException e) {
                    //We don't find this cellar id!
                }
            }
            if (currentContentCellarIdentifier != null) {
                contentStream.setCellarId(new ContentIdentifier(currentContentCellarIdentifier.getUuid()));
                contentStream.setCCR_datastream(StringUtils.substringAfterLast(currentContentCellarIdentifier.getUuid(), "/"));
            } else {
                throw ExceptionBuilder.get(StructMapProcessorException.class).withCode(CoreErrors.E_033)
                        .withMessage("Content id(s) [" + StringUtils.join(contentIds, ", ") + "] has(ve) no corresponding cellar Id")
                        .build();
            }
        }
    }

    /**
     * Set in the data model the cellar identifier generated previously.
     * @param digitalObject the digital object where to set the cellar identifier.
     * @param pid the production identifier which contains the cellar identifier to set.
     */
    public static void assignCellarIdentifier(final DigitalObject digitalObject, final ProductionIdentifier pid) {
        DigitalObject parentDigitalObject = digitalObject.getParentObject();
        if (parentDigitalObject != null) {
            String parentCellarId = parentDigitalObject.getCellarId().getIdentifier();
            String cellarIdToAssignToDigitalObject = pid.getCellarIdentifier().getUuid();
            if (!cellarIdToAssignToDigitalObject.contains(parentCellarId)) {
                throw ExceptionBuilder
                        .get(CellarException.class)
                        .withCode(CommonErrors.INVALID_CELLARID_HIERARCHY)
                        .withMessage("Child (pid : {} / cellarid : {}) cannot be part of parent's hierarchy (pid : ({}) / cellarid : {})")
                        .withMessageArgs(pid.getProductionId(), cellarIdToAssignToDigitalObject, parentDigitalObject.getContentids(), parentCellarId)
                        .build();
            }
        }

        digitalObject.setCellarId(new ContentIdentifier(pid.getCellarIdentifier().getUuid()));
    }
}
