/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor
 *             FILE : ExportBatchJobProcessor.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 5, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2015 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor;

import eu.europa.ec.opoce.cellar.cl.concurrency.Concurrent;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob.BATCH_JOB_TYPE;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.BatchJobProcessingService;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cl.service.export.exporter.IMetsElementExporter;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.MetsElement;
import eu.europa.ec.opoce.cellar.common.factory.IFactory;
import eu.europa.ec.opoce.cellar.common.util.CellarIdUtils;
import eu.europa.ec.opoce.cellar.core.dao.FileDao;
import eu.europa.ec.opoce.cellar.domain.content.mets.OffIngestionOperationType;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.BehaviorSecType;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.Mets;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.ObjectType;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.StructMapType;
import eu.europa.ec.opoce.cellar.mets.parser.MetsBehavior.BehaviourType;
import eu.europa.ec.opoce.cellar.mets.v2.builder.BehaviorBuilder;
import eu.europa.ec.opoce.cellar.mets.v2.builder.BehaviorSecBuilder;
import eu.europa.ec.opoce.cellar.mets.v2.builder.MetsBuilder;
import eu.europa.ec.opoce.cellar.mets.v2.builder.StructMapBuilder;
import eu.europa.ec.opoce.cellar.mets.v2.builder.enums.LocType;
import eu.europa.ec.opoce.cellar.mets.v2.builder.enums.MetsProfile;
import eu.europa.ec.opoce.cellar.mets.v2.builder.enums.MetsType;
import eu.europa.ec.opoce.cellar.server.service.DisseminationDbGateway;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.update.UpdateAction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.ws.rs.core.UriBuilder;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.FileOutputStream;
import java.net.URLEncoder;
import java.util.Date;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * <class_description> Batch job export processor.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 5, 2015
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Configurable
public class UpdateBatchJobProcessor extends AbstractBatchJobProcessor {

    private static final Logger LOG = LogManager.getLogger(UpdateBatchJobProcessor.class);

    /** The Constant DELETE_ALL_QUERY. */
    private static final String DELETE_ALL_QUERY = "DELETE {?s ?p ?o} WHERE {?s ?p ?o}";

    /**
     * Dissemination db gateway service.
     */
    @Autowired
    private DisseminationDbGateway disseminationDbGateway;

    /**
     * Production identifiers service.
     */
    @Autowired
    @Qualifier("pidManagerService")
    private IdentifierService identifierService;

    /**
     * Exporter factory.
     */
    @Autowired
    @Qualifier("metsElementExporterFactory")
    private IFactory<IMetsElementExporter, MetsElement> metsElementExporterFactory;

    /**
     * Batch job processing service.
     */
    @Autowired(required = true)
    private BatchJobProcessingService batchJobProcessingService;

    /**
     * File manager.
     */
    @Autowired
    private FileDao fileDao;

    /**
     * Dst export folder.
     */
    private File batchJobExportFolder;

    /**
     * Instantiates a new update batch job processor.
     *
     * @param batchJob the batch job
     */
    public UpdateBatchJobProcessor(final BatchJob batchJob) {
        super(batchJob);
    }

    /** {@inheritDoc} */
    @Override
    protected void initBatchJobProcessing() {

        final String folderName = "export-update" + this.batchJob.getId();

        this.batchJobExportFolder = new File(this.fileDao.getArchiveFolderExportUpdateResponse(), folderName);
        if (!this.batchJobExportFolder.exists()) {
            this.batchJobExportFolder.mkdirs();
        }
    }

    /**
     * Gets the mets document id.
     *
     * @param pid the pid
     * @return the mets document id
     */
    private static String getMetsDocumentId(final String pid) {
        return CellarIdUtils.getCellarIdAsFilename(pid + "_" + UUID.randomUUID().toString());
    }

    /**
     * Gets the struct map id.
     *
     * @param pid the pid
     * @return the struct map id
     */
    private static String getStructMapId(final String pid) {
        return CellarIdUtils.getCellarIdAsFilename("structMap_" + pid);
    }

    /**
     * Gets the mets xml filename.
     *
     * @param metsDocumentId the mets document id
     * @return the mets xml filename
     */
    private static String getMetsXmlFilename(final String metsDocumentId) {
        return metsDocumentId + ".mets.xml";
    }

    /**
     * Gets the zip filename.
     *
     * @param metsDocumentId the mets document id
     * @return the zip filename
     */
    private static String getZipFilename(final String metsDocumentId) {
        return metsDocumentId + ".zip";
    }

    /**
     * Pre execute.
     */
    @Override
    protected void preExecute() {
        batchJobService.updateExecutingSparqlBatchJob(batchJob);// AUTO -> EXECUTING_SPARQL
        batchJobProcessingService.executeSparql(batchJob);
        batchJobService.updateProcessingBatchJob(batchJob);//EXECUTING_SPARQL -> PROCESSING
    }

    /**
     * Post execute.
     */
    @Override
    protected void postExecute() {
        final BatchJob job = this.batchJobService.getBatchJob(batchJob.getId(), BATCH_JOB_TYPE.UPDATE);
        job.setJobStatus(BatchJob.STATUS.AUTO);
        this.batchJobService.updateBatchJob(job);
    }

    /** {@inheritDoc} */
    @Override
    @Concurrent(locker = OffIngestionOperationType.EXPORT)
    protected void doExecute(final String workId, final BatchJob batchJob) {

        try {

            final String cellarId = this.identifierService.getCellarPrefixed(workId);

            final MetsElement metsElement = this.disseminationDbGateway.getCompleteTree(cellarId);

            final String metsDocumentId = getMetsDocumentId(workId);

            final MetsBuilder metsBuilder = MetsBuilder.create().withCREATEDATE(new Date()).withPROFILE(MetsProfile.v2)
                    .withTYPE(MetsType.update).withMetsDocumentID(metsDocumentId);

            final StructMapBuilder structMapBuilder = StructMapBuilder.get().withID(getStructMapId(cellarId));

            try (FileOutputStream fos = new FileOutputStream(new File(this.batchJobExportFolder, getZipFilename(metsDocumentId)));
                 ZipOutputStream zos = new ZipOutputStream(fos)) {

                //Create the entire model from metsElement
                final Model model = ModelFactory.createDefaultModel();
                final IMetsElementExporter metsElementExporter = this.metsElementExporterFactory.create(metsElement);
                metsElementExporter.createModel(model, metsElement);

                //Execute update query on the model
                final String sparqlUpdateQuery = batchJob.getSparqlUpdateQuery();
                UpdateAction.parseExecute(sparqlUpdateQuery, model);

                //Export the updated model
                metsElementExporter.exportMetsElement(model, metsBuilder, structMapBuilder, metsElement, zos);

                final StructMapType structMap = structMapBuilder.build();

                final ObjectType mechanism = new ObjectType();
                mechanism.setLOCTYPE(LocType.URL.getValue());
                mechanism.setLABEL("DELETE Mechanism");
                final UriBuilder href = UriBuilder.fromPath(BehaviourType.delete.getPrefix());
                href.queryParam(BehaviourType.delete.getAcceptableParamsByName("query"), URLEncoder.encode(DELETE_ALL_QUERY, "UTF-8"));
                mechanism.setHref(href.build().toString());
                final BehaviorSecType behaviorSec = BehaviorSecBuilder.get().withBehavior(
                        BehaviorBuilder.get().withStructId(structMap).withBtype(BehaviourType.delete).withMechanism(mechanism).build())
                        .build();
                metsBuilder.withBehaviorSec(behaviorSec);

                final Mets mets = metsBuilder.withStructMap(structMap).build();

                final JAXBContext jaxbContext = JAXBContext.newInstance(Mets.class);
                final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

                jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true); // output pretty printed

                LOG.info("Add the mets file '{}' in the package.", getMetsXmlFilename(metsDocumentId));
                zos.putNextEntry(new ZipEntry(getMetsXmlFilename(metsDocumentId)));
                jaxbMarshaller.marshal(mets, zos);
            }
        } catch (final Exception exception) {
            LOG.error("Cannot export mets", exception);
        }
    }

    /** {@inheritDoc} */
    @Override
    protected BATCH_JOB_TYPE getBatchJobType() {
        return BATCH_JOB_TYPE.UPDATE;
    }

}
