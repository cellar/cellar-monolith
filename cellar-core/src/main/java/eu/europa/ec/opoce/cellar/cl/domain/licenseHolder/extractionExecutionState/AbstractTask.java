/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState
 *             FILE : AbstractTask.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 18, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState;

import org.springframework.stereotype.Component;

import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution;

/**
 * <class_description> Abstract class of the task logic.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 18, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Component
public abstract class AbstractTask extends AbstractExecution {

    /**
     * Returns the next step.
     * @param extractionExecution the execution
     * @return the next step
     */
    public abstract AbstractTask next(final ExtractionExecution extractionExecution);

    /**
     * Executes the step and return the next step.
     * @param extractionExecution the execution
     * @return the next step
     * @throws Exception
     */
    public abstract AbstractTask execute(final ExtractionExecution extractionExecution) throws Exception;
}
