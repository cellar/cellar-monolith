package eu.europa.ec.opoce.cellar.ccr.validator;

import org.springframework.validation.AbstractBindingResult;

/**
 * Very basic class used to hold all validation errors that may be thrown while validating them. 
 * 
 * @author lderavet
 */
public class CellarPropertyError extends AbstractBindingResult {

    private static final long serialVersionUID = 7841646924928334370L;

    /**
     * Assign a name to the error handler.
     * 
     * @param objectName the name of the error handler.
     */
    public CellarPropertyError(String objectName) {
        super(objectName);
    }

    /**
     * Does nothing yet.
     * 
     * @return <b>null</b>.
     */
    public Object getTarget() {
        return null;
    }

    /**
     * Does nothing yet.
     * 
     * @return <b>null</b>.
     */
    protected Object getActualFieldValue(String field) {
        return null;
    }
}
