/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.concurrency.impl
 *             FILE : ConcurrencyLoggingScheduler.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : May 3, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.concurrency.impl;

import eu.europa.ec.opoce.cellar.cl.concurrency.ICellarThreadsManager;
import eu.europa.ec.opoce.cellar.logging.LogContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.CMR_CONCURRENCY;

/**
 * <class_description> Concurrency controller autologging job.
 * <br/><br/>
 * ON : May 3, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@LogContext
public class ConcurrencyLoggingScheduler implements Runnable {

    private static final Logger LOG = LogManager.getLogger(ConcurrencyLoggingScheduler.class);

    /**
     * Generic Cellar Locks Manager for all operation types except for Indexation.
     */
    private final CellarLocksManager cellarLocksManager;

    /**
     * Indexing-specific Cellar Locks Manager.
     */
    private final CellarLocksManager indexingOnlyCellarLocksManager;

    /**
     * The cellar threads manager.
     */
    private final ICellarThreadsManager cellarThreadsManager;

    /**
     * Constructor.
     *
     * @param cellarLocksManager   the cellar locks manager
     * @param cellarThreadsManager the cellar threads manager
     */
    public ConcurrencyLoggingScheduler(final CellarLocksManager cellarLocksManager, final CellarLocksManager indexingOnlyCellarLocksManager, final ICellarThreadsManager cellarThreadsManager) {
        this.cellarLocksManager = cellarLocksManager;
        this.indexingOnlyCellarLocksManager = indexingOnlyCellarLocksManager;
        this.cellarThreadsManager = cellarThreadsManager;
    }

    @Override
    @LogContext(CMR_CONCURRENCY)
    public void run() {
        LOG.info("Non-Indexing:[{} waiting, {} working, {} locked identifiers], Indexing:[{} waiting, {} working, {} locked identifiers]",
                cellarThreadsManager.getNonIndexingWaitingThreadsCount(),cellarThreadsManager.getNonIndexingWorkingThreadsCount(),cellarLocksManager.getLockedIdentifiersCount(),
                cellarThreadsManager.getIndexingWaitingThreadsCount(),cellarThreadsManager.getIndexingWorkingThreadsCount(),indexingOnlyCellarLocksManager.getLockedIdentifiersCount());
    }

}
