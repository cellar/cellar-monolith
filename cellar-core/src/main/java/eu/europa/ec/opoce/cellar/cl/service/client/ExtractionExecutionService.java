/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.client
 *             FILE : ExtractionExecutionService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 9, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.client;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ArchiveTreeManager;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution;

/**
 * <class_description> Service for scheduling, cleaning and extracting {@link ExtractionExecution} instances.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 9, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface ExtractionExecutionService {

    /**
     * Executes the pending executions.
     */
    public void executeExtractionExecutions();

    /**
     * Executes the chain execution states.
     * @param extractionExecution the execution
     * @throws Exception
     */
    public void executeExtractionExecution(final ExtractionExecution extractionExecution) throws Exception;

    /**
     * Executes a batch archiving.
     * @param extractionExecution the execution
     * @param firstOfBatch the first index of batch (inclusive)
     * @param lastOfBatch the last index of batch (exclusive)
     * @param archiveTreeManager the archive tree manager
     * @param readWriteLock the reader writer lock for this execution
     * @param archivingBatchWorker the batch counter for this execution
     * @throws Exception
     */
    public void executeBatch(final ExtractionExecution extractionExecution, final int firstOfBatch, final int lastOfBatch,
            final ArchiveTreeManager archiveTreeManager, final ReentrantReadWriteLock readWriteLock,
            final AtomicInteger archivingBatchWorker) throws Exception;

    /**
     * Adds an execution task in the corresponding threadpool.
     * @param extractionExecution the execution task
     */
    public void addExecutionTask(final ExtractionExecution extractionExecution);

    /**
     * Adds an archiving batch task in the corresponding threadpool.
     * @param extractionExecution the execution task
     * @param firstOfBatch the first index of batch (inclusive)
     * @param lastOfBatch the last index of batch (exclusive)
     * @param archiveTreeManager the archive tree manager
     * @param readWriteLock the reader writer lock for this execution
     * @param archivingBatchWorker the batch counter for this execution
     */
    public void addArchivingBatchTask(final ExtractionExecution extractionExecution, final int firstOfBatch, final int lastOfBatch,
            final ArchiveTreeManager archiveTreeManager, final ReentrantReadWriteLock readWriteLock,
            final AtomicInteger archivingBatchWorker);

    /**
     * Schedules executions.
     */
    public void scheduleExtractionExecutions();

    /**
     * Cleans and deletes the executions outdated.
     */
    public void cleanAndDeleteExtractionExecutions();

    /**
     * Cleans and deletes the identified configuration.
     * @param configurationId the configuration identifier
     * @param cleanFinalArchive True if the final archives must be deleted. Otherwise, false.
     */
    public void cleanAndDeleteExtractionConfiguration(final Long configurationId, final boolean cleanFinalArchive);

    /**
     * Cleans the unachieved executions for the current instance.
     */
    public void cleanExtractionExecutions();

    /**
     * Cleans the execution.
     * @param extractionExecution the execution to clean
     * @param cleanFinalArchive True if the final archive must be deleted. Otherwise, false.
     * @throws Exception
     */
    public void clean(final ExtractionExecution extractionExecution, final boolean cleanFinalArchive) throws Exception;
}
