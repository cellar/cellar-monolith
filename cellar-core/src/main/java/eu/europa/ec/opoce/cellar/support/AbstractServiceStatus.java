package eu.europa.ec.opoce.cellar.support;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.status.IServiceStatus;
import eu.europa.ec.opoce.cellar.exception.CellarConfigurationException;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.annotation.PreDestroy;
import java.io.IOException;

/**
 * @author ARHS Developments
 * @version $Revision$
 */
@Component
public abstract class AbstractServiceStatus implements IServiceStatus {

    private transient static final Logger LOG = LogManager.getLogger(AbstractServiceStatus.class);

    protected final ICellarConfiguration cellarConfiguration;

    private final String serviceName;
    private final CloseableHttpClient client;

    protected AbstractServiceStatus(final String serviceName, ICellarConfiguration cellarConfiguration) {
        this.serviceName = serviceName;
        this.client = HttpClientBuilder.create().build();
        this.cellarConfiguration = cellarConfiguration;
    }

    @PreDestroy
    public void close() {
        try {
            client.close();
        } catch (IOException e) {
            LOG.warn("Error when closing httpClient", e);
        }
    }

    public void check() throws CellarException {
        final String url = resolveServiceUrl();
        LOG.info("Checking {} availability on [{}]..", serviceName, url);

        try (CloseableHttpResponse response = client.execute(new HttpGet(url))) {
            int code = response.getStatusLine().getStatusCode();
            if (code == 200) {
                LOG.info("{} is available at [{}]", serviceName, url);
            } else {
                LOG.error("{} is NOT available at [{}], status code: {}", serviceName, url, code);
            }
        } catch (Exception e) {
            throw ExceptionBuilder.get(CellarConfigurationException.class)
                    .withCode(CommonErrors.SERVICE_NOT_AVAILABLE)
                    .withMessage(serviceName + " is NOT available at [{}].")
                    .withMessageArgs(url)
                    .withCause(e)
                    .build();
        }
    }

    protected abstract String resolveServiceUrl();
}
