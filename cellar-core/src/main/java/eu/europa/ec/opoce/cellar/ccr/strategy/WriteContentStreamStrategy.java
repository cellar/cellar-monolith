package eu.europa.ec.opoce.cellar.ccr.strategy;

import com.amazonaws.services.s3.Headers;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.MetaDataIngestionHelper;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentStream;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectStrategy;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import eu.europa.ec.opoce.cellar.util.MigrationUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.FileSystemResource;

import java.io.File;
import java.nio.file.Paths;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class WriteContentStreamStrategy implements DigitalObjectStrategy {

    private static final Logger LOG = LogManager.getLogger(WriteContentStreamStrategy.class);

    private final File metsDirectory;
    private final ContentStreamService contentStreamService;

    public WriteContentStreamStrategy(File metsDirectory, ContentStreamService contentStreamService) {
        this.metsDirectory = metsDirectory;
        this.contentStreamService = contentStreamService;
    }

    @Override
    public void applyStrategy(DigitalObject digitalObject) {
        for (int i = 0; i < digitalObject.getContentStreams().size(); i++) {
            ContentStream contentStream = digitalObject.getContentStreams().get(i);
            int docIndex = i + 1;
            // TODO: validate, analyze the impact of IS3Configuration.getS3ServiceIngestionDatastreamRenamingEnabled
            // TODO: S3DataStreamNamingServiceImpl.getContentStreamSourceId
            final String sourceId = MigrationUtils.getContentStreamSourceId(contentStream, docIndex);
            digitalObject.getStructMap().getMetsDocument().addContentStream(sourceId, contentStream);

            final ContentIdentifier newCellarId = new ContentIdentifier(digitalObject.getCellarId().getIdentifier() + "/" + sourceId);
            newCellarId.setDocIndex(docIndex);
            contentStream.setCellarId(newCellarId);
            contentStream.setCCR_datastream(sourceId);

            writeContentStream(contentStream);
        }
    }

    private void writeContentStream(ContentStream contentStream) {
        FileSystemResource r = new FileSystemResource(Paths.get(metsDirectory.getAbsolutePath(), contentStream.getFileRef()).toFile());
        if (r.exists()) {
            Map<String, String> metadata = new HashMap<>();
            metadata.put(Headers.CONTENT_TYPE, contentStream.getMimeType());
            metadata.put(ContentStreamService.Metadata.FILENAME, MetaDataIngestionHelper.dropPath(contentStream.getFileRef()));
            String newVersion = contentStreamService.writeContent(contentStream.getCellarId().getIdentifier(), r, ContentType.FILE, metadata,
                    () -> {
                        CellarResource item = new CellarResource();
                        item.setCellarId(contentStream.getCellarId().getIdentifier());
                        item.setMimeType(contentStream.getMimeType());
                        item.setCellarType(DigitalObjectType.ITEM);
                        item.setLastModificationDate(new Date());
                        return item;
                    });
            contentStream.setVersion(newVersion);
        } else {
            LOG.warn("File {} for content stream not found", r.getFile());
        }
    }

}