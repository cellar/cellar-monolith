/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.sparql
 *             FILE : UriTemplatesServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 06-Feb-2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.sparql;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.dao.UriTemplatesDao;
import eu.europa.ec.opoce.cellar.cl.domain.sparql.UriTemplates;
import eu.europa.ec.opoce.cellar.cl.service.client.UriTemplatesService;

import java.io.File;
import java.io.FileFilter;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Helper class to handle response templates for the sparql.
 *
 * @author ARHS Developments
 * @version $$Revision$$$
 */
@Service
@Transactional
public class UriTemplatesServiceImpl implements UriTemplatesService {

    /**
     * The Cellar configuration storing runtime parameters.
     */
    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    /** The uri templates dao. */
    @Autowired
    private UriTemplatesDao uriTemplatesDao;

    /**
     * {@inheritDoc}
     */
    @Override
    @Cacheable(value = "getUriTemplatesById", unless = "#result == null")
    public UriTemplates getUriTemplatesById(final Long uriTemplatesId) {
        return this.uriTemplatesDao.getObject(uriTemplatesId);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @CacheEvict(value = {
            "findAllOrderedBySequence", "findUriTemplatesBySequence", "findAllUriTemplates", "getUriTemplatesById"}, allEntries = true)
    public UriTemplates savelUriTemplates(final UriTemplates uriTemplates) {
        return this.uriTemplatesDao.saveObject(uriTemplates);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @CacheEvict(value = {
            "findAllOrderedBySequence", "findUriTemplatesBySequence", "findAllUriTemplates", "getUriTemplatesById"}, allEntries = true)
    public UriTemplates updatelUriTemplates(final UriTemplates uriTemplates) {
        return this.uriTemplatesDao.updateObject(uriTemplates);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @CacheEvict(value = {
            "findAllOrderedBySequence", "findUriTemplatesBySequence", "findAllUriTemplates", "getUriTemplatesById"}, allEntries = true)
    public void deleteUriTemplates(final UriTemplates uriTemplates) {
        this.uriTemplatesDao.deleteObject(uriTemplates);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Cacheable(value = "findAllUriTemplates", unless = "#result == null")
    public List<UriTemplates> findAllUriTemplates() {
        return this.uriTemplatesDao.findAll();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Cacheable(value = "findAllOrderedBySequence", unless = "#result == null")
    public List<UriTemplates> findAllOrderedBySequence() {
        return this.uriTemplatesDao.findAllOrderedBySequence();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Cacheable(value = "findUriTemplatesBySequence", unless = "#result == null")
    public List<UriTemplates> findUriTemplatesBySequence(final Long sequence) {
        return this.uriTemplatesDao.findUriTemplatesBySequence(sequence);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String[] getXslt() {
        String[] xslt = new String[1];
        final File xsltFolder = new File(this.cellarConfiguration.getCellarFolderSparqlResponseTemplates());
        if (xsltFolder.isDirectory()) {
            final File[] fileList = xsltFolder.listFiles(getXsltFileFilter());
            xslt = new String[fileList.length + 1];
            int i = 1;
            for (final File file : fileList) {
                xslt[i] = file.getName();
                i++;
            }
        }
        xslt[0] = "";
        return xslt;
    }

    /**
     * <p>getQueryFileFilter.</p>
     *
     * @return a {@link java.io.FileFilter} object.
     *
     */
    private static FileFilter getXsltFileFilter() {
        return new FileFilter() {

            @Override
            public boolean accept(final File file) {
                if (file.isFile()) {
                    final int delim = file.getName().lastIndexOf(".");
                    return file.getName().substring(delim + 1, file.getName().length()).equalsIgnoreCase("xslt")
                            || file.getName().substring(delim + 1, file.getName().length()).equalsIgnoreCase("xsl");
                }
                return false;
            }
        };
    }

}
