/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *        FILE : SIPQueueManagerProxyImpl.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 20-06-2023
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2023 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import eu.europa.ec.opoce.cellar.cl.configuration.impl.CellarPersistentConfiguration;
import eu.europa.ec.opoce.cellar.cl.service.client.SIPQueueManager;

@Service("sipQueueManagerProxy")
public class SIPQueueManagerProxyImpl {
	
	/**
	 * 
	 */
	private final CellarPersistentConfiguration cellarConfiguration;
	/**
	 * 
	 */
	private final SIPQueueManager sipQueueManager;

	@Autowired
	public SIPQueueManagerProxyImpl(@Qualifier("cellarConfiguration") CellarPersistentConfiguration cellarConfiguration, SIPQueueManager sipQueueManager) {
		this.cellarConfiguration = cellarConfiguration;
		this.sipQueueManager = sipQueueManager;
	}
	
	/**
	 * Wrapper method around {@code SIPQueueManager.prioritizePackagesOnInsert()} ensuring
	 * single-threaded access to the DB SIP reference queue when performing prioritization
	 * on insertion of new packages.
	 */
	public void prioritizePackagesOnInsertExclusively() {
		// Execute only if ingestion is enabled.
		if (this.cellarConfiguration.isCellarServiceIngestionEnabled()) {
			// Acquire isolated modification access to the DB SIP reference queue.
			this.sipQueueManager.getSipQueueLock().lock();
			try {
				// Execute the actual prioritization logic.
				this.sipQueueManager.prioritizePackagesOnInsert();
			} finally {
				// Always release the lock.
				this.sipQueueManager.getSipQueueLock().unlock();
			}
		}
	}
	
	@Async("sipQueueManagerExecutor")
	public void prioritizePackagesOnInsertExclusivelyAsynchronously() {
		 prioritizePackagesOnInsertExclusively();
	}
	
}
