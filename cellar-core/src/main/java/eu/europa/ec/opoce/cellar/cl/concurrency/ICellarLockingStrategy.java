/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.concurrency
 *             FILE : ICellarLockingStrategy.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 7, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.concurrency;

import eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLock.MODE;

/**
 * <class_description> Cellar locking strategy.
 * <br/><br/>
 * ON : Jun 7, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface ICellarLockingStrategy {

    /**
     * Get locking mode to use for root.
     * @return locking mode to use for root
     */
    MODE getRootLockingMode();

    /**
     * Get locking mode to use for children.
     * @return locking mode to use for children
     */
    MODE getChildrenLockingMode();

    /**
     * Get locking mode to use for relations.
     * @return locking mode to use for relations
     */
    MODE getRelationsLockingMode();
}
