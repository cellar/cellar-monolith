/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ccr.service.impl
 *             FILE : RetrievingServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 19-Apr-2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.service.impl;

import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.server.service.RetrievingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author ARHS Developments
 */
@Service
public class RetrievingServiceImpl implements RetrievingService {

    private static final String ALL_MANIFESTATION_TYPE_PARAMETER = "*/*";

    private final CellarResourceDao cellarResourceDao;

    @Autowired
    public RetrievingServiceImpl(CellarResourceDao cellarResourceDao) {
        this.cellarResourceDao = cellarResourceDao;
    }

    @Override
    public List<CellarResource> getUriFromDatastream(final String cellarId, Map<ContentType, String> versions, DigitalObjectType type, final List<String> manifestationTypeList) {
        List<CellarResource> result = new ArrayList<>();
        if (type == DigitalObjectType.EXPRESSION || type == DigitalObjectType.MANIFESTATION) {
            result = getManifestationsContent(cellarId, manifestationTypeList);
        }
        return result;
    }

    private List<CellarResource> getManifestationsContent(final String expressionId, final List<String> manifestationTypes) {
        Collection<CellarResource> items = cellarResourceDao.findResourceStartWith(expressionId, DigitalObjectType.ITEM);
        if (items != null && !items.isEmpty()) {
            boolean allTypes = manifestationTypes.contains(ALL_MANIFESTATION_TYPE_PARAMETER);
            return items.stream()
                    .filter(r -> allTypes || filter(r.getMimeType(), manifestationTypes))
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    private static boolean filter(String mimeType, List<String> expectedTypes) {
        return expectedTypes.isEmpty() || expectedTypes.stream().anyMatch(s -> s.equals(mimeType));
    }

}
