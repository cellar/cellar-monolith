/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.rdfStoreCleaner
 *             FILE : AbstractCleaner.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 11, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.rdfStoreCleaner;

import eu.europa.ec.opoce.cellar.cl.delegator.AbstractDelegator;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 11, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public interface DelegateCleaner {

    void startProcessing(final AbstractDelegator delegator);

    void clean(final AbstractDelegator delegator);
}
