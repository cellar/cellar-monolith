/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.common
 *             FILE : CellarPaginatedList.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Nov 14, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.common;

import java.util.List;

import org.displaytag.pagination.PaginatedList;
import org.displaytag.properties.SortOrderEnum;

/**
 * <class_description> An externally sorted and paginated list for display tag.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Nov 14, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class CellarPaginatedList<T> implements PaginatedList {

    /**
     * Default value for objectsPerPage.
     */
    protected static final int DEFAULT_OBJETS_PER_PAGE = 30;
    private int fullListSize;
    private List<T> list;
    private int objectsPerPage;
    private int pageNumber;
    private String searchId;
    private String sortCriterion;
    private SortOrderEnum sortOrderEnum;
    private String filterValue;

    /**
     * Constructor.
     * @param objectsPerPage the number of objects per page
     * @param pageNumber the current page number
     * @param sortCriterion the sort criterion
     * @param sortDir the sort direction
     * @param filterValue the filter value to use
     */
    public CellarPaginatedList(final int objectsPerPage, final Integer pageNumber, final String sortCriterion, final String sortDir,
            final String filterValue) {
        this.objectsPerPage = objectsPerPage;
        this.pageNumber = (pageNumber == null ? 1 : pageNumber);
        this.sortCriterion = sortCriterion;
        this.sortOrderEnum = this.getSortOrderEnum(sortDir);
        this.filterValue = filterValue;
    }

    /**
     * Constructor.
     * @param pageNumber the current page number
     * @param sortCriterion the sort criterion
     * @param sortDir the sort direction
     * @param filterValue the filter value to use
     */
    public CellarPaginatedList(final Integer pageNumber, final String sortCriterion, final String sortDir, final String filterValue) {
        this(DEFAULT_OBJETS_PER_PAGE, pageNumber, sortCriterion, sortDir, filterValue);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getFullListSize() {
        return this.fullListSize;
    }

    /**
     * Sets the size of the full list.
     * @param fullListSize the size to set
     */
    public void setFullListSize(final int fullListSize) {
        this.fullListSize = fullListSize;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<T> getList() {
        return this.list;
    }

    /**
     * Sets the list.
     * @param list the list to set
     */
    public void setList(final List<T> list) {
        this.list = list;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getObjectsPerPage() {
        return this.objectsPerPage;
    }

    /**
     * Sets the number of objects per page.
     * @param objectsPerPage the number of objects per page to set
     */
    public void setObjectsPerPage(final int objectsPerPage) {
        this.objectsPerPage = objectsPerPage;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getPageNumber() {
        return this.pageNumber;
    }

    /**
     * Sets the page number.
     * @param pageNumber the page number to set
     */
    public void setPageNumber(final int pageNumber) {
        this.pageNumber = pageNumber;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSearchId() {
        return this.searchId;
    }

    /**
     * Sets the search identifier.
     * @param searchId the search identifier to set
     */
    public void setSearchId(final String searchId) {
        this.searchId = searchId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSortCriterion() {
        return this.sortCriterion;
    }

    /**
     * Sets the sort criterion.
     * @param sortCriterion the sort criterion to set
     */
    public void setSortCriterion(final String sortCriterion) {
        this.sortCriterion = sortCriterion;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SortOrderEnum getSortDirection() {
        return this.sortOrderEnum;
    }

    /**
     * Sets the sort direction.
     * @param sortOrder the sort direction to set
     */
    public void setSortDirection(final SortOrderEnum sortOrder) {
        this.sortOrderEnum = sortOrder;
    }

    /**
     * Sets the sort direction.
     * @param dir the sort direction to set
     */
    public void setSortDirection(final String dir) {
        this.sortOrderEnum = this.getSortOrderEnum(dir);
    }

    /**
     * Gets the sort direction.
     * @return the sort direction to set
     */
    public String getSortDirectionCode() {
        if (this.sortOrderEnum.equals(SortOrderEnum.DESCENDING))
            return "DESC";
        else
            return "ASC";
    }

    /**
     * Gets the sort order.
     * @param dir the String order
     * @return the enum order
     */
    private SortOrderEnum getSortOrderEnum(final String dir) {
        if (dir == null || !dir.equalsIgnoreCase("asc"))
            return SortOrderEnum.DESCENDING;
        else
            return SortOrderEnum.ASCENDING;
    }

    /**
     * Sets the filter value to use.
     * @param filterValue the filter value to set
     */
    public void setFilterValue(final String filterValue) {
        this.filterValue = filterValue;
    }

    /**
     * Gets the filter value to use.
     * @return the filter value to use
     */
    public String getFilterValue() {
        return this.filterValue;
    }
}
