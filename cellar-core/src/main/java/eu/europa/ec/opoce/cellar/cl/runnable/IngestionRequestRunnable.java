package eu.europa.ec.opoce.cellar.cl.runnable;

import eu.europa.ec.opoce.cellar.cl.domain.audit.*;
import eu.europa.ec.opoce.cellar.cl.service.IngestionService;
import eu.europa.ec.opoce.cellar.common.concurrent.PriorityCallable;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork;
import eu.europa.ec.opoce.cellar.logging.LogContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

import java.io.File;
import java.util.Date;
import java.util.UUID;

import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.CMR_INGESTION;

/**
 * <class_description> <br/>
 * <br/>
 * <notes> This class cannot be annotated with @configurable due to the issue
 * reported in CELLARM-611 <br/>
 * <br/>
 * ON : Nov 20, 2014.
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@LogContext
public class IngestionRequestRunnable implements PriorityCallable<IngestionRequestRunnable> {

    /**
     * Logger instance.
     */
    private static final Logger LOG = LogManager.getLogger(IngestionRequestRunnable.class);

    private final IngestionService ingestionService;
    /**
     * The sip work.
     */
    private final SIPWork sipWork;

    private static final String INGESTION_KEY = "ingestion_key";

    /**
     * Constructor
     *
     * @param ingestionService the ingestion service
     * @param sipWork          the current {@link SIPWork}
     */
    public IngestionRequestRunnable(IngestionService ingestionService, SIPWork sipWork) {
        this.ingestionService = ingestionService;
        this.sipWork = sipWork;
    }

    /**
     * Gets the sip work.
     *
     * @return the sip work
     */
    public SIPWork getSipWork() {
        return this.sipWork;
    }

    @Override
    public int compareTo(PriorityCallable<IngestionRequestRunnable> o) {
        IngestionRequestRunnable other = (IngestionRequestRunnable) o;
        return sipWork.compareTo(other.sipWork);
    }

    @Override
    @LogContext(CMR_INGESTION)
    public IngestionRequestRunnable call() throws Exception {
        // make sure the logging happens in the ingestion logfile
        final File sipFile = sipWork.getSipFile();
        setExecutionContext(sipFile.getName());

        sipWork.setStartDate(new Date());
        try {
            ingestionService.sipTreatment(sipWork);
        } catch (final Exception e) {
            // Nothing to do
            // exceptions should be handled by the thread building a
            // cellar response package
            LOG.error("Unexpected exception was thrown", e);
        } finally {
            sipWork.setEndDate(new Date());
            clearExecutionContext();
        }
        //Log the end of ingestion process
        AuditBuilder.get(AuditTrailEventProcess.Ingestion).withAction(AuditTrailEventAction.Package)
                .withType(AuditTrailEventType.End).withDuration(sipWork.getEndDate().getTime() - sipWork.getStartDate().getTime())
                .withPackageHistory(sipWork.getPackageHistory())
                .withMessage("Finished processing for the SIP file: [{}] ")
                .withMessageArgs(sipFile.getName())
                .withLogger(LOG).withLogDatabase(false).logEvent();
        return this;
    }

    /**
     * Add a unique key to the process in order to debug it more
     * easily.
     *
     * @param sipFileName the name of the current SIP
     * @see <a href="https://logging.apache.org/log4j/2.x/manual/thread-context.html">Log4j2 ThreadContext</a>
     */
    private static void setExecutionContext(String sipFileName) {
        ThreadContext.put(INGESTION_KEY, UUID.randomUUID().toString());
        ThreadContext.put("sip", sipFileName);
        ThreadContext.put(AuditableAdvice.AUDIT_KEY, ThreadContext.get(INGESTION_KEY));
    }

    /**
     * Clear the {@link ThreadContext}
     *
     * @see #setExecutionContext(String)
     */
    private static void clearExecutionContext() {
        ThreadContext.remove(INGESTION_KEY);
        ThreadContext.remove("sip");
        ThreadContext.remove(AuditableAdvice.AUDIT_KEY);
    }
}
