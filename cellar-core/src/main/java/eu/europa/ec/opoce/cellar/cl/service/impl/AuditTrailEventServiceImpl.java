/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *        FILE : AuditTrailEventServiceImpl.java
 * 
 *  CREATED BY : ARHS Developments
 *          ON : 09-01-2013
 * 
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.dao.AuditTrailEventDao;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEvent;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventAction;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventProcess;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventType;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditableAdvice;
import eu.europa.ec.opoce.cellar.cl.domain.package_history.PackageHistory;
import eu.europa.ec.opoce.cellar.cl.domain.structmap_status_history.StructMapStatusHistory;
import eu.europa.ec.opoce.cellar.cl.service.client.AuditTrailEventService;
import eu.europa.ec.opoce.cellar.common.util.StringUtils;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import org.apache.logging.log4j.ThreadContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.Date;
import java.util.List;

import static eu.europa.ec.opoce.cellar.AppConstants.NEWLINE;

/**
 * <class_description> Service for logging events on the audit trail event table.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 09-01-2013
 * 
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class AuditTrailEventServiceImpl implements AuditTrailEventService {

    private transient static final Logger LOGGER = LoggerFactory.getLogger(AuditTrailEventServiceImpl.class);

    private final AuditTrailEventDao auditTrailEventDao;

    private final ICellarConfiguration cellarConfiguration;

    @Autowired
    public AuditTrailEventServiceImpl(AuditTrailEventDao auditTrailEventDao, @Qualifier("cellarConfiguration") ICellarConfiguration cellarConfiguration) {
        this.auditTrailEventDao = auditTrailEventDao;
        this.cellarConfiguration = cellarConfiguration;
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.client.AuditTrailEventService#saveEvent(eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEvent)
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void saveEvent(final AuditTrailEvent event) {
        try {
            // try to save the event
            if (this.cellarConfiguration.isCellarDatabaseAuditEnabled()) {
                this.auditTrailEventDao.saveAuditTrailEvent(event);
            }
        } catch (final Throwable t) {
            // print out the error on the console should a problem occur during the save
            LOGGER.error("A problem occurred while saving Audit Trail Event '" + event + "'.", t);
            // mark this event transaction with rollback: this allow us not to throw the exception up and handle the rollback here
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.client.AuditTrailEventService#saveEvent(eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventProcess, eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventAction, eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventType, java.lang.String, java.lang.String)
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void saveEvent(final AuditTrailEventProcess process, final AuditTrailEventAction action, final AuditTrailEventType type,
                          final String objectIdentifier, final String message, final String processId,
                          final StructMapStatusHistory structMapStatusHistory, final PackageHistory packageHistory) {
        if (process == AuditTrailEventProcess.Undefined) {
            throw ExceptionBuilder.get(CellarException.class).withMessage("Unknown audit trail event process '{}'.")
                    .withMessageArgs(process).build();
        }

        final AuditTrailEvent event = new AuditTrailEvent(process, action, objectIdentifier, type);
        event.setMessage(StringUtils.abbreviate(message.replace(NEWLINE, "\n"), 4000));
        event.setThreadName(processId);
        event.setStructMapStatusHistory(structMapStatusHistory);
        event.setPackageHistory(packageHistory);
        this.saveEvent(event);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.client.AuditTrailEventService#deleteEvent(eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEvent)
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void deleteEvent(final AuditTrailEvent event) {
        try {
            // try to delete the event
            this.auditTrailEventDao.deleteAuditTrailEvent(event);
        } catch (final Throwable t) {
            // print out the error on the console should a problem occur during the delete
            LOGGER.error("A problem occurred while deleting Audit Trail Event '" + event + "'.", t);
            // mark this event transaction with rollback: this allow us not to throw the exception up and handle the rollback here
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.client.AuditTrailEventService#deleteAuditTrailEvent(java.util.Date)
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void deleteEvent(final Date endDate) {
        try {
            // try to delete the events until date endDate
            this.auditTrailEventDao.deleteAuditTrailEvent(endDate);
        } catch (final Throwable t) {
            // print out the error on the console should a problem occur during the delete
            LOGGER.error("A problem occurred while deleting Audit Trail Events created until " + endDate + "'.", t);
            // mark this event transaction with rollback: this allow us not to throw the exception up and handle the rollback here
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.client.AuditTrailEventService#getLogsByProcess(eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventProcess)
     */
    @Override
    public List<AuditTrailEvent> getLogsByProcess(final AuditTrailEventProcess process) {
        return this.auditTrailEventDao.getLogsByProcess(process);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.client.AuditTrailEventService#getLogsByIdentifier(java.lang.String)
     */
    @Override
    public List<AuditTrailEvent> getLogsByIdentifier(final String id) {
        return this.auditTrailEventDao.getLogsByIdentifier(id);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.client.AuditTrailEventService#getLogsByIdentifierWildcard(java.lang.String)
     */
    @Override
    public List<AuditTrailEvent> getLogsByIdentifierWildcard(final String id) {
        return this.auditTrailEventDao.getLogsByIdentifierWildcard(id);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.client.AuditTrailEventService#getLogsByProcessAndIdentifierWildcard(eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventProcess, java.lang.String)
     */
    @Override
    public List<AuditTrailEvent> getLogsByProcessAndIdentifierWildcard(final AuditTrailEventProcess process, final String id) {
        return this.auditTrailEventDao.getLogsByProcessAndIdentifierWildcard(process, id);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.client.AuditTrailEventService#getLogsByProcessAndIdentifier(eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventProcess, java.lang.String)
     */
    @Override
    public List<AuditTrailEvent> getLogsByProcessAndIdentifier(final AuditTrailEventProcess process, final String id) {
        return this.auditTrailEventDao.getLogsByProcessAndIdentifier(process, id);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.client.AuditTrailEventService#getLogs(java.util.Date, java.util.Date)
     */
    @Override
    public List<AuditTrailEvent> getLogs(final Date from, final Date to) {
        return this.auditTrailEventDao.getLogs(from, to);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.client.AuditTrailEventService#getLogs(java.util.Date, java.util.Date, int, int)
     */
    @Override
    public List<AuditTrailEvent> getLogs(final Date from, final Date to, final int start, final int end) {
        return this.auditTrailEventDao.getLogs(from, to, start, end);
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.client.AuditTrailEventService#getLogs()
     */
    @Override
    public List<AuditTrailEvent> getLogs() {
        return this.auditTrailEventDao.getLogsByThreadName(ThreadContext.get(AuditableAdvice.AUDIT_KEY));
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.client.AuditTrailEventService#getXMLLogs()
     */
    @Override
    public StringBuffer getXMLLogs() {
        return getXMLLogs(this.getLogs());
    }

    @Override
    public StringBuffer getXMLLogsFromIdentifier(final String objectId) {
        return getXMLLogs(this.getLogsByIdentifier(objectId));
    }

    /**
     * @see eu.europa.ec.opoce.cellar.cl.service.client.AuditTrailEventService#countLogsBetweenDates(java.util.Date, java.util.Date, eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventProcess, java.lang.String)
     */
    @Override
    public Long countLogsBetweenDates(final Date from, final Date to, final AuditTrailEventProcess process, final String objectId) {
        return this.auditTrailEventDao.countLogsBetweenDates(from, to, process, objectId);
    }

    private static StringBuffer getXMLLogs(final List<AuditTrailEvent> auditTrailEvents) {
        final StringBuffer buffer = new StringBuffer().append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
        buffer.append("<log>\n");

        for (final AuditTrailEvent auditTrailEvent : auditTrailEvents) {
            buffer.append("\t<event type=\"" + auditTrailEvent.getType()).append("\" id=\"").append(auditTrailEvent.getId())
                    .append("\">\n");
            buffer.append("\t\t<date>").append(auditTrailEvent.getDate()).append("</date>\n");
            buffer.append("\t\t<process>").append(auditTrailEvent.getProcess()).append("</process>\n");
            buffer.append("\t\t<action>").append(auditTrailEvent.getAction()).append("</action>\n");
            buffer.append("\t\t<identifier>").append(auditTrailEvent.getObjectIdentifier()).append("</identifier>\n");
            if (auditTrailEvent.getMessage() != null) {
                buffer.append("\t\t<message>").append("<![CDATA[").append(auditTrailEvent.getMessage()).append("]]></message>\n");
            }
            if (auditTrailEvent.getStructMapStatusHistory() != null) {
                buffer.append("\t\t<structmap_id>").append(auditTrailEvent.getStructMapStatusHistory().getId()).append("</structmap_id>\n");
            }
            if (auditTrailEvent.getPackageHistory() != null) {
                buffer.append("\t\t<package_id>").append(auditTrailEvent.getPackageHistory().getId()).append("</package_id>\n");
            }
            buffer.append("\t</event>\n");
        }
        buffer.append("</log>");
        return buffer;
    }

}
