/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.batchJob
 *             FILE : BatchJobWorkIdentifier.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Feb 11, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.batchJob;

import javax.persistence.*;

/**
 * <class_description> Batch job work identifier.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Feb 11, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Entity
@Table(name = "BATCH_JOB_WORK_IDENTIFIER")
@NamedQueries({
        @NamedQuery(name = "findWorkIdentifiersByWorkId", query = "select w from BatchJobWorkIdentifier w where w.workId like ?0 and w.batchJob.id = ?1 and w.batchJob.jobType = ?2"),
        @NamedQuery(name = "findWorkIdentifiersByBatchJobId", query = "select w from BatchJobWorkIdentifier w where w.batchJob.id = ?0 and w.batchJob.jobType = ?1"),
        @NamedQuery(name = "findWorkIdsByBatchJobId", query = "select distinct w.workId from BatchJobWorkIdentifier w where w.batchJob.id = ?0 and w.batchJob.jobType = ?1"),
        @NamedQuery(name = "countWorkIdentifiersByBatchJobId", query = "select count(w.id) from BatchJobWorkIdentifier w where w.batchJob.id = :batchJobId and w.batchJob.jobType = :jobType and w.workId like :filterValue")})
public class BatchJobWorkIdentifier {

    /**
     * Internal id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    /**
     * The work identifier.
     */
    @Column(name = "WORK_ID", nullable = false)
    private String workId;

    /**
     * The batch job (parent).
     */
    @ManyToOne
    @JoinColumn(name = "BATCH_JOB_ID")
    private BatchJob batchJob;

    /**
     * Default constructor.
     */
    public BatchJobWorkIdentifier() {

    }

    /**
     * Constructs a new work with his identifier.
     * @param workId the work identifier
     */
    public BatchJobWorkIdentifier(final String workId) {
        this.workId = workId;
    }

    /**
     * Constructs a new work.
     * @param workId the work identifier
     * @param reindexationJob the parent batch job
     */
    public BatchJobWorkIdentifier(final String workId, final BatchJob batchJob) {
        this.workId = workId;
        this.batchJob = batchJob;
    }

    /**
     * Gets the value of the id.
     * 
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the value of the id.
     * 
     * @param id the id to set
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Gets the work identifier.
     * @return the work identifier
     */
    public String getWorkId() {
        return this.workId;
    }

    /**
     * Sets the work identifier.
     * @param workId the work identifier to set
     */
    public void setWorkId(final String workId) {
        this.workId = workId;
    }

    /**
     * Gets the batch job identifier.
     * @return the batch job identifier
     */
    @Column(name = "BATCH_JOB_ID", nullable = false, insertable = false, updatable = false)
    public Long getReindexationJobId() {
        return this.batchJob.getId();
    }

    /**
     * Gets the batch job (parent of the work).
     * @return the batch job
     */
    public BatchJob getBatchJob() {
        return this.batchJob;
    }

    /**
     * Sets the batch job (parent of the work) identifier.
     * @param batchJob the batch job to set
     */
    public void setBatchJob(final BatchJob batchJob) {
        this.batchJob = batchJob;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null) {
            return false;
        }

        if (getClass() != obj.getClass()) {
            return false;
        }

        final BatchJobWorkIdentifier other = (BatchJobWorkIdentifier) obj;
        if (!this.workId.equals(other.getWorkId()))
            return false;

        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return this.workId.hashCode();
    }
}
