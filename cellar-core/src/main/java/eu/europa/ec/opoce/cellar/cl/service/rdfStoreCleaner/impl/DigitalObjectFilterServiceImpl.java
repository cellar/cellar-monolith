/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.rdfStoreCleaner.impl
 *             FILE : DigitalObjectFilterServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 12, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.rdfStoreCleaner.impl;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Selector;
import org.apache.jena.rdf.model.SimpleSelector;
import org.apache.jena.rdf.model.Statement;
import eu.europa.ec.opoce.cellar.cl.service.rdfStoreCleaner.DigitalObjectFilterService;
import eu.europa.ec.opoce.cellar.cmr.model.ModelUtils;
import eu.europa.ec.opoce.cellar.common.util.Triple;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 12, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
public class DigitalObjectFilterServiceImpl implements DigitalObjectFilterService {

    protected static final Logger LOG = LogManager.getLogger(DigitalObjectFilterServiceImpl.class);

    @Autowired
    @Qualifier("rdfStoreCleanerFilterPatterns")
    protected ArrayList<Triple<String, String, String>> tripleFilterPatterns;

    @Override
    public void filter(final Model model) {
        final Selector selector = new SimpleSelector() {

            @Override
            public boolean selects(final Statement s) {

                return isFiltered(s);
            }
        };

        final long startSize = model.size();

        model.remove(model.listStatements(selector));

        LOG.info("{} triple(s) has/have been filtered.", String.valueOf(startSize - model.size()));
    }

    @Override
    public boolean isFiltered(final Statement s) {
        boolean filter;
        for (final Triple<String, String, String> pattern : this.tripleFilterPatterns) {
            filter = true;

            filter = ModelUtils.match(s.getSubject(), pattern.getOne());

            filter = filter && ModelUtils.match(s.getPredicate(), pattern.getTwo());

            filter = filter && ModelUtils.match(s.getObject(), pattern.getThree());

            if (filter) {
                return true;
            }
        }

        return false;
    }
}
