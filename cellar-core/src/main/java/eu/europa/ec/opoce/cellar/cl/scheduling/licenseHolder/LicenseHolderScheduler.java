/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.scheduling.licenseHolder
 *             FILE : LicenseHolderScheduler.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 10, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.scheduling.licenseHolder;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.service.client.ExtractionExecutionService;
import eu.europa.ec.opoce.cellar.common.spring.CloseableConcurrentTaskScheduler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 10, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
@Lazy(false)
public class LicenseHolderScheduler {

    private static final Logger LOG = LogManager.getLogger(LicenseHolderScheduler.class);

    /**
     * The Cellar configuration storing runtime parameters.
     */
    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    @Autowired(required = true)
    private ExtractionExecutionService extractionExecutionService;

    private ExecutionWorker licenseHolderWorker;
    @SuppressWarnings("rawtypes")
    private ScheduledFuture licenseHolderWorkerFuture;

    private ExecutionScheduler licenseHolderScheduler;
    @SuppressWarnings("rawtypes")
    private ScheduledFuture licenseHolderSchedulerFuture;

    private ExecutionCleaner licenseHolderCleaner;
    @SuppressWarnings("rawtypes")
    private ScheduledFuture licenseHolderCleanerFuture;

    private CloseableConcurrentTaskScheduler licenseHolderTaskScheduler;
    private CloseableConcurrentTaskScheduler licenseHolderWorkerTaskScheduler;
    private CloseableConcurrentTaskScheduler licenseHolderCleanerScheduler;

    /**
     * Post constructor initialization.
     */
    @PostConstruct
    public void init() {
        if (!this.cellarConfiguration.isCellarDatabaseReadOnly() && this.cellarConfiguration.isCellarServiceLicenseHolderEnabled()) {

            this.extractionExecutionService.cleanExtractionExecutions();

            this.licenseHolderWorker = new ExecutionWorker(this.extractionExecutionService);
            this.licenseHolderWorkerTaskScheduler = CloseableConcurrentTaskScheduler.newScheduler("license-holder-worker");
            this.licenseHolderWorkerFuture = licenseHolderWorkerTaskScheduler.schedule(this.licenseHolderWorker,
                    new CronTrigger(this.cellarConfiguration.getCellarServiceLicenseHolderWorkerCronSettings()));

            this.licenseHolderScheduler = new ExecutionScheduler(this.extractionExecutionService);
            this.licenseHolderTaskScheduler = CloseableConcurrentTaskScheduler.newScheduler("license-holder");
            this.licenseHolderSchedulerFuture = licenseHolderTaskScheduler.schedule(this.licenseHolderScheduler,
                    new CronTrigger(this.cellarConfiguration.getCellarServiceLicenseHolderSchedulerCronSettings()));

            this.licenseHolderCleaner = new ExecutionCleaner(this.extractionExecutionService);
            this.licenseHolderCleanerScheduler = CloseableConcurrentTaskScheduler.newScheduler("license-holder-cleaner");
            this.licenseHolderCleanerFuture = licenseHolderCleanerScheduler.schedule(this.licenseHolderCleaner,
                    new CronTrigger(this.cellarConfiguration.getCellarServiceLicenseHolderCleanerCronSettings()));
            LOG.info(ICellarConfiguration.CONFIG, "License holder scheduler started.");
        }
    }

    public void setCronLicenseHolderWorkerCronSettings(final String cron) {
        if (!this.cellarConfiguration.isCellarDatabaseReadOnly() && this.cellarConfiguration.isCellarServiceLicenseHolderEnabled()) {
            this.licenseHolderWorkerFuture.cancel(false);
            this.licenseHolderWorkerTaskScheduler.shutdownNow();
            this.licenseHolderWorkerTaskScheduler = CloseableConcurrentTaskScheduler.newScheduler("license-holder-worker");
            this.licenseHolderWorkerFuture = licenseHolderWorkerTaskScheduler.schedule(this.licenseHolderWorker, new CronTrigger(cron));
        }
        this.cellarConfiguration.setCellarServiceLicenseHolderWorkerCronSettings(cron);
    }

    public String getCronLicenseHolderWorkerCronSettings() {
        return this.cellarConfiguration.getCellarServiceLicenseHolderWorkerCronSettings();
    }

    public void setCronLicenseHolderSchedulerCronSettings(final String cron) {
        if (!this.cellarConfiguration.isCellarDatabaseReadOnly() && this.cellarConfiguration.isCellarServiceLicenseHolderEnabled()) {
            this.licenseHolderSchedulerFuture.cancel(false);
            this.licenseHolderTaskScheduler = CloseableConcurrentTaskScheduler.newScheduler("license-holder");
            this.licenseHolderSchedulerFuture = licenseHolderTaskScheduler.schedule(this.licenseHolderScheduler, new CronTrigger(cron));
        }
        this.cellarConfiguration.setCellarServiceLicenseHolderSchedulerCronSettings(cron);
    }

    public String getCronLicenseHolderSchedulerCronSettings() {
        return this.cellarConfiguration.getCellarServiceLicenseHolderSchedulerCronSettings();
    }

    public void setCronLicenseHolderCleanerCronSettings(final String cron) {
        if (!this.cellarConfiguration.isCellarDatabaseReadOnly() && this.cellarConfiguration.isCellarServiceLicenseHolderEnabled()) {
            this.licenseHolderCleanerFuture.cancel(false);
            this.licenseHolderCleanerScheduler = CloseableConcurrentTaskScheduler.newScheduler("license-holder-cleaner");
            this.licenseHolderCleanerFuture = licenseHolderCleanerScheduler.schedule(this.licenseHolderCleaner, new CronTrigger(cron));
        }
        this.cellarConfiguration.setCellarServiceLicenseHolderCleanerCronSettings(cron);
    }

    public String getCronLicenseHolderCleanerCronSettings() {
        return this.cellarConfiguration.getCellarServiceLicenseHolderCleanerCronSettings();
    }

    public void shutdown(boolean awaitTermination, long timeout, TimeUnit unit) throws InterruptedException {
        if (licenseHolderWorkerTaskScheduler != null) {
            licenseHolderWorkerFuture.cancel(!awaitTermination);
            licenseHolderWorkerTaskScheduler.shutdown(awaitTermination, timeout, unit);
        }
        if (licenseHolderCleanerScheduler != null) {
            licenseHolderCleanerFuture.cancel(!awaitTermination);
            licenseHolderCleanerScheduler.shutdown(awaitTermination, timeout, unit);
        }
        if (licenseHolderTaskScheduler != null) {
            licenseHolderSchedulerFuture.cancel(!awaitTermination);
            licenseHolderTaskScheduler.shutdown(awaitTermination, timeout, unit);
        }
    }
}
