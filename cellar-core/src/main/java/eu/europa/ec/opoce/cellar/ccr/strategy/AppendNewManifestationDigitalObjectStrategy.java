/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : CellarIdentifier.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 10 mars 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.strategy;

import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.ccr.helper.ResponseHelper;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.cl.domain.history.service.IdentifiersHistoryService;
import eu.europa.ec.opoce.cellar.cl.domain.response.UpdateStructMapOperation;
import eu.europa.ec.opoce.cellar.cl.service.client.CellarIdentifierService;
import eu.europa.ec.opoce.cellar.cl.service.client.PidManagerService;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentStream;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectStrategy;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.domain.content.mets.OperationType;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.StructMapProcessorException;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork.TYPE;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * It's the append strategy.
 * We create all missing digital object into S3 and database.
 * This class contains a recursive method applyStrategy that allow to apply the same process for
 * each digital object and their children.
 * <p>
 * The process consist to generate the next cellar identifier of a digital object.
 * Store it in the database.
 * After, we set in the data model StructMap the created cellar identifier.
 * At the end, all missing digital object exist in S3.
 * Datamodel and database are synchronized.
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public final class AppendNewManifestationDigitalObjectStrategy implements DigitalObjectStrategy {

    private static final Logger LOGGER = LoggerFactory.getLogger(AppendNewManifestationDigitalObjectStrategy.class);
    private final PidManagerService pidManagerService;
    private final ContentStreamService contentStreamService;
    private final IdentifiersHistoryService identifiersHistoryService;
    private final String expressionUUID;
    private final SIPResource sipResource;
    private final List<DigitalObject> appendedDigitalObjectList;
    private final File metsDirectory;
    private final UpdateStructMapOperation operation;
    private final CellarIdentifierService cellarIdentifierService;
    private final CellarResourceDao cellarResourceDao;

    /**
     * Instantiates a new append new manifestation digital object strategy.
     *
     * @param expressionUUID            the expression uuid
     * @param sipResource               the sip resource
     * @param metsDirectory             the mets directory
     * @param operation                 the operation
     * @param appendedDigitalObjectList the appended digital object list
     * @param pidManagerService         the pid manager service
     * @param contentStreamService
     * @param cellarResourceDao
     */
    public AppendNewManifestationDigitalObjectStrategy(String expressionUUID, SIPResource sipResource, File metsDirectory, UpdateStructMapOperation operation,
                                                       List<DigitalObject> appendedDigitalObjectList, PidManagerService pidManagerService,
                                                       ContentStreamService contentStreamService, IdentifiersHistoryService identifiersHistoryService,
                                                       CellarIdentifierService cellarIdentifierService, CellarResourceDao cellarResourceDao) {
        this.expressionUUID = expressionUUID;
        this.sipResource = sipResource;
        this.appendedDigitalObjectList = appendedDigitalObjectList;
        this.metsDirectory = metsDirectory;
        this.operation = operation;
        this.pidManagerService = pidManagerService;
        this.contentStreamService = contentStreamService;
        this.identifiersHistoryService = identifiersHistoryService;
        this.cellarIdentifierService = cellarIdentifierService;
        this.cellarResourceDao = cellarResourceDao;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void applyStrategy(final DigitalObject digitalObject) {
        Assert.isTrue(DigitalObjectType.MANIFESTATION.equals(digitalObject.getType()), "Strategy only compatible with Manifestation type.");

        if (cellarIdentifierService.isAnyParentReadOnly(digitalObject)) {
            final ExceptionBuilder<StructMapProcessorException> exceptionBuilder = ExceptionBuilder.get(StructMapProcessorException.class);
            exceptionBuilder.withCode(CoreErrors.E_391);
            exceptionBuilder.withMessage("Cannot append {} as a parent element or itself is marked as read only.");
            exceptionBuilder.withMessageArgs(digitalObject.getType().toString());
            throw exceptionBuilder.build();
        }

        // find the next available manifestation UUID
        final int manifestationUUIDSuffix = getNextCellarIdSuffix(expressionUUID);
        final String manifestationUUID = identifiersHistoryService.generateCellarID(digitalObject, expressionUUID,
                manifestationUUIDSuffix, TYPE.AUTHENTICOJ);

        if (manifestationUUID == null) {
            return;
        }

        // Generate cellarId and link it with pid of this DigitalObject.
        LOGGER.debug("Append : Check PIDs : {}", StringUtils.join(digitalObject.getContentids(), ", "));
        LOGGER.debug("Append : And generate for them : {}", manifestationUUID);

        final boolean ignoreDuplicatedPids = OperationType.CreateOrIgnore.equals(digitalObject.getStructMap().getMetsDocument().getType());
        this.pidManagerService.save(digitalObject, manifestationUUID, ignoreDuplicatedPids);

        final ContentIdentifier contentIdentifier = new ContentIdentifier(manifestationUUID);
        digitalObject.setCellarId(contentIdentifier);

        this.appendedDigitalObjectList.add(digitalObject);

        new WriteContentStreamStrategy(metsDirectory, contentStreamService).applyStrategy(digitalObject);

        // save new content identifier
        for (final ContentStream contentStream : digitalObject.getContentStreams()) {
            LOGGER.debug("Append : Check CONTENT PIDs : " + StringUtils.join(contentStream.getContentids(), ", "));
            LOGGER.debug("Append : And generate for them : {}", contentStream.getCellarId().getIdentifier());
            pidManagerService.save(contentStream.getContentids(), contentStream.getCellarId().getIdentifier(), ignoreDuplicatedPids, digitalObject.getReadOnly());
        }
        operation.addOperation(ResponseHelper.fillCreateOperation(digitalObject));
    }

    private int getNextCellarIdSuffix(String cellarID) {
        int maxCellarIdSuffix = 0;
        final List<String> childrenPids = getChildrenPIDs(cellarID).stream()
                .map(CellarResource::getCellarId)
                .sorted()
                .collect(Collectors.toList());
        for (final String childrenPid : childrenPids) {
            final int currentCellarIdSuffix = Integer.parseInt(StringUtils.substringAfterLast(childrenPid, "."));
            if (maxCellarIdSuffix < currentCellarIdSuffix) {
                maxCellarIdSuffix = currentCellarIdSuffix;
            }
        }

        return maxCellarIdSuffix + 1;
    }

    private List<CellarResource> getChildrenPIDs(final String digitalObjectPID) {
        return new ArrayList<>(cellarResourceDao.findResourceStartWith(digitalObjectPID, DigitalObjectType.MANIFESTATION));
    }

}
