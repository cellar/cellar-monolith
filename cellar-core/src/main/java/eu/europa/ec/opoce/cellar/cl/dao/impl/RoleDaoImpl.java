/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.dao.impl
 *             FILE : RoleDaoImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jan 10, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao.impl;

import org.springframework.stereotype.Repository;

import eu.europa.ec.opoce.cellar.cl.dao.RoleDao;
import eu.europa.ec.opoce.cellar.cl.domain.admin.Role;

/**
 * <class_description> Implementation of the interface {@link RoleDao}.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jan 10, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Repository
public class RoleDaoImpl extends BaseDaoImpl<Role, Long> implements RoleDao {

}
