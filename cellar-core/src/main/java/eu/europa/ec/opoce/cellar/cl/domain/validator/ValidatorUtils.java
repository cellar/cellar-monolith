package eu.europa.ec.opoce.cellar.cl.domain.validator;

import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;

public class ValidatorUtils {

    public static ValidationResult validate(Object o, SIPResource sipResource, Validator validator) {
        ValidationDetails validationDetails = new ValidationDetails();
        validationDetails.setToValidate(o);
        validationDetails.setSipResource(sipResource);

        return validator.validate(validationDetails);

    }

}
