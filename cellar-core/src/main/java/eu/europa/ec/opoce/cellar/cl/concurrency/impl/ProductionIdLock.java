/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.concurrency.impl
 *             FILE : ProductionIdsSharedLock.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 30, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.concurrency.impl;

import java.util.List;

/**
 * <class_description> The Cellar lock implementation to use when no cellar id exists.
 * <br/><br/>
 * ON : Apr 30, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class ProductionIdLock extends CellarLock {

    /**
     * The production ids synonyms.
     */
    private final List<String> pids;

    /**
     * Constructor.
     * @param pids the production ids synonyms
     */
    public ProductionIdLock(final List<String> pids) {
        super();
        this.pids = pids;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getPids() {
        return this.pids;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return this.pids.toString();
    }

    /**
     * Add a production id synonym.
     * @param pid the production id to add
     */
    public void addPid(final String pid) {
        this.pids.add(pid);
    }

    /**
     * Add a production ids synonyms.
     * @param pids the production ids to add
     */
    public void addAllPids(final List<String> pids) {
        this.pids.addAll(pids);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MODE getDefaultMode() {
        return MODE.READ;
    }
}
