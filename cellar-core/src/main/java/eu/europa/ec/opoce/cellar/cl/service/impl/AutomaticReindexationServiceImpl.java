/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *             FILE : java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 13 sept. 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.index.ScheduledReindexation;
import eu.europa.ec.opoce.cellar.cl.service.AutomaticReindexationService;
import eu.europa.ec.opoce.cellar.cl.service.ScheduledReindexationService;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cl.service.client.SparqlExecutingService;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CmrIndexRequestDao;
import eu.europa.ec.opoce.cellar.cmr.indexing.AutomaticReindexationRunnable;
import eu.europa.ec.opoce.cellar.cmr.indexing.service.IIndexRequestSchedulerService;
import eu.europa.ec.opoce.cellar.cmr.notice.indexing.cmr.CmrIndexRequestGenerationService;
import eu.europa.ec.opoce.cellar.server.admin.sparql.Query;
import eu.europa.ec.opoce.cellar.support.SparqlUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;

/**
 * The Class
 * <class_description> A meaningful description of the class that will be
 * displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 13 sept. 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@Service
//@DependsOn("org.springframework.context.config.internalBeanConfigurerAspect")
//necessary to load an @configurable within the post-construct
public class AutomaticReindexationServiceImpl implements AutomaticReindexationService {

    /**
     * The Constant LOG.
     */
    private static final Logger LOG = LogManager.getLogger(AutomaticReindexationServiceImpl.class);

    /**
     * The scheduled reindexation service.
     */
    @Autowired
    private ScheduledReindexationService scheduledReindexationService;

    /**
     * The task scheduler.
     */
    private ThreadPoolTaskScheduler taskScheduler;

    /**
     * The automatic reindexation runnable.
     */
    private AutomaticReindexationRunnable automaticReindexationRunnable;

    /**
     * The cellar configuration.
     */
    @Autowired
    @Qualifier("cellarConfiguration")
    private ICellarConfiguration cellarConfiguration;

    @Autowired
    private CmrIndexRequestGenerationService cmrIndexRequestGenerationService;

    @Autowired
    private CellarResourceDao cellarResourceDao;

    @Autowired
    private SparqlExecutingService sparqlExecutingService;

    @Autowired
    private CmrIndexRequestDao cmrIndexRequestDao;

    @Autowired
    @Qualifier("pidManagerService")
    private IdentifierService identifierService;

    @Autowired
    private IIndexRequestSchedulerService indexRequestSchedulerService;

    /**
     * Inits the scheduler.
     */
    @PostConstruct
    private void init() {
        final ScheduledReindexation scheduledReindexationConfiguration = this.scheduledReindexationService
                .getScheduledReindexationConfiguration();
        if (scheduledReindexationConfiguration != null) {
            final boolean valid = this.scheduledReindexationService.isValid(scheduledReindexationConfiguration);
            if (valid) {
                final boolean enabled = scheduledReindexationConfiguration.isEnabled();
                if (enabled) {//if the application is turned off but there's an active configuration
                    final String cron = scheduledReindexationConfiguration.getCron();
                    final CronTrigger cronTrigger = new CronTrigger(cron);
                    this.automaticReindexationRunnable = new AutomaticReindexationRunnable(scheduledReindexationService,
                            cellarConfiguration, cmrIndexRequestGenerationService, cellarResourceDao, sparqlExecutingService,
                            cmrIndexRequestDao, identifierService, indexRequestSchedulerService);
                    this.taskScheduler = new ThreadPoolTaskScheduler();
                    this.taskScheduler.initialize();
                    this.taskScheduler.schedule(this.automaticReindexationRunnable, cronTrigger);
                }
            } else {
                LOG.warn("There is no valid (re)indexation scheduler configuration.");
            }
        }
    }

    @PreDestroy
    private void destroy() {
        stop();
        taskScheduler.destroy();
    }

    /**
     * (re)schedule the execution of the tasks according to configuration.
     */
    private void schedule() {
        if (this.taskScheduler != null) {
            LOG.info("Future executions are being canceled.");
            this.taskScheduler.shutdown();
        }
        stop();

        final ScheduledReindexation scheduledReindexationConfiguration = this.scheduledReindexationService
                .getScheduledReindexationConfiguration();
        if (scheduledReindexationConfiguration != null) {
            final boolean valid = this.scheduledReindexationService.isValid(scheduledReindexationConfiguration);
            if (valid) {
                LOG.info("Scheduling a new configuration");
                this.automaticReindexationRunnable = new AutomaticReindexationRunnable(scheduledReindexationService,
                        cellarConfiguration, cmrIndexRequestGenerationService, cellarResourceDao, sparqlExecutingService,
                        cmrIndexRequestDao, identifierService, indexRequestSchedulerService);
                final String cron = scheduledReindexationConfiguration.getCron();
                final CronTrigger cronTrigger = new CronTrigger(cron);
                this.taskScheduler = new ThreadPoolTaskScheduler();
                this.taskScheduler.initialize();
                this.taskScheduler.schedule(this.automaticReindexationRunnable, cronTrigger);
                scheduledReindexationConfiguration.setEnabled(true);
                this.scheduledReindexationService.saveOrUpdate(scheduledReindexationConfiguration);
            } else {
                LOG.warn("There is no valid (re)indexation scheduler configuration.");
            }
        } else {
            LOG.warn("There is no (re)indexation scheduler configuration.");
        }

    }

    /**
     * Stop.
     */
    @Override
    public void stop() {
        if (this.automaticReindexationRunnable != null) {
            LOG.info("The automatic reindexation thread will be stopped. The indexation requests that were created " +
                    "before will continue on the queue.");
            this.automaticReindexationRunnable.stop();
        } else {
            LOG.warn("No reindexation thread was instantiated.");
        }
    }

    @Override
    public void disable() {
        stop();
        if (this.taskScheduler != null) {
            LOG.info("Future executions are being canceled.");
            this.taskScheduler.shutdown();
        }
        LOG.info("Disabling configuration.");
        final ScheduledReindexation scheduledReindexationConfiguration = this.scheduledReindexationService
                .getScheduledReindexationConfiguration();
        if (scheduledReindexationConfiguration != null) {
            scheduledReindexationConfiguration.setEnabled(false);
            this.scheduledReindexationService.saveOrUpdate(scheduledReindexationConfiguration);
        } else {
            LOG.warn("There is no (re)indexation scheduler configuration.");
        }
    }

    @Override
    public void enable() {
        schedule();
    }

    @Override
    public boolean isRunning() {
        return this.automaticReindexationRunnable != null && this.automaticReindexationRunnable.isRunning();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Query[] getExampleQueries() {
        final String pathToExampleQueries = this.cellarConfiguration.getCellarServiceIndexingScheduledExampleSparqlQueries();
        return SparqlUtils.getQueries(pathToExampleQueries);
    }

    @Override
    public String getQuery(final String query) throws IOException {
        final String pathToExampleQueries = this.cellarConfiguration.getCellarServiceIndexingScheduledExampleSparqlQueries();
        return SparqlUtils.getQuery(pathToExampleQueries, query);
    }

    @Override
    public void pause() {
        if (isRunning()) {
            if (this.automaticReindexationRunnable != null) {
                LOG.info("The automatic reindexation thread will be paused.");
                this.automaticReindexationRunnable.pause();
            } else {
                LOG.warn("No reindexation thread was instantiated.");
            }
        } else {
            LOG.warn("No reindexation thread is running.");
        }
    }

    @Override
    public void resume() {
        if (isPaused()) {
            if (this.automaticReindexationRunnable != null) {
                LOG.info("The automatic reindexation thread will be resumed.");
                this.automaticReindexationRunnable.resume();
            } else {
                LOG.warn("No reindexation thread was instantiated.");
            }
        } else {
            LOG.warn("No reindexation thread is paused.");
        }
    }

    @Override
    public boolean isPaused() {
        return this.automaticReindexationRunnable != null && this.automaticReindexationRunnable.isPaused();
    }

}
