/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.concurrency.impl
 *             FILE : CellarSharedLock2.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Apr 30, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.concurrency.impl;

import eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLock;
import eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLockSession;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * <class_description> Cellar lock.
 * <br/><br/>
 * ON : Apr 30, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public abstract class CellarLock implements ICellarLock {

    private final ReentrantReadWriteLock lock;

    private Set<ICellarLockSession> cellarLockSessions;

    /**
     * Default constructor.
     */
    protected CellarLock() {
        this.lock = new ReentrantReadWriteLock();
        this.cellarLockSessions = ConcurrentHashMap.newKeySet();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addLockSession(final ICellarLockSession cellarLockSession) {
        this.cellarLockSessions.add(cellarLockSession);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean removeLockSession(final ICellarLockSession cellarLockSession) {
        return this.cellarLockSessions.remove(cellarLockSession);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getLockCount() {
        return this.cellarLockSessions.size();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isNotUsed() {
        return this.cellarLockSessions.isEmpty();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Set<ICellarLockSession> getCellarLockSessions() {
        return this.cellarLockSessions;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void lock(final MODE mode) throws InterruptedException {
        if (this.getLockingMode(mode) == MODE.READ) {
            this.lock.readLock().lock();
        } else {
            this.lock.writeLock().lock();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean tryLock(final MODE mode) {
        if (this.getLockingMode(mode) == MODE.READ) {
            return this.lock.readLock().tryLock();
        } else {
            return this.lock.writeLock().tryLock();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void unlock(final MODE mode) {
        if (this.getLockingMode(mode) == MODE.READ) {
            if (this.lock != null && this.lock.readLock() != null) {
                this.lock.readLock().unlock();
            }

        } else {
            if (this.lock != null && this.lock.writeLock() != null) {
                this.lock.writeLock().unlock();
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MODE getLockingMode(final MODE mode) {
        if (mode == MODE.DEFAULT) {
            return this.getDefaultMode();
        } else {
            return mode;
        }
    }

}
