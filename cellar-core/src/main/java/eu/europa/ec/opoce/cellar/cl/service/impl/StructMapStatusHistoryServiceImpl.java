package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.cl.dao.StructMapStatusHistoryDao;
import eu.europa.ec.opoce.cellar.cl.domain.enums.IndexExecutionStatus;
import eu.europa.ec.opoce.cellar.cl.domain.enums.ProcessStatus;
import eu.europa.ec.opoce.cellar.cl.domain.package_history.PackageHistory;
import eu.europa.ec.opoce.cellar.cl.domain.structmap_status_history.StructMapStatusHistory;
import eu.europa.ec.opoce.cellar.cl.service.client.StructMapStatusHistoryService;
import java.util.Collections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.util.Date;
import java.util.List;

/**
 * <class_description> Service for logging entries in the 'STRUCTMAP_STATUS_HISTORY' table.
 *
 * ON : 15-06-2023
 *
 * @author EUROPEAN DYNAMICS S.A.
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
public class StructMapStatusHistoryServiceImpl implements StructMapStatusHistoryService {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(StructMapStatusHistoryServiceImpl.class);
    
    private final StructMapStatusHistoryDao structMapStatusHistoryDao;
    
    @Autowired
    public StructMapStatusHistoryServiceImpl(StructMapStatusHistoryDao structMapStatusHistoryDao) {
        this.structMapStatusHistoryDao = structMapStatusHistoryDao;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public StructMapStatusHistory saveEntry(StructMapStatusHistory structMapStatusHistory) {
        try{
            this.structMapStatusHistoryDao.saveObject(structMapStatusHistory);
            return structMapStatusHistory;
        }
        catch (final Exception e) {
            // Log error message
            LOGGER.error("An exception occurred while saving StructMapStatusHistory '" + structMapStatusHistory + "'.", e);
            // Mark this event transaction with rollback: this allows us not to throw the exception up and handle the rollback here
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return null;
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public StructMapStatusHistory getEntry(Long id) {
        try{
            return this.structMapStatusHistoryDao.getObject(id);
        }
        catch (Exception e) {
            // Log error message
            LOGGER.error("An exception occurred while retrieving the StructMapStatusHistory with id : " + id, e);
            return null;
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public List<StructMapStatusHistory> getByPackageHistory(PackageHistory packageHistory) {
        try {
            return this.structMapStatusHistoryDao.getByPackageHistory(packageHistory);
        }
        catch (Exception e) {
            // Log error message
            LOGGER.error("An exception occurred while retrieving the StructMapStatusHistory entries of PackageHistory : " + packageHistory, e);
            return Collections.emptyList();
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateAllStructMapIngestionStatus(PackageHistory packageHistory, ProcessStatus newStatus) {
        try{
            List<StructMapStatusHistory> structMapStatusHistories = this.getByPackageHistory(packageHistory);
            for (StructMapStatusHistory ssh : structMapStatusHistories){
                ssh.setIngestionStatus(newStatus);
                this.structMapStatusHistoryDao.updateObject(ssh);
            }
        }
        catch (Exception e) {
            // Log error message
            LOGGER.error("An exception occurred while updating the INGESTION_STATUS of all StructMapStatusHistory entries of PackageHistory '" + packageHistory + "'.", e);
            // Mark this event transaction with rollback: this allows us not to throw the exception up and handle the rollback here
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateMultipleStructMapIngestionStatus(List<StructMapStatusHistory> structMapStatusHistories, ProcessStatus newStatus) {
        try{
            for (StructMapStatusHistory ssh : structMapStatusHistories){
                ssh.setIngestionStatus(newStatus);
                this.structMapStatusHistoryDao.updateObject(ssh);
            }
        }
        catch (Exception e) {
            // Log error message
            LOGGER.error("An exception occurred while updating the INGESTION_STATUS of multiple StructMapStatusHistory entries.", e);
            // Mark this event transaction with rollback: this allows us not to throw the exception up and handle the rollback here
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public StructMapStatusHistory updateStructMapIngestionStatus(StructMapStatusHistory structMapStatusHistory, ProcessStatus newStatus) {
        try{
            StructMapStatusHistory cloneStructMapStatusHistory = new StructMapStatusHistory(structMapStatusHistory);
            cloneStructMapStatusHistory.setIngestionStatus(newStatus);
            return this.structMapStatusHistoryDao.updateObject(cloneStructMapStatusHistory);
        }
        catch (Exception e) {
            // Log error message
            LOGGER.error("An exception occurred while updating the INGESTION_STATUS of StructMapStatusHistory '" + structMapStatusHistory + "'.", e);
            // Mark this event transaction with rollback: this allows us not to throw the exception up and handle the rollback here
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            // Return original object
            return structMapStatusHistory;
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public StructMapStatusHistory updateStructMapCellarId(StructMapStatusHistory structMapStatusHistory, String cellarId) {
        try{
            StructMapStatusHistory cloneStructMapStatusHistory = new StructMapStatusHistory(structMapStatusHistory);
            cloneStructMapStatusHistory.setCellarId(cellarId);
            return this.structMapStatusHistoryDao.updateObject(cloneStructMapStatusHistory);
        }
        catch (Exception e) {
            // Log error message
            LOGGER.error("An exception occurred while updating the CELLAR_ID of StructMapStatusHistory '" + structMapStatusHistory + "'.", e);
            // Mark this event transaction with rollback: this allows us not to throw the exception up and handle the rollback here
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            // Return original object
            return structMapStatusHistory;
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public StructMapStatusHistory updateStructMapShaclReport(StructMapStatusHistory structMapStatusHistory, String shaclReport) {
        try{
            StructMapStatusHistory cloneStructMapStatusHistory = new StructMapStatusHistory(structMapStatusHistory);
            cloneStructMapStatusHistory.setShaclReport(shaclReport);
            return this.structMapStatusHistoryDao.updateObject(cloneStructMapStatusHistory);
        }
        catch (Exception e) {
            // Log error message
            LOGGER.error("An exception occurred while updating the SHACL_REPORT of StructMapStatusHistory '" + structMapStatusHistory + "'.", e);
            // Mark this event transaction with rollback: this allows us not to throw the exception up and handle the rollback here
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            // Return original object
            return structMapStatusHistory;
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public StructMapStatusHistory updateStructMapIndxStatusAndCreatedOn(StructMapStatusHistory structMapStatusHistory, Date createdOnDate) {
        try{
            StructMapStatusHistory cloneStructMapStatusHistory = new StructMapStatusHistory(structMapStatusHistory);
            cloneStructMapStatusHistory.setIndxExecutionStatus(IndexExecutionStatus.N);
            cloneStructMapStatusHistory.setIndxCreatedOnDate(createdOnDate);
            return this.structMapStatusHistoryDao.updateObject(cloneStructMapStatusHistory);
        }
        catch (Exception e) {
            // Log error message
            LOGGER.error("An exception occurred while updating the INDX_CREATED_ON of StructMapStatusHistory '" + structMapStatusHistory + "'.", e);
            // Mark this event transaction with rollback: this allows us not to throw the exception up and handle the rollback here
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            // Return original object
            return structMapStatusHistory;
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public StructMapStatusHistory updateStructMapIndxStatusToPending(StructMapStatusHistory structMapStatusHistory) {
        try {
            StructMapStatusHistory cloneStructMapStatusHistory = new StructMapStatusHistory(structMapStatusHistory);
            cloneStructMapStatusHistory.setIndxExecutionStatus(IndexExecutionStatus.P);
            return this.structMapStatusHistoryDao.updateObject(cloneStructMapStatusHistory);
        } catch (Exception e) {
            // Log error message
            LOGGER.error("An exception occurred while updating the INDX_EXECUTION_STATUS of StructMapStatusHistory '" + structMapStatusHistory + "' to PENDING.", e);
            // Mark this event transaction with rollback: this allows us not to throw the exception up and handle the rollback here
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            // Return original object
            return structMapStatusHistory;
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public StructMapStatusHistory updateStructMapIndxStatusAndExecutionStartDate(StructMapStatusHistory structMapStatusHistory, Date executionStartDate) {
        try{
            StructMapStatusHistory cloneStructMapStatusHistory = new StructMapStatusHistory(structMapStatusHistory);
            cloneStructMapStatusHistory.setIndxExecutionStatus(IndexExecutionStatus.X);
            cloneStructMapStatusHistory.setIndxExecutionStartDate(executionStartDate);
            return this.structMapStatusHistoryDao.updateObject(cloneStructMapStatusHistory);
        }
        catch (Exception e) {
            // Log error message
            LOGGER.error("An exception occurred while updating the INDX_EXECUTION_START_DATE of StructMapStatusHistory '" + structMapStatusHistory + "'.", e);
            // Mark this event transaction with rollback: this allows us not to throw the exception up and handle the rollback here
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            // Return original object
            return structMapStatusHistory;
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public StructMapStatusHistory updateStructMapIndxStatusAndExecutionDate(StructMapStatusHistory structMapStatusHistory, Date executionDate, IndexExecutionStatus newExecutionStatus) {
        try{
            StructMapStatusHistory cloneStructMapStatusHistory = new StructMapStatusHistory(structMapStatusHistory);
            cloneStructMapStatusHistory.setIndxExecutionStatus(newExecutionStatus);
            cloneStructMapStatusHistory.setIndxExecutionDate(executionDate);
            return this.structMapStatusHistoryDao.updateObject(cloneStructMapStatusHistory);
        }
        catch (Exception e) {
            // Log error message
            LOGGER.error("An exception occurred while updating the INDX_EXECUTION_ (DATE and STATUS) of StructMapStatusHistory '" + structMapStatusHistory + "'.", e);
            // Mark this event transaction with rollback: this allows us not to throw the exception up and handle the rollback here
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            // Return original object
            return structMapStatusHistory;
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public StructMapStatusHistory updateStructMapLanguages(StructMapStatusHistory structMapStatusHistory, String languages) {
        try{
            StructMapStatusHistory cloneStructMapStatusHistory = new StructMapStatusHistory(structMapStatusHistory);
            cloneStructMapStatusHistory.setLanguages(languages);
            return this.structMapStatusHistoryDao.updateObject(cloneStructMapStatusHistory);
        }
        catch (Exception e) {
            // Log error message
            LOGGER.error("An exception occurred while updating the LANGUAGES of StructMapStatusHistory '" + structMapStatusHistory + "'.", e);
            // Mark this event transaction with rollback: this allows us not to throw the exception up and handle the rollback here
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            // Return original object
            return structMapStatusHistory;
        }
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public StructMapStatusHistory resetStructMapAfterFailedIngestion(StructMapStatusHistory structMapStatusHistory) {
        try{
            StructMapStatusHistory cloneStructMapStatusHistory = new StructMapStatusHistory(structMapStatusHistory);
            cloneStructMapStatusHistory.setIngestionStatus(ProcessStatus.F);
            cloneStructMapStatusHistory.setIndxExecutionStatus(null);
            cloneStructMapStatusHistory.setIndxCreatedOnDate(null);
            return this.structMapStatusHistoryDao.updateObject(cloneStructMapStatusHistory);
        }
        catch (Exception e) {
            // Log error message
            LOGGER.error("An exception occurred while resetting the StructMapStatusHistory '" + structMapStatusHistory + "'.", e);
            // Mark this event transaction with rollback: this allows us not to throw the exception up and handle the rollback here
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            // Return original object
            return structMapStatusHistory;
        }
    }
}
