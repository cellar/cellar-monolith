/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR
 * 
 *     PACKAGE : eu.europa.ec.opoce.cellar.cl.dao
 *        FILE : PackageHasParentPackageDao.java
 * 
 *  CREATED BY : EUROPEAN DYNAMICS S.A.
 *          ON : 10-12-2021
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2021 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao;

import java.util.Collection;
import java.util.List;

import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.PackageHasParentPackage;

/**
 * Interface providing methods for accessing the
 * PACKAGE_HAS_PARENT_PACKAGE DB table.
 * @author EUROPEAN DYNAMICS S.A.
 */
public interface PackageHasParentPackageDao extends BaseDao<PackageHasParentPackage, Long> {

	/**
	 * Retrieves the DB queue entry whose SIP_ID matches the
	 * provided ID.
	 * @param sipId the SIP_ID to look for.
	 * @return the matching DB queue entry.
	 */
	PackageHasParentPackage findBySipId(Long sipId);
	
	/**
	 * Retrieves the DB queue entries whose SIP_ID or PARENT_SIP_ID
	 * match any of the provided IDs.
	 * @param sipIds the IDs to look for matches in the SIP_ID and PARENT_SIP_ID
	 * columns.
	 * @return the matching DB queue entries.
	 */
	List<PackageHasParentPackage> findBySipIdOrParentSipIdIn(Collection<Long> sipIds);
	
}
