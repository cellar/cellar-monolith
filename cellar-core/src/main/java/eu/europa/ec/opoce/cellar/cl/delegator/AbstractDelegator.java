/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.rdfStoreCleaner.delegator
 *             FILE : AbstractDelegator.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Oct 14, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.delegator;

import eu.europa.ec.opoce.cellar.cmr.CellarResource;

import java.util.Collection;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Oct 14, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public abstract class AbstractDelegator {

    /** The root cellar id. */
    private final String rootCellarId;

    /** The cellar resources. */
    protected Collection<CellarResource> cellarResources;

    /**
     * Instantiates a new abstract delegator.
     *
     * @param rootCellarId the root cellar id
     */
    protected AbstractDelegator(final String rootCellarId) {
        this(rootCellarId, null);
    }

    /**
     * Instantiates a new abstract delegator.
     *
     * @param rootCellarId the root cellar id
     * @param cellarResources the cellar resources
     */
    protected AbstractDelegator(final String rootCellarId, final Collection<CellarResource> cellarResources) {
        this.rootCellarId = rootCellarId;
        this.cellarResources = cellarResources;
    }

    /**
     * Gets the processable cellar resources.
     *
     * @return the processable cellar resources
     */
    public abstract Collection<CellarResource> getProcessableCellarResources();

    /**
     * Gets the cellar resources.
     *
     * @return the cellarResources
     */
    public Collection<CellarResource> getCellarResources() {
        return this.cellarResources;
    }

    /**
     * Gets the root cellar id.
     *
     * @return the rootCellarId
     */
    public String getRootCellarId() {
        return this.rootCellarId;
    }
}
