<html>
	<body>
		<p>The following EU-Login user has requested access to the CELLAR AdminUI:</p>
		<ul>
			<li>EU-Login ID: <b>${euLoginId}</b></li>
			<li>First name: ${firstName}</li>
			<li>Last name: ${lastName}</li>
			<li>Email: ${email}</li>
		</ul>
		<br>
		<p>You may respond to this access request via the "<i>Security</i>"/"<i>User Access Request Management</i>" panel of the CELLAR AdminUI.</p>
	</body>
</html>