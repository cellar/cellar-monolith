-----------------------------------------------------------------------------
 Cellar        (status: ${cellarAvailability})
-----------------------------------------------------------------------------
 Version       ${cellarConfigurationVersion}
 Base URL      ${cellarServerBaseUrl}
 Config. file  ${cellarConfigurationPath}
 Archive       ${cellarServiceArchiveEnabled}
 Indexing      ${cellarServiceIndexingEnabled}
 Ingestion     ${cellarServiceIngestionEnabled}
 NAL load      ${cellarServiceNalLoadEnabled}
 Ontol. load   ${cellarServiceOntoLoadEnabled}

-----------------------------------------------------------------------------
 S3            (status: ${s3Availability})
-----------------------------------------------------------------------------
 URL           https://s3-${s3Region}.amazonaws.com/${s3Bucket}

-----------------------------------------------------------------------------
 SHACL         (status: ${validationServiceAvailability})
-----------------------------------------------------------------------------
 URL           ${shaclServerBaseUrl}

-----------------------------------------------------------------------------
 SPARQL        (status: ${sparqlAvailability})
-----------------------------------------------------------------------------
 URL           ${sparqlServiceUri}

-----------------------------------------------------------------------------
 Status API    (status: ${cellarStatusServiceAvailability})
-----------------------------------------------------------------------------
 URL           ${cellarStatusServiceBaseUrl}

-----------------------------------------------------------------------------
 Database      (status: ${databaseAvailability})
-----------------------------------------------------------------------------
 URL           ${databaseUrl}
 Mode          ${databaseReadOnly}
 Cellar        ${databaseCellarAvailability}
 CMR           ${databaseCmrAvailability}
 Idol          ${databaseIdolDSAvailability}
 Virtuoso      ${databaseVirtuosoDSAvailability}

-----------------------------------------------------------------------------
 Health log    ${health}
-----------------------------------------------------------------------------
 ${msg0}
 ${msg1}
 ${msg2}
 ${msg3}
 ${msg4}
 ${msg5}
 ${msg6}
 ${msg7}
 ${msg8}
 ${msg9}
 ${msg10}
 ${msg11}
 ${msg12}
 ${msg13}
 ${msg14}
 ${msg15}
 ${msg16}
 ${msg17}
 ${msg18}
 ${msg19}
 ${msg20}
 ${msg21}
 ${msg22}
 ${msg23}
 ${msg24}
 ${msg25}
 ${msg26}
 ${msg27}
 ${msg28}
 ${msg29}
 ${msg30}

#############################################################################
Copyright (c) 2011-${year} European Union
#############################################################################
