/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *             FILE : ExternalLinkPatternCacheServiceTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 17, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.cl.domain.externalLink.ExternalLinkPattern;
import eu.europa.ec.opoce.cellar.common.Placeholder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;

/**
 * <class_description> External link patterns cache manager implementation test.
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 17, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class ExternalLinkPatternCacheServiceTest {

    private final static ExternalLinkPattern EXTERNAL_LINK_PATTERN;

    private ExternalLinkPatternCacheServiceImpl externalLinkPatternCacheService;

    static {
        EXTERNAL_LINK_PATTERN = new ExternalLinkPattern(new Long(1), "ESC/([0-9]{4})/([0-9]{1,4})/",
                "http://eescopinions.eesc.europa.eu/eescopiniondocument.aspx?language=${return lang;}$&docnr=${return idData[1];}$&year=${return idData[0];}$");
    }

    /**
     * Set up of the test.
     */
    @Before
    public void setUp() {
        this.externalLinkPatternCacheService = new ExternalLinkPatternCacheServiceImpl();
    }

    /**
     * Test the placeholder extraction.
     */
    @Test
    public void getExternalLinkPlaceholdersTest() {
        final LinkedList<Placeholder> placeholders = this.externalLinkPatternCacheService
                .getExternalLinkPlaceholders(EXTERNAL_LINK_PATTERN);

        String cmd = null;

        Placeholder placeholder = placeholders.get(0);
        Assert.assertEquals(69, placeholder.getStart());
        Assert.assertEquals(85, placeholder.getEnd());

        cmd = placeholder.getValue().substring(2);
        cmd = cmd.substring(0, cmd.length() - 2);

        Assert.assertEquals("return lang;", cmd);

        placeholder = placeholders.get(1);
        Assert.assertEquals(92, placeholder.getStart());
        Assert.assertEquals(113, placeholder.getEnd());

        cmd = placeholder.getValue().substring(2);
        cmd = cmd.substring(0, cmd.length() - 2);

        Assert.assertEquals("return idData[1];", cmd);

        placeholder = placeholders.get(2);
        Assert.assertEquals(119, placeholder.getStart());
        Assert.assertEquals(140, placeholder.getEnd());

        cmd = placeholder.getValue().substring(2);
        cmd = cmd.substring(0, cmd.length() - 2);

        Assert.assertEquals("return idData[0];", cmd);
    }
}
