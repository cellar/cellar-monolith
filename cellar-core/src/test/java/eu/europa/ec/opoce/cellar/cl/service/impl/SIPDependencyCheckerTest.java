package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.CoreErrors;
import eu.europa.ec.opoce.cellar.ccr.service.client.MetsUploadService;
import eu.europa.ec.opoce.cellar.ccr.service.impl.MetsServiceFactory;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.PrefixConfigurationService;
import eu.europa.ec.opoce.cellar.cl.dao.ProductionIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.service.client.*;
import eu.europa.ec.opoce.cellar.common.CatalogService;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import eu.europa.ec.opoce.cellar.common.service.TestingThreadService;
import eu.europa.ec.opoce.cellar.core.exception.UncompressException;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.exception.MetsException;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork;
import eu.europa.ec.opoce.cellar.support.FileExtensionUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ServiceLocator.class)
@PowerMockIgnore({"org.w3c.*", "org.xml.sax.*", "javax.xml.*", "javax.management.*"})
public class SIPDependencyCheckerTest {

    private FileService fileService;
    private MetsAnalyzer metsAnalyzer;
    private IdentifierService identifierService;
    private SipDependencyCheckerMetsParser sipDependencyCheckerMetsParser;
    private PrefixConfigurationService prefixConfigurationService;
    private ICellarConfiguration cellarConfiguration;
    private BackupService backupService;
    private MetsServiceFactory metsServiceFactory;
    private SIPDependencyCheckerImpl service;
    private AuditTrailEventService auditService;
    private CatalogService catalogService;
    private PackageHistoryService packageHistoryService;
    private StructMapStatusHistoryService structMapStatusHistoryService;
    private StructMapPidService structMapPidService;
    private TestingThreadService testingThreadService;
    private ProductionIdentifierDao productionIdentifierDao;
    private MetsUploadService metsUploadService;

    @Before
    public void setup() {
        prefixConfigurationService = mock(PrefixConfigurationService.class);
        fileService = mock(FileService.class);
        metsAnalyzer = mock(MetsAnalyzer.class);
        identifierService = mock(IdentifierService.class);
        sipDependencyCheckerMetsParser = mock(SipDependencyCheckerMetsParser.class);
        cellarConfiguration = mock(ICellarConfiguration.class);
        backupService = mock(BackupService.class);
        metsServiceFactory = mock(MetsServiceFactory.class);
        auditService = mock(AuditTrailEventService.class);
        catalogService = mock(CatalogService.class);
        packageHistoryService = mock(PackageHistoryService.class);
        structMapStatusHistoryService = mock(StructMapStatusHistoryService.class);
        structMapPidService = mock(StructMapPidService.class);
        testingThreadService = mock(TestingThreadService.class);
        productionIdentifierDao = mock(ProductionIdentifierDao.class);
        metsUploadService = mock(MetsUploadService.class);

        when(prefixConfigurationService.getPrefixConfiguration()).thenReturn(null);
        mockStatic(ServiceLocator.class);
        PowerMockito.when(ServiceLocator.getService(PrefixConfigurationService.class)).thenReturn(prefixConfigurationService);
        PowerMockito.when(ServiceLocator.getService(AuditTrailEventService.class)).thenReturn(auditService);
        PowerMockito.when(ServiceLocator.getService(CatalogService.class)).thenReturn(catalogService);

        service = new SIPDependencyCheckerImpl(fileService, metsAnalyzer, identifierService,
                sipDependencyCheckerMetsParser, prefixConfigurationService, cellarConfiguration,
                backupService, metsServiceFactory, packageHistoryService, structMapStatusHistoryService, structMapPidService,
                testingThreadService, productionIdentifierDao, metsUploadService);
    }

    @Test
    public void discoverResourcesTest() throws IOException {
        SIPWork sipWork = new SIPWork();
        sipWork.setSipFile(getSipFile());

        when(fileService.uncompressQuietlyMetadataOnly(any()))
                .thenReturn(StringUtils.removeEnd(getSipFile().getName(), FileExtensionUtils.SIP));
        when(fileService.getMetsFileFromTemporaryWorkFolder(any())).thenReturn(getMetsFile());
        when(cellarConfiguration.getCellarFolderTemporaryWork())
                .thenReturn("src/test/resources/eu/europa/ec/opoce/cellar/cl/service/impl/tmp");
        when(fileService.getAllFilesFromUncompressedLocation(anyString())).thenReturn(new File[]{getMetsFile()});
        when(sipDependencyCheckerMetsParser.parse(any(), any(), any())).thenReturn(getMetsInfo());
        when(cellarConfiguration.getCellarServiceSipQueueManagerFileAttributeDateType()).thenReturn("unix:ctime");
        when(cellarConfiguration.isTestEnabled()).thenReturn(false);
        service.discoverResources(sipWork);

        verify(fileService).uncompressQuietlyMetadataOnly(any());
        verify(fileService).getMetsFileFromTemporaryWorkFolder(any());
        verify(fileService).getAllFilesFromUncompressedLocation(anyString());
        verify(cellarConfiguration).getCellarFolderTemporaryWork();
        verify(sipDependencyCheckerMetsParser).parse(any(), any(), any());
    }

    @Test(expected = NullPointerException.class)
    public void uncompressExceptionE_018Test() throws IOException {
        SIPWork sipWork = new SIPWork();
        sipWork.setSipFile(getSipFile());

        when(fileService.uncompressQuietlyMetadataOnly(any()))
                .thenThrow(ExceptionBuilder.get(UncompressException.class).withCode(CoreErrors.E_018)
                .withMessage("Test").withMessageArgs("Test").silent(false).build());
        when(cellarConfiguration.getCellarServiceIngestionSipCopyRetryPauseSeconds()).thenReturn(1);
        when(cellarConfiguration.getCellarServiceIngestionSipCopyTimeoutSeconds()).thenReturn(1800);
        when(fileService.uncompressMetadataOnly(any()))
                .thenReturn(StringUtils.removeEnd(getSipFile().getName(), FileExtensionUtils.SIP));
        when(fileService.getMetsFileFromTemporaryWorkFolder(any()))
                .thenThrow(ExceptionBuilder.get(MetsException.class).build());
        when(cellarConfiguration.isTestEnabled()).thenReturn(false);
        service.discoverResources(sipWork);

        verify(fileService).uncompressQuietlyMetadataOnly(any());
        verify(fileService).uncompressMetadataOnly(any());
        verify(fileService).getMetsFileFromTemporaryWorkFolder(any());
        verify(cellarConfiguration).getCellarServiceIngestionSipCopyRetryPauseSeconds();
        verify(cellarConfiguration).getCellarServiceIngestionSipCopyTimeoutSeconds();
    }

    @Test(expected = NullPointerException.class)
    public void uncompressExceptionE_012Test() throws IOException {
        SIPWork sipWork = new SIPWork();
        sipWork.setSipFile(getSipFile());

        when(fileService.uncompressQuietlyMetadataOnly(any()))
                .thenThrow(ExceptionBuilder.get(UncompressException.class).withCode(CoreErrors.E_012)
                .withMessage("Test").withMessageArgs("Test").silent(false).build());
        when(cellarConfiguration.getCellarServiceIngestionSipCopyRetryPauseSeconds()).thenReturn(1);
        when(cellarConfiguration.getCellarServiceIngestionSipCopyTimeoutSeconds()).thenReturn(1800);
        when(fileService.uncompressMetadataOnly(any()))
                .thenReturn(StringUtils.removeEnd(getSipFile().getName(), FileExtensionUtils.SIP));
        when(fileService.getMetsFileFromTemporaryWorkFolder(any()))
                .thenThrow(ExceptionBuilder.get(MetsException.class).build());
        when(cellarConfiguration.isTestEnabled()).thenReturn(false);
        service.discoverResources(sipWork);

        verify(fileService).uncompressQuietlyMetadataOnly(any());
        verify(fileService).uncompressMetadataOnly(any());
        verify(fileService).getMetsFileFromTemporaryWorkFolder(any());
        verify(cellarConfiguration).getCellarServiceIngestionSipCopyRetryPauseSeconds();
        verify(cellarConfiguration).getCellarServiceIngestionSipCopyTimeoutSeconds();
    }

    @Test(expected = NullPointerException.class)
    public void uncompressExceptionOtherTest() {
        SIPWork sipWork = new SIPWork();
        sipWork.setSipFile(getSipFile());

        when(fileService.uncompressQuietlyMetadataOnly(any()))
                .thenThrow(ExceptionBuilder.get(UncompressException.class).withCode(CoreErrors.E_000)
                .withMessage("Test").withMessageArgs("Test").silent(false).build());
        when(cellarConfiguration.isTestEnabled()).thenReturn(false);
        service.discoverResources(sipWork);

        verify(fileService).uncompressQuietlyMetadataOnly(any());
    }

    @Test(expected = NullPointerException.class)
    public void parseExceptionTest() throws IOException {
        SIPWork sipWork = new SIPWork();
        sipWork.setSipFile(getSipFile());

        when(fileService.uncompressQuietlyMetadataOnly(any()))
                .thenReturn(StringUtils.removeEnd(getSipFile().getName(), FileExtensionUtils.SIP));
        when(fileService.getMetsFileFromTemporaryWorkFolder(any())).thenReturn(getMetsFile());
        when(cellarConfiguration.getCellarFolderTemporaryWork())
                .thenReturn("src/test/resources/eu/europa/ec/opoce/cellar/cl/service/impl/tmp");
        when(fileService.getAllFilesFromUncompressedLocation(anyString())).thenReturn(new File[]{getMetsFile()});
        when(sipDependencyCheckerMetsParser.parse(any(), any(), any()))
                .thenThrow(ExceptionBuilder.get(CellarException.class).withCode(CoreErrors.E_000)
                .withMessage("Test").withMessageArgs("Test").silent(false).build());
        when(cellarConfiguration.isTestEnabled()).thenReturn(false);
        service.discoverResources(sipWork);

        verify(fileService).uncompressQuietlyMetadataOnly(any());
        verify(fileService).getMetsFileFromTemporaryWorkFolder(any());
        verify(fileService).getAllFilesFromUncompressedLocation(anyString());
        verify(cellarConfiguration).getCellarFolderTemporaryWork();
        verify(sipDependencyCheckerMetsParser).parse(any(), any(), any());

    }

    @Test(expected = NullPointerException.class)
    public void operationExceptionTest() throws IOException {
        SIPWork sipWork = new SIPWork();
        sipWork.setSipFile(getSipFile());

        when(fileService.uncompressQuietlyMetadataOnly(any()))
                .thenReturn(StringUtils.removeEnd(getSipFile().getName(), FileExtensionUtils.SIP));
        when(fileService.getMetsFileFromTemporaryWorkFolder(any())).thenReturn(getMetsFile());
        when(cellarConfiguration.getCellarFolderTemporaryWork())
                .thenReturn("src/test/resources/eu/europa/ec/opoce/cellar/cl/service/impl/tmp");
        when(fileService.getAllFilesFromUncompressedLocation(anyString())).thenReturn(new File[]{getMetsFile()});
        when(sipDependencyCheckerMetsParser.parse(any(), any(), any())).thenReturn(getWrongMetsInfo());
        when(cellarConfiguration.isTestEnabled()).thenReturn(false);
        service.discoverResources(sipWork);

        verify(fileService).uncompressQuietlyMetadataOnly(any());
        verify(fileService).getMetsFileFromTemporaryWorkFolder(any());
        verify(fileService).getAllFilesFromUncompressedLocation(anyString());
        verify(cellarConfiguration).getCellarFolderTemporaryWork();
        verify(sipDependencyCheckerMetsParser).parse(any(), any(), any());

    }

    private File getSipFile() {
        String location = "src/test/resources/eu/europa/ec/opoce/cellar/cl/service/impl/sipFile.zip";
        return new File(location);
    }

    private File getMetsFile() {
        String location = "src/test/resources/eu/europa/ec/opoce/cellar/cl/service/impl/metsFile.zip";
        return new File(location);
    }

    private Map<String, String> getMetsInfo() {
        Map<String, String> mets = new HashMap<>();
        mets.put("metsType", "create");
        mets.put("metsId", "1");

        return mets;
    }

    private Map<String, String> getWrongMetsInfo() {
        Map<String, String> mets = new HashMap<>();
        mets.put("metsType", "wrong");
        mets.put("metsId", "1");

        return mets;
    }
}
