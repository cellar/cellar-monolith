package eu.europa.ec.opoce.cellar.cl.service.impl;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import eu.europa.ec.opoce.cellar.cl.dao.UserAccessRequestDao;
import eu.europa.ec.opoce.cellar.cl.dao.impl.UserAccessRequestDaoImpl;
import eu.europa.ec.opoce.cellar.cl.domain.admin.User;
import eu.europa.ec.opoce.cellar.cl.domain.admin.UserAccessRequest;
import eu.europa.ec.opoce.cellar.cl.service.client.SecurityService;
import eu.europa.ec.opoce.cellar.cl.service.client.UserAccessRequestService;
import eu.europa.ec.opoce.cellar.cl.service.mail.MailService;
import eu.europa.ec.opoce.cellar.cl.service.mail.MailServiceImpl;

/**
 * 
 * @author EUROPEAN DYNAMICS S.A
 */
public class UserAccessRequestServiceTest {

	/**
	 * 
	 */
	private UserAccessRequestService userAccessRequestService;
	/**
	 * 
	 */
	private UserAccessRequestDao userAccessRequestDao;
	/**
	 * 
	 */
	private SecurityService securityService;
	/**
	 * 
	 */
	private MailService mailService;
	
	@Before
	public void setup() {
		this.userAccessRequestDao = mock(UserAccessRequestDaoImpl.class);
		this.securityService = mock(SecurityServiceImpl.class);
		this.mailService = mock(MailServiceImpl.class);
		this.userAccessRequestService = new UserAccessRequestServiceImpl(userAccessRequestDao, securityService, mailService, null);
	}
	
	@Test
	public void findAllUserAccessRequestsTest() {
		when(this.userAccessRequestDao.findAll()).thenReturn(new ArrayList<>());
		this.userAccessRequestService.findAllUserAccessRequests();
		verify(this.userAccessRequestDao).findAll();
	}
	
	@Test
	public void findUserAccessRequestTest() {
		when(this.userAccessRequestDao.getObject(any())).thenReturn(new UserAccessRequest());
		this.userAccessRequestService.findUserAccessRequest(1L);
		verify(this.userAccessRequestDao).getObject(any());
	}
	
	@Test
	public void findByUsernameTest() {
		when(this.userAccessRequestDao.findByUsername(any())).thenReturn(new UserAccessRequest());
		this.userAccessRequestService.findByUsername("username");
		verify(this.userAccessRequestDao).findByUsername(any());
	}
	
	@Test
	public void deleteUserAccessRequestTest() {
		doNothing().when(this.userAccessRequestDao).deleteObject(any());
		this.userAccessRequestService.deleteUserAccessRequest(new UserAccessRequest(1L));
		verify(this.userAccessRequestDao).deleteObject(any());
	}
	
	@Test
	public void notifyAdminsViaEmailTest() {
		User adminUser = new User();
		adminUser.setId(1L);
		adminUser.setUsername("admin");
		adminUser.setEnabled(true);
		adminUser.setEmail("test");
		List<User> adminUsers = new ArrayList<>();
		adminUsers.add(adminUser);
		
		when(this.securityService.findUsersHavingRole(any())).thenReturn(adminUsers);
		doNothing().when(this.mailService).createEmail(any(), any(), any(), any());
		this.userAccessRequestService.notifyAdminsViaEmail(new HashMap<>());
		verify(this.mailService).createEmail(any(), any(), any(), any());
	}
	
	@Test
	public void createUserAccessRequestTest() {
		when(this.userAccessRequestDao.saveObject(new UserAccessRequest(1L))).thenReturn(new UserAccessRequest(1L));
		this.userAccessRequestService.createUserAccessRequest("id", "email");
		verify(this.userAccessRequestDao).saveObject(any());
	}
	
}
