package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.cl.dao.PackageHasParentPackageDao;
import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.PackageHasParentPackage;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class SIPQueueManagerStorageServiceTest {

    private PackageHasParentPackageDao packageHasParentPackageDao;

    private SIPQueueManagerStorageServiceImpl service;

    @Before
    public void setup() {
        packageHasParentPackageDao = mock(PackageHasParentPackageDao.class);
        service = new SIPQueueManagerStorageServiceImpl(packageHasParentPackageDao);
    }

    @Test
    public void updateQueueOnInsertTest() {
        Set<PackageHasParentPackage> entriesToInsert = createQueueEntriesToInsert();
        Map<Long, Long> entriesToUpdate = createQueueEntriesToUpdateOnInsert();

        when(packageHasParentPackageDao.findAll()).thenReturn(createExistingQueueEntries());
        service.updateQueueOnInsert(entriesToInsert, entriesToUpdate);

        verify(packageHasParentPackageDao).saveObject(anyObject());
        verify(packageHasParentPackageDao).findAll();
        verify(packageHasParentPackageDao).updateObject(anyObject());
    }

    @Test
    public void findBySipIdTest() {
        Collection<Long> ids = new ArrayList<>();
        ids.add(1L);
        ids.add(10L);

        when(packageHasParentPackageDao.findBySipIdOrParentSipIdIn(ids)).thenReturn(new ArrayList<>(createExistingQueueEntries()));
        List<PackageHasParentPackage> list = service.findBySipIdOrParentSipIdIn(ids);

        verify(packageHasParentPackageDao).findBySipIdOrParentSipIdIn(anyCollection());
        assertEquals(list, new ArrayList<>(createExistingQueueEntries()));
    }

    @Test
    public void updateQueueOnRemovalTest() {
        service.updateQueueOnRemoval(createQueueEntriesToRemove(), createQueueEntriesToUpdateOnRemoval());
        verify(packageHasParentPackageDao).updateObject(anyObject());
        verify(packageHasParentPackageDao).deleteObjects(anyObject());
    }

    private List<PackageHasParentPackage> createExistingQueueEntries() {
        List<PackageHasParentPackage> list = new ArrayList<>();
        PackageHasParentPackage phpp1 = new PackageHasParentPackage();
        PackageHasParentPackage phpp2 = new PackageHasParentPackage();

        phpp1.setId(1L);
        phpp1.setSipId(10L);
        phpp1.setParentSipId(null);
        list.add(phpp1);

        phpp2.setId(2L);
        phpp2.setSipId(11L);
        phpp2.setParentSipId(10L);
        list.add(phpp2);

        return list;
    }
    
    private Set<PackageHasParentPackage> createQueueEntriesToInsert() {
        Set<PackageHasParentPackage> set = new HashSet<>();
        PackageHasParentPackage phpp3 = new PackageHasParentPackage();

        phpp3.setId(3L);
        phpp3.setSipId(12L);
        phpp3.setParentSipId(10L);
        set.add(phpp3);

        return set;
    }
    
    private Map<Long, Long> createQueueEntriesToUpdateOnInsert() {
        Map<Long, Long> entriesToUpdate = new HashMap<>();
        entriesToUpdate.put(11L, 12L);

        return entriesToUpdate;
    }
    
    private Set<PackageHasParentPackage> createQueueEntriesToRemove() {
        Set<PackageHasParentPackage> set = new HashSet<>();
        PackageHasParentPackage phpp1 = new PackageHasParentPackage();
        PackageHasParentPackage phpp2 = new PackageHasParentPackage();

        phpp1.setId(1L);
        phpp1.setSipId(10L);
        phpp1.setParentSipId(null);
        set.add(phpp1);

        phpp2.setId(2L);
        phpp2.setSipId(11L);
        phpp2.setParentSipId(10L);
        set.add(phpp2);

        return set;
    }
    
    private Set<PackageHasParentPackage> createQueueEntriesToUpdateOnRemoval() {
        Set<PackageHasParentPackage> set = new HashSet<>();
        PackageHasParentPackage phpp3 = new PackageHasParentPackage();
        
        phpp3.setId(3L);
        phpp3.setSipId(12L);
        phpp3.setParentSipId(null);
        set.add(phpp3);
        
        return set;
    }

}
