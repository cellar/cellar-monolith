package eu.europa.ec.opoce.cellar.ccr.helper;

import eu.europa.ec.opoce.cellar.cl.configuration.PrefixConfigurationService;
import eu.europa.ec.opoce.cellar.cl.domain.admin.ProductionIdentifier;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.PrefixConfiguration;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.contains;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ServiceLocator.class)
@PowerMockIgnore("javax.management.*")
public class DigitalObjectHelperTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        PrefixConfiguration prefixConfiguration = mock(PrefixConfiguration.class);
        when(prefixConfiguration.getPrefixUri("cellar")).thenReturn("cellar");
        when(prefixConfiguration.getPrefixUri("celex")).thenReturn("celex");
        when(prefixConfiguration.getPrefixUri("oj")).thenReturn("oj");

        PowerMockito.mockStatic(ServiceLocator.class);
        when(ServiceLocator.getService(PrefixConfigurationService.class)).thenReturn(mock(PrefixConfigurationService.class));
        when(ServiceLocator.getService(PrefixConfigurationService.class).getPrefixConfiguration()).thenReturn(prefixConfiguration);
    }

    @Test
    public void assignCellarIdToWorkAlwaysSucceeds() throws Exception {
        DigitalObject digitalObject = new DigitalObject(null);
        ProductionIdentifier productionIdentifier = new ProductionIdentifier(
                new ContentIdentifier("cellar:uuid", true),
                new ContentIdentifier("celex:id", true));
        assertNull(digitalObject.getCellarId());
        DigitalObjectHelper.assignCellarIdentifier(digitalObject, productionIdentifier);
        assertEquals("cellar:uuid", digitalObject.getCellarId().getIdentifier());
    }

    @Test
    public void assignCellarIdToExpressionSucceedsIfPartOfWorkHierarchy() throws Exception {
        DigitalObject workDigitalObject = new DigitalObject(null);
        workDigitalObject.setCellarId(new ContentIdentifier("cellar:uuid", true));

        DigitalObject expressionDigitalObject = new DigitalObject(null);
        expressionDigitalObject.setParentObject(workDigitalObject);
        ProductionIdentifier productionIdentifier = new ProductionIdentifier(
                new ContentIdentifier("cellar:uuid.0001", true),
                new ContentIdentifier("celex:id", true));
        DigitalObjectHelper.assignCellarIdentifier(expressionDigitalObject, productionIdentifier);
        assertEquals("cellar:uuid.0001", expressionDigitalObject.getCellarId().getIdentifier());
    }

    @Test
    public void assignCellarIdToExpressionFailsIfExpressionNotPartOfWorkHierarchy() throws Exception {
        expectedException.expect(CellarException.class);
        expectedException.expectMessage(contains("E-039 [details=Child (pid : celex:id / cellarid : cellar:not_same_uuid.0001) cannot be part of parent's hierarchy (pid : () / cellarid : cellar:uuid)"));

        DigitalObject workDigitalObject = new DigitalObject(null);
        workDigitalObject.setCellarId(new ContentIdentifier("cellar:uuid", true));

        DigitalObject expressionDigitalObject = new DigitalObject(null);
        expressionDigitalObject.setParentObject(workDigitalObject);
        ProductionIdentifier productionIdentifier = new ProductionIdentifier(
                new ContentIdentifier("cellar:not_same_uuid.0001", true),
                new ContentIdentifier("celex:id", true));
        DigitalObjectHelper.assignCellarIdentifier(expressionDigitalObject, productionIdentifier);
    }

    @Test
    public void assignCellarIdToManifestationSucceedsIfPartOfExpressionHierarchy() throws Exception {
        DigitalObject workDigitalObject = new DigitalObject(null);
        workDigitalObject.setCellarId(new ContentIdentifier("cellar:uuid", true));

        DigitalObject expressionDigitalObject = new DigitalObject(null);
        expressionDigitalObject.setCellarId(new ContentIdentifier("cellar:uuid.0001", true));
        expressionDigitalObject.setParentObject(workDigitalObject);

        DigitalObject manifestationDigitalObject = new DigitalObject(null);
        ProductionIdentifier productionIdentifier = new ProductionIdentifier(
                new ContentIdentifier("cellar:uuid.0001.01", true),
                new ContentIdentifier("celex:id", true));
        DigitalObjectHelper.assignCellarIdentifier(manifestationDigitalObject, productionIdentifier);
        assertEquals("cellar:uuid.0001.01", manifestationDigitalObject.getCellarId().getIdentifier());
    }

    @Test
    public void assignCellarIdToManifestationFailsIfPartOfExpressionNotPartOfExpressionHierarchy() throws Exception {
        expectedException.expect(CellarException.class);
        expectedException.expectMessage(contains("E-039 [details=Child (pid : celex:id / cellarid : cellar:uuid.0002.04) cannot be part of parent's hierarchy (pid : () / cellarid : cellar:uuid.0028)"));

        DigitalObject workDigitalObject = new DigitalObject(null);
        workDigitalObject.setCellarId(new ContentIdentifier("cellar:uuid", true));

        DigitalObject expressionDigitalObject = new DigitalObject(null);
        expressionDigitalObject.setCellarId(new ContentIdentifier("cellar:uuid.0028", true));
        expressionDigitalObject.setParentObject(workDigitalObject);

        DigitalObject manifestationDigitalObject = new DigitalObject(null);
        manifestationDigitalObject.setParentObject(expressionDigitalObject);
        ProductionIdentifier productionIdentifier = new ProductionIdentifier(
                new ContentIdentifier("cellar:uuid.0002.04", true),
                new ContentIdentifier("celex:id", true));
        DigitalObjectHelper.assignCellarIdentifier(manifestationDigitalObject, productionIdentifier);
    }
}