package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.ccr.service.FileTypesNalSkosLoaderService;
import eu.europa.ec.opoce.cellar.cl.domain.mimeType.MimeTypeMapping;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class MimeTypeCacheServiceImplTest {

    private static MimeTypeCacheServiceImpl mimeTypeCacheService;
    private SkosLoaderServiceMock skosService;

    @BeforeClass
    public static void oneTimeSetup() throws Exception {
        mimeTypeCacheService = new MimeTypeCacheServiceImpl();
    }

    @Before
    public void setUp() throws Exception {
        skosService = new SkosLoaderServiceMock();
        mimeTypeCacheService.setSkosLoaderService(skosService);
    }

    @Test
    public void emptyPrioSetReturnsEmptyValueForAllMethods() throws Exception {
        assertEquals(0, mimeTypeCacheService.getManifestationTypesMap().size());
        assertEquals(0, mimeTypeCacheService.getMimeTypeMap().size());
        assertEquals(0, mimeTypeCacheService.getMimeTypesByPriorityDescList().size());
        assertEquals(0, mimeTypeCacheService.getMimeTypesSet().size());
    }

    @Test
    public void testGetMimeTypeSetWithOneElement() throws Exception {
        String mimeType = "application/mimetype";
        MimeTypeMapping mimeTypeMapping = new MimeTypeMapping();
        mimeTypeMapping.setMimeType(mimeType);
        skosService.addMimeTypeMapping(mimeTypeMapping);

        Set<String> mimeTypes = mimeTypeCacheService.getMimeTypesSet();
        assertEquals(1, mimeTypes.size());
        assertTrue(mimeTypes.contains(mimeType));
    }

    @Test
    public void getGetMimeTypeSetWithMultipleElements() throws Exception {
        String mimeTypeOne = "application/mimetypeone";
        MimeTypeMapping mimeTypeMapping = new MimeTypeMapping();
        mimeTypeMapping.setMimeType(mimeTypeOne);
        skosService.addMimeTypeMapping(mimeTypeMapping);

        String mimeTypeTwo = "application/mimetypetwo";
        mimeTypeMapping = new MimeTypeMapping();
        mimeTypeMapping.setMimeType(mimeTypeTwo);
        skosService.addMimeTypeMapping(mimeTypeMapping);

        Set<String> mimeTypes = mimeTypeCacheService.getMimeTypesSet();
        assertEquals(2, mimeTypes.size());
        assertTrue(mimeTypes.contains(mimeTypeOne));
        assertTrue(mimeTypes.contains(mimeTypeTwo));
    }

    @Test
    public void testPriorityOrderedDesc() throws Exception {
        String mimeTypePrioOne = "application/mimeTypePrio1";
        MimeTypeMapping mimeTypeMapping = new MimeTypeMapping();
        mimeTypeMapping.setMimeType(mimeTypePrioOne);
        mimeTypeMapping.setPriority(1L);
        skosService.addMimeTypeMapping(mimeTypeMapping);

        String mimeTypePrioThree = "application/mimeTypePrio3";
        mimeTypeMapping = new MimeTypeMapping();
        mimeTypeMapping.setMimeType(mimeTypePrioThree);
        mimeTypeMapping.setPriority(3L);
        skosService.addMimeTypeMapping(mimeTypeMapping);

        String mimeTypePrioTwo = "application/mimeTypePrio2";
        mimeTypeMapping = new MimeTypeMapping();
        mimeTypeMapping.setMimeType(mimeTypePrioTwo);
        mimeTypeMapping.setPriority(2L);
        skosService.addMimeTypeMapping(mimeTypeMapping);

        List<String> mimeTypesPrioDescOrdered = mimeTypeCacheService.getMimeTypesByPriorityDescList();
        assertEquals(3, mimeTypesPrioDescOrdered.size());
        assertEquals(mimeTypePrioThree, mimeTypesPrioDescOrdered.get(0));
        assertEquals(mimeTypePrioTwo, mimeTypesPrioDescOrdered.get(1));
        assertEquals(mimeTypePrioOne, mimeTypesPrioDescOrdered.get(2));
    }

    @Test
    public void testGetManifestationTypesMapWithOneElement() throws Exception {
        String mimeType = "image/tiff";
        String manifestationType = "tiff";
        String extension = "tiff";
        MimeTypeMapping mimeTypeMapping = new MimeTypeMapping();
        mimeTypeMapping.setMimeType(mimeType);
        mimeTypeMapping.setManifestationType(manifestationType);
        mimeTypeMapping.setExtension(extension);
        skosService.addMimeTypeMapping(mimeTypeMapping);

        Map<String, List<MimeTypeMapping>> manifestationTypeMap = mimeTypeCacheService.getManifestationTypesMap();
        assertEquals(1, manifestationTypeMap.size());
        assertNotNull(manifestationTypeMap.get(manifestationType));
        List<MimeTypeMapping> expectedMimeTypeMappingList = manifestationTypeMap.get(manifestationType);
        assertEquals(1, expectedMimeTypeMappingList.size());
        assertTrue(expectedMimeTypeMappingList.contains(mimeTypeMapping));
    }

    @Test
    public void testGetManifestationTypesMapWithMultipleElements() throws Exception {
        String mimeType = "text/html";
        String manifestationType = "html";
        String extension = "html";

        MimeTypeMapping mimeTypeMappingOne = new MimeTypeMapping();
        mimeTypeMappingOne.setMimeType(mimeType);
        mimeTypeMappingOne.setManifestationType(manifestationType);
        mimeTypeMappingOne.setExtension(extension);
        skosService.addMimeTypeMapping(mimeTypeMappingOne);

        MimeTypeMapping mimeTypeMappingTwo = new MimeTypeMapping();
        mimeTypeMappingTwo.setMimeType(mimeType);
        mimeTypeMappingTwo.setManifestationType(manifestationType);
        extension = "htm";
        mimeTypeMappingTwo.setExtension(extension);
        skosService.addMimeTypeMapping(mimeTypeMappingTwo);

        Map<String, List<MimeTypeMapping>> manifestationTypeMap = mimeTypeCacheService.getManifestationTypesMap();
        assertEquals(1, manifestationTypeMap.size());
        assertNotNull(manifestationTypeMap.get(manifestationType));
        List<MimeTypeMapping> mimeTypeMappingList = manifestationTypeMap.get(manifestationType);
        assertEquals(2, mimeTypeMappingList.size());
        assertTrue(mimeTypeMappingList.containsAll(Arrays.asList(mimeTypeMappingOne, mimeTypeMappingTwo)));
    }

    @Test
    public void testGetMimeTypeMapWithOneElement() throws Exception {
        String mimeType = "text/html";
        String manifestationType = "html";
        String extension = "html";

        MimeTypeMapping mimeTypeMapping = new MimeTypeMapping();
        mimeTypeMapping.setMimeType(mimeType);
        mimeTypeMapping.setManifestationType(manifestationType);
        mimeTypeMapping.setExtension(extension);
        skosService.addMimeTypeMapping(mimeTypeMapping);

        Map<String, List<MimeTypeMapping>> mimeTypeMap = mimeTypeCacheService.getMimeTypeMap();
        assertEquals(1, mimeTypeMap.size());
        assertNotNull(mimeTypeMap.get(mimeType));
        assertTrue(mimeTypeMap.get(mimeType).contains(mimeTypeMapping));
    }

    @Test
    public void testGetMimeTypeMapWithMultipleElements() throws Exception {
        String mimeType = "application/xhtml+xml;type=simplified";
        String manifestationType = "xhtml_simplified";
        String extension = "html";

        MimeTypeMapping mimeTypeMappingOne = new MimeTypeMapping();
        mimeTypeMappingOne.setMimeType(mimeType);
        mimeTypeMappingOne.setManifestationType(manifestationType);
        mimeTypeMappingOne.setExtension(extension);
        skosService.addMimeTypeMapping(mimeTypeMappingOne);

        MimeTypeMapping mimeTypeMappingTwo = new MimeTypeMapping();
        mimeTypeMappingTwo.setMimeType(mimeType);
        mimeTypeMappingTwo.setManifestationType(manifestationType);
        extension = "xhtml";
        mimeTypeMappingTwo.setExtension(extension);
        skosService.addMimeTypeMapping(mimeTypeMappingTwo);

        Map<String, List<MimeTypeMapping>> mimeTypeMap = mimeTypeCacheService.getMimeTypeMap();
        assertEquals(1, mimeTypeMap.size());
        assertNotNull(mimeTypeMap.get(mimeType));
        assertTrue(mimeTypeMap.get(mimeType).containsAll(Arrays.asList(mimeTypeMappingOne, mimeTypeMappingTwo)));
    }

    private class SkosLoaderServiceMock implements FileTypesNalSkosLoaderService {

        private Set<MimeTypeMapping> mimeTypeMappingByPriorityDesc;

        SkosLoaderServiceMock() {
            mimeTypeMappingByPriorityDesc = new HashSet<>();
        }

        public void addMimeTypeMapping(MimeTypeMapping mimeTypeMapping) {
            mimeTypeMappingByPriorityDesc.add(mimeTypeMapping);
        }

        @Override
        public boolean areAllItemsLoaded() {
            return true;
        }

        @Override
        public int numberOfItemDescriptionsLoaded() {
            return mimeTypeMappingByPriorityDesc.size();
        }

        @Override
        public List<MimeTypeMapping> getMimeTypeMappingsByExtension(String contentExtension) {
            return null;
        }

        @Override
        public Set<MimeTypeMapping> getMimeTypeMappingsByPriorityDesc() {
            return mimeTypeMappingByPriorityDesc;
        }

        @Override
        public List<MimeTypeMapping> getMimeTypeMappingByManifestationType(String manifestationType) {
            return null;
        }

        @Override
        public Set<String> getFileExtensionMatchingMimetype(String mimeType) {
            return null;
        }

        @Override
        public Set<String> getAllPossibleExtensions() {
            return null;
        }

        @Override
        public List<MimeTypeMapping> getMimeTypeMappingByMimeType(String mimeType) {
            return null;
        }
    }
}
