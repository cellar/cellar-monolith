package eu.europa.ec.opoce.cellar.splash;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SplashUtilsTest {

    @Test
    public void justifyHtmlSplashDiscardLineWithPlaceholder() {
        final String splash = SplashUtils.justifyHtmlSplash("line1 \n line2 \n ${line3}");
        assertEquals("line1&nbsp;<br/>&nbsp;line2&nbsp;<br/>", splash);
    }

}