package eu.europa.ec.opoce.cellar.ccr.utils;

import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

/**
 * @author EUROPEAN DYNAMICS S.A.
 */
public class MetsUploadUtilsTest {

    @Test
    public void resolveMetsIdTest() throws Exception {
        final String metsId = MetsUploadUtils.resolveMetsId(new File("src/test/resources/mets.zip"));
        assertEquals("filename", metsId);
    }
}
