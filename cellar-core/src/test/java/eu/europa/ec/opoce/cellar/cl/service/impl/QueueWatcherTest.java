package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.impl.CellarPersistentConfiguration;
import eu.europa.ec.opoce.cellar.cl.dao.PackageQueueDao;
import eu.europa.ec.opoce.cellar.cl.domain.enums.ProcessStatus;
import eu.europa.ec.opoce.cellar.cl.domain.package_history.PackageHistory;
import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.PackageQueue;
import eu.europa.ec.opoce.cellar.cl.runnable.IngestionRequestRunnable;
import eu.europa.ec.opoce.cellar.cl.service.IngestionService;
import eu.europa.ec.opoce.cellar.cl.service.client.PackageHistoryService;
import eu.europa.ec.opoce.cellar.cl.service.client.SIPDependencyCheckerStorageService;
import eu.europa.ec.opoce.cellar.common.concurrent.FutureCallback;
import eu.europa.ec.opoce.cellar.common.concurrent.ListenableFuture;
import eu.europa.ec.opoce.cellar.common.concurrent.ListeningExecutorService;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class QueueWatcherTest {

    private CellarPersistentConfiguration cellarConfiguration;
    private ListeningExecutorService priorityThreadPoolExecutor;
    private IngestionService ingestionService;
    private PackageQueueDao packageQueueDao;
    private FileSystemService fileSystemService;
    private SIPDependencyCheckerStorageService sipDependencyCheckerStorageService;
    private QueueWatcherImpl service;
    private PackageHistoryService packageHistoryService;

    @Before
    public void setup() {
        cellarConfiguration = mock(CellarPersistentConfiguration.class);
        priorityThreadPoolExecutor = mock(ListeningExecutorService.class);
        ingestionService = mock(IngestionService.class);
        packageQueueDao = mock(PackageQueueDao.class);
        fileSystemService = mock(FileSystemService.class);
        sipDependencyCheckerStorageService = mock(SIPDependencyCheckerStorageService.class);
        packageHistoryService = mock(PackageHistoryService.class);
        service = new QueueWatcherImpl(ingestionService, cellarConfiguration, priorityThreadPoolExecutor,
                packageQueueDao, fileSystemService, sipDependencyCheckerStorageService, packageHistoryService, null);
    }

    @Test
    public void watchQueueTest() {
        when(cellarConfiguration.isCellarServiceIngestionEnabled()).thenReturn(true);
        when(cellarConfiguration.getCellarServiceIngestionPoolThreads()).thenReturn(2,1, 0);
        when(packageQueueDao.findAll()).thenReturn(getPackageQueueList());
        when(fileSystemService.getSipFromReceptionFolder(any(), any()))
                .thenReturn(new File("src/test/resources/eu/europa/ec/opoce/cellar/cl/service/impl/sipFile.zip"));
        when(sipDependencyCheckerStorageService.updatePackageStatus(any(), any())).thenReturn(true);
        when(packageHistoryService.getEntry(getPackageQueueList().get(0).getPackageHistoryId())).thenReturn(getPackageHistory());
        when(priorityThreadPoolExecutor.submit(any(IngestionRequestRunnable.class))).thenReturn(getFuture());
        service.initialize();
        service.watchQueue();

        verify(cellarConfiguration,atLeastOnce()).isCellarServiceIngestionEnabled();
        verify(cellarConfiguration, times(2)).getCellarServiceIngestionPoolThreads();
        verify(packageQueueDao).findAll();
        verify(fileSystemService).getSipFromReceptionFolder(any(), any());
        verify(sipDependencyCheckerStorageService).updatePackageStatus(any(), any());
        verify(packageHistoryService).getEntry(getPackageQueueList().get(0).getPackageHistoryId());
        verify(packageHistoryService).updatePackageHistoryStatus(any(), any());
        verify(priorityThreadPoolExecutor).submit(any(IngestionRequestRunnable.class));
    }

    @Test
    public void reconfigureThreadPoolExecutorTest() {
        when(cellarConfiguration.getCellarServiceIngestionPoolThreads()).thenReturn(1);
        service.reconfigureThreadPoolExecutor();

        verify(cellarConfiguration).getCellarServiceIngestionPoolThreads();
    }

    @Test
    public void isIngestionTreatmentSetEmptyTest() {
        service.initialize();
        boolean result = service.isIngestionTreatmentSetEmpty();
        assertTrue(result);
    }

    private List<PackageQueue> getPackageQueueList() {
        List<PackageQueue> list = new ArrayList<>();

        PackageQueue packageQueue = new PackageQueue();
        packageQueue.setSipId(1L);
        packageQueue.setName("Test1");
        packageQueue.setType(SIPWork.TYPE.DAILY);
        packageQueue.setDetectionDate(new Date());
        packageQueue.setPackageHistoryId(12345L);
        list.add(packageQueue);

        packageQueue = new PackageQueue();
        packageQueue.setSipId(2L);
        packageQueue.setName("Test2");
        packageQueue.setType(SIPWork.TYPE.AUTHENTICOJ);
        packageQueue.setDetectionDate(new Date());
        packageQueue.setPackageHistoryId(56789L);
        list.add(packageQueue);

        return list;
    }
    
    private PackageHistory getPackageHistory(){
        return new PackageHistory(
                UUID.randomUUID().toString(),
                new Date(),
                "Test1", null, ProcessStatus.W);
    }

    private ListenableFuture<IngestionRequestRunnable> getFuture() {

        return new ListenableFuture<IngestionRequestRunnable>() {

            @Override
            public boolean cancel(boolean mayInterruptIfRunning) {
                return false;
            }

            @Override
            public boolean isCancelled() {
                return false;
            }

            @Override
            public boolean isDone() {
                return false;
            }

            @Override
            public IngestionRequestRunnable get() throws InterruptedException, ExecutionException {
                return null;
            }

            @Override
            public IngestionRequestRunnable get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
                return null;
            }

            @Override
            public void addCallback(FutureCallback<? super IngestionRequestRunnable> futureCallback) {

            }
        };
    }
}
