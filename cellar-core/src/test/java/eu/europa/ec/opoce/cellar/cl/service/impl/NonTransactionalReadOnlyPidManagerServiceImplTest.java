/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *             FILE : NonTransactionalReadOnlyPidManagerServiceImplTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 03 29, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-03-29 14:16:55 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.PrefixConfigurationService;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.when;

/**
 * @author ARHS Developments
 */
public class NonTransactionalReadOnlyPidManagerServiceImplTest {

    @Rule
    public ExpectedException thrown = ExpectedException.none();
    private NonTransactionalReadOnlyPidManagerServiceImpl pidService;

    @Before
    public void setup() {
        this.pidService = new NonTransactionalReadOnlyPidManagerServiceImpl();

        final PrefixConfigurationService prefixConfigurationService = Mockito.mock(PrefixConfigurationService.class);
        final Map<String, String> prefixes = new HashMap<>();
        prefixes.put("cellar", "http://[dissemination_base]/cellar/");
        prefixes.put("documentation", "http://[dissemination_base]/documentation/");
        when(prefixConfigurationService.getPrefixUris()).thenReturn(prefixes);
        when(prefixConfigurationService.getPrefixUri("cellar")).thenReturn(prefixes.get("cellar"));
        when(prefixConfigurationService.getPrefixUri("documentation")).thenReturn(prefixes.get("documentation"));

        ReflectionTestUtils.setField(pidService, "prefixConfigurationService", prefixConfigurationService);
    }

    @Test
    public void getNullPrefixedFromUriTest() {
        this.thrown.expect(CellarException.class);
        this.pidService.getPrefixedFromUri(null);
    }

    @Test
    public void getUnknownPrefixedFromUriTest() {
        this.thrown.expect(IllegalArgumentException.class);
        this.pidService.getPrefixedFromUri("http://[dissemination_base]/oj/JOC_2006_321_E_SOM.SWE");
    }


    @Test
    public void getPrefixedFromUriTest() {
        String pid = this.pidService.getPrefixedFromUri("http://[dissemination_base]/cellar/34383898-3350-11e8-9d6c-01aa75ed71a1");
        Assert.assertEquals(pid, "cellar:34383898-3350-11e8-9d6c-01aa75ed71a1");

        pid = this.pidService.getPrefixedFromUri("http://[dissemination_base]/cellar/34383898-3350-11e8-9d6c-01aa75ed71a1.0001.01/DOC_1");
        Assert.assertEquals(pid, "cellar:34383898-3350-11e8-9d6c-01aa75ed71a1.0001.01/DOC_1");

        pid = this.pidService.getPrefixedFromUri("http://[dissemination_base]/documentation/release/20180326-0");
        Assert.assertEquals(pid, "documentation:release/20180326-0");
    }
}