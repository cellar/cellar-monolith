/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *             FILE : ExtractionArchivingServiceTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 6, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.op.cellar.api.service.ICellarResponse;
import eu.europa.ec.op.cellar.api.service.ICellarService;
import eu.europa.ec.op.cellar.api.service.impl.CellarException;
import eu.europa.ec.op.cellar.api.util.WeightedLanguagePairs;
import eu.europa.ec.op.cellar.api.util.WeightedManifestationTypes;
import eu.europa.ec.opoce.cellar.ccr.service.FileTypesNalSkosLoaderService;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ArchiveTreeManager;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionWorkIdentifier;
import eu.europa.ec.opoce.cellar.cl.domain.mimeType.MimeTypeMapping;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import javax.ws.rs.core.MultivaluedMap;
import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

/**
 * <class_description> Test cases for the service
 * {@link ExtractionArchivingServiceImpl}. <br/>
 * <br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 * reminders about desired improvements, etc. <br/>
 * <br/>
 * ON : Dec 6, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class ExtractionArchivingServiceTest {

    private final static List<MimeTypeMapping> MIME_TYPE_MAPPINGS;

    private final static String BRANCH = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><NOTICE decoding=\"eng\" type=\"branch\"><WORK><URI><VALUE>http://cellar-dev.publications.europa.eu/resource/cellar/fdb43740-3af9-11e2-a29a-01aa75ed71a1.0007.01</VALUE><IDENTIFIER>fdb43740-3af9-11e2-a29a-01aa75ed71a1.0007.01</IDENTIFIER><TYPE>cellar</TYPE></URI></WORK></NOTICE>";
    private final static String CONTENT = "<!DOCTYPE html PUBLIC \"-//IETF//DTD HTML 2.0//EN\"><html><head><title>Content stream HTML</title></head><body><p>Content stream test</p></body></html>";

    /**
     * Test folder.
     */
    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder(); // must be
    // public

    /**
     * Extraction archiving service to test.
     */
    private ExtractionArchivingServiceImpl extractionArchivingService;

    /**
     * Weighted type service.
     */
    private WeightedTypeCacheServiceImpl weightedTypeCacheServiceImpl;

    /**
     * Cellar-API mock.
     */
    private ICellarService cellarApiServiceMock;

    /**
     * Mime type dao mock.
     */
    private FileTypesNalSkosLoaderService fileTypesNalSkosLoaderServiceMock;

    /**
     * Static constructor.
     */
    static {
        MIME_TYPE_MAPPINGS = new ArrayList<MimeTypeMapping>(4);
        MIME_TYPE_MAPPINGS.add(new MimeTypeMapping("text/html", "html", "html", "HTML", 32L, true));
        MIME_TYPE_MAPPINGS.add(new MimeTypeMapping("application/xhtml+xml", "xhtml", "html", "HTML", 36L, true));
        MIME_TYPE_MAPPINGS.add(new MimeTypeMapping("text/html;type=simplified", "html_simplified", "html", "HTML", 31L, true));
        MIME_TYPE_MAPPINGS.add(new MimeTypeMapping("application/xhtml+xml;type=simplified", "xhtml_simplified", "html", "HTML", 35L, true));
    }

    /**
     * Set up of the test.
     */
    @Before
    public void setUp() {
        this.extractionArchivingService = new ExtractionArchivingServiceImpl();
        this.fileTypesNalSkosLoaderServiceMock = EasyMock.createMock(FileTypesNalSkosLoaderService.class);
        this.cellarApiServiceMock = EasyMock.createMock(ICellarService.class);
        this.weightedTypeCacheServiceImpl = new WeightedTypeCacheServiceImpl();
        this.weightedTypeCacheServiceImpl.setFileTypesNalSkosLoaderService(fileTypesNalSkosLoaderServiceMock);
        this.extractionArchivingService.setMimeTypeMappingService(this.weightedTypeCacheServiceImpl);
        this.extractionArchivingService.setCellarApiService(this.cellarApiServiceMock);
    }

    /**
     * Test the extraction process.
     *
     * @throws CellarException
     * @throws IOException
     */
    @Test
    public void extractWorkIdentifiersTest() throws CellarException, IOException {
        final File extractionFolder = this.temporaryFolder.newFolder("extractionFolder");
        final String extractionFolderPath = extractionFolder.getAbsolutePath();

        // init mocks
        EasyMock.expect(this.cellarApiServiceMock
                                .getManifestation((String) EasyMock.notNull(), (WeightedManifestationTypes) EasyMock.notNull(),
                                                  (WeightedLanguagePairs) EasyMock.notNull())).andReturn(this.getCellarResponseContent())
                .times(4);
        EasyMock.expect(this.cellarApiServiceMock.getBranchNotice((String) EasyMock.notNull(), (WeightedLanguagePairs) EasyMock.notNull()))
                .andReturn(this.getCellarResponseBranch()).times(4);
        EasyMock.replay(this.cellarApiServiceMock);

        EasyMock.expect(this.fileTypesNalSkosLoaderServiceMock.getMimeTypeMappingsByExtension("html")).andReturn(MIME_TYPE_MAPPINGS)
                .times(1);
        EasyMock.replay(this.fileTypesNalSkosLoaderServiceMock);

        final List<ExtractionWorkIdentifier> workIdentifiers = new ArrayList<ExtractionWorkIdentifier>(4);
        for (int i = 0; i < 4; i++) {
            workIdentifiers.add(new ExtractionWorkIdentifier("id_" + i, null));
        }

        final ArchiveTreeManager archiveTreeManager = new ArchiveTreeManager(0, new Long(8), extractionFolderPath);

        this.extractionArchivingService.extractWorkIdentifiers("en", workIdentifiers, archiveTreeManager);

        Assert.assertEquals(8, extractionFolder.list().length); // 4 branches
        // and 4
        // contents

        EasyMock.verify(this.cellarApiServiceMock);
        EasyMock.verify(this.fileTypesNalSkosLoaderServiceMock);
    }

    /**
     * Test an error management in the extraction process.
     *
     * @throws CellarException
     * @throws IOException
     */
    @Test
    public void extractWorkIdentifiersTestFailed() throws CellarException, IOException {
        final File extractionFolder = this.temporaryFolder.newFolder("extractionFolder");
        final String extractionFolderPath = extractionFolder.getAbsolutePath();

        // init mocks
        EasyMock.expect(this.cellarApiServiceMock
                                .getManifestation((String) EasyMock.notNull(), (WeightedManifestationTypes) EasyMock.notNull(),
                                                  (WeightedLanguagePairs) EasyMock.notNull()))
                .andThrow(new CellarException(new Exception("Failed"), 1)).times(4); // throws
        // exceptions
        EasyMock.expect(this.cellarApiServiceMock.getBranchNotice((String) EasyMock.notNull(), (WeightedLanguagePairs) EasyMock.notNull()))
                .andThrow(new CellarException(new Exception("Failed"), 1)).times(4); // throws exceptions
        EasyMock.replay(this.cellarApiServiceMock);

        EasyMock.expect(this.fileTypesNalSkosLoaderServiceMock.getMimeTypeMappingsByExtension("html")).andReturn(MIME_TYPE_MAPPINGS)
                .times(1);
        EasyMock.replay(this.fileTypesNalSkosLoaderServiceMock);

        final List<ExtractionWorkIdentifier> workIdentifiers = new ArrayList<ExtractionWorkIdentifier>(4);
        for (int i = 0; i < 4; i++) {
            workIdentifiers.add(new ExtractionWorkIdentifier("id_" + i, null));
        }

        final ArchiveTreeManager archiveTreeManager = new ArchiveTreeManager(0, new Long(8), extractionFolderPath);

        this.extractionArchivingService.extractWorkIdentifiers("en", workIdentifiers, archiveTreeManager);

        Assert.assertEquals(0, extractionFolder.list().length); // nothing is
        // extracted

        EasyMock.verify(this.cellarApiServiceMock);
        EasyMock.verify(this.fileTypesNalSkosLoaderServiceMock);
    }

    /**
     * Test the creation of the tar.gz file.
     *
     * @throws IOException
     */
    @Test
    public void createTarGzOfFilesTest() throws IOException {
        final String archiveName = "/archiveTest";
        final File filesFolder = this.temporaryFolder.newFolder("filesFolder");
        final String filesFolderPath = filesFolder.getAbsolutePath();

        // fill a temp extraction folder
        for (int i = 0; i < 4; i++) {
            FileUtils.writeStringToFile(new File(filesFolderPath + "/id_" + i), CONTENT, (Charset) null);
        }

        final File tarGzFolder = this.temporaryFolder.newFolder("tarGzFolder");
        final String tarGzFolderPath = tarGzFolder.getAbsolutePath();

        this.extractionArchivingService.createTarGzOfFiles(filesFolderPath, tarGzFolderPath + archiveName + ".tar.gz"); // create
        // the
        // tar.gz

        // untargz the archive

        final File tarFolder = this.temporaryFolder.newFolder("tarFolder");
        final String tarFolderPath = tarFolder.getAbsolutePath();
        try(final FileInputStream fis=new FileInputStream(tarGzFolderPath + archiveName + ".tar.gz");
            final GZIPInputStream input = new GZIPInputStream(fis);
            final FileOutputStream fos = new FileOutputStream(new File(tarFolderPath + archiveName + ".tar"));)
    {
        org.apache.commons.compress.utils.IOUtils.copy(input, fos);
    }


        final File filesExtractedFolder = this.temporaryFolder.newFolder("filesExtractedFolder");
        try(final FileInputStream fiss=new FileInputStream(tarFolderPath + archiveName + ".tar");
            final TarArchiveInputStream tis = new TarArchiveInputStream(fiss);) {
            for (TarArchiveEntry entry = tis.getNextTarEntry(); entry != null; ) {
                this.unpackEntries(tis, entry, filesExtractedFolder);
                entry = tis.getNextTarEntry();
            }
        }
        Assert.assertEquals(4, filesExtractedFolder.list().length); // checks
        // the
        // number of
        // files
    }

    /**
     * Unpack a tar entry.
     *
     * @param tis
     *            the tar InputStream reader
     * @param entry
     *            tar the entry to unpack
     * @param outputDir
     *            the destination folder
     * @throws IOException
     */
    private void unpackEntries(final TarArchiveInputStream tis, final TarArchiveEntry entry, final File outputDir) throws IOException {
        final File outputFile = new File(outputDir, entry.getName());
        final BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(outputFile));

        try {
            final byte[] content = new byte[(int) entry.getSize()];

            tis.read(content);

            if (content.length > 0) {
                IOUtils.copy(new ByteArrayInputStream(content), outputStream);
            }
        } finally {
            IOUtils.closeQuietly(outputStream);
        }
    }

    /**
     * Returns an instance of {@link ICellarResponse} corresponding to a branch
     * notice.
     *
     * @return a branch notice response
     */
    private ICellarResponse getCellarResponseBranch() {
        return new ICellarResponse() {

            @Override
            public int getResponseStatus() {
                return 0;
            }

            @Override
            public InputStream getResponse() {
                return new ByteArrayInputStream(BRANCH.getBytes());
            }

            @Override
            public MultivaluedMap<String, String> getHeaders() {
                return null;
            }

            @Override
            public String getHeader(final String header) {
                return null;
            }

            @Override
            public void setResponse(final InputStream inputStream) {
            }
        };
    }

    /**
     * Returns an instance of {@link ICellarResponse} corresponding to a content
     * stream.
     *
     * @return a content stream response
     */
    private ICellarResponse getCellarResponseContent() {
        return new ICellarResponse() {

            @Override
            public int getResponseStatus() {
                return 0;
            }

            @Override
            public InputStream getResponse() {
                return new ByteArrayInputStream(CONTENT.getBytes());
            }

            @Override
            public MultivaluedMap<String, String> getHeaders() {
                return null;
            }

            @Override
            public String getHeader(final String header) {
                return null;
            }

            @Override
            public void setResponse(final InputStream inputStream) {
            }
        };
    }
}
