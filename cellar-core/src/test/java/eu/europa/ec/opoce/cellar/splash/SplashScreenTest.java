/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.splash
 *             FILE : SplashScreenTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Jun 29, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.splash;

import eu.europa.ec.opoce.cellar.CommonErrors;
import eu.europa.ec.opoce.cellar.ccr.service.FileTypesNalSkosLoaderService;
import eu.europa.ec.opoce.cellar.ccr.service.LanguagesNalSkosLoaderService;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.IS3Configuration;
import eu.europa.ec.opoce.cellar.cl.status.IDataSourceStatus;
import eu.europa.ec.opoce.cellar.exception.CellarConfigurationException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.server.admin.nal.OntoAdminConfigurationService;
import org.apache.commons.io.IOUtils;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Jun 29, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class SplashScreenTest {

    private ICellarConfiguration cellarConfigurationMock;
    private IS3Configuration S3ConfigurationMock;
    private IDataSourceStatus dataSourceStatusMock;
    private IDataSourceStatus oneDatasourceStatusMock;
    private SplashScreen splashScreen;
    private LanguagesNalSkosLoaderService languagesNalSkosLoaderServiceMock;
    private FileTypesNalSkosLoaderService fileTypesNalSkosLoaderServiceMock;
    private OntoAdminConfigurationService ontoAdminConfigurationService;

    /**
     * Set up of the test.
     */
    @Before
    public void setUp() {
        this.cellarConfigurationMock = EasyMock.createNiceMock(ICellarConfiguration.class);
        this.S3ConfigurationMock = EasyMock.createNiceMock(IS3Configuration.class);
        this.dataSourceStatusMock = EasyMock.createNiceMock(IDataSourceStatus.class);
        this.languagesNalSkosLoaderServiceMock = EasyMock.createNiceMock(LanguagesNalSkosLoaderService.class);
        this.fileTypesNalSkosLoaderServiceMock = EasyMock.createNiceMock(FileTypesNalSkosLoaderService.class);
        this.ontoAdminConfigurationService = EasyMock.createNiceMock(OntoAdminConfigurationService.class);
        this.oneDatasourceStatusMock = EasyMock.createNiceMock(IDataSourceStatus.class);

        this.splashScreen = new SplashScreen();
        this.splashScreen.cellarConfiguration = this.cellarConfigurationMock;
        this.splashScreen.s3Configuration = this.S3ConfigurationMock;
        this.splashScreen.validationServiceStatus = this.dataSourceStatusMock;
        this.splashScreen.statusServiceStatus = this.dataSourceStatusMock;
        this.splashScreen.sparqlServerStatus = this.dataSourceStatusMock;
        this.splashScreen.cellarDataSourceStatus = this.dataSourceStatusMock;
        this.splashScreen.cmrDataSourceStatus = this.dataSourceStatusMock;
        this.splashScreen.idolDataSourceStatus = this.oneDatasourceStatusMock;
        this.splashScreen.virtuosoDataSourceStatus = this.dataSourceStatusMock;
        this.splashScreen.languagesNalSkosLoaderService = this.languagesNalSkosLoaderServiceMock;
        this.splashScreen.fileTypesNalSkosLoaderService = this.fileTypesNalSkosLoaderServiceMock;
        this.splashScreen.ontoAdminConfigurationService = this.ontoAdminConfigurationService;

    }

    /**
     * Tests the coherence of the splash screen.
     */
    @Test
    public void test() throws IOException {
        EasyMock.replay(this.cellarConfigurationMock);
        EasyMock.replay(this.S3ConfigurationMock);
        EasyMock.replay(this.dataSourceStatusMock);
        EasyMock.replay(this.languagesNalSkosLoaderServiceMock);
        EasyMock.replay(this.fileTypesNalSkosLoaderServiceMock);
        EasyMock.replay(this.ontoAdminConfigurationService);
        this.splashScreen.refresh();
        EasyMock.verify(this.cellarConfigurationMock);
        EasyMock.verify(this.S3ConfigurationMock);
        EasyMock.verify(this.dataSourceStatusMock);
        EasyMock.verify(this.languagesNalSkosLoaderServiceMock);
        EasyMock.verify(this.fileTypesNalSkosLoaderServiceMock);
        EasyMock.verify(this.ontoAdminConfigurationService);

        final String splash = this.splashScreen.getPlainSplash();
        BufferedReader br = null;
        try {
            br = new BufferedReader(new StringReader(splash));
            String thisLine = null;
            int splashWidth = -1;
            while ((thisLine = br.readLine()) != null) {
                // get the length of the first line as the splash width
                if (splashWidth == -1) {
                    splashWidth = thisLine.length();
                }

                // check that each line has the same length as the first
                assertEquals(thisLine.length(), splashWidth);

                // check that each line ends with "###", "--#" or "  #"
                Assert.assertTrue(//
                        thisLine.endsWith("###") || //
                                thisLine.endsWith("--#") || //
                                thisLine.endsWith("  #") //
                );

                // check that each line does not contain ${
                Assert.assertTrue(!thisLine.contains("${"));
            }
        } finally {
            IOUtils.closeQuietly(br);
        }
    }

    @Test
    public void indexingEnableFlagHasNoImpactOnDatasourceOnlineStatus() throws Exception {
        EasyMock.expect(cellarConfigurationMock.isCellarServiceIndexingEnabled()).andReturn(false);
        EasyMock.replay(cellarConfigurationMock);
        this.splashScreen.refresh();
        final String splash = this.splashScreen.getPlainSplash();

        Pattern idolDsOnlinePattern = Pattern.compile("#\\sIdol\\s*online");
        assertTrue(idolDsOnlinePattern.matcher(splash).find());

        Pattern indexingDisabledPattern = Pattern.compile("#\\sIndexing\\s*disabled");
        assertTrue(indexingDisabledPattern.matcher(splash).find());
    }

    @Test
    public void virtuosoIngestionEnabledHasNoImpactOnDatasourceOnlineStatus() throws Exception {
        EasyMock.expect(cellarConfigurationMock.isVirtuosoIngestionEnabled()).andReturn(false);
        EasyMock.expect(cellarConfigurationMock.isCellarServiceIngestionEnabled()).andReturn(true);
        EasyMock.replay(cellarConfigurationMock);
        this.splashScreen.refresh();
        final String splash = this.splashScreen.getPlainSplash();

        Pattern virtuosoDsOnlinePattern = Pattern.compile("#\\sVirtuoso\\s*online");
        assertTrue(virtuosoDsOnlinePattern.matcher(splash).find());
    }

    @Test
    public void testIdolDataSourceOffline() throws Exception {
        EasyMock.expect(cellarConfigurationMock.isCellarServiceIndexingEnabled()).andReturn(true);
        oneDatasourceStatusMock.check();
        EasyMock.expectLastCall().andThrow(
                ExceptionBuilder.get(CellarConfigurationException.class).withCode(CommonErrors.SERVICE_NOT_AVAILABLE)
                        .withMessage("exception thrown for testing").build()
        );
        EasyMock.replay(oneDatasourceStatusMock);
        EasyMock.replay(cellarConfigurationMock);
        this.splashScreen.refresh();
        final String splash = this.splashScreen.getPlainSplash();

        Pattern idolDsOfflinePattern = Pattern.compile("#\\sIdol\\s*offline");
        assertTrue(idolDsOfflinePattern.matcher(splash).find());
    }
}
