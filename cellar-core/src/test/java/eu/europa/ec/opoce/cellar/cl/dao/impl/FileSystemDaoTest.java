package eu.europa.ec.opoce.cellar.cl.dao.impl;

import eu.europa.ec.opoce.cellar.audit.AuditAdviceTest;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.impl.CellarStaticConfiguration;
import eu.europa.ec.opoce.cellar.cl.service.client.AuditTrailEventService;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import eu.europa.ec.opoce.cellar.core.exception.UncompressException;
import eu.europa.ec.opoce.cellar.exception.FileSystemException;
import eu.europa.ec.opoce.cellar.exception.MetsException;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork;
import org.apache.commons.io.FileUtils;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PrepareForTest(ServiceLocator.class)
@PowerMockIgnore({"org.w3c.*", "org.xml.sax.*", "javax.xml.*", "javax.management.*"})
@ContextConfiguration(classes = AuditAdviceTest.AuditAdviceTestConfiguration.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FileSystemDaoTest {

    private static File sipFileWithRelativeHrefVersionOne;
    private static File sipFileWithoutRelativeHrefVersionOne;
    private static File textFileWithZipExtension;
    private static File textFileWithFileExtension;
    private static Path rootPathDirectory;

    private FileSystemDao fileSystemDao;
    private static ICellarConfiguration cellarConfiguration;

    @Autowired
    private AuditTrailEventService auditTrailEventService;

    @BeforeClass
    public static void oneTimeSetup() throws Exception {
        String sipFileWithRelativeHrefVersionOnePath = "src/test/resources/eu/europa/ec/opoce/cellar/cl/dao/impl/sipFileWithRelativeHref_v1.zip";
        sipFileWithRelativeHrefVersionOne = new File(sipFileWithRelativeHrefVersionOnePath);

        String sipFileWithoutRelativeHrefVersionOnePath = "src/test/resources/eu/europa/ec/opoce/cellar/cl/dao/impl/sipFileWithoutRelativeHref_v1.zip";
        sipFileWithoutRelativeHrefVersionOne = new File(sipFileWithoutRelativeHrefVersionOnePath);

        String textFile = "src/test/resources/eu/europa/ec/opoce/cellar/cl/dao/impl/file.txt";
        textFileWithFileExtension = new File(textFile);

        String textFileWithZipExt = "src/test/resources/eu/europa/ec/opoce/cellar/cl/dao/impl/file_2.zip";
        textFileWithZipExtension = new File(textFileWithZipExt);

        rootPathDirectory = Paths.get(System.getProperty("java.io.tmpdir") + File.separator + "CELLAR");

        cellarConfiguration = mock(CellarStaticConfiguration.class);
        when(cellarConfiguration.getCellarFolderAuthenticOJReception()).thenReturn(rootPathDirectory + File.separator + "authOj");
        when(cellarConfiguration.getCellarFolderDailyReception()).thenReturn(rootPathDirectory + File.separator + "dailyReception");
        when(cellarConfiguration.getCellarFolderExportRest()).thenReturn(rootPathDirectory + File.separator + "exportRest");
        when(cellarConfiguration.getCellarFolderExportUpdateResponse()).thenReturn(rootPathDirectory + File.separator + "exportUpdateResponse");
        when(cellarConfiguration.getCellarFolderExportBatchJob()).thenReturn(rootPathDirectory + File.separator + "exportBatchJob");
        when(cellarConfiguration.getCellarFolderError()).thenReturn(rootPathDirectory + File.separator + "error");
        when(cellarConfiguration.getCellarFolderBulkReception()).thenReturn(rootPathDirectory + File.separator + "bulkReception");
        when(cellarConfiguration.getCellarFolderBulkLowPriorityReception()).thenReturn(rootPathDirectory + File.separator + "bulkLowPriorityReception");
        when(cellarConfiguration.getCellarFolderTemporaryWork()).thenReturn(rootPathDirectory + File.separator + "temporaryWork");
        when(cellarConfiguration.getCellarFolderFoxml()).thenReturn(rootPathDirectory + File.separator + "foxml");
        when(cellarConfiguration.getCellarFolderDailyResponse()).thenReturn(rootPathDirectory + File.separator + "dailyResponse");
        when(cellarConfiguration.getCellarFolderBulkResponse()).thenReturn(rootPathDirectory + File.separator + "bulkResponse");
        when(cellarConfiguration.getCellarFolderBulkLowPriorityResponse()).thenReturn(rootPathDirectory + File.separator + "bulkLowPriorityResponse");
        when(cellarConfiguration.getCellarFolderBackup()).thenReturn(rootPathDirectory + File.separator + "backup");
        when(cellarConfiguration.getCellarFolderExportBatchJobTemporary()).thenReturn(rootPathDirectory + File.separator + "batchJobTemporary");
        when(cellarConfiguration.getCellarFolderAuthenticOJResponse()).thenReturn(rootPathDirectory + File.separator + "authOjResponse");
        when(cellarConfiguration.getCellarFolderRoot()).thenReturn(rootPathDirectory + File.separator);
        when(cellarConfiguration.getCellarFolderCmrLog()).thenReturn(rootPathDirectory + File.separator + "logs");
        when(cellarConfiguration.getCellarFolderTemporaryDissemination()).thenReturn(rootPathDirectory + File.separator + "tmpDissemination");
        when(cellarConfiguration.getCellarFolderTemporaryWork()).thenReturn(rootPathDirectory + File.separator + "tmpWork");
        when(cellarConfiguration.getCellarFolderTemporaryStore()).thenReturn(rootPathDirectory + File.separator + "tmpStore");
        when(cellarConfiguration.getCellarFolderExportRest()).thenReturn(rootPathDirectory + File.separator + "exportRest");
        when(cellarConfiguration.getCellarFolderExportUpdateResponse()).thenReturn(rootPathDirectory + File.separator + "exportUpdateReponse");
        when(cellarConfiguration.getCellarFolderLicenseHolderArchiveExtraction()).thenReturn(rootPathDirectory + File.separator + "licenceArchiveExtract");
        when(cellarConfiguration.getCellarFolderLicenseHolderArchiveAdhocExtraction()).thenReturn(rootPathDirectory + File.separator + "licenceArchiveExtractAdHoc");
        when(cellarConfiguration.getCellarFolderLicenseHolderTemporaryArchive()).thenReturn(rootPathDirectory + File.separator + "licenceArchivetmp");
        when(cellarConfiguration.getCellarFolderLicenseHolderTemporarySparql()).thenReturn(rootPathDirectory + File.separator + "licencetmpSparql");
        when(cellarConfiguration.getCellarFolderBatchJobSparqlQueries()).thenReturn(rootPathDirectory + File.separator + "batchJobSparqlQueries");
        when(cellarConfiguration.getCellarFolderSparqlResponseTemplates()).thenReturn(rootPathDirectory + File.separator + "sparqlResponseTemplates");
        when(cellarConfiguration.getCellarFolderAuthenticOJReception()).thenReturn(rootPathDirectory + File.separator + "authenticOJReception");
        when(cellarConfiguration.getCellarFolderAuthenticOJResponse()).thenReturn(rootPathDirectory + File.separator + "authenticOJResponse");
        when(cellarConfiguration.getCellarFolderLock()).thenReturn(rootPathDirectory + File.separator + "lock");
    }

    @Before
    public void setup() throws Exception {
        PowerMockito.mockStatic(ServiceLocator.class);
        PowerMockito.when(ServiceLocator.getService(AuditTrailEventService.class)).thenReturn(auditTrailEventService);

        if (Files.exists(rootPathDirectory)) {
            Files.walk(rootPathDirectory).filter(path -> !path.equals(rootPathDirectory)).sorted(Comparator.reverseOrder())
                    .map(Path::toFile).forEach(File::delete);
        }
        when(cellarConfiguration.isCellarServiceIngestionEnabled()).thenReturn(true);
        fileSystemDao = new FileSystemDao(cellarConfiguration);
        fileSystemDao.initialize();
    }

    @Test
    public void uncompressWithoutRelativeFolderInXlinkhrefShouldWork() throws Exception {
        String uncompressed = fileSystemDao.uncompress(sipFileWithoutRelativeHrefVersionOne);
        assertEquals("sipFileWithoutRelativeHref_v1", uncompressed);
    }

    @Test
    public void uncompressWithRelativeFolderInXlinkhrefShouldWork() throws Exception {
        String uncompressed = fileSystemDao.uncompress(sipFileWithRelativeHrefVersionOne);
        assertEquals("sipFileWithRelativeHref_v1", uncompressed);
    }

    @Test
    public void getAllFilesFromDirectoryWithoutSubfoldersReturnAllFilename() throws Exception {
        String uncompressedFolderName = fileSystemDao.uncompress(sipFileWithoutRelativeHrefVersionOne);
        Path uncompressedFolder = Paths.get(uncompressedFolderName);
        File[] uncompressedFilesList = fileSystemDao.getAllFilesFromUncompressedLocation(uncompressedFolder.toString());
        for (File file : uncompressedFilesList) {
            assertFalse(file.toString().contains(File.separator));
            Path filePath = Paths.get(cellarConfiguration.getCellarFolderTemporaryWork() + File.separator + uncompressedFolderName + File.separator + file.toString());
            assertTrue(Files.exists(filePath));
        }
    }

    @Test
    public void getAllFilesFromDirectoryWithSubfoldersReturnFilenamesRelativeToDirectory() throws Exception {
        String uncompressedFolderName = fileSystemDao.uncompress(sipFileWithRelativeHrefVersionOne);
        Path uncompressedFolder = Paths.get(uncompressedFolderName);
        File[] uncompressedFilesList = fileSystemDao.getAllFilesFromUncompressedLocation(uncompressedFolder.toString());
        for (File file : uncompressedFilesList) {
            Path filePath = Paths.get(cellarConfiguration.getCellarFolderTemporaryWork() + File.separator + uncompressedFolderName + File.separator + file.toString());
            assertTrue(Files.exists(filePath));
        }
    }

    @Test
    public void movingFolderToErrorFolderAlsoDeletesSourceFolder() throws Exception {
        String uncompressedFolderName = fileSystemDao.uncompress(sipFileWithRelativeHrefVersionOne);
        fileSystemDao.moveMetsToErrorFolder(uncompressedFolderName);

        Path uncompressedPath = Paths.get(cellarConfiguration.getCellarFolderTemporaryWork() + File.separator + uncompressedFolderName);
        assertFalse(Files.exists(uncompressedPath));

        Path uncompressedFolderInErrorPath = Paths
                .get(cellarConfiguration.getCellarFolderError() + File.separator + uncompressedFolderName);
        assertTrue(Files.exists(uncompressedFolderInErrorPath));
    }

    @Test
    public void movingFolderToErrorFolderKeepsFolderHierarchy() throws Exception {
        String uncompressedFolderName = fileSystemDao.uncompress(sipFileWithRelativeHrefVersionOne);
        Path sourceRootFolder = Paths.get(cellarConfiguration.getCellarFolderTemporaryWork() + File.separator + uncompressedFolderName);
        String sourceRootRelativePathRepresentation = getRelativeFilesPathAsString(sourceRootFolder);
        fileSystemDao.moveMetsToErrorFolder(uncompressedFolderName);
        Path destinationFolderInErrorPath = Paths.get(cellarConfiguration.getCellarFolderError() + File.separator + uncompressedFolderName);
        String destinationRootRelativePathRepresentation = getRelativeFilesPathAsString(destinationFolderInErrorPath);
        assertEquals(sourceRootRelativePathRepresentation, destinationRootRelativePathRepresentation);
    }

    /*
     * Simple method to append all files name (relative path) from a folder
     */
    private String getRelativeFilesPathAsString(Path rootPath) {
        StringBuilder builder = new StringBuilder();
        List<File> filesInFolder = new ArrayList<>(FileUtils.listFiles(rootPath.toFile(), null, true));
        Collections.sort(filesInFolder);
        for (File file : filesInFolder) {
            Path relativePath = rootPath.relativize(file.toPath().toAbsolutePath());
            builder.append(relativePath.toString());
        }
        return builder.toString();
    }

    @Test
    public void movingFoxmlFolderToBackupFolderMovesHierarchyToDestinationAndDeleteSource() throws Exception {
        String uncompressedFolderName = fileSystemDao.uncompress(sipFileWithRelativeHrefVersionOne);
        Path foxmlFolder = Paths.get(cellarConfiguration.getCellarFolderFoxml() + File.separator + uncompressedFolderName);
        FileUtils.copyDirectory(
                Paths.get(cellarConfiguration.getCellarFolderTemporaryWork() + File.separator + uncompressedFolderName).toFile(),
                foxmlFolder.toFile());
        String sourceRootRelativePathRepresentation = getRelativeFilesPathAsString(foxmlFolder);
        fileSystemDao.moveFoxmlToBackupFolder(uncompressedFolderName);
        Path destinationFolderInBackup = Paths.get(cellarConfiguration.getCellarFolderBackup() + File.separator + uncompressedFolderName);
        String destinationRootRelativePathRepresentation = getRelativeFilesPathAsString(destinationFolderInBackup);
        assertEquals(sourceRootRelativePathRepresentation, destinationRootRelativePathRepresentation);
        assertFalse(Files.exists(foxmlFolder));
    }

    @Test
    public void movingMetsFolderToBackupFolderMovesHierarchyToDestinationAndDeleteSource() throws Exception {
        String uncompressedFolderName = fileSystemDao.uncompress(sipFileWithRelativeHrefVersionOne);
        Path metsFolder = Paths.get(cellarConfiguration.getCellarFolderTemporaryWork() + File.separator + uncompressedFolderName);
        String sourceRootRelativePathRepresentation = getRelativeFilesPathAsString(metsFolder);
        fileSystemDao.moveMetsToBackupFolder(uncompressedFolderName);
        Path destinationFolderInBackup = Paths.get(cellarConfiguration.getCellarFolderBackup() + File.separator + uncompressedFolderName);
        String destinationRootRelativePathRepresentation = getRelativeFilesPathAsString(destinationFolderInBackup);
        assertEquals(sourceRootRelativePathRepresentation, destinationRootRelativePathRepresentation);
        assertFalse(Files.exists(metsFolder));
    }

    @Test
    public void movingSipFileToBackupFolderMovesFileToDestinationAndDeleteSource() throws Exception {
        Path copyDestination = Paths
                .get(cellarConfiguration.getCellarFolderTemporaryWork() + File.separator + sipFileWithRelativeHrefVersionOne.getName());
        Files.copy(sipFileWithRelativeHrefVersionOne.toPath().toAbsolutePath(), copyDestination);
        assertTrue(Files.exists(copyDestination));

        fileSystemDao.moveSIPToBackupFolder(copyDestination.toFile());
        assertFalse(Files.exists(copyDestination));

        Path sipFileInBackupFolder = Paths
                .get(cellarConfiguration.getCellarFolderBackup() + File.separator + copyDestination.getFileName());
        assertTrue(Files.exists(sipFileInBackupFolder));
    }

    @Test
    public void zipFileCheckOnValidZipFileReturnsTrue() throws Exception {
        assertTrue(FileSystemDao.isZipFileValid(sipFileWithRelativeHrefVersionOne));
    }

    @Test
    public void zipFileCheckOnInvalidZipFileReturnsFalse() throws Exception {
        assertFalse(FileSystemDao.isZipFileValid(textFileWithZipExtension));
        assertFalse(FileSystemDao.isZipFileValid(textFileWithFileExtension));
        assertFalse(FileSystemDao.isZipFileValid(new File("do_not_exist.zip")));
    }

    @Test
    public void copyingSipToErrorFolderDoesNotDeleteSipSource() throws Exception {
        fileSystemDao.copySIPToErrorFolder(sipFileWithRelativeHrefVersionOne);
        String errorFolder = cellarConfiguration.getCellarFolderError();
        String sipName = sipFileWithRelativeHrefVersionOne.getName();
        Path sipInErrorFolderPath = Paths.get(errorFolder + File.separator + sipName);
        assertTrue(Files.exists(sipInErrorFolderPath));
        assertTrue(Files.exists(sipFileWithRelativeHrefVersionOne.toPath()));
    }

    @Test
    public void movingSipToErrorFolderDeletesSipSource() throws Exception {
        Path copyDestination = copySipToWorkingDirectoryAndReturnPath();
        assertTrue(Files.exists(copyDestination));
        fileSystemDao.moveSIPToErrorFolder(copyDestination.toFile());
        assertFalse(Files.exists(copyDestination));

        String errorFolder = cellarConfiguration.getCellarFolderError();
        String sipName = sipFileWithRelativeHrefVersionOne.getName();
        Path sipInErrorFolderPath = Paths.get(errorFolder + File.separator + sipName);
        assertTrue(Files.exists(sipInErrorFolderPath));
    }

    @Test
    public void moveToResponseErrorFolder() throws Exception{
        List<File> fileList = new ArrayList<>(); //initialize list<File>
        SIPWork.TYPE[] receptionFolderTypes = { SIPWork.TYPE.AUTHENTICOJ, SIPWork.TYPE.DAILY, SIPWork.TYPE.BULK,
                SIPWork.TYPE.BULK_LOWPRIORITY }; //get all 4 reception folder types

        //test every reception folder
        for (final SIPWork.TYPE receptionFolder : receptionFolderTypes) {
            //create 5 files: txt, pdf, log, html, ZIP
            File txtFile = new File(cellarConfiguration.getCellarFolderTemporaryWork() + File.separator + "dummy.txt");
            Files.createFile(txtFile.toPath().toAbsolutePath());
            File pdfFile = new File(cellarConfiguration.getCellarFolderTemporaryWork() + File.separator + "dummy.pdf");
            Files.createFile(pdfFile.toPath().toAbsolutePath());
            File logFile = new File(cellarConfiguration.getCellarFolderTemporaryWork() + File.separator + "dummy.log");
            Files.createFile(logFile.toPath().toAbsolutePath());
            File htmlFile = new File(cellarConfiguration.getCellarFolderTemporaryWork() + File.separator + "dummy.html");
            Files.createFile(htmlFile.toPath().toAbsolutePath());
            File ZIPFile = new File(cellarConfiguration.getCellarFolderTemporaryWork() + File.separator + "dummy.ZIP");
            Files.createFile(ZIPFile.toPath().toAbsolutePath());

            //create 1 folder
            File folder = new File(cellarConfiguration.getCellarFolderTemporaryWork() + File.separator + "folder");
            assertTrue(folder.mkdir());

            //add them to the list<File>
            fileList.add(txtFile);
            fileList.add(pdfFile);
            fileList.add(logFile);
            fileList.add(htmlFile);
            fileList.add(ZIPFile);
            fileList.add(folder);

            fileSystemDao.moveToResponseErrorFolder(fileList, receptionFolder); //call the testing method

            fileList.clear();
        }
        //Expect 5+1 items in each error response folder
        assertEquals(6, new File(cellarConfiguration.getCellarFolderAuthenticOJResponse() + File.separator + "error").list().length);
        assertEquals(6, new File(cellarConfiguration.getCellarFolderDailyResponse() + File.separator + "error").list().length);
        assertEquals(6, new File(cellarConfiguration.getCellarFolderBulkResponse() + File.separator + "error").list().length);
        assertEquals(6, new File(cellarConfiguration.getCellarFolderBulkLowPriorityResponse() + File.separator + "error").list().length);
    }

    @Test
    public void copyingSameFileMultipleTimeToSameFolderGetsSuffixAppended() throws Exception {
        Path errorFolder = Paths.get(cellarConfiguration.getCellarFolderError());
        fileSystemDao.copySIPToErrorFolder(sipFileWithRelativeHrefVersionOne);
        Path firstCopy = Paths.get(errorFolder.toString() + File.separator + sipFileWithRelativeHrefVersionOne.getName());
        String fileNameWithoutExtension = sipFileWithRelativeHrefVersionOne.getName().replace(".zip", "");
        Path secondCopy = Paths.get(errorFolder.toString() + File.separator + fileNameWithoutExtension + "_1.zip");
        fileSystemDao.copySIPToErrorFolder(sipFileWithRelativeHrefVersionOne);
        assertTrue(Files.exists(firstCopy));
        assertTrue(Files.exists(secondCopy));
    }

    @Test(expected = FileSystemException.class)
    public void createInvalidFolderThrowsFileSystemException() throws Exception {
        fileSystemDao.createFolder("abc:*?\0/invalid_path");
    }

    @Test
    public void testSuccessfulFileDeletetion() throws Exception {
        String sipName = sipFileWithRelativeHrefVersionOne.getName();

        Path folder = createDestinationFolder();
        Path destination = copyFile(sipName, folder);
        assertTrue(Files.exists(destination));
        fileSystemDao.deleteFromBulkReceptionFolder(sipName);
        assertFalse(Files.exists(destination));
    }

    private Path copyFile(String sipName, Path folder) throws IOException {
        Path source = sipFileWithRelativeHrefVersionOne.toPath().toAbsolutePath();
        Path destination = Paths.get(folder.toString() + File.separator + sipName);
        Files.copy(source, destination);
        return destination;
    }

    private Path createDestinationFolder() throws Exception {
        Path folder = Paths.get(cellarConfiguration.getCellarFolderBulkReception()).toAbsolutePath();
        createDirectoryIfNotExists(folder);
        return folder;
    }

    private void createDirectoryIfNotExists(Path folder) throws Exception {
        if (!Files.exists(folder))
            Files.createDirectories(folder);
    }

    @Test
    public void testSuccessfulDirectoryDeletion() throws Exception {
        String uncompressedFolderName = fileSystemDao.uncompress(sipFileWithRelativeHrefVersionOne);
        Path folder = Paths.get(cellarConfiguration.getCellarFolderTemporaryWork(), uncompressedFolderName);
        assertTrue(Files.exists(folder));
        fileSystemDao.deleteFromTemporaryWorkFolder(uncompressedFolderName);
        assertFalse(Files.exists(folder));
    }

    @Test
    public void ifNoFilesInFolderThenEmptyFileListIsReturned() throws Exception {
        int limit = 2;
        Path destinationFolder = Paths.get(cellarConfiguration.getCellarFolderBulkReception());
        createDirectoryIfNotExists(destinationFolder);
        File[] filesInFolder = fileSystemDao.getAllSipArchivesFromBulkReceptionFolder(limit);
        assertEquals(0, filesInFolder.length);
    }

    @Test
    public void ifTotalFilesInNonAuthenticFoldersIsLessThanLimitThenReturnAll() throws Exception {
        int limit = 3;
        Path destinationFolder = Paths.get(cellarConfiguration.getCellarFolderBulkReception());
        File[] filesInFolder = getLimitedNumberOfFilesFromFolder(limit, destinationFolder);
        assertEquals(2, filesInFolder.length);
    }

    private File[] getLimitedNumberOfFilesFromFolder(int limit, Path folder) throws Exception {
        createDirectoryIfNotExists(folder);
        Files.copy(sipFileWithRelativeHrefVersionOne.toPath().toAbsolutePath(),
                folder.resolve(sipFileWithRelativeHrefVersionOne.getName()));
        Files.copy(sipFileWithoutRelativeHrefVersionOne.toPath().toAbsolutePath(),
                folder.resolve(sipFileWithoutRelativeHrefVersionOne.getName()));
        return fileSystemDao.getAllSipArchivesFromBulkReceptionFolder(limit);
    }

    @Test
    public void ifTotalFilesInNonAuthenticFoldersIsMoreThanLimitThenReturnLimit() throws Exception {
        int limit = 1;
        Path destinationFolder = Paths.get(cellarConfiguration.getCellarFolderBulkReception());
        File[] filesInFolder = getLimitedNumberOfFilesFromFolder(limit, destinationFolder);
        assertEquals(1, filesInFolder.length);
    }

    @Test
    public void ifTotalFilesInNonAuthenticFoldersIsEqualToLimitThenReturnLimit() throws Exception {
        int limit = 2;
        Path destinationFolder = Paths.get(cellarConfiguration.getCellarFolderBulkReception());
        File[] filesInFolder = getLimitedNumberOfFilesFromFolder(limit, destinationFolder);
        assertEquals(2, filesInFolder.length);
    }

    @Test
    public void allFilesFromAuthenticFolderReceptionAreReturned() throws Exception {
        Path destinationFolder = Paths.get(cellarConfiguration.getCellarFolderAuthenticOJReception());
        createDirectoryIfNotExists(destinationFolder);
        Files.copy(sipFileWithRelativeHrefVersionOne.toPath().toAbsolutePath(),
                destinationFolder.resolve(sipFileWithRelativeHrefVersionOne.getName()));
        Files.copy(sipFileWithoutRelativeHrefVersionOne.toPath().toAbsolutePath(),
                destinationFolder.resolve(sipFileWithoutRelativeHrefVersionOne.getName()));
        File[] filesInFolder = fileSystemDao.getAllSipArchivesFromAuthenticOJReceptionFolder();
        assertEquals(2, filesInFolder.length);
    }

    @Test
    public void allFilesFromDailyReceptionFolderAreReturned() throws Exception {
        Path destinationFolder = Paths.get(cellarConfiguration.getCellarFolderDailyReception());
        createDirectoryIfNotExists(destinationFolder);
        Files.copy(sipFileWithRelativeHrefVersionOne.toPath().toAbsolutePath(),
                destinationFolder.resolve(sipFileWithRelativeHrefVersionOne.getName()));
        Files.copy(sipFileWithoutRelativeHrefVersionOne.toPath().toAbsolutePath(),
                destinationFolder.resolve(sipFileWithoutRelativeHrefVersionOne.getName()));
        File[] filesInFolder = fileSystemDao.getAllSipArchivesFromDailyReceptionFolder();
        assertEquals(2, filesInFolder.length);
    }
    
    @Test
    public void allFilesFromBulkReceptionFolderAreReturned() throws Exception {
        Path destinationFolder = Paths.get(cellarConfiguration.getCellarFolderBulkReception());
        createDirectoryIfNotExists(destinationFolder);
        Files.copy(sipFileWithRelativeHrefVersionOne.toPath().toAbsolutePath(),
                destinationFolder.resolve(sipFileWithRelativeHrefVersionOne.getName()));
        Files.copy(sipFileWithoutRelativeHrefVersionOne.toPath().toAbsolutePath(),
                destinationFolder.resolve(sipFileWithoutRelativeHrefVersionOne.getName()));
        File[] filesInFolder = fileSystemDao.getAllSipArchivesFromBulkReceptionFolder(Integer.MAX_VALUE);
        assertEquals(2, filesInFolder.length);
    }

    @Test
    public void batchFolderShouldBeReturnedForBatchExport() throws Exception {
        Path expected = Paths.get(cellarConfiguration.getCellarFolderExportBatchJob());
        Path actual = fileSystemDao.getArchiveFolderBatchJobExport().toPath();
        assertEquals(expected, actual);
    }

    @Test
    public void batchFolderShouldBeCreatedIfNotExistWhenBatchExport() throws Exception {
        Path expected = Paths.get(cellarConfiguration.getCellarFolderExportBatchJob());
        Files.deleteIfExists(expected);
        assertFalse(Files.exists(expected));
        Path actual = fileSystemDao.getArchiveFolderBatchJobExport().toPath();
        assertTrue(Files.exists(expected));
        assertTrue(Files.exists(actual));
        assertEquals(expected, actual);
    }

    @Test
    public void temporaryBatchFolderShouldBeReturnedForBatchExport() throws Exception {
        Path expected = Paths.get(cellarConfiguration.getCellarFolderExportBatchJobTemporary());
        Path actual = fileSystemDao.getArchiveTemporaryFolderBatchJobExport().toPath();
        assertEquals(expected, actual);
    }

    @Test
    public void archiveFolderExportRestShouldBeReturned() throws Exception {
        Path expected = Paths.get(cellarConfiguration.getCellarFolderExportRest());
        Path actual = fileSystemDao.getArchiveFolderExportRest().toPath();
        assertEquals(expected, actual);
    }

    @Test
    public void archiveFolderExportUpdateResponseShouldBeReturned() throws Exception {
        Path expected = Paths.get(cellarConfiguration.getCellarFolderExportUpdateResponse());
        Path actual = fileSystemDao.getArchiveFolderExportUpdateResponse().toPath();
        assertEquals(expected, actual);
    }

    @Test
    public void whenOneMetsFileIsInFolderThenMetsFileIsReturned() throws Exception {
        String uncompressedFolderName = fileSystemDao.uncompress(sipFileWithRelativeHrefVersionOne);
        File metsFile = fileSystemDao.getMetsFileFromTemporaryWorkFolder(uncompressedFolderName);
        assertTrue(Files.exists(metsFile.toPath()));
    }

    @Test(expected = MetsException.class)
    public void whenNoMetsFileIsInFOlderThenMetsExceptionIsReturned() throws Exception {
        Path noMetsFilePath = Paths.get("src/test/resources/eu/europa/ec/opoce/cellar/cl/dao/impl/no_mets_file.zip");
        String uncompressedFolderName = fileSystemDao.uncompress(noMetsFilePath.toFile());
        File metsFile = fileSystemDao.getMetsFileFromTemporaryWorkFolder(uncompressedFolderName);
        assertFalse(Files.exists(metsFile.toPath()));
    }

    @Test(expected = MetsException.class)
    public void whenMultipleMetsFileInFolderThenMetsExceptionIsReturned() throws Exception {
        Path multipleMetsFile = Paths.get("src/test/resources/eu/europa/ec/opoce/cellar/cl/dao/impl/more_than_one_mets_file.zip");
        String uncompressedFolderName = fileSystemDao.uncompress(multipleMetsFile.toFile());
        File metsFile = fileSystemDao.getMetsFileFromTemporaryWorkFolder(uncompressedFolderName);
        assertFalse(Files.exists(metsFile.toPath()));
    }

    @Test
    public void responseBulkFolderShouldBeReturned() throws Exception {
        Path expected = Paths.get(cellarConfiguration.getCellarFolderBulkResponse());
        Path actual = fileSystemDao.getResponseBulkFolder().toPath();
        assertEquals(expected, actual);
    }
    
    @Test
    public void responseBulkLowPriorityFolderShouldBeReturned() throws Exception {
        Path expected = Paths.get(cellarConfiguration.getCellarFolderBulkLowPriorityResponse());
        Path actual = fileSystemDao.getResponseBulkLowPriorityFolder().toPath();
        assertEquals(expected, actual);
    }

    @Test
    public void responseDailyFolderShouldBeReturned() throws Exception {
        Path expected = Paths.get(cellarConfiguration.getCellarFolderDailyResponse());
        Path actual = fileSystemDao.getResponseDailyFolder().toPath();
        assertEquals(expected, actual);
    }

    @Test
    public void responseAuthenticOJShouldBeReturned() throws Exception {
        Path expected = Paths.get(cellarConfiguration.getCellarFolderAuthenticOJResponse());
        Path actual = fileSystemDao.getResponseAuthenticOJFolder().toPath();
        assertEquals(expected, actual);
    }

    @Test
    public void movingSipToFolderMovesItThenReturnsName() throws Exception {
        Path copyPath = copySipToWorkingDirectoryAndReturnPath();

        String metsName = copyPath.getFileName().toString().replace(".zip", "");
        String name = fileSystemDao.moveSIPToDailyReceptionFolder(copyPath.toFile(), metsName);
        assertEquals(metsName + ".zip", name);

        copyPath = copySipToWorkingDirectoryAndReturnPath();
        name = fileSystemDao.moveSIPToAuthenticOJReceptionFolder(copyPath.toFile(), metsName);
        assertEquals(metsName + ".zip", name);
    }

    private Path copySipToWorkingDirectoryAndReturnPath() throws IOException {
        Path copyDestination = Paths
                .get(cellarConfiguration.getCellarFolderTemporaryWork() + File.separator + sipFileWithRelativeHrefVersionOne.getName());
        Files.copy(sipFileWithRelativeHrefVersionOne.toPath().toAbsolutePath(), copyDestination);
        assertTrue(Files.exists(copyDestination));
        return copyDestination;
    }

    @Test(expected = UncompressException.class)
    public void emptyFileNameReturnsUncompressException() throws Exception {
        File fileWithoutFileName = new File("");
        String uncompressedFolderName = fileSystemDao.uncompress(fileWithoutFileName);
    }

    @Test
    public void folderMustNotBeCreatedIfTheIngestionIsDisabled() {

        deleteFolder(cellarConfiguration.getCellarFolderDailyReception());
        deleteFolder(cellarConfiguration.getCellarFolderBulkReception());
        deleteFolder(cellarConfiguration.getCellarFolderBulkLowPriorityReception());
        deleteFolder(cellarConfiguration.getCellarFolderTemporaryWork());
        deleteFolder(cellarConfiguration.getCellarFolderFoxml());
        deleteFolder(cellarConfiguration.getCellarFolderDailyResponse());
        deleteFolder(cellarConfiguration.getCellarFolderBulkResponse());
        deleteFolder(cellarConfiguration.getCellarFolderBulkLowPriorityResponse());
        deleteFolder(cellarConfiguration.getCellarFolderError());
        deleteFolder(cellarConfiguration.getCellarFolderBackup());

        when(cellarConfiguration.isCellarServiceIngestionEnabled()).thenReturn(false);
        fileSystemDao.initialize();

        Assert.assertFalse(Files.exists(Paths.get(cellarConfiguration.getCellarFolderDailyReception())));

        when(cellarConfiguration.isCellarServiceIngestionEnabled()).thenReturn(true);
        fileSystemDao.initialize();

        Assert.assertTrue(Files.exists(Paths.get(cellarConfiguration.getCellarFolderDailyReception())));
    }

    private static void deleteFolder(String path) {
        Path p = Paths.get(path);
        if(p != null && Files.exists(p)) {
            try {
                Files.delete(Paths.get(path));
            } catch (IOException e) {
                throw new UncheckedIOException(e);
            }
        }
    }
}
