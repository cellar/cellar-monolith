/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *             FILE : ConcurrencyControllerTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : May 20, 2013
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2013 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLock.MODE;
import eu.europa.ec.opoce.cellar.cl.concurrency.ICellarLockSession;
import eu.europa.ec.opoce.cellar.cl.concurrency.ICellarThreadsManager;
import eu.europa.ec.opoce.cellar.cl.concurrency.impl.*;
import eu.europa.ec.opoce.cellar.cl.domain.content.Identifier;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

/**
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : May 20, 2013
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class ConcurrencyControllerTest {

    /**
     * Retrieve an incremented lock for a cellar id.
     */
    @Test
    public void getIncrementedCellarIdLockTest() {
        final String cellarId = "cellar:1";
        final Identifier identifier = new Identifier(cellarId);

        final CellarLocksManager cellarLocksManager = new CellarLocksManager();

        final CellarLock cellarLock = cellarLocksManager.getIncrementedCellarIdLock(identifier);
        cellarLock.addLockSession(CellarLockSession.create(null));
        
        Assert.assertTrue(cellarLock instanceof CellarIdLock);

        final CellarIdLock cellarIdLock = (CellarIdLock) cellarLock;

        Assert.assertEquals(cellarId, cellarIdLock.getCellarId());
        Assert.assertEquals(1, cellarIdLock.getLockCount());
    }

    /**
     * Retrieve an incremented lock for a production id.
     */
    @Test
    public void getIncrementedProductionIdsLockTest() {
        final String productionId = "oj:1";
        final Identifier identifier = new Identifier(Arrays.asList(productionId));

        final CellarLocksManager cellarLocksManager = new CellarLocksManager();

        final CellarLock cellarLock = cellarLocksManager.getIncrementedProductionIdsLock(identifier);
        cellarLock.addLockSession(CellarLockSession.create(null));

        Assert.assertTrue(cellarLock instanceof ProductionIdLock);

        final ProductionIdLock productionIdLock = (ProductionIdLock) cellarLock;

        Assert.assertEquals(productionId, productionIdLock.getPids().get(0));
        Assert.assertEquals(1, productionIdLock.getLockCount());
    }

    /**
     * Check the isWorking status of the cellar lock session.
     */
    @Test
    public void isWorkingTest() {
        final ICellarThreadsManager cellarThreadsManager = new CellarThreadsManager();

        final ICellarLockSession cellarLockSession = CellarLockSession.create(null);
        Assert.assertFalse(cellarLockSession.isWorking());

        cellarThreadsManager.incWorkingThreadsCount(cellarLockSession);
        Assert.assertTrue(cellarLockSession.isWorking());

        cellarThreadsManager.decWorkingThreadsCount(cellarLockSession);
        Assert.assertFalse(cellarLockSession.isWorking());
    }

    /**
     * Check the locking mode computation.
     */
    @Test
    public void getLockingMode() {
        final CellarIdLock cellarIdLock = new CellarIdLock("");

        Assert.assertEquals(MODE.WRITE, cellarIdLock.getDefaultMode());
        Assert.assertEquals(MODE.WRITE, cellarIdLock.getLockingMode(MODE.DEFAULT));
        Assert.assertEquals(MODE.WRITE, cellarIdLock.getLockingMode(MODE.WRITE));
        Assert.assertEquals(MODE.READ, cellarIdLock.getLockingMode(MODE.READ));

        final ProductionIdLock productionIdLock = new ProductionIdLock(null);

        Assert.assertEquals(MODE.READ, productionIdLock.getDefaultMode());
        Assert.assertEquals(MODE.READ, productionIdLock.getLockingMode(MODE.DEFAULT));
        Assert.assertEquals(MODE.WRITE, productionIdLock.getLockingMode(MODE.WRITE));
        Assert.assertEquals(MODE.READ, productionIdLock.getLockingMode(MODE.READ));
    }
}
