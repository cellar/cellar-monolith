/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *             FILE : LicenseHolderServiceTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 7, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionConfiguration.CONFIGURATION_TYPE;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution.EXECUTION_STATUS;
import eu.europa.ec.opoce.cellar.server.admin.resource.ResourceConfigInvoker;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * <class_description> Test cases for the service {@link LicenseHolderServiceImpl}.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 7, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class LicenseHolderServiceTest {

    private final static Calendar CALENDAR;
    private final static Date CONFIGURATION_START_DATE;

    private final static String ARCHIVE_NAME_OVERVIEW_STD_ENG = "/${extraction_configuration_id}-STD-ENG.tar.gz";
    private final static String ARCHIVE_NAME_OVERVIEW_STDD_ENG = "/${extraction_configuration_id}-STDD-ENG-${start_date}-${end_date}.tar.gz";
    private final static String ARCHIVE_NAME_OVERVIEW_STDW_ENG = "/${extraction_configuration_id}-STDW-ENG-${start_date}-${end_date}.tar.gz";
    private final static String ARCHIVE_NAME_OVERVIEW_STDM_ENG = "/${extraction_configuration_id}-STDM-ENG-${start_date}-${end_date}.tar.gz";

    private final static String ARCHIVE_PATH_OVERVIEW_STD_ENG = "/";
    private final static String ARCHIVE_PATH_OVERVIEW_STDD_ENG = "/ENG/DAILY/${update_date}/";
    private final static String ARCHIVE_PATH_OVERVIEW_STDW_ENG = "/ENG/WEEKLY/${update_date}/";
    private final static String ARCHIVE_PATH_OVERVIEW_STDM_ENG = "/ENG/MONTHLY/${update_date}/";

    /**
     * The license holder service to test.
     */
    private LicenseHolderServiceImpl licenseHolderService;

    /**
     * The language service mock.
     */
    private ResourceConfigInvoker languageServiceMock;

    /**
     * The Cellar configuration mock.
     */
    private ICellarConfiguration cellarConfigurationMock;

    /**
     * Static constructor.
     */
    static {
        CALENDAR = Calendar.getInstance();
        CALENDAR.set(2012, 11, 20);
        CONFIGURATION_START_DATE = CALENDAR.getTime();
    }

    /**
     * Set up the test.
     */
    @Before
    public void setUp() {
        this.licenseHolderService = new LicenseHolderServiceImpl();
        this.languageServiceMock = EasyMock.createMock(ResourceConfigInvoker.class);
        this.cellarConfigurationMock = EasyMock.createMock(ICellarConfiguration.class);
        this.licenseHolderService.setLanguageService(languageServiceMock);
        this.licenseHolderService.setCellarConfiguration(cellarConfigurationMock);
    }

    /**
     * Test the archive name overview generation.
     */
    @Test
    public void getArchiveNameOverviewTest() {
        // init mock
        EasyMock.expect(this.languageServiceMock.getLanguage(LicenseHolderTestUtils.getLanguageBeanEng().getIsoCodeThreeChar()))
                .andReturn(LicenseHolderTestUtils.getLanguageBeanEng()).times(4);
        EasyMock.replay(this.languageServiceMock);

        // generate and check the STD generation
        final String archiveNameOverviewSTD = this.licenseHolderService.getArchiveNameOverview(CONFIGURATION_TYPE.STD,
                LicenseHolderTestUtils.getLanguageBeanEng().getIsoCodeThreeChar());
        Assert.assertEquals(archiveNameOverviewSTD, ARCHIVE_NAME_OVERVIEW_STD_ENG);

        // generate and check the STDD generation
        final String archiveNameOverviewSTDD = this.licenseHolderService.getArchiveNameOverview(CONFIGURATION_TYPE.STDD,
                LicenseHolderTestUtils.getLanguageBeanEng().getIsoCodeThreeChar());
        Assert.assertEquals(archiveNameOverviewSTDD, ARCHIVE_NAME_OVERVIEW_STDD_ENG);

        // generate and check the STDW generation
        final String archiveNameOverviewSTDW = this.licenseHolderService.getArchiveNameOverview(CONFIGURATION_TYPE.STDW,
                LicenseHolderTestUtils.getLanguageBeanEng().getIsoCodeThreeChar());
        Assert.assertEquals(archiveNameOverviewSTDW, ARCHIVE_NAME_OVERVIEW_STDW_ENG);

        // generate and check the STDM generation
        final String archiveNameOverviewSTDM = this.licenseHolderService.getArchiveNameOverview(CONFIGURATION_TYPE.STDM,
                LicenseHolderTestUtils.getLanguageBeanEng().getIsoCodeThreeChar());
        Assert.assertEquals(archiveNameOverviewSTDM, ARCHIVE_NAME_OVERVIEW_STDM_ENG);

        EasyMock.verify(this.languageServiceMock);
    }

    /**
     * Test the archive path overview generation.
     */
    @Test
    public void getArchivePathOverviewTest() {
        // init mock
        EasyMock.expect(this.languageServiceMock.getLanguage(LicenseHolderTestUtils.getLanguageBeanEng().getIsoCodeThreeChar()))
                .andReturn(LicenseHolderTestUtils.getLanguageBeanEng()).times(4);
        EasyMock.replay(this.languageServiceMock);

        // generate and check the STD generation
        final String archivePathOverviewSTD = this.licenseHolderService.getArchivePathOverview(CONFIGURATION_TYPE.STD,
                LicenseHolderTestUtils.getLanguageBeanEng().getIsoCodeThreeChar());
        Assert.assertEquals(archivePathOverviewSTD, ARCHIVE_PATH_OVERVIEW_STD_ENG);

        // generate and check the STDD generation
        final String archivePathOverviewSTDD = this.licenseHolderService.getArchivePathOverview(CONFIGURATION_TYPE.STDD,
                LicenseHolderTestUtils.getLanguageBeanEng().getIsoCodeThreeChar());
        Assert.assertEquals(archivePathOverviewSTDD, ARCHIVE_PATH_OVERVIEW_STDD_ENG);

        // generate and check the STDW generation
        final String archivePathOverviewSTDW = this.licenseHolderService.getArchivePathOverview(CONFIGURATION_TYPE.STDW,
                LicenseHolderTestUtils.getLanguageBeanEng().getIsoCodeThreeChar());
        Assert.assertEquals(archivePathOverviewSTDW, ARCHIVE_PATH_OVERVIEW_STDW_ENG);

        // generate and check the STDM generation
        final String archivePathOverviewSTDM = this.licenseHolderService.getArchivePathOverview(CONFIGURATION_TYPE.STDM,
                LicenseHolderTestUtils.getLanguageBeanEng().getIsoCodeThreeChar());
        Assert.assertEquals(archivePathOverviewSTDM, ARCHIVE_PATH_OVERVIEW_STDM_ENG);

        EasyMock.verify(this.languageServiceMock);
    }

    /**
     * Test the construction of the absolute archive file path.
     */
    @Test
    public void getAbsoluteArchiveFilePathTest() {
        // init mock
        LicenseHolderTestUtils.expectFolderLicenseHolderArchiveAdhocExtraction(this.cellarConfigurationMock, 1);
        LicenseHolderTestUtils.expectFolderLicenseHolderArchiveExtraction(this.cellarConfigurationMock, 3);
        EasyMock.replay(this.cellarConfigurationMock);

        ExtractionConfiguration extractionConfiguration = null;
        ExtractionExecution extractionExecution = null;
        String absoluteArchiveFilePath = null;

        extractionConfiguration = new ExtractionConfiguration(LicenseHolderTestUtils.getConfigurationName(), CONFIGURATION_TYPE.STD,
                LicenseHolderTestUtils.getLanguageBeanEng().getIsoCodeThreeChar(), LicenseHolderTestUtils.getPath(),
                CONFIGURATION_START_DATE, LicenseHolderTestUtils.getSparqlQuery());
        extractionExecution = extractionConfiguration.getExtractionExecution(); // generate the first execution

        // generate and check the STD generation
        extractionExecution
                .setArchiveFilePath(ARCHIVE_PATH_OVERVIEW_STD_ENG + LicenseHolderTestUtils.getPath() + ARCHIVE_NAME_OVERVIEW_STD_ENG);
        absoluteArchiveFilePath = this.licenseHolderService.getAbsoluteArchiveFilePath(extractionExecution);
        Assert.assertEquals(absoluteArchiveFilePath, LicenseHolderTestUtils.getFolderLicenseHolderArchiveAdhocExtraction()
                + ARCHIVE_PATH_OVERVIEW_STD_ENG + LicenseHolderTestUtils.getPath() + ARCHIVE_NAME_OVERVIEW_STD_ENG);

        // generate and check the STDD generation
        extractionConfiguration.setConfigurationType(CONFIGURATION_TYPE.STDD);
        extractionExecution
                .setArchiveFilePath(ARCHIVE_PATH_OVERVIEW_STDD_ENG + LicenseHolderTestUtils.getPath() + ARCHIVE_NAME_OVERVIEW_STDD_ENG);
        absoluteArchiveFilePath = this.licenseHolderService.getAbsoluteArchiveFilePath(extractionExecution);
        Assert.assertEquals(absoluteArchiveFilePath, LicenseHolderTestUtils.getFolderLicenseHolderArchiveExtraction()
                + ARCHIVE_PATH_OVERVIEW_STDD_ENG + LicenseHolderTestUtils.getPath() + ARCHIVE_NAME_OVERVIEW_STDD_ENG);

        // generate and check the STDW generation
        extractionConfiguration.setConfigurationType(CONFIGURATION_TYPE.STDW);
        extractionExecution
                .setArchiveFilePath(ARCHIVE_PATH_OVERVIEW_STDW_ENG + LicenseHolderTestUtils.getPath() + ARCHIVE_NAME_OVERVIEW_STDW_ENG);
        absoluteArchiveFilePath = this.licenseHolderService.getAbsoluteArchiveFilePath(extractionExecution);
        Assert.assertEquals(absoluteArchiveFilePath, LicenseHolderTestUtils.getFolderLicenseHolderArchiveExtraction()
                + ARCHIVE_PATH_OVERVIEW_STDW_ENG + LicenseHolderTestUtils.getPath() + ARCHIVE_NAME_OVERVIEW_STDW_ENG);

        // generate and check the STDM generation
        extractionConfiguration.setConfigurationType(CONFIGURATION_TYPE.STDM);
        extractionExecution
                .setArchiveFilePath(ARCHIVE_PATH_OVERVIEW_STDM_ENG + LicenseHolderTestUtils.getPath() + ARCHIVE_NAME_OVERVIEW_STDM_ENG);
        absoluteArchiveFilePath = this.licenseHolderService.getAbsoluteArchiveFilePath(extractionExecution);
        Assert.assertEquals(absoluteArchiveFilePath, LicenseHolderTestUtils.getFolderLicenseHolderArchiveExtraction()
                + ARCHIVE_PATH_OVERVIEW_STDM_ENG + LicenseHolderTestUtils.getPath() + ARCHIVE_NAME_OVERVIEW_STDM_ENG);

        EasyMock.verify(this.cellarConfigurationMock);
    }

    /**
     * Test the start date validation.
     */
    @Test
    public void isStartDateValidTest() {
        // init mock
        LicenseHolderTestUtils.expectServiceLicenseHolderKeptDaysDaily(this.cellarConfigurationMock, 2);
        LicenseHolderTestUtils.expectServiceLicenseHolderKeptDaysWeekly(this.cellarConfigurationMock, 2);
        LicenseHolderTestUtils.expectServiceLicenseHolderKeptDaysMonthly(this.cellarConfigurationMock, 2);
        EasyMock.replay(this.cellarConfigurationMock);

        // valid dates
        CALENDAR.setTime(new Date());
        CALENDAR.add(Calendar.DATE, -2);

        Assert.assertTrue(this.licenseHolderService.isStartDateValid(CONFIGURATION_TYPE.STDD, CALENDAR.getTime()));
        Assert.assertTrue(this.licenseHolderService.isStartDateValid(CONFIGURATION_TYPE.STDW, CALENDAR.getTime()));
        Assert.assertTrue(this.licenseHolderService.isStartDateValid(CONFIGURATION_TYPE.STDM, CALENDAR.getTime()));

        // invalid dates
        CALENDAR.setTime(new Date());
        CALENDAR.add(Calendar.DATE, -7);

        Assert.assertFalse(this.licenseHolderService.isStartDateValid(CONFIGURATION_TYPE.STDD, CALENDAR.getTime()));
        Assert.assertFalse(this.licenseHolderService.isStartDateValid(CONFIGURATION_TYPE.STDW, CALENDAR.getTime()));
        Assert.assertFalse(this.licenseHolderService.isStartDateValid(CONFIGURATION_TYPE.STDM, CALENDAR.getTime()));

        EasyMock.verify(this.cellarConfigurationMock);
    }

    /**
     * Test the generation of the deadlines.
     */
    @Test
    public void getDeadlinesTest() {
        // init mock
        LicenseHolderTestUtils.expectServiceLicenseHolderKeptDaysDaily(this.cellarConfigurationMock, 1);
        LicenseHolderTestUtils.expectServiceLicenseHolderKeptDaysWeekly(this.cellarConfigurationMock, 1);
        LicenseHolderTestUtils.expectServiceLicenseHolderKeptDaysMonthly(this.cellarConfigurationMock, 1);
        EasyMock.replay(this.cellarConfigurationMock);

        final List<Date> deadlines = this.licenseHolderService.getDeadlines();
        final Date now = new Date();
        Date tmp = null;

        CALENDAR.setTime(now);
        CALENDAR.add(Calendar.DATE, -LicenseHolderTestUtils.getServiceLicenseHolderKeptDaysDaily());
        CALENDAR.add(Calendar.MINUTE, -5);
        tmp = CALENDAR.getTime();
        Assert.assertTrue(tmp.compareTo(deadlines.get(0)) < 0);

        CALENDAR.setTime(now);
        CALENDAR.add(Calendar.DATE, -LicenseHolderTestUtils.getServiceLicenseHolderKeptDaysWeekly());
        CALENDAR.add(Calendar.MINUTE, -5);
        tmp = CALENDAR.getTime();
        Assert.assertTrue(tmp.compareTo(deadlines.get(1)) < 0);

        CALENDAR.setTime(now);
        CALENDAR.add(Calendar.DATE, -LicenseHolderTestUtils.getServiceLicenseHolderKeptDaysMonthly());
        CALENDAR.add(Calendar.MINUTE, -5);
        tmp = CALENDAR.getTime();
        Assert.assertTrue(tmp.compareTo(deadlines.get(2)) < 0);

        EasyMock.verify(this.cellarConfigurationMock);
    }

    /**
     * Test the generation of the available options.
     */
    @Test
    public void getExtractionExecutionsOptionsTest() {
        final List<ExtractionExecution> extractionExecutions = new ArrayList<ExtractionExecution>();

        // create a non-restartable execution
        ExtractionExecution extractionExecution = new ExtractionExecution();
        extractionExecutions.add(extractionExecution);

        // create a restartable execution
        extractionExecution = new ExtractionExecution();
        extractionExecution.setExecutionStatus(EXECUTION_STATUS.PENDING);
        extractionExecution.setExecutionStatus(EXECUTION_STATUS.SPARQL_EXECUTING);
        extractionExecution.setExecutionStatus(EXECUTION_STATUS.SPARQL_ERROR);
        extractionExecutions.add(extractionExecution);

        // create a non-restartable execution
        extractionExecution = new ExtractionExecution();
        extractionExecution.setExecutionStatus(EXECUTION_STATUS.PENDING);
        extractionExecution.setExecutionStatus(EXECUTION_STATUS.SPARQL_EXECUTING);
        extractionExecutions.add(extractionExecution);

        final boolean[][] extractionExecutionsOptions = this.licenseHolderService.getExtractionExecutionsOptions(extractionExecutions);
        // check the result
        Assert.assertFalse(extractionExecutionsOptions[0][0]);
        Assert.assertFalse(extractionExecutionsOptions[0][1]);

        Assert.assertTrue(extractionExecutionsOptions[1][0]);
        Assert.assertTrue(extractionExecutionsOptions[1][1]);

        Assert.assertFalse(extractionExecutionsOptions[2][0]);
        Assert.assertFalse(extractionExecutionsOptions[2][1]);
    }
}
