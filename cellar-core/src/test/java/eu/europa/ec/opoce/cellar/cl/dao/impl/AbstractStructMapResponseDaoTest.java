/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.dao.impl
 *             FILE : AbstractStructMapResponseDaoTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 12 11, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-12-11 12:42:56 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.dao.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.response.StructMapResponse;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;

import static eu.europa.ec.opoce.cellar.cl.dao.impl.AbstractStructMapResponseDao.INGEST_RESPONSE_EXTENSION;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author ARHS Developments
 */
public class AbstractStructMapResponseDaoTest {
    private AbstractStructMapResponseDao structMapResponseDao;
    private Path rootPathDirectory;

    @Before
    public void setUp() throws Exception {
        rootPathDirectory = Files.createTempDirectory("structMapResponseDaoTest");
        if (Files.exists(rootPathDirectory)) {
            Files.walk(rootPathDirectory)
                    .filter(path -> !path.equals(rootPathDirectory))
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        }
        Files.deleteIfExists(rootPathDirectory);
        Files.createDirectories(rootPathDirectory);

        ICellarConfiguration cellarConfiguration = mock(ICellarConfiguration.class);
        when(cellarConfiguration.getCellarFolderAuthenticOJResponse()).thenReturn(rootPathDirectory.toString());
        structMapResponseDao = new StructMapAuthenticOJResponseDao(cellarConfiguration);
    }

    @Test
    public void structMapResponseIsSavedInAFile() throws Exception {
        String documentId = "documentId";
        StructMapResponse structMapResponse = new StructMapResponse();
        structMapResponse.setMetsDocumentId(documentId);
        File savedFile = this.structMapResponseDao.save(structMapResponse);
        assertTrue(savedFile.exists());
        assertEquals(documentId + INGEST_RESPONSE_EXTENSION, savedFile.getName());
        File responseFile = this.structMapResponseDao.getResponseFile(documentId);
        assertEquals(savedFile, responseFile);
    }

    @Test
    public void getResponseFileTest() {
        final AbstractStructMapResponseDao dao = new TestStructMapResponseDao();
        final String metsDocumentId = "metsDocumentId";
        final File responseFile = dao.getResponseFile(metsDocumentId);
        assertEquals("/base/folder/metsDocumentId.response.xml", responseFile.getAbsolutePath());
    }

    private static class TestStructMapResponseDao extends AbstractStructMapResponseDao {
        @Override
        protected String resolveResponseFolderPath() {
            return "/base/folder";
        }
    }
}