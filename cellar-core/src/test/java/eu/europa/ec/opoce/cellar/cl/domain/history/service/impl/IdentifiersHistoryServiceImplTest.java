/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.history.service.impl
 *             FILE : IdentifiersHistoryServiceImplTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 09 29, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-09-29 09:20:57 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.history.service.impl;

import eu.europa.ec.opoce.cellar.cl.domain.history.IdentifiersHistory;
import eu.europa.ec.opoce.cellar.cl.domain.history.dao.IdentifiersHistoryDao;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObject;
import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;
import eu.europa.ec.opoce.cellar.exception.CellarValidationException;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * @author ARHS Developments
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class IdentifiersHistoryServiceImplTest {
    private static final String WORK_CELLAR_ID = "cellar:e0dc4243-6d28-11e7-b2f2-01aa75ed71a1";
    private static final String EXPR_CELLAR_ID_0001 = "cellar:e0dc4243-6d28-11e7-b2f2-01aa75ed71a1.0001";
    private static final String EXPR_CELLAR_ID_0003 = "cellar:e0dc4243-6d28-11e7-b2f2-01aa75ed71a1.0003";
    private static final String MANIF_CELLAR_ID_01_OF_EXPR_0001 = EXPR_CELLAR_ID_0001 + ".01";
    private static final String MANIF_CELLAR_ID_03_OF_EXPR_0001 = EXPR_CELLAR_ID_0001 + ".03";

    private IdentifiersHistoryServiceImpl identifiersHistoryService;
    private IdentifiersHistoryDao identifiersHistoryDao;
    private DigitalObject work;
    private DigitalObject expression0001;
    private DigitalObject expression0003;
    private DigitalObject manifestation01;
    private DigitalObject manifestation03;

    /*
    In these unit tests, we just check the generated cellar ID, the assignment of this ID to the digital object
    is done in the calling method of generateCellarID() so it doesn't matter if we use an existing digital object
    to generate an ID. Normally, we would need to use a digital object which doesn't have a valid CELLAR ID but it doesn't
    matter here.
     */
    @Before
    public void setUp() throws Exception {
        identifiersHistoryDao = mock(IdentifiersHistoryDao.class);
        when(this.identifiersHistoryDao.getLatest(any(Collection.class))).thenReturn(null);

        identifiersHistoryService = new IdentifiersHistoryServiceImpl(identifiersHistoryDao);

        ContentIdentifier contentidentifier = mock(ContentIdentifier.class);
        when(contentidentifier.getIdentifier()).thenReturn("celex:not_important");

        work = new DigitalObject(null);
        ContentIdentifier contentIdentifierWork = mock(ContentIdentifier.class);
        when(contentIdentifierWork.getIdentifier()).thenReturn(WORK_CELLAR_ID);
        work.setCellarId(contentIdentifierWork);
        work.setContentids(Collections.singletonList(contentidentifier));
        work.setType(DigitalObjectType.WORK);

        expression0001 = new DigitalObject(null);
        ContentIdentifier contentIdentifierExpr0001 = mock(ContentIdentifier.class);
        when(contentIdentifierExpr0001.getIdentifier()).thenReturn(EXPR_CELLAR_ID_0001);
        expression0001.setCellarId(contentIdentifierExpr0001);
        expression0001.setParentObject(work);
        expression0001.setContentids(Collections.singletonList(contentidentifier));
        expression0001.setType(DigitalObjectType.EXPRESSION);

        expression0003 = new DigitalObject(null);
        ContentIdentifier contentIdentifierExpr0003 = mock(ContentIdentifier.class);
        when(contentIdentifierExpr0003.getIdentifier()).thenReturn(EXPR_CELLAR_ID_0003);
        expression0003.setCellarId(contentIdentifierExpr0003);
        expression0003.setParentObject(work);
        expression0003.setContentids(Collections.singletonList(contentidentifier));
        expression0003.setType(DigitalObjectType.EXPRESSION);

        manifestation01 = new DigitalObject(null);
        ContentIdentifier contentIdentifierManif01 = mock(ContentIdentifier.class);
        when(contentIdentifierManif01.getIdentifier()).thenReturn(MANIF_CELLAR_ID_01_OF_EXPR_0001);
        manifestation01.setCellarId(contentIdentifierManif01);
        manifestation01.setParentObject(expression0001);
        manifestation01.setContentids(Collections.singletonList(contentidentifier));
        manifestation01.setType(DigitalObjectType.MANIFESTATION);

        manifestation03 = new DigitalObject(null);
        ContentIdentifier contentIdentifierManif03 = mock(ContentIdentifier.class);
        when(contentIdentifierManif03.getIdentifier()).thenReturn(MANIF_CELLAR_ID_03_OF_EXPR_0001);
        manifestation03.setCellarId(contentIdentifierManif03);
        manifestation03.setParentObject(expression0001);
        manifestation03.setContentids(Collections.singletonList(contentidentifier));
        manifestation03.setType(DigitalObjectType.MANIFESTATION);
    }

    @Test
    public void noHistoryFoundArgumentIdIsUsed() throws Exception {
        when(this.identifiersHistoryDao.getAllRecordsStartingWithCellarId(any(String.class))).thenReturn(null);

        DigitalObject digitalObject = mock(DigitalObject.class);
        when(digitalObject.getType()).thenReturn(DigitalObjectType.EXPRESSION);

        ContentIdentifier contentidentifier = mock(ContentIdentifier.class);
        when(contentidentifier.getIdentifier()).thenReturn("celex:blabla");
        when(digitalObject.getContentids()).thenReturn(Collections.singletonList(contentidentifier));

        String generatedCellarId = this.identifiersHistoryService.generateCellarID(digitalObject, WORK_CELLAR_ID, 1, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals(WORK_CELLAR_ID + ".0001", generatedCellarId);

        generatedCellarId = this.identifiersHistoryService.generateCellarID(digitalObject, WORK_CELLAR_ID, 0, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals(WORK_CELLAR_ID + ".0001", generatedCellarId);

        generatedCellarId = this.identifiersHistoryService.generateCellarID(digitalObject, WORK_CELLAR_ID, 99, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals(WORK_CELLAR_ID + ".0099", generatedCellarId);

        when(digitalObject.getType()).thenReturn(DigitalObjectType.MANIFESTATION);
        generatedCellarId = this.identifiersHistoryService.generateCellarID(digitalObject, EXPR_CELLAR_ID_0001, 1, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals(EXPR_CELLAR_ID_0001 + ".01", generatedCellarId);

        generatedCellarId = this.identifiersHistoryService.generateCellarID(digitalObject, EXPR_CELLAR_ID_0001, 0, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals(EXPR_CELLAR_ID_0001 + ".01", generatedCellarId);

        when(digitalObject.getType()).thenReturn(DigitalObjectType.EVENT);
        generatedCellarId = this.identifiersHistoryService.generateCellarID(digitalObject, WORK_CELLAR_ID, 5, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals(WORK_CELLAR_ID + ".0005", generatedCellarId);
    }

    @Test(expected = CellarValidationException.class)
    public void manifestationIdCannotBeOver99() throws Exception {
        when(this.identifiersHistoryDao.getAllRecordsStartingWithCellarId(any(String.class))).thenReturn(null);

        DigitalObject digitalObject = mock(DigitalObject.class);
        when(digitalObject.getType()).thenReturn(DigitalObjectType.MANIFESTATION);

        ContentIdentifier contentidentifier = mock(ContentIdentifier.class);
        when(contentidentifier.getIdentifier()).thenReturn("celex:blabla");
        when(digitalObject.getContentids()).thenReturn(Collections.singletonList(contentidentifier));

        this.identifiersHistoryService.generateCellarID(digitalObject, EXPR_CELLAR_ID_0001, 100, SIPWork.TYPE.AUTHENTICOJ);
    }

    @Test(expected = CellarValidationException.class)
    public void expressionIdCannotBeOver9999() throws Exception {
        when(this.identifiersHistoryDao.getAllRecordsStartingWithCellarId(any(String.class))).thenReturn(null);

        DigitalObject digitalObject = mock(DigitalObject.class);
        when(digitalObject.getType()).thenReturn(DigitalObjectType.EXPRESSION);

        ContentIdentifier contentidentifier = mock(ContentIdentifier.class);
        when(contentidentifier.getIdentifier()).thenReturn("celex:blabla");
        when(digitalObject.getContentids()).thenReturn(Collections.singletonList(contentidentifier));

        this.identifiersHistoryService.generateCellarID(digitalObject, WORK_CELLAR_ID, 10000, SIPWork.TYPE.AUTHENTICOJ);
    }

    @Test
    public void doNotGenerateNewManifestationCellarIdIfCardinalMatchesDigitalObjectId() throws Exception {
        IdentifiersHistory identifierHistory04 = new IdentifiersHistory();
        identifierHistory04.setCellarId(EXPR_CELLAR_ID_0001 + ".04");
        when(this.identifiersHistoryDao.getAllRecordsStartingWithCellarId(EXPR_CELLAR_ID_0001)).thenReturn(Collections.singletonList(identifierHistory04));

        expression0001.addChildObject(manifestation01);
        expression0001.addChildObject(manifestation03);
        work.addChildObject(expression0001);

        String generatedCellarId = this.identifiersHistoryService.generateCellarID(manifestation01, EXPR_CELLAR_ID_0001, 1, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals(EXPR_CELLAR_ID_0001 + ".01", generatedCellarId);

        generatedCellarId = this.identifiersHistoryService.generateCellarID(manifestation03, EXPR_CELLAR_ID_0001, 3, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals(EXPR_CELLAR_ID_0001 + ".03", generatedCellarId);

        generatedCellarId = this.identifiersHistoryService.generateCellarID(manifestation01, EXPR_CELLAR_ID_0001, 0, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals(EXPR_CELLAR_ID_0001 + ".05", generatedCellarId);

        generatedCellarId = this.identifiersHistoryService.generateCellarID(manifestation01, EXPR_CELLAR_ID_0001, 2, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals(EXPR_CELLAR_ID_0001 + ".05", generatedCellarId);
    }

    @Test
    public void doNotGenerateNewExpressionCellarIdIfCardinalMatchesDigitalObjectId() throws Exception {
        IdentifiersHistory identifierHistory0004 = new IdentifiersHistory();
        identifierHistory0004.setCellarId(WORK_CELLAR_ID + ".0004");
        when(this.identifiersHistoryDao.getAllRecordsStartingWithCellarId(WORK_CELLAR_ID)).thenReturn(Collections.singletonList(identifierHistory0004));

        work.addChildObject(expression0001);
        work.addChildObject(expression0003);

        String generatedCellarId = this.identifiersHistoryService.generateCellarID(expression0001, WORK_CELLAR_ID, 1, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals(WORK_CELLAR_ID + ".0001", generatedCellarId);

        generatedCellarId = this.identifiersHistoryService.generateCellarID(expression0003, WORK_CELLAR_ID, 3, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals(WORK_CELLAR_ID + ".0003", generatedCellarId);

        generatedCellarId = this.identifiersHistoryService.generateCellarID(expression0001, WORK_CELLAR_ID, 0, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals(WORK_CELLAR_ID + ".0005", generatedCellarId);

        generatedCellarId = this.identifiersHistoryService.generateCellarID(expression0001, WORK_CELLAR_ID, 2, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals(WORK_CELLAR_ID + ".0005", generatedCellarId);
    }

    @Test
    public void historyFoundForExpressionAndOtherExprInWorkAddExprAfterLatestHistory() throws Exception {
        IdentifiersHistory identifierHistory0002 = new IdentifiersHistory();
        identifierHistory0002.setCellarId(WORK_CELLAR_ID + ".0002");
        IdentifiersHistory identifierHistory0004 = new IdentifiersHistory();
        identifierHistory0004.setCellarId(WORK_CELLAR_ID + ".0004");
        when(this.identifiersHistoryDao.getAllRecordsStartingWithCellarId(WORK_CELLAR_ID)).thenReturn(Arrays.asList(identifierHistory0002, identifierHistory0004));

        work.addChildObject(expression0001);
        work.addChildObject(expression0003);

        String generatedCellarId = this.identifiersHistoryService.generateCellarID(expression0001, WORK_CELLAR_ID, 0, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals(WORK_CELLAR_ID + ".0005", generatedCellarId);
    }

    @Test
    public void historyFoundForExpressionAndOtherExprInWorkAddExprAfterLatestHistoryEvenIfRoomInHierarchy() throws Exception {
        IdentifiersHistory identifierHistory0004 = new IdentifiersHistory();
        identifierHistory0004.setCellarId(WORK_CELLAR_ID + ".0004");
        when(this.identifiersHistoryDao.getAllRecordsStartingWithCellarId(WORK_CELLAR_ID)).thenReturn(Collections.singletonList(identifierHistory0004));

        work.addChildObject(expression0001);
        work.addChildObject(expression0003);

        String generatedCellarId = this.identifiersHistoryService.generateCellarID(expression0001, WORK_CELLAR_ID, 0, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals(WORK_CELLAR_ID + ".0005", generatedCellarId);
    }

    @Test
    public void historyFoundForExpressionAndOtherExprInWorkAddMultipleExpressions() throws Exception {
        IdentifiersHistory identifierHistory0002 = new IdentifiersHistory();
        identifierHistory0002.setCellarId(WORK_CELLAR_ID + ".0002");
        when(this.identifiersHistoryDao.getAllRecordsStartingWithCellarId(WORK_CELLAR_ID)).thenReturn(Collections.singletonList(identifierHistory0002));

        work.addChildObject(expression0001);

        String generatedCellarId = this.identifiersHistoryService.generateCellarID(expression0001, WORK_CELLAR_ID, 0, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals("should be 0003", WORK_CELLAR_ID + ".0003", generatedCellarId);

        work.addChildObject(expression0003);

        generatedCellarId = this.identifiersHistoryService.generateCellarID(expression0001, WORK_CELLAR_ID, 0, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals("should be 0004 since we have 0001 and 0003 in work, 0002 in history", WORK_CELLAR_ID + ".0004", generatedCellarId);
    }

    @Test
    public void historyFoundForExpressionAndOtherExprInWorkAddSameExpression() throws Exception {
        IdentifiersHistory identifierHistory0002 = new IdentifiersHistory();
        identifierHistory0002.setCellarId(WORK_CELLAR_ID + ".0002");
        when(this.identifiersHistoryDao.getAllRecordsStartingWithCellarId(WORK_CELLAR_ID)).thenReturn(Collections.singletonList(identifierHistory0002));

        work.addChildObject(expression0001);
        work.addChildObject(expression0003);

        String generatedCellarId = this.identifiersHistoryService.generateCellarID(expression0001, WORK_CELLAR_ID, 0, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals(WORK_CELLAR_ID + ".0004", generatedCellarId);

        when(this.identifiersHistoryDao.getLatest(any(Collection.class))).thenReturn(identifierHistory0002);
        generatedCellarId = this.identifiersHistoryService.generateCellarID(expression0001, WORK_CELLAR_ID, 0, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals(WORK_CELLAR_ID + ".0002", generatedCellarId);
    }

    @Test
    public void historyFoundForExpressionAndNoExprInWorkAddNewExpressionLatestHistoryIncrementedCellarId() throws Exception {
        IdentifiersHistory identifierHistory0001 = new IdentifiersHistory();
        identifierHistory0001.setCellarId(WORK_CELLAR_ID + ".0001");
        when(this.identifiersHistoryDao.getAllRecordsStartingWithCellarId(WORK_CELLAR_ID)).thenReturn(Collections.singletonList(identifierHistory0001));

        String generatedCellarId = this.identifiersHistoryService.generateCellarID(expression0001, WORK_CELLAR_ID, 0, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals(WORK_CELLAR_ID + ".0002", generatedCellarId);
    }

    @Test
    public void historyFoundForExpressionAndNoExprInWorkAddSameExpressionThenCellarIdFromHistoryIsused() throws Exception {
        IdentifiersHistory identifierHistory0001 = new IdentifiersHistory();
        identifierHistory0001.setCellarId(WORK_CELLAR_ID + ".0001");
        when(this.identifiersHistoryDao.getAllRecordsStartingWithCellarId(WORK_CELLAR_ID)).thenReturn(Collections.singletonList(identifierHistory0001));

        when(this.identifiersHistoryDao.getLatest(any(Collection.class))).thenReturn(identifierHistory0001);

        String generatedCellarId = this.identifiersHistoryService.generateCellarID(expression0001, WORK_CELLAR_ID, 0, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals(WORK_CELLAR_ID + ".0001", generatedCellarId);
    }

    @Test
    public void historyFoundForEventAndOtherEventInDossierAddMultipleEventWithOneSame() throws Exception {
        IdentifiersHistory identifierHistory0002 = new IdentifiersHistory();
        identifierHistory0002.setCellarId(WORK_CELLAR_ID + ".0002");
        when(this.identifiersHistoryDao.getAllRecordsStartingWithCellarId(WORK_CELLAR_ID)).thenReturn(Collections.singletonList(identifierHistory0002));

        expression0001.setType(DigitalObjectType.EVENT);
        work.addChildObject(expression0001);

        expression0003.setType(DigitalObjectType.EVENT);
        work.addChildObject(expression0003);

        String generatedCellarId = this.identifiersHistoryService.generateCellarID(expression0001, WORK_CELLAR_ID, 0, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals(WORK_CELLAR_ID + ".0004", generatedCellarId);

        when(this.identifiersHistoryDao.getLatest(any(Collection.class))).thenReturn(identifierHistory0002);
        generatedCellarId = this.identifiersHistoryService.generateCellarID(expression0001, WORK_CELLAR_ID, 0, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals(WORK_CELLAR_ID + ".0002", generatedCellarId);
    }

    @Test
    public void historyFoundForEventAndNoEventInDossierAddNewEventThenLatestHistoryIncrementedUsed() throws Exception {
        IdentifiersHistory identifierHistory0001 = new IdentifiersHistory();
        identifierHistory0001.setCellarId(WORK_CELLAR_ID + ".0001");
        when(this.identifiersHistoryDao.getAllRecordsStartingWithCellarId(WORK_CELLAR_ID)).thenReturn(Collections.singletonList(identifierHistory0001));

        expression0001.setType(DigitalObjectType.EVENT);
        String generatedCellarId = this.identifiersHistoryService.generateCellarID(expression0001, WORK_CELLAR_ID, 0, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals(WORK_CELLAR_ID + ".0002", generatedCellarId);
    }

    @Test
    public void historyFoundForEventAndNoEventInDossierAddSameEventThenCellarIdFromHistoryIsused() throws Exception {
        IdentifiersHistory identifierHistory0001 = new IdentifiersHistory();
        identifierHistory0001.setCellarId(WORK_CELLAR_ID + ".0001");
        when(this.identifiersHistoryDao.getAllRecordsStartingWithCellarId(WORK_CELLAR_ID)).thenReturn(Collections.singletonList(identifierHistory0001));

        when(this.identifiersHistoryDao.getLatest(any(Collection.class))).thenReturn(identifierHistory0001);

        expression0001.setType(DigitalObjectType.EVENT);
        String generatedCellarId = this.identifiersHistoryService.generateCellarID(expression0001, WORK_CELLAR_ID, 0, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals(WORK_CELLAR_ID + ".0001", generatedCellarId);
    }

    @Test
    public void historyFoundForManifestationAndOtherManifInExprThenUseLatestHistoryAndManifIdIncremented() throws Exception {
        IdentifiersHistory identifierHistory02 = new IdentifiersHistory();
        identifierHistory02.setCellarId(EXPR_CELLAR_ID_0001 + ".02");
        IdentifiersHistory identifierHistory04 = new IdentifiersHistory();
        identifierHistory04.setCellarId(EXPR_CELLAR_ID_0001 + ".04");
        when(this.identifiersHistoryDao.getAllRecordsStartingWithCellarId(EXPR_CELLAR_ID_0001)).thenReturn(Arrays.asList(identifierHistory02, identifierHistory04));

        expression0001.addChildObject(manifestation01);
        expression0001.addChildObject(manifestation03);
        work.addChildObject(expression0001);

        String generatedCellarId = this.identifiersHistoryService.generateCellarID(manifestation01, EXPR_CELLAR_ID_0001, 0, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals(EXPR_CELLAR_ID_0001 + ".05", generatedCellarId);
    }

    @Test
    public void historyFoundForManifestationAndOtherManifInExprThenUseLatestHistoryAndManifIdIncrementedEvenIfHoleInHierarchy() throws Exception {
        IdentifiersHistory identifierHistory04 = new IdentifiersHistory();
        identifierHistory04.setCellarId(EXPR_CELLAR_ID_0001 + ".04");
        when(this.identifiersHistoryDao.getAllRecordsStartingWithCellarId(EXPR_CELLAR_ID_0001)).thenReturn(Collections.singletonList(identifierHistory04));

        expression0001.addChildObject(manifestation01);
        expression0001.addChildObject(manifestation03);
        work.addChildObject(expression0001);

        String generatedCellarId = this.identifiersHistoryService.generateCellarID(manifestation01, EXPR_CELLAR_ID_0001, 0, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals(EXPR_CELLAR_ID_0001 + ".05", generatedCellarId);
    }

    @Test
    public void historyFoundForManifestationAndOtherManifInExprAddMultipleManifestation() throws Exception {
        IdentifiersHistory identifierHistory02 = new IdentifiersHistory();
        identifierHistory02.setCellarId(EXPR_CELLAR_ID_0001 + ".02");
        when(this.identifiersHistoryDao.getAllRecordsStartingWithCellarId(EXPR_CELLAR_ID_0001)).thenReturn(Collections.singletonList(identifierHistory02));

        expression0001.addChildObject(manifestation01);
        work.addChildObject(expression0001);

        String generatedCellarId = this.identifiersHistoryService.generateCellarID(manifestation01, EXPR_CELLAR_ID_0001, 0, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals("should be 03", EXPR_CELLAR_ID_0001 + ".03", generatedCellarId);

        expression0001.addChildObject(manifestation03);

        generatedCellarId = this.identifiersHistoryService.generateCellarID(manifestation01, EXPR_CELLAR_ID_0001, 0, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals("should be 04 since we have 0001 and 03 in expr, 02 in history", EXPR_CELLAR_ID_0001 + ".04", generatedCellarId);
    }

    @Test
    public void historyFoundForManifestationAndOtherManifInExprReassignSameManifestation() throws Exception {
        IdentifiersHistory identifierHistory02 = new IdentifiersHistory();
        identifierHistory02.setCellarId(EXPR_CELLAR_ID_0001 + ".02");
        when(this.identifiersHistoryDao.getAllRecordsStartingWithCellarId(EXPR_CELLAR_ID_0001)).thenReturn(Collections.singletonList(identifierHistory02));

        expression0001.addChildObject(manifestation01);
        expression0001.addChildObject(manifestation03);
        work.addChildObject(expression0001);

        String generatedCellarId = this.identifiersHistoryService.generateCellarID(manifestation01, EXPR_CELLAR_ID_0001, 0, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals("should be 04", EXPR_CELLAR_ID_0001 + ".04", generatedCellarId);

        when(this.identifiersHistoryDao.getLatest(any(Collection.class))).thenReturn(identifierHistory02);
        generatedCellarId = this.identifiersHistoryService.generateCellarID(manifestation01, EXPR_CELLAR_ID_0001, 0, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals("should be 02 since we have the same manif in history", EXPR_CELLAR_ID_0001 + ".02", generatedCellarId);
    }

    @Test
    public void historyFoundForManifestationAndNoOtherManifInExprThenUseLatestHistoryIncremented() throws Exception {
        IdentifiersHistory identifierHistory01 = new IdentifiersHistory();
        identifierHistory01.setCellarId(EXPR_CELLAR_ID_0001 + ".01");
        when(this.identifiersHistoryDao.getAllRecordsStartingWithCellarId(EXPR_CELLAR_ID_0001)).thenReturn(Collections.singletonList(identifierHistory01));

        String generatedCellarId = this.identifiersHistoryService.generateCellarID(manifestation01, EXPR_CELLAR_ID_0001, 0, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals("should be 02", EXPR_CELLAR_ID_0001 + ".02", generatedCellarId);
    }

    @Test
    public void historyFoundForManifestationAndNoOtherManifInExprWhenAddingSameManifThenIdFromHistoryReused() throws Exception {
        IdentifiersHistory identifierHistory01 = new IdentifiersHistory();
        identifierHistory01.setCellarId(EXPR_CELLAR_ID_0001 + ".01");
        when(this.identifiersHistoryDao.getAllRecordsStartingWithCellarId(EXPR_CELLAR_ID_0001)).thenReturn(Collections.singletonList(identifierHistory01));
        when(this.identifiersHistoryDao.getLatest(any(Collection.class))).thenReturn(identifierHistory01);

        String generatedCellarId = this.identifiersHistoryService.generateCellarID(manifestation01, EXPR_CELLAR_ID_0001, 0, SIPWork.TYPE.AUTHENTICOJ);
        assertEquals("should be 01 because taken from history", EXPR_CELLAR_ID_0001 + ".01", generatedCellarId);
    }
}