/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ccr.service.impl
 *             FILE : MetsServiceFactoryTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 12 11, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-12-11 13:23:24 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.service.impl;

import eu.europa.ec.opoce.cellar.ccr.service.client.MetsResponseService;
import eu.europa.ec.opoce.cellar.cl.dao.StructMapResponseDao;
import eu.europa.ec.opoce.cellar.cl.dao.impl.StructMapAuthenticOJResponseDao;
import eu.europa.ec.opoce.cellar.cl.dao.impl.StructMapBulkLowPriorityResponseDao;
import eu.europa.ec.opoce.cellar.cl.dao.impl.StructMapBulkResponseDao;
import eu.europa.ec.opoce.cellar.cl.dao.impl.StructMapDailyResponseDao;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * @author ARHS Developments
 */
public class MetsServiceFactoryTest {
    private MetsServiceFactory metsServiceFactory;

    @Before
    public void setUp() throws Exception {
         MetsResponseService metsAuthenticOJResponseService = new MetsAuthenticOJResponseService(null, null, null, null, null, null);
         MetsResponseService metsDailyResponseService = new MetsDailyResponseService(null, null, null, null, null, null);
         MetsResponseService metsBulkResponseService = new MetsBulkResponseService(null, null, null, null, null, null);
         MetsResponseService metsBulkLowPriorityResponseService = new MetsBulkLowPriorityResponseService(null, null, null, null, null, null);
         StructMapResponseDao structMapAuthenticOJResponseDao = new StructMapAuthenticOJResponseDao(null);
         StructMapResponseDao structMapDailyResponseDao = new StructMapDailyResponseDao(null);
         StructMapResponseDao structMapBulkResponseDao = new StructMapBulkResponseDao(null);
         StructMapResponseDao structMapBulkLowPriorityResponseDao = new StructMapBulkLowPriorityResponseDao(null);
         metsServiceFactory = new MetsServiceFactory(metsAuthenticOJResponseService, metsDailyResponseService, metsBulkResponseService,
        		 metsBulkLowPriorityResponseService, structMapAuthenticOJResponseDao, structMapDailyResponseDao,
        		 structMapBulkResponseDao, structMapBulkLowPriorityResponseDao);
    }

    @Test
    public void testResponseService() throws Exception {
        SIPResource sipResource = new SIPResource();
        sipResource.setSipType(SIPWork.TYPE.AUTHENTICOJ);
        assertTrue(metsServiceFactory.getMetsResponseService(sipResource) instanceof MetsAuthenticOJResponseService);

        sipResource.setSipType(SIPWork.TYPE.DAILY);
        assertTrue(metsServiceFactory.getMetsResponseService(sipResource) instanceof MetsDailyResponseService);

        sipResource.setSipType(SIPWork.TYPE.BULK);
        assertTrue(metsServiceFactory.getMetsResponseService(sipResource) instanceof MetsBulkResponseService);
        
        sipResource.setSipType(SIPWork.TYPE.BULK_LOWPRIORITY);
        assertTrue(metsServiceFactory.getMetsResponseService(sipResource) instanceof MetsBulkLowPriorityResponseService);
    }

    @Test
    public void testStructMapResponseDao() throws Exception {
        SIPResource sipResource = new SIPResource();
        sipResource.setSipType(SIPWork.TYPE.AUTHENTICOJ);
        assertTrue(metsServiceFactory.getStructMapResponseDao(sipResource) instanceof StructMapAuthenticOJResponseDao);

        sipResource.setSipType(SIPWork.TYPE.DAILY);
        assertTrue(metsServiceFactory.getStructMapResponseDao(sipResource) instanceof StructMapDailyResponseDao);

        sipResource.setSipType(SIPWork.TYPE.BULK);
        assertTrue(metsServiceFactory.getStructMapResponseDao(sipResource) instanceof StructMapBulkResponseDao);
        
        sipResource.setSipType(SIPWork.TYPE.BULK_LOWPRIORITY);
        assertTrue(metsServiceFactory.getStructMapResponseDao(sipResource) instanceof StructMapBulkLowPriorityResponseDao);
    }
}