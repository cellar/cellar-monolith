/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState
 *             FILE : CleaningTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 10, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.extractionExecutionState;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionConfiguration.CONFIGURATION_TYPE;
import eu.europa.ec.opoce.cellar.cl.domain.licenseHolder.ExtractionExecution;
import eu.europa.ec.opoce.cellar.cl.service.impl.LicenseHolderServiceImpl;
import eu.europa.ec.opoce.cellar.cl.service.impl.LicenseHolderTestUtils;
import org.apache.commons.io.FileUtils;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.nio.charset.Charset;
import java.util.Calendar;
import java.util.Date;

/**
 * <class_description> Test cases for the classes {@link ArchivingCleaning} and {@link SparqlCleaning}. In particularly, we test the files cleaning.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 10, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class CleaningTest {

    private final static Calendar CALENDAR;
    private final static Date CONFIGURATION_START_DATE;

    /**
     * License holder service to test.
     */
    private LicenseHolderServiceImpl licenseHolderService;

    /**
     * Cellar configuration mock.
     */
    private ICellarConfiguration cellarConfigurationMock;

    /**
     * Test folder.
     */
    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder(); // must be public

    /**
     * Static constructor.
     */
    static {
        CALENDAR = Calendar.getInstance();
        CALENDAR.set(2012, 11, 20);
        CONFIGURATION_START_DATE = CALENDAR.getTime();
    }

    /**
     * Set up of the test.
     */
    @Before
    public void setUp() {
        this.licenseHolderService = new LicenseHolderServiceImpl();
        this.cellarConfigurationMock = EasyMock.createMock(ICellarConfiguration.class);
        this.licenseHolderService.setCellarConfiguration(cellarConfigurationMock);
    }

    /**
     * Test the archiving cleaning of an ad-hoc extraction.
     * @throws Exception
     */
    @Test
    public void archivingCleaningSTDTest() throws Exception {
        final File archivingCleaningFolder = this.temporaryFolder.newFolder("archivingCleaningSTDFolder");
        final String archivingCleaningFolderPath = archivingCleaningFolder.getAbsolutePath();

        // create the temp extraction folder
        final String archiveTempDirectoryName = "/1-tmp-archive-directory";
        final File archiveTempDirectory = new File(archivingCleaningFolderPath + archiveTempDirectoryName);
        archiveTempDirectory.mkdir();

        // create the extraction folder
        final File archiveDirectory = new File(archivingCleaningFolderPath + "/" + LicenseHolderTestUtils.getPath());
        FileUtils.forceMkdir(archiveDirectory);

        // construct the archive file
        final String archivePathFileName = "/" + LicenseHolderTestUtils.getPath() + "/1-STD-ENG.tar.gz";
        final File archiveFile = new File(archivingCleaningFolderPath + archivePathFileName);
        FileUtils.writeStringToFile(archiveFile, "Test data", (Charset) null);

        final ExtractionConfiguration extractionConfiguration = new ExtractionConfiguration(LicenseHolderTestUtils.getConfigurationName(),
                CONFIGURATION_TYPE.STD, LicenseHolderTestUtils.getLanguageBeanEng().getIsoCodeThreeChar(), LicenseHolderTestUtils.getPath(),
                CONFIGURATION_START_DATE, "");
        extractionConfiguration.setLanguageBean(LicenseHolderTestUtils.getLanguageBeanEng());

        final ExtractionExecution extractionExecution = extractionConfiguration.getExtractionExecution(); // generate the first execution
        // set the temp folder path and the archive file
        extractionExecution.setArchiveTempDirectoryPath(archiveTempDirectoryName);
        extractionExecution.setArchiveFilePath(archivePathFileName);

        // init the configuration mock
        LicenseHolderTestUtils.expectFolderLicenseHolderArchiveAdhocExtraction(this.cellarConfigurationMock, archivingCleaningFolderPath,
                2);
        LicenseHolderTestUtils.expectFolderLicenseHolderTemporaryArchive(this.cellarConfigurationMock, archivingCleaningFolderPath, 1);
        EasyMock.replay(this.cellarConfigurationMock);

        final ArchivingCleaning archivingCleaning = new ArchivingCleaning();
        archivingCleaning.setCellarConfiguration(this.cellarConfigurationMock);
        archivingCleaning.setLicenseHolderService(this.licenseHolderService);

        archivingCleaning.clean(extractionExecution, true);

        Assert.assertNull(extractionExecution.getArchiveFilePath());
        Assert.assertNull(extractionExecution.getArchiveTempDirectoryPath());

        Assert.assertTrue(archivingCleaningFolder.list().length == 0); // check the file system

        EasyMock.verify(this.cellarConfigurationMock);
    }

    /**
     * Test the archiving cleaning of a daily extraction.
     * @throws Exception
     */
    @Test
    public void archivingCleaningSTDDTest() throws Exception {
        final File archivingCleaningFolder = this.temporaryFolder.newFolder("archivingCleaningSTDDFolder");
        final String archivingCleaningFolderPath = archivingCleaningFolder.getAbsolutePath();

        // create the temp extraction folder
        final String archiveTempDirectoryName = "/1-tmp-archive-directory";
        final File archiveTempDirectory = new File(archivingCleaningFolderPath + archiveTempDirectoryName);
        archiveTempDirectory.mkdir();

        final String archivePathFileName = "/ENG/DAILY/20121121/" + LicenseHolderTestUtils.getPath() + "/1-STD-ENG.tar.gz";

        final File archiveFile = new File(archivingCleaningFolderPath + archivePathFileName);
        FileUtils.writeStringToFile(archiveFile, "Test data", (Charset) null);

        final ExtractionConfiguration extractionConfiguration = new ExtractionConfiguration(LicenseHolderTestUtils.getConfigurationName(),
                CONFIGURATION_TYPE.STDD, LicenseHolderTestUtils.getLanguageBeanEng().getIsoCodeThreeChar(),
                LicenseHolderTestUtils.getPath(), CONFIGURATION_START_DATE, "");
        extractionConfiguration.setLanguageBean(LicenseHolderTestUtils.getLanguageBeanEng());

        final ExtractionExecution extractionExecution = extractionConfiguration.getExtractionExecution(); // generate the first execution
        extractionExecution.setArchiveTempDirectoryPath(archiveTempDirectoryName);
        extractionExecution.setArchiveFilePath(archivePathFileName);

        // init the configuration mock
        LicenseHolderTestUtils.expectFolderLicenseHolderArchiveExtraction(this.cellarConfigurationMock, archivingCleaningFolderPath, 2);
        LicenseHolderTestUtils.expectFolderLicenseHolderTemporaryArchive(this.cellarConfigurationMock, archivingCleaningFolderPath, 1);
        EasyMock.replay(this.cellarConfigurationMock);

        final ArchivingCleaning archivingCleaning = new ArchivingCleaning();
        archivingCleaning.setCellarConfiguration(this.cellarConfigurationMock);
        archivingCleaning.setLicenseHolderService(this.licenseHolderService);

        archivingCleaning.clean(extractionExecution, true);

        Assert.assertNull(extractionExecution.getArchiveFilePath());
        Assert.assertNull(extractionExecution.getArchiveTempDirectoryPath());

        // check the file system
        Assert.assertTrue(archivingCleaningFolder.list().length == 1); // the folder /ENG/DAILY is conserved (because it's shared with other extractions)
        File dailyFolder = new File(archivingCleaningFolderPath + "/ENG/DAILY");
        Assert.assertTrue(dailyFolder.list().length == 0);

        EasyMock.verify(this.cellarConfigurationMock);
    }

    /**
     * Test the SPARQL cleaning.
     * @throws Exception
     */
    @Test
    public void sparqlCleaningTest() throws Exception {
        final File sparqlCleaningFolder = this.temporaryFolder.newFolder("sparqlCleaningFolder");
        final String sparqlCleaningFolderPath = sparqlCleaningFolder.getAbsolutePath();

        // init the configuration mock
        LicenseHolderTestUtils.expectFolderLicenseHolderTemporarySparql(this.cellarConfigurationMock, sparqlCleaningFolderPath, 1);
        EasyMock.replay(this.cellarConfigurationMock);

        // construct the SPARQL response file
        final ExtractionExecution extractionExecution = new ExtractionExecution();
        extractionExecution.setSparqlTempFilePath("/1-tmp-sparql-response.xml");

        final File sparqlFile = new File(sparqlCleaningFolderPath + extractionExecution.getSparqlTempFilePath());
        FileUtils.writeStringToFile(sparqlFile, "Test data", (Charset) null);

        final SparqlCleaning sparqlCleaning = new SparqlCleaning();
        sparqlCleaning.setCellarConfiguration(this.cellarConfigurationMock);
        sparqlCleaning.setLicenseHolderService(this.licenseHolderService);

        sparqlCleaning.clean(extractionExecution, true);

        Assert.assertNull(extractionExecution.getSparqlTempFilePath());

        Assert.assertTrue(sparqlCleaningFolder.list().length == 0); // check the file system

        EasyMock.verify(this.cellarConfigurationMock);
    }
}
