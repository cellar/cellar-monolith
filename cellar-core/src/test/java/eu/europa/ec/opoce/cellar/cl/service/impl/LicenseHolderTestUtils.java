/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *             FILE : LicenseHolderTestUtils.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 7, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import org.easymock.EasyMock;

/**
 * <class_description> License holder test helper.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 7, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class LicenseHolderTestUtils {

    private final static LanguageBean LANGUAGE_BEAN_ENG;

    private final static String CONFIGURATION_NAME = "configuration name";
    private final static String PATH = "folderX/folderY";
    private final static String SPARQL_QUERY = "SELECT ?uri ?y ?z WHERE { ?uri ?y ?z }";

    private final static String FOLDER_LICENSE_HOLDER_ARCHIVE_EXTRACTION = "C:/projects/Cellar/jboss/conf/cellarapp/license-holder-extraction";
    private final static String FOLDER_LICENSE_HOLDER_ARCHIVE_ADHOC_EXTRACTION = "C:/projects/Cellar/jboss/conf/cellarapp/license-holder-adhoc-extraction";
    private final static String FOLDER_LICENSE_HOLDER_TEMPORARY_ARCHIVE = "C:/projects/Cellar/jboss/conf/cellarapp/temporary-license-holder/archive";
    private final static String FOLDER_LICENSE_HOLDER_TEMPORARY_SPARQL = "C:/projects/Cellar/jboss/conf/cellarapp/temporary-license-holder/sparql";

    private final static int SERVICE_LICENSE_HOLDER_KEPT_DAYS_DAILY = 5;
    private final static int SERVICE_LICENSE_HOLDER_KEPT_DAYS_WEEKLY = 5;
    private final static int SERVICE_LICENSE_HOLDER_KEPT_DAYS_MONTHLY = 5;

    /**
     * Static constructor.
     */
    static {
        LANGUAGE_BEAN_ENG = new LanguageBean();
        LANGUAGE_BEAN_ENG.setCode("ENG");
        LANGUAGE_BEAN_ENG.setIsoCodeThreeChar("eng");
        LANGUAGE_BEAN_ENG.setIsoCodeTwoChar("en");
        LANGUAGE_BEAN_ENG.setUri("http://publications.europa.eu/resource/authority/language/ENG");
    }

    /**
     * Return the language bean ENG.
     * @return the language bean ENG
     */
    public static LanguageBean getLanguageBeanEng() {
        return LANGUAGE_BEAN_ENG;
    }

    /**
     * Return the configuration name.
     * @return the configuration name
     */
    public static String getConfigurationName() {
        return CONFIGURATION_NAME;
    }

    /**
     * Return the path.
     * @return the path
     */
    public static String getPath() {
        return PATH;
    }

    /**
     * Return the SPARQL query.
     * @return the SPARQL query
     */
    public static String getSparqlQuery() {
        return SPARQL_QUERY;
    }

    /**
     * Init the getCellarFolderLicenseHolderArchiveExtraction method of the configuration mock.
     * @param cellarConfigurationMock the configuration mock
     * @param times the number of invocations expected
     */
    public static void expectFolderLicenseHolderArchiveExtraction(final ICellarConfiguration cellarConfigurationMock, final int times) {
        EasyMock.expect(cellarConfigurationMock.getCellarFolderLicenseHolderArchiveExtraction())
                .andReturn(FOLDER_LICENSE_HOLDER_ARCHIVE_EXTRACTION).times(times);
    }

    /**
     * Init the getCellarFolderLicenseHolderArchiveExtraction method of the configuration mock.
     * @param cellarConfigurationMock the configuration mock
     * @param path the path to return
     * @param times the number of invocations expected
     */
    public static void expectFolderLicenseHolderArchiveExtraction(final ICellarConfiguration cellarConfigurationMock, final String path,
            final int times) {
        EasyMock.expect(cellarConfigurationMock.getCellarFolderLicenseHolderArchiveExtraction()).andReturn(path).times(times);
    }

    /**
     * Init the getCellarFolderLicenseHolderArchiveAdhocExtraction method of the configuration mock.
     * @param cellarConfigurationMock the configuration mock
     * @param times the number of invocations expected
     */
    public static void expectFolderLicenseHolderArchiveAdhocExtraction(final ICellarConfiguration cellarConfigurationMock,
            final int times) {
        EasyMock.expect(cellarConfigurationMock.getCellarFolderLicenseHolderArchiveAdhocExtraction())
                .andReturn(FOLDER_LICENSE_HOLDER_ARCHIVE_ADHOC_EXTRACTION).times(times);
    }

    /**
     * Init the getCellarFolderLicenseHolderArchiveAdhocExtraction method of the configuration mock.
     * @param cellarConfigurationMock the configuration mock
     * @param path the path to return
     * @param times the number of invocations expected
     */
    public static void expectFolderLicenseHolderArchiveAdhocExtraction(final ICellarConfiguration cellarConfigurationMock,
            final String path, final int times) {
        EasyMock.expect(cellarConfigurationMock.getCellarFolderLicenseHolderArchiveAdhocExtraction()).andReturn(path).times(times);
    }

    /**
     * Init the getCellarFolderLicenseHolderTemporaryArchive method of the configuration mock.
     * @param cellarConfigurationMock the configuration mock
     * @param times the number of invocations expected
     */
    public static void expectFolderLicenseHolderTemporaryArchive(final ICellarConfiguration cellarConfigurationMock, final int times) {
        EasyMock.expect(cellarConfigurationMock.getCellarFolderLicenseHolderTemporaryArchive())
                .andReturn(FOLDER_LICENSE_HOLDER_TEMPORARY_ARCHIVE).times(times);
    }

    /**
     * Init the getCellarFolderLicenseHolderTemporaryArchive method of the configuration mock.
     * @param cellarConfigurationMock the configuration mock
     * @param path the path to return
     * @param times the number of invocations expected
     */
    public static void expectFolderLicenseHolderTemporaryArchive(final ICellarConfiguration cellarConfigurationMock, final String path,
            final int times) {
        EasyMock.expect(cellarConfigurationMock.getCellarFolderLicenseHolderTemporaryArchive()).andReturn(path).times(times);
    }

    /**
     * Init the getCellarFolderLicenseHolderTemporarySparql method of the configuration mock.
     * @param cellarConfigurationMock the configuration mock
     * @param times the number of invocations expected
     */
    public static void expectFolderLicenseHolderTemporarySparql(final ICellarConfiguration cellarConfigurationMock, final int times) {
        EasyMock.expect(cellarConfigurationMock.getCellarFolderLicenseHolderTemporarySparql())
                .andReturn(FOLDER_LICENSE_HOLDER_TEMPORARY_SPARQL).times(times);
    }

    /**
     * Init the getCellarFolderLicenseHolderTemporarySparql method of the configuration mock.
     * @param cellarConfigurationMock the configuration mock
     * @param path the path to return
     * @param times the number of invocations expected
     */
    public static void expectFolderLicenseHolderTemporarySparql(final ICellarConfiguration cellarConfigurationMock, final String path,
            final int times) {
        EasyMock.expect(cellarConfigurationMock.getCellarFolderLicenseHolderTemporarySparql()).andReturn(path).times(times);
    }

    /**
     * Init the getCellarServiceLicenseHolderKeptDaysDaily method of the configuration mock.
     * @param cellarConfigurationMock the configuration mock
     * @param times the number of invocations expected
     */
    public static void expectServiceLicenseHolderKeptDaysDaily(final ICellarConfiguration cellarConfigurationMock, final int times) {
        EasyMock.expect(cellarConfigurationMock.getCellarServiceLicenseHolderKeptDaysDaily())
                .andReturn(SERVICE_LICENSE_HOLDER_KEPT_DAYS_DAILY).times(times);
    }

    /**
     * Init the getCellarServiceLicenseHolderKeptDaysWeekly method of the configuration mock.
     * @param cellarConfigurationMock the configuration mock
     * @param times the number of invocations expected
     */
    public static void expectServiceLicenseHolderKeptDaysWeekly(final ICellarConfiguration cellarConfigurationMock, final int times) {
        EasyMock.expect(cellarConfigurationMock.getCellarServiceLicenseHolderKeptDaysWeekly())
                .andReturn(SERVICE_LICENSE_HOLDER_KEPT_DAYS_WEEKLY).times(times);
    }

    /**
     * Init the getCellarServiceLicenseHolderKeptDaysMonthly method of the configuration mock.
     * @param cellarConfigurationMock the configuration mock
     * @param times the number of invocations expected
     */
    public static void expectServiceLicenseHolderKeptDaysMonthly(final ICellarConfiguration cellarConfigurationMock, final int times) {
        EasyMock.expect(cellarConfigurationMock.getCellarServiceLicenseHolderKeptDaysMonthly())
                .andReturn(SERVICE_LICENSE_HOLDER_KEPT_DAYS_MONTHLY).times(times);
    }

    /**
     * Return the archive extraction folder.
     * @return the archive extraction folder
     */
    public static String getFolderLicenseHolderArchiveExtraction() {
        return FOLDER_LICENSE_HOLDER_ARCHIVE_EXTRACTION;
    }

    /**
     * Return the archive ad-hoc extraction folder.
     * @return the archive ad-hoc extraction folder
     */
    public static String getFolderLicenseHolderArchiveAdhocExtraction() {
        return FOLDER_LICENSE_HOLDER_ARCHIVE_ADHOC_EXTRACTION;
    }

    /**
     * Return the temporary archive folder.
     * @return the temporary archive folder
     */
    public static String getFolderLicenseHolderTemporaryArchive() {
        return FOLDER_LICENSE_HOLDER_TEMPORARY_ARCHIVE;
    }

    /**
     * Return the temporary SPARQL folder.
     * @return the temporary SPARQL folder
     */
    public static String getFolderLicenseHolderTemporarySparql() {
        return FOLDER_LICENSE_HOLDER_TEMPORARY_SPARQL;
    }

    /**
     * Return the number of kept days daily.
     * @return the number of kept days daily
     */
    public static int getServiceLicenseHolderKeptDaysDaily() {
        return SERVICE_LICENSE_HOLDER_KEPT_DAYS_DAILY;
    }

    /**
     * Return the number of kept days weekly.
     * @return the number of kept days weekly
     */
    public static int getServiceLicenseHolderKeptDaysWeekly() {
        return SERVICE_LICENSE_HOLDER_KEPT_DAYS_WEEKLY;
    }

    /**
     * Return the number of kept days monthly.
     * @return the number of kept days monthly
     */
    public static int getServiceLicenseHolderKeptDaysMonthly() {
        return SERVICE_LICENSE_HOLDER_KEPT_DAYS_MONTHLY;
    }
}
