package eu.europa.ec.opoce.cellar.cl.service.impl;

import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SipDependencyCheckerMetsParserTest {

	private static File inputMetsFile;
	private static Map<String, String> expectedBusinessMetadata;
	private static Map<String, List<String>> expectedContentIdsPerDmdId;
	private SipDependencyCheckerMetsParser metsParser;
	
	static {
		expectedBusinessMetadata = new HashMap<>();
		expectedBusinessMetadata.put("md_cellar_90596cc1-5163-4502-94d9-935520ce7364", "md_cellar_90596cc1-5163-4502-94d9-935520ce7364.rdf");
		expectedBusinessMetadata.put("md_cellar_90596cc1-5163-4502-94d9-935520ce7364_0001", "md_cellar_90596cc1-5163-4502-94d9-935520ce7364_0001.rdf");
		expectedBusinessMetadata.put("md_cellar_90596cc1-5163-4502-94d9-935520ce7364_0001_02", "md_cellar_90596cc1-5163-4502-94d9-935520ce7364_0001_02.rdf");
		expectedBusinessMetadata.put("md_cellar_90596cc1-5163-4502-94d9-935520ce7364_0001_03", "md_cellar_90596cc1-5163-4502-94d9-935520ce7364_0001_03.rdf");
		expectedBusinessMetadata.put("md_cellar_90596cc1-5163-4502-94d9-935520ce7364_0001_04", "md_cellar_90596cc1-5163-4502-94d9-935520ce7364_0001_04.rdf");
		expectedBusinessMetadata.put("md_cellar_90596cc1-5163-4502-94d9-935520ce7364_0001_05", "md_cellar_90596cc1-5163-4502-94d9-935520ce7364_0001_05.rdf");
		
		expectedContentIdsPerDmdId = new HashMap<>();
		expectedContentIdsPerDmdId.put("md_cellar_90596cc1-5163-4502-94d9-935520ce7364", Arrays.asList(new String[] {
				"eli:dir:1973:23:corrigendum:2006-04-25:oj",
				"oj:JOL_2006_111_R_0015_02_DIR_1973_23_15",
				"celex:31973L0023R%2803%29",
				"cellar:90596cc1-5163-4502-94d9-935520ce7364"
		}));
		expectedContentIdsPerDmdId.put("md_cellar_90596cc1-5163-4502-94d9-935520ce7364_0001", Arrays.asList(new String[] {
				"oj:JOL_2006_111_R_0015_02_DIR_1973_23_15.POR",
				"celex:31973L0023R%2803%29.POR",
				"uriserv:OJ.L_.2006.111.01.0015.02.POR",
				"cellar:90596cc1-5163-4502-94d9-935520ce7364.0001"
		}));
		expectedContentIdsPerDmdId.put("md_cellar_90596cc1-5163-4502-94d9-935520ce7364_0001_02", Arrays.asList(new String[] {
				"oj:JOL_2006_111_R_0015_02_DIR_1973_23_15.POR.pdf",
				"celex:31973L0023R%2803%29.POR.pdf",
				"uriserv:OJ.L_.2006.111.01.0015.02.POR.pdf",
				"cellar:90596cc1-5163-4502-94d9-935520ce7364.0001.02"
		}));
		expectedContentIdsPerDmdId.put("md_cellar_90596cc1-5163-4502-94d9-935520ce7364_0001_03", Arrays.asList(new String[] {
				"oj:JOL_2006_111_R_0015_02_DIR_1973_23_15.POR.xhtml",
				"celex:31973L0023R%2803%29.POR.xhtml",
				"uriserv:OJ.L_.2006.111.01.0015.02.POR.xhtml",
				"cellar:90596cc1-5163-4502-94d9-935520ce7364.0001.03"
		}));
		expectedContentIdsPerDmdId.put("md_cellar_90596cc1-5163-4502-94d9-935520ce7364_0001_04", Arrays.asList(new String[] {
				"oj:JOL_2006_111_R_0015_02_DIR_1973_23_15.POR.print",
				"celex:31973L0023R%2803%29.POR.print",
				"uriserv:OJ.L_.2006.111.01.0015.02.POR.print",
				"cellar:90596cc1-5163-4502-94d9-935520ce7364.0001.04"
		}));
		expectedContentIdsPerDmdId.put("md_cellar_90596cc1-5163-4502-94d9-935520ce7364_0001_05", Arrays.asList(new String[] {
				"oj:JOL_2006_111_R_0015_02_DIR_1973_23_15.POR.fmx4",
				"celex:31973L0023R%2803%29.POR.fmx4",
				"uriserv:OJ.L_.2006.111.01.0015.02.POR.fmx4",
				"cellar:90596cc1-5163-4502-94d9-935520ce7364.0001.05"
		}));
		
	}

	@Before
    public void setup() throws Exception {
        String metsFile = "src/test/resources/eu/europa/ec/opoce/cellar/cl/service/impl/sipDependencyCheckerMetsParser.mets.xml";
        inputMetsFile = new File(metsFile);
        this.metsParser = new SipDependencyCheckerMetsParser();
	}
	
	@Test
	public void validateDataExtractedFromMets() throws Exception {
		Map<String, String> businessMetadataRefsPerId = new HashMap<>();
        Map<String, List<String>> contentIdsPerDmdid = new HashMap<>();
		Map<String, String> metsInfo = metsParser.parse(inputMetsFile, businessMetadataRefsPerId, contentIdsPerDmdid);
		
		assertEquals("create", metsInfo.get("metsType"));
		
		assertEquals(6, businessMetadataRefsPerId.size());
		for (Entry<String, String> expectedEntry : expectedBusinessMetadata.entrySet()) {
			assertEquals(expectedEntry.getValue(), businessMetadataRefsPerId.get(expectedEntry.getKey()));
		}
		
		assertEquals(6, contentIdsPerDmdid.size());
		for (Entry<String, List<String>> expectedEntry : expectedContentIdsPerDmdId.entrySet()) {
			Set<String> contentIdsFound = contentIdsPerDmdid.get(expectedEntry.getKey()).stream()
				.filter(Objects::nonNull)
				.map(s -> Arrays.asList(s.split(" ")))
				.flatMap(List::stream)
				.collect(Collectors.toSet());
			
			assertEquals(4, contentIdsFound.size());
			for (String expectedContentId : expectedEntry.getValue()) {
				assertTrue(contentIdsFound.contains(expectedContentId));
			}
		}
	}
	
}
