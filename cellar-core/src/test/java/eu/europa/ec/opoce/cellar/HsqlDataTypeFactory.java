/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : CellarIdentifier.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 22 mars 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar;

import org.dbunit.dataset.datatype.DataType;
import org.dbunit.dataset.datatype.DataTypeException;
import org.hsqldb.Types;

/**
 * A factory for creating DefaultDataType objects.
 *  A new data type factory that extends the DBUnit class DefaultDataTypeFactory.
 *  This new class just handles the SQL type Types.BOOLEAN as a special case
 */
public class HsqlDataTypeFactory extends org.dbunit.dataset.datatype.DefaultDataTypeFactory {

    /** {@inheritDoc} */
    @Override
    public DataType createDataType(final int sqlType, final String sqlTypeName) throws DataTypeException {
        if (sqlType == Types.BOOLEAN) {
            return DataType.BOOLEAN;
        }
        final DataType result = super.createDataType(sqlType, sqlTypeName);
        return result;
    }
}
