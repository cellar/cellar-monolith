package eu.europa.ec.opoce.cellar.cl.domain.validator;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.dao.impl.FileSystemDao;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPObject;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.cl.service.client.MetsAnalyzer;
import eu.europa.ec.opoce.cellar.cl.service.impl.MetsAnalyzerImpl;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork;
import net.lingala.zip4j.core.ZipFile;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ContentValidatorTest {

    private static Path sipFileWithoutRelativeHrefVersionOnePath;
    private static Path sipFileWithRelativeHrefVersionOnePath;
    private static Path testDirectoryPath;
    private ValidationDetails sipFileWithoutRelativeHrefVersionOne;
    private ValidationDetails sipFileWithRelativeHrefVersionOne;

    private static ContentValidator contentValidator;
    private static FileSystemDao fileSystemDao;
    private static MetsAnalyzer metsAnalyzer;
    private static ICellarConfiguration cellarConfiguration;

    @BeforeClass
    public static void oneTimeSetup() throws Exception {
        testDirectoryPath = Paths.get(System.getProperty("java.io.tmpdir") + File.separator + "CELLAR");
        createTestDirectory();

        cellarConfiguration = mock(ICellarConfiguration.class);
        when(cellarConfiguration.getCellarFolderAuthenticOJReception()).thenReturn(testDirectoryPath + File.separator + "authOj");
        when(cellarConfiguration.getCellarFolderDailyReception()).thenReturn(testDirectoryPath + File.separator + "dailyReception");
        when(cellarConfiguration.getCellarFolderExportRest()).thenReturn(testDirectoryPath + File.separator + "exportRest");
        when(cellarConfiguration.getCellarFolderExportUpdateResponse()).thenReturn(testDirectoryPath + File.separator + "exportUpdateResponse");
        when(cellarConfiguration.getCellarFolderExportBatchJob()).thenReturn(testDirectoryPath + File.separator + "exportBatchJob");
        when(cellarConfiguration.getCellarFolderError()).thenReturn(testDirectoryPath + File.separator + "error");
        when(cellarConfiguration.getCellarFolderBulkReception()).thenReturn(testDirectoryPath + File.separator + "bulkReception");
        when(cellarConfiguration.getCellarFolderBulkLowPriorityReception()).thenReturn(testDirectoryPath + File.separator + "bulkLowPriorityReception");
        when(cellarConfiguration.getCellarFolderTemporaryWork()).thenReturn(testDirectoryPath + File.separator + "temporaryWork");
        when(cellarConfiguration.getCellarFolderFoxml()).thenReturn(testDirectoryPath + File.separator + "foxml");
        when(cellarConfiguration.getCellarFolderDailyResponse()).thenReturn(testDirectoryPath + File.separator + "dailyResponse");
        when(cellarConfiguration.getCellarFolderBulkResponse()).thenReturn(testDirectoryPath + File.separator + "bulkResponse");
        when(cellarConfiguration.getCellarFolderBulkLowPriorityResponse()).thenReturn(testDirectoryPath + File.separator + "bulkLowPriorityResponse");
        when(cellarConfiguration.getCellarFolderBackup()).thenReturn(testDirectoryPath + File.separator + "backup");
        when(cellarConfiguration.getCellarFolderExportBatchJobTemporary()).thenReturn(testDirectoryPath + File.separator + "batchJobTemporary");
        when(cellarConfiguration.getCellarFolderAuthenticOJResponse()).thenReturn(testDirectoryPath + File.separator + "authOjResponse");
        when(cellarConfiguration.getCellarFolderRoot()).thenReturn(testDirectoryPath + File.separator);
        when(cellarConfiguration.getCellarFolderCmrLog()).thenReturn(testDirectoryPath + File.separator + "logs");
        when(cellarConfiguration.getCellarFolderTemporaryDissemination()).thenReturn(testDirectoryPath + File.separator + "tmpDissemination");
        when(cellarConfiguration.getCellarFolderTemporaryWork()).thenReturn(testDirectoryPath + File.separator + "tmpWork");
        when(cellarConfiguration.getCellarFolderTemporaryStore()).thenReturn(testDirectoryPath + File.separator + "tmpStore");
        when(cellarConfiguration.getCellarFolderExportRest()).thenReturn(testDirectoryPath + File.separator + "exportRest");
        when(cellarConfiguration.getCellarFolderExportUpdateResponse()).thenReturn(testDirectoryPath + File.separator + "exportUpdateReponse");
        when(cellarConfiguration.getCellarFolderLicenseHolderArchiveExtraction()).thenReturn(testDirectoryPath + File.separator + "licenceArchiveExtract");
        when(cellarConfiguration.getCellarFolderLicenseHolderArchiveAdhocExtraction()).thenReturn(testDirectoryPath + File.separator + "licenceArchiveExtractAdHoc");
        when(cellarConfiguration.getCellarFolderLicenseHolderTemporaryArchive()).thenReturn(testDirectoryPath + File.separator + "licenceArchivetmp");
        when(cellarConfiguration.getCellarFolderLicenseHolderTemporarySparql()).thenReturn(testDirectoryPath + File.separator + "licencetmpSparql");
        when(cellarConfiguration.getCellarFolderBatchJobSparqlQueries()).thenReturn(testDirectoryPath + File.separator + "batchJobSparqlQueries");
        when(cellarConfiguration.getCellarFolderSparqlResponseTemplates()).thenReturn(testDirectoryPath + File.separator + "sparqlResponseTemplates");
        when(cellarConfiguration.getCellarFolderAuthenticOJReception()).thenReturn(testDirectoryPath + File.separator + "authenticOJReception");
        when(cellarConfiguration.getCellarFolderAuthenticOJResponse()).thenReturn(testDirectoryPath + File.separator + "authenticOJResponse");
        when(cellarConfiguration.getCellarFolderLock()).thenReturn(testDirectoryPath + File.separator + "lock");
        when(cellarConfiguration.isCellarServiceIngestionEnabled()).thenReturn(true);
        fileSystemDao = new FileSystemDao(cellarConfiguration);
        fileSystemDao.initialize();

        metsAnalyzer = new MetsAnalyzerImpl(cellarConfiguration);
        contentValidator = new ContentValidator(metsAnalyzer);

        sipFileWithoutRelativeHrefVersionOnePath = Paths
                .get("src/test/resources/eu/europa/ec/opoce/cellar/cl/domain/validator/sipFileWithoutRelativeHref_v1.zip");
        sipFileWithRelativeHrefVersionOnePath = Paths
                .get("src/test/resources/eu/europa/ec/opoce/cellar/cl/domain/validator/sipFileWithRelativeHref_v1.zip");

        copyTestZipFilesToTestDirectory();
        unzipFilesInTestDirectory();
    }

    @AfterClass
    public static void oneTimeTearDown() throws Exception {
        cleanTestDirectory();
    }

    private static void unzipFilesInTestDirectory() throws Exception {
        Path workFolder = Paths.get(cellarConfiguration.getCellarFolderTemporaryWork());
        File[] zipFiles = workFolder.toFile().listFiles();
        if (zipFiles != null) {
            for (File zipFile : zipFiles) {
                ZipFile zip = new ZipFile(zipFile.getPath());
                String destinationFolder = workFolder + File.separator + zipFile.getName().replace(".zip", "");
                zip.extractAll(destinationFolder);
            }
        }
    }

    private static void copyTestZipFilesToTestDirectory() throws IOException {
        Path destination = Paths
                .get(cellarConfiguration.getCellarFolderTemporaryWork() + File.separator + sipFileWithoutRelativeHrefVersionOnePath.getFileName());
        Files.copy(sipFileWithoutRelativeHrefVersionOnePath, destination);

        destination = Paths.get(cellarConfiguration.getCellarFolderTemporaryWork() + File.separator + sipFileWithRelativeHrefVersionOnePath.getFileName());
        Files.copy(sipFileWithRelativeHrefVersionOnePath, destination);
    }

    private static void createTestDirectory() throws Exception {
        if (!Files.exists(testDirectoryPath))
            Files.createDirectories(testDirectoryPath);
    }

    private static void cleanTestDirectory() throws IOException {
        if (Files.exists(testDirectoryPath)) {
            Files.walk(testDirectoryPath)
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        }
    }

    @Before
    public void setup() throws Exception {
        SIPResource sipResource = new SIPResource("sipFileWithoutRelativeHref_v1", cellarConfiguration.getCellarFolderTemporaryWork(),
                "sipFileWithoutRelativeHref_v1.zip", SIPWork.TYPE.AUTHENTICOJ);
        SIPObject sipFileWithoutRelativeHrefVersionOneObject = createSIPObjectFromResource(sipResource);
        sipFileWithoutRelativeHrefVersionOne = new ValidationDetails(sipFileWithoutRelativeHrefVersionOneObject, sipResource);

        sipResource = new SIPResource("sipFileWithRelativeHref_v1", cellarConfiguration.getCellarFolderTemporaryWork(),
                "sipFileWithRelativeHref_v1.zip", SIPWork.TYPE.AUTHENTICOJ);
        SIPObject sipFileWithRelativeHrefVersionOneObject = createSIPObjectFromResource(sipResource);
        sipFileWithRelativeHrefVersionOne = new ValidationDetails(sipFileWithRelativeHrefVersionOneObject, sipResource);

        contentValidator = new ContentValidator(metsAnalyzer);
    }

    private SIPObject createSIPObjectFromResource(SIPResource sipResource) throws IOException {
        SIPObject sipObject = new SIPObject();
        sipObject.setSipType(sipResource.getSipType());
        String extractionFolder = sipResource.getFolderName() + File.separator + sipResource.getMetsID();
        sipObject.setZipExtractionFolder(new File(extractionFolder));
        sipObject.setZipFile(new File(sipResource.getFolderName(), sipResource.getArchiveName()));
        List<File> uncompressedFilesList = new ArrayList<>(
                Arrays.asList(fileSystemDao.getAllFilesFromUncompressedLocation(sipResource.getMetsID())));
        sipObject.setFileList(uncompressedFilesList);
        return sipObject;
    }

    @Test
    public void validMetsContainingRelativePathForHrefShouldBeValid() throws Exception {
        ValidationResult validationResult = contentValidator.validate(sipFileWithRelativeHrefVersionOne);
        assertTrue(validationResult.isValid());
    }

    @Test
    public void validMetsContainingAbsolutePathForHrefShouldBeValid() throws Exception {
        ValidationResult validationResult = contentValidator.validate(sipFileWithoutRelativeHrefVersionOne);
        assertTrue(validationResult.isValid());
    }
}
