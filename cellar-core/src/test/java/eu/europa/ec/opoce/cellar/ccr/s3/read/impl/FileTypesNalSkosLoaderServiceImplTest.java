package eu.europa.ec.opoce.cellar.ccr.s3.read.impl;

import eu.europa.ec.opoce.cellar.cl.domain.mimeType.MimeTypeMapping;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import org.apache.jena.rdf.model.Model;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

public class FileTypesNalSkosLoaderServiceImplTest {

    private Path emptySkos = Paths.get("src/test/resources/eu/europa/ec/opoce/cellar/ccr/s3/read/impl/empty-filetypes-skos.rdf");
    private Path completeSkos = Paths.get("src/test/resources/eu/europa/ec/opoce/cellar/ccr/s3/read/impl/complete-filetypes-skos.rdf");
    private Path missingInformationSkos = Paths
            .get("src/test/resources/eu/europa/ec/opoce/cellar/ccr/s3/read/impl/missing-info-filetypes-skos.rdf");
    private Path oneConceptAllRequiredFieldInfoSkos = Paths
            .get("src/test/resources/eu/europa/ec/opoce/cellar/ccr/s3/read/impl/single-concept-one-each-mandatory-filetypes-skos.rdf");
    private Path oneConceptOneDuplicateSkos = Paths
            .get("src/test/resources/eu/europa/ec/opoce/cellar/ccr/s3/read/impl/single-concept-one-duplicate-code-filetypes-skos.rdf");
    private Path oneConceptTwoDuplicateSkos = Paths
            .get("src/test/resources/eu/europa/ec/opoce/cellar/ccr/s3/read/impl/single-concept-two-duplicate-code-filetypes-skos.rdf");
    private Path twoConceptWithDifferentPrioritySkos = Paths
            .get("src/test/resources/eu/europa/ec/opoce/cellar/ccr/s3/read/impl/two-concept-different-priority-filetypes-skos.rdf");
    private Path twoConceptFourExtensionsSkos = Paths
            .get("src/test/resources/eu/europa/ec/opoce/cellar/ccr/s3/read/impl/two-concept-four-extensions-filetypes-skos.rdf");
    private FileTypesNalSkosLoaderServiceImpl nalService;

    @Before
    public void setUp() throws Exception {
        nalService = new NoReloadFileTypesNalSkosLoaderServiceImpl();
        nalService.resetInMemoryMaps();
    }

    static class NoReloadFileTypesNalSkosLoaderServiceImpl extends FileTypesNalSkosLoaderServiceImpl {
        @Override
        protected void reloadIfNecessary() {
        }
    }

    @Test
    public void emptySkosModelResultsInEmptyItemInNalService() throws Exception {
        Model model = JenaUtils.read(new String(Files.readAllBytes(emptySkos)));
        nalService.iterateThroughSkosConcepts(model);
        assertEquals(0, nalService.numberOfItemDescriptionsLoaded());
    }

    @Test
    public void conceptWithMissingManifestationTypeOrFileExtensionOrMimeTypeIsIgnored() throws Exception {
        Model model = JenaUtils.read(new String(Files.readAllBytes(missingInformationSkos)));
        nalService.iterateThroughSkosConcepts(model);
        assertEquals(0, nalService.numberOfItemDescriptionsLoaded());
    }

    @Test
    public void conceptWithOneOfEachMandatoryFieldIsAdded() throws Exception {
        Model model = JenaUtils.read(new String(Files.readAllBytes(oneConceptAllRequiredFieldInfoSkos)));
        nalService.iterateThroughSkosConcepts(model);
        assertEquals(1, nalService.numberOfItemDescriptionsLoaded());

        Set<String> extensions = nalService.getFileExtensionMatchingMimetype("application/sparql-query");
        assertEquals(1, extensions.size());

        List<MimeTypeMapping> mimeTypeMappingList = nalService.getMimeTypeMappingByManifestationType("sparqlq");
        assertEquals(1, mimeTypeMappingList.size());

        mimeTypeMappingList = nalService.getMimeTypeMappingsByExtension("sparqlq");
        assertEquals(1, mimeTypeMappingList.size());

        mimeTypeMappingList = nalService.getMimeTypeMappingByMimeType("application/sparql-query");
        assertEquals(1, mimeTypeMappingList.size());
    }

    @Test
    public void conceptWithOneDuplicate() throws Exception {
        Model model = JenaUtils.read(new String(Files.readAllBytes(oneConceptOneDuplicateSkos)));
        nalService.iterateThroughSkosConcepts(model);
        assertEquals(1, nalService.numberOfItemDescriptionsLoaded());

        Set<String> extensions = nalService.getFileExtensionMatchingMimetype("application/sparql-query");
        assertEquals(1, extensions.size());

        List<MimeTypeMapping> mimeTypeMappingList = nalService.getMimeTypeMappingByManifestationType("sparqlq");
        assertEquals(2, mimeTypeMappingList.size());

        mimeTypeMappingList = nalService.getMimeTypeMappingsByExtension("sparqlq");
        assertEquals(1, mimeTypeMappingList.size());

        mimeTypeMappingList = nalService.getMimeTypeMappingsByExtension("rq");
        assertEquals(1, mimeTypeMappingList.size());

        mimeTypeMappingList = nalService.getMimeTypeMappingByMimeType("application/sparql-query");
        assertEquals(2, mimeTypeMappingList.size());
    }

    @Test
    public void conceptWithTwoDuplicate() throws Exception {
        Model model = JenaUtils.read(new String(Files.readAllBytes(oneConceptTwoDuplicateSkos)));
        nalService.iterateThroughSkosConcepts(model);
        assertEquals(1, nalService.numberOfItemDescriptionsLoaded());

        Set<String> extensions = nalService.getFileExtensionMatchingMimetype("image/tiff");
        assertEquals(1, extensions.size());

        extensions = nalService.getFileExtensionMatchingMimetype("image/tiff-fx");
        assertEquals(1, extensions.size());

        List<MimeTypeMapping> mimeTypeMappingList = nalService.getMimeTypeMappingByManifestationType("tiff");
        assertEquals(4, mimeTypeMappingList.size());

        mimeTypeMappingList = nalService.getMimeTypeMappingsByExtension("tiff");
        assertEquals(2, mimeTypeMappingList.size());

        mimeTypeMappingList = nalService.getMimeTypeMappingsByExtension("tif");
        assertEquals(2, mimeTypeMappingList.size());

        mimeTypeMappingList = nalService.getMimeTypeMappingByMimeType("image/tiff");
        assertEquals(2, mimeTypeMappingList.size());

        mimeTypeMappingList = nalService.getMimeTypeMappingByMimeType("image/tiff-fx");
        assertEquals(2, mimeTypeMappingList.size());
    }

    @Test
    public void twoConceptWithDifferentPriorityAreSortedDesc() throws Exception {
        Model model = JenaUtils.read(new String(Files.readAllBytes(twoConceptWithDifferentPrioritySkos)));
        nalService.iterateThroughSkosConcepts(model);
        assertEquals("2 should be loaded", 2, nalService.numberOfItemDescriptionsLoaded());

        Set<MimeTypeMapping> descOrderedMimeTypeMapping = nalService.getMimeTypeMappingsByPriorityDesc();
        assertEquals(6, descOrderedMimeTypeMapping.size());
        List<Long> priorityList = descOrderedMimeTypeMapping.stream().map(MimeTypeMapping::getPriority).collect(Collectors.toList());
        List<Long> descPriorityList = new ArrayList<>(priorityList);
        descPriorityList.sort(Collections.reverseOrder());
        assertEquals(priorityList, descPriorityList);
    }

    @Test
    public void testRetrievalOfExtensions() throws Exception {
        Model model = JenaUtils.read(new String(Files.readAllBytes(twoConceptFourExtensionsSkos)));
        nalService.iterateThroughSkosConcepts(model);
        assertEquals("2 should be loaded", 2, nalService.numberOfItemDescriptionsLoaded());

        Set<String> extensions = nalService.getAllPossibleExtensions();
        assertEquals(4, extensions.size());

        Set<MimeTypeMapping> mimeTypeMappings = nalService.getMimeTypeMappingsByPriorityDesc();
        assertEquals(6, mimeTypeMappings.size());
    }

    @Test
    public void completeFileTypeParsing() throws Exception {
        Model model = JenaUtils.read(new String(Files.readAllBytes(completeSkos)));
        nalService.iterateThroughSkosConcepts(model);
        assertEquals("34 concepts should be loaded since the file has one manifestation type, at least one mime type and at least one file extension",
                     34,
                     nalService.numberOfItemDescriptionsLoaded());

        Set<MimeTypeMapping> prio = nalService.getMimeTypeMappingsByPriorityDesc();
        assertEquals("54 unique different mime type mapping are found in the complete file",
                     54,
                     nalService.getMimeTypeMappingsByPriorityDesc().size());

        assertEquals("35 unique extension are found in the complete file",
                     35,
                     nalService.getAllPossibleExtensions().size());
    }
}
