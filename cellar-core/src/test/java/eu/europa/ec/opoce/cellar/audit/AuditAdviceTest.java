/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.audit
 *             FILE : AuditAdviceTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 02 08, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-02-08 11:01:59 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.audit;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventAction;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditTrailEventProcess;
import eu.europa.ec.opoce.cellar.cl.domain.audit.Auditable;
import eu.europa.ec.opoce.cellar.cl.domain.audit.AuditableAdvice;
import eu.europa.ec.opoce.cellar.cl.service.client.AuditTrailEventService;
import eu.europa.ec.opoce.cellar.cl.service.impl.AuditTrailEventServiceImpl;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import org.apache.logging.log4j.ThreadContext;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.UUID;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * @author ARHS Developments
 */
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PrepareForTest(ServiceLocator.class)
@PowerMockIgnore({"org.w3c.*", "org.xml.sax.*", "javax.xml.*", "javax.management.*"})
@ContextConfiguration(classes = AuditAdviceTest.AuditAdviceTestConfiguration.class)
public class AuditAdviceTest {

    @Autowired
    private AuditTrailEventService auditTrailEventService;

    @Autowired
    private A a;

    @Before
    public void init() {
        PowerMockito.mockStatic(ServiceLocator.class);
        when(ServiceLocator.getService(AuditTrailEventService.class)).thenReturn(auditTrailEventService);
    }

    @Test
    public void processIdMustBeDefinedByThreadContext() {
        String uuid = UUID.randomUUID().toString();
        ThreadContext.put(AuditableAdvice.AUDIT_KEY, uuid);
        PowerMockito.doAnswer(invocation -> {
            String uuidSaveDB = (String) invocation.getArguments()[5];
            Assert.assertEquals(uuid, uuidSaveDB);
            return null;
        }).when(auditTrailEventService).saveEvent(any(), any(), any(), anyString(), anyString(), anyString(),any(), any());

        a.a();
    }

    @Configuration
    @EnableAspectJAutoProxy
    public static class AuditAdviceTestConfiguration {

        @Bean
        AuditableAdvice auditableAdvice() {
            return new AuditableAdvice();
        }

        @Bean
        AuditTrailEventService auditTrailEventService() {
            AuditTrailEventService auditTrailEventService = new AuditTrailEventServiceImpl(null, cellarConfiguration());
            return PowerMockito.spy(auditTrailEventService);
        }

        ICellarConfiguration cellarConfiguration() {
            ICellarConfiguration cfg = PowerMockito.mock(ICellarConfiguration.class);
            PowerMockito.when(cfg.isCellarDatabaseAuditEnabled()).thenReturn(true);
            return cfg;
        }

        @Bean
        public A a() {
            return new A();
        }
    }
}

class A {
    @Auditable(process = AuditTrailEventProcess.Ingestion,
            action = AuditTrailEventAction.Create)
    public void a() {
    }
}
