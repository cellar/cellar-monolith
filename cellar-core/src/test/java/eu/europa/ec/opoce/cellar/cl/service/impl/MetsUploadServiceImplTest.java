package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.ccr.enums.status.StatusType;
import eu.europa.ec.opoce.cellar.ccr.mets.response.MetsResponse;
import eu.europa.ec.opoce.cellar.ccr.service.client.StatusService;
import eu.europa.ec.opoce.cellar.ccr.service.impl.MetsUploadServiceImpl;
import eu.europa.ec.opoce.cellar.ccr.utils.MetsUploadUtils;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.enums.ProcessStatus;
import eu.europa.ec.opoce.cellar.cl.domain.package_history.PackageHistory;
import eu.europa.ec.opoce.cellar.cl.service.client.FileService;
import eu.europa.ec.opoce.cellar.cl.service.client.PackageHistoryService;
import eu.europa.ec.opoce.cellar.exception.MetsUploadException;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork;
import eu.europa.ec.opoce.cellar.semantic.helper.DateFormats;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * @author EUROPEAN DYNAMICS S.A.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(MetsUploadUtils.class)
@PowerMockIgnore({"javax.management.*", "javax.script.*"})
public class MetsUploadServiceImplTest {

    @Mock
    private FileService fileService;

    @Mock
    private StatusService statusService;

    @Mock
    private PackageHistoryService packageHistoryService;

    @Mock
    private ICellarConfiguration cellarConfiguration;

    @Mock
    private RestTemplate restTemplate;

    private MetsUploadServiceImpl metsUploadService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        metsUploadService=new MetsUploadServiceImpl(fileService,statusService,packageHistoryService,cellarConfiguration);
        metsUploadService.setRestTemplate(restTemplate);

        // Mock the response entity
        ResponseEntity<Void> responseEntityVoid = new ResponseEntity<>(HttpStatus.OK);
        ResponseEntity<String> responseEntity = new ResponseEntity<>("{\"key\": \"value\"}", HttpStatus.OK);

        // Mock the restTemplate behavior
        when(restTemplate.exchange(anyString(), eq(HttpMethod.POST), any(HttpEntity.class), eq(Void.class)))
                .thenReturn(responseEntityVoid);
        when(restTemplate.getForEntity(anyString(), eq(String.class)))
                .thenReturn(responseEntity);
    }

    @Test
    public void testMetsUpload_Success() throws Exception {
        // Mock the multipart file
        byte[] testData = "Test data content".getBytes();
        MultipartFile fileData = new MockMultipartFile("test.zip", testData);

        // Mock the SIPWork.TYPE priority
        String priorityStr = "DAILY";

        // Mock the resolved mets ID
        String metsID = "mets123";

        // Mock the moved file
        File movedFile = new File("path/to/movedFile.zip");

        // Mock the PackageHistory
        String callbackURI = "http://example.com/callback";
        PackageHistory packageHistory = new PackageHistory();
        packageHistory.setReceivedDate(new Date());
        packageHistory.setUuid("123456");

        // Mock the fileService method calls
        when(fileService.moveSIPToReceptionFolderByPriority(any(File.class), anyString(), any(SIPWork.TYPE.class)))
                .thenReturn(movedFile);

        when(cellarConfiguration.getCellarMetsUploadBufferSize())
                .thenReturn(4096);
        // Mock the MetsUploadUtils method call and directly set the metsID without invoking the method
        PowerMockito.mockStatic(MetsUploadUtils.class);
        when(MetsUploadUtils.resolveMetsId(any(File.class))).thenReturn(metsID);

        // Mock the createPackageHistoryDb method
        when(metsUploadService.createPackageHistoryDb(movedFile, callbackURI)).thenReturn(packageHistory);

        // Perform the test
        MetsResponse response = metsUploadService.metsUpload(fileData, priorityStr, callbackURI);

        // Verify the expected behavior
        assertEquals(DateFormats.formatXmlDateTime(packageHistory.getReceivedDate()), response.getReceived());
        assertEquals(statusService.getCellarProxiedStatusServicePackageLevelEndpointWithUuid(packageHistory.getUuid()), response.getStatus_endpoint());
    }

    @Test(expected = MetsUploadException.class)
    public void testMetsUpload_Exception() throws Exception {
        // Mock the multipart file
        byte[] testData = "Test data content".getBytes();
        MultipartFile fileData = new MockMultipartFile("test.zip", testData);

        // Mock the SIPWork.TYPE priority
        String priorityStr = "DAILY";

        // Mock the moved file
        File movedFile = new File("path/to/movedFile.zip");
        when(cellarConfiguration.getCellarMetsUploadBufferSize())
                .thenReturn(4096);
        // Mock the fileService method calls to throw an exception
        when(fileService.moveSIPToReceptionFolderByPriority(any(File.class), anyString(), any(SIPWork.TYPE.class)))
                .thenThrow(new IOException());

        // Perform the test, expecting an exception to be thrown
        metsUploadService.metsUpload(fileData, priorityStr, "http://example.com/callback");
    }

    @Test
    public void testGetStatusResponseAndSendCallbackRequest_CallbackUrlDefined() {
        // Mock the PackageHistory
        PackageHistory packageHistory = new PackageHistory();
        packageHistory.setCallbackUrl("http://example.com/callback");
        packageHistory.setUuid("123456");

        // Mock the status response
        String statusResponse = "Status response";

        // Mock the statusService method call
        when(statusService.getStatusResponse(anyString(), anyBoolean(), anyBoolean(), any(), anyInt(), any(StatusType.class)))
                .thenReturn(statusResponse);

        // Perform the test
        metsUploadService.getStatusResponseAndSendCallbackRequest(new File("sipFile"), false, packageHistory);

        // Verify that the status response was received
        verify(statusService).getStatusResponse(eq(packageHistory.getUuid()), eq(true),
               eq(false), eq(null), eq(1), any(StatusType.class));
        // Verify that the callback request was sent
        verify(restTemplate).exchange(eq(packageHistory.getCallbackUrl()), eq(HttpMethod.POST), any(HttpEntity.class), eq(Void.class));
    }

    @Test
    public void testGetStatusResponseAndSendCallbackRequest_CallbackUrlNotDefined() {
        // Mock the PackageHistory without callback URL
        PackageHistory packageHistory = new PackageHistory();
        packageHistory.setUuid("123456");

        // Perform the test
        metsUploadService.getStatusResponseAndSendCallbackRequest(new File("sipFile"), false, packageHistory);

        // Verify that the status response was not received
        verify(statusService,never()).getStatusResponse(eq(packageHistory.getUuid()), eq(true),
                eq(false), eq(null), eq(1), any(StatusType.class));
        // Verify that no callback request was sent
        verify(restTemplate, never()).exchange(anyString(), eq(HttpMethod.POST), any(HttpEntity.class), eq(Void.class));
    }

    @Test
    public void testCreatePackageHistoryDb() {
        // Mock the SIP file and callback URL
        File sipFile = new File("path/to/sipFile.zip");
        String callbackURL = "http://example.com/callback";

        // Perform the test
        PackageHistory packageHistory = metsUploadService.createPackageHistoryDb(sipFile, callbackURL);

        // Verify the created PackageHistory object
        assertEquals(sipFile.getName(), packageHistory.getPackageName());
        assertEquals(callbackURL, packageHistory.getCallbackUrl());
        assertEquals(ProcessStatus.NA, packageHistory.getPreprocessIngestionStatus());
    }
}
