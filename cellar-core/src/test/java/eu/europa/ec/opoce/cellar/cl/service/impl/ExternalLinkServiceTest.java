/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.service.impl
 *             FILE : ExternalLinkServiceTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 17, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.cl.dao.ExternalLinkPatternDao;
import eu.europa.ec.opoce.cellar.cl.domain.externalLink.ExternalLink;
import eu.europa.ec.opoce.cellar.cl.domain.externalLink.ExternalLinkPattern;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * <class_description> External link generation manager test. 
 * 
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 17, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class ExternalLinkServiceTest {

    private final static String DOCUMENT_REFERENCE = "ESC/1999/1118/";
    private final static String LANG = "en";
    private final static String TARGET_URL = "http://eescopinions.eesc.europa.eu/eescopiniondocument.aspx?language=en&docnr=1118&year=1999";

    private final static List<ExternalLinkPattern> EXTERNAL_LINK_PATTERNS;

    private ExternalLinkServiceImpl externalLinkService;

    private ExternalLinkPatternDao externalLinkPatternDaoMock;

    private ExternalLinkPatternCacheServiceImpl externalLinkPatternCacheService;

    static {
        EXTERNAL_LINK_PATTERNS = new ArrayList<ExternalLinkPattern>(4);
        EXTERNAL_LINK_PATTERNS.add(new ExternalLinkPattern(new Long(1), "ESC/([0-9]{4})/([0-9]{1,4})/",
                "http://eescopinions.eesc.europa.eu/eescopiniondocument.aspx?language=${return lang;}$&docnr=${return idData[1];}$&year=${return idData[0];}$"));
        EXTERNAL_LINK_PATTERNS.add(new ExternalLinkPattern(new Long(2), "Opinion COR/([0-9]{4})/([0-9]{1,4})/",
                "http://coropinions.cor.europa.eu/coropiniondocument.aspx?language=${return lang;}$&docnr=${return idData[1]}$&year=${return idData[0]}$"));
        EXTERNAL_LINK_PATTERNS.add(new ExternalLinkPattern(new Long(3), "PRES/([0-9]{4})/([0-9]{1,4})/",
                "http://europa.eu/rapid/pressReleasesAction.do?reference=PRES/${return idData[0].substring(2);}$/${return idData[1]}$&format=HTML&aged=0&lg=${return lang;}$&guiLanguage=${return lang;}$"));
    }

    /**
     * Set up of the test.
     */
    @Before
    public void setUp() {
        this.externalLinkPatternDaoMock = EasyMock.createMock(ExternalLinkPatternDao.class);
        this.externalLinkPatternCacheService = new ExternalLinkPatternCacheServiceImpl();
        this.externalLinkPatternCacheService.setExternalLinkPatternDao(this.externalLinkPatternDaoMock);
        this.externalLinkService = new ExternalLinkServiceImpl();
        this.externalLinkService.setExternalLinkPatternCacheService(this.externalLinkPatternCacheService);
    }

    /**
     * Test the generation of external link.
     */
    @Test
    public void convertTest() {
        EasyMock.expect(this.externalLinkPatternDaoMock.getExternalLinkPatterns()).andReturn(EXTERNAL_LINK_PATTERNS).times(1);
        EasyMock.replay(this.externalLinkPatternDaoMock);

        final ExternalLink externalLink = this.externalLinkService.convert(DOCUMENT_REFERENCE, LANG);

        EasyMock.verify(this.externalLinkPatternDaoMock);
        Assert.assertEquals(TARGET_URL, externalLink.getUrl());
    }
}
