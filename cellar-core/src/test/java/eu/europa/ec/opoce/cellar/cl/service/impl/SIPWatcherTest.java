package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.enums.CellarServiceIngestionPickupMode;
import eu.europa.ec.opoce.cellar.cl.configuration.impl.CellarPersistentConfiguration;
import eu.europa.ec.opoce.cellar.cl.service.client.AuditTrailEventService;
import eu.europa.ec.opoce.cellar.cl.service.client.FileService;
import eu.europa.ec.opoce.cellar.cl.service.client.SIPDependencyChecker;
import eu.europa.ec.opoce.cellar.cl.service.client.SIPDependencyCheckerStorageService;
import eu.europa.ec.opoce.cellar.cl.service.client.SIPQueueManager;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import eu.europa.ec.opoce.cellar.common.concurrent.ListeningExecutorService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.Mockito.*;

import java.util.concurrent.locks.ReentrantLock;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ServiceLocator.class)
@PowerMockIgnore({"javax.management.*", "javax.script.*"})
public class SIPWatcherTest {

    private CellarPersistentConfiguration cellarConfiguration;
    private SIPQueueManager sipQueueManager;
    private SIPWatcherImpl service;
    private FileService fileService;
    private SIPQueueManagerProxyImpl sipQueueManagerProxyImpl;

    @Before
    public void setup() {
    	PowerMockito.mockStatic(ServiceLocator.class);
        SIPDependencyChecker sipDependencyCheckerImpl = mock(SIPDependencyChecker.class);
        cellarConfiguration= mock(CellarPersistentConfiguration.class);
        sipQueueManager = mock(SIPQueueManager.class);
        fileService = mock(FileService.class);
        ListeningExecutorService priorityThreadPoolExecutor = mock(ListeningExecutorService.class);
        SIPDependencyCheckerStorageService sipDependencyCheckerStorageServiceImpl = mock(SIPDependencyCheckerStorageService.class);
        sipQueueManagerProxyImpl = mock(SIPQueueManagerProxyImpl.class);
        service = new SIPWatcherImpl(cellarConfiguration, sipDependencyCheckerImpl,
                sipDependencyCheckerStorageServiceImpl, sipQueueManager, sipQueueManagerProxyImpl, fileService, priorityThreadPoolExecutor);
    }

    @Test
    public void watchFoldersTest() {
        String tmpdir = "src/test/resources/eu/europa/ec/opoce/cellar/cl/service/impl";
        when(cellarConfiguration.isCellarServiceSipDependencyCheckerEnabled()).thenReturn(true);
        when(cellarConfiguration.getCellarFolderAuthenticOJReception()).thenReturn(tmpdir);
        when(cellarConfiguration.getCellarFolderBulkReception()).thenReturn(tmpdir);
        when(cellarConfiguration.getCellarFolderDailyReception()).thenReturn(tmpdir);
        when(cellarConfiguration.getCellarFolderBulkLowPriorityReception()).thenReturn(tmpdir);
        when(cellarConfiguration.getCellarServiceIngestionPickupMode()).thenReturn(CellarServiceIngestionPickupMode.none);
        when(cellarConfiguration.getCellarServiceSipQueueManagerFileAttributeDateType()).thenReturn("unix:ctime");
        when(sipQueueManager.getSipQueueLock()).thenReturn(new ReentrantLock(true));
        when(ServiceLocator.getService(AuditTrailEventService.class)).thenReturn(mock(AuditTrailEventService.class));
        service.initialize();
        service.reconfigureThreadPoolExecutor();
        service.watchFolders();

        verify(cellarConfiguration, times(2)).getCellarFolderAuthenticOJReception();
        verify(cellarConfiguration, times(2)).getCellarFolderBulkReception();
        verify(cellarConfiguration, times(2)).getCellarFolderDailyReception();
        verify(cellarConfiguration, times(2)).getCellarFolderBulkLowPriorityReception();
        verify(cellarConfiguration, times(2)).getCellarServiceIngestionPickupMode();
    }

}
