/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.licenseHolder
 *             FILE : ArchiveTreeManagerTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Dec 9, 2012
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.cl.domain.licenseHolder;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;

/**
 * <class_description> Test cases for the class {@link ArchiveTreeManager}. In particularly, we test the path generation.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : Dec 9, 2012
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class ArchiveTreeManagerTest {

    /**
     * Test folder.
     */
    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder(); // must be public

    /**
     * Test the generation of the root path (no max folder size).
     * @throws IOException
     */
    @Test
    public void getFolderPathTestRoot() throws IOException {
        final File atmFolder = this.temporaryFolder.newFolder("atmTestRoot");
        final String atmFolderPath = atmFolder.getAbsolutePath();

        final ArchiveTreeManager atm = new ArchiveTreeManager(0, new Long(100), atmFolderPath);

        Assert.assertEquals(atmFolderPath, atm.getFolderPath());
        Assert.assertEquals(atmFolderPath, atm.getFolderPath());
    }

    /**
     * Test the generation of folders with 1 level.
     * @throws IOException
     */
    @Test
    public void getFolderPathTestTree1Level() throws IOException {
        final File atmFolder = this.temporaryFolder.newFolder("atmTestTree1Level");
        final String atmFolderPath = atmFolder.getAbsolutePath();

        final ArchiveTreeManager atm = new ArchiveTreeManager(50, new Long(80), atmFolderPath);

        for (int i = 0; i < 50; i++)
            Assert.assertEquals(atmFolderPath + "/0-49", atm.getFolderPath());

        File folder = new File(atmFolderPath + "/0-49");
        Assert.assertTrue(folder.exists() && folder.isDirectory());

        for (int i = 50; i < 80; i++)
            Assert.assertEquals(atmFolderPath + "/50-99", atm.getFolderPath());

        folder = new File(atmFolderPath + "/50-99");
        Assert.assertTrue(folder.exists() && folder.isDirectory());
    }

    /**
     * Test the generation of folders with 2 levels.
     * @throws IOException
     */
    @Test
    public void getFolderPathTestTree2Levels() throws IOException {
        final File atmFolder = this.temporaryFolder.newFolder("atmTestTree2Levels");
        final String atmFolderPath = atmFolder.getAbsolutePath();

        final ArchiveTreeManager atm = new ArchiveTreeManager(4, new Long(20), atmFolderPath);

        for (int i = 0; i < 4; i++)
            Assert.assertEquals(atmFolderPath + "/0-15/0-3", atm.getFolderPath());

        File folder = new File(atmFolderPath + "/0-15/0-3");
        Assert.assertTrue(folder.exists() && folder.isDirectory());

        for (int i = 4; i < 8; i++)
            Assert.assertEquals(atmFolderPath + "/0-15/4-7", atm.getFolderPath());

        folder = new File(atmFolderPath + "/0-15/4-7");
        Assert.assertTrue(folder.exists() && folder.isDirectory());

        for (int i = 8; i < 12; i++)
            Assert.assertEquals(atmFolderPath + "/0-15/8-11", atm.getFolderPath());

        folder = new File(atmFolderPath + "/0-15/8-11");
        Assert.assertTrue(folder.exists() && folder.isDirectory());

        for (int i = 12; i < 16; i++)
            Assert.assertEquals(atmFolderPath + "/0-15/12-15", atm.getFolderPath());

        folder = new File(atmFolderPath + "/0-15/12-15");
        Assert.assertTrue(folder.exists() && folder.isDirectory());

        for (int i = 16; i < 20; i++)
            Assert.assertEquals(atmFolderPath + "/16-31/16-19", atm.getFolderPath());

        folder = new File(atmFolderPath + "/16-31/16-19");
        Assert.assertTrue(folder.exists() && folder.isDirectory());

    }
}
