package eu.europa.ec.opoce.cellar.atomikos;

import com.atomikos.icatch.jta.UserTransactionImp;
import com.atomikos.icatch.jta.UserTransactionManager;
import com.atomikos.jdbc.AtomikosDataSourceBean;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.dao.GroupDao;
import eu.europa.ec.opoce.cellar.cl.dao.RoleDao;
import eu.europa.ec.opoce.cellar.cl.dao.impl.GroupDaoImpl;
import eu.europa.ec.opoce.cellar.cl.dao.impl.RoleDaoImpl;
import eu.europa.ec.opoce.cellar.cl.domain.admin.Group;
import eu.europa.ec.opoce.cellar.cl.domain.admin.Role;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import oracle.jdbc.xa.client.OracleXADataSource;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.jta.JtaTransactionManager;

import javax.sql.DataSource;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;

/**
 * This test is ignore by default. It must be moved into a real integration
 * test when CELLAR tests will be improved.
 *
 * @author ARHS Developments
 */
@Ignore
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PrepareForTest(ServiceLocator.class)
@PowerMockIgnore({"org.w3c.*", "org.xml.sax.*", "javax.xml.*", "javax.management.*"})
@ContextConfiguration(classes = AtomikosOracleTest.AtomikosTestConfiguration.class)
public class AtomikosOracleTest {

    private static final int DEFAULT_TRANSACTION_TIMEOUT = 2;

    @Autowired
    private TransactionalRoleService transactionalRoleService;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private GroupDao groupDao;

    @Test
    public void transactionMustRollbackOnException() {
        int size = roleDao.findAll().size();
        try {
            transactionalRoleService.rollbackOnException();
        } catch (RuntimeException e) {
        }
        Assert.assertEquals(size, roleDao.findAll().size());
    }

    @Test
    public void transactionMustRollbackNestedExceptionOnException() {
        int size = groupDao.findAll().size();
        try {
            transactionalRoleService.rollbackNestedCall();
        } catch (RuntimeException e) {
            Assert.assertEquals(size, groupDao.findAll().size());
        }
    }

    @Test
    public void transactionMustRollbackOnTimeoutAtomikos() {
        int size = roleDao.findAll().size();
        try {
            transactionalRoleService.rollbackOnTimeout();
        } catch (Exception e) {
        }
        Assert.assertEquals(size, roleDao.findAll().size());
    }

    @Configuration
    @EnableTransactionManagement
    @PropertySource("cellar-test.properties")
    @EnableAspectJAutoProxy(proxyTargetClass = true)
    static class AtomikosTestConfiguration {

        @Value("${cellar.oracle.url:undefined}")
        private String url;

        @Value("${cellar.oracle.user:undefined}")
        private String user;

        @Value("${cellar.oracle.password:undefined}")
        private String password;

        @Bean
        public PlatformTransactionManager transactionManager() throws SystemException {
            UserTransactionManager userTransactionManager = new UserTransactionManager();
            userTransactionManager.setTransactionTimeout(DEFAULT_TRANSACTION_TIMEOUT);
            userTransactionManager.setForceShutdown(false);

            UserTransaction userTransactionImp = new UserTransactionImp();
            userTransactionImp.setTransactionTimeout(DEFAULT_TRANSACTION_TIMEOUT);

            return new JtaTransactionManager(userTransactionImp, userTransactionManager);
        }

        @Bean
        public LocalSessionFactoryBean cellarSessionFactory() throws SQLException, SystemException {
            LocalSessionFactoryBean localSessionFactoryBean = new LocalSessionFactoryBean();
            localSessionFactoryBean.setDataSource(cellarDataSource());
            localSessionFactoryBean.setJtaTransactionManager(transactionManager());
            localSessionFactoryBean.setConfigLocation(new ClassPathResource("hibernate.cfg.xml"));
            return localSessionFactoryBean;
        }

        @Bean
        @Primary
        public DataSource cellarDataSource() throws SQLException {
            OracleXADataSource dataSource = new OracleXADataSource();
            dataSource.setURL(url);
            dataSource.setUser(user);
            dataSource.setPassword(password);
            AtomikosDataSourceBean xaDataSource = new AtomikosDataSourceBean();
            xaDataSource.setXaDataSource(dataSource);
            xaDataSource.setUniqueResourceName("cellarDatasource");
            return xaDataSource;
        }

        @Bean
        public TransactionalRoleService transactionalAtomikosService() {
            return new TransactionalRoleService(roleDao(), transactionalGroupsService());
        }

        @Bean
        public RoleDao roleDao() {
            return new RoleDaoImpl();
        }

        @Bean
        public TransactionalGroupsService transactionalGroupsService() {
            return new TransactionalGroupsService(groupDao());
        }

        @Bean
        public GroupDao groupDao() {
            return new GroupDaoImpl();
        }

        @Bean
        public ICellarConfiguration cellarStaticConfiguration() {
            return Mockito.mock(ICellarConfiguration.class);
        }

        @Bean
        public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
            return new PropertySourcesPlaceholderConfigurer();
        }

    }

    static class TransactionalRoleService {

        private final RoleDao roleDao;
        private final TransactionalGroupsService transactionalGroupsService;

        TransactionalRoleService(RoleDao roleDao, TransactionalGroupsService transactionalGroupsService) {
            this.roleDao = roleDao;
            this.transactionalGroupsService = transactionalGroupsService;
        }

        @Transactional
        public void rollbackOnException() {
            addRole(1);
            addRole(2);
            addRole(3);
            throw new RuntimeException();
        }

        @Transactional
        public void rollbackOnTimeout() throws InterruptedException {
            addRole(1);
            for (int i = 0; i < 5; i++) {
                TimeUnit.SECONDS.sleep(1);
            }
            addRole(2);
        }

        @Transactional
        public void rollbackNestedCall() {
            transactionalGroupsService.addGroup();
            throw new RuntimeException();
        }

        private void addRole(int id) {
            Role role = new Role();
            role.setRoleName("TEST-TMP-" + id);
            role.setRoleAccess("TEST-TMP-ACCESS-" + id);
            roleDao.saveObject(role);
        }
    }

    static class TransactionalGroupsService {

        private final GroupDao groupDao;

        TransactionalGroupsService(GroupDao groupDao) {
            this.groupDao = groupDao;
        }

        @Transactional
        public void addGroup() {
            Group group = new Group();
            group.setGroupName("TMP");
            groupDao.saveObject(group);
        }

    }
}
