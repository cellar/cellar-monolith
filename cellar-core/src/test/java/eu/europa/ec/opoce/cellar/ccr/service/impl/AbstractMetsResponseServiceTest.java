/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ccr.service.impl
 *             FILE : AbstractMetsResponseServiceTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 11 06, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-11-06 15:55:12 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.service.impl;

import eu.europa.ec.opoce.cellar.ccr.mets.service.MetsMarshallerImpl;
import eu.europa.ec.opoce.cellar.ccr.velocity.VelocityMarshaller;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.impl.CellarStaticConfiguration;
import eu.europa.ec.opoce.cellar.cl.dao.StructMapResponseDao;
import eu.europa.ec.opoce.cellar.cl.dao.impl.StructMapAuthenticOJResponseDao;
import eu.europa.ec.opoce.cellar.cl.domain.content.SIPResource;
import eu.europa.ec.opoce.cellar.cl.domain.validator.SystemValidator;
import eu.europa.ec.opoce.cellar.cl.domain.validator.XmlValidator;
import eu.europa.ec.opoce.cellar.cl.service.client.AuditTrailEventService;
import eu.europa.ec.opoce.cellar.cl.service.client.FileService;
import eu.europa.ec.opoce.cellar.cl.service.client.ValidationService;
import eu.europa.ec.opoce.cellar.cl.service.impl.AuditTrailEventServiceImpl;
import eu.europa.ec.opoce.cellar.cl.service.impl.FileSystemService;
import eu.europa.ec.opoce.cellar.cl.service.impl.ValidationServiceImpl;
import eu.europa.ec.opoce.cellar.domain.content.mets.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Comparator;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author ARHS Developments
 */
public class AbstractMetsResponseServiceTest {
    private MetsDocument metsDocument;
    private SIPResource sipResource;
    private AbstractMetsResponseService metsResponseService;

    private static Path TMP_DIRECTORY;

    @BeforeClass
    public static void oneTimeSetUp() throws Exception {
        TMP_DIRECTORY = Paths.get(System.getProperty("java.io.tmpdir") + File.separator + "cellar_testing_response");
    }

    @Before
    public void setUp() throws Exception {
        recursiveDelete();
        Files.createDirectories(TMP_DIRECTORY);

        metsDocument = new MetsDocument();
        metsDocument.setDocumentId("documentId");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        metsDocument.setCreateDate(simpleDateFormat.parse("2017-11-06"));
        metsDocument.setType(OperationType.Create);
        metsDocument.setProfile(MetsProfile.METS_3);

        sipResource = new SIPResource();
        sipResource.setMetsHdr("metsHeaders");

        FileService fileService = mock(FileSystemService.class);
        when(fileService.getResponseAuthenticOJFolder()).thenReturn(TMP_DIRECTORY.toFile());

        ICellarConfiguration cellarConfiguration = mock(CellarStaticConfiguration.class);
        when(cellarConfiguration.getCellarFolderAuthenticOJResponse()).thenReturn(TMP_DIRECTORY.toAbsolutePath().toString());
        StructMapResponseDao structMapResponseDao = new StructMapAuthenticOJResponseDao(cellarConfiguration);

        VelocityMarshaller velocityMarshaller = new MetsMarshallerImpl();
        SystemValidator systemValidator = new XmlValidator("/xsd/mets/cellar/v2/cellar-mets.xsd");
        AuditTrailEventService auditTrailEventService = mock(AuditTrailEventServiceImpl.class);
        when(auditTrailEventService.getXMLLogs()).thenReturn(new StringBuffer());
        ValidationService validationService = mock(ValidationServiceImpl.class);
        metsResponseService = new MetsAuthenticOJResponseService(fileService, structMapResponseDao, velocityMarshaller, systemValidator, auditTrailEventService, validationService);
    }

    private void recursiveDelete() throws IOException {
        if (Files.exists(TMP_DIRECTORY)) {
            Files.walk(TMP_DIRECTORY).sorted(Comparator.reverseOrder())
                    .map(Path::toFile).forEach(File::delete);
        }
    }

    @Test
    public void metsResponseFolderAreValidationServiceAutomaticallyCreated() throws Exception {
        assertFalse(Files.exists(Paths.get(TMP_DIRECTORY.toString(), "documentId")));
        metsResponseService.loadStructMapResponse(metsDocument, sipResource);
        assertTrue(Files.exists(Paths.get(TMP_DIRECTORY.toString(), "documentId")));
    }

    @Test
    public void testRequiredInfoCopyFromInputToOutputMetsDocument() throws Exception {
        MetsDocument metsDocumentResponse = metsResponseService.loadStructMapResponse(metsDocument, sipResource);
        assertEquals("documentId", metsDocumentResponse.getDocumentId());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        assertEquals("2017-11-06", simpleDateFormat.format(metsDocumentResponse.getCreateDate()));
        assertEquals(OperationType.Create, metsDocumentResponse.getType());
        assertEquals(MetsProfile.METS_3, metsDocumentResponse.getProfile());
        assertEquals("metsHeaders", metsDocumentResponse.getHeader());
    }

    @Test
    public void defaultValueIsUsedInCaseOfEmptyStructMaps() throws Exception {
        MetsDocument metsDocumentResponse = metsResponseService.loadStructMapResponse(metsDocument, sipResource);
        StructMap structMap = metsDocumentResponse.getStructMap("SM-response-documentId");
        assertNotNull(structMap);
        DigitalObject digitalObject = structMap.getDigitalObject();
        assertNotNull(digitalObject);
    }

    @Test
    public void copyValidationErrorForEachStructMap() throws Exception {
        String validationResultFilename = "structMapId.validationResult.json";
        String validationInfoFilename = "structMapId.validationInfo.json";

        StructMap structMap = new StructMap(metsDocument);
        DigitalObject digitalObject = new DigitalObject(structMap);
        structMap.setId("structMapId");
        structMap.setDigitalObject(digitalObject);
        metsDocument.addStructMap(structMap);

        Path validationInfoPath = TMP_DIRECTORY.resolve(validationResultFilename);
        Path validationResultPath = TMP_DIRECTORY.resolve(validationInfoFilename);
        Files.write(validationInfoPath, "validationInfo".getBytes());
        Files.write(validationResultPath, "validationResult".getBytes());

        metsResponseService.loadStructMapResponse(metsDocument, sipResource);

        TMP_DIRECTORY.resolve(metsDocument.getDocumentId()).resolve(validationResultFilename);
        TMP_DIRECTORY.resolve(metsDocument.getDocumentId()).resolve(validationInfoFilename);

        assertTrue(Files.exists(TMP_DIRECTORY.resolve(validationResultFilename)));
        assertTrue(Files.exists(TMP_DIRECTORY.resolve(validationInfoFilename)));
    }
}