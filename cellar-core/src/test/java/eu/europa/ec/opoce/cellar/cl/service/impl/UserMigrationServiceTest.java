package eu.europa.ec.opoce.cellar.cl.service.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;

import eu.europa.ec.opoce.cellar.cl.service.client.SecurityService;
import eu.europa.ec.opoce.cellar.cl.service.client.UserMigrationService;
import eu.europa.ec.opoce.cellar.cl.service.impl.UserMigrationServiceImpl.UserNotFoundException;

/**
 * 
 * @author EUROPEAN DYNAMICS S.A
 */
public class UserMigrationServiceTest {

	/**
	 * 
	 */
	private SecurityService securityService;
	/**
	 * 
	 */
	private UserMigrationService userMigrationService;
	
	@Before
	public void setup() {
		this.securityService = mock(SecurityServiceImpl.class);
		this.userMigrationService = new UserMigrationServiceImpl(null, securityService);
	}
	
	@Test(expected = UserNotFoundException.class)
	public void migrateUserUserNotFound() {
		when(this.securityService.findUserByUsername(any())).thenReturn(null);
		this.userMigrationService.migrateUser(StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY);
	}
	
}
