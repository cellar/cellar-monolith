package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.cl.dao.PackageHasParentPackageDao;
import eu.europa.ec.opoce.cellar.cl.dao.PackageListDao;
import eu.europa.ec.opoce.cellar.cl.dao.SIPPackageDao;
import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.PackageHasParentPackage;
import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.RelationProdId;
import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.SIPPackage;
import eu.europa.ec.opoce.cellar.cl.domain.sip.dependencyChecker.SIPPackageProcessingStatus;
import eu.europa.ec.opoce.cellar.cl.service.client.SIPQueueManagerStorageService;
import eu.europa.ec.opoce.cellar.domain.content.mets.OperationType;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.*;

public class SIPQueueManagerTest {

    private SIPPackageDao sipPackageDao;
    private PackageHasParentPackageDao packageHasParentPackageDao;
	private PackageListDao packageListDao;
    private SIPQueueManagerStorageService sipQueueManagerStorageService;
    private SIPQueueManagerImpl service;

    @Before
    public void setup() {
        sipPackageDao = mock(SIPPackageDao.class);
        packageHasParentPackageDao = mock(PackageHasParentPackageDao.class);
        packageListDao = mock(PackageListDao.class);
        sipQueueManagerStorageService = mock(SIPQueueManagerStorageServiceImpl.class);
        service = new SIPQueueManagerImpl(sipPackageDao, packageHasParentPackageDao, packageListDao, sipQueueManagerStorageService);
    }

    @Test
    public void prioritizePackagesOnInsertTest() {
    	List<SIPPackage> packages = createSIPPackages();
    	List<PackageHasParentPackage> queueEntries = createQueueEntriesForPrioritizePackagesOnInsertTest();
        when(this.packageHasParentPackageDao.findAll()).thenReturn(queueEntries);
        when(this.sipPackageDao.countBySipStatusIn(Arrays.asList(SIPPackageProcessingStatus.NEW))).thenReturn(1L);
        when(sipPackageDao.findAll()).thenReturn(packages);
        when(sipPackageDao.countBySipStatusIn(any())).thenReturn((long) 1.0);
        service.prioritizePackagesOnInsert();

        verify(sipPackageDao).countBySipStatusIn(any());
        verify(sipPackageDao).findAll();
        verify(sipQueueManagerStorageService).updateQueueOnInsert(any(), any());
        verify(sipPackageDao).updateObject(anyObject());
    }

    @Test
    public void prioritizePackagesOnRemovalTest() {
        List<SIPPackage> packages = createSIPPackages();
        List<PackageHasParentPackage> queueEntries = createQueueEntriesForPrioritizePackagesOnRemovalTest();
        when(sipQueueManagerStorageService.findBySipIdOrParentSipIdIn(any())).thenReturn(queueEntries);
        service.prioritizePackagesOnRemoval(packages);

        verify(packageHasParentPackageDao, times(2)).findAll();
        verify(sipQueueManagerStorageService).updateQueueOnRemoval(any(), any());
    }

    private List<SIPPackage> createSIPPackages() {
        SIPPackage sipPackage1 = new SIPPackage();
        sipPackage1.setId(1L);
        sipPackage1.setName("Test1");
        sipPackage1.setType(SIPWork.TYPE.AUTHENTICOJ);
        sipPackage1.setDetectionDate(new Date());
        sipPackage1.setOperationType(OperationType.Create);
        sipPackage1.setStatus(SIPPackageProcessingStatus.NEW);
        sipPackage1.setRelatedProdIds(Collections.singleton(new RelationProdId()));

        SIPPackage sipPackage2 = new SIPPackage();
        sipPackage2.setId(2L);
        sipPackage2.setName("Test2");
        sipPackage2.setType(SIPWork.TYPE.AUTHENTICOJ);
        sipPackage2.setDetectionDate(new Date());
        sipPackage2.setOperationType(OperationType.Update);
        sipPackage2.setStatus(SIPPackageProcessingStatus.SCHEDULED);
        sipPackage2.setRelatedProdIds(Collections.singleton(new RelationProdId()));

        List<SIPPackage> list = new ArrayList<>();
        list.add(sipPackage1);
        list.add(sipPackage2);

        return list;
    }
    
    private List<PackageHasParentPackage> createQueueEntriesForPrioritizePackagesOnInsertTest() {
    	 List<PackageHasParentPackage> entries = new ArrayList<>();
    	 PackageHasParentPackage phpp1 = new PackageHasParentPackage();
    	 phpp1.setId(10L);
         phpp1.setSipId(2L);
         phpp1.setParentSipId(null);
         entries.add(phpp1);
    	 return entries;
    }

    private List<PackageHasParentPackage> createQueueEntriesForPrioritizePackagesOnRemovalTest() {
        List<PackageHasParentPackage> list = new ArrayList<>();
        PackageHasParentPackage phpp1 = new PackageHasParentPackage();
        PackageHasParentPackage phpp2 = new PackageHasParentPackage();

        phpp1.setId(10L);
        phpp1.setSipId(1L);
        phpp1.setParentSipId(100L);
        list.add(phpp1);

        phpp2.setId(200L);
        phpp2.setSipId(20L);
        phpp2.setParentSipId(2L);
        list.add(phpp2);

        return list;
    }
}
