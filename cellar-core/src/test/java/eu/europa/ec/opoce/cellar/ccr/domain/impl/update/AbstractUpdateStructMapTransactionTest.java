package eu.europa.ec.opoce.cellar.ccr.domain.impl.update;

import com.google.common.collect.ImmutableSet;
import eu.europa.ec.opoce.cellar.cmr.CellarResource;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.s3.ContentStreamService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static eu.europa.ec.opoce.cellar.s3.ContentStreamService.Metadata.MANIFESTATION_LAST_INDEX;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class AbstractUpdateStructMapTransactionTest {

    @Mock
    private ContentStreamService contentStreamService;

    @InjectMocks
    private AbstractUpdateStructMapTransaction updateStructMapTransaction = new UpdateStructMapTransaction();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void getNextDatastreamIndexTest_fallback() {
        CellarResource manifestation = manifestationMock();
        Set<String> itemCellarIds = Collections.emptySet();

        when(contentStreamService.getMetadata("cellar:work.expr.man", "v1", ContentType.DIRECT))
                .thenReturn(Collections.emptyMap());
        when(contentStreamService.updateMetadata("cellar:work.expr.man", "v1", ContentType.DIRECT, metadata(1)))
                .thenReturn("v2");

        int index = updateStructMapTransaction.getNextDatastreamIndex(manifestation, itemCellarIds);
        assertEquals(1, index);
        assertEquals("v2", manifestation.getVersion(ContentType.DIRECT).get());

        verify(contentStreamService).getMetadata("cellar:work.expr.man", "v1", ContentType.DIRECT);
        verify(contentStreamService).updateMetadata("cellar:work.expr.man", "v1", ContentType.DIRECT, metadata(1));
        verifyNoMoreInteractions(contentStreamService);
    }

    @Test
    public void getNextDatastreamIndexTest_extractLastIndexFromS3Metadata() {
        CellarResource manifestation = manifestationMock();
        Set<String> itemCellarIds = Collections.emptySet();

        when(contentStreamService.getMetadata("cellar:work.expr.man", "v1", ContentType.DIRECT))
                .thenReturn(Collections.singletonMap(MANIFESTATION_LAST_INDEX, "3"));
        when(contentStreamService.updateMetadata("cellar:work.expr.man", "v1", ContentType.DIRECT, metadata(4)))
                .thenReturn("v2");

        int index = updateStructMapTransaction.getNextDatastreamIndex(manifestation, itemCellarIds);
        assertEquals(4, index);
        assertEquals("v2", manifestation.getVersion(ContentType.DIRECT).get());

        verify(contentStreamService).getMetadata("cellar:work.expr.man", "v1", ContentType.DIRECT);
        verify(contentStreamService).updateMetadata("cellar:work.expr.man", "v1", ContentType.DIRECT, metadata(4));
        verifyNoMoreInteractions(contentStreamService);
    }

    @Test
    public void getNextDatastreamIndexTest_extractLastIndexFromIdentifiers() {
        CellarResource manifestation = manifestationMock();
        Set<String> itemCellarIds = ImmutableSet.of("cellar:work.expr.man/DOC_1", "cellar:work.expr.man/DOC_2");

        when(contentStreamService.getMetadata("cellar:work.expr.man", "v1", ContentType.DIRECT))
                .thenReturn(Collections.emptyMap());
        when(contentStreamService.updateMetadata("cellar:work.expr.man", "v1", ContentType.DIRECT, metadata(3)))
                .thenReturn("v2");

        int index = updateStructMapTransaction.getNextDatastreamIndex(manifestation, itemCellarIds);
        assertEquals(3, index);
        assertEquals("v2", manifestation.getVersion(ContentType.DIRECT).get());

        verify(contentStreamService).getMetadata("cellar:work.expr.man", "v1", ContentType.DIRECT);
        verify(contentStreamService).updateMetadata("cellar:work.expr.man", "v1", ContentType.DIRECT, metadata(3));
        verifyNoMoreInteractions(contentStreamService);
    }

    private CellarResource manifestationMock() {
        Map<ContentType, String> versions = new HashMap<>();
        versions.put(ContentType.DIRECT, "v1");

        CellarResource manifestation = new CellarResource();
        manifestation.setCellarId("cellar:work.expr.man");
        manifestation.setVersions(versions);

        return manifestation;
    }

    private Map<String, String> metadata(int index) {
        return Collections.singletonMap(MANIFESTATION_LAST_INDEX, String.valueOf(index));
    }
}