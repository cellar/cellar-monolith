package eu.europa.ec.opoce.cellar.cl.service.batchJob.manager;

import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.BatchJobService;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.BatchJobServiceImpl;
import eu.europa.ec.opoce.cellar.cl.service.batchJob.impl.processor.BatchJobProcessorFactory;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BatchJobRunableTest {
    private BatchJobService batchJobService;
    private BatchJobProcessorFactory batchJobProcessorFactory;
    private BatchJob batchJobWithIdOne;
    private BatchJob batchJobWithIdTwo;
    private BatchJob secondBatchJobWithIdOne;

    @Before
    public void setup() throws Exception {
        batchJobService = new BatchJobServiceImpl();
        batchJobProcessorFactory = new BatchJobProcessorFactory();

        batchJobWithIdOne = new BatchJob();
        batchJobWithIdOne.setId(1L);

        batchJobWithIdTwo = new BatchJob();
        batchJobWithIdTwo.setId(2L);

        secondBatchJobWithIdOne = new BatchJob();
        secondBatchJobWithIdOne.setId(1L);
    }

    @Test
    public void objectShouldNotBeEqualToNull() throws Exception {
        BatchJobRunable batchJobRunable = new BatchJobRunable(batchJobWithIdOne, batchJobService, batchJobProcessorFactory) {
            @Override
            public void run() {

            }
        };
        assertFalse(batchJobRunable.equals(null));
    }

    @Test
    public void sameObjectIsEqual() throws Exception {
        BatchJobRunable batchJobRunable = new BatchJobRunable(batchJobWithIdOne, batchJobService, batchJobProcessorFactory) {
            @Override
            public void run() {

            }
        };
        assertTrue(batchJobRunable.equals(batchJobRunable));
    }

    @Test
    public void differentBatchJobWithDifferentIdAreNotEquals() throws Exception {
        BatchJobRunable batchJobRunable = new BatchJobRunable(batchJobWithIdOne, batchJobService, batchJobProcessorFactory) {
            @Override
            public void run() {

            }
        };

        BatchJobRunable secondBatchJobRunable = new BatchJobRunable(batchJobWithIdTwo, batchJobService, batchJobProcessorFactory) {
            @Override
            public void run() {

            }
        };
        assertFalse(batchJobRunable.equals(secondBatchJobRunable));
    }

    @Test
    public void differentBatchJobWithSameIdAreEquals() throws Exception {
        BatchJobRunable batchJobRunable = new BatchJobRunable(batchJobWithIdOne, batchJobService, batchJobProcessorFactory) {
            @Override
            public void run() {

            }
        };

        BatchJobRunable secondBatchJobRunable = new BatchJobRunable(secondBatchJobWithIdOne, batchJobService, batchJobProcessorFactory) {
            @Override
            public void run() {

            }
        };
        assertTrue(batchJobRunable.equals(secondBatchJobRunable));
    }
}
