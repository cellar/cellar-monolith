package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.op.cellar.api.service.impl.ManifestationType;
import eu.europa.ec.op.cellar.api.util.WeightedManifestationType;
import eu.europa.ec.op.cellar.api.util.WeightedManifestationTypes;
import eu.europa.ec.opoce.cellar.ccr.service.FileTypesNalSkosLoaderService;
import eu.europa.ec.opoce.cellar.cl.domain.mimeType.MimeTypeMapping;
import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class WeightedTypeCacheServiceImplTest {

    private WeightedTypeCacheServiceImpl cacheService;
    private FileTypesNalSkosLoaderServiceMock nalSkosLoaderService;

    @Before
    public void setUp() throws Exception {
        nalSkosLoaderService = new FileTypesNalSkosLoaderServiceMock();
        cacheService = new WeightedTypeCacheServiceImpl();
        cacheService.setFileTypesNalSkosLoaderService(nalSkosLoaderService);
    }

    @Test
    public void emptyExtensionMapReturnsDefaultWeightedManifestationType() throws Exception {
        nalSkosLoaderService.clearExtensionMap();
        WeightedManifestationTypes weightedManifestationType = cacheService.getWeightedManifestationTypes("extension");
        List<WeightedManifestationType> weightedManifestationTypes = weightedManifestationType.getWeightedManifestationTypes();
        assertEquals(1, weightedManifestationTypes.size());
        assertTrue(isDefaultManifestationType(weightedManifestationTypes.get(0)));
    }

    private boolean isDefaultManifestationType(WeightedManifestationType weightedManifestationType) {
        return weightedManifestationType.getManifestationType() == ManifestationType.DEFAULT;
    }

    @Test
    public void noMatchingExtensionReturnsDefaultWeightedManifestationType() throws Exception {
        List<MimeTypeMapping> mimeTypeMappingListOne = new ArrayList<>();
        MimeTypeMapping mimeTypeMappingOne = new MimeTypeMapping();
        String extensionOne = "extensionOne";
        mimeTypeMappingOne.setExtension(extensionOne);
        mimeTypeMappingOne.setManifestationType("manifestationType");
        mimeTypeMappingListOne.add(mimeTypeMappingOne);
        nalSkosLoaderService.feedMappingByExtensionMap(extensionOne, mimeTypeMappingListOne);

        List<MimeTypeMapping> mimeTypeMappingListTwo = new ArrayList<>();
        MimeTypeMapping mimeTypeMappingTwo = new MimeTypeMapping();
        String extensionTwo = "extensionTwo";
        mimeTypeMappingTwo.setExtension(extensionTwo);
        mimeTypeMappingTwo.setManifestationType("manifestationType");
        mimeTypeMappingListTwo.add(mimeTypeMappingTwo);
        nalSkosLoaderService.feedMappingByExtensionMap(extensionTwo, mimeTypeMappingListTwo);

        WeightedManifestationTypes weightedManifestationType = cacheService.getWeightedManifestationTypes("extension");
        List<WeightedManifestationType> weightedManifestationTypes = weightedManifestationType.getWeightedManifestationTypes();
        assertEquals(1, weightedManifestationTypes.size());
        assertTrue(isDefaultManifestationType(weightedManifestationTypes.get(0)));
    }

    @Test
    public void oneMatchingExtension() throws Exception {
        List<MimeTypeMapping> mimeTypeMappingList = new ArrayList<>();
        MimeTypeMapping mimeTypeMappingOne = new MimeTypeMapping();
        String extension = "extension";
        mimeTypeMappingOne.setExtension(extension);
        mimeTypeMappingOne.setManifestationType("fmx4");
        mimeTypeMappingList.add(mimeTypeMappingOne);
        nalSkosLoaderService.feedMappingByExtensionMap(extension, mimeTypeMappingList);

        WeightedManifestationTypes weightedManifestationTypes = cacheService.getWeightedManifestationTypes("extension");
        List<WeightedManifestationType> weightedManifestationTypeList = weightedManifestationTypes.getWeightedManifestationTypes();
        assertEquals(1, weightedManifestationTypeList.size());
        assertFalse(isDefaultManifestationType(weightedManifestationTypeList.get(0)));
    }

    @Test
    public void threeMatchingExtensionAndOnlyFirstHasNoWeight() throws Exception {
        List<MimeTypeMapping> mimeTypeMappingList = new LinkedList<>();
        String extension = "extension";

        MimeTypeMapping mimeTypeMappingOne = new MimeTypeMapping();
        mimeTypeMappingOne.setExtension(extension);
        mimeTypeMappingOne.setManifestationType("fmx4");
        mimeTypeMappingList.add(mimeTypeMappingOne);

        MimeTypeMapping mimeTypeMappingTwo = new MimeTypeMapping();
        mimeTypeMappingTwo.setExtension(extension);
        mimeTypeMappingTwo.setManifestationType("fmx3");
        mimeTypeMappingList.add(mimeTypeMappingTwo);

        MimeTypeMapping mimeTypeMappingThree = new MimeTypeMapping();
        mimeTypeMappingThree.setExtension(extension);
        mimeTypeMappingThree.setManifestationType("fmx2");
        mimeTypeMappingList.add(mimeTypeMappingThree);

        nalSkosLoaderService.feedMappingByExtensionMap(extension, mimeTypeMappingList);

        WeightedManifestationTypes weightedManifestationType = cacheService.getWeightedManifestationTypes("extension");
        List<WeightedManifestationType> weightedManifestationTypes = weightedManifestationType.getWeightedManifestationTypes();
        assertEquals(3, weightedManifestationTypes.size());

        // normally, we should test that the weight of 0 is null but we get a null pointer exception because of unboxing
        assertFalse(isDefaultManifestationType(weightedManifestationTypes.get(1)));
        assertNotNull(weightedManifestationTypes.get(1).getWeight());

        assertFalse(isDefaultManifestationType(weightedManifestationTypes.get(2)));
        assertNotNull(weightedManifestationTypes.get(2).getWeight());
    }

    private class FileTypesNalSkosLoaderServiceMock implements FileTypesNalSkosLoaderService {

        private Map<String, List<MimeTypeMapping>> map;

        FileTypesNalSkosLoaderServiceMock() {
            map = new HashMap<>();
        }

        public void feedMappingByExtensionMap(String extension, List<MimeTypeMapping> mimeTypeMappingList) {
            map.put(extension, mimeTypeMappingList);
        }

        public void clearExtensionMap() {
            map.clear();
        }

        @Override
        public boolean areAllItemsLoaded() {
            return false;
        }

        @Override
        public int numberOfItemDescriptionsLoaded() {
            return 0;
        }

        @Override
        public List<MimeTypeMapping> getMimeTypeMappingsByExtension(String contentExtension) {
            return map.get(contentExtension);
        }

        @Override
        public Set<MimeTypeMapping> getMimeTypeMappingsByPriorityDesc() {
            return null;
        }

        @Override
        public List<MimeTypeMapping> getMimeTypeMappingByManifestationType(String manifestationType) {
            return null;
        }

        @Override
        public Set<String> getFileExtensionMatchingMimetype(String mimeType) {
            return null;
        }

        @Override
        public Set<String> getAllPossibleExtensions() {
            return null;
        }

        @Override
        public List<MimeTypeMapping> getMimeTypeMappingByMimeType(String mimeType) {
            return null;
        }
    }
}
