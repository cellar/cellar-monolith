/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.audit
 *             FILE : IngestionAuditTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 10 11, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-10-11 14:44:50 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.audit;

import eu.europa.ec.opoce.cellar.cl.runnable.IngestionRequestRunnable;
import eu.europa.ec.opoce.cellar.cl.service.IngestionService;
import eu.europa.ec.opoce.cellar.cl.service.client.AuditTrailEventService;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import eu.europa.ec.opoce.cellar.ingestion.SIPWork;
import org.apache.logging.log4j.ThreadContext;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.modules.junit4.PowerMockRunnerDelegate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;
import java.util.concurrent.*;

/**
 * @author ARHS Developments
 */
@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(SpringJUnit4ClassRunner.class)
@PrepareForTest(ServiceLocator.class)
@PowerMockIgnore({"org.w3c.*", "org.xml.sax.*", "javax.xml.*", "javax.management.*"})
@ContextConfiguration(classes = AuditAdviceTest.AuditAdviceTestConfiguration.class)
public class IngestionRequestRunnableTest {

    @Autowired
    private AuditTrailEventService auditTrailEventService;

    @Before
    public void setup() throws Exception {
        PowerMockito.mockStatic(ServiceLocator.class);
        PowerMockito.when(ServiceLocator.getService(AuditTrailEventService.class)).thenReturn(auditTrailEventService);
    }

    @Test
    public void threadLocalContextMustBeClearedWhenIngestionEnds() throws Exception {
        ConcurrentMap<SIPWork, Boolean> map = new ConcurrentHashMap<>();

        SIPWork sipWork = new SIPWork();
        File tmpFile = File.createTempFile("ingestionAudit", "");
        sipWork.setSipFile(tmpFile);

        IngestionService ingestionService = PowerMockito.mock(IngestionService.class);
        PowerMockito.doNothing().when(ingestionService).sipTreatment(sipWork);

        ExecutorService executor = Executors.newSingleThreadExecutor();
        try {
            for (int i = 0; i < 2; i++) {
                map.put(sipWork, true);
                Future<?> f = executor.submit(new ThreadContextCheck(new IngestionRequestRunnable(ingestionService, sipWork)));
                f.get();
            }

        } finally {
            executor.shutdownNow();
        }
    }

    static class ThreadContextCheck implements Runnable {

        private Callable<IngestionRequestRunnable> delegate;

        ThreadContextCheck(Callable<IngestionRequestRunnable> delegate) {
            this.delegate = delegate;
        }

        @Override
        public void run() {
            Assert.assertTrue("ThreadContext is not empty", ThreadContext.getImmutableContext().isEmpty());
            try {
                delegate.call();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            Assert.assertTrue("ThreadContext is not empty", ThreadContext.getImmutableContext().isEmpty());
        }
    }
}
