package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.cl.dao.CellarIdentifierDao;
import eu.europa.ec.opoce.cellar.cl.domain.admin.CellarIdentifier;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.powermock.api.mockito.PowerMockito;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.when;
@RunWith(MockitoJUnitRunner.class)
public class CellarIdentifierServiceImplTest {

    CellarIdentifierServiceImpl service;

    @Mock
    CellarIdentifierDao dao;

    @Before
    public void setup(){
        service=new CellarIdentifierServiceImpl(dao);
    }

    public List<CellarIdentifier> setupList(CellarIdentifier baseIdentifier){
        List<CellarIdentifier> idList=new ArrayList<>();
        idList.add(baseIdentifier);
        idList.add(new CellarIdentifier("cellar:123.0001"));
        idList.add(new CellarIdentifier("cellar:123.0002"));
        idList.add(new CellarIdentifier("cellar:123.0003"));
        idList.add(new CellarIdentifier("cellar:123.0004"));

        idList.add(new CellarIdentifier("cellar:123.0001.01"));
        idList.add(new CellarIdentifier("cellar:123.0001.02"));
        idList.add(new CellarIdentifier("cellar:123.0001.03"));

        idList.add(new CellarIdentifier("cellar:123.0003.01"));
        idList.add(new CellarIdentifier("cellar:123.0003.02"));
        idList.add(new CellarIdentifier("cellar:123.0003.03"));

        idList.add(new CellarIdentifier("cellar:123.0001.01/DOC_1"));
        idList.add(new CellarIdentifier("cellar:123.0003.02/DOC_1"));

        for(CellarIdentifier identifier:idList)
        {
            identifier.setReadOnly(false);
        }
        return idList;
    }
    @Test
    public void testRedundancy(){
        CellarIdentifier baseIdentifier=new CellarIdentifier("cellar:123");
        List<CellarIdentifier> idList=setupList(baseIdentifier);
        //Replace all CellarIdentifier with their spy
        idList.replaceAll(PowerMockito::spy);

        CellarIdentifierServiceImpl spy= PowerMockito.spy(service);

        when(dao.getAllChildrenPids(baseIdentifier.getUuid())).thenReturn(idList);

        service.downwardsCheck(baseIdentifier);
        //Verify each identifier was only accessed once
        idList.forEach(s->verify(s,Mockito.times(1)).getReadOnly());
        //Verify that the downwards check was performed only once
        verify(spy, Mockito.times(0)).downwardsCheck(any());


        }
    }
