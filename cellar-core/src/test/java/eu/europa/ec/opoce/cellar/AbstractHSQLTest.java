/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.cl.domain.admin
 *             FILE : CellarIdentifier.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 22 mars 2016
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$
 *          VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ReplacementDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.InputSource;

/**
 * The Class AbstractHSQLTest.
 * <class_description> A meaningful description of the class that will be
 *                     displayed as the class summary on the JavaDoc package page.
 * <br/><br/>
 * <notes> Includes guaranteed invariants, usage instructions and/or examples,
 *         reminders about desired improvements, etc.
 * <br/><br/>
 * ON : 22 mars 2016
 *
 * @author ARHS Developments
 * @version $Revision$
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public abstract class AbstractHSQLTest {

    /** The data source. */
    @Autowired
    private DataSource dataSource;

    /** The identifiers history dao. */

    @BeforeClass
    public static void setArgs() {
        System.setProperty("allow.internal.configuration", "true");
    }

    /**
     * Gets the dastaset xml.
     *
     * @return the dastaset xml
     */
    abstract protected String getDastasetXml();

    /**
     * Inits the db.
     *
     * @throws DataSetException the data set exception
     * @throws DatabaseUnitException the database unit exception
     * @throws SQLException the SQL exception
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws Exception the exception
     */
    @Before
    public void initDb() throws DataSetException, DatabaseUnitException, SQLException, IOException, Exception {
        DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSet());
    }

    /**
     * Gets the data set.
     *
     * @return the data set
     * @throws DataSetException the data set exception
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private IDataSet getDataSet() throws DataSetException, IOException {
        final InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(getDastasetXml());
        final InputSource xmlSource = new InputSource(inputStream);
        //        final FlatXmlProducer flatXmlProducer = new FlatXmlProducer(xmlSource);
        final FlatXmlDataSet flatXmlDataSet = new FlatXmlDataSet(xmlSource);
        final ReplacementDataSet dataSet = new ReplacementDataSet(flatXmlDataSet);
        dataSet.addReplacementObject("[null]", null);
        return dataSet;
    }

    /**
     * Gets the connection.
     *
     * @return the connection
     * @throws Exception the exception
     */
    private IDatabaseConnection getConnection() throws Exception {
        final Connection jdbcConnection = dataSource.getConnection();
        final IDatabaseConnection connection = new DatabaseConnection(jdbcConnection);
        final DatabaseConfig config = connection.getConfig();
        final HsqlDataTypeFactory value = new HsqlDataTypeFactory();
        config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, value);
        return connection;

    }
}
