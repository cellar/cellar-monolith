package eu.europa.ec.opoce.cellar.cl.service.impl;

import eu.europa.ec.opoce.cellar.ccr.enums.status.StatusType;
import eu.europa.ec.opoce.cellar.ccr.service.impl.StatusServiceImpl;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;
import static org.mockito.Mockito.*;

/**
 * @author EUROPEAN DYNAMICS S.A.
 */
public class StatusServiceImplTest {

    private ICellarConfiguration cellarConfiguration;
    private RestTemplate restTemplate;
    private StatusServiceImpl statusService;

    @Before
    public void setUp() {
        cellarConfiguration = mock(ICellarConfiguration.class);
        restTemplate = mock(RestTemplate.class);
        statusService = new StatusServiceImpl(cellarConfiguration);
        statusService.setRestTemplate(restTemplate);
    }

    @Test
    public void testGetStatusResponseForPackageLevel() {
        // Mocking the required data
        String id = "some-uuid";
        boolean ingestion = true;
        boolean indexation = false;
        int verbosity = 2;
        StatusType statusType = StatusType.PACKAGE_LEVEL;
        String expectedResponse = "Package Level Status Response";

        // Mock the getStatusServicePackageLevelEndpointWithUuid method
        when(cellarConfiguration.getCellarStatusServiceBaseUrl()).thenReturn("http://example.com");
        when(cellarConfiguration.getCellarStatusServicePackageLevelEndpoint()).thenReturn("/package");
        when(restTemplate.getForEntity(anyString(), eq(String.class))).thenReturn(ResponseEntity.ok(expectedResponse));

        // Test the method
        String actualResponse = statusService.getStatusResponse(id, ingestion, indexation, null, verbosity, statusType);

        // Verify the method calls and the response
        String expectedUrl = "http://example.com/package?id=some-uuid&ingestion=true&indexation=false&verbosity=2";
        verify(cellarConfiguration).getCellarStatusServiceBaseUrl();
        verify(cellarConfiguration).getCellarStatusServicePackageLevelEndpoint();
        verify(restTemplate).getForEntity(eq(expectedUrl), eq(String.class));
        assertEquals(expectedResponse, actualResponse);
    }

    @Test
    public void testGetStatusResponseForObjectLevel() {
        // Mocking the required data
        String id = "some-pid";
        boolean ingestion = true;
        boolean indexation = false;
        int verbosity = 2;
        StatusType statusType = StatusType.OBJECT_LEVEL;
        LocalDateTime datetime = LocalDateTime.of(2023, 8, 8, 8, 26, 49);
        String expectedResponse = "Object Level Status Response";

        // Mock the getStatusServiceObjectLevelEndpointWithPid method
        when(cellarConfiguration.getCellarStatusServiceBaseUrl()).thenReturn("http://example.com");
        when(cellarConfiguration.getCellarStatusServiceObjectLevelEndpoint()).thenReturn("/object");
        when(restTemplate.getForEntity(anyString(), eq(String.class))).thenReturn(ResponseEntity.ok(expectedResponse));

        // Test the method
        String actualResponse = statusService.getStatusResponse(id, ingestion, indexation, datetime, verbosity, statusType);

        // Verify the method calls and the response
        String expectedUrl = "http://example.com/object?id=some-pid&ingestion=true&indexation=false&verbosity=2&datetime=" + datetime;
        verify(cellarConfiguration).getCellarStatusServiceBaseUrl();
        verify(cellarConfiguration).getCellarStatusServiceObjectLevelEndpoint();
        verify(restTemplate).getForEntity(eq(expectedUrl), eq(String.class));
        assertEquals(expectedResponse, actualResponse);
    }

    @Test
    public void testGetStatusResponseForLookup() {
        // Mocking the required data
        String packageName = "some-package-name";
        boolean ingestion = false;
        boolean indexation = false;
        int verbosity = -1;
        StatusType statusType = StatusType.LOOKUP;
        String expectedResponse = "Lookup Status Response";

        // Mock the getStatusServiceLookupEndpointWithPackageName method
        when(cellarConfiguration.getCellarStatusServiceBaseUrl()).thenReturn("http://example.com");
        when(cellarConfiguration.getCellarStatusServiceLookupEndpoint()).thenReturn("/lookup");
        when(restTemplate.getForEntity(anyString(), eq(String.class))).thenReturn(ResponseEntity.ok(expectedResponse));

        // Test the method
        String actualResponse = statusService.getStatusResponse(packageName, ingestion, indexation, null, verbosity, statusType);

        // Verify the method calls and the response
        String expectedUrl = "http://example.com/lookup?id=some-package-name";
        verify(cellarConfiguration).getCellarStatusServiceBaseUrl();
        verify(cellarConfiguration).getCellarStatusServiceLookupEndpoint();
        verify(restTemplate).getForEntity(eq(expectedUrl), eq(String.class));
        assertEquals(expectedResponse, actualResponse);
    }

    @Test
    public void testGetStatusResponseWithInvalidDateTime() {
        // Mocking the required data
        String id = "some-uuid";
        boolean ingestion = true;
        boolean indexation = false;
        int verbosity = 2;
        StatusType statusType = StatusType.OBJECT_LEVEL;
        LocalDateTime invalidDateTime = null; // Invalid DateTime (null)

        // Test the method with invalid DateTime
        assertThrows(NullPointerException.class, () -> statusService.getStatusResponse(id, ingestion, indexation, invalidDateTime, verbosity, statusType));
    }
}
