package eu.europa.ec.opoce.cellar.cl.service.export.impl;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.configuration.PrefixConfigurationService;
import eu.europa.ec.opoce.cellar.cl.service.client.IdentifierService;
import eu.europa.ec.opoce.cellar.cl.service.export.exporter.MetsElementExporterFactory;
import eu.europa.ec.opoce.cellar.cmr.database.dao.CellarResourceDao;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Agent;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Dossier;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.TopLevelEvent;
import eu.europa.ec.opoce.cellar.cmr.metsbeans.Work;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import eu.europa.ec.opoce.cellar.core.dao.FileDao;
import eu.europa.ec.opoce.cellar.domain.content.mets.ContentIdentifier;
import eu.europa.ec.opoce.cellar.domain.content.mets.PrefixConfiguration;
import eu.europa.ec.opoce.cellar.generated.mets.cellar.v2.MetsType;
import eu.europa.ec.opoce.cellar.mets.v2.builder.MetsBuilder;
import eu.europa.ec.opoce.cellar.server.admin.nal.GroupedNalOntoVersion;
import eu.europa.ec.opoce.cellar.server.admin.nal.NalOntoVersion;
import eu.europa.ec.opoce.cellar.server.admin.nal.OntoAdminConfigurationService;
import eu.europa.ec.opoce.cellar.server.service.DisseminationDbGateway;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Collection;
import java.util.Date;

import static com.google.common.collect.Lists.newArrayList;
import static eu.europa.ec.opoce.cellar.cl.service.export.impl.ExportServiceImpl.RecordStatus.COMPLETE;
import static eu.europa.ec.opoce.cellar.cl.service.export.impl.ExportServiceImpl.RecordStatus.PARTIAL;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ServiceLocator.class)
@PowerMockIgnore({"javax.management.*", "javax.xml.*", "org.xml.sax.*", "org.w3c.dom.*"})
public class ExportServiceImplTest {

    private ExportServiceImpl exportService;
    private CellarResourceDao cellarResourceDao;

    @Before
    public void setup() {
        final PrefixConfigurationService prefixConfigurationService = mock(PrefixConfigurationService.class);
        when(prefixConfigurationService.getPrefixConfiguration()).thenReturn(mock(PrefixConfiguration.class));
        PowerMockito.mockStatic(ServiceLocator.class);
        PowerMockito.when(ServiceLocator.getService(PrefixConfigurationService.class)).thenReturn(prefixConfigurationService);

        OntoAdminConfigurationService ontoConfig = mock(OntoAdminConfigurationService.class);
        cellarResourceDao = mock(CellarResourceDao.class);
        exportService = new ExportServiceImpl(
                mock(DisseminationDbGateway.class),
                mock(IdentifierService.class),
                mock(MetsElementExporterFactory.class),
                mock(FileDao.class),
                mock(ICellarConfiguration.class),
                ontoConfig,
                cellarResourceDao
        );
        when(ontoConfig.getGroupedOntologyVersions()).thenReturn(groupedNalOntoVersions());

    }

    private static Collection<GroupedNalOntoVersion> groupedNalOntoVersions() {
        NalOntoVersion version = new NalOntoVersion();
        version.setVersionNumber("1.2.3");
        return newArrayList(new GroupedNalOntoVersion("CDM", "http://publications.europa.eu/ontology/cdm", version));
    }

    @Test
    public void metsCreatorMustBeCellarAndRoleCreator() {
        Date createDate = new Date();
        MetsType.MetsHdr metsHdr = ExportServiceImpl.createMetsHdr("1.2.3", "1.2.3", COMPLETE.toString(), createDate);
        MetsType.MetsHdr.Agent creator = metsHdr.getAgent().get(0);
        Assert.assertEquals("CELLAR", creator.getName());
        Assert.assertEquals("CREATOR", creator.getROLE());
        Assert.assertEquals("OTHER", creator.getTYPE());
        Assert.assertEquals("SOFTWARE", creator.getOTHERTYPE());
        Assert.assertEquals(MetsBuilder.fromDate(createDate), metsHdr.getCREATEDATE());
    }

    @Test
    public void noMetadataOnlyMustReturnRecordStatusComplete() {
        Assert.assertEquals(COMPLETE, exportService.resolveRecordStatus(null, false));
    }

    @Test
    public void metadataOnlyOnWorkWithManifestationMustReturnRecordStatusPartial() {
        ContentIdentifier identifier = new ContentIdentifier("123456", true);
        Work work = new Work();
        work.setCellarId(identifier);
        when(cellarResourceDao.countManifestations("123456")).thenReturn(10);
        Assert.assertEquals(PARTIAL, exportService.resolveRecordStatus(work, true));
    }

    @Test
    public void metadataOnlyOnDossierMustReturnRecordStatusComplete() {
        Assert.assertEquals(COMPLETE, exportService.resolveRecordStatus(new Dossier(), true));
    }

    @Test
    public void metadataOnlyOnTopLevelEventMustReturnRecordStatusComplete() {
        Assert.assertEquals(COMPLETE, exportService.resolveRecordStatus(new TopLevelEvent(), true));
    }

    @Test
    public void metadataOnlyOnAgentMustReturnRecordStatusComplete() {
        Assert.assertEquals(COMPLETE, exportService.resolveRecordStatus(new Agent(), true));
    }


}
