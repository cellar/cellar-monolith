package eu.europa.ec.opoce.cellar.cl.domain.batchjob;

import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJob;
import eu.europa.ec.opoce.cellar.cl.domain.batchJob.BatchJobWorkIdentifier;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BatchJobWorkIdentifierTest {

    /** The test subject_ a. */
    private BatchJobWorkIdentifier testSubject_A;

    /** The test subject_ b. */
    private BatchJobWorkIdentifier testSubject_B;

    /** The test subject_ c. */
    private BatchJobWorkIdentifier testSubject_C;

    /** The test subject_ d. */
    private BatchJobWorkIdentifier testSubject_D;

    @Before
    public void before() throws Exception {
        BatchJob batchJob1 = new BatchJob();
        BatchJob batchJob2 = new BatchJob();
        testSubject_A = generateObject(new Long(1), "workId1", batchJob1);
        testSubject_B = generateObject(new Long(2), "workId2", batchJob2);
        testSubject_C = generateObject(new Long(1), "workId1", batchJob1);
        testSubject_D = generateObject(new Long(4), "workId4", batchJob2);
    }

    private BatchJobWorkIdentifier generateObject(final Long id, final String workId, final BatchJob batchJob) {
        final BatchJobWorkIdentifier result = new BatchJobWorkIdentifier();
        result.setId(id);
        result.setWorkId(workId);
        result.setBatchJob(batchJob);
        return result;
    }

    @Test
    public void testEquals() {
        Assert.assertTrue(testSubject_A.equals(testSubject_C));
        Assert.assertTrue(!testSubject_A.equals(testSubject_B));
        Assert.assertTrue(!testSubject_A.equals(testSubject_D));
        Assert.assertTrue(!testSubject_B.equals(testSubject_D));
    }

    @Test
    public void testHashCode() {
        Assert.assertTrue(testSubject_A.hashCode() == testSubject_C.hashCode());
        Assert.assertTrue(testSubject_A.hashCode() != testSubject_B.hashCode());
        Assert.assertTrue(testSubject_A.hashCode() != testSubject_D.hashCode());
        Assert.assertTrue(testSubject_B.hashCode() != testSubject_D.hashCode());
    }

}
