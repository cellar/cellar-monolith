/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ccr.s3.read.impl
 *             FILE : LanguagesNalSkosLoaderServiceImplTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 04 23, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-04-23 07:17:24 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.ccr.s3.read.impl;

import com.google.common.io.Resources;
import eu.europa.ec.opoce.cellar.cmr.ContentType;
import eu.europa.ec.opoce.cellar.exception.CellarConfigurationException;
import eu.europa.ec.opoce.cellar.nal.dao.NalConfig;
import eu.europa.ec.opoce.cellar.nal.languageconfiguration.LanguageBean;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import org.apache.commons.io.IOUtils;
import org.apache.jena.rdf.model.Model;
import org.junit.*;
import org.junit.rules.ExpectedException;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.*;

/**
 * @author ARHS Developments
 */
public class LanguagesNalSkosLoaderServiceImplTest {

    private static final String NAL_PID = "nal:language";
    private static final NalConfig NAL_CONFIG;

    static {
        NAL_CONFIG = new NalConfig();
        NAL_CONFIG.setExternalPid(NAL_PID);
    }

    private static final Path completeSkos = Paths.get("src/test/resources/eu/europa/ec/opoce/cellar/ccr/s3/read/impl/language-complete.rdf");
    private static final Path invalidSkos = Paths.get("src/test/resources/eu/europa/ec/opoce/cellar/ccr/s3/read/impl/language-invalid.rdf");
    private static final Path validSkos = Paths.get("src/test/resources/eu/europa/ec/opoce/cellar/ccr/s3/read/impl/language-valid.rdf");
    // path is relative to the class in the build folder, different usage than the other paths
    private static final Path fallbackSkos = Paths.get("language-fallback.rdf");
    private static NoReloadLanguagesNalSkosLoaderServiceImpl languagesNalSkosLoaderService;
    private static Model completeSkosModel;
    private static Model invalidSkosModel;
    private static Model validSkosModel;
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @BeforeClass
    public static void oneTimeSetUp() {

    }

    @Before
    public void setup() throws Exception {
        completeSkosModel = JenaUtils.read(new String(Files.readAllBytes(completeSkos)));
        invalidSkosModel = JenaUtils.read(new String(Files.readAllBytes(invalidSkos)));
        validSkosModel = JenaUtils.read(new String(Files.readAllBytes(validSkos)));

        languagesNalSkosLoaderService = new LanguagesNalSkosLoaderServiceImplTest.NoReloadLanguagesNalSkosLoaderServiceImpl();
        languagesNalSkosLoaderService.setNalConfig(NAL_CONFIG);
        languagesNalSkosLoaderService.resetInMemoryMaps();
    }

    @After
    public void tearDown() {
        JenaUtils.closeQuietly(completeSkosModel, invalidSkosModel, validSkosModel);
    }

    @Test
    public void testLanguageWithoutTwoCharCode() {
        // setup complete default model with languages ENG, SLA, 0D0, 0E0
        languagesNalSkosLoaderService.setModel(completeSkosModel);
        languagesNalSkosLoaderService.initialize();

        LanguageBean sla = languagesNalSkosLoaderService.getLanguageBeanByUri("http://publications.europa.eu/resource/authority/language/SLA");
        assertNotNull(sla);
        assertEquals("sla", sla.getIsoCodeThreeChar());
        assertNull(sla.getIsoCodeTwoChar());
    }

    @Test
    public void testLanguageWithTwoCharCode() {
        // setup complete default model with languages ENG, SLA, 0D0, 0E0
        languagesNalSkosLoaderService.setModel(completeSkosModel);
        languagesNalSkosLoaderService.initialize();

        LanguageBean eng = languagesNalSkosLoaderService.getLanguageBeanByUri("http://publications.europa.eu/resource/authority/language/ENG");
        assertNotNull(eng);
        assertEquals("eng", eng.getIsoCodeThreeChar());
        assertEquals("en", eng.getIsoCodeTwoChar());
    }

    // these languages are deprecated and should not be used by OP but they will not be removed from the NAL yet
    // => make sure that processing them will not make the process crash
    // => if they are used, they will fail the validation during the ingestion when we enrich the model with cmr:lang property
    @Test
    public void testLanguageWithInvalidXsdLanguage() {
        // setup complete default model with languages ENG, SLA, 0D0, 0E0
        languagesNalSkosLoaderService.setModel(completeSkosModel);
        languagesNalSkosLoaderService.initialize();

        LanguageBean invalidXsdLanguageOne = languagesNalSkosLoaderService.getLanguageBeanByUri("http://publications.europa.eu/resource/authority/language/0D0");
        assertNotNull(invalidXsdLanguageOne);
        assertEquals("0d0", invalidXsdLanguageOne.getIsoCodeThreeChar());
        assertNull(invalidXsdLanguageOne.getIsoCodeTwoChar());

        LanguageBean invalidXsdLanguageTwo = languagesNalSkosLoaderService.getLanguageBeanByUri("http://publications.europa.eu/resource/authority/language/0E0");
        assertNotNull(invalidXsdLanguageTwo);
        assertEquals("0e0", invalidXsdLanguageTwo.getIsoCodeThreeChar());
        assertNull(invalidXsdLanguageTwo.getIsoCodeTwoChar());
    }

    @Test
    public void processingErrorsAtStartupThenFallbackIsLoaded() {
        languagesNalSkosLoaderService.setModel(invalidSkosModel);

        // trigger the CELLAR boot process with an invalid NAL in S3
        languagesNalSkosLoaderService.initialize();

        // check that language ZRN is not part of the languages collection
        // since it is valid and the "eng" language was loaded from fallback
        thrown.expect(CellarConfigurationException.class);
        thrown.expectMessage("CELLAR is unable to locate the language http://publications.europa.eu/resource/authority/language/ZRN");
        LanguageBean zrnLanguage = languagesNalSkosLoaderService
                .getLanguageBeanByUri("http://publications.europa.eu/resource/authority/language/ZRN");
        assertNull(zrnLanguage);

        // check that english language from fallback is found
        LanguageBean engLanguage = languagesNalSkosLoaderService
                .getLanguageBeanByUri("http://publications.europa.eu/resource/authority/language/ENG");
        assertNotNull(engLanguage);
    }

    @Test
    public void processingErrorsNalFromS3MissingDuringCacheReloadThenExistingMapNotChanged() {
        // setup complete default model with languages ENG, SLA, 0D0, 0E0
        languagesNalSkosLoaderService.setModel(completeSkosModel);
        languagesNalSkosLoaderService.initialize();

        LanguageBean engLanguage = languagesNalSkosLoaderService
                .getLanguageBeanByUri("http://publications.europa.eu/resource/authority/language/ENG");
        assertNotNull(engLanguage);

        LanguageBean slaLanguage = languagesNalSkosLoaderService
                .getLanguageBeanByUri("http://publications.europa.eu/resource/authority/language/SLA");
        assertNotNull(slaLanguage);

        // fake NAL missing in S3 during a cache reload mechanism -> error so no update
        languagesNalSkosLoaderService.setModel(null);
        languagesNalSkosLoaderService.reloadModel();

        engLanguage = languagesNalSkosLoaderService
                .getLanguageBeanByUri("http://publications.europa.eu/resource/authority/language/ENG");
        assertNotNull(engLanguage);

        slaLanguage = languagesNalSkosLoaderService
                .getLanguageBeanByUri("http://publications.europa.eu/resource/authority/language/SLA");
        assertNotNull(slaLanguage);
    }

    @Test
    public void processingErrorsInvalidNalFromS3DuringCacheReloadThenExistingMapNotChanged() {
        // setup complete default model with languages ENG, SLA, 0D0, 0E0
        languagesNalSkosLoaderService.setModel(completeSkosModel);
        languagesNalSkosLoaderService.initialize();

        LanguageBean engLanguage = languagesNalSkosLoaderService
                .getLanguageBeanByUri("http://publications.europa.eu/resource/authority/language/ENG");
        assertNotNull(engLanguage);

        LanguageBean slaLanguage = languagesNalSkosLoaderService
                .getLanguageBeanByUri("http://publications.europa.eu/resource/authority/language/SLA");
        assertNotNull(slaLanguage);

        // fake invalid NAL from S3 during a cache reload mechanism -> error so no update
        languagesNalSkosLoaderService.setModel(invalidSkosModel);
        languagesNalSkosLoaderService.reloadModel();

        // check that language ZRN is not part of the languages collection
        // and that the previous languages were loaded and not the fallback one
        engLanguage = languagesNalSkosLoaderService
                .getLanguageBeanByUri("http://publications.europa.eu/resource/authority/language/ENG");
        assertNotNull(engLanguage);

        slaLanguage = languagesNalSkosLoaderService
                .getLanguageBeanByUri("http://publications.europa.eu/resource/authority/language/SLA");
        assertNotNull(slaLanguage);

        thrown.expect(CellarConfigurationException.class);
        thrown.expectMessage("CELLAR is unable to locate the language http://publications.europa.eu/resource/authority/language/ZRN");
        LanguageBean zrnLanguage = languagesNalSkosLoaderService
                .getLanguageBeanByUri("http://publications.europa.eu/resource/authority/language/ZRN");
        assertNull(zrnLanguage);
    }

    @Test
    public void validNalDuringCacheReloadSuccessfullyUpdatesTheCollections() {
        // setup complete default model with languages ENG, SLA, 0D0, 0E0
        languagesNalSkosLoaderService.setModel(completeSkosModel);
        languagesNalSkosLoaderService.initialize();

        LanguageBean engLanguage = languagesNalSkosLoaderService
                .getLanguageBeanByUri("http://publications.europa.eu/resource/authority/language/ENG");
        assertNotNull(engLanguage);

        LanguageBean slaLanguage = languagesNalSkosLoaderService
                .getLanguageBeanByUri("http://publications.europa.eu/resource/authority/language/SLA");
        assertNotNull(slaLanguage);

        // setup valid NAL (ZRN language) from S3 during a cache reload mechanism -> no error so correct update
        languagesNalSkosLoaderService.setModel(validSkosModel);
        languagesNalSkosLoaderService.reloadModel();

        // check that language ZRN is part of the languages collection
        // and that the previous languages have been replaced by the valid NAL language
        LanguageBean zrnLanguage = languagesNalSkosLoaderService
                .getLanguageBeanByUri("http://publications.europa.eu/resource/authority/language/ZRN");
        assertNotNull(zrnLanguage);

        thrown.expect(CellarConfigurationException.class);
        thrown.expectMessage("CELLAR is unable to locate the language http://publications.europa.eu/resource/authority/language/ENG");
        engLanguage = languagesNalSkosLoaderService
                .getLanguageBeanByUri("http://publications.europa.eu/resource/authority/language/ENG");
        assertNull(engLanguage);

        thrown.expect(CellarConfigurationException.class);
        thrown.expectMessage("CELLAR is unable to locate the language http://publications.europa.eu/resource/authority/language/SLA");
        slaLanguage = languagesNalSkosLoaderService
                .getLanguageBeanByUri("http://publications.europa.eu/resource/authority/language/SLA");
        assertNull(slaLanguage);
    }

    private static class NoReloadLanguagesNalSkosLoaderServiceImpl extends LanguagesNalSkosLoaderServiceImpl {
        // the model to be returned to mock a model retrieval from S3
        private Model model;

        // the nal pid supposed to be extracted from the database, too time consuming to mock the DAO
        private NalConfig nalConfig;

        @Override
        protected NalConfig getNalConfig() {
            return nalConfig;
        }

        void setNalConfig(NalConfig nalConfig) {
            this.nalConfig = nalConfig;
        }

        @Override
        protected void initializeS3Context() {

        }

        @Override
        protected void reloadIfNecessary() {
        }


        @Override
        protected Model getNalFromRepository(String nalPid, String version, String uri, ContentType contentType) {
            return model;
        }

        @Override
        protected String getEmbeddedNal() throws IOException {
            URL resource = Resources.getResource(LanguagesNalSkosLoaderServiceImplTest.class, "language-fallback.rdf");
            return  IOUtils.toString(resource, "UTF-8");
        }

        void setModel(Model model) {
            this.model = model;
        }
    }
}