package eu.europa.ec.opoce.cellar.semantic.helper;

import org.junit.Assert;
import org.junit.Test;

import java.time.Instant;
import java.util.Date;

public class DateFormatsTest {

    @Test
    public void formatEtagDatetimeWithMillisecondsTest() throws Exception {
        final String formattedDate = format("2018-12-25T11:22:33.444Z");
        Assert.assertEquals(formattedDate, "20181225122233444");
    }

    @Test
    public void formatEtagDatetimeWithoutMillisecondsTest() throws Exception {
        final String formattedDate = format("2018-12-25T11:22:33Z");
        Assert.assertEquals(formattedDate, "20181225122233000");
    }

    private static String format(final String date) {
        final Instant instant = Instant.parse(date);
        return DateFormats.formatUnreadableFullDateTime(Date.from(instant));
    }
}