/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.semantic.jena
 *             FILE : JenaUtilsTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 02 26, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-02-26 08:23:52 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.semantic.jena;

import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.shared.JenaException;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;

/**
 * @author ARHS Developments
 */
public class JenaUtilsTest {


    @Test(expected = JenaException.class)
    public void readModelMustThrowAnExceptionIfEmptyContent() throws IOException {
        try (InputStream is = JenaUtilsTest.class.getClassLoader().getResourceAsStream("two-concept-different-priority-filetypes-skos.rdf")) {
            String content = CharStreams.toString(new InputStreamReader(is, Charsets.UTF_8));
            Model m = JenaUtils.read(content);
            Assert.assertTrue(m.size() > 0);

            JenaUtils.read("");
        }
    }

    @Test
    public void readModelMustNotReturnNullIfEmptyModel() throws IOException {
        StringWriter sw = new StringWriter();
        ModelFactory.createDefaultModel().write(sw);
        Model empty = JenaUtils.read(sw.toString());
        Assert.assertTrue(empty.size() == 0);
    }
}
