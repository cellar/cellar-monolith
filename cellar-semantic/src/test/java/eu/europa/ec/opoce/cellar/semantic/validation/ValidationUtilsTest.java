/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.semantic.validation
 *             FILE : ValidationUtilsTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 05 23, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-05-23 14:56:28 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.semantic.validation;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author ARHS Developments
 */
public class ValidationUtilsTest {
    @Test
    public void testInvalidUri() throws Exception {
        Resource resource = ResourceFactory.createResource("http://publications.europa.eu/resourceµ/authority/corporate body/COMM");
        assertTrue(ValidationUtils.isInvalidUri(resource));
    }

    @Test
    public void testInvalidUriInModel() throws Exception {
        Model model = ModelFactory.createDefaultModel();
        model.add(ResourceFactory.createResource("http://publications.europa.eu/resourceµ/authority/corporate body/COMM"),
                ResourceFactory.createProperty("property"),
                "test");
        List<String> invalidUris = ValidationUtils.validateUris(model);
        assertNotNull(invalidUris);
        assertEquals(1, invalidUris.size());
    }

    @Test
    public void testValidUri() throws Exception {
        Resource resource = ResourceFactory.createResource("http://publications.europa.eu/resource/authority/corporate-body/COMM");
        assertFalse(ValidationUtils.isInvalidUri(resource));
    }
}