package eu.europa.ec.opoce.cellar.semantic.validation;

import eu.europa.ec.opoce.cellar.cl.domain.validation.MetadataValidationResult;
import eu.europa.ec.opoce.cellar.domain.content.mets.MetsDocument;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ValidationResultCacheTest {

    @Test
    public void test() {
        final StructMap structMap0 = newStructMap("metsId0", "structMap0");
        final StructMap structMap1 = newStructMap("metsId1", "structMap1");
        final StructMap structMap2 = newStructMap("metsId2", "structMap2");
        final StructMap structMap3 = newStructMap("metsId3", "structMap3");

        ValidationResultCache.put(null, new MetadataValidationResult());
        ValidationResultCache.put(structMap0, null);
        ValidationResultCache.put(structMap1, new MetadataValidationResult());
        ValidationResultCache.put(structMap2, new MetadataValidationResult());
        ValidationResultCache.put(structMap2, new MetadataValidationResult());

        assertEquals(0, ValidationResultCache.get(structMap0).size());
        assertEquals(1, ValidationResultCache.get(structMap1).size());
        assertEquals(2, ValidationResultCache.get(structMap2).size());
        assertEquals(0, ValidationResultCache.get(structMap3).size());
        assertEquals(0, ValidationResultCache.get(null).size());

        ValidationResultCache.remove(structMap0);
        ValidationResultCache.remove(structMap1);
        ValidationResultCache.remove(structMap2);
        ValidationResultCache.remove(structMap3);
        ValidationResultCache.remove(null);

        assertEquals(0, ValidationResultCache.get(structMap0).size());
        assertEquals(0, ValidationResultCache.get(structMap1).size());
        assertEquals(0, ValidationResultCache.get(structMap2).size());
        assertEquals(0, ValidationResultCache.get(structMap3).size());
    }

    private StructMap newStructMap(final String metsId, final String structMapId) {
        final MetsDocument metsDocument = new MetsDocument();
        metsDocument.setDocumentId(metsId);

        final StructMap structMap = new StructMap(metsDocument);
        structMap.setId(structMapId);

        return structMap;
    }
}
