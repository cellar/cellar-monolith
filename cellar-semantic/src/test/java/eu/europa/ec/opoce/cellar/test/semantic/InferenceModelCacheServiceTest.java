/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.test.semantic
 *             FILE : InferredModelCacheServiceTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 09 06, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-09-06 11:57:04 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.test.semantic;

import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.common.util.IOUtils;
import eu.europa.ec.opoce.cellar.semantic.ontology.InferenceModelCacheService;
import eu.europa.ec.opoce.cellar.semantic.ontology.InferenceModelCacheServiceImpl;
import eu.europa.ec.opoce.cellar.semantic.ontology.InferenceModelDescription;
import org.apache.jena.ontology.OntModel;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

/**
 * @author ARHS Developments
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = InferenceModelCacheServiceTest.TestConfig.class)
public class InferenceModelCacheServiceTest {

    private static final Path MODEL_TEMP_PATH = Paths.get(System.getProperty("java.io.tmpdir"), "test_inference.rdf");
    private static final String RESOURCE = "/inference_model_7.5.rdf";

    private ICellarConfiguration cellarConfiguration;
    private InferenceModelCacheService inferenceModelCacheService;

    
    @Autowired
    ApplicationContext context;
    
    
    @BeforeClass
    public static void copy() throws IOException {
        if (Files.exists(MODEL_TEMP_PATH)) {
            cleanup();
        }
        try(InputStream is=InferenceModelCacheServiceTest.class.getResourceAsStream(RESOURCE)) {
            Files.copy(is, MODEL_TEMP_PATH);
        }
    }

    @AfterClass
    public static void cleanup() throws IOException {
        Files.delete(MODEL_TEMP_PATH);
    }

    
    @Before
    public void setup() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
    	cellarConfiguration = context.getBean("cellarConfigurationMock", ICellarConfiguration.class);
    	inferenceModelCacheService = context.getBean("inferenceModelCacheService", InferenceModelCacheService.class);
    }

    @Test
    public void inferenceModelMustBeRetrieveFromConfigurationPath() {
        Assert.assertEquals(cellarConfiguration.getInferenceOntologyPath(), inferenceModelCacheService.getDescription().getName());
        Assert.assertNotNull(inferenceModelCacheService.getInferenceModel());
    }

    @Test
    public void setNewModelMustWriteOnDisk() throws IOException, InterruptedException {
        InferenceModelDescription oldDesc = inferenceModelCacheService.getDescription();
        MultipartFile mock = new MockMultipartFile("mock", InferenceModelCacheServiceTest.class.getResourceAsStream(RESOURCE));
        TimeUnit.SECONDS.sleep(2);
        Assert.assertTrue(inferenceModelCacheService.setInferenceModelFile(mock));
        InferenceModelDescription newDesc = inferenceModelCacheService.getDescription();
        Assert.assertTrue(newDesc.getActivationDate().isAfter(oldDesc.getActivationDate()));
    }

    @Test
    public void setNewModelMustCreateANewModel() throws IOException {
        OntModel ontModel = inferenceModelCacheService.getInferenceModel();
        MultipartFile mock = new MockMultipartFile("mock", InferenceModelCacheServiceTest.class.getResourceAsStream(RESOURCE));
        Assert.assertTrue(inferenceModelCacheService.setInferenceModelFile(mock));
        Assert.assertTrue(ontModel != inferenceModelCacheService.getInferenceModel());
    }

    @Test
    public void setNullOrEmptyModelMustReturnFalse() throws IOException {
        MultipartFile mockEmpty = Mockito.mock(MultipartFile.class);
        Mockito.when(mockEmpty.getBytes()).then(i -> new byte[0]);
        Assert.assertFalse(inferenceModelCacheService.setInferenceModelFile(mockEmpty));

        MultipartFile mockNull = Mockito.mock(MultipartFile.class);
        Mockito.when(mockNull.getBytes()).then(i -> null);
        Assert.assertFalse(inferenceModelCacheService.setInferenceModelFile(mockNull));
    }
    
    
    @Configuration
    static class TestConfig {
    	
    	@Bean
    	public ICellarConfiguration cellarConfigurationMock() {
    		ICellarConfiguration mockCellarConfig = Mockito.mock(ICellarConfiguration.class);
    		Mockito.when(mockCellarConfig.getInferenceOntologyPath()).thenReturn(MODEL_TEMP_PATH.toString());
    		return mockCellarConfig;
    	}
    	
    	@Bean
    	public InferenceModelCacheService inferenceModelCacheService(ICellarConfiguration cellarConfigurationMock) {
    		return new InferenceModelCacheServiceImpl(cellarConfigurationMock);
    	}
    	
    }
}
