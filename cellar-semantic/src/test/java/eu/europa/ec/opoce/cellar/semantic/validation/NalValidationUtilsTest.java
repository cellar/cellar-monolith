/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.semantic.validation
 *             FILE : NalValidationUtilsTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 05 23, 2018
 *
 *      MODIFIED BY : EUROPEAN DYNAMICS S.A.
 *               ON : 29-07-2022
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2022 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.semantic.validation;

import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author ARHS Developments
 * @author EUROPEAN DYNAMICS S.A.
 */
public class NalValidationUtilsTest {
    private static final Path completeLanguageSkos = Paths.get("src/test/resources/models/language-complete.rdf");
    private static final Path validLanguageSkos = Paths.get("src/test/resources/models/language-valid.rdf");
    private static final Path invalidLanguageSkos = Paths.get("src/test/resources/models/language-invalid.rdf");
    private static final Path missingIdentifierFileTypeSkos = Paths.get("src/test/resources/models/missing-identifier-filetypes-skos.rdf");
    private static final Path missingOpCodeFileTypeSkos = Paths.get("src/test/resources/models/missing-op-code-filetypes-skos.rdf");

    private static final Path completeFileTypeSkos = Paths.get("src/test/resources/models/complete-filetypes-skos.rdf");
    private static final Path missingInformationFileTypeSkos = Paths.get("src/test/resources/models/missing-info-filetypes-skos.rdf");
    private static final Path emptyFileTypeSkos = Paths.get("src/test/resources/models/empty-filetypes-skos.rdf");
    private static final Path missingIdentifierLanguageSkos = Paths.get("src/test/resources/models/language-missing-identifier.rdf");
    private static final Path missingOpCodeLanguageSkos = Paths.get("src/test/resources/models/language-missing-op-code.rdf");


    private static Model completeLanguageSkosModel;
    private static Model validLanguageSkosModel;
    private static Model invalidLanguageSkosModel;
    private static Model missingIdentifierLanguageSkosModel;
    private static Model missingOpCodeLanguageSkosModel;

    private static Model completeFileTypeSkosModel;
    private static Model missingInformationFileTypeSkosModel;
    private static Model emptyFileTypeSkosModel;
    private static Model missingIdentifierFileTypeSkosModel;
    private static Model missingOpCodeFileTypeSkosModel;


    @Before
    public void setup() throws Exception {
        completeLanguageSkosModel = JenaUtils.read(new String(Files.readAllBytes(completeLanguageSkos)));
        validLanguageSkosModel = JenaUtils.read(new String(Files.readAllBytes(validLanguageSkos)));
        invalidLanguageSkosModel = JenaUtils.read(new String(Files.readAllBytes(invalidLanguageSkos)));
        missingIdentifierLanguageSkosModel = JenaUtils.read(new String(Files.readAllBytes(missingIdentifierLanguageSkos)));
        missingOpCodeLanguageSkosModel = JenaUtils.read(new String(Files.readAllBytes(missingOpCodeLanguageSkos)));

        completeFileTypeSkosModel = JenaUtils.read(new String(Files.readAllBytes(completeFileTypeSkos)));
        missingInformationFileTypeSkosModel = JenaUtils.read(new String(Files.readAllBytes(missingInformationFileTypeSkos)));
        emptyFileTypeSkosModel = JenaUtils.read(new String(Files.readAllBytes(emptyFileTypeSkos)));
        missingIdentifierFileTypeSkosModel = JenaUtils.read(new String(Files.readAllBytes(missingIdentifierFileTypeSkos)));
        missingOpCodeFileTypeSkosModel = JenaUtils.read(new String(Files.readAllBytes(missingOpCodeFileTypeSkos)));
    }

    @After
    public void tearDown() {
        JenaUtils.closeQuietly(completeLanguageSkosModel, validLanguageSkosModel, invalidLanguageSkosModel,
                missingIdentifierLanguageSkosModel, missingOpCodeLanguageSkosModel,
                completeFileTypeSkosModel, missingInformationFileTypeSkosModel, emptyFileTypeSkosModel,
                missingIdentifierFileTypeSkosModel, missingOpCodeFileTypeSkosModel);
    }


    @Test
    public void testValidateSkosConcepts() throws Exception {
        assertTrue(NalValidationUtils.validateSkosConcepts(completeLanguageSkosModel));
        assertTrue(NalValidationUtils.validateSkosConcepts(validLanguageSkosModel));
        assertFalse(NalValidationUtils.validateSkosConcepts(invalidLanguageSkosModel));
        assertFalse(NalValidationUtils.validateSkosConcepts(missingIdentifierLanguageSkosModel));
        assertFalse(NalValidationUtils.validateSkosConcepts(missingOpCodeLanguageSkosModel));

        assertTrue(NalValidationUtils.validateSkosConcepts(completeFileTypeSkosModel));
        assertTrue(NalValidationUtils.validateSkosConcepts(missingInformationFileTypeSkosModel));
        assertTrue(NalValidationUtils.validateSkosConcepts(emptyFileTypeSkosModel));
        assertFalse(NalValidationUtils.validateSkosConcepts(missingIdentifierFileTypeSkosModel));
        assertFalse(NalValidationUtils.validateSkosConcepts(missingOpCodeFileTypeSkosModel));
    }

    @Test
    public void testInvalidUri() throws Exception {
        Resource resource = ResourceFactory.createResource("http://publications.europa.eu/resourceµ/authority/corporate body/COMM");
        assertTrue(NalValidationUtils.isInvalidUri(resource));
    }

    @Test
    public void testInvalidUriInModel() throws Exception {
        Model model = ModelFactory.createDefaultModel();
        model.add(ResourceFactory.createResource("http://publications.europa.eu/resourceµ/authority/corporate body/COMM"),
                ResourceFactory.createProperty("property"),
                "test");
        List<String> invalidUris = NalValidationUtils.validateUris(model);
        assertNotNull(invalidUris);
        assertEquals(1, invalidUris.size());
    }

    @Test
    public void testValidUri() throws Exception {
        Resource resource = ResourceFactory.createResource("http://publications.europa.eu/resource/authority/corporate-body/COMM");
        assertFalse(NalValidationUtils.isInvalidUri(resource));
    }
}