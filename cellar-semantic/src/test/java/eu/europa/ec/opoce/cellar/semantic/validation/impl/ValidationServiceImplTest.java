/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.semantic.validation.impl
 *             FILE : ValidationServiceImplTest.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 12 11, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-12-11 06:31:18 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.semantic.validation.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.validation.MetadataValidationRequest;
import eu.europa.ec.opoce.cellar.cl.domain.validation.MetadataValidationResult;
import eu.europa.ec.opoce.cellar.cl.domain.validation.ValidateContext;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.semantic.validation.ValidationService;
import io.micrometer.core.instrument.MeterRegistry;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.riot.writer.RDFJSONWriter;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.UUID;

import static eu.europa.ec.opoce.cellar.cl.domain.validation.MetadataValidationResult.ModelType.DIRECT;
import static eu.europa.ec.opoce.cellar.semantic.validation.impl.ValidationServiceImpl.SEVERITY_PROPERTY;
import static org.apache.jena.rdf.model.ModelFactory.createDefaultModel;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author ARHS Developments
 */
public class ValidationServiceImplTest {
    private ValidationService validationService;
    private RestTemplate restTemplate;

    @Before
    public void setUp() throws Exception {
        ICellarConfiguration cellarConfiguration = mock(ICellarConfiguration.class);
        when(cellarConfiguration.getCellarServiceIntegrationValidationBaseUrl()).thenReturn("http://localhost");

        restTemplate = mock(RestTemplate.class);
        validationService = new ValidationServiceImpl(cellarConfiguration, Mockito.mock(MeterRegistry.class), restTemplate);
    }

    @Test
    public void testSuccessfulRequestValidation() throws Exception {
        ValidateContext validateContext = new ValidateContext();
        StringWriter stringWriter = new StringWriter();
        RDFJSONWriter.output(stringWriter, createDefaultModel().getGraph());
        validateContext.setModel(stringWriter.toString());

        UUID requestId = UUID.randomUUID();
        MetadataValidationResult metadataValidationResult = new MetadataValidationResult();
        metadataValidationResult.setCellarId(requestId.toString());
        metadataValidationResult.setModelType(DIRECT);
        metadataValidationResult.setValidJenaModelProvided(true);
        metadataValidationResult.setConformModelProvided(true);
        when(restTemplate.postForObject(any(String.class), eq(validateContext), eq(MetadataValidationResult.class))).thenReturn(metadataValidationResult);

        MetadataValidationRequest request = new MetadataValidationRequest(createDefaultModel());
        MetadataValidationResult result = validationService.validate(request);
        result.setModelType(DIRECT);

        assertNotNull(result);
        assertEquals(metadataValidationResult, result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void inputModelCannotBeNull() throws Exception {
        validationService.validate(null);
    }

    @Test
    public void testBadRequestException() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        MetadataValidationResult metadataValidationResult = new MetadataValidationResult();
        metadataValidationResult.setErrorMessage("Model cannot be empty");
        metadataValidationResult.setValidationResult(mapper.writeValueAsString(JenaUtils.toString(createDefaultModel(), RDFFormat.JSONLD)));
        StringWriter stringWriter = new StringWriter();
        mapper.writeValue(stringWriter, metadataValidationResult);

        HttpClientErrorException exception = new HttpClientErrorException(HttpStatus.BAD_REQUEST, "", stringWriter.toString().getBytes(), Charset.forName("UTF-8"));
        when(restTemplate.postForObject(any(String.class), any(ValidateContext.class), eq(MetadataValidationResult.class))).thenThrow(exception);

        MetadataValidationResult result = validationService.validate(new MetadataValidationRequest(createDefaultModel()));

        assertNotNull(result);
        assertEquals("Model cannot be empty", result.getErrorMessage());
    }

    @Test
    public void testServerErrorException() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        MetadataValidationResult metadataValidationResult = new MetadataValidationResult();
        metadataValidationResult.setModelType(DIRECT);
        metadataValidationResult.setValidationResult(mapper.writeValueAsString(JenaUtils.toString(createDefaultModel(), RDFFormat.JSONLD)));
        metadataValidationResult.setErrorMessage("Shapes not set up");
        StringWriter stringWriter = new StringWriter();
        mapper.writeValue(stringWriter, metadataValidationResult);

        HttpClientErrorException exception = new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "", stringWriter.toString().getBytes(), Charset.forName("UTF-8"));
        when(restTemplate.postForObject(any(String.class), any(ValidateContext.class), eq(MetadataValidationResult.class))).thenThrow(exception);

        MetadataValidationResult result = validationService.validate(new MetadataValidationRequest(createDefaultModel()));
        result.setModelType(DIRECT);

        assertNotNull(result);
        assertEquals("Shapes not set up", result.getErrorMessage());
    }

    @Test
    public void testShaclException() throws Exception {
        // If the response returned from Shacl contains a property which is not present in ValidationResult, the property should be ignored
        final String response = "{\n" +
                "    \"unMappedProperty\": \"894f8388-4a2d-45cf-8937-f8e9ae3a6847\",\n" +
                "    \"requestId\": \"894f8388-4a2d-45cf-8937-f8e9ae3a6847\",\n" +
                "    \"errorMessage\": \"Function http://topbraid.org/tosh#validatorForContext does not define a valid body\",\n" +
                "    \"validationResult\": \"\",\n" +
                "    \"validJenaModelProvided\": true,\n" +
                "    \"conformModelProvided\": false,\n" +
                "    \"totalTimeForInferenceInMilliSeconds\": 315,\n" +
                "    \"totalTimeForValidationInMilliSeconds\": -1\n" +
                "}";

        HttpClientErrorException exception = new HttpClientErrorException(HttpStatus.INTERNAL_SERVER_ERROR, "", response.getBytes(), Charset.forName("UTF-8"));
        when(restTemplate.postForObject(any(String.class), any(ValidateContext.class), eq(MetadataValidationResult.class))).thenThrow(exception);

        MetadataValidationResult result = validationService.validate(new MetadataValidationRequest(createDefaultModel()));

        assertNotNull(result);
        assertEquals("894f8388-4a2d-45cf-8937-f8e9ae3a6847", result.getRequestId());
        assertEquals("Function http://topbraid.org/tosh#validatorForContext does not define a valid body", result.getErrorMessage());
        assertEquals("", result.getValidationResult());
        assertTrue(result.isValidJenaModelProvided());
        assertFalse(result.isConformModelProvided());
        assertEquals(315, result.getTotalTimeForInferenceInMilliSeconds());
        assertEquals(-1, result.getTotalTimeForValidationInMilliSeconds());
    }

    @Test
    public void containViolationTest() throws Exception {
        final Model validationResultModel = validationModel("http://www.w3.org/ns/shacl#Violation");
        assertTrue(ValidationServiceImpl.containViolation(validationResultModel));
        assertFalse(ValidationServiceImpl.containViolation(createDefaultModel()));
    }

    @Test
    public void containWarningTest() throws Exception {
        final Model validationResultModel = validationModel("http://www.w3.org/ns/shacl#Warning");
        assertTrue(ValidationServiceImpl.containWarning(validationResultModel));
        assertFalse(ValidationServiceImpl.containWarning(createDefaultModel()));
    }

    @Test
    public void containInfoTest() throws Exception {
        final Model validationResultModel = validationModel("http://www.w3.org/ns/shacl#Info");
        assertTrue(ValidationServiceImpl.containInformation(validationResultModel));
        assertFalse(ValidationServiceImpl.containInformation(createDefaultModel()));
    }

    private Model validationModel(final String severity) {
        return createDefaultModel()
                .add(ResourceFactory.createResource(), SEVERITY_PROPERTY, ResourceFactory.createResource(severity));
    }
}