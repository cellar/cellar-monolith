/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.semantic.jena
 *             FILE : ZipTests.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 02 26, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-02-26 09:12:22 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.semantic.jena;

import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.semantic.spring.ZipEntryResource;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.zip.ZipFile;

/**
 * @author ARHS Developments
 */
public class ZipTests {

    @Test(expected = CellarException.class)
    public void zipResourceThrowsAnExceptionIfEntryNameDoesNotExist() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("001_comnat-JOL_2006_088_R_0063_0120100930125926Irr.zip").getFile());
        ZipFile zipFile = new ZipFile(file);
        new ZipEntryResource(zipFile, "not_existing_entry");
    }

    @Test
    public void zipResourceCreatedIfEntryNameExist() throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("001_comnat-JOL_2006_088_R_0063_0120100930125926Irr.zip").getFile());
        ZipFile zipFile = new ZipFile(file);
        new ZipEntryResource(zipFile, "sample_ProCat_Madagaskar_32006D0241.rdf");
    }

}
