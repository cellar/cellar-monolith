package eu.europa.ec.opoce.cellar.semantic.jena.transformers;

import org.apache.commons.collections15.Transformer;

import org.apache.jena.rdf.model.Statement;

/**
 * <p>Statement2ObjectUri class.</p>
 *
 */
public class Statement2ObjectUri implements Transformer<Statement, String> {

    /** Constant <code>statement2ObjectUri_null</code> */
    public static Transformer<Statement, String> statement2ObjectUri_null = new Statement2ObjectUri(true);
    /** Constant <code>statement2ObjectUri_fail</code> */
    public static Transformer<Statement, String> statement2ObjectUri_fail = new Statement2ObjectUri(false);

    private final boolean nullOnNoUri;

    /**
     * <p>Constructor for Statement2ObjectUri.</p>
     *
     * @param nullOnNoUri a boolean.
     */
    private Statement2ObjectUri(boolean nullOnNoUri) {
        this.nullOnNoUri = nullOnNoUri;
    }

    /** {@inheritDoc} */
    @Override
    public String transform(Statement input) {
        if (nullOnNoUri && !input.getObject().isURIResource())
            return null;
        return input.getResource().getURI();
    }
}
