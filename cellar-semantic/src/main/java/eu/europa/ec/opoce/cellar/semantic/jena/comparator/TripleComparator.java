package eu.europa.ec.opoce.cellar.semantic.jena.comparator;

import java.util.Comparator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.jena.graph.Node;
import org.apache.jena.graph.Triple;

/**
 * <p>TripleComparator class.</p>
 *
 */
public class TripleComparator implements Comparator<Triple> {

    private static final Logger LOGGER = LoggerFactory.getLogger(TripleComparator.class);

    private final NodeComparator nodeComparator = new NodeComparator();

    /** {@inheritDoc} */
    @Override
    public int compare(Triple thisOne, Triple other) {

        LOGGER.debug("Comparing {},{}", new Object[] {
                thisOne, other});

        final Node thisSubject = thisOne.getSubject();
        final Node otherSubject = other.getSubject();
        int result = nodeComparator.compare(thisSubject, otherSubject);
        if (result != 0) {
            return result;
        }

        final Node thisPredicate = thisOne.getPredicate();
        final Node otherPredicate = other.getPredicate();
        result = nodeComparator.compare(thisPredicate, otherPredicate);
        if (result != 0) {
            return result;
        }

        final Node thisObject = thisOne.getObject();
        final Node otherObject = other.getObject();
        result = nodeComparator.compare(thisObject, otherObject);
        return result;
    }
}
