package eu.europa.ec.opoce.cellar.semantic.config;

import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.exception.CellarSemanticException;
import eu.europa.ec.opoce.cellar.semantic.helper.MessageFormatter;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.FileSystemResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;

/**
 * Class that can be used to configure the application on start up.
 * This configuration class can be customized through various ways. It provides:
 * <ul>
 * <li>Constructors and methods to set, load and overload configuration and application properties.</li>
 * <li>Different hook methods that can be overridden.</li>
 * <li>A pluggin mechanism to add custom configuration logic after the initial configuration properties
 * have been loaded.</li>
 * </ul>
 */
public class ConfigurationSettings {

    private Resource configFile;

    private String configFilePath;

    private String configFilePathParameter;

    private Properties configurationProperties = new Properties();

    private Properties applicationProperties = new Properties();

    private boolean overloadProperties = true;

    private PropertyOverloader propertyOverloader = new SystemPropertyOverloader();

    private ConfigurationBean[] configurationBeans;

    private ResourceLoader resourceLoader = new FileSystemResourceLoader();

    private Logger logger;

    /**
     * <p>Constructor for ConfigurationSettings.</p>
     */
    public ConfigurationSettings() {
    }

    /**
     * <p>Constructor for ConfigurationSettings.</p>
     *
     * @param configFile a Java .properties file containing configuration properties that must be loaded
     */
    public ConfigurationSettings(File configFile) {
        this.configFile = new FileSystemResource(configFile);
    }

    /**
     * <p>Constructor for ConfigurationSettings.</p>
     *
     * @param configProperties configuration properties to set
     */
    public ConfigurationSettings(Properties configProperties) {
        ExceptionBuilder.throwExc(CellarSemanticException.class, configProperties == null);

        this.configurationProperties = configProperties;
    }

    /**
     * Sets a Java .properties file containing configuration properties that must be loaded.
     *
     * @param configFile a Java .properties file
     */
    public void setConfigFile(File configFile) {
        this.configFile = new FileSystemResource(configFile);
    }

    /**
     * Sets the full path of a Java .properties file containing configuration properties that must be loaded
     *
     * @param configFilePath the full path to a Java .properties file
     */
    public void setConfigFilePath(String configFilePath) {
        this.configFilePath = configFilePath;
    }

    /**
     * Sets the name of a system- or vm parameter that specifies the full path of a Java .properties file
     * containing configuration properties that must be loaded
     *
     * @param configFilePathParameter the name of a system- or vm parameter that specifies the full path of a Java .properties file
     */
    public void setConfigFilePathParameter(String configFilePathParameter) {
        this.configFilePathParameter = configFilePathParameter;
    }

    /**
     * Sets the configuration properties
     *
     * @param configurationProperties configuration properties
     */
    public void setConfigurationProperties(Properties configurationProperties) {
        ExceptionBuilder.throwExc(CellarSemanticException.class, configurationProperties == null);

        this.configurationProperties = configurationProperties;
    }

    /**
     * Sets the application properties
     *
     * @param applicationProperties application properties
     */
    public void setApplicationProperties(Properties applicationProperties) {
        ExceptionBuilder.throwExc(CellarSemanticException.class, applicationProperties == null);

        this.applicationProperties = applicationProperties;
    }

    /**
     * Sets the <code>ConfigurationBeans</code> that will be invoked after all properties have been loaded.
     * A {@link ConfigurationBean} can be used to perform custom configuration logic only
     * possible after the initial configuration properties have been loaded and to throw an
     * exception in the event of misconfiguration.
     *
     * @param configurationBeans beans implementing custom configuration logic
     * @see ConfigurationBean
     */
    public void setConfigurationBeans(ConfigurationBean[] configurationBeans) {
        this.configurationBeans = configurationBeans;
    }

    /**
     * Sets if the given and/or loaded configuration properties must be overloaded or not.
     * If <code>true</code> the configuration properties will be overloaded by a {@link PropertyOverloader}.
     * The default <code>PropertyOverloader</code> is a {@link SystemPropertyOverloader}.
     *
     * @param overloadProperties true if the configuration properties must be overloaded
     * @see #setPropertyOverloader
     * @see PropertyOverloader
     * @see SystemPropertyOverloader
     */
    public void setOverloadProperties(boolean overloadProperties) {
        this.overloadProperties = overloadProperties;
    }

    /**
     * Sets the {@link PropertyOverloader} to use for overloading the given and/or loaded configuration properties.
     * Default is {@link SystemPropertyOverloader}
     *
     * @param propertyOverloader a {@link eu.europa.ec.opoce.cellar.semantic.config.PropertyOverloader} object.
     * @see #setOverloadProperties
     * @see PropertyOverloader
     * @see SystemPropertyOverloader
     */
    public void setPropertyOverloader(PropertyOverloader propertyOverloader) {
        this.propertyOverloader = propertyOverloader;
    }

    /**
     * Gets the set, loaded and overloaded configuration properties and all application properties.
     * Configuration properties will be overriden by application properties in case they have the same name.
     *
     * @return a Properties object containing all configuration and application properties.
     */
    public final Properties getProperties() {
        Properties result = new Properties();
        CollectionUtils.mergePropertiesIntoMap(configurationProperties, result);
        CollectionUtils.mergePropertiesIntoMap(applicationProperties, result);
        return result;
    }

    /**
     * Gets a property with the given key.
     * Returns null if no such property.
     *
     * @param key the name of the property
     * @return the value of the property with the given key or null
     */
    public final String getProperty(String key) {
        String value = applicationProperties.getProperty(key);
        return value != null ? value : configurationProperties.getProperty(key);
    }

    /**
     * Loads the configuration.
     * This method must be called to load the configuration and to perform all custom configuration logic.
     * All properties are loaded. Hook methods are invoked and the custom configuration beans will do their job.
     *
     * @throws java.lang.Exception if any.
     */
    public synchronized final void load() throws Exception {
        try {
            initialize();
        } catch (Exception e) {
            handleException(e);
        }
    }

    /**
     * <p>initialize</p>
     *
     * @throws java.lang.Exception if any.
     */
    private void initialize() throws Exception {
        init();

        beforePropertiesLoad();

        loadProperties();

        afterPropertiesLoad();

        if (overloadProperties) {
            overloadProperties();
        }

        beforeConfigurationLoad();

        loadConfiguration();

        afterConfigurationLoad();
    }

    /**
     * <p>init</p>
     *
     * @throws java.lang.Exception if any.
     */
    protected void init() throws Exception {
        if (logger == null)
            this.logger = LoggerFactory.getLogger(ConfigurationSettings.class);
    }

    /**
     * <p>beforePropertiesLoad</p>
     *
     * @throws java.lang.Exception if any.
     */
    protected void beforePropertiesLoad() throws Exception {
    }

    /**
     * <p>loadProperties</p>
     */
    private void loadProperties() {
        if (!StringUtils.isBlank(configFilePathParameter)) {
            String configFilePath = getContextProperty(configFilePathParameter);
            if (!StringUtils.isBlank(configFilePath)) {
                // The specified parameter must be either a URL: either a special "classpath" pseudo URL or a standard URL("http:", "file:" )
                if (ResourceUtils.isUrl(configFilePath)) {
                    configFile = resourceLoader.getResource(configFilePath);
                } else { // or an absolute path
                    configFile = new FileSystemResource(configFilePath);
                }
            }
        }

        if (configFile == null && !StringUtils.isBlank(configFilePath)) {
            // The specified path is either a URL (special "classpath" pseudo URL or a standard URL)
            // or a relative path to the current VM working directory, even if starts start with a slash
            configFile = resourceLoader.getResource(configFilePath);
        }

        if (configFile != null && configFile.exists()) {
            info("Using configuration file '{}'.", configFile.getDescription());

            loadPropertiesFromFile(configurationProperties, configFile);
        }
    }

    /**
     * <p>afterPropertiesLoad</p>
     *
     * @throws java.lang.Exception if any.
     */
    protected void afterPropertiesLoad() throws Exception {
        if (overloadProperties) {
            info("Looking for overloading configuration properties(e.g. overriding system-, environment-, servletcontext- and/or custom properties)...");
        }
    }

    /**
     * <p>overloadProperties</p>
     *
     * @throws java.lang.Exception if any.
     */
    protected void overloadProperties() throws Exception {
        if (propertyOverloader == null)
            return;

        for (Enumeration<?> keys = configurationProperties.propertyNames(); keys.hasMoreElements(); ) {
            overload((String) keys.nextElement());
        }
    }

    /**
     * <p>beforeConfigurationLoad</p>
     *
     * @throws java.lang.Exception if any.
     */
    protected void beforeConfigurationLoad() throws Exception {
    }

    /**
     * <p>loadConfiguration</p>
     *
     * @throws java.lang.Exception if any.
     */
    protected void loadConfiguration() throws Exception {
        if (configurationBeans == null)
            return;

        for (ConfigurationBean configurationBean : configurationBeans) {
            configurationBean.configure(new Settings() {

                @Override
                public Properties getProperties() {
                    return getProperties();
                }
            });
        }
    }

    /**
     * <p>afterConfigurationLoad</p>
     *
     * @throws java.lang.Exception if any.
     */
    protected void afterConfigurationLoad() throws Exception {
    }

    /**
     * <p>handleException</p>
     *
     * @param e a {@link java.lang.Exception} object.
     * @throws java.lang.Exception if any.
     */
    protected void handleException(Exception e) throws Exception {
        throw e;
    }

    /**
     * <p>overload</p>
     *
     * @param key a {@link java.lang.String} object.
     */
    protected void overload(String key) {
        if (propertyOverloader == null)
            return;

        String value = getProperty(key);
        String overridingValue = propertyOverloader.overload(key, value);
        if (!StringUtils.equals(value, overridingValue)) {
            info("Overloading property '{}' with value '{}' [old value '{}']", key, overridingValue, value);

            configurationProperties.setProperty(key, overridingValue);
        }
    }

    /**
     * <p>getContextProperty</p>
     *
     * @param key a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    protected String getContextProperty(String key) {
        String value = System.getProperty(key);
        if (StringUtils.isBlank(value)) {
            value = System.getenv(key);
        }
        return value;
    }

    /**
     * <p>Getter for the field <code>logger</code>.</p>
     *
     * @return a {@link org.slf4j.Logger} object.
     */
    protected Logger getLogger() {
        return logger;
    }

    /**
     * <p>Setter for the field <code>logger</code>.</p>
     *
     * @param logger a {@link org.slf4j.Logger} object.
     */
    protected void setLogger(Logger logger) {
        this.logger = logger;
    }

    /**
     * <p>loadPropertiesFromFile</p>
     *
     * @param props    a {@link java.util.Properties} object.
     * @param resource a {@link org.springframework.core.io.Resource} object.
     */
    private static void loadPropertiesFromFile(Properties props, Resource resource) {
        try (InputStream stream = resource.getInputStream()) {
            props.load(stream);
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class)
                    .withMessage("Failed to load configuration properties from resource '{}'.")
                    .withMessageArgs(resource.getDescription()).withCause(e).build();
        }
    }

    /**
     * <p>info</p>
     *
     * @param format a {@link java.lang.String} object.
     * @param args   a {@link java.lang.Object} object.
     */
    protected void info(String format, Object... args) {
        if (logger != null)
            logger.info(format, args);
    }

    /**
     * <p>error</p>
     *
     * @param format a {@link java.lang.String} object.
     * @param args   a {@link java.lang.Object} object.
     */
    protected void error(String format, Object... args) {
        if (logger != null)
            logger.error(format, args);
    }

    /**
     * <p>error</p>
     *
     * @param t      a {@link java.lang.Throwable} object.
     * @param format a {@link java.lang.String} object.
     * @param args   a {@link java.lang.Object} object.
     */
    protected void error(Throwable t, String format, Object... args) {
        if (logger != null)
            logger.error(MessageFormatter.arrayFormat(format, args), t);
    }

}
