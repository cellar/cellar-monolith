package eu.europa.ec.opoce.cellar.semantic.ontology.property;

import static eu.europa.ec.opoce.cellar.common.Namespace.annotation;
import static eu.europa.ec.opoce.cellar.common.Namespace.cmr;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.ResourceFactory;

/**
 * <p>CellarAnnotationProperty interface.</p>
 */
public interface CellarAnnotationProperty {

    /**
     * Annotation property {@code all_numbered_levels}.
     */
    String annotation_allNumberedLevels = annotation + "all_numbered_levels";

    /**
     * Annotation property {@code is_facet}.
     */
    String annotation_isFacet = annotation + "is_facet";

    /**
     * Annotation property {@code to_be_indexed}.
     */
    String annotation_toBeIndexed = annotation + "to_be_indexed";

    /**
     * Annotation property {@code expand_on_indexing}.
     */
    String annotation_expandOnIndexing = annotation + "expand_on_indexing";

    /**
     * Annotation property {@code in_embedded_notice}.
     */
    String annotation_inEmbeddedNotice = annotation + "in_embedded_notice";

    /**
     * Annotation property {@code write-once}.
     */
    String annotation_writeOnce = annotation + "write-once";

    /**
     * Annotation property {@code in_notice}.
     */
    String annotation_inNotice = annotation + "in_notice";

    /**
     * CMR property {@code facetAcronym}.
     */
    String cmr_facetAcronym = cmr + "facetAcronym";

    /**
     * Annotation property {@code in_notice}.
     */
    String cmr_annotation_inNotice = cmr + "in_notice";

    /**
     * Annotation property {@code to_be_indexed}.
     */
    String cmr_annotation_toBeIndexed = cmr + "to_be_indexed";

    /** The Constant annotation_read_only. */
    String annotation_read_only = annotation + "read_only";

    /** The Constant annotation_read_onlyP. */
    Property annotation_read_onlyP = ResourceFactory.createProperty(annotation_read_only);
}
