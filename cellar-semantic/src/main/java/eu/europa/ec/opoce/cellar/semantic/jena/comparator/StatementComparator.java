package eu.europa.ec.opoce.cellar.semantic.jena.comparator;

import java.util.Comparator;

import org.apache.jena.rdf.model.Statement;

/**
 * <p>StatementComparator class.</p>
 *
 */
public class StatementComparator implements Comparator<Statement> {

    private ResourceComparator resourceComparator = new ResourceComparator();
    private PropertyComparator propertyComparator = new PropertyComparator();
    private RdfNodeComparator rdfNodeComparator = new RdfNodeComparator();

    /** {@inheritDoc} */
    @Override
    public int compare(Statement one, Statement other) {
        int result = resourceComparator.compare(one.getSubject(), other.getSubject());
        if (result != 0) {
            return result;
        }

        result = propertyComparator.compare(one.getPredicate(), other.getPredicate());
        if (result != 0) {
            return result;
        }

        return rdfNodeComparator.compare(one.getObject(), other.getObject());
    }
}
