package eu.europa.ec.opoce.cellar.semantic.ontology.data;

import eu.europa.ec.opoce.cellar.semantic.ontology.cache.Cardinality;
import eu.europa.ec.opoce.cellar.semantic.ontology.cache.annotation.MaxCardinalities;
import eu.europa.ec.opoce.cellar.semantic.ontology.cache.annotation.MinCardinalities;
import eu.europa.ec.opoce.cellar.semantic.ontology.cache.annotation.Properties;
import eu.europa.ec.opoce.cellar.semantic.ontology.cache.annotation.PropertyDatatype;
import eu.europa.ec.opoce.cellar.semantic.ontology.cache.annotation.SingleMaxCardinalityProperties;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarAnnotationProperty;

import java.util.SortedSet;

/**
 * <p>OntologyData interface.</p>
 */
public interface OntologyData {

    @PropertyDatatype
    String getDatatype(String propertyUri);

    //TODO: we should also pass the expected value of the annotation, now if the value is false, the property is still in the list !!!

    @Properties(annotations = CellarAnnotationProperty.annotation_isFacet)
    SortedSet<String> getFacetPropertyUris();

    @Properties(annotations = CellarAnnotationProperty.annotation_inEmbeddedNotice)
    SortedSet<String> getEmbeddedProperties();

    @Properties(annotations = CellarAnnotationProperty.annotation_expandOnIndexing)
    SortedSet<String> getEmbeddingNotices();

    @Properties(annotations = CellarAnnotationProperty.annotation_toBeIndexed)
    SortedSet<String> getToBeIndexedPropertyUris();

    @Properties(annotations = CellarAnnotationProperty.annotation_allNumberedLevels)
    SortedSet<String> getAllNumberedLevelsPropertyUris();

    @Properties(annotations = CellarAnnotationProperty.annotation_writeOnce)
    SortedSet<String> getWriteOncePropertyUris();

    @Properties(annotations = CellarAnnotationProperty.annotation_inNotice)
    SortedSet<String> getInNoticePropertyUris();

    @MinCardinalities
    Cardinality[] getMinCardinalities();

    @MaxCardinalities
    Cardinality[] getMaxCardinalities();

    @SingleMaxCardinalityProperties
    SortedSet<String> getSingleMaxCardinalityPropertyUris();
}
