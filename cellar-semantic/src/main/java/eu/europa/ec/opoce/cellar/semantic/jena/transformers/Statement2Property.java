package eu.europa.ec.opoce.cellar.semantic.jena.transformers;

import org.apache.commons.collections15.Transformer;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Statement;

/**
 * <p>Statement2Property class.</p>
 *
 */
public class Statement2Property implements Transformer<Statement, Property> {

    /** Constant <code>instance</code> */
    public static Statement2Property instance = new Statement2Property();

    /** {@inheritDoc} */
    @Override
    public Property transform(Statement input) {
        return input.getPredicate();
    }
}
