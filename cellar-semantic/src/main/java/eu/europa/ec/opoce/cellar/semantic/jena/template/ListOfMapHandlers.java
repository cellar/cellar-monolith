package eu.europa.ec.opoce.cellar.semantic.jena.template;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;

/**
 * <p>Abstract ListOfMapHandlers class.</p>
 *
 */
public abstract class ListOfMapHandlers {

    /**
     * <p>toSetOfResourcesResolver</p>
     *
     * @param param a {@link java.lang.String} object.
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.jena.template.ListOfMapHandler} object.
     */
    public static ListOfMapHandler<List<Resource>> toSetOfResourcesResolver(final String param) {
        return new ListOfMapHandler<List<Resource>>() {

            @Override
            public List<Resource> handle(List<Map<String, RDFNode>> resultSet) {
                @SuppressWarnings({
                        "rawtypes", "unchecked"})
                Set<Resource> resources = new HashSet(resultSet.size());

                for (Map<String, RDFNode> binding : resultSet) {
                    resources.add((Resource) binding.get(param));
                }
                return new ArrayList<Resource>(resources);
            }
        };
    }

}
