package eu.europa.ec.opoce.cellar.semantic.inferredmodel;

import eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration;
import eu.europa.ec.opoce.cellar.semantic.jena.transformers.QueryString2UpdateRequest;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.collections15.CollectionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import org.apache.jena.update.UpdateRequest;

@Service
/**
 * <p>InferredModelConfiguration class.</p>
 *
 */
public class InferredModelConfiguration {

    private static final Logger LOG = LogManager.getLogger(InferredModelConfiguration.class);

    private static final String INFERRED_MODEL_CONFIGURATION = "eu/europa/ec/opoce/cellar/semantic/config/inferred-model-configuration.xml";

    private List<UpdateRequest> defaultQueries;

    private UpdateRequest insertSuperclass;
    private UpdateRequest insertRange;
    private UpdateRequest insertDomain;
    private UpdateRequest insertRestriction;
    private UpdateRequest insertSuperproperty;
    private UpdateRequest insertInverse;
    private UpdateRequest annotationSuperproperty;

    /**
     * <p>init</p>
     */
    @PostConstruct
    private void init() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(INFERRED_MODEL_CONFIGURATION);
        context.start();

        //noinspection unchecked
        @SuppressWarnings("unchecked")
        final List<String> defaultQueries = context.getBean("defaultQueries", List.class);
        this.defaultQueries = CollectionUtils.collect(defaultQueries, QueryString2UpdateRequest.instance, new ArrayList<>());

        insertSuperclass = getUpdateRequest(context, "superclass");
        insertRange = getUpdateRequest(context, "range");
        insertDomain = getUpdateRequest(context, "domain");
        insertRestriction = getUpdateRequest(context, "restriction");
        insertSuperproperty = getUpdateRequest(context, "superproperty");
        insertInverse = getUpdateRequest(context, "inverse");
        annotationSuperproperty = getUpdateRequest(context, "annotation_superproperty");

        context.close();

        LOG.info(IConfiguration.CONFIG, "Inferred model loaded, {} default queries.", defaultQueries.size());
    }

    /**
     * <p>getUpdateRequest</p>
     *
     * @param context a {@link org.springframework.context.support.ClassPathXmlApplicationContext} object.
     * @param bean a {@link java.lang.String} object.
     * @return a {@link org.apache.jena.update.UpdateRequest} object.
     */
    private UpdateRequest getUpdateRequest(ClassPathXmlApplicationContext context, String bean) {
        String query = context.getBean(bean, String.class);
        return QueryString2UpdateRequest.instance.transform(query);
    }

    /**
     * <p>Getter for the field <code>defaultQueries</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<UpdateRequest> getDefaultQueries() {
        return defaultQueries;
    }

    /**
     * <p>Getter for the field <code>insertSuperclass</code>.</p>
     *
     * @return a {@link org.apache.jena.update.UpdateRequest} object.
     */
    public UpdateRequest getInsertSuperclass() {
        return insertSuperclass;
    }

    /**
     * <p>Getter for the field <code>insertRange</code>.</p>
     *
     * @return a {@link org.apache.jena.update.UpdateRequest} object.
     */
    public UpdateRequest getInsertRange() {
        return insertRange;
    }

    /**
     * <p>Getter for the field <code>insertDomain</code>.</p>
     *
     * @return a {@link org.apache.jena.update.UpdateRequest} object.
     */
    public UpdateRequest getInsertDomain() {
        return insertDomain;
    }

    /**
     * <p>Getter for the field <code>insertRestriction</code>.</p>
     *
     * @return a {@link org.apache.jena.update.UpdateRequest} object.
     */
    public UpdateRequest getInsertRestriction() {
        return insertRestriction;
    }

    /**
     * <p>Getter for the field <code>insertSuperproperty</code>.</p>
     *
     * @return a {@link org.apache.jena.update.UpdateRequest} object.
     */
    public UpdateRequest getInsertSuperproperty() {
        return insertSuperproperty;
    }

    /**
     * <p>Getter for the field <code>insertInverse</code>.</p>
     *
     * @return a {@link org.apache.jena.update.UpdateRequest} object.
     */
    public UpdateRequest getInsertInverse() {
        return insertInverse;
    }
    
    public UpdateRequest getAnnotationSuperproperty() {
        return annotationSuperproperty;
    }
}
