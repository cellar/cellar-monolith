package eu.europa.ec.opoce.cellar.semantic.jena.comparator;

import org.apache.jena.graph.BlankNodeId;
import org.apache.jena.graph.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Comparator;

/**
 * <p>NodeComparator class.</p>
 *
 */
public class NodeComparator implements Comparator<Node> {

    private static final Logger LOGGER = LoggerFactory.getLogger(NodeComparator.class);

    /** {@inheritDoc} */
    @Override
    public int compare(Node thisOne, Node other) {

        LOGGER.debug("Comparing {},{}", new Object[] {
                thisOne, other});

        final boolean thisIsLiteral = thisOne.isLiteral();
        final boolean otherIsLiteral = other.isLiteral();
        if (thisIsLiteral && otherIsLiteral) {
            final String thisLiteralLexicalForm = thisOne.getLiteralLexicalForm();
            final String otherLiteralLexicalForm = other.getLiteralLexicalForm();
            LOGGER.debug("Comparing Literals in Lexical Form {},{}", new Object[] {
                    thisLiteralLexicalForm, otherLiteralLexicalForm});
            return thisLiteralLexicalForm.compareTo(otherLiteralLexicalForm);
        }
        final boolean thisIsUri = thisOne.isURI();
        final boolean otherIsUri = other.isURI();
        if (thisIsUri && otherIsUri) {
            final String thisUri = thisOne.getURI();
            final String otherUri = other.getURI();
            LOGGER.debug("Comparing URIs {},{}", new Object[] {
                    thisUri, otherUri});
            return thisUri.compareTo(otherUri);
        }
        if (thisIsLiteral && otherIsLiteral) {
            final String thisBlankNodeLabel = thisOne.getBlankNodeLabel();
            final String otherBlankNodeLabel = other.getBlankNodeLabel();
            LOGGER.debug("Comparing blank nodes {},{}", new Object[] {
                    thisBlankNodeLabel, otherBlankNodeLabel});
            return thisBlankNodeLabel.compareTo(otherBlankNodeLabel);
        }
        final boolean otherIsBlank = other.isBlank();
        if (thisIsUri && (otherIsLiteral || otherIsBlank)) {
            LOGGER.debug("Comparing thisIsUri && (otherIsLiteral || otherIsBlank)");
            return -1;
        }
        if (thisIsLiteral && otherIsBlank) {
            LOGGER.debug("Comparing if (thisIsLiteral && otherIsBlank)");
            return -1;
        }
        final boolean thisOneIsBlank = thisOne.isBlank();
        if (thisOneIsBlank && otherIsBlank) {
            final BlankNodeId thisBlankNodeId = thisOne.getBlankNodeId();
            final BlankNodeId otherBlankNodeId = other.getBlankNodeId();
            final String thisLabelString = thisBlankNodeId.getLabelString();
            final String otherLabelString = otherBlankNodeId.getLabelString();
            return thisLabelString.compareTo(otherLabelString);
        }
        return 1;
    }

}
