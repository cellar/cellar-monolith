package eu.europa.ec.opoce.cellar.semantic.validation;

import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.exception.CellarSemanticException;
import eu.europa.ec.opoce.cellar.semantic.helper.StringHelper;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>ValidationResults class.</p>
 */
public class ValidationResults {

    /**
     * Constant <code>log</code>
     */
    private static final Logger log = LoggerFactory.getLogger(ValidationResults.class);

    public enum Level {
        INFO, WARN, ERROR, FATAL
    }

    private Level minimumLevel = Level.INFO;
    private List<ValidationResult> validationResults = new ArrayList<ValidationResult>();

    /**
     * <p>Setter for the field <code>minimumLevel</code>.</p>
     *
     * @param minimumLevel a {@link eu.europa.ec.opoce.cellar.semantic.validation.ValidationResults.Level} object.
     */
    public void setMinimumLevel(Level minimumLevel) {
        Assert.notNull(minimumLevel);
        this.minimumLevel = minimumLevel;
    }

    /**
     * <p>getWorstLevel</p>
     *
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.validation.ValidationResults.Level} object.
     */
    public Level getWorstLevel() {
        ValidationResult validationResult = getWorstValidationResult();
        return validationResult != null ? validationResult.getLevel() : null;
    }

    /**
     * <p>getWorstMessage</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getWorstMessage() {
        ValidationResult validationResult = getWorstValidationResult();
        return validationResult != null ? validationResult.getMessage() : null;
    }

    /**
     * <p>getWorstValidationResult</p>
     *
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.validation.ValidationResults.ValidationResult} object.
     */
    public ValidationResult getWorstValidationResult() {
        ValidationResult result = null;
        for (ValidationResult validationResult : validationResults) {
            result = result == null ? validationResult
                    : validationResult.getLevel().ordinal() > validationResult.getLevel().ordinal() ? validationResult : result;
        }
        return result;
    }

    /**
     * <p>matchLevel</p>
     *
     * @param level a {@link eu.europa.ec.opoce.cellar.semantic.validation.ValidationResults.Level} object.
     * @return a boolean.
     */
    public boolean matchLevel(Level level) {
        Assert.notNull(level);

        for (ValidationResult validationResult : validationResults) {
            if (level.ordinal() <= validationResult.getLevel().ordinal()) {
                return true;
            }
        }

        return false;
    }

    /**
     * <p>add</p>
     *
     * @param level   a {@link eu.europa.ec.opoce.cellar.semantic.validation.ValidationResults.Level} object.
     * @param message a {@link java.lang.String} object.
     */
    public void add(Level level, String message) {
        addValidationResult(new ValidationResult(level, message));
    }

    /**
     * <p>add</p>
     *
     * @param level      a {@link eu.europa.ec.opoce.cellar.semantic.validation.ValidationResults.Level} object.
     * @param message    a {@link java.lang.String} object.
     * @param parameters a {@link java.lang.Object} object.
     */
    public void add(Level level, String message, Object... parameters) {
        addValidationResult(new ValidationResult(level, message, parameters));
    }

    /**
     * <p>add</p>
     *
     * @param level     a {@link eu.europa.ec.opoce.cellar.semantic.validation.ValidationResults.Level} object.
     * @param message   a {@link java.lang.String} object.
     * @param exception a {@link java.lang.Exception} object.
     */
    public void add(Level level, String message, Exception exception) {
        addValidationResult(new ValidationResult(level, message, exception));
    }

    /**
     * <p>add</p>
     *
     * @param level      a {@link eu.europa.ec.opoce.cellar.semantic.validation.ValidationResults.Level} object.
     * @param message    a {@link java.lang.String} object.
     * @param parameters an array of {@link java.lang.Object} objects.
     * @param exception  a {@link java.lang.Exception} object.
     */
    public void add(Level level, String message, Object[] parameters, Exception exception) {
        addValidationResult(new ValidationResult(level, message, parameters, exception));
    }

    /**
     * <p>add</p>
     *
     * @param validationResults a {@link eu.europa.ec.opoce.cellar.semantic.validation.ValidationResults} object.
     */
    public void add(ValidationResults validationResults) {
        Assert.notNull(validationResults);

        for (ValidationResult validationResult : validationResults.validationResults) {
            addValidationResult(validationResult);
        }
    }

    /**
     * <p>addValidationResult</p>
     *
     * @param validationResult a {@link eu.europa.ec.opoce.cellar.semantic.validation.ValidationResults.ValidationResult} object.
     */
    private void addValidationResult(ValidationResult validationResult) {
        if (validationResult.level.ordinal() < minimumLevel.ordinal()) {
            return;
        }

        validationResults.add(validationResult);
    }

    /**
     * <p>log</p>
     */
    public void log() {
        log(log);
    }

    /**
     * <p>log</p>
     *
     * @param logger a {@link org.slf4j.Logger} object.
     */
    public void log(Logger logger) {
        for (ValidationResult validationResult : validationResults) {
            validationResult.log(logger);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Validation Results: " + StringHelper.newline + StringUtils.join(validationResults, StringHelper.newline);
    }

    private static class ValidationResult {

        private Level level;
        private String message;
        private Object[] parameters;
        private Exception exception;

        private ValidationResult(Level level, String message) {
            Assert.notNull(level, "Level cannot be 'null'.");
            Assert.notNull(message, "Message cannot be 'null'.");
            this.level = level;
            this.message = message;
        }

        private ValidationResult(Level level, String message, Object[] parameters) {
            this(level, message);
            this.parameters = parameters;
        }

        private ValidationResult(Level level, String message, Exception exception) {
            this(level, message);
            this.exception = exception;
        }

        private ValidationResult(Level level, String message, Object[] parameters, Exception exception) {
            this(level, message);
            this.parameters = parameters;
            this.exception = exception;
        }

        public Level getLevel() {
            return level;
        }

        public String getMessage() {
            return StringHelper.format(message, parameters);
        }

        public void log(Logger logger) {
            switch (level) {
                case INFO:
                    logger.info(StringHelper.format(message, parameters), exception);
                    break;
                case WARN:
                    logger.warn(StringHelper.format(message, parameters), exception);
                    break;
                case ERROR:
                    logger.error(StringHelper.format(message, parameters), exception);
                    break;
                case FATAL:
                    logger.error(StringHelper.format(message, parameters), exception);
                    break;
                default:
                    throw ExceptionBuilder.get(CellarSemanticException.class).withMessage("Unknown level '{}'.")
                            .withMessageArgs(level).build();
            }
        }
    }

}
