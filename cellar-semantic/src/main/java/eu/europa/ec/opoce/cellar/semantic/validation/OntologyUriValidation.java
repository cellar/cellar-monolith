package eu.europa.ec.opoce.cellar.semantic.validation;

import eu.europa.ec.opoce.cellar.semantic.jena.transformers.RdfNode2Uri;
import eu.europa.ec.opoce.cellar.semantic.pattern.Command;
import org.apache.commons.collections15.Predicate;
import org.apache.commons.collections15.functors.NotNullPredicate;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.vocabulary.DC_11;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

import static org.apache.commons.collections15.CollectionUtils.collect;
import static org.apache.commons.collections15.CollectionUtils.select;

/**
 * <p>OntologyUriValidation class.</p>
 *
 */
public class OntologyUriValidation implements Command<ValidationResults> {

    /** Constant <code>defaultOntologyUris</code> */
    private static final Collection<String> defaultOntologyUris = Arrays.asList(OWL.getURI(), RDF.getURI(), RDFS.getURI(), DC_11.getURI());

    private final OntModel ontologyModel;
    private final Collection<String> allowedUriPrefixes;
    private final ValidationResults validationResults;

    private boolean useDefaultOntologyUris = true;

    private ValidationResults.Level wrongOntologyUriClassLevel = ValidationResults.Level.ERROR;
    private ValidationResults.Level wrongOntologyUriPropertyLevel = ValidationResults.Level.ERROR;

    private final Predicate<String> wrongOntologyUriFilter = new Predicate<String>() {

        @Override
        public boolean evaluate(String clazz) {
            for (String allowedUriPrefix : allowedUriPrefixes) {
                if (clazz.startsWith(allowedUriPrefix)) {
                    return false;
                }
            }
            return true;
        }
    };

    /**
     * <p>Constructor for OntologyUriValidation.</p>
     *
     * @param validationResults a {@link eu.europa.ec.opoce.cellar.semantic.validation.ValidationResults} object.
     * @param ontologyModel a {@link org.apache.jena.ontology.OntModel} object.
     * @param allowedUriPrefixes a {@link java.lang.String} object.
     */
    public OntologyUriValidation(ValidationResults validationResults, OntModel ontologyModel, String... allowedUriPrefixes) {
        Assert.notNull(validationResults, "ValidationResults cannot be empty.");
        Assert.notNull(ontologyModel, "Ontology model cannot be empty.");
        Assert.notNull(allowedUriPrefixes, "Allowed URI prefixes collection cannot be empty.");

        this.ontologyModel = ontologyModel;
        this.validationResults = validationResults;
        this.allowedUriPrefixes = new ArrayList<String>();
        this.allowedUriPrefixes.addAll(Arrays.asList(allowedUriPrefixes));
    }

    /**
     * <p>Setter for the field <code>useDefaultOntologyUris</code>.</p>
     *
     * @param useDefaultOntologyUris a boolean.
     */
    public void setUseDefaultOntologyUris(boolean useDefaultOntologyUris) {
        this.useDefaultOntologyUris = useDefaultOntologyUris;
    }

    /**
     * <p>Setter for the field <code>wrongOntologyUriClassLevel</code>.</p>
     *
     * @param wrongOntologyUriClassLevel a {@link eu.europa.ec.opoce.cellar.semantic.validation.ValidationResults.Level} object.
     */
    public void setWrongOntologyUriClassLevel(ValidationResults.Level wrongOntologyUriClassLevel) {
        this.wrongOntologyUriClassLevel = wrongOntologyUriClassLevel;
    }

    /**
     * <p>Setter for the field <code>wrongOntologyUriPropertyLevel</code>.</p>
     *
     * @param wrongOntologyUriPropertyLevel a {@link eu.europa.ec.opoce.cellar.semantic.validation.ValidationResults.Level} object.
     */
    public void setWrongOntologyUriPropertyLevel(ValidationResults.Level wrongOntologyUriPropertyLevel) {
        this.wrongOntologyUriPropertyLevel = wrongOntologyUriPropertyLevel;
    }

    /** {@inheritDoc} */
    @Override
    public ValidationResults execute() {
        if (useDefaultOntologyUris) {
            allowedUriPrefixes.addAll(defaultOntologyUris);
        }

        validateUriOfClasses();
        validateUriOfProperties();

        return validationResults;
    }

    /**
     * <p>validateUriOfClasses</p>
     */
    private void validateUriOfClasses() {
        final Collection<String> classes = collect(ontologyModel.listClasses().toSet(), RdfNode2Uri.instance, new HashSet<String>());
        Collection<String> problemClasses = select(select(classes, NotNullPredicate.getInstance()), wrongOntologyUriFilter);

        if (!problemClasses.isEmpty()) {
            validationResults.add(wrongOntologyUriClassLevel,
                    "Some classes do not match the required ontology URI set. \n\tAllowed URIs: {}\n\tProblem classes: {}",
                    allowedUriPrefixes, problemClasses);
        }
    }

    /**
     * <p>validateUriOfProperties</p>
     */
    private void validateUriOfProperties() {
        final Collection<String> properties = collect(ontologyModel.listOntProperties().toSet(), RdfNode2Uri.instance,
                new HashSet<String>());
        Collection<String> problemProperties = select(select(properties, NotNullPredicate.getInstance()), wrongOntologyUriFilter);

        if (!problemProperties.isEmpty()) {
            validationResults.add(wrongOntologyUriPropertyLevel,
                    "Some properties do not match the required ontology URI set. \n\tAllowed URIs: {}\n\tProblem properties: {}",
                    allowedUriPrefixes, problemProperties);
        }
    }
}
