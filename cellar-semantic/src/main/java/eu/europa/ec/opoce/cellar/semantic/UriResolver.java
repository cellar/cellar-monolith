package eu.europa.ec.opoce.cellar.semantic;

import org.springframework.core.io.Resource;

/**
 * <p>UriResolver interface.</p>
 *
 */
public interface UriResolver {

    /**
     * <p>resolveUri</p>
     *
     * @param uri a {@link java.lang.String} object.
     * @return a {@link org.springframework.core.io.Resource} object.
     */
    Resource resolveUri(String uri);
}
