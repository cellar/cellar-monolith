package eu.europa.ec.opoce.cellar.semantic.config;

import java.io.File;
import java.util.Properties;

import org.springframework.beans.factory.InitializingBean;

/**
 * Class that extends the {@link ConfigurationSettings} class so it easily can be uses and configured
 * as a bean in a Spring context.
 * Implements the {@link org.springframework.beans.factory.InitializingBean} interface to load the
 * <code>ConfigurationSettings</code> after all its properties have been set by a BeanFactory.
 */
public class ConfigurationSettingsBean extends ConfigurationSettings implements InitializingBean {

    /**
     * <p>Constructor for ConfigurationSettingsBean.</p>
     */
    public ConfigurationSettingsBean() {
    }

    /**
     * <p>Constructor for ConfigurationSettingsBean.</p>
     *
     * @param configFile a Java .properties file containing configuration properties that must be loaded
     */
    public ConfigurationSettingsBean(File configFile) {
        super(configFile);
    }

    /**
     * <p>Constructor for ConfigurationSettingsBean.</p>
     *
     * @param configProperties configuration properties to set
     */
    public ConfigurationSettingsBean(Properties configProperties) {
        super(configProperties);
    }

    /** {@inheritDoc} */
    @Override
    public void afterPropertiesSet() throws Exception {
        load();
    }

}
