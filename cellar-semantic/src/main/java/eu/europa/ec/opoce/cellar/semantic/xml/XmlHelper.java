package eu.europa.ec.opoce.cellar.semantic.xml;

import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.exception.CellarSemanticException;
import net.sf.saxon.TransformerFactoryImpl;
import net.sf.saxon.event.Sink;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import javax.xml.XMLConstants;
import javax.xml.transform.*;
import javax.xml.transform.sax.SAXSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * <p>XmlHelper class.</p>
 */
public class XmlHelper {

    /**
     * Constant <code>xml_declaration="<?xml version=\"1.0\" encoding=\"UTF-8\"{trunked}</code>
     */
    public static final String xml_declaration = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";

    /**
     * Constant <code>xmlValidatorTransformer</code>
     */
    private static final ThreadLocal<Transformer> xmlValidatorTransformer = ThreadLocal.withInitial(() -> {
        try {
            TransformerFactory trfactory = TransformerFactory.newInstance(TransformerFactoryImpl.class.getName(), TransformerFactoryImpl.class.getClassLoader());
            trfactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);

            return trfactory.newTransformer();
        } catch (TransformerConfigurationException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    });

    /**
     * <p>Constructor for XmlHelper.</p>
     */
    private XmlHelper() {
    }

    /**
     * <p>isValidXml</p>
     *
     * @param xmlFile a {@link java.io.File} object.
     * @return a boolean.
     */
    public static boolean isValidXml(File xmlFile) throws SAXException, FileNotFoundException {
        try {
            XMLReader reader= XMLReaderFactory.createXMLReader();
            EntityResolver ent = new EntityResolver() {

                @Override
                public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {

                    return new InputSource();
                }
            };

            reader.setEntityResolver(ent);
            Source saxSource=new SAXSource(reader,new InputSource(new FileInputStream(xmlFile)));
            xmlValidatorTransformer.get().transform(saxSource, new Sink());
            return true;
        } catch (TransformerException ignore) {
            return false;
        }
    }

}
