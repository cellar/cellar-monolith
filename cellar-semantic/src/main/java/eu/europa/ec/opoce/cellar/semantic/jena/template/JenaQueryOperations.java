package eu.europa.ec.opoce.cellar.semantic.jena.template;

import org.apache.jena.query.Query;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import eu.europa.ec.opoce.cellar.common.closure.RPClosure;

import java.util.List;
import java.util.Map;

/**
 * <p>JenaQueryOperations interface.</p>
 *
 */
public interface JenaQueryOperations {

    /**
     * <p>select</p>
     *
     * @param sparql a {@link org.apache.jena.query.Query} object.
     * @return a {@link java.util.List} object.
     */
    List<Map<String, RDFNode>> select(Query sparql);

    /**
     * <p>select</p>
     *
     * @param sparql a {@link org.apache.jena.query.Query} object.
     * @param resultSetResolver a {@link eu.europa.ec.opoce.cellar.semantic.jena.template.JenaResultSetHandler} object.
     * @param <T> a T object.
     * @return a T object.
     */
    <T> T select(Query sparql, JenaResultSetHandler<T> resultSetResolver);

    /**
     * <p>select</p>
     *
     * @param sparql a {@link org.apache.jena.query.Query} object.
     * @param listOfMapHandler a {@link eu.europa.ec.opoce.cellar.semantic.jena.template.ListOfMapHandler} object.
     * @param <T> a T object.
     * @return a T object.
     */
    <T> T select(Query sparql, ListOfMapHandler<T> listOfMapHandler);

    /**
     * <p>ask</p>
     *
     * @param sparql a {@link org.apache.jena.query.Query} object.
     * @return a boolean.
     */
    boolean ask(Query sparql);

    /**
     * <p>ask</p>
     *
     * @param sparql a {@link org.apache.jena.query.Query} object.
     * @param booleanExtractor a {@link eu.europa.ec.opoce.cellar.semantic.jena.template.JenaBooleanHandler} object.
     * @param <T> a T object.
     * @return a T object.
     */
    <T> T ask(final Query sparql, final JenaBooleanHandler<T> booleanExtractor);

    /**
     * <p>construct</p>
     *
     * @param sparql a {@link org.apache.jena.query.Query} object.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    Model construct(final Query sparql);

    /**
     * <p>construct</p>
     *
     * @param sparql a {@link org.apache.jena.query.Query} object.
     * @param resultModel a {@link org.apache.jena.rdf.model.Model} object.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    Model construct(Query sparql, Model resultModel);

    /**
     * <p>construct</p>
     *
     * @param sparql a {@link org.apache.jena.query.Query} object.
     * @param resultModel a {@link org.apache.jena.rdf.model.Model} object.
     * @param closeInput a boolean.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    Model construct(Query sparql, Model resultModel, boolean closeInput);

    /**
     * <p>construct</p>
     *
     * @param sparql a {@link org.apache.jena.query.Query} object.
     * @param modelCallBack a {@link RPClosure} object.
     * @param closeResult a boolean.
     * @param <T> a T object.
     * @return a T object.
     */
    <T> T construct(final Query sparql, final RPClosure<T, Model> modelCallBack, boolean closeResult);

    /**
     * <p>execute</p>
     *
     * @param modelOperations a {@link RPClosure} object.
     * @param <T> a T object.
     * @return a T object.
     */
    <T> T execute(RPClosure<T, Model> modelOperations);

}
