package eu.europa.ec.opoce.cellar.semantic.spring;

import org.apache.commons.collections15.Predicate;
import org.springframework.core.io.Resource;

/**
 * <p>ResourceFilenameFilter class.</p>
 *
 */
public class ResourceFilenameFilter implements Predicate<Resource> {

    private String regex;

    /**
     * <p>Constructor for ResourceFilenameFilter.</p>
     *
     * @param regex a {@link java.lang.String} object.
     */
    public ResourceFilenameFilter(String regex) {
        this.regex = regex;
    }

    /** {@inheritDoc} */
    @Override
    public boolean evaluate(Resource resource) {
        return resource.getFilename().matches(regex);
    }
}
