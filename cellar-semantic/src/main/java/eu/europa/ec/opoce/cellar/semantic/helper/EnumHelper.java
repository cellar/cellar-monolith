package eu.europa.ec.opoce.cellar.semantic.helper;

import org.apache.commons.collections15.Transformer;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;

/**
 * <p>EnumHelper class.</p>
 *
 */
public class EnumHelper {

    /** Constant <code>enum2Name</code> */
    public static final Transformer<Enum<?>, String> enum2Name = new EnumToName();

    /**
     * <p>Constructor for EnumHelper.</p>
     */
    private EnumHelper() {
    }

    /**
     * <p>exists</p>
     *
     * @param enumType a {@link java.lang.Class} object.
     * @param value a {@link java.lang.String} object.
     * @param <T> a T object.
     * @return a boolean.
     */
    public static <T extends Enum<T>> boolean exists(Class<T> enumType, String value) {
        T t = valueOf(enumType, value);
        return null != t;
    }

    /**
     * <p>valueOf</p>
     *
     * @param enumType a {@link java.lang.Class} object.
     * @param value a {@link java.lang.String} object.
     * @param <T> a T object.
     * @return a T object.
     */
    @Nullable
    public static <T extends Enum<T>> T valueOf(Class<T> enumType, String value) {
        if (null == value)
            return null;
        try {
            return Enum.valueOf(enumType, value);
        } catch (IllegalArgumentException ignored) {
            return null;
        }
    }

    /**
     * <p>toList</p>
     *
     * @param enumType a {@link java.lang.Class} object.
     * @param values a {@link java.lang.Iterable} object.
     * @param <T> a T object.
     * @return a {@link java.util.List} object.
     */
    public static <T extends Enum<T>> List<T> toList(Class<T> enumType, Iterable<String> values) {
        return fill(enumType, values, new ArrayList<T>());
    }

    /**
     * <p>toEnumSet</p>
     *
     * @param enumType a {@link java.lang.Class} object.
     * @param values a {@link java.lang.Iterable} object.
     * @param <T> a T object.
     * @return a {@link java.util.EnumSet} object.
     */
    public static <T extends Enum<T>> EnumSet<T> toEnumSet(Class<T> enumType, Iterable<String> values) {
        return fill(enumType, values, EnumSet.noneOf(enumType));
    }

    /**
     * <p>fill</p>
     *
     * @param enumType a {@link java.lang.Class} object.
     * @param values a {@link java.lang.Iterable} object.
     * @param toFill a C object.
     * @param <C> a C object.
     * @param <T> a T object.
     * @return a C object.
     */
    private static <C extends Collection<T>, T extends Enum<T>> C fill(Class<T> enumType, Iterable<String> values, C toFill) {
        for (String value : values) {
            T enumValue = valueOf(enumType, value);
            if (null == enumValue)
                continue;
            toFill.add(enumValue);
        }
        return toFill;
    }

    private static class EnumToName implements Transformer<Enum<?>, String> {

        @Override
        public String transform(Enum<?> input) {
            return input.name();
        }
    }

}
