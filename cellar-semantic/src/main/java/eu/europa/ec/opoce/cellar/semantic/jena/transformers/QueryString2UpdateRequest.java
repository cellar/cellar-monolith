package eu.europa.ec.opoce.cellar.semantic.jena.transformers;

import org.apache.commons.collections15.Transformer;

import org.apache.jena.update.UpdateFactory;
import org.apache.jena.update.UpdateRequest;

/**
 * <p>QueryString2UpdateRequest class.</p>
 *
 */
public class QueryString2UpdateRequest implements Transformer<String, UpdateRequest> {

    /** Constant <code>instance</code> */
    public static Transformer<String, UpdateRequest> instance = new QueryString2UpdateRequest();
    /** Constant <code>queryString2UpdateRequest</code> */
    public static Transformer<String, UpdateRequest> queryString2UpdateRequest = new QueryString2UpdateRequest();

    /** {@inheritDoc} */
    @Override
    public UpdateRequest transform(String input) {
        return UpdateFactory.create(input);
    }
}
