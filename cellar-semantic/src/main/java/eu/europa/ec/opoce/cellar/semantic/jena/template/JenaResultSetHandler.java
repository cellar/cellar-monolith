package eu.europa.ec.opoce.cellar.semantic.jena.template;

import org.apache.jena.query.ResultSet;

/**
 * <p>JenaResultSetHandler interface.</p>
 *
 */
public interface JenaResultSetHandler<T> {

    /**
     * <p>handle</p>
     *
     * @param resultSet a {@link org.apache.jena.query.ResultSet} object.
     * @param <T> a T object.
     * @return a T object.
     */
    public T handle(ResultSet resultSet);

}
