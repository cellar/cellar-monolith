package eu.europa.ec.opoce.cellar.semantic.spring;

import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.exception.CellarSemanticException;
import org.springframework.core.io.AbstractResource;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * <p>ZipEntryResource class.</p>
 */
public class ZipEntryResource extends AbstractResource {

    private ZipFile zipFile;
    private ZipEntry zipEntry;

    /**
     * <p>Constructor for ZipEntryResource.</p>
     *
     * @param zipFile   a object.
     * @param entryName a {@link java.lang.String} object.
     */
    public ZipEntryResource(ZipFile zipFile, String entryName) {
        this.zipFile = zipFile;
        this.zipEntry = zipFile.getEntry(entryName.replace('\\', '/'));
        if (null == this.zipEntry)
            this.zipEntry = zipFile.getEntry(entryName.replace('/', '\\'));

        if (null == this.zipEntry) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withMessage("ZipEntry not found: {}")
                    .withMessageArgs(entryName).build();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean exists() {
        return true;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getDescription() {
        return "ZipEntryResource: " + zipEntry.getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getFilename() {
        return new File(zipEntry.getName()).getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InputStream getInputStream() throws IOException {
        return zipFile.getInputStream(zipEntry);
    }
}
