package eu.europa.ec.opoce.cellar.semantic.jena.template;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import eu.europa.ec.opoce.cellar.common.closure.RPClosure;

import java.util.List;
import java.util.Map;

/**
 * <p>JenaOperations interface.</p>
 *
 */
public interface JenaOperations {

    /**
     * <p>select</p>
     *
     * @param sparql a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    List<Map<String, RDFNode>> select(String sparql);

    /**
     * <p>select</p>
     *
     * @param sparql a {@link java.lang.String} object.
     * @param resultSetResolver a {@link eu.europa.ec.opoce.cellar.semantic.jena.template.JenaResultSetHandler} object.
     * @param <T> a T object.
     * @return a T object.
     */
    <T> T select(String sparql, JenaResultSetHandler<T> resultSetResolver);

    /**
     * <p>select</p>
     *
     * @param sparql a {@link java.lang.String} object.
     * @param listOfMapHandler a {@link eu.europa.ec.opoce.cellar.semantic.jena.template.ListOfMapHandler} object.
     * @param <T> a T object.
     * @return a T object.
     */
    <T> T select(String sparql, ListOfMapHandler<T> listOfMapHandler);

    /**
     * <p>ask</p>
     *
     * @param sparql a {@link java.lang.String} object.
     * @return a boolean.
     */
    boolean ask(String sparql);

    /**
     * <p>ask</p>
     *
     * @param sparql a {@link java.lang.String} object.
     * @param booleanExtractor a {@link eu.europa.ec.opoce.cellar.semantic.jena.template.JenaBooleanHandler} object.
     * @param <T> a T object.
     * @return a T object.
     */
    <T> T ask(final String sparql, final JenaBooleanHandler<T> booleanExtractor);

    /**
     * <p>construct</p>
     *
     * @param sparql a {@link java.lang.String} object.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    Model construct(final String sparql);

    /**
     * <p>construct</p>
     *
     * @param sparql a {@link java.lang.String} object.
     * @param resultModel a {@link org.apache.jena.rdf.model.Model} object.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    Model construct(String sparql, Model resultModel);

    /**
     * <p>construct</p>
     *
     * @param sparql a {@link java.lang.String} object.
     * @param resultModel a {@link org.apache.jena.rdf.model.Model} object.
     * @param closeInput a boolean.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    Model construct(String sparql, Model resultModel, boolean closeInput);

    /**
     * <p>construct</p>
     *
     * @param sparql a {@link java.lang.String} object.
     * @param modelCallBack a {@link RPClosure} object.
     * @param closeResult a boolean.
     * @param <T> a T object.
     * @return a T object.
     */
    <T> T construct(final String sparql, final RPClosure<T, Model> modelCallBack, boolean closeResult);

    /**
     * <p>execute</p>
     *
     * @param modelOperations a {@link RPClosure} object.
     * @param <T> a T object.
     * @return a T object.
     */
    <T> T execute(RPClosure<T, Model> modelOperations);

}
