package eu.europa.ec.opoce.cellar.semantic.zip;

import eu.europa.ec.opoce.cellar.common.util.ZipUtils;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.exception.CellarSemanticException;
import eu.europa.ec.opoce.cellar.semantic.helper.FileHelper;
import eu.europa.ec.opoce.cellar.semantic.spring.ZipEntryResource;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.CloseShieldInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.zip.CRC32;
import java.util.zip.CheckedOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

/**
 * <p>ZipTools class.</p>
 */
public class ZipTools {

    /**
     * Constant <code>log</code>
     */
    private static final Logger log = LoggerFactory.getLogger(ZipTools.class);

    /**
     * <p>Constructor for ZipTools.</p>
     */
    private ZipTools() {
    }

    /**
     * <p>createZipFile</p>
     *
     * @param zipFile a {@link java.io.File} object.
     * @return a  object.
     */
    public static ZipFile createZipFile(File zipFile) {
        try {
            return new ZipFile(zipFile);
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withMessage("Failed to create/open zipFile: {}")
                    .withMessageArgs(zipFile).withCause(e).build();
        }
    }

    /**
     * <p>listFiles</p>
     *
     * @param zipFile a  object.
     * @return a {@link java.util.Collection} object.
     */
    public static Collection<String> listFiles(ZipFile zipFile) {
        return list(zipFile, true);
    }

    /**
     * <p>listDirectories</p>
     *
     * @param zipFile a  object.
     * @return a {@link java.util.Collection} object.
     */
    public static Collection<String> listDirectories(ZipFile zipFile) {
        return list(zipFile, false);
    }

    /**
     * <p>mapFileEntries</p>
     *
     * @param zipFile a  object.
     * @return a {@link java.util.Map} object.
     */
    public static Map<String, ZipEntry> mapFileEntries(ZipFile zipFile) {
        return mapEntries(zipFile, true);
    }

    /**
     * <p>mapDirectoryEntries</p>
     *
     * @param zipFile a  object.
     * @return a {@link java.util.Map} object.
     */
    public static Map<String, ZipEntry> mapDirectoryEntries(ZipFile zipFile) {
        return mapEntries(zipFile, false);
    }

    /**
     * <p>getParentPath</p>
     *
     * @param path a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String getParentPath(String path) {
        int pos = path.lastIndexOf("/");
        if (pos == -1) {
            return null;
        }
        return path.substring(0, pos);
    }

    /**
     * <p>getName</p>
     *
     * @param path a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String getName(String path) {
        int pos = path.lastIndexOf("/");
        if (pos == -1) {
            return path;
        }
        return path.substring(pos + 1);
    }

    /**
     * <p>list</p>
     *
     * @param zipFile a  object.
     * @param files   a boolean.
     * @return a {@link java.util.Collection} object.
     */
    private static Collection<String> list(ZipFile zipFile, boolean files) {
        Collection<String> result = new HashSet<>();

        Enumeration<?> entries = zipFile.entries();
        while (entries.hasMoreElements()) {
            ZipEntry zipEntry = (ZipEntry) entries.nextElement();
            String name = zipEntry.getName().replace('\\', '/');

            boolean fileMatch = !zipEntry.isDirectory() && files;
            boolean directoryMatch = zipEntry.isDirectory() && !files;
            if (fileMatch || directoryMatch) {
                result.add(name);
            }
        }

        return result;
    }

    /**
     * <p>mapEntries</p>
     *
     * @param zipFile a  object.
     * @param files   a boolean.
     * @return a {@link java.util.Map} object.
     */
    private static Map<String, ZipEntry> mapEntries(ZipFile zipFile, boolean files) {
        Map<String, ZipEntry> result = new HashMap<>();

        Enumeration<?> entries = zipFile.entries();
        while (entries.hasMoreElements()) {
            ZipEntry zipEntry = (ZipEntry) entries.nextElement();
            String name = zipEntry.getName().replace('\\', '/');

            boolean fileMatch = !zipEntry.isDirectory() && files;
            boolean directoryMatch = zipEntry.isDirectory() && !files;
            if (fileMatch || directoryMatch) {
                result.put(name, zipEntry);
            }
        }

        return result;
    }

    /**
     * <p>calculateExtractMap</p>
     *
     * @param zipFile              a  object.
     * @param destinationDirectory a {@link java.io.File} object.
     * @return a {@link java.util.Map} object.
     */
    public static Map<File, String> calculateExtractMap(ZipFile zipFile, File destinationDirectory) {
        Map<File, String> extractedFiles = new HashMap<>();

        int counter = 0;
        Enumeration<?> entries = zipFile.entries();
        while (entries.hasMoreElements()) {
            if (++counter % 1000 == 0) {
                log.debug("calculateExtractMap: {} files done.", counter);
            }

            ZipEntry entry = (ZipEntry) entries.nextElement();
            String entryName = ZipUtils.validateFilenameInDir(entry.getName()).replace('\\', '/');
            if (!entry.isDirectory()) {
                File outputFile = new File(destinationDirectory, entryName);
                extractedFiles.put(outputFile, entryName);
            }
        }
        return extractedFiles;
    }

    /**
     * Extract all files from a zipfile.
     *
     * @param zipFile              The zipFile to extract.
     * @param destinationDirectory The destinationDirectory. If this directory does not exist, it is created.
     * @return A map of the created files (key = created file, value = entry in zipfile), only files are added, not the directories.
     * @throws java.io.IOException If something fails while extracting the files.
     */
    public static Map<File, String> extract(final ZipFile zipFile, File destinationDirectory) throws IOException {
        final Enumeration<?> entries = zipFile.entries();
        ZipData zipData = new ZipData() {

            @Override
            public ZipEntry getNextEntry() {
                return entries.hasMoreElements() ? (ZipEntry) entries.nextElement() : null;
            }

            @Override
            public InputStream getInputStream(ZipEntry entry) throws IOException {
                return zipFile.getInputStream(entry);
            }
        };
        return extract(zipData, destinationDirectory);
    }

    /**
     * <p>extract</p>
     *
     * @param zipInputStream       a {@link java.util.zip.ZipInputStream} object.
     * @param destinationDirectory a {@link java.io.File} object.
     * @return a {@link java.util.Map} object.
     * @throws java.io.IOException if any.
     */
    public static Map<File, String> extract(final ZipInputStream zipInputStream, File destinationDirectory) throws IOException {
        ZipData zipData = new ZipData() {

            @Override
            public ZipEntry getNextEntry() {
                return ZipTools.getNextEntry(zipInputStream);
            }

            @Override
            public InputStream getInputStream(ZipEntry entry) {
                return new CloseShieldInputStream(zipInputStream);
            }
        };
        return extract(zipData, destinationDirectory);
    }

    /**
     * <p>extract</p>
     *
     * @param zipData              a {@link eu.europa.ec.opoce.cellar.semantic.zip.ZipTools.ZipData} object.
     * @param destinationDirectory a {@link java.io.File} object.
     * @return a {@link java.util.Map} object.
     * @throws java.io.IOException if any.
     */
    public static Map<File, String> extract(ZipData zipData, File destinationDirectory) throws IOException {
        Map<File, String> extractedFiles = new HashMap<>();
        FileHelper.mkdirs(destinationDirectory);
        if (!destinationDirectory.isDirectory()) {
            throw new IOException("Given destination is not a directory.");
        }

        int counter = 0;
        ZipEntry entry = zipData.getNextEntry();
        while (entry != null) {
            if (++counter % 1000 == 0) {
                log.debug("Unzipping: {} files done.", counter);
            }

            String entryName = ZipUtils.validateFilenameInDir(entry.getName()).replace('\\', '/');
            if (entry.isDirectory()) {
                File outputDirectory = new File(destinationDirectory, entryName);
                FileHelper.mkdirs(outputDirectory);
            } else {
                BufferedOutputStream bufferedOutputStream = null;
                CheckedOutputStream checkedOutputStream = null;
                FileOutputStream fileOutputStream = null;
                InputStream entryInputStream = null;
                try {
                    File outputFile = new File(destinationDirectory, entryName);
                    FileHelper.mkdirs(outputFile.getParentFile());

                    CRC32 checksum = new CRC32();
                    fileOutputStream = new FileOutputStream(outputFile);
                    checkedOutputStream = new CheckedOutputStream(fileOutputStream, checksum);
                    bufferedOutputStream = new BufferedOutputStream(checkedOutputStream);

                    entryInputStream = zipData.getInputStream(entry);
                    IOUtils.copy(entryInputStream, bufferedOutputStream);
                    extractedFiles.put(outputFile, entryName);

                    bufferedOutputStream.flush();
                    checkedOutputStream.flush();

                    long zipCrc = entry.getCrc();
                    if ((-1 != zipCrc) && (checksum.getValue() != zipCrc)) {
                        throw new IOException("CRC error in entry: " + entryName);
                    }

                    fileOutputStream.flush();
                } finally {
                    IOUtils.closeQuietly(entryInputStream);
                    IOUtils.closeQuietly(bufferedOutputStream);
                    IOUtils.closeQuietly(checkedOutputStream);
                    IOUtils.closeQuietly(fileOutputStream);
                }
            }

            entry = zipData.getNextEntry();
        }

        return extractedFiles;
    }

    private interface ZipData {

        ZipEntry getNextEntry();

        InputStream getInputStream(ZipEntry entry) throws IOException;
    }

    /**
     * <p>getInputStream</p>
     *
     * @param zipFile   a  object.
     * @param entryName a {@link java.lang.String} object.
     * @return a {@link java.io.InputStream} object.
     */
    public static InputStream getInputStream(ZipFile zipFile, String entryName) {
        ZipEntry zipEntry = zipFile.getEntry(entryName);
        return null == zipEntry ? null : getInputStream(zipFile, zipEntry);
    }

    /**
     * <p>getInputStream</p>
     *
     * @param zipFile a  object.
     * @param entry   a  object.
     * @return a {@link java.io.InputStream} object.
     */
    public static InputStream getInputStream(ZipFile zipFile, ZipEntry entry) {
        try {
            return zipFile.getInputStream(entry);
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }

    /**
     * <p>getZipEntryResource</p>
     *
     * @param zipFile   a  object.
     * @param entryName a {@link java.lang.String} object.
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.spring.ZipEntryResource} object.
     */
    public static ZipEntryResource getZipEntryResource(ZipFile zipFile, String entryName) {
        ZipEntry zipEntry = zipFile.getEntry(entryName.replace('\\', '/'));
        if (zipEntry == null)
            zipEntry = zipFile.getEntry(entryName.replace('/', '\\'));

        return zipEntry != null ? new ZipEntryResource(zipFile, entryName) : null;
    }

    /**
     * <p>getNextEntry</p>
     *
     * @param inputStream a {@link java.util.zip.ZipInputStream} object.
     * @return the next entry in the ZIP
     */
    public static ZipEntry getNextEntry(ZipInputStream inputStream) {
        try {
            java.util.zip.ZipEntry temp = inputStream.getNextEntry();
            return temp != null ? new ZipEntry(temp) : null;
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }

    /**
     * <p>closeQuietly</p>
     *
     * @param zipFile a  object.
     */
    public static void closeQuietly(ZipFile zipFile) {
        if (null == zipFile)
            return;
        try {
            zipFile.close();
        } catch (IOException ignore) {
        }
    }

    /**
     * <p>close</p>
     *
     * @param zipFile a  object.
     */
    public static void close(ZipFile zipFile) {
        if (null == zipFile)
            return;
        try {
            zipFile.close();
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withMessage("Failed to close zipfile").withCause(e).build();
        }
    }
}
