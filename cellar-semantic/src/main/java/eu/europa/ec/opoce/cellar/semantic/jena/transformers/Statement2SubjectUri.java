package eu.europa.ec.opoce.cellar.semantic.jena.transformers;

import org.apache.commons.collections15.Transformer;
import org.apache.jena.rdf.model.Statement;

import javax.annotation.Nullable;

/**
 * <p>Statement2SubjectUri class.</p>
 *
 */
public class Statement2SubjectUri implements Transformer<Statement, String> {

    /** Constant <code>statement2SubjectUri_null</code> */
    public static final Statement2SubjectUri statement2SubjectUri_null = new Statement2SubjectUri(true);
    /** Constant <code>statement2SubjectUri_fail</code> */
    public static final Statement2SubjectUri statement2SubjectUri_fail = new Statement2SubjectUri(false);
    private final boolean nullOnNoUri;

    /**
     * <p>Constructor for Statement2SubjectUri.</p>
     *
     * @param nullOnNoUri a boolean.
     */
    private Statement2SubjectUri(boolean nullOnNoUri) {
        this.nullOnNoUri = nullOnNoUri;
    }

    @Nullable
    @Override
    public String transform(Statement statement) {
        if (nullOnNoUri && !statement.getSubject().isURIResource())
            return null;
        return statement.getSubject().getURI();
    }
}
