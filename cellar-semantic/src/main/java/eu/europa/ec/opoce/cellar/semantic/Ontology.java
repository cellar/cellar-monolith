package eu.europa.ec.opoce.cellar.semantic;

import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import eu.europa.ec.opoce.cellar.common.ServiceLocator;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.exception.CellarSemanticException;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.semantic.ontology.InferenceModelCacheService;
import org.apache.jena.riot.RDFFormat;
import org.springframework.core.io.Resource;

import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * <p>Ontology class.</p>
 */
public class Ontology {

    private final List<String> inferredOntologyQueries;
    private final String[] loadedOntologyUris;
    private final Model model;

    private OntModel ontModel;
    private Model inferredOntologyModel;

    // Using @Autowired to inject that service requires too much refactoring
    private final InferenceModelCacheService inferenceModelCacheService = ServiceLocator.getService(InferenceModelCacheService.class);

    /**
     * <p>Constructor for Ontology.</p>
     *
     * @param inferredOntologyQueries a {@link java.util.List} object.
     * @param ontologyUri             a {@link java.lang.String} object.
     * @param uriResolver             a {@link eu.europa.ec.opoce.cellar.semantic.UriResolver} object.
     * @param additionalOntologyUris  a {@link java.util.Set} object.
     * @param loadImports             a boolean.
     */
    public Ontology(List<String> inferredOntologyQueries, String ontologyUri, UriResolver uriResolver,
                    Set<OntologyUri> additionalOntologyUris, boolean loadImports) {
        this(inferredOntologyQueries, new String[]{ontologyUri}, uriResolver, additionalOntologyUris, loadImports);
    }

    /**
     * <p>Constructor for Ontology.</p>
     *
     * @param inferredOntologyQueries a {@link java.util.List} object.
     * @param ontologyUris            an array of {@link java.lang.String} objects.
     * @param uriResolver             a {@link eu.europa.ec.opoce.cellar.semantic.UriResolver} object.
     * @param additionalOntologyUris  a {@link java.util.Set} object.
     * @param loadImports             a boolean.
     */
    public Ontology(List<String> inferredOntologyQueries, String[] ontologyUris, UriResolver uriResolver,
                    Set<OntologyUri> additionalOntologyUris, boolean loadImports) {
        this.inferredOntologyQueries = inferredOntologyQueries;
        final ModelLoader.LoadInfo loadInfo = new ModelLoader(ontologyUris, uriResolver, additionalOntologyUris, loadImports).execute();
        this.model = loadInfo.getModel();
        this.loadedOntologyUris = loadInfo.getLoadedOntologyUris();
    }

    /**
     * <p>Constructor for Ontology.</p>
     *
     * @param resources a {@link java.util.Collection} object.
     */
    public Ontology(Collection<Resource> resources) {
        this.inferredOntologyQueries = null;
        this.loadedOntologyUris = null;
        this.model = JenaUtils.read(resources, RDFFormat.RDFXML_PLAIN);
    }

    /**
     * <p>Getter for the field <code>model</code>.</p>
     *
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    public Model getModel() {
        return model;
    }

    /**
     * <p>Getter for the field <code>ontModel</code>.</p>
     *
     * @return a {@link org.apache.jena.ontology.OntModel} object.
     */
    public OntModel getOntModel() {
        if (ontModel == null) {
            ontModel = ModelFactory.createOntologyModel();
            ontModel.add(getModel());
        }

        return ontModel;
    }

    /**
     * <p>Getter for the field <code>loadedOntologyUris</code>.</p>
     *
     * @return an array of {@link java.lang.String} objects.
     */
    public String[] getLoadedOntologyUris() {
        return loadedOntologyUris;
    }

    /**
     * <p>getInferredOntology</p>
     *
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    public Model getInferredOntology() {
        if (inferredOntologyQueries == null) {
            throw ExceptionBuilder.get(CellarSemanticException.class)
                    .withMessage("Cannot build InferredOntology, inferredOntologyQueries not specified").build();
        }
        return inferenceModelCacheService.getInferenceModel();
    }

    /**
     * <p>closeQuietly</p>
     */
    public void closeQuietly() {
        try {
            close();
        } catch (final Exception ignore) { // Todo log as warning
        }
    }

    /**
     * <p>close</p>
     */
    public void close() {
        if (model != null) {
            model.close();
        }
        if (ontModel != null) {
            ontModel.close();
        }
        if (inferredOntologyModel != null) {
            inferredOntologyModel.close();
        }

        ontModel = null;
        inferredOntologyModel = null;
    }
}
