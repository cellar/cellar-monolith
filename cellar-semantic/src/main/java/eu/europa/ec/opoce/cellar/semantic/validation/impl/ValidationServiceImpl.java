/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.semantic.validation.impl
 *             FILE : ValidationServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 10 05, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-10-05 11:23:59 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.semantic.validation.impl;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Stopwatch;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.cl.domain.validation.MetadataValidationRequest;
import eu.europa.ec.opoce.cellar.cl.domain.validation.MetadataValidationResult;
import eu.europa.ec.opoce.cellar.cl.domain.validation.ValidateContext;
import eu.europa.ec.opoce.cellar.common.util.StringUtils;
import eu.europa.ec.opoce.cellar.logging.LogContext;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.semantic.validation.ValidationService;
import io.micrometer.core.instrument.*;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.writer.RDFJSONWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.StringWriter;

import static eu.europa.ec.opoce.cellar.AppConstants.NEWLINE;
import static eu.europa.ec.opoce.cellar.logging.LogContext.Context.CMR_SHACL;
import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * @author ARHS Developments
 */
@Service("MetadataValidationService")
@LogContext(CMR_SHACL)
public class ValidationServiceImpl implements ValidationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ValidationServiceImpl.class);
    static final Property SEVERITY_PROPERTY = ResourceFactory.createProperty("http://www.w3.org/ns/shacl#resultSeverity");
    private static final Resource SHACL_INFO_RESOURCE = ResourceFactory.createProperty("http://www.w3.org/ns/shacl#Info");
    private static final Resource SHACL_WARN_RESOURCE = ResourceFactory.createProperty("http://www.w3.org/ns/shacl#Warning");
    private static final Resource SHACL_VIOL_RESOURCE = ResourceFactory.createProperty("http://www.w3.org/ns/shacl#Violation");

    private RestTemplate restTemplate;
    private final String validationEndpointUrl;
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

    // Metrics
    private final Counter validationCounter = Metrics.counter("validation.count");
    private final Timer validationTimer = Metrics.timer("validation.timer");
    private final DistributionSummary validationDistribution;

    @Autowired
    public ValidationServiceImpl(@Qualifier("cellarConfiguration") ICellarConfiguration cellarConfiguration, MeterRegistry meterRegistry) {
        this.restTemplate = new RestTemplate();
        this.validationEndpointUrl = cellarConfiguration.getCellarServiceIntegrationValidationBaseUrl() + "/validate";
        this.validationDistribution = DistributionSummary.builder("validation.triples.count.distribution")
                .scale(1)
                .register(meterRegistry);
    }

    @VisibleForTesting
    public ValidationServiceImpl(ICellarConfiguration cellarConfiguration, MeterRegistry meterRegistry, RestTemplate restTemplate) {
        this(cellarConfiguration, meterRegistry);
        this.restTemplate = restTemplate;
    }

    @Override
    @LogContext(CMR_SHACL)
    public MetadataValidationResult validate(MetadataValidationRequest request) {
        Assert.notNull(request, "Model should not be null");

        StringWriter stringWriter = new StringWriter();
        RDFJSONWriter.output(stringWriter, request.getModel().getGraph());
        ValidateContext validateContext = new ValidateContext();
        validateContext.setModel(stringWriter.toString());

        MetadataValidationResult metadataValidationResult;
        try {
            Stopwatch watch = Stopwatch.createStarted();
            metadataValidationResult = restTemplate.postForObject(validationEndpointUrl, validateContext, MetadataValidationResult.class);
            metadataValidationResult.setCellarId(request.getCellarId());
            watch.stop();
            validationCounter.increment();
            if (validationDistribution != null) {
                validationDistribution.record(request.getModel().size());
                validationTimer.record(watch.elapsed(MILLISECONDS), MILLISECONDS);
            }
            LOGGER.info("Validation result: {}", metadataValidationResult.toShortString());
            logValidationResult(metadataValidationResult);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            LOGGER.warn("There were some errors during the validation process of the metadata : {}", e.toString());
            try {
                metadataValidationResult = OBJECT_MAPPER.readValue(e.getResponseBodyAsString(), MetadataValidationResult.class);
                metadataValidationResult.setCellarId(request.getCellarId());
            } catch (IOException ioe) {
                throw new RuntimeException("Cannot parse response from validation service : " + ioe);
            }
        }

        return metadataValidationResult;
    }

    private static void logValidationResult(MetadataValidationResult result) {
        if (StringUtils.isBlank(result.getValidationResult())) {
            return;
        }
        String rawModel = result.getValidationResult();
        result.setIgnored(true);
        Model validationModel = null;
        try {
            validationModel = JenaUtils.read(rawModel, Lang.RDFJSON);
            if (containViolation(validationModel)) {
                LOGGER.error("Constraint violation: {}", rawModel.replaceAll("\n", NEWLINE));
                result.setIgnored(false);
            } else if(containWarning(validationModel)) {
                LOGGER.warn("Constraint violation warning");
                LOGGER.debug("The full response of the warning message is: Constraint violation: {}", rawModel.replaceAll("\n", NEWLINE));
                result.setIgnored(false);
            } else if(containInformation(validationModel)) {
                LOGGER.info("Constraint violation info");
                LOGGER.debug("The full response of the info message is: Constraint violation: {}", rawModel.replaceAll("\n", NEWLINE));
            }
        } finally {
            JenaUtils.closeQuietly(validationModel);
        }
    }

    @VisibleForTesting
    static boolean containViolation(final Model validationModel) {
        return validationModel.contains(null, SEVERITY_PROPERTY, SHACL_VIOL_RESOURCE);
    }

    @VisibleForTesting
    static boolean containWarning(final Model validationModel) {
        return validationModel.contains(null, SEVERITY_PROPERTY, SHACL_WARN_RESOURCE);
    }

    @VisibleForTesting
    static boolean containInformation(final Model validationModel) {
        return validationModel.contains(null, SEVERITY_PROPERTY, SHACL_INFO_RESOURCE);
    }

}
