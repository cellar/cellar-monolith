package eu.europa.ec.opoce.cellar.semantic;

import eu.europa.ec.opoce.cellar.common.CatalogResolver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;

import java.io.IOException;
import java.net.URL;
import java.util.Objects;

/**
 * <p>CatalogUriResolver class.</p>
 */
public class CatalogUriResolver implements UriResolver {

    private static final Logger LOG = LogManager.getLogger(CatalogUriResolver.class);

    private final CatalogResolver catalogResolver;

    public CatalogUriResolver(CatalogResolver catalogResolver) {
        this.catalogResolver = Objects.requireNonNull(catalogResolver);
    }

    @Override
    public Resource resolveUri(String uri) {
        try {
            return new UrlResource(new URL(catalogResolver.resolve(uri)));
        } catch (IOException e) {
            LOG.warn("Error resolving [" + uri + "]", e);
            return null;
        }
    }
}
