package eu.europa.ec.opoce.cellar.semantic.jena.template;

/**
 * <p>JenaBooleanHandler interface.</p>
 *
 */
public interface JenaBooleanHandler<T> {

    /**
     * <p>handle</p>
     *
     * @param askResult a boolean.
     * @param <T> a T object.
     * @return a T object.
     */
    public T handle(boolean askResult);

    /** Constant <code>booleanAskResultExtractor</code> */
    public final JenaBooleanHandler<Boolean> booleanAskResultExtractor = new JenaBooleanHandler<Boolean>() {

        public Boolean handle(boolean askResult) {
            return askResult;
        }
    };

    /** Constant <code>stringAskResultExtractor</code> */
    public final JenaBooleanHandler<String> stringAskResultExtractor = new JenaBooleanHandler<String>() {

        public String handle(boolean askResult) {
            return String.valueOf(askResult);
        }
    };
}
