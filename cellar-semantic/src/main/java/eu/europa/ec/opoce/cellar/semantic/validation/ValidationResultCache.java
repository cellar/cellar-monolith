package eu.europa.ec.opoce.cellar.semantic.validation;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import eu.europa.ec.opoce.cellar.cl.domain.validation.MetadataValidationResult;
import eu.europa.ec.opoce.cellar.domain.content.mets.StructMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public final class ValidationResultCache {

    /**
     * CELLAR-2095: temporary workaround
     * NB: we can have several validations per structmap (DMD, TDM etc.)
     */
    private static final Logger LOG = LoggerFactory.getLogger(ValidationResultCache.class);
    private static final int CACHE_SIZE = 2000;
    private static final int CACHE_EXPIRATION_HOUR = 10;
    private static Cache<String, List<MetadataValidationResult>> cache = CacheBuilder.newBuilder()
            .maximumSize(CACHE_SIZE)
            .expireAfterWrite(CACHE_EXPIRATION_HOUR, TimeUnit.HOURS)
            .build();

    public static void put(final StructMap structMap, final MetadataValidationResult validationResult) {
        if (structMap == null || validationResult == null) {
            return;
        }

        final String key = key(structMap);

        try {
            final List<MetadataValidationResult> validationResults = cache.get(key, ArrayList::new);
            validationResults.add(validationResult);
            cache.put(key, validationResults);
        } catch (final ExecutionException e) {
            LOG.error("Failed to put key '{}' in validation results cache: {}", key, e.getMessage());
        }
    }

    public static List<MetadataValidationResult> get(final StructMap structMap) {
        if (structMap == null) {
            return Collections.emptyList();
        }

        final String key = key(structMap);

        try {
            return cache.get(key, ArrayList::new);
        } catch (ExecutionException e) {
            LOG.warn("Failed to get key '{}' from validation results cache: {}", key, e.getMessage());
            return Collections.emptyList();
        }
    }

    public static void remove(final StructMap structMap) {
        if (structMap != null) {
            cache.invalidate(key(structMap));
        }
    }

    private static String key(final StructMap structMap) {
        return structMap.getMetsDocument().getDocumentId() + structMap.getId();
    }
}
