package eu.europa.ec.opoce.cellar.semantic.ontology.cache.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>Properties class.</p>
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Properties {

    /**
     * <p>annotations</p>
     *
     * @return an array of {@link java.lang.String} objects.
     */
    String[] annotations() default {};

    /**
     * <p>type</p>
     *
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.ontology.cache.annotation.PropertyType} object.
     */
    PropertyType type() default PropertyType.ANY;
}
