package eu.europa.ec.opoce.cellar.semantic.spring;

import eu.europa.ec.opoce.cellar.common.util.StringUtils;

import org.springframework.core.io.ByteArrayResource;

/**
 * <p>NamedByteArrayResource class.</p>
 *
 */
public class NamedByteArrayResource extends ByteArrayResource {

    /**
     * <p>Constructor for NamedByteArrayResource.</p>
     *
     * @param byteArray an array of byte.
     * @param filename a {@link java.lang.String} object.
     */
    public NamedByteArrayResource(final byte[] byteArray, final String filename) {
        super(byteArray, filename);
    }

    /** {@inheritDoc} */
    @Override
    public String getFilename() {
        final String description = getDescription();
        String name = description;
        if (description.contains("[")) {
            name = StringUtils.substringBetween(description, "[", "]");
        }
        return name;
    }
}
