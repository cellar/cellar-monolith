package eu.europa.ec.opoce.cellar.semantic.config;

import java.util.Properties;

/**
 * Helper interface to expose all loaded configuration properties of a {@link ConfigurationSettings} instance
 * to a {@link ConfigurationBean}
 *
 * @see ConfigurationBean#configure(Settings)
 */
public interface Settings {

    /**
     * <p>getProperties</p>
     *
     * @return a {@link java.util.Properties} object.
     */
    Properties getProperties();
}
