/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.semantic.validation
 *             FILE : ValidationService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 10 05, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-10-05 11:21:46 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.semantic.validation;

import eu.europa.ec.opoce.cellar.cl.domain.validation.MetadataValidationRequest;
import eu.europa.ec.opoce.cellar.cl.domain.validation.MetadataValidationResult;

/**
 * @author ARHS Developments
 */
public interface ValidationService {

    /**
     *
     * @param request
     * @return
     */
    MetadataValidationResult validate(MetadataValidationRequest request);
}
