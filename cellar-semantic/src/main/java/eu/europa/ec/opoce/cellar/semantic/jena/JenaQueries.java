package eu.europa.ec.opoce.cellar.semantic.jena;

import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.exception.CellarSemanticException;
import eu.europa.ec.opoce.cellar.semantic.helper.EnumHelper;
import org.apache.commons.lang.StringUtils;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * <p>JenaQueries class.</p>
 */
public class JenaQueries {

    /**
     * <p>getResourceUris</p>
     *
     * @param resource a {@link org.apache.jena.rdf.model.Resource} object.
     * @param property a {@link org.apache.jena.rdf.model.Property} object.
     * @return a {@link java.util.List} object.
     */
    public static List<String> getResourceUris(Resource resource, Property property) {
        return getResourceUris(resource, property, false);
    }

    /**
     * <p>getResourceUris</p>
     *
     * @param resource   a {@link org.apache.jena.rdf.model.Resource} object.
     * @param property   a {@link org.apache.jena.rdf.model.Property} object.
     * @param failOnNone a boolean.
     * @return a {@link java.util.List} object.
     */
    public static List<String> getResourceUris(Resource resource, Property property, boolean failOnNone) {
        List<String> result = new ArrayList<String>();
        for (Statement statement : new StatementIterable(getIterator(resource, property, failOnNone))) {
            result.add(statement.getResource().getURI());
        }
        return result;
    }

    /**
     * <p>getResourceUri</p>
     *
     * @param resource   a {@link org.apache.jena.rdf.model.Resource} object.
     * @param property   a {@link org.apache.jena.rdf.model.Property} object.
     * @param failOnNone a boolean.
     * @return a {@link java.lang.String} object.
     */
    @Nullable
    public static String getResourceUri(Resource resource, Property property, boolean failOnNone) {
        return getResourceUri(resource, property, failOnNone, true);
    }

    /**
     * <p>getResourceUri</p>
     *
     * @param resource   a {@link org.apache.jena.rdf.model.Resource} object.
     * @param property   a {@link org.apache.jena.rdf.model.Property} object.
     * @param failOnNone a boolean.
     * @param failOnMany a boolean.
     * @return a {@link java.lang.String} object.
     */
    @Nullable
    public static String getResourceUri(Resource resource, Property property, boolean failOnNone, boolean failOnMany) {
        Resource resourceValue = getResource(resource, property, failOnNone, failOnMany);
        return null == resourceValue ? null : resourceValue.getURI();
    }

    /**
     * <p>getResources</p>
     *
     * @param resource   a {@link org.apache.jena.rdf.model.Resource} object.
     * @param property   a {@link org.apache.jena.rdf.model.Property} object.
     * @param failOnNone a boolean.
     * @return a {@link java.util.List} object.
     */
    public static List<Resource> getResources(Resource resource, Property property, boolean failOnNone) {
        List<Resource> result = new ArrayList<Resource>();
        for (Statement statement : new StatementIterable(getIterator(resource, property, failOnNone))) {
            result.add(statement.getResource());
        }
        return result;
    }

    /**
     * <p>getResource</p>
     *
     * @param resource   a {@link org.apache.jena.rdf.model.Resource} object.
     * @param property   a {@link org.apache.jena.rdf.model.Property} object.
     * @param failOnNone a boolean.
     * @param failOnMany a boolean.
     * @return a {@link org.apache.jena.rdf.model.Resource} object.
     */
    @Nullable
    public static Resource getResource(Resource resource, Property property, boolean failOnNone, boolean failOnMany) {
        StmtIterator iterator = getIterator(resource, property, failOnNone);
        try {
            if (!iterator.hasNext())
                return null;
            Statement theStatement = iterator.next();
            if (failOnMany && iterator.hasNext()) {
                throw ExceptionBuilder.get(CellarSemanticException.class)
                        .withMessage("Too many statements found for property {} for resource {}")
                        .withMessageArgs(property.getURI(), resource.toString()).build();
            }
            return theStatement.getResource();
        } finally {
            iterator.close();
        }
    }

    /**
     * <p>getLiteralValue</p>
     *
     * @param resource a {@link org.apache.jena.rdf.model.Resource} object.
     * @param property a {@link org.apache.jena.rdf.model.Property} object.
     * @return a {@link java.lang.String} object.
     */
    @Nullable
    public static String getLiteralValue(Resource resource, Property property) {
        return getLiteralValue(resource, property, false);
    }

    /**
     * <p>getLiteralValue</p>
     *
     * @param resource   a {@link org.apache.jena.rdf.model.Resource} object.
     * @param property   a {@link org.apache.jena.rdf.model.Property} object.
     * @param failOnNone a boolean.
     * @param failOnMany a boolean.
     * @return a {@link java.lang.String} object.
     */
    @Nullable
    public static String getLiteralValue(Resource resource, Property property, boolean failOnNone, boolean failOnMany) {
        Literal literal = getLiteral(resource, property, failOnNone, failOnMany);
        return null == literal ? null : JenaUtils.stringize(literal);
    }

    /**
     * <p>getLiteralValue</p>
     *
     * @param resource   a {@link org.apache.jena.rdf.model.Resource} object.
     * @param property   a {@link org.apache.jena.rdf.model.Property} object.
     * @param failOnNone a boolean.
     * @return a {@link java.lang.String} object.
     */
    @Nullable
    public static String getLiteralValue(Resource resource, Property property, boolean failOnNone) {
        Literal literal = getLiteral(resource, property, failOnNone, true);
        return null == literal ? null : JenaUtils.stringize(literal);
    }

    /**
     * <p>getEnumValue</p>
     *
     * @param enumType   a {@link java.lang.Class} object.
     * @param resource   a {@link org.apache.jena.rdf.model.Resource} object.
     * @param property   a {@link org.apache.jena.rdf.model.Property} object.
     * @param failOnNone a boolean.
     * @param failOnMany a boolean.
     * @param <T>        a T object.
     * @return a T object.
     */
    @Nullable
    public static <T extends Enum<T>> T getEnumValue(Class<T> enumType, Resource resource, Property property, boolean failOnNone,
                                                     boolean failOnMany) {
        return EnumHelper.valueOf(enumType, getLiteralValue(resource, property, failOnNone, failOnMany));
    }

    /**
     * <p>getBooleanValue</p>
     *
     * @param resource a {@link org.apache.jena.rdf.model.Resource} object.
     * @param property a {@link org.apache.jena.rdf.model.Property} object.
     * @return a {@link java.lang.Boolean} object.
     */
    @Nullable
    public static Boolean getBooleanValue(Resource resource, Property property) {
        return getBooleanValue(resource, property, false, true);
    }

    /**
     * <p>getBooleanValue</p>
     *
     * @param resource     a {@link org.apache.jena.rdf.model.Resource} object.
     * @param property     a {@link org.apache.jena.rdf.model.Property} object.
     * @param defaultValue a {@link java.lang.Boolean} object.
     * @return a boolean.
     */
    public static boolean getBooleanValue(Resource resource, Property property, Boolean defaultValue) {
        Literal literal = getLiteral(resource, property, null == defaultValue, true);
        //noinspection ConstantConditions
        return null == literal ? defaultValue : literal.getBoolean();
    }

    /**
     * <p>getBooleanValue</p>
     *
     * @param resource   a {@link org.apache.jena.rdf.model.Resource} object.
     * @param property   a {@link org.apache.jena.rdf.model.Property} object.
     * @param failOnNone a boolean.
     * @param failOnMany a boolean.
     * @return a {@link java.lang.Boolean} object.
     */
    @Nullable
    public static Boolean getBooleanValue(Resource resource, Property property, boolean failOnNone, boolean failOnMany) {
        Literal literal = getLiteral(resource, property, failOnNone, failOnMany);
        return null == literal ? null : literal.getBoolean();
    }

    /**
     * <p>getBooleanValue</p>
     *
     * @param resource     a {@link org.apache.jena.rdf.model.Resource} object.
     * @param property     a {@link org.apache.jena.rdf.model.Property} object.
     * @param defaultValue a boolean.
     * @param failOnNone   a boolean.
     * @param failOnMany   a boolean.
     * @return a {@link java.lang.Boolean} object.
     */
    public static Boolean getBooleanValue(Resource resource, Property property, Boolean defaultValue, boolean failOnNone,
                                          boolean failOnMany) {
        Literal literal = getLiteral(resource, property, failOnNone, failOnMany);
        return null == literal ? defaultValue : literal.getBoolean();
    }

    /**
     * <p>getLiteral</p>
     *
     * @param resource   a {@link org.apache.jena.rdf.model.Resource} object.
     * @param property   a {@link org.apache.jena.rdf.model.Property} object.
     * @param failOnNone a boolean.
     * @param failOnMany a boolean.
     * @return a {@link org.apache.jena.rdf.model.Literal} object.
     */
    @Nullable
    public static Literal getLiteral(Resource resource, Property property, boolean failOnNone, boolean failOnMany) {
        StmtIterator iterator = getIterator(resource, property, failOnNone);
        try {
            if (!iterator.hasNext())
                return null;
            Statement theStatement = iterator.next();
            if (failOnMany && iterator.hasNext()) {
                throw ExceptionBuilder.get(CellarSemanticException.class)
                        .withMessage("Too many statements found for property {} for resource {}")
                        .withMessageArgs(property.getURI(), resource.toString()).build();
            }
            return theStatement.getLiteral();
        } finally {
            iterator.close();
        }
    }

    /**
     * <p>getLiteralValues</p>
     *
     * @param resources a {@link java.lang.Iterable} object.
     * @param property  a {@link org.apache.jena.rdf.model.Property} object.
     * @return a {@link java.util.List} object.
     */
    public static List<String> getLiteralValues(Iterable<Resource> resources, Property property) {
        Set<String> result = new HashSet<String>();
        for (Resource resource : resources) {
            result.addAll(getLiteralValues(resource, property));
        }
        return new ArrayList<String>(result);
    }

    /**
     * <p>getLiteralValues</p>
     *
     * @param resource a {@link org.apache.jena.rdf.model.Resource} object.
     * @param property a {@link org.apache.jena.rdf.model.Property} object.
     * @return a {@link java.util.List} object.
     */
    public static List<String> getLiteralValues(Resource resource, Property property) {
        return getLiteralValues(resource, property, false);
    }

    /**
     * <p>getLiteralValues</p>
     *
     * @param resource   a {@link org.apache.jena.rdf.model.Resource} object.
     * @param property   a {@link org.apache.jena.rdf.model.Property} object.
     * @param failOnNone a boolean.
     * @return a {@link java.util.List} object.
     */
    public static List<String> getLiteralValues(Resource resource, Property property, boolean failOnNone) {
        List<String> result = new ArrayList<String>();
        for (Statement statement : new StatementIterable(getIterator(resource, property, failOnNone))) {
            result.add(JenaUtils.stringize(statement.getLiteral()));
        }
        return result;
    }

    /**
     * <p>getLiterals</p>
     *
     * @param resource   a {@link org.apache.jena.rdf.model.Resource} object.
     * @param property   a {@link org.apache.jena.rdf.model.Property} object.
     * @param failOnNone a boolean.
     * @return a {@link java.util.List} object.
     */
    public static List<Literal> getLiterals(Resource resource, Property property, boolean failOnNone) {
        List<Literal> result = new ArrayList<Literal>();
        for (Statement statement : new StatementIterable(getIterator(resource, property, failOnNone))) {
            result.add(statement.getLiteral());
        }
        return result;
    }

    /**
     * <p>getLiteralValue</p>
     *
     * @param resource   a {@link org.apache.jena.rdf.model.Resource} object.
     * @param property   a {@link org.apache.jena.rdf.model.Property} object.
     * @param language   a {@link java.lang.String} object.
     * @param failOnNone a boolean.
     * @param failOnMany a boolean.
     * @return a {@link java.lang.String} object.
     */
    @Nullable
    public static String getLiteralValue(Resource resource, Property property, String language, boolean failOnNone, boolean failOnMany) {
        Literal literal = getLiteral(resource, property, language, failOnNone, failOnMany);
        return null == literal ? null : JenaUtils.stringize(literal);
    }

    /**
     * <p>getLiteralValues</p>
     *
     * @param resource   a {@link org.apache.jena.rdf.model.Resource} object.
     * @param property   a {@link org.apache.jena.rdf.model.Property} object.
     * @param language   a {@link java.lang.String} object.
     * @param failOnNone a boolean.
     * @return a {@link java.util.List} object.
     */
    public static List<String> getLiteralValues(Resource resource, Property property, String language, boolean failOnNone) {
        List<Literal> literals = getLiterals(resource, property, language, failOnNone);
        List<String> result = new ArrayList<String>();
        for (Literal literal : literals) {
            result.add(JenaUtils.stringize(literal));
        }
        return result;
    }

    /**
     * <p>getLiteral</p>
     *
     * @param resource   a {@link org.apache.jena.rdf.model.Resource} object.
     * @param property   a {@link org.apache.jena.rdf.model.Property} object.
     * @param language   a {@link java.lang.String} object.
     * @param failOnNone a boolean.
     * @param failOnMany a boolean.
     * @return a {@link org.apache.jena.rdf.model.Literal} object.
     */
    @Nullable
    public static Literal getLiteral(Resource resource, Property property, String language, boolean failOnNone, boolean failOnMany) {
        List<Literal> literals = getLiterals(resource, property, language, failOnNone);
        if (failOnMany && literals.size() > 1) {
            throw ExceptionBuilder.get(CellarSemanticException.class)
                    .withMessage("Too many literals found for property {} for resource {} and language {}")
                    .withMessageArgs(property.getURI(), resource.toString(), language).build();
        }
        return literals.isEmpty() ? null : literals.get(0);
    }

    /**
     * <p>getLiterals</p>
     *
     * @param resource   a {@link org.apache.jena.rdf.model.Resource} object.
     * @param property   a {@link org.apache.jena.rdf.model.Property} object.
     * @param language   a {@link java.lang.String} object.
     * @param failOnNone a boolean.
     * @return a {@link java.util.List} object.
     */
    public static List<Literal> getLiterals(Resource resource, Property property, String language, boolean failOnNone) {
        List<Literal> result = new ArrayList<Literal>();
        for (Literal literal : getLiterals(resource, property, false)) {
            if (!StringUtils.equals(literal.getLanguage(), language))
                continue;
            result.add(literal);
        }
        if (failOnNone && result.isEmpty()) {
            throw ExceptionBuilder.get(CellarSemanticException.class)
                    .withMessage("No literals found for property {} for resource {} and language {}")
                    .withMessageArgs(property.getURI(), resource.toString(), language).build();
        }
        return result;
    }

    /**
     * <p>getIterator</p>
     *
     * @param resource   a {@link org.apache.jena.rdf.model.Resource} object.
     * @param property   a {@link org.apache.jena.rdf.model.Property} object.
     * @param failOnNone a boolean.
     * @return a {@link org.apache.jena.rdf.model.StmtIterator} object.
     */
    private static StmtIterator getIterator(Resource resource, Property property, boolean failOnNone) {
        StmtIterator iterator = resource.listProperties(property);
        if (failOnNone && !iterator.hasNext()) {
            throw ExceptionBuilder.get(CellarSemanticException.class)
                    .withMessage("No statements found for property {} for resource {}")
                    .withMessageArgs(property.getURI(), resource.toString()).build();
        }
        return iterator;
    }

}
