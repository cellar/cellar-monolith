package eu.europa.ec.opoce.cellar.semantic.jena.rules;

import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.reasoner.Reasoner;
import org.apache.jena.reasoner.ValidityReport;
import org.apache.jena.reasoner.rulesys.GenericRuleReasoner;
import org.apache.jena.reasoner.rulesys.Rule;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.exception.CellarSemanticException;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * <p>ReasonerFactory class.</p>
 */
public class ReasonerFactory {

    /**
     * Constant <code>log</code>
     */
    private static final Logger log = LoggerFactory.getLogger(ReasonerFactory.class);

    /**
     * <p>createReasoner</p>
     *
     * @param ontologyModel a {@link org.apache.jena.rdf.model.Model} object.
     * @param rulesList     a {@link java.util.List} object.
     * @return a {@link org.apache.jena.reasoner.Reasoner} object.
     */
    public static Reasoner createReasoner(Model ontologyModel, @SuppressWarnings("unchecked") List<Rule>... rulesList) {
        List<Rule> allRules = new ArrayList<>();
        for (List<Rule> rules : rulesList) {
            if (rules == null) {
                continue;
            }
            allRules.addAll(rules);
        }

        Reasoner reasoner = new GenericRuleReasoner(allRules);
        //    reasoner.setDerivationLogging(true);
        if (ontologyModel != null) {
            JenaUtils.trace(ontologyModel, "bind schema");
            reasoner = reasoner.bindSchema(ontologyModel);
        }

        return reasoner;
    }

    /**
     * <p>getDeductionsModel</p>
     *
     * @param reasoner   a {@link org.apache.jena.reasoner.Reasoner} object.
     * @param inputModel a {@link org.apache.jena.rdf.model.Model} object.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    public static Model getDeductionsModel(Reasoner reasoner, Model inputModel) {
        log.info("Run reasoner.");
        JenaUtils.trace(inputModel, "input model");

        InfModel reasonerModel = ModelFactory.createInfModel(reasoner, inputModel);
        Model deductionsModel = reasonerModel.getDeductionsModel();
        JenaUtils.debug(deductionsModel, "deductions model");

        ValidityReport validityReport = reasonerModel.validate();
        if (!validityReport.isValid()) {
            throw ExceptionBuilder.get(CellarSemanticException.class)
                    .withMessageArgs(getValidityReportString(validityReport)).build();
        }

        log.info("Run reasoner done.");
        return deductionsModel;
    }

    /**
     * Updates inputModel by executing rules of reasoner.
     *
     * @param reasoner   a {@link org.apache.jena.reasoner.Reasoner} object.
     * @param inputModel a {@link org.apache.jena.rdf.model.Model} object.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    public static Model getReasonedModel(Reasoner reasoner, Model inputModel) {
        Model deductionsModel = null;
        try {
            deductionsModel = getDeductionsModel(reasoner, inputModel);

            // why not reasonerModel? because we don't want ontology data here!!
            Model result = JenaUtils.create(inputModel, deductionsModel);
            JenaUtils.trace(result, "result model");

            return result;
        } finally {
            JenaUtils.closeQuietly(deductionsModel);
        }
    }

    /**
     * <p>getValidityReportString</p>
     *
     * @param validityReport a {@link org.apache.jena.reasoner.ValidityReport} object.
     * @return a {@link java.lang.String} object.
     */
    private static String getValidityReportString(ValidityReport validityReport) {
        StringBuilder builder = new StringBuilder();
        builder.append("Reasoner failed with ").append(getReports(validityReport).size()).append(" errors!");

        for (ValidityReport.Report report : getReports(validityReport)) {
            builder.append("\n\t").append(report.getType()).append(": ").append(report.getDescription().replace("\n", "\n\t\t"));
        }

        return builder.toString();
    }

    /**
     * <p>getReports</p>
     *
     * @param validityReport a {@link org.apache.jena.reasoner.ValidityReport} object.
     * @return a {@link java.util.List} object.
     */
    private static List<ValidityReport.Report> getReports(ValidityReport validityReport) {
        List<ValidityReport.Report> result = new ArrayList<>();

        Iterator<ValidityReport.Report> reportIterator = validityReport.getReports();
        while (reportIterator.hasNext()) {
            result.add(reportIterator.next());
        }

        return result;
    }

}
