package eu.europa.ec.opoce.cellar.semantic.ontology.property;

import static eu.europa.ec.opoce.cellar.common.Namespace.at;
import static eu.europa.ec.opoce.cellar.common.Namespace.cdm;
import static eu.europa.ec.opoce.cellar.common.Namespace.ev;
import static eu.europa.ec.opoce.cellar.common.Namespace.owl;
import static eu.europa.ec.opoce.cellar.common.Namespace.skos;

import eu.europa.ec.opoce.cellar.domain.content.mets.DigitalObjectType;

import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

/**
 * <p>CellarType interface.</p>
 */
public interface CellarType {

    /**
     * Constant <code>cdm_agentR</code>
     */
    Resource cdm_agentR = DigitalObjectType.AGENT.getModelResource();
    /**
     * Constant <code>cdm_topleveleventR</code>
     */
    Resource cdm_topleveleventR = DigitalObjectType.TOPLEVELEVENT.getModelResource();
    /**
     * Constant <code>cdm_workR</code>
     */
    Resource cdm_workR = DigitalObjectType.WORK.getModelResource();
    /**
     * Constant <code>cdm_expressionR</code>
     */
    Resource cdm_expressionR = DigitalObjectType.EXPRESSION.getModelResource();
    /**
     * Constant <code>cdm_manifestationR</code>
     */
    Resource cdm_manifestationR = DigitalObjectType.MANIFESTATION.getModelResource();
    /**
     * Constant <code>cdm_dossierR</code>
     */
    Resource cdm_dossierR = DigitalObjectType.DOSSIER.getModelResource();
    /**
     * Constant <code>cdm_eventR</code>
     */
    Resource cdm_eventR = DigitalObjectType.EVENT.getModelResource();
    /**
     * Constant <code>cdm_itemR</code>
     */
    Resource cdm_itemR = DigitalObjectType.ITEM.getModelResource();

    /**
     * Constant <code>cdm_entityTemporal</code>
     */
    String cdm_entityTemporal = cdm + "entity_temporal";
    /**
     * Constant <code>cdm_entityTemporalR</code>
     */
    Resource cdm_entityTemporalR = ResourceFactory.createResource(cdm_entityTemporal);
    /**
     * Constant <code>cdm_unitAdministrative</code>
     */
    String cdm_unitAdministrative = cdm + "unit_administrative";
    /**
     * Constant <code>cdm_unitAdministrativeR</code>
     */
    Resource cdm_unitAdministrativeR = ResourceFactory.createResource(cdm_unitAdministrative);
    /**
    * Constant <code>at_Language="at + Language"</code>
    */
    String at_Language = at + "Language";
    /**
     * Constant <code>at_LanguageR="at + ResourceFactory.createResource(at_"{trunked}</code>
     */
    String at_LanguageR = at + ResourceFactory.createResource(at_Language);
    /**
     * Constant <code>eu_MicroThesaurus="ev + MicroThesaurus"</code>
     */
    String eu_MicroThesaurus = ev + "MicroThesaurus";
    /**
     * Constant <code>eu_MicroThesaurusR="ev + ResourceFactory.createResource(eu_"{trunked}</code>
     */
    String eu_MicroThesaurusR = ev + ResourceFactory.createResource(eu_MicroThesaurus);
    /**
     * Constant <code>eu_Domain="ev + Domain"</code>
     */
    String eu_Domain = ev + "Domain";
    /**
     * Constant <code>eu_DomainR="ev + ResourceFactory.createResource(eu_"{trunked}</code>
     */
    String eu_DomainR = ev + ResourceFactory.createResource(eu_Domain);

    /**
     * Constant <code>skos_Concept="skos + Concept"</code>
     */
    String skos_Concept = skos + "Concept";
    /**
     * Constant <code>skos_ConceptR</code>
     */
    Resource skos_ConceptR = ResourceFactory.createResource(skos_Concept);
    /**
     * Constant <code>skos_ConceptScheme="skos + ConceptScheme"</code>
     */
    String skos_ConceptScheme = skos + "ConceptScheme";
    /**
     * Constant <code>skos_ConceptSchemeR</code>
     */
    Resource skos_ConceptSchemeR = ResourceFactory.createResource(skos_ConceptScheme);

    /**
     * Constant <code>owl_axiom="owl + Axiom"</code>
     */
    String owl_axiom = owl + "Axiom";
    /**
     * Constant <code>owl_axiomR</code>
     */
    Resource owl_axiomR = ResourceFactory.createResource(owl_axiom);
}
