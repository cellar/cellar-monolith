package eu.europa.ec.opoce.cellar.semantic.jena.template;

import org.apache.jena.query.ResultSet;

/**
 * <p>Abstract JenaResultSetHandlerWithoutResult class.</p>
 *
 */
public abstract class JenaResultSetHandlerWithoutResult implements JenaResultSetHandler<Object> {

    /** {@inheritDoc} */
    @Override
    public Object handle(ResultSet resultSet) {
        doHandle(resultSet);

        return null;
    }

    /**
     * <p>doHandle</p>
     *
     * @param resultSet a {@link org.apache.jena.query.ResultSet} object.
     */
    protected abstract void doHandle(ResultSet resultSet);
}
