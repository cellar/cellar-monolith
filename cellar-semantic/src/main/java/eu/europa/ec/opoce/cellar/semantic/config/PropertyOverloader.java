package eu.europa.ec.opoce.cellar.semantic.config;

/**
 * Interface that must be implemented to provide custom property overloading functionality
 * in a {@link ConfigurationSettings} instance.
 *
 * @see ConfigurationSettings#setPropertyOverloader(PropertyOverloader)
 */
public interface PropertyOverloader {

    /**
     * Returns the new value if overloaded else the original value.
     * This method must return the original value if the property is not being overloaded!
     *
     * @param key   the name of the property to overload
     * @param value the original value of the the property to overload
     * @return either the original value or the overloaded value
     */
    public String overload(String key, String value);
}
