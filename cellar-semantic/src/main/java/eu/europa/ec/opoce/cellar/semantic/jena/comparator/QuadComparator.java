package eu.europa.ec.opoce.cellar.semantic.jena.comparator;

import java.util.Comparator;

import org.apache.jena.sparql.core.Quad;

/**
 * <p>QuadComparator class.</p>
 *
 */
public class QuadComparator implements Comparator<Quad> {

    public enum FirstSort {
        Triple, Graph
    }

    private FirstSort firstSort;
    private final NodeComparator nodeComparator = new NodeComparator();
    private final TripleComparator tripleComparator = new TripleComparator();

    /**
     * <p>Constructor for QuadComparator.</p>
     *
     * @param firstSort a {@link eu.europa.ec.opoce.cellar.semantic.jena.comparator.QuadComparator.FirstSort} object.
     */
    public QuadComparator(FirstSort firstSort) {
        this.firstSort = firstSort;
    }

    /** {@inheritDoc} */
    @Override
    public int compare(Quad one, Quad other) {
        int result = firstSort == FirstSort.Graph ? graphCompare(one, other) : tripleCompare(one, other);
        if (result != 0) {
            return result;
        }

        return firstSort == FirstSort.Graph ? tripleCompare(one, other) : graphCompare(one, other);
    }

    /**
     * <p>tripleCompare</p>
     *
     * @param one a {@link org.apache.jena.sparql.core.Quad} object.
     * @param other a {@link org.apache.jena.sparql.core.Quad} object.
     * @return a int.
     */
    private int tripleCompare(Quad one, Quad other) {
        return tripleComparator.compare(one.asTriple(), other.asTriple());
    }

    /**
     * <p>graphCompare</p>
     *
     * @param one a {@link org.apache.jena.sparql.core.Quad} object.
     * @param other a {@link org.apache.jena.sparql.core.Quad} object.
     * @return a int.
     */
    private int graphCompare(Quad one, Quad other) {
        return nodeComparator.compare(one.getGraph(), other.getGraph());
    }
}
