package eu.europa.ec.opoce.cellar.semantic.xml;

import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.exception.CellarSemanticException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>XmlBuilder class.</p>
 */
public class XmlBuilder {

    private Map<String, String> namespaces;

    private final boolean skipParentNamespace;
    private String defaultNamespace;
    private String defaultPrefix;

    private Document document;
    private final XmlBuilder parent;
    private Element element;

    /**
     * <p>Constructor for XmlBuilder.</p>
     */
    public XmlBuilder() {
        this.parent = null;
        this.skipParentNamespace = true;
    }

    /**
     * <p>Constructor for XmlBuilder.</p>
     *
     * @param defaultPrefix    a {@link java.lang.String} object.
     * @param defaultNamespace a {@link java.lang.String} object.
     * @param elementName      a {@link java.lang.String} object.
     */
    public XmlBuilder(String defaultPrefix, String defaultNamespace, String elementName) {
        this.parent = null;
        this.skipParentNamespace = true;

        document = DomUtils.getDocumentBuilder().newDocument();

        this.defaultPrefix = defaultPrefix;
        this.defaultNamespace = defaultNamespace;

        element = document.createElementNS(defaultNamespace, elementName);
        element.setPrefix(defaultPrefix);

        document.appendChild(element);
    }

    /**
     * <p>Constructor for XmlBuilder.</p>
     *
     * @param elementName a {@link java.lang.String} object.
     */
    public XmlBuilder(String elementName) {
        this.parent = null;
        this.skipParentNamespace = true;

        document = DomUtils.getDocumentBuilder().newDocument();

        element = document.createElement(elementName);
        document.appendChild(element);
    }

    /**
     * <p>Constructor for XmlBuilder.</p>
     *
     * @param parent      a {@link eu.europa.ec.opoce.cellar.semantic.xml.XmlBuilder} object.
     * @param elementName a {@link java.lang.String} object.
     */
    public XmlBuilder(XmlBuilder parent, String elementName) {
        this(parent, elementName, false);
    }

    /**
     * <p>Constructor for XmlBuilder.</p>
     *
     * @param parent              a {@link eu.europa.ec.opoce.cellar.semantic.xml.XmlBuilder} object.
     * @param elementName         a {@link java.lang.String} object.
     * @param skipParentNamespace a boolean.
     */
    public XmlBuilder(XmlBuilder parent, String elementName, boolean skipParentNamespace) {
        this.skipParentNamespace = skipParentNamespace;
        this.parent = parent;

        if (getDefaultNamespace() == null) {
            element = getDocument().createElement(elementName);
        } else {
            element = getDocument().createElementNS(getDefaultNamespace(), elementName);
            element.setPrefix(getDefaultPrefix());
        }

        parent.element.appendChild(element);
    }

    /**
     * <p>addNamespace</p>
     *
     * @param prefix    a {@link java.lang.String} object.
     * @param namespace a {@link java.lang.String} object.
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.xml.XmlBuilder} object.
     */
    public XmlBuilder addNamespace(String prefix, String namespace) {
        if (null != parent) {
            parent.addNamespace(prefix, namespace);
            return this;
        }

        if (null == namespaces)
            namespaces = new HashMap<>();
        String previous = namespaces.put(prefix, namespace);
        if (null != previous && !previous.equals(namespace)) {
            throw ExceptionBuilder.get(CellarSemanticException.class)
                    .withMessage("Tried to set prefix {} to {} but was already mapped to {}")
                    .withMessageArgs(prefix, namespace, previous).build();
        }
        attribute("xmlns:" + prefix, namespace);
        return this;
    }

    /**
     * <p>Getter for the field <code>document</code>.</p>
     *
     * @return a {@link org.w3c.dom.Document} object.
     */
    public Document getDocument() {
        return document == null ? parent.getDocument() : document;
    }

    /**
     * <p>Setter for the field <code>defaultNamespace</code>.</p>
     *
     * @param namespace a {@link java.lang.String} object.
     */
    public void setDefaultNamespace(String namespace) {
        this.defaultNamespace = namespace;
    }

    /**
     * <p>Getter for the field <code>defaultNamespace</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDefaultNamespace() {
        return defaultNamespace == null && parent != null && !skipParentNamespace ? parent.getDefaultNamespace() : defaultNamespace;
    }

    /**
     * <p>Getter for the field <code>defaultPrefix</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDefaultPrefix() {
        return defaultPrefix == null && parent != null && !skipParentNamespace ? parent.getDefaultPrefix() : defaultPrefix;
    }

    /**
     * <p>Getter for the field <code>element</code>.</p>
     *
     * @return a {@link org.w3c.dom.Element} object.
     */
    public Element getElement() {
        return element;
    }

    /**
     * <p>child</p>
     *
     * @param name a {@link java.lang.String} object.
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.xml.XmlBuilder} object.
     */
    public XmlBuilder child(String name) {
        return new XmlBuilder(this, name);
    }

    /**
     * <p>child</p>
     *
     * @param name                a {@link java.lang.String} object.
     * @param skipParentNamespace a boolean.
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.xml.XmlBuilder} object.
     */
    public XmlBuilder child(String name, boolean skipParentNamespace) {
        return new XmlBuilder(this, name, skipParentNamespace);
    }

    /**
     * <p>attribute</p>
     *
     * @param name a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public String attribute(String name) {
        return element.getAttribute(name);
    }

    /**
     * <p>attribute</p>
     *
     * @param name  a {@link java.lang.String} object.
     * @param value a {@link java.lang.String} object.
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.xml.XmlBuilder} object.
     */
    public XmlBuilder attribute(String name, String value) {
        element.setAttribute(name, value);
        return this;
    }

    /**
     * <p>text</p>
     *
     * @param text a {@link java.lang.String} object.
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.xml.XmlBuilder} object.
     */
    public XmlBuilder text(String text) {
        element.setTextContent(text);
        return this;
    }

    /**
     * <p>xml</p>
     *
     * @param xml a {@link java.lang.String} object.
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.xml.XmlBuilder} object.
     */
    public XmlBuilder xml(String xml) {
        Element xmlElement = DomUtils.read(xml).getDocumentElement();
        Node imported = getDocument().importNode(xmlElement, true);
        element.appendChild(imported);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        StringWriter writer = new StringWriter();
        DomUtils.write(getDocument(), writer);
        return writer.toString();
    }
}
