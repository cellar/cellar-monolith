package eu.europa.ec.opoce.cellar.semantic.pattern;

/**
 * <p>Refreshable interface.</p>
 *
 */
public interface Refreshable {

    /**
     * <p>refresh</p>
     */
    void refresh();
}
