package eu.europa.ec.opoce.cellar.semantic.jena.template;

import java.util.List;
import java.util.Map;

import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.RDFNode;

/**
 * <p>Abstract JenaResultSetHandlers class.</p>
 *
 */
public abstract class JenaResultSetHandlers {

    /** Constant <code>listOfMapsResolver</code> */
    @SuppressWarnings({
            "unchecked", "rawtypes"})
    public static final JenaResultSetHandler<List<Map<String, RDFNode>>> listOfMapsResolver = new JenaResultSetHandler() {

        public List<Map<String, RDFNode>> handle(ResultSet resultSet) {
            return JenaQueryUtils.convertToListOfMaps(resultSet);
        }
    };

}
