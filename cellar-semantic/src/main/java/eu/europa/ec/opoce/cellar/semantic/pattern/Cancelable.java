package eu.europa.ec.opoce.cellar.semantic.pattern;

/**
 * <p>Cancelable interface.</p>
 *
 */
public interface Cancelable {

    /**
     * <p>cancel</p>
     */
    void cancel();
}
