package eu.europa.ec.opoce.cellar.semantic.ontology.cache.annotation;

/**
 * <p>PropertyType class.</p>
 *
 */
public enum PropertyType {
    ANY, Data, Object, Annotation
}
