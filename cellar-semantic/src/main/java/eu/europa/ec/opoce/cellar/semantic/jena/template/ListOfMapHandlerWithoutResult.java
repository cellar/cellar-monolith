package eu.europa.ec.opoce.cellar.semantic.jena.template;

import java.util.List;
import java.util.Map;

import org.apache.jena.rdf.model.RDFNode;

/**
 * <p>Abstract ListOfMapHandlerWithoutResult class.</p>
 *
 */
public abstract class ListOfMapHandlerWithoutResult implements ListOfMapHandler<Object> {

    /** {@inheritDoc} */
    @Override
    public Object handle(List<Map<String, RDFNode>> resultSet) {
        doHandle(resultSet);

        return null;
    }

    /**
     * <p>doHandle</p>
     *
     * @param resultSet a {@link java.util.List} object.
     */
    protected abstract void doHandle(List<Map<String, RDFNode>> resultSet);
}
