package eu.europa.ec.opoce.cellar.semantic.jena.transformers;

import org.apache.commons.collections15.Transformer;

import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;

/**
 * <p>Statement2ObjectResource class.</p>
 *
 */
public class Statement2ObjectResource implements Transformer<Statement, Resource> {

    /** Constant <code>statement2ObjectResource</code> */
    public static Statement2ObjectResource statement2ObjectResource = new Statement2ObjectResource();

    /**
     * <p>Constructor for Statement2ObjectResource.</p>
     */
    private Statement2ObjectResource() {
    }

    /** {@inheritDoc} */
    @Override
    public Resource transform(Statement input) {
        return input.getResource();
    }
}
