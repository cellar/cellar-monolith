package eu.europa.ec.opoce.cellar.semantic.config;

import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.exception.CellarSemanticException;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Implementation of the {@link eu.europa.ec.opoce.cellar.semantic.config.PropertyOverloader} interface.
 * Overloads the given property with a System or Environment property with the same name.
 *
 * @see PropertyOverloader
 * @see ConfigurationSettings#setPropertyOverloader(PropertyOverloader)
 */
public class SystemPropertyOverloader implements PropertyOverloader {

    private Set<String> keys;

    /**
     * <p>Constructor for SystemPropertyOverloader.</p>
     */
    public SystemPropertyOverloader() {
    }

    /**
     * <p>Constructor for SystemPropertyOverloader.</p>
     *
     * @param keys an array of {@link java.lang.String} objects.
     */
    public SystemPropertyOverloader(String[] keys) {
        ExceptionBuilder.throwExc(CellarSemanticException.class, keys == null);

        this.keys = new HashSet<>(Arrays.asList(keys));
    }

    /**
     * <p>Constructor for SystemPropertyOverloader.</p>
     *
     * @param keys a {@link java.util.Collection} object.
     */
    public SystemPropertyOverloader(Collection<String> keys) {
        ExceptionBuilder.throwExc(CellarSemanticException.class, keys == null);

        this.keys = new HashSet<>(keys);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final String overload(String key, String value) {
        if (keys != null && !keys.contains(key))
            return value;

        String overridingValue = overload(key);
        return overridingValue != null ? overridingValue : value;
    }

    /**
     * <p>overload</p>
     *
     * @param key a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    private String overload(String key) {
        return getSystemOrEnvironmentProperty(key);
    }

    /**
     * <p>getSystemOrEnvironmentProperty</p>
     *
     * @param key a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    protected String getSystemOrEnvironmentProperty(String key) {
        String value = System.getProperty(key);
        return value != null ? value : System.getenv(key);
    }
}
