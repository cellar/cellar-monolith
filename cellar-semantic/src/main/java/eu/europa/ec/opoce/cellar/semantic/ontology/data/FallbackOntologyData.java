package eu.europa.ec.opoce.cellar.semantic.ontology.data;

import eu.europa.ec.opoce.cellar.semantic.ontology.cache.annotation.AnnotationProperties;
import eu.europa.ec.opoce.cellar.semantic.ontology.cache.annotation.Properties;
import eu.europa.ec.opoce.cellar.semantic.ontology.property.CellarAnnotationProperty;

import java.util.SortedSet;

public interface FallbackOntologyData {

    @AnnotationProperties(value = CellarAnnotationProperty.cmr_facetAcronym, failOnNone = true)
    String getFacetAcronym(String propertyUri);

    @Properties(annotations = CellarAnnotationProperty.cmr_annotation_toBeIndexed)
    SortedSet<String> getToBeIndexedPropertyUris();

    @Properties(annotations = CellarAnnotationProperty.cmr_annotation_inNotice)
    SortedSet<String> getInNoticePropertyUris();
}
