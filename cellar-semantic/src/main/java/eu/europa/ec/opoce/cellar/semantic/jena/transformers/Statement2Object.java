package eu.europa.ec.opoce.cellar.semantic.jena.transformers;

import org.apache.commons.collections15.Transformer;

import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Statement;

/**
 * <p>Statement2Object class.</p>
 *
 */
public class Statement2Object implements Transformer<Statement, RDFNode> {

    /** Constant <code>instance</code> */
    public static Statement2Object instance = new Statement2Object();

    /** {@inheritDoc} */
    @Override
    public RDFNode transform(Statement input) {
        return input.getObject();
    }
}
