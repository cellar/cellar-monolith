package eu.europa.ec.opoce.cellar.semantic.jena;

import org.apache.jena.datatypes.RDFDatatype;
import org.apache.jena.graph.Graph;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.Triple;
import org.apache.jena.rdf.model.Alt;
import org.apache.jena.rdf.model.AnonId;
import org.apache.jena.rdf.model.Bag;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelChangedListener;
import org.apache.jena.rdf.model.NodeIterator;
import org.apache.jena.rdf.model.NsIterator;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFList;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.RDFReader;
import org.apache.jena.rdf.model.RDFWriter;
import org.apache.jena.rdf.model.RSIterator;
import org.apache.jena.rdf.model.ReifiedStatement;
import org.apache.jena.rdf.model.ResIterator;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceF;
import org.apache.jena.rdf.model.Selector;
import org.apache.jena.rdf.model.Seq;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.shared.Command;
import org.apache.jena.shared.Lock;
import org.apache.jena.shared.PrefixMapping;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Abstract class for subclasses following the decorator pattern.
 * Most functionality will be delegated, some can be overridden.
 *
 * And possibly functionality will be added in subclasses.
 */
@SuppressWarnings("deprecation")
public abstract class ModelDecorator implements Model {

    private final Model actualModel;

    /**
     * <p>Constructor for ModelDecorator.</p>
     *
     * @param actualModel a {@link org.apache.jena.rdf.model.Model} object.
     */
    protected ModelDecorator(Model actualModel) {
        this.actualModel = actualModel;
    }

    /**
     * <p>Getter for the field <code>actualModel</code>.</p>
     *
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    public final Model getActualModel() {
        return actualModel;
    }

    /** {@inheritDoc} */
    @Override
    public long size() {
        return actualModel.size();
    }

    /** {@inheritDoc} */
    @Override
    public boolean isEmpty() {
        return actualModel.isEmpty();
    }

    /** {@inheritDoc} */
    @Override
    public ResIterator listSubjects() {
        return actualModel.listSubjects();
    }

    /** {@inheritDoc} */
    @Override
    public NsIterator listNameSpaces() {
        return actualModel.listNameSpaces();
    }

    /** {@inheritDoc} */
    @Override
    public Resource getResource(String uri) {
        return actualModel.getResource(uri);
    }

    /** {@inheritDoc} */
    @Override
    public Property getProperty(String nameSpace, String localName) {
        return actualModel.getProperty(nameSpace, localName);
    }

    /** {@inheritDoc} */
    @Override
    public Resource createResource() {
        return actualModel.createResource();
    }

    /** {@inheritDoc} */
    @Override
    public Resource createResource(AnonId id) {
        return actualModel.createResource(id);
    }

    /** {@inheritDoc} */
    @Override
    public Resource createResource(String uri) {
        return actualModel.createResource(uri);
    }

    /** {@inheritDoc} */
    @Override
    public Property createProperty(String nameSpace, String localName) {
        return actualModel.createProperty(nameSpace, localName);
    }

    /** {@inheritDoc} */
    @Override
    public Literal createLiteral(String v, String language) {
        return actualModel.createLiteral(v, language);
    }

    /** {@inheritDoc} */
    @Override
    public Literal createLiteral(String v, boolean wellFormed) {
        return actualModel.createLiteral(v, wellFormed);
    }

    /** {@inheritDoc} */
    @Override
    public Literal createTypedLiteral(String lex, RDFDatatype dtype) {
        return actualModel.createTypedLiteral(lex, dtype);
    }

    /** {@inheritDoc} */
    @Override
    public Literal createTypedLiteral(Object value, RDFDatatype dtype) {
        return actualModel.createTypedLiteral(value, dtype);
    }

    /** {@inheritDoc} */
    @Override
    public Literal createTypedLiteral(Object value) {
        return actualModel.createTypedLiteral(value);
    }

    /** {@inheritDoc} */
    @Override
    public Statement createStatement(Resource s, Property p, RDFNode o) {
        return actualModel.createStatement(s, p, o);
    }

    /** {@inheritDoc} */
    @Override
    public RDFList createList() {
        return actualModel.createList();
    }

    /** {@inheritDoc} */
    @Override
    public RDFList createList(Iterator<? extends RDFNode> members) {
        return actualModel.createList(members);
    }

    /** {@inheritDoc} */
    @Override
    public RDFList createList(RDFNode[] members) {
        return actualModel.createList(members);
    }

    /** {@inheritDoc} */
    @Override
    public Model add(Statement s) {
        return actualModel.add(s);
    }

    /** {@inheritDoc} */
    @Override
    public Model add(Statement[] statements) {
        return actualModel.add(statements);
    }

    /** {@inheritDoc} */
    @Override
    public Model remove(Statement[] statements) {
        return actualModel.remove(statements);
    }

    /** {@inheritDoc} */
    @Override
    public Model add(List<Statement> statements) {
        return actualModel.add(statements);
    }

    /** {@inheritDoc} */
    @Override
    public Model remove(List<Statement> statements) {
        return actualModel.remove(statements);
    }

    /** {@inheritDoc} */
    @Override
    public Model add(StmtIterator iter) {
        return actualModel.add(iter);
    }

    /** {@inheritDoc} */
    @Override
    public Model add(Model m) {
        return actualModel.add(m);
    }

    /** {@inheritDoc} */
    @Override
    public Model read(String url) {
        return actualModel.read(url);
    }

    /** {@inheritDoc} */
    @Override
    public Model read(InputStream in, String base) {
        return actualModel.read(in, base);
    }

    /** {@inheritDoc} */
    @Override
    public Model read(InputStream in, String base, String lang) {
        return actualModel.read(in, base, lang);
    }

    /** {@inheritDoc} */
    @Override
    public Model read(Reader reader, String base) {
        return actualModel.read(reader, base);
    }

    /** {@inheritDoc} */
    @Override
    public Model read(String url, String lang) {
        return actualModel.read(url, lang);
    }

    /** {@inheritDoc} */
    @Override
    public Model read(Reader reader, String base, String lang) {
        return actualModel.read(reader, base, lang);
    }

    /** {@inheritDoc} */
    @Override
    public Model read(String url, String base, String lang) {
        return actualModel.read(url, base, lang);
    }

    /** {@inheritDoc} */
    @Override
    public Model write(Writer writer) {
        RDFDataMgr.write(writer, actualModel, RDFFormat.RDFXML_PLAIN);
        return actualModel;
    }

    /** {@inheritDoc} */
    @Override
    public Model write(Writer writer, String lang) {
        return actualModel.write(writer, lang);
    }

    /** {@inheritDoc} */
    @Override
    public Model write(Writer writer, String lang, String base) {
        return actualModel.write(writer, lang, base);
    }

    /** {@inheritDoc} */
    @Override
    public Model write(OutputStream out) {
        RDFDataMgr.write(out, actualModel, RDFFormat.RDFXML_PLAIN);
        return actualModel;
    }

    /** {@inheritDoc} */
    @Override
    public Model write(OutputStream out, String lang) {
        return actualModel.write(out, lang);
    }

    /** {@inheritDoc} */
    @Override
    public Model write(OutputStream out, String lang, String base) {
        return actualModel.write(out, lang, base);
    }

    /** {@inheritDoc} */
    @Override
    public Model remove(Statement s) {
        return actualModel.remove(s);
    }

    /** {@inheritDoc} */
    @Override
    public Statement getRequiredProperty(Resource s, Property p) {
        return actualModel.getRequiredProperty(s, p);
    }

    /** {@inheritDoc} */
    @Override
    public Statement getProperty(Resource s, Property p) {
        return actualModel.getProperty(s, p);
    }

    /** {@inheritDoc} */
    @Override
    public ResIterator listSubjectsWithProperty(Property p) {
        return actualModel.listSubjectsWithProperty(p);
    }

    /** {@inheritDoc} */
    @Override
    public ResIterator listResourcesWithProperty(Property p) {
        return actualModel.listResourcesWithProperty(p);
    }

    /** {@inheritDoc} */
    @Override
    public ResIterator listSubjectsWithProperty(Property p, RDFNode o) {
        return actualModel.listSubjectsWithProperty(p, o);
    }

    /** {@inheritDoc} */
    @Override
    public ResIterator listResourcesWithProperty(Property p, RDFNode o) {
        return actualModel.listResourcesWithProperty(p, o);
    }

    /** {@inheritDoc} */
    @Override
    public NodeIterator listObjects() {
        return actualModel.listObjects();
    }

    /** {@inheritDoc} */
    @Override
    public NodeIterator listObjectsOfProperty(Property p) {
        return actualModel.listObjectsOfProperty(p);
    }

    /** {@inheritDoc} */
    @Override
    public NodeIterator listObjectsOfProperty(Resource s, Property p) {
        return actualModel.listObjectsOfProperty(s, p);
    }

    /** {@inheritDoc} */
    @Override
    public boolean contains(Resource s, Property p) {
        return actualModel.contains(s, p);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsResource(RDFNode r) {
        return actualModel.containsResource(r);
    }

    /** {@inheritDoc} */
    @Override
    public boolean contains(Resource s, Property p, RDFNode o) {
        return actualModel.contains(s, p, o);
    }

    /** {@inheritDoc} */
    @Override
    public boolean contains(Statement s) {
        return actualModel.contains(s);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAny(StmtIterator iter) {
        return actualModel.containsAny(iter);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAll(StmtIterator iter) {
        return actualModel.containsAll(iter);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAny(Model model) {
        return actualModel.containsAny(model);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsAll(Model model) {
        return actualModel.containsAll(model);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isReified(Statement s) {
        return actualModel.isReified(s);
    }

    /** {@inheritDoc} */
    @Override
    public Resource getAnyReifiedStatement(Statement s) {
        return actualModel.getAnyReifiedStatement(s);
    }

    /** {@inheritDoc} */
    @Override
    public void removeAllReifications(Statement s) {
        actualModel.removeAllReifications(s);
    }

    /** {@inheritDoc} */
    @Override
    public void removeReification(ReifiedStatement rs) {
        actualModel.removeReification(rs);
    }

    /** {@inheritDoc} */
    @Override
    public StmtIterator listStatements() {
        return actualModel.listStatements();
    }

    /** {@inheritDoc} */
    @Override
    public StmtIterator listStatements(Selector s) {
        return actualModel.listStatements(s);
    }

    /** {@inheritDoc} */
    @Override
    public StmtIterator listStatements(Resource s, Property p, RDFNode o) {
        return actualModel.listStatements(s, p, o);
    }

    /** {@inheritDoc} */
    @Override
    public ReifiedStatement createReifiedStatement(Statement s) {
        return actualModel.createReifiedStatement(s);
    }

    /** {@inheritDoc} */
    @Override
    public ReifiedStatement createReifiedStatement(String uri, Statement s) {
        return actualModel.createReifiedStatement(uri, s);
    }

    /** {@inheritDoc} */
    @Override
    public RSIterator listReifiedStatements() {
        return actualModel.listReifiedStatements();
    }

    /** {@inheritDoc} */
    @Override
    public RSIterator listReifiedStatements(Statement st) {
        return actualModel.listReifiedStatements(st);
    }

    /** {@inheritDoc} */
    @Override
    public Model query(Selector s) {
        return actualModel.query(s);
    }

    /** {@inheritDoc} */
    @Override
    public Model union(Model model) {
        return actualModel.union(model);
    }

    /** {@inheritDoc} */
    @Override
    public Model intersection(Model model) {
        return actualModel.intersection(model);
    }

    /** {@inheritDoc} */
    @Override
    public Model difference(Model model) {
        return actualModel.difference(model);
    }

    /** {@inheritDoc} */
    @Override
    public Model begin() {
        return actualModel.begin();
    }

    /** {@inheritDoc} */
    @Override
    public Model abort() {
        return actualModel.abort();
    }

    /** {@inheritDoc} */
    @Override
    public Model commit() {
        return actualModel.commit();
    }

    /** {@inheritDoc} */
    @Override
    public Object executeInTransaction(Command cmd) {
        return actualModel.executeInTransaction(cmd);
    }

    /** {@inheritDoc} */
    @Override
    public boolean independent() {
        return actualModel.independent();
    }

    /** {@inheritDoc} */
    @Override
    public boolean supportsTransactions() {
        return actualModel.supportsTransactions();
    }

    /** {@inheritDoc} */
    @Override
    public boolean supportsSetOperations() {
        return actualModel.supportsSetOperations();
    }

    /** {@inheritDoc} */
    @Override
    public boolean isIsomorphicWith(Model g) {
        return actualModel.isIsomorphicWith(g);
    }

    /** {@inheritDoc} */
    @Override
    public void close() {
        actualModel.close();
    }

    /** {@inheritDoc} */
    @Override
    public Lock getLock() {
        return actualModel.getLock();
    }

    /** {@inheritDoc} */
    @Override
    public Model register(ModelChangedListener listener) {
        return actualModel.register(listener);
    }

    /** {@inheritDoc} */
    @Override
    public Model unregister(ModelChangedListener listener) {
        return actualModel.unregister(listener);
    }

    /** {@inheritDoc} */
    @Override
    public Model notifyEvent(Object e) {
        return actualModel.notifyEvent(e);
    }

    /** {@inheritDoc} */
    @Override
    public Model removeAll() {
        return actualModel.removeAll();
    }

    /** {@inheritDoc} */
    @Override
    public Model removeAll(Resource s, Property p, RDFNode r) {
        return actualModel.removeAll(s, p, r);
    }

    /** {@inheritDoc} */
    @Override
    public boolean isClosed() {
        return actualModel.isClosed();
    }

    /** {@inheritDoc} */
    @Override
    public void enterCriticalSection(boolean readLockRequested) {
        actualModel.enterCriticalSection(readLockRequested);
    }

    /** {@inheritDoc} */
    @Override
    public void leaveCriticalSection() {
        actualModel.leaveCriticalSection();
    }

    /** {@inheritDoc} */
    @Override
    public Resource getResource(String uri, ResourceF f) {
        return actualModel.getResource(uri, f);
    }

    /** {@inheritDoc} */
    @Override
    public Property getProperty(String uri) {
        return actualModel.getProperty(uri);
    }

    /** {@inheritDoc} */
    @Override
    public Bag getBag(String uri) {
        return actualModel.getBag(uri);
    }

    /** {@inheritDoc} */
    @Override
    public Bag getBag(Resource r) {
        return actualModel.getBag(r);
    }

    /** {@inheritDoc} */
    @Override
    public Alt getAlt(String uri) {
        return actualModel.getAlt(uri);
    }

    /** {@inheritDoc} */
    @Override
    public Alt getAlt(Resource r) {
        return actualModel.getAlt(r);
    }

    /** {@inheritDoc} */
    @Override
    public Seq getSeq(String uri) {
        return actualModel.getSeq(uri);
    }

    /** {@inheritDoc} */
    @Override
    public Seq getSeq(Resource r) {
        return actualModel.getSeq(r);
    }

    /** {@inheritDoc} */
    @Override
    public Resource createResource(Resource type) {
        return actualModel.createResource(type);
    }

    /** {@inheritDoc} */
    @Override
    public RDFNode getRDFNode(Node n) {
        return actualModel.getRDFNode(n);
    }

    /** {@inheritDoc} */
    @Override
    public Resource createResource(String uri, Resource type) {
        return actualModel.createResource(uri, type);
    }

    /** {@inheritDoc} */
    @Override
    public Resource createResource(ResourceF f) {
        return actualModel.createResource(f);
    }

    /** {@inheritDoc} */
    @Override
    public Resource createResource(String uri, ResourceF f) {
        return actualModel.createResource(uri, f);
    }

    /** {@inheritDoc} */
    @Override
    public Property createProperty(String uri) {
        return actualModel.createProperty(uri);
    }

    /** {@inheritDoc} */
    @Override
    public Literal createLiteral(String v) {
        return actualModel.createLiteral(v);
    }

    /** {@inheritDoc} */
    @Override
    public Literal createTypedLiteral(boolean v) {
        return actualModel.createTypedLiteral(v);
    }

    /** {@inheritDoc} */
    @Override
    public Literal createTypedLiteral(int v) {
        return actualModel.createTypedLiteral(v);
    }

    /** {@inheritDoc} */
    @Override
    public Literal createTypedLiteral(long v) {
        return actualModel.createTypedLiteral(v);
    }

    /** {@inheritDoc} */
    @Override
    public Literal createTypedLiteral(Calendar d) {
        return actualModel.createTypedLiteral(d);
    }

    /** {@inheritDoc} */
    @Override
    public Literal createTypedLiteral(char v) {
        return actualModel.createTypedLiteral(v);
    }

    /** {@inheritDoc} */
    @Override
    public Literal createTypedLiteral(float v) {
        return actualModel.createTypedLiteral(v);
    }

    /** {@inheritDoc} */
    @Override
    public Literal createTypedLiteral(double v) {
        return actualModel.createTypedLiteral(v);
    }

    /** {@inheritDoc} */
    @Override
    public Literal createTypedLiteral(String v) {
        return actualModel.createTypedLiteral(v);
    }

    /** {@inheritDoc} */
    @Override
    public Literal createTypedLiteral(String lex, String typeURI) {
        return actualModel.createTypedLiteral(lex, typeURI);
    }

    /** {@inheritDoc} */
    @Override
    public Literal createTypedLiteral(Object value, String typeURI) {
        return actualModel.createTypedLiteral(value, typeURI);
    }

    /** {@inheritDoc} */
    @Override
    public Statement createLiteralStatement(Resource s, Property p, boolean o) {
        return actualModel.createLiteralStatement(s, p, o);
    }

    /** {@inheritDoc} */
    @Override
    public Statement createLiteralStatement(Resource s, Property p, float o) {
        return actualModel.createLiteralStatement(s, p, o);
    }

    /** {@inheritDoc} */
    @Override
    public Statement createLiteralStatement(Resource s, Property p, double o) {
        return actualModel.createLiteralStatement(s, p, o);
    }

    /** {@inheritDoc} */
    @Override
    public Statement createLiteralStatement(Resource s, Property p, long o) {
        return actualModel.createLiteralStatement(s, p, o);
    }

    /** {@inheritDoc} */
    @Override
    public Statement createLiteralStatement(Resource s, Property p, int o) {
        return actualModel.createLiteralStatement(s, p, o);
    }

    /** {@inheritDoc} */
    @Override
    public Statement createLiteralStatement(Resource s, Property p, char o) {
        return actualModel.createLiteralStatement(s, p, o);
    }

    /** {@inheritDoc} */
    @Override
    public Statement createLiteralStatement(Resource s, Property p, Object o) {
        return actualModel.createLiteralStatement(s, p, o);
    }

    /** {@inheritDoc} */
    @Override
    public Statement createStatement(Resource s, Property p, String o) {
        return actualModel.createStatement(s, p, o);
    }

    /** {@inheritDoc} */
    @Override
    public Statement createStatement(Resource s, Property p, String o, String l) {
        return actualModel.createStatement(s, p, o, l);
    }

    /** {@inheritDoc} */
    @Override
    public Statement createStatement(Resource s, Property p, String o, boolean wellFormed) {
        return actualModel.createStatement(s, p, o, wellFormed);
    }

    /** {@inheritDoc} */
    @Override
    public Statement createStatement(Resource s, Property p, String o, String l, boolean wellFormed) {
        return actualModel.createStatement(s, p, o, l, wellFormed);
    }

    /** {@inheritDoc} */
    @Override
    public Bag createBag() {
        return actualModel.createBag();
    }

    /** {@inheritDoc} */
    @Override
    public Bag createBag(String uri) {
        return actualModel.createBag(uri);
    }

    /** {@inheritDoc} */
    @Override
    public Alt createAlt() {
        return actualModel.createAlt();
    }

    /** {@inheritDoc} */
    @Override
    public Alt createAlt(String uri) {
        return actualModel.createAlt(uri);
    }

    /** {@inheritDoc} */
    @Override
    public Seq createSeq() {
        return actualModel.createSeq();
    }

    /** {@inheritDoc} */
    @Override
    public Seq createSeq(String uri) {
        return actualModel.createSeq(uri);
    }

    /** {@inheritDoc} */
    @Override
    public Model add(Resource s, Property p, RDFNode o) {
        return actualModel.add(s, p, o);
    }

    /** {@inheritDoc} */
    @Override
    public Model addLiteral(Resource s, Property p, boolean o) {
        return actualModel.addLiteral(s, p, o);
    }

    /** {@inheritDoc} */
    @Override
    public Model addLiteral(Resource s, Property p, long o) {
        return actualModel.addLiteral(s, p, o);
    }

    /** {@inheritDoc} */
    @Override
    public Model addLiteral(Resource s, Property p, int o) {
        return actualModel.addLiteral(s, p, o);
    }

    /** {@inheritDoc} */
    @Override
    public Model addLiteral(Resource s, Property p, char o) {
        return actualModel.addLiteral(s, p, o);
    }

    /** {@inheritDoc} */
    @Override
    public Model addLiteral(Resource s, Property p, float o) {
        return actualModel.addLiteral(s, p, o);
    }

    /** {@inheritDoc} */
    @Override
    public Model addLiteral(Resource s, Property p, double o) {
        return actualModel.addLiteral(s, p, o);
    }

    /** {@inheritDoc} */
    @Override
    public Model addLiteral(Resource s, Property p, Object o) {
        return actualModel.addLiteral(s, p, o);
    }

    /** {@inheritDoc} */
    @Override
    public Model addLiteral(Resource s, Property p, Literal o) {
        return actualModel.addLiteral(s, p, o);
    }

    /** {@inheritDoc} */
    @Override
    public Model add(Resource s, Property p, String o) {
        return actualModel.add(s, p, o);
    }

    /** {@inheritDoc} */
    @Override
    public Model add(Resource s, Property p, String lex, RDFDatatype datatype) {
        return actualModel.add(s, p, lex, datatype);
    }

    /** {@inheritDoc} */
    @Override
    public Model add(Resource s, Property p, String o, boolean wellFormed) {
        return actualModel.add(s, p, o, wellFormed);
    }

    /** {@inheritDoc} */
    @Override
    public Model add(Resource s, Property p, String o, String l) {
        return actualModel.add(s, p, o, l);
    }

    /** {@inheritDoc} */
    @Override
    public Model remove(Resource s, Property p, RDFNode o) {
        return actualModel.remove(s, p, o);
    }

    /** {@inheritDoc} */
    @Override
    public Model remove(StmtIterator iter) {
        return actualModel.remove(iter);
    }

    /** {@inheritDoc} */
    @Override
    public Model remove(Model m) {
        return actualModel.remove(m);
    }

    /** {@inheritDoc} */
    @Override
    public StmtIterator listLiteralStatements(Resource subject, Property predicate, boolean object) {
        return actualModel.listLiteralStatements(subject, predicate, object);
    }

    /** {@inheritDoc} */
    @Override
    public StmtIterator listLiteralStatements(Resource subject, Property predicate, char object) {
        return actualModel.listLiteralStatements(subject, predicate, object);
    }

    /** {@inheritDoc} */
    @Override
    public StmtIterator listLiteralStatements(Resource subject, Property predicate, long object) {
        return actualModel.listLiteralStatements(subject, predicate, object);
    }

    /** {@inheritDoc} */
    @Override
    public StmtIterator listLiteralStatements(Resource subject, Property predicate, float object) {
        return actualModel.listLiteralStatements(subject, predicate, object);
    }

    /** {@inheritDoc} */
    @Override
    public StmtIterator listLiteralStatements(Resource subject, Property predicate, double object) {
        return actualModel.listLiteralStatements(subject, predicate, object);
    }

    /** {@inheritDoc} */
    @Override
    public StmtIterator listStatements(Resource subject, Property predicate, String object) {
        return actualModel.listStatements(subject, predicate, object);
    }

    /** {@inheritDoc} */
    @Override
    public StmtIterator listStatements(Resource subject, Property predicate, String object, String lang) {
        return actualModel.listStatements(subject, predicate, object, lang);
    }

    /** {@inheritDoc} */
    @Override
    public ResIterator listResourcesWithProperty(Property p, boolean o) {
        return actualModel.listResourcesWithProperty(p, o);
    }

    /** {@inheritDoc} */
    @Override
    public ResIterator listResourcesWithProperty(Property p, long o) {
        return actualModel.listResourcesWithProperty(p, o);
    }

    /** {@inheritDoc} */
    @Override
    public ResIterator listResourcesWithProperty(Property p, char o) {
        return actualModel.listResourcesWithProperty(p, o);
    }

    /** {@inheritDoc} */
    @Override
    public ResIterator listResourcesWithProperty(Property p, float o) {
        return actualModel.listResourcesWithProperty(p, o);
    }

    /** {@inheritDoc} */
    @Override
    public ResIterator listResourcesWithProperty(Property p, double o) {
        return actualModel.listResourcesWithProperty(p, o);
    }

    /** {@inheritDoc} */
    @Override
    public ResIterator listResourcesWithProperty(Property p, Object o) {
        return actualModel.listResourcesWithProperty(p, o);
    }

    /** {@inheritDoc} */
    @Override
    public ResIterator listSubjectsWithProperty(Property p, String o) {
        return actualModel.listSubjectsWithProperty(p, o);
    }

    /** {@inheritDoc} */
    @Override
    public ResIterator listSubjectsWithProperty(Property p, String o, String l) {
        return actualModel.listSubjectsWithProperty(p, o, l);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsLiteral(Resource s, Property p, boolean o) {
        return actualModel.containsLiteral(s, p, o);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsLiteral(Resource s, Property p, long o) {
        return actualModel.containsLiteral(s, p, o);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsLiteral(Resource s, Property p, int o) {
        return actualModel.containsLiteral(s, p, o);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsLiteral(Resource s, Property p, char o) {
        return actualModel.containsLiteral(s, p, o);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsLiteral(Resource s, Property p, float o) {
        return actualModel.containsLiteral(s, p, o);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsLiteral(Resource s, Property p, double o) {
        return actualModel.containsLiteral(s, p, o);
    }

    /** {@inheritDoc} */
    @Override
    public boolean containsLiteral(Resource s, Property p, Object o) {
        return actualModel.containsLiteral(s, p, o);
    }

    /** {@inheritDoc} */
    @Override
    public boolean contains(Resource s, Property p, String o) {
        return actualModel.contains(s, p, o);
    }

    /** {@inheritDoc} */
    @Override
    public boolean contains(Resource s, Property p, String o, String l) {
        return actualModel.contains(s, p, o, l);
    }

    /** {@inheritDoc} */
    @Override
    public Statement asStatement(Triple t) {
        return actualModel.asStatement(t);
    }

    /** {@inheritDoc} */
    @Override
    public Graph getGraph() {
        return actualModel.getGraph();
    }

    /** {@inheritDoc} */
    @Override
    public RDFNode asRDFNode(Node n) {
        return actualModel.asRDFNode(n);
    }

    /** {@inheritDoc} */
    @Override
    public Resource wrapAsResource(Node n) {
        return actualModel.wrapAsResource(n);
    }

    /** {@inheritDoc} */
    @Override
    public PrefixMapping setNsPrefix(String prefix, String uri) {
        return actualModel.setNsPrefix(prefix, uri);
    }

    /** {@inheritDoc} */
    @Override
    public PrefixMapping removeNsPrefix(String prefix) {
        return actualModel.removeNsPrefix(prefix);
    }

    /** {@inheritDoc} */
    @Override
    public PrefixMapping setNsPrefixes(PrefixMapping other) {
        return actualModel.setNsPrefixes(other);
    }

    /** {@inheritDoc} */
    @Override
    public PrefixMapping setNsPrefixes(Map<String, String> map) {
        return actualModel.setNsPrefixes(map);
    }

    /** {@inheritDoc} */
    @Override
    public PrefixMapping withDefaultMappings(PrefixMapping map) {
        return actualModel.withDefaultMappings(map);
    }

    /** {@inheritDoc} */
    @Override
    public String getNsPrefixURI(String prefix) {
        return actualModel.getNsPrefixURI(prefix);
    }

    /** {@inheritDoc} */
    @Override
    public String getNsURIPrefix(String uri) {
        return actualModel.getNsURIPrefix(uri);
    }

    /** {@inheritDoc} */
    @Override
    public Map<String, String> getNsPrefixMap() {
        return actualModel.getNsPrefixMap();
    }

    /** {@inheritDoc} */
    @Override
    public String expandPrefix(String prefixed) {
        return actualModel.expandPrefix(prefixed);
    }

    /** {@inheritDoc} */
    @Override
    public String shortForm(String uri) {
        return actualModel.shortForm(uri);
    }

    /** {@inheritDoc} */
    @Override
    public String qnameFor(String uri) {
        return actualModel.qnameFor(uri);
    }

    /** {@inheritDoc} */
    @Override
    public PrefixMapping lock() {
        return actualModel.lock();
    }

    /** {@inheritDoc} */
    @Override
    public boolean samePrefixMappingAs(PrefixMapping other) {
        return actualModel.samePrefixMappingAs(other);
    }

    /** {@inheritDoc} */
    @Override
    public RDFReader getReader() {
        return actualModel.getReader();
    }

    /** {@inheritDoc} */
    @Override
    public RDFReader getReader(String lang) {
        return actualModel.getReader(lang);
    }

    /** {@inheritDoc} */
    @Override
    public String setReaderClassName(String lang, String className) {
        return actualModel.setReaderClassName(lang, className);
    }

    /** {@inheritDoc} */
    @Override
    public RDFWriter getWriter() {
        return actualModel.getWriter();
    }

    /** {@inheritDoc} */
    @Override
    public RDFWriter getWriter(String lang) {
        return actualModel.getWriter(lang);
    }

    /** {@inheritDoc} */
    @Override
    public String setWriterClassName(String lang, String className) {
        return actualModel.setWriterClassName(lang, className);
    }
}
