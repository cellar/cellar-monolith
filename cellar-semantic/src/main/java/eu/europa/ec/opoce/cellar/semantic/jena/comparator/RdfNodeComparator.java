package eu.europa.ec.opoce.cellar.semantic.jena.comparator;

import org.apache.jena.rdf.model.RDFNode;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.exception.CellarSemanticException;

import java.util.Comparator;

/**
 * <p>RdfNodeComparator class.</p>
 */
public class RdfNodeComparator implements Comparator<RDFNode> {

    private ResourceComparator resourceComparator = new ResourceComparator();
    private LiteralComparator literalComparator = new LiteralComparator();

    /**
     * {@inheritDoc}
     */
    @Override
    public int compare(RDFNode one, RDFNode other) {
        if (one.isResource() && other.isResource()) {
            return resourceComparator.compare(one.asResource(), other.asResource());
        } else if (one.isLiteral() && other.isLiteral()) {
            return literalComparator.compare(one.asLiteral(), other.asLiteral());
        } else if (one.isResource() && other.isLiteral()) {
            return -1;
        } else if (one.isLiteral() && other.isResource()) {
            return 1;
        }

        throw ExceptionBuilder.get(CellarSemanticException.class).withMessage("Unreachable.").build();
    }
}
