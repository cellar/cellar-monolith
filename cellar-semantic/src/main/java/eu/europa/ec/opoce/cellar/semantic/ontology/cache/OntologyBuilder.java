package eu.europa.ec.opoce.cellar.semantic.ontology.cache;

import org.apache.jena.ontology.AnnotationProperty;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.util.iterator.ExtendedIterator;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.OWL2;
import org.apache.jena.vocabulary.RDFS;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.Ontology;
import eu.europa.ec.opoce.cellar.semantic.exception.CellarSemanticException;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.semantic.jena.template.JenaTemplate;
import eu.europa.ec.opoce.cellar.semantic.ontology.cache.annotation.AnnotationProperties;
import eu.europa.ec.opoce.cellar.semantic.ontology.cache.annotation.Classes;
import eu.europa.ec.opoce.cellar.semantic.ontology.cache.annotation.MaxCardinalities;
import eu.europa.ec.opoce.cellar.semantic.ontology.cache.annotation.MinCardinalities;
import eu.europa.ec.opoce.cellar.semantic.ontology.cache.annotation.Properties;
import eu.europa.ec.opoce.cellar.semantic.ontology.cache.annotation.PropertyDatatype;
import eu.europa.ec.opoce.cellar.semantic.ontology.cache.annotation.PropertyDomains;
import eu.europa.ec.opoce.cellar.semantic.ontology.cache.annotation.PropertyRanges;
import eu.europa.ec.opoce.cellar.semantic.ontology.cache.annotation.PropertyType;
import eu.europa.ec.opoce.cellar.semantic.ontology.cache.annotation.SingleMaxCardinalityProperties;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * <p>OntologyBuilder class.</p>
 */
public class OntologyBuilder {

    /**
     * <p>build</p>
     *
     * @param clazz    The ontology class to load, should be an interface and all methods should have 1 ontology cache annotation
     * @param ontology The ontology to use.
     * @param <T>      a T object.
     * @return A proxy instance for the given clazz.
     */
    @SuppressWarnings("unchecked")
    public static <T> T build(Class<T> clazz, Ontology ontology) {
        //noinspection unchecked
        return (T) Proxy.newProxyInstance(OntologyBuilder.class.getClassLoader(), new Class[]{
                clazz}, new OntologyInvocationHandler(ontology, clazz));
    }

    private static class OntologyInvocationHandler implements InvocationHandler {

        private final Map<Method, Invoker> invokerMap = new HashMap<>();

        private Ontology ontology;
        private List<OntProperty> allOntProperties;
        private List<OntClass> allOntClasses;

        private OntologyInvocationHandler(Ontology ontology, Class<?> clazz) {
            this.ontology = ontology;
            for (Method method : clazz.getMethods()) {
                init(method);
            }
            clean();
        }

        private void clean() {
            allOntProperties = null;
            allOntClasses = null;
            ontology = null;
        }

        private void init(Method method) {
            int foundAnnotations = 0;
            Properties properties = method.getAnnotation(Properties.class);
            if (null != properties)
                foundAnnotations++;
            PropertyDatatype propertyDatatype = method.getAnnotation(PropertyDatatype.class);
            if (null != propertyDatatype)
                foundAnnotations++;
            PropertyRanges propertyRanges = method.getAnnotation(PropertyRanges.class);
            if (null != propertyRanges)
                foundAnnotations++;
            PropertyDomains propertyDomains = method.getAnnotation(PropertyDomains.class);
            if (null != propertyDomains)
                foundAnnotations++;
            Classes classes = method.getAnnotation(Classes.class);
            if (null != classes)
                foundAnnotations++;
            MinCardinalities minCardinalities = method.getAnnotation(MinCardinalities.class);
            if (null != minCardinalities)
                foundAnnotations++;
            MaxCardinalities maxCardinalities = method.getAnnotation(MaxCardinalities.class);
            if (null != maxCardinalities)
                foundAnnotations++;
            SingleMaxCardinalityProperties singleMaxCardinalityProperties = method.getAnnotation(SingleMaxCardinalityProperties.class);
            if (null != singleMaxCardinalityProperties)
                foundAnnotations++;
            AnnotationProperties annotationProperties = method.getAnnotation(AnnotationProperties.class);
            if (null != annotationProperties)
                foundAnnotations++;

            if (0 == foundAnnotations) {
                throw ExceptionBuilder.get(CellarSemanticException.class).withMessage("No Ontology annotations found for method {}")
                        .withMessageArgs(method.getName()).build();
            }
            if (1 < foundAnnotations) {
                throw ExceptionBuilder.get(CellarSemanticException.class).withMessage("More than 1 Ontology annotations found for method {}")
                        .withMessageArgs(method.getName()).build();
            }

            if (null != properties) {
                initProperties(properties, method);
            } else if (null != propertyDatatype) {
                initPropertyDatatype(method);
            } else if (null != propertyRanges) {
                initPropertyRanges(method);
            } else if (null != propertyDomains) {
                initPropertyDomains(method);
            } else if (null != classes) {
                initClasses(method);
            } else if (null != minCardinalities) {
                initMinCardinalities(method);
            } else if (null != maxCardinalities) {
                initMaxCardinalities(method);
            } else if (null != singleMaxCardinalityProperties) {
                initSingleMaxCardinalityProperties(method);
            } else if (null != annotationProperties) {
                initAnnotationProperties(annotationProperties, method);
            }
        }

        private void initProperties(Properties properties, Method method) {
            if (!method.getReturnType().isAssignableFrom(SortedSet.class)) {
                throw ExceptionBuilder.get(CellarSemanticException.class)
                        .withMessage("Only returntype SortedSet is supported for @Properties").build();
            }
            if (method.getParameterTypes().length != 0) {
                throw ExceptionBuilder.get(CellarSemanticException.class)
                        .withMessage("No methodarguments are supported for @Properties").build();
            }

            String[] annotationUris = properties.annotations();
            if (annotationUris.length == 0) {
                initAllProperties(method, properties.type());
            } else {
                initAnnotatedProperties(method, properties.type(), annotationUris);
            }
        }

        private void initAllProperties(Method method, PropertyType propertyType) {
            SortedSet<String> propertyUris = new TreeSet<>();
            for (OntProperty ontProperty : loadOntProperties(propertyType)) {
                if (!ontProperty.isURIResource())
                    continue;
                propertyUris.add(ontProperty.getURI());
            }
            invokerMap.put(method, new NoArgumentInvoker(propertyUris));
        }

        private void initAnnotatedProperties(Method method, PropertyType propertyType, String[] annotationUris) {
            List<AnnotationProperty> annotationProperties = new ArrayList<>();
            for (String annotationUri : annotationUris) {
                AnnotationProperty annotationProperty = ontology.getOntModel().getAnnotationProperty(annotationUri);
                if (null == annotationProperty) {
                    throw ExceptionBuilder.get(CellarSemanticException.class)
                            .withMessage("AnnotationProperty {} not found in ontology").withMessageArgs(annotationUri).build();
                }

                annotationProperties.add(annotationProperty);
            }

            SortedSet<String> propertyUris = new TreeSet<>();
            for (OntProperty ontProperty : loadOntProperties(propertyType)) {
                if (!ontProperty.isURIResource() || !containsAllAnnotations(ontProperty, annotationProperties))
                    continue;
                propertyUris.add(ontProperty.getURI());
            }
            invokerMap.put(method, new NoArgumentInvoker(propertyUris));
        }

        private boolean containsAllAnnotations(OntProperty ontProperty, List<AnnotationProperty> annotationProperties) {
            for (AnnotationProperty annotationProperty : annotationProperties) {
                if (!ontProperty.hasProperty(annotationProperty))
                    return false;
            }
            return true;
        }

        private void initPropertyDatatype(Method method) {
            if (!method.getReturnType().isAssignableFrom(String.class)) {
                throw ExceptionBuilder.get(CellarSemanticException.class)
                        .withMessage("Only returntype java.lang.String is supported for @PropertyDatatype").build();
            }
            if (method.getParameterTypes().length != 1 || !method.getParameterTypes()[0].equals(String.class)) {
                throw ExceptionBuilder.get(CellarSemanticException.class)
                        .withMessage("Just one methodargument of type java.lang.String needed for @PropertyDatatype").build();
            }

            Map<String, String> map = new HashMap<>();
            for (OntProperty ontProperty : loadOntProperties(PropertyType.Data)) {
                if (!ontProperty.isURIResource() || ontProperty.equals(OWL2.bottomDataProperty))
                    continue;

                Set<? extends Resource> ranges = ontProperty.listRange().toSet();
                ranges.remove(RDFS.Literal);

                if (ranges.isEmpty())
                    continue;
                String propertyUri = ontProperty.getURI();
                if (ranges.size() > 1) {
                    throw ExceptionBuilder.get(CellarSemanticException.class)
                            .withMessage("Found more than 1 datatype for property = {}: {}").withMessageArgs(propertyUri, ranges).build();
                }

                Resource range = ranges.iterator().next();
                if (!range.isURIResource()) {
                    throw ExceptionBuilder.get(CellarSemanticException.class)
                            .withMessage("Non uri datatype not supported (property = {}): {}").withMessageArgs(propertyUri, ranges).build();
                }

                map.put(propertyUri, range.getURI());
            }
            invokerMap.put(method, new MapInvoker(map, null));
        }

        private void initPropertyRanges(Method method) {
            if (!method.getReturnType().isAssignableFrom(SortedSet.class)) {
                throw ExceptionBuilder.get(CellarSemanticException.class)
                        .withMessage("Only returntype SortedSet is supported for @PropertyRanges").build();
            }
            if (method.getParameterTypes().length != 1 || !method.getParameterTypes()[0].equals(String.class)) {
                throw ExceptionBuilder.get(CellarSemanticException.class)
                        .withMessage("Just one methodargument of type java.lang.String needed for @PropertyRanges").build();
            }

            Map<String, SortedSet<String>> map = new HashMap<>();
            for (OntProperty ontProperty : loadOntProperties(PropertyType.Object)) {
                if (!ontProperty.isURIResource())
                    continue;

                SortedSet<String> rangeUris = new TreeSet<>();
                ExtendedIterator<? extends OntResource> ranges = ontProperty.listRange();
                try {
                    while (ranges.hasNext()) {
                        OntResource range = ranges.next();
                        if (!range.isURIResource() || range.equals(OWL.Thing))
                            continue;
                        rangeUris.add(range.getURI());
                    }
                    map.put(ontProperty.getURI(), Collections.unmodifiableSortedSet(rangeUris));
                } finally {
                    ranges.close();
                }
            }
            invokerMap.put(method, new MapInvoker(map, Collections.unmodifiableSortedSet(new TreeSet<>())));
        }

        private void initPropertyDomains(Method method) {
            if (!method.getReturnType().isAssignableFrom(SortedSet.class)) {
                throw ExceptionBuilder.get(CellarSemanticException.class)
                        .withMessage("Only returntype SortedSet is supported for @PropertyDomains").build();
            }
            if (method.getParameterTypes().length != 1 || !method.getParameterTypes()[0].equals(String.class)) {
                throw ExceptionBuilder.get(CellarSemanticException.class)
                        .withMessage("Just one methodargument of type java.lang.String needed for @PropertyDomains").build();
            }

            Map<String, SortedSet<String>> map = new HashMap<>();
            for (OntProperty ontProperty : loadOntProperties(PropertyType.ANY)) {
                if (!ontProperty.isURIResource())
                    continue;

                SortedSet<String> domainUris = new TreeSet<>();

                ExtendedIterator<? extends OntResource> domains = ontProperty.listDomain();
                try {
                    while (domains.hasNext()) {
                        OntResource domain = domains.next();
                        if (!domain.isURIResource() || domain.equals(OWL.Thing))
                            continue;
                        domainUris.add(domain.getURI());
                    }
                    map.put(ontProperty.getURI(), Collections.unmodifiableSortedSet(domainUris));
                } finally {
                    domains.close();
                }
            }
            invokerMap.put(method, new MapInvoker(map, Collections.unmodifiableSortedSet(new TreeSet<>())));
        }

        private void initClasses(Method method) {
            if (!method.getReturnType().isAssignableFrom(SortedSet.class)) {
                throw ExceptionBuilder.get(CellarSemanticException.class)
                        .withMessage("Only returntype SortedSet is supported for @Classes").build();
            }
            if (method.getParameterTypes().length != 0) {
                throw ExceptionBuilder.get(CellarSemanticException.class)
                        .withMessage("No methodarguments are supported for @Classes").build();
            }

            SortedSet<String> classUris = new TreeSet<>();
            for (OntClass ontClass : loadOntClasses()) {
                if (!ontClass.isURIResource())
                    continue;
                classUris.add(ontClass.getURI());
            }
            invokerMap.put(method, new NoArgumentInvoker(classUris));
        }

        private void initMinCardinalities(final Method method) {
            final String query = "" + //
                    "PREFIX owl: <http://www.w3.org/2002/07/owl#> " + //
                    "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " + //
                    "SELECT ?c ?p ?l ?o " + //
                    "WHERE { " + //
                    "  ?c rdfs:subClassOf ?r . " + //
                    "  ?r a owl:Restriction . " + //
                    "  ?r owl:onProperty ?p . " + //
                    "  {?r owl:minQualifiedCardinality ?l . } UNION {?r owl:minCardinality ?l . } UNION {?r owl:cardinality ?l . } . " + //
                    "  OPTIONAL {?r owl:onClass ?o . } . " + //
                    "}";
            this.initCardinalities(method, query);
        }

        private void initMaxCardinalities(final Method method) {
            final String query = "" + //
                    "PREFIX owl: <http://www.w3.org/2002/07/owl#> " + //
                    "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " + //
                    "SELECT ?c ?p ?l ?o " + //
                    "WHERE { " + //
                    "  ?c rdfs:subClassOf ?r . " + //
                    "  ?r a owl:Restriction . " + //
                    "  ?r owl:onProperty ?p . " + //
                    "  ?r owl:maxCardinality ?l . " + //
                    "  OPTIONAL {?r owl:onClass ?o . } . " + //
                    "}";
            this.initCardinalities(method, query);
        }

        private void initSingleMaxCardinalityProperties(final Method method) {
            if (!method.getReturnType().isAssignableFrom(SortedSet.class)) {
                throw ExceptionBuilder.get(CellarSemanticException.class)
                        .withMessage("Only returntype SortedSet is supported for @SingleMaxCardinalityProperties").build();
            }
            if (method.getParameterTypes().length != 0) {
                throw ExceptionBuilder.get(CellarSemanticException.class)
                        .withMessage("No methodarguments are supported for @SingleMaxCardinalityProperties").build();
            }

            final String query = "" + //
                    "PREFIX owl: <http://www.w3.org/2002/07/owl#> " + //
                    "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> " + //
                    "SELECT ?c ?p ?o " + //
                    "WHERE { " + //
                    "  ?c rdfs:subClassOf ?r . " + //
                    "  ?r a owl:Restriction . " + //
                    "  ?r owl:onProperty ?p . " + //
                    "  ?r owl:maxCardinality 1 . " + //
                    "  OPTIONAL {?r owl:onClass ?o . } . " + //
                    "}";

            final SortedSet<String> values = new TreeSet<>();
            final List<Map<String, RDFNode>> maps = JenaTemplate.select(ontology.getModel(), query, false);
            for (Map<String, RDFNode> map : maps) {
                final RDFNode clazz = map.get("c");
                final RDFNode property = map.get("p");
                final RDFNode valueClazz = map.get("o");
                if (!clazz.isURIResource() || !property.isURIResource() || (null != valueClazz && !valueClazz.isURIResource())) {
                    continue;
                }

                values.add(property.asResource().getURI());
            }
            invokerMap.put(method, new NoArgumentInvoker(values));
        }

        private void initCardinalities(final Method method, final String query) {
            if (!method.getReturnType().isAssignableFrom(Cardinality[].class)) {
                throw ExceptionBuilder.get(CellarSemanticException.class)
                        .withMessage("Only returntype Cardinalities[] is supported for annotations representing cardinalities").build();
            }
            if (method.getParameterTypes().length != 0) {
                throw ExceptionBuilder.get(CellarSemanticException.class)
                        .withMessage("No methodarguments are supported for annotations representing cardinalities").build();
            }

            final List<Cardinality> values = new ArrayList<>();
            final List<Map<String, RDFNode>> maps = JenaTemplate.select(ontology.getModel(), query, false);
            for (Map<String, RDFNode> map : maps) {
                final RDFNode clazz = map.get("c");
                final RDFNode property = map.get("p");
                final RDFNode cardinality = map.get("l");
                final RDFNode valueClazz = map.get("o");
                if (!clazz.isURIResource() || !property.isURIResource() || (null != valueClazz && !valueClazz.isURIResource())) {
                    continue;
                }

                final String valueClazzUri = null == valueClazz ? null : valueClazz.asResource().getURI();
                values.add(new Cardinality(property.asResource().getURI(), clazz.asResource().getURI(), cardinality.asLiteral().getInt(),
                        valueClazzUri));
            }
            invokerMap.put(method, new NoArgumentInvoker(values.toArray(new Cardinality[values.size()])));
        }

        private List<OntProperty> loadOntProperties(PropertyType propertyType) {
            if (null == allOntProperties)
                allOntProperties = ontology.getOntModel().listOntProperties().toList();
            if (propertyType == PropertyType.ANY)
                return allOntProperties;

            //TODO: need to cache this array, could do this with a simpel Map<PropertyType, List>
            List<OntProperty> result = new ArrayList<>();
            for (OntProperty ontProperty : allOntProperties) {
                if (propertyType == PropertyType.Annotation && !ontProperty.isAnnotationProperty())
                    continue;
                if (propertyType == PropertyType.Data && !ontProperty.isDatatypeProperty())
                    continue;
                if (propertyType == PropertyType.Object && !ontProperty.isObjectProperty())
                    continue;
                result.add(ontProperty);
            }
            return result;
        }

        private void initAnnotationProperties(AnnotationProperties annotationProperties, Method method) {
            if (method.getParameterTypes().length != 1 || !method.getParameterTypes()[0].equals(String.class)) {
                throw ExceptionBuilder.get(CellarSemanticException.class)
                        .withMessage("Just one methodargument of type java.lang.String needed for @AnnotationProperties").build();
            }
            Class<?> returnType = method.getReturnType();
            boolean correctType = returnType.equals(String.class) || returnType.equals(Collection.class) || returnType.equals(Set.class)
                    || returnType.equals(List.class);
            if (!correctType) {
                throw ExceptionBuilder.get(CellarSemanticException.class)
                        .withMessage("ReturnType for @AnnotationProperties should be String, Collection, Set or List").build();
            }

            String annotationPropertyUri = annotationProperties.value();
            AnnotationProperty annotationProperty = ontology.getOntModel().getAnnotationProperty(annotationPropertyUri);
            if (null == annotationProperty) {
                throw ExceptionBuilder.get(CellarSemanticException.class)
                        .withMessage("AnnotationProperty {} not found").withMessageArgs(annotationPropertyUri).build();
            }

            Map<String, Object> map = new HashMap<>();
            for (OntProperty ontProperty : loadOntProperties(PropertyType.ANY)) {
                if (!ontProperty.hasProperty(annotationProperty))
                    continue;
                Object value = findAnnotationPropertiesValues(ontProperty, annotationProperty, returnType);
                map.put(ontProperty.getURI(), value);
            }

            if (annotationProperties.failOnNone()) {
                invokerMap.put(method, new MapInvoker(map));
            } else if (returnType.equals(String.class)) {
                invokerMap.put(method, new MapInvoker(map, null));
            } else if (returnType.equals(List.class)) {
                invokerMap.put(method, new MapInvoker(map, Collections.emptyList()));
            } else {
                invokerMap.put(method, new MapInvoker(map, Collections.emptySet()));
            }

        }

        private Object findAnnotationPropertiesValues(Resource source, Property property, Class<?> returnType) {
            Collection<String> result = returnType.equals(List.class) ? new ArrayList<>() : new HashSet<>();

            StmtIterator stmtIterator = source.listProperties(property);
            try {
                while (stmtIterator.hasNext()) {
                    result.add(JenaUtils.stringize(stmtIterator.nextStatement().getObject()));
                }
            } finally {
                stmtIterator.close();
            }

            if (!returnType.equals(String.class)) {
                return result;
            }
            if (result.isEmpty()) {
                return null;
            }

            return result.iterator().next();
        }

        private List<OntClass> loadOntClasses() {
            if (null == allOntClasses)
                allOntClasses = ontology.getOntModel().listClasses().toList();
            return allOntClasses;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            if (method.getName().equals("toString") && null == args) {
                return "OntologyDataObject";
            }

            Invoker invoker = invokerMap.get(method);
            if (null == invoker) {
                throw ExceptionBuilder.get(CellarSemanticException.class)
                        .withMessage("Invoker not initialized for method {}").withMessageArgs(method.getName()).build();
            }

            //noinspection ConstantConditions
            return invoker.getValue(args);
        }

        private interface Invoker {

            Object getValue(Object[] args);
        }

        private static class MapInvoker implements Invoker {

            private final Map<?, ?> values;
            private final Object defaultValue;
            private final boolean failOnNone;

            private MapInvoker(Map<?, ?> values, Object defaultValue) {
                this(values, defaultValue, false);
            }

            private MapInvoker(Map<?, ?> values) {
                this(values, null, true);
            }

            private MapInvoker(Map<?, ?> values, Object defaultValue, boolean failOnNone) {
                this.values = values;
                this.defaultValue = defaultValue;
                this.failOnNone = failOnNone;
            }

            @Override
            public Object getValue(Object[] args) {
                Object key = args[0];
                if (values.containsKey(key))
                    return values.get(key);
                if (failOnNone) {
                    throw ExceptionBuilder.get(CellarSemanticException.class)
                            .withMessage("No values found for {}").withMessageArgs(key).build();
                }

                return values.containsKey(key) ? values.get(key) : defaultValue;
            }
        }

        private static class NoArgumentInvoker implements Invoker {

            private final Object object;

            private NoArgumentInvoker(Object object) {
                this.object = object;
            }

            @Override
            public Object getValue(Object[] args) {
                return object;
            }
        }

    }
}
