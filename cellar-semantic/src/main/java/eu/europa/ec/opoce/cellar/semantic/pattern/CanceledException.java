package eu.europa.ec.opoce.cellar.semantic.pattern;

/**
 * <p>CanceledException class.</p>
 *
 */
public class CanceledException extends RuntimeException {

    private static final long serialVersionUID = 4223663220527526800L;

    /**
     * <p>Constructor for CanceledException.</p>
     */
    public CanceledException() {
    }

    /**
     * <p>Constructor for CanceledException.</p>
     *
     * @param message a {@link java.lang.String} object.
     */
    public CanceledException(String message) {
        super(message);
    }
}
