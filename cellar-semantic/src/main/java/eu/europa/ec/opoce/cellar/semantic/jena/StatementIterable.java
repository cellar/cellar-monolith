package eu.europa.ec.opoce.cellar.semantic.jena;

import java.util.Iterator;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;

/**
 * <p>StatementIterable class.</p>
 *
 */
public class StatementIterable implements Iterable<Statement> {

    private final StatementIterator statementIterator;

    /**
     * <p>Constructor for StatementIterable.</p>
     *
     * @param statements a {@link org.apache.jena.rdf.model.StmtIterator} object.
     */
    public StatementIterable(StmtIterator statements) {
        this.statementIterator = new StatementIterator(statements);
    }

    /** {@inheritDoc} */
    @Override
    public Iterator<Statement> iterator() {
        return statementIterator;
    }

    /**
     * <p>list</p>
     *
     * @param model a {@link org.apache.jena.rdf.model.Model} object.
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.jena.StatementIterable} object.
     */
    public static StatementIterable list(Model model) {
        return new StatementIterable(model.listStatements());
    }

    /**
     * <p>list</p>
     *
     * @param model a {@link org.apache.jena.rdf.model.Model} object.
     * @param subject a {@link org.apache.jena.rdf.model.Resource} object.
     * @param property a {@link org.apache.jena.rdf.model.Property} object.
     * @param value a {@link org.apache.jena.rdf.model.RDFNode} object.
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.jena.StatementIterable} object.
     */
    public static StatementIterable list(Model model, Resource subject, Property property, RDFNode value) {
        return new StatementIterable(model.listStatements(subject, property, value));
    }

    /**
     * <p>list</p>
     *
     * @param subject a {@link org.apache.jena.rdf.model.Resource} object.
     * @param property a {@link org.apache.jena.rdf.model.Property} object.
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.jena.StatementIterable} object.
     */
    public static StatementIterable list(Resource subject, Property property) {
        return new StatementIterable(subject.listProperties(property));
    }

    /**
     * <p>list</p>
     *
     * @param subject a {@link org.apache.jena.rdf.model.Resource} object.
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.jena.StatementIterable} object.
     */
    public static StatementIterable list(Resource subject) {
        return new StatementIterable(subject.listProperties());
    }

    private static class StatementIterator implements Iterator<Statement> {

        private final StmtIterator statements;

        public StatementIterator(StmtIterator statements) {
            this.statements = statements;
        }

        @Override
        public boolean hasNext() {
            return statements.hasNext();
        }

        @Override
        public Statement next() {
            return statements.nextStatement();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }

}
