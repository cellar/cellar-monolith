package eu.europa.ec.opoce.cellar.semantic.inferredontology;

import java.util.Collections;
import java.util.List;

import javax.annotation.PostConstruct;

import eu.europa.ec.opoce.cellar.cl.configuration.IConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

@Service
/**
 * <p>InferredOntologyConfiguration class.</p>
 *
 */
public class InferredOntologyConfiguration {

    private static final Logger LOG = LogManager.getLogger(InferredOntologyConfiguration.class);

    private static final String INFERRED_ONTOLOGY_CONFIGURATION = "eu/europa/ec/opoce/cellar/semantic/config/inferred-ontology-configuration.xml";

    private List<String> defaultQueries;

    private String selectSubPropertyOfQuery;
    private String selectSubClassOfQuery;
    private String selectInverseOf;
    private String selectRange;
    private String selectDomain;
    private String selectRestrictions;
    private String selectRestrictionsSup;
    private String selectRestrictionsSupEq;

    /**
     * <p>init</p>
     */
    @SuppressWarnings("unchecked")
    @PostConstruct
    private void init() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(INFERRED_ONTOLOGY_CONFIGURATION);
        context.start();

        //noinspection unchecked
        defaultQueries = Collections.unmodifiableList(context.getBean("defaultQueries", List.class));

        selectSubPropertyOfQuery = context.getBean("selectSubPropertyOf", String.class);
        selectSubClassOfQuery = context.getBean("selectSubClassOf", String.class);
        selectInverseOf = context.getBean("selectInverseOf", String.class);
        selectRange = context.getBean("selectRange", String.class);
        selectDomain = context.getBean("selectDomain", String.class);
        selectRestrictions = context.getBean("selectRestrictions", String.class);
        selectRestrictionsSup = context.getBean("selectRestrictionsSup", String.class);
        selectRestrictionsSupEq = context.getBean("selectRestrictionsSupEq", String.class);

        context.close();

        LOG.info(IConfiguration.CONFIG, "Inferred ontology loaded, {} default queries.", defaultQueries.size());
    }

    /**
     * <p>Getter for the field <code>defaultQueries</code>.</p>
     *
     * @return a {@link java.util.List} object.
     */
    public List<String> getDefaultQueries() {
        return defaultQueries;
    }

    /**
     * <p>Getter for the field <code>selectSubPropertyOfQuery</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSelectSubPropertyOfQuery() {
        return selectSubPropertyOfQuery;
    }

    /**
     * <p>Getter for the field <code>selectSubClassOfQuery</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSelectSubClassOfQuery() {
        return selectSubClassOfQuery;
    }

    /**
     * <p>Getter for the field <code>selectInverseOf</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSelectInverseOf() {
        return selectInverseOf;
    }

    /**
     * <p>Getter for the field <code>selectRange</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSelectRange() {
        return selectRange;
    }

    /**
     * <p>Getter for the field <code>selectDomain</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSelectDomain() {
        return selectDomain;
    }

    /**
     * <p>Getter for the field <code>selectRestrictions</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSelectRestrictions() {
        return selectRestrictions;
    }

    /**
     * <p>Getter for the field <code>selectRestrictionsSup</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSelectRestrictionsSup() {
        return selectRestrictionsSup;
    }

    /**
     * <p>Getter for the field <code>selectRestrictionsSupEq</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getSelectRestrictionsSupEq() {
        return selectRestrictionsSupEq;
    }
}
