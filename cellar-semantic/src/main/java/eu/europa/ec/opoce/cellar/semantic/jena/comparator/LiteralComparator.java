package eu.europa.ec.opoce.cellar.semantic.jena.comparator;

import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;

import java.util.Comparator;

import org.apache.jena.rdf.model.Literal;

/**
 * <p>LiteralComparator class.</p>
 *
 */
public class LiteralComparator implements Comparator<Literal> {

    /** {@inheritDoc} */
    @Override
    public int compare(Literal one, Literal other) {
        return JenaUtils.stringize(one).compareTo(JenaUtils.stringize(other));
    }
}
