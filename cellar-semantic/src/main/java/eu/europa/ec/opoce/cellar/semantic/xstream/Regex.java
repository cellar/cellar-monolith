package eu.europa.ec.opoce.cellar.semantic.xstream;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 *
 */
@XStreamAlias("regex")
public class Regex {

    @XStreamAlias("prefix")
    private String prefix;

    @XStreamAlias("pattern")
    private String pattern;

    @XStreamAlias("url")
    private String url;

    public Regex() {
    }

    public Regex(String prefix, String pattern, String url) {
        this.prefix = prefix;
        this.pattern = pattern;
        this.url = url;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
