package eu.europa.ec.opoce.cellar.semantic;

import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

/**
 * <p>OntologyUri class.</p>
 *
 */
public enum OntologyUri {
    owl(OWL.getURI()), rdf(RDF.getURI()), rdfs(RDFS.getURI());

    private final String uri;

    /**
     * <p>Constructor for OntologyUri.</p>
     *
     * @param uri a {@link java.lang.String} object.
     */
    OntologyUri(String uri) {
        this.uri = uri;
    }

    /**
     * <p>Getter for the field <code>uri</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getUri() {
        return uri;
    }
}
