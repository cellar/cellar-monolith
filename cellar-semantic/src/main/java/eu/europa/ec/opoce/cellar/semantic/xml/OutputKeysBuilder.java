package eu.europa.ec.opoce.cellar.semantic.xml;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;

/**
 * <p>OutputKeysBuilder class.</p>
 *
 */
public class OutputKeysBuilder {

    /**
     * <p>main</p>
     *
     * @param args an array of {@link java.lang.String} objects.
     */
    public static void main(String[] args) {
        System.setProperty("javax.xml.transform.TransformerFactory", "net.sf.saxon.TransformerFactoryImpl");

        XmlBuilder xmlBuilder = new XmlBuilder("demo");
        DomUtils.write(xmlBuilder.getDocument(), System.out, new OutputKeysBuilder().setStandalone(Standalone.yes).setIndent(Indent.yes));
    }

    public enum Indent {
        yes, no
    }

    public enum Standalone {
        yes, no
    }
    public enum OmitXmlDeclaration {
        yes, no

    }

    private Indent indent = Indent.no;
    private Standalone standalone;
    private OmitXmlDeclaration omitXmlDeclaration = OmitXmlDeclaration.no;
    private String docTypePublic;
    private String docTypeSystem;

    /**
     * <p>Setter for the field <code>indent</code>.</p>
     *
     * @param indent a {@link eu.europa.ec.opoce.cellar.semantic.xml.OutputKeysBuilder.Indent} object.
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.xml.OutputKeysBuilder} object.
     */
    public OutputKeysBuilder setIndent(Indent indent) {
        this.indent = indent;
        return this;
    }

    /**
     * <p>Setter for the field <code>standalone</code>.</p>
     *
     * @param standalone a {@link eu.europa.ec.opoce.cellar.semantic.xml.OutputKeysBuilder.Standalone} object.
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.xml.OutputKeysBuilder} object.
     */
    public OutputKeysBuilder setStandalone(Standalone standalone) {
        this.standalone = standalone;
        return this;
    }

    /**
     * <p>Setter for the field <code>omitXmlDeclaration</code>.</p>
     *
     * @param omitXmlDeclaration a {@link eu.europa.ec.opoce.cellar.semantic.xml.OutputKeysBuilder.OmitXmlDeclaration} object.
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.xml.OutputKeysBuilder} object.
     */
    public OutputKeysBuilder setOmitXmlDeclaration(OmitXmlDeclaration omitXmlDeclaration) {
        this.omitXmlDeclaration = omitXmlDeclaration;
        return this;
    }

    /**
     * <p>Setter for the field <code>docTypePublic</code>.</p>
     *
     * @param docTypePublic a {@link java.lang.String} object.
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.xml.OutputKeysBuilder} object.
     */
    public OutputKeysBuilder setDocTypePublic(String docTypePublic) {
        this.docTypePublic = docTypePublic;
        return this;
    }

    /**
     * <p>Setter for the field <code>docTypeSystem</code>.</p>
     *
     * @param docTypeSystem a {@link java.lang.String} object.
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.xml.OutputKeysBuilder} object.
     */
    public OutputKeysBuilder setDocTypeSystem(String docTypeSystem) {
        this.docTypeSystem = docTypeSystem;
        return this;
    }

    /**
     * <p>applyTo</p>
     *
     * @param transformer a {@link javax.xml.transform.Transformer} object.
     * @return a {@link javax.xml.transform.Transformer} object.
     */
    public Transformer applyTo(Transformer transformer) {
        transformer.setOutputProperty(OutputKeys.INDENT, indent.toString());
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, omitXmlDeclaration.toString());

        if (null != standalone) {
            transformer.setOutputProperty(OutputKeys.STANDALONE, standalone.toString());
        }
        if (null != docTypePublic) {
            transformer.setOutputProperty(OutputKeys.DOCTYPE_PUBLIC, docTypePublic);
        }
        if (null != docTypeSystem) {
            transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, docTypeSystem);
        }
        return transformer;
    }

}
