package eu.europa.ec.opoce.cellar.semantic.xstream;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.exception.CellarSemanticException;

import java.util.ArrayList;

/**
 * <p>StringList class.</p>
 */
public class StringList extends ArrayList<String> {

    private static final long serialVersionUID = 3116079355866906585L;

    /**
     * <p>createConverter</p>
     *
     * @param valueTagname a {@link java.lang.String} object.
     * @return a {@link com.thoughtworks.xstream.converters.Converter} object.
     */
    public static Converter createConverter(String valueTagname) {
        return new StringListConvertor(valueTagname);
    }

    private static class StringListConvertor implements Converter {

        private final String valueTagname;

        public StringListConvertor(String valueTagname) {
            this.valueTagname = valueTagname;
        }

        @Override
        public void marshal(Object sourceObj, HierarchicalStreamWriter writer, MarshallingContext context) {
            StringList stringList = (StringList) sourceObj;

            for (String value : stringList) {
                writer.startNode(valueTagname);
                writer.setValue(value);
                writer.endNode();
            }
        }

        @Override
        public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
            StringList stringList = new StringList();
            while (reader.hasMoreChildren()) {
                reader.moveDown();
                ExceptionBuilder.throwExc(CellarSemanticException.class, !reader.getNodeName().equals(valueTagname));
                stringList.add(reader.getValue());
                reader.moveUp();
            }
            return stringList;
        }

        @Override
        public boolean canConvert(@SuppressWarnings("rawtypes") Class type) {
            return type.equals(StringList.class);
        }
    }

}
