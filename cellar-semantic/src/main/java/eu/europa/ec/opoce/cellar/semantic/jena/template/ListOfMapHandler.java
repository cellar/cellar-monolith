package eu.europa.ec.opoce.cellar.semantic.jena.template;

import java.util.List;
import java.util.Map;

import org.apache.jena.rdf.model.RDFNode;

/**
 * <p>ListOfMapHandler interface.</p>
 * @deprecated Use JenaUtils#select(query, model)
 */
@Deprecated
public interface ListOfMapHandler<T> {

    /**
     * <p>handle</p>
     *
     * @param resultSet a {@link java.util.List} object.
     * @param <T> a T object.
     * @return a T object.
     */
    public T handle(List<Map<String, RDFNode>> resultSet);

}
