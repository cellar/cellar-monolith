/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.semantic.ontology
 *             FILE : OntologyCacheService.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 09 04, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-09-04 14:04:54 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.semantic.ontology;

import org.apache.jena.ontology.OntModel;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author ARHS Developments
 */
public interface InferenceModelCacheService {

    /**
     * Return a new instance of the {@link OntModel}.
     *
     * @return the inferred model
     */
    OntModel getInferenceModel();

    boolean setInferenceModelFile(MultipartFile file);

    InferenceModelDescription getDescription();

}
