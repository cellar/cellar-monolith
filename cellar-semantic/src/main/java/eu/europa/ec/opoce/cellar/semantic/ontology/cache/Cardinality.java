package eu.europa.ec.opoce.cellar.semantic.ontology.cache;

/**
 * <p>Cardinality class.</p>
 *
 */
public class Cardinality {

    private final String property;
    private final String clazz;
    private final int cardinality;
    private final String valueClazz;

    /**
     * <p>Constructor for Cardinality.</p>
     *
     * @param property a {@link java.lang.String} object.
     * @param clazz a {@link java.lang.String} object.
     * @param cardinality a int.
     * @param valueClazz a {@link java.lang.String} object.
     */
    public Cardinality(String property, String clazz, int cardinality, String valueClazz) {
        this.property = property;
        this.clazz = clazz;
        this.cardinality = cardinality;
        this.valueClazz = valueClazz;
    }

    /**
     * <p>Getter for the field <code>property</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getProperty() {
        return property;
    }

    /**
     * <p>Getter for the field <code>clazz</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getClazz() {
        return clazz;
    }

    /**
     * <p>Getter for the field <code>cardinality</code>.</p>
     *
     * @return a int.
     */
    public int getCardinality() {
        return cardinality;
    }

    /**
     * <p>Getter for the field <code>valueClazz</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getValueClazz() {
        return valueClazz;
    }
}
