package eu.europa.ec.opoce.cellar.semantic.jena.template;

import org.apache.jena.rdf.model.Model;
import eu.europa.ec.opoce.cellar.common.closure.RPClosure;

/**
 * <p>Abstract ModelCallbacks class.</p>
 */
public abstract class ModelCallbacks {

    /**
     * Constant <code>modelAsResultCallback</code>
     */
    public static final RPClosure<Model, Model> modelAsResultCallback = model -> model;

}
