/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.semantic.validation
 *             FILE : NalValidationUtils.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 04 06, 2018
 *
 *
 *      MODIFIED BY : EUROPEAN DYNAMICS S.A.
 *               ON : 29-07-2022
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2022 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.semantic.validation;

import eu.europa.ec.opoce.cellar.common.Namespace;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.rdf.model.*;
import org.apache.jena.sparql.ARQException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Class providing validation utilities
 *
 * @author ARHS Developments
 * @author EUROPEAN DYNAMICS S.A.
 */
public final class NalValidationUtils {

    private static final Logger LOG = LogManager.getLogger(NalValidationUtils.class);

    private static final Pattern VALID_URI_CHARACTERS_PATTERN = Pattern.compile("[-~\\.!*'();:@&=+$,\\/?#\\[\\]%\\w]*");


    private static final String opCodeUri = "http://publications.europa.eu/resource/authority/op-code";

    private static final String dcIdentifier = "http://purl.org/dc/elements/1.1/identifier";

    private static final String opMappedCodeUri = "http://publications.europa.eu/ontology/authority/op-mapped-code";

    private static final String sourceUri = "http://purl.org/dc/elements/1.1/source";

    private static final String legacyCodeUri = "http://publications.europa.eu/ontology/authority/legacy-code";

    private static final String skosConceptsSparqlQuery = "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>" +
            "PREFIX skos: <http://www.w3.org/2004/02/skos/core#>" +
            "CONSTRUCT { ?s ?p ?o. } " +
            "WHERE {  " +
            " ?s rdf:type  skos:Concept." +
            " ?s ?p ?o . " +
            "}";


    private NalValidationUtils() {

    }

    /**
     * Validate skos concepts of file-type/language NAL.
     * Process consists of iterating through subjects and
     * trying to retrieve skos concepts without any exception.
     *
     * @param nalModel the NAL model
     * @return true if no exception is thrown, false otherwise
     */
    public static boolean validateSkosConcepts(final Model nalModel) {
    	final ResIterator subjectsIterator = filterOutSkosConceptsFromNalModel(nalModel).listSubjects();
        try {
            //iterate subjects
            final List<Resource> subjectsList = subjectsIterator.toList();
            for (final Resource subject : subjectsList) {
                final String subjectURI = subject.getURI();
                //iterate only through languages or file-types
                if (subject.isURIResource() && (subjectURI.startsWith(Namespace.language_ontology) || subjectURI.startsWith(Namespace.fileType_ontology))) {
                    //try getting dc:identifier and op-code
                    final String identifier = subject.getProperty(dcIdentifierCreateProperty()).getString();
                    subject.getProperty(opCodeCreateProperty()).getString();
                    //special treatment for language
                    if(subjectURI.startsWith(Namespace.language_ontology) && !isValidCharCode(identifier)){
                        LOG.error("Invalid char code provided '{}', length must be 3.", identifier);
                        return false;
                    }
                    //validate subject statements for the description of this language or file-type
                    if (!isValidSubjectStatements(subject)){
                        LOG.error("Missing/Invalid statements in subject '{}'", subject);
                        return false;
                    }
                }
            }
        }
        catch (Exception e) {
            LOG.error("Missing/Invalid skos concepts" , e);
            return false;
        } finally {
        	subjectsIterator.close();
        }
        return true;
    }


    /**
     * Filters out skos concepts from NAL model
     *
     * @param model the model
     * @return the filtered model
     *
     * @throws ARQException failed to execute sparql query
     */
    public static Model filterOutSkosConceptsFromNalModel(final Model model) {
        Model skosConceptModel = ModelFactory.createDefaultModel();

        Query query = QueryFactory.create(skosConceptsSparqlQuery);
        try(QueryExecution qexec = QueryExecutionFactory.create(query, model)) {
            return skosConceptModel.add(qexec.execConstruct());
        } catch (Exception e) {
            LOG.error("Failed to filter out skos concepts from the Nal model.", e);
            throw new ARQException();
        }
    }

    /**
     * Is valid statements of given subject. Confirm by their existence
     *
     * @param subject the subject
     * @return true if no exception is thrown, false otherwise
     */
    public static boolean isValidSubjectStatements(final Resource subject) {
        for (final StmtIterator opMappedCodeNodes = subject.listProperties(opMappedCodeCreateProperty()); opMappedCodeNodes.hasNext();) {
            final Statement opMappedCodeNode = opMappedCodeNodes.next();
            //try getting triples
            try {
                opMappedCodeNode.getSubject().asResource();
                opMappedCodeNode.getPredicate().asResource();
                opMappedCodeNode.getObject().asResource();
            } catch (Exception e) {
                return false;
            }
        }
        return true;
    }

    /**
     * Is valid char code
     *
     * @param identifier the identifier
     * @return true if length of char code is 3 or "op_datpro"
     */
    public static boolean isValidCharCode(final String identifier) {
        if ("op_datpro".equalsIgnoreCase(identifier)) {
            return true;
        }
        return identifier.length() == 3;
    }

    public static List<String> validateUris(Model model) {
        List<String> invalidUris = new ArrayList<>();
        StmtIterator iterator = model.listStatements();
        try {
            while (iterator.hasNext()) {
                Statement statement = iterator.nextStatement();

                Resource subject = statement.getSubject();
                if (isInvalidUri(subject)) {
                    invalidUris.add(subject.getURI());
                }

                RDFNode object = statement.getObject();
                if (isInvalidUri(object)) {
                    invalidUris.add(object.asResource().getURI());
                }
            }
        } finally {
            iterator.close();
        }

        return invalidUris;
    }

    public static boolean isInvalidUri(final RDFNode node) {
        if (!node.isURIResource()) {
            return false;
        }
        final Resource resource = (Resource) node;
        return !VALID_URI_CHARACTERS_PATTERN.matcher(resource.getURI()).matches();
    }

    /**
     * Create the opCode property
     *
     * @return the created opCode property
     */
    public static Property opCodeCreateProperty() {
        return ResourceFactory.createProperty(opCodeUri);
    }

    /**
     * Create the dcIdentifier property
     *
     * @return the created dcIdentifier property
     */
    public static Property dcIdentifierCreateProperty() {
        return ResourceFactory.createProperty(dcIdentifier);
    }

    /**
     * Create the opMappedCode property
     *
     * @return the created opMappedCode property
     */
    public static Property opMappedCodeCreateProperty() {
        return ResourceFactory.createProperty(opMappedCodeUri);
    }

    /**
     * Create the source property
     *
     * @return the created source property
     */
    public static Property sourceCreateProperty() {
        return ResourceFactory.createProperty(sourceUri);
    }

    /**
     * Create the legacyCode property
     *
     * @return the created legacyCode property
     */
    public static Property legacyCodeUriCreateProperty() {
        return ResourceFactory.createProperty(legacyCodeUri);
    }
}
