package eu.europa.ec.opoce.cellar.semantic.jena.transformers;

import org.apache.commons.collections15.Transformer;

import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;

/**
 * <p>RdfNode2Uri class.</p>
 *
 */
public class RdfNode2Uri implements Transformer<RDFNode, String> {

    /** Constant <code>instance</code> */
    public static Transformer<RDFNode, String> instance = new RdfNode2Uri();

    /** {@inheritDoc} */
    @Override
    public String transform(RDFNode input) {
        return input.isURIResource() ? ((Resource) input).getURI() : null;
    }
}
