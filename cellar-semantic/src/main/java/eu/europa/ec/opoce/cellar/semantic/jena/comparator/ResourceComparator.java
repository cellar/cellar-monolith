package eu.europa.ec.opoce.cellar.semantic.jena.comparator;

import java.util.Comparator;

import org.springframework.util.Assert;

import org.apache.jena.rdf.model.Resource;

/**
 * <p>ResourceComparator class.</p>
 *
 */
public class ResourceComparator implements Comparator<Resource> {

    /** {@inheritDoc} */
    @Override
    public int compare(Resource one, Resource other) {
        Assert.isTrue(!one.isAnon(), "Anonymous resources are not supported.");
        Assert.isTrue(!other.isAnon(), "Anonymous resources are not supported.");

        return one.getURI().compareTo(other.getURI());
    }
}
