package eu.europa.ec.opoce.cellar.semantic.json;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.MappingJsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.europa.ec.opoce.cellar.common.closure.PClosure;
import eu.europa.ec.opoce.cellar.semantic.helper.MessageFormatter;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Collection;

/**
 * <p>Abstract JacksonUtils class.</p>
 */
public abstract class JacksonUtils {

    /**
     * Constant <code>log</code>
     */
    private static final Logger log = LoggerFactory.getLogger(JacksonUtils.class);

    /**
     * <p>streamAsJson</p>
     *
     * @param outputStream    a {@link java.io.OutputStream} object.
     * @param objectsToStream a {@link java.util.Collection} object.
     * @param close           a boolean.
     */
    public static void streamAsJson(OutputStream outputStream, final Collection<?> objectsToStream, boolean close) {
        PClosure<JsonGenerator> generatorCallBack = generator -> {
            generator.writeStartArray();
            for (Object object : objectsToStream) {
                generator.writeObject(object);
            }
            generator.writeEndArray();
        };
        streamAsJson(outputStream, generatorCallBack, close);
    }

    /**
     * <p>streamAsJson</p>
     *
     * @param outputStream a {@link java.io.OutputStream} object.
     * @param object       a {@link java.lang.Object} object.
     * @param close        a boolean.
     */
    public static void streamAsJson(OutputStream outputStream, final Object object, boolean close) {
        PClosure<JsonGenerator> generatorCallBack = generator -> generator.writeObject(object);
        streamAsJson(outputStream, generatorCallBack, close);
    }

    /**
     * <p>streamAsJson</p>
     *
     * @param outputStream      a {@link java.io.OutputStream} object.
     * @param generatorCallBack a {@link PClosure} object.
     * @param close             a boolean.
     */
    public static void streamAsJson(OutputStream outputStream, PClosure<JsonGenerator> generatorCallBack, boolean close) {
        JsonFactory f = new MappingJsonFactory();
        JsonGenerator generator = null;
        try {
            generator = f.createGenerator(outputStream, JsonEncoding.UTF8);
            generatorCallBack.doCall(generator);
            generator.flush();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            if (generator != null) {
                try {
                    generator.close();
                } catch (Exception ignore) {
                    if (log != null) {
                        log.warn(MessageFormatter.format("error while closing json generator: {}", ignore.getMessage()), ignore);
                    }
                }
            }

            if (close) {
                IOUtils.closeQuietly(outputStream);
            }
        }
    }

    /**
     * <p>parseJsonStream</p>
     *
     * @param inputStream a {@link java.io.InputStream} object.
     * @param valueType   a {@link java.lang.Class} object.
     * @param <T>         a T object.
     * @return a T object.
     */
    public static <T> T parseJsonStream(InputStream inputStream, Class<T> valueType) {
        try {
            return (T) new ObjectMapper().readValue(inputStream, valueType);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
        }
    }

    /**
     * <p>parseJsonStream</p>
     *
     * @param inputStream   a {@link java.io.InputStream} object.
     * @param typeReference
     * @param <T>           a T object.
     * @return a T object.
     */
    @SuppressWarnings("unchecked")
    public static <T> T parseJsonStream(InputStream inputStream, TypeReference<T> typeReference) {
        try {
            return (T) new ObjectMapper().readValue(inputStream, typeReference);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
        }
    }
}
