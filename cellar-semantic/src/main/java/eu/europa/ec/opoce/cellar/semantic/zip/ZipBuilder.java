package eu.europa.ec.opoce.cellar.semantic.zip;

import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.exception.CellarSemanticException;
import eu.europa.ec.opoce.cellar.semantic.helper.IOHelper;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.Resource;
import org.springframework.util.Assert;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * <p>ZipBuilder class.</p>
 */
public class ZipBuilder {

    /**
     * Constant <code>INSTANCE</code>
     */
    public static final NoFilter INSTANCE = new NoFilter();

    private final File zipFile;
    private final Map<String, File> pathFileCombo = new HashMap<>();
    private final Map<String, Resource> resources = new HashMap<>();

    /**
     * <p>Constructor for ZipBuilder.</p>
     *
     * @param zipFile a {@link java.io.File} object.
     */
    public ZipBuilder(File zipFile) {
        this.zipFile = zipFile;
    }

    /**
     * <p>createZip</p>
     */
    public void createZip() {
        FileUtils.deleteQuietly(zipFile);

        FileOutputStream fileOut = null;
        ZipOutputStream zipOut = null;
        try {
            fileOut = new FileOutputStream(zipFile);
            zipOut = new ZipOutputStream(fileOut);

            for (Map.Entry<String, File> entry : pathFileCombo.entrySet()) {
                String path = entry.getKey();
                File file = entry.getValue();
                if (file.isFile()) {
                    addFileToZip(zipOut, path, file);
                } else {
                    addFolderToZip(zipOut, path);
                }
            }
            for (Map.Entry<String, Resource> entry : resources.entrySet()) {
                addResourceToZip(zipOut, entry.getKey(), entry.getValue());
            }
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        } finally {
            IOHelper.flushAndClose(zipOut);
            IOHelper.flushAndClose(fileOut);
        }
    }

    /**
     * <p>addFileToZip</p>
     *
     * @param zip  a {@link java.util.zip.ZipOutputStream} object.
     * @param path a {@link java.lang.String} object.
     * @param file a {@link java.io.File} object.
     * @throws java.io.IOException if any.
     */
    private static void addFileToZip(ZipOutputStream zip, String path, File file) throws IOException {
        FileInputStream in = null;
        try {
            in = new FileInputStream(file);
            zip.putNextEntry(new ZipEntry(path));

            byte[] bytes = new byte[(int) (4 * FileUtils.ONE_KB)];
            int len;
            while ((len = in.read(bytes)) > 0) {
                zip.write(bytes, 0, len);
            }
        } finally {
            IOUtils.closeQuietly(in);
        }
    }

    /**
     * <p>addResourceToZip</p>
     *
     * @param zip      a {@link java.util.zip.ZipOutputStream} object.
     * @param path     a {@link java.lang.String} object.
     * @param resource a {@link org.springframework.core.io.Resource} object.
     * @throws java.io.IOException if any.
     */
    private static void addResourceToZip(ZipOutputStream zip, String path, Resource resource) throws IOException {
        InputStream in = null;
        try {
            in = resource.getInputStream();
            zip.putNextEntry(new ZipEntry(path));
            IOUtils.copy(in, zip);
        } finally {
            IOUtils.closeQuietly(in);
        }

    }

    /**
     * <p>addFolderToZip</p>
     *
     * @param zipOut a {@link java.util.zip.ZipOutputStream} object.
     * @param path   a {@link java.lang.String} object.
     * @throws java.io.IOException if any.
     */
    private static void addFolderToZip(ZipOutputStream zipOut, String path) throws IOException {
        zipOut.putNextEntry(new ZipEntry(path + '/'));
    }

    /**
     * <p>add</p>
     *
     * @param files a {@link java.io.File} object.
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.zip.ZipBuilder} object.
     */
    public ZipBuilder add(File... files) {
        for (File file : files) {
            add(file, "", INSTANCE, true);
        }
        return this;
    }

    /**
     * <p>addChildren</p>
     *
     * @param directory a {@link java.io.File} object.
     * @param recurse   a boolean.
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.zip.ZipBuilder} object.
     */
    public ZipBuilder addChildren(File directory, boolean recurse) {
        addChildren(directory, recurse, INSTANCE);
        return this;
    }

    /**
     * <p>addChildren</p>
     *
     * @param file        a {@link java.io.File} object.
     * @param recurse     a boolean.
     * @param childFilter a {@link java.io.FileFilter} object.
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.zip.ZipBuilder} object.
     */
    public ZipBuilder addChildren(File file, boolean recurse, FileFilter childFilter) {
        if (!file.isDirectory()) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withMessage("Cannot call addChildren for non-directory").build();
        }
        File[] childFiles = file.listFiles(childFilter);
        for (File childFile : childFiles) {
            add(childFile, "", recurse);
        }
        return this;
    }

    /**
     * <p>add</p>
     *
     * @param resource a {@link org.springframework.core.io.Resource} object.
     * @param path     a {@link java.lang.String} object.
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.zip.ZipBuilder} object.
     */
    public ZipBuilder add(Resource resource, String path) {
        resources.put(path, resource);
        return this;
    }

    /**
     * <p>add</p>
     *
     * @param file    a {@link java.io.File} object.
     * @param recurse a boolean.
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.zip.ZipBuilder} object.
     */
    public ZipBuilder add(File file, boolean recurse) {
        add(file, "", recurse);
        return this;
    }

    /**
     * <p>add</p>
     *
     * @param file        a {@link java.io.File} object.
     * @param zipRootPath a {@link java.lang.String} object.
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.zip.ZipBuilder} object.
     */
    public ZipBuilder add(File file, String zipRootPath) {
        add(file, zipRootPath, true);
        return this;
    }

    /**
     * <p>add</p>
     *
     * @param file        a {@link java.io.File} object.
     * @param zipRootPath a {@link java.lang.String} object.
     * @param recurse     a boolean.
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.zip.ZipBuilder} object.
     */
    public ZipBuilder add(File file, String zipRootPath, boolean recurse) {
        add(file, zipRootPath, INSTANCE, recurse);
        return this;
    }

    /**
     * <p>add</p>
     *
     * @param file        a {@link java.io.File} object.
     * @param zipRootPath a {@link java.lang.String} object.
     * @param fileFilter  a {@link java.io.FileFilter} object.
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.zip.ZipBuilder} object.
     */
    public ZipBuilder add(File file, String zipRootPath, FileFilter fileFilter) {
        add(file, zipRootPath, fileFilter, true);
        return this;
    }

    /**
     * <p>add</p>
     *
     * @param file        a {@link java.io.File} object.
     * @param zipRootPath a {@link java.lang.String} object.
     * @param fileFilter  a {@link java.io.FileFilter} object.
     * @param recurse     a boolean.
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.zip.ZipBuilder} object.
     */
    public ZipBuilder add(File file, String zipRootPath, FileFilter fileFilter, boolean recurse) {
        if (file.isFile()) {
            addFile(file, zipRootPath);
        } else if (file.isDirectory()) {
            addDirectory(file, zipRootPath, fileFilter, recurse);
        } else {
            throw new IllegalArgumentException(file.toString());
        }
        return this;
    }

    /**
     * <p>addFile</p>
     *
     * @param file        a {@link java.io.File} object.
     * @param zipRootPath a {@link java.lang.String} object.
     */
    private void addFile(File file, String zipRootPath) {
        String filePath = zipRootPath.isEmpty() ? file.getName() : zipRootPath + '/' + file.getName();
        File oldFile = pathFileCombo.put(filePath, file);
        Assert.isTrue(oldFile == null || oldFile.equals(file));
    }

    /**
     * <p>addFile</p>
     *
     * @param file         a {@link java.io.File} object.
     * @param zipRootPath  a {@link java.lang.String} object.
     * @param fullFilepath a {@link java.lang.String} object.
     */
    public void addFile(File file, String zipRootPath, String fullFilepath) {
        String filePath = zipRootPath.isEmpty() ? fullFilepath : zipRootPath + '/' + fullFilepath;
        File oldFile = pathFileCombo.put(filePath, file);
        Assert.isTrue(oldFile == null || oldFile.equals(file));
    }

    /**
     * <p>addDirectory</p>
     *
     * @param directory   a {@link java.io.File} object.
     * @param zipRootPath a {@link java.lang.String} object.
     * @param fileFilter  a {@link java.io.FileFilter} object.
     * @param recurse     a boolean.
     */
    private void addDirectory(File directory, String zipRootPath, FileFilter fileFilter, boolean recurse) {
        String directoryPath = zipRootPath.isEmpty() ? directory.getName() : zipRootPath + '/' + directory.getName();
        pathFileCombo.put(directoryPath, directory);

        if (fileFilter == null) {
            return;
        }

        for (File file : directory.listFiles(fileFilter)) {
            if (recurse) {
                add(file, directoryPath, fileFilter, recurse);
            } else {
                add(file, directoryPath);
            }
        }
    }

    public static class NoFilter implements FileFilter {

        @Override
        public boolean accept(File file) {
            return true;
        }
    }
}
