package eu.europa.ec.opoce.cellar.semantic.jena.template;

import java.util.List;
import java.util.Map;

import org.apache.jena.rdf.model.RDFNode;

/**
 * <p>Abstract ListOfMapLoopHandlerWithoutResult class.</p>
 *
 */
public abstract class ListOfMapLoopHandlerWithoutResult extends ListOfMapHandlerWithoutResult {

    /** {@inheritDoc} */
    @Override
    protected void doHandle(List<Map<String, RDFNode>> resultSet) {
        for (Map<String, RDFNode> result : resultSet) {
            process(result);
        }
    }

    /**
     * <p>process</p>
     *
     * @param result a {@link java.util.Map} object.
     */
    protected abstract void process(Map<String, RDFNode> result);
}
