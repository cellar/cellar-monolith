package eu.europa.ec.opoce.cellar.semantic.jena.template;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.jena.graph.Graph;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.query.Syntax;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;

/**
 * <p>Abstract JenaQueryUtils class.</p>
 *
 */
public abstract class JenaQueryUtils {

    /**
     * <p>convertToListOfMaps</p>
     *
     * @param resultSet a {@link org.apache.jena.query.ResultSet} object.
     * @return a {@link java.util.List} object.
     */
    public static List<Map<String, RDFNode>> convertToListOfMaps(ResultSet resultSet) {
        List<Map<String, RDFNode>> result = new ArrayList<Map<String, RDFNode>>();

        while (resultSet.hasNext()) {
            QuerySolution querySolution = resultSet.next();
            result.add(convertRowToMap(querySolution));
        }

        return result;
    }

    /**
     * <p>convertRowToMap</p>
     *
     * @param querySolution a {@link org.apache.jena.query.QuerySolution} object.
     * @return a {@link java.util.Map} object.
     */
    public static Map<String, RDFNode> convertRowToMap(QuerySolution querySolution) {
        Map<String, RDFNode> result = new HashMap<String, RDFNode>();

        Iterator<?> varNames = querySolution.varNames();
        while (varNames.hasNext()) {
            String varName = (String) varNames.next();
            result.put(varName, querySolution.get(varName));
        }

        return result;
    }

    /**
     * <p>newQueryExecution</p>
     *
     * @param model a {@link org.apache.jena.rdf.model.Model} object.
     * @param sparql a {@link java.lang.String} object.
     * @return a {@link org.apache.jena.query.QueryExecution} object.
     */
    public static QueryExecution newQueryExecution(Model model, String sparql) {
        return newQueryExecution(model, sparql, null);
    }

    /**
     * <p>newQueryExecution</p>
     *
     * @param model a {@link org.apache.jena.rdf.model.Model} object.
     * @param sparql a {@link org.apache.jena.query.Query} object.
     * @return a {@link org.apache.jena.query.QueryExecution} object.
     */
    public static QueryExecution newQueryExecution(Model model, Query sparql) {
        return newQueryExecution(model, sparql, null);
    }

    /**
     * <p>newQueryExecution</p>
     *
     * @param model a {@link org.apache.jena.rdf.model.Model} object.
     * @param sparql a {@link java.lang.String} object.
     * @param querySolution a {@link org.apache.jena.query.QuerySolution} object.
     * @return a {@link org.apache.jena.query.QueryExecution} object.
     */
    public static QueryExecution newQueryExecution(Model model, String sparql, QuerySolution querySolution) {
        return newQueryExecution(model, QueryFactory.create(sparql, Syntax.syntaxARQ), querySolution);
    }

    /**
     * <p>newQueryExecution</p>
     *
     * @param model a {@link org.apache.jena.rdf.model.Model} object.
     * @param sparql a {@link org.apache.jena.query.Query} object.
     * @param querySolution a {@link org.apache.jena.query.QuerySolution} object.
     * @return a {@link org.apache.jena.query.QueryExecution} object.
     */
    public static QueryExecution newQueryExecution(Model model, Query sparql, QuerySolution querySolution) {
        return QueryExecutionFactory.create(sparql, model, querySolution);
    }

    /**
     * <p>closeQuietly</p>
     *
     * @param queryExecution a {@link org.apache.jena.query.QueryExecution} object.
     */
    public static void closeQuietly(QueryExecution queryExecution) {
        try {
            if (queryExecution != null)
                queryExecution.close();
        } catch (Exception ignore) {
        }
    }

    /**
     * <p>closeQuietly</p>
     *
     * @param model a {@link org.apache.jena.rdf.model.Model} object.
     */
    public static void closeQuietly(Model model) {
        try {
            if (model != null)
                model.close();
        } catch (Exception ignore) {
        }
    }

    /**
     * <p>closeQuietly</p>
     *
     * @param graph a {@link org.apache.jena.graph.Graph} object.
     */
    public static void closeQuietly(Graph graph) {
        try {
            if (graph != null)
                graph.close();
        } catch (Exception ignore) {
        }
    }

    /**
     * <p>closeQuietly</p>
     *
     * @param queryExecution a {@link org.apache.jena.query.QueryExecution} object.
     * @param model a {@link org.apache.jena.rdf.model.Model} object.
     */
    public static void closeQuietly(QueryExecution queryExecution, Model model) {
        closeQuietly(queryExecution);
        closeQuietly(model);
    }

    /**
     * <p>closeQuietly</p>
     *
     * @param model a {@link org.apache.jena.rdf.model.Model} object.
     * @param graph a {@link org.apache.jena.graph.Graph} object.
     */
    public static void closeQuietly(Model model, Graph graph) {
        closeQuietly(model);
        closeQuietly(graph);
    }

}
