package eu.europa.ec.opoce.cellar.semantic.xml;

import eu.europa.ec.opoce.cellar.common.util.IOUtils;
import eu.europa.ec.opoce.cellar.common.util.XMLUtils;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.exception.CellarSemanticException;
import eu.europa.ec.opoce.cellar.semantic.helper.FileHelper;
import eu.europa.ec.opoce.cellar.semantic.helper.StringHelper;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.Collection;

/**
 * <p>DomUtils class.</p>
 */
public class DomUtils {

    /**
     * Constant <code>documentBuilder</code>
     */
    private static ThreadLocal<DocumentBuilder> documentBuilder = ThreadLocal.withInitial(() -> {
        try {
            return DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (final ParserConfigurationException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    });

    /**
     * Constant <code>namespaceAwareDocumentBuilder</code>
     */
    private static ThreadLocal<DocumentBuilder> namespaceAwareDocumentBuilder = ThreadLocal.withInitial(() -> {
        try {
            final DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
            documentBuilderFactory.setNamespaceAware(true);

            return documentBuilderFactory.newDocumentBuilder();
        } catch (final ParserConfigurationException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    });

    /**
     * Constant <code>transformerFactory</code>
     */
    private static ThreadLocal<TransformerFactory> transformerFactory = ThreadLocal.withInitial(TransformerFactory::newInstance);

    /**
     * <p>Getter for the field <code>documentBuilder</code>.</p>
     *
     * @return a {@link javax.xml.parsers.DocumentBuilder} object.
     */
    public static DocumentBuilder getDocumentBuilder() {
        return documentBuilder.get();
    }

    /**
     * <p>Getter for the field <code>namespaceAwareDocumentBuilder</code>.</p>
     *
     * @return a {@link javax.xml.parsers.DocumentBuilder} object.
     */
    public static DocumentBuilder getNamespaceAwareDocumentBuilder() {
        return namespaceAwareDocumentBuilder.get();
    }

    /**
     * <p>read</p>
     *
     * @param xml a {@link java.lang.String} object.
     * @return a {@link org.w3c.dom.Document} object.
     */
    public static Document read(String xml) {
        ByteArrayInputStream input = null;
        try {
            input = new ByteArrayInputStream(xml.getBytes(StandardCharsets.UTF_8));
            return read(input);
        } finally {
            IOUtils.closeQuietly(input);
        }
    }

    /**
     * <p>read</p>
     *
     * @param input a {@link java.io.InputStream} object.
     * @return a {@link org.w3c.dom.Document} object.
     */
    public static Document read(InputStream input) {
        try {
            final InputSource source = new InputSource(input);
            final DocumentBuilder documentBuilder = getDocumentBuilder();
            return documentBuilder.parse(source);
        } catch (final SAXException | IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }

    /**
     * <p>write</p>
     *
     * @param document a {@link org.w3c.dom.Document} object.
     * @param file     a {@link java.io.File} object.
     */
    public static void write(Document document, File file) {
        final FileOutputStream outputStream = FileHelper.openOutputStream(file);
        try {
            write(document, outputStream);
        } finally {
            IOUtils.closeQuietly(outputStream);
        }
    }

    /**
     * <p>write</p>
     *
     * @param document a {@link org.w3c.dom.Document} object.
     * @param out      a {@link java.io.OutputStream} object.
     */
    public static void write(Document document, OutputStream out) {
        if (document == null) {
            throw ExceptionBuilder.get(CellarSemanticException.class)
                    .withMessage("Error writing document. Document is 'null'.").build();
        }

        write(document, out, new OutputKeysBuilder().setIndent(OutputKeysBuilder.Indent.yes));
    }

    /**
     * <p>write</p>
     *
     * @param document          a {@link org.w3c.dom.Document} object.
     * @param out               a {@link java.io.OutputStream} object.
     * @param outputKeysBuilder a {@link eu.europa.ec.opoce.cellar.semantic.xml.OutputKeysBuilder} object.
     */
    public static void write(Document document, OutputStream out, OutputKeysBuilder outputKeysBuilder) {
        write(document, new OutputStreamWriter(out, StandardCharsets.UTF_8), outputKeysBuilder);
    }

    /**
     * <p>write</p>
     *
     * @param document a {@link org.w3c.dom.Document} object.
     * @param out      a {@link java.io.Writer} object.
     */
    public static void write(Document document, Writer out) {
        final OutputKeysBuilder outputKeysBuilder = new OutputKeysBuilder().setIndent(OutputKeysBuilder.Indent.yes);
        write(document, out, outputKeysBuilder);
    }

    /**
     * <p>write</p>
     *
     * @param document          a {@link org.w3c.dom.Document} object.
     * @param out               a {@link java.io.Writer} object.
     * @param outputKeysBuilder a {@link eu.europa.ec.opoce.cellar.semantic.xml.OutputKeysBuilder} object.
     */
    public static void write(Document document, Writer out, OutputKeysBuilder outputKeysBuilder) {
        try {
            final TransformerFactory factory = transformerFactory.get();
            final Transformer transformer = factory.newTransformer();

            outputKeysBuilder.applyTo(transformer);

            final DOMSource source = new DOMSource(document);
            final StreamResult result = new StreamResult(out);
            transformer.transform(source, result);
        } catch (final TransformerException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }

    /**
     * <p>toString</p>
     *
     * @param document a {@link org.w3c.dom.Document} object.
     * @return a {@link java.lang.String} object.
     */
    public static String toString(Document document) {
        final StringWriter stringWriter = new StringWriter();
        write(document, stringWriter);
        return stringWriter.toString();
    }

    /**
     * <p>convertXmlSnippet2Root</p>
     *
     * @param string a {@link java.lang.String} object.
     * @return a {@link org.w3c.dom.Element} object.
     */
    public static Element convertXmlSnippet2Root(String string) {
        final StringBuilder newString = new StringBuilder().append(XmlHelper.xml_declaration).append(StringHelper.newline);

        newString.append("<root>").append(string).append("</root>");

        final Document document = read(newString.toString());
        return document.getDocumentElement();
    }

    /**
     * <p>getOneElement</p>
     *
     * @param parentElement a {@link org.w3c.dom.Element} object.
     * @param elementName   a {@link java.lang.String} object.
     * @return a {@link org.w3c.dom.Element} object.
     */
    public static Element getOneElement(Element parentElement, String elementName) {
        Element result = null;

        final NodeList childNodes = parentElement.getChildNodes();
        for (int i = 0; i < childNodes.getLength(); i++) {
            final Node node = childNodes.item(i);
            if (!(node instanceof Element)) {
                continue;
            }

            final Element element = (Element) node;
            if (element.getNodeName().equals(elementName)) {
                if (null != result) {
                    throw ExceptionBuilder.get(CellarSemanticException.class).withMessage("More than one element found for name '{}'")
                            .withMessageArgs(elementName).build();
                }
                result = element;
            }
        }

        return result;
    }

    /**
     * <p>xpathAsString</p>
     *
     * @param xpathExpression  a {@link java.lang.String} object.
     * @param element          a {@link org.w3c.dom.Element} object.
     * @param namespaceContext a {@link javax.xml.namespace.NamespaceContext} object.
     * @return a {@link java.lang.String} object.
     */
    public static String xpathAsString(String xpathExpression, Element element, NamespaceContext namespaceContext) {
        try {
            final XPath xpath = XMLUtils.newXPathFactory().newXPath();
            if (null != namespaceContext) {
                xpath.setNamespaceContext(namespaceContext);
            }
            return (String) xpath.compile(xpathExpression).evaluate(element, XPathConstants.STRING);
        } catch (final Exception e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }

    /**
     * <p>xpathAsNodeList</p>
     *
     * @param xpathExpression a {@link java.lang.String} object.
     * @param object          a {@link java.lang.Object} object.
     * @return a {@link org.w3c.dom.NodeList} object.
     */
    public static NodeList xpathAsNodeList(String xpathExpression, Object object) {
        try {
            final XPath xpath = XMLUtils.newXPathFactory().newXPath();
            return (NodeList) xpath.compile(xpathExpression).evaluate(object, XPathConstants.NODESET);
        } catch (final Exception e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }

    /**
     * <p>convertNodeListTo</p>
     *
     * @param nodeList            a {@link org.w3c.dom.NodeList} object.
     * @param convertedCollection a T object.
     * @param <T>                 a T object.
     * @return a T object.
     */
    public static <T extends Collection<String>> T convertNodeListTo(NodeList nodeList, T convertedCollection) {
        for (int i = 0; i < nodeList.getLength(); i++) {
            convertedCollection.add(nodeList.item(i).getNodeValue());
        }
        return convertedCollection;
    }

}
