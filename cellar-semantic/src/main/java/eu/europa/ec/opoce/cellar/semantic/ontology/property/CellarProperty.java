/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.ontology
 *             FILE : CellarProperty.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : Nov 18, 2015
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate$$
 *          VERSION : $LastChangedRevision$$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2012 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.semantic.ontology.property;

import static eu.europa.ec.opoce.cellar.common.Namespace.at;
import static eu.europa.ec.opoce.cellar.common.Namespace.cdm;
import static eu.europa.ec.opoce.cellar.common.Namespace.cmr;
import static eu.europa.ec.opoce.cellar.common.Namespace.ev;
import static eu.europa.ec.opoce.cellar.common.Namespace.owl;
import static eu.europa.ec.opoce.cellar.common.Namespace.skos;
import static eu.europa.ec.opoce.cellar.common.Namespace.skosXl;
import static eu.europa.ec.opoce.cellar.common.Namespace.tdm;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.vocabulary.DC_11;



/**
 * <p>CellarProperty interface.</p>
 *
 * @author ARHS Developments
 * @version $Revision$$
 */
public interface CellarProperty {

    interface HierarchyProperty {

        /** Constant <code>cdm_work_has_expression="cdm + work_has_expression"</code>. */
        String cdm_work_has_expression = cdm + "work_has_expression";

        /** Constant <code>cdm_work_has_expressionP</code>. */
        Property cdm_work_has_expressionP = ResourceFactory.createProperty(cdm_work_has_expression);

        /** Constant <code>cmd_expression_belongs_to_work="cdm + expression_belongs_to_work"</code>. */
        String cdm_expression_belongs_to_work = cdm + "expression_belongs_to_work";

        /** Constant <code>cmd_expression_belongs_to_workP</code>. */
        Property cdm_expression_belongs_to_workP = ResourceFactory.createProperty(cdm_expression_belongs_to_work);

        /** Constant <code>cdm_expression_manifested_by_manifestation="cdm + expression_manifested_by_manifest"{trunked}</code>. */
        String cdm_expression_manifested_by_manifestation = cdm + "expression_manifested_by_manifestation";

        /** Constant <code>cdm_expression_manifested_by_manifestationP</code>. */
        Property cdm_expression_manifested_by_manifestationP = ResourceFactory.createProperty(cdm_expression_manifested_by_manifestation);

        /** Constant <code>cdm_manifestation_manifests_expression="cdm + manifestation_manifests_expression"{trunked}</code>. */
        String cdm_manifestation_manifests_expression = cdm + "manifestation_manifests_expression";

        /** Constant <code>cdm_manifestation_manifests_expressionP</code>. */
        Property cdm_manifestation_manifests_expressionP = ResourceFactory.createProperty(cdm_manifestation_manifests_expression);

        /** Constant <code>cdm_manifestation_has_item="cdm + manifestation_has_item"</code>. */
        String cdm_manifestation_has_item = cdm + "manifestation_has_item";

        /** Constant <code>cdm_manifestation_has_itemP</code>. */
        Property cdm_manifestation_has_itemP = ResourceFactory.createProperty(cdm_manifestation_has_item);

        /** Constant <code>cdm_item_belongs_to_manifestation="cdm + item_belongs_to_manifestation"</code>. */
        String cdm_item_belongs_to_manifestation = cdm + "item_belongs_to_manifestation";

        /** Constant <code>cdm_item_belongs_to_manifestationP</code>. */
        Property cdm_item_belongs_to_manifestationP = ResourceFactory.createProperty(cdm_item_belongs_to_manifestation);

        /** Constant <code>cdm_dossier_contains_event="cdm + dossier_contains_event"</code>. */
        String cdm_dossier_contains_event = cdm + "dossier_contains_event";

        /** Constant <code>cdm_dossier_contains_eventP</code>. */
        Property cdm_dossier_contains_eventP = ResourceFactory.createProperty(cdm_dossier_contains_event);

        /** Constant <code>cdm_event_part_of_dossier="cdm + event_part_of_dossier"</code>. */
        String cdm_event_part_of_dossier = cdm + "event_part_of_dossier";

        /** Constant <code>cdm_mevent_part_of_dossierP</code>. */
        Property cdm_event_part_of_dossierP = ResourceFactory.createProperty(cdm_event_part_of_dossier);

        Set<String> cdm_hierarchy_properties_set = new HashSet<>(Arrays.asList( //
                cdm_work_has_expression, //
                cdm_expression_belongs_to_work, //
                cdm_expression_manifested_by_manifestation, //
                cdm_manifestation_manifests_expression, //
                cdm_manifestation_has_item, //
                cdm_item_belongs_to_manifestation, //
                cdm_dossier_contains_event, //
                cdm_event_part_of_dossier //
        ));
    }

    /** Constant <code>cmr_fallback="cmr + fallback"</code>. */
    String cmr_fallback = cmr + "fallback";

    /** Constant <code>cmr_fallbackP</code>. */
    Property cmr_fallbackP = ResourceFactory.createProperty(cmr_fallback);

    /** Constant <code>cmr_lang="cmr + lang"</code>. */
    String cmr_lang = cmr + "lang";

    /** Constant <code>cmr_langP</code>. */
    Property cmr_langP = ResourceFactory.createProperty(cmr_lang);

    /** Constant <code>cmr_prefLabel="cmr + prefLabel"</code>. */
    String cmr_prefLabel = cmr + "prefLabel";

    /** Constant <code>cmr_prefLabelP</code>. */
    Property cmr_prefLabelP = ResourceFactory.createProperty(cmr_prefLabel);

    /** Constant <code>cmr_facet="cmr + facet"</code>. */
    String cmr_facet = cmr + "facet";

    /** Constant <code>cmr_facetP</code>. */
    Property cmr_facetP = ResourceFactory.createProperty(cmr_facet);

    /** Constant <code>cmr_facet_D="cmr + facet_D"</code>. */
    String cmr_facet_D = cmr + "facet_D";

    /** Constant <code>cmr_facet_DP</code>. */
    Property cmr_facet_DP = ResourceFactory.createProperty(cmr_facet_D);

    /** Constant <code>cmr_facet_M="cmr + facet_M"</code>. */
    String cmr_facet_M = cmr + "facet_M";

    /** Constant <code>cmr_facet_MP</code>. */
    Property cmr_facet_MP = ResourceFactory.createProperty(cmr_facet_M);

    /** Constant <code>cmr_facet_T="cmr + facet_T"</code>. */
    String cmr_facet_T = cmr + "facet_T";

    /** Constant <code>cmr_facet_TP</code>. */
    Property cmr_facet_TP = ResourceFactory.createProperty(cmr_facet_T);

    /** Constant <code>cmr_level="cmr + level"</code>. */
    String cmr_level = cmr + "level";

    /** Constant <code>cmr_levelP</code>. */
    Property cmr_levelP = ResourceFactory.createProperty(cmr_level);

    /** Constant <code>cmr_lastmodificationdate="cmr + lastModificationDate"</code>. */
    String cmr_lastmodificationdate = cmr + "lastModificationDate";

    /** Constant <code>cmr_lastmodificationdateP</code>. */
    Property cmr_lastmodificationdateP = ResourceFactory.createProperty(cmr_lastmodificationdate);

    /** Constant <code>cmr_creationdate="cmr + creationDate"</code>. */
    String cmr_creationdate = cmr + "creationDate";

    /** Constant <code>cmr_creationdateP</code>. */
    Property cmr_creationdateP = ResourceFactory.createProperty(cmr_creationdate);

    /** Constant <code>cmr_metsStructSubDiv="cmr + metsStructSubDiv"</code>. */
    String cmr_metsStructSubDiv = cmr + "metsStructSubDiv";

    /** Constant <code>cmr_metsStructSubDivP</code>. */
    Property cmr_metsStructSubDivP = ResourceFactory.createProperty(cmr_metsStructSubDiv);

    /** Constant <code>cmr_manifestationMimeType="cmr + manifestationMimeType"</code>. */
    String cmr_manifestationMimeType = cmr + "manifestationMimeType";

    /** Constant <code>cmr_manifestationMimeTypeP</code>. */
    Property cmr_manifestationMimeTypeP = ResourceFactory.createProperty(cmr_manifestationMimeType);

    /**
     * Constant <code>dc_identifier="DC_11.getURI() + identifier"</code>
     */
    String dc_identifier = DC_11.getURI() + "identifier";

    /** Constant <code>dc_identifierP</code>. */
    Property dc_identifierP = ResourceFactory.createProperty(dc_identifier);

    /** The Constant cdm_work_embargo. */
    String cdm_work_embargo = cdm + "work_embargo";

    /** The Constant cdm_work_embargoP. */
    Property cdm_work_embargoP = ResourceFactory.createProperty(cdm_work_embargo);

    /** The Constant cdm_expression_embargo. */
    String cdm_expression_embargo = cdm + "expression_embargo";

    /** The Constant cdm_expression_embargoP. */
    Property cdm_expression_embargoP = ResourceFactory.createProperty(cdm_expression_embargo);

    /** The Constant cdm_manifestation_embargo. */
    String cdm_manifestation_embargo = cdm + "manifestation_embargo";

    /** The Constant cdm_manifestation_embargoP. */
    Property cdm_manifestation_embargoP = ResourceFactory.createProperty(cdm_manifestation_embargo);

    /** The Constant cdm_item_embargo. */
    String cdm_item_embargo = cdm + "item_embargo";

    /** The Constant cdm_item_embargoP. */
    Property cdm_item_embargoP = ResourceFactory.createProperty(cdm_item_embargo);

    /** The Constant cdm_dossier_embargo. */
    String cdm_dossier_embargo = cdm + "dossier_embargo";

    /** The Constant cdm_dossier_embargoP. */
    Property cdm_dossier_embargoP = ResourceFactory.createProperty(cdm_dossier_embargo);

    /** The Constant cdm_event_embargo. */
    String cdm_event_embargo = cdm + "event_embargo";

    /** The Constant cdm_event_embargoP. */
    Property cdm_event_embargoP = ResourceFactory.createProperty(cdm_event_embargo);

    /** The Constant cdm_agent_embargo. */
    String cdm_agent_embargo = cdm + "agent_embargo";

    /** The Constant cdm_agent_embargoP. */
    Property cdm_agent_embargoP = ResourceFactory.createProperty(cdm_agent_embargo);

    /** Constant <code>cdm_objectCcrDataStream="cdm + object_ccr_data-stream"</code>. */
    String cdm_objectCcrDataStream = cdm + "object_ccr_data-stream";

    /** Constant <code>cdm_objectCcrDataStreamP</code>. */
    Property cdm_objectCcrDataStreamP = ResourceFactory.createProperty(cdm_objectCcrDataStream);
    /**
     * Constant <code>cdm_nalVersionPropertyUrl="http://publications.europa.eu/ontology/authority/table.version.number"</code>.
     */
    String cdm_nalVersionPropertyUrl = "http://publications.europa.eu/ontology/authority/table.version.number";

    /** Constant <code>cdm_nalVersionPropertyUrlP</code>. */
    Property cdm_nalVersionPropertyUrlP = ResourceFactory.createProperty(cdm_nalVersionPropertyUrl);

    /** Constant <code>cdm_objectCcrIdData="cdm + object_ccr_id_object_ccr"</code>. */
    String cdm_objectCcrIdData = cdm + "object_ccr_id_object_ccr";

    /** Constant <code>cdm_objectCcrIdDataP</code>. */
    Property cdm_objectCcrIdDataP = ResourceFactory.createProperty(cdm_objectCcrIdData);

    /** Constant <code>cdm_resourceLegalRepealedByResourceLegal="cdm + resource_legal_repealed_by_resour"{trunked}</code>. */
    String cdm_resourceLegalRepealedByResourceLegal = cdm + "resource_legal_repealed_by_resource_legal";

    /** Constant <code>cdm_resourceLegalRepealedByResourceLegalP</code>. */
    Property cdm_resourceLegalRepealedByResourceLegalP = ResourceFactory.createProperty(cdm_resourceLegalRepealedByResourceLegal);

    /** Constant <code>cdm_type="cdm + type"</code>. */
    String cdm_type = cdm + "type";

    /** Constant <code>cdm_typeP</code>. */
    Property cdm_typeP = ResourceFactory.createProperty(cdm_type);

    /** Constant <code>cdm_work_has_expression="cdm + work_has_expression"</code>. */
    String cdm_work_has_expression = cdm + "work_has_expression";

    /** Constant <code>cdm_work_has_expressionP</code>. */
    Property cdm_work_has_expressionP = ResourceFactory.createProperty(cdm_work_has_expression);

    /** The Constant cdm_manifestation_part_of_manifestation. */
    String cdm_manifestation_part_of_manifestation = cdm + "manifestation_part_of_manifestation";

    /** The Constant cdm_manifestation_part_of_manifestationP. */
    Property cdm_manifestation_part_of_manifestationP = ResourceFactory.createProperty(cdm_manifestation_part_of_manifestation);
    /** Constant <code>cdm_item_identifier="cdm + item_identifier"</code>. */
    String cdm_item_identifier = cdm + "item_identifier";

    /** Constant <code>cdm_item_identifierP</code>. */
    Property cdm_item_identifierP = ResourceFactory.createProperty(cdm_item_identifier);

    /** Constant <code>cdm_item_identifier="cdm + item_identifier"</code>. */
    String cdm_item = cdm + "item";

    /** Constant <code>cdm_item_identifierP</code>. */
    Property cdm_itemP = ResourceFactory.createProperty(cdm_item);

    /** Constant <code>cdm_manifestation_type="cdm + manifestation_type"</code>. */
    String cdm_manifestation_type = cdm + "manifestation_type";

    /** Constant <code>cdm_manifestation_typeP</code>. */
    Property cdm_manifestation_typeP = ResourceFactory.createProperty(cdm_manifestation_type);

    /** Constant <code>cdm_memberList="cdm + memberList"</code>. */
    String cdm_memberList = cdm + "memberList";

    /** Constant <code>cdm_memberListP</code>. */
    Property cdm_memberListP = ResourceFactory.createProperty(cdm_memberList);

    /** Constant <code>cdm_nestedList="cdm + nestedList"</code>. */
    String cdm_nestedList = cdm + "nestedList";

    /** Constant <code>cdm_nestedListP</code>. */
    Property cdm_nestedListP = ResourceFactory.createProperty(cdm_nestedList);

    /** Constant <code>cdm_listedProperty="cdm + listedProperty"</code>. */
    String cdm_listedProperty = cdm + "listedProperty";

    /** Constant <code>cdm_listedPropertyP</code>. */
    Property cdm_listedPropertyP = ResourceFactory.createProperty(cdm_listedProperty);

    /** The Constant cdm_workIdDocument. */
    String cdm_workIdDocument = cdm + "work_id_document";

    /** The Constant cdm_workIdDocumentP. */
    Property cdm_workIdDocumentP = ResourceFactory.createProperty(cdm_workIdDocument);

    /** Constant <code>ontology_expression_uses_language="cdm + expression_uses_language"</code>. */
    String ontology_expression_uses_language = cdm + "expression_uses_language";

    /** Constant <code>ontology_expression_uses_languageP</code>. */
    Property ontology_expression_uses_languageP = ResourceFactory.createProperty(ontology_expression_uses_language);

    /** Constant <code>tdm_stream_page_physical_first="tdm + stream_page_physical_first"</code>. */
    String tdm_stream_page_physical_first = tdm + "stream_page_physical_first";

    String tdm_stream=tdm+"stream";
    Property tdm_streamP= ResourceFactory.createProperty(tdm_stream);

    /** Constant <code>tdm_stream_page_physical_firstP</code>. */
    Property tdm_stream_page_physical_firstP = ResourceFactory.createProperty(tdm_stream_page_physical_first);

    /** Constant <code>tdm_stream_page_physical_last="tdm + stream_page_physical_last"</code>. */
    String tdm_stream_page_physical_last = tdm + "stream_page_physical_last";

    /** Constant <code>tdm_stream_page_physical_lastP</code>. */
    Property tdm_stream_page_physical_lastP = ResourceFactory.createProperty(tdm_stream_page_physical_last);

    /** Constant <code>tdm_stream_order="tdm + stream_order"</code>. */
    String tdm_stream_order = tdm + "stream_order";

    /** Constant <code>tdm_stream_orderP</code>. */
    Property tdm_stream_orderP = ResourceFactory.createProperty(tdm_stream_order);

    /** The Constant tdm_stream_name. */
    String tdm_stream_name = tdm + "stream_name";

    /** The Constant tdm_stream_nameP. */
    Property tdm_stream_nameP = ResourceFactory.createProperty(tdm_stream_name);

    /** The Constant tdm_stream_label. */
    String tdm_stream_label = tdm + "stream_label";

    /** The Constant tdm_stream_labelP. */
    Property tdm_stream_labelP = ResourceFactory.createProperty(tdm_stream_label);

    /** Constant <code>at_code="at + op-code"</code>. */
    String at_code = at + "op-code";

    /** Constant <code>at_codeP</code>. */
    Property at_codeP = ResourceFactory.createProperty(at_code);

    /** Constant <code>at_mappedCode="at + op-mapped-code"</code>. */
    String at_mappedCode = at + "op-mapped-code";

    /** Constant <code>at_mappedCodeP</code>. */
    Property at_mappedCodeP = ResourceFactory.createProperty(at_mappedCode);

    /** Constant <code>at_iso2code="at + iso-639-1"</code>. */
    String at_iso2code = at + "iso-639-1";

    /** Constant <code>at_iso2codeP</code>. */
    Property at_iso2codeP = ResourceFactory.createProperty(at_iso2code);

    /** Constant <code>at_iso3code="at + iso-639-3"</code>. */
    String at_iso3code = at + "iso-639-3";

    /** Constant <code>at_iso3codeP</code>. */
    Property at_iso3codeP = ResourceFactory.createProperty(at_iso3code);

    /** Constant <code>eu_domain="ev + domain"</code>. */
    String eu_domain = ev + "domain";

    /** Constant <code>eu_domainP</code>. */
    Property eu_domainP = ResourceFactory.createProperty(eu_domain);

    /** Constant <code>eu_inDomain="ev + inDomain"</code>. */
    String eu_inDomain = ev + "inDomain";

    /** Constant <code>eu_inDomainP</code>. */
    Property eu_inDomainP = ResourceFactory.createProperty(eu_inDomain);

    /** Constant <code>eu_supportedLanguage="ev + supportedLanguage"</code>. */
    String eu_supportedLanguage = ev + "supportedLanguage";

    /** Constant <code>eu_supportedLanguageP</code>. */
    Property eu_supportedLanguageP = ResourceFactory.createProperty(eu_supportedLanguage);

    /** Constant <code>eu_language="ev + language"</code>. */
    String eu_language = ev + "language";

    /** Constant <code>skos_broader="skos + broader"</code>. */
    String skos_broader = skos + "broader";

    /** Constant <code>skos_broaderP</code>. */
    Property skos_broaderP = ResourceFactory.createProperty(skos_broader);

    /** Constant <code>skos_broaderTransitive="skos + broaderTransitive"</code>. */
    String skos_broaderTransitive = skos + "broaderTransitive";

    /** Constant <code>skos_broaderTransitiveP</code>. */
    Property skos_broaderTransitiveP = ResourceFactory.createProperty(skos_broaderTransitive);

    /** Constant <code>skos_narrower="skos + narrower"</code>. */
    String skos_narrower = skos + "narrower";

    /** Constant <code>skos_narrowerP</code>. */
    Property skos_narrowerP = ResourceFactory.createProperty(skos_narrower);

    /** Constant <code>skos_narrowerTransitive="skos + narrowerTransitive"</code>. */
    String skos_narrowerTransitive = skos + "narrowerTransitive";

    /** Constant <code>skos_narrowerTransitiveP</code>. */
    Property skos_narrowerTransitiveP = ResourceFactory.createProperty(skos_narrowerTransitive);

    /** Constant <code>skos_inScheme="skos + inScheme"</code>. */
    String skos_inScheme = skos + "inScheme";

    /** Constant <code>skos_inSchemeP</code>. */
    Property skos_inSchemeP = ResourceFactory.createProperty(skos_inScheme);

    /** Constant <code>skos_topConceptOf="skos + topConceptOf"</code>. */
    String skos_topConceptOf = skos + "topConceptOf";

    /** Constant <code>skos_topConceptOfP</code>. */
    Property skos_topConceptOfP = ResourceFactory.createProperty(skos_topConceptOf);

    /** Constant <code>skos_hasTopConcept="skos + hasTopConcept"</code>. */
    String skos_hasTopConcept = skos + "hasTopConcept";

    /** Constant <code>skos_hasTopConceptP</code>. */
    Property skos_hasTopConceptP = ResourceFactory.createProperty(skos_hasTopConcept);

    /** Constant <code>skos_prefLabel="skos + prefLabel"</code>. */
    String skos_prefLabel = skos + "prefLabel";

    /** Constant <code>skos_prefLabelP</code>. */
    Property skos_prefLabelP = ResourceFactory.createProperty(skos_prefLabel);

    /** Constant <code>skos_altLabel="skos + altLabel"</code>. */
    String skos_altLabel = skos + "altLabel";

    /** Constant <code>skos_altLabelP</code>. */
    Property skos_altLabelP = ResourceFactory.createProperty(skos_altLabel);

    /** Constant <code>skos_hiddenLabel="skos + hiddenLabel"</code>. */
    String skos_hiddenLabel = skos + "hiddenLabel";

    /** Constant <code>skos_hiddenLabelP</code>. */
    Property skos_hiddenLabelP = ResourceFactory.createProperty(skos_hiddenLabel);

    /** Constant <code>skos_notation="skos + notation"</code>. */
    String skos_notation = skos + "notation";

    /** Constant <code>skos_notationP</code>. */
    Property skos_notationP = ResourceFactory.createProperty(skos_notation);

    /** Constant <code>skosXl_prefLabel="skosXl + prefLabel"</code>. */
    String skosXl_prefLabel = skosXl + "prefLabel";

    /** Constant <code>skosXl_prefLabelP</code>. */
    Property skosXl_prefLabelP = ResourceFactory.createProperty(skosXl_prefLabel);

    /** Constant <code>skosXl_altLabel="skosXl + altLabel"</code>. */
    String skosXl_altLabel = skosXl + "altLabel";

    /** Constant <code>skosXl_altLabelP</code>. */
    Property skosXl_altLabelP = ResourceFactory.createProperty(skosXl_altLabel);

    /** Constant <code>skosXl_hiddenLabel="skosXl + hiddenLabel"</code>. */
    String skosXl_hiddenLabel = skosXl + "hiddenLabel";

    /** Constant <code>skosXl_hiddenLabelP</code>. */
    Property skosXl_hiddenLabelP = ResourceFactory.createProperty(skosXl_hiddenLabel);

    /** Constant <code>skosXl_literalForm="skosXl + literalForm"</code>. */
    String skosXl_literalForm = skosXl + "literalForm";

    /** Constant <code>skosXl_literalFormP</code>. */
    Property skosXl_literalFormP = ResourceFactory.createProperty(skosXl_literalForm);

    /** Constant <code>owl_annotatedSource="owl + annotatedSource"</code>. */
    String owl_annotatedSource = owl + "annotatedSource";

    /** Constant <code>owl_annotatedSourceP</code>. */
    Property owl_annotatedSourceP = ResourceFactory.createProperty(owl_annotatedSource);

    /** Constant <code>owl_annotatedProperty="owl + annotatedProperty"</code>. */
    String owl_annotatedProperty = owl + "annotatedProperty";

    /** Constant <code>owl_annotatedPropertyP</code>. */
    Property owl_annotatedPropertyP = ResourceFactory.createProperty(owl_annotatedProperty);

    /** Constant <code>owl_annotatedTarget="owl + annotatedTarget"</code>. */
    String owl_annotatedTarget = owl + "annotatedTarget";

    /** Constant <code>owl_annotatedTargetP</code>. */
    Property owl_annotatedTargetP = ResourceFactory.createProperty(owl_annotatedTarget);

    /** Constant <code>owl_propertyChainAxiom="owl + propertyChainAxiom"</code>. */
    String owl_propertyChainAxiom = owl + "propertyChainAxiom";

    /** Constant for unknown version of NAL/Ontology>. */
    String cdm_NALOntoVersionUnknown = "[UNKNOWN]";

    /** Constant <code>cdm_do_not_index="cdm + do_not_index"</code>. */
    String cdm_do_not_index = cdm + "do_not_index";

    /** Constant <code>cdm_do_not_indexP</code>. */
    Property cdm_do_not_indexP = ResourceFactory.createProperty(cdm_do_not_index);
}
