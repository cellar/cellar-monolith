package eu.europa.ec.opoce.cellar.semantic.jena;

import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.exception.CellarSemanticException;
import eu.europa.ec.opoce.cellar.semantic.helper.FileHelper;
import eu.europa.ec.opoce.cellar.semantic.helper.IOHelper;
import eu.europa.ec.opoce.cellar.semantic.helper.StringHelper;
import org.apache.commons.io.IOUtils;
import org.apache.jena.graph.Graph;
import org.apache.jena.query.*;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.sparql.ARQConstants;
import org.apache.logging.log4j.LogManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static eu.europa.ec.opoce.cellar.AppConstants.NEWLINE;

/**
 * <p>JenaUtils class.</p>
 */
public class JenaUtils {

    /**
     * Constant <code>log</code>
     */
    private static final Logger log = LoggerFactory.getLogger(JenaUtils.class);

    /**
     * For literals:
     * xsd:string must preserve any whitespace.
     * xsd:normalizedString must convert any whitespace (tab 0x09, line feed 0x0A and CR 0x0D) into a blank (0x20).
     * All others are collapsed (i.e. all whitespace is set to 0x20, then it is collapsed and trimmed left and right.
     *
     * @param model a {@link org.apache.jena.rdf.model.Model} object.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    public static Model normalize(final Model model) {
        final List<Statement> toRemove = new ArrayList<>();
        final List<Statement> toAdd = new ArrayList<>();

        final StmtIterator statements = model.listStatements();
        try {
            while (statements.hasNext()) {
                final Statement statement = statements.next();
                if (!statement.getObject().isLiteral()) {
                    continue;
                }

                final Literal literal = statement.getObject().asLiteral();
                final String literalDatatypeUri = literal.getDatatypeURI();
                if (!literalDatatypeUri.startsWith(ARQConstants.XML_SCHEMA_NS)
                        || literalDatatypeUri.equals(ARQConstants.XML_SCHEMA_NS + "string")) {
                    continue;
                }

                final Literal normalizedLiteral = model.createTypedLiteral(getNormalizedLiteralValue(literal), literal.getDatatype());
                final Statement normalizedStatement = ResourceFactory.createStatement(statement.getSubject(), statement.getPredicate(),
                        normalizedLiteral);
                toAdd.add(normalizedStatement);
                toRemove.add(statement);
            }
        } finally {
            statements.close();
        }

        model.remove(toRemove);
        model.add(toAdd);
        return model;
    }

    /**
     * <p>getNormalizedLiteralValue</p>
     *
     * @param literal a {@link org.apache.jena.rdf.model.Literal} object.
     * @return a {@link java.lang.String} object.
     */
    private static String getNormalizedLiteralValue(final Literal literal) {
        if (literal.getDatatypeURI().equals(ARQConstants.XML_SCHEMA_NS + "normalizedString")) {
            return literal.getString().replace((char) 0x09, ' ').replace((char) 0x0A, ' ').replace((char) 0x0D, ' ');
        } else {
            return trimAndCollapse(literal.getString());
        }
    }

    /**
     * <p>trimAndCollapse</p>
     *
     * @param result a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    private static String trimAndCollapse(String result) {
        try {
            for (final Method method : getMethods()) {
                result = method.invoke(null, result).toString();
            }
        } catch (final IllegalAccessException | InvocationTargetException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
        return result;
    }

    /**
     * <p>getMethods</p>
     *
     * @return an array of {@link java.lang.reflect.Method} objects.
     */
    private static synchronized Method[] getMethods() {
        try {
            final Class<?> clazz = Class.forName("javax.xml.bind.WhiteSpaceProcessor");
            final Method trim = clazz.getMethod("trim", CharSequence.class);
            trim.setAccessible(true);

            final Method collapse = clazz.getMethod("collapse", CharSequence.class);
            collapse.setAccessible(true);

            return new Method[]{trim, collapse};
        } catch (final ClassNotFoundException | NoSuchMethodException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }

    /**
     * <p>create</p>
     *
     * @param models a {@link org.apache.jena.rdf.model.Model} object.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    public static Model create(final Model... models) {
        final Model result = ModelFactory.createDefaultModel();
        for (final Model model : models) {
            if (model != null) {
                result.add(model);
            }
        }
        return result;
    }

    /**
     * <p>create</p>
     *
     * @param models a {@link java.util.Collection} object.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    public static Model create(final Collection<Model> models) {
        final Model result = ModelFactory.createDefaultModel();
        for (final Model model : models) {
            if (model != null) {
                result.add(model);
            }
        }
        return result;
    }

    /**
     * <p>read</p>
     *
     * @param content a {@link java.lang.String} object.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    public static Model read(final String content) {
        final Model model = ModelFactory.createDefaultModel();
        if (content != null) {
            model.read(new ByteArrayInputStream(getBytesFromStringEfficiently(content)), "");
        }
        return model;
    }

    public static Model read(final String content, final Lang lang) {
        final Model model = ModelFactory.createDefaultModel();
        model.read(new ByteArrayInputStream(getBytesFromStringEfficiently(content)), "", lang.getName());
        return model;
    }

    public static Model read(final InputStream content, final Lang lang) {
        final Model model = ModelFactory.createDefaultModel();
        model.read(content, "", lang.getName());
        return model;
    }

    public static byte[] bytes(Model model) {
        return bytes(model, RDFFormat.RDFXML_PLAIN);
    }

    public static byte[] bytes(Model model, RDFFormat rdfFormat) {
        return getBytesFromStringEfficiently(toString(model, rdfFormat));
    }

    /**
     * Converts a given string into a byte array by splitting it into smaller chunks and encoding
     * each chunk as UTF-8 bytes before concatenating them together. This method is valuable for
     * breaking down larger strings into manageable chunks, preventing the consumption of excessive
     * memory all at once, and avoiding the depletion of system resources.
     *
     * <p>If the input string's length is less than or equal to 10 megabytes (10 MB), it is directly
     * converted to bytes. Otherwise, the string is divided into chunks to optimize memory usage.</p>
     *
     * @param s The string to be converted into bytes.
     * @return A byte array containing the UTF-8 encoded representation of the input string
     * split into smaller chunks.
     */
    private static byte[] getBytesFromStringEfficiently(String s) {
        int len = s.length();
        if (len <= 10000000) { // 10 MB
            return s.getBytes();
        }
        return getBytesFromLargeString(s, len);
    }

    /**
     * Converts a large input string into a byte array by splitting it into 10 smaller chunks and encoding
     * each chunk as UTF-8 bytes before concatenating them together. This method is used for breaking
     * down larger strings into manageable chunks to optimize memory usage.
     *
     * @param s   The string to be converted into bytes.
     * @param len The length of the input string.
     * @return A byte array containing the UTF-8 encoded representation of the input string
     * split into smaller chunks.
     * @throws CellarSemanticException If an IOException occurs during the byte conversion process.
     */
    private static byte[] getBytesFromLargeString(String s, int len) throws CellarSemanticException {
        int chunksLength = len / 10;

        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
            for (int i = 0; i < len; i += chunksLength) {
                outputStream.write(s.substring(i, Math.min(len, i + chunksLength)).getBytes(StandardCharsets.UTF_8));
            }
            return outputStream.toByteArray();
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class)
                    .withMessage("Failed to get bytes from resource")
                    .withCause(e)
                    .build();
        }
    }

    public static Model read(org.springframework.core.io.Resource resource, final RDFFormat format) {
        return read(Collections.singleton(resource), format);
    }

    /**
     * <p>read</p>
     *
     * @param resources a {@link java.util.Collection} object.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    public static Model read(final Collection<org.springframework.core.io.Resource> resources, final RDFFormat format) {
        final Model model = ModelFactory.createDefaultModel();
        for (org.springframework.core.io.Resource resource : resources) {
            try (InputStream inputstream = resource.getInputStream()) {
                final RDFReader reader;
                if (format.equals(RDFFormat.NT) || format.equals(RDFFormat.NTRIPLES)) {
                    reader = model.getReader("N-TRIPLE");
                } else {
                    reader = model.getReader();
                }
                reader.setErrorHandler(new SimpleErrorHandler());
                reader.read(model, inputstream, null);
            } catch (IOException | IllegalArgumentException e) {
                throw ExceptionBuilder.get(CellarSemanticException.class)
                        .withMessage("Failed to read resource")
                        .withCause(e)
                        .build();
            }
        }
        return model;
    }

    static class SimpleErrorHandler implements RDFErrorHandler {

        private static final org.apache.logging.log4j.Logger LOG = LogManager.getLogger(SimpleErrorHandler.class);

        @Override
        public void warning(Exception e) {
            LOG.warn("", e);
        }

        @Override
        public void error(Exception e) {
            throw new IllegalArgumentException(e);
        }

        @Override
        public void fatalError(Exception e) {
            throw new IllegalArgumentException(e);
        }
    }


    public static Model read(final InputStream inputStream, final String streamName, RDFFormat format) {
        final Model model = ModelFactory.createDefaultModel();
        return read(model, inputStream, streamName, format);
    }

    public static Model read(final File file, RDFFormat rdfFormat) {
        return read(Collections.singleton(new FileSystemResource(file)), rdfFormat);
    }

    public static Model read(final Model model, final InputStream inputStream, final String streamName, RDFFormat format) {
        try {
            final RDFReader reader = model.getReader();
            final InternalRdfErrorHandler errorHandler = new InternalRdfErrorHandler(streamName);
            reader.setErrorHandler(errorHandler);
            reader.read(model, inputStream, null);

            if (errorHandler.isFailure()) {
                throw ExceptionBuilder.get(CellarSemanticException.class).withMessage(errorHandler.getInfo()).build();
            }
            return model;
        } finally {
            IOUtils.closeQuietly(inputStream);
        }
    }

    /**
     * <p>write</p>
     *
     * @param model a {@link org.apache.jena.rdf.model.Model} object.
     * @param file  a {@link java.io.File} object.
     */
    public static void write(final Model model, final File file) {
        write(model, FileHelper.openOutputStream(file));
    }

    /**
     * <p>write</p>
     *
     * @param model a {@link org.apache.jena.rdf.model.Model} object.
     * @param out   a {@link java.io.OutputStream} object.
     */
    public static void write(final Model model, final OutputStream out) {
        try {
            RDFDataMgr.write(out, model, RDFFormat.RDFXML_PLAIN);
        } finally {
            IOHelper.flushAndClose(out);
        }
    }

    public static String translate(String content, RDFFormat source, RDFFormat target) {
        if (content != null) {
            final Model model = ModelFactory.createDefaultModel();
            model.read(new ByteArrayInputStream(getBytesFromStringEfficiently(content)), "", source.getLang().getName());
            final StringWriter writer = new StringWriter();
            RDFDataMgr.write(writer, model, target);
            return writer.toString();
        }
        return toString(ModelFactory.createDefaultModel(), target);
    }

    private static class InternalRdfErrorHandler implements RDFErrorHandler {

        private String info;
        private boolean failure;

        private InternalRdfErrorHandler(final String loadedFile) {
            if (loadedFile == null) {
                this.info = "Load rdf file problem.";
            } else {
                this.info = StringHelper.format("Load rdf file ({}) problem.", loadedFile);
            }
        }

        public boolean isFailure() {
            return failure;
        }

        public String getInfo() {
            return info;
        }

        @Override
        public void warning(final Exception e) {
            final String message = e.getMessage();
            if ((null != message) && message.contains("ISO-639 does not define language:")) {
                log.warn("{}: {}", info, message);
                return;
            }
            log.warn(info, e);
        }

        @Override
        public void error(final Exception e) {
            failure = true;
            log.error(info, e);
        }

        @Override
        public void fatalError(final Exception e) {
            failure = true;
            log.error(info, e);
        }
    }

    /**
     * <p>closeQuietly</p>
     *
     * @param models a {@link java.util.Collection} object.
     */
    public static void closeQuietly(final Collection<Model> models) {
        closeQuietly(models.toArray(new Model[models.size()]));
    }

    /**
     * <p>closeQuietly</p>
     *
     * @param models a {@link org.apache.jena.rdf.model.Model} object.
     */
    public static void closeQuietly(final Model... models) {
        if (models == null) {
            return;
        }

        int i = 0;
        for (final Model model : models) {
            if (model == null) {
                continue;
            }

            if (model.isClosed()) {
                log.warn("Closing an already closed model.", new RuntimeException("Position " + ++i + " (base is 1)."));
                continue;
            }

            try {
                model.close();
            } catch (final Exception ignore) {
                log.warn("Closing model failed.", ignore);
            }
        }
    }

    /**
     * <p>closeQuietly</p>
     *
     * @param graphs a {@link org.apache.jena.graph.Graph} object.
     */
    public static void closeQuietly(final Graph... graphs) {
        if (graphs == null) {
            return;
        }

        int i = 0;
        for (final Graph graph : graphs) {
            if (graph == null) {
                continue;
            }

            if (graph.isClosed()) {
                log.warn("Closing an already closed graph.", new RuntimeException("Position " + ++i + " (base is 1)."));
                continue;
            }

            try {
                graph.close();
            } catch (final Exception ignore) {
                log.warn("Closing graph failed.", ignore);
            }
        }
    }

    /**
     * <p>debug</p>
     *
     * @param model a {@link org.apache.jena.rdf.model.Model} object.
     * @param name  a {@link java.lang.String} object.
     */
    public static void debug(final Model model, final String name) {
        if (model == null) {
            log.debug("Model '{}' is 'null'.", name);
            return;
        }

        log.info("Model '{}' size is {}.", name, model.size());
        log.debug("'{}': " + NEWLINE + "{}", name, JenaUtils.toString(model, "N-TRIPLE"));
    }

    /**
     * <p>trace</p>
     *
     * @param model a {@link org.apache.jena.rdf.model.Model} object.
     * @param name  a {@link java.lang.String} object.
     */
    public static void trace(final Model model, final String name) {
        if (model == null) {
            log.trace("Model '{}' is 'null'.", name);
            return;
        }

        log.info("Model '{}' size is {}.", name, model.size());
        if (log.isTraceEnabled()) {
            log.trace("'{}': " + NEWLINE + "{}", name, JenaUtils.toString(model, "N-TRIPLE"));
        }
    }

    /**
     * <p>stringize</p>
     *
     * @param node a {@link org.apache.jena.rdf.model.RDFNode} object.
     * @return a {@link java.lang.String} object.
     */
    public static String stringize(final RDFNode node) {
        return null == node ? "" : (String) node.visitWith(StringGetterRDFVisitor.instance);
    }

    /**
     * <p>toString</p>
     *
     * @param model a {@link org.apache.jena.rdf.model.Model} object.
     * @return a {@link java.lang.String} object.
     */
    public static String toString(final Model model) {
        return toString(model, RDFFormat.RDFXML_PLAIN);
    }

    public static String toString(final Model model, RDFFormat rdfFormat) {
        final StringWriter writer = new StringWriter();
        RDFDataMgr.write(writer, model, rdfFormat);
        return writer.toString();
    }

    public static List<QuerySolution> select(String query, Model model) {
        try (QueryExecution exec = QueryExecutionFactory.create(QueryFactory.create(query), model)) {
            final ResultSet results = exec.execSelect();
            final List<QuerySolution> solutions = new ArrayList<>();
            while (results.hasNext()) {
                solutions.add(results.next());
            }
            return solutions;
        }
    }

    /**
     * <p>toString</p>
     *
     * @param model  a {@link org.apache.jena.rdf.model.Model} object.
     * @param format a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String toString(final Model model, final String format) {
        final StringWriter writer = new StringWriter();
        model.write(writer, format);
        return writer.toString();
    }

    private static class StringGetterRDFVisitor implements RDFVisitor {

        private static final StringGetterRDFVisitor instance = new StringGetterRDFVisitor();

        @Override
        public Object visitBlank(final Resource r, final AnonId id) {
            return "";
        }

        @Override
        public Object visitURI(final Resource r, final String uri) {
            return uri;
        }

        @Override
        public Object visitLiteral(final Literal literal) {
            return literal.getLexicalForm();
        }
    }
}
