package eu.europa.ec.opoce.cellar.semantic.jena;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * <p>ModelQueries class.</p>
 *
 */
public class ModelQueries {

    /**
     * <p>Constructor for ModelQueries.</p>
     */
    private ModelQueries() {
    }

    /**
     * <p>listStatements</p>
     *
     * @param model a {@link org.apache.jena.rdf.model.Model} object.
     * @param subjects a {@link java.util.Collection} object.
     * @param property a {@link java.lang.String} object.
     * @param objectUri a {@link java.lang.String} object.
     * @return a {@link java.util.Set} object.
     */
    @Nonnull
    public static Set<Statement> listStatements(@Nonnull Model model, @Nonnull Collection<String> subjects, @Nullable String property,
            @Nullable String objectUri) {
        return listStatements(model, subjects, null == property ? null : model.createProperty(property), objectUri);
    }

    /**
     * <p>listStatements</p>
     *
     * @param model a {@link org.apache.jena.rdf.model.Model} object.
     * @param subjects a {@link java.util.Collection} object.
     * @param property a {@link org.apache.jena.rdf.model.Property} object.
     * @param objectUri a {@link java.lang.String} object.
     * @return a {@link java.util.Set} object.
     */
    @Nonnull
    public static Set<Statement> listStatements(@Nonnull Model model, @Nonnull Collection<String> subjects, @Nullable Property property,
            @Nullable String objectUri) {
        HashSet<Statement> result = new HashSet<Statement>();

        Resource objectResource = null == objectUri ? null : model.createResource(objectUri);
        for (String subject : subjects) {
            result.addAll(model.listStatements(model.createResource(subject), property, objectResource).toList());
        }
        return result;
    }

}
