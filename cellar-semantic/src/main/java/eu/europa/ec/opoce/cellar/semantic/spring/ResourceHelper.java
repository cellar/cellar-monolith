package eu.europa.ec.opoce.cellar.semantic.spring;

import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.exception.CellarSemanticException;
import eu.europa.ec.opoce.cellar.semantic.helper.FileHelper;
import org.apache.commons.io.IOUtils;
import org.springframework.core.io.Resource;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * <p>ResourceHelper class.</p>
 */
public class ResourceHelper {

    /**
     * <p>Constructor for ResourceHelper.</p>
     */
    private ResourceHelper() {
    }

    /**
     * <p>getInputStream</p>
     *
     * @param resource a {@link org.springframework.core.io.Resource} object.
     * @return a {@link java.io.InputStream} object.
     */
    public static InputStream getInputStream(Resource resource) {
        try {
            return resource.getInputStream();
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }

    /**
     * <p>isEmpty</p>
     *
     * @param resource a {@link org.springframework.core.io.Resource} object.
     * @return a boolean.
     */
    public static boolean isEmpty(Resource resource) {
        if (null == resource || !resource.isReadable())
            return true;

        try (InputStream inputStream = resource.getInputStream()) {
            return inputStream.read() == -1;
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }

    /**
     * <p>write</p>
     *
     * @param resource a {@link org.springframework.core.io.Resource} object.
     * @param file     a {@link java.io.File} object.
     */
    public static void write(Resource resource, File file) throws IOException {
        try(InputStream is=getInputStream(resource)) {
            FileHelper.writeToFile(is, file);
        }
    }

    /**
     * <p>getFile</p>
     *
     * @param resource a {@link org.springframework.core.io.Resource} object.
     * @return a {@link java.io.File} object.
     */
    public static File getFile(Resource resource) {
        try {
            return resource.getFile();
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }

    /**
     * <p>toByteArray</p>
     *
     * @param resource a {@link org.springframework.core.io.Resource} object.
     * @return an array of byte.
     */
    public static byte[] toByteArray(Resource resource) {
        try {
            return IOUtils.toByteArray(resource.getInputStream());
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }
}
