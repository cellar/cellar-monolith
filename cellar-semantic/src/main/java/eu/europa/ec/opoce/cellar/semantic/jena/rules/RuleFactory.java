package eu.europa.ec.opoce.cellar.semantic.jena.rules;

import org.apache.jena.reasoner.rulesys.Rule;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.exception.CellarSemanticException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.List;

/**
 * <p>RuleFactory class.</p>
 */
public class RuleFactory {

    /**
     * Constant <code>log</code>
     */
    private static final Logger log = LoggerFactory.getLogger(RuleFactory.class);

    /**
     * <p>createRules</p>
     *
     * @param rules    a {@link java.lang.String} object.
     * @param ruleInfo a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    public static List<Rule> createRules(final String rules, final String ruleInfo) {
        return createRules(new StringReader(rules), ruleInfo);
    }

    /**
     * <p>createRules</p>
     *
     * @param ruleResource a {@link org.springframework.core.io.Resource} object.
     * @return a {@link java.util.List} object.
     */
    public static List<Rule> createRules(final Resource ruleResource) {
        try {
            log.debug("Loading rules {}.", ruleResource.getDescription());
            final String description = ruleResource.getDescription();
            String name = description;
            if (description.contains("[")) {
                name = StringUtils.substringBetween(description, "[", "]");
            }
            return createRules(new InputStreamReader(ruleResource.getInputStream()), name);
        } catch (final IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }

    /**
     * <p>createRules</p>
     *
     * @param reader   a {@link java.io.Reader} object.
     * @param ruleInfo a {@link java.lang.String} object.
     * @return a {@link java.util.List} object.
     */
    private static List<Rule> createRules(final Reader reader, final String ruleInfo) {
        try {
            final BufferedReader bufferedReader = new BufferedReader(reader);
            final List<Rule> rules = Rule.parseRules(Rule.rulesParserFromReader(bufferedReader));

            logRules(rules);

            return rules;
        } catch (final Throwable e) {
            try {
                reader.reset();
                throw ExceptionBuilder.get(CellarSemanticException.class).withMessage("Create rules failed for '{}'.")
                        .withMessageArgs(ruleInfo).withCause(e).build();
            } catch (final IOException exc) {
                throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
            }
        }
    }

    /**
     * <p>logRules</p>
     *
     * @param rules a {@link java.util.List} object.
     */
    private static void logRules(final List<Rule> rules) {
        for (final Rule rule : rules) {
            log.debug("Rule: {}", rule);
        }
    }
}
