/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.semantic.ontology
 *             FILE : InferenceModelDescription.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 09 05, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-09-05 11:36:15 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.semantic.ontology;

import java.time.ZonedDateTime;

/**
 * @author ARHS Developments
 */
public class InferenceModelDescription {

    private String name;
    private ZonedDateTime activationDate;

    public InferenceModelDescription() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ZonedDateTime getActivationDate() {
        return activationDate;
    }

    public void setActivationDate(ZonedDateTime activationDate) {
        this.activationDate = activationDate;
    }
}
