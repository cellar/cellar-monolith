package eu.europa.ec.opoce.cellar.semantic.ontology.cache.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <p>AnnotationProperties class.</p>
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface AnnotationProperties {

    /**
     * <p>value</p>
     *
     * @return a {@link java.lang.String} object.
     */
    String value();

    /**
     * <p>failOnNone</p>
     *
     * @return a boolean.
     */
    boolean failOnNone() default false;
}
