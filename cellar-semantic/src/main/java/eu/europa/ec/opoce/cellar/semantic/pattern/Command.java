package eu.europa.ec.opoce.cellar.semantic.pattern;

/**
 * <p>Command interface.</p>
 *
 */
public interface Command<OUT> {

    /**
     * <p>execute</p>
     *
     * @param <OUT> a OUT object.
     * @return a OUT object.
     */
    OUT execute();
}
