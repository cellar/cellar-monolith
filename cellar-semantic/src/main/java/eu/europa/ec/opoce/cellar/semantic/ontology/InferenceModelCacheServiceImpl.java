/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.semantic.ontology
 *             FILE : OntologyCacheServiceImpl.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 09 04, 2017
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2017-09-04 14:12:33 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.semantic.ontology;

import com.google.common.base.Strings;
import eu.europa.ec.opoce.cellar.cl.configuration.ICellarConfiguration;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.RDFFormat;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.time.ZoneId;

/**
 * @author ARHS Developments
 * @since 8.0
 */
@Service
public class InferenceModelCacheServiceImpl implements InferenceModelCacheService {

    private static final Logger LOG = LogManager.getLogger(InferenceModelCacheServiceImpl.class);
    private static final String DEFAULT_ONTOLOGY = "/eu/europa/ec/opoce/cellar/semantic/config/inferenceModel.rdf";

    private final String inferenceModelPath;
    private volatile OntModel inferenceModel;

    @Autowired
    public InferenceModelCacheServiceImpl(@Qualifier("cellarStaticConfiguration") ICellarConfiguration cellarConfiguration) {
        if (Strings.isNullOrEmpty(cellarConfiguration.getInferenceOntologyPath())) {
            LOG.warn("Empty value for property cellar.service.ontology.inference.path, fallback to embedded inferenceModel.rdf");
            inferenceModelPath = "/missing/path/inferenceModel.rdf"; // force fallback by settings an arbitrary path
        } else {
            inferenceModelPath = cellarConfiguration.getInferenceOntologyPath();
        }
    }

    @PostConstruct
    private void load() throws IOException {
        Path path = Paths.get(inferenceModelPath);
        if (!Files.exists(path)) {
            // Default configuration from JAR
            final ClassPathResource resource = new ClassPathResource(DEFAULT_ONTOLOGY);
            try (InputStream is = resource.getInputStream()) {
                setInferenceModel(createInferenceModel(JenaUtils.read(is, "", RDFFormat.RDFXML_PLAIN)));
            }

        } else {
            // Load the inferred ontology from the file system
            setInferenceModel(createInferenceModel(JenaUtils.read(path.toFile(), RDFFormat.RDFXML_PLAIN)));
        }
    }

    private static OntModel createInferenceModel(Model model) {
        final OntModel ontModel = ModelFactory.createOntologyModel();
        ontModel.add(model);
        return ontModel;
    }

    @Override
    public OntModel getInferenceModel() {
        return inferenceModel;
    }

    private void setInferenceModel(OntModel inferenceModel) {
        this.inferenceModel = inferenceModel;
    }

    @Override
    public boolean setInferenceModelFile(MultipartFile file) {
        try {
            if (file.getBytes() == null || file.getBytes().length == 0) {
                LOG.warn("Model is null or empty, the exiting model will not be replaced.");
                return false;
            }
            file.transferTo(Paths.get(inferenceModelPath).toFile());
            load(); // reload the file
        } catch (IOException e) {
            LOG.error("Error transferring the new inference model file", e);
            return false;
        }
        return true;
    }

    @Override
    public InferenceModelDescription getDescription() {
        final Path inferenceModelFile = Paths.get(inferenceModelPath);
        try {
            final BasicFileAttributes attr = Files.readAttributes(inferenceModelFile, BasicFileAttributes.class);
            final InferenceModelDescription description = new InferenceModelDescription();
            description.setName(inferenceModelFile.toString());
            description.setActivationDate(attr.lastModifiedTime().toInstant().atZone(ZoneId.systemDefault()));
            return description;
        } catch (IOException e) {
            LOG.error("Error retrieving attributes of " + inferenceModelFile, e);
        }
        return null;
    }
}
