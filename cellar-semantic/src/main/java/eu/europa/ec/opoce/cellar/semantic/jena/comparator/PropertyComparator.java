package eu.europa.ec.opoce.cellar.semantic.jena.comparator;

import java.util.Comparator;

import org.apache.jena.rdf.model.Property;

/**
 * <p>PropertyComparator class.</p>
 *
 */
public class PropertyComparator implements Comparator<Property> {

    /** {@inheritDoc} */
    @Override
    public int compare(Property one, Property other) {
        return one.getURI().compareTo(other.getURI());
    }
}
