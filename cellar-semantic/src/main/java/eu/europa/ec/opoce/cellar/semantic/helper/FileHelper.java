package eu.europa.ec.opoce.cellar.semantic.helper;

import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.exception.CellarSemanticException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static org.apache.commons.io.filefilter.DirectoryFileFilter.DIRECTORY;

/**
 * <p>FileHelper class.</p>
 */
public class FileHelper {

    /**
     * <p>Constructor for FileHelper.</p>
     */
    private FileHelper() {
    }

    /**
     * <p>openInputStream</p>
     *
     * @param file a {@link java.io.File} object.
     * @return a {@link java.io.FileInputStream} object.
     */
    public static FileInputStream openInputStream(File file) {
        try {
            return FileUtils.openInputStream(file);
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }

    /**
     * <p>writeByteArrayToFile</p>
     *
     * @param file a {@link java.io.File} object.
     * @param data an array of byte.
     */
    public static void writeByteArrayToFile(File file, byte[] data) {
        try {
            FileUtils.writeByteArrayToFile(file, data);
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }

    /**
     * <p>ensureDirectory</p>
     *
     * @param directory a {@link java.io.File} object.
     * @return a {@link java.io.File} object.
     */
    public static File ensureDirectory(File directory) {
        directory.mkdirs();
        if (!directory.isDirectory()) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withMessage("Failed to create directory '{}'.")
                    .withMessageArgs(directory.getAbsolutePath()).build();
        }
        return directory;
    }

    /**
     * <p>mkdirs</p>
     *
     * @param directory a {@link java.io.File} object.
     */
    public static void mkdirs(File directory) {
        try {
            FileUtils.forceMkdir(directory);
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }

    /**
     * <p>recurseFindFiles</p>
     *
     * @param folder       a {@link java.io.File} object.
     * @param folderFilter a {@link java.io.FileFilter} object.
     * @param fileFilter   a {@link java.io.FileFilter} object.
     * @return an array of {@link java.io.File} objects.
     */
    @Nonnull
    public static File[] recurseFindFiles(@Nonnull File folder, @Nullable FileFilter folderFilter, @Nonnull FileFilter fileFilter) {
        List<File> resultList = new ArrayList<File>();
        recurseFindFiles(resultList, folder, null == folderFilter ? DIRECTORY : folderFilter, fileFilter);
        return resultList.toArray(new File[resultList.size()]);
    }

    /**
     * <p>recurseFindFiles</p>
     *
     * @param resultList   a {@link java.util.List} object.
     * @param folder       a {@link java.io.File} object.
     * @param folderFilter a {@link java.io.FileFilter} object.
     * @param fileFilter   a {@link java.io.FileFilter} object.
     */
    private static void recurseFindFiles(@Nonnull List<File> resultList, @Nonnull File folder, @Nullable FileFilter folderFilter,
                                         @Nonnull FileFilter fileFilter) {
        if (!folder.isDirectory())
            return;

        Collections.addAll(resultList, folder.listFiles(fileFilter));
        for (File childFolder : folder.listFiles(folderFilter)) {
            recurseFindFiles(resultList, childFolder, folderFilter, fileFilter);
        }
    }

    /**
     * Returns the path for the given file, starting with the given parentDirectory as root.
     * Returns null if the given parentDirectory is not a parent of the given file.
     * If the 2 arguments point to the same file, an empty string is returned.
     *
     * @param parentDirectory a {@link java.io.File} object.
     * @param file            a {@link java.io.File} object.
     * @return a {@link java.lang.String} object.
     */
    @Nullable
    public static String getPathFrom(File parentDirectory, File file) {
        try {
            parentDirectory = parentDirectory.getCanonicalFile();
            file = file.getCanonicalFile();
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }

        if (parentDirectory.equals(file))
            return "";

        StringBuilder stringBuilder = new StringBuilder(file.getName());
        while (null != (file = file.getParentFile())) {
            if (parentDirectory.equals(file))
                return stringBuilder.toString();
            stringBuilder.insert(0, '/');
            stringBuilder.insert(0, file.getName());
        }
        return null;
    }

    /**
     * Returns the extension for the given file (without .)
     * If no extension is found, an empty string is returned, never null.
     *
     * @param file a {@link java.io.File} object.
     * @return a {@link java.lang.String} object.
     */
    public static String getExtension(File file) {
        return getExtension(file.getName());
    }

    /**
     * Returns the extension for the given filename (without .)
     * If no extension is found, an empty string is returned, never null.
     *
     * @param filename a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String getExtension(String filename) {
        int index = filename.lastIndexOf(".");
        return (-1 == index) ? "" : filename.substring(index + 1);
    }

    /**
     * <p>removeExtention</p>
     *
     * @param filename a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String removeExtention(String filename) {
        int index = filename.lastIndexOf(".");
        return (-1 == index) ? filename : filename.substring(0, index);
    }

    /**
     * <p>createTempFile</p>
     *
     * @param prefix    a {@link java.lang.String} object.
     * @param suffix    a {@link java.lang.String} object.
     * @param directory a {@link java.io.File} object.
     * @return a {@link java.io.File} object.
     */
    public static File createTempFile(String prefix, String suffix, File directory) {
        try {
            return File.createTempFile(prefix, suffix, directory);
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }

    /**
     * <p>createTempDirectory</p>
     *
     * @param prefix a {@link java.lang.String} object.
     * @return a {@link java.io.File} object.
     */
    public static File createTempDirectory(String prefix) {
        File tempDir = new File(FilenameUtils.normalize(System.getProperty("java.io.tmpdir")));
        long counter = 0;

        File result = new File(tempDir, prefix);
        while (!result.mkdirs()) {
            result = new File(tempDir, prefix + '-' + counter++);
        }

        forceDeleteOnExit(result, false);
        return result;
    }

    /**
     * <p>openOutputStream</p>
     *
     * @param file a {@link java.io.File} object.
     * @return a {@link java.io.FileOutputStream} object.
     */
    public static FileOutputStream openOutputStream(File file) {
        try {
            File parentFile = file.getParentFile();
            if (null != parentFile && !parentFile.isDirectory()) {
                mkdirs(parentFile);
            }
            return FileUtils.openOutputStream(file);
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withMessage("Failed to open outputstream to {}")
                    .withMessageArgs(file).withCause(e).build();
        }
    }

    /**
     * <p>writeToFile</p>
     *
     * @param inputStream a {@link java.io.InputStream} object.
     * @param outputFile  a {@link java.io.File} object.
     */
    public static void writeToFile(InputStream inputStream, File outputFile) {
        try (FileOutputStream outputStream = openOutputStream(outputFile);) {
            IOUtils.copy(inputStream, outputStream);
            outputStream.flush();
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withMessage("Failed to write inputstream to file {}")
                    .withMessageArgs(outputFile).withCause(e).build();
        }
    }

    /**
     * <p>writeStringToFile</p>
     *
     * @param file a {@link java.io.File} object.
     * @param data a {@link java.lang.String} object.
     */
    public static void writeStringToFile(File file, String data) {
        try {
            FileUtils.writeStringToFile(file, data, "UTF-8");
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }

    /**
     * <p>writeLines</p>
     *
     * @param file  a {@link java.io.File} object.
     * @param lines a {@link java.util.Collection} object.
     */
    public static void writeLines(File file, Collection<String> lines) {
        try {
            FileUtils.writeLines(file, "UTF-8", lines);
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }

    /**
     * <p>mkdirs</p>
     *
     * @param dirs a {@link java.io.File} object.
     */
    public static void mkdirs(File... dirs) {
        for (File dir : dirs) {
            if (dir.isFile()) {
                throw ExceptionBuilder.get(CellarSemanticException.class)
                        .withMessage("Cannot create directory '{}', it exists as a file")
                        .withMessageArgs(dir.getAbsolutePath()).build();
            }
            dir.mkdir();
            if (dir.isDirectory()) {
                continue;
            }

            try {
                mkdirsLoop(dir.getCanonicalFile(), dir.getAbsolutePath());
            } catch (IOException e) {
                throw ExceptionBuilder.get(CellarSemanticException.class).withMessage("Failed to create directory '{}'")
                        .withMessageArgs(dir.getAbsolutePath()).build();
            }
        }
    }

    /**
     * <p>mkdirsLoop</p>
     *
     * @param canonDir     a {@link java.io.File} object.
     * @param originalPath a {@link java.lang.String} object.
     */
    private static void mkdirsLoop(File canonDir, String originalPath) {
        File parent = canonDir.getParentFile();
        if (null == parent) {
            throw ExceptionBuilder.get(CellarSemanticException.class)
                    .withMessage("Failed to create directory '{}', parent of '{}' is null")
                    .withMessageArgs(originalPath, canonDir.getAbsolutePath()).build();
        }
        if (parent.isFile()) {
            throw ExceptionBuilder.get(CellarSemanticException.class)
                    .withMessage("Failed to create directory '{}', parent '{}' is a file")
                    .withMessageArgs(originalPath, parent.getAbsolutePath()).build();
        }

        if (!parent.isDirectory()) {
            mkdirsLoop(parent, originalPath);
        }

        canonDir.mkdir();
        if (!canonDir.isDirectory()) {
            throw ExceptionBuilder.get(CellarSemanticException.class)
                    .withMessage("Failed to create directory '{}' - could not create directory '{}' in loop")
                    .withMessageArgs(originalPath, canonDir.getAbsolutePath()).build();
        }
    }

    /**
     * <p>ensureCleanDirectory</p>
     *
     * @param directory a {@link java.io.File} object.
     * @return a {@link java.io.File} object.
     */
    public static File ensureCleanDirectory(File directory) {
        if (directory.exists()) {
            cleanDirectory(directory);
        }
        return ensureDirectory(directory);
    }

    /**
     * <p>cleanDirectory</p>
     *
     * @param directory a {@link java.io.File} object.
     */
    public static void cleanDirectory(File directory) {
        try {
            FileUtils.cleanDirectory(directory);
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withMessage("Could not clean directory '{}'.")
                    .withMessageArgs(directory.getAbsolutePath()).withCause(e).build();
        }
    }

    /**
     * <p>forceDeleteSilently</p>
     *
     * @param file a {@link java.io.File} object.
     */
    public static void forceDeleteSilently(File file) {
        try {
            FileUtils.forceDelete(file);
        } catch (IOException ignore) {
        }
    }

    /**
     * <p>forceDelete</p>
     *
     * @param file a {@link java.io.File} object.
     */
    public static void forceDelete(File file) {
        try {
            FileUtils.forceDelete(file);
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }

    /**
     * <p>forceDeleteOnExit</p>
     *
     * @param file a {@link java.io.File} object.
     */
    public static void forceDeleteOnExit(File file) {
        forceDeleteOnExit(file, true);
    }

    /**
     * <p>forceDeleteOnExit</p>
     *
     * @param file            a {@link java.io.File} object.
     * @param exceptionOnFail a boolean.
     */
    public static void forceDeleteOnExit(File file, boolean exceptionOnFail) {
    }

    /**
     * <p>copyFileToDirectory</p>
     *
     * @param sourceFile           a {@link java.io.File} object.
     * @param destinationDirectory a {@link java.io.File} object.
     */
    public static void copyFileToDirectory(File sourceFile, File destinationDirectory) {
        try {
            FileUtils.copyFileToDirectory(sourceFile, destinationDirectory);
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }

    /**
     * <p>moveFile</p>
     *
     * @param sourceFile      a {@link java.io.File} object.
     * @param destinationFile a {@link java.io.File} object.
     */
    public static void moveFile(File sourceFile, File destinationFile) {
        try {
            FileUtils.moveFile(sourceFile, destinationFile);
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }

    /**
     * <p>moveFileToDirectory</p>
     *
     * @param sourceFile           a {@link java.io.File} object.
     * @param destinationDirectory a {@link java.io.File} object.
     * @param createDestination    a boolean.
     * @return a {@link java.io.File} object.
     */
    public static File moveFileToDirectory(File sourceFile, File destinationDirectory, boolean createDestination) {
        try {
            FileUtils.moveFileToDirectory(sourceFile, destinationDirectory, createDestination);
            return new File(destinationDirectory, sourceFile.getName());
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }

    /**
     * <p>copyDirectory</p>
     *
     * @param srcDir  a {@link java.io.File} object.
     * @param destDir a {@link java.io.File} object.
     */
    public static void copyDirectory(File srcDir, File destDir) {
        try {
            FileUtils.copyDirectory(srcDir, destDir);
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }

    /**
     * <p>copyDirectory</p>
     *
     * @param srcDir     a {@link java.io.File} object.
     * @param destDir    a {@link java.io.File} object.
     * @param fileFilter a {@link java.io.FileFilter} object.
     */
    public static void copyDirectory(File srcDir, File destDir, FileFilter fileFilter) {
        try {
            FileUtils.copyDirectory(srcDir, destDir, fileFilter);
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }

    /**
     * <p>copyFile</p>
     *
     * @param sourceFile      a {@link java.io.File} object.
     * @param destinationFile a {@link java.io.File} object.
     */
    public static void copyFile(File sourceFile, File destinationFile) {
        try {
            FileUtils.copyFile(sourceFile, destinationFile);
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }

    /**
     * <p>readFileToString</p>
     *
     * @param file a {@link java.io.File} object.
     * @return a {@link java.lang.String} object.
     */
    public static String readFileToString(File file) {
        return readFileToString(file, "UTF-8");
    }

    /**
     * <p>readFileToString</p>
     *
     * @param file     a {@link java.io.File} object.
     * @param encoding a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String readFileToString(File file, String encoding) {
        try {
            return FileUtils.readFileToString(file, encoding);
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }

    /**
     * <p>readLines</p>
     *
     * @param file a {@link java.io.File} object.
     * @return a {@link java.util.List} object.
     */
    public static List<String> readLines(File file) {
        try {
            return FileUtils.readLines(file, "UTF-8");
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }

    /**
     * <p>toUrl</p>
     *
     * @param file a {@link java.io.File} object.
     * @return a {@link java.lang.String} object.
     */
    public static String toUrl(File file) {
        try {
            return file.toURI().toURL().toExternalForm();
        } catch (MalformedURLException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withMessage("Failed to convert file to url: {}")
                    .withMessageArgs(file.getAbsolutePath()).withCause(e).build();
        }
    }

    /**
     * <p>moveSelectionToDirectory</p>
     *
     * @param sourceDir            a {@link java.io.File} object.
     * @param destinationDirectory a {@link java.io.File} object.
     * @param fileFilter           a {@link java.io.FileFilter} object.
     */
    public static void moveSelectionToDirectory(File sourceDir, File destinationDirectory, FileFilter fileFilter) {
        if (!sourceDir.isDirectory()) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withMessage("sourceDir is not a directory").build();
        }

        try {
            for (File file : sourceDir.listFiles(fileFilter)) {
                FileUtils.moveToDirectory(file, destinationDirectory, true);
            }
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }

    }

    /**
     * <p>isParentOf</p>
     *
     * @param parent a {@link java.io.File} object.
     * @param child  a {@link java.io.File} object.
     * @return a boolean.
     */
    public static boolean isParentOf(File parent, File child) {
        try {
            String parentPath = parent.getCanonicalPath();
            String childPath = child.getCanonicalPath();
            return childPath.startsWith(parentPath);
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }

    /**
     * <p>isSameFile</p>
     *
     * @param file1 a {@link java.io.File} object.
     * @param file2 a {@link java.io.File} object.
     * @return a boolean.
     */
    public static boolean isSameFile(File file1, File file2) {
        try {
            return file1.getCanonicalPath().equals(file2.getCanonicalPath());
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }
}
