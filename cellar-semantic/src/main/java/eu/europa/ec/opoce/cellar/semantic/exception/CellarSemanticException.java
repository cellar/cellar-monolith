/* ----------------------------------------------------------------------------
 *     PROJECT : CELLAR maintenance
 *
 *     PACKAGE : eu.europa.ec.opoce.cellar.semantic.exception
 *        FILE : CellarSemanticException.java
 *
 *  CREATED BY : ARHS Developments
 *          ON : 02-08-2017
 *
 * MODIFIED BY : ARHS Developments
 *          ON : $LastChangedDate$
 *     VERSION : $LastChangedRevision$
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2017 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.semantic.exception;

import eu.europa.ec.opoce.cellar.exception.BuildableException;
import eu.europa.ec.opoce.cellar.exception.CellarException;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;

/**
 * This is a {@link CellarSemanticException} buildable by an {@link ExceptionBuilder}.</br>
 * It is usually thrown when performing semantic operations on metadata.</br>
 * </br>
 * ON : 02-08-2017
 *
 * @author ARHS Developments
 * @version $Revision$
 */
public class CellarSemanticException extends CellarException {

    /**
     * Constructs a new exception with its associated builder.
     *
     * @param builder the builder to use for building the exception
     */
    public CellarSemanticException(final ExceptionBuilder<? extends BuildableException> builder) {
        super(builder);
    }

}
