package eu.europa.ec.opoce.cellar.semantic.config;

import java.util.Properties;

import org.springframework.util.CollectionUtils;

/**
 * Helper class to merge multiple <code>Properties</code> objects into one <code>Properties</code> Object.
 * Used as Factory class for a merged <code>Properties</code> object.
 */
public class PropertiesFactoryBean {

    private Properties[] propertiesArray;

    /**
     * <p>Constructor for PropertiesFactoryBean.</p>
     */
    public PropertiesFactoryBean() {
    }

    /**
     * <p>Constructor for PropertiesFactoryBean.</p>
     *
     * @param properties a {@link java.util.Properties} object.
     */
    public PropertiesFactoryBean(Properties properties) {
        this.propertiesArray = new Properties[] {
                properties};
    }

    /**
     * <p>Constructor for PropertiesFactoryBean.</p>
     *
     * @param propertiesArray an array of {@link java.util.Properties} objects.
     */
    public PropertiesFactoryBean(Properties[] propertiesArray) {
        this.propertiesArray = propertiesArray;
    }

    /**
     * Merge all the Properties instances into a new Properties instance,
     * copying all properties (key-value pairs) over.
     * <p>Uses <code>Properties.propertyNames()</code> to even catch
     * default properties linked into the original Properties instance.
     *
     * @return a new merged Properties instance
     * @see org.springframework.util.CollectionUtils#mergePropertiesIntoMap
     */
    public Properties getProperties() {
        Properties result = new Properties();
        if (propertiesArray != null) {
            for (Properties props : propertiesArray) {
                CollectionUtils.mergePropertiesIntoMap(props, result);
            }
        }
        return result;
    }

    /**
     * <p>Setter for the field <code>propertiesArray</code>.</p>
     *
     * @param propertiesArray an array of {@link java.util.Properties} objects.
     */
    public void setPropertiesArray(Properties[] propertiesArray) {
        this.propertiesArray = propertiesArray;
    }
}
