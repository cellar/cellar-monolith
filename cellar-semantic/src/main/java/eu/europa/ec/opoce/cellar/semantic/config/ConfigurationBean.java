package eu.europa.ec.opoce.cellar.semantic.config;

/**
 * Interface to be implemented by beans that need to react once all
 * configuration properties are loaded by a {@link ConfigurationSettings} bean.
 * <code>ConfigurationBean</code> objects can be used as a kind of pluggin
 * to implement custom configuration logic on application start up.
 * For example:
 * <ol>
 * <li>to perform custom configuration</li>
 * <li>to perform validations (e.g. that all mandatory properties have been set)</li>
 * <li>to perform initialisations</li>
 * <li>to perform output</li>
 * <li>...</li>
 * <ol>
 *
 * @see eu.europa.ec.opoce.cellar.semantic.config.ConfigurationSettings
 */
public interface ConfigurationBean {

    /**
     * Invoked by a {@link ConfigurationSettings} bean after it has loaded all configuration properties.
     * <p>This method allows the bean instance to perform custom configuration logic only
     * possible after the initial configuration properties have been loaded and to throw an
     * exception in the event of misconfiguration.
     *
     * @param settings the configuration settings that have been set and/or loaded. Contains all configuration properties.
     * @throws java.lang.Exception in the event of misconfiguration.
     */
    public void configure(Settings settings) throws Exception;
}
