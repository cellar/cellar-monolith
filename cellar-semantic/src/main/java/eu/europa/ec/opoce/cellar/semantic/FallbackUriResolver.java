package eu.europa.ec.opoce.cellar.semantic;

import org.springframework.core.io.Resource;

/**
 * <p>Abstract FallbackUriResolver class.</p>
 *
 */
public abstract class FallbackUriResolver implements UriResolver {

    private final UriResolver fallback;

    /**
     * <p>Constructor for FallbackUriResolver.</p>
     *
     * @param fallback a {@link eu.europa.ec.opoce.cellar.semantic.UriResolver} object.
     */
    protected FallbackUriResolver(UriResolver fallback) {
        this.fallback = fallback;
    }

    /** {@inheritDoc} */
    @Override
    public final Resource resolveUri(String uri) {
        Resource resource = tryResolverUri(uri);
        return null != resource ? resource : fallback.resolveUri(uri);
    }

    /**
     * <p>tryResolverUri</p>
     *
     * @param uri a {@link java.lang.String} object.
     * @return a {@link org.springframework.core.io.Resource} object.
     */
    protected abstract Resource tryResolverUri(String uri);
}
