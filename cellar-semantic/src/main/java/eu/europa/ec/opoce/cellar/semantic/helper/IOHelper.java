package eu.europa.ec.opoce.cellar.semantic.helper;

import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.exception.CellarSemanticException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.Flushable;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;

/**
 * <p>IOHelper class.</p>
 */
public class IOHelper {

    /**
     * <p>Constructor for IOHelper.</p>
     */
    private IOHelper() {
    }

    /**
     * <p>closeQuietly</p>
     *
     * @param closable a X object.
     * @param <X>      a X object.
     */
    public static <X extends Closeable> void closeQuietly(X closable) {
        if (closable == null) {
            return;
        }

        try {
            closable.close();
        } catch (IOException ignore) {
        }
    }

    /**
     * <p>flushAndClose</p>
     *
     * @param closeFlusher a X object.
     * @param <X>          a X object.
     */
    public static <X extends Flushable & Closeable> void flushAndClose(X closeFlusher) {
        if (closeFlusher == null)
            return;

        try {
            closeFlusher.flush();
        } catch (IOException ignore) {
        }

        try {
            closeFlusher.close();
        } catch (IOException ignore) {
        }
    }

    /**
     * <p>writeObjectToStream</p>
     *
     * @param object a T object.
     * @param out    a {@link java.io.OutputStream} object.
     * @param <T>    a T object.
     */
    public static <T extends Serializable> void writeObjectToStream(T object, OutputStream out) {
        try (ObjectOutputStream oos = new ObjectOutputStream(out)) {
            oos.writeObject(object);
            oos.flush();
            out.flush();
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }

    /**
     * <p>writeObjectToFile</p>
     *
     * @param object a T object.
     * @param file   a {@link java.io.File} object.
     * @param <T>    a T object.
     */
    public static <T extends Serializable> void writeObjectToFile(T object, File file) {
        try (FileOutputStream fos = FileUtils.openOutputStream(file)) {
            writeObjectToStream(object, fos);
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withMessage("Failed to save object to file '{}'.")
                    .withMessageArgs(file.getAbsolutePath()).withCause(e).build();
        }
    }

    /**
     * <p>cloneObject</p>
     *
     * @param object a T object.
     * @param <T>    a T object.
     * @return a T object.
     */
    public static <T extends Serializable> T cloneObject(T object) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        writeObjectToStream(object, out);

        ByteArrayInputStream input = new ByteArrayInputStream(out.toByteArray());
        return readObjectFromStream(input);
    }

    /**
     * <p>readObjectFromStream</p>
     *
     * @param input a {@link java.io.InputStream} object.
     * @param <T>   a T object.
     * @return a T object.
     */
    @SuppressWarnings("unchecked")
    public static <T extends Serializable> T readObjectFromStream(InputStream input) {
        try (ObjectInputStream ois = new ObjectInputStream(input)) {
            return (T) ois.readObject();
        } catch (ClassNotFoundException | IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }

    /**
     * <p>readObjectFromFile</p>
     *
     * @param file a {@link java.io.File} object.
     * @param <T>  a T object.
     * @return a T object.
     */
    public static <T extends Serializable> T readObjectFromFile(File file) {
        try (FileInputStream fis = FileUtils.openInputStream(file)) {
            return readObjectFromStream(fis);
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }

    /**
     * <p>createOutputStreamWriter</p>
     *
     * @param outputStream a {@link java.io.OutputStream} object.
     * @param charsetName  a {@link java.lang.String} object.
     * @return a {@link java.io.OutputStreamWriter} object.
     */
    public static OutputStreamWriter createOutputStreamWriter(OutputStream outputStream, String charsetName) {
        try {
            return new OutputStreamWriter(outputStream, charsetName);
        } catch (UnsupportedEncodingException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }

    /**
     * <p>toString</p>
     *
     * @param inputStream a {@link java.io.InputStream} object.
     * @return a {@link java.lang.String} object.
     */
    public static String toString(InputStream inputStream) {
        try {
            return IOUtils.toString(inputStream, "UTF-8");
        } catch (IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        }
    }
}
