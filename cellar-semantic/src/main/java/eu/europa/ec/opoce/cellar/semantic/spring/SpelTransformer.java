package eu.europa.ec.opoce.cellar.semantic.spring;

import org.apache.commons.collections15.Transformer;
import org.springframework.expression.Expression;
import org.springframework.expression.spel.standard.SpelExpressionParser;

/**
 * <p>SpelTransformer class.</p>
 *
 */
public class SpelTransformer<I, O> implements Transformer<I, O> {

    private final Expression expression;

    /**
     * <p>Constructor for SpelTransformer.</p>
     *
     * @param expression a {@link java.lang.String} object.
     * @param <I> a I object.
     * @param <O> a O object.
     */
    public SpelTransformer(String expression) {
        this.expression = new SpelExpressionParser().parseExpression(expression);
    }

    /** {@inheritDoc} */
    @SuppressWarnings("unchecked")
    @Override
    public O transform(I input) {
        return (O) expression.getValue(input);
    }
}
