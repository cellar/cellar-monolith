package eu.europa.ec.opoce.cellar.semantic.jena.transformers;

import org.apache.commons.collections15.Transformer;

import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;

/**
 * <p>Statement2Subject class.</p>
 *
 */
public class Statement2Subject implements Transformer<Statement, Resource> {

    /** Constant <code>statement2Subject</code> */
    public static final Statement2Subject statement2Subject = new Statement2Subject();

    /**
     * <p>Constructor for Statement2Subject.</p>
     */
    private Statement2Subject() {
    }

    /** {@inheritDoc} */
    @Override
    public Resource transform(Statement input) {
        return input.getSubject();
    }
}
