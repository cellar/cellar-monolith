/* ----------------------------------------------------------------------------
 *          PROJECT : CELLAR maintenance
 *
 *          PACKAGE : eu.europa.ec.opoce.cellar.semantic.validation
 *             FILE : ValidationUtils.java
 *
 *       CREATED BY : ARHS Developments
 *               ON : 04 06, 2018
 *
 *      MODIFIED BY : ARHS Developments
 *               ON : $LastChangedDate: 2018-04-06 08:38:58 +0200 $
 *          VERSION : $LastChangedRevision: 0 $
 *
 * ----------------------------------------------------------------------------
 * Copyright (c) 2011-2018 European Commission - Publications Office
 * ----------------------------------------------------------------------------
 */
package eu.europa.ec.opoce.cellar.semantic.validation;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * @author ARHS Developments
 */
public final class ValidationUtils {
    private static final Pattern VALID_URI_CHARACTERS_PATTERN = Pattern.compile("[-~\\.!*'();:@&=+$,\\/?#\\[\\]%\\w]*");

    private ValidationUtils() {

    }

    public static List<String> validateUris(Model model) {
        List<String> invalidUris = new ArrayList<>();
        StmtIterator iterator = model.listStatements();
        while (iterator.hasNext()) {
            Statement statement = iterator.nextStatement();

            Resource subject = statement.getSubject();
            if (isInvalidUri(subject)) {
                invalidUris.add(subject.getURI());
            }

            RDFNode object = statement.getObject();
            if (isInvalidUri(object)) {
                invalidUris.add(object.asResource().getURI());
            }
        }

        return invalidUris;
    }

    public static boolean isInvalidUri(final RDFNode node) {
        if (!node.isURIResource()) {
            return false;
        }
        final Resource resource = (Resource) node;
        return !VALID_URI_CHARACTERS_PATTERN.matcher(resource.getURI()).matches();
    }
}
