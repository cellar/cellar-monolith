package eu.europa.ec.opoce.cellar.semantic.jena.template;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.QuerySolutionMap;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.RDFNode;
import eu.europa.ec.opoce.cellar.common.closure.RPClosure;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.exception.CellarSemanticException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

import static eu.europa.ec.opoce.cellar.semantic.jena.template.JenaQueryUtils.closeQuietly;
import static eu.europa.ec.opoce.cellar.semantic.jena.template.JenaQueryUtils.newQueryExecution;
import static eu.europa.ec.opoce.cellar.semantic.jena.template.JenaResultSetHandlers.listOfMapsResolver;

/**
 * <p>JenaQueryTemplate class.</p>
 */
public class JenaQueryTemplate implements JenaQueryOperations {

    /**
     * Constant <code>log</code>
     */
    private static final Logger log = LoggerFactory.getLogger(JenaQueryTemplate.class);

    /**
     * <p>select</p>
     *
     * @param model  a {@link org.apache.jena.rdf.model.Model} object.
     * @param sparql a {@link org.apache.jena.query.Query} object.
     * @return a {@link java.util.List} object.
     */
    public static List<Map<String, RDFNode>> select(Model model, Query sparql) {
        return select(model, sparql, (QuerySolution) null, false);
    }

    /**
     * <p>select</p>
     *
     * @param model         a {@link org.apache.jena.rdf.model.Model} object.
     * @param sparql        a {@link org.apache.jena.query.Query} object.
     * @param querySolution a {@link org.apache.jena.query.QuerySolution} object.
     * @return a {@link java.util.List} object.
     */
    public static List<Map<String, RDFNode>> select(Model model, Query sparql, QuerySolution querySolution) {
        return select(model, sparql, querySolution, false);
    }

    /**
     * <p>select</p>
     *
     * @param model  a {@link org.apache.jena.rdf.model.Model} object.
     * @param sparql a {@link org.apache.jena.query.Query} object.
     * @param close  a boolean.
     * @return a {@link java.util.List} object.
     */
    public static List<Map<String, RDFNode>> select(Model model, Query sparql, boolean close) {
        return select(model, sparql, (QuerySolution) null, close);
    }

    /**
     * <p>select</p>
     *
     * @param model         a {@link org.apache.jena.rdf.model.Model} object.
     * @param sparql        a {@link org.apache.jena.query.Query} object.
     * @param querySolution a {@link org.apache.jena.query.QuerySolution} object.
     * @param close         a boolean.
     * @return a {@link java.util.List} object.
     */
    public static List<Map<String, RDFNode>> select(Model model, Query sparql, QuerySolution querySolution, boolean close) {
        return select(model, sparql, listOfMapsResolver, querySolution, close);
    }

    /**
     * <p>select</p>
     *
     * @param model             a {@link org.apache.jena.rdf.model.Model} object.
     * @param sparql            a {@link org.apache.jena.query.Query} object.
     * @param resultSetResolver a {@link eu.europa.ec.opoce.cellar.semantic.jena.template.JenaResultSetHandler} object.
     * @param <T>               a T object.
     * @return a T object.
     */
    public static <T> T select(Model model, Query sparql, JenaResultSetHandler<T> resultSetResolver) {
        return select(model, sparql, resultSetResolver, false);
    }

    /**
     * <p>select</p>
     *
     * @param model             a {@link org.apache.jena.rdf.model.Model} object.
     * @param sparql            a {@link org.apache.jena.query.Query} object.
     * @param resultSetResolver a {@link eu.europa.ec.opoce.cellar.semantic.jena.template.JenaResultSetHandler} object.
     * @param close             a boolean.
     * @param <T>               a T object.
     * @return a T object.
     */
    public static <T> T select(Model model, Query sparql, JenaResultSetHandler<T> resultSetResolver, boolean close) {
        return select(model, sparql, resultSetResolver, null, close);
    }

    /**
     * <p>select</p>
     *
     * @param model             a {@link org.apache.jena.rdf.model.Model} object.
     * @param sparql            a {@link org.apache.jena.query.Query} object.
     * @param resultSetResolver a {@link eu.europa.ec.opoce.cellar.semantic.jena.template.JenaResultSetHandler} object.
     * @param querySolution     a {@link org.apache.jena.query.QuerySolution} object.
     * @param close             a boolean.
     * @param <T>               a T object.
     * @return a T object.
     */
    public static <T> T select(Model model, Query sparql, JenaResultSetHandler<T> resultSetResolver, QuerySolution querySolution,
                               boolean close) {
        QueryExecution queryExecution = null;
        try {
            queryExecution = newQueryExecution(model, sparql, querySolution);

            long start = System.currentTimeMillis();
            log.debug("EXECUTING SPARQL QUERY... [{}]", sparql);

            ResultSet resultSet = queryExecution.execSelect();

            log.debug("Select executed in '{}' ms. ...[{}]", System.currentTimeMillis() - start, sparql);

            start = System.currentTimeMillis();

            T result = resultSetResolver.handle(resultSet);

            log.debug("Resultset processed in '{}' ms. ...[{}]", System.currentTimeMillis() - start, sparql);

            return result;
        } catch (Exception e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withMessage("Error while executing sparql query [{}]")
                    .withMessageArgs(sparql).withCause(e).build();
        } finally {
            closeQuietly(queryExecution, close ? model : null);
        }
    }

    /**
     * <p>select</p>
     *
     * @param model   a {@link org.apache.jena.rdf.model.Model} object.
     * @param sparql  a {@link org.apache.jena.query.Query} object.
     * @param handler a {@link eu.europa.ec.opoce.cellar.semantic.jena.template.ListOfMapHandler} object.
     * @param <T>     a T object.
     * @return a T object.
     */
    public static <T> T select(Model model, Query sparql, ListOfMapHandler<T> handler) {
        return select(model, sparql, handler, false);
    }

    /**
     * <p>select</p>
     *
     * @param model   a {@link org.apache.jena.rdf.model.Model} object.
     * @param sparql  a {@link org.apache.jena.query.Query} object.
     * @param handler a {@link eu.europa.ec.opoce.cellar.semantic.jena.template.ListOfMapHandler} object.
     * @param close   a boolean.
     * @param <T>     a T object.
     * @return a T object.
     */
    public static <T> T select(Model model, Query sparql, ListOfMapHandler<T> handler, boolean close) {
        log.debug("Timing query [{}]", sparql);
        long start = System.currentTimeMillis();

        List<Map<String, RDFNode>> listOfBindings = select(model, sparql, close);

        log.debug("Execution time: '{}' ms.[{}]", System.currentTimeMillis() - start, sparql);

        long time = System.currentTimeMillis();

        T result = handler.handle(listOfBindings);

        log.debug("Processing time: '{}' ms.[{}]", System.currentTimeMillis() - time, sparql);
        log.debug("TOTAL TIME(EXECUTION AND PROCESSING): '{}' ms.[{}]", System.currentTimeMillis() - start, sparql);

        return result;
    }

    /**
     * <p>ask</p>
     *
     * @param model  a {@link org.apache.jena.rdf.model.Model} object.
     * @param sparql a {@link org.apache.jena.query.Query} object.
     * @return a boolean.
     */
    public static boolean ask(Model model, Query sparql) {
        return ask(model, sparql, false);
    }

    /**
     * <p>ask</p>
     *
     * @param model  a {@link org.apache.jena.rdf.model.Model} object.
     * @param sparql a {@link org.apache.jena.query.Query} object.
     * @param close  a boolean.
     * @return a boolean.
     */
    public static boolean ask(Model model, Query sparql, boolean close) {
        return ask(model, sparql, JenaBooleanHandler.booleanAskResultExtractor, close);
    }

    /**
     * <p>ask</p>
     *
     * @param model            a {@link org.apache.jena.rdf.model.Model} object.
     * @param sparql           a {@link org.apache.jena.query.Query} object.
     * @param booleanExtractor a {@link eu.europa.ec.opoce.cellar.semantic.jena.template.JenaBooleanHandler} object.
     * @param <T>              a T object.
     * @return a T object.
     */
    public static <T> T ask(Model model, Query sparql, JenaBooleanHandler<T> booleanExtractor) {
        return ask(model, sparql, booleanExtractor, false);
    }

    /**
     * <p>ask</p>
     *
     * @param model            a {@link org.apache.jena.rdf.model.Model} object.
     * @param sparql           a {@link org.apache.jena.query.Query} object.
     * @param booleanExtractor a {@link eu.europa.ec.opoce.cellar.semantic.jena.template.JenaBooleanHandler} object.
     * @param close            a boolean.
     * @param <T>              a T object.
     * @return a T object.
     */
    public static <T> T ask(Model model, Query sparql, JenaBooleanHandler<T> booleanExtractor, boolean close) {
        QueryExecution queryExecution = null;
        try {
            queryExecution = newQueryExecution(model, sparql);

            log.debug("EXECUTING SPARQL QUERY... [{}]", sparql);
            boolean result = queryExecution.execAsk();
            return booleanExtractor.handle(result);
        } catch (Exception e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withMessage("Error while executing sparql query [{}]")
                    .withMessageArgs(sparql).withCause(e).build();
        } finally {
            closeQuietly(queryExecution, close ? model : null);
        }
    }

    /**
     * <p>construct</p>
     *
     * @param model  a {@link org.apache.jena.rdf.model.Model} object.
     * @param sparql a {@link org.apache.jena.query.Query} object.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    public static Model construct(Model model, final Query sparql) {
        return construct(model, sparql, ModelCallbacks.modelAsResultCallback, false, false);
    }

    /**
     * <p>construct</p>
     *
     * @param model          a {@link org.apache.jena.rdf.model.Model} object.
     * @param sparql         a {@link org.apache.jena.query.Query} object.
     * @param initialBinding a {@link org.apache.jena.query.QuerySolutionMap} object.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    public static Model construct(Model model, final Query sparql, final QuerySolutionMap initialBinding) {
        return construct(model, sparql, initialBinding, ModelCallbacks.modelAsResultCallback, false, false);
    }

    /**
     * {@inheritDoc}
     */
    public static Model construct(Model model, final Query sparql, boolean closeInput) {
        return construct(model, sparql, ModelCallbacks.modelAsResultCallback, false, closeInput);
    }

    /**
     * <p>construct</p>
     *
     * @param model          a {@link org.apache.jena.rdf.model.Model} object.
     * @param sparql         a {@link org.apache.jena.query.Query} object.
     * @param initialBinding a {@link org.apache.jena.query.QuerySolutionMap} object.
     * @param closeInput     a boolean.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    public static Model construct(Model model, final Query sparql, final QuerySolutionMap initialBinding, boolean closeInput) {
        return construct(model, sparql, initialBinding, ModelCallbacks.modelAsResultCallback, false, closeInput);
    }

    /**
     * <p>construct</p>
     *
     * @param model         a {@link org.apache.jena.rdf.model.Model} object.
     * @param sparql        a {@link org.apache.jena.query.Query} object.
     * @param modelCallBack a {@link RPClosure} object.
     * @param closeResult   a boolean.
     * @param closeInput    a boolean.
     * @param <T>           a T object.
     * @return a T object.
     */
    public static <T> T construct(Model model, Query sparql, RPClosure<T, Model> modelCallBack, boolean closeResult, boolean closeInput) {
        return construct(model, sparql, null, modelCallBack, closeResult, closeInput);
    }

    /**
     * <p>construct</p>
     *
     * @param model          a {@link org.apache.jena.rdf.model.Model} object.
     * @param sparql         a {@link org.apache.jena.query.Query} object.
     * @param initialBinding a {@link org.apache.jena.query.QuerySolutionMap} object.
     * @param modelCallBack  a {@link RPClosure} object.
     * @param closeResult    a boolean.
     * @param closeInput     a boolean.
     * @param <T>            a T object.
     * @return a T object.
     */
    public static <T> T construct(Model model, Query sparql, QuerySolutionMap initialBinding, RPClosure<T, Model> modelCallBack,
                                  boolean closeResult, boolean closeInput) {
        QueryExecution queryExecution = null;
        Model result = null;
        try {
            queryExecution = newQueryExecution(model, sparql, initialBinding);

            log.debug("EXECUTING SPARQL QUERY... [{}]", sparql);
            result = queryExecution.execConstruct();
            return modelCallBack.doCall(result);
        } catch (Exception e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withMessage("Error while executing sparql query [{}]")
                    .withMessageArgs(sparql).withCause(e).build();
        } finally {
            if (closeResult)
                closeQuietly(result);
            closeQuietly(queryExecution, closeInput ? model : null);
        }
    }

    /**
     * <p>construct</p>
     *
     * @param inputModel  a {@link org.apache.jena.rdf.model.Model} object.
     * @param sparql      a {@link org.apache.jena.query.Query} object.
     * @param resultModel a {@link org.apache.jena.rdf.model.Model} object.
     * @param closeInput  a boolean.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    public static Model construct(Model inputModel, Query sparql, Model resultModel, boolean closeInput) {
        QueryExecution queryExecution = null;
        try {
            queryExecution = newQueryExecution(inputModel, sparql);

            log.debug("EXECUTING SPARQL QUERY... [{}]", sparql);
            return queryExecution.execConstruct(resultModel);
        } catch (Exception e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withMessage("Error while executing sparql query [{}]")
                    .withMessageArgs(sparql).withCause(e).build();
        } finally {
            closeQuietly(queryExecution, closeInput ? inputModel : null);

        }
    }

    /**
     * <p>execute</p>
     *
     * @param model         a {@link org.apache.jena.rdf.model.Model} object.
     * @param modelCallback a {@link RPClosure} object.
     * @param <T>           a T object.
     * @return a T object.
     */
    public static <T> T execute(Model model, RPClosure<T, Model> modelCallback) {
        return execute(model, modelCallback, false);
    }

    /**
     * <p>execute</p>
     *
     * @param model         a {@link org.apache.jena.rdf.model.Model} object.
     * @param modelCallback a {@link RPClosure} object.
     * @param close         a boolean.
     * @param <T>           a T object.
     * @return a T object.
     */
    public static <T> T execute(Model model, RPClosure<T, Model> modelCallback, boolean close) {
        try {
            return modelCallback.doCall(model);
        } catch (Exception e) {
            throw ExceptionBuilder.get(CellarSemanticException.class).withCause(e).build();
        } finally {
            if (close) {
                closeQuietly(model);
            }
        }
    }

    private final Model model;

    /**
     * <p>Constructor for JenaQueryTemplate.</p>
     *
     * @param model a {@link org.apache.jena.rdf.model.Model} object.
     */
    public JenaQueryTemplate(Model model) {
        this.model = model;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Map<String, RDFNode>> select(Query sparql) {
        return select(model, sparql);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> T select(final Query sparql, final JenaResultSetHandler<T> resultSetResolver) {
        return select(model, sparql, resultSetResolver);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> T select(Query sparql, ListOfMapHandler<T> handler) {
        return select(model, sparql, handler);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean ask(final Query sparql) {
        return ask(sparql, JenaBooleanHandler.booleanAskResultExtractor);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> T ask(final Query sparql, final JenaBooleanHandler<T> booleanExtractor) {
        return ask(model, sparql, booleanExtractor);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Model construct(final Query sparql) {
        return construct(sparql, ModelCallbacks.modelAsResultCallback, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Model construct(Query sparql, Model resultModel) {
        return construct(sparql, resultModel, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Model construct(Query sparql, Model resultModel, boolean closeInput) {
        return construct(model, sparql, resultModel, closeInput);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> T construct(final Query sparql, final RPClosure<T, Model> modelCallBack, boolean closeResult) {
        return construct(model, sparql, modelCallBack, closeResult, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public <T> T execute(RPClosure<T, Model> modelCallBack) {
        return execute(model, modelCallBack);
    }

}
