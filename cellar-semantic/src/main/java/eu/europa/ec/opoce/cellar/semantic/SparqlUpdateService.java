package eu.europa.ec.opoce.cellar.semantic;

import org.apache.jena.rdf.listeners.StatementListener;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.update.UpdateAction;
import org.apache.jena.update.UpdateRequest;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.semantic.ontology.InferenceModelCacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>SparqlUpdateService class.</p>
 *
 */
@Service
public class SparqlUpdateService {

    private final InferenceModelCacheService inferenceModelCacheService;

    @Autowired
    public SparqlUpdateService(InferenceModelCacheService inferenceModelCacheService) {
        this.inferenceModelCacheService = inferenceModelCacheService;
    }

    /**
     * Run an update sparql on a given model.
     *
     * @param model          The model with the source data and which will contain the result.
     * @param updateRequest  The update sparql.
     * @param useInference   If true, a pellet ontology model will be created to run the sparql on.
     * @param ontologyModels Optional model(s) containing the ontology.
     */
    public void runUpdate(Model model, UpdateRequest updateRequest, boolean useInference, Model... ontologyModels) {
        runUpdate(model, model, updateRequest, useInference, ontologyModels);
    }

    /**
     * Run an update sparql without impacting the source model.
     *
     * @param inModel        The model containing the input data. This model won't be changed.
     * @param outModel       The model which should be updated (can contain data or can be empty).
     * @param updateRequest  The update sparql.
     * @param useInference   If true, a pellet ontology model will be created to run the sparql on.
     * @param ontologyModels Optional model(s) containing the ontology.
     */
    public void runUpdate(Model inModel, Model outModel, UpdateRequest updateRequest, boolean useInference, Model... ontologyModels) {
        Model toUseInModel = getInModel(inModel, useInference, ontologyModels);

        if (toUseInModel == outModel) {
            UpdateAction.execute(updateRequest, toUseInModel);
            return;
        }

        UpdateModelListener listener = new UpdateModelListener(outModel);
        toUseInModel.register(listener);
        UpdateAction.execute(updateRequest, toUseInModel);
        inModel.unregister(listener);

        if (inModel != toUseInModel) {
            JenaUtils.closeQuietly(toUseInModel);
        }
    }

    /**
     * <p>getInModel</p>
     *
     * @param origInModel a {@link org.apache.jena.rdf.model.Model} object.
     * @param useInference a boolean.
     * @param ontologyModels a {@link org.apache.jena.rdf.model.Model} object.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     */
    private Model getInModel(Model origInModel, boolean useInference, Model... ontologyModels) {
        if (!useInference && ontologyModels.length == 0)
            return origInModel;

        Model inModel = ModelFactory.createDefaultModel();
        inModel.add(origInModel);
        for (Model ontologyModel : ontologyModels) {
            inModel.add(ontologyModel);
        }

        return inModel;
    }

    private static class UpdateModelListener extends StatementListener {

        private final Model model;

        private UpdateModelListener(Model model) {
            this.model = model;
        }

        @Override
        public void addedStatement(Statement s) {
            model.add(s);
        }

        @Override
        public void removedStatement(Statement s) {
            model.remove(s);

        }
    }
}
