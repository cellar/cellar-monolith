package eu.europa.ec.opoce.cellar.semantic.inferredontology;

import eu.europa.ec.opoce.cellar.semantic.jena.template.JenaOperations;
import eu.europa.ec.opoce.cellar.semantic.jena.template.JenaTemplate;
import eu.europa.ec.opoce.cellar.semantic.pattern.Command;

import java.util.List;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;

/**
 * <p>InferredOntologyBuilder class.</p>
 *
 */
public class InferredOntologyBuilder implements Command<Model> {

    private final JenaOperations jenaOperations;
    private final List<String> constructQueries;

    /**
     * <p>Constructor for InferredOntologyBuilder.</p>
     *
     * @param ontologyModel a {@link org.apache.jena.rdf.model.Model} object.
     * @param constructQueries a {@link java.util.List} object.
     */
    public InferredOntologyBuilder(Model ontologyModel, List<String> constructQueries) {
        this.jenaOperations = new JenaTemplate(ontologyModel);
        this.constructQueries = constructQueries;
    }

    /** {@inheritDoc} */
    @Override
    public Model execute() {
        Model inferenceMetamodel = ModelFactory.createDefaultModel();

        for (String constructQuery : constructQueries) {
            jenaOperations.construct(constructQuery, inferenceMetamodel);
        }

        return inferenceMetamodel;
    }

}
