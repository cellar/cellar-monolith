package eu.europa.ec.opoce.cellar.semantic;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.rdf.model.StmtIterator;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.vocabulary.OWL;
import eu.europa.ec.opoce.cellar.exception.ExceptionBuilder;
import eu.europa.ec.opoce.cellar.semantic.exception.CellarSemanticException;
import eu.europa.ec.opoce.cellar.semantic.jena.JenaUtils;
import eu.europa.ec.opoce.cellar.semantic.pattern.Command;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

/**
 * <p>ModelLoader class.</p>
 */
public class ModelLoader implements Command<ModelLoader.LoadInfo> {

    private static final Logger LOG = LogManager.getLogger(ModelLoader.class);

    private final UriResolver uriResolver;
    private final Set<OntologyUri> additionalOntologyUris;
    private final boolean loadImports;
    private final String[] ontologyUris;
    private final Set<String> loadedOntologyUris = new HashSet<>();
    private final Model fullModel = ModelFactory.createDefaultModel();

    /**
     * <p>Constructor for ModelLoader.</p>
     *
     * @param ontologyUris           an array of {@link java.lang.String} objects.
     * @param uriResolver            a {@link eu.europa.ec.opoce.cellar.semantic.UriResolver} object.
     * @param additionalOntologyUris a {@link java.util.Set} object.
     * @param loadImports            a boolean.
     */
    public ModelLoader(final String[] ontologyUris, final UriResolver uriResolver, final Set<OntologyUri> additionalOntologyUris,
                       final boolean loadImports) {
        this.ontologyUris = ontologyUris;
        this.uriResolver = uriResolver;
        this.additionalOntologyUris = additionalOntologyUris;
        this.loadImports = loadImports;
    }

    /**
     * <p>execute</p>
     *
     * @return a {@link eu.europa.ec.opoce.cellar.semantic.ModelLoader.LoadInfo} object.
     */
    @Override
    public LoadInfo execute() {

        try {
            for (final OntologyUri additionalOntologyUri : additionalOntologyUris) {
                addModel(additionalOntologyUri.getUri(), false);
            }
            for (final String ontologyUri : ontologyUris) {
                addModel(ontologyUri, loadImports);
            }
        } catch (final IOException e) {
            throw ExceptionBuilder.get(CellarSemanticException.class)
                    .withMessage("Failed to load all data for ontology: {}")
                    .withMessageArgs(StringUtils.join(ontologyUris, ", ")).withCause(e).build();
        }

        return new LoadInfo();
    }

    /**
     * <p>addModel</p>
     *
     * @param uri                   a {@link java.lang.String} object.
     * @param loadImports a boolean.
     * @throws java.io.IOException if any.
     */
    private void addModel(final String uri, final boolean loadImports) throws IOException {
        if (loadedOntologyUris.add(uri)) {
            LOG.info("Loading ontology in model '{}'.", uri);
            final Model model = loadModel(uri);
            fullModel.add(model);
            LOG.info("Loading ontology in model '{}' done.", uri);
            if (loadImports) {
                final StmtIterator stmtIterator = model.listStatements(null, OWL.imports, (Resource) null);
                while (stmtIterator.hasNext()) {
                    final Statement importedURI = stmtIterator.nextStatement();
                    addModel(JenaUtils.stringize(importedURI.getResource()), true);
                }
            }
        }
    }

    /**
     * <p>loadModel</p>
     *
     * @param uri a {@link java.lang.String} object.
     * @return a {@link org.apache.jena.rdf.model.Model} object.
     * @throws java.io.IOException if any.
     */
    private Model loadModel(final String uri) throws IOException {
        LOG.debug("getModel loads uri: {}", uri);
        final org.springframework.core.io.Resource resource = uriResolver.resolveUri(uri);
        if (resource == null) {
            throw ExceptionBuilder.get(CellarSemanticException.class)
                    .withMessage("Could not resolve uri: {}").withMessageArgs(uri).build();
        }
        final Model model = JenaUtils.read(resource, RDFFormat.NT);
        LOG.debug("getModel {} size is {}.", uri, model.size());
        return model;
    }

    public class LoadInfo {

        public Model getModel() {
            return fullModel;
        }

        public String[] getLoadedOntologyUris() {
            return loadedOntologyUris.toArray(new String[loadedOntologyUris.size()]);
        }
    }
}
