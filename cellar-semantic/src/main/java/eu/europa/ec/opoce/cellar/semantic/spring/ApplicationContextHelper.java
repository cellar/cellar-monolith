package eu.europa.ec.opoce.cellar.semantic.spring;

import org.springframework.context.ApplicationContext;

/**
 * <p>ApplicationContextHelper class.</p>
 *
 */
public class ApplicationContextHelper {

    /**
     * <p>Constructor for ApplicationContextHelper.</p>
     */
    private ApplicationContextHelper() {
    }

    /**
     * <p>getBean</p>
     *
     * @param applicationContext a {@link org.springframework.context.ApplicationContext} object.
     * @param name a {@link java.lang.String} object.
     * @param <T> a T object.
     * @return a T object.
     */
    @SuppressWarnings("unchecked")
    public static <T> T getBean(ApplicationContext applicationContext, String name) {
        //noinspection unchecked
        return (T) applicationContext.getBean(name);
    }
}
