package eu.europa.ec.opoce.cellar.semantic.xstream;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
@XStreamAlias("regexs")
public class Regexs {

    @XStreamImplicit(itemFieldName = "regex")
    private List<Regex> regexs = new ArrayList<Regex>();

    private Map<String, String> map = new HashMap<String, String>();

    public Regexs() {
    }

    public void addRegex(Regex regex) {
        regexs.add(regex);
    }

    public List<Regex> getRegexs() {
        return regexs;
    }

    public Map<String, String> getPrefixUris(String baseUrl) {
        if (map == null) {
            map=new HashMap<>();
        }
        for (Regex regex : regexs) {
            map.put(regex.getPrefix(), baseUrl + regex.getUrl());
        }
        return map;
    }
}
