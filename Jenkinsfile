pipeline {
    agent {
        kubernetes {
            yaml '''
                    apiVersion: v1
                    kind: Pod
                    metadata:
                      name: cellar
                      namespace: jenkins
                    spec:
                      affinity:
                        podAntiAffinity:
                          preferredDuringSchedulingIgnoredDuringExecution:
                          - weight: 50
                            podAffinityTerm:
                              labelSelector:
                                matchExpressions:
                                - key: jenkins/jenkins-jenkins-agent
                                  operator: In
                                  values:
                                  - "true"
                              topologyKey: kubernetes.io/hostname
                      securityContext:
                        runAsUser: 0
                        runAsGroup: 0
                      containers:
                      - name: cellar-builder
                        image: eddevopsd2/maven-java-npm-docker:mvn3.6.3-jdk8-node14.17.6-npm6-docker
                        volumeMounts:
                        - name: maven
                          mountPath: /root/.m2/
                          subPath: cellar
                        tty: true
                        securityContext:
                          privileged: true
                          runAsUser: 0
                          runAsGroup: 0
                      imagePullSecrets:
                      - name: regcred
                      restartPolicy: OnFailure
                      volumes:
                      - name: maven
                        persistentVolumeClaim:
                          claimName: maven-nfs-pvc
            '''
            workspaceVolume persistentVolumeClaimWorkspaceVolume(claimName: 'workspace-nfs-pvc', readOnly: false)
        }
    }
    options {
        disableConcurrentBuilds()
        buildDiscarder(logRotator(numToKeepStr: '10'))
        timeout(time: 3, unit: 'HOURS')
    }
    stages {
        stage('Pre') {
            steps {
                container (name: 'cellar-builder') {
                    sh 'curl -sL https://dl.min.io/client/mc/release/linux-amd64/mc --create-dirs -o "$HOME/minio-binaries/mc" && chmod +x "$HOME/minio-binaries/mc" && export PATH="$PATH:$HOME/minio-binaries" && mc --help'
                }
            }
        }
        stage('Build') {
            steps {
                container (name: 'cellar-builder') {
                    sh 'mvn clean install -Pdigit,ed-dev,minimal -Duser.timezone="Europe/Paris" -Dmaven.test.failure.ignore=true -T 2C -s /root/.m2/cellar-settings.xml'
                }
            }
        }
        stage('Dependency Check') {
            steps {
                container (name: 'cellar-builder') {
                    sh 'mvn org.owasp:dependency-check-maven:aggregate -Ddependency-check.suppressionFile=owasp-dependency-check-exclude.xml -Ddependency-check.assemblyAnalyzerEnabled=false -s /root/.m2/cellar-settings.xml'
                    sh '''
                        export PATH=$PATH:$HOME/minio-binaries/
                        mc alias set myminio/ https://minio.devops-d2.eurodyn.com ${MINIO_ACCESS_KEY} ${MINIO_SECRET_KEY}
                        cp -f -v target/dependency-check-report.html target/$BRANCH_NAME-$BUILD_NUMBER-dependency-check-report.html
                        mc cp target/$BRANCH_NAME-$BUILD_NUMBER-dependency-check-report.html myminio/cellar/Cellar
                    '''
                }
            }
        }
        stage('Sonar Analysis') {
            steps {
                container (name: 'cellar-builder') {
                    withSonarQubeEnv('sonar'){
                        sh 'update-alternatives --set java /usr/lib/jvm/java-11-openjdk-amd64/bin/java'
                        sh 'mvn sonar:sonar -Dsonar.projectName=Cellar -Dsonar.host.url=${SONAR_HOST_URL} -Dsonar.login=${SONAR_GLOBAL_KEY} -Dsonar.working.directory="/tmp" -s /root/.m2/cellar-settings.xml'
                    }
                }
            }
        }
        stage('Produce bom.xml'){
            steps{
                container (name: 'cellar-builder') {
                    sh 'mvn org.cyclonedx:cyclonedx-maven-plugin:makeAggregateBom -s /root/.m2/cellar-settings.xml'
                }
            }
        }
        stage('Dependency-Track Analysis'){
            steps{
                container (name: 'cellar-builder') {
                    sh '''
                        cat > payload.json <<__HERE__
                        {
                            "project": "091c1943-d3ba-473b-ba80-a3d593d8136b",
                            "bom": "$(cat target/bom.xml |base64 -w 0 -)"
                        }
                        __HERE__
                    '''

                    sh '''
                        curl -X "PUT" ${DEPENDENCY_TRACK_URL} -H 'Content-Type: application/json' -H 'X-API-Key: '${DEPENDENCY_TRACK_API_KEY} -d @payload.json
                    '''
                }
            }
        }
    }
    post {
        changed {
            script {
                if (currentBuild.result == 'SUCCESS') {
                        rocketSend avatar: "http://d2-np.eurodyn.com/jenkins/jenkins.png", channel: 'cellar-ci', message: ":white_check_mark: | ${BUILD_URL} \n\nBuild succeeded on branch *${env.BRANCH_NAME}* \nChangelog: ${getChangeString(10)}", rawMessage: true
                } else {
                        rocketSend avatar: "http://d2-np.eurodyn.com/jenkins/jenkins.png", channel: 'cellar-ci', message: ":negative_squared_cross_mark: | ${BUILD_URL} \n\nBuild failed on branch *${env.BRANCH_NAME}* \nChangelog: ${getChangeString(10)}", rawMessage: true
                }
            }
            emailext subject: '$DEFAULT_SUBJECT',
                     body: '$DEFAULT_CONTENT',
                     to: 'cellar-dev@eurodyn.com'
        }
    }
}
@NonCPS
def getChangeString(maxMessages) {
    MAX_MSG_LEN = 100
    def changeString = ""

    def changeLogSets = currentBuild.changeSets

    for (int i = 0; i < changeLogSets.size(); i++) {
        def entries = changeLogSets[i].items
        for (int j = 0; j < entries.length && i + j < maxMessages; j++) {
            def entry = entries[j]
            truncated_msg = entry.msg.take(MAX_MSG_LEN)
            changeString += "*${truncated_msg}* _by author ${entry.author}_\n"
        }
    }

    if (!changeString) {
        changeString = " There have not been any changes since the last build"
    }

    return changeString
}